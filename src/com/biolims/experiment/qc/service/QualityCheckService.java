package com.biolims.experiment.qc.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.pooling.model.PoolingTemp;
import com.biolims.experiment.qc.dao.QualityCheckDao;
import com.biolims.experiment.qc.dao.WKQpcrSampleTaskDao;
import com.biolims.experiment.qc.dao.WKQualitySampleTaskDao;
import com.biolims.experiment.qc.model.Qc2100TaskTemp;
import com.biolims.experiment.qc.model.QcCheck;
import com.biolims.experiment.qc.model.QcQpcrTaskTemp;
import com.biolims.experiment.sequencing.model.SequencingTaskTemp;
import com.biolims.experiment.wk.dao.WkTaskDao;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.project.feedback.dao.QualityFeedbackDao;
import com.biolims.project.feedback.model.FeedbackQpcrQuality;
import com.biolims.project.feedback.model.FeedbackQuality;
import com.biolims.project.feedback.model.FeedbackWk;
import com.biolims.sample.model.SampleInfo;
import com.biolims.system.work.model.WorkOrder;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class QualityCheckService {
	@Resource
	private QualityCheckDao qualityCheckDao;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private WKQualitySampleTaskDao wKQualitySampleTaskDao;
	@Resource
	private WKQpcrSampleTaskDao wKQpcrSampleTaskDao;
	@Resource
	private QualityFeedbackDao qualityFeedbackDao;
	@Resource
	private WkTaskDao wkTaskDao;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findQualityCheckList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return qualityCheckDao.selectQualityCheckList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(QcCheck i) throws Exception {

		qualityCheckDao.saveOrUpdate(i);

	}
	public QcCheck get(String id) {
		QcCheck qualityCheck = commonDAO.get(QcCheck.class, id);
		return qualityCheck;
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(QcCheck sc, Map jsonMap) throws Exception {
		if (sc != null) {
			qualityCheckDao.saveOrUpdate(sc);
		
			String jsonStr = "";
		}
    }
	//保存文库质检审核
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void saveQualityCheck(String itemDataJson) throws Exception {
//		List<QcCheck> saveItems = new ArrayList<QcCheck>();
//		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
//		for (Map<String, Object> map : list) {
//			QcCheck sbi = new QcCheck();
//			sbi = (QcCheck) qualityCheckDao.Map2Bean(map, sbi);
//			if (sbi.getId() != null && sbi.getId().equals(""))
//				sbi.setId(null);
//			if(sbi!=null && sbi.getNextFlow()!=null && sbi.getResult()!=null){
//					String next=sbi.getNextFlow();
//					if(next!=null && next.equals("0")){
//						//将该样本的数据，存放到POOLING临时表
//						PoolingTemp plt = new PoolingTemp();
////						plt.setSampleCode(sbi.getSampleCode());
//						SampleInfo si = sampleInfoMainDao.findSampleInfo(sbi.getSampleCode());
//						if(si.getOrderId()!=null){
//							WorkOrder wo =sampleInfoMainDao.get(WorkOrder.class, si.getOrderId().getId());
//							if(wo!=null){
////								plt.setSequencingPlatform(wo.getSequencingPlatform());
////								plt.setSequencingReadLong(wo.getSequencingReadLong());
////								plt.setSequencingType(wo.getSequencingMethod());
////								plt.setMixRatio(wo.getMixRatio());
////								plt.setYw(wo.getYw());
////								plt.setDensity(wo.getDensity());
//							}
//						}
//						plt.setProductId(si.getProductId());
//						plt.setProductName(si.getProductName());
//						plt.setName(sbi.getName());
//						plt.setNote(sbi.getNote());
//						plt.setWkIndex(sbi.getIndexa());
//						plt.setConcentration(sbi.getQpcrCon());
//						plt.setState("1");
//						plt.setWkCode(sbi.getWkCode());
//						plt.setSampleCode(sbi.getSampleCode());
//						//plt.setConcentration(sbi.getQualityConcentrer());
//						plt.setClassify(sbi.getClassify());
//						poolingReceiveDao.saveOrUpdate(plt);
//					}else if(next!=null && next.equals("1")){//让步上机
//						//将该样本的数据，存放到上机测序临时表
//						SequencingTaskTemp st=new SequencingTaskTemp();
//						st.setSampleCode(sbi.getSampleCode());
//						st.setAcceptDate(sbi.getAcceptDate());
//						st.setIdCard(sbi.getIdCard());
//						st.setInspectDate(sbi.getInspectDate());
//						st.setName(sbi.getName());
//						st.setNote(sbi.getNote());
//						st.setOrderId(sbi.getOrderId());
//						st.setPatientName(sbi.getPatientName());
//						st.setPhone(sbi.getPhone());
//						st.setProductId(sbi.getProductId());
//						st.setProductName(sbi.getProductName());
//						st.setReportDate(sbi.getReportDate());
//						st.setSequenceFun(sbi.getSequenceFun());
//						st.setClassify(sbi.getClassify());
//						qualityCheckDao.saveOrUpdate(st);
//					}else if(next!=null && next.equals("2")){//重新纯化
//						if(sbi.getWkType().equals("0")){
//							//将该样本的数据，存放到2100文库质控临时表
//							Qc2100TaskTemp wt = new Qc2100TaskTemp();
//							wt.setCode(sbi.getCode());
//							wt.setWkCode(sbi.getWkCode());
//							wt.setName(sbi.getName());
//							wt.setSampleCode(sbi.getSampleCode());
//							wt.setWkType(sbi.getWkType());
////							wt.setWkVolume(sbi.getWkVolume());
////							wt.setFragminSize(sbi.getFragminSize());
//							wt.setResult(sbi.getResult());
//							wt.setReason(sbi.getReason());
//							wt.setFragminSize(sbi.getLength());
//							wt.setReason(sbi.getReason());
//							wt.setNote(sbi.getNote());
//							wt.setPatientName(sbi.getPatientName());
//							wt.setProductId(sbi.getProductId());
//							wt.setProductName(sbi.getProductName());
//							wt.setInspectDate(sbi.getInspectDate());
//							wt.setAcceptDate(sbi.getAcceptDate());
//							wt.setIdCard(sbi.getIdCard());
//							wt.setSequenceFun(sbi.getSequenceFun());
//							wt.setReportDate(sbi.getReportDate());
////							wt.setLocation(sbi.getLocation());
//							wt.setState("1");
//						   wt.setClassify(sbi.getClassify());
//							wKQualitySampleTaskDao.saveOrUpdate(wt);
//						}else{
//							//将该样本的数据，存放到QPCR文库质控临时表
//							QcQpcrTaskTemp wqt = new QcQpcrTaskTemp();
//							wqt.setCode(sbi.getCode());
//							wqt.setWkCode(sbi.getWkCode());
//							wqt.setName(sbi.getName());
//							wqt.setSampleCode(sbi.getSampleCode());
//							wqt.setWkType(sbi.getWkType());
////							wqt.setWkVolume(sbi.getWkVolume());
////							wqt.setFragminSize(sbi.getFragminSize());
//							wqt.setResult(sbi.getResult());
//							wqt.setReason(sbi.getReason());
//							wqt.setFragminSize(sbi.getLength());
//							wqt.setReason(sbi.getReason());
//							wqt.setNote(sbi.getNote());
//							wqt.setPatientName(sbi.getPatientName());
//							wqt.setProductId(sbi.getProductId());
//							wqt.setProductName(sbi.getProductName());
//							wqt.setInspectDate(sbi.getInspectDate());
//							wqt.setAcceptDate(sbi.getAcceptDate());
//							wqt.setIdCard(sbi.getIdCard());
//							wqt.setSequenceFun(sbi.getSequenceFun());
//							wqt.setReportDate(sbi.getReportDate());
////							wqt.setLocation(sbi.getLocation());
//							wqt.setState("1");
//						   wqt.setClassify(sbi.getClassify());
//							wKQpcrSampleTaskDao.saveOrUpdate(wqt);
//						}
//					}else if(next.equals("3")){//停止检测
//						//终止，改变SampleInfo中原始样本的状态为“实验终止”
//						if(sbi.getWkType().equals("0")){
//							FeedbackQuality qfb = new FeedbackQuality();
//							qfb.setCode(sbi.getCode());
//							qfb.setSampleCode(sbi.getSampleCode());
//							qfb.setWkCode(sbi.getWkCode());
//							qfb.setIndexa(sbi.getIndexa());
//							qfb.setName(sbi.getName());
//							qfb.setNextFlow(sbi.getNextFlow());
//							qfb.setResult(sbi.getResult());
//							qfb.setNote(sbi.getNote());
//							qfb.setPatientName(sbi.getPatientName());
//							qfb.setProductId(sbi.getProductId());
//							qfb.setProductName(sbi.getProductName());
//							qfb.setInspectDate(sbi.getInspectDate());
//							qfb.setAcceptDate(sbi.getAcceptDate());
//							qfb.setIdCard(sbi.getIdCard());
//							qfb.setReportDate(sbi.getReportDate());
//							qfb.setWkType(sbi.getWkType());
//							qfb.setState("1");
//							qfb.setClassify(sbi.getClassify());
//							qualityFeedbackDao.saveOrUpdate(qfb);
//						}else{
//							FeedbackQpcrQuality qfb = new FeedbackQpcrQuality();
//							qfb.setCode(sbi.getCode());
//							qfb.setSampleCode(sbi.getSampleCode());
//							qfb.setWkCode(sbi.getWkCode());
//							qfb.setIndexa(sbi.getIndexa());
//							qfb.setName(sbi.getName());
//							qfb.setNextFlow(sbi.getNextFlow());
//							qfb.setResult(sbi.getResult());
//							qfb.setNote(sbi.getNote());
//							qfb.setPatientName(sbi.getPatientName());
//							qfb.setProductId(sbi.getProductId());
//							qfb.setProductName(sbi.getProductName());
//							qfb.setInspectDate(sbi.getInspectDate());
//							qfb.setAcceptDate(sbi.getAcceptDate());
//							qfb.setIdCard(sbi.getIdCard());
//							qfb.setReportDate(sbi.getReportDate());
//							qfb.setWkType(sbi.getWkType());
//							qfb.setState("1");
//							qfb.setClassify(sbi.getClassify());
//							qualityFeedbackDao.saveOrUpdate(qfb);
//						}
//						SampleInfo sf=sampleInfoMainDao.findSampleInfo(sbi.getSampleCode());
//						if(sf!=null){
//							sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
//							sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
//						}
//					}else if(next.equals("4")){//向建库组反馈
//						//将该样本的数据，存放到项目管理的质控异常反馈
//						FeedbackWk qfb = new FeedbackWk();
//						qfb.setCode(sbi.getCode());
//						qfb.setSampleCode(sbi.getSampleCode());
//						qfb.setWkCode(sbi.getWkCode());
//						qfb.setIndexs(sbi.getIndexa());
//						qfb.setName(sbi.getName());
//						qfb.setNextflow(sbi.getNextFlow());
//						qfb.setResult(sbi.getResult());
//						qfb.setNote(sbi.getNote());
//						qfb.setPatientName(sbi.getPatientName());
//						qfb.setProductId(sbi.getProductId());
//						qfb.setProductName(sbi.getProductName());
//						qfb.setInspectDate(sbi.getInspectDate());
//						qfb.setAcceptDate(sbi.getAcceptDate());
//						qfb.setIdCard(sbi.getIdCard());
//						qfb.setReportDate(sbi.getReportDate());
//						qfb.setState("1");
//						qfb.setNote("QC NOT PASS");
//						qfb.setClassify(sbi.getClassify());
//						qualityFeedbackDao.saveOrUpdate(qfb);
////						//重建库
////						WkTaskTemp wt=new WkTaskTemp();
////						wt.setSampleCode(sbi.getSampleCode());
////						wt.setAcceptDate(sbi.getAcceptDate());
////						wt.setIdCard(sbi.getIdCard());
////						wt.setInspectDate(sbi.getInspectDate());
////						wt.setNote(sbi.getNote());
////						wt.setOrderId(sbi.getOrderId());
////						wt.setPatientName(sbi.getPatientName());
////						wt.setPhone(sbi.getPhone());
////						wt.setProductId(sbi.getProductId());
////						wt.setProductName(sbi.getProductName());
////						wt.setReportDate(sbi.getReportDate());
////						wt.setSequenceFun(sbi.getSequenceFun());
////						wt.setState("1");
////						wKSampleTaskDao.saveOrUpdate(wt);
//					}
////					else{
////						//重建库
////						WkTaskTemp wt=new WkTaskTemp();
////						//wt.setCode(sbi.getSampleCode());
////						wt.setSampleCode(sbi.getSampleCode());
////						wt.setState("1");
////						wKSampleTaskDao.saveOrUpdate(wt);
////					}
//				}
////			if(sbi.getHandleState().equals("0")&&sbi.getTestState().equals("0")){
////				//将该样本的数据，存放到POOLING临时表
////				PoolingTemp plt = new PoolingTemp();
////				plt.setCode(sbi.getSampleCode());
////				SampleInfo si = sampleInfoMainDao.findSampleInfo(sbi.getSampleCode());
////				WorkOrder wo =sampleInfoMainDao.get(WorkOrder.class, si.getOrderId().getId());
////				plt.setSequencingPlatform(wo.getSequencingPlatform());
////				plt.setSequencingReadLong(wo.getSequencingReadLong());
////				plt.setSequencingType(wo.getSequencingMethod());
////				plt.setProductId(si.getProductId());
////				plt.setProductName(si.getProductName());
////				plt.setName(sbi.getName());
////				plt.setNote(sbi.getNote());
////				plt.setWkIndex(sbi.getIndexa());
////				plt.setEndDate(sbi.getEndDate());
////				plt.setState("1");
////				
////				plt.setConcentration(sbi.getQualityConcentrer());
////				
////				poolingReceiveDao.saveOrUpdate(plt);
////				
////			}else if(sbi.getHandleState().equals("1")&&sbi.getTestState().equals("0")){
////				//将该样本的数据，存放到上机测序临时表
////				
////			}else if(sbi.getHandleState().equals("2")&&sbi.getTestState().equals("0")){
////				if(sbi.getWkType().equals("0")){
////					//将该样本的数据，存放到2100文库质控临时表
////					WKQualityTemp wt = new WKQualityTemp();
////					wt.setName(sbi.getName());
////					wt.setSampleCode(sbi.getSampleCode());
////					wt.setWkType(sbi.getWkType());
////					wt.setFragminSize(sbi.getLength());
////					wt.setReason(sbi.getFailReason());
////					wt.setNote(sbi.getNote());
////					wt.setCode(sbi.getWkCode());
////					wt.setEndDate(sbi.getEndDate());
////					wt.setState("1");
////				   
////					wKQualitySampleTaskDao.saveOrUpdate(wt);
////				}else{
////					//将该样本的数据，存放到QPCR文库质控临时表
////					WKQpcrTemp wqt = new WKQpcrTemp();
////					wqt.setName(sbi.getName());
////					wqt.setSampleCode(sbi.getSampleCode());
////					wqt.setWkType(sbi.getWkType());
////					wqt.setFragminSize(sbi.getLength());
////					wqt.setReason(sbi.getFailReason());
////					wqt.setNote(sbi.getNote());
////					wqt.setCode(sbi.getWkCode());
////					wqt.setEndDate(sbi.getEndDate());
////					wqt.setState("1");
////				   
////					wKQpcrSampleTaskDao.saveOrUpdate(wqt);
////				}
////			}else if(sbi.getHandleState().equals("4")&&sbi.getTestState().equals("1")){
////				//将该样本的数据，存放到项目管理的质控异常反馈
////				QualityFeedback qfb = new QualityFeedback();
////				qfb.setName(sbi.getName());
////				qfb.setSampleCode(sbi.getSampleCode());
////				qfb.setIsgood(sbi.getTestState());
////				qfb.setNextflow(sbi.getHandleState());
////				qfb.setIndexs(sbi.getIndexa());
////			    qfb.setWkCode(sbi.getWkCode());
////			    qfb.setEndDate(sbi.getEndDate());
////			    
////				qualityFeedbackDao.saveOrUpdate(qfb);
////			}else if(sbi.getHandleState().equals("5")&&sbi.getTestState().equals("0")){
////				//将更改后的结果，存放到文库构建临时表
////				List<WKTemp> wkt = wKSampleTaskDao.searchWKTempList();
////				for(WKTemp wkti : wkt){
////					if(wkti.getCode().equals(sbi.getSampleCode())){
////						wkti.setState("1");
////					}
////				}
////			}
//			saveItems.add(sbi);
//		}
//		qualityCheckDao.saveOrUpdateAll(saveItems);
//	}
	
}
