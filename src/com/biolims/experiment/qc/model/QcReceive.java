package com.biolims.experiment.qc.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.common.model.user.User;
/**   
 * @Title: Model
 * @Description: 文库接收
 * @author lims-platform
 * @date 2015-11-24 11:24:54
 * @version V1.0   
 *
 */
@Entity
@Table(name = "QC_RECEIVE")
@SuppressWarnings("serial")
public class QcReceive extends EntityDao<QcReceive> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
	/**接收人*/
	private User receiveUser;
	/**接收日期*/
	private String receiverDate;
	/**储位提示*/
	private String location;
	/**状态*/
	private String state;
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	/**工作流状态*/
	private String stateName;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得User
	 *@return: User  接收人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "RECEIVE_USER")
	public User getReceiveUser(){
		return this.receiveUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  接收人
	 */
	public void setReceiveUser(User receiveUser){
		this.receiveUser = receiveUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  接收日期
	 */
	@Column(name ="RECEIVER_DATE", length = 50)
	public String getReceiverDate(){
		return this.receiverDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  接收日期
	 */
	public void setReceiverDate(String receiverDate){
		this.receiverDate = receiverDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  储位提示
	 */
	@Column(name ="LOCATION", length = 50)
	public String getLocation(){
		return this.location;
	}
	/**
	 *方法: 设置String
	 *@param: String  储位提示
	 */
	public void setLocation(String location){
		this.location = location;
	}
	/**
	 *方法: 取得String
	 *@return: String  工作流状态
	 */
	@Column(name ="STATE_NAME", length = 50)
	public String getStateName(){
		return this.stateName;
	}
	/**
	 *方法: 设置String
	 *@param: String  工作流状态
	 */
	public void setStateName(String stateName){
		this.stateName = stateName;
	}
}