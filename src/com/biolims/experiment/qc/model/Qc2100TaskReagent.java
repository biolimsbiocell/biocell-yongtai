package com.biolims.experiment.qc.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 试剂明细
 * @author lims-platform
 * @date 2015-11-24 11:25:41
 * @version V1.0
 * 
 */
@Entity
@Table(name = "QC_2100_TASK_REAGENT")
@SuppressWarnings("serial")
public class Qc2100TaskReagent extends EntityDao<Qc2100TaskReagent> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 试剂编号 */
	private String code;
	/** 描述 */
	private String name;
	/* 单个用量 */
	private Double oneNum;
	/* 样本数量 */
	private Double sampleNum;
	/* 用量 */
	private Double num;
	/** 批次 */
	private String batch;
	/** 数量 */
	private Double count;
	/** 是否通过检验 */
	private String isTestSuccess;
	/** 相关主表 */
	private Qc2100Task qc2100Task;
	// 关联步骤的id
	private String itemId;
	// 模板试剂ID
	private String tReagent;
	// 备注
	private String note;
	// sn
	private String sn;

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 试剂编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 试剂编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 100)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "ONE_NUM", length = 50)
	public Double getOneNum() {
		return oneNum;
	}

	public void setOneNum(Double oneNum) {
		this.oneNum = oneNum;
	}

	@Column(name = "SAMPLE_NUM", length = 50)
	public Double getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(Double sampleNum) {
		this.sampleNum = sampleNum;
	}

	@Column(name = "NUM", length = 50)
	public Double getNum() {
		return num;
	}

	public void setNum(Double num) {
		this.num = num;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 批次
	 */
	@Column(name = "BATCH", length = 50)
	public String getBatch() {
		return this.batch;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 批次
	 */
	public void setBatch(String batch) {
		this.batch = batch;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 数量
	 */
	@Column(name = "COUNT", length = 50)
	public Double getCount() {
		return this.count;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 数量
	 */
	public void setCount(Double count) {
		this.count = count;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 是否通过检验
	 */
	@Column(name = "IS_TEST_SUCCESS", length = 50)
	public String getIsTestSuccess() {
		return this.isTestSuccess;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 是否通过检验
	 */
	public void setIsTestSuccess(String isTestSuccess) {
		this.isTestSuccess = isTestSuccess;
	}

	/**
	 * 方法: 取得WKQuality
	 * 
	 * @return: WKQuality 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "WK_QUALITY_SAMPLE_TASK")
	public Qc2100Task getQc2100Task() {
		return qc2100Task;
	}

	public void setQc2100Task(Qc2100Task qc2100Task) {
		this.qc2100Task = qc2100Task;
	}

	@Column(name = "CODE", length = 20)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String gettReagent() {
		return tReagent;
	}

	public void settReagent(String tReagent) {
		this.tReagent = tReagent;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

}