package com.biolims.experiment.qc.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.system.template.model.Template;
import com.biolims.common.model.user.User;
import com.biolims.common.model.user.User;
import com.biolims.core.model.user.UserGroup;

/**
 * @Title: Model
 * @Description: QPCR质控
 * @author lims-platform
 * @date 2015-11-24 11:25:59
 * @version V1.0
 * 
 */
@Entity
@Table(name = "QC_QPCR_TASK")
@SuppressWarnings("serial")
public class QcQpcrTask extends EntityDao<QcQpcrTask> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 描述 */
	private String name;
	/** 下达人 */
	private User releasUser;
	/** 下达日期 */
	private String releaseDate;
	// 审核人
	private User confirmUser;
	/** 完成时间 */
	private Date confirmDate;
	/** 实验员 */
	private User testUser;
	/** 实验日期 */
	private Date testDate;
	/** 选择执行单 */
	private Template template;
	/* index */
	private String indexa;
	/* 工作流状态ID */
	private String state;
	/** 工作流状态 */
	private String stateName;
	/** 容器数量 */
	private Integer maxNum;
	/** 实验组 */
	private UserGroup acceptUser;

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 100)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 下达人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "RELEAS_USER")
	public User getReleasUser() {
		return this.releasUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 下达人
	 */
	public void setReleasUser(User releasUser) {
		this.releasUser = releasUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 下达日期
	 */
	@Column(name = "RELEASE_DATE", length = 50)
	public String getReleaseDate() {
		return this.releaseDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 下达日期
	 */
	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 实验员
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TEST_USER")
	public User getTestUser() {
		return this.testUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 实验员
	 */
	public void setTestUser(User testUser) {
		this.testUser = testUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 实验日期
	 */
	@Column(name = "TEST_DATE", length = 50)
	public Date getTestDate() {
		return this.testDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 实验日期
	 */
	public void setTestDate(Date testDate) {
		this.testDate = testDate;
	}

	/**
	 * 方法: 取得Template
	 * 
	 * @return: Template 选择执行单
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TEMPLATE")
	public Template getTemplate() {
		return this.template;
	}

	/**
	 * 方法: 设置Template
	 * 
	 * @param: Template 选择执行单
	 */
	public void setTemplate(Template template) {
		this.template = template;
	}

	@Column(name = "INDEXA", length = 50)
	public String getIndexa() {
		return indexa;
	}

	public void setIndexa(String indexa) {
		this.indexa = indexa;
	}

	@Column(name = "STATE", length = 50)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 工作流状态
	 */
	@Column(name = "STATE_NAME", length = 50)
	public String getStateName() {
		return this.stateName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 工作流状态
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "T_USER_GROUP")
	public UserGroup getAcceptUser() {
		return acceptUser;
	}

	public Integer getMaxNum() {
		return maxNum;
	}

	public void setMaxNum(Integer maxNum) {
		this.maxNum = maxNum;
	}

	public void setAcceptUser(UserGroup acceptUser) {
		this.acceptUser = acceptUser;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CONFIRM_USER")
	public User getConfirmUser() {
		return confirmUser;
	}

	public void setConfirmUser(User confirmUser) {
		this.confirmUser = confirmUser;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

}