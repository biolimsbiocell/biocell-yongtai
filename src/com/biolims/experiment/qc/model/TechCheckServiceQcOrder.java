package com.biolims.experiment.qc.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 检测任务单
 * @author lims-platform
 * @date 2016-02-18 11:37:19
 * @version V1.0   
 *
 */
@Entity
@Table(name = "TECH_CHECK_SERVICE_QC_ORDER")
@SuppressWarnings("serial")
public class TechCheckServiceQcOrder extends EntityDao<TechCheckServiceQcOrder> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
	/**状态*/
	private String state;
	/**状态*/
	private String stateName;
	/**项目编号*/
	private String projectId;
	/**项目名称*/
	private String projectName;
	/**合同编号*/
	private String contractId;
	/**任务单类型*/
	private String orderType;
	/**合作单位*/
	private String cooperation;
	/**项目负责人*/
	private String projectLeader;
	/**实验负责人*/
	private String testLeader;
	/**信息负责人*/
	private String analysisLeader;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE_NAME", length = 50)
	public String getStateName(){
		return this.stateName;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setStateName(String stateName){
		this.stateName = stateName;
	}
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public String getContractId() {
		return contractId;
	}
	public void setContractId(String contractId) {
		this.contractId = contractId;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getCooperation() {
		return cooperation;
	}
	public void setCooperation(String cooperation) {
		this.cooperation = cooperation;
	}
	public String getProjectLeader() {
		return projectLeader;
	}
	public void setProjectLeader(String projectLeader) {
		this.projectLeader = projectLeader;
	}
	public String getTestLeader() {
		return testLeader;
	}
	public void setTestLeader(String testLeader) {
		this.testLeader = testLeader;
	}
	public String getAnalysisLeader() {
		return analysisLeader;
	}
	public void setAnalysisLeader(String analysisLeader) {
		this.analysisLeader = analysisLeader;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
}