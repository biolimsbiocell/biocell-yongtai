package com.biolims.experiment.qc.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.system.product.model.Product;
import com.biolims.system.template.model.Template;

/**
 * @Title: Model
 * @Description: QPCR结果明细
 * @author lims-platform
 * @date 2015-11-24 11:25:39
 * @version V1.0
 * 
 */
@Entity
@Table(name = "SAMPLE_QC_POOLING_INFO")
@SuppressWarnings("serial")
public class SampleQcPoolingInfo extends EntityDao<SampleQcPoolingInfo>
		implements java.io.Serializable {
	/** 编号 */
	private String id;
	/* pooling编号 */
	private String poolingCode;
	/** 样本编号 */
	private String code;
	/** 原始样本编号 */
	private String sampleCode;
	/* 任务单编号 */
	private String orderId;
	/** 文库号 */
	private String wkId;
	/* INDEX */
	private String indexa;
	/** 选择模板 */
	private Template template;
	/** 测序读长 */
	private String sequencingReadLong;
	/** 测序类型 */
	private String sequencingType;
	/** 测序平台 */
	private String sequencingPlatform;
	/** 业务类型 */
	private Product product;
	/** 总数据量 */
	private Integer totalAmount;
	/** 总体积 */
	private String totalVolume;
	/** 片段长度 */
	private Double fragmentSize;
	/** 浓度 */
	private Double concentration;
	/** qpcr浓度 */
	private Double qpcr;
	/** 比值 */
	private String specific;
	/** 处理结果 */
	private String result;
	/** 下一步流向 */
	private String nextFlow;
	/* 是否提交 */
	private String submit;
	// /**处理方式*/
	// private String handleWay;
	/** 异常原因 */
	private String reason;
	/** 备注 */
	private String note;
	/* 患者姓名 */
	private String patientName;
	/* 检测项目 */
	private String productId;
	/* 检测项目 */
	private String productName;
	/* 取样日期 */
	private String inspectDate;
	/* 接收日期 */
	private Date acceptDate;
	/* 身份证 */
	private String idCard;
	/* 手机号 */
	private String phone;
	/* 检测方法 */
	private String sequenceFun;
	/* 应出报告日期 */
	private Date reportDate;
	/* 状态 */
	private String state;
	/* 处理意见 */
	private String method;
	/* 确认执行 */
	private String isExecute;
	/** 相关主表 */
	private QcQpcrTask qcQpcrTask;
	/** 描述 */
	private String name;
	/** 文库类型 */
	private String wkType;
	/** 临时表Id */
	private String tempId;

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "POOLING_CODE", length = 20)
	public String getPoolingCode() {
		return poolingCode;
	}

	public void setPoolingCode(String poolingCode) {
		this.poolingCode = poolingCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 文库号
	 */
	@Column(name = "WK_ID", length = 20)
	public String getWkId() {
		return this.wkId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 文库号
	 */
	public void setWkId(String wkId) {
		this.wkId = wkId;
	}

	@Column(name = "SAMPLE_CODE", length = 100)
	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	@Column(name = "ORDER_ID", length = 50)
	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	@Column(name = "INDEXA", length = 50)
	public String getIndexa() {
		return indexa;
	}

	public void setIndexa(String indexa) {
		this.indexa = indexa;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TEMPLATE")
	public Template getTemplate() {
		return template;
	}

	public void setTemplate(Template template) {
		this.template = template;
	}

	// 读长
	@Column(name = "SEQUENCING_READLONG", length = 50)
	public String getSequencingReadLong() {
		return sequencingReadLong;
	}

	public void setSequencingReadLong(String sequencingReadLong) {
		this.sequencingReadLong = sequencingReadLong;
	}

	// 测序类型
	@Column(name = "SEQUENCING_TYPE", length = 50)
	public String getSequencingType() {
		return sequencingType;
	}

	public void setSequencingType(String sequencingType) {
		this.sequencingType = sequencingType;
	}

	// 测序平台
	@Column(name = "SEQUENCING_PLATFORM", length = 50)
	public String getSequencingPlatform() {
		return sequencingPlatform;
	}

	public void setSequencingPlatform(String sequencingPlatform) {
		this.sequencingPlatform = sequencingPlatform;
	}

	// 业务类型
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PRODUCT")
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 总数据量
	 */
	@Column(name = "TOTAL_AMOUNT", length = 50)
	public Integer getTotalAmount() {
		return totalAmount;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 总数据量
	 */
	public void setTotalAmount(Integer totalAmount) {
		this.totalAmount = totalAmount;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 总体积
	 */
	@Column(name = "TOTAL_VOLUME", length = 50)
	public String getTotalVolume() {
		return totalVolume;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 总体积
	 */
	public void setTotalVolume(String totalVolume) {
		this.totalVolume = totalVolume;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 片段长度
	 */
	@Column(name = "FRAGMENT_SIZE", length = 50)
	public Double getFragmentSize() {
		return this.fragmentSize;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 片段长度
	 */
	public void setFragmentSize(Double fragmentSize) {
		this.fragmentSize = fragmentSize;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 浓度
	 */
	@Column(name = "CONCENTRATION", length = 50)
	public Double getConcentration() {
		return this.concentration;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 浓度
	 */
	public void setConcentration(Double concentration) {
		this.concentration = concentration;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double qpcr浓度
	 */
	@Column(name = "QPCR", length = 50)
	public Double getQpcr() {
		return this.qpcr;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double qpcr浓度
	 */
	public void setQpcr(Double qpcr) {
		this.qpcr = qpcr;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 比值
	 */
	@Column(name = "SPECIFIC_", length = 100)
	public String getSpecific() {
		return this.specific;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 比值
	 */
	public void setSpecific(String specific) {
		this.specific = specific;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 结果判定
	 */
	@Column(name = "RESULT", length = 50)
	public String getResult() {
		return this.result;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 结果判定
	 */
	public void setResult(String result) {
		this.result = result;
	}

	@Column(name = "NEXT_FLOW", length = 50)
	public String getNextFlow() {
		return nextFlow;
	}

	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	@Column(name = "SUBMIT", length = 50)
	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

	// /**
	// *方法: 取得String
	// *@return: String 处理方式
	// */
	// @Column(name ="HANDLE_WAY", length = 50)
	// public String getHandleWay(){
	// return this.handleWay;
	// }
	// /**
	// *方法: 设置String
	// *@param: String 处理方式
	// */
	// public void setHandleWay(String handleWay){
	// this.handleWay = handleWay;
	// }
	@Column(name = "REASON", length = 20)
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 150)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	@Column(name = "PATIENT_NAME", length = 20)
	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	@Column(name = "PRODUCT_ID", length = 20)
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Column(name = "PRODUCT_NAME", length = 20)
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name = "INSPECT_DATE", length = 20)
	public String getInspectDate() {
		return inspectDate;
	}

	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}

	@Column(name = "ACCEPT_DATE", length = 20)
	public Date getAcceptDate() {
		return acceptDate;
	}

	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	@Column(name = "ID_CARD", length = 20)
	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	@Column(name = "PHONE", length = 20)
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Column(name = "SEQUENCE_FUN", length = 20)
	public String getSequenceFun() {
		return sequenceFun;
	}

	public void setSequenceFun(String sequenceFun) {
		this.sequenceFun = sequenceFun;
	}

	@Column(name = "REPORT_DATE", length = 20)
	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	@Column(name = "STATE", length = 50)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "METHOD", length = 20)
	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	@Column(name = "IS_EXECUTE", length = 20)
	public String getIsExecute() {
		return isExecute;
	}

	public void setIsExecute(String isExecute) {
		this.isExecute = isExecute;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "QC_QPCR_TASK")
	public QcQpcrTask getQcQpcrTask() {
		return qcQpcrTask;
	}

	public void setQcQpcrTask(QcQpcrTask qcQpcrTask) {
		this.qcQpcrTask = qcQpcrTask;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 50)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "WK_TYPE", length = 20)
	public String getWkType() {
		return wkType;
	}

	public void setWkType(String wkType) {
		this.wkType = wkType;
	}

	@Column(name = "CODE", length = 20)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}