package com.biolims.experiment.qc.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.system.product.model.Product;
import com.biolims.system.template.model.Template;
import com.biolims.common.model.user.User;
import com.biolims.common.model.user.User;
/**   
 * @Title: Model
 * @Description: 临时表2
 * @author lims-platform
 * @date 2015-11-29 19:16:00
 * @version V1.0   
 *
 */
@Entity
@Table(name = "QC_POOLING_TASK_TEMP")
@SuppressWarnings("serial")
public class QcPoolingTaskTemp extends EntityDao<QcPoolingTaskTemp> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
	/**实验员*/
	private User reciveUser;
	/**实验时间*/
	private Date reciveDate;
	/**下达人*/
	private User createUser;
	/**下达时间*/
	private Date createDate;
	/**状态*/
	private String state;
	/**备注*/
	private String note;
	/*POOLING编号*/
	private String poolingCode;
	/**选择模板*/
	private Template template;
	/**测序读长*/
	private String sequencingReadLong;
	/**测序类型*/
	private String sequencingType;
	/**测序平台*/
	private String sequencingPlatform;
	/**业务类型*/
	private Product product;
	/**总数据量*/
	private Integer totalAmount;
	/**总体积*/
	private String totalVolume;
	/**其他*/
	private String others;
	/**应出报告日期*/
	private Date reportDate;
	/**身份证号*/
	private String idCard;
	/**检测方法*/
	private String sequencingFun;
	/**取样日期*/
	private String inspectDate;
	/**接收日期*/
	private Date acceptDate;
	/**结果*/
	private String result;
	/**任务单id*/
	private String orderId;
	/**手机号*/
	private String phone;
	/**检测项目*/
	private String productId;
	/**检测项目*/
	private String productName;
	/**患者姓名*/
	private String patient;
	//处理意见
	private String method;
	//原因
	private String reason;
	/**理论QPCR浓度*/
	private String qpcrConcentration;
	
	
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得User
	 *@return: User  实验员
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "RECIVE_USER")
	public User getReciveUser(){
		return this.reciveUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  实验员
	 */
	public void setReciveUser(User reciveUser){
		this.reciveUser = reciveUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  实验时间
	 */
	@Column(name ="RECIVE_DATE", length = 50)
	public Date getReciveDate(){
		return this.reciveDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  实验时间
	 */
	public void setReciveDate(Date reciveDate){
		this.reciveDate = reciveDate;
	}
	/**
	 *方法: 取得User
	 *@return: User  下达人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser(){
		return this.createUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  下达人
	 */
	public void setCreateUser(User createUser){
		this.createUser = createUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  下达时间
	 */
	@Column(name ="CREATE_DATE", length = 50)
	public Date getCreateDate(){
		return this.createDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  下达时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态id
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态id
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	@Column(name ="POOLING_CODE", length = 50)
	public String getPoolingCode() {
		return poolingCode;
	}
	public void setPoolingCode(String poolingCode) {
		this.poolingCode = poolingCode;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TEMPLATE")
	public Template getTemplate() {
		return template;
	}
	public void setTemplate(Template template) {
		this.template = template;
	}
	//读长
	@Column(name ="SEQUENCING_READLONG", length = 50)
	public String getSequencingReadLong() {
		return sequencingReadLong;
	}
	public void setSequencingReadLong(String sequencingReadLong) {
		this.sequencingReadLong = sequencingReadLong;
	}
	//测序类型
	@Column(name ="SEQUENCING_TYPE", length = 50)
	public String getSequencingType() {
		return sequencingType;
	}
	public void setSequencingType(String sequencingType) {
		this.sequencingType = sequencingType;
	}
	//测序平台
	@Column(name ="SEQUENCING_PLATFORM", length = 50)
	public String getSequencingPlatform() {
		return sequencingPlatform;
	}
	public void setSequencingPlatform(String sequencingPlatform) {
		this.sequencingPlatform = sequencingPlatform;
	}
	//业务类型
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PRODUCT")
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	/**
	 *方法: 取得Double
	 *@return: Double  总数据量
	 */
	@Column(name ="TOTAL_AMOUNT", length = 50)
	public Integer getTotalAmount() {
		return totalAmount;
	}
	/**
	 *方法: 设置Double
	 *@param: Double  总数据量
	 */
	public void setTotalAmount(Integer totalAmount) {
		this.totalAmount = totalAmount;
	}
	/**
	 *方法: 取得Double
	 *@return: Double  总体积
	 */
	@Column(name ="TOTAL_VOLUME", length = 50)
	public String getTotalVolume() {
		return totalVolume;
	}
	/**
	 *方法: 设置Double
	 *@param: Double  总体积
	 */
	public void setTotalVolume(String totalVolume) {
		this.totalVolume = totalVolume;
	}
	/**
	 *方法: 取得String
	 *@return: String  其他
	 */
	@Column(name ="OTHERS", length = 50)
	public String getOthers() {
		return others;
	}
	/**
	 *方法: 设置String
	 *@param: String  其他
	 */
	public void setOthers(String others) {
		this.others = others;
	}
	@Column(name ="REPORT_DATE", length = 50)
	public Date getReportDate() {
		return reportDate;
	}
	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}
	public String getIdCard() {
		return idCard;
	}
	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	public String getSequencingFun() {
		return sequencingFun;
	}
	public void setSequencingFun(String sequencingFun) {
		this.sequencingFun = sequencingFun;
	}
	public String getInspectDate() {
		return inspectDate;
	}
	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}
	public Date getAcceptDate() {
		return acceptDate;
	}
	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getPatient() {
		return patient;
	}
	public void setPatient(String patient) {
		this.patient = patient;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getQpcrConcentration() {
		return qpcrConcentration;
	}
	public void setQpcrConcentration(String qpcrConcentration) {
		this.qpcrConcentration = qpcrConcentration;
	}
	
}