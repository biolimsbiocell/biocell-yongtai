package com.biolims.experiment.qc.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: QPCR执行步骤明细
 * @author lims-platform
 * @date 2015-11-24 11:25:34
 * @version V1.0   
 *
 */
@Entity
@Table(name = "QC_QPCR_TASK_TEMPLATE")
@SuppressWarnings("serial")
public class QcQpcrTaskTemplate extends EntityDao<QcQpcrTaskTemplate> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**实验员*/
	private User reciveUser;
	/**步骤编号*/
	private String code;
	/**描述*/
	private String name;
	/**步骤名称*/
	private String stepName;
	/**步骤描述*/
	private String tItem;
	/**开始时间*/
	private String startTime;
	/**结束时间*/
	private String endTime;
	/**备注*/
	private String note;
	/**状态*/
	private String state;
	/*关联样本*/
	private String codes;
	/**相关主表*/
	private QcQpcrTask qcQpcrTask;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "RECIVE_USER")
	public User getReciveUser() {
		return reciveUser;
	}
	public void setReciveUser(User reciveUser) {
		this.reciveUser = reciveUser;
	}
	/**
	 *方法: 取得String
	 *@return: String  步骤名称
	 */
	@Column(name ="STEP_NAME", length = 100)
	public String getStepName(){
		return this.stepName;
	}
	/**
	 *方法: 设置String
	 *@param: String  步骤名称
	 */
	public void setStepName(String stepName){
		this.stepName = stepName;
	}
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String gettItem() {
		return tItem;
	}
	public void settItem(String tItem) {
		this.tItem = tItem;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  开始时间
	 */
	@Column(name ="START_TIME", length = 50)
	public String getStartTime(){
		return this.startTime;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  开始时间
	 */
	public void setStartTime(String startTime){
		this.startTime = startTime;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  结束时间
	 */
	@Column(name ="END_TIME", length = 50)
	public String getEndTime(){
		return this.endTime;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  结束时间
	 */
	public void setEndTime(String endTime){
		this.endTime = endTime;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	@Column(name ="CODES", length = 5000)
	public String getCodes() {
		return codes;
	}
	public void setCodes(String codes) {
		this.codes = codes;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得WKQuality
	 *@return: WKQuality  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "QC_QPCR_TASK")
	public QcQpcrTask getQcQpcrTask() {
		return qcQpcrTask;
	}
	public void setQcQpcrTask(QcQpcrTask qcQpcrTask) {
		this.qcQpcrTask = qcQpcrTask;
	}
	
	@Column(name="CODE",length=20)
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
}