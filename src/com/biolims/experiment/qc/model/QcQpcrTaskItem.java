package com.biolims.experiment.qc.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.sample.model.SampleInfo;
import com.biolims.system.quality.model.QualityProduct;

/**
 * @Title: Model
 * @Description: QPCR质控明细
 * @author lims-platform
 * @date 2015-11-24 11:25:31
 * @version V1.0
 * 
 */
@Entity
@Table(name = "QC_QPCR_TASK_ITEM")
@SuppressWarnings("serial")
public class QcQpcrTaskItem extends EntityDao<QcQpcrTaskItem> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/* 序号 */
	private Integer orderNumber;
	/** 样本编号 */
	private String code;
	/** 原始样本编号 */
	private String sampleCode;
	/* 任务单编号 */
	private String orderId;
	/** 文库号 */
	private String wkId;
	/* index */
	private String indexa;
	/** 阴性样本号 */
	private String femaleSampleNo;
	/** 摩尔浓度 */
	private Double molality;
	/** 处理结果 */
	private String result;
	/** 失败原因 */
	private String reason;
	/** 步骤编号 */
	private String stepNum;
	/** 描述 */
	private String name;
	/** 文库类型 */
	private String wkType;
	/* 患者姓名 */
	private String patientName;
	/* 检测项目 */
	private String productId;
	/* 检测项目 */
	private String productName;
	/* 取样日期 */
	private String inspectDate;
	/* 接收日期 */
	private Date acceptDate;
	/* 身份证 */
	private String idCard;
	/* 手机号 */
	private String phone;
	/* 检测方法 */
	private String sequenceFun;
	/* 应出报告日期 */
	private Date reportDate;
	/* 质控品 */
	private QualityProduct qualityProduct;
	/** 相关主表 */
	private QcQpcrTask qcQpcrTask;
	/** 行号 */
	private String rowCode;
	/** 列号 */
	private String colCode;
	/** 板号 */
	private String counts;
	/** 体积 */
	private Double volume;
	/** 单位 */
	private String unit;

	/** 科技服务任务单 */
	private String techTaskId;
	// 合同ID
	private String contractId;
	// 项目ID
	private String projectId;
	/** 任务单类型 */
	private String orderType;
	// 区分临床还是科技服务 0 临床 1 科技服务
	private String classify;
	// 样本类型
	private String sampleType;
	// i5
	private String i5;
	// i7
	private String i7;
	/** 样本数量 */
	private Double sampleNum;
	// 样本用量
	private Double sampleConsume;
	/** 临时表Id */
	private String tempId;
	// 文库浓度
	private Double wkConcentration;
	// 文库体积
	private Double wkVolume;
	// 文库总量
	private Double wkSumTotal;
	// 循环数
	private String loopNum;
	// 扩增比例
	private Double pcrRatio;
	// RIN
	private String rin;
	// 实验室样本号
	private String labCode;
	/** 样本主数据 */
	private SampleInfo sampleInfo;

	/**
	 * 方法: 取得SampleInfo
	 * 
	 * @return: 样本主数据
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}

	/**
	 * 方法: 设置SampleInfo
	 * 
	 * @param: 样本主数据
	 */
	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}

	public String getLabCode() {
		return labCode;
	}

	public void setLabCode(String labCode) {
		this.labCode = labCode;
	}

	public Double getWkConcentration() {
		return wkConcentration;
	}

	public void setWkConcentration(Double wkConcentration) {
		this.wkConcentration = wkConcentration;
	}

	public Double getWkVolume() {
		return wkVolume;
	}

	public void setWkVolume(Double wkVolume) {
		this.wkVolume = wkVolume;
	}

	public Double getWkSumTotal() {
		return wkSumTotal;
	}

	public void setWkSumTotal(Double wkSumTotal) {
		this.wkSumTotal = wkSumTotal;
	}

	public String getLoopNum() {
		return loopNum;
	}

	public void setLoopNum(String loopNum) {
		this.loopNum = loopNum;
	}

	public Double getPcrRatio() {
		return pcrRatio;
	}

	public void setPcrRatio(Double pcrRatio) {
		this.pcrRatio = pcrRatio;
	}

	public String getRin() {
		return rin;
	}

	public void setRin(String rin) {
		this.rin = rin;
	}

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	public Double getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(Double sampleNum) {
		this.sampleNum = sampleNum;
	}

	public Double getSampleConsume() {
		return sampleConsume;
	}

	public void setSampleConsume(Double sampleConsume) {
		this.sampleConsume = sampleConsume;
	}

	public String getI5() {
		return i5;
	}

	public void setI5(String i5) {
		this.i5 = i5;
	}

	public String getI7() {
		return i7;
	}

	public void setI7(String i7) {
		this.i7 = i7;
	}

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "ORDER_NUMBER", length = 20)
	public Integer getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}

	@Column(name = "CODE", length = 20)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "SAMPLE_CODE", length = 20)
	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	@Column(name = "ORDER_ID", length = 50)
	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 文库号
	 */
	@Column(name = "WK_ID", length = 50)
	public String getWkId() {
		return this.wkId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 文库号
	 */
	public void setWkId(String wkId) {
		this.wkId = wkId;
	}

	public String getIndexa() {
		return indexa;
	}

	public void setIndexa(String indexa) {
		this.indexa = indexa;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 阴性样本号
	 */
	@Column(name = "FEMALE_SAMPLE_NO", length = 50)
	public String getFemaleSampleNo() {
		return this.femaleSampleNo;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 阴性样本号
	 */
	public void setFemaleSampleNo(String femaleSampleNo) {
		this.femaleSampleNo = femaleSampleNo;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 摩尔浓度
	 */
	@Column(name = "MOLALITY", length = 50)
	public Double getMolality() {
		return this.molality;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 摩尔浓度
	 */
	public void setMolality(Double molality) {
		this.molality = molality;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 失败原因
	 */
	@Column(name = "REASON", length = 50)
	public String getReason() {
		return this.reason;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 失败原因
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	@Column(name = "STEP_NUM", length = 50)
	public String getStepNum() {
		return stepNum;
	}

	public void setStepNum(String stepNum) {
		this.stepNum = stepNum;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 50)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "WK_TYPE", length = 50)
	public String getWkType() {
		return wkType;
	}

	public void setWkType(String wkType) {
		this.wkType = wkType;
	}

	@Column(name = "PATIENT_NAME", length = 20)
	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	@Column(name = "PRODUCT_ID", length = 20)
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Column(name = "PRODUCT_NAME", length = 20)
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name = "INSPECT_DATE", length = 20)
	public String getInspectDate() {
		return inspectDate;
	}

	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}

	@Column(name = "ACCEPT_DATE", length = 20)
	public Date getAcceptDate() {
		return acceptDate;
	}

	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	@Column(name = "ID_CARD", length = 20)
	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	@Column(name = "PHONE", length = 20)
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Column(name = "SEQUENCE_FUN", length = 20)
	public String getSequenceFun() {
		return sequenceFun;
	}

	public void setSequenceFun(String sequenceFun) {
		this.sequenceFun = sequenceFun;
	}

	@Column(name = "REPORT_DATE", length = 20)
	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "QUALITY_PRODUCT")
	public QualityProduct getQualityProduct() {
		return qualityProduct;
	}

	public void setQualityProduct(QualityProduct qualityProduct) {
		this.qualityProduct = qualityProduct;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "QC_QPCR_TASK")
	public QcQpcrTask getQcQpcrTask() {
		return qcQpcrTask;
	}

	public void setQcQpcrTask(QcQpcrTask qcQpcrTask) {
		this.qcQpcrTask = qcQpcrTask;
	}

	public String getRowCode() {
		return rowCode;
	}

	public void setRowCode(String rowCode) {
		this.rowCode = rowCode;
	}

	public String getColCode() {
		return colCode;
	}

	public void setColCode(String colCode) {
		this.colCode = colCode;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getCounts() {
		return counts;
	}

	public void setCounts(String counts) {
		this.counts = counts;
	}

	public String getTechTaskId() {
		return techTaskId;
	}

	public void setTechTaskId(String techTaskId) {
		this.techTaskId = techTaskId;
	}

	public String getContractId() {
		return contractId;
	}

	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

}