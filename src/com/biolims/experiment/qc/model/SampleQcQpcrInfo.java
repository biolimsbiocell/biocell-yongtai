package com.biolims.experiment.qc.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.sample.model.SampleInfo;

/**
 * @Title: Model
 * @Description: QPCR结果明细
 * @author lims-platform
 * @date 2015-11-24 11:25:39
 * @version V1.0
 * 
 */
@Entity
@Table(name = "SAMPLE_QC_QPCR_INFO")
@SuppressWarnings("serial")
public class SampleQcQpcrInfo extends EntityDao<SampleQcQpcrInfo> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 样本编号 */
	private String code;
	/** 原始样本编号 */
	private String sampleCode;
	/* 任务单编号 */
	private String orderId;
	/** 文库号 */
	private String wkId;
	/* INDEX */
	private String indexa;
	/** 片段长度 */
	private Double fragmentSize;
	/** 浓度 */
	private Double concentration;
	/** qpcr浓度 */
	private Double qpcr;
	/** 比值 */
	private Double specific;
	/** 处理结果 */
	private String result;
	/** 下一步流向ID */
	private String nextFlowId;
	/** 下一步流向 */
	private String nextFlow;
	/* 是否提交 */
	private String submit;
	// /**处理方式*/
	// private String handleWay;
	/** 异常原因 */
	private String reason;
	/** 备注 */
	private String note;
	/* 患者姓名 */
	private String patientName;
	/* 检测项目 */
	private String productId;
	/* 检测项目 */
	private String productName;
	/* 取样日期 */
	private String inspectDate;
	/* 接收日期 */
	private Date acceptDate;
	/* 身份证 */
	private String idCard;
	/* 手机号 */
	private String phone;
	/* 检测方法 */
	private String sequenceFun;
	/* 应出报告日期 */
	private Date reportDate;
	/* 状态 */
	private String state;
	/* 处理意见 */
	private String method;
	/* 确认执行 */
	private String isExecute;
	/** 相关主表 */
	private QcQpcrTask qcQpcrTask;
	/** 描述 */
	private String name;
	/** 文库类型 */
	private String wkType;
	/** 体积 */
	private Double volume;
	/** 单位 */
	private String unit;

	/** 科技服务任务单 */
	private String techTaskId;
	// 合同ID
	private String contractId;
	// 项目ID
	private String projectId;
	/** 任务单类型 */
	private String orderType;
	// 区分临床还是科技服务 0 临床 1 科技服务
	private String classify;
	// 样本类型
	private String sampleType;
	// i5
	private String i5;
	// i7
	private String i7;
	/** 样本数量 */
	private Double sampleNum;
	/** 临时表Id */
	private String tempId;
	// Her2
	private String her2;
	// ER
	private String er;
	// PR
	private String pr;
	// RS
	private String rs;
	// 文库浓度
	private Double wkConcentration;
	// 文库体积
	private Double wkVolume;
	// 文库总量
	private Double wkSumTotal;
	// 循环数
	private String loopNum;
	// 扩增比例
	private Double pcrRatio;
	// RIN
	private String rin;
	// 实验室样本号
	private String labCode;
	/** 样本主数据 */
	private SampleInfo sampleInfo;

	/**
	 * 方法: 取得SampleInfo
	 * 
	 * @return: 样本主数据
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}

	/**
	 * 方法: 设置SampleInfo
	 * 
	 * @param: 样本主数据
	 */
	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}

	public String getLabCode() {
		return labCode;
	}

	public void setLabCode(String labCode) {
		this.labCode = labCode;
	}

	public Double getWkConcentration() {
		return wkConcentration;
	}

	public void setWkConcentration(Double wkConcentration) {
		this.wkConcentration = wkConcentration;
	}

	public Double getWkVolume() {
		return wkVolume;
	}

	public void setWkVolume(Double wkVolume) {
		this.wkVolume = wkVolume;
	}

	public Double getWkSumTotal() {
		return wkSumTotal;
	}

	public void setWkSumTotal(Double wkSumTotal) {
		this.wkSumTotal = wkSumTotal;
	}

	public String getLoopNum() {
		return loopNum;
	}

	public void setLoopNum(String loopNum) {
		this.loopNum = loopNum;
	}

	public Double getPcrRatio() {
		return pcrRatio;
	}

	public void setPcrRatio(Double pcrRatio) {
		this.pcrRatio = pcrRatio;
	}

	public String getRin() {
		return rin;
	}

	public void setRin(String rin) {
		this.rin = rin;
	}

	public String getHer2() {
		return her2;
	}

	public void setHer2(String her2) {
		this.her2 = her2;
	}

	public String getEr() {
		return er;
	}

	public void setEr(String er) {
		this.er = er;
	}

	public String getPr() {
		return pr;
	}

	public void setPr(String pr) {
		this.pr = pr;
	}

	public String getRs() {
		return rs;
	}

	public void setRs(String rs) {
		this.rs = rs;
	}

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	public Double getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(Double sampleNum) {
		this.sampleNum = sampleNum;
	}

	public String getI5() {
		return i5;
	}

	public void setI5(String i5) {
		this.i5 = i5;
	}

	public String getI7() {
		return i7;
	}

	public void setI7(String i7) {
		this.i7 = i7;
	}

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 文库号
	 */
	@Column(name = "WK_ID", length = 100)
	public String getWkId() {
		return this.wkId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 文库号
	 */
	public void setWkId(String wkId) {
		this.wkId = wkId;
	}

	@Column(name = "SAMPLE_CODE", length = 100)
	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	@Column(name = "ORDER_ID", length = 50)
	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	@Column(name = "INDEXA", length = 50)
	public String getIndexa() {
		return indexa;
	}

	public void setIndexa(String indexa) {
		this.indexa = indexa;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 片段长度
	 */
	@Column(name = "FRAGMENT_SIZE", length = 50)
	public Double getFragmentSize() {
		return this.fragmentSize;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 片段长度
	 */
	public void setFragmentSize(Double fragmentSize) {
		this.fragmentSize = fragmentSize;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 浓度
	 */
	@Column(name = "CONCENTRATION", length = 50)
	public Double getConcentration() {
		return this.concentration;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 浓度
	 */
	public void setConcentration(Double concentration) {
		this.concentration = concentration;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double qpcr浓度
	 */
	@Column(name = "QPCR", length = 50)
	public Double getQpcr() {
		return this.qpcr;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double qpcr浓度
	 */
	public void setQpcr(Double qpcr) {
		this.qpcr = qpcr;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 比值
	 */
	@Column(name = "SPECIFIC_", length = 100)
	public Double getSpecific() {
		return this.specific;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 比值
	 */
	public void setSpecific(Double specific) {
		this.specific = specific;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 结果判定
	 */
	@Column(name = "RESULT", length = 50)
	public String getResult() {
		return this.result;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 结果判定
	 */
	public void setResult(String result) {
		this.result = result;
	}

	@Column(name = "NEXT_FLOW", length = 50)
	public String getNextFlow() {
		return nextFlow;
	}

	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	@Column(name = "SUBMIT", length = 50)
	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

	// /**
	// *方法: 取得String
	// *@return: String 处理方式
	// */
	// @Column(name ="HANDLE_WAY", length = 50)
	// public String getHandleWay(){
	// return this.handleWay;
	// }
	// /**
	// *方法: 设置String
	// *@param: String 处理方式
	// */
	// public void setHandleWay(String handleWay){
	// this.handleWay = handleWay;
	// }
	@Column(name = "REASON", length = 20)
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 150)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	@Column(name = "PATIENT_NAME", length = 20)
	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	@Column(name = "PRODUCT_ID", length = 20)
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Column(name = "PRODUCT_NAME", length = 20)
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name = "INSPECT_DATE", length = 20)
	public String getInspectDate() {
		return inspectDate;
	}

	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}

	@Column(name = "ACCEPT_DATE", length = 20)
	public Date getAcceptDate() {
		return acceptDate;
	}

	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	@Column(name = "ID_CARD", length = 20)
	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	@Column(name = "PHONE", length = 20)
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Column(name = "SEQUENCE_FUN", length = 20)
	public String getSequenceFun() {
		return sequenceFun;
	}

	public void setSequenceFun(String sequenceFun) {
		this.sequenceFun = sequenceFun;
	}

	@Column(name = "REPORT_DATE", length = 20)
	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	@Column(name = "STATE", length = 50)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "METHOD", length = 20)
	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	@Column(name = "IS_EXECUTE", length = 20)
	public String getIsExecute() {
		return isExecute;
	}

	public void setIsExecute(String isExecute) {
		this.isExecute = isExecute;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "QC_QPCR_TASK")
	public QcQpcrTask getQcQpcrTask() {
		return qcQpcrTask;
	}

	public void setQcQpcrTask(QcQpcrTask qcQpcrTask) {
		this.qcQpcrTask = qcQpcrTask;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 50)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "WK_TYPE", length = 20)
	public String getWkType() {
		return wkType;
	}

	public void setWkType(String wkType) {
		this.wkType = wkType;
	}

	@Column(name = "CODE", length = 20)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getTechTaskId() {
		return techTaskId;
	}

	public void setTechTaskId(String techTaskId) {
		this.techTaskId = techTaskId;
	}

	public String getContractId() {
		return contractId;
	}

	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

	public String getNextFlowId() {
		return nextFlowId;
	}

	public void setNextFlowId(String nextFlowId) {
		this.nextFlowId = nextFlowId;
	}

}