package com.biolims.experiment.qc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: QPCR质控异常反馈
 * @author lims-platform
 * @date 2015-12-05 16:25:55
 * @version V1.0
 * 
 */
@Entity
@Table(name = "QC_QPCR_ABNORMAL")
@SuppressWarnings("serial")
public class QcQpcrAbnormal extends EntityDao<QcQpcrAbnormal> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 样本编号 */
	private String code;
	/** 原始样本编号 */
	private String sampleCode;
	// 文库编号
	private String wkCode;
	/* INDEX */
	private String indexa;
	/** 描述 */
	private String name;
	/** 文库类型 */
	private String wkType;
	/** 片段长度 */
	private Double length;
	/** 质量浓度 */
	private Double qualityConcentrer;
	// /**摩尔浓度*/
	// private String molarity;
	/** 异常原因 */
	private String reason;
	/** 下一步流向ID */
	private String nextFlowId;
	/** 下一步流向 */
	private String nextFlow;
	/** 处理结果 */
	private String result;
	/* 处理意见 */
	private String method;
	/** 备注 */
	private String note;
	/* 患者姓名 */
	private String patientName;
	/* 检测项目 */
	private String productId;
	/* 检测项目 */
	private String productName;
	/* 取样日期 */
	private String inspectDate;
	/* 接收日期 */
	private Date acceptDate;
	/* 身份证 */
	private String idCard;
	/* 手机号 */
	private String phone;
	/* 任务单 */
	private String orderId;
	/* 检测方法 */
	private String sequenceFun;
	/* 应出报告日期 */
	private Date reportDate;
	/* 状态 */
	private String state;
	/* 是否提交 */
	private String submit;
	/* 是否执行 */
	private String isRun;

	// 区分临床还是科技服务 0 临床 1 科技服务
	private String classify;
	/** 批次号 */
	private String batch;
	// 样本类型
	private String sampleType;
	// i5
	private String i5;
	// i7
	private String i7;
	// 样本数量
	private Double sampleNum;

	public Double getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(Double sampleNum) {
		this.sampleNum = sampleNum;
	}

	public String getI5() {
		return i5;
	}

	public void setI5(String i5) {
		this.i5 = i5;
	}

	public String getI7() {
		return i7;
	}

	public void setI7(String i7) {
		this.i7 = i7;
	}

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "CODE", length = 50)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "SAMPLE_CODE", length = 100)
	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	@Column(name = "WK_CODE", length = 50)
	public String getWkCode() {
		return wkCode;
	}

	public void setWkCode(String wkCode) {
		this.wkCode = wkCode;
	}

	@Column(name = "INDEXA", length = 50)
	public String getIndexa() {
		return indexa;
	}

	public void setIndexa(String indexa) {
		this.indexa = indexa;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 100)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "WK_TYPE", length = 100)
	public String getWkType() {
		return wkType;
	}

	public void setWkType(String wkType) {
		this.wkType = wkType;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 片段长度
	 */
	@Column(name = "LENGTH")
	public Double getLength() {
		return this.length;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 片段长度
	 */
	public void setLength(Double length) {
		this.length = length;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 质量浓度
	 */
	@Column(name = "QUALITY_CONCENTRER", length = 50)
	public Double getQualityConcentrer() {
		return this.qualityConcentrer;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 质量浓度
	 */
	public void setQualityConcentrer(Double qualityConcentrer) {
		this.qualityConcentrer = qualityConcentrer;
	}

	@Column(name = "REASON", length = 50)
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@Column(name = "NEXT_FLOW", length = 50)
	public String getNextFlow() {
		return nextFlow;
	}

	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	// /**
	// *方法: 取得String
	// *@return: String 摩尔浓度
	// */
	// @Column(name ="MOLARITY", length = 50)
	// public String getMolarity(){
	// return this.molarity;
	// }
	// /**
	// *方法: 设置String
	// *@param: String 摩尔浓度
	// */
	// public void setMolarity(String molarity){
	// this.molarity = molarity;
	// }
	@Column(name = "RESULT", length = 20)
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	@Column(name = "METHOD", length = 20)
	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 150)
	public String getNote() {
		return this.note;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	@Column(name = "PATIENT_NAME", length = 20)
	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	@Column(name = "PRODUCT_ID", length = 20)
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Column(name = "PRODUCT_NAME", length = 20)
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name = "INSPECT_DATE", length = 20)
	public String getInspectDate() {
		return inspectDate;
	}

	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}

	@Column(name = "ACCEPT_DATE", length = 20)
	public Date getAcceptDate() {
		return acceptDate;
	}

	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	@Column(name = "ID_CARD", length = 20)
	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	@Column(name = "PHONE", length = 20)
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Column(name = "ORDER_ID", length = 20)
	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	@Column(name = "SEQUENCE_FUN", length = 20)
	public String getSequenceFun() {
		return sequenceFun;
	}

	public void setSequenceFun(String sequenceFun) {
		this.sequenceFun = sequenceFun;
	}

	@Column(name = "REPORT_DATE", length = 20)
	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	@Column(name = "STATE", length = 20)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

	public String getIsRun() {
		return isRun;
	}

	public void setIsRun(String isRun) {
		this.isRun = isRun;
	}

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

	public String getNextFlowId() {
		return nextFlowId;
	}

	public void setNextFlowId(String nextFlowId) {
		this.nextFlowId = nextFlowId;
	}

}