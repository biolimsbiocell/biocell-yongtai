package com.biolims.experiment.qc.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 科技服务质控临时表
 * @author lims-platform
 * @date 2016-02-19 11:16:49
 * @version V1.0   
 *
 */
@Entity
@Table(name = "TECH_CHECK_SERVICE_QC_TEMP")
@SuppressWarnings("serial")
public class TechCheckServiceQcTemp extends EntityDao<TechCheckServiceQcTemp> implements java.io.Serializable {
	/**编码*/
	private String id;
	/**样本编号*/
	private String sampleCode;
	/**文库编号*/
	private String wkCode;
	/**片段大小*/
	private Double pdSzie;
	/**质量浓度*/
	private Double massCon;
	/**摩尔浓度*/
	private Double molarCon;
	/**体积*/
	private Double volume;
	/**下一 步流向*/
	private String nextFlow;
	/**片段大小判定*/
	private String pdResult;
	/**摩尔浓度判定*/
	private String molarResult;
	/**备注*/
	private String note;
	/**任务单号*/
	private String orderId;
	/**任务类型*/
	private String orderType;
	/**结果判定*/
	private String result;
	/**是否提交*/
	private String submit;
	/**状态*/
	private String state;
	/**
	 *方法: 取得String
	 *@return: String  编码
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编码
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本编号
	 */
	@Column(name ="SAMPLE_CODE", length = 60)
	public String getSampleCode(){
		return this.sampleCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本编号
	 */
	public void setSampleCode(String sampleCode){
		this.sampleCode = sampleCode;
	}

	
	/**
	 *方法: 取得Integer
	 *@return: Integer  体积
	 */
	@Column(name ="VOLUME", length = 36)
	public Double getVolume(){
		return this.volume;
	}
	/**
	 *方法: 设置Integer
	 *@param: Integer  体积
	 */
	public void setVolume(Double volume){
		this.volume = volume;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  下一步流向
	 */
	@Column(name ="NEXT_FLOW", length = 60)
	public String getNextFlow(){
		return this.nextFlow;
	}
	/**
	 *方法: 设置String
	 *@param: String  下一步流向
	 */
	public void setNextFlow(String nextFlow){
		this.nextFlow = nextFlow;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 120)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}


	public String getSubmit() {
		return submit;
	}
	public void setSubmit(String submit) {
		this.submit = submit;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getWkCode() {
		return wkCode;
	}
	public void setWkCode(String wkCode) {
		this.wkCode = wkCode;
	}
	public Double getPdSzie() {
		return pdSzie;
	}
	public void setPdSzie(Double pdSzie) {
		this.pdSzie = pdSzie;
	}
	public Double getMassCon() {
		return massCon;
	}
	public void setMassCon(Double massCon) {
		this.massCon = massCon;
	}
	public Double getMolarCon() {
		return molarCon;
	}
	public void setMolarCon(Double molarCon) {
		this.molarCon = molarCon;
	}
	public String getPdResult() {
		return pdResult;
	}
	public void setPdResult(String pdResult) {
		this.pdResult = pdResult;
	}
	public String getMolarResult() {
		return molarResult;
	}
	public void setMolarResult(String molarResult) {
		this.molarResult = molarResult;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	
}