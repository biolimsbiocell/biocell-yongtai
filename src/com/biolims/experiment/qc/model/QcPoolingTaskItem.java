package com.biolims.experiment.qc.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.system.product.model.Product;
import com.biolims.system.template.model.Template;
import com.biolims.common.model.user.User;
/**   
 * @Title: Model
 * @Description: pooling明细表
 * @author lims-platform
 * @date 2015-11-29 19:16:00
 * @version V1.0   
 *
 */
@Entity
@Table(name = "QC_POOLING_TASK_ITEM")
@SuppressWarnings("serial")
public class QcPoolingTaskItem extends EntityDao<QcPoolingTaskItem> implements java.io.Serializable {
	/**编号*/
	private String id;
	/*序号*/
	private Integer orderNumber;
	/**描述*/
	private String name;
	/**状态*/
	private String state;
	/**备注*/
	private String note;
	/*POOLING编号*/
	private String poolingCode;
	/**选择模板*/
	private Template template;
	/**测序读长*/
	private String sequencingReadLong;
	/**测序类型*/
	private String sequencingType;
	/**测序平台*/
	private String sequencingPlatform;
	/**业务类型*/
	private Product product;
	/**总数据量*/
	private Integer totalAmount;
	/**总体积*/
	private String totalVolume;
	/**其他*/
	private String others;
	/**处理结果*/
	private String result;
	/**失败原因*/
	private String reason;
	/**步骤编号*/
	private String stepNum;
	/*患者姓名*/
	private String patientName;
	/*检测项目*/
	private String productId;
	/*检测项目*/
	private String productName;
	/*取样日期*/
	private String inspectDate;
	/*接收日期*/
	private Date acceptDate;
	/*身份证*/
	private String idCard;
	/*手机号*/
	private String phone;
	/*任务单*/
	private String orderId;
	/*检测方法*/
	private String sequenceFun;
	/*应出报告日期*/
	private Date reportDate;
	/**相关主表*/
	private QcQpcrTask qcQpcrTask;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	@Column(name ="ORDER_NUMBER", length = 20)
	public Integer getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态id
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态id
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	@Column(name ="POOLING_CODE", length = 50)
	public String getPoolingCode() {
		return poolingCode;
	}
	public void setPoolingCode(String poolingCode) {
		this.poolingCode = poolingCode;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TEMPLATE")
	public Template getTemplate() {
		return template;
	}
	public void setTemplate(Template template) {
		this.template = template;
	}
	//读长
	@Column(name ="SEQUENCING_READLONG", length = 50)
	public String getSequencingReadLong() {
		return sequencingReadLong;
	}
	public void setSequencingReadLong(String sequencingReadLong) {
		this.sequencingReadLong = sequencingReadLong;
	}
	//测序类型
	@Column(name ="SEQUENCING_TYPE", length = 50)
	public String getSequencingType() {
		return sequencingType;
	}
	public void setSequencingType(String sequencingType) {
		this.sequencingType = sequencingType;
	}
	//测序平台
	@Column(name ="SEQUENCING_PLATFORM", length = 50)
	public String getSequencingPlatform() {
		return sequencingPlatform;
	}
	public void setSequencingPlatform(String sequencingPlatform) {
		this.sequencingPlatform = sequencingPlatform;
	}
	//业务类型
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PRODUCT")
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	/**
	 *方法: 取得Double
	 *@return: Double  总数据量
	 */
	@Column(name ="TOTAL_AMOUNT", length = 50)
	public Integer getTotalAmount() {
		return totalAmount;
	}
	/**
	 *方法: 设置Double
	 *@param: Double  总数据量
	 */
	public void setTotalAmount(Integer totalAmount) {
		this.totalAmount = totalAmount;
	}
	/**
	 *方法: 取得Double
	 *@return: Double  总体积
	 */
	@Column(name ="TOTAL_VOLUME", length = 50)
	public String getTotalVolume() {
		return totalVolume;
	}
	/**
	 *方法: 设置Double
	 *@param: Double  总体积
	 */
	public void setTotalVolume(String totalVolume) {
		this.totalVolume = totalVolume;
	}
	/**
	 *方法: 取得String
	 *@return: String  其他
	 */
	@Column(name ="OTHERS", length = 50)
	public String getOthers() {
		return others;
	}
	/**
	 *方法: 设置String
	 *@param: String  其他
	 */
	public void setOthers(String others) {
		this.others = others;
	}
	@Column(name ="RESULT", length = 50)
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	/**
	 *方法: 取得String
	 *@return: String  失败原因
	 */
	@Column(name ="REASON", length = 50)
	public String getReason(){
		return this.reason;
	}
	/**
	 *方法: 设置String
	 *@param: String  失败原因
	 */
	public void setReason(String reason){
		this.reason = reason;
	}
	@Column(name ="STEP_NUM", length = 50)
	public String getStepNum() {
		return stepNum;
	}
	public void setStepNum(String stepNum) {
		this.stepNum = stepNum;
	}
	@Column(name ="PATIENT_NAME", length = 20)
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	@Column(name = "PRODUCT_ID", length = 200)
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	@Column(name = "PRODUCT_NAME", length = 500)
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	@Column(name ="INSPECT_DATE", length = 20)
	public String getInspectDate() {
		return inspectDate;
	}
	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}
	@Column(name ="ACCEPT_DATE", length = 20)
	public Date getAcceptDate() {
		return acceptDate;
	}
	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}
	@Column(name ="ID_CARD", length = 20)
	public String getIdCard() {
		return idCard;
	}
	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	@Column(name ="PHONE", length = 20)
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	@Column(name ="ORDER_ID", length = 20)
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	@Column(name ="SEQUENCE_FUN", length = 20)
	public String getSequenceFun() {
		return sequenceFun;
	}
	public void setSequenceFun(String sequenceFun) {
		this.sequenceFun = sequenceFun;
	}
	@Column(name ="REPORT_DATE", length = 50)
	public Date getReportDate() {
		return reportDate;
	}
	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "QC_QPCR_TASK")
	public QcQpcrTask getQcQpcrTask() {
		return qcQpcrTask;
	}
	public void setQcQpcrTask(QcQpcrTask qcQpcrTask) {
		this.qcQpcrTask = qcQpcrTask;
	}
	
}