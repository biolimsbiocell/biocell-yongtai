package com.biolims.experiment.qc.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: QPCR设备明细
 * @author lims-platform
 * @date 2015-11-24 11:25:37
 * @version V1.0   
 *
 */
@Entity
@Table(name = "QC_QPCR_TASK_COS")
@SuppressWarnings("serial")
public class QcQpcrTaskCos extends EntityDao<QcQpcrTaskCos> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**设备编号*/
	private String code;
	/**描述*/
	private String name;
	/*温度*/
	private Double temperature;
	/*转速*/
	private String speed;
	/*时间*/
	private Double time;
	/**备注*/
	private String note;
	/**是否通过检验*/
	private String isTestSuccess;
	/**相关主表*/
	private QcQpcrTask qcQpcrTask;
	//关联步骤的id
	private String itemId;
	//模板设备Id
	private String tCos;
	
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	/**
	 *方法: 取得String
	 *@return: String  设备编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  设备编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	@Column(name ="TEMPERATURE", length = 50)
	public Double getTemperature() {
		return temperature;
	}
	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}
	@Column(name ="SPEED", length = 50)
	public String getSpeed() {
		return speed;
	}
	public void setSpeed(String speed) {
		this.speed = speed;
	}
	@Column(name ="TIME", length = 50)
	public Double getTime() {
		return time;
	}
	public void setTime(Double time) {
		this.time = time;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得String
	 *@return: String  是否通过检验
	 */
	@Column(name ="IS_TEST_SUCCESS", length = 50)
	public String getIsTestSuccess(){
		return this.isTestSuccess;
	}
	/**
	 *方法: 设置String
	 *@param: String  是否通过检验
	 */
	public void setIsTestSuccess(String isTestSuccess){
		this.isTestSuccess = isTestSuccess;
	}
	/**
	 *方法: 取得WKQpcr
	 *@return: WKQpcr  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "WK_QPCR_SAMPLE_TASK")
	public QcQpcrTask getQcQpcrTask() {
		return qcQpcrTask;
	}
	public void setQcQpcrTask(QcQpcrTask qcQpcrTask) {
		this.qcQpcrTask = qcQpcrTask;
	}
	
	@Column(name="CODE",length=20)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	public String gettCos() {
		return tCos;
	}
	public void settCos(String tCos) {
		this.tCos = tCos;
	}
	
}