package com.biolims.experiment.qc.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.qc.service.WKQpcrSampleTaskService;

public class WKQpcrSampleTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		WKQpcrSampleTaskService wkqService = (WKQpcrSampleTaskService) ctx.getBean("WKQpcrSampleTaskService");
		wkqService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
