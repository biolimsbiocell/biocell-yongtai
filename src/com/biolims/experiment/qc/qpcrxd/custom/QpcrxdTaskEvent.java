package com.biolims.experiment.qc.qpcrxd.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.qc.qpcrxd.service.QpcrxdTaskService;

public class QpcrxdTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		QpcrxdTaskService mbService = (QpcrxdTaskService) ctx.getBean("qpcrxdTaskService");
//		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}