package com.biolims.experiment.qc.qpcrxd.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.storage.model.Storage;

/**
 * @Title: Model
 * @Description: QPCR相对定量明细(文库混合QPCR)
 * @author lims-platform
 * @date 2016-07-21 14:41:17
 * @version V1.0
 * 
 */
@Entity
@Table(name = "QPCRXD_TASK_ITEM")
@SuppressWarnings("serial")
public class QpcrxdTaskItem extends EntityDao<QpcrxdTaskItem> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 描述 */
	private String name;
	/** 样本编号 */
	private String code;
	/** 原始样本编号 */
	private String sampleCode;
	/** 实验室样本号 */
	private String labCode;
	/** 样本类型 */
	private String sampleType;
	/** 检测项目编号 */
	private String productId;
	/** 检测项目名称 */
	private String productName;
	/** 步骤编号 */
	private String stepNum;
	/** 取样日期 */
	private Date inspectDate;
	/** 接收日期 */
	private Date acceptDate;
	/** 应出报告日期 */
	private Date reportDate;
	/** 体积 */
	private Double volume;
	/** 样本用量 */
	private Double sampleNum;
	/** 中间产物数量 */
	private Double productNum;
	/** 关联任务单 */
	private String orderId;
	/** 序号 */
	private String orderNumber;
	/** 状态 */
	private String state;
	/** 相关主表 */
	private QpcrxdTask qpcrxdTask;
	/** 备注 */
	private String note;
	/** 临时表Id */
	private String tempId;
	/** 探针 */
	private Storage probeCode;
	// 血液文库比例
	private String xylr;
	// ctDNA文库比例
	private String clr;
	// FFPR文库比例
	private String flr;
	// 新鲜组织文库比例
	private String xzLr;
	// 其他文库比例
	private String qtlr;
	// PCR循环数
	private String loopNum;
	// 通量
	private String flux;
	// 预混合组号
	private String yhhNum;
	// 取样
	private String qy;
	// 文库类型
	private String wkType;

	public String getWkType() {
		return wkType;
	}

	public void setWkType(String wkType) {
		this.wkType = wkType;
	}

	public String getYhhNum() {
		return yhhNum;
	}

	public void setYhhNum(String yhhNum) {
		this.yhhNum = yhhNum;
	}

	public String getQy() {
		return qy;
	}

	public void setQy(String qy) {
		this.qy = qy;
	}

	public String getFlux() {
		return flux;
	}

	public void setFlux(String flux) {
		this.flux = flux;
	}

	public String getXylr() {
		return xylr;
	}

	public void setXylr(String xylr) {
		this.xylr = xylr;
	}

	public String getClr() {
		return clr;
	}

	public void setClr(String clr) {
		this.clr = clr;
	}

	public String getFlr() {
		return flr;
	}

	public void setFlr(String flr) {
		this.flr = flr;
	}

	public String getXzLr() {
		return xzLr;
	}

	public void setXzLr(String xzLr) {
		this.xzLr = xzLr;
	}

	public String getQtlr() {
		return qtlr;
	}

	public void setQtlr(String qtlr) {
		this.qtlr = qtlr;
	}

	public String getLoopNum() {
		return loopNum;
	}

	public void setLoopNum(String loopNum) {
		this.loopNum = loopNum;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PROBE_CODE")
	public Storage getProbeCode() {
		return probeCode;
	}

	public void setProbeCode(Storage probeCode) {
		this.probeCode = probeCode;
	}

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 100)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本编号
	 */
	@Column(name = "CODE", length = 50)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 原始样本编号
	 */
	@Column(name = "SAMPLE_CODE", length = 100)
	public String getSampleCode() {
		return this.sampleCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 原始样本编号
	 */
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 实验室样本号
	 */
	@Column(name = "LAB_CODE", length = 50)
	public String getLabCode() {
		return this.labCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 实验室样本号
	 */
	public void setLabCode(String labCode) {
		this.labCode = labCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本类型
	 */
	@Column(name = "SAMPLE_TYPE", length = 50)
	public String getSampleType() {
		return this.sampleType;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本类型
	 */
	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 检测项目编号
	 */
	@Column(name = "PRODUCT_ID", length = 50)
	public String getProductId() {
		return this.productId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 检测项目编号
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 检测项目名称
	 */
	@Column(name = "PRODUCT_NAME", length = 500)
	public String getProductName() {
		return this.productName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 检测项目名称
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 步骤编号
	 */
	@Column(name = "STEP_NUM", length = 50)
	public String getStepNum() {
		return this.stepNum;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 步骤编号
	 */
	public void setStepNum(String stepNum) {
		this.stepNum = stepNum;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 取样日期
	 */
	@Column(name = "INSPECT_DATE", length = 50)
	public Date getInspectDate() {
		return this.inspectDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 取样日期
	 */
	public void setInspectDate(Date inspectDate) {
		this.inspectDate = inspectDate;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 接收日期
	 */
	@Column(name = "ACCEPT_DATE", length = 50)
	public Date getAcceptDate() {
		return this.acceptDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 接收日期
	 */
	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 应出报告日期
	 */
	@Column(name = "REPORT_DATE", length = 50)
	public Date getReportDate() {
		return this.reportDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 应出报告日期
	 */
	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 体积
	 */
	@Column(name = "VOLUME", length = 50)
	public Double getVolume() {
		return this.volume;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 体积
	 */
	public void setVolume(Double volume) {
		this.volume = volume;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 样本用量
	 */
	@Column(name = "SAMPLE_NUM", length = 50)
	public Double getSampleNum() {
		return this.sampleNum;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 样本用量
	 */
	public void setSampleNum(Double sampleNum) {
		this.sampleNum = sampleNum;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 中间产物数量
	 */
	@Column(name = "PRODUCT_NUM", length = 50)
	public Double getProductNum() {
		return this.productNum;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 中间产物数量
	 */
	public void setProductNum(Double productNum) {
		this.productNum = productNum;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 关联任务单
	 */
	@Column(name = "ORDER_ID", length = 50)
	public String getOrderId() {
		return this.orderId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 关联任务单
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 序号
	 */
	@Column(name = "ORDER_NUMBER", length = 50)
	public String getOrderNumber() {
		return this.orderNumber;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 序号
	 */
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得QpcrxdTask
	 * 
	 * @return: QpcrxdTask 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "QPCRXD_TASK")
	public QpcrxdTask getQpcrxdTask() {
		return this.qpcrxdTask;
	}

	/**
	 * 方法: 设置QpcrxdTask
	 * 
	 * @param: QpcrxdTask 相关主表
	 */
	public void setQpcrxdTask(QpcrxdTask qpcrxdTask) {
		this.qpcrxdTask = qpcrxdTask;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}
}