package com.biolims.experiment.qc.qpcrxd.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.pooling.dao.PoolingDao;
import com.biolims.experiment.pooling.model.PoolingItem;
import com.biolims.experiment.qc.qpcrxd.dao.QpcrxdTaskDao;
import com.biolims.experiment.qc.qpcrxd.model.QpcrxdTask;
import com.biolims.experiment.qc.qpcrxd.model.QpcrxdTaskCos;
import com.biolims.experiment.qc.qpcrxd.model.QpcrxdTaskItem;
import com.biolims.experiment.qc.qpcrxd.model.QpcrxdTaskReagent;
import com.biolims.experiment.qc.qpcrxd.model.QpcrxdTaskResult;
import com.biolims.experiment.qc.qpcrxd.model.QpcrxdTaskTemp;
import com.biolims.experiment.qc.qpcrxd.model.QpcrxdTaskTemplate;
import com.biolims.experiment.sj.wkblend.model.WkBlendTaskTemp;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.storage.main.service.StorageService;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.work.dao.WorkOrderDao;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class QpcrxdTaskService {
	@Resource
	private QpcrxdTaskDao qpcrxdTaskDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private DicSampleTypeDao dicSampleTypeDao;
	@Resource
	private WorkOrderDao workOrderDao;
	@Resource
	private PoolingDao poolingDao;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private StorageService storageService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findQpcrxdTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return qpcrxdTaskDao.selectQpcrxdTaskList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(QpcrxdTask i) throws Exception {

		qpcrxdTaskDao.saveOrUpdate(i);

	}

	public QpcrxdTask get(String id) {
		QpcrxdTask qpcrxdTask = commonDAO.get(QpcrxdTask.class, id);
		return qpcrxdTask;
	}

	public Map<String, Object> findQpcrxdTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = qpcrxdTaskDao.selectQpcrxdTaskItemList(
				scId, startNum, limitNum, dir, sort);
		List<QpcrxdTaskItem> list = (List<QpcrxdTaskItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findQpcrxdTaskTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = qpcrxdTaskDao
				.selectQpcrxdTaskTemplateList(scId, startNum, limitNum, dir,
						sort);
		List<QpcrxdTaskTemplate> list = (List<QpcrxdTaskTemplate>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findQpcrxdTaskReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = qpcrxdTaskDao.selectQpcrxdTaskReagentList(
				scId, startNum, limitNum, dir, sort);
		List<QpcrxdTaskReagent> list = (List<QpcrxdTaskReagent>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findQpcrxdTaskCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = qpcrxdTaskDao.selectQpcrxdTaskCosList(
				scId, startNum, limitNum, dir, sort);
		List<QpcrxdTaskCos> list = (List<QpcrxdTaskCos>) result.get("list");
		return result;
	}

	public Map<String, Object> findQpcrxdTaskResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = qpcrxdTaskDao.selectQpcrxdTaskResultList(
				scId, startNum, limitNum, dir, sort);
		List<QpcrxdTaskResult> list = (List<QpcrxdTaskResult>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findQpcrxdTaskTempList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = qpcrxdTaskDao.selectQpcrxdTaskTempList(
				scId, startNum, limitNum, dir, sort);
		List<QpcrxdTaskTemp> list = (List<QpcrxdTaskTemp>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveQpcrxdTaskItem(QpcrxdTask sc, String itemDataJson)
			throws Exception {
		List<QpcrxdTaskItem> saveItems = new ArrayList<QpcrxdTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			QpcrxdTaskItem scp = new QpcrxdTaskItem();
			// 将map信息读入实体类
			scp = (QpcrxdTaskItem) qpcrxdTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setQpcrxdTask(sc);

			saveItems.add(scp);
		}
		qpcrxdTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQpcrxdTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			QpcrxdTaskItem scp = qpcrxdTaskDao.get(QpcrxdTaskItem.class, id);
			qpcrxdTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveQpcrxdTaskTemplate(QpcrxdTask sc, String itemDataJson)
			throws Exception {
		List<QpcrxdTaskTemplate> saveItems = new ArrayList<QpcrxdTaskTemplate>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			QpcrxdTaskTemplate scp = new QpcrxdTaskTemplate();
			// 将map信息读入实体类
			scp = (QpcrxdTaskTemplate) qpcrxdTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setQpcrxdTask(sc);

			saveItems.add(scp);
		}
		qpcrxdTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQpcrxdTaskTemplate(String[] ids) throws Exception {
		for (String id : ids) {
			QpcrxdTaskTemplate scp = qpcrxdTaskDao.get(
					QpcrxdTaskTemplate.class, id);
			qpcrxdTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQpcrxdTaskTemplateOne(String ids) throws Exception {

		QpcrxdTaskTemplate scp = qpcrxdTaskDao.get(QpcrxdTaskTemplate.class,
				ids);
		qpcrxdTaskDao.delete(scp);

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveQpcrxdTaskReagent(QpcrxdTask sc, String itemDataJson)
			throws Exception {
		List<QpcrxdTaskReagent> saveItems = new ArrayList<QpcrxdTaskReagent>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			QpcrxdTaskReagent scp = new QpcrxdTaskReagent();
			// 将map信息读入实体类
			scp = (QpcrxdTaskReagent) qpcrxdTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setQpcrxdTask(sc);
			// 用量 = 单个用量*样本数量
			if (scp != null && scp.getSampleNum() != null
					&& scp.getOneNum() != null) {
				scp.setNum(scp.getOneNum() * scp.getSampleNum());
			}

			saveItems.add(scp);
		}
		qpcrxdTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQpcrxdTaskReagent(String[] ids) throws Exception {
		for (String id : ids) {
			QpcrxdTaskReagent scp = qpcrxdTaskDao.get(QpcrxdTaskReagent.class,
					id);
			qpcrxdTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQpcrxdTaskReagentOne(String ids) throws Exception {

		QpcrxdTaskReagent scp = qpcrxdTaskDao.get(QpcrxdTaskReagent.class, ids);
		qpcrxdTaskDao.delete(scp);

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveQpcrxdTaskCos(QpcrxdTask sc, String itemDataJson)
			throws Exception {
		List<QpcrxdTaskCos> saveItems = new ArrayList<QpcrxdTaskCos>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			QpcrxdTaskCos scp = new QpcrxdTaskCos();
			// 将map信息读入实体类
			scp = (QpcrxdTaskCos) qpcrxdTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setQpcrxdTask(sc);

			saveItems.add(scp);
		}
		qpcrxdTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQpcrxdTaskCos(String[] ids) throws Exception {
		for (String id : ids) {
			QpcrxdTaskCos scp = qpcrxdTaskDao.get(QpcrxdTaskCos.class, id);
			qpcrxdTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQpcrxdTaskCosOne(String ids) throws Exception {

		QpcrxdTaskCos scp = qpcrxdTaskDao.get(QpcrxdTaskCos.class, ids);
		qpcrxdTaskDao.delete(scp);

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveQpcrxdTaskResult(QpcrxdTask sc, String itemDataJson)
			throws Exception {
		List<QpcrxdTaskResult> saveItems = new ArrayList<QpcrxdTaskResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			QpcrxdTaskResult scp = new QpcrxdTaskResult();
			// 将map信息读入实体类
			scp = (QpcrxdTaskResult) qpcrxdTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setQpcrxdTask(sc);

			saveItems.add(scp);
		}
		qpcrxdTaskDao.saveOrUpdateAll(saveItems);

		// // 设置保留三维小数
		// DecimalFormat df = new DecimalFormat("######.000");
		// // 查询Cq mean的最大值
		// String max = this.qpcrxdTaskDao.setResultListgetmax(sc.getId());
		// List<QpcrxdTaskResult> list2 = this.qpcrxdTaskDao.setResultList(sc
		// .getId());
		// for (QpcrxdTaskResult q : list2) {
		// // Double d = Double.parseDouble(q.getTllr())
		// // - Double.parseDouble(max);
		// // q.setCdlr(Double.valueOf(df.format(d)).toString());
		// // Double e = Math.pow(Double.parseDouble(q.getKzxl()), -d);
		// // q.setEct(Double.valueOf(df.format(e)).toString());
		// if (q.getConcentration() != null
		// && !q.getConcentration().equals(""))
		// q.setSumTotal(Double.valueOf(df.format(q.getConcentration()
		// * q.getVolume())));
		// }
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQpcrxdTaskResult(String[] ids) throws Exception {
		for (String id : ids) {
			QpcrxdTaskResult scp = qpcrxdTaskDao
					.get(QpcrxdTaskResult.class, id);
			qpcrxdTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delQpcrxdTaskTemp(String[] ids) throws Exception {
		for (String id : ids) {
			QpcrxdTaskTemp scp = qpcrxdTaskDao.get(QpcrxdTaskTemp.class, id);
			qpcrxdTaskDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(QpcrxdTask sc, Map jsonMap) throws Exception {
		if (sc != null) {
			qpcrxdTaskDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("qpcrxdTaskItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveQpcrxdTaskItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("qpcrxdTaskTemplate");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveQpcrxdTaskTemplate(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("qpcrxdTaskReagent");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveQpcrxdTaskReagent(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("qpcrxdTaskCos");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveQpcrxdTaskCos(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("qpcrxdTaskResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveQpcrxdTaskResult(sc, jsonStr);
			}
		}
	}

	// 审核完成
//	public void changeState(String applicationTypeActionId, String id)
//			throws Exception {
//		QpcrxdTask sct = qpcrxdTaskDao.get(QpcrxdTask.class, id);
//		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
//		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
//		User user = (User) ServletActionContext.getRequest().getSession()
//				.getAttribute(SystemConstants.USER_SESSION_KEY);
//		sct.setConfirmUser(user);
//		sct.setConfirmDate(new Date());
//		qpcrxdTaskDao.update(sct);
//
//		// 将批次信息反馈到模板中
//		List<QpcrxdTaskReagent> list1 = qpcrxdTaskDao.setReagentList(id);
//		for (QpcrxdTaskReagent dt : list1) {
//			String bat1 = dt.getBatch(); // 实验中试剂的批次
//			String drid = dt.getTReagent(); // 实验中保存的试剂ID
//			List<ReagentItem> list2 = templateDao.setReagentListById(drid);
//			for (ReagentItem ri : list2) {
//				if (bat1 != null) {
//					ri.setBatch(bat1);
//				}
//			}
//			// 改变库存主数据试剂数量
//			this.storageService.UpdateStorageAndItemNum(dt.getCode(),
//					dt.getBatch(), dt.getNum());
//		}
//
//		List<QpcrxdTaskResult> list = this.qpcrxdTaskDao.setResultList(id);
//		for (QpcrxdTaskResult scp : list) {
//			if (scp != null && scp.getNextFlow() != null) {
//				if (scp.getNextFlowId().equals("0009")) {// 入库
//					SampleInItemTemp st = new SampleInItemTemp();
//					st.setCode(scp.getCode());
//					st.setSampleCode(scp.getSampleCode());
//					st.setSampleType(scp.getLabCode());
//					st.setInfoFrom("QpcrxdTaskResult");
//					st.setState("1");
//					st.setConcentration(scp.getConcentration());
//					st.setVolume(scp.getVolume());
//					st.setSumTotal(scp.getSumTotal());
//					qpcrxdTaskDao.saveOrUpdate(st);
//				} else if (scp.getNextFlowId().equals("0032")) {// 文库混合
//					WkBlendTaskTemp w = new WkBlendTaskTemp();
////					w.setFjwk(scp.getCode());
////					w.setSjfz(scp.getSampleCode());
////					w.setProbeCode(scp.getProbeCode());
////					w.setKybh(scp.getSampleType());
////					w.setXylr(scp.getXylr());
////					w.setClr(scp.getClr());
////					w.setFlr(scp.getFlr());
////					w.setXzlr(scp.getXzLr());
////					w.setQtlr(scp.getQtlr());
////					w.setTl(scp.getFlux());
////					w.setConcentration(scp.getConcentration().toString());
////					w.setVolume(scp.getVolume().toString());
////					w.setSumTotal(scp.getSumTotal().toString());
////					w.setCxlx(scp.getProductId());
////					w.setCxpt(scp.getProductName());
////					w.setCxdc(scp.getName());
//					w.setState("1");
//					qpcrxdTaskDao.saveOrUpdate(w);
//				} else {
//					// 得到下一步流向的相关表单
//					List<NextFlow> list_nextFlow = nextFlowDao
//							.seletNextFlowById(scp.getNextFlowId());
//					for (NextFlow n : list_nextFlow) {
//						Object o = Class.forName(
//								n.getApplicationTypeTable().getClassPath())
//								.newInstance();
//						scp.setState("1");
//						sampleInputService.copy(o, scp);
//					}
//				}
//			}
//			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//			// 根据富集文库查询明细样本
//			List<PoolingItem> listItem = this.poolingDao
//					.getItemListByfjwk(scp.getCode());
//			for (PoolingItem p : listItem) {
//				sampleStateService
//						.saveSampleState(
//								p.getCode(),
//								p.getSampleCode(),
//								p.getProductId(),
//								p.getProductName(),
//								"",
//								format.format(sct.getCreateDate()),
//								format.format(new Date()),
//								"QpcrxdTask",
//								"文库混合QPCR",
//								(User) ServletActionContext
//										.getRequest()
//										.getSession()
//										.getAttribute(
//												SystemConstants.USER_SESSION_KEY),
//								id, scp.getNextFlow(), null, null, null, null,
//								null, null, null, null, null);
//			}
//		}
//	}

	public List<QpcrxdTaskItem> findDnaItemList(String scId) throws Exception {
		List<QpcrxdTaskItem> list = qpcrxdTaskDao.setItemList(scId);
		return list;
	}

	// 科技服务-任务单主表
	public Map<String, Object> findTechJkServiceTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return this.qpcrxdTaskDao.selectTechJkServiceTaskList(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	// 科技服务-任务单明细表
	public Map<String, Object> findTechServiceTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = this.qpcrxdTaskDao
				.selectTechServiceTaskItemList(scId, startNum, limitNum, dir,
						sort);
		List<TechJkServiceTaskItem> list = (List<TechJkServiceTaskItem>) result
				.get("list");
		return result;
	}

	// 根据科技服务主表id 查询明细信息
	public List<Map<String, Object>> selectTechTaskItemByTaskId(String taskId) {
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		Map<String, Object> result = this.qpcrxdTaskDao
				.selectTechTaskItemByTaskId(taskId);
		List<TechJkServiceTaskItem> list = (List<TechJkServiceTaskItem>) result
				.get("list");
		if (list != null && list.size() > 0) {
			for (TechJkServiceTaskItem ti : list) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("id", ti.getId());
				map.put("name", ti.getName());
				map.put("sampleCode", ti.getSampleCode());
				map.put("state", ti.getState());
				map.put("stateName", ti.getStateName());

				map.put("projectId", ti.getTechJkServiceTask().getProjectId());
				map.put("contractId", ti.getTechJkServiceTask().getContractId());
				map.put("orderType", ti.getTechJkServiceTask().getOrderType());
				map.put("experimentUser", ti.getExperimentUser());
				map.put("endDate", ti.getEndDate());
				map.put("species", ti.getSpecies());
				map.put("classify", ti.getClassify());

				map.put("techJkServiceTaskId", ti.getTechJkServiceTask()
						.getId());
				map.put("techJkServiceTaskName", ti.getTechJkServiceTask()
						.getName());
				// if(ti.getSampleCode()!=null){
				// List<TechJkServiceTask> l =
				// commonService.get(TechJkServiceTask.class, "sampleCode",
				// ti.getSampleCode());
				// if(l.size()>0)
				// ti.setTechJkServiceTask(l.get(0));
				// }
				mapList.add(map);
			}
		}
		return mapList;
	}

	public List<QpcrxdTaskItem> findQpcrxdTaskItemList(String scId)
			throws Exception {
		List<QpcrxdTaskItem> list = qpcrxdTaskDao
				.selectQpcrxdTaskItemList(scId);
		return list;
	}

}
