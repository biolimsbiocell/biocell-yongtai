package com.biolims.experiment.qc.qpcrxd.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.dna.model.DnaTaskReagent;
import com.biolims.experiment.qc.qpcrjd.model.QpcrjdTaskItem;
import com.biolims.experiment.qc.qpcrjd.model.QpcrjdTaskResult;
import com.biolims.experiment.qc.qpcrjd.model.QpcrjdTaskTemp;
import com.biolims.experiment.qc.qpcrxd.model.QpcrxdTask;
import com.biolims.experiment.qc.qpcrxd.model.QpcrxdTaskCos;
import com.biolims.experiment.qc.qpcrxd.model.QpcrxdTaskItem;
import com.biolims.experiment.qc.qpcrxd.model.QpcrxdTaskReagent;
import com.biolims.experiment.qc.qpcrxd.model.QpcrxdTaskResult;
import com.biolims.experiment.qc.qpcrxd.model.QpcrxdTaskTemp;
import com.biolims.experiment.qc.qpcrxd.model.QpcrxdTaskTemplate;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;

@Repository
@SuppressWarnings("unchecked")
public class QpcrxdTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectQpcrxdTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from QpcrxdTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<QpcrxdTask> list = new ArrayList<QpcrxdTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectQpcrxdTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QpcrxdTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qpcrxdTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QpcrxdTaskItem> list = new ArrayList<QpcrxdTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectQpcrxdTaskTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QpcrxdTaskTemplate where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qpcrxdTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QpcrxdTaskTemplate> list = new ArrayList<QpcrxdTaskTemplate>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectQpcrxdTaskReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QpcrxdTaskReagent where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qpcrxdTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QpcrxdTaskReagent> list = new ArrayList<QpcrxdTaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectQpcrxdTaskCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QpcrxdTaskCos where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qpcrxdTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QpcrxdTaskCos> list = new ArrayList<QpcrxdTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectQpcrxdTaskResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QpcrxdTaskResult where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qpcrxdTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QpcrxdTaskResult> list = new ArrayList<QpcrxdTaskResult>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectQpcrxdTaskTempList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QpcrxdTaskTemp where 1=1  and state='1'";
		String key = "";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QpcrxdTaskTemp> list = new ArrayList<QpcrxdTaskTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by code desc";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public List<QpcrxdTaskItem> setItemList(String id) {
		String hql = "from QpcrxdTaskItem where 1=1 and qpcrxdTask.id='" + id
				+ "'";
		List<QpcrxdTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<QpcrxdTaskTemp> setTempList(String code) {
		String hql = "from QpcrxdTaskTemp where 1=1 and code='" + code + "'";
		List<QpcrxdTaskTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<QpcrxdTaskResult> setResultList(String code) {
		String hql = "from QpcrxdTaskResult where 1=1 and qpcrxdTask.id='"
				+ code + "'";
		List<QpcrxdTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 查询最大Cq mean值
	public String setResultListgetmax(String code) {
		String hql = "from QpcrxdTaskResult where 1=1 and qpcrxdTask.id='"
				+ code + "'";
		String list = (String) this.getSession()
				.createQuery("select max(tllr) " + hql).uniqueResult();
		return list;
	}

	// 根据上机分组号查询
	public List<QpcrxdTaskResult> setResultListbySjfz(String sampleCode) {
		String hql = "from QpcrxdTaskResult where 1=1 and sampleCode='"
				+ sampleCode + "'";
		List<QpcrxdTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 科技服务
	public Map<String, Object> selectTechJkServiceTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		// String hql =
		// " from TechJkServiceTask t where 1=1 and t.orderType = '0'";//任务单类型为：建库
		String hql = " from TechJkServiceTask t where 1=1";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<TechJkServiceTask> list = new ArrayList<TechJkServiceTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectTechServiceTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from TechJkServiceTaskItem where 1=1 and state = '1'";
		String key = "";
		if (scId != null)
			key = key + " and techJkServiceTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<TechJkServiceTaskItem> list = new ArrayList<TechJkServiceTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据科技服务主表id获取明细表信息
	public Map<String, Object> selectTechTaskItemByTaskId(String taskId) {
		String hql = "from TechJkServiceTaskItem t where 1=1 and t.techJkServiceTask='"
				+ taskId + "' and t.state = '1'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<TechJkServiceTaskItem> list = this.getSession().createQuery(hql)
				.list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public List<QpcrxdTaskItem> selectQpcrxdTaskItemList(String scId)
			throws Exception {
		String hql = "from QpcrxdTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qpcrxd.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QpcrxdTaskItem> list = new ArrayList<QpcrxdTaskItem>();
		list = this.getSession().createQuery(hql + key).list();
		return list;
	}

	// 根据样本编号查询
	public List<QpcrxdTaskResult> setQpcrxdResultById(String code) {
		String hql = "from QpcrxdTaskResult t where (submit is null or submit='') and qpcrxdTask.id='"
				+ code + "'";
		List<QpcrxdTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据样本编号查询
	public List<QpcrxdTaskResult> setQpcrxdResultByIds(String[] codes) {

		String insql = "''";
		for (String code : codes) {

			insql += ",'" + code + "'";

		}

		String hql = "from QpcrxdTaskResult t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<QpcrxdTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/*
	 * 根据主表ID查询试剂明细
	 */
	public List<QpcrxdTaskReagent> setReagentList(String code) {
		String hql = "from QpcrxdTaskReagent where 1=1 and qpcrxdTask.id='"
				+ code + "'";
		List<QpcrxdTaskReagent> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}
}
