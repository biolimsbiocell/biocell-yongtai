package com.biolims.experiment.qc.qpcrxd.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.qc.qpcrxd.model.QpcrxdTask;
import com.biolims.experiment.qc.qpcrxd.model.QpcrxdTaskCos;
import com.biolims.experiment.qc.qpcrxd.model.QpcrxdTaskItem;
import com.biolims.experiment.qc.qpcrxd.model.QpcrxdTaskReagent;
import com.biolims.experiment.qc.qpcrxd.model.QpcrxdTaskResult;
import com.biolims.experiment.qc.qpcrxd.model.QpcrxdTaskTemp;
import com.biolims.experiment.qc.qpcrxd.model.QpcrxdTaskTemplate;
import com.biolims.experiment.qc.qpcrxd.service.QpcrxdTaskService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

@Namespace("/experiment/qc/qpcrxd/qpcrxdTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class QpcrxdTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "240407";
	@Autowired
	private QpcrxdTaskService qpcrxdTaskService;
	private QpcrxdTask qpcrxdTask = new QpcrxdTask();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;

	@Action(value = "showQpcrxdTaskList")
	public String showQpcrxdTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qc/qpcrxd/qpcrxdTask.jsp");
	}

	@Action(value = "showQpcrxdTaskListJson")
	public void showQpcrxdTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = qpcrxdTaskService.findQpcrxdTaskList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<QpcrxdTask> list = (List<QpcrxdTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("reciveDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "qpcrxdTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogQpcrxdTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qc/qpcrxd/qpcrxdTaskDialog.jsp");
	}

	@Action(value = "showDialogQpcrxdTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogQpcrxdTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = qpcrxdTaskService.findQpcrxdTaskList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<QpcrxdTask> list = (List<QpcrxdTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("reciveDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editQpcrxdTask")
	public String editQpcrxdTask() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			qpcrxdTask = qpcrxdTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "qpcrxdTask");
			String tName = workflowProcessInstanceService
					.findToDoTaskNameByFormId(id);
			putObjToContext("taskName", tName);
		} else {
			qpcrxdTask.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			qpcrxdTask.setCreateUser(user);
			qpcrxdTask.setCreateDate(new Date());
			qpcrxdTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			qpcrxdTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}

		toState(qpcrxdTask.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/qc/qpcrxd/qpcrxdTaskEdit.jsp");
	}

	@Action(value = "copyQpcrxdTask")
	public String copyQpcrxdTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		qpcrxdTask = qpcrxdTaskService.get(id);
		qpcrxdTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/qc/qpcrxd/qpcrxdTaskEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = qpcrxdTask.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "QpcrxdTask";
			String markCode = "QPCRXD";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			qpcrxdTask.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("qpcrxdTaskItem",
				getParameterFromRequest("qpcrxdTaskItemJson"));

		aMap.put("qpcrxdTaskTemplate",
				getParameterFromRequest("qpcrxdTaskTemplateJson"));

		aMap.put("qpcrxdTaskReagent",
				getParameterFromRequest("qpcrxdTaskReagentJson"));

		aMap.put("qpcrxdTaskCos", getParameterFromRequest("qpcrxdTaskCosJson"));

		aMap.put("qpcrxdTaskResult",
				getParameterFromRequest("qpcrxdTaskResultJson"));

		qpcrxdTaskService.save(qpcrxdTask, aMap);

		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/experiment/qc/qpcrxd/qpcrxdTask/editQpcrxdTask.action?id="
				+ qpcrxdTask.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);

	}

	@Action(value = "viewQpcrxdTask")
	public String toViewQpcrxdTask() throws Exception {
		String id = getParameterFromRequest("id");
		qpcrxdTask = qpcrxdTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/qc/qpcrxd/qpcrxdTaskEdit.jsp");
	}

	@Action(value = "showQpcrxdTaskItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQpcrxdTaskItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qc/qpcrxd/qpcrxdTaskItem.jsp");
	}

	@Action(value = "showQpcrxdTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQpcrxdTaskItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = qpcrxdTaskService
					.findQpcrxdTaskItemList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<QpcrxdTaskItem> list = (List<QpcrxdTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("labCode", "");
			map.put("sampleType", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("stepNum", "");
			map.put("inspectDate", "yyyy-MM-dd");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("volume", "");
			map.put("sampleNum", "");
			map.put("productNum", "");
			map.put("orderId", "");
			map.put("orderNumber", "");
			map.put("state", "");
			map.put("qpcrxdTask-name", "");
			map.put("qpcrxdTask-id", "");
			map.put("note", "");
			map.put("tempId", "");
			map.put("probeCode-id", "");
			map.put("probeCode-name", "");
			map.put("xylr", "");
			map.put("clr", "");
			map.put("flr", "");
			map.put("xzLr", "");
			map.put("qtlr", "");
			map.put("loopNum", "");
			map.put("flux", "");
			map.put("yhhNum", "");
			map.put("qy", "");
			map.put("wkType", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delQpcrxdTaskItem")
	public void delQpcrxdTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			qpcrxdTaskService.delQpcrxdTaskItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showQpcrxdTaskTemplateList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQpcrxdTaskTemplateList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qc/qpcrxd/qpcrxdTaskTemplate.jsp");
	}

	@Action(value = "showQpcrxdTaskTemplateListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQpcrxdTaskTemplateListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = qpcrxdTaskService
					.findQpcrxdTaskTemplateList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<QpcrxdTaskTemplate> list = (List<QpcrxdTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("reciveUser-name", "");
			map.put("reciveUser-id", "");
			map.put("tItem", "");
			map.put("code", "");
			map.put("stepName", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("state", "");
			map.put("note", "");
			map.put("sampleCodes", "");
			map.put("qpcrxdTask-name", "");
			map.put("qpcrxdTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "showTemplateWaitList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTemplateWaitList() throws Exception {
		String id = getParameterFromRequest("id");
		putObjToContext("id", id);
		return dispatcher("/WEB-INF/page/experiment/qc/qpcrxd/showTemplateWaitList.jsp");
	}

	@Action(value = "showTemplateWaitListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTemplateWaitListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = qpcrxdTaskService
					.findQpcrxdTaskTemplateList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<QpcrxdTaskTemplate> list = (List<QpcrxdTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("reciveUser-name", "");
			map.put("reciveUser-id", "");
			map.put("tItem", "");
			map.put("code", "");
			map.put("stepName", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("state", "");
			map.put("note", "");
			map.put("sampleCodes", "");
			map.put("qpcrxdTask-name", "");
			map.put("qpcrxdTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delQpcrxdTaskTemplate")
	public void delQpcrxdTaskTemplate() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			qpcrxdTaskService.delQpcrxdTaskTemplate(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delQpcrxdTaskTemplateOne")
	public void delQpcrxdTaskTemplateOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			qpcrxdTaskService.delQpcrxdTaskTemplateOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showQpcrxdTaskReagentList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQpcrxdTaskReagentList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qc/qpcrxd/qpcrxdTaskReagent.jsp");
	}

	@Action(value = "showQpcrxdTaskReagentListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQpcrxdTaskReagentListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = qpcrxdTaskService
					.findQpcrxdTaskReagentList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<QpcrxdTaskReagent> list = (List<QpcrxdTaskReagent>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("batch", "");
			map.put("count", "");
			map.put("oneNum", "");
			map.put("sampleNum", "");
			map.put("num", "");
			map.put("isGood", "");
			map.put("note", "");
			map.put("qpcrxdTask-name", "");
			map.put("qpcrxdTask-id", "");
			map.put("itemId", "");
			map.put("tReagent", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delQpcrxdTaskReagent")
	public void delQpcrxdTaskReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			qpcrxdTaskService.delQpcrxdTaskReagent(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delQpcrxdTaskReagentOne")
	public void delQpcrxdTaskReagentOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			qpcrxdTaskService.delQpcrxdTaskReagentOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showQpcrxdTaskCosList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQpcrxdTaskCosList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qc/qpcrxd/qpcrxdTaskCos.jsp");
	}

	@Action(value = "showQpcrxdTaskCosListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQpcrxdTaskCosListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = qpcrxdTaskService
					.findQpcrxdTaskCosList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<QpcrxdTaskCos> list = (List<QpcrxdTaskCos>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("temperature", "");
			map.put("speed", "");
			map.put("time", "");
			map.put("note", "");
			map.put("isGood", "");
			map.put("qpcrxdTask-name", "");
			map.put("qpcrxdTask-id", "");
			map.put("itemId", "");
			map.put("tCos", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delQpcrxdTaskCos")
	public void delQpcrxdTaskCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			qpcrxdTaskService.delQpcrxdTaskCos(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delQpcrxdTaskCosOne")
	public void delQpcrxdTaskCosOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			qpcrxdTaskService.delQpcrxdTaskCosOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showQpcrxdTaskResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQpcrxdTaskResultList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qc/qpcrxd/qpcrxdTaskResult.jsp");
	}

	@Action(value = "showQpcrxdTaskResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQpcrxdTaskResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = qpcrxdTaskService
					.findQpcrxdTaskResultList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<QpcrxdTaskResult> list = (List<QpcrxdTaskResult>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("labCode", "");
			map.put("sampleType", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("indexa", "");
			map.put("concentration", "");
			map.put("volume", "");
			map.put("sumTotal", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("submit", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("inspectDate", "yyyy-MM-dd");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("orderId", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("method", "");
			map.put("note", "");
			map.put("qpcrxdTask-name", "");
			map.put("qpcrxdTask-id", "");
			map.put("tempId", "");
			map.put("sampleNum", "");

			map.put("xylr", "");
			map.put("clr", "");
			map.put("flr", "");
			map.put("xzLr", "");
			map.put("qtlr", "");
			map.put("tllr", "");
			map.put("kzxl", "");
			map.put("ect", "");
			map.put("qpcrlr", "");
			map.put("cdlr", "");
			map.put("flux", "");
			map.put("yhhNum", "");
			map.put("probeCode-id", "");
			map.put("probeCode-name", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delQpcrxdTaskResult")
	public void delQpcrxdTaskResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			qpcrxdTaskService.delQpcrxdTaskResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showQpcrxdTaskTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQpcrxdTaskTempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qc/qpcrxd/qpcrxdTaskTemp.jsp");
	}

	@Action(value = "showQpcrxdTaskTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQpcrxdTaskTempListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = qpcrxdTaskService
					.findQpcrxdTaskTempList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<QpcrxdTaskTemp> list = (List<QpcrxdTaskTemp>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("sampleType", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("volume", "");
			map.put("inspectDate", "yyyy-MM-dd");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("orderId", "");
			map.put("state", "");
			map.put("note", "");
			map.put("probeCode-id", "");
			map.put("probeCode-name", "");
			map.put("loopNum", "");
			map.put("productNum", "");
			map.put("volume", "");
			map.put("sampleNum", "");
			map.put("labCode", "");
			map.put("flux", "");
			map.put("wkType", "");
			map.put("xylr", "");
			map.put("clr", "");
			map.put("flr", "");
			map.put("xzLr", "");
			map.put("qtlr", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delQpcrxdTaskTemp")
	public void delQpcrxdTaskTemp() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			qpcrxdTaskService.delQpcrxdTaskTemp(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public QpcrxdTaskService getQpcrxdTaskService() {
		return qpcrxdTaskService;
	}

	public void setQpcrxdTaskService(QpcrxdTaskService qpcrxdTaskService) {
		this.qpcrxdTaskService = qpcrxdTaskService;
	}

	public QpcrxdTask getQpcrxdTask() {
		return qpcrxdTask;
	}

	public void setQpcrxdTask(QpcrxdTask qpcrxdTask) {
		this.qpcrxdTask = qpcrxdTask;
	}

}
