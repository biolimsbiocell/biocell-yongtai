﻿package com.biolims.experiment.qc.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.qc.dao.WKQualitySampleTaskDao;
import com.biolims.experiment.qc.model.Qc2100Task;
import com.biolims.experiment.qc.model.Qc2100TaskCos;
import com.biolims.experiment.qc.model.Qc2100TaskItem;
import com.biolims.experiment.qc.model.Qc2100TaskReagent;
import com.biolims.experiment.qc.model.Qc2100TaskTemplate;
import com.biolims.experiment.qc.model.SampleQc2100Info;
import com.biolims.experiment.qc.service.WKQualitySampleTaskService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

@Namespace("/experiment/qc/wKQualitySampleTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class WKQualitySampleTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "240402";
	@Autowired
	private WKQualitySampleTaskService wKQualitySampleTaskService;
	private Qc2100Task wKQualitySampleTask = new Qc2100Task();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private WKQualitySampleTaskDao wKQualitySampleTaskDao;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;

	@Action(value = "showWKQualitySampleTaskList")
	public String showWKQualitySampleTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qc/wKQualitySampleTask.jsp");
	}

	@Action(value = "showWKQualitySampleTaskListJson")
	public void showWKQualitySampleTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = wKQualitySampleTaskService
				.findWKQualityList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<Qc2100Task> list = (List<Qc2100Task>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("releasUser-id", "");
		map.put("releasUser-name", "");
		map.put("releaseDate", "");
		map.put("testUser-id", "");
		map.put("testUser-name", "");
		map.put("testDate", "yyyy-MM-dd");
		map.put("indexa", "");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd HH:mm:ss");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "wKQualitySampleTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogWKQualityList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qc/wKQualityDialog.jsp");
	}

	@Action(value = "showDialogWKQualityListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogWKQualityListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = wKQualitySampleTaskService
				.findWKQualityList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<Qc2100Task> list = (List<Qc2100Task>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("releasUser-id", "");
		map.put("releasUser-name", "");
		map.put("releaseDate", "");
		map.put("testUser-id", "");
		map.put("testUser-name", "");
		map.put("testDate", "yyyy-MM-dd HH:mm");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editWKQualitySampleTask")
	public String editWKQualitySampleTask() throws Exception {
		String id = getParameterFromRequest("id");
		String maxNum = getParameterFromRequest("maxNum");
		long num = 0;
		if (id != null && !id.equals("")) {
			wKQualitySampleTask = wKQualitySampleTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "wKQualitySampleTask");
			String tName = workflowProcessInstanceService
					.findToDoTaskNameByFormId(id);
			putObjToContext("taskName", tName);
		} else {
			wKQualitySampleTask.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			wKQualitySampleTask.setReleasUser(user);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stime = format.format(date);
			wKQualitySampleTask.setReleaseDate(stime);
			wKQualitySampleTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			wKQualitySampleTask
					.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(wKQualitySampleTask.getState());
		putObjToContext("fileNum", num);
		putObjToContext("maxNum", maxNum);
		return dispatcher("/WEB-INF/page/experiment/qc/wKQualitySampleTaskEdit.jsp");
	}

	@Action(value = "copyWKQualitySampleTask")
	public String copyWKQualitySampleTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		wKQualitySampleTask = wKQualitySampleTaskService.get(id);
		wKQualitySampleTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/qc/wKQualitySampleTaskEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = wKQualitySampleTask.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "Qc2100Task";
			String markCode = "2100QC";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			wKQualitySampleTask.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("wKQualityItem", getParameterFromRequest("wKQualityItemJson"));

		aMap.put("wKQualityTemplate",
				getParameterFromRequest("wKQualityTemplateJson"));

		aMap.put("wKQualityDevice",
				getParameterFromRequest("wKQualityDeviceJson"));

		aMap.put("wKQualitySampleInfo",
				getParameterFromRequest("wKQualitySampleInfoJson"));

		aMap.put("wKQualityReagent",
				getParameterFromRequest("wKQualityReagentJson"));

		wKQualitySampleTaskService.save(wKQualitySampleTask, aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/experiment/qc/wKQualitySampleTask/editWKQualitySampleTask.action?id="
				+ wKQualitySampleTask.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);
		// return
		// redirect("/experiment/qc/wKQualitySampleTask/editWKQualitySampleTask.action?id="
		// + wKQualitySampleTask.getId());

	}

	@Action(value = "viewWKQualitySampleTask")
	public String toViewWKQualitySampleTask() throws Exception {
		String id = getParameterFromRequest("id");
		wKQualitySampleTask = wKQualitySampleTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/qc/wKQualitySampleTaskEdit.jsp");
	}

	@Action(value = "showWKQualityItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWKQualityItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qc/wKQualityItem.jsp");
	}

	@Action(value = "showWKQualityItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWKQualityItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wKQualitySampleTaskService
					.findWKQualityItemList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<Qc2100TaskItem> list = (List<Qc2100TaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("orderNumber", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("orderId", "");
			map.put("wkId", "");
			map.put("indexa", "");
			map.put("stepNum", "");
			map.put("femaleSampleNo", "");
			map.put("molality", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("name", "");
			map.put("wkType", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("qualityProduct-name", "");
			map.put("qualityProduct-id", "");
			map.put("qc2100Task-name", "");
			map.put("qc2100Task-id", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("counts", "");
			map.put("volume", "");
			map.put("unit", "");

			map.put("techTaskId", "");
			map.put("contractId", "");
			map.put("projectId", "");
			map.put("orderType", "");
			map.put("classify", "");
			map.put("sampleType", "");
			map.put("i5", "");
			map.put("i7", "");
			map.put("sampleNum", "");
			map.put("sampleConsume", "");
			map.put("tempId", "");

			map.put("wkConcentration", "");
			map.put("wkVolume", "");
			map.put("wkSumTotal", "");
			map.put("loopNum", "");
			map.put("pcrRatio", "");
			map.put("rin", "");
			map.put("labCode", "");
			map.put("sampleInfo-note", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWKQualityItem")
	public void delWKQualityItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wKQualitySampleTaskService.delWKQualityItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// ============2015-12-02 ly================
	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWKQualityDeviceOne")
	public void delWKQualityDeviceOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			wKQualitySampleTaskService.delWKQualityDeviceOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// ============================
	@Action(value = "showWKQualityTemplateList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWKQualityTemplateList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qc/wKQualityTemplate.jsp");
	}

	@Action(value = "showWKQualityTemplateListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWKQualityTemplateListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wKQualitySampleTaskService
					.findExecuteStepItemList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<Qc2100TaskTemplate> list = (List<Qc2100TaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("reciveUser-name", "");
			map.put("reciveUser-id", "");
			map.put("code", "");
			map.put("stepName", "");
			map.put("tItem", "");
			map.put("state", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("note", "");
			map.put("codes", "");
			map.put("name", "");
			map.put("wKQualitySampleTask-name", "");
			map.put("wKQualitySampleTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWKQualityTemplate")
	public void delWKQualityTemplate() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wKQualitySampleTaskService.delExecuteStepItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// =============2015-12-02 ly=================
	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delExecuteStepItemOne")
	public void delExecuteStepItemOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			wKQualitySampleTaskService.delExecuteStepItemOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// ==============================

	@Action(value = "showWKQualityDeviceList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWKQualityDeviceList() throws Exception {
		// String itemId=getParameterFromRequest("itemId");
		// putObjToContext("itemId",itemId);
		return dispatcher("/WEB-INF/page/experiment/qc/wKQualityDevice.jsp");
	}

	@Action(value = "showWKQualityDeviceListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWKQualityDeviceListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wKQualitySampleTaskService
					.findDeviceItemList(scId, startNum, limitNum, dir, sort);
			// String scId = getRequest().getParameter("id");
			// String itemId=getParameterFromRequest("itemId");
			// Map<String, Object> result=new HashMap<String, Object>();
			// if(itemId.equals("")){
			// result = wKQualitySampleTaskService.findDeviceItemList(scId,
			// startNum, limitNum, dir,
			// sort);
			// }else{
			// result = wKQualitySampleTaskService.findCosItemListByItemId(scId,
			// startNum, limitNum, dir,
			// sort,itemId);
			// }
			Long total = (Long) result.get("total");
			List<Qc2100TaskCos> list = (List<Qc2100TaskCos>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("temperature", "");
			map.put("speed", "");
			map.put("time", "");
			map.put("note", "");
			map.put("tCos", "");
			map.put("isTestSuccess", "");
			map.put("itemId", "");
			map.put("qc2100Task-name", "");
			map.put("qc2100Task-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWKQualityDevice")
	public void delWKQualityDevice() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wKQualitySampleTaskService.delDeviceItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showWKQualitySampleInfoList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWKQualitySampleInfoList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qc/wKQualitySampleInfo.jsp");
	}

	@Action(value = "showWKQualitySampleInfoListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWKQualitySampleInfoListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wKQualitySampleTaskService
					.findQualityResultItemList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<SampleQc2100Info> list = (List<SampleQc2100Info>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("orderId", "");
			map.put("wkId", "");
			map.put("indexa", "");
			map.put("fragmentSize", "");
			map.put("concentration", "");
			map.put("qpcr", "");
			map.put("specific", "");
			map.put("result", "");
			// map.put("handleWay", "");
			map.put("reason", "");
			map.put("note", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("submit", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("wKQualitySampleTask-name", "");
			map.put("wKQualitySampleTask-id", "");
			map.put("name", "");
			map.put("wkType", "");

			map.put("volume", "");
			map.put("unit", "");
			map.put("techTaskId", "");
			map.put("contractId", "");
			map.put("projectId", "");
			map.put("orderType", "");
			map.put("classify", "");
			map.put("sampleType", "");
			map.put("i5", "");
			map.put("i7", "");
			map.put("sampleNum", "");
			map.put("tempId", "");
			map.put("wkConcentration", "");
			map.put("wkVolume", "");
			map.put("wkSumTotal", "");
			map.put("loopNum", "");
			map.put("pcrRatio", "");
			map.put("rin", "");
			map.put("labCode", "");
			map.put("sampleInfo-note", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWKQualitySampleInfo")
	public void delWKQualitySampleInfo() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wKQualitySampleTaskService.delQualityResultItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showWKQualityReagentList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWKQualityReagentList() throws Exception {
		// String itemId=getParameterFromRequest("itemId");
		// putObjToContext("itemId",itemId);
		return dispatcher("/WEB-INF/page/experiment/qc/wKQualityReagent.jsp");
	}

	@Action(value = "showWKQualityReagentListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWKQualityReagentListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wKQualitySampleTaskService
					.findReagentItemList(scId, startNum, limitNum, dir, sort);
			// String scId = getRequest().getParameter("id");
			// String itemId=getParameterFromRequest("itemId");
			// Map<String, Object> result=new HashMap<String, Object>();
			// if(itemId.equals("")){
			// result = wKQualitySampleTaskService.findReagentItemList(scId,
			// startNum, limitNum, dir,
			// sort);
			// }else{
			// result =
			// wKQualitySampleTaskService.findReagentItemListByItemId(scId,
			// startNum, limitNum, dir,
			// sort,itemId);
			// }
			Long total = (Long) result.get("total");
			List<Qc2100TaskReagent> list = (List<Qc2100TaskReagent>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("batch", "");
			map.put("count", "");
			map.put("oneNum", "");
			map.put("sampleNum", "");
			map.put("num", "");
			map.put("tReagent", "");
			map.put("itemId", "");
			map.put("isTestSuccess", "");
			map.put("qc2100Task-name", "");
			map.put("qc2100Task-id", "");
			map.put("sn", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWKQualityReagent")
	public void delWKQualityReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wKQualitySampleTaskService.delReagentItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// ==============2015-12-02 ly===================
	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delReagentListOne")
	public void delReagentListOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			wKQualitySampleTaskService.delReagentItemOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// =================================
	// 判断样本被使用的次数
	@Action(value = "selectCodeCount")
	public void selectCodeCount() throws Exception {
		String code = getParameterFromRequest("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			Long num = wKQualitySampleTaskDao.selectCount(code);
			int num1 = Integer.valueOf(String.valueOf(num));
			result.put("success", true);
			result.put("data", num1);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 模板
	@Action(value = "showTemplateQcWaitList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTemplateQcWaitList() throws Exception {
		String scId = getRequest().getParameter("id");
		putObjToContext("id", scId);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qc/showTemplateQcWaitList.jsp");
	}

	@Action(value = "showTemplateQcWaitListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTemplateQcWaitListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wKQualitySampleTaskService
					.findExecuteStepItemList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<Qc2100TaskTemplate> list = (List<Qc2100TaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("note", "");
			map.put("tItem", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("reciveUser-id", "");
			map.put("reciveUser-name", "");
			map.put("codes", "");
			map.put("state", "");
			map.put("wKQualitySampleTask-name", "");
			map.put("wKQualitySampleTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public WKQualitySampleTaskService getWKQualityService() {
		return wKQualitySampleTaskService;
	}

	public void setWKQualitySampleTaskService(
			WKQualitySampleTaskService wKQualitySampleTaskService) {
		this.wKQualitySampleTaskService = wKQualitySampleTaskService;
	}

	public Qc2100Task getWKQualitySampleTask() {
		return wKQualitySampleTask;
	}

	public void setWKQualitySampleTask(Qc2100Task wKQualitySampleTask) {
		this.wKQualitySampleTask = wKQualitySampleTask;
	}

	// 根据选中的步骤异步加载相关的试剂数据
	@Action(value = "setReagent")
	public void setReagent() throws Exception {
		String code = getRequest().getParameter("code");
		String id = getRequest().getParameter("tid");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.wKQualitySampleTaskService
					.setReagent(id, code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 根据选中的步骤异步加载相关的设备数据
	@Action(value = "setCos")
	public void setCos() throws Exception {
		String code = getRequest().getParameter("code");
		String id = getRequest().getParameter("tid");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.wKQualitySampleTaskService
					.setCos(id, code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 提交样本
	@Action(value = "submitSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void submitSample() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.wKQualitySampleTaskService.submitSample(id, ids);
			result.put("success", true);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
