﻿
package com.biolims.experiment.qc.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.experiment.qc.model.QcCheck;
import com.biolims.experiment.qc.model.QcReceiveItem;
import com.biolims.experiment.qc.service.QualityCheckService;
import com.biolims.experiment.qc.service.WKAcceptService;
import com.biolims.util.DateHelper;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.util.HttpUtils;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
@Namespace("/experiment/qc/qualityCheck")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class QualityCheckAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "240404";
	@Autowired
	private QualityCheckService qualityCheckService;
	private QcCheck qualityCheck = new QcCheck();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private WKAcceptService wKAcceptService;
	@Action(value = "showQualityCheckList")
	public String showQualityCheckList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qc/qualityCheck.jsp");
	}

	@Action(value = "showQualityCheckListJson")
	public void showQualityCheckListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		else
			map2Query.put("nextFlow", "is##@@##null");
		Map<String, Object> result = qualityCheckService.findQualityCheckList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<QcCheck> list = (List<QcCheck>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("sampleCode", "");
		map.put("code", "");
		map.put("wkCode", "");
		map.put("indexa", "");
		map.put("name", "");
		map.put("wkType", "");
		map.put("length", "");
		map.put("qualityConcentrer", "");
		map.put("qpcrCon", "");
		map.put("reason", "");
		map.put("nextFlow", "");
		map.put("result", "");
		map.put("note", "");
		map.put("patientName", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("inspectDate", "");
		map.put("acceptDate", "");
		map.put("idCard", "");
		map.put("phone", "");
		map.put("orderId", "");
		map.put("sequenceFun", "");
		map.put("classify", "");
		map.put("reportDate", "yyyy-MM-dd");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "qualityCheckSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogQualityCheckList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qc/qualityCheckDialog.jsp");
	}

	@Action(value = "showDialogQualityCheckListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogQualityCheckListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = qualityCheckService.findQualityCheckList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<QcCheck> list = (List<QcCheck>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("sampleCode", "");
		map.put("length", "");
		map.put("qualityConcentrer", "");
		map.put("molarity", "");
		map.put("failReason", "");
		map.put("testState", "");
		map.put("handleState", "");
		map.put("qpcrCon", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editQualityCheck")
	public String editQualityCheck() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			qualityCheck = qualityCheckService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "qualityCheck");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//			qualityCheck.setReceiver(user);
//			qualityCheck.setTestState(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/qc/qualityCheckEdit.jsp");
	}

	@Action(value = "copyQualityCheck")
	public String copyQualityCheck() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		qualityCheck = qualityCheckService.get(id);
		qualityCheck.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/qc/qualityCheckEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = qualityCheck.getId();
		if(id!=null&&id.equals("")){
			qualityCheck.setId(null);
		}
		Map aMap = new HashMap();
		qualityCheckService.save(qualityCheck,aMap);
		return redirect("/experiment/qc/qualityCheck/editQualityCheck.action?id=" + qualityCheck.getId());

	}
	//保存质检审核信息
//	@Action(value = "saveQualityCheck")
//	public void saveQualityCheck() throws Exception {
//		//String id=getRequest().getParameter("code");
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			String itemDataJson = getRequest().getParameter("itemDataJson");
//			qualityCheckService.saveQualityCheck(itemDataJson);
//			result.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}
	@Action(value = "viewQualityCheck")
	public String toViewQualityCheck() throws Exception {
		String id = getParameterFromRequest("id");
		qualityCheck = qualityCheckService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/qc/qualityCheckEdit.jsp");
	}
	
	


	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public QualityCheckService getQualityCheckService() {
		return qualityCheckService;
	}

	public void setQualityCheckService(QualityCheckService qualityCheckService) {
		this.qualityCheckService = qualityCheckService;
	}

	public QcCheck getQualityCheck() {
		return qualityCheck;
	}

	public void setQualityCheck(QcCheck qualityCheck) {
		this.qualityCheck = qualityCheck;
	}


}
