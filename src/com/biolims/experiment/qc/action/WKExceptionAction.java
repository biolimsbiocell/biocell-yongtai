﻿package com.biolims.experiment.qc.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.experiment.qc.dao.WKExceptionDao;
import com.biolims.experiment.qc.model.Qc2100Abnormal;
import com.biolims.experiment.qc.model.QcQpcrAbnormal;
import com.biolims.experiment.qc.model.SampleQcPoolingInfo;
import com.biolims.experiment.qc.service.WKExceptionService;
import com.biolims.file.service.FileInfoService;
import com.biolims.project.feedback.model.FeedbackQpcrQuality;
import com.biolims.project.feedback.model.FeedbackQuality;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/qc/wKException")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class WKExceptionAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "240403";
	@Autowired
	private WKExceptionService wKExceptionService;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private WKExceptionDao wKExceptionDao;

	@Action(value = "showWKExceptionList")
	public String showWKExceptionList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qc/wKException.jsp");
	}

	@Action(value = "wKExceptionSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogWKExceptionList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qc/wKExceptionDialog.jsp");
	}

	@Action(value = "qcExceptionMain")
	public String qcExceptionMain() throws Exception {
		Long num1 = wKExceptionDao.selectCount1();
		;
		int num11 = Integer.valueOf(String.valueOf(num1));
		putObjToContext("count1", num11);

		Long num2 = wKExceptionDao.selectCount2();
		;
		int num22 = Integer.valueOf(String.valueOf(num2));
		putObjToContext("count2", num22);

		Long num3 = wKExceptionDao.selectCount3();
		;
		int num33 = Integer.valueOf(String.valueOf(num3));
		putObjToContext("count3", num33);

		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qc/qcExceptionMain.jsp");
	}

	@Action(value = "showWKQualityExceptionList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWKQualityExceptionList() throws Exception {

		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qc/wKQualityException.jsp");
	}

	@Action(value = "showWKQualityExceptionListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWKQualityExceptionListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = wKExceptionService
				.findWKQualityExceptionList(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<FeedbackQuality> list = (List<FeedbackQuality>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("orderId", "");

		map.put("wkCode", "");
		// map.put("fragmentSize", "");
		// map.put("concentration", "");
		// map.put("qpcr", "");

		map.put("indexa", "");
		map.put("specific", "");
		map.put("result", "");
		map.put("reason", "");
		map.put("note", "");
		map.put("nextFlow", "");
		map.put("submit", "");
		map.put("patientName", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("inspectDate", "");
		map.put("acceptDate", "yyyy-MM-dd");
		map.put("idCard", "");
		map.put("phone", "");
		map.put("sequenceFun", "");
		map.put("reportDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("method", "");
		map.put("isRun", "");
		// map.put("wKQualitySampleTask-name", "");
		// map.put("wKQualitySampleTask-id", "");
		map.put("name", "");
		map.put("wkType", "");
		map.put("classify", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());

	}

	@Action(value = "showWKQpcrExceptionList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWKQpcrExceptionList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qc/wKQpcrException.jsp");
	}

	@Action(value = "showWKQpcrExceptionListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWKQpcrExceptionListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = wKExceptionService
				.findSampleQcQpcrInfoList(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<FeedbackQpcrQuality> list = (List<FeedbackQpcrQuality>) result
				.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("orderId", "");
		map.put("wkCode", "");
		map.put("indexa", "");
		// map.put("fragmentSize", "");
		// map.put("concentration", "");
		// map.put("qpcr", "");
		// map.put("specific", "");
		map.put("result", "");
		map.put("nextFlow", "");
		map.put("reason", "");
		map.put("note", "");
		map.put("submit", "");
		map.put("patientName", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("inspectDate", "");
		map.put("acceptDate", "yyyy-MM-dd");
		map.put("idCard", "");
		map.put("phone", "");
		map.put("sequenceFun", "");
		map.put("reportDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("method", "");
		map.put("isRun", "");
		map.put("name", "");
		map.put("wkType", "");
		map.put("classify", "");
		// map.put("wkQpcrSampleTask-name", "");
		// map.put("wkQpcrSampleTask-id", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());

	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	// @Action(value = "delWKQualityException")
	// public void delWKQualityException() throws Exception {
	// Map<String, Object> map = new HashMap<String, Object>();
	// try {
	// String[] ids = getRequest().getParameterValues("ids[]");
	// wKExceptionService.delWKQualityException(ids);
	// map.put("success", true);
	// } catch (Exception e) {
	// e.printStackTrace();
	// map.put("success", false);
	// }
	// HttpUtils.write(JsonUtils.toJsonString(map));
	// }

	@Action(value = "showQcPoolingAbnormalList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQcPoolingAbnormalList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qc/qcPoolingAbnormal.jsp");
	}

	@Action(value = "showQcPoolingAbnormalListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQcPoolingAbnormalListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = wKExceptionService
				.findQcPoolingAbnormalList(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<SampleQcPoolingInfo> list = (List<SampleQcPoolingInfo>) result
				.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("poolingCode", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("orderId", "");
		map.put("wkId", "");
		map.put("indexa", "");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("sequencingReadLong", "");
		map.put("sequencingType", "");
		map.put("sequencingPlatform", "");
		map.put("product-id", "");
		map.put("product-name", "");
		map.put("totalAmount", "");
		map.put("totalVolume", "");
		map.put("fragmentSize", "");
		map.put("concentration", "");
		map.put("qpcr", "");
		map.put("specific", "");
		map.put("result", "");
		map.put("nextFlow", "");
		map.put("reason", "");
		map.put("note", "");
		map.put("submit", "");
		map.put("patientName", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("inspectDate", "");
		map.put("acceptDate", "yyyy-MM-dd");
		map.put("idCard", "");
		map.put("phone", "");
		map.put("sequenceFun", "");
		map.put("reportDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("method", "");
		map.put("isExecute", "");
		map.put("name", "");
		map.put("wkType", "");
		map.put("qcQpcrTask-name", "");
		map.put("qcQpcrTask-id", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());

	}

	// 保存文库异常信息
//	@Action(value = "saveWKQualityException")
//	public void saveWKQualityException() throws Exception {
//		// String id=getRequest().getParameter("code");
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			String itemDataJson = getRequest().getParameter("itemDataJson");
//			wKExceptionService.saveWKQualityException(itemDataJson);
//			result.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}

	// 保存QPCR异常信息
//	@Action(value = "saveWKQpcrException")
//	public void saveWKQpcrException() throws Exception {
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			String itemDataJson = getRequest().getParameter("itemDataJson");
//			wKExceptionService.saveWKQpcrException(itemDataJson);
//			result.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}

	// 保存pooling异常信息
//	@Action(value = "saveQcPoolingAbnormal")
//	public void saveQcPoolingAbnormal() throws Exception {
//		// String id=getRequest().getParameter("code");
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			String itemDataJson = getRequest().getParameter("itemDataJson");
//			wKExceptionService.saveQcPoolingAbnormal(itemDataJson);
//			result.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public WKExceptionService getWKExceptionService() {
		return wKExceptionService;
	}

	public void setWKExceptionService(WKExceptionService wKExceptionService) {
		this.wKExceptionService = wKExceptionService;
	}

	// Qc2100异常
	@Action(value = "showQc2100AbnormalList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQc2100AbnormalList() throws Exception {

		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qc/wKQualityException.jsp");
	}

	@Action(value = "showQc2100AbnormalListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQc2100AbnormalListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = wKExceptionService.findQc2100AbnormalList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<Qc2100Abnormal> list = (List<Qc2100Abnormal>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("orderId", "");
		map.put("wkCode", "");
		map.put("indexa", "");
		map.put("name", "");
		map.put("wkType", "");
		map.put("length", "");
		map.put("qualityConcentrer", "");
		map.put("reason", "");
		map.put("nextFlowId", "");
		map.put("nextFlow", "");
		map.put("result", "");
		map.put("method", "");
		map.put("note", "");
		map.put("patientName", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("inspectDate", "");
		map.put("acceptDate", "yyyy-MM-dd");
		map.put("idCard", "");
		map.put("phone", "");
		map.put("orderId", "");
		map.put("sequenceFun", "");
		map.put("reportDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("submit", "");
		map.put("isRun", "");
		map.put("batch", "");
		map.put("classify", "");
		map.put("sampleType", "");
		map.put("i5", "");
		map.put("i7", "");
		map.put("sampleNum", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());

	}

	// QPCR异常
	@Action(value = "showQcQpcrAbnormalList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQcQpcrAbnormalList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qc/wKQpcrException.jsp");
	}

	@Action(value = "showQcQpcrAbnormalListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQcQpcrAbnormalListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = wKExceptionService.findQcQpcrAbnormalList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<QcQpcrAbnormal> list = (List<QcQpcrAbnormal>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("orderId", "");
		map.put("wkCode", "");
		map.put("indexa", "");
		map.put("name", "");
		map.put("wkType", "");
		map.put("length", "");
		map.put("qualityConcentrer", "");
		map.put("reason", "");
		map.put("nextFlowId", "");
		map.put("nextFlow", "");
		map.put("result", "");
		map.put("method", "");
		map.put("note", "");
		map.put("patientName", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("inspectDate", "");
		map.put("acceptDate", "yyyy-MM-dd");
		map.put("idCard", "");
		map.put("phone", "");
		map.put("orderId", "");
		map.put("sequenceFun", "");
		map.put("reportDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("submit", "");
		map.put("isRun", "");
		map.put("batch", "");
		map.put("classify", "");
		map.put("sampleType", "");
		map.put("i5", "");
		map.put("i7", "");
		map.put("sampleNum", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());

	}

}
