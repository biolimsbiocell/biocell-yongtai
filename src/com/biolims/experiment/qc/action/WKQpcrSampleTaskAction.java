package com.biolims.experiment.qc.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.qc.dao.WKQpcrSampleTaskDao;
import com.biolims.experiment.qc.model.QcPoolingTaskItem;
import com.biolims.experiment.qc.model.QcQpcrTask;
import com.biolims.experiment.qc.model.QcQpcrTaskCos;
import com.biolims.experiment.qc.model.QcQpcrTaskItem;
import com.biolims.experiment.qc.model.QcQpcrTaskReagent;
import com.biolims.experiment.qc.model.QcQpcrTaskTemplate;
import com.biolims.experiment.qc.model.SampleQcPoolingInfo;
import com.biolims.experiment.qc.model.SampleQcQpcrInfo;
import com.biolims.experiment.qc.service.WKQpcrSampleTaskService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

@Namespace("/experiment/qc/wKQpcrSampleTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class WKQpcrSampleTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "240405";
	@Autowired
	private WKQpcrSampleTaskService wKQpcrSampleTaskService;
	private QcQpcrTask wKQpcrSampleTask = new QcQpcrTask();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private WKQpcrSampleTaskDao wKQpcrSampleTaskDao;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;

	@Action(value = "showWKQpcrSampleTaskList")
	public String showWKQpcrSampleTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qc/wKQpcrSampleTask.jsp");
	}

	@Action(value = "showWKQpcrSampleTaskListJson")
	public void showWKQpcrSampleTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = wKQpcrSampleTaskService.findWKQpcrList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<QcQpcrTask> list = (List<QcQpcrTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("releasUser-id", "");
		map.put("releasUser-name", "");
		map.put("releaseDate", "");
		map.put("testUser-id", "");
		map.put("testUser-name", "");
		map.put("testDate", "yyyy-MM-dd");
		map.put("indexa", "");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("state", "");
		map.put("stateName", "");
		// 实验组
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd HH:mm:ss");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "wKQpcrSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogWKQpcrList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qc/wKQpcrDialog.jsp");
	}

	@Action(value = "showDialogWKQpcrListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogWKQpcrListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = wKQpcrSampleTaskService.findWKQpcrList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<QcQpcrTask> list = (List<QcQpcrTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("releasUser-id", "");
		map.put("releasUser-name", "");
		map.put("releaseDate", "");
		map.put("testUser-id", "");
		map.put("testUser-name", "");
		map.put("testDate", "yyyy-MM-dd HH:mm");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editWKQpcrSampleTask")
	public String editWKQpcrSampleTask() throws Exception {
		String id = getParameterFromRequest("id");
		String maxNum = getParameterFromRequest("maxNum");
		long num = 0;
		if (id != null && !id.equals("")) {
			wKQpcrSampleTask = wKQpcrSampleTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "wKQpcrSampleTask");
			String tName = workflowProcessInstanceService
					.findToDoTaskNameByFormId(id);
			putObjToContext("taskName", tName);
		} else {
			wKQpcrSampleTask.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			wKQpcrSampleTask.setReleasUser(user);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stime = format.format(date);
			wKQpcrSampleTask.setReleaseDate(stime);
			wKQpcrSampleTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			wKQpcrSampleTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(wKQpcrSampleTask.getState());
		putObjToContext("fileNum", num);
		putObjToContext("maxNum", maxNum);
		return dispatcher("/WEB-INF/page/experiment/qc/wKQpcrSampleTaskEdit.jsp");
	}

	@Action(value = "copyWKQpcrSampleTask")
	public String copyWKQpcrSampleTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		wKQpcrSampleTask = wKQpcrSampleTaskService.get(id);
		wKQpcrSampleTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/qc/wKQpcrSampleTaskEdit.jsp");
	}

//	@Action(value = "save")
//	public String save() throws Exception {
//		String id = wKQpcrSampleTask.getId();
//		if ((id != null && id.equals("")) || id.equals("NEW")) {
//			String modelName = "QcQpcrTask";
//			String markCode = "QPCR";
//			String autoID = codingRuleService.genTransID(modelName, markCode);
//			wKQpcrSampleTask.setId(autoID);
//		}
//		Map aMap = new HashMap();
//		aMap.put("wKQpcrItem", getParameterFromRequest("wKQpcrItemJson"));
//
//		aMap.put("qcQpcrTaskItemm",
//				getParameterFromRequest("qcQpcrTaskItemmJson"));
//
//		aMap.put("wKQpcrTemplate",
//				getParameterFromRequest("wKQpcrTemplateJson"));
//
//		aMap.put("wKQpcrDevice", getParameterFromRequest("wKQpcrDeviceJson"));
//
//		aMap.put("wKQpcrReagent", getParameterFromRequest("wKQpcrReagentJson"));
//
//		aMap.put("wKQpcrSampleInfo",
//				getParameterFromRequest("wKQpcrSampleInfoJson"));
//
//		aMap.put("sampleQcPoolingInfo",
//				getParameterFromRequest("sampleQcPoolingInfoJson"));
//
//		wKQpcrSampleTaskService.save(wKQpcrSampleTask, aMap);
//		String bpmTaskId = getParameterFromRequest("bpmTaskId");
//		String url = "/experiment/qc/wKQpcrSampleTask/editWKQpcrSampleTask.action?id="
//				+ wKQpcrSampleTask.getId();
//		if (bpmTaskId != null && !bpmTaskId.equals("")
//				&& !bpmTaskId.equals("null"))
//			url += "&bpmTaskId=" + bpmTaskId;
//		return redirect(url);
//		// return
//		// redirect("/experiment/qc/wKQpcrSampleTask/editWKQpcrSampleTask.action?id="
//		// + wKQpcrSampleTask.getId());
//
//	}

	@Action(value = "viewWKQpcrSampleTask")
	public String toViewWKQpcrSampleTask() throws Exception {
		String id = getParameterFromRequest("id");
		wKQpcrSampleTask = wKQpcrSampleTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/qc/wKQpcrSampleTaskEdit.jsp");
	}

	@Action(value = "showWKQpcrItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWKQpcrItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qc/wKQpcrItem.jsp");
	}

	@Action(value = "showWKQpcrItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWKQpcrItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wKQpcrSampleTaskService
					.findWKQpcrItemList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<QcQpcrTaskItem> list = (List<QcQpcrTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("orderNumber", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("orderId", "");
			map.put("wkId", "");
			map.put("indexa", "");
			map.put("stepNum", "");
			map.put("femaleSampleNo", "");
			map.put("molality", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("name", "");
			map.put("wkType", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("qualityProduct-name", "");
			map.put("qualityProduct-id", "");
			map.put("qcQpcrTask-name", "");
			map.put("qcQpcrTask-id", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("counts", "");
			map.put("volume", "");
			map.put("unit", "");
			map.put("techTaskId", "");
			map.put("contractId", "");
			map.put("projectId", "");
			map.put("orderType", "");
			map.put("classify", "");
			map.put("sampleType", "");
			map.put("i5", "");
			map.put("i7", "");
			map.put("sampleNum", "");
			map.put("sampleConsume", "");
			map.put("tempId", "");
			map.put("wkConcentration", "");
			map.put("wkVolume", "");
			map.put("wkSumTotal", "");
			map.put("loopNum", "");
			map.put("pcrRatio", "");
			map.put("rin", "");
			map.put("labCode", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWKQpcrItem")
	public void delWKQpcrItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wKQpcrSampleTaskService.delWKQpcrItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// pooling明细表
	@Action(value = "showQcQpcrTaskItemmList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQcQpcrTaskItemmList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qc/qcQpcrTaskItemm.jsp");
	}

	@Action(value = "showQcQpcrTaskItemmListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQcQpcrTaskItemmListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wKQpcrSampleTaskService
					.findQcQpcrTaskItemmList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<QcPoolingTaskItem> list = (List<QcPoolingTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("orderNumber", "");
			map.put("name", "");
			map.put("note", "");
			map.put("state", "");
			map.put("poolingCode", "");
			map.put("template-id", "");
			map.put("template-name", "");
			map.put("sequencingReadLong", "");
			map.put("sequencingType", "");
			map.put("sequencingPlatform", "");
			map.put("product-id", "");
			map.put("product-name", "");
			map.put("totalAmount", "");
			map.put("totalVolume", "");
			map.put("others", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("stepNum", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("qcQpcrTask-name", "");
			map.put("qcQpcrTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除pooling明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delQcQpcrTaskItemm")
	public void delQcQpcrTaskItemm() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wKQpcrSampleTaskService.delQcQpcrTaskItemm(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// ============2015-12-02 ly================
	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWKQpcrDeviceOne")
	public void delWKQpcrDeviceOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			wKQpcrSampleTaskService.delWKQpcrDeviceOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// ============================
	@Action(value = "showWKQpcrTemplateList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWKQpcrTemplateList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qc/wKQpcrTemplate.jsp");
	}

	@Action(value = "showWKQpcrTemplateListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWKQpcrTemplateListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wKQpcrSampleTaskService
					.findWKQpcrTemplateList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<QcQpcrTaskTemplate> list = (List<QcQpcrTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("reciveUser-name", "");
			map.put("reciveUser-id", "");
			map.put("code", "");
			map.put("stepName", "");
			map.put("tItem", "");
			map.put("state", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("note", "");
			map.put("codes", "");
			map.put("name", "");
			map.put("qcQpcrTask-name", "");
			map.put("qcQpcrTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWKQpcrTemplate")
	public void delWKQpcrTemplate() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wKQpcrSampleTaskService.delWKQpcrTemplate(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// =============2015-12-02 ly=================
	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWKQpcrTemplateOne")
	public void delWKQpcrTemplateOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			wKQpcrSampleTaskService.delWKQpcrTemplateOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// ==============================

	@Action(value = "showWKQpcrDeviceList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWKQpcrDeviceList() throws Exception {
		// String itemId=getParameterFromRequest("itemId");
		// putObjToContext("itemId",itemId);
		return dispatcher("/WEB-INF/page/experiment/qc/wKQpcrDevice.jsp");
	}

	@Action(value = "showWKQpcrDeviceListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWKQpcrDeviceListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wKQpcrSampleTaskService
					.findWKQpcrDeviceList(scId, startNum, limitNum, dir, sort);
			// String scId = getRequest().getParameter("id");
			// String itemId=getParameterFromRequest("itemId");
			// Map<String, Object> result=new HashMap<String, Object>();
			// if(itemId.equals("")){
			// result = wKQpcrSampleTaskService.findWKQpcrDeviceList(scId,
			// startNum, limitNum, dir,
			// sort);
			// }else{
			// result = wKQpcrSampleTaskService.findCosItemListByItemId(scId,
			// startNum, limitNum, dir,
			// sort,itemId);
			// }
			Long total = (Long) result.get("total");
			List<QcQpcrTaskCos> list = (List<QcQpcrTaskCos>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("temperature", "");
			map.put("speed", "");
			map.put("time", "");
			map.put("note", "");
			map.put("tCos", "");
			map.put("isTestSuccess", "");
			map.put("itemId", "");
			map.put("qcQpcrTask-name", "");
			map.put("qcQpcrTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWKQpcrDevice")
	public void delWKQpcrDevice() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wKQpcrSampleTaskService.delWKQpcrDevice(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showWKQpcrSampleInfoList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWKQpcrSampleInfoList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qc/wKQpcrSampleInfo.jsp");
	}

	@Action(value = "showWKQpcrSampleInfoListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWKQpcrSampleInfoListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wKQpcrSampleTaskService
					.findWKQpcrResultList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<SampleQcQpcrInfo> list = (List<SampleQcQpcrInfo>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("orderId", "");
			map.put("wkId", "");
			map.put("indexa", "");
			map.put("fragmentSize", "");
			map.put("concentration", "");
			map.put("qpcr", "");
			map.put("specific", "");
			map.put("result", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("reason", "");
			map.put("note", "");
			map.put("submit", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("qcQpcrTask-name", "");
			map.put("qcQpcrTask-id", "");
			map.put("name", "");
			map.put("wkType", "");

			map.put("volume", "");
			map.put("unit", "");
			map.put("techTaskId", "");
			map.put("contractId", "");
			map.put("projectId", "");
			map.put("orderType", "");
			map.put("classify", "");
			map.put("sampleType", "");
			map.put("i5", "");
			map.put("i7", "");
			map.put("sampleNum", "");
			map.put("tempId", "");
			map.put("her2", "");
			map.put("er", "");
			map.put("pr", "");
			map.put("rs", "");
			map.put("wkConcentration", "");
			map.put("wkVolume", "");
			map.put("wkSumTotal", "");
			map.put("loopNum", "");
			map.put("pcrRatio", "");
			map.put("rin", "");
			map.put("labCode", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWKQpcrSampleInfo")
	public void delWKQpcrSampleInfo() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wKQpcrSampleTaskService.delWKQpcrResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// pooling结果表
	@Action(value = "showSampleQcPoolingInfoList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleQcPoolingInfoList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qc/sampleQcPoolingInfo.jsp");
	}

	@Action(value = "showSampleQcPoolingInfoListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleQcPoolingInfoListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wKQpcrSampleTaskService
					.findSampleQcPoolingInfoList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<SampleQcPoolingInfo> list = (List<SampleQcPoolingInfo>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("poolingCode", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("orderId", "");
			map.put("wkId", "");
			map.put("indexa", "");
			map.put("template-id", "");
			map.put("template-name", "");
			map.put("sequencingReadLong", "");
			map.put("sequencingType", "");
			map.put("sequencingPlatform", "");
			map.put("product-id", "");
			map.put("product-name", "");
			map.put("totalAmount", "");
			map.put("totalVolume", "");
			map.put("fragmentSize", "");
			map.put("concentration", "");
			map.put("qpcr", "");
			map.put("specific", "");
			map.put("result", "");
			map.put("nextFlow", "");
			map.put("reason", "");
			map.put("note", "");
			map.put("submit", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("qcQpcrTask-name", "");
			map.put("qcQpcrTask-id", "");
			map.put("name", "");
			map.put("wkType", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSampleQcPoolingInfo")
	public void delSampleQcPoolingInfo() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wKQpcrSampleTaskService.delSampleQcPoolingInfo(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showWKQpcrReagentList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWKQpcrReagentList() throws Exception {
		// String itemId=getParameterFromRequest("itemId");
		// putObjToContext("itemId",itemId);
		return dispatcher("/WEB-INF/page/experiment/qc/wKQpcrReagent.jsp");
	}

	@Action(value = "showWKQpcrReagentListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWKQpcrReagentListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wKQpcrSampleTaskService
					.findWKQpcrReagentList(scId, startNum, limitNum, dir, sort);
			// String scId = getRequest().getParameter("id");
			// String itemId=getParameterFromRequest("itemId");
			// Map<String, Object> result=new HashMap<String, Object>();
			// if(itemId.equals("")){
			// result = wKQpcrSampleTaskService.findWKQpcrReagentList(scId,
			// startNum, limitNum, dir,
			// sort);
			// }else{
			// result =
			// wKQpcrSampleTaskService.findReagentItemListByItemId(scId,
			// startNum, limitNum, dir,
			// sort,itemId);
			// }
			Long total = (Long) result.get("total");
			List<QcQpcrTaskReagent> list = (List<QcQpcrTaskReagent>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("batch", "");
			map.put("count", "");
			map.put("oneNum", "");
			map.put("sampleNum", "");
			map.put("num", "");
			map.put("tReagent", "");
			map.put("itemId", "");
			map.put("isTestSuccess", "");
			map.put("qcQpcrTask-name", "");
			map.put("qcQpcrTask-id", "");
			map.put("sn", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWKQpcrReagent")
	public void delWKQpcrReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wKQpcrSampleTaskService.delWKQpcrReagent(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// ==============2015-12-02 ly===================
	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWKQpcrReagentOne")
	public void delWKQpcrReagentOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			wKQpcrSampleTaskService.delWKQpcrReagentOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// =================================
	// 判断样本被使用的次数
	@Action(value = "selectCodeCount")
	public void selectCodeCount() throws Exception {
		String code = getParameterFromRequest("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			Long num = wKQpcrSampleTaskDao.selectCount(code);
			int num1 = Integer.valueOf(String.valueOf(num));
			result.put("success", true);
			result.put("data", num1);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 模板
	@Action(value = "showTemplateWaitList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTemplateWaitList() throws Exception {
		String scId = getRequest().getParameter("id");
		putObjToContext("id", scId);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qc/showTemplateWaitList.jsp");
	}

	@Action(value = "showTemplateWaitListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTemplateWaitListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wKQpcrSampleTaskService
					.findWKQpcrTemplateList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<QcQpcrTaskTemplate> list = (List<QcQpcrTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("note", "");
			map.put("tItem", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("reciveUser-id", "");
			map.put("reciveUser-name", "");
			map.put("codes", "");
			map.put("state", "");
			map.put("qcQpcrTask-name", "");
			map.put("qcQpcrTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public WKQpcrSampleTaskService getWKQpcrService() {
		return wKQpcrSampleTaskService;
	}

	public void setWKQpcrSampleTaskService(
			WKQpcrSampleTaskService wKQpcrSampleTaskService) {
		this.wKQpcrSampleTaskService = wKQpcrSampleTaskService;
	}

	public QcQpcrTask getWKQpcrSampleTask() {
		return wKQpcrSampleTask;
	}

	public void setWKQpcrSampleTask(QcQpcrTask wKQpcrSampleTask) {
		this.wKQpcrSampleTask = wKQpcrSampleTask;
	}

	// 根据选中的步骤异步加载相关的试剂数据
	@Action(value = "setReagent")
	public void setReagent() throws Exception {
		String code = getRequest().getParameter("code");
		String id = getRequest().getParameter("tid");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.wKQpcrSampleTaskService
					.setReagent(id, code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 根据选中的步骤异步加载相关的设备数据
	@Action(value = "setCos")
	public void setCos() throws Exception {
		String code = getRequest().getParameter("code");
		String id = getRequest().getParameter("tid");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.wKQpcrSampleTaskService
					.setCos(id, code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 提交样本
	@Action(value = "submitSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void submitSample() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.wKQpcrSampleTaskService.submitSample(id, ids);
			result.put("success", true);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
