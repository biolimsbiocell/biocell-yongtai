﻿package com.biolims.experiment.qc.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.experiment.qc.model.QcPoolingTaskTemp;
import com.biolims.experiment.qc.model.QcReceive;
import com.biolims.experiment.qc.model.QcReceiveItem;
import com.biolims.experiment.qc.model.QcReceiveTemp;
import com.biolims.experiment.qc.model.QcQpcrTaskTemp;
import com.biolims.experiment.qc.model.Qc2100TaskTemp;
import com.biolims.experiment.qc.model.TechCheckServiceQcOrder;
import com.biolims.experiment.qc.model.TechCheckServiceQcTemp;
import com.biolims.experiment.qc.service.WKAcceptService;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.util.HttpUtils;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;

@Namespace("/experiment/qc/wKAccept")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class WKAcceptAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "240401";
	@Autowired
	private WKAcceptService wKAcceptService;
	private QcReceive wKAccept = new QcReceive();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;

	@Action(value = "showWKAcceptList")
	public String showWKAcceptList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qc/wKAccept.jsp");
	}

	@Action(value = "showWKAcceptListJson")
	public void showWKAcceptListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = wKAcceptService.findWKAcceptList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<QcReceive> list = (List<QcReceive>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("receiveUser-id", "");
		map.put("receiveUser-name", "");
		map.put("receiverDate", "");
		map.put("location", "");
		map.put("stateName", "");
		map.put("state", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	/**
	 * 左侧任务单表
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showWKAcceptTechOrderList")
	public String showWKAcceptTechOrderList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qc/wKAcceptTechOrder.jsp");
	}

	@Action(value = "showWKAcceptTechOrderListJson")
	public void showWKAcceptTechOrderListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = wKAcceptService
				.findTechCheckServiceQcOrder(map2Query, startNum, limitNum,
						dir, sort);
		Long count = (Long) result.get("total");
		List<TechCheckServiceQcOrder> list = (List<TechCheckServiceQcOrder>) result
				.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("projectId", "");
		map.put("projectName", "");

		map.put("contractId", "");
		map.put("orderType", "");
		map.put("cooperation", "");
		map.put("projectLeader", "");
		map.put("testLeader", "");

		map.put("analysisLeader", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "showTempList")
	public String showTempList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qc/wKAcceptTechOrder.jsp");
	}

	/**
	 * 左侧科技服务样本表
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showWKAcceptTechTempListJson")
	public void showWKAcceptTechTempListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = wKAcceptService
				.findTechCheckServiceQcTemp(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<TechCheckServiceQcTemp> list = (List<TechCheckServiceQcTemp>) result
				.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("sampleCode", "");
		map.put("wkCode", "");
		map.put("pdSzie", "");
		map.put("massCon", "");
		map.put("molarCon", "");
		map.put("volume", "");
		map.put("pdResult", "");
		map.put("molarResult", "");
		map.put("orderId", "");
		map.put("orderType", "");
		map.put("state", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "wKAcceptSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogWKAcceptList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/qc/wKAcceptDialog.jsp");
	}

	@Action(value = "showDialogWKAcceptListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogWKAcceptListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = wKAcceptService.findWKAcceptList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<QcReceive> list = (List<QcReceive>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("receiveUser-id", "");
		map.put("receiveUser-name", "");
		map.put("receiverDate", "");
		map.put("location", "");
		map.put("stateName", "");
		map.put("state", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editWKAccept")
	public String editWKAccept() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			wKAccept = wKAcceptService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "wKAccept");
		} else {
			wKAccept.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			wKAccept.setReceiveUser(user);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stime = format.format(date);
			wKAccept.setReceiverDate(stime);
			wKAccept.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			wKAccept.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(wKAccept.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/qc/wKAcceptEdit.jsp");
	}

	@Action(value = "copyWKAccept")
	public String copyWKAccept() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		wKAccept = wKAcceptService.get(id);
		wKAccept.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/qc/wKAcceptEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = wKAccept.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "QcReceive";
			String markCode = "ZKJJ";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			wKAccept.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("wKAcceptItem", getParameterFromRequest("wKAcceptItemJson"));

		aMap.put("poolingAcceptItem",
				getParameterFromRequest("poolingAcceptItemJson"));

		wKAcceptService.save(wKAccept, aMap);
		return redirect("/experiment/qc/wKAccept/editWKAccept.action?id="
				+ wKAccept.getId());

	}

	@Action(value = "viewWKAccept")
	public String toViewWKAccept() throws Exception {
		String id = getParameterFromRequest("id");
		wKAccept = wKAcceptService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/qc/wKAcceptEdit.jsp");
	}

	@Action(value = "showWKAcceptItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWKAcceptItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qc/wKAcceptItem.jsp");
	}

	@Action(value = "showWKAcceptItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWKAcceptItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wKAcceptService.findWKAcceptItemList(
					scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<QcReceiveItem> list = (List<QcReceiveItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("wkCode", "");
			map.put("name", "");
			map.put("sampleCode", "");
			map.put("wkType", "");
			map.put("wkVolume", "");
			map.put("fragminSize", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("note", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("location", "");
			map.put("state", "");
			map.put("wkAccept-name", "");
			map.put("wkAccept-id", "");

			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("counts", "");
			map.put("xyName", "");
			map.put("techTaskId", "");
			map.put("contractId", "");
			map.put("projectId", "");
			map.put("orderType", "");
			map.put("classify", "");
			map.put("sampleType", "");
			map.put("i5", "");
			map.put("i7", "");
			map.put("indexa", "");
			map.put("sampleNum", "");
			map.put("tempId", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWKAcceptItem")
	public void delWKAcceptItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wKAcceptService.delWKAcceptItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showPoolingAcceptItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showPoolingAcceptItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qc/poolingAcceptItem.jsp");
	}

	// 查询临时表
	@Action(value = "showWKAcceptTemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWKAcceptTempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qc/wKAcceptTem.jsp");
	}

	@Action(value = "showWKAcceptTemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWKAcceptTemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			// String scId = getRequest().getParameter("id");
			Map<String, Object> result = wKAcceptService.findWKAcceptTemList(
					startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<QcReceiveTemp> list = (List<QcReceiveTemp>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("wkType", "");
			map.put("wkCode", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("volume", "");
			map.put("unit", "");
			map.put("idCard", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("orderId", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("counts", "");
			map.put("techTaskId", "");
			map.put("contractId", "");
			map.put("projectId", "");
			map.put("orderType", "");
			map.put("classify", "");
			map.put("sampleType", "");
			map.put("i5", "");
			map.put("i7", "");
			map.put("indexa", "");
			map.put("sampleNum", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 2100质控左侧表查询
	@Action(value = "showWKQualityTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWKQualityTempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qc/wKQualityTemp.jsp");
	}

	@Action(value = "showWKQualityTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWKQualityTempListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			// String scId = getRequest().getParameter("id");
			Map<String, Object> result = wKAcceptService.findWKQualityTempList(
					startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<Qc2100TaskTemp> list = (List<Qc2100TaskTemp>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("wkCode", "");
			map.put("name", "");
			map.put("sampleCode", "");
			map.put("wkType", "");
			map.put("wkVolume", "");
			map.put("fragminSize", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("note", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("location", "");
			map.put("state", "");
			map.put("wkAccept-name", "");
			map.put("wkAccept-id", "");
			map.put("volume", "");
			map.put("unit", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("counts", "");
			map.put("techTaskId", "");
			map.put("contractId", "");
			map.put("projectId", "");
			map.put("orderType", "");
			map.put("classify", "");
			map.put("sampleType", "");
			map.put("i5", "");
			map.put("i7", "");
			map.put("indexa", "");
			map.put("sampleNum", "");
			map.put("wkConcentration", "");
			map.put("wkVolume", "");
			map.put("wkSumTotal", "");
			map.put("loopNum", "");
			map.put("pcrRatio", "");
			map.put("rin", "");
			map.put("labCode", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// QPCR质控左侧表查询
	@Action(value = "showWKQpcrTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWKQpcrTempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/qc/wKQpcrTemp.jsp");
	}

	@Action(value = "showWKQpcrTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWKQpcrTempListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			// String scId = getRequest().getParameter("id");
			Map<String, Object> result = wKAcceptService.findWKQpcrTempList(
					startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<QcQpcrTaskTemp> list = (List<QcQpcrTaskTemp>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("wkCode", "");
			map.put("name", "");
			map.put("sampleCode", "");
			map.put("wkType", "");
			map.put("wkVolume", "");
			map.put("fragminSize", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("note", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("location", "");
			map.put("state", "");
			map.put("wkAccept-name", "");
			map.put("wkAccept-id", "");

			map.put("volume", "");
			map.put("unit", "");

			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("counts", "");
			map.put("techTaskId", "");
			map.put("contractId", "");
			map.put("projectId", "");
			map.put("orderType", "");
			map.put("classify", "");
			map.put("sampleType", "");
			map.put("i5", "");
			map.put("i7", "");
			map.put("indexa", "");
			map.put("sampleNum", "");
			map.put("wkConcentration", "");
			map.put("wkVolume", "");
			map.put("wkSumTotal", "");
			map.put("loopNum", "");
			map.put("pcrRatio", "");
			map.put("rin", "");
			map.put("labCode", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// //QPCR质控左下侧表查询
	// @Action(value = "showQcQpcrTaskTemppList", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public String showQcQpcrTaskTemppList() throws Exception {
	// return dispatcher("/WEB-INF/page/experiment/qc/qcQpcrTaskTempp.jsp");
	// }

	@Action(value = "showQcQpcrTaskTemppListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQcQpcrTaskTemppListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			// String scId = getRequest().getParameter("id");
			Map<String, Object> result = wKAcceptService
					.findQcQpcrTaskTemppList(startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<QcPoolingTaskTemp> list = (List<QcPoolingTaskTemp>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("reciveUser-id", "");
			map.put("reciveUser-name", "");
			map.put("reciveDate", "yyyy-MM-dd");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("note", "");
			map.put("poolingCode", "");
			map.put("template-id", "");
			map.put("template-name", "");
			map.put("sequencingReadLong", "");
			map.put("sequencingType", "");
			map.put("sequencingPlatform", "");
			map.put("product-id", "");
			map.put("product-name", "");
			map.put("totalAmount", "");
			map.put("totalVolume", "");
			map.put("others", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("patient", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "");
			map.put("acceptDate", "");
			map.put("idCard", "");
			map.put("sequencingFun", "");
			map.put("result", "");
			map.put("orderId", "");
			map.put("phone", "");
			map.put("method", "");
			map.put("reason", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public WKAcceptService getWKAcceptService() {
		return wKAcceptService;
	}

	public void setWKAcceptService(WKAcceptService wKAcceptService) {
		this.wKAcceptService = wKAcceptService;
	}

	public QcReceive getWKAccept() {
		return wKAccept;
	}

	public void setWKAccept(QcReceive wKAccept) {
		this.wKAccept = wKAccept;
	}

	/**
	 * 根据选中的任务单异步加载相关的样本
	 * 
	 * @throws Exception
	 */
	@Action(value = "setTempByOrder", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void setTempByOrder() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.wKAcceptService
					.setTempByOrder(code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
