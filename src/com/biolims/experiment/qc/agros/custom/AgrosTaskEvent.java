package com.biolims.experiment.qc.agros.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.qc.agros.service.AgrosTaskService;

public class AgrosTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		AgrosTaskService mbService = (AgrosTaskService) ctx.getBean("agrosTaskService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}