package com.biolims.experiment.qc.agros.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.sample.model.SampleInfo;

/**
 * @Title: Model
 * @Description: 琼脂糖电泳AGROS结果
 * @author lims-platform
 * @date 2016-07-21 13:02:07
 * @version V1.0
 * 
 */
@Entity
@Table(name = "AGROS_TASK_RESULT")
@SuppressWarnings("serial")
public class AgrosTaskResult extends EntityDao<AgrosTaskResult> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 描述 */
	private String name;
	/** 样本编号 */
	private String code;
	/** 原始样本编号 */
	private String sampleCode;
	/** 实验室样本号 */
	private String labCode;
	/** 样本类型 */
	private String sampleType;
	// 样本数量
	private Double sampleNum;
	/** 检测项目编号 */
	private String productId;
	/** 检测项目名称 */
	private String productName;
	/** index */
	private String indexa;
	/** 浓度 */
	private Double concentration;
	/** 体积 */
	private Double volume;
	/** 总量 */
	private Double sumTotal;
	/** 处理结果 */
	private String result;
	/** 失败原因 */
	private String reason;
	/** 是否提交 */
	private String submit;
	/** 下一步流向id */
	private String nextFlowId;
	/** 下一步流向 */
	private String nextFlow;
	/** 取样时间 */
	private Date inspectDate;
	/** 接收日期 */
	private Date acceptDate;
	/** 关联任务单 */
	private String orderId;
	/** 应出报告时间 */
	private Date reportDate;
	/** 状态 */
	private String state;
	/** 处理意见 */
	private String method;
	/** 备注 */
	private String note;
	/** 相关主表 */
	private AgrosTask agrosTask;
	/** 临时表Id */
	private String tempId;
	/** 样本主数据 */
	private SampleInfo sampleInfo;

	/**
	 * 方法: 取得SampleInfo
	 * 
	 * @return: 样本主数据
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}

	/**
	 * 方法: 设置SampleInfo
	 * 
	 * @param: 样本主数据
	 */
	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 100)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本编号
	 */
	@Column(name = "CODE", length = 50)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 原始样本编号
	 */
	@Column(name = "SAMPLE_CODE", length = 100)
	public String getSampleCode() {
		return this.sampleCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 原始样本编号
	 */
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 实验室样本号
	 */
	@Column(name = "LAB_CODE", length = 50)
	public String getLabCode() {
		return this.labCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 实验室样本号
	 */
	public void setLabCode(String labCode) {
		this.labCode = labCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本类型
	 */
	@Column(name = "SAMPLE_TYPE", length = 50)
	public String getSampleType() {
		return this.sampleType;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本类型
	 */
	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	public Double getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(Double sampleNum) {
		this.sampleNum = sampleNum;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 检测项目编号
	 */
	@Column(name = "PRODUCT_ID", length = 50)
	public String getProductId() {
		return this.productId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 检测项目编号
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 检测项目名称
	 */
	@Column(name = "PRODUCT_NAME", length = 500)
	public String getProductName() {
		return this.productName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 检测项目名称
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String index
	 */
	@Column(name = "INDEXA", length = 50)
	public String getIndexa() {
		return this.indexa;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String index
	 */
	public void setIndexa(String indexa) {
		this.indexa = indexa;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 浓度
	 */
	@Column(name = "CONCENTRATION", length = 50)
	public Double getConcentration() {
		return this.concentration;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 浓度
	 */
	public void setConcentration(Double concentration) {
		this.concentration = concentration;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 体积
	 */
	@Column(name = "VOLUME", length = 50)
	public Double getVolume() {
		return this.volume;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 体积
	 */
	public void setVolume(Double volume) {
		this.volume = volume;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 总量
	 */
	@Column(name = "SUM_TOTAL", length = 50)
	public Double getSumTotal() {
		return this.sumTotal;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 总量
	 */
	public void setSumTotal(Double sumTotal) {
		this.sumTotal = sumTotal;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 处理结果
	 */
	@Column(name = "RESULT", length = 50)
	public String getResult() {
		return this.result;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 处理结果
	 */
	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 失败原因
	 */
	@Column(name = "REASON", length = 50)
	public String getReason() {
		return this.reason;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 失败原因
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 是否提交
	 */
	@Column(name = "SUBMIT", length = 50)
	public String getSubmit() {
		return this.submit;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 是否提交
	 */
	public void setSubmit(String submit) {
		this.submit = submit;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 下一步流向id
	 */
	@Column(name = "NEXT_FLOW_ID", length = 50)
	public String getNextFlowId() {
		return this.nextFlowId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 下一步流向id
	 */
	public void setNextFlowId(String nextFlowId) {
		this.nextFlowId = nextFlowId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 下一步流向
	 */
	@Column(name = "NEXT_FLOW", length = 50)
	public String getNextFlow() {
		return this.nextFlow;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 下一步流向
	 */
	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 取样时间
	 */
	@Column(name = "INSPECT_DATE", length = 50)
	public Date getInspectDate() {
		return this.inspectDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 取样时间
	 */
	public void setInspectDate(Date inspectDate) {
		this.inspectDate = inspectDate;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 接收日期
	 */
	@Column(name = "ACCEPT_DATE", length = 50)
	public Date getAcceptDate() {
		return this.acceptDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 接收日期
	 */
	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 关联任务单
	 */
	@Column(name = "ORDER_ID", length = 50)
	public String getOrderId() {
		return this.orderId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 关联任务单
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 应出报告时间
	 */
	@Column(name = "REPORT_DATE", length = 50)
	public Date getReportDate() {
		return this.reportDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 应出报告时间
	 */
	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 处理意见
	 */
	@Column(name = "METHOD", length = 50)
	public String getMethod() {
		return this.method;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 处理意见
	 */
	public void setMethod(String method) {
		this.method = method;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得AgrosTask
	 * 
	 * @return: AgrosTask 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "AGROS_TASK")
	public AgrosTask getAgrosTask() {
		return this.agrosTask;
	}

	/**
	 * 方法: 设置AgrosTask
	 * 
	 * @param: AgrosTask 相关主表
	 */
	public void setAgrosTask(AgrosTask agrosTask) {
		this.agrosTask = agrosTask;
	}
}