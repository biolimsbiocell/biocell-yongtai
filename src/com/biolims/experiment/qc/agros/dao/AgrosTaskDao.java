package com.biolims.experiment.qc.agros.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.qc.agros.model.AgrosTask;
import com.biolims.experiment.qc.agros.model.AgrosTaskCos;
import com.biolims.experiment.qc.agros.model.AgrosTaskItem;
import com.biolims.experiment.qc.agros.model.AgrosTaskReagent;
import com.biolims.experiment.qc.agros.model.AgrosTaskResult;
import com.biolims.experiment.qc.agros.model.AgrosTaskTemp;
import com.biolims.experiment.qc.agros.model.AgrosTaskTemplate;
import com.biolims.experiment.uf.model.UfTaskItem;
import com.biolims.experiment.uf.model.UfTaskResult;
import com.biolims.experiment.uf.model.UfTaskTemp;
import com.biolims.experiment.wk.model.WkTaskItem;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;

@Repository
@SuppressWarnings("unchecked")
public class AgrosTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectAgrosTaskList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from AgrosTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<AgrosTask> list = new ArrayList<AgrosTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
		public Map<String, Object> selectAgrosTaskItemList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from AgrosTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and agrosTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<AgrosTaskItem> list = new ArrayList<AgrosTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectAgrosTaskTemplateList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from AgrosTaskTemplate where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and agrosTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<AgrosTaskTemplate> list = new ArrayList<AgrosTaskTemplate>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectAgrosTaskReagentList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from AgrosTaskReagent where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and agrosTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<AgrosTaskReagent> list = new ArrayList<AgrosTaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectAgrosTaskCosList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from AgrosTaskCos where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and agrosTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<AgrosTaskCos> list = new ArrayList<AgrosTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectAgrosTaskResultList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from AgrosTaskResult where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and agrosTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<AgrosTaskResult> list = new ArrayList<AgrosTaskResult>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectAgrosTaskTempList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from AgrosTaskTemp where 1=1 and state='1'";
		String key = "";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<AgrosTaskTemp> list = new ArrayList<AgrosTaskTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by code desc";

			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		
		


		public List<AgrosTaskItem> setItemList(String id) {
			String hql = "from AgrosTaskItem where 1=1 and agrosTask.id='" + id + "'";
			List<AgrosTaskItem> list = this.getSession().createQuery(hql).list();
			return list;
		}

		public List<AgrosTaskTemp> setTempList(String code) {
			String hql = "from AgrosTaskTemp where 1=1 and code='" + code + "'";
			List<AgrosTaskTemp> list = this.getSession().createQuery(hql).list();
			return list;
		}

		public List<AgrosTaskResult> setResultList(String code) {
			String hql = "from AgrosTaskResult where 1=1 and agrosTask.id='" + code + "'";
			List<AgrosTaskResult> list = this.getSession().createQuery(hql).list();
			return list;
		}

		// 科技服务
		public Map<String, Object> selectTechJkServiceTaskList(
				Map<String, String> mapForQuery, Integer startNum,
				Integer limitNum, String dir, String sort) {
			String key = " ";
			// String hql =
			// " from TechJkServiceTask t where 1=1 and t.orderType = '0'";//任务单类型为：建库
			String hql = " from TechJkServiceTask t where 1=1";
			if (mapForQuery != null)
				key = map2where(mapForQuery);
			Long total = (Long) this.getSession()
					.createQuery(" select count(*) " + hql + key).uniqueResult();
			List<TechJkServiceTask> list = new ArrayList<TechJkServiceTask>();
			if (total > 0) {
				if (dir != null && dir.length() > 0 && sort != null
						&& sort.length() > 0) {
					if (sort.indexOf("-") != -1) {
						sort = sort.substring(0, sort.indexOf("-"));
					}
					key = key + " order by " + sort + " " + dir;
				}
				if (startNum == null || limitNum == null) {
					list = this.getSession().createQuery(hql + key).list();
				} else {
					list = this.getSession().createQuery(hql + key)
							.setFirstResult(startNum).setMaxResults(limitNum)
							.list();
				}
			}
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("total", total);
			result.put("list", list);
			return result;

		}
		
		public Map<String, Object> selectTechServiceTaskItemList(String scId,
				Integer startNum, Integer limitNum, String dir, String sort)
				throws Exception {
			String hql = "from TechJkServiceTaskItem where 1=1 and state = '1'";
			String key = "";
			if (scId != null)
				key = key + " and techJkServiceTask.id='" + scId + "'";
			Long total = (Long) this.getSession()
					.createQuery("select count(*) " + hql + key).uniqueResult();
			List<TechJkServiceTaskItem> list = new ArrayList<TechJkServiceTaskItem>();
			if (total > 0) {
				if (dir != null && dir.length() > 0 && sort != null
						&& sort.length() > 0) {
					if (sort.indexOf("-") != -1) {
						sort = sort.substring(0, sort.indexOf("-"));
					}
					key = key + " order by " + sort + " " + dir;
				}
				if (startNum == null || limitNum == null) {
					list = this.getSession().createQuery(hql + key).list();
				} else {
					list = this.getSession().createQuery(hql + key)
							.setFirstResult(startNum).setMaxResults(limitNum)
							.list();
				}
			}
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("total", total);
			result.put("list", list);
			return result;
		}

		// 根据科技服务主表id获取明细表信息
		public Map<String, Object> selectTechTaskItemByTaskId(String taskId) {
			String hql = "from TechJkServiceTaskItem t where 1=1 and t.techJkServiceTask='"
					+ taskId + "' and t.state = '1'";
			Long total = (Long) this.getSession()
					.createQuery("select count(*)" + hql).uniqueResult();
			List<TechJkServiceTaskItem> list = this.getSession().createQuery(hql)
					.list();
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("total", total);
			result.put("list", list);
			return result;
		}

		public List<AgrosTaskItem> selectAgrosTaskItemList(String scId) throws Exception {
			String hql = "from AgrosTaskItem where 1=1 ";
			String key = "";
			if (scId != null)
				key = key + " and agros.id='" + scId + "'";
			Long total = (Long) this.getSession()
					.createQuery("select count(*) " + hql + key).uniqueResult();
			List<AgrosTaskItem> list = new ArrayList<AgrosTaskItem>();
			list = this.getSession().createQuery(hql + key).list();
			return list;
		}
		
		
		// 根据样本编号查询
		public List<AgrosTaskResult> setAgrosResultById(String code) {
			String hql = "from AgrosTaskResult t where (submit is null or submit='') and agrosTask.id='"
					+ code + "'";
			List<AgrosTaskResult> list = this.getSession().createQuery(hql).list();
			return list;
		}

		// 根据样本编号查询
		public List<AgrosTaskResult> setAgrosResultByIds(String[] codes) {

			String insql = "''";
			for (String code : codes) {

				insql += ",'" + code + "'";

			}

			String hql = "from AgrosTaskResult t where (submit is null or submit='') and id in ("
					+ insql + ")";
			List<AgrosTaskResult> list = this.getSession().createQuery(hql).list();
			return list;
		}
	}
