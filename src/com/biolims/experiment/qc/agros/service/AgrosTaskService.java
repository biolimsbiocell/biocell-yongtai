package com.biolims.experiment.qc.agros.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.dna.model.DnaTaskAbnormal;
import com.biolims.experiment.plasma.model.PlasmaAbnormal;
import com.biolims.experiment.pooling.model.PoolingTemp;
import com.biolims.experiment.qc.agros.dao.AgrosTaskDao;
import com.biolims.experiment.qc.agros.model.AgrosTask;
import com.biolims.experiment.qc.agros.model.AgrosTaskCos;
import com.biolims.experiment.qc.agros.model.AgrosTaskItem;
import com.biolims.experiment.qc.agros.model.AgrosTaskReagent;
import com.biolims.experiment.qc.agros.model.AgrosTaskResult;
import com.biolims.experiment.qc.agros.model.AgrosTaskTemp;
import com.biolims.experiment.qc.agros.model.AgrosTaskTemplate;
import com.biolims.experiment.qc.model.Qc2100TaskTemp;
import com.biolims.experiment.qc.model.QcQpcrTaskTemp;
import com.biolims.experiment.wk.model.WkTaskAbnormal;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.project.feedback.model.FeedbackWk;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.work.dao.WorkOrderDao;
import com.biolims.system.work.model.WorkOrder;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class AgrosTaskService {
	@Resource
	private AgrosTaskDao agrosTaskDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private DicSampleTypeDao dicSampleTypeDao;
	@Resource
	private WorkOrderDao workOrderDao;

	StringBuffer json = new StringBuffer();

	public Map<String, Object> findAgrosTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return agrosTaskDao.selectAgrosTaskList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(AgrosTask i) throws Exception {

		agrosTaskDao.saveOrUpdate(i);

	}

	public AgrosTask get(String id) {
		AgrosTask agrosTask = commonDAO.get(AgrosTask.class, id);
		return agrosTask;
	}

	public Map<String, Object> findAgrosTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = agrosTaskDao.selectAgrosTaskItemList(scId,
				startNum, limitNum, dir, sort);
		List<AgrosTaskItem> list = (List<AgrosTaskItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findAgrosTaskTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = agrosTaskDao.selectAgrosTaskTemplateList(
				scId, startNum, limitNum, dir, sort);
		List<AgrosTaskTemplate> list = (List<AgrosTaskTemplate>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findAgrosTaskReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = agrosTaskDao.selectAgrosTaskReagentList(
				scId, startNum, limitNum, dir, sort);
		List<AgrosTaskReagent> list = (List<AgrosTaskReagent>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findAgrosTaskCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = agrosTaskDao.selectAgrosTaskCosList(scId,
				startNum, limitNum, dir, sort);
		List<AgrosTaskCos> list = (List<AgrosTaskCos>) result.get("list");
		return result;
	}

	public Map<String, Object> findAgrosTaskResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = agrosTaskDao.selectAgrosTaskResultList(
				scId, startNum, limitNum, dir, sort);
		List<AgrosTaskResult> list = (List<AgrosTaskResult>) result.get("list");
		return result;
	}

	public Map<String, Object> findAgrosTaskTempList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = agrosTaskDao.selectAgrosTaskTempList(scId,
				startNum, limitNum, dir, sort);
		List<AgrosTaskTemp> list = (List<AgrosTaskTemp>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveAgrosTaskItem(AgrosTask sc, String itemDataJson)
			throws Exception {
		List<AgrosTaskItem> saveItems = new ArrayList<AgrosTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			AgrosTaskItem scp = new AgrosTaskItem();
			// 将map信息读入实体类
			scp = (AgrosTaskItem) agrosTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setAgrosTask(sc);

			saveItems.add(scp);
		}
		agrosTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delAgrosTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			AgrosTaskItem scp = agrosTaskDao.get(AgrosTaskItem.class, id);
			agrosTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveAgrosTaskTemplate(AgrosTask sc, String itemDataJson)
			throws Exception {
		List<AgrosTaskTemplate> saveItems = new ArrayList<AgrosTaskTemplate>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			AgrosTaskTemplate scp = new AgrosTaskTemplate();
			// 将map信息读入实体类
			scp = (AgrosTaskTemplate) agrosTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setAgrosTask(sc);

			saveItems.add(scp);
		}
		agrosTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delAgrosTaskTemplate(String[] ids) throws Exception {
		for (String id : ids) {
			AgrosTaskTemplate scp = agrosTaskDao.get(AgrosTaskTemplate.class,
					id);
			agrosTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveAgrosTaskReagent(AgrosTask sc, String itemDataJson)
			throws Exception {
		List<AgrosTaskReagent> saveItems = new ArrayList<AgrosTaskReagent>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			AgrosTaskReagent scp = new AgrosTaskReagent();
			// 将map信息读入实体类
			scp = (AgrosTaskReagent) agrosTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setAgrosTask(sc);
			// 用量 = 单个用量*样本数量
			if (scp != null && scp.getSampleNum() != null
					&& scp.getOneNum() != null) {
				scp.setNum(scp.getOneNum() * scp.getSampleNum());
			}

			saveItems.add(scp);
		}
		agrosTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delAgrosTaskReagent(String[] ids) throws Exception {
		for (String id : ids) {
			AgrosTaskReagent scp = agrosTaskDao.get(AgrosTaskReagent.class, id);
			agrosTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveAgrosTaskCos(AgrosTask sc, String itemDataJson)
			throws Exception {
		List<AgrosTaskCos> saveItems = new ArrayList<AgrosTaskCos>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			AgrosTaskCos scp = new AgrosTaskCos();
			// 将map信息读入实体类
			scp = (AgrosTaskCos) agrosTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setAgrosTask(sc);

			saveItems.add(scp);
		}
		agrosTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delAgrosTaskCos(String[] ids) throws Exception {
		for (String id : ids) {
			AgrosTaskCos scp = agrosTaskDao.get(AgrosTaskCos.class, id);
			agrosTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveAgrosTaskResult(AgrosTask sc, String itemDataJson)
			throws Exception {
		List<AgrosTaskResult> saveItems = new ArrayList<AgrosTaskResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			AgrosTaskResult scp = new AgrosTaskResult();
			// 将map信息读入实体类
			scp = (AgrosTaskResult) agrosTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setAgrosTask(sc);

			// saveItems.add(scp);
			agrosTaskDao.saveOrUpdate(scp);
		}

	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delAgrosTaskResult(String[] ids) throws Exception {
		for (String id : ids) {
			AgrosTaskResult scp = agrosTaskDao.get(AgrosTaskResult.class, id);
			agrosTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delAgrosTaskTemp(String[] ids) throws Exception {
		for (String id : ids) {
			AgrosTaskTemp scp = agrosTaskDao.get(AgrosTaskTemp.class, id);
			agrosTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delAgrosTaskCosOne(String ids) throws Exception {

		AgrosTaskCos scp = agrosTaskDao.get(AgrosTaskCos.class, ids);
		agrosTaskDao.delete(scp);

	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delAgrosTaskReagentOne(String ids) throws Exception {

		AgrosTaskReagent scp = agrosTaskDao.get(AgrosTaskReagent.class, ids);
		agrosTaskDao.delete(scp);

	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delAgrosTaskTemplateOne(String ids) throws Exception {

		AgrosTaskTemplate scp = agrosTaskDao.get(AgrosTaskTemplate.class, ids);
		agrosTaskDao.delete(scp);

	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(AgrosTask sc, Map jsonMap) throws Exception {
		if (sc != null) {
			agrosTaskDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("agrosTaskItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveAgrosTaskItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("agrosTaskTemplate");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveAgrosTaskTemplate(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("agrosTaskReagent");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveAgrosTaskReagent(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("agrosTaskCos");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveAgrosTaskCos(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("agrosTaskResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveAgrosTaskResult(sc, jsonStr);
			}
		}
	}

	// 审核完成
	public void changeState(String applicationTypeActionId, String id)
			throws Exception {
		AgrosTask sct = agrosTaskDao.get(AgrosTask.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		agrosTaskDao.update(sct);
		List<AgrosTaskItem> item = agrosTaskDao.setItemList(sct.getId());
		for (AgrosTaskItem c : item) {
			// List<UfTaskTemp> temp = ufTaskDao.setTempList(c.getCode());
			// if (temp.size() > 0) {
			// for (UfTaskTemp t : temp) {
			// t.setState("2");
			// }
			// }
			AgrosTaskTemp temp = commonDAO.get(AgrosTaskTemp.class,
					c.getTempId());
			if (temp != null) {
				temp.setState("2");
			}

		}
		submitSample(id, null);
	}

	public List<AgrosTaskItem> findDnaItemList(String scId) throws Exception {
		List<AgrosTaskItem> list = agrosTaskDao.setItemList(scId);
		return list;
	}

	// 科技服务-任务单主表
	public Map<String, Object> findTechJkServiceTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return this.agrosTaskDao.selectTechJkServiceTaskList(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	// 科技服务-任务单明细表
	public Map<String, Object> findTechServiceTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = this.agrosTaskDao
				.selectTechServiceTaskItemList(scId, startNum, limitNum, dir,
						sort);
		List<TechJkServiceTaskItem> list = (List<TechJkServiceTaskItem>) result
				.get("list");
		return result;
	}

	// 根据科技服务主表id 查询明细信息
	public List<Map<String, Object>> selectTechTaskItemByTaskId(String taskId) {
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		Map<String, Object> result = this.agrosTaskDao
				.selectTechTaskItemByTaskId(taskId);
		List<TechJkServiceTaskItem> list = (List<TechJkServiceTaskItem>) result
				.get("list");
		if (list != null && list.size() > 0) {
			for (TechJkServiceTaskItem ti : list) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("id", ti.getId());
				map.put("name", ti.getName());
				map.put("sampleCode", ti.getSampleCode());
				map.put("state", ti.getState());
				map.put("stateName", ti.getStateName());

				map.put("projectId", ti.getTechJkServiceTask().getProjectId());
				map.put("contractId", ti.getTechJkServiceTask().getContractId());
				map.put("orderType", ti.getTechJkServiceTask().getOrderType());
				map.put("experimentUser", ti.getExperimentUser());
				map.put("endDate", ti.getEndDate());
				map.put("species", ti.getSpecies());
				map.put("classify", ti.getClassify());

				map.put("techJkServiceTaskId", ti.getTechJkServiceTask()
						.getId());
				map.put("techJkServiceTaskName", ti.getTechJkServiceTask()
						.getName());
				// if(ti.getSampleCode()!=null){
				// List<TechJkServiceTask> l =
				// commonService.get(TechJkServiceTask.class, "sampleCode",
				// ti.getSampleCode());
				// if(l.size()>0)
				// ti.setTechJkServiceTask(l.get(0));
				// }
				mapList.add(map);
			}
		}
		return mapList;
	}

	public List<AgrosTaskItem> findAgrosTaskItemList(String scId)
			throws Exception {
		List<AgrosTaskItem> list = agrosTaskDao.selectAgrosTaskItemList(scId);
		return list;
	}

	// 提交样本
	public void submitSample(String id, String[] ids) throws Exception {
		// 获取主表信息
		AgrosTask sc = this.agrosTaskDao.get(AgrosTask.class, id);
		// 获取结果表样本信息
		List<AgrosTaskResult> list;
		if (ids == null)
			list = this.agrosTaskDao.setAgrosResultById(id);
		else
			list = this.agrosTaskDao.setAgrosResultByIds(ids);
		for (AgrosTaskResult scp : list) {
			if (scp != null) {
				if (scp.getResult() != null && (scp.getSubmit() == null || (scp.getSubmit() != null && scp.getSubmit().equals("")))
						&& scp.getNextFlowId() != null) {
					if (scp.getResult().equals("1")) {
						if (scp.getNextFlowId().equals("0009")) {// 样本入库
							SampleInItemTemp st = new SampleInItemTemp();
							st.setCode(scp.getCode());
							st.setSampleCode(scp.getSampleCode());
							st.setNum(scp.getSampleNum());
							st.setSampleType(scp.getSampleType());
							DicSampleType d = dicSampleTypeDao
									.selectDicSampleTypeByName(scp
											.getSampleType());
							st.setSampleTypeId(d.getId());
							st.setInfoFrom("AgrosTaskResult");
							st.setState("1");
							agrosTaskDao.saveOrUpdate(st);
						} else if (scp.getNextFlowId().equals("0010")) {// 重提取
							DnaTaskAbnormal eda = new DnaTaskAbnormal();
							scp.setState("4");// 状态为4 在重提取中显示
							sampleInputService.copy(eda, scp);

						} else if (scp.getNextFlowId().equals("0011")) {// 重建库
							WkTaskAbnormal wka = new WkTaskAbnormal();
							scp.setState("4");
							sampleInputService.copy(wka, scp);
						} else if (scp.getNextFlowId().equals("0024")) {// 重抽血
							PlasmaAbnormal wka = new PlasmaAbnormal();
							scp.setState("4");
							sampleInputService.copy(wka, scp);
						} else if (scp.getNextFlowId().equals("0012")) {// 暂停
						} else if (scp.getNextFlowId().equals("0013")) {// 终止
						} else if (scp.getNextFlowId().equals("0020")) {// 2100质控
							Qc2100TaskTemp wqt = new Qc2100TaskTemp();
							scp.setState("1");
							sampleInputService.copy(wqt, scp);
							wqt.setWkType("2");
							wqt.setSampleNum(scp.getSumTotal());
							agrosTaskDao.saveOrUpdate(wqt);
						} else if (scp.getNextFlowId().equals("0021")) {// QPCR质控
							QcQpcrTaskTemp wkqt = new QcQpcrTaskTemp();
							scp.setState("1");
							sampleInputService.copy(wkqt, scp);
							wkqt.setWkType("1");
							wkqt.setSampleNum(scp.getSumTotal());
							agrosTaskDao.saveOrUpdate(wkqt);
							// }else
							// if(scp.getNextFlowId().equals("0025")){//QPCR相对定量
							// QpcrxdTaskTemp qxtt = new QpcrxdTaskTemp();
							// scp.setState("1");
							// sampleInputService.copy(qxtt, scp);
							// qxtt.setWkType("1");
							// qxtt.setSampleNum(scp.getSumTotal());
							// agrosTaskDao.saveOrUpdate(qxtt);
							// }else
							// if(scp.getNextFlowId().equals("0026")){//QPCR绝对定量
							// QpcrjdTaskTemp qjtt = new QpcrjdTaskTemp();
							// scp.setState("1");
							// sampleInputService.copy(qjtt, scp);
							// qjtt.setWkType("1");
							// qjtt.setSampleNum(scp.getSumTotal());
							// agrosTaskDao.saveOrUpdate(qjtt);
						} else if (scp.getNextFlowId().equals("0015")) {// pooling
							PoolingTemp ptt = new PoolingTemp();
							scp.setState("1");
							sampleInputService.copy(ptt, scp);

//							ptt.setWkIndex(scp.getIndexa());
//
//							WorkOrder wo = workOrderDao
//									.findWorkOrderByProduct(scp.getProductId());
//
//							ptt.setSequencingPlatform(wo
//									.getSequencingPlatform());
//							ptt.setSequencingReadLong(wo
//									.getSequencingReadLong());
//							ptt.setWkIndex(scp.getIndexa());
							agrosTaskDao.saveOrUpdate(ptt);

						} else {
							// 得到下一步流向的相关表单
							List<NextFlow> list_nextFlow = nextFlowDao
									.seletNextFlowById(scp.getNextFlowId());
							for (NextFlow n : list_nextFlow) {
								Object o = Class.forName(
										n.getApplicationTypeTable()
												.getClassPath()).newInstance();
								scp.setState("1");
								sampleInputService.copy(o, scp);
							}
						}
					} else {// 不合格
						if (scp.getNextFlowId().equals("0014")) {// 反馈项目组
							// 下一步流向是：反馈至项目组
							FeedbackWk wf = new FeedbackWk();
							sampleInputService.copy(wf, scp);
							wf.setState("1");
							wf.setSubmit("");
							agrosTaskDao.saveOrUpdate(wf);
						} else {
							WkTaskAbnormal wka = new WkTaskAbnormal();
							scp.setState("2");
							sampleInputService.copy(wka, scp);
						}

					}
					DateFormat format = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");

					sampleStateService.saveSampleState(
							scp.getCode(),
							scp.getSampleCode(),
							scp.getProductId(),
							scp.getProductName(),
							"",
							format.format(sc.getCreateDate()),
							format.format(new Date()),
							"AgrosTask",
							"琼脂糖电泳AGROS",
							(User) ServletActionContext
									.getRequest()
									.getSession()
									.getAttribute(
											SystemConstants.USER_SESSION_KEY),
							sc.getId(), scp.getNextFlow(), scp.getResult(),
							null, null, null, null, null, null, null, null);
					scp.setSubmit("1");
					agrosTaskDao.saveOrUpdate(scp);
				}
			}
		}
	}
}
// sampleStateService.saveSampleState(
// scp.getCode(),
// scp.getSampleCode(),
// scp.getProductId(),
// scp.getProductName(),
// "",
// sc.getCreateDate(),
// format.format(new Date()),
// "AgrosTask",
// "琼脂糖电泳AGROS",
// (User) ServletActionContext
// .getRequest()
// .getSession()
// .getAttribute(
// SystemConstants.USER_SESSION_KEY),
// sc.getId(), scp.getNextFlow(), scp.getResult(),
// null, null, null, null, null, null, null, null);
// scp.setSubmit("1");
// agrosTaskDao.saveOrUpdate(scp);
// }
// }
// }
// }

//
// // 提交样本
// public void submitSample(String id, String[] ids) throws Exception {
// // 获取主表信息
// AgrosTask sc = this.agrosTaskDao.get(AgrosTask.class, id);
// // 获取结果表样本信息
// List<AgrosTaskResult> list;
// if (ids == null)
// list = this.agrosTaskDao.setAgrosResultById(id);
// else
// list = this.agrosTaskDao.setAgrosResultByIds(ids);
//
// for (AgrosTaskResult scp : list) {
// if (scp.getResult() != null && scp.getNextFlowId() != null
// && (scp.getSubmit() == null || (scp.getSubmit() != null && scp.getSubmit().equals("")))) {
// if (scp.getResult().equals("1")) {// 合格
// if (scp.getNextFlowId().equals("0009")) {// 样本入库
// SampleInItemTemp st = new SampleInItemTemp();
// st.setCode(scp.getCode());
// st.setSampleCode(scp.getSampleCode());
// st.setNum(scp.getSampleNum());
// st.setSampleType(scp.getSampleType());
// DicSampleType d = dicSampleTypeDao
// .selectDicSampleTypeByName(scp.getSampleType());
// st.setSampleTypeId(d.getId());
// st.setInfoFrom("AgrosTaskResult");
// st.setState("1");
// agrosTaskDao.saveOrUpdate(st);
// } else if (scp.getNextFlowId().equals("0010")) {// 重提取
// DnaAbnormal eda = new DnaAbnormal();
// scp.setState("4");// 状态为4 在重提取中显示
// sampleInputService.copy(eda, scp);
//
// } else if (scp.getNextFlowId().equals("0011")) {// 重建库
// WkAbnormalBack wka = new WkAbnormalBack();
// scp.setState("4");
// sampleInputService.copy(wka, scp);
// } else if (scp.getNextFlowId().equals("0024")) {// 重抽血
// PlasmaAbnormal wka = new PlasmaAbnormal();
// scp.setState("4");
// sampleInputService.copy(wka, scp);
// } else if (scp.getNextFlowId().equals("0012")) {// 暂停
// } else if (scp.getNextFlowId().equals("0013")) {// 终止
// } else if (scp.getNextFlowId().equals("0018")) {// 文库构建
// WkTaskTemp d = new WkTaskTemp();
// scp.setState("1");
// sampleInputService.copy(d, scp);
// if (scp.getSampleNum() != null) {
// d.setSampleNum(scp.getSampleNum());
// }
// agrosTaskDao.saveOrUpdate(d);
// } else {
// // 得到下一步流向的相关表单
// List<NextFlow> list_nextFlow = nextFlowDao
// .seletNextFlowById(scp.getNextFlowId());
// for (NextFlow n : list_nextFlow) {
// Object o = Class.forName(
// n.getApplicationTypeTable().getClassPath())
// .newInstance();
// scp.setState("1");
// sampleInputService.copy(o, scp);
// }
// }
// }
// DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
// sampleStateService
// .saveSampleState(
// scp.getCode(),
// scp.getSampleCode(),
// scp.getProductId(),
// scp.getProductName(),
// "",
// format.format(sc.getCreateDate()),
// format.format(new Date()),
// "AagrosTask",
// "琼脂糖电泳AGROS",
// (User) ServletActionContext
// .getRequest()
// .getSession()
// .getAttribute(
// SystemConstants.USER_SESSION_KEY),
// sc.getId(), scp.getNextFlow(), scp.getResult(),
// null, null, null, null, null, null, null, null);
// scp.setSubmit("1");
// agrosTaskDao.saveOrUpdate(scp);
// }
// }
// }
// }
//
//
//
