package com.biolims.experiment.qc.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.qc.model.Qc2100Task;
import com.biolims.experiment.qc.model.Qc2100TaskCos;
import com.biolims.experiment.qc.model.Qc2100TaskItem;
import com.biolims.experiment.qc.model.Qc2100TaskReagent;
import com.biolims.experiment.qc.model.Qc2100TaskTemp;
import com.biolims.experiment.qc.model.Qc2100TaskTemplate;
import com.biolims.experiment.qc.model.QcQpcrTaskItem;
import com.biolims.experiment.qc.model.SampleQc2100Info;
import com.biolims.experiment.qc.model.SampleQcQpcrInfo;
import com.biolims.experiment.uf.model.UfTaskResult;

@Repository
@SuppressWarnings("unchecked")
public class WKQualitySampleTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectWKQualityList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from Qc2100Task where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<Qc2100Task> list = new ArrayList<Qc2100Task>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectWKQualityItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from Qc2100TaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qc2100Task.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<Qc2100TaskItem> list = new ArrayList<Qc2100TaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by orderNumber asc ";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectExecuteStepItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from Qc2100TaskTemplate where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and wKQualitySampleTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<Qc2100TaskTemplate> list = new ArrayList<Qc2100TaskTemplate>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectDeviceItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from Qc2100TaskCos where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qc2100Task.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<Qc2100TaskCos> list = new ArrayList<Qc2100TaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据关联的item查询
	public Map<String, Object> selectCosItemListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		String hql = "from Qc2100TaskCos where 1=1 and itemId='" + itemId + "'";
		String key = "";
		if (scId != null)
			key = key + " and qc2100Task.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<Qc2100TaskCos> list = new ArrayList<Qc2100TaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectQualityResultItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SampleQc2100Info where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and wKQualitySampleTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleQc2100Info> list = new ArrayList<SampleQc2100Info>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by productId asc ";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectReagentItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from Qc2100TaskReagent where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qc2100Task.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<Qc2100TaskReagent> list = new ArrayList<Qc2100TaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据选中的步骤查询相关的试剂明细
	public Map<String, Object> setReagent(String id, String code) {
		String hql = "from Qc2100TaskReagent t where 1=1 and t.qc2100Task='"
				+ id + "' and t.itemId='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<Qc2100TaskReagent> list = this.getSession().createQuery(hql)
				.list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/*
	 * 根据主表ID查询试剂明细
	 */
	public List<Qc2100TaskReagent> setReagentList(String code) {
		String hql = "from Qc2100TaskReagent where 1=1 and qc2100Task='" + code
				+ "'";
		List<Qc2100TaskReagent> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	// 根据选中的步骤查询相关的设备明细
	public Map<String, Object> setCos(String id, String code) {
		String hql = "from Qc2100TaskCos t where 1=1 and t.qc2100Task='" + id
				+ "' and t.itemId='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<Qc2100TaskCos> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据关联itemId查询Reagent
	public Map<String, Object> selectReagentItemListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		String hql = "from Qc2100TaskReagent where 1=1 and itemId='" + itemId
				+ "'";
		String key = "";
		if (scId != null)
			key = key + " and qc2100Task.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<Qc2100TaskReagent> list = new ArrayList<Qc2100TaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 获取下一步为POOLING的数据，存放到POOLING的临时表里
	public List<SampleQc2100Info> searchWKQualitySampleInfo(String id) {
		String hql = "from SampleQc2100Info where 1=1 and result='0' and handleWay='0' wKQualitySampleTask.id='"
				+ id + "'";
		List<SampleQc2100Info> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 更改结果表中的异常数据
	public List<SampleQc2100Info> setQualityResultItemList(String id) {
		String hql = "from SampleQc2100Info where 1=1 and  sampleCode='" + id
				+ "'";
		List<SampleQc2100Info> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 查询临时表中状态为有效的数据
	public List<Qc2100TaskTemp> findWKQualityTempList() {
		String hql = "from Qc2100TaskTemp where 1=1 and state='1'";
		List<Qc2100TaskTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 查询结果表中的所有的数据
	public List<SampleQc2100Info> findQualityResultItemList(String id) {
		String hql = "from SampleQc2100Info where 1=1 and  wKQualitySampleTask.id='"
				+ id + "'";
		List<SampleQc2100Info> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据样本编号查询
	public List<SampleQc2100Info> setQc2100ResultById(String code) {
		String hql = "from SampleQc2100Info t where (submit is null or submit='') and wKQualitySampleTask.id='"
				+ code + "'";
		List<SampleQc2100Info> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据样本编号查询
	public List<SampleQc2100Info> setQc2100ResultByIds(String[] codes) {

		String insql = "''";
		for (String code : codes) {

			insql += ",'" + code + "'";

		}

		String hql = "from SampleQc2100Info t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<SampleQc2100Info> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<Qc2100TaskItem> setItemList(String id) {
		String hql = "from Qc2100TaskItem where 1=1 and qc2100Task.id='" + id
				+ "'";
		List<Qc2100TaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询结果表中的所有属于同一个样本的数据
	 * 
	 * @param scode
	 * @return
	 */
	public List<SampleQc2100Info> getSampleQc2100InfoList(String scode) {
		String hql = "from SampleQc2100Info where 1=1 and  code='" + scode
				+ "'";
		List<SampleQc2100Info> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询结果表中的所有属于同一个样本的文库号不同的数据
	 * 
	 * @param scode
	 * @return
	 */
	public List<SampleQc2100Info> getSampleQc2100InfoList1(String scode,
			String wcode) {
		String hql = "from SampleQc2100Info where 1=1 and  code='" + scode
				+ "' and wkId!='" + wcode + "'";
		List<SampleQc2100Info> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据文库编号查询2100质控结果表对象
	 * 
	 * @param id
	 * @return
	 */
	public List<SampleQc2100Info> getQualityResult(String id) {
		String hql = "from SampleQc2100Info where 1=1 and  wkId='" + id + "'";
		List<SampleQc2100Info> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据主表ID查询结果表中的异常数据
	 * 
	 * @param id
	 * @return
	 */
	public List<SampleQc2100Info> selectQualityResultList(String id) {
		String hql = "from SampleQc2100Info where 1=1 and  wKQualitySampleTask.id='"
				+ id + "'";
		List<SampleQc2100Info> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 查询结果表中的异常数据
	public List<SampleQc2100Info> selectQualityResultItemList(String id) {
		String hql = "from SampleQc2100Info where 1=1 and result='1' and  wKQualitySampleTask.id='"
				+ id + "'";
		List<SampleQc2100Info> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据ID相关样本的提取次数
	public Long selectCount(String code) {
		String hql = "from Qc2100TaskItem  where 1=1 and sampleId='" + code
				+ "'";
		Long list = (Long) this.getSession()
				.createQuery("select count(sampleId)" + hql).uniqueResult();
		return list;
	}

	// 根据ID相关样本的数量
	public Long selectCountMax(String code) {
		String hql = "from Qc2100TaskItem t where 1=1 and t.qc2100Task.id='"
				+ code + "'";
		Long list = (Long) this.getSession()
				.createQuery("select count(t.id) " + hql).uniqueResult();
		return list;
	}

	// 获取字典表index的最大值
	public String getIndexMax() {
		String hql = "from DicType  where 1=1 and type = 'indexa'";
		String list = (String) this.getSession()
				.createQuery("select max(name)" + hql).uniqueResult();
		// System.out.print(list);
		return list;
	}

	// 获取主表index的值
	public List<Qc2100Task> getIndexValue(String id) {
		String hql = "from Qc2100Task  where 1=1 and id='" + id + "'";
		List<Qc2100Task> list = this.getSession().createQuery(hql).list();
		// System.out.print(list);
		return list;
	}

	// 获取明细表index的最大值
	public Integer getIndexNum() {
		String hql = "from Qc2100TaskItem  where 1=1";
		Integer list = (Integer) this.getSession()
				.createQuery("select max(indexa)" + hql).uniqueResult();
		// System.out.print(list);
		return list;
	}

	// 根据原始样本号 主表id查询2100明细表
	// public List<Qc2100TaskItem> select2100Item(String sampleCode, String
	// scId){
	// String hql
	// ="from Qc2100TaskItem t where t.sampleCode = '"+sampleCode+"' and t.qc2100Task = '"+scId+"'";
	// List<Qc2100TaskItem> list = this.getSession().createQuery(hql).list();
	// return list;
	// }

	// 根据文库编号查询文库结果表
	// public List<SampleWkInfo> selectWkInfoByWkCode(String wkCode){
	// String hql = "from SampleWkInfo t where t.wkCode = '"+wkCode+"'";
	// List<SampleWkInfo> list = this.getSession().createQuery(hql).list();
	// return list;
	// }

	// 根据原始样本编号查询QPCR质控结果表 计算“比值”
	public List<SampleQcQpcrInfo> selectQpcrInfoByCode(String code) {
		String hql = "from SampleQcQpcrInfo t where t.code ='" + code + "'";
		List<SampleQcQpcrInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<Qc2100TaskItem> selectQc2100ItemList(String scId)
			throws Exception {
		String hql = "from Qc2100TaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qc2100Task.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<Qc2100TaskItem> list = new ArrayList<Qc2100TaskItem>();
		list = this.getSession().createQuery(hql + key).list();
		return list;
	}

}