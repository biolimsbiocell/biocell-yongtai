package com.biolims.experiment.qc.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.qc.model.QcPoolingTaskTemp;
import com.biolims.experiment.qc.model.QcReceive;
import com.biolims.experiment.qc.model.QcReceiveItem;
import com.biolims.experiment.qc.model.QcReceiveTemp;
import com.biolims.experiment.qc.model.QcQpcrTaskTemp;
import com.biolims.experiment.qc.model.Qc2100TaskTemp;
import com.biolims.experiment.qc.model.TechCheckServiceQcOrder;
import com.biolims.experiment.qc.model.TechCheckServiceQcTemp;

@Repository
@SuppressWarnings("unchecked")
public class WKAcceptDao extends BaseHibernateDao {
	public Map<String, Object> selectWKAcceptList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from QcReceive where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<QcReceive> list = new ArrayList<QcReceive>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id desc";

			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	/**
	 * 查询任务单中间表
	 * 
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 */
	public Map<String, Object> selectTechCheckServiceQcOrder(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from TechCheckServiceQcOrder where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<TechCheckServiceQcOrder> list = new ArrayList<TechCheckServiceQcOrder>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	/**
	 * 查询科技服务样本中间表
	 * 
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 */
	public Map<String, Object> selectTechCheckServiceQcTemp(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from TechCheckServiceQcTemp where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<TechCheckServiceQcTemp> list = new ArrayList<TechCheckServiceQcTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectWKAcceptItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QcReceiveItem where 1=1";
		String key = "";
		if (scId != null)
			key = key + " and wkAccept.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QcReceiveItem> list = new ArrayList<QcReceiveItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectPOOLINGAcceptItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from PoolingAcceptItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and wkAccept.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		// List<PoolingAcceptItem> list = new ArrayList<PoolingAcceptItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				// list = this.getSession().createQuery(hql + key).list();
			} else {
				// list = this.getSession().createQuery(hql +
				// key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		// result.put("list", list);
		return result;
	}

	// 查询子表样本，加载到2100质控左侧
	public Map<String, Object> selectWKQualityTempList(Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		// String hql =
		// "from Qc2100TaskTemp where 1=1 and state='1' and (wkType='0' or wkType='2')";
		String hql = "from Qc2100TaskTemp where 1=1 and state='1'";
		String key = "";

		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql).uniqueResult();
		List<Qc2100TaskTemp> list = new ArrayList<Qc2100TaskTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by code desc";

			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(999999).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 查询临时表，加载到QPCR质控左侧
	public Map<String, Object> selectWKQpcrTempList(Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = "from QcQpcrTaskTemp where 1=1 and state='1'";
		String key = "";

		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql).uniqueResult();
		List<QcQpcrTaskTemp> list = new ArrayList<QcQpcrTaskTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by  " + sort + " " + dir;
			} else {

				key = key + " order by code desc";

			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(999999).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 查询临时表，加载到QPCR质控左侧
	public Map<String, Object> selectQcPoolingTaskTempList(Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = "from QcPoolingTaskTemp where 1=1 and state='1'";
		String key = "";

		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql).uniqueResult();
		List<QcPoolingTaskTemp> list = new ArrayList<QcPoolingTaskTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by reportDate ASC" + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 查询样本子表，获取所有数据
	public List<QcReceiveItem> findWKAcceptItemList() throws Exception {
		String hql = "from QcReceiveItem where 1=1";
		List<QcReceiveItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 查询样本子表
	public List<QcReceiveItem> findWKAcceptItemList(String scId)
			throws Exception {
		String hql = "from QcReceiveItem where 1=1 and result='1' and wkAccept.id='"
				+ scId + "'";
		List<QcReceiveItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据文库编号查询已完成的文库质控样本接收的子表对象
	 * 
	 * @param scId
	 * @return
	 * @throws Exception
	 */
	public QcReceiveItem getWKAcceptItem(String scId) throws Exception {
		String hql = "from QcReceiveItem where 1=1 and wkCode='"
				+ scId
				+ "' and wkAccept in (select id from QcReceive where state='1')";
		List<QcReceiveItem> list = (List<QcReceiveItem>) this.getSession()
				.createQuery(hql).list();
		if (list.size() > 0)
			return list.get(0);
		return null;

	}

	/**
	 * 根据文库编号、样本编号、主表的状态查询已完成的文库质控样本接收的子表集合
	 * 
	 * @param scId
	 * @return
	 * @throws Exception
	 */
	public List<QcReceiveItem> getWKAcceptItemList(String wcode, String code)
			throws Exception {
		String hql = "from QcReceiveItem where 1=1 and wkCode !='"
				+ wcode
				+ "' and code='"
				+ code
				+ "' and wkAccept in (select id from QcReceive where state='1')";
		List<QcReceiveItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询板号最大和横坐标最大的记录
	 * 
	 * @return
	 */
	public Object selectMaxBoardNumAndMaxXNum() {
		String hql = " select max(counts) ,max(numX),max(numY) from QcReceiveItem where counts = (select max(counts) from QcReceiveItem)";
		return this.queryUniqueResult(hql);
	}

	// 查询临时表
	public List<QcReceiveTemp> findWKAcceptTemList() throws Exception {
		String hql = "from QcReceiveTemp where 1=1 and state='1'";
		List<QcReceiveTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 查询临时表样本
	public Map<String, Object> findWKAcceptTemList(Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = "from QcReceiveTemp where 1=1 and state='1'";
		String key = "";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QcReceiveTemp> list = new ArrayList<QcReceiveTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by reportDate ASC" + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据选中的任务单查询相关的待检测样本
	 * 
	 * @param code
	 * @return
	 */
	public Map<String, Object> setTempByOrder(String code) {
		String hql = "from TechCheckServiceQcTemp t where 1=1 and t.orderId='"
				+ code + "' and t.state='1'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<TechCheckServiceQcTemp> list = this.getSession().createQuery(hql)
				.list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
}