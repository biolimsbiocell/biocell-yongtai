package com.biolims.experiment.qc.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.qc.model.QcPoolingTaskItem;
import com.biolims.experiment.qc.model.QcPoolingTaskTemp;
import com.biolims.experiment.qc.model.QcQpcrTask;
import com.biolims.experiment.qc.model.QcQpcrTaskCos;
import com.biolims.experiment.qc.model.QcQpcrTaskItem;
import com.biolims.experiment.qc.model.QcQpcrTaskReagent;
import com.biolims.experiment.qc.model.QcQpcrTaskTemp;
import com.biolims.experiment.qc.model.QcQpcrTaskTemplate;
import com.biolims.experiment.qc.model.SampleQc2100Info;
import com.biolims.experiment.qc.model.SampleQcPoolingInfo;
import com.biolims.experiment.qc.model.SampleQcQpcrInfo;
import com.biolims.experiment.uf.model.UfTaskItem;

@Repository
@SuppressWarnings("unchecked")
public class WKQpcrSampleTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectWKQpcrList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from QcQpcrTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<QcQpcrTask> list = new ArrayList<QcQpcrTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id desc";

			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectWKQpcrItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QcQpcrTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qcQpcrTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QcQpcrTaskItem> list = new ArrayList<QcQpcrTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by orderNumber asc ";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectQcPoolingTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QcPoolingTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qcQpcrTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QcPoolingTaskItem> list = new ArrayList<QcPoolingTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectWKQpcrTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QcQpcrTaskTemplate where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qcQpcrTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QcQpcrTaskTemplate> list = new ArrayList<QcQpcrTaskTemplate>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectWKQpcrDeviceList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QcQpcrTaskCos where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qcQpcrTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QcQpcrTaskCos> list = new ArrayList<QcQpcrTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据关联的item查询
	public Map<String, Object> selectCosItemListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		String hql = "from QcQpcrTaskCos where 1=1 and itemId='" + itemId + "'";
		String key = "";
		if (scId != null)
			key = key + " and qcQpcrTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QcQpcrTaskCos> list = new ArrayList<QcQpcrTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectWKQpcrResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SampleQcQpcrInfo where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qcQpcrTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleQcQpcrInfo> list = new ArrayList<SampleQcQpcrInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by productId asc ";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectSampleQcPoolingInfoList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SampleQcPoolingInfo where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qcQpcrTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleQcPoolingInfo> list = new ArrayList<SampleQcPoolingInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectWKQpcrReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from QcQpcrTaskReagent where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qcQpcrTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QcQpcrTaskReagent> list = new ArrayList<QcQpcrTaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据选中的步骤查询相关的试剂明细
	public Map<String, Object> setReagent(String id, String code) {
		String hql = "from QcQpcrTaskReagent t where 1=1 and t.qcQpcrTask='"
				+ id + "' and t.itemId='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<QcQpcrTaskReagent> list = this.getSession().createQuery(hql)
				.list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/*
	 * 根据主表ID查询试剂明细
	 */
	public List<QcQpcrTaskReagent> setReagentList(String code) {
		String hql = "from QcQpcrTaskReagent t where 1=1 and t.qcQpcrTask='"
				+ code + "'";
		List<QcQpcrTaskReagent> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	// 根据选中的步骤查询相关的设备明细
	public Map<String, Object> setCos(String id, String code) {
		String hql = "from QcQpcrTaskCos t where 1=1 and t.qcQpcrTask='" + id
				+ "' and t.itemId='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<QcQpcrTaskCos> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据关联itemId查询Reagent
	public Map<String, Object> selectReagentItemListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		String hql = "from QcQpcrTaskReagent where 1=1 and itemId='" + itemId
				+ "'";
		String key = "";
		if (scId != null)
			key = key + " and qcQpcrTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QcQpcrTaskReagent> list = new ArrayList<QcQpcrTaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 获取下一步为POOLING的数据，存放到POOLING的临时表里
	public List<SampleQcQpcrInfo> searchWKQpcrSampleInfo(String id) {
		String hql = "from SampleQcQpcrInfo where 1=1 and result='0' and handleWay='0' qcQpcrTask.id='"
				+ id + "'";
		List<SampleQcQpcrInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 更改结果表中的异常数据
	public List<SampleQcQpcrInfo> setWKQpcrResultList(String id) {
		String hql = "from SampleQcQpcrInfo where 1=1 and  sampleCode='" + id
				+ "'";
		List<SampleQcQpcrInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 查询QPCR左上侧临时表中状态为有效的数据
	public List<QcQpcrTaskTemp> findWKQpcrTemptList() {
		String hql = "from QcQpcrTaskTemp where 1=1 and state='1'";
		List<QcQpcrTaskTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 查询QPCR左下侧临时表中状态为有效的数据
	public List<QcPoolingTaskTemp> findQcPoolingTaskTempList() {
		String hql = "from QcPoolingTaskTemp where 1=1 and state='1'";
		List<QcPoolingTaskTemp> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	public List<QcQpcrTaskItem> setItemList(String id) {
		String hql = "from QcQpcrTaskItem where 1=1 and qcQpcrTask.id='" + id
				+ "'";
		List<QcQpcrTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据主表ID查询结果表中的所有的数据
	 * 
	 * @param id
	 * @return
	 */
	public List<SampleQcQpcrInfo> findWKQpcrResultList(String id) {
		String hql = "from SampleQcQpcrInfo where 1=1 and  qcQpcrTask.id='"
				+ id + "'";
		List<SampleQcQpcrInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据样本编号查询
	public List<SampleQcQpcrInfo> setQcQpcrResultById(String code) {
		String hql = "from SampleQcQpcrInfo t where (submit is null or submit='') and qcQpcrTask.id='"
				+ code + "'";
		List<SampleQcQpcrInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据样本编号查询
	public List<SampleQcQpcrInfo> setQcQpcrResultByIds(String[] codes) {

		String insql = "''";
		for (String code : codes) {

			insql += ",'" + code + "'";

		}

		String hql = "from SampleQcQpcrInfo t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<SampleQcQpcrInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询结果表中的所有属于同一样本的数据
	 * 
	 * @param scode
	 * @return
	 */
	public List<SampleQcQpcrInfo> getWKQpcrResultList(String scode) {
		String hql = "from SampleQcQpcrInfo where 1=1 and  sampleCode='"
				+ scode + "'";
		List<SampleQcQpcrInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询结果表中的所有属于同一样本的文库编号不同的数据
	 * 
	 * @param scode
	 * @return
	 */
	public List<SampleQcQpcrInfo> getWKQpcrResultList1(String scode,
			String wcode) {
		String hql = "from SampleQcQpcrInfo where 1=1 and  sampleCode='"
				+ scode + "' and wkId!='" + wcode + "'";
		List<SampleQcQpcrInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据文库编号获取QPCR质控结果表对象
	 * 
	 * @param id
	 * @return
	 */
	public SampleQcQpcrInfo getWKQpcrResult(String id) {
		String hql = "from SampleQcQpcrInfo where 1=1 and  wkId='" + id + "'";
		List<SampleQcQpcrInfo> list = (List<SampleQcQpcrInfo>) this
				.getSession().createQuery(hql).list();
		if (list.size() > 0)
			return list.get(0);
		return null;
	}

	/**
	 * 根据文库编号、样本编号获取QPCR质控结果表对象
	 * 
	 * @param wk
	 * @param scode
	 * @return
	 */
	public SampleQcQpcrInfo getWKQpcrResult1(String wk, String scode) {
		String hql = "from SampleQcQpcrInfo where 1=1 and  wkId !='" + wk
				+ "' and sampleCode='" + scode + "'";
		List<SampleQcQpcrInfo> list = (List<SampleQcQpcrInfo>) this
				.getSession().createQuery(hql).list();
		if (list.size() > 0)
			return list.get(0);
		return null;
	}

	// 查询结果表中的异常数据
	public List<SampleQcQpcrInfo> selectWKQpcrResultList(String id) {
		String hql = "from SampleQcQpcrInfo where 1=1 and result='1' and  qcQpcrTask.id='"
				+ id + "'";
		List<SampleQcQpcrInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据ID相关样本的提取次数
	public Long selectCount(String code) {
		String hql = "from QcQpcrTaskItem  where 1=1 and sampleId='" + code
				+ "'";
		Long list = (Long) this.getSession()
				.createQuery("select count(sampleId)" + hql).uniqueResult();
		return list;
	}

	// 根据ID相关样本的数量
	public Long selectCountMax(String code) {
		String hql = "from QcQpcrTaskItem t where 1=1 and t.qcQpcrTask.id='"
				+ code + "'";
		Long list = (Long) this.getSession()
				.createQuery("select count(t.id) " + hql).uniqueResult();
		return list;
	}

	// 获取字典表index的最大值
	public String getIndexMax() {
		String hql = "from DicType  where 1=1 and type = 'indexa'";
		String list = (String) this.getSession()
				.createQuery("select max(name)" + hql).uniqueResult();
		// System.out.print(list);
		return list;
	}

	// 获取主表index的值
	public List<QcQpcrTask> getIndexValue(String id) {
		String hql = "from QcQpcrTask  where 1=1 and id='" + id + "'";
		List<QcQpcrTask> list = this.getSession().createQuery(hql).list();
		// System.out.print(list);
		return list;
	}

	// 获取明细表index的最大值
	public Integer getIndexNum() {
		String hql = "from QcQpcrTaskItem  where 1=1";
		Integer list = (Integer) this.getSession()
				.createQuery("select max(indexa)" + hql).uniqueResult();
		// System.out.print(list);
		return list;
	}

	// 根据原始样本编号、主表id查询QPCR明细表
	public List<QcQpcrTaskItem> selectQcQpcrItem(String sampleCode, String scId) {
		String hql = "from QcQpcrTaskItem t where t.sampleCode = '"
				+ sampleCode + "' and t.qcQpcrTask= '" + scId + "'";
		List<QcQpcrTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据文库编号查询文库结果表
	// public List<SampleWkInfo> selectWkInfoByWkCode(String wkCode){
	// String hql = "from SampleWkInfo t where t.wkCode = '"+wkCode+"'";
	// List<SampleWkInfo> list = this.getSession().createQuery(hql).list();
	// return list;
	// }
	public List<QcQpcrTaskItem> selectQcQpcrItemList(String scId)
			throws Exception {
		String hql = "from QcQpcrTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qcQpcrTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QcQpcrTaskItem> list = new ArrayList<QcQpcrTaskItem>();
		list = this.getSession().createQuery(hql + key).list();
		return list;
	}

	// 根据原始样本号 查询文库明细表
	// public List<WkTaskItem> selectWkItemBySampleCode(String sampleCode){
	// String hql = "from WkTaskItem t where t.sampleCode = '"+sampleCode+"'";
	// List<WkTaskItem> list = this.getSession().createQuery(hql).list();
	// return list;
	// }

	// 根据原始样本编号 查询QPCR结果表
	// public List<SampleQc2100Info> select2100BySampleCode(String sampleCode){
	// String hql =
	// "from SampleQc2100Info t where t.sampleCode = '"+sampleCode+"'";
	// List<SampleQc2100Info> list = this.getSession().createQuery(hql).list();
	// return list;
	// }

	// 根据原始样本编号查询2100质控结果表 计算“比值”
	public List<SampleQc2100Info> select2100InfoBySampleCode(String sampleCode) {
		String hql = "from SampleQc2100Info t where t.sampleCode = '"
				+ sampleCode + "'";
		List<SampleQc2100Info> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据试剂编号查询试剂
	// public List<ReagentItem> selectReagentByCode(String temp,String code){
	// String hql =
	// "from ReagentItem t where t.template = '"+temp+"' and t.code = '"+code+"'";
	// List<ReagentItem> list = this.getSession().createQuery(hql).list();
	// return list;
	// }

}