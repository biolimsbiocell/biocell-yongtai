﻿
package com.biolims.experiment.regionalManagement.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.crm.customer.customer.model.CrmCustomerIteam;
import com.biolims.experiment.regionalManagement.model.RegionalManagement;
import com.biolims.experiment.regionalManagement.model.RegionalManagementItem;
import com.biolims.experiment.regionalManagement.service.RegionalManagementService;
import com.biolims.file.service.FileInfoService;
import com.biolims.stamp.birtVersion.model.BirtVersionItem;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/regionalManagement/regionalManagement")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class RegionalManagementAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "240109080";
	@Autowired
	private RegionalManagementService regionalManagementService;
	private RegionalManagement regionalManagement = new RegionalManagement();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CommonService commonService;
	@Resource
	private FieldService fieldService;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	private String log="";

	@Action(value = "showRegionalManagementList")
	public String showRegionalManagementList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/regionalManagement/regionalManagement.jsp");
	}

	@Action(value = "showRegionalManagementEditList")
	public String showRegionalManagementEditList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/regionalManagement/regionalManagementEditList.jsp");
	}

	@Action(value = "showRegionalManagementTableJson")
	public void showRegionalManagementTableJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = regionalManagementService.findRegionalManagementTable(start, length, query,
					col, sort);
			Long count = (Long) result.get("total");
			List<RegionalManagement> list = (List<RegionalManagement>) result.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("stateName", "");
			map.put("state", "");
			map.put("regionalState", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			// Map<String, Object> mapField =
			// fieldService.findFieldByModuleValue("RegionalManagement");
			// String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "regionalManagementSelectTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogRegionalManagementList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/regionalManagement/regionalManagementSelectTable.jsp");
	}

	@Action(value = "editRegionalManagement")
	public String editRegionalManagement() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			regionalManagement = regionalManagementService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "regionalManagement");
		} else {
			regionalManagement.setId("NEW");
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			regionalManagement.setCreateUser(user);
			regionalManagement.setCreateDate(new Date());
			regionalManagement.setState(SystemConstants.DIC_STATE_NEW);
			regionalManagement.setStateName(SystemConstants.DIC_STATE_NEW_NAME);
			// regionalManagement.setScopeId((String)
			// ActionContext.getContext().getSession().get("scopeId"));
			// regionalManagement.setScopeName((String)
			// ActionContext.getContext().getSession().get("scopeName"));

			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/regionalManagement/regionalManagementEdit.jsp");
	}

	@Action(value = "copyRegionalManagement")
	public String copyRegionalManagement() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		regionalManagement = regionalManagementService.get(id);
		regionalManagement.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/regionalManagement/regionalManagementEdit.jsp");
	}

	@Action(value = "save")
	public void save() throws Exception {

		String msg = "";
		String zId = "";
		boolean bool = true;

		try {
			String dataValue = getParameterFromRequest("dataValue");
			String str = "[" + dataValue + "]";
			String changeLog = getParameterFromRequest("changeLog");
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str, List.class);

			for (Map<String, Object> map : list) {
				regionalManagement = (RegionalManagement) commonDAO.Map2Bean(map, regionalManagement);
			}
			String id = regionalManagement.getId();
			if ((id != null && id.equals("")) || "NEW".equals(id)) {
				log="123";
				String modelName = "RegionalManagement";
				String markCode = "QY";
				Date date = new Date();
				DateFormat format = new SimpleDateFormat("yy");
				String stime = format.format(date);
				String autoID = codingRuleService.getCodeByPrefix(modelName, markCode, stime, 000000, 6, null);
				regionalManagement.setId(autoID);
			}
			Map aMap = new HashMap();
			Map lMap = new HashMap();
			aMap.put("regionalManagementItem", getParameterFromRequest("itemJson"));
			regionalManagementService.save(regionalManagement, aMap, changeLog, lMap,log);

			zId = regionalManagement.getId();
		} catch (Exception e) {
			e.printStackTrace();
			msg = "保存失败！";
			bool = false;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("msg", msg);
		map.put("success", bool);
		map.put("id", zId);

		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		if (bpmTaskId != null && !bpmTaskId.equals("") && !bpmTaskId.equals("null")) {
			map.put("bpmTaskId", bpmTaskId);

		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "viewRegionalManagement")
	public String toViewRegionalManagement() throws Exception {
		String id = getParameterFromRequest("id");
		regionalManagement = regionalManagementService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/regionalManagement/regionalManagementEdit.jsp");
	}

	@Action(value = "saveRegionalManagementTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveRegionalManagementTable() throws Exception {
		String dataValue = getParameterFromRequest("dataJson");
		String changeLog = getParameterFromRequest("changeLog");

		String str = "[" + dataValue + "]";

		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str, List.class);

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			for (Map<String, Object> map1 : list) {

				RegionalManagement a = new RegionalManagement();

				Map aMap = new HashMap();
				Map lMap = new HashMap();
				a = (RegionalManagement) commonDAO.Map2Bean(map1, a);
				// a.setScopeId((String)
				// ActionContext.getContext().getSession().get("scopeId"));
				// a.setScopeName((String)
				// ActionContext.getContext().getSession().get("scopeName"));

				regionalManagementService.save(a, aMap, changeLog, lMap,log);
				map.put("id", a.getId());
			}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public RegionalManagementService getRegionalManagementService() {
		return regionalManagementService;
	}

	public void setRegionalManagementService(RegionalManagementService regionalManagementService) {
		this.regionalManagementService = regionalManagementService;
	}

	public RegionalManagement getRegionalManagement() {
		return regionalManagement;
	}

	public void setRegionalManagement(RegionalManagement regionalManagement) {
		this.regionalManagement = regionalManagement;
	}

	@Action(value = "showRegionalManagement", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showRegionalManagement() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/regionalManagement/showRegionalManagementTop.jsp");
	}

	@Action(value = "showRegionalManagementTopTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showRegionalManagementTopTable() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = regionalManagementService.showRegionalManagementTopTable(start, length, query, col,
				sort);
		List<RegionalManagement> list = (List<RegionalManagement>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("regionalState", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}
	//子表展示数据
	@Action(value = "showRegionalManagementItemTableJson")
	public void showRegionalManagementItemTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = regionalManagementService.findRegionalManagementItemTable(start, length, query, col, sort, id);
		List<RegionalManagementItem> list = (List<RegionalManagementItem>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("cordon", "");
		map.put("cordonf", "");
		map.put("deviationCorrectionLine", "");
		map.put("deviationCorrectionLinef", "");
		map.put("model", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
  }
	//子表删除
		@Action(value = "delbv", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
		public void delbv() throws Exception {
			Map<String, Object> map = new HashMap<String, Object>();
			try {
				String[] ids = getRequest().getParameterValues("ids[]");
				regionalManagementService.delbv(ids);
				map.put("success", true);
			} catch (Exception e) {
				e.printStackTrace();
				map.put("success", false);
			}
			HttpUtils.write(JsonUtils.toJsonString(map));
		}
		
		@Action(value = "showRegionalTableJson")
		public String showRegionalTableJson() throws Exception {
			return dispatcher("/WEB-INF/page/experiment/regionalManagement/showRegionalTableJson.jsp");
		}
		@Action(value = "showRegionalMTableJson")
		public void showRegionalMTableJson() throws Exception {
			String query = getParameterFromRequest("query");
			String colNum = getParameterFromRequest("order[0][column]");
			String col = getParameterFromRequest("columns[" + colNum + "][data]");
			String sort = getParameterFromRequest("order[0][dir]");
			Integer start = Integer.valueOf(getParameterFromRequest("start"));
			Integer length = Integer.valueOf(getParameterFromRequest("length"));
			String draw = getParameterFromRequest("draw");
			Map<String, Object> result = regionalManagementService.findrm(start, length, query, col, sort);
			List<RegionalManagement> list = (List<RegionalManagement>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		}
		@Action(value = "findByModel")
		public void findByModel() throws Exception {
			String qId = getParameterFromRequest("qId");
			 String modelN = getParameterFromRequest("modelN");
			 List<RegionalManagementItem> items =new ArrayList<RegionalManagementItem>();
			 items =regionalManagementService.findByModel(qId,modelN);
			 Map<String, String> map = new HashMap<String, String>();
			 for (RegionalManagementItem rItem : items) {
				map.put("cordon", rItem.getCordon());
				map.put("deviationCorrectionLine",rItem.getDeviationCorrectionLine());
				map.put("cordonf", rItem.getCordonf());
				map.put("deviationCorrectionLinef",rItem.getDeviationCorrectionLinef() );
			}
			HttpUtils.write(JsonUtils.toJsonString(map));
		}
}
