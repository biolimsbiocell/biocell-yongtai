package com.biolims.experiment.regionalManagement.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.regionalManagement.dao.RegionalManagementDao;
import com.biolims.experiment.regionalManagement.model.RegionalManagement;
import com.biolims.experiment.regionalManagement.model.RegionalManagementItem;
import com.biolims.experiment.roomManagement.model.RoomManagement;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.stamp.birtVersion.model.BirtVersionItem;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class RegionalManagementService {
	@Resource
	private RegionalManagementDao regionalManagementDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findRegionalManagementTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return regionalManagementDao.findRegionalManagementTable(start, length, query, col, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(RegionalManagement i) throws Exception {

		regionalManagementDao.saveOrUpdate(i);

	}

	public RegionalManagement get(String id) {
		RegionalManagement regionalManagement = commonDAO.get(RegionalManagement.class, id);
		return regionalManagement;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(RegionalManagement sc, Map jsonMap, String logInfo, Map logMap,String log) throws Exception {
		if (sc != null) {
			regionalManagementDao.saveOrUpdate(sc);
			if ("0".equals(sc.getRegionalState())) {
				changeRoomManagementState(sc.getId());
			}
			String jso = (String) jsonMap.get("regionalManagementItem");
			if (jso != null && !jso.equals("{}") && !jso.equals("")) {
				saveRegionalManagementItem(sc, jso);
			}
			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(sc.getId());
				li.setClassName("RegionalManagement");
				li.setModifyContent(logInfo);
				if("123".equals(log)) {
					li.setState("1");
					li.setStateName("数据新增");
				}else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				commonDAO.saveOrUpdate(li);
			}

			String jsonStr = "";
			String logStr = "";
		}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveRegionalManagementItem(RegionalManagement sc, String itemDataJson) throws Exception {
		List<RegionalManagementItem> regionalManagementItems = new ArrayList<RegionalManagementItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			RegionalManagementItem rmt = new RegionalManagementItem();
			// 将map信息读入实体类
			rmt = (RegionalManagementItem) regionalManagementDao.Map2Bean(map, rmt);
			if (rmt.getId() != null && rmt.getId().equals(""))
				rmt.setId(null);
			rmt.setRegionalManagement(sc);
			// scp.setReceiveDate(new Date());

			regionalManagementItems.add(rmt);
		}
		regionalManagementDao.saveOrUpdateAll(regionalManagementItems);
		
	}

	public Map<String, Object> showRegionalManagementTopTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return regionalManagementDao.showRegionalManagementTopTable(start, length, query, col, sort);
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeRoomManagementState(String id) {
		List<RoomManagement> list = new ArrayList<RoomManagement>();
		list = regionalManagementDao.getRoomManagementFromRegional(id);
		if (!list.isEmpty()) {
			for (RoomManagement rm : list) {
				rm.setRoomState("0");
				commonDAO.saveOrUpdate(rm);
			}
		}
	}
	
	// 子表删除
		@WriteOperLog
		@WriteExOperLog
		@Transactional(rollbackFor = Exception.class)
		public void delbv(String[] ids) {
			for (String id : ids) {
				RegionalManagementItem scp = regionalManagementDao.get(RegionalManagementItem.class, id);
				regionalManagementDao.delete(scp);
				
//				if (ids.length>0) {
//					LogInfo li = new LogInfo();
//					li.setLogDate(new Date());
//					User u = (User) ServletActionContext
//							.getRequest()
//							.getSession()
//							.getAttribute(
//									SystemConstants.USER_SESSION_KEY);
//					li.setUserId(u.getId());
////					li.setFileId(scp.getRegionalManagement()().getId());
//					li.setClassName("BirtVersion");
////					li.setModifyContent("区域管理:"+"版本号:"+scp.getId()+",名称:"+scp.getReportN()+"的数据已删除");
//					commonDAO.saveOrUpdate(li);
//				}
			}
		}
		
// 子表展示数据
public Map<String, Object> findRegionalManagementItemTable(Integer start, Integer length, String query, String col,
				String sort, String id) throws Exception {
			return regionalManagementDao.findRegionalManagementItemTable(start, length, query, col, sort, id);
		}

//查区域
	public Map<String, Object> findrm(Integer start,
			Integer length, String query, String col,
			String sort) throws Exception{
		return regionalManagementDao.findrm(start,length,query,col,sort);
	}
	public List<RegionalManagementItem> findByModel(String qId,String modelN) throws Exception{
		return regionalManagementDao.findByModel(qId,modelN);
	}
}
