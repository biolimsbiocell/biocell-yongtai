package com.biolims.experiment.regionalManagement.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;

@Entity
@Table(name = "REGIONAL_MANAGEMENT_ITEM")
@SuppressWarnings("serial")
public class RegionalManagementItem extends EntityDao<RegionalManagementItem> implements java.io.Serializable, Cloneable {

	/** 区域编号 子表*/
	private String id;
	/** 警戒线/0.5 */
	private String cordon;
	/** 纠偏线/0.5 */
	private String deviationCorrectionLine;
	
	/** 警戒线/5.0 */
	private String cordonf;
	/** 纠偏线/5.0 */
	private String deviationCorrectionLinef;
	
	/**模块*/
	private String model;
	
	/**主表*/
	private  RegionalManagement regionalManagement;
	
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCordon() {
		return cordon;
	}

	public void setCordon(String cordon) {
		this.cordon = cordon;
	}

	public String getDeviationCorrectionLine() {
		return deviationCorrectionLine;
	}

	public void setDeviationCorrectionLine(String deviationCorrectionLine) {
		this.deviationCorrectionLine = deviationCorrectionLine;
	}

	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	public RegionalManagement getRegionalManagement() {
		return regionalManagement;
	}

	public void setRegionalManagement(RegionalManagement regionalManagement) {
		this.regionalManagement = regionalManagement;
	}

	public String getCordonf() {
		return cordonf;
	}

	public void setCordonf(String cordonf) {
		this.cordonf = cordonf;
	}

	public String getDeviationCorrectionLinef() {
		return deviationCorrectionLinef;
	}

	public void setDeviationCorrectionLinef(String deviationCorrectionLinef) {
		this.deviationCorrectionLinef = deviationCorrectionLinef;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}
	
	

	
}
