package com.biolims.experiment.regionalManagement.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;

/**
 * @author created by yunhe.Qin
 * @date 2019年5月16日---上午9:20:12
 * @Description 区域管理
 */
@Entity
@Table(name = "REGIONAL_MANAGEMENT")
@SuppressWarnings("serial")
public class RegionalManagement extends EntityDao<RegionalManagement> implements java.io.Serializable, Cloneable {

	/** 区域编号 */
	private String id;
	/** 区域名称 */
	private String name;
	/** 区域描述 */
	private String note;
	/** 创建人 */
	private User createUser;
	/** 创建时间 */
	private Date createDate;
	/** 区域状态 */
	private String regionalState;
	/** 状态 */
	private String state;
	/** 状态名称 */
	private String stateName;

	public String getRegionalState() {
		return regionalState;
	}

	public void setRegionalState(String regionalState) {
		this.regionalState = regionalState;
	}

	@Id
	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getNote() {
		return note;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	public User getCreateUser() {
		return createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public String getState() {
		return state;
	}

	public String getStateName() {
		return stateName;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

}
