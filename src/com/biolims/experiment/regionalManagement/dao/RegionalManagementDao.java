package com.biolims.experiment.regionalManagement.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.crm.customer.customer.model.CrmCustomerIteam;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.regionalManagement.model.RegionalManagement;
import com.biolims.experiment.regionalManagement.model.RegionalManagementItem;
import com.biolims.experiment.roomManagement.model.RoomManagement;
import com.biolims.stamp.birtVersion.model.BirtVersionItem;

@Repository
@SuppressWarnings("unchecked")
public class RegionalManagementDao extends BaseHibernateDao {

	public Map<String, Object> findRegionalManagementTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from  RegionalManagement where 1=1";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		// String scopeId = (String)
		// ActionContext.getContext().getSession().get("scopeId");
		// if (!"all".equals(scopeId)) {
		// key += " and scopeId='" + scopeId + "'";
		// }
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from RegionalManagement where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<RegionalManagement> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> showRegionalManagementTopTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from  RegionalManagement where 1=1 and regionalState='1'";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from RegionalManagement where 1=1 and regionalState='1'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<RegionalManagement> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<RoomManagement> getRoomManagementFromRegional(String id) {
		String hql = "from RoomManagement where 1=1 and regionalManagement='" + id + "'";
		List<RoomManagement> list = new ArrayList<RoomManagement>();
		list = getSession().createQuery(hql).list();
		return list;
	}
	//子表展示数据
		public Map<String, Object> findRegionalManagementItemTable(Integer start, Integer length, String query, String col,
				String sort, String id) throws Exception {
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from RegionalManagementItem where 1=1 and regionalManagement.id='" + id + "'";
			String key = "";

			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if (0l != sumCount) {
				Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
				String hql = "from RegionalManagementItem where 1=1 and regionalManagement.id='" + id + "' ";
				if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
					col = col.replace("-", ".");
					key += " order by " + col + " " + sort;
				}
				List<RegionalManagementItem> list = new ArrayList<RegionalManagementItem>();
				list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
			}
			return map;
		}
		//查主表
		public Map<String, Object> findrm(Integer start, Integer length, String query, String col,
				String sort) throws Exception {
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from  RegionalManagement where 1=1 and regionalState='1'";
			String key = "";
			if (query != null && !"".equals(query)) {
				key = map2Where(query);
			}
			Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			if (0l != sumCount) {
				Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
				String hql = "from RegionalManagement where 1=1 and regionalState='1'";
				if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
					col = col.replace("-", ".");
					key += " order by " + col + " " + sort;
				}
				List<RegionalManagement> list = getSession().createQuery(hql + key).setFirstResult(start)
						.setMaxResults(length).list();
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
			}
			return map;
		}
		
		public List<RegionalManagementItem> findByModel(String qId,String modelN) throws Exception {
			Map<String, Object> map = new HashMap<String, Object>();
			 List<RegionalManagementItem> items =new ArrayList<RegionalManagementItem>();
			String hql = "from RegionalManagementItem where 1=1 and regionalManagement.id='" + qId + "' and model='"+modelN+"'";
			items =getSession().createQuery(hql).list();
			return items;
		}
}