﻿
package com.biolims.experiment.journal.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.experiment.journal.model.JournalQaInfo;
import com.biolims.experiment.journal.service.JournalQaInfoService;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/journal/journalQaInfo")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class JournalQaInfoAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "rz1601";
	@Autowired
	private JournalQaInfoService journalQaInfoService;
	private JournalQaInfo journalQaInfo = new JournalQaInfo();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CommonService commonService;
	@Resource
	private FieldService fieldService;
	@Resource
	private CommonDAO commonDAO;

	@Action(value = "showJournalQaInfoList")
	public String showJournalQaInfoList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/journal/journalQaInfo.jsp");
	}

	@Action(value = "showJournalQaInfoEditList")
	public String showJournalQaInfoEditList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/journal/journalQaInfoEditList.jsp");
	}

	@Action(value = "showJournalQaInfoTableJson")
	public void showJournalQaInfoTableJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = journalQaInfoService.findJournalQaInfoTable(start, length, query, col, sort);
			Long count = (Long) result.get("total");
			List<JournalQaInfo> list = (List<JournalQaInfo>) result.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			// Map<String, Object> mapField =
			// fieldService.findFieldByModuleValue("JournalQaInfo");
			// String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "journalQaInfoSelectTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogJournalQaInfoList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/journal/journalQaInfoSelectTable.jsp");
	}

	@Action(value = "editJournalQaInfo")
	public String editJournalQaInfo() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			journalQaInfo = journalQaInfoService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "journalQaInfo");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			journalQaInfo.setCreateUser(user);
			journalQaInfo.setCreateDate(new Date());
			// journalQaInfo.setState(SystemConstants.DIC_STATE_NEW);
			journalQaInfo.setStateName(SystemConstants.DIC_STATE_NEW_NAME);
			// journalQaInfo.setScopeId((String)
			// ActionContext.getContext().getSession().get("scopeId"));
			// journalQaInfo.setScopeName((String)
			// ActionContext.getContext().getSession().get("scopeName"));

			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/journal/journalQaInfoEdit.jsp");
	}

	@Action(value = "copyJournalQaInfo")
	public String copyJournalQaInfo() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		journalQaInfo = journalQaInfoService.get(id);
		journalQaInfo.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/journal/journalQaInfoEdit.jsp");
	}

	@Action(value = "save")
	public void save() throws Exception {

		String msg = "";
		String zId = "";
		boolean bool = true;

		try {
			String dataValue = getParameterFromRequest("dataValue");
			String str = "[" + dataValue + "]";
			String changeLog = getParameterFromRequest("changeLog");
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str, List.class);

			for (Map<String, Object> map : list) {
				journalQaInfo = (JournalQaInfo) commonDAO.Map2Bean(map, journalQaInfo);
			}
			String id = journalQaInfo.getId();
			if (id != null && id.equals("")) {
				journalQaInfo.setId(null);
			}
			Map aMap = new HashMap();
			Map lMap = new HashMap();

			journalQaInfoService.save(journalQaInfo, aMap, changeLog, lMap);

			zId = journalQaInfo.getId();
		} catch (Exception e) {
			e.printStackTrace();
			msg = "保存失败！";
			bool = false;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("msg", msg);
		map.put("success", bool);
		map.put("id", zId);

		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		if (bpmTaskId != null && !bpmTaskId.equals("") && !bpmTaskId.equals("null")) {
			map.put("bpmTaskId", bpmTaskId);

		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "viewJournalQaInfo")
	public String toViewJournalQaInfo() throws Exception {
		String id = getParameterFromRequest("id");
		journalQaInfo = journalQaInfoService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/journal/journalQaInfoEdit.jsp");
	}

	@Action(value = "saveJournalQaInfoTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveJournalQaInfoTable() throws Exception {
		String dataValue = getParameterFromRequest("dataJson");
		String changeLog = getParameterFromRequest("changeLog");

		String str = "[" + dataValue + "]";

		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str, List.class);

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			for (Map<String, Object> map1 : list) {

				JournalQaInfo a = new JournalQaInfo();

				Map aMap = new HashMap();
				Map lMap = new HashMap();
				a = (JournalQaInfo) commonDAO.Map2Bean(map1, a);
				// a.setScopeId((String)
				// ActionContext.getContext().getSession().get("scopeId"));
				// a.setScopeName((String)
				// ActionContext.getContext().getSession().get("scopeName"));

				journalQaInfoService.save(a, aMap, changeLog, lMap);
				map.put("id", a.getId());
			}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public JournalQaInfoService getJournalQaInfoService() {
		return journalQaInfoService;
	}

	public void setJournalQaInfoService(JournalQaInfoService journalQaInfoService) {
		this.journalQaInfoService = journalQaInfoService;
	}

	public JournalQaInfo getJournalQaInfo() {
		return journalQaInfo;
	}

	public void setJournalQaInfo(JournalQaInfo journalQaInfo) {
		this.journalQaInfo = journalQaInfo;
	}

	@Action(value = "saveJournalQaInfo")
	public void saveResult() throws Exception {
		String dataJson = getParameterFromRequest("dataJson");
		String changeLog = getParameterFromRequest("changeLog");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			journalQaInfoService.saveJournalQaInfo(dataJson,changeLog);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "delJournalQaInfo")
	public void delSangerTaskResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			journalQaInfoService.delJournalQaInfo(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
}
