package com.biolims.experiment.journal.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: QA日志管理里
 * @author lims-platform
 * @date 2019-06-20 16:13:14
 * @version V1.0
 *
 */
@Entity
@Table(name = "JOURNAL_QA_INFO")
@SuppressWarnings("serial")
public class JournalQaInfo extends EntityDao<JournalQaInfo> implements java.io.Serializable {
	/** 编码 */
	private String id;
	/** 创建人 */
	private User createUser;
	/** 创建时间 */
	private Date createDate;
	/** 状态 */
	private String stateName;
	/** 描述 */
	private String name;

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 40)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	public User getCreateUser() {
		return createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public String getStateName() {
		return stateName;
	}

	public String getName() {
		return name;
	}

	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public void setName(String name) {
		this.name = name;
	}

}
