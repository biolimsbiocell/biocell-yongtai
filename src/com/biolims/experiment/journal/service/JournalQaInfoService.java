package com.biolims.experiment.journal.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.journal.dao.JournalQaInfoDao;
import com.biolims.experiment.journal.model.JournalQaInfo;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class JournalQaInfoService {
	@Resource
	private JournalQaInfoDao journalQaInfoDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findJournalQaInfoTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return journalQaInfoDao.findJournalQaInfoTable(start, length, query, col, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(JournalQaInfo i) throws Exception {

		journalQaInfoDao.saveOrUpdate(i);

	}

	public JournalQaInfo get(String id) {
		JournalQaInfo journalQaInfo = commonDAO.get(JournalQaInfo.class, id);
		return journalQaInfo;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(JournalQaInfo sc, Map jsonMap, String logInfo, Map logMap) throws Exception {
		if (sc != null) {
			journalQaInfoDao.saveOrUpdate(sc);

			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(sc.getId());
				li.setModifyContent(logInfo);
				commonDAO.saveOrUpdate(li);
			}

			String jsonStr = "";
			String logStr = "";
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveJournalQaInfo(String dataJson,String changLog) throws Exception {
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(dataJson, List.class);
		for (Map<String, Object> map : list) {
			JournalQaInfo scp = new JournalQaInfo();
			// 将map信息读入实体类
			scp = (JournalQaInfo) journalQaInfoDao.Map2Bean(map, scp);
			if (scp.getId() != null && "".equals(scp.getId())) {
				scp.setCreateUser(u);
				scp.setCreateDate(new Date());
				scp.setId(null);
			}
			commonDAO.saveOrUpdate(scp);
		}
		
		if (changLog != null && !"".equals(changLog)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u1 = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u1.getId());
//			li.setFileId(pt.getId());
			li.setModifyContent(changLog);
			commonDAO.saveOrUpdate(li);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delJournalQaInfo(String[] ids) {
		if (ids != null) {
			for (String id : ids) {
				JournalQaInfo jqi = new JournalQaInfo();
				jqi = commonDAO.get(JournalQaInfo.class, id);
				if (jqi != null) {
					commonDAO.delete(jqi);
				}
			}
		}
	}
}
