package com.biolims.experiment.qpcr.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.biolims.crm.project.model.Project;
import com.biolims.dao.EntityDao;
import com.biolims.sample.model.DicSampleType;

/**
 * @Title: Model
 * @Description: 异常反馈(样本异常反馈)
 * @author lims-platform
 * @date 
 * @version V1.0
 * 
 */
@Entity
@Table(name = "QPCR_TASK_ABNORMAL")
@SuppressWarnings("serial")
public class QpcrTaskAbnormal extends EntityDao<QpcrTaskAbnormal> implements
		java.io.Serializable {
	/** 血浆id */
	private String id;
	/** 样本编号 */
	private String code;
	/** 原始样本编号 */
	private String sampleCode;
	/** 样本类型 */
	private String sampleType;
	/** 样本情况 */
	private String sampleCond;
	/** 下一步流向ID */
	private String nextFlowId;
	/** 下一步流向 */
	private String nextFlow;
	/** 确认执行 */
	private String isExecute;
	/** 反馈时间 */
	private String backTime;
	/** 备注 */
	private String note;
	/** 处理意见 */
	private String method;
	/** 状态 */
	private String state;
	/** 状态名称 */
	private String stateName;
	/** 结果 */
	private String result;
	/** 体积 */
	private Double volume;
	/** 单位 */
	private String unit;
	/** 患者姓名 */
	private String patient;
	/** 应出报告日期 */
	private Date reportDate;
	/** 原因 */
	private String reason;
	/** 浓度 */
	private Double concentration;
	/** 检测项目 */
	private String productId;
	/** 检测项目 */
	private String productName;
	/** 任务单Id */
	private String orderId;
	/** 临床 1 科研 2 */
	private String classify;
	/** 样本数量 */
	private Double sampleNum;
	/** 范围Id */
	private String scopeId;
	/** 范围 */
	private String scopeName;
	/** 项目 */
	private Project project;
	/** 样本类型 */
	private DicSampleType dicSampleType;
	public Double getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(Double sampleNum) {
		this.sampleNum = sampleNum;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String id
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本类型
	 */
	@Column(name = "SAMPLE_TYPE", length = 50)
	public String getSampleType() {
		return this.sampleType;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本类型
	 */
	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本情况
	 */
	@Column(name = "SAMPLE_COND", length = 50)
	public String getSampleCond() {
		return this.sampleCond;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本情况
	 */
	public void setSampleCond(String sampleCond) {
		this.sampleCond = sampleCond;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 下一步流向
	 */
	@Column(name = "NEXT_FLOW", length = 50)
	public String getNextFlow() {
		return this.nextFlow;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 下一步流向
	 */
	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 确认执行
	 */
	@Column(name = "IS_EXECUTE", length = 50)
	public String getIsExecute() {
		return this.isExecute;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 确认执行
	 */
	public void setIsExecute(String isExecute) {
		this.isExecute = isExecute;
	}

	public String getBackTime() {
		return backTime;
	}

	public void setBackTime(String backTime) {
		this.backTime = backTime;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得AbnoalSample
	 * 
	 * @return: AbnoalSample 关联主表
	 */

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getPatient() {
		return patient;
	}

	public void setPatient(String patient) {
		this.patient = patient;
	}

	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Double getConcentration() {
		return concentration;
	}

	public void setConcentration(Double concentration) {
		this.concentration = concentration;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

	public String getNextFlowId() {
		return nextFlowId;
	}

	public void setNextFlowId(String nextFlowId) {
		this.nextFlowId = nextFlowId;
	}
	/**
	 * @return the scopeId
	 */
	public String getScopeId() {
		return scopeId;
	}

	/**
	 * @param scopeId the scopeId to set
	 */
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	/**
	 * @return the scopeName
	 */
	public String getScopeName() {
		return scopeName;
	}

	/**
	 * @param scopeName the scopeName to set
	 */
	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

	/**
	 * @return the project
	 */
	public Project getProject() {
		return project;
	}

	/**
	 * @param project the project to set
	 */
	public void setProject(Project project) {
		this.project = project;
	}

	/**
	 * @return the dicSampleType
	 */
	public DicSampleType getDicSampleType() {
		return dicSampleType;
	}

	/**
	 * @param dicSampleType the dicSampleType to set
	 */
	public void setDicSampleType(DicSampleType dicSampleType) {
		this.dicSampleType = dicSampleType;
	}
	
}