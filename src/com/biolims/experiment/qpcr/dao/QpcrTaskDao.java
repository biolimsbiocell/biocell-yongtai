package com.biolims.experiment.qpcr.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import com.biolims.common.constants.SystemConstants;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.qpcr.model.QpcrTask;
import com.biolims.experiment.qpcr.model.QpcrTaskCos;
import com.biolims.experiment.qpcr.model.QpcrTaskItem;
import com.biolims.experiment.qpcr.model.QpcrTaskReagent;
import com.biolims.experiment.qpcr.model.QpcrTaskTemp;
import com.biolims.experiment.qpcr.model.QpcrTaskTemplate;
import com.biolims.experiment.qpcr.model.QpcrTaskInfo;
import com.opensymphony.xwork2.ActionContext;


@Repository
@SuppressWarnings("unchecked")
public class QpcrTaskDao extends BaseHibernateDao {

	public Map<String, Object> findQpcrTaskTable(Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from QpcrTask where 1=1";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from QpcrTask where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<QpcrTask> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public Map<String, Object> selectQpcrTaskTempTable(String[] codes, Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
			
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from QpcrTaskTemp where 1=1 and state='1'";
			String key = "";
			if(query!=null&&!"".equals(query)){
				key=map2Where(query);
			}
			if(codes!=null&&!codes.equals("")){
				key+=" and code in (:alist)";
			}
			String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
			if(!"all".equals(scopeId)){
				key+=" and scopeId='"+scopeId+"'";
			}
			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				List<QpcrTaskTemp> list=null;
				Long filterCount = (Long) getSession().createQuery(countHql).uniqueResult();
				String hql = "from QpcrTaskTemp  where 1=1 and state='1'";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					col=col.replace("-", ".");
					key+=" order by "+col+" "+sort;
				}
				if(codes!=null&&!codes.equals("")){
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).setParameterList("alist", codes).list();
				}else{
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
				}
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
				}
			return map;
		
			
	}

	public Map<String, Object> findQpcrTaskItemTable(String id,Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from QpcrTaskItem where 1=1 and state='1' and qpcrTask.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from QpcrTaskItem  where 1=1 and state='1' and qpcrTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<QpcrTaskItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public List<QpcrTaskItem> showWellPlate(String id) {
		String hql="from QpcrTaskItem  where 1=1 and state='2' and  qpcrTask.id='"+id+"'";
		List<QpcrTaskItem> list = getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> findQpcrTaskItemAfTable(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from QpcrTaskItem where 1=1 and state='2' and qpcrTask.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from QpcrTaskItem  where 1=1 and state='2' and qpcrTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<QpcrTaskItem> list =this.getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
		}

	public List<String> getCountGroup(String id) {

		String countHql = "select count(*) from QpcrTaskItem where 1=1 and state='2' and qpcrTask.id='"+id+"'";
		String key = "";
		List<String> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "select counts from QpcrTaskItem where 1=1 and state='2' and qpcrTask.id='"+id+"' group by counts";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
		
	}

	public List<QpcrTaskTemplate> showQpcrTaskStepsJson(String id,String code) {

		String countHql = "select count(*) from QpcrTaskTemplate where 1=1 and qpcrTask.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and orderNum ='"+code+"'";
		}else{
		}
		List<QpcrTaskTemplate> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from QpcrTaskTemplate where 1=1 and qpcrTask.id='"+id+"'";
			key+=" order by orderNum asc";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<QpcrTaskReagent> showQpcrTaskReagentJson(String id,String code) {
		String countHql = "select count(*) from QpcrTaskReagent where 1=1 and qpcrTask.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<QpcrTaskReagent> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from QpcrTaskReagent where 1=1 and qpcrTask.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<QpcrTaskCos> showQpcrTaskCosJson(String id,String code) {
		String countHql = "select count(*) from QpcrTaskCos where 1=1 and qpcrTask.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<QpcrTaskCos> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from QpcrTaskCos where 1=1 and qpcrTask.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<QpcrTaskTemplate> delTemplateItem(String id) {
		List<QpcrTaskTemplate> list=new ArrayList<QpcrTaskTemplate>();
		String hql = "from QpcrTaskTemplate where 1=1 and qpcrTask.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<QpcrTaskReagent> delReagentItem(String id) {
		List<QpcrTaskReagent> list=new ArrayList<QpcrTaskReagent>();
		String hql = "from QpcrTaskReagent where 1=1 and qpcrTask.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<QpcrTaskCos> delCosItem(String id) {
		List<QpcrTaskCos> list=new ArrayList<QpcrTaskCos>();
		String hql = "from QpcrTaskCos where 1=1 and qpcrTask.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> showQpcrTaskResultTableJson(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from QpcrTaskInfo where 1=1 and qpcrTask.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from QpcrTaskInfo  where 1=1 and qpcrTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<QpcrTaskInfo> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	}

	public List<Object> showWellList(String id) {
		
		String hql="select counts,count(*) from QpcrTaskItem where 1=1 and qpcrTask.id='"+id+"' group by counts";
		List<Object> list=getSession().createQuery(hql).list();
	
		return list;
	}

	public List<QpcrTaskItem> plateSample(String id, String counts) {
		String hql="from QpcrTaskItem where 1=1 and qpcrTask.id='"+id+"'";
		String key="";
		if(counts==null||counts.equals("")){
			key=" and counts is null";
		}else{
			key="and counts='"+counts+"'";
		}
		List<QpcrTaskItem> list=getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> plateSampleTable(String id, String counts,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from QpcrTaskItem where 1=1 and qpcrTask.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		if(counts==null||counts.equals("")){
			key+=" and counts is null";
		}else{
			key+="and counts='"+counts+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from QpcrTaskItem  where 1=1 and qpcrTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<QpcrTaskItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	
	}

	public List<QpcrTaskInfo> findQpcrTaskInfoByCode(String code) {
		String hql="from QpcrTaskInfo where 1=1 and code='"+code+"'";
		List<QpcrTaskInfo> list=new ArrayList<QpcrTaskInfo>();
		list=getSession().createQuery(hql).list();
		return list;
	}
	
	public List<QpcrTaskInfo> selectAllResultListById(String code) {
		String hql = "from QpcrTaskInfo  where (submit is null or submit='') and qpcrTask.id='"
				+ code + "'";
		List<QpcrTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	public List<QpcrTaskInfo> selectResultListById(String id) {
		String hql = "from QpcrTaskInfo  where qpcrTask.id='"
				+ id + "'";
		List<QpcrTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<QpcrTaskInfo> selectAllResultListByIds(String[] codes) {
		String insql = "''";
		for (String code : codes) {
			insql += ",'" + code + "'";
		}
		String hql = "from QpcrTaskInfo t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<QpcrTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

		public List<QpcrTaskItem> selectQpcrTaskItemList(String scId)
			throws Exception {
		String hql = "from QpcrTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and qpcrTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<QpcrTaskItem> list = new ArrayList<QpcrTaskItem>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}
	
		public Integer generateBlendCode(String id) {
			String hql="select max(blendCode) from QpcrTaskItem where 1=1 and qpcrTask.id='"+id+"'";
			Integer blendCode=(Integer) this.getSession().createQuery(hql).uniqueResult();
			return blendCode;
		}

		public QpcrTaskInfo getResultByCode(String code) {
			String hql=" from QpcrTaskInfo where 1=1 and code='"+code+"'";
			QpcrTaskInfo spi=(QpcrTaskInfo) this.getSession().createQuery(hql).uniqueResult();
			return spi;
		}
	
}