package com.biolims.experiment.karyotyping.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.karyoget.model.KaryoGetTaskResult;
import com.biolims.experiment.karyotyping.model.KaryotypingTask;
import com.biolims.experiment.karyotyping.model.KaryotypingTaskItem;
import com.biolims.experiment.karyotyping.model.KaryotypingTaskTemp;
import com.biolims.file.model.FileInfo;

@Repository
@SuppressWarnings("unchecked")
public class KaryotypingTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectKaryotypingTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from KaryotypingTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<KaryotypingTask> list = new ArrayList<KaryotypingTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectKaryotypingTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from KaryotypingTaskItem t where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and karyotypingTask.id='" + scId
					+ "' order by t.productId ";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<KaryotypingTaskItem> list = new ArrayList<KaryotypingTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectKaryotypingTaskTempList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from KaryotypingTaskTemp where 1=1 and state='1'";
		String key = "";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<KaryotypingTaskTemp> list = new ArrayList<KaryotypingTaskTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public List<KaryotypingTaskItem> getKaryotypingTaskItemList(String id) {
		String hql = "from KaryotypingTaskItem t where 1=1 and karyotypingTask.id='"
				+ id + "'";
		List<KaryotypingTaskItem> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	/**
	 * 根据主表Id获取子表去重后的原始样本号
	 * 
	 * @param id
	 * @return
	 */
	public List<String> getItemList(String id) {
		String hql = "from KaryotypingTaskItem t where t.karyotypingTask='"
				+ id + "'";
		List<String> list = this.getSession()
				.createQuery("select distinct t.sampleCode " + hql).list();
		return list;
	}

	/**
	 * 根据实体名和id查询图片
	 * 
	 * @param id
	 * @return
	 */
	public List<FileInfo> findFileName(String id, String model) {
		String hql = "from FileInfo t where t.modelContentId in (" + id
				+ ") and t.ownerModel='" + model + "'";
		List<FileInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据实体名和id查询图片
	 * 
	 * @param id
	 * @return
	 */
	public List<FileInfo> findPicture(String id, String model) {
		String hql = "from FileInfo t where t.modelContentId ='" + id
				+ "' and t.ownerModel='" + model + "'";
		List<FileInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	public List<KaryotypingTaskItem> selectItem(String id) {
		String hql = "from KaryotypingTaskItem where 1=1 and karyotypingTask.id='"
				+ id + "'";
		List<KaryotypingTaskItem> list = this.getSession().createQuery(hql)
				.list();

		return list;
	}
	/**
	 * 根据样本编号查询
	 * @param code
	 * @return
	 */
	public List<KaryotypingTaskItem> setResultById(String code) {
		String hql = "from KaryotypingTaskItem t where (isCommit is null or isCommit='') and karyotypingTask.id='"
				+ code + "'";
		List<KaryotypingTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据样本编号查询
	 * @param codes
	 * @return
	 */
	public List<KaryotypingTaskItem> setResultByIds(String[] codes) {

		String insql = "''";
		for (String code : codes) {

			insql += ",'" + code + "'";

		}

		String hql = "from KaryotypingTaskItem t where (isCommit is null or isCommit='') and id in ("
				+ insql + ")";
		List<KaryotypingTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}
}