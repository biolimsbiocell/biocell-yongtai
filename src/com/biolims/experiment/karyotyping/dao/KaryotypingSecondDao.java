package com.biolims.experiment.karyotyping.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.karyotyping.model.KaryotypingFirst;
import com.biolims.experiment.karyotyping.model.KaryotypingSecond;
import com.biolims.experiment.karyotyping.model.KaryotypingTask;
import com.biolims.experiment.karyotyping.model.KaryotypingTaskItem;
import com.biolims.experiment.karyotyping.model.KaryotypingTaskTemp;
import com.biolims.file.model.FileInfo;
import com.biolims.sample.model.SampleOrder;

@Repository
@SuppressWarnings("unchecked")
public class KaryotypingSecondDao extends BaseHibernateDao {
	public Map<String, Object> selectKaryotypingSecondList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from KaryotypingSecond where 1=1 ";
		if (mapForQuery.size()>0){
			key = map2where(mapForQuery);
		}else{
			key = " and state='1'";
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<KaryotypingSecond> list = new ArrayList<KaryotypingSecond>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	public List<KaryotypingSecond> getKaryotypingSecondList(String code){
		String hql="from KaryotypingSecond where 1=1 and code='"+code+"'";
		List<KaryotypingSecond> list=this.createQuery(hql).list();
		return list;
	}
	/**
	 * 根据样本编号查询
	 * @param codes
	 * @return
	 */
	public List<KaryotypingSecond> setResultByIds(String[] codes) {

		String insql = "''";
		for (String code : codes) {

			insql += ",'" + code + "'";

		}

		String hql = "from KaryotypingSecond t where (isCommit is null or isCommit='') and id in ("
				+ insql + ")";
		List<KaryotypingSecond> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据原始样本编号查询订单
	 * @param codes
	 * @return
	 */
		public List<SampleOrder> setSampleOrderById(String codes) {
			String hql = " from  SampleOrder where 1=1 and id = '" + codes + "'";
			return this.getSession()
					.createQuery( hql).list();
		}
}