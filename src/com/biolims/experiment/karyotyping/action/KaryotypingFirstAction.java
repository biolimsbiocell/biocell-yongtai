package com.biolims.experiment.karyotyping.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.karyotyping.model.KaryotypingFirst;
import com.biolims.experiment.karyotyping.service.KaryotypingFirstService;
import com.biolims.experiment.snpjc.first.model.SnpFirstInstance;
import com.biolims.experiment.snpjc.first.service.SnpFirstService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

@Namespace("/experiment/karyotyping/karyotypingFirst")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class KaryotypingFirstAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "241607";
	@Autowired
	private KaryotypingFirstService karyotypingFirstService;
	private KaryotypingFirst karyotypingFirst = new KaryotypingFirst();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;

	@Action(value = "showKaryotypingFirstList")
	public String showKaryotypingFirstList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/karyotyping/karyotypingFirst.jsp");
	}

	@Action(value = "showKaryotypingFirstListJson")
	public void showKaryotypingFirstListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = karyotypingFirstService.findKaryotypingFirstList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<KaryotypingFirst> list = (List<KaryotypingFirst>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("slideCode", "");
		map.put("name", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("sampleType", "");
		map.put("advice", "");
		map.put("isException", "");
		map.put("result", "");
		map.put("resultDetail", "");
		map.put("nextStepId", "");
		map.put("nextStep", "");
		map.put("note", "");
		map.put("isGood", "");
		map.put("isCommit", "");
		map.put("state", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("template-id", "");
		map.put("template-fileName", "");
		map.put("reportInfo-id", "");
		map.put("reportInfo-name", "");
		map.put("fileNum", "");
		map.put("taskType", "");
		map.put("taskResultId", "");
		map.put("taskId", "");
		map.put("upTime", "");
		map.put("flxCellNum", "");
		map.put("ktCellNum", "");
		map.put("formerDate", "yyyy-MM-dd");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKaryotypingFirst")
	public void delKaryotypingFirst() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			karyotypingFirstService.delKaryotypingFirst(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public KaryotypingFirstService getKaryotypingFirstService() {
		return karyotypingFirstService;
	}

	public void setKaryotypingFirstService(KaryotypingFirstService karyotypingFirstService) {
		this.karyotypingFirstService = karyotypingFirstService;
	}

	public KaryotypingFirst getKaryotypingFirst() {
		return karyotypingFirst;
	}

	public void setKaryotypingFirst(KaryotypingFirst karyotypingFirst) {
		this.karyotypingFirst = karyotypingFirst;
	}
	
	/**
	 * 保存一审结果
	 */
	@Action(value = "saveKaryotypingFirst",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveKaryotypingFirst() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getRequest().getParameter("itemDataJson");
			karyotypingFirstService.saveKaryotypingFirst(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 生成PDF报告文件
	 * 
	 * @throws Exception
	 */
	@Action(value = "createReportFile", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void createReportFile() throws Exception {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			karyotypingFirstService.createReportFilePDF(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}

		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	@Action(value = "submitSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void submitSample() throws Exception {
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.karyotypingFirstService.submitSample(ids);

			result.put("success", true);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
