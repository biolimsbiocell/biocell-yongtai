package com.biolims.experiment.karyotyping.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.comsearch.dao.ComSearchDao;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.core.model.user.UserGroup;
import com.biolims.experiment.karyotyping.dao.KaryotypingTaskDao;
import com.biolims.experiment.karyotyping.model.KaryotypingTask;
import com.biolims.experiment.karyotyping.model.KaryotypingTaskItem;
import com.biolims.experiment.karyotyping.model.KaryotypingTaskTemp;
import com.biolims.experiment.karyotyping.service.KaryotypingTaskService;
import com.biolims.experiment.producer.model.ProducerTask;
import com.biolims.experiment.snpjc.analysis.model.SnpAnalysis;
import com.biolims.file.model.FileInfo;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.template.model.Template;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

@Namespace("/experiment/karyotyping/karyotypingTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class KaryotypingTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "249005";
	@Autowired
	private KaryotypingTaskService karyotypingTaskService;
	private KaryotypingTask karyotypingTask = new KaryotypingTask();
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private ComSearchDao comSearchDao;

	@Resource
	private KaryotypingTaskDao karyotypingTaskDao;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;

	@Action(value = "showKaryotypingTaskList")
	public String showKaryotypingTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/karyotyping/karyotypingTask.jsp");
	}

	@Action(value = "showKaryotypingTaskListJson")
	public void showKaryotypingTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = karyotypingTaskService
				.findKaryotypingTaskList(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<KaryotypingTask> list = (List<KaryotypingTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("producerTask-id", "");
		map.put("producerTask-name", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "karyotypingTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogKaryotypingTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/karyotyping/karyotypingTaskDialog.jsp");
	}

	@Action(value = "showDialogKaryotypingTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogKaryotypingTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = karyotypingTaskService
				.findKaryotypingTaskList(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<KaryotypingTask> list = (List<KaryotypingTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("producerTask-id", "");
		map.put("producerTask-name", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editKaryotypingTask")
	public String editKaryotypingTask() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			karyotypingTask = karyotypingTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "karyotypingTask");
			String tName = workflowProcessInstanceService
					.findToDoTaskNameByFormId(id);
			putObjToContext("taskName", tName);
		} else {
			karyotypingTask.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			karyotypingTask.setCreateUser(user);
			karyotypingTask.setCreateDate(new Date());
			karyotypingTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			karyotypingTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			/*List<Template> tlist=comSearchDao.getTemplateByType("doCka");
			Template t=tlist.get(0);
			karyotypingTask.setTemplate(t);*/
			UserGroup u=comSearchDao.get(UserGroup.class,"HXFX001");
			//if(karyotypingTask.getAcceptUser()!=null){
				karyotypingTask.setAcceptUser(u);
			//}
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(karyotypingTask.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/karyotyping/karyotypingTaskEdit.jsp");
	}

	@Action(value = "copyKaryotypingTask")
	public String copyKaryotypingTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		karyotypingTask = karyotypingTaskService.get(id);
		karyotypingTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/karyotyping/karyotypingTaskEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = karyotypingTask.getId();
		/*
		 * if(id!=null&&id.equals("")){ karyotypingTask.setId(null); }
		 */
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "KaryotypingTask";
			String markCode = "HXFX";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			karyotypingTask.setId(autoID);
//			karyotypingTask.setName("核型分析-"+autoID);
		}
		Map aMap = new HashMap();
		aMap.put("karyotypingTaskItem",
				getParameterFromRequest("karyotypingTaskItemJson"));

		// aMap.put("karyotypingTaskTemp",getParameterFromRequest("karyotypingTaskTempJson"));

		karyotypingTaskService.save(karyotypingTask, aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/experiment/karyotyping/karyotypingTask/editKaryotypingTask.action?id="
				+ karyotypingTask.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);
		// return
		// redirect("/experiment/karyotyping/karyotypingTask/editKaryotypingTask.action?id="
		// + karyotypingTask.getId());

	}

	@Action(value = "viewKaryotypingTask")
	public String toViewKaryotypingTask() throws Exception {
		String id = getParameterFromRequest("id");
		karyotypingTask = karyotypingTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/karyotyping/karyotypingTaskEdit.jsp");
	}

	@Action(value = "showKaryotypingTaskItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showKaryotypingTaskItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/karyotyping/karyotypingTaskItem.jsp");
	}

	@Action(value = "showKaryotypingTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showKaryotypingTaskItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = karyotypingTaskService
					.findKaryotypingTaskItemList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<KaryotypingTaskItem> list = (List<KaryotypingTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("slideCode", "");
			map.put("name", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("sampleType", "");
			map.put("advice", "");
			map.put("isException", "");
			map.put("result", "");
			map.put("resultDetail", "");
			map.put("nextStepId", "");
			map.put("nextStep", "");
			map.put("note", "");
			map.put("isGood", "");
			map.put("isCommit", "");
			map.put("state", "");
			map.put("karyotypingTask-name", "");
			map.put("karyotypingTask-id", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("template-id", "");
			map.put("template-fileName", "");
			map.put("reportInfo-id", "");
			map.put("reportInfo-name", "");
			map.put("fileNum", "");
			map.put("upTime", "");
			map.put("flxCellNum", "");
			map.put("ktCellNum", "");
			map.put("tempId", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKaryotypingTaskItem")
	public void delKaryotypingTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			karyotypingTaskService.delKaryotypingTaskItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKaryotypingTaskItemOne")
	public void delKaryotypingTaskItemOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			karyotypingTaskService.delKaryotypingTaskItemOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showKaryotypingTaskTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showKaryotypingTaskTempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/karyotyping/karyotypingTaskTemp.jsp");
	}

	@Action(value = "showKaryotypingTaskTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showKaryotypingTaskTempListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = karyotypingTaskService
				.findKaryotypingTaskTempList(map2Query, startNum, limitNum, dir,
						sort);
		Long total = (Long) result.get("total");
		List<KaryotypingTaskTemp> list = (List<KaryotypingTaskTemp>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("note", "");
		map.put("sampleCode", "");
		map.put("state", "");
		map.put("code", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("sampleType", "");
		map.put("slideCode", "");
		map.put("chargeNote", "");
		map.put("seqDate", "yyyy-MM-dd");
		new SendData().sendDateJson(map, list, total,
				ServletActionContext.getResponse());
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKaryotypingTaskTemp")
	public void delKaryotypingTaskTemp() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			karyotypingTaskService.delKaryotypingTaskTemp(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public KaryotypingTaskService getKaryotypingTaskService() {
		return karyotypingTaskService;
	}

	public void setKaryotypingTaskService(
			KaryotypingTaskService karyotypingTaskService) {
		this.karyotypingTaskService = karyotypingTaskService;
	}

	public KaryotypingTask getKaryotypingTask() {
		return karyotypingTask;
	}

	public void setKaryotypingTask(KaryotypingTask karyotypingTask) {
		this.karyotypingTask = karyotypingTask;
	}
	/**
	 * 保存核型分析结果
	 */
	@Action(value = "saveKaryotypingTaskResult",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveKaryotypingTaskResult() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getRequest().getParameter("itemDataJson");
			String id = getRequest().getParameter("id");
			KaryotypingTask sc=karyotypingTaskDao.get(KaryotypingTask.class, id);
			karyotypingTaskService.saveKaryotypingTaskItem(sc, itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	/**
	 * 生成PDF报告文件
	 * 
	 * @throws Exception
	 */
	@Action(value = "createReportFile", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void createReportFile() throws Exception {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			karyotypingTaskService.createReportFilePDF(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}

		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 加载图片
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "loadPic", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String loadPic() throws Exception {
		String ids = getRequest().getParameter("id");
		String model = getRequest().getParameter("model");
		List<FileInfo> flist = karyotypingTaskDao.findFileName(ids, model);
		putObjToContext("flist", flist);
		return dispatcher("/WEB-INF/page/experiment/snpjc/sample/showTechAnalysisPic.jsp");
	}
	/**
	 * 提交样本
	 * @throws Exception
	 */
	@Action(value = "submitSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void submitSample() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.karyotypingTaskService.submitSample(id, ids);

			result.put("success", true);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	 /**
	 * 保存核型分析明细
	 * @throws Exception
	 */
	@Action(value = "saveKaryotypingTaskItem",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveKaryotypingTaskItem() throws Exception {
		String id = getRequest().getParameter("id");
		String itemDataJson = getRequest().getParameter("itemDataJson");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			KaryotypingTask sc=commonDAO.get(KaryotypingTask.class, id);
			Map aMap = new HashMap();
			aMap.put("karyotypingTaskItem",
					itemDataJson);
			if(sc!=null){
				karyotypingTaskService.save(sc,aMap);
			}
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
