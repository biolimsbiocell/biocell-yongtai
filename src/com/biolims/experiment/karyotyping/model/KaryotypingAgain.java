package com.biolims.experiment.karyotyping.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.file.model.FileInfo;
import com.biolims.system.sysreport.model.ReportTemplateInfo;
/**   
 * @Title: Model
 * @Description: 核型分析数据复核
 * @author lims-platform
 * @date 2016-05-30 17:12:03
 * @version V1.0   
 *
 */
@Entity
@Table(name = "KARYOTYPING_AGAIN")
@SuppressWarnings("serial")
public class KaryotypingAgain extends EntityDao<KaryotypingAgain> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**玻片编号*/
	private String slideCode;
	/**描述*/
	private String name;
	/**样本编号*/
	private String code;
	/**原始样本编号*/
	private String sampleCode;
	/** 样本类型 */
	private String sampleType;
	/**检测项目ID*/
	private String productId;
	/**检测项目*/
	private String productName;
	/**临床建议*/
	private String advice;
	/**是否异常报告*/
	private String isException;
	/**结果*/
	private String result;
	/**结果解释*/
	private String resultDetail;
	/**下一步流向*/
	private String nextStep;
	/**下一步流向ID*/
	private String nextStepId;
	/**备注*/
	private String note;
	/**是否合格*/
	private String isGood;
	/**是否提交*/
	private String isCommit;
	/**状态*/
	private String state;
	/**模板*/
	private ReportTemplateInfo reportInfo;
	/**附件*/
	private FileInfo template;
	/**附件数量 */
	private String fileNum;
	/**相关 实验模块*/
	private String taskType;
	/**实验分析结果ID*/
	private String taskResultId;
	/**相关实验模块ID*/
	private String taskId;
	/**上传时间*/
	private String upTime;
	/** 分裂相细胞计数 */
	private String flxCellNum;
	/** 核型分析细胞计数 */
	private String ktCellNum;
	/**分析时间*/
	private Date formerDate;
	
	public Date getFormerDate() {
		return formerDate;
	}

	public void setFormerDate(Date formerDate) {
		this.formerDate = formerDate;
	}

	public String getFlxCellNum() {
		return flxCellNum;
	}

	public void setFlxCellNum(String flxCellNum) {
		this.flxCellNum = flxCellNum;
	}

	public String getKtCellNum() {
		return ktCellNum;
	}

	public void setKtCellNum(String ktCellNum) {
		this.ktCellNum = ktCellNum;
	}

	public String getUpTime() {
		return upTime;
	}

	public void setUpTime(String upTime) {
		this.upTime = upTime;
	}
	public String getFileNum() {
		return fileNum;
	}
	public void setFileNum(String fileNum) {
		this.fileNum = fileNum;
	}
	
	public String getTaskType() {
		return taskType;
	}
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
	public String getTaskResultId() {
		return taskResultId;
	}
	public void setTaskResultId(String taskResultId) {
		this.taskResultId = taskResultId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  玻片编号
	 */
	@Column(name ="SLIDE_CODE", length = 50)
	public String getSlideCode(){
		return this.slideCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  玻片编号
	 */
	public void setSlideCode(String slideCode){
		this.slideCode = slideCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本编号
	 */
	@Column(name ="CODE", length = 50)
	public String getCode(){
		return this.code;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本编号
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 *方法: 取得String
	 *@return: String  原始样本编号
	 */
	@Column(name ="SAMPLE_CODE", length = 50)
	public String getSampleCode(){
		return this.sampleCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  原始样本编号
	 */
	public void setSampleCode(String sampleCode){
		this.sampleCode = sampleCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  临床建议
	 */
	@Column(name ="ADVICE", length = 500)
	public String getAdvice(){
		return this.advice;
	}
	/**
	 *方法: 设置String
	 *@param: String  临床建议
	 */
	public void setAdvice(String advice){
		this.advice = advice;
	}
	/**
	 *方法: 取得String
	 *@return: String  是否异常报告
	 */
	@Column(name ="IS_EXCEPTION", length = 50)
	public String getIsException(){
		return this.isException;
	}
	/**
	 *方法: 设置String
	 *@param: String  是否异常报告
	 */
	public void setIsException(String isException){
		this.isException = isException;
	}
	/**
	 *方法: 取得String
	 *@return: String  结果
	 */
	@Column(name ="RESULT", length = 50)
	public String getResult(){
		return this.result;
	}
	/**
	 *方法: 设置String
	 *@param: String  结果
	 */
	public void setResult(String result){
		this.result = result;
	}
	/**
	 *方法: 取得String
	 *@return: String  结果解释
	 */
	@Column(name ="RESULT_DETAIL", length = 4000)
	public String getResultDetail(){
		return this.resultDetail;
	}
	/**
	 *方法: 设置String
	 *@param: String  结果解释
	 */
	public void setResultDetail(String resultDetail){
		this.resultDetail = resultDetail;
	}
	/**
	 *方法: 取得String
	 *@return: String  下一步流向
	 */
	@Column(name ="NEXT_STEP", length = 50)
	public String getNextStep(){
		return this.nextStep;
	}
	/**
	 *方法: 设置String
	 *@param: String  下一步流向
	 */
	public void setNextStep(String nextStep){
		this.nextStep = nextStep;
	}
	/**
	 *方法: 取得String
	 *@return: String  下一步流向ID
	 */
	@Column(name ="NEXT_STEP_ID", length = 50)
	public String getNextStepId(){
		return this.nextStepId;
	}
	/**
	 *方法: 设置String
	 *@param: String  下一步流向ID
	 */
	public void setNextStepId(String nextStepId){
		this.nextStepId = nextStepId;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得String
	 *@return: String  是否合格
	 */
	@Column(name ="IS_GOOD", length = 50)
	public String getIsGood(){
		return this.isGood;
	}
	/**
	 *方法: 设置String
	 *@param: String  是否合格
	 */
	public void setIsGood(String isGood){
		this.isGood = isGood;
	}
	/**
	 *方法: 取得String
	 *@return: String  是否提交
	 */
	@Column(name ="IS_COMMIT", length = 50)
	public String getIsCommit(){
		return this.isCommit;
	}
	/**
	 *方法: 设置String
	 *@param: String  是否提交
	 */
	public void setIsCommit(String isCommit){
		this.isCommit = isCommit;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setState(String state){
		this.state = state;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TEMPLATE")
	public FileInfo getTemplate() {
		return template;
	}
	public void setTemplate(FileInfo template) {
		this.template = template;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "REPORT_INFO")
	public ReportTemplateInfo getReportInfo() {
		return reportInfo;
	}
	public void setReportInfo(ReportTemplateInfo reportInfo) {
		this.reportInfo = reportInfo;
	}
	public String getSampleType() {
		return sampleType;
	}
	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}
	
}