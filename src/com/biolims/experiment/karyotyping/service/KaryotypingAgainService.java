package com.biolims.experiment.karyotyping.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.comsearch.service.ComSearchService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.experiment.karyoabnormal.model.KaryoAbnormal;
import com.biolims.experiment.karyotyping.dao.KaryotypingAgainDao;
import com.biolims.experiment.karyotyping.dao.KaryotypingTaskDao;
import com.biolims.experiment.karyotyping.model.KaryotypingAgain;
import com.biolims.experiment.karyotyping.model.KaryotypingFirst;
import com.biolims.experiment.karyotyping.model.KaryotypingSecond;
import com.biolims.experiment.karyotyping.model.KaryotypingTask;
import com.biolims.experiment.karyotyping.model.KaryotypingTaskItem;
import com.biolims.experiment.snpjc.analysis.model.SnpAnalysis;
import com.biolims.experiment.snpjc.analysis.model.SnpAnalysisItem;
import com.biolims.experiment.snpjc.second.model.SnpSecondInstance;
import com.biolims.file.model.FileInfo;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.report.model.EndReport;
import com.biolims.report.model.SampleReportTemp;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.sysreport.model.ReportTemplateInfo;
import com.biolims.util.ConfigFileUtil;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class KaryotypingAgainService {
	@Resource
	private KaryotypingTaskDao karyotypingTaskDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private KaryotypingAgainDao karyotypingAgainDao;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private ComSearchService comSearchService;
	@Resource
	private CommonService commonService;

	StringBuffer json = new StringBuffer();

	public Map<String, Object> findKaryotypingAgainList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return karyotypingAgainDao.selectKaryotypingAgainList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(KaryotypingAgain i) throws Exception {

		karyotypingAgainDao.saveOrUpdate(i);

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveKaryotypingAgain(String itemDataJson)
			throws Exception {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		List<KaryotypingAgain> saveItems = new ArrayList<KaryotypingAgain>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			KaryotypingAgain scp = new KaryotypingAgain();
			// 将map信息读入实体类
			scp = (KaryotypingAgain) karyotypingAgainDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			saveItems.add(scp);
		
			karyotypingAgainDao.saveOrUpdate(scp);
			
		}
		karyotypingAgainDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKaryotypingAgain(String[] ids) throws Exception {
		for (String id : ids) {
			KaryotypingAgain scp = karyotypingAgainDao.get(KaryotypingAgain.class, id);
			karyotypingAgainDao.delete(scp);
		}
	}
	/**
	 * 生成PDF报告文件
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void createReportFilePDF(String[] ids) throws Exception {
		for (String id : ids) {
			KaryotypingAgain sri = commonDAO.get(KaryotypingAgain.class, id);
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			SampleInfo sf = sampleInfoMainDao.findSampleInfo(sri
					.getSampleCode());
			/*SampleOrder so = new SampleOrder();
			if (sf != null) {
				if (sf.getSampleOrder() != null) {
					so = sf.getSampleOrder();
				}
			}*/
			SampleOrder so=commonDAO.get(SampleOrder.class, sri
					.getSampleCode());
			if (sri.getReportInfo() != null && sri.getTemplate() != null) {
				FileInfo fileInfo = sri.getTemplate();
				if (fileInfo != null) {
					// Map<String, File> imgMap = new HashMap<String, File>();
					// 临时地址
					String tempFile = ConfigFileUtil
							.getValueByKey("file.report.temp.path");
					// 正式地址
					String formFile = ConfigFileUtil
							.getValueByKey("file.report.form.path");
					FileInputStream in = new FileInputStream(
							fileInfo.getFilePath());
					PdfReader reader = new PdfReader(in);
					String root = ConfigFileUtil.getRootPath() + File.separator
							+ DateUtil.format(new Date(), "yyyyMMdd");
					if (!new File(root).exists()) {
						new File(root).mkdirs();
					}
					File deskFile = new File(tempFile, "PDF" + sri.getCode()
							+ ".pdf");
					PdfStamper ps = new PdfStamper(reader,
							new FileOutputStream(deskFile)); // 生成的输出流

					// 获取订单信息
					AcroFields s = ps.getAcroFields();
					s.setField("name", so.getName());
					s.setField("gender", so.getGender());
					if (so.getAge() != null) {
						s.setField("age", so.getAge().toString());
					} else {
						s.setField("age", "");
					}
					if (so.getBirthDate() != null) {
						s.setField("birthDay", format.format(so.getBirthDate()));
					} else {
						s.setField("birthDay", "");
					}
					if(sri.getSampleCode()!=null){
						s.setField("slideCode", sri.getSampleCode());
					}else{
						s.setField("slideCode", "");
					}
					s.setField("medicalNum", so.getMedicalNumber());
					s.setField("sampleType", sri.getSampleType());
					if(so.getCrmCustomer()!=null){
						s.setField("crmCustomer", so.getCrmCustomer().getName());
					}else{
						s.setField("crmCustomer", "");
					}
					if (so.getSamplingDate() != null) {
						s.setField("samplingDate",
								format.format(so.getSamplingDate()));
					} else {
						s.setField("samplingDate", "");
					}
					String jg=sri.getResult();
					jg=jg.replace("\r","");
					jg=jg.replace("\n","");
					int num0=jg.getBytes("UTF-8").length;	//字节长度
					String jgjs=sri.getResultDetail();	//结果解释
					jgjs=jgjs.replace("\r","");
					jgjs=jgjs.replace("\n","");
					int num1=jgjs.getBytes("UTF-8").length;	//字节长度
					int n0=0;
					int n1=0;
					String jg1="";
					String jg2="";
					String jg3="";
					
					String jgjs1="";
					String jgjs2="";
					String jgjs3="";
					//结果
					if(num0>0){
						if(num0<=88){
							jg1=jg;
						}else{
					        for (int i1=1; i1 <=(num0/88); i1++) {
				        		jg2=jg.replace(jg3, "");
				        		n0=jg2.getBytes("UTF-8").length;
				        		if(n0>=88){
				        			jg1+=comSearchService.bSubstring(jg2, 88)+"\r";
				        			jg3+=comSearchService.bSubstring(jg2, 88);
				        		}else if(n0<90 && n0>0){
				        			jg1+=comSearchService.bSubstring(jg2, 88)+"\r";
				        			jg3+=comSearchService.bSubstring(jg2, 88);
				        		}
					        }
						}
					}
					//jg1+=jg1+bSubstring(jg.replace(jg3, ""), n0);
					System.out.println(jg1+jg.replace(jg3, ""));
					if(!jg.replace(jg3, "").equals("")){
						if(sri.getResult()!=null){
							s.setField("result", jg1+jg.replace(jg3, ""));
						}else{
							s.setField("result", "");
						}
					}else{
						if(sri.getResult()!=null){
							s.setField("result", jg1);
						}else{
							s.setField("result", "");
						}
					}
					
					//结果解释
					if(num1>0){
						if(num1<=84){
							jgjs1=jgjs;
						}else{
					        for (int i1=0; i1 <=(num1/84); i1++) {
				        		jgjs2=jgjs.replace(jgjs3, "");
				        		n1=jgjs2.getBytes("UTF-8").length;
				        		if(n1>=84){
				        			jgjs1+=comSearchService.bSubstring(jgjs2, 84)+"\r";
				        			jgjs3+=comSearchService.bSubstring(jgjs2, 84);
				        		}else if(n1<84 && n1>0){
				        			jgjs1+=comSearchService.bSubstring(jgjs2, n1)+"\r";
				        			jgjs3+=comSearchService.bSubstring(jgjs2, n1);
				        		}
					        }
						}
					}
					System.out.println(jgjs1+jgjs.replace(jgjs3, ""));
					if(!jgjs.replace(jgjs3, "").equals("")){
						//jgjs1+=jgjs1+jgjs.replace(jgjs3, "");
						if(sri.getResultDetail()!=null){
							s.setField("resultDescription", jgjs1+jgjs.replace(jgjs3, ""));
						}else{
							s.setField("resultDescription", "");
						}
					}else{
						if(sri.getResultDetail()!=null){
							s.setField("resultDescription", jgjs1);
						}else{
							s.setField("resultDescription", "");
						}
					}
					
					if(sri.getAdvice()!=null){
						if(sri.getAdvice().equals("1")){
							s.setField("advice", "门诊随访");
						}else{
							s.setField("advice", "遗传门诊随访");
						}
					}else{
						s.setField("advice", "");
					}
					
					//临床症状描述
					if(so.getSubmitReasonName()!=null){
						s.setField("lczz", so.getSubmitReasonName());
					}else{
						s.setField("lczz", "");
					}
					s.setField("reportDate", format.format(new Date()));
					
					//s.setField("checkUser", sri.getSnpAnalysis()
					//		.getAcceptUser().getName());
					//s.setField("confirmUser", sri.getSnpAnalysis()
					//		.getCreateUser().getName());
					//获取相关图片信息
					List<FileInfo> picList=karyotypingTaskDao.
							findPicture(id, "karyotypingAgain");
					//获取报告模板信息
					ReportTemplateInfo rt=sri.getReportInfo();
					//获取模板类型	0:2图	1:3图
					String rtType=rt.getType();
					//根据模板类型和上传的图片数量向PDF中插入图片
					if(picList.size()>0){
						if(rtType.equals("0")){
							if(picList.size()==1){
								insertImage(ps,s,picList.get(0).getFilePath(),"Text1");
							}else if(picList.size()>1){
								insertImage(ps,s,picList.get(0).getFilePath(),"Text1");
								insertImage(ps,s,picList.get(1).getFilePath(),"Text2");
							}
						}else{
							if(picList.size()==1){
								insertImage(ps,s,picList.get(0).getFilePath(),"Text1");
							}else if(picList.size()==2){
								insertImage(ps,s,picList.get(0).getFilePath(),"Text1");
								insertImage(ps,s,picList.get(1).getFilePath(),"Text2");
							}else if(picList.size()>2){
								insertImage(ps,s,picList.get(0).getFilePath(),"Text1");
								insertImage(ps,s,picList.get(1).getFilePath(),"Text2");
								insertImage(ps,s,picList.get(2).getFilePath(),"Text3");
							}
						}
					}
					ps.setFormFlattening(true);// 这句不能少
					ps.close();
					reader.close();
					in.close();
					commonDAO.update(sri);
				}
			}
		}
	}
	/**
	 * 向PDF插入图片
	 * @param ps
	 * @param s
	 * @param path
	 * @param text
	 */
	public static void insertImage(PdfStamper ps,AcroFields s,String path,String text) {
		
		try {  
			Image image = Image.getInstance(path);  
			List<AcroFields.FieldPosition> list = s.getFieldPositions(text);  
			Rectangle signRect = list.get(0).position;  
			PdfContentByte under = ps.getOverContent(1);  
			float x = signRect.getLeft();  
			float y = signRect.getBottom();  
			//System.out.println(x);  
			//System.out.println(y);  
			image.setAbsolutePosition(x, y);  
			image.scaleToFit(200, 200);  
			  
			under.addImage(image);  
		}catch (Exception e){  
			// TODO Auto-generated catch block  
			e.printStackTrace();  
		}  
	}
	/**
	 * 提交样本
	 * @param id
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitSample(String[] ids) throws Exception {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		List<KaryotypingAgain> list = this.karyotypingAgainDao.setResultByIds(ids);
		for (KaryotypingAgain scp : list) {
			if (scp != null) {
		    	//提交合格的到1审
				SampleReportTemp st = new SampleReportTemp();
				
				st.setCode(scp.getCode());
				st.setSampleCode(scp.getSampleCode());
				st.setProductId(scp.getProductId());
				st.setProductName(scp.getProductName());
				st.setState("1");
				st.setNote(scp.getNote());
				String orderNum="";
				List<SampleInfo> ab = commonService.get(SampleInfo.class, "code",
						scp.getSampleCode());
				if (ab.size() > 0){
					orderNum=ab.get(0).getOrderNum();
				}else{
					orderNum="";
				}
				st.setOrderNum(orderNum);
				st.setSampleCode(scp.getSampleCode());
				commonDAO.saveOrUpdate(st);
//		    	KaryotypingFirst sf=new KaryotypingFirst();
//		    	sf.setCode(scp.getCode());
//		    	sf.setSampleCode(scp.getSampleCode());
//		    	sf.setSampleType(scp.getSampleType());
//		    	sf.setSlideCode(scp.getSlideCode());
//		    	sf.setResult(scp.getResult());
//		    	sf.setResultDetail(scp.getResultDetail());
//		    	sf.setAdvice(scp.getAdvice());
//		    	sf.setProductId(scp.getProductId());
//		    	sf.setProductName(scp.getProductName());
//		    	sf.setFlxCellNum(scp.getFlxCellNum());
//		    	sf.setKtCellNum(scp.getKtCellNum());
//		    	sf.setIsException(scp.getIsException());
//		    	if(scp.getTemplate()!=null){
//		    		sf.setTemplate(scp.getTemplate());
//		    	}
//		    	if(scp.getReportInfo()!=null){
//		    		sf.setReportInfo(scp.getReportInfo());
//		    	}
//		    	sf.setTaskId(scp.getTaskId());
//		    	sf.setTaskResultId(scp.getTaskResultId());
//		    	sf.setTaskType(scp.getTaskType());
//		    	sf.setState("1");
//		    	sf.setFormerDate(new Date());
//		    	
//		    	commonDAO.saveOrUpdate(sf);
		    	
		    	//完成后改变复核的状态
				scp.setState("2");
		    //}
//		    sampleStateService
//			.saveSampleState1(
//					scp.getCode(),
//					scp.getSampleCode(),
//					scp.getProductId(),
//					scp.getProductName(),
//					"",
//					format.format(new Date()),
//					format.format(new Date()),
//					"KaryotypingAgain",
//					"核型分析数据复核",
//					(User) ServletActionContext
//							.getRequest()
//							.getSession()
//							.getAttribute(
//									SystemConstants.USER_SESSION_KEY),
//					null, "核型数据审核","1", null, null, null, null, null,
//					null, null, null,scp.getResult(),scp.getResultDetail(),scp.getAdvice(),scp.getIsException());
//			}
			sampleStateService
			.saveSampleState1(
					scp.getCode(),
					scp.getSampleCode(),
					scp.getProductId(),
					scp.getProductName(),
					"",
					format.format(new Date()),
					format.format(new Date()),
					"KaryotypingAgain",
					"核型分析数据复核",
					(User) ServletActionContext
							.getRequest()
							.getSession()
							.getAttribute(
									SystemConstants.USER_SESSION_KEY),
					null, "核报告上传","1", null, null, null, null, null,
					null, null, null,scp.getResult(),scp.getResultDetail(),scp.getAdvice(),scp.getIsException());
			}
			scp.setIsCommit("1");
		}
	}
}
