package com.biolims.experiment.karyotyping.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.comsearch.dao.ComSearchDao;
import com.biolims.common.comsearch.service.ComSearchService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.karyotyping.dao.KaryotypingFirstDao;
import com.biolims.experiment.karyotyping.dao.KaryotypingSecondDao;
import com.biolims.experiment.karyotyping.dao.KaryotypingTaskDao;
import com.biolims.experiment.karyotyping.model.KaryotypingFirst;
import com.biolims.experiment.karyotyping.model.KaryotypingSecond;
import com.biolims.experiment.snpjc.analysis.model.SnpAnalysis;
import com.biolims.experiment.snpjc.analysis.model.SnpAnalysisItem;
import com.biolims.file.model.FileInfo;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.report.model.CreateReport;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.sysreport.model.ReportTemplateInfo;
import com.biolims.util.ConfigFileUtil;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class KaryotypingSecondService {
	@Resource
	private KaryotypingTaskDao karyotypingTaskDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private KaryotypingSecondDao karyotypingSecondDao;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private ComSearchDao comSearchDao;
	@Resource
	private ComSearchService comSearchService;
	@Resource
	private KaryotypingFirstDao karyotypingFirstDao;

	StringBuffer json = new StringBuffer();

	public Map<String, Object> findKaryotypingSecondList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return karyotypingSecondDao.selectKaryotypingSecondList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(KaryotypingSecond i) throws Exception {

		karyotypingSecondDao.saveOrUpdate(i);

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveKaryotypingSecond(String itemDataJson)
			throws Exception {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		List<KaryotypingSecond> saveItems = new ArrayList<KaryotypingSecond>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		List<String> sList=new ArrayList<String>();
		String ids="";
		for (Map<String, Object> map : list) {
			KaryotypingSecond scp = new KaryotypingSecond();
			// 将map信息读入实体类
			scp = (KaryotypingSecond) karyotypingSecondDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			saveItems.add(scp);
			karyotypingSecondDao.saveOrUpdate(scp);
			//获取相关图片信息
			List<FileInfo> picList=comSearchDao.findPicture(scp.getTaskResultId(), "karyotypingTaskItem");
			List<ReportTemplateInfo> rlist=new ArrayList<ReportTemplateInfo>();
			if(picList.size()>0){
				if(picList.size()<=5){
					//根据图片数量查询对应页数的模板
					rlist=comSearchDao.getReportTemplateInfo(scp.getProductId(),String.valueOf(picList.size()));
				}else{
					rlist=comSearchDao.getReportTemplateInfo(scp.getProductId(),String.valueOf(5));
				}
				
				scp.setReportInfo(rlist.get(0));
				scp.setTemplate(rlist.get(0).getAttach());
			}else{
				
				scp.setReportInfo(null);
				scp.setTemplate(null);
			}
			SampleInfo sf = sampleInfoMainDao.findSampleInfo(scp
					.getSampleCode());
			SampleOrder so = new SampleOrder();
			if (sf != null) {
				if (sf.getSampleOrder() != null) {
					so = sf.getSampleOrder();
				}
			}
//			if (scp != null) {
//				if ( scp.getIsCommit() != null
//					 //&& scp.getIsGood() != null
//					// && !scp.getIsGood().equals("")
//					 && !scp.getIsCommit().equals("")) {
//					 if (scp.getIsCommit().equals("1")) {
//					    if (scp.getIsGood().equals("1")) {
//					    	//提交合格的到报告生成
//					    	CreateReport cr = new CreateReport();
//							cr.setCode(scp.getCode());
//							cr.setSlideCode(scp.getSlideCode());
//							cr.setSampleCode(scp.getSampleCode());
//							cr.setSampleType(scp.getSampleType());
//							cr.setProductId(scp.getProductId());
//							cr.setProductName(scp.getProductName());
//							cr.setResult(scp.getResult());
//							cr.setResultDescription(scp.getResultDetail());
//							cr.setAdvice(scp.getAdvice());
//							cr.setYcbg(scp.getIsException());
//							cr.setTemplate(scp.getTemplate());
//							cr.setReportInfo(scp.getReportInfo());
//							cr.setTaskId(scp.getTaskId());
//							cr.setTaskType(scp.getTaskType());
//							cr.setTaskResultId(scp.getTaskResultId());
//							cr.setWaitDate(new Date());
//							cr.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_REPORT_NEW);
//							cr.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_REPORT_NEW_NAME);
//							cr.setOrderCode(so.getId());
//					    	commonDAO.saveOrUpdate(cr);
//					    	
//					    	//完成后改变二审的状态
//							scp.setState("2");
//					    }else{
//					    	//不合格返回数据审核
//					    	List<KaryotypingFirst> listf=karyotypingFirstDao.getKaryotypingFirstList(scp.getCode());
//					    	if(listf.size()>0){
//					    		KaryotypingFirst kf=listf.get(0);
//					    		kf.setIsCommit(null);
//					    		kf.setState("1");
//					    		kf.setNote(scp.getNote());
//					    		commonDAO.update(kf);
//					    	}
//					    }
//							sampleStateService
//							.saveSampleState1(
//									scp.getCode(),
//									scp.getSampleCode(),
//									scp.getProductId(),
//									scp.getProductName(),
//									"",
//									format.format(new Date()),
//									format.format(new Date()),
//									"KaryotypingSecond",
//									"核型分析报告审核",
//									(User) ServletActionContext
//											.getRequest()
//											.getSession()
//											.getAttribute(
//													SystemConstants.USER_SESSION_KEY),
//									null, "报告生成","1", null, null, null, null, null,
//									null, null, null,scp.getResult(),scp.getResultDetail(),scp.getAdvice(),scp.getIsException());
//					 }
//				 }
//			}
//			sList.add(scp.getId());
//			ids+=scp.getId()+",";
		}
		karyotypingSecondDao.saveOrUpdateAll(saveItems);
//		String [] ids1=ids.split(",");
//		this.delKaryotypingSecond(ids1);
//		if(sList.size()>0){
//			for(String str:sList){
//				
//			}
//		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKaryotypingSecond(String[] ids) throws Exception {
		for (String id : ids) {
			KaryotypingSecond scp = karyotypingSecondDao.get(KaryotypingSecond.class, id);
			karyotypingSecondDao.delete(scp);
		}
	}
	/**
	 * 生成PDF报告文件
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
//	public void createReportFilePDF(String[] ids) throws Exception {
//		for (String id : ids) {
//			KaryotypingSecond sri = commonDAO.get(KaryotypingSecond.class, id);
//			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//			DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
//			SampleInfo sf = sampleInfoMainDao.findSampleInfo(sri
//					.getSampleCode());
//			SampleOrder so=commonDAO.get(SampleOrder.class, sri
//					.getSampleCode());
//			/*SampleOrder so = new SampleOrder();
//			if (sf != null) {
//				if (sf.getSampleOrder() != null) {
//					so = sf.getSampleOrder();
//				}
//			}*/
//			if (sri.getReportInfo() != null && sri.getTemplate() != null) {
//				FileInfo fileInfo = sri.getTemplate();
//				if (fileInfo != null) {
//					// Map<String, File> imgMap = new HashMap<String, File>();
//					// 临时地址
//					String tempFile = ConfigFileUtil
//							.getValueByKey("file.report.temp.path");
//					// 正式地址
//					String formFile = ConfigFileUtil
//							.getValueByKey("file.report.form.path");
//					FileInputStream in = new FileInputStream(
//							fileInfo.getFilePath());
//					PdfReader reader = new PdfReader(in);
//					String root = ConfigFileUtil.getRootPath() + File.separator
//							+ DateUtil.format(new Date(), "yyyyMMdd");
//					if (!new File(root).exists()) {
//						new File(root).mkdirs();
//					}
//					File deskFile = new File(tempFile, "PDF" + sri.getCode()
//							+ ".pdf");
//					PdfStamper ps = new PdfStamper(reader,
//							new FileOutputStream(deskFile)); // 生成的输出流
//
//					// 获取订单信息
//					AcroFields s = ps.getAcroFields();
//					s.setField("name", so.getName());
//					s.setField("gender", so.getGender());
//					if (so.getAge() != null) {
//						s.setField("age", so.getAge().toString());
//					} else {
//						s.setField("age", "");
//					}
//					if (so.getBirthDate() != null) {
//						s.setField("birthDay", so.getBirthDate());
//					} else {
//						s.setField("birthDay", "");
//					}
//					if(sri.getSampleCode()!=null){
//						s.setField("slideCode", sri.getSampleCode());
//					}else{
//						s.setField("slideCode", "");
//					}
//					s.setField("medicalNum", so.getMedicalNumber());
//					s.setField("sampleType", sri.getSampleType());
//					if(so.getCrmCustomer()!=null){
//						s.setField("crmCustomer", so.getCrmCustomer().getName());
//					}else{
//						s.setField("crmCustomer", "");
//					}
//					if (so.getSamplingDate() != null) {
//						s.setField("samplingDate",
//								so.getSamplingDate());
//					} else {
//						s.setField("samplingDate", "");
//					}
//					if(sri.getResult()!=null){
//						s.setField("result", sri.getResult());
//					}else{
//						s.setField("result", "");
//					}
//					if(sri.getFlxCellNum()!=null){
//						s.setField("flxCell", sri.getFlxCellNum());
//					}else{
//						s.setField("flxCell", "");
//					}
//					
//					if(sri.getKtCellNum()!=null){
//						s.setField("ktCell", sri.getKtCellNum());
//					}else{
//						s.setField("ktCell", "");
//					}
//					if(sri.getResultDetail()!=null){
//						s.setField("resultDescription", sri.getResultDetail());
//					}else{
//						s.setField("resultDescription", "");
//					}
//					/*if(sri.getAdvice()!=null){
//						s.setField("advice", sri.getAdvice());
//					}else{
//						s.setField("advice", "");
//					}*/
//					if(sri.getAdvice()!=null){
//						if(sri.getAdvice().equals("1")){
//							s.setField("advice", "门诊随访");
//						}else{
//							s.setField("advice", "遗传门诊随访");
//						}
//					}else{
//						s.setField("advice", "");
//					}
//					
//					//临床症状描述
//					if(so.getSubmitReasonName()!=null){
//						s.setField("lczz", so.getSubmitReasonName());
//					}else{
//						s.setField("lczz", "");
//					}
//					s.setField("reportDate", format1.format(new Date()));
//					
//					/*s.setField("checkUser", sri.getSnpAnalysis()
//							.getAcceptUser().getName());
//					s.setField("confirmUser", sri.getSnpAnalysis()
//							.getCreateUser().getName());*/
//					//获取相关图片信息
//					List<FileInfo> picList=karyotypingTaskDao.
//							findPicture(id, "karyotypingTaskItem");
//					//获取报告模板信息
//					ReportTemplateInfo rt=sri.getReportInfo();
//					//获取模板类型	0:2图	1:3图
//					String rtType=rt.getType();
//					//根据模板类型和上传的图片数量向PDF中插入图片
//					if(picList.size()>0){
//						if(rtType.equals("0")){
//							if(picList.size()==1){
//								insertImage(ps,s,picList.get(0).getFilePath(),"Text1");
//							}else if(picList.size()>1){
//								insertImage(ps,s,picList.get(0).getFilePath(),"Text1");
//								insertImage(ps,s,picList.get(1).getFilePath(),"Text2");
//							}
//						}else{
//							if(picList.size()==1){
//								insertImage(ps,s,picList.get(0).getFilePath(),"Text1");
//							}else if(picList.size()==2){
//								insertImage(ps,s,picList.get(0).getFilePath(),"Text1");
//								insertImage(ps,s,picList.get(1).getFilePath(),"Text2");
//							}else if(picList.size()>2){
//								insertImage(ps,s,picList.get(0).getFilePath(),"Text1");
//								insertImage(ps,s,picList.get(1).getFilePath(),"Text2");
//								insertImage(ps,s,picList.get(2).getFilePath(),"Text3");
//							}
//						}
//					}
//					ps.setFormFlattening(true);// 这句不能少
//					ps.close();
//					reader.close();
//					in.close();
//					commonDAO.update(sri);
//				}
//			}
//		}
//	}
	public void createReportFilePDF(String[] ids) throws Exception {
		// 临时地址
		String tempFile = ConfigFileUtil
				.getValueByKey("file.report.temp.path");
		// 正式地址
		String formFile = ConfigFileUtil
				.getValueByKey("file.report.form.path");
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH");
		DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		for (String id : ids) {
			KaryotypingSecond sri = commonDAO.get(KaryotypingSecond.class, id);
			SampleInfo sf = sampleInfoMainDao.findSampleInfo(sri
					.getSampleCode());
			/*SampleOrder so = new SampleOrder();
			if (sf != null) {
				if (sf.getSampleOrder() != null) {
					so = sf.getSampleOrder();
				}
			}*/
			SampleOrder so=commonDAO.get(SampleOrder.class, sri
					.getSampleCode());
			//获取相关图片
			List<FileInfo> picList=comSearchDao.findPicture(sri.getTaskResultId(), "karyotypingTaskItem");
			//报告模板
			ReportTemplateInfo reportInfo=sri.getReportInfo();
			//PDF文件
			FileInfo fileInfo = sri.getTemplate();
			if (fileInfo != null) {
				FileInputStream in = new FileInputStream(
						fileInfo.getFilePath());
				PdfReader reader = new PdfReader(in);
				String root = ConfigFileUtil.getRootPath() + File.separator
						+ DateUtil.format(new Date(), "yyyyMMdd");
				if (!new File(root).exists()) {
					new File(root).mkdirs();
				}
				File deskFile = new File(tempFile, "PDF" + sri.getCode()
						+ ".pdf");
				PdfStamper ps = new PdfStamper(reader,
						new FileOutputStream(deskFile)); // 生成的输出流
				
				AcroFields s = ps.getAcroFields();//生成新的PDF文件
				int count=picList.size();
				if(count>0){
					if(count==1){
						insertData(so,sri,s);
						insertImage(ps,picList.get(0).getFilePath(),s,"Text1");
					}else{
						for(int i=0;i<count;i++){
							insertDatas(so,sri,s,String.valueOf(i+1),String.valueOf(count));
							String jg=sri.getResult();
							jg=jg.replace("\r","");
							jg=jg.replace("\n","");
							int num0=jg.getBytes("UTF-8").length;	//字节长度
							String jgjs=sri.getResultDetail();	//结果解释
							jgjs=jgjs.replace("\r","");
							jgjs=jgjs.replace("\n","");
							int num1=jgjs.getBytes("UTF-8").length;	//字节长度
							int n0=0;
							int n1=0;
							String jg1="";
							String jg2="";
							String jg3="";
							
							String jgjs1="";
							String jgjs2="";
							String jgjs3="";
							//结果
							if(num0>0){
								if(num0<=88){
									jg1=jg;
								}else{
							        for (int i1=1; i1 <=(num0/88); i1++) {
						        		jg2=jg.replace(jg3, "");
						        		n0=jg2.getBytes("UTF-8").length;
						        		if(n0>=88){
						        			jg1+=comSearchService.bSubstring(jg2, 88)+"\r";
						        			jg3+=comSearchService.bSubstring(jg2, 88);
						        		}else if(n0<90 && n0>0){
						        			jg1+=comSearchService.bSubstring(jg2, 88)+"\r";
						        			jg3+=comSearchService.bSubstring(jg2, 88);
						        		}
							        }
								}
							}
							//jg1+=jg1+bSubstring(jg.replace(jg3, ""), n0);
							System.out.println(jg1+jg.replace(jg3, ""));
							if(!jg.replace(jg3, "").equals("")){
								if(sri.getResult()!=null){
									s.setField("result", jg1+jg.replace(jg3, ""));
								}else{
									s.setField("result", "");
								}
							}else{
								if(sri.getResult()!=null){
									s.setField("result", jg1);
								}else{
									s.setField("result", "");
								}
							}
							
							//结果解释
							if(num1>0){
								if(num1<=84){
									jgjs1=jgjs;
								}else{
							        for (int i1=0; i1 <=(num1/84); i1++) {
						        		jgjs2=jgjs.replace(jgjs3, "");
						        		n1=jgjs2.getBytes("UTF-8").length;
						        		if(n1>=84){
						        			jgjs1+=comSearchService.bSubstring(jgjs2, 84)+"\r";
						        			jgjs3+=comSearchService.bSubstring(jgjs2, 84);
						        		}else if(n1<84 && n1>0){
						        			jgjs1+=comSearchService.bSubstring(jgjs2, n1)+"\r";
						        			jgjs3+=comSearchService.bSubstring(jgjs2, n1);
						        		}
							        }
								}
							}
							System.out.println(jgjs1+jgjs.replace(jgjs3, ""));
							if(!jgjs.replace(jgjs3, "").equals("")){
								//jgjs1+=jgjs1+jgjs.replace(jgjs3, "");
								if(sri.getResultDetail()!=null){
									s.setField("resultDescription", jgjs1+jgjs.replace(jgjs3, ""));
								}else{
									s.setField("resultDescription", "");
								}
							}else{
								if(sri.getResultDetail()!=null){
									s.setField("resultDescription", jgjs1);
								}else{
									s.setField("resultDescription", "");
								}
							}
							
							if(sri.getAdvice()!=null){
								if(sri.getAdvice().equals("1")){
									s.setField("advice", "门诊随访");
								}else{
									s.setField("advice", "遗传门诊随访");
								}
							}else{
								s.setField("advice", "");
							}
							
							//临床症状描述
							if(so.getSubmitReasonName()!=null){
								s.setField("lczz", so.getSubmitReasonName());
							}else{
								s.setField("lczz", "");
							}
							
							if(sri.getFlxCellNum()!=null){
								s.setField("flxCell", sri.getFlxCellNum());
							}else{
								s.setField("flxCell", "");
							}
							
							if(sri.getKtCellNum()!=null){
								s.setField("ktCell", sri.getKtCellNum());
							}else{
								s.setField("ktCell", "");
							}
							System.out.println(sri.getFlxCellNum()+":细胞计数:"+sri.getKtCellNum());
							s.setField("reportDate", format1.format(new Date()));
							/*KaryotypingTask kt=commonDAO.get(KaryotypingTask.class, sri.getTaskId());
							s.setField("checkUser", kt
									.getAcceptUser().getName());
							s.setField("confirmUser", kt
									.getCreateUser().getName());*/
							
							insertImage(ps,picList.get(i).getFilePath(),s,"Text"+String.valueOf(i+1));
						}
					}
				}
				ps.setFormFlattening(true);// 这句不能少
				ps.close();
				reader.close();
				in.close();
				
				commonDAO.update(sri);
			}
		}
	}
	/**
	 * 向PDF插入数据
	 * @param ps
	 * @param s
	 * @param path
	 * @param text
	 */
	@SuppressWarnings("static-access")
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void insertData(SampleOrder so,KaryotypingSecond sri,
			AcroFields s) throws Exception{
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String names=so.getName();
		/*String res="";
		byte[] buff = new byte[1024];  
        //从字符串获取字节写入流  
        InputStream is = new ByteArrayInputStream(names.getBytes("UTF-8"));  
        int len = -1;  
        while(-1 != (len = is.read(buff))) {  
            //将字节数组转换为字符串  
            res = new String(buff, 0, len);  
            System.out.println(res);  
        }  */
		if(so.getName()!=null){
			s.setField("names", names);
		}else{
			s.setField("names", "");
		}
		System.out.println("姓名"+names);
		if(so.getGender()!=null){
			String m="男";
			String w="女";
			if(so.getGender().equals("1")){
				s.setField("genders", m);
			}else{
				s.setField("genders", w);
			}
		}else{
			s.setField("genders", "");
		}
		System.out.println("性别"+so.getGender());
		if (so.getAge() != null) {
			s.setField("age", so.getAge());
		} else {
			s.setField("age", "");
		}
		if (so.getBirthDate() != null) {
			s.setField("birthDay", format.format(so.getBirthDate()));
		} else {
			s.setField("birthDay", "");
		}
		if(sri.getSampleCode()!=null){
			s.setField("slideCode", sri.getSampleCode());
		}else{
			s.setField("slideCode", "");
		}
		s.setField("medicalNum", so.getMedicalNumber());
		if(sri.getSampleType()!=null){
			String t=sri.getSampleType();
			s.setField("sampleTypes", t);
		}else{
			s.setField("sampleTypes", "");
		}
	
		System.out.println("样本类型"+sri.getSampleType());
		if(so.getCrmCustomer()!=null){
			s.setField("crmCustomer", so.getCrmCustomer().getName());
		}else{
			s.setField("crmCustomer", "");
		}
		if (so.getSamplingDate() != null) {
			s.setField("samplingDate",
					format.format(so.getSamplingDate()));
		} else {
			s.setField("samplingDate", "");
		}
		String jg=sri.getResult();
		jg=jg.replace("\r","");
		jg=jg.replace("\n","");
		int num0=jg.getBytes("UTF-8").length;	//字节长度
		String jgjs=sri.getResultDetail();	//结果解释
		jgjs=jgjs.replace("\r","");
		jgjs=jgjs.replace("\n","");
		int num1=jgjs.getBytes("UTF-8").length;	//字节长度
		int n0=0;
		int n1=0;
		String jg1="";
		String jg2="";
		String jg3="";
		
		String jgjs1="";
		String jgjs2="";
		String jgjs3="";
		//结果
		if(num0>0){
			if(num0<=88){
				jg1=jg;
			}else{
		        for (int i1=1; i1 <=(num0/88); i1++) {
	        		jg2=jg.replace(jg3, "");
	        		n0=jg2.getBytes("UTF-8").length;
	        		if(n0>=88){
	        			jg1+=comSearchService.bSubstring(jg2, 88)+"\r";
	        			jg3+=comSearchService.bSubstring(jg2, 88);
	        		}else if(n0<90 && n0>0){
	        			jg1+=comSearchService.bSubstring(jg2, 88)+"\r";
	        			jg3+=comSearchService.bSubstring(jg2, 88);
	        		}
		        }
			}
		}
		//jg1+=jg1+bSubstring(jg.replace(jg3, ""), n0);
		System.out.println(jg1+jg.replace(jg3, ""));
		if(!jg.replace(jg3, "").equals("")){
			if(sri.getResult()!=null){
				s.setField("result", jg1+jg.replace(jg3, ""));
			}else{
				s.setField("result", "");
			}
		}else{
			if(sri.getResult()!=null){
				s.setField("result", jg1);
			}else{
				s.setField("result", "");
			}
		}
		
		//结果解释
		if(num1>0){
			if(num1<=84){
				jgjs1=jgjs;
			}else{
		        for (int i1=0; i1 <=(num1/84); i1++) {
	        		jgjs2=jgjs.replace(jgjs3, "");
	        		n1=jgjs2.getBytes("UTF-8").length;
	        		if(n1>=84){
	        			jgjs1+=comSearchService.bSubstring(jgjs2, 84)+"\r";
	        			jgjs3+=comSearchService.bSubstring(jgjs2, 84);
	        		}else if(n1<84 && n1>0){
	        			jgjs1+=comSearchService.bSubstring(jgjs2, n1)+"\r";
	        			jgjs3+=comSearchService.bSubstring(jgjs2, n1);
	        		}
		        }
			}
		}
		System.out.println(jgjs1+jgjs.replace(jgjs3, ""));
		if(!jgjs.replace(jgjs3, "").equals("")){
			//jgjs1+=jgjs1+jgjs.replace(jgjs3, "");
			if(sri.getResultDetail()!=null){
				s.setField("resultDescription", jgjs1+jgjs.replace(jgjs3, ""));
			}else{
				s.setField("resultDescription", "");
			}
		}else{
			if(sri.getResultDetail()!=null){
				s.setField("resultDescription", jgjs1);
			}else{
				s.setField("resultDescription", "");
			}
		}
		if(sri.getAdvice()!=null){
			if(sri.getAdvice().equals("1")){
				s.setField("advice", "门诊随访");
			}else{
				s.setField("advice", "遗传门诊随访");
			}
		}else{
			s.setField("advice", "");
		}
		
		//临床症状描述
		if(so.getSubmitReasonName()!=null){
			s.setField("lczz", so.getSubmitReasonName());
		}else{
			s.setField("lczz", "");
		}
		if(sri.getFlxCellNum()!=null){
			s.setField("flxCell", sri.getFlxCellNum());
		}else{
			s.setField("flxCell", "");
		}
		
		if(sri.getKtCellNum()!=null){
			s.setField("ktCell", sri.getKtCellNum());
		}else{
			s.setField("ktCell", "");
		}
		System.out.println(sri.getFlxCellNum()+":细胞计数:"+sri.getKtCellNum());
		s.setField("reportDate", format.format(new Date()));
		/*KaryotypingTask kt=commonDAO.get(KaryotypingTask.class, sri.getTaskId());
		s.setField("checkUser", kt
				.getAcceptUser().getName());
		s.setField("confirmUser", kt
				.getCreateUser().getName());*/
		//s.setField("pageNum", "1/1");
		commonDAO.update(sri);
	}
	/**
	 * 向PDF插入数据
	 * @param ps
	 * @param s
	 * @param path
	 * @param text
	 */
	@SuppressWarnings("static-access")
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void insertDatas(SampleOrder so,KaryotypingSecond sri,
			AcroFields s,String i,String a) throws Exception{
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		if(so.getName()!=null){
			String names=so.getName();
			s.setField("names"+i, names);
		}else{
			s.setField("names"+i, "");
		}
		System.out.println("姓名"+so.getName());
		if(so.getGender()!=null){
			String m="男";
			String w="女";
			if(so.getGender().equals("1")){
				s.setField("genders"+i, m);
			}else{
				s.setField("genders"+i, w);
			}
		}else{
			s.setField("genders"+i, "");
		}
		System.out.println("性别"+so.getGender());
		if (so.getAge() != null) {
			s.setField("age"+i, so.getAge());
		} else {
			s.setField("age"+i, "");
		}
		if (so.getBirthDate() != null) {
			s.setField("birthDay"+i, format.format(so.getBirthDate()));
		} else {
			s.setField("birthDay"+i, "");
		}
		if(sri.getSampleCode()!=null){
			s.setField("slideCode"+i, sri.getSampleCode());
		}else{
			s.setField("slideCode"+i, "");
		}
		s.setField("medicalNum"+i, so.getMedicalNumber());
		if(sri.getSampleType()!=null){
			String t=sri.getSampleType();
			s.setField("sampleTypes"+i, t);
		}else{
			s.setField("sampleTypes"+i, "");
		}
	
		System.out.println("样本类型"+sri.getSampleType());
		if(so.getCrmCustomer()!=null){
			s.setField("crmCustomer"+i, so.getCrmCustomer().getName());
		}else{
			s.setField("crmCustomer"+i, "");
		}
		if (so.getSamplingDate() != null) {
			s.setField("samplingDate"+i,
					format.format(so.getSamplingDate()));
		} else {
			s.setField("samplingDate"+i, "");
		}
		//s.setField("pageNum"+i, i+"/"+a);
		commonDAO.update(sri);
	}
	/**
	 * 向第PDF插入图片
	 * @param ps
	 * @param s
	 * @param path
	 * @param text
	 */
	@SuppressWarnings("static-access")
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void insertImage(PdfStamper ps,String path,AcroFields s,String text) throws Exception{
		try { 
		    int pageNo = s.getFieldPositions(text).get(0).page;
		    Rectangle signRect = s.getFieldPositions(text).get(0).position;
	        float x = signRect.getLeft();
	        float y = signRect.getBottom();

	        // 读图片
	        Image image = Image.getInstance(path);
	        // 获取操作的页面
	        PdfContentByte under = ps.getOverContent(pageNo);
	        // 根据域的大小缩放图片
	        image.scaleToFit(signRect.getWidth(), signRect.getHeight());
	        System.out.println(x);  
			System.out.println(y);  
	        // 添加图片
	        image.setAbsolutePosition(x, y);
	        under.addImage(image);

		}catch (Exception e){  
			// TODO Auto-generated catch block  
			e.printStackTrace();  
		}  
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitSample(String[] ids) throws Exception {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		List<KaryotypingSecond> list = this.karyotypingSecondDao.setResultByIds(ids);
		List<String> sList=new ArrayList<String>();
		String ids1="";
		for (KaryotypingSecond scp : list) {
			if (scp != null) {
				
				List<SampleOrder> listOrder = this.karyotypingSecondDao.setSampleOrderById(scp.getSampleCode());
				SampleOrder so = null;
				if(listOrder.size()>0){
				for (int i = 0; i < listOrder.size(); i++) {
					so =listOrder.get(i);
				    }
				}
				
				if(scp.getIsGood()!=null && !scp.getIsGood().equals("")){
					if(scp.getIsGood().equals("1")){
				    	//提交合格的到报告生成
				    	CreateReport cr = new CreateReport();
				    	if(so.getCreateDate() != null){
				    		cr.setReceivedDate(so.getCreateDate());
				    	}
				    	cr.setAttendingDoctor(so.getAttendingDoctor());
				    	cr.setCrmCustomerId(so.getCrmCustomer().getId());
				    	cr.setCrmCustomerName(so.getCrmCustomer().getName());
						cr.setCode(scp.getCode());
						cr.setSlideCode(scp.getSlideCode());
						cr.setSampleCode(scp.getSampleCode());
						cr.setSampleType(scp.getSampleType());
						cr.setProductId(scp.getProductId());
						cr.setProductName(scp.getProductName());
						cr.setResult(scp.getResult());
						cr.setResultDescription(scp.getResultDetail());
						cr.setAdvice(scp.getAdvice());
						cr.setYcbg(scp.getIsException());
						cr.setTemplate(scp.getTemplate());
						cr.setReportInfo(scp.getReportInfo());
						cr.setTaskId(scp.getTaskId());
						cr.setTaskType(scp.getTaskType());
						cr.setTaskResultId(scp.getTaskResultId());
						cr.setWaitDate(new Date());
						cr.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_REPORT_NEW);
						cr.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_REPORT_NEW_NAME);
						//cr.setOrderCode(so.getId());
				    	commonDAO.saveOrUpdate(cr);
				    	
				    	//完成后改变二审的状态
						scp.setState("2");
					}else{
				    	//不合格返回数据审核
				    	List<KaryotypingFirst> listf=karyotypingFirstDao.getKaryotypingFirstList(scp.getCode());
				    	if(listf.size()>0){
				    		KaryotypingFirst kf=listf.get(0);
				    		kf.setIsCommit(null);
				    		kf.setState("1");
				    		kf.setNote(scp.getNote());
				    		commonDAO.update(kf);
				    		ids1+=scp.getId()+",";
				    	}
				    }
				}
				scp.setIsCommit("1");
				sampleStateService
				.saveSampleState1(
						scp.getCode(),
						scp.getSampleCode(),
						scp.getProductId(),
						scp.getProductName(),
						"",
						format.format(new Date()),
						format.format(new Date()),
						"KaryotypingSecond",
						"核型分析报告审核",
						(User) ServletActionContext
								.getRequest()
								.getSession()
								.getAttribute(
										SystemConstants.USER_SESSION_KEY),
						null, "报告生成","1", null, null, null, null, null,
						null, null, null,scp.getResult(),scp.getResultDetail(),scp.getAdvice(),scp.getIsException());
			}
		}
		if(!ids1.equals("")){
			String [] ids2=ids1.split(",");
			this.delKaryotypingSecond(ids2);
		}
	}
}
