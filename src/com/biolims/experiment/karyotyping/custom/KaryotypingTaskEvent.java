package com.biolims.experiment.karyotyping.custom;


import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.karyotyping.service.KaryotypingTaskService;
import com.biolims.experiment.plasma.service.BloodSplitService;

public class KaryotypingTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		KaryotypingTaskService mbService = (KaryotypingTaskService) ctx.getBean("karyotypingTaskService");
		mbService.chengeState(applicationTypeActionId, contentId);
		return "";
	}
}
