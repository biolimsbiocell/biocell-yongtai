package com.biolims.experiment.sj.wkblend.service;


import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.dic.model.DicType;
import com.biolims.experiment.sequencing.model.SequencingTaskTemp;
import com.biolims.experiment.sj.wkblend.dao.WkBlendTaskDao;
import com.biolims.experiment.sj.wkblend.model.WkBlendTask;
import com.biolims.experiment.sj.wkblend.model.WkBlendTaskAbnormal;
import com.biolims.experiment.sj.wkblend.model.WkBlendTaskCos;
import com.biolims.experiment.sj.wkblend.model.WkBlendTaskInfo;
import com.biolims.experiment.sj.wkblend.model.WkBlendTaskItem;
import com.biolims.experiment.sj.wkblend.model.WkBlendTaskReagent;
import com.biolims.experiment.sj.wkblend.model.WkBlendTaskTemp;
import com.biolims.experiment.sj.wkblend.model.WkBlendTaskTemplate;
import com.biolims.file.model.FileInfo;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.sample.dao.SampleReceiveDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.storage.main.service.StorageService;
import com.biolims.storage.out.service.StorageOutService;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.primers.model.MappingPrimersLibraryItem;
import com.biolims.system.primers.service.MappingPrimersLibraryService;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.CosItem;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.model.TemplateItem;
import com.biolims.system.template.service.TemplateService;
import com.biolims.util.JsonUtils;
import com.csvreader.CsvReader;
import com.opensymphony.xwork2.ActionContext;
@Service
@Transactional
public class WkBlendTaskService {
	
	@Resource
	private WkBlendTaskDao wkBlendTaskDao;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private TemplateService templateService;
	@Resource
	private MappingPrimersLibraryService mappingPrimersLibraryService;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private StorageService storageService;
	@Resource
	private StorageOutService storageOutService;
	@Resource
	private SampleReceiveDao sampleReceiveDao;
	@Resource
	private DicSampleTypeDao dicSampleTypeDao;

	/**
	 * 
	 * @Title: get
	 * @Description:根据ID获取主表
	 * @author : 
	 * @date 
	 * @param id
	 * @return WkBlendTask
	 * @throws
	 */
	public WkBlendTask get(String id) {
		WkBlendTask wkBlendTask = commonDAO.get(WkBlendTask.class, id);
		return wkBlendTask;
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWkBlendTaskItem(String delStr, String[] ids, User user, String wkBlendTask_id) throws Exception {
		String delId="";
		for (String id : ids) {
			WkBlendTaskItem scp = wkBlendTaskDao.get(WkBlendTaskItem.class, id);
			if (scp.getId() != null) {
				WkBlendTask pt = wkBlendTaskDao.get(WkBlendTask.class, scp
						.getWkBlendTask().getId());
				pt.setSampleNum(pt.getSampleNum() - 1);
				// 改变左侧样本状态
				WkBlendTaskTemp wkBlendTaskTemp = this.commonDAO.get(
						WkBlendTaskTemp.class, scp.getTempId());
				if (wkBlendTaskTemp != null) {
					wkBlendTaskTemp.setState("1");
					wkBlendTaskDao.update(wkBlendTaskTemp);
				}
				wkBlendTaskDao.update(pt);
				wkBlendTaskDao.delete(scp);
			}
		delId+=scp.getSampleCode()+",";
		}
		if (ids.length>0) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setUserId(user.getId());
			li.setFileId(wkBlendTask_id);
			li.setModifyContent(delStr+delId);
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 
	 * @Title: delWkBlendTaskItemAf
	 * @Description: 重新排板
	 * @author : 
	 * @date 
	 * @param ids
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWkBlendTaskItemAf(String[] ids) throws Exception {
		for (String id : ids) {
			WkBlendTaskItem scp = wkBlendTaskDao.get(WkBlendTaskItem.class, id);
			if (scp.getId() != null) {
				scp.setState("1");
				wkBlendTaskDao.saveOrUpdate(scp);
			}

		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWkBlendTaskResult(String[] ids) throws Exception {
		for (String id : ids) {
			WkBlendTaskInfo scp = wkBlendTaskDao
					.get(WkBlendTaskInfo.class, id);
			if ((scp.getSubmit() == null || (scp.getSubmit() != null && scp
					.getSubmit().equals(""))))
				wkBlendTaskDao.delete(scp);
		}
	}

	/**
	 * 删除试剂明细
	 * 
	 * @param id2
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWkBlendTaskReagent(String id) throws Exception {
		if (id != null && !"".equals(id)) {
			WkBlendTaskReagent scp = wkBlendTaskDao.get(WkBlendTaskReagent.class,
					id);
			wkBlendTaskDao.delete(scp);
		}
	}

	/**
	 * 删除设备明细
	 * 
	 * @param id
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWkBlendTaskCos(String id) throws Exception {
		if (id != null && !"".equals(id)) {
			WkBlendTaskCos scp = wkBlendTaskDao.get(WkBlendTaskCos.class, id);
			wkBlendTaskDao.delete(scp);
		}
	}

	/**
	 * 
	 * @Title: saveWkBlendTaskTemplate
	 * @Description: 保存模板
	 * @author : 
	 * @date 
	 * @param sc
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveWkBlendTaskTemplate(WkBlendTask sc) {
		List<WkBlendTaskTemplate> tlist2 = wkBlendTaskDao.delTemplateItem(sc
				.getId());
		List<WkBlendTaskReagent> rlist2 = wkBlendTaskDao.delReagentItem(sc
				.getId());
		List<WkBlendTaskCos> clist2 = wkBlendTaskDao.delCosItem(sc.getId());
		commonDAO.deleteAll(tlist2);
		commonDAO.deleteAll(rlist2);
		commonDAO.deleteAll(clist2);
		List<TemplateItem> tlist = templateService.getTemplateItem(sc
				.getTemplate().getId(), null);
		List<ReagentItem> rlist = templateService.getReagentItem(sc
				.getTemplate().getId(), null);
		List<CosItem> clist = templateService.getCosItem(sc.getTemplate()
				.getId(), null);
		for (TemplateItem t : tlist) {
			WkBlendTaskTemplate ptt = new WkBlendTaskTemplate();
			ptt.setWkBlendTask(sc);
			ptt.setOrderNum(t.getOrderNum());
			ptt.setName(t.getName());
			ptt.setNote(t.getNote());
			ptt.setContent(t.getContent());
			wkBlendTaskDao.saveOrUpdate(ptt);
		}
		for (ReagentItem t : rlist) {
			WkBlendTaskReagent ptr = new WkBlendTaskReagent();
			ptr.setWkBlendTask(sc);
			ptr.setCode(t.getCode());
			ptr.setName(t.getName());
			ptr.setItemId(t.getItemId());
			ptr.setName(t.getName());
			ptr.setNote(t.getNote());
			wkBlendTaskDao.saveOrUpdate(ptr);
		}
		for (CosItem t : clist) {
			WkBlendTaskCos ptc = new WkBlendTaskCos();
			ptc.setWkBlendTask(sc);
			ptc.setItemId(t.getItemId());
			ptc.setName(t.getName());
			ptc.setNote(t.getNote());
			ptc.setType(t.getType());
			wkBlendTaskDao.saveOrUpdate(ptc);
		}
	}

	/**
	 * 
	 * @Title: changeState
	 * @Description:改变状态
	 * @author : 
	 * @date 
	 * @param applicationTypeActionId
	 * @param id
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String id)
			throws Exception {
		WkBlendTask sct = wkBlendTaskDao.get(WkBlendTask.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		wkBlendTaskDao.update(sct);
		if (sct.getTemplate() != null) {
			String iid = sct.getTemplate().getId();
			if (iid != null) {
				templateService.setCosIsUsedOver(iid);
			}
		}

		submitSample(id, null);

	}

	/**
	 * 
	 * @Title: submitSample
	 * @Description: 提交样本
	 * @author :
	 * @date 
	 * @param id
	 * @param ids
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitSample(String id, String[] ids) throws Exception {
		// 获取主表信息
		WkBlendTask sc = this.wkBlendTaskDao.get(WkBlendTask.class, id);
		// 获取结果表样本信息
		List<WkBlendTaskInfo> list;
		if (ids == null)
			list = this.wkBlendTaskDao.selectAllResultListById(id);
		else
			list = this.wkBlendTaskDao.selectAllResultListByIds(ids);

		for (WkBlendTaskInfo scp : list) {
			if (scp != null
					&& scp.getResult() != null
					&& (scp.getSubmit() == null || (scp.getSubmit() != null && scp
							.getSubmit().equals("")))) {
				if (scp.getResult().equals("1")) {// 合格
					String next = scp.getNextFlowId();

					if (next.equals("0009")) {// 样本入库
						SampleInItemTemp st = new SampleInItemTemp();
						st.setSampleState("1");
						st.setScopeId(sc.getScopeId());
						st.setScopeName(sc.getScopeName());
						st.setProductId(scp.getProductId());
						st.setProductName(scp.getProductName());
						st.setCode(scp.getCode());
						st.setSampleCode(scp.getSampleCode());
						st.setConcentration(scp.getConcentration());
						st.setVolume(scp.getVolume());
						DicSampleType t = commonDAO.get(DicSampleType.class, scp
								.getDicSampleType().getId());
						st.setSampleType(t.getName());
						st.setDicSampleType(scp.getDicSampleType());
						st.setState("1");
						st.setPatientName(scp.getPatientName());
						st.setSampleTypeId(scp.getDicSampleType().getId());
						st.setInfoFrom("SamplePlasmaInfo");
						sampleReceiveDao.saveOrUpdate(st);
					} else if (next.equals("0012")) {// 暂停
					} else if (next.equals("0013")) {// 终止
					} else if (next.equals("0023")) {// 上机测序
						SequencingTaskTemp d = new SequencingTaskTemp();
						scp.setState("1");
						d.setState("1");
						d.setConcentration(scp.getConcentration());
						d.setVolume(scp.getVolume());
						d.setOrderId(sc.getId());
						d.setScopeId(sc.getScopeId());
						d.setScopeName(sc.getScopeName());
						d.setProductId(scp.getProductId());
						d.setProductName(scp.getProductName());
						d.setSampleCode(scp.getSampleCode());
						d.setSampleInfo(scp.getSampleInfo());
						d.setCode(scp.getCode());
						DicSampleType t = commonDAO.get(DicSampleType.class,
								scp.getDicSampleType().getId());
						d.setSampleType(t.getName());
						wkBlendTaskDao.saveOrUpdate(d);
					} else {
						scp.setState("1");
						scp.setScopeId(sc.getScopeId());
						scp.setScopeName(sc.getScopeName());
						// 得到下一步流向的相关表单
						List<NextFlow> list_nextFlow = nextFlowDao
								.seletNextFlowById(next);
						for (NextFlow n : list_nextFlow) {
							Object o = Class.forName(
									n.getApplicationTypeTable().getClassPath())
									.newInstance();
							sampleInputService.copy(o, scp);
						}
					}
				} else {// 不合格
					WkBlendTaskAbnormal pa = new WkBlendTaskAbnormal();// 样本异常
					sampleInputService.copy(pa, scp);
					pa.setOrderId(sc.getId());
					pa.setState("1");
					pa.setScopeId(sc.getScopeId());
					pa.setScopeName(sc.getScopeName());
					commonDAO.saveOrUpdate(pa);
				}
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				sampleStateService
						.saveSampleState(
								scp.getCode(),
								scp.getSampleCode(),
								scp.getProductId(),
								scp.getProductName(),
								"",
								sc.getCreateDate(),
								format.format(new Date()),
								"WkBlendTask",
								"文库混合",
								(User) ServletActionContext
										.getRequest()
										.getSession()
										.getAttribute(
												SystemConstants.USER_SESSION_KEY),
								id, scp.getNextFlow(), scp.getResult(), null,
								null, null, null, null, null, null, null);
				
				scp.setSubmit("1");
				wkBlendTaskDao.saveOrUpdate(scp);
			}
		}
	}

	/**
	 * 
	 * @Title: findWkBlendTaskTable
	 * @Description: 展示主表
	 * @author :
	 * @date 
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> findWkBlendTaskTable(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		return wkBlendTaskDao.findWkBlendTaskTable(start, length, query, col,
				sort);
	}

	/**
	 * 
	 * @Title: selectWkBlendTaskTempTable
	 * @Description: 展示临时表
	 * @author : 
	 * @date 
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> selectWkBlendTaskTempTable(String[] codes, Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		return wkBlendTaskDao.selectWkBlendTaskTempTable(codes,start, length, query,
				col, sort);
	}
	/**
	 * 
	 * @Title: saveAllot
	 * @Description: 保存任务分配
	 * @author : 
	 * @date 
	 * @param main
	 * @param tempId
	 * @param userId
	 * @param templateId
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String saveAllot(String main, String[] tempId, String userId,
			String templateId, String logInfo) throws Exception {
		String id = "";
		if (main != null) {
			String mainJson = "[" + main + "]";
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					mainJson, List.class);
			WkBlendTask pt = new WkBlendTask();
			pt = (WkBlendTask) commonDAO.Map2Bean(list.get(0), pt);
			String name=pt.getName();
			Integer sampleNum=pt.getSampleNum();
			Integer maxNum=pt.getMaxNum();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if ((pt.getId() != null && pt.getId().equals(""))
					|| pt.getId().equals("NEW")) {
				String modelName = "WkBlendTask";
				String markCode = "WKHH";
				id = codingRuleService.genTransID(modelName, markCode);
				pt.setId(id);
				pt.setCreateDate(sdf.format(new Date()));
				pt.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				pt.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
				
			} else {
				id = pt.getId();
				pt=commonDAO.get(WkBlendTask.class, id);
				pt.setName(name);
				pt.setSampleNum(sampleNum);
				pt.setMaxNum(maxNum);
			}
			Template t = commonDAO.get(Template.class, templateId);
			t.setId(templateId);
			pt.setTemplate(t);
			pt.setTestUserOneId(userId);
			String [] users=userId.split(",");
			String userName="";
			for(int i=0;i<users.length;i++){
				if(i==users.length-1){
					User user=commonDAO.get(User.class, users[i]);
					userName+=user.getName();
				}else{
					User user=commonDAO.get(User.class, users[i]);
					userName+=user.getName()+",";
				}
			}
			pt.setTestUserOneName(userName);
			wkBlendTaskDao.saveOrUpdate(pt);
			if (pt.getTemplate() != null) {
				saveWkBlendTaskTemplate(pt);
			}
			if (tempId != null) {
				if (pt.getTemplate() != null) {
						for (String temp : tempId) {
							WkBlendTaskTemp ptt = wkBlendTaskDao.get(
									WkBlendTaskTemp.class, temp);
							if (t.getIsSeparate().equals("1")) {
								List<MappingPrimersLibraryItem> listMPI = mappingPrimersLibraryService
										.getMappingPrimersLibraryItem(ptt
												.getProductId());
								for (MappingPrimersLibraryItem mpi : listMPI) {
									WkBlendTaskItem pti = new WkBlendTaskItem();
									pti.setSampleCode(ptt.getSampleCode());
									pti.setProductId(ptt.getProductId());
									pti.setProductName(ptt.getProductName());
									pti.setSampleType(ptt.getSampleType());
									pti.setDicSampleTypeId(t.getDicSampleType() != null
											&& !"".equals(t.getDicSampleType()) ? t
											.getDicSampleType().getId() : t
											.getDicSampleTypeId());
									pti.setDicSampleTypeName(t
											.getDicSampleType() != null
											&& !"".equals(t.getDicSampleType()) ? t
											.getDicSampleType().getName() : t
											.getDicSampleTypeName());
									pti.setProductName(ptt.getProductName());
									pti.setProductNum(t.getProductNum() != null
											&& !"".equals(t.getProductNum()) ? t
											.getProductNum() : t
											.getProductNum1());
									pti.setCode(ptt.getCode());
									pti.setIndexa(ptt.getIndexa());
									pti.setConcentration(ptt.getConcentration());
									pti.setVolume(ptt.getVolume());
									if (t.getStorageContainer() != null) {
										pti.setState("1");
									}else{
										pti.setState("2");
									}
									ptt.setState("2");
									
									pti.setTempId(temp);
									pti.setWkBlendTask(pt);
									pti.setSampleInfo(ptt.getSampleInfo());
									pti.setChromosomalLocation(mpi
											.getChromosomalLocation());
									wkBlendTaskDao.saveOrUpdate(pti);
								}
							} else {
								WkBlendTaskItem pti = new WkBlendTaskItem();
								pti.setSampleCode(ptt.getSampleCode());
								pti.setProductId(ptt.getProductId());
								pti.setProductName(ptt.getProductName());
								pti.setDicSampleTypeId(t.getDicSampleType() != null
										&& !"".equals(t.getDicSampleType()) ? t
										.getDicSampleType().getId() : t
										.getDicSampleTypeId());
								pti.setProductNum(t.getProductNum() != null
										&& !"".equals(t.getProductNum()) ? t
										.getProductNum() : t.getProductNum1());
								pti.setDicSampleTypeName(t.getDicSampleType() != null
										&& !"".equals(t.getDicSampleType()) ? t
										.getDicSampleType().getName() : t
										.getDicSampleTypeName());
								pti.setCode(ptt.getCode());
								pti.setIndexa(ptt.getIndexa());
								pti.setSampleType(ptt.getSampleType());
								pti.setConcentration(ptt.getConcentration());
								pti.setVolume(ptt.getVolume());
								pti.setSampleInfo(ptt.getSampleInfo());
								if (t.getStorageContainer() != null) {
									pti.setState("1");
								}else{
									pti.setState("2");
								}
								ptt.setState("2");
								pti.setTempId(temp);
								pti.setWkBlendTask(pt);
								wkBlendTaskDao.saveOrUpdate(pti);
							}
							wkBlendTaskDao.saveOrUpdate(ptt);
						}
				}

			
			}
			
			if(logInfo!=null&&!"".equals(logInfo)){
				LogInfo li=new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(pt.getId());
				li.setModifyContent(logInfo);
				commonDAO.saveOrUpdate(li);
			}
		}
		return id;

	}

	/**
	 * 
	 * @Title: findWkBlendTaskItemTable
	 * @Description:展示未排版样本
	 * @author : 
	 * @date 
	 * @param id
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> findWkBlendTaskItemTable(String id,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return wkBlendTaskDao.findWkBlendTaskItemTable(id, start, length, query,
				col, sort);
	}

	/**
	 * 
	 * @Title: saveMakeUp
	 * @Description: 保存排板数据
	 * @author : 
	 * @date 
	 * @param id
	 * @param item
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveMakeUp(String id, String item, String logInfo) throws Exception {
		List<WkBlendTaskItem> saveItems = new ArrayList<WkBlendTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(item,
				List.class);
		WkBlendTask pt = commonDAO.get(WkBlendTask.class, id);
		WkBlendTaskItem scp = new WkBlendTaskItem();
		for (Map<String, Object> map : list) {
			// 将map信息读入实体类
			scp = (WkBlendTaskItem) wkBlendTaskDao.Map2Bean(map, scp);
			WkBlendTaskItem pti = commonDAO.get(WkBlendTaskItem.class,
					scp.getId());
			pti.setDicSampleTypeId(scp.getDicSampleTypeId());
			pti.setDicSampleTypeName(scp.getDicSampleTypeName());
			pti.setProductNum(scp.getProductNum());
			pti.setBlendCode(scp.getBlendCode());
			pti.setMixAmount(scp.getMixAmount());
			pti.setMixVolume(scp.getMixVolume());
			pti.setIndexa(scp.getIndexa());
			pti.setColor(scp.getColor());    
			scp.setWkBlendTask(pt);
			saveItems.add(pti);
		}
		if(logInfo!=null&&!"".equals(logInfo)){
			LogInfo li=new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		wkBlendTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 
	 * @Title: showWellPlate
	 * @Description: 展示孔板
	 * @author : 
	 * @date 
	 * @param id
	 * @return
	 * @throws Exception
	 *             List<WkBlendTaskItem>
	 * @throws
	 */
	public List<WkBlendTaskItem> showWellPlate(String id) throws Exception {
		List<WkBlendTaskItem> list = wkBlendTaskDao.showWellPlate(id);
		return list;
	}

	/**
	 * 
	 * @Title: findWkBlendTaskItemAfTable
	 * @Description: 展示排板后
	 * @author :
	 * @date 
	 * @param scId
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> findWkBlendTaskItemAfTable(String scId,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return wkBlendTaskDao.findWkBlendTaskItemAfTable(scId, start, length,
				query, col, sort);
	}

	/**
	 * 
	 * @Title: plateLayout
	 * @Description: 排板
	 * @author : 
	 * @date 
	 * @param data
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void plateLayout(String[] data) {
		for (String da : data) {
			String[] d = da.split(",");
			WkBlendTaskItem pti = commonDAO.get(WkBlendTaskItem.class, d[0]);
			pti.setPosId(d[1]);
			pti.setCounts(d[2]);
			pti.setState("2");
			wkBlendTaskDao.saveOrUpdate(pti);
		}
	}

	/**
	 * 
	 * @Title: showWkBlendTaskStepsJson
	 * @Description: 展示实验步骤
	 * @author :
	 * @date 
	 * @param id
	 * @param code
	 * @return Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> showWkBlendTaskStepsJson(String id, String code) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<WkBlendTaskTemplate> pttList = wkBlendTaskDao
				.showWkBlendTaskStepsJson(id, code);
		List<WkBlendTaskReagent> ptrList = wkBlendTaskDao
				.showWkBlendTaskReagentJson(id, code);
		List<WkBlendTaskCos> ptcList = wkBlendTaskDao.showWkBlendTaskCosJson(id,
				code);
		List<Object> plate = wkBlendTaskDao.showWellList(id);
		map.put("template", pttList);
		map.put("reagent", ptrList);
		map.put("cos", ptcList);
		map.put("plate", plate);
		return map;
	}

	/**
	 * 
	 * @Title: showWkBlendTaskResultTableJson
	 * @Description: 展示结果
	 * @author : 
	 * @date
	 * @param id
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> showWkBlendTaskResultTableJson(String id,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return wkBlendTaskDao.showWkBlendTaskResultTableJson(id, start, length,
				query, col, sort);
	}

	/**
	 * 
	 * @Title: plateSample
	 * @Description: 孔板样本
	 * @author : 
	 * @date
	 * @param id
	 * @param counts
	 * @return Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> plateSample(String id, String counts) {
		WkBlendTask pt = commonDAO.get(WkBlendTask.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		List<WkBlendTaskItem> list = wkBlendTaskDao.plateSample(id, counts);
		Integer row = null;
		Integer col = null;
		if (pt.getTemplate().getStorageContainer() != null) {
		row = pt.getTemplate().getStorageContainer().getRowNum();
		col = pt.getTemplate().getStorageContainer().getColNum();
		}
		map.put("list", list);
		map.put("rowNum", row);
		map.put("colNum", col);
		return map;
	}

	/**
	 * 
	 * @Title: getCsvContent
	 * @Description: 上传结果
	 * @author : 
	 * @date 
	 * @param id
	 * @param fileId
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void getCsvContent(String id, String fileId) throws Exception {

		FileInfo fileInfo = wkBlendTaskDao.get(FileInfo.class, fileId);
		String filepath = fileInfo.getFilePath();
		File a = new File(filepath);
		if (a.isFile()) {
			WkBlendTask pt = wkBlendTaskDao.get(WkBlendTask.class, id);
			InputStream is = new FileInputStream(filepath);
			if (is != null) {
				CsvReader reader = new CsvReader(is, Charset.forName("GBK"));
				reader.readHeaders();// 去除表头
				while (reader.readRecord()) {
					String code = reader.get(0);
					if (code != null && !"".equals(code)) {
						List<WkBlendTaskInfo> listPTI = wkBlendTaskDao
								.findWkBlendTaskInfoByCode(code);
						if (listPTI.size() > 0) {
							WkBlendTaskInfo spi = listPTI.get(0);
							spi.setLane(reader.get(4));
							spi.setConcentration(Double.parseDouble(reader
									.get(5)));
							spi.setVolume(Double.parseDouble(reader.get(6)));
							wkBlendTaskDao.saveOrUpdate(spi);
						}
					}
				}
			}
		}
	}

	/**
	 * 
	 * @Title: saveSteps
	 * @Description: 保存实验步骤
	 * @author : 
	 * @date 
	 * @param id
	 * @param templateJson
	 * @param reagentJson
	 * @param cosJson
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSteps(String id, String templateJson, String reagentJson,
			String cosJson, String logInfo) {
		if(logInfo!=null&&!"".equals(logInfo)){
			LogInfo li=new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}	
		if (templateJson != null && !templateJson.equals("{}")
				&& !templateJson.equals("")) {
			saveTemplate(id, templateJson);
		}
		if (reagentJson != null && !reagentJson.equals("{}")
				&& !reagentJson.equals("")) {
			saveReagent(id, reagentJson);
		}
		if (cosJson != null && !cosJson.equals("{}") && !cosJson.equals("")) {
			saveCos(id, cosJson);
		}
	}

	/**
	 * 
	 * @Title: saveTemplate
	 * @Description: 保存模板明细
	 * @author :
	 * @date 
	 * @param id
	 * @param templateJson
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveTemplate(String id, String templateJson) {
		JSONArray array = JSONArray.fromObject("[" + templateJson + "]");
		JSONObject object = JSONObject.fromObject(array.get(0));
		String templateId = (String) object.get("id");
		if (templateId != null && !"".equals(templateId)) {
			WkBlendTaskTemplate ptt = wkBlendTaskDao.get(
					WkBlendTaskTemplate.class, templateId);
			String contentData = object.get("contentData").toString();
			if (contentData != null && !"".equals(contentData)) {
				ptt.setContentData(contentData);
			}
			String startTime = (String) object.get("startTime");
			if (startTime != null && !"".equals(startTime)) {
				ptt.setStartTime(startTime);
			}
			String endTime = (String) object.get("endTime");
			if (endTime != null && !"".equals(endTime)) {
				ptt.setEndTime(endTime);
			}
			wkBlendTaskDao.saveOrUpdate(ptt);
		}
	}

	/**
	 * 
	 * @Title: saveReagent
	 * @Description: 保存试剂
	 * @author : 
	 * @date 
	 * @param id
	 * @param reagentJson
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveReagent(String id, String reagentJson) {
	
		JSONArray array = JSONArray.fromObject(reagentJson);
		for (int i = 0; i < array.size(); i++) {
			JSONObject object = JSONObject.fromObject(array.get(i));
			String reagentId = (String) object.get("id");
			WkBlendTaskReagent ptr = null;
			if (reagentId != null && !"".equals(reagentId)) {
				ptr = commonDAO.get(WkBlendTaskReagent.class, reagentId);
				String batch = (String) object.get("batch");
				if (batch != null && !"".equals(batch)) {
					ptr.setBatch(batch);
				}
				String sn = (String) object.get("sn");
				if (sn != null && !"".equals(sn)) {
					ptr.setSn(sn);
				}

			} else {
				ptr = new WkBlendTaskReagent();
				String code = (String) object.get("code");
				if (code != null && !"".equals(code)) {
					ptr.setCode(code);
					String name = (String) object.get("name");
					if (name != null && !"".equals(name)) {
						ptr.setName(name);
					}
					String itemid = (String) object.get("itemId");
					if (itemid != null && !"".equals(itemid)) {
						ptr.setItemId(itemid);
					}
					String batch = (String) object.get("batch");
					if (batch != null && !"".equals(batch)) {
						ptr.setBatch(batch);
					}
					String sn = (String) object.get("sn");
					if (sn != null && !"".equals(sn)) {
						ptr.setSn(sn);
					}
					ptr.setWkBlendTask(commonDAO.get(WkBlendTask.class, id));
				}

			}
			wkBlendTaskDao.saveOrUpdate(ptr);
		}
	
	}

	/**
	 * 
	 * @Title: saveCos
	 * @Description: 保存设备
	 * @author :
	 * @date
	 * @param id
	 * @param cosJson
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveCos(String id, String cosJson) {
	
		JSONArray array = JSONArray.fromObject(cosJson);
		for (int j = 0; j < array.size(); j++) {
			JSONObject object = JSONObject.fromObject(array.get(j));
			String cosId = (String) object.get("id");
			WkBlendTaskCos ptc=null;
			if (cosId != null && !"".equals(cosId)) {
				ptc = commonDAO.get(WkBlendTaskCos.class, cosId);
				String code = (String) object.get("code");
				if (code != null && !"".equals(code)) {
					ptc.setCode(code);
				}
				String name = (String) object.get("name");
				if (name != null && !"".equals(name)) {
					ptc.setName(name);
				}
			} else {
				ptc = new WkBlendTaskCos();
				String typeId = (String) object.get("typeId");
				if (typeId != null && !"".equals(typeId)) {
					DicType dt = commonDAO.get(DicType.class, typeId);
					ptc.setType(dt);
					String code = (String) object.get("code");
					if (code != null && !"".equals(code)) {
						ptc.setCode(code);
					}
					String name = (String) object.get("name");
					if (name != null && !"".equals(name)) {
						ptc.setName(name);
					}
					String itemid = (String) object.get("itemId");
					if (itemid != null && !"".equals(itemid)) {
						ptc.setItemId(itemid);
					}
					ptc.setWkBlendTask(commonDAO.get(WkBlendTask.class, id));
				}
			}
			wkBlendTaskDao.saveOrUpdate(ptc);
		}
	
	
	}

	/**
	 * 
	 * @Title: plateSampleTable
	 * @Description: 孔板样本列表
	 * @author : 
	 * @date 
	 * @param id
	 * @param counts
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> plateSampleTable(String id, String counts,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return wkBlendTaskDao.plateSampleTable(id, counts, start, length, query,
				col, sort);
	}

	/**
	 * 
	 * @Title: bringResult
	 * @Description: 生成结果
	 * @author :
	 * @date
	 * @param id
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void bringResult(String id) throws Exception {
	
	
		List<WkBlendTaskItem> list = wkBlendTaskDao.showWellPlate(id);
		Map<String, Object> map = new HashMap<String, Object>();
		List<WkBlendTaskInfo> spiList = wkBlendTaskDao.selectResultListById(id);
		for (WkBlendTaskItem pti : list) {
			boolean b = true;
			for (WkBlendTaskInfo spi : spiList) {
				if (spi.getSampleCode().indexOf(pti.getSampleCode()) > -1) {
					b = false;
				}
			}
			if (b) {
				if (pti.getBlendCode() == null || "".equals(pti.getBlendCode())) {
					if (pti.getProductNum() != null
							&& !"".equals(pti.getProductNum())) {
						Integer pn = Integer.parseInt(pti.getProductNum());
						for (int i = 0; i < pn; i++) {
							WkBlendTaskInfo scp = new WkBlendTaskInfo();
							DicSampleType ds = commonDAO.get(
									DicSampleType.class,
									pti.getDicSampleTypeId());
							scp.setDicSampleType(ds);
							scp.setSampleCode(pti.getSampleCode());
							scp.setProductId(pti.getProductId());
							scp.setProductName(pti.getProductName());
							scp.setSampleType(pti.getSampleType());
							scp.setMixAmount(pti.getMixAmount());
							scp.setMixVolume(pti.getMixVolume());
							scp.setWkBlendTask(pti.getWkBlendTask());
							scp.setSampleInfo(pti.getSampleInfo());
							scp.setResult("1");
							String markCode = "";
							if (scp.getDicSampleType() != null) {
								DicSampleType d = commonDAO.get(
										DicSampleType.class, scp
												.getDicSampleType().getId());
								String[] sampleCode = scp.getSampleCode()
										.split(",");
								if (sampleCode.length > 1) {
									String str = sampleCode[0].substring(0,
											sampleCode[0].length() - 4);
									for (int j = 0; j < sampleCode.length; j++) {
										str += sampleCode[j].substring(
												sampleCode[j].length() - 4,
												sampleCode[j].length());
									}
									if (d == null) {
										markCode = str;
									} else {
										markCode = str + d.getCode();
									}
								} else {
									if (d == null) {
										markCode = scp.getSampleCode();
									} else {
										markCode = scp.getSampleCode()
												+ d.getCode();
									}
								}

							}
							String code = codingRuleService.getCode(
									"WkBlendTaskInfo", markCode, 00, 2, null);
							scp.setCode(code);
							wkBlendTaskDao.saveOrUpdate(scp);
						}
					}
				} else {
					if (!map.containsKey(String.valueOf(pti.getBlendCode()))) {
						if (pti.getProductNum() != null
								&& !"".equals(pti.getProductNum())) {
							Integer pn = Integer.parseInt(pti.getProductNum());
							for (int i = 0; i < pn; i++) {
								WkBlendTaskInfo scp = new WkBlendTaskInfo();
								DicSampleType ds = commonDAO.get(
										DicSampleType.class,
										pti.getDicSampleTypeId());
								scp.setDicSampleType(ds);
								scp.setSampleCode(pti.getSampleCode());
								scp.setProductId(pti.getProductId());
								scp.setProductName(pti.getProductName());
								scp.setMixAmount(pti.getMixAmount());
								scp.setMixVolume(pti.getMixVolume());
								scp.setSampleType(pti.getSampleType());
								scp.setWkBlendTask(pti.getWkBlendTask());
								scp.setSampleInfo(pti.getSampleInfo());
								scp.setResult("1");
								String markCode = "";
								if (scp.getDicSampleType() != null) {
									DicSampleType d = commonDAO
											.get(DicSampleType.class, scp
													.getDicSampleType().getId());
									String[] sampleCode = scp.getSampleCode()
											.split(",");
									if (sampleCode.length > 1) {
										String str = sampleCode[0].substring(0,
												sampleCode[0].length() - 4);
										for (int j = 0; j < sampleCode.length; j++) {
											str += sampleCode[j].substring(
													sampleCode[j].length() - 4,
													sampleCode[j].length());
										}
										if (d == null) {
											markCode = str;
										} else {
											markCode = str + d.getCode();
										}
									} else {
										if (d == null) {
											markCode = scp.getSampleCode();
										} else {
											markCode = scp.getSampleCode()
													+ d.getCode();
										}
									}

								}
								String code = codingRuleService.getCode(
										"WkBlendTaskInfo", markCode, 00, 2,
										null);
								scp.setCode(code);
								map.put(String.valueOf(pti.getBlendCode()),
										code);
								wkBlendTaskDao.saveOrUpdate(scp);
							}
						}
					} else {
						String code = (String) map.get(String.valueOf(pti
								.getBlendCode()));
						WkBlendTaskInfo spi = wkBlendTaskDao
								.getResultByCode(code);
						spi.setSampleCode(spi.getSampleCode() + ","
								+ pti.getSampleCode());
						wkBlendTaskDao.saveOrUpdate(spi);
					}
				}
			}
		}
	
	
	}

	/**
	 * 
	 * @Title: saveResult
	 * @Description: 保存结果
	 * @author : 
	 * @date 
	 * @param id
	 * @param dataJson
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveResult(String id, String dataJson, String logInfo,String confirmUser) throws Exception {

		List<WkBlendTaskInfo> saveItems = new ArrayList<WkBlendTaskInfo>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(dataJson,
				List.class);
		WkBlendTask pt = commonDAO.get(WkBlendTask.class, id);		
		for (Map<String, Object> map : list) {
		WkBlendTaskInfo scp = new WkBlendTaskInfo();
			// 将map信息读入实体类
			scp = (WkBlendTaskInfo) wkBlendTaskDao.Map2Bean(map, scp);
			Double v1 = scp.getConcentration();
			Double v2 = scp.getVolume();
			BigDecimal one = null;
			BigDecimal two = null;
			if(v1!=null) {
				one = new BigDecimal(Double.toString(v1));  
			}
			if(v2!=null) {
				two = new BigDecimal(Double.toString(v2));   
			}
		    if(one!=null&&two!=null) {
		    	BigDecimal result = one.multiply(two);
		    	double there = result.doubleValue();
		    	scp.setSumTotal(there);
		    }
			scp.setWkBlendTask(pt);
			saveItems.add(scp);
		}
		if(confirmUser!=null&&!"".equals(confirmUser)){
			User confirm=commonDAO.get(User.class, confirmUser);
			pt.setConfirmUser(confirm);
			wkBlendTaskDao.saveOrUpdate(pt);
		}
		if(logInfo!=null&&!"".equals(logInfo)){
			LogInfo li=new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		wkBlendTaskDao.saveOrUpdateAll(saveItems);

	}

	public List<WkBlendTaskItem> findWkBlendTaskItemList(String scId)
			throws Exception {
		List<WkBlendTaskItem> list = wkBlendTaskDao.selectWkBlendTaskItemList(scId);
		return list;
	}
	public Integer generateBlendCode(String id) {
		return wkBlendTaskDao.generateBlendCode(id);
	}

	public WkBlendTaskInfo getInfoById(String id) {
		// TODO Auto-generated method stub
		return wkBlendTaskDao.get(WkBlendTaskInfo.class, id);
	}
}

