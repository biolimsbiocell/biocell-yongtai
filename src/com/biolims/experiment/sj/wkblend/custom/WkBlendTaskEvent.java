package com.biolims.experiment.sj.wkblend.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.sj.wkblend.service.WkBlendTaskService;

public class WkBlendTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		WkBlendTaskService mbService = (WkBlendTaskService) ctx
				.getBean("wkBlendTaskService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
