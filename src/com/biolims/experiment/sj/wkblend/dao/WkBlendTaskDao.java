package com.biolims.experiment.sj.wkblend.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import com.biolims.common.constants.SystemConstants;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.sj.wkblend.model.WkBlendTask;
import com.biolims.experiment.sj.wkblend.model.WkBlendTaskCos;
import com.biolims.experiment.sj.wkblend.model.WkBlendTaskItem;
import com.biolims.experiment.sj.wkblend.model.WkBlendTaskReagent;
import com.biolims.experiment.sj.wkblend.model.WkBlendTaskTemp;
import com.biolims.experiment.sj.wkblend.model.WkBlendTaskTemplate;
import com.biolims.experiment.sj.wkblend.model.WkBlendTaskInfo;
import com.opensymphony.xwork2.ActionContext;


@Repository
@SuppressWarnings("unchecked")
public class WkBlendTaskDao extends BaseHibernateDao {

	public Map<String, Object> findWkBlendTaskTable(Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from WkBlendTask where 1=1";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from WkBlendTask where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<WkBlendTask> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public Map<String, Object> selectWkBlendTaskTempTable(String[] codes, Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
			
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from WkBlendTaskTemp where 1=1 and state='1'";
			String key = "";
			if(query!=null&&!"".equals(query)){
				key=map2Where(query);
			}
			if(codes!=null&&!codes.equals("")){
				key+=" and code in (:alist)";
			}
			String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
			if(!"all".equals(scopeId)){
				key+=" and scopeId='"+scopeId+"'";
			}
			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				List<WkBlendTaskTemp> list=null;
				Long filterCount = (Long) getSession().createQuery(countHql).uniqueResult();
				String hql = "from WkBlendTaskTemp  where 1=1 and state='1'";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					col=col.replace("-", ".");
					key+=" order by "+col+" "+sort;
				}
				if(codes!=null&&!codes.equals("")){
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).setParameterList("alist", codes).list();
				}else{
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
				}
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
				}
			return map;
		
			
	}

	public Map<String, Object> findWkBlendTaskItemTable(String id,Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from WkBlendTaskItem where 1=1 and state='1' and wkBlendTask.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from WkBlendTaskItem  where 1=1 and state='1' and wkBlendTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<WkBlendTaskItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public List<WkBlendTaskItem> showWellPlate(String id) {
		String hql="from WkBlendTaskItem  where 1=1 and state='2' and  wkBlendTask.id='"+id+"'";
		List<WkBlendTaskItem> list = getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> findWkBlendTaskItemAfTable(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from WkBlendTaskItem where 1=1 and state='2' and wkBlendTask.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from WkBlendTaskItem  where 1=1 and state='2' and wkBlendTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<WkBlendTaskItem> list =this.getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
		}

	public List<String> getCountGroup(String id) {

		String countHql = "select count(*) from WkBlendTaskItem where 1=1 and state='2' and wkBlendTask.id='"+id+"'";
		String key = "";
		List<String> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "select counts from WkBlendTaskItem where 1=1 and state='2' and wkBlendTask.id='"+id+"' group by counts";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
		
	}

	public List<WkBlendTaskTemplate> showWkBlendTaskStepsJson(String id,String code) {

		String countHql = "select count(*) from WkBlendTaskTemplate where 1=1 and wkBlendTask.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and orderNum ='"+code+"'";
		}else{
		}
		List<WkBlendTaskTemplate> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from WkBlendTaskTemplate where 1=1 and wkBlendTask.id='"+id+"'";
			key+=" order by orderNum asc";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<WkBlendTaskReagent> showWkBlendTaskReagentJson(String id,String code) {
		String countHql = "select count(*) from WkBlendTaskReagent where 1=1 and wkBlendTask.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<WkBlendTaskReagent> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from WkBlendTaskReagent where 1=1 and wkBlendTask.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<WkBlendTaskCos> showWkBlendTaskCosJson(String id,String code) {
		String countHql = "select count(*) from WkBlendTaskCos where 1=1 and wkBlendTask.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<WkBlendTaskCos> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from WkBlendTaskCos where 1=1 and wkBlendTask.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<WkBlendTaskTemplate> delTemplateItem(String id) {
		List<WkBlendTaskTemplate> list=new ArrayList<WkBlendTaskTemplate>();
		String hql = "from WkBlendTaskTemplate where 1=1 and wkBlendTask.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<WkBlendTaskReagent> delReagentItem(String id) {
		List<WkBlendTaskReagent> list=new ArrayList<WkBlendTaskReagent>();
		String hql = "from WkBlendTaskReagent where 1=1 and wkBlendTask.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<WkBlendTaskCos> delCosItem(String id) {
		List<WkBlendTaskCos> list=new ArrayList<WkBlendTaskCos>();
		String hql = "from WkBlendTaskCos where 1=1 and wkBlendTask.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> showWkBlendTaskResultTableJson(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from WkBlendTaskInfo where 1=1 and wkBlendTask.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from WkBlendTaskInfo  where 1=1 and wkBlendTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<WkBlendTaskInfo> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	}

	public List<Object> showWellList(String id) {
		
		String hql="select counts,count(*) from WkBlendTaskItem where 1=1 and wkBlendTask.id='"+id+"' group by counts";
		List<Object> list=getSession().createQuery(hql).list();
	
		return list;
	}

	public List<WkBlendTaskItem> plateSample(String id, String counts) {
		String hql="from WkBlendTaskItem where 1=1 and wkBlendTask.id='"+id+"'";
		String key="";
		if(counts==null||counts.equals("")){
			key=" and counts is null";
		}else{
			key="and counts='"+counts+"'";
		}
		List<WkBlendTaskItem> list=getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> plateSampleTable(String id, String counts,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from WkBlendTaskItem where 1=1 and wkBlendTask.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		if(counts==null||counts.equals("")){
			key+=" and counts is null";
		}else{
			key+="and counts='"+counts+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from WkBlendTaskItem  where 1=1 and wkBlendTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<WkBlendTaskItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	
	}

	public List<WkBlendTaskInfo> findWkBlendTaskInfoByCode(String code) {
		String hql="from WkBlendTaskInfo where 1=1 and code='"+code+"'";
		List<WkBlendTaskInfo> list=new ArrayList<WkBlendTaskInfo>();
		list=getSession().createQuery(hql).list();
		return list;
	}
	
	public List<WkBlendTaskInfo> selectAllResultListById(String code) {
		String hql = "from WkBlendTaskInfo  where (submit is null or submit='') and wkBlendTask.id='"
				+ code + "'";
		List<WkBlendTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	public List<WkBlendTaskInfo> selectResultListById(String id) {
		String hql = "from WkBlendTaskInfo  where wkBlendTask.id='"
				+ id + "'";
		List<WkBlendTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<WkBlendTaskInfo> selectAllResultListByIds(String[] codes) {
		String insql = "''";
		for (String code : codes) {
			insql += ",'" + code + "'";
		}
		String hql = "from WkBlendTaskInfo t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<WkBlendTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

		public List<WkBlendTaskItem> selectWkBlendTaskItemList(String scId)
			throws Exception {
		String hql = "from WkBlendTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and wkBlendTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<WkBlendTaskItem> list = new ArrayList<WkBlendTaskItem>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}
	
		public Integer generateBlendCode(String id) {
			String hql="select max(blendCode) from WkBlendTaskItem where 1=1 and wkBlendTask.id='"+id+"'";
			Integer blendCode=(Integer) this.getSession().createQuery(hql).uniqueResult();
			return blendCode;
		}

		public WkBlendTaskInfo getResultByCode(String code) {
			String hql=" from WkBlendTaskInfo where 1=1 and code='"+code+"'";
			WkBlendTaskInfo spi=(WkBlendTaskInfo) this.getSession().createQuery(hql).uniqueResult();
			return spi;
		}
	
}