package com.biolims.experiment.sj.wknd.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.pooling.dao.PoolingDao;
import com.biolims.experiment.qc.qpcrxd.dao.QpcrxdTaskDao;
import com.biolims.experiment.qc.qpcrxd.model.QpcrxdTaskResult;
import com.biolims.experiment.sj.wknd.dao.WkndTaskDao;
import com.biolims.experiment.sj.wknd.model.WkndTask;
import com.biolims.experiment.sj.wknd.model.WkndTaskCos;
import com.biolims.experiment.sj.wknd.model.WkndTaskItem;
import com.biolims.experiment.sj.wknd.model.WkndTaskReagent;
import com.biolims.experiment.sj.wknd.model.WkndTaskResult;
import com.biolims.experiment.sj.wknd.model.WkndTaskTemp;
import com.biolims.experiment.sj.wknd.model.WkndTaskTemplate;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.storage.main.service.StorageService;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class WkndTaskService {
	@Resource
	private WkndTaskDao wkndTaskDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private QpcrxdTaskDao qpcrxdTaskDao;
	@Resource
	private PoolingDao poolingDao;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private StorageService storageService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findWkndTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return wkndTaskDao.selectWkndTaskList(mapForQuery, startNum, limitNum,
				dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(WkndTask i) throws Exception {

		wkndTaskDao.saveOrUpdate(i);

	}

	public WkndTask get(String id) {
		WkndTask wkndTask = commonDAO.get(WkndTask.class, id);
		return wkndTask;
	}

	public Map<String, Object> findWkndTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = wkndTaskDao.selectWkndTaskItemList(scId,
				startNum, limitNum, dir, sort);
		List<WkndTaskItem> list = (List<WkndTaskItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findWkndTaskTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = wkndTaskDao.selectWkndTaskTemplateList(
				scId, startNum, limitNum, dir, sort);
		List<WkndTaskTemplate> list = (List<WkndTaskTemplate>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findWkndTaskReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = wkndTaskDao.selectWkndTaskReagentList(
				scId, startNum, limitNum, dir, sort);
		List<WkndTaskReagent> list = (List<WkndTaskReagent>) result.get("list");
		return result;
	}

	public Map<String, Object> findWkndTaskCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = wkndTaskDao.selectWkndTaskCosList(scId,
				startNum, limitNum, dir, sort);
		List<WkndTaskCos> list = (List<WkndTaskCos>) result.get("list");
		return result;
	}

	public Map<String, Object> findWkndTaskResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = wkndTaskDao.selectWkndTaskResultList(scId,
				startNum, limitNum, dir, sort);
		List<WkndTaskResult> list = (List<WkndTaskResult>) result.get("list");
		return result;
	}

	public Map<String, Object> findWkndTaskTempList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = wkndTaskDao.selectWkndTaskTempList(scId,
				startNum, limitNum, dir, sort);
		List<WkndTaskTemp> list = (List<WkndTaskTemp>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWkndTaskItem(WkndTask sc, String itemDataJson)
			throws Exception {
		List<WkndTaskItem> saveItems = new ArrayList<WkndTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			WkndTaskItem scp = new WkndTaskItem();
			// 将map信息读入实体类
			scp = (WkndTaskItem) wkndTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setWkndTask(sc);

			saveItems.add(scp);
		}
		wkndTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWkndTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			WkndTaskItem scp = wkndTaskDao.get(WkndTaskItem.class, id);
			wkndTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWkndTaskTemplate(WkndTask sc, String itemDataJson)
			throws Exception {
		List<WkndTaskTemplate> saveItems = new ArrayList<WkndTaskTemplate>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			WkndTaskTemplate scp = new WkndTaskTemplate();
			// 将map信息读入实体类
			scp = (WkndTaskTemplate) wkndTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setWkndTask(sc);

			saveItems.add(scp);
		}
		wkndTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWkndTaskTemplate(String[] ids) throws Exception {
		for (String id : ids) {
			WkndTaskTemplate scp = wkndTaskDao.get(WkndTaskTemplate.class, id);
			wkndTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWkndTaskReagent(WkndTask sc, String itemDataJson)
			throws Exception {
		List<WkndTaskReagent> saveItems = new ArrayList<WkndTaskReagent>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			WkndTaskReagent scp = new WkndTaskReagent();
			// 将map信息读入实体类
			scp = (WkndTaskReagent) wkndTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setWkndTask(sc);
			// 用量 = 单个用量*样本数量
			if (scp != null && scp.getSampleNum() != null
					&& scp.getOneNum() != null) {
				scp.setNum(scp.getOneNum() * scp.getSampleNum());
			}

			saveItems.add(scp);
		}
		wkndTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWkndTaskReagent(String[] ids) throws Exception {
		for (String id : ids) {
			WkndTaskReagent scp = wkndTaskDao.get(WkndTaskReagent.class, id);
			wkndTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWkndTaskCos(WkndTask sc, String itemDataJson)
			throws Exception {
		List<WkndTaskCos> saveItems = new ArrayList<WkndTaskCos>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			WkndTaskCos scp = new WkndTaskCos();
			// 将map信息读入实体类
			scp = (WkndTaskCos) wkndTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setWkndTask(sc);

			saveItems.add(scp);
		}
		wkndTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWkndTaskCos(String[] ids) throws Exception {
		for (String id : ids) {
			WkndTaskCos scp = wkndTaskDao.get(WkndTaskCos.class, id);
			wkndTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWkndTaskResult(WkndTask sc, String itemDataJson)
			throws Exception {
		List<WkndTaskResult> saveItems = new ArrayList<WkndTaskResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			WkndTaskResult scp = new WkndTaskResult();
			// 将map信息读入实体类
			scp = (WkndTaskResult) wkndTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setWkndTask(sc);

			saveItems.add(scp);
		}
		wkndTaskDao.saveOrUpdateAll(saveItems);

		// // 设置保留三维小数
		// DecimalFormat df = new DecimalFormat("######0.000");
		//
		// List<WkndTaskResult> list2 =
		// this.wkndTaskDao.setResultList(sc.getId());
		// for (WkndTaskResult q : list2) {
		//
		// if (q.getYmend() != null && q.getWkpdcd() != null
		// && !"".equals(q.getWkpdcd())) {
		// // sox = (ymend-wkpdcd)/wkpdcd 上调或下调计算公式
		// Double d = (Double.parseDouble(q.getYmend()) - Double
		// .parseDouble(q.getWkpdcd()))
		// / Double.parseDouble(q.getWkpdcd());
		// q.setSox(Double.valueOf(df.format(d * 100)).toString());
		// if (q.getQy() != null && !"".equals(q.getQy())) {
		// // js = ymend*qy/wkpdcd-qy 加水或干燥计算公式
		// Double a = Double.parseDouble(q.getYmend())
		// * Double.parseDouble(q.getQy())
		// / Double.parseDouble(q.getWkpdcd())
		// - Double.parseDouble(q.getQy());
		// q.setJs(Double.valueOf(df.format(a)).toString());
		//
		// if (q.getYzlnd() != null) {
		// // tzzlnd = yzlnd*qy/(qy+js) 调整后质量浓度计算公式
		// Double b = Double.parseDouble(q.getYzlnd())
		// * Double.parseDouble(q.getQy())
		// / (Double.parseDouble(q.getQy()) + Double
		// .parseDouble(q.getJs()));
		// q.setTzzlnd(Double.valueOf(df.format(b)).toString());
		//
		// }
		// }
		// // tzmend = wkpdcd 调整后摩尔浓度计算公式
		// q.setTzmend(q.getWkpdcd());
		// }
		//
		// }
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWkndTaskResult(String[] ids) throws Exception {
		for (String id : ids) {
			WkndTaskResult scp = wkndTaskDao.get(WkndTaskResult.class, id);
			wkndTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWkndTaskTemp(WkndTask sc, String itemDataJson)
			throws Exception {
		List<WkndTaskTemp> saveItems = new ArrayList<WkndTaskTemp>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			WkndTaskTemp scp = new WkndTaskTemp();
			// 将map信息读入实体类
			scp = (WkndTaskTemp) wkndTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			// scp.setWkndTask(sc);

			saveItems.add(scp);
		}
		wkndTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWkndTaskTemp(String[] ids) throws Exception {
		for (String id : ids) {
			WkndTaskTemp scp = wkndTaskDao.get(WkndTaskTemp.class, id);
			wkndTaskDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(WkndTask sc, Map jsonMap) throws Exception {
		if (sc != null) {
			wkndTaskDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("wkndTaskItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveWkndTaskItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("wkndTaskTemplate");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveWkndTaskTemplate(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("wkndTaskReagent");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveWkndTaskReagent(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("wkndTaskCos");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveWkndTaskCos(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("wkndTaskResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveWkndTaskResult(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("wkndTaskTemp");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveWkndTaskTemp(sc, jsonStr);
			}
		}
	}

	// 审核完成
//	public void changeState(String applicationTypeActionId, String id)
//			throws Exception {
//		WkndTask sct = wkndTaskDao.get(WkndTask.class, id);
//		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
//		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
//		User user = (User) ServletActionContext.getRequest().getSession()
//				.getAttribute(SystemConstants.USER_SESSION_KEY);
//		sct.setConfirmUser(user);
//		wkndTaskDao.update(sct);
//
//		// 将批次信息反馈到模板中
//		List<WkndTaskReagent> list1 = wkndTaskDao.setReagentList(id);
//		for (WkndTaskReagent dt : list1) {
//			String bat1 = dt.getBatch(); // 实验中试剂的批次
//			String drid = dt.getTReagent(); // 实验中保存的试剂ID
//			List<ReagentItem> list2 = templateDao.setReagentListById(drid);
//			for (ReagentItem ri : list2) {
//				if (bat1 != null) {
//					ri.setBatch(bat1);
//				}
//			}
//			// 改变库存主数据试剂数量
//			this.storageService.UpdateStorageAndItemNum(dt.getCode(),
//					dt.getBatch(), dt.getNum());
//		}
//
//		// 获取结果表样本信息
//		List<WkndTaskResult> list = this.wkndTaskDao.setWkndResultById(id);
//		for (WkndTaskResult scp : list) {
//			if (scp != null) {
//				if (scp.getNextFlow() != null) {
//					if (scp.getNextFlowId().equals("0009")) {// 样本入库
//						SampleInItemTemp st = new SampleInItemTemp();
//						st.setCode(scp.getYhhzh());
//						st.setSampleCode(scp.getSjlane());
//						st.setInfoFrom("WkndTaskResult");
//						st.setVolume(Double.parseDouble(scp.getTl()));
//						st.setState("1");
//						this.wkndTaskDao.saveOrUpdate(st);
//					} else if (scp.getNextFlowId().equals("0023")) {// 上机测序
////						SequencingTaskTemp s = new SequencingTaskTemp();
////						s.setCode(scp.getSjfz());
////						s.setPoolingCode(scp.getYhhzh());
////						s.setSampleCode(scp.getTzzlnd());
////						s.setName(scp.getTzmend());
////						Double lane = Double.parseDouble(scp.getQy())
////								+ Double.parseDouble(scp.getJs());
////						s.setLane(lane.toString());
////						s.setState("1");
////						this.wkndTaskDao.saveOrUpdate(s);
//					} else {
//						// 得到下一步流向的相关表单
//						List<NextFlow> list_nextFlow = nextFlowDao
//								.seletNextFlowById(scp.getNextFlowId());
//						for (NextFlow n : list_nextFlow) {
//							Object o = Class.forName(
//									n.getApplicationTypeTable().getClassPath())
//									.newInstance();
//							scp.setState("1");
//							sampleInputService.copy(o, scp);
//						}
//					}
//				}
//			}
//			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//			// 根据上机分组号查询文库混合QPCR
//			List<QpcrxdTaskResult> listQpcrxdTaskResult = this.qpcrxdTaskDao
//					.setResultListbySjfz(scp.getSjfz());
//			for (QpcrxdTaskResult q : listQpcrxdTaskResult) {
//				// 根据富集文库查询明细样本
//				List<PoolingTaskItem> listPoolingTaskItem = this.poolingDao
//						.getItemListByfjwk(q.getCode());
//				for (PoolingTaskItem p : listPoolingTaskItem) {
//					sampleStateService.saveSampleState(
//							p.getCode(),
//							p.getSampleCode(),
//							p.getProductId(),
//							p.getProductName(),
//							"",
//							format.format(sct.getCreateDate()),
//							format.format(new Date()),
//							"WkndTask",
//							"文库浓度调整",
//							(User) ServletActionContext
//									.getRequest()
//									.getSession()
//									.getAttribute(
//											SystemConstants.USER_SESSION_KEY),
//							id, scp.getNextFlow(), null, null, null, null,
//							null, null, null, null, null);
//				}
//			}
//		}
//	}

	public List<WkndTaskItem> findDnaItemList(String scId) throws Exception {
		List<WkndTaskItem> list = wkndTaskDao.setItemList(scId);
		return list;
	}

	/**
	 * 明细入库
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void rukuWkndTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			WkndTaskItem scp = wkndTaskDao.get(WkndTaskItem.class, id);
			SampleInItemTemp st = new SampleInItemTemp();
			st.setCode(scp.getSjfz());
			st.setSampleCode(scp.getHhzh());
			st.setInfoFrom("WkndTaskItem");
			st.setState("1");
			this.wkndTaskDao.saveOrUpdate(st);
			scp.setState("1");
		}
	}

	/**
	 * 
	 * @Title: getwkndTaskByIds
	 * @Description: TODO(通过id查找对应的文库混合信息)
	 * @param @param idc
	 * @param @return    设定文件
	 * @return PoolingBlendInfo    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-8-24 下午5:54:58
	 * @throws
	 */
	public WkndTaskResult getwkndTaskByIds(String id) {
		return wkndTaskDao.findwkndTaskByid(id);
	}

}
