package com.biolims.experiment.sj.wknd.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.sj.wknd.model.WkndTask;
import com.biolims.experiment.sj.wknd.model.WkndTaskCos;
import com.biolims.experiment.sj.wknd.model.WkndTaskItem;
import com.biolims.experiment.sj.wknd.model.WkndTaskReagent;
import com.biolims.experiment.sj.wknd.model.WkndTaskResult;
import com.biolims.experiment.sj.wknd.model.WkndTaskTemp;
import com.biolims.experiment.sj.wknd.model.WkndTaskTemplate;

@Repository
@SuppressWarnings("unchecked")
public class WkndTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectWkndTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from WkndTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<WkndTask> list = new ArrayList<WkndTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectWkndTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from WkndTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and wkndTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<WkndTaskItem> list = new ArrayList<WkndTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectWkndTaskTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from WkndTaskTemplate where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and wkndTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<WkndTaskTemplate> list = new ArrayList<WkndTaskTemplate>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectWkndTaskReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from WkndTaskReagent where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and wkndTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<WkndTaskReagent> list = new ArrayList<WkndTaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectWkndTaskCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from WkndTaskCos where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and wkndTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<WkndTaskCos> list = new ArrayList<WkndTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectWkndTaskResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from WkndTaskResult where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and wkndTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<WkndTaskResult> list = new ArrayList<WkndTaskResult>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectWkndTaskTempList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from WkndTaskTemp where 1=1 and state='1'";
		String key = "";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<WkndTaskTemp> list = new ArrayList<WkndTaskTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by fjwk desc";

			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public List<WkndTaskItem> setItemList(String id) {
		String hql = "from WkndTaskItem where 1=1 and wkndTask.id='" + id + "'";
		List<WkndTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<WkndTaskResult> setResultList(String code) {
		String hql = "from WkndTaskResult where 1=1 and wkndTask.id='" + code
				+ "'";
		List<WkndTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据样本编号查询
	public List<WkndTaskResult> setWkndResultById(String code) {
		String hql = "from WkndTaskResult t where wkndTask.id='" + code + "'";
		List<WkndTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/*
	 * 根据主表ID查询试剂明细
	 */
	public List<WkndTaskReagent> setReagentList(String code) {
		String hql = "from WkndTaskReagent where 1=1 and wkndTask.id='" + code
				+ "'";
		List<WkndTaskReagent> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 
	 * @Title: findwkndTaskByid
	 * @Description: TODO(通过id查找对应的信息)
	 * @param @param id
	 * @param @return    设定文件
	 * @return WkndTaskResult    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-8-24 下午5:57:43
	 * @throws
	 */
	public WkndTaskResult findwkndTaskByid(String id) {
		String hql = "from WkndTaskResult t where 1=1 and t.id='" + id + "'";
		WkndTaskResult list = (WkndTaskResult) this.getSession()
				.createQuery(hql).uniqueResult();
		return list;
	}

}