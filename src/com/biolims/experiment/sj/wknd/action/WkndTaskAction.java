package com.biolims.experiment.sj.wknd.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.sj.wkblend.model.WkBlendTaskTemplate;
import com.biolims.experiment.sj.wknd.model.WkndTask;
import com.biolims.experiment.sj.wknd.model.WkndTaskCos;
import com.biolims.experiment.sj.wknd.model.WkndTaskItem;
import com.biolims.experiment.sj.wknd.model.WkndTaskReagent;
import com.biolims.experiment.sj.wknd.model.WkndTaskResult;
import com.biolims.experiment.sj.wknd.model.WkndTaskTemp;
import com.biolims.experiment.sj.wknd.model.WkndTaskTemplate;
import com.biolims.experiment.sj.wknd.service.WkndTaskService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

@Namespace("/experiment/sj/wknd/wkndTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class WkndTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "250302";
	@Autowired
	private WkndTaskService wkndTaskService;
	private WkndTask wkndTask = new WkndTask();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;
	@Resource
	private CodingRuleService codingRuleService;

	@Action(value = "showWkndTaskList")
	public String showWkndTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/sj/wknd/wkndTask.jsp");
	}

	@Action(value = "showWkndTaskListJson")
	public void showWkndTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = wkndTaskService.findWkndTaskList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<WkndTask> list = (List<WkndTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("reciveDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "wkndTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogWkndTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/sj/wknd/wkndTaskDialog.jsp");
	}

	@Action(value = "showDialogWkndTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogWkndTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = wkndTaskService.findWkndTaskList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<WkndTask> list = (List<WkndTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("reciveDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editWkndTask")
	public String editWkndTask() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			wkndTask = wkndTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "qpcrxdTask");
			String tName = workflowProcessInstanceService
					.findToDoTaskNameByFormId(id);
			putObjToContext("taskName", tName);
		} else {
			wkndTask.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			wkndTask.setCreateUser(user);
			wkndTask.setCreateDate(new Date());
			wkndTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			wkndTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}

		toState(wkndTask.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/sj/wknd/wkndTaskEdit.jsp");
	}

	@Action(value = "copyWkndTask")
	public String copyWkndTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		wkndTask = wkndTaskService.get(id);
		wkndTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/sj/wknd/wkndTaskEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = wkndTask.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "WkndTask";
			String markCode = "WKND";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			wkndTask.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("wkndTaskItem", getParameterFromRequest("wkndTaskItemJson"));

		aMap.put("wkndTaskTemplate",
				getParameterFromRequest("wkndTaskTemplateJson"));

		aMap.put("wkndTaskReagent",
				getParameterFromRequest("wkndTaskReagentJson"));

		aMap.put("wkndTaskCos", getParameterFromRequest("wkndTaskCosJson"));

		aMap.put("wkndTaskResult",
				getParameterFromRequest("wkndTaskResultJson"));

		aMap.put("wkndTaskTemp", getParameterFromRequest("wkndTaskTempJson"));

		wkndTaskService.save(wkndTask, aMap);

		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/experiment/sj/wknd/wkndTask/editWkndTask.action?id="
				+ wkndTask.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);

	}

	@Action(value = "viewWkndTask")
	public String toViewWkndTask() throws Exception {
		String id = getParameterFromRequest("id");
		wkndTask = wkndTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/sj/wknd/wkndTaskEdit.jsp");
	}

	@Action(value = "showWkndTaskItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWkndTaskItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/sj/wknd/wkndTaskItem.jsp");
	}

	@Action(value = "showWkndTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWkndTaskItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wkndTaskService.findWkndTaskItemList(
					scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<WkndTaskItem> list = (List<WkndTaskItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("fjwk", "");
			map.put("sjfz", "");
			map.put("yhhzh", "");
			map.put("hhzh", "");
			map.put("sjlane", "");
			map.put("xylr", "");
			map.put("clr", "");
			map.put("flr", "");
			map.put("xzlr", "");
			map.put("qtlr", "");
			map.put("tl", "");
			map.put("yzlnd", "");
			map.put("ymend", "");
			map.put("zjcwgs", "");
			map.put("zjcwlx", "");
			map.put("wkndTask-name", "");
			map.put("wkndTask-id", "");
			map.put("wkpdcd", "");
			map.put("cxlx", "");
			map.put("cxpt", "");
			map.put("cxdc", "");
			map.put("tempId", "");
			map.put("state", "");
			map.put("stepNum", "");
			map.put("orderNumber", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "showTemplateWaitList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTemplateWaitList() throws Exception {
		String scId = getRequest().getParameter("id");
		putObjToContext("id", scId);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/sj/wknd/showTemplateWaitList.jsp");
	}

	@Action(value = "showTemplateWaitListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTemplateWaitListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wkndTaskService
					.findWkndTaskTemplateList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<WkndTaskTemplate> list = (List<WkndTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("reciveUser-name", "");
			map.put("reciveUser-id", "");
			map.put("tItem", "");
			map.put("code", "");
			map.put("stepName", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("state", "");
			map.put("note", "");
			map.put("sampleCodes", "");
			map.put("wkndTask-name", "");
			map.put("wkndTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWkndTaskItem")
	public void delWkndTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wkndTaskService.delWkndTaskItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showWkndTaskTemplateList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWkndTaskTemplateList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/sj/wknd/wkndTaskTemplate.jsp");
	}

	@Action(value = "showWkndTaskTemplateListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWkndTaskTemplateListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wkndTaskService
					.findWkndTaskTemplateList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<WkndTaskTemplate> list = (List<WkndTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("reciveUser-name", "");
			map.put("reciveUser-id", "");
			map.put("tItem", "");
			map.put("code", "");
			map.put("stepName", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("state", "");
			map.put("note", "");
			map.put("sampleCodes", "");
			map.put("wkndTask-name", "");
			map.put("wkndTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWkndTaskTemplate")
	public void delWkndTaskTemplate() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wkndTaskService.delWkndTaskTemplate(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showWkndTaskReagentList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWkndTaskReagentList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/sj/wknd/wkndTaskReagent.jsp");
	}

	@Action(value = "showWkndTaskReagentListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWkndTaskReagentListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wkndTaskService
					.findWkndTaskReagentList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<WkndTaskReagent> list = (List<WkndTaskReagent>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("batch", "");
			map.put("count", "");
			map.put("oneNum", "");
			map.put("sampleNum", "");
			map.put("num", "");
			map.put("isGood", "");
			map.put("note", "");
			map.put("wkndTask-name", "");
			map.put("wkndTask-id", "");
			map.put("itemId", "");
			map.put("tReagent", "");
			map.put("state", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWkndTaskReagent")
	public void delWkndTaskReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wkndTaskService.delWkndTaskReagent(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showWkndTaskCosList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWkndTaskCosList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/sj/wknd/wkndTaskCos.jsp");
	}

	@Action(value = "showWkndTaskCosListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWkndTaskCosListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wkndTaskService.findWkndTaskCosList(
					scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<WkndTaskCos> list = (List<WkndTaskCos>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("temperature", "");
			map.put("speed", "");
			map.put("time", "");
			map.put("note", "");
			map.put("isGood", "");
			map.put("wkndTask-name", "");
			map.put("wkndTask-id", "");
			map.put("itemId", "");
			map.put("tCos", "");
			map.put("state", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWkndTaskCos")
	public void delWkndTaskCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wkndTaskService.delWkndTaskCos(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showWkndTaskResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWkndTaskResultList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/sj/wknd/wkndTaskResult.jsp");
	}

	@Action(value = "showWkndTaskResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWkndTaskResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wkndTaskService
					.findWkndTaskResultList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<WkndTaskResult> list = (List<WkndTaskResult>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("fjwk", "");
			map.put("sjfz", "");
			map.put("yhhzh", "");
			map.put("hhzh", "");
			map.put("sjlane", "");
			map.put("xylr", "");
			map.put("clr", "");
			map.put("flr", "");
			map.put("xzlr", "");
			map.put("qtlr", "");
			map.put("tl", "");
			map.put("wkpdcd", "");
			map.put("yzlnd", "");
			map.put("ymend", "");
			map.put("sox", "");
			map.put("qy", "");
			map.put("js", "");
			map.put("gz", "");
			map.put("tzzlnd", "");
			map.put("tzmend", "");
			map.put("nextFlow", "");
			map.put("note", "");
			map.put("wkndTask-id", "");
			map.put("wkndTask-name", "");
			map.put("tempId", "");
			map.put("state", "");
			map.put("nextFlowId", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWkndTaskResult")
	public void delWkndTaskResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wkndTaskService.delWkndTaskResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showWkndTaskTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWkndTaskTempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/sj/wknd/wkndTaskTemp.jsp");
	}

	@Action(value = "showWkndTaskTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWkndTaskTempListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wkndTaskService.findWkndTaskTempList(
					scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<WkndTaskTemp> list = (List<WkndTaskTemp>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("fjwk", "");
			map.put("sjfz", "");
			map.put("yhhzh", "");
			map.put("hhzh", "");
			map.put("xylr", "");
			map.put("clr", "");
			map.put("flr", "");
			map.put("xzlr", "");
			map.put("qtlr", "");
			map.put("tl", "");
			map.put("wkpdcd", "");
			map.put("yzlnd", "");
			map.put("ymend", "");
			map.put("cxlx", "");
			map.put("cxpt", "");
			map.put("cxdc", "");
			map.put("tempid", "");
			map.put("state", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWkndTaskTemp")
	public void delWkndTaskTemp() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wkndTaskService.delWkndTaskTemp(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 明细入库信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "rukuWkndTaskItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void rukuWkndTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wkndTaskService.rukuWkndTaskItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public WkndTaskService getWkndTaskService() {
		return wkndTaskService;
	}

	public void setWkndTaskService(WkndTaskService wkndTaskService) {
		this.wkndTaskService = wkndTaskService;
	}

	public WkndTask getWkndTask() {
		return wkndTask;
	}

	public void setWkndTask(WkndTask wkndTask) {
		this.wkndTask = wkndTask;
	}

}
