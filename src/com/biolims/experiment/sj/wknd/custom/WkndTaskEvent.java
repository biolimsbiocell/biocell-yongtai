package com.biolims.experiment.sj.wknd.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.sj.wknd.service.WkndTaskService;

public class WkndTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		WkndTaskService mbService = (WkndTaskService) ctx.getBean("wkndTaskService");
//		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}