package com.biolims.experiment.sj.wknd.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 文库浓度调整结果
 * @author lims-platform
 * @date 2016-08-01 14:49:53
 * @version V1.0
 * 
 */
@Entity
@Table(name = "WKND_TASK_RESULT")
@SuppressWarnings("serial")
public class WkndTaskResult extends EntityDao<WkndTaskResult> implements
		java.io.Serializable {
	/** id */
	private String id;
	/** 富集文库 */
	private String fjwk;
	/** 上机分组 */
	private String sjfz;
	/** 预混合组号 */
	private String yhhzh;
	/** 混合组号 */
	private String hhzh;
	/** 上机lane号 */
	private String sjlane;
	/** 血液文库比例% */
	private String xylr;
	/** ctdna文库比例% */
	private String clr;
	/** ffpe文库比例% */
	private String flr;
	/** 新鲜组织文库比例% */
	private String xzlr;
	/** 其他文库比例% */
	private String qtlr;
	/** 通量 */
	private String tl;
	/** 各组文库片段长度 */
	private String wkpdcd;
	/** 原质量浓度（ng/ul） */
	private String yzlnd;
	/** 原摩尔浓度（nm） */
	private String ymend;
	/** 下调或上调（%） */
	private String sox;
	/** 取样（ul） */
	private String qy;
	/** 加水（ul） */
	private String js;
	/** 干燥（min） */
	private String gz;
	/** 调整后质量浓度（ng/ul） */
	private String tzzlnd;
	/** 调整后摩尔浓度（nm） */
	private String tzmend;
	/** 下一步流向 */
	private String nextFlow;
	/** 备注 */
	private String note;
	/** 相关主表 */
	private WkndTask wkndTask;
	// 临时表id
	private String tempId;
	// 状态
	private String state;
	/** 下一步流向ID */
	private String nextFlowId;

	public String getNextFlowId() {
		return nextFlowId;
	}

	public void setNextFlowId(String nextFlowId) {
		this.nextFlowId = nextFlowId;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String id
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 富集文库
	 */
	@Column(name = "FJWK", length = 50)
	public String getFjwk() {
		return this.fjwk;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 富集文库
	 */
	public void setFjwk(String fjwk) {
		this.fjwk = fjwk;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 上机分组
	 */
	@Column(name = "SJFZ", length = 100)
	public String getSjfz() {
		return this.sjfz;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 上机分组
	 */
	public void setSjfz(String sjfz) {
		this.sjfz = sjfz;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 预混合组号
	 */
	@Column(name = "YHHZH", length = 50)
	public String getYhhzh() {
		return this.yhhzh;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 预混合组号
	 */
	public void setYhhzh(String yhhzh) {
		this.yhhzh = yhhzh;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 混合组号
	 */
	@Column(name = "HHZH", length = 50)
	public String getHhzh() {
		return this.hhzh;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 混合组号
	 */
	public void setHhzh(String hhzh) {
		this.hhzh = hhzh;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 上机lane号
	 */
	@Column(name = "SJLANE", length = 50)
	public String getSjlane() {
		return this.sjlane;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 上机lane号
	 */
	public void setSjlane(String sjlane) {
		this.sjlane = sjlane;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 血液文库比例%
	 */
	@Column(name = "XYLR", length = 50)
	public String getXylr() {
		return this.xylr;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 血液文库比例%
	 */
	public void setXylr(String xylr) {
		this.xylr = xylr;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String ctdna文库比例%
	 */
	@Column(name = "CLR", length = 50)
	public String getClr() {
		return this.clr;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String ctdna文库比例%
	 */
	public void setClr(String clr) {
		this.clr = clr;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String ffpe文库比例%
	 */
	@Column(name = "FLR", length = 50)
	public String getFlr() {
		return this.flr;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String ffpe文库比例%
	 */
	public void setFlr(String flr) {
		this.flr = flr;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 新鲜组织文库比例%
	 */
	@Column(name = "XZLR", length = 50)
	public String getXzlr() {
		return this.xzlr;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 新鲜组织文库比例%
	 */
	public void setXzlr(String xzlr) {
		this.xzlr = xzlr;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 其他文库比例%
	 */
	@Column(name = "QTLR", length = 50)
	public String getQtlr() {
		return this.qtlr;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 其他文库比例%
	 */
	public void setQtlr(String qtlr) {
		this.qtlr = qtlr;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 通量
	 */
	@Column(name = "TL", length = 50)
	public String getTl() {
		return this.tl;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 通量
	 */
	public void setTl(String tl) {
		this.tl = tl;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 各组文库片段长度
	 */
	@Column(name = "WKPDCD", length = 50)
	public String getWkpdcd() {
		return this.wkpdcd;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 各组文库片段长度
	 */
	public void setWkpdcd(String wkpdcd) {
		this.wkpdcd = wkpdcd;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 原质量浓度（ng/ul）
	 */
	@Column(name = "YZLND", length = 50)
	public String getYzlnd() {
		return this.yzlnd;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 原质量浓度（ng/ul）
	 */
	public void setYzlnd(String yzlnd) {
		this.yzlnd = yzlnd;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 原摩尔浓度（nm）
	 */
	@Column(name = "YMEND", length = 50)
	public String getYmend() {
		return this.ymend;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 原摩尔浓度（nm）
	 */
	public void setYmend(String ymend) {
		this.ymend = ymend;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 下调或上调（%）
	 */
	@Column(name = "SOX", length = 50)
	public String getSox() {
		return this.sox;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 下调或上调（%）
	 */
	public void setSox(String sox) {
		this.sox = sox;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 取样（ul）
	 */
	@Column(name = "QY", length = 50)
	public String getQy() {
		return this.qy;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 取样（ul）
	 */
	public void setQy(String qy) {
		this.qy = qy;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 加水（ul）
	 */
	@Column(name = "JS", length = 50)
	public String getJs() {
		return this.js;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 加水（ul）
	 */
	public void setJs(String js) {
		this.js = js;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 干燥（min）
	 */
	@Column(name = "GZ", length = 50)
	public String getGz() {
		return this.gz;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 干燥（min）
	 */
	public void setGz(String gz) {
		this.gz = gz;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 调整后质量浓度（ng/ul）
	 */
	@Column(name = "TZZLND", length = 50)
	public String getTzzlnd() {
		return this.tzzlnd;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 调整后质量浓度（ng/ul）
	 */
	public void setTzzlnd(String tzzlnd) {
		this.tzzlnd = tzzlnd;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 调整后摩尔浓度（nm）
	 */
	@Column(name = "TZMEND", length = 50)
	public String getTzmend() {
		return this.tzmend;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 调整后摩尔浓度（nm）
	 */
	public void setTzmend(String tzmend) {
		this.tzmend = tzmend;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 下一步流向
	 */
	@Column(name = "NEXT_FLOW", length = 50)
	public String getNextFlow() {
		return this.nextFlow;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 下一步流向
	 */
	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得WkndTask
	 * 
	 * @return: WkndTask 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "WKND_TASK")
	public WkndTask getWkndTask() {
		return this.wkndTask;
	}

	/**
	 * 方法: 设置WkndTask
	 * 
	 * @param: WkndTask 相关主表
	 */
	public void setWkndTask(WkndTask wkndTask) {
		this.wkndTask = wkndTask;
	}
}