package com.biolims.experiment.sj.wknd.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.core.model.user.UserGroup;
import com.biolims.dao.EntityDao;
import com.biolims.system.template.model.Template;

/**
 * @Title: Model
 * @Description: 文库浓度调整
 * @author lims-platform
 * @date 2016-08-01 14:50:01
 * @version V1.0
 * 
 */
@Entity
@Table(name = "WKND_TASK")
@SuppressWarnings("serial")
public class WkndTask extends EntityDao<WkndTask> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 描述 */
	private String name;
	/** 实验时间 */
	private Date reciveDate;
	/** 模板 */
	private Template template;
	/** 实验组 */
	private UserGroup acceptUser;
	/** 下达人 */
	private User createUser;
	/** 下达时间 */
	private Date createDate;
	/** 审核人 */
	private User confirmUser;
	/** 完成时间 */
	private Date confirmDate;
	/** 状态 */
	private String state;
	/** 状态名称 */
	private String stateName;
	/** 备注 */
	private String note;

	// 实验员
	private User testUser;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TEST_USER")
	public User getTestUser() {
		return testUser;
	}

	public void setTestUser(User testUser) {
		this.testUser = testUser;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 100)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 实验时间
	 */
	@Column(name = "RECIVE_DATE", length = 50)
	public Date getReciveDate() {
		return this.reciveDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 实验时间
	 */
	public void setReciveDate(Date reciveDate) {
		this.reciveDate = reciveDate;
	}

	/**
	 * 方法: 取得Template
	 * 
	 * @return: Template 模板
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TEMPLATE")
	public Template getTemplate() {
		return this.template;
	}

	/**
	 * 方法: 设置Template
	 * 
	 * @param: Template 模板
	 */
	public void setTemplate(Template template) {
		this.template = template;
	}

	/**
	 * 方法: 取得UserGroup
	 * 
	 * @return: UserGroup 实验组
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "ACCEPT_USER")
	public UserGroup getAcceptUser() {
		return this.acceptUser;
	}

	/**
	 * 方法: 设置UserGroup
	 * 
	 * @param: UserGroup 实验组
	 */
	public void setAcceptUser(UserGroup acceptUser) {
		this.acceptUser = acceptUser;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 下达人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 下达人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 下达时间
	 */
	@Column(name = "CREATE_DATE", length = 50)
	public Date getCreateDate() {
		return this.createDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 下达时间
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 审核人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CONFIRM_USER")
	public User getConfirmUser() {
		return this.confirmUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 审核人
	 */
	public void setConfirmUser(User confirmUser) {
		this.confirmUser = confirmUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 完成时间
	 */
	@Column(name = "CONFIRM_DATE", length = 50)
	public Date getConfirmDate() {
		return this.confirmDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 完成时间
	 */
	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态名称
	 */
	@Column(name = "STATE_NAME", length = 50)
	public String getStateName() {
		return this.stateName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态名称
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 100)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}
}