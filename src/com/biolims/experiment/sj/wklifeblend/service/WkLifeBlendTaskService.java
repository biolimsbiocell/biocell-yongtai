package com.biolims.experiment.sj.wklifeblend.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.equipment.model.Instrument;
import com.biolims.experiment.pooling.dao.PoolingDao;
import com.biolims.experiment.qc.qpcrjd.model.QpcrjdTaskTemp;
import com.biolims.experiment.sj.mbenrichment.model.MbEnrichmentTemp;
import com.biolims.experiment.sj.wklifeblend.dao.WkLifeBlendTaskDao;
import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTask;
import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTaskCos;
import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTaskItem;
import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTaskReagent;
import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTaskResult;
import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTaskTemp;
import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTaskTemplate;
import com.biolims.experiment.wkLife.dao.WKLifeReceiveDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.storage.main.service.StorageService;
import com.biolims.storage.model.StorageOutItem;
import com.biolims.storage.out.service.StorageOutService;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.work.dao.WorkOrderDao;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class WkLifeBlendTaskService {
	@Resource
	private WkLifeBlendTaskDao wkLifeBlendTaskDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WKLifeReceiveDao wKLifeReceiveDao;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private DicSampleTypeDao dicSampleTypeDao;
	@Resource
	private WorkOrderDao workOrderDao;
	@Resource
	private PoolingDao poolingDao;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private StorageService storageService;
	@Resource
	private StorageOutService storageOutService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findWkBlendTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return wkLifeBlendTaskDao.selectWkBlendTaskList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(WkLifeBlendTask i) throws Exception {

		wkLifeBlendTaskDao.saveOrUpdate(i);

	}

	public WkLifeBlendTask get(String id) {
		WkLifeBlendTask wkBlendTask = commonDAO.get(WkLifeBlendTask.class, id);
		return wkBlendTask;
	}

	public Map<String, Object> findWkBlendTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = wkLifeBlendTaskDao.selectWkBlendTaskItemList(
				scId, startNum, limitNum, dir, sort);
		List<WkLifeBlendTaskItem> list = (List<WkLifeBlendTaskItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findWkBlendTaskTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = wkLifeBlendTaskDao
				.selectWkBlendTaskTemplateList(scId, startNum, limitNum, dir,
						sort);
		List<WkLifeBlendTaskTemplate> list = (List<WkLifeBlendTaskTemplate>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findWkBlendTaskReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = wkLifeBlendTaskDao
				.selectWkBlendTaskReagentList(scId, startNum, limitNum, dir,
						sort);
		List<WkLifeBlendTaskReagent> list = (List<WkLifeBlendTaskReagent>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findWkBlendTaskCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = wkLifeBlendTaskDao.selectWkBlendTaskCosList(
				scId, startNum, limitNum, dir, sort);
		List<WkLifeBlendTaskCos> list = (List<WkLifeBlendTaskCos>) result.get("list");
		return result;
	}

	public Map<String, Object> findWkBlendTaskCosListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		Map<String, Object> result = wkLifeBlendTaskDao
				.selectWkBlendTaskCosListByItemId(scId, startNum, limitNum,
						dir, sort, itemId);
		List<WkLifeBlendTaskCos> list = (List<WkLifeBlendTaskCos>) result.get("list");
		return result;
	}

	public Map<String, Object> findWkBlendTaskResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = wkLifeBlendTaskDao
				.selectWkBlendTaskResultList(scId, startNum, limitNum, dir,
						sort);
		List<WkLifeBlendTaskResult> list = (List<WkLifeBlendTaskResult>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findWkBlendTaskTempList(
			Map<String, String> mapForQuery, String scId, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = wkLifeBlendTaskDao.selectWkBlendTaskTempList(
				mapForQuery, scId, startNum, limitNum, dir, sort);
		List<WkLifeBlendTaskTemp> list = (List<WkLifeBlendTaskTemp>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWkBlendTaskItem(WkLifeBlendTask sc, String itemDataJson)
			throws Exception {
		List<WkLifeBlendTaskItem> saveItems = new ArrayList<WkLifeBlendTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			WkLifeBlendTaskItem scp = new WkLifeBlendTaskItem();
			// 将map信息读入实体类
			scp = (WkLifeBlendTaskItem) wkLifeBlendTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setWkblendTask(sc);

			saveItems.add(scp);

			// 改变左侧样本状态
			WkLifeBlendTaskTemp t = this.commonDAO.get(WkLifeBlendTaskTemp.class,
					scp.getTempId());
			if (t != null) {
				t.setState("2");
			}
		}
		wkLifeBlendTaskDao.saveOrUpdateAll(saveItems);

	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWkBlendTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			WkLifeBlendTaskItem scp = wkLifeBlendTaskDao.get(WkLifeBlendTaskItem.class, id);
			if (scp != null) {
				wkLifeBlendTaskDao.delete(scp);
				// 改变左侧样本状态
				WkLifeBlendTaskTemp t = this.commonDAO.get(WkLifeBlendTaskTemp.class,
						scp.getTempId());
				if (t != null) {
					t.setState("1");
				}
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWkBlendTaskTemplate(WkLifeBlendTask sc, String itemDataJson)
			throws Exception {
		List<WkLifeBlendTaskTemplate> saveItems = new ArrayList<WkLifeBlendTaskTemplate>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			WkLifeBlendTaskTemplate scp = new WkLifeBlendTaskTemplate();
			// 将map信息读入实体类
			scp = (WkLifeBlendTaskTemplate) wkLifeBlendTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setWkblendTask(sc);

			saveItems.add(scp);
		}
		wkLifeBlendTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWkBlendTaskTemplate(String[] ids) throws Exception {
		for (String id : ids) {
			WkLifeBlendTaskTemplate scp = wkLifeBlendTaskDao.get(
					WkLifeBlendTaskTemplate.class, id);
			wkLifeBlendTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Integer saveWkBlendTaskReagent(WkLifeBlendTask sc, String itemDataJson)
			throws Exception {
		List<WkLifeBlendTaskItem> item = this.wkLifeBlendTaskDao
				.setWkBlendTaskItemBysjfz(sc.getId());
		Integer re = 0;
		int n = item.size();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			WkLifeBlendTaskReagent scp = new WkLifeBlendTaskReagent();
			// 将map信息读入实体类
			scp = (WkLifeBlendTaskReagent) wkLifeBlendTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setWkblendTask(sc);
			scp.setSampleNum((double) n);
			// 用量 = 单个用量*样本数量
			if (scp != null && scp.getSampleNum() != null
					&& scp.getOneNum() != null) {
				scp.setNum(scp.getOneNum() * scp.getSampleNum());
			}
			if (scp.getSn() != null && !scp.getSn().equals("")) {
				StorageOutItem soi = storageOutService
						.findStorageOutItem(scp.getSn());
				if (soi != null) {
					if (scp.getCode().equals(soi.getStorage().getId())) {
						scp.setBatch(soi.getCode());
						scp.setExpireDate(soi.getExpireDate());
						re++;
					} else {
						scp.setSn("");
					}
				}
			}
			wkLifeBlendTaskDao.saveOrUpdate(scp);
		}
		return re;
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWkBlendTaskReagent(String[] ids) throws Exception {
		for (String id : ids) {
			WkLifeBlendTaskReagent scp = wkLifeBlendTaskDao.get(
					WkLifeBlendTaskReagent.class, id);
			wkLifeBlendTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Integer saveWkBlendTaskCos(WkLifeBlendTask sc, String itemDataJson)
			throws Exception {
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		Integer saveItem = 0;
		for (Map<String, Object> map : list) {
			WkLifeBlendTaskCos scp = new WkLifeBlendTaskCos();
			// 将map信息读入实体类
			scp = (WkLifeBlendTaskCos) wkLifeBlendTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			Instrument ins = null;
			if (scp.getCode() != null && !scp.equals("")) {
				ins = wkLifeBlendTaskDao.get(Instrument.class, scp.getCode());
			}
			if (ins != null
					&& ins.getType().getId().equals(scp.getType().getId())) {
				scp.setName(ins.getName());
				scp.setState(ins.getState().getName());
				saveItem++;
			} else {
				scp.setName("");
				scp.setCode("");
				scp.setState("");
			}
			scp.setWkblendTask(sc);
			wkLifeBlendTaskDao.saveOrUpdate(scp);

		}
		return saveItem;
	
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWkBlendTaskCos(String[] ids) throws Exception {
		for (String id : ids) {
			WkLifeBlendTaskCos scp = wkLifeBlendTaskDao.get(WkLifeBlendTaskCos.class, id);
			wkLifeBlendTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWkBlendTaskResult(WkLifeBlendTask sc, String itemDataJson)
			throws Exception {
		List<WkLifeBlendTaskResult> saveItems = new ArrayList<WkLifeBlendTaskResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			WkLifeBlendTaskResult scp = new WkLifeBlendTaskResult();
			// 将map信息读入实体类
			scp = (WkLifeBlendTaskResult) wkLifeBlendTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setWkblendTask(sc);
			if (scp.getHhzh() == null || "".equals(scp.getHhzh())) {
				String hhzh = codingRuleService.get("hhzh",
						"WkLifeBlendTaskResult", "LF", 000, 3, null);
				scp.setHhzh(hhzh);
			}
			wkLifeBlendTaskDao.saveOrUpdate(scp);
		}

		// // 设置保留三维小数
		// DecimalFormat df = new DecimalFormat("######.000");
		//
		// List<WkBlendTaskResult> list2 = this.wkBlendTaskDao.setResultList(sc
		// .getId());
		// for (WkBlendTaskResult q : list2) {
		// // p.set("lltj",(wknd[i]/mbnd[i]).toFixed(3));
		// // p.set("bsgz",(lltj[i]-wkpdcd[i]).toFixed(3));
		// // p.set("tzhwkzl",(tzhwknd[i]*tzhwktj[i]).toFixed(3));
		// // if (q.getWknd() != null || !"".equals(q.getWknd())) {
		// // Double d = Double.parseDouble(q.getWknd())
		// // / Double.parseDouble(q.getMbnd());
		// // q.setLltj(Double.valueOf(df.format(d)).toString());
		// // Double a = Double.parseDouble(q.getLltj())
		// // - Double.parseDouble(q.getWkpdcd());
		// // q.setBsgz(Double.valueOf(df.format(a)).toString());
		// // // 调整后文库总量
		// // Double b = Double.parseDouble(q.getTzhwknd())
		// // * Double.parseDouble(q.getTzhwktj());
		// // q.setTzhwkzl(Double.valueOf(df.format(b)).toString());
		// // }
		// }
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWkBlendTaskResult(String[] ids) throws Exception {
		for (String id : ids) {
			WkLifeBlendTaskResult scp = wkLifeBlendTaskDao.get(WkLifeBlendTaskResult.class,
					id);
			wkLifeBlendTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWkBlendTaskTemp(WkLifeBlendTask sc, String itemDataJson)
			throws Exception {
		List<WkLifeBlendTaskTemp> saveItems = new ArrayList<WkLifeBlendTaskTemp>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			WkLifeBlendTaskTemp scp = new WkLifeBlendTaskTemp();
			// 将map信息读入实体类
			scp = (WkLifeBlendTaskTemp) wkLifeBlendTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			// scp.setWkblendTask(sc);

			saveItems.add(scp);
		}
		wkLifeBlendTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWkBlendTaskTemp(String[] ids) throws Exception {
		for (String id : ids) {
			WkLifeBlendTaskTemp scp = wkLifeBlendTaskDao.get(WkLifeBlendTaskTemp.class, id);
			wkLifeBlendTaskDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Map<String, Object> save(WkLifeBlendTask sc, Map jsonMap) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer re = 0;
		Integer in = 0;
		if (sc != null) {
			wkLifeBlendTaskDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("wkBlendTaskItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveWkBlendTaskItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("wkBlendTaskTemplate");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveWkBlendTaskTemplate(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("wkBlendTaskReagent");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				re =saveWkBlendTaskReagent(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("wkBlendTaskCos");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				in =saveWkBlendTaskCos(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("wkBlendTaskResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveWkBlendTaskResult(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("wkBlendTaskTemp");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveWkBlendTaskTemp(sc, jsonStr);
			}
		}
		map.put("equip", in);
		map.put("reagent", re);
		return map;
	}

	// 审核完成
//	@WriteOperLogTable
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void changeState(String applicationTypeActionId, String id)
//			throws Exception {
//		WkLifeBlendTask sct = wkLifeBlendTaskDao.get(WkLifeBlendTask.class, id);
//		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
//		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
//		User user = (User) ServletActionContext.getRequest().getSession()
//				.getAttribute(SystemConstants.USER_SESSION_KEY);
//		sct.setConfirmUser(user);
//		wkLifeBlendTaskDao.update(sct);
//
//		// 将批次信息反馈到模板中
//		List<WkLifeBlendTaskReagent> list1 = wkLifeBlendTaskDao.setReagentList(id);
//		for (WkLifeBlendTaskReagent dt : list1) {
//			String bat1 = dt.getBatch(); // 实验中试剂的批次
//			String drid = dt.getTReagent(); // 实验中保存的试剂ID
//			List<ReagentItem> list2 = templateDao.setReagentListById(drid);
//			for (ReagentItem ri : list2) {
//				if (bat1 != null) {
//					ri.setBatch(bat1);
//				}
//			}
//			// 改变库存主数据试剂数量
//			this.storageService.UpdateStorageAndItemNum(dt.getCode(),
//					dt.getBatch(), dt.getNum());
//		}
//
//		// 获取结果表样本信息
//		List<WkLifeBlendTaskResult> list = this.wkLifeBlendTaskDao
//				.setWkBlendResultById(id);
//		for (WkLifeBlendTaskResult scp : list) {
//			if (scp != null) {
//				if (scp.getNextFlow() != null) {
//					if (scp.getNextFlowId().equals("0009")) {// 样本入库
//						SampleInItemTemp st = new SampleInItemTemp();
//						st.setCode(scp.getSjfz());
//						st.setSampleCode(scp.getHhzh());
//						st.setInfoFrom("WkLifeBlendTaskResult");
//						st.setState("1");
//						st.setConcentration(Double.parseDouble(scp.getTzhwknd()));
//						st.setVolume(Double.parseDouble(scp.getTzhwktj()));
//						st.setSumTotal(Double.parseDouble(scp.getTzhwkzl()));
//						this.wkLifeBlendTaskDao.saveOrUpdate(st);
//					} else if (scp.getNextFlowId().equals("0027")) {// 文库定量QPCR
//						QpcrjdTaskTemp q = new QpcrjdTaskTemp();
//						sampleInputService.copy(q, scp);
//						q.setSampleCode(scp.getSjfz());
//						q.setWknd(scp.getTzhwknd());
//						q.setTl(scp.getTzhwktj());
//						q.setSampleType(scp.getTzhwkzl());
//						q.setProductId(scp.getCxlx());
//						q.setProductName(scp.getCxpt());
//						q.setName(scp.getCxdc());
//						q.setState("1");
//						this.wkLifeBlendTaskDao.saveOrUpdate(q);
//					} else if (scp.getNextFlowId().equals("0023")) {// 上机
////						SequencingTaskTemp q = new SequencingTaskTemp();
////						sampleInputService.copy(q, scp);
////						q.setPoolingCode(scp.getSjfz());
////						q.setPoolingId(scp.getHhzh());
////						q.setSequenceType(scp.getCxlx());
////						q.setSequencingPlatform(scp.getCxpt());
////						q.setSequencingReadLong(scp.getCxdc());
////						q.setState("1");
////						this.wkLifeBlendTaskDao.saveOrUpdate(q);
//					}else if (scp.getNextFlowId().equals("0068")) {// Life模板富集待实验
//						MbEnrichmentTemp q = new MbEnrichmentTemp();
//						sampleInputService.copy(q, scp);
//						q.setState("1");
//						this.wkLifeBlendTaskDao.saveOrUpdate(q);
//					} else {
//						// 得到下一步流向的相关表单
//						List<NextFlow> list_nextFlow = nextFlowDao
//								.seletNextFlowById(scp.getNextFlowId());
//						for (NextFlow n : list_nextFlow) {
//							Object o = Class.forName(
//									n.getApplicationTypeTable().getClassPath())
//									.newInstance();
//							scp.setState("1");
//							sampleInputService.copy(o, scp);
//						}
//					}
//				}
//				DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//				// 根据上机分组号查询
//				List<WkLifeBlendTaskItem> listItem1 = this.wkLifeBlendTaskDao
//						.setWkBlendTaskItemBysjfz(id);
//				for (WkLifeBlendTaskItem w : listItem1) {
//					// 根据富集文库查询明细样本
//					List<PoolingTaskItem> listPoolingTaskItem = this.poolingDao
//							.getItemListByfjwk(w.getFjwk());
//					for (PoolingTaskItem p : listPoolingTaskItem) {
//						sampleStateService
//								.saveSampleState(
//										p.getCode(),
//										p.getSampleCode(),
//										p.getProductId(),
//										p.getProductName(),
//										"",
//										format.format(sct.getCreateDate()),
//										format.format(new Date()),
//										"WkBlendTask",
//										"文库混合",
//										(User) ServletActionContext
//												.getRequest()
//												.getSession()
//												.getAttribute(
//														SystemConstants.USER_SESSION_KEY),
//										id, scp.getNextFlow(), null, null,
//										null, null, null, null, null, null,
//										null);
//					}
//				}
//			}
//		}
//
//	}

	public List<WkLifeBlendTaskItem> findDnaItemList(String scId) throws Exception {
		List<WkLifeBlendTaskItem> list = wkLifeBlendTaskDao.setItemList(scId);
		return list;
	}

	/**
	 * 明细入库
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void rukuWkBlendTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			WkLifeBlendTaskItem scp = wkLifeBlendTaskDao.get(WkLifeBlendTaskItem.class, id);
			SampleInItemTemp st = new SampleInItemTemp();
			st.setCode(scp.getFjwk());
			st.setSampleCode(scp.getSampleCode());
			st.setInfoFrom("WkLifeBlendTaskItem");
			st.setState("1");
			this.wkLifeBlendTaskDao.saveOrUpdate(st);
			scp.setState("1");
		}
	}

	/**
	 * 根据选中的步骤查询相关的试剂明细
	 * 
	 * @param id
	 * @param code
	 * @return
	 */
	public List<Map<String, String>> setReagent(String id, String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = wkLifeBlendTaskDao.setReagent(id, code);
		List<WkLifeBlendTaskReagent> list = (List<WkLifeBlendTaskReagent>) result
				.get("list");
		if (list != null && list.size() > 0) {
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			for (WkLifeBlendTaskReagent ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("code", ti.getCode());
				map.put("name", ti.getName());
				map.put("batch", ti.getBatch());
				map.put("isGood", ti.getIsGood());
				map.put("note", ti.getNote());
				map.put("tReagent", ti.getReagentCode());
				if (ti.getOneNum() != null) {
					map.put("oneNum", ti.getOneNum().toString());
				} else {
					map.put("oneNum", "");
				}
				if (ti.getNum() != null) {
					map.put("sampleNum", ti.getSampleNum().toString());
				} else {
					map.put("sampleNum", "");
				}
				if (ti.getNum() != null) {
					map.put("num", ti.getNum().toString());
				} else {
					map.put("num", "");
				}
				if(ti.getExpireDate()!=null){
					map.put("expireDate", sdf.format(ti.getExpireDate()));
				}
				map.put("sn", ti.getSn());
				map.put("itemId", ti.getItemId());
				map.put("tId", ti.getWkblendTask().getId());
				map.put("tName", ti.getWkblendTask().getName());
				mapList.add(map);
			}
		}
		return mapList;
	}

	/**
	 * 根据选中的步骤查询相关的设备明细
	 * 
	 * @param id
	 * @param code
	 * @return
	 */
	public List<Map<String, String>> setCos(String id, String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = wkLifeBlendTaskDao.setCos(id, code);
		List<WkLifeBlendTaskCos> list = (List<WkLifeBlendTaskCos>) result.get("list");
		if (list != null && list.size() > 0) {
			for (WkLifeBlendTaskCos ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("code", ti.getCode());
				map.put("name", ti.getName());
				map.put("isGood", ti.getIsGood());
				if (ti.getTemperature() != null) {
					map.put("temperature", ti.getTemperature().toString());
				} else {
					map.put("temperature", "");
				}
				if (ti.getSpeed() != null) {
					map.put("speed", ti.getSpeed().toString());
				} else {
					map.put("speed", "");
				}
				if (ti.getTime() != null) {
					map.put("time", ti.getTime().toString());
				} else {
					map.put("time", "");
				}
				if (ti.getType() != null) {
					map.put("typeId", ti.getType().getId());
					map.put("typeName", ti.getType().getName());
				} else {
					map.put("typeId", "");
					map.put("typeName", "");
				}
				map.put("state", ti.getState());
				map.put("itemId", ti.getItemId());
				map.put("note", ti.getNote());
				map.put("tCos", ti.getTCos());
				map.put("tId", ti.getWkblendTask().getId());
				map.put("tName", ti.getWkblendTask().getName());
				mapList.add(map);
			}
		}
		return mapList;
	}
}