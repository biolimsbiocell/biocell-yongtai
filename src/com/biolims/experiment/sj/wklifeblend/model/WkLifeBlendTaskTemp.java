package com.biolims.experiment.sj.wklifeblend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.storage.model.Storage;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;

/**
 * @Title: Model
 * @Description: 文库混合临时表
 * @author lims-platform
 * @date 2016-08-01 14:26:00
 * @version V1.0
 * 
 */
@Entity
@Table(name = "WK_LIFE_BLEND_TASK_TEMP")
@SuppressWarnings("serial")
public class WkLifeBlendTaskTemp extends EntityDao<WkLifeBlendTaskTemp> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 富集文库 */
	private String fjwk;
	/** 上机分组 */
	private String sjfz;
	/** 预混合组号 */
	private String yhhzh;
	/** 文库类型 */
	private String wklx;
	/** 探针 */
	private Storage probeCode;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "PROBE_CODE")
	public Storage getProbeCode() {
		return probeCode;
	}

	public void setProbeCode(Storage probeCode) {
		this.probeCode = probeCode;
	}

	/** 科研编号 */
	private String kybh;
	/** 测序类型 */
	private String cxlx;
	/** 测序平台 */
	private String cxpt;
	/** 测序读长 */
	private String cxdc;
	/** 通量 */
	private String tl;
	/** 临时表id */
	private String tempid;
	// 状态
	private String state;
	/** 血液文库比例% */
	private String xylr;
	/** ctdna文库比例% */
	private String clr;
	/** ffpe文库比例% */
	private String flr;
	/** 新鲜组织文库比例% */
	private String xzlr;
	/** 其他文库比例% */
	private String qtlr;

	/** 通量比例 */
	private String tlbl;
	/** 扩增效率 */
	private String kzxl;
	/** e^-δct */
	private String ect;
	/** qpcr比例 */
	private String qpcrbl;
	/** 长度比例 */
	private String blcd;
	// 浓度
	private String concentration;
	// 体积
	private String volume;
	// 总量
	private String sumTotal;
	// 科技服务
	private TechJkServiceTask techJkServiceTask;
	// 是否质控品
	private String isZkp;
	// 科技服务明细
	private TechJkServiceTaskItem tjItem;
	// index
	private String indexa;
	// 文库片段大小
	private String insertSize;
	// 原始样本编号
	private String sampleCode;

	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	public String getInsertSize() {
		return insertSize;
	}

	public void setInsertSize(String insertSize) {
		this.insertSize = insertSize;
	}

	public String getIndexa() {
		return indexa;
	}

	public void setIndexa(String indexa) {
		this.indexa = indexa;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "TJ_ITEM")
	public TechJkServiceTaskItem getTjItem() {
		return tjItem;
	}

	public void setTjItem(TechJkServiceTaskItem tjItem) {
		this.tjItem = tjItem;
	}

	public String getIsZkp() {
		return isZkp;
	}

	public void setIsZkp(String isZkp) {
		this.isZkp = isZkp;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "TECH_JK_SERVICE_TASK")
	public TechJkServiceTask getTechJkServiceTask() {
		return techJkServiceTask;
	}

	public void setTechJkServiceTask(TechJkServiceTask techJkServiceTask) {
		this.techJkServiceTask = techJkServiceTask;
	}

	public String getConcentration() {
		return concentration;
	}

	public void setConcentration(String concentration) {
		this.concentration = concentration;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public String getSumTotal() {
		return sumTotal;
	}

	public void setSumTotal(String sumTotal) {
		this.sumTotal = sumTotal;
	}

	public String getXylr() {
		return xylr;
	}

	public void setXylr(String xylr) {
		this.xylr = xylr;
	}

	public String getClr() {
		return clr;
	}

	public void setClr(String clr) {
		this.clr = clr;
	}

	public String getFlr() {
		return flr;
	}

	public void setFlr(String flr) {
		this.flr = flr;
	}

	public String getXzlr() {
		return xzlr;
	}

	public void setXzlr(String xzlr) {
		this.xzlr = xzlr;
	}

	public String getQtlr() {
		return qtlr;
	}

	public void setQtlr(String qtlr) {
		this.qtlr = qtlr;
	}

	public String getTlbl() {
		return tlbl;
	}

	public void setTlbl(String tlbl) {
		this.tlbl = tlbl;
	}

	public String getKzxl() {
		return kzxl;
	}

	public void setKzxl(String kzxl) {
		this.kzxl = kzxl;
	}

	public String getEct() {
		return ect;
	}

	public void setEct(String ect) {
		this.ect = ect;
	}

	public String getQpcrbl() {
		return qpcrbl;
	}

	public void setQpcrbl(String qpcrbl) {
		this.qpcrbl = qpcrbl;
	}

	public String getBlcd() {
		return blcd;
	}

	public void setBlcd(String blcd) {
		this.blcd = blcd;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 富集文库
	 */
	@Column(name = "FJWK", length = 100)
	public String getFjwk() {
		return this.fjwk;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 富集文库
	 */
	public void setFjwk(String fjwk) {
		this.fjwk = fjwk;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 上机分组
	 */
	@Column(name = "SJFZ", length = 50)
	public String getSjfz() {
		return this.sjfz;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 上机分组
	 */
	public void setSjfz(String sjfz) {
		this.sjfz = sjfz;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 预混合组号
	 */
	@Column(name = "YHHZH", length = 50)
	public String getYhhzh() {
		return this.yhhzh;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 预混合组号
	 */
	public void setYhhzh(String yhhzh) {
		this.yhhzh = yhhzh;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 文库类型
	 */
	@Column(name = "WKLX", length = 50)
	public String getWklx() {
		return this.wklx;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 文库类型
	 */
	public void setWklx(String wklx) {
		this.wklx = wklx;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 科研编号
	 */
	@Column(name = "KYBH", length = 50)
	public String getKybh() {
		return this.kybh;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 科研编号
	 */
	public void setKybh(String kybh) {
		this.kybh = kybh;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 测序类型
	 */
	@Column(name = "CXLX", length = 50)
	public String getCxlx() {
		return this.cxlx;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 测序类型
	 */
	public void setCxlx(String cxlx) {
		this.cxlx = cxlx;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 测序平台
	 */
	@Column(name = "CXPT", length = 50)
	public String getCxpt() {
		return this.cxpt;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 测序平台
	 */
	public void setCxpt(String cxpt) {
		this.cxpt = cxpt;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 测序读长
	 */
	@Column(name = "CXDC", length = 50)
	public String getCxdc() {
		return this.cxdc;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 测序读长
	 */
	public void setCxdc(String cxdc) {
		this.cxdc = cxdc;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 通量
	 */
	@Column(name = "TL", length = 50)
	public String getTl() {
		return this.tl;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 通量
	 */
	public void setTl(String tl) {
		this.tl = tl;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 临时表id
	 */
	@Column(name = "TEMPID", length = 50)
	public String getTempid() {
		return this.tempid;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 临时表id
	 */
	public void setTempid(String tempid) {
		this.tempid = tempid;
	}
}