package com.biolims.experiment.sj.wklifeblend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;


/**
 * @Title: Model
 * @Description: 文库混合明细
 * @author lims-platform
 * @date 2016-08-01 14:25:56
 * @version V1.0
 * 
 */
@Entity
@Table(name = "WK_LIFE_BLEND_TASK_ITEM")
@SuppressWarnings("serial")
public class WkLifeBlendTaskItem extends EntityDao<WkLifeBlendTaskItem> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 富集文库 */
	private String fjwk;
	/** 上机分组 */
	private String sjfz;
	/** 预混合组号 */
	private String yhhzh;
	/** 混合组号 */
	private String hhzh;
	/** 血液文库比例% */
	private String xylr;
	/** ctdna文库比例% */
	private String clr;
	/** ffpe文库比例% */
	private String flr;
	/** 新鲜组织文库比例% */
	private String xzlr;
	/** 其他文库比例% */
	private String qtlr;
	/** 通量 */
	private String tl;
	/** 通量比例 */
	private String tlbl;
	/** 扩增效率 */
	private String kzxl;
	/** e^-δct */
	private String ect;
	/** qpcr比例 */
	private String qpcrbl;
	/** 长度比例 */
	private String blcd;
	/** 文库混合进入量ng */
	private String wkhhl;
	/** 文库混合体积 */
	private String wkhhtl;
	/** 相关主表 */
	private WkLifeBlendTask wkblendTask;
	/** 混合后理论浓度（ng/ul） */
	private String hhhnd;
	/** 各组文库片段长度(文库长度) */
	private String wkpdcd;
	/** 测序类型 */
	private String cxlx;
	/** 测序平台 */
	private String cxpt;
	/** 测序读长 */
	private String cxdc;
	// 临时表id
	private String tempId;
	// 状态
	private String state;
	// 步骤编号
	private String stepNum;
	// 序号
	private String orderNumber;
	// 浓度
	private String concentration;
	// 体积
	private String volume;
	// 总量
	private String sumTotal;
	// 混合比例
	private String hhbl;
	// 科技服务
	private TechJkServiceTask techJkServiceTask;
	// 是否质控品
	private String isZkp;
	// 样本名称
	private String sampleName;
	// 插入片段(文库片段大小)
	private String insertSize;
	// 通量比例和
	private String sumFlux;
	// 1比例的量
	private String ratioOne;
	// 按比例所需量
	private String ratioNeed;
	// 物种
	private String species;
	// 科技服务明细
	private TechJkServiceTaskItem tjItem;
	// 文库质量浓度
	private Double wkmConcentration;
	// 文库摩尔浓度
	private Double wkMoleConcentration;
	// 取样体积
	private Double sampleVolume;
	// 稀释终浓度
	private Double xszConcentration;
	// 补加RSB体积
	private Double addRsbVolume;
	// 稀释后总体积
	private Double xshSumVolume;

	/** 单位组 */
	private DicType unitGroup;
	// index
	private String indexa;
	// 原始样本编号
	private String sampleCode;

	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	public String getIndexa() {
		return indexa;
	}

	public void setIndexa(String indexa) {
		this.indexa = indexa;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "UNIT_GROUP")
	public DicType getUnitGroup() {
		return unitGroup;
	}

	public void setUnitGroup(DicType unitGroup) {
		this.unitGroup = unitGroup;
	}

	public Double getWkmConcentration() {
		return wkmConcentration;
	}

	public void setWkmConcentration(Double wkmConcentration) {
		this.wkmConcentration = wkmConcentration;
	}

	public Double getWkMoleConcentration() {
		return wkMoleConcentration;
	}

	public void setWkMoleConcentration(Double wkMoleConcentration) {
		this.wkMoleConcentration = wkMoleConcentration;
	}

	public Double getSampleVolume() {
		return sampleVolume;
	}

	public void setSampleVolume(Double sampleVolume) {
		this.sampleVolume = sampleVolume;
	}

	public Double getXszConcentration() {
		return xszConcentration;
	}

	public void setXszConcentration(Double xszConcentration) {
		this.xszConcentration = xszConcentration;
	}

	public Double getAddRsbVolume() {
		return addRsbVolume;
	}

	public void setAddRsbVolume(Double addRsbVolume) {
		this.addRsbVolume = addRsbVolume;
	}

	public Double getXshSumVolume() {
		return xshSumVolume;
	}

	public void setXshSumVolume(Double xshSumVolume) {
		this.xshSumVolume = xshSumVolume;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "TJ_ITEM")
	public TechJkServiceTaskItem getTjItem() {
		return tjItem;
	}

	public void setTjItem(TechJkServiceTaskItem tjItem) {
		this.tjItem = tjItem;
	}

	public String getSampleName() {
		return sampleName;
	}

	public void setSampleName(String sampleName) {
		this.sampleName = sampleName;
	}

	public String getInsertSize() {
		return insertSize;
	}

	public void setInsertSize(String insertSize) {
		this.insertSize = insertSize;
	}

	public String getSumFlux() {
		return sumFlux;
	}

	public void setSumFlux(String sumFlux) {
		this.sumFlux = sumFlux;
	}

	public String getRatioOne() {
		return ratioOne;
	}

	public void setRatioOne(String ratioOne) {
		this.ratioOne = ratioOne;
	}

	public String getRatioNeed() {
		return ratioNeed;
	}

	public void setRatioNeed(String ratioNeed) {
		this.ratioNeed = ratioNeed;
	}

	public String getSpecies() {
		return species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public String getIsZkp() {
		return isZkp;
	}

	public void setIsZkp(String isZkp) {
		this.isZkp = isZkp;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "TECH_JK_SERVICE_TASK")
	public TechJkServiceTask getTechJkServiceTask() {
		return techJkServiceTask;
	}

	public void setTechJkServiceTask(TechJkServiceTask techJkServiceTask) {
		this.techJkServiceTask = techJkServiceTask;
	}

	public String getConcentration() {
		return concentration;
	}

	public void setConcentration(String concentration) {
		this.concentration = concentration;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public String getSumTotal() {
		return sumTotal;
	}

	public void setSumTotal(String sumTotal) {
		this.sumTotal = sumTotal;
	}

	public String getHhbl() {
		return hhbl;
	}

	public void setHhbl(String hhbl) {
		this.hhbl = hhbl;
	}

	public String getStepNum() {
		return stepNum;
	}

	public void setStepNum(String stepNum) {
		this.stepNum = stepNum;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 富集文库
	 */
	@Column(name = "FJWK", length = 100)
	public String getFjwk() {
		return this.fjwk;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 富集文库
	 */
	public void setFjwk(String fjwk) {
		this.fjwk = fjwk;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 上机分组
	 */
	@Column(name = "SJFZ", length = 50)
	public String getSjfz() {
		return this.sjfz;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 上机分组
	 */
	public void setSjfz(String sjfz) {
		this.sjfz = sjfz;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 预混合组号
	 */
	@Column(name = "YHHZH", length = 50)
	public String getYhhzh() {
		return this.yhhzh;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 预混合组号
	 */
	public void setYhhzh(String yhhzh) {
		this.yhhzh = yhhzh;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 混合组号
	 */
	@Column(name = "HHZH", length = 50)
	public String getHhzh() {
		return this.hhzh;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 混合组号
	 */
	public void setHhzh(String hhzh) {
		this.hhzh = hhzh;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 血液文库比例%
	 */
	@Column(name = "XYLR", length = 50)
	public String getXylr() {
		return this.xylr;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 血液文库比例%
	 */
	public void setXylr(String xylr) {
		this.xylr = xylr;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String ctdna文库比例%
	 */
	@Column(name = "CLR", length = 50)
	public String getClr() {
		return this.clr;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String ctdna文库比例%
	 */
	public void setClr(String clr) {
		this.clr = clr;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String ffpe文库比例%
	 */
	@Column(name = "FLR", length = 50)
	public String getFlr() {
		return this.flr;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String ffpe文库比例%
	 */
	public void setFlr(String flr) {
		this.flr = flr;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 新鲜组织文库比例%
	 */
	@Column(name = "XZLR", length = 50)
	public String getXzlr() {
		return this.xzlr;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 新鲜组织文库比例%
	 */
	public void setXzlr(String xzlr) {
		this.xzlr = xzlr;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 其他文库比例%
	 */
	@Column(name = "QTLR", length = 50)
	public String getQtlr() {
		return this.qtlr;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 其他文库比例%
	 */
	public void setQtlr(String qtlr) {
		this.qtlr = qtlr;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 通量
	 */
	@Column(name = "TL", length = 50)
	public String getTl() {
		return this.tl;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 通量
	 */
	public void setTl(String tl) {
		this.tl = tl;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 通量比例
	 */
	@Column(name = "TLBL", length = 50)
	public String getTlbl() {
		return this.tlbl;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 通量比例
	 */
	public void setTlbl(String tlbl) {
		this.tlbl = tlbl;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 扩增效率
	 */
	@Column(name = "KZXL", length = 50)
	public String getKzxl() {
		return this.kzxl;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 扩增效率
	 */
	public void setKzxl(String kzxl) {
		this.kzxl = kzxl;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String e^-δct
	 */
	@Column(name = "ECT", length = 50)
	public String getEct() {
		return this.ect;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String e^-δct
	 */
	public void setEct(String ect) {
		this.ect = ect;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String qpcr比例
	 */
	@Column(name = "QPCRBL", length = 50)
	public String getQpcrbl() {
		return this.qpcrbl;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String qpcr比例
	 */
	public void setQpcrbl(String qpcrbl) {
		this.qpcrbl = qpcrbl;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 长度比例
	 */
	@Column(name = "BLCD", length = 50)
	public String getBlcd() {
		return this.blcd;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 长度比例
	 */
	public void setBlcd(String blcd) {
		this.blcd = blcd;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 文库混合进入量ng
	 */
	@Column(name = "WKHHL", length = 50)
	public String getWkhhl() {
		return this.wkhhl;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 文库混合进入量ng
	 */
	public void setWkhhl(String wkhhl) {
		this.wkhhl = wkhhl;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 文库混合体积
	 */
	@Column(name = "WKHHTL", length = 50)
	public String getWkhhtl() {
		return this.wkhhtl;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 文库混合体积
	 */
	public void setWkhhtl(String wkhhtl) {
		this.wkhhtl = wkhhtl;
	}

	/**
	 * 方法: 取得WkBlendTask
	 * 
	 * @return: WkBlendTask 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "WKBLEND_TASK")
	public WkLifeBlendTask getWkblendTask() {
		return this.wkblendTask;
	}

	/**
	 * 方法: 设置WkBlendTask
	 * 
	 * @param: WkBlendTask 相关主表
	 */
	public void setWkblendTask(WkLifeBlendTask wkblendTask) {
		this.wkblendTask = wkblendTask;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 混合后理论浓度（ng/ul）
	 */
	@Column(name = "HHHND", length = 50)
	public String getHhhnd() {
		return this.hhhnd;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 混合后理论浓度（ng/ul）
	 */
	public void setHhhnd(String hhhnd) {
		this.hhhnd = hhhnd;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 各组文库片段长度
	 */
	@Column(name = "WKPDCD", length = 50)
	public String getWkpdcd() {
		return this.wkpdcd;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 各组文库片段长度
	 */
	public void setWkpdcd(String wkpdcd) {
		this.wkpdcd = wkpdcd;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 测序类型
	 */
	@Column(name = "CXLX", length = 50)
	public String getCxlx() {
		return this.cxlx;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 测序类型
	 */
	public void setCxlx(String cxlx) {
		this.cxlx = cxlx;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 测序平台
	 */
	@Column(name = "CXPT", length = 50)
	public String getCxpt() {
		return this.cxpt;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 测序平台
	 */
	public void setCxpt(String cxpt) {
		this.cxpt = cxpt;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 测序读长
	 */
	@Column(name = "CXDC", length = 50)
	public String getCxdc() {
		return this.cxdc;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 测序读长
	 */
	public void setCxdc(String cxdc) {
		this.cxdc = cxdc;
	}
}