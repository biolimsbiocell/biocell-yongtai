package com.biolims.experiment.sj.wklifeblend.model;


import java.lang.Integer;
import java.lang.String;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;

/**   
 * @Title: Model
 * @Description: 执行步骤
 * @author lims-platform
 * @date 2016-08-01 14:25:57
 * @version V1.0   
 *
 */
@Entity
@Table(name = "WK_LIFE_BLEND_TASK_TEMPLATE")
@SuppressWarnings("serial")
public class WkLifeBlendTaskTemplate extends EntityDao<WkLifeBlendTaskTemplate> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
	/**实验员*/
	private String testUserId;
	private String testUserName;
	/**模板步骤编id*/
	private String tItem;
	/**步骤编号*/
	private Integer code;
	/**步骤名称*/
	private String stepName;
	/**开始时间*/
	private String startTime;
	/**结束时间*/
	private String endTime;
	/**状态*/
	private String state;
	/**备注*/
	private String note;
	/**关联样本*/
	private String sampleCodes;
	/**相关主表*/
	private WkLifeBlendTask wkblendTask;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}

	/**
	 * @return the testUserId
	 */
	public String getTestUserId() {
		return testUserId;
	}
	/**
	 * @param testUserId the testUserId to set
	 */
	public void setTestUserId(String testUserId) {
		this.testUserId = testUserId;
	}
	/**
	 * @return the testUserName
	 */
	public String getTestUserName() {
		return testUserName;
	}
	/**
	 * @param testUserName the testUserName to set
	 */
	public void setTestUserName(String testUserName) {
		this.testUserName = testUserName;
	}
	/**
	 *方法: 取得String
	 *@return: String  模板步骤编id
	 */
	@Column(name ="T_ITEM", length = 50)
	public String getTItem(){
		return this.tItem;
	}
	/**
	 *方法: 设置String
	 *@param: String  模板步骤编id
	 */
	public void setTItem(String tItem){
		this.tItem = tItem;
	}
	/**
	 *方法: 取得String
	 *@return: String  步骤编号
	 */

	/**
	 *方法: 取得String
	 *@return: String  步骤名称
	 */
	@Column(name ="STEP_NAME", length = 50)
	public String getStepName(){
		return this.stepName;
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	/**
	 *方法: 设置String
	 *@param: String  步骤名称
	 */
	public void setStepName(String stepName){
		this.stepName = stepName;
	}
	/**
	 *方法: 取得String
	 *@return: String  开始时间
	 */
	@Column(name ="START_TIME", length = 50)
	public String getStartTime(){
		return this.startTime;
	}
	/**
	 *方法: 设置String
	 *@param: String  开始时间
	 */
	public void setStartTime(String startTime){
		this.startTime = startTime;
	}
	/**
	 *方法: 取得String
	 *@return: String  结束时间
	 */
	@Column(name ="END_TIME", length = 50)
	public String getEndTime(){
		return this.endTime;
	}
	/**
	 *方法: 设置String
	 *@param: String  结束时间
	 */
	public void setEndTime(String endTime){
		this.endTime = endTime;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得String
	 *@return: String  关联样本
	 */
	@Column(name ="SAMPLE_CODES", length = 50)
	public String getSampleCodes(){
		return this.sampleCodes;
	}
	/**
	 *方法: 设置String
	 *@param: String  关联样本
	 */
	public void setSampleCodes(String sampleCodes){
		this.sampleCodes = sampleCodes;
	}
	/**
	 *方法: 取得WkBlendTask
	 *@return: WkBlendTask  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "WKBLEND_TASK")
	public WkLifeBlendTask getWkblendTask(){
		return this.wkblendTask;
	}
	/**
	 *方法: 设置WkBlendTask
	 *@param: WkBlendTask  相关主表
	 */
	public void setWkblendTask(WkLifeBlendTask wkblendTask){
		this.wkblendTask = wkblendTask;
	}
}