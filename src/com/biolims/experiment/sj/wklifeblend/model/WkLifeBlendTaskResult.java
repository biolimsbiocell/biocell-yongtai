package com.biolims.experiment.sj.wklifeblend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;

/**
 * @Title: Model
 * @Description: 文库混合结果
 * @author lims-platform
 * @date 2016-08-01 14:25:59
 * @version V1.0
 * 
 */
@Entity
@Table(name = "WK_LIFE_BLEND_TASK_RESULT")
@SuppressWarnings("serial")
public class WkLifeBlendTaskResult extends EntityDao<WkLifeBlendTaskResult> implements
		java.io.Serializable {
	/** id */
	private String id;
	/** 富集文库 */
	private String fjwk;
	/** 上机分组 */
	private String sjfz;
	/** 预混合组号 */
	private String yhhzh;
	/** 混合组号 */
	private String hhzh;
	/** 血液文库比例% */
	private String xylr;
	/** ctdna文库比例% */
	private String clr;
	/** ffpe文库比例% */
	private String flr;
	/** 新鲜组织文库比例% */
	private String xzlr;
	/** 其他文库比例% */
	private String qtlr;
	/** 通量 */
	private String tl;
	/** 文库浓度（ng/ul） */
	private String wknd;
	/** 各组文库片段长度 */
	private String wkpdcd;
	/** 测序类型 */
	private String cxlx;
	/** 测序平台 */
	private String cxpt;
	/** 测序读长 */
	private String cxdc;
	/** 下一步流向 */
	private String nextFlow;
	/** 备注 */
	private String note;
	/** 相关主表 */
	private WkLifeBlendTask wkblendTask;
	// 临时表id
	private String tempId;
	// 状态
	private String state;
	// 目标浓度
	private String mbnd;
	// 理论体积
	private String lltj;
	// 补水或干燥
	private String bsgz;
	// 调整后文库浓度（浓度）
	private String tzhwknd;
	// 调整后文库体积（体积）
	private String tzhwktj;
	// 调整后文库总量
	private String tzhwkzl;
	// 下一步流向ID
	private String nextFlowId;
	// 科技服务
	private TechJkServiceTask techJkServiceTask;
	// 是否质控品
	private String isZkp;

	// 样本名称
	private String sampleName;
	// 插入片段
	private String insertSize;
	// 通量比例和
	private String sumFlux;
	// 1比例的量
	private String ratioOne;
	// 按比例所需量
	private String ratioNeed;
	// 物种
	private String species;
	// Lane数
	private String laneNum;
	// 科技服务明细
	private TechJkServiceTaskItem tjItem;

	/** 单位组 */
	private DicType unitGroup;
	// 原始样本编号
	private String sampleCode;
	
	// 第一次稀释倍数
	private String dycxsbs;
	// 第一次补水量
	private String dycbsl;
	// 第二次稀释倍数
	private String decxsbs;
	// 第二次补水量
	private String decbsl;
	// 上机浓度（pmol）
	private String pmol;

	
	public String getDycxsbs() {
		return dycxsbs;
	}

	public void setDycxsbs(String dycxsbs) {
		this.dycxsbs = dycxsbs;
	}

	public String getDycbsl() {
		return dycbsl;
	}

	public void setDycbsl(String dycbsl) {
		this.dycbsl = dycbsl;
	}

	public String getDecxsbs() {
		return decxsbs;
	}

	public void setDecxsbs(String decxsbs) {
		this.decxsbs = decxsbs;
	}

	public String getDecbsl() {
		return decbsl;
	}

	public void setDecbsl(String decbsl) {
		this.decbsl = decbsl;
	}

	public String getPmol() {
		return pmol;
	}

	public void setPmol(String pmol) {
		this.pmol = pmol;
	}

	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "UNIT_GROUP")
	public DicType getUnitGroup() {
		return unitGroup;
	}

	public void setUnitGroup(DicType unitGroup) {
		this.unitGroup = unitGroup;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "TJ_ITEM")
	public TechJkServiceTaskItem getTjItem() {
		return tjItem;
	}

	public void setTjItem(TechJkServiceTaskItem tjItem) {
		this.tjItem = tjItem;
	}

	public String getLaneNum() {
		return laneNum;
	}

	public void setLaneNum(String laneNum) {
		this.laneNum = laneNum;
	}

	public String getSampleName() {
		return sampleName;
	}

	public void setSampleName(String sampleName) {
		this.sampleName = sampleName;
	}

	public String getInsertSize() {
		return insertSize;
	}

	public void setInsertSize(String insertSize) {
		this.insertSize = insertSize;
	}

	public String getSumFlux() {
		return sumFlux;
	}

	public void setSumFlux(String sumFlux) {
		this.sumFlux = sumFlux;
	}

	public String getRatioOne() {
		return ratioOne;
	}

	public void setRatioOne(String ratioOne) {
		this.ratioOne = ratioOne;
	}

	public String getRatioNeed() {
		return ratioNeed;
	}

	public void setRatioNeed(String ratioNeed) {
		this.ratioNeed = ratioNeed;
	}

	public String getSpecies() {
		return species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public String getIsZkp() {
		return isZkp;
	}

	public void setIsZkp(String isZkp) {
		this.isZkp = isZkp;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "TECH_JK_SERVICE_TASK")
	public TechJkServiceTask getTechJkServiceTask() {
		return techJkServiceTask;
	}

	public void setTechJkServiceTask(TechJkServiceTask techJkServiceTask) {
		this.techJkServiceTask = techJkServiceTask;
	}

	public String getNextFlowId() {
		return nextFlowId;
	}

	public void setNextFlowId(String nextFlowId) {
		this.nextFlowId = nextFlowId;
	}

	public String getMbnd() {
		return mbnd;
	}

	public void setMbnd(String mbnd) {
		this.mbnd = mbnd;
	}

	public String getLltj() {
		return lltj;
	}

	public void setLltj(String lltj) {
		this.lltj = lltj;
	}

	public String getBsgz() {
		return bsgz;
	}

	public void setBsgz(String bsgz) {
		this.bsgz = bsgz;
	}

	public String getTzhwknd() {
		return tzhwknd;
	}

	public void setTzhwknd(String tzhwknd) {
		this.tzhwknd = tzhwknd;
	}

	public String getTzhwktj() {
		return tzhwktj;
	}

	public void setTzhwktj(String tzhwktj) {
		this.tzhwktj = tzhwktj;
	}

	public String getTzhwkzl() {
		return tzhwkzl;
	}

	public void setTzhwkzl(String tzhwkzl) {
		this.tzhwkzl = tzhwkzl;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String id
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String id
	 */
	public void setId(String id) {
		this.id = id;
	}



	 
	/**
	 * 方法: 取得String
	 * 
	 * @return: String 富集文库
	 */
	@Column(name = "FJWK", length = 50)
	public String getFjwk() {
		return this.fjwk;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 富集文库
	 */
	public void setFjwk(String fjwk) {
		this.fjwk = fjwk;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 上机分组
	 */
	@Column(name = "SJFZ", length = 100)
	public String getSjfz() {
		return this.sjfz;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 上机分组
	 */
	public void setSjfz(String sjfz) {
		this.sjfz = sjfz;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 预混合组号
	 */
	@Column(name = "YHHZH", length = 50)
	public String getYhhzh() {
		return this.yhhzh;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 预混合组号
	 */
	public void setYhhzh(String yhhzh) {
		this.yhhzh = yhhzh;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 混合组号
	 */
	@Column(name = "HHZH", length = 50)
	public String getHhzh() {
		return this.hhzh;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 混合组号
	 */
	public void setHhzh(String hhzh) {
		this.hhzh = hhzh;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 血液文库比例%
	 */
	@Column(name = "XYLR", length = 50)
	public String getXylr() {
		return this.xylr;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 血液文库比例%
	 */
	public void setXylr(String xylr) {
		this.xylr = xylr;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String ctdna文库比例%
	 */
	@Column(name = "CLR", length = 50)
	public String getClr() {
		return this.clr;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String ctdna文库比例%
	 */
	public void setClr(String clr) {
		this.clr = clr;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String ffpe文库比例%
	 */
	@Column(name = "FLR", length = 50)
	public String getFlr() {
		return this.flr;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String ffpe文库比例%
	 */
	public void setFlr(String flr) {
		this.flr = flr;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 新鲜组织文库比例%
	 */
	@Column(name = "XZLR", length = 50)
	public String getXzlr() {
		return this.xzlr;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 新鲜组织文库比例%
	 */
	public void setXzlr(String xzlr) {
		this.xzlr = xzlr;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 其他文库比例%
	 */
	@Column(name = "QTLR", length = 50)
	public String getQtlr() {
		return this.qtlr;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 其他文库比例%
	 */
	public void setQtlr(String qtlr) {
		this.qtlr = qtlr;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 通量
	 */
	@Column(name = "TL", length = 50)
	public String getTl() {
		return this.tl;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 通量
	 */
	public void setTl(String tl) {
		this.tl = tl;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 文库浓度（ng/ul）
	 */
	@Column(name = "WKND", length = 50)
	public String getWknd() {
		return this.wknd;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 文库浓度（ng/ul）
	 */
	public void setWknd(String wknd) {
		this.wknd = wknd;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 各组文库片段长度
	 */
	@Column(name = "WKPDCD", length = 50)
	public String getWkpdcd() {
		return this.wkpdcd;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 各组文库片段长度
	 */
	public void setWkpdcd(String wkpdcd) {
		this.wkpdcd = wkpdcd;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 测序类型
	 */
	@Column(name = "CXLX", length = 50)
	public String getCxlx() {
		return this.cxlx;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 测序类型
	 */
	public void setCxlx(String cxlx) {
		this.cxlx = cxlx;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 测序平台
	 */
	@Column(name = "CXPT", length = 50)
	public String getCxpt() {
		return this.cxpt;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 测序平台
	 */
	public void setCxpt(String cxpt) {
		this.cxpt = cxpt;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 测序读长
	 */
	@Column(name = "CXDC", length = 50)
	public String getCxdc() {
		return this.cxdc;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 测序读长
	 */
	public void setCxdc(String cxdc) {
		this.cxdc = cxdc;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 下一步流向
	 */
	@Column(name = "NEXT_FLOW", length = 50)
	public String getNextFlow() {
		return this.nextFlow;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 下一步流向
	 */
	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得WkBlendTask
	 * 
	 * @return: WkBlendTask 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "WKBLEND_TASK")
	public WkLifeBlendTask getWkblendTask() {
		return this.wkblendTask;
	}

	/**
	 * 方法: 设置WkBlendTask
	 * 
	 * @param: WkBlendTask 相关主表
	 */
	public void setWkblendTask(WkLifeBlendTask wkblendTask) {
		this.wkblendTask = wkblendTask;
	}
}