package com.biolims.experiment.sj.wklifeblend.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;

import com.biolims.experiment.sanger.model.SangerTaskCos;
import com.biolims.experiment.sanger.model.SangerTaskReagent;
import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTask;
import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTaskCos;
import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTaskItem;
import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTaskReagent;
import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTaskResult;
import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTaskTemp;
import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTaskTemplate;


@Repository
@SuppressWarnings("unchecked")
public class WkLifeBlendTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectWkBlendTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from WkLifeBlendTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<WkLifeBlendTask> list = new ArrayList<WkLifeBlendTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by id desc";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	// 根据上机分组号查询信息
	public List<WkLifeBlendTaskItem> setWkBlendTaskItemBysjfz(String id) {
		String hql = "from WkLifeBlendTaskItem where 1=1 and wkblendTask.id='" + id
				+ "'";
		List<WkLifeBlendTaskItem> list = new ArrayList<WkLifeBlendTaskItem>();
		list = this.getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> selectWkBlendTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from WkLifeBlendTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and wkblendTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<WkLifeBlendTaskItem> list = new ArrayList<WkLifeBlendTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectWkBlendTaskTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from WkLifeBlendTaskTemplate where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and wkblendTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<WkLifeBlendTaskTemplate> list = new ArrayList<WkLifeBlendTaskTemplate>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by code ASC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectWkBlendTaskReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from WkLifeBlendTaskReagent where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and wkblendTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<WkLifeBlendTaskReagent> list = new ArrayList<WkLifeBlendTaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectWkBlendTaskCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from WkLifeBlendTaskCos where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and wkblendTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<WkLifeBlendTaskCos> list = new ArrayList<WkLifeBlendTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	public Map<String, Object> selectWkBlendTaskCosListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,String itemId)
			throws Exception {
		String hql = "from WkLifeBlendTaskCos where 1=1 and itemId='"+itemId+"'";
		String key = "";
		if (scId != null)
			key = key + " and wkblendTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<WkLifeBlendTaskCos> list = new ArrayList<WkLifeBlendTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}


	public Map<String, Object> selectWkBlendTaskResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from WkLifeBlendTaskResult where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and wkblendTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<WkLifeBlendTaskResult> list = new ArrayList<WkLifeBlendTaskResult>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectWkBlendTaskTempList(
			Map<String, String> mapForQuery, String scId, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = "from WkLifeBlendTaskTemp where 1=1 and state='1'";
		String key = "";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<WkLifeBlendTaskTemp> list = new ArrayList<WkLifeBlendTaskTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
//			else {
//				key = key + " order by techJkServiceTask.sequenceBillDate desc";
//				// key = key + " order by fjwk desc";
//
//			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public List<WkLifeBlendTaskItem> setItemList(String id) {
		String hql = "from WkLifeBlendTaskItem where 1=1 and wkblendTask.id='" + id
				+ "'";
		List<WkLifeBlendTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据主表id查询
	public List<WkLifeBlendTaskResult> setWkBlendResultById(String code) {
		String hql = "from WkLifeBlendTaskResult where wkblendTask.id='" + code
				+ "'";
		List<WkLifeBlendTaskResult> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	// // 根据样本编号查询
	// public List<WkBlendTaskResult> setWkBlendResultByIds(String[] codes) {
	//
	// String insql = "''";
	// for (String code : codes) {
	//
	// insql += ",'" + code + "'";
	//
	// }
	//
	// String hql = "from WkBlendTaskResult t where (submit is null or submit='') and id in ("
	// + insql + ")";
	// List<WkBlendTaskResult> list = this.getSession().createQuery(hql).list();
	// return list;
	// }

	// 查询上机分组号
	public List<String> getsjfzByid(String id) {
		String hql = "from WkLifeBlendTaskItem where 1=1 and wkblendTask.id='" + id
				+ "' order by sjfz asc";
		List<String> list = new ArrayList<String>();
		list = this.getSession().createQuery("select distinct sjfz " + hql)
				.list();
		return list;
	}

	// 根据主表ID查询子表通量最小的值
	public String selectTlMin(String id) {
		String hql = "min(tl) from WkLifeBlendTaskItem where 1=1 and wkblendTask.id='"
				+ id + "'";
		String str = (String) this.getSession().createQuery(hql).uniqueResult();
		return str;
	}

	// 根据主表ID查询子表通量最小的值
	public String selecTlblSum(String id) {
		String hql = "sum(tlbl) from WkLifeBlendTaskItem where 1=1 and wkblendTask.id='"
				+ id + "'";
		String str = (String) this.getSession().createQuery(hql).uniqueResult();
		return str;
	}

	// 根据上机分组号查询信息
	public List<WkLifeBlendTaskItem> setWkBlendTaskItemBysjfz(String sjfz, String id) {
		String hql = "from WkLifeBlendTaskItem where 1=1 and wkblendTask.id='" + id
				+ "' and sjfz='" + sjfz + "'";
		List<WkLifeBlendTaskItem> list = new ArrayList<WkLifeBlendTaskItem>();
		list = this.getSession().createQuery(hql).list();
		return list;
	}

	/*
	 * 根据主表ID查询试剂明细
	 */
	public List<WkLifeBlendTaskReagent> setReagentList(String code) {
		String hql = "from WkLifeBlendTaskReagent where 1=1 and wkblendTask.id='"
				+ code + "'";
		List<WkLifeBlendTaskReagent> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}
	/**
	 * 根据选中的步骤查询相关的设备明细
	 * 
	 * @param id
	 * @param code
	 * @return
	 */
	public Map<String, Object> setCos(String id, String code) {
		String hql = "from WkLifeBlendTaskCos t where 1=1 and t.wkblendTask='" + id
				+ "' and t.itemId='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<SangerTaskCos> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	/**
	 * 根据选中的步骤查询相关的试剂明细
	 * 
	 * @param id
	 * @param code
	 * @return
	 */
	public Map<String, Object> setReagent(String id, String code) {
		String hql = "from WkLifeBlendTaskReagent t where 1=1 and t.wkblendTask='"
				+ id + "' and t.itemId='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<SangerTaskReagent> list = this.getSession().createQuery(hql)
				.list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
}