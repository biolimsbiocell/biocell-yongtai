package com.biolims.experiment.sj.wklifeblend.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.sj.wklifeblend.service.WkLifeBlendTaskService;


public class WkLifeBlendTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		WkLifeBlendTaskService mbService = (WkLifeBlendTaskService) ctx.getBean("wkLifeBlendTaskService");
//		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}