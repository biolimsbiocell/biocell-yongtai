package com.biolims.experiment.sj.wklifeblend.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;


import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTask;
import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTaskCos;
import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTaskItem;
import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTaskReagent;
import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTaskResult;
import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTaskTemp;
import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTaskTemplate;
import com.biolims.experiment.sj.wklifeblend.service.WkLifeBlendTaskService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

@Namespace("/experiment/sj/wklifeblend/wkBlendTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class WkLifeBlendTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "250501";
	@Autowired
	private WkLifeBlendTaskService wkLifeBlendTaskService;
	
	public WkLifeBlendTaskService getWkLifeBlendTaskService() {
		return wkLifeBlendTaskService;
	}

	public void setWkLifeBlendTaskService(
			WkLifeBlendTaskService wkLifeBlendTaskService) {
		this.wkLifeBlendTaskService = wkLifeBlendTaskService;
	}

	private WkLifeBlendTask wkBlendTask = new WkLifeBlendTask();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;
	@Resource
	private CommonService commonService;

	@Action(value = "showWkBlendTaskList")
	public String showWkBlendTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/sj/wklifeblend/wkLifeBlendTask.jsp");
	}

	@Action(value = "showWkBlendTaskListJson")
	public void showWkBlendTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = wkLifeBlendTaskService.findWkBlendTaskList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<WkLifeBlendTask> list = (List<WkLifeBlendTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("reciveDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("testUserOneId", "");
		map.put("testUserOneName", "");
		map.put("testUserTwoId", "");
		map.put("testUserTwoName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "wkBlendTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogWkBlendTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/sj/wklifeblend/wkLifeBlendTaskDialog.jsp");
	}

	@Action(value = "showDialogWkBlendTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogWkBlendTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = wkLifeBlendTaskService.findWkBlendTaskList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<WkLifeBlendTask> list = (List<WkLifeBlendTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("reciveDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editWkBlendTask")
	public String editWkBlendTask() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			wkBlendTask = wkLifeBlendTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "wkLifeBlendTask");
			String tName = workflowProcessInstanceService
					.findToDoTaskNameByFormId(id);
			putObjToContext("taskName", tName);
		} else {
			wkBlendTask.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			wkBlendTask.setCreateUser(user);
			wkBlendTask.setCreateDate(new Date());
			wkBlendTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			wkBlendTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(wkBlendTask.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/sj/wklifeblend/wkLifeBlendTaskEdit.jsp");
	}

	@Action(value = "copyWkBlendTask")
	public String copyWkBlendTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		wkBlendTask = wkLifeBlendTaskService.get(id);
		wkBlendTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/sj/wklifeblend/wkLifeBlendTaskEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = wkBlendTask.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "WkLifeBlendTask";
			String markCode = "WKHL";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			wkBlendTask.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("wkBlendTaskItem",
				getParameterFromRequest("wkBlendTaskItemJson"));

		aMap.put("wkBlendTaskTemplate",
				getParameterFromRequest("wkBlendTaskTemplateJson"));

		aMap.put("wkBlendTaskReagent",
				getParameterFromRequest("wkBlendTaskReagentJson"));

		aMap.put("wkBlendTaskCos",
				getParameterFromRequest("wkBlendTaskCosJson"));

		aMap.put("wkBlendTaskResult",
				getParameterFromRequest("wkBlendTaskResultJson"));

		wkLifeBlendTaskService.save(wkBlendTask, aMap);

		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/experiment/sj/wklifeblend/wkBlendTask/editWkBlendTask.action?id="
				+ wkBlendTask.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);

	}

	@Action(value = "saveAjax")
	public void saveAjax() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String id = getParameterFromRequest("id");
			wkBlendTask = commonService.get(WkLifeBlendTask.class, id);

			Map aMap = new HashMap();
			aMap.put("wkBlendTaskItem",
					getParameterFromRequest("wkBlendTaskItemJson"));

			aMap.put("wkBlendTaskTemplate",
					getParameterFromRequest("wkBlendTaskTemplateJson"));

			aMap.put("wkBlendTaskReagent",
					getParameterFromRequest("wkBlendTaskReagentJson"));

			aMap.put("wkBlendTaskCos",
					getParameterFromRequest("wkBlendTaskCosJson"));

			aMap.put("wkBlendTaskResult",
					getParameterFromRequest("wkBlendTaskResultJson"));

			map =wkLifeBlendTaskService.save(wkBlendTask, aMap);

			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));

	}

	@Action(value = "viewWkBlendTask")
	public String toViewWkBlendTask() throws Exception {
		String id = getParameterFromRequest("id");
		wkBlendTask = wkLifeBlendTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/sj/wklifeblend/wkLifeBlendTaskEdit.jsp");
	}

	@Action(value = "showWkBlendTaskItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWkBlendTaskItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/sj/wklifeblend/wkLifeBlendTaskItem.jsp");
	}

	@Action(value = "showWkBlendTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWkBlendTaskItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wkLifeBlendTaskService
					.findWkBlendTaskItemList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<WkLifeBlendTaskItem> list = (List<WkLifeBlendTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("fjwk", "");
			map.put("sjfz", "");
			map.put("yhhzh", "");
			map.put("hhzh", "");
			map.put("xylr", "");
			map.put("clr", "");
			map.put("flr", "");
			map.put("xzlr", "");
			map.put("qtlr", "");
			map.put("tl", "");
			map.put("tlbl", "");
			map.put("kzxl", "");
			map.put("ect", "");
			map.put("qpcrbl", "");
			map.put("blcd", "");
			map.put("wkhhl", "");
			map.put("wkhhtl", "");
			map.put("wkblendTask-name", "");
			map.put("wkblendTask-id", "");
			map.put("hhhnd", "");
			map.put("wkpdcd", "");
			map.put("cxlx", "");
			map.put("cxpt", "");
			map.put("cxdc", "");
			map.put("tempId", "");
			map.put("state", "");
			map.put("stepNum", "");
			map.put("orderNumber", "");
			map.put("concentration", "");
			map.put("volume", "");
			map.put("sumTotal", "");
			map.put("hhbl", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("isZkp", "");

			map.put("sampleName", "");
			map.put("insertSize", "");
			map.put("sumFlux", "");
			map.put("ratioOne", "");
			map.put("ratioNeed", "");
			map.put("species", "");

			map.put("wkmConcentration", "");
			map.put("wkMoleConcentration", "");
			map.put("sampleVolume", "");
			map.put("xszConcentration", "");
			map.put("addRsbVolume", "");
			map.put("xshSumVolume", "");

			map.put("unitGroup-id", "");
			map.put("unitGroup-name", "");
			map.put("indexa", "");
			map.put("tjItem-id", "");
			map.put("tjItem-note", "");
			map.put("sampleCode", "");

			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "showTemplateWaitList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTemplateWaitList() throws Exception {
		String scId = getRequest().getParameter("id");
		putObjToContext("id", scId);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/sj/wklifeblend/showTemplateWaitList.jsp");
	}

	@Action(value = "showTemplateWaitListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTemplateWaitListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wkLifeBlendTaskService
					.findWkBlendTaskTemplateList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<WkLifeBlendTaskTemplate> list = (List<WkLifeBlendTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("reciveUser-name", "");
			map.put("reciveUser-id", "");
			map.put("tItem", "");
			map.put("code", "");
			map.put("stepName", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("state", "");
			map.put("note", "");
			map.put("sampleCodes", "");
			map.put("wkblendTask-name", "");
			map.put("wkblendTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWkBlendTaskItem")
	public void delWkBlendTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wkLifeBlendTaskService.delWkBlendTaskItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showWkBlendTaskTemplateList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWkBlendTaskTemplateList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/sj/wklifeblend/wkLifeBlendTaskTemplate.jsp");
	}

	@Action(value = "showWkBlendTaskTemplateListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWkBlendTaskTemplateListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wkLifeBlendTaskService
					.findWkBlendTaskTemplateList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<WkLifeBlendTaskTemplate> list = (List<WkLifeBlendTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("testUserId", "");
			map.put("testUserName", "");
			map.put("tItem", "");
			map.put("code", "");
			map.put("stepName", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("state", "");
			map.put("note", "");
			map.put("sampleCodes", "");
			map.put("wkblendTask-name", "");
			map.put("wkblendTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWkBlendTaskTemplate")
	public void delWkBlendTaskTemplate() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wkLifeBlendTaskService.delWkBlendTaskTemplate(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showWkBlendTaskReagentList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWkBlendTaskReagentList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/sj/wklifeblend/wkLifeBlendTaskReagent.jsp");
	}

	@Action(value = "showWkBlendTaskReagentListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWkBlendTaskReagentListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wkLifeBlendTaskService
					.findWkBlendTaskReagentList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<WkLifeBlendTaskReagent> list = (List<WkLifeBlendTaskReagent>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("batch", "");
			map.put("count", "");
			map.put("oneNum", "");
			map.put("sampleNum", "");
			map.put("num", "");
			map.put("isGood", "");
			map.put("note", "");
			map.put("wkblendTask-name", "");
			map.put("wkblendTask-id", "");
			map.put("itemId", "");
			map.put("tReagent", "");
			map.put("sn", "");
			map.put("expireDate", "yyyy-MM-dd");
			map.put("reagentCode", "");
			map.put("isRunout", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWkBlendTaskReagent")
	public void delWkBlendTaskReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wkLifeBlendTaskService.delWkBlendTaskReagent(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showWkBlendTaskCosList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWkBlendTaskCosList() throws Exception {
//		String itemId = getParameterFromRequest("itemId");
//		putObjToContext("itemId", itemId);
		return dispatcher("/WEB-INF/page/experiment/sj/wklifeblend/wkLifeBlendTaskCos.jsp");
	}

	@Action(value = "showWkBlendTaskCosListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWkBlendTaskCosListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
//			String itemId = getParameterFromRequest("itemId");
			Map<String, Object> result = new HashMap<String, Object>();
			String scId = getRequest().getParameter("id");
//			if (itemId.equals("")) {
//				result = wkBlendTaskService.findWkBlendTaskCosList(scId,
//						startNum, limitNum, dir, sort);
//			} else {
				result = wkLifeBlendTaskService.findWkBlendTaskCosList(scId,
						startNum, limitNum, dir, sort);
//			}
			Long total = (Long) result.get("total");
			List<WkLifeBlendTaskCos> list = (List<WkLifeBlendTaskCos>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("state", "");
			map.put("code", "");
			map.put("temperature", "");
			map.put("speed", "");
			map.put("time", "");
			map.put("note", "");
			map.put("isGood", "");
			map.put("type-id", "");
			map.put("type-name", "");
			map.put("wkblendTask-name", "");
			map.put("wkblendTask-id", "");
			map.put("itemId", "");
			map.put("tCos", "");
			map.put("state", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWkBlendTaskCos")
	public void delWkBlendTaskCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wkLifeBlendTaskService.delWkBlendTaskCos(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showWkBlendTaskResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWkBlendTaskResultList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/sj/wklifeblend/wkLifeBlendTaskResult.jsp");
	}

	@Action(value = "showWkBlendTaskResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWkBlendTaskResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wkLifeBlendTaskService
					.findWkBlendTaskResultList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<WkLifeBlendTaskResult> list = (List<WkLifeBlendTaskResult>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("fjwk", "");
			map.put("sjfz", "");
			map.put("yhhzh", "");
			map.put("hhzh", "");
			map.put("xylr", "");
			map.put("clr", "");
			map.put("flr", "");
			map.put("xzlr", "");
			map.put("qtlr", "");
			map.put("tl", "");
			map.put("wknd", "");
			map.put("wkpdcd", "");
			map.put("cxlx", "");
			map.put("cxpt", "");
			map.put("cxdc", "");
			map.put("nextFlow", "");
			map.put("note", "");
			map.put("wkblendTask-name", "");
			map.put("wkblendTask-id", "");
			map.put("tempId", "");
			map.put("state", "");
			map.put("mbnd", "");
			map.put("lltj", "");
			map.put("bsgz", "");
			map.put("tzhwknd", "");
			map.put("tzhwktj", "");
			map.put("tzhwkzl", "");
			map.put("nextFlowId", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("isZkp", "");
			map.put("sampleName", "");
			map.put("insertSize", "");
			map.put("sumFlux", "");
			map.put("ratioOne", "");
			map.put("ratioNeed", "");
			map.put("species", "");
			map.put("laneNum", "");
			map.put("sampleCode", "");
			map.put("dycxsbs", "");
			map.put("dycbsl", "");
			map.put("decxsbs", "");
			map.put("decbsl", "");
			map.put("pmol", "");
			map.put("unitGroup-id", "");
			map.put("unitGroup-name", "");
			map.put("sampleCode", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWkBlendTaskResult")
	public void delWkBlendTaskResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wkLifeBlendTaskService.delWkBlendTaskResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showWkBlendTaskTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWkBlendTaskTempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/sj/wklifeblend/wkLifeBlendTaskTemp.jsp");
	}

	@Action(value = "showWkBlendTaskTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWkBlendTaskTempListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			String data = getParameterFromRequest("data");
			Map<String, String> map2Query = new HashMap<String, String>();
			if (data != null && data.length() > 0)
				map2Query = JsonUtils.toObjectByJson(data, Map.class);
			Map<String, Object> result = wkLifeBlendTaskService
					.findWkBlendTaskTempList(map2Query, scId, startNum,
							limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<WkLifeBlendTaskTemp> list = (List<WkLifeBlendTaskTemp>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("fjwk", "");
			map.put("sjfz", "");
			map.put("yhhzh", "");
			map.put("wklx", "");
			map.put("kybh", "");
			map.put("cxlx", "");
			map.put("cxpt", "");
			map.put("cxdc", "");
			map.put("tempid", "");
			map.put("state", "");

			map.put("xylr", "");
			map.put("clr", "");
			map.put("flr", "");
			map.put("xzlr", "");
			map.put("qtlr", "");
			map.put("tl", "");
			map.put("tlbl", "");
			map.put("kzxl", "");
			map.put("ect", "");
			map.put("qpcrbl", "");
			map.put("blcd", "");
			map.put("probeCode-id", "");
			map.put("probeCode-name", "");
			map.put("concentration", "");
			map.put("volume", "");
			map.put("sumTotal", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("techJkServiceTask-sequenceBillDate", "yyyy-MM-dd");
			map.put("isZkp", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			map.put("tjItem-blendLane", "");
			map.put("tjItem-blendFc", "");
			map.put("tjItem-note", "");
			map.put("indexa", "");
			map.put("insertSize", "");
			map.put("sampleCode", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWkBlendTaskTemp", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void delWkBlendTaskTemp() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wkLifeBlendTaskService.delWkBlendTaskTemp(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 明细入库信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "rukuWkBlendTaskItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void rukuWkBlendTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wkLifeBlendTaskService.rukuWkBlendTaskItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	

	public WkLifeBlendTask getWkBlendTask() {
		return wkBlendTask;
	}

	public void setWkBlendTask(WkLifeBlendTask wkLifeBlendTask) {
		this.wkBlendTask = wkLifeBlendTask;
	}

	// 查询科研样本
	@Action(value = "selTechJkServicTaskByid", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selTechJkServicTaskByid() throws Exception {
		String id = getParameterFromRequest("id");
		Map<String, String> map = new HashMap<String, String>();
		map.put("techJkServiceTask.id", id);
		Map<String, Object> map2 = wkLifeBlendTaskService.findWkBlendTaskTempList(
				map, null, null, null, null, null);
		List<WkLifeBlendTaskTemp> list = (List<WkLifeBlendTaskTemp>) map2.get("list");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			result.put("success", true);
			result.put("list", list);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	/**
	 * 根据选中的步骤异步加载相关的试剂数据
	 * 
	 * @throws Exception
	 */
	@Action(value = "setReagent")
	public void setReagent() throws Exception {
		String code = getRequest().getParameter("code");
		String id = getRequest().getParameter("tid");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.wkLifeBlendTaskService
					.setReagent(id, code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 根据选中的步骤异步加载相关的设备数据
	 * 
	 * @throws Exception
	 */
	@Action(value = "setCos")
	public void setCos() throws Exception {
		String code = getRequest().getParameter("code");
		String id = getRequest().getParameter("tid");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.wkLifeBlendTaskService
					.setCos(id, code);
			result.put("success", true);
			result.put("data", dataListMap);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
