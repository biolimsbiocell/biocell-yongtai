package com.biolims.experiment.sj.mbenrichment.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.sj.mbenrichment.service.MbEnrichmentService;

public class MbEnrichmentEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		MbEnrichmentService mbService = (MbEnrichmentService) ctx.getBean("mbEnrichmentService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}