package com.biolims.experiment.sj.mbenrichment.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTaskItem;
import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTaskResult;

@Repository
@SuppressWarnings("unchecked")
public class MbEnrichmentManageDao extends BaseHibernateDao {
	
	public Map<String, Object> selectExperimentDnaManageList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from WkLifeBlendTaskResult where 1=1 and wkblendTask.state='1' ";//and state='1'";
		if (mapForQuery.size() > 0) {
			key = map2where(mapForQuery);
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<WkLifeBlendTaskResult> list = new ArrayList<WkLifeBlendTaskResult>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by method," + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	
//	/**
//	 * 核酸提取结果取消明细样本
//	 * @param id
//	 */
//	public int  RemoveExperimentDnaManage(String id){
//		String hql = "Update WkLifeBlendTaskResult s set s.state = '0' where s.id = ?";
//		int count = this.getSession().createQuery(hql)
//						.setParameter(0, id)
//						.executeUpdate();
//		return count;
//	}
	
	/**
	 * 核酸管理
	 * 
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 */
	public Map<String, Object> selectDnaTaskItemList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from WkLifeBlendTaskItem where 1=1 and wkblendTask.state='1' ";//and state='0' ";
		if (mapForQuery.size() > 0) {
			key = map2where(mapForQuery);
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<WkLifeBlendTaskItem> list = new ArrayList<WkLifeBlendTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	
	/**
	 * 判断样本下一步流向是否执行
	 * @param id
	 * @param code 样本编号
	 * @return ture没被执行/false已执行
	 */
	public String getTempTable(String id, String code) {
//		String sql = "select A2.APPLICATION_TYPE_TABLE from SAMPLE_DNA_INFO A1 "
//					+"left join NEXT_FLOW A2 "
//					+"on A1.next_flow_id = A2.id "
//					+"where a1.id= ?";
//		//得到下一步流向对应的临时表
//		String temp = this.getSession().createSQLQuery(sql).setParameter(0, id)
//						  .uniqueResult().toString();
		
		String hql = "SELECT count(*) from MbEnrichmentTemp WHERE hhzh= ? ";
		String flag = this.getSession().createQuery(hql)
						  .setParameter(0, code).uniqueResult().toString();
		return flag;
		
	}

}