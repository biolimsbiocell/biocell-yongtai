package com.biolims.experiment.sj.mbenrichment.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.sj.mbenrichment.model.MbEnrichment;
import com.biolims.experiment.sj.mbenrichment.model.MbEnrichmentCos;
import com.biolims.experiment.sj.mbenrichment.model.MbEnrichmentItem;
import com.biolims.experiment.sj.mbenrichment.model.MbEnrichmentReagent;
import com.biolims.experiment.sj.mbenrichment.model.MbEnrichmentResult;
import com.biolims.experiment.sj.mbenrichment.model.MbEnrichmentTemp;
import com.biolims.experiment.sj.mbenrichment.model.MbEnrichmentTemplate;
import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTaskItem;
import com.biolims.experiment.wkLife.model.SampleWkLifeInfo;

@Repository
@SuppressWarnings("unchecked")
public class MbEnrichmentDao extends BaseHibernateDao {
	public Map<String, Object> selectMbEnrichmentList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from MbEnrichment where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<MbEnrichment> list = new ArrayList<MbEnrichment>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
		public Map<String, Object> selectMbEnrichmentItemList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from MbEnrichmentItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and mbEnrichment.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<MbEnrichmentItem> list = new ArrayList<MbEnrichmentItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectMbEnrichmentTemplateList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from MbEnrichmentTemplate where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and mbEnrichment.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<MbEnrichmentTemplate> list = new ArrayList<MbEnrichmentTemplate>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectMbEnrichmentReagentList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from MbEnrichmentReagent where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and mbEnrichment.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<MbEnrichmentReagent> list = new ArrayList<MbEnrichmentReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectMbEnrichmentCosList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from MbEnrichmentCos where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and mbEnrichment.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<MbEnrichmentCos> list = new ArrayList<MbEnrichmentCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectMbEnrichmentResultList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from MbEnrichmentResult where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and mbEnrichment.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<MbEnrichmentResult> list = new ArrayList<MbEnrichmentResult>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectMbEnrichmentTempList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from MbEnrichmentTemp where 1=1 ";
		String key = "";
//		if (scId != null)
//			key = key + " and mbEnrichment.id='" + scId + "'";
		key = key + " and state='1' ";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<MbEnrichmentTemp> list = new ArrayList<MbEnrichmentTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		/**
		 * 根据选中的步骤查询相关的设备明细
		 * 
		 * @param id
		 * @param code
		 * @return
		 */
		public Map<String, Object> setCos(String id, String code) {
			String hql = "from MbEnrichmentCos t where 1=1 and t.mbEnrichment='" + id
					+ "' and t.itemId='" + code + "'";
			Long total = (Long) this.getSession()
					.createQuery("select count(*)" + hql).uniqueResult();
			List<MbEnrichmentCos> list = this.getSession().createQuery(hql).list();
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("total", total);
			result.put("list", list);
			return result;
		}
		/**
		 * 根据选中的步骤查询相关的试剂明细
		 * 
		 * @param id
		 * @param code
		 * @return
		 */
		public Map<String, Object> setReagent(String id, String code) {
			String hql = "from MbEnrichmentReagent t where 1=1 and t.mbEnrichment='"
					+ id + "' and t.itemId='" + code + "'";
			Long total = (Long) this.getSession()
					.createQuery("select count(*)" + hql).uniqueResult();
			List<MbEnrichmentReagent> list = this.getSession().createQuery(hql)
					.list();
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("total", total);
			result.put("list", list);
			return result;
		}
		// 根据主表id查询
	public List<MbEnrichmentResult> setWkBlendResultById(String code) {
		String hql = "from MbEnrichmentResult where mbEnrichment.id='" + code
				+ "'";
		List<MbEnrichmentResult> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}
	// 根据pooling号查询文库混合life信息
	public List<WkLifeBlendTaskItem> setWkBlendTaskItemBysjfz(String id) {
		String hql = "from WkLifeBlendTaskItem where 1=1 and hhzh='" + id
				+ "'";
		List<WkLifeBlendTaskItem> list = new ArrayList<WkLifeBlendTaskItem>();
		list = this.getSession().createQuery(hql).list();
		return list;
	}
	// 根据富集文库查询文库结果信息
	public List<SampleWkLifeInfo> getItemListByfjwk(String fjwk) {
		String hql = "from SampleWkLifeInfo where 1=1 and wkCode='" + fjwk
				+ "'";
		List<SampleWkLifeInfo> list = new ArrayList<SampleWkLifeInfo>();
		list = this.getSession().createQuery(hql).list();
		return list;
	}
}