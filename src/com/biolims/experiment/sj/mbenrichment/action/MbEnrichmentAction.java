package com.biolims.experiment.sj.mbenrichment.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.sj.mbenrichment.model.MbEnrichment;
import com.biolims.experiment.sj.mbenrichment.model.MbEnrichmentCos;
import com.biolims.experiment.sj.mbenrichment.model.MbEnrichmentItem;
import com.biolims.experiment.sj.mbenrichment.model.MbEnrichmentReagent;
import com.biolims.experiment.sj.mbenrichment.model.MbEnrichmentResult;
import com.biolims.experiment.sj.mbenrichment.model.MbEnrichmentTemp;
import com.biolims.experiment.sj.mbenrichment.model.MbEnrichmentTemplate;
import com.biolims.experiment.sj.mbenrichment.service.MbEnrichmentService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;
@Namespace("/experiment/sj/mbenrichment/mbEnrichment")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class MbEnrichmentAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "250502";
	@Autowired
	private MbEnrichmentService mbEnrichmentService;
	private MbEnrichment mbEnrichment = new MbEnrichment();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;
	
	
	@Action(value = "showMbEnrichmentList")
	public String showMbEnrichmentList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/sj/mbenrichment/mbEnrichment.jsp");
	}

	@Action(value = "showMbEnrichmentListJson")
	public void showMbEnrichmentListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = mbEnrichmentService.findMbEnrichmentList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<MbEnrichment> list = (List<MbEnrichment>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("reciveDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "mbEnrichmentSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogMbEnrichmentList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/sj/mbenrichment/mbEnrichmentDialog.jsp");
	}

	@Action(value = "showDialogMbEnrichmentListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogMbEnrichmentListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = mbEnrichmentService.findMbEnrichmentList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<MbEnrichment> list = (List<MbEnrichment>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("reciveDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editMbEnrichment")
	public String editMbEnrichment() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			mbEnrichment = mbEnrichmentService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "mbEnrichment");
			String tName = workflowProcessInstanceService
					.findToDoTaskNameByFormId(id);
			putObjToContext("taskName", tName);
		} else {
			mbEnrichment.setId("NEW");
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			mbEnrichment.setCreateUser(user);
			mbEnrichment.setCreateDate(new Date());
			mbEnrichment.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			mbEnrichment.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(mbEnrichment.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/sj/mbenrichment/mbEnrichmentEdit.jsp");
	}

	@Action(value = "copyMbEnrichment")
	public String copyMbEnrichment() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		mbEnrichment = mbEnrichmentService.get(id);
		mbEnrichment.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/sj/mbenrichment/mbEnrichmentEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = mbEnrichment.getId();
		if((id!=null&&id.equals(""))||id.equals("NEW")){
			String modelName = "MbEnrichment";
			String markCode = "MBFJ";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			mbEnrichment.setId(autoID);
		}
		Map aMap = new HashMap();
			aMap.put("mbEnrichmentItem",getParameterFromRequest("mbEnrichmentItemJson"));
		
			aMap.put("mbEnrichmentTemplate",getParameterFromRequest("mbEnrichmentTemplateJson"));
		
			aMap.put("mbEnrichmentReagent",getParameterFromRequest("mbEnrichmentReagentJson"));
		
			aMap.put("mbEnrichmentCos",getParameterFromRequest("mbEnrichmentCosJson"));
		
			aMap.put("mbEnrichmentResult",getParameterFromRequest("mbEnrichmentResultJson"));
		
//			aMap.put("mbEnrichmentTemp",getParameterFromRequest("mbEnrichmentTempJson"));
		
		mbEnrichmentService.save(mbEnrichment,aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/experiment/sj/mbenrichment/mbEnrichment/editMbEnrichment.action?id="
				+ mbEnrichment.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);
//		return redirect("/experiment/sj/mbenrichment/mbEnrichment/editMbEnrichment.action?id=" + mbEnrichment.getId());

	}

	@Action(value = "viewMbEnrichment")
	public String toViewMbEnrichment() throws Exception {
		String id = getParameterFromRequest("id");
		mbEnrichment = mbEnrichmentService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/sj/mbenrichment/mbEnrichmentEdit.jsp");
	}
	

	@Action(value = "showMbEnrichmentItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showMbEnrichmentItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/sj/mbenrichment/mbEnrichmentItem.jsp");
	}

	@Action(value = "showMbEnrichmentItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showMbEnrichmentItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = mbEnrichmentService.findMbEnrichmentItemList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<MbEnrichmentItem> list = (List<MbEnrichmentItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("fjwk", "");
			map.put("hhzh", "");
			map.put("tl", "");
			map.put("wknd", "");
			map.put("wkpdcd", "");
			map.put("cxlx", "");
			map.put("cxpt", "");
			map.put("cxdc", "");
			map.put("nextFlow", "");
			map.put("note", "");
			map.put("mbEnrichment-name", "");
			map.put("mbEnrichment-id", "");
			map.put("tzhwknd", "");
			map.put("tzhwktj", "");
			map.put("dycxsbs", "");
			map.put("dycbsl", "");
			map.put("decxsbs", "");
			map.put("decbsl", "");
			map.put("pmol", "");
			map.put("tempId", "");
			map.put("orderNumber", "");
			map.put("sampleCode", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delMbEnrichmentItem")
	public void delMbEnrichmentItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			mbEnrichmentService.delMbEnrichmentItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	

	@Action(value = "showMbEnrichmentTemplateList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showMbEnrichmentTemplateList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/sj/mbenrichment/mbEnrichmentTemplate.jsp");
	}

	@Action(value = "showMbEnrichmentTemplateListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showMbEnrichmentTemplateListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = mbEnrichmentService.findMbEnrichmentTemplateList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<MbEnrichmentTemplate> list = (List<MbEnrichmentTemplate>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("reciveUser-name", "");
			map.put("reciveUser-id", "");
			map.put("tItem", "");
			map.put("code", "");
			map.put("stepName", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("state", "");
			map.put("note", "");
			map.put("sampleCodes", "");
			map.put("mbEnrichment-name", "");
			map.put("mbEnrichment-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delMbEnrichmentTemplate")
	public void delMbEnrichmentTemplate() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			mbEnrichmentService.delMbEnrichmentTemplate(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	

	@Action(value = "showMbEnrichmentReagentList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showMbEnrichmentReagentList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/sj/mbenrichment/mbEnrichmentReagent.jsp");
	}

	@Action(value = "showMbEnrichmentReagentListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showMbEnrichmentReagentListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = mbEnrichmentService.findMbEnrichmentReagentList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<MbEnrichmentReagent> list = (List<MbEnrichmentReagent>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("batch", "");
			map.put("count", "");
			map.put("oneNum", "");
			map.put("sampleNum", "");
			map.put("num", "");
			map.put("isGood", "");
			map.put("note", "");
			map.put("mbEnrichment-name", "");
			map.put("mbEnrichment-id", "");
			map.put("itemId", "");
			map.put("tReagent", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delMbEnrichmentReagent")
	public void delMbEnrichmentReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			mbEnrichmentService.delMbEnrichmentReagent(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	

	@Action(value = "showMbEnrichmentCosList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showMbEnrichmentCosList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/sj/mbenrichment/mbEnrichmentCos.jsp");
	}

	@Action(value = "showMbEnrichmentCosListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showMbEnrichmentCosListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = mbEnrichmentService.findMbEnrichmentCosList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<MbEnrichmentCos> list = (List<MbEnrichmentCos>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("temperature", "");
			map.put("speed", "");
			map.put("time", "");
			map.put("note", "");
			map.put("isGood", "");
			map.put("mbEnrichment-name", "");
			map.put("mbEnrichment-id", "");
			map.put("itemId", "");
			map.put("tCos", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delMbEnrichmentCos")
	public void delMbEnrichmentCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			mbEnrichmentService.delMbEnrichmentCos(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	

	@Action(value = "showMbEnrichmentResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showMbEnrichmentResultList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/sj/mbenrichment/mbEnrichmentResult.jsp");
	}

	@Action(value = "showMbEnrichmentResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showMbEnrichmentResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = mbEnrichmentService.findMbEnrichmentResultList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<MbEnrichmentResult> list = (List<MbEnrichmentResult>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("fjwk", "");
			map.put("hhzh", "");
			map.put("tl", "");
			map.put("wknd", "");
			map.put("wkpdcd", "");
			map.put("cxlx", "");
			map.put("cxpt", "");
			map.put("cxdc", "");
			map.put("nextFlow", "");
			map.put("note", "");
			map.put("mbEnrichment-name", "");
			map.put("mbEnrichment-id", "");
			map.put("tzhwknd", "");
			map.put("tzhwktj", "");
			map.put("dycxsbs", "");
			map.put("dycbsl", "");
			map.put("decxsbs", "");
			map.put("decbsl", "");
			map.put("pmol", "");
			map.put("tempId", "");
			map.put("otNum", "");
			map.put("otState", "");
			map.put("esNum", "");
			map.put("esState", "");
			map.put("chefNum", "");
			map.put("chefState", "");
			map.put("result", "");
			
			map.put("sampleCode", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delMbEnrichmentResult")
	public void delMbEnrichmentResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			mbEnrichmentService.delMbEnrichmentResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	

	@Action(value = "showMbEnrichmentTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showMbEnrichmentTempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/sj/mbenrichment/mbEnrichmentTemp.jsp");
	}

	@Action(value = "showMbEnrichmentTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showMbEnrichmentTempListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = mbEnrichmentService.findMbEnrichmentTempList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<MbEnrichmentTemp> list = (List<MbEnrichmentTemp>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("fjwk", "");
			map.put("hhzh", "");
			map.put("tl", "");
			map.put("wknd", "");
			map.put("wkpdcd", "");
			map.put("cxlx", "");
			map.put("cxpt", "");
			map.put("cxdc", "");
			map.put("nextFlow", "");
			map.put("note", "");
			map.put("tzhwknd", "");
			map.put("tzhwktj", "");
			map.put("dycxsbs", "");
			map.put("dycbsl", "");
			map.put("decxsbs", "");
			map.put("decbsl", "");
			map.put("pmol", "");
			map.put("state", "");
			map.put("sampleCode", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delMbEnrichmentTemp")
	public void delMbEnrichmentTemp() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			mbEnrichmentService.delMbEnrichmentTemp(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	/**
	 * 根据选中的步骤异步加载相关的试剂数据
	 * 
	 * @throws Exception
	 */
	@Action(value = "setReagent")
	public void setReagent() throws Exception {
		String code = getRequest().getParameter("code");
		String id = getRequest().getParameter("tid");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.mbEnrichmentService
					.setReagent(id, code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 根据选中的步骤异步加载相关的设备数据
	 * 
	 * @throws Exception
	 */
	@Action(value = "setCos")
	public void setCos() throws Exception {
		String code = getRequest().getParameter("code");
		String id = getRequest().getParameter("tid");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.mbEnrichmentService
					.setCos(id, code);
			result.put("success", true);
			result.put("data", dataListMap);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}


	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public MbEnrichmentService getMbEnrichmentService() {
		return mbEnrichmentService;
	}

	public void setMbEnrichmentService(MbEnrichmentService mbEnrichmentService) {
		this.mbEnrichmentService = mbEnrichmentService;
	}

	public MbEnrichment getMbEnrichment() {
		return mbEnrichment;
	}

	public void setMbEnrichment(MbEnrichment mbEnrichment) {
		this.mbEnrichment = mbEnrichment;
	}


}
