﻿package com.biolims.experiment.sj.mbenrichment.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.sj.mbenrichment.model.MbEnrichmentWaitManage;
import com.biolims.experiment.sj.mbenrichment.service.MbEnrichmentManageService;
import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTaskItem;
import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTaskResult;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/sj/mbenrichment/mbEnrichmentManage")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class MbEnrichmentManageAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "250503";
	@Autowired
	private MbEnrichmentManageService mbEnrichmentManageService;
	private MbEnrichmentWaitManage mbEnrichmentManage = new MbEnrichmentWaitManage();
	@Resource
	private FileInfoService fileInfoService;

	@Action(value = "showMbEnrichmentManageEditList")
	public String showMbEnrichmentManageEditList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/sj/mbenrichment/mbEnrichmentManageEdit.jsp");
	}

	@Action(value = "showMbEnrichmentManageList")
	public String showMbEnrichmentManageList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/sj/mbenrichment/mbEnrichmentManage.jsp");
	}

	@Action(value = "showMbEnrichmentManageListJson")
	public void showMbEnrichmentManageListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");//排序
		String data = getParameterFromRequest("data");//查询条件
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = mbEnrichmentManageService
				.findExperimentDnaManageList(map2Query, startNum, limitNum,
						dir, sort);
		Long count = (Long) result.get("total");
		List<WkLifeBlendTaskResult> list = (List<WkLifeBlendTaskResult>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("fjwk", "");
		map.put("sjfz", "");
		map.put("yhhzh", "");
		map.put("hhzh", "");
		map.put("xylr", "");
		map.put("clr", "");
		map.put("flr", "");
		map.put("xzlr", "");
		map.put("qtlr", "");
		map.put("tl", "");
		map.put("wknd", "");
		map.put("wkpdcd", "");
		map.put("cxlx", "");
		map.put("cxpt", "");
		map.put("cxdc", "");
		map.put("nextFlow", "");
		map.put("note", "");
		map.put("wkblendTask-name", "");
		map.put("wkblendTask-id", "");
		map.put("tempId", "");
		map.put("state", "");
		map.put("mbnd", "");
		map.put("lltj", "");
		map.put("bsgz", "");
		map.put("tzhwknd", "");
		map.put("tzhwktj", "");
		map.put("tzhwkzl", "");
		map.put("nextFlowId", "");
		map.put("techJkServiceTask-id", "");
		map.put("techJkServiceTask-name", "");
		map.put("isZkp", "");
		map.put("sampleName", "");
		map.put("insertSize", "");
		map.put("sumFlux", "");
		map.put("ratioOne", "");
		map.put("ratioNeed", "");
		map.put("species", "");
		map.put("laneNum", "");
		map.put("sampleCode", "");
		map.put("dycxsbs", "");
		map.put("dycbsl", "");
		map.put("decxsbs", "");
		map.put("decbsl", "");
		map.put("pmol", "");
		map.put("unitGroup-id", "");
		map.put("unitGroup-name", "");
		map.put("sampleCode", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

//	/**
//	 * 样本结果管理明细取消
//	 * @throws Exception
//	 */
//	@Action(value = "RemoveMbEnrichmentManageById")
//	public void RemoveMbEnrichmentManageById() throws Exception {
//		String id = getRequest().getParameter("id");
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			mbEnrichmentManageService.RemoveExperimentDnaManage(id);
//			result.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}
	/**
	 * 判断样本下一步流向是否执行
	 * @return ture没被执行/false已执行
	 */
	@Action(value = "serchExcuteByNext")
	public void serchExcuteByNext() throws Exception {
		String id = getRequest().getParameter("id");
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		Boolean flag = false;
		try {
			flag = mbEnrichmentManageService.getTempTable(id, code);
			result.put("success", true);
			result.put("flag", flag);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	/**
	 * 
	* @Title: RemoveExperimentDnaManageById 
	* @Description: TODO(样本结果管理明细取消) 
	* @param @throws Exception    设定文件 
	* @return void    返回类型 
	* @author  zhiqiang.yang@biolims.cn 
	* @date 2017-8-10 下午3:36:04 
	* @throws
	 */
	@Action(value = "RemoveExperimentDnaManageById")
	public void RemoveExperimentDnaManageById() throws Exception {
		String id = getRequest().getParameter("id");
		String code = getRequest().getParameter("code");
		String iscz = getRequest().getParameter("iscz");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			mbEnrichmentManageService.RemoveExperimentDnaManage(id,code,iscz);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	@Action(value = "MbEnrichmentManageSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogMbEnrichmentManageList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/sj/mbenrichment/experimentDnaManageDialog.jsp");
	}

	@Action(value = "showDialogMbEnrichmentManageListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogMbEnrichmentManageListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = mbEnrichmentManageService
				.findExperimentDnaManageList(map2Query, startNum, limitNum,
						dir, sort);
		Long count = (Long) result.get("total");
		List<MbEnrichmentWaitManage> list = (List<MbEnrichmentWaitManage>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("volume", "");
		map.put("note", "");
		map.put("method", "");
		map.put("nextFlow", "");
		map.put("code", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editMbEnrichmentManage")
	public String editMbEnrichmentManage() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			mbEnrichmentManage = mbEnrichmentManageService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "experimentDnaManage");
		} else {
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/sj/mbenrichment/mbEnrichmentManageEdit.jsp");
	}

	@Action(value = "copyMbEnrichmentManage")
	public String copyMbEnrichmentManage() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		mbEnrichmentManage = mbEnrichmentManageService.get(id);
		mbEnrichmentManage.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/sj/mbenrichment/mbEnrichmentManageEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = mbEnrichmentManage.getId();
		if (id != null && id.equals("")) {
			mbEnrichmentManage.setId(null);
		}
		Map aMap = new HashMap();
		mbEnrichmentManageService.save(mbEnrichmentManage, aMap);
		return redirect("/experiment/sj/mbenrichment/experimentDnaManage/editExperimentDnaManage.action?id="
				+ mbEnrichmentManage.getId());

	}

	@Action(value = "viewMbEnrichmentManage")
	public String toViewMbEnrichmentManage() throws Exception {
		String id = getParameterFromRequest("id");
		mbEnrichmentManage = mbEnrichmentManageService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/sj/mbenrichment/mbEnrichmentManageEdit.jsp");
	}



	/**
	 * 保存中间产物
	 * @throws Exception
	 */
	@Action(value = "saveDNAManage")
	public void saveDNAAbnormal() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getParameterFromRequest("itemDataJson");
			mbEnrichmentManageService
					.saveExperimentDnaInfoManager(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 保存样本
	 * @throws Exception
	 */
	@Action(value = "saveDnaGetItemManager")
	public void saveDnaGetItemManager() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getParameterFromRequest("itemDataJson");
			mbEnrichmentManageService.saveDnaGetItemManager(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	@Action(value = "showDnaGetItemManagerList")
	public String showDnaGetItemManagerList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/sj/mbenrichment/mbEbrichmentItemManage.jsp");
	}

	@Action(value = "showDnaGetItemManagerListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDnaGetItemManagerListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		// 排序方式
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		try {
			Map<String, Object> result = mbEnrichmentManageService
					.findDnaGetItemList(map2Query, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<WkLifeBlendTaskItem> list = (List<WkLifeBlendTaskItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("fjwk", "");
			map.put("sjfz", "");
			map.put("yhhzh", "");
			map.put("hhzh", "");
			map.put("xylr", "");
			map.put("clr", "");
			map.put("flr", "");
			map.put("xzlr", "");
			map.put("qtlr", "");
			map.put("tl", "");
			map.put("tlbl", "");
			map.put("kzxl", "");
			map.put("ect", "");
			map.put("qpcrbl", "");
			map.put("blcd", "");
			map.put("wkhhl", "");
			map.put("wkhhtl", "");
			map.put("wkblendTask-name", "");
			map.put("wkblendTask-id", "");
			map.put("hhhnd", "");
			map.put("wkpdcd", "");
			map.put("cxlx", "");
			map.put("cxpt", "");
			map.put("cxdc", "");
			map.put("tempId", "");
			map.put("state", "");
			map.put("stepNum", "");
			map.put("orderNumber", "");
			map.put("concentration", "");
			map.put("volume", "");
			map.put("sumTotal", "");
			map.put("hhbl", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("isZkp", "");

			map.put("sampleName", "");
			map.put("insertSize", "");
			map.put("sumFlux", "");
			map.put("ratioOne", "");
			map.put("ratioNeed", "");
			map.put("species", "");

			map.put("wkmConcentration", "");
			map.put("wkMoleConcentration", "");
			map.put("sampleVolume", "");
			map.put("xszConcentration", "");
			map.put("addRsbVolume", "");
			map.put("xshSumVolume", "");

			map.put("unitGroup-id", "");
			map.put("unitGroup-name", "");
			map.put("indexa", "");
			map.put("tjItem-id", "");
			map.put("tjItem-note", "");
			map.put("sampleCode", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 样本管理明细入库
	 * @throws Exception
	 */
	@Action(value = "dnaGetManageItemRuku")
	public void plasmaManageItemRuku() throws Exception {
		String ids = getRequest().getParameter("ids");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			mbEnrichmentManageService.dnaGetManageItemRuku(ids);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 样本管理明细提取待办
	 * @throws Exception
	 */
	@Action(value = "dnaGetManageItemTiqu")
	public void dnaGetManageItemTiqu() throws Exception {
		String id = getRequest().getParameter("id");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			mbEnrichmentManageService.dnaGetManageItemTiqu(id);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public MbEnrichmentManageService getMbEnrichmentManageService() {
		return mbEnrichmentManageService;
	}

	public void setMbEnrichmentManageService(
			MbEnrichmentManageService mbEnrichmentManageService) {
		this.mbEnrichmentManageService = mbEnrichmentManageService;
	}

	public MbEnrichmentWaitManage getMbEnrichmentManage() {
		return mbEnrichmentManage;
	}

	public void setMbEnrichmentManage(MbEnrichmentWaitManage mbEnrichmentManage) {
		this.mbEnrichmentManage = mbEnrichmentManage;
	}

	public FileInfoService getFileInfoService() {
		return fileInfoService;
	}

	public void setFileInfoService(FileInfoService fileInfoService) {
		this.fileInfoService = fileInfoService;
	}
	
	
	
}
