package com.biolims.experiment.sj.mbenrichment.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 模板富集结果
 * @author lims-platform
 * @date 2017-08-28 14:18:43
 * @version V1.0   
 *
 */
@Entity
@Table(name = "MB_ENRICHMENT_RESULT")
@SuppressWarnings("serial")
public class MbEnrichmentResult extends EntityDao<MbEnrichmentResult> implements java.io.Serializable {
	/**id*/
	private String id;
	/**富集文库*/
	private String fjwk;
	/**混合组号*/
	private String hhzh;
	/**通量*/
	private String tl;
	/**文库浓度（ng/ul）*/
	private String wknd;
	/**各组文库片段长度*/
	private String wkpdcd;
	/**测序类型*/
	private String cxlx;
	/**测序平台*/
	private String cxpt;
	/**测序读长*/
	private String cxdc;
	/**下一步流向*/
	private String nextFlow;
	/**备注*/
	private String note;
	/**相关主表*/
	private MbEnrichment mbEnrichment;
	/**浓度*/
	private String tzhwknd;
	/**体积*/
	private String tzhwktj;
	/**第一次稀释倍数*/
	private String dycxsbs;
	/**第一次补水量*/
	private String dycbsl;
	/**第二次稀释倍数*/
	private String decxsbs;
	/**第二次补水量*/
	private String decbsl;
	/**上机浓度*/
	private String pmol;
	/**临时表id*/
	private String tempId;
	/**ot2设备编号*/
	private String otNum;
	/**ot2设备状态*/
	private String otState;
	/**es设备编号*/
	private String esNum;
	/**es设备状态*/
	private String esState;
	/**chef设备编号*/
	private String chefNum;
	/**chef设备状态*/
	private String chefState;
	/**结果*/
	private String result;
	/**原始样本编号*/
	private String sampleCode;
	
	
	public String getSampleCode() {
		return sampleCode;
	}
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  富集文库
	 */
	@Column(name ="FJWK", length = 50)
	public String getFjwk(){
		return this.fjwk;
	}
	/**
	 *方法: 设置String
	 *@param: String  富集文库
	 */
	public void setFjwk(String fjwk){
		this.fjwk = fjwk;
	}
	/**
	 *方法: 取得String
	 *@return: String  混合组号
	 */
	@Column(name ="HHZH", length = 50)
	public String getHhzh(){
		return this.hhzh;
	}
	/**
	 *方法: 设置String
	 *@param: String  混合组号
	 */
	public void setHhzh(String hhzh){
		this.hhzh = hhzh;
	}
	/**
	 *方法: 取得String
	 *@return: String  通量
	 */
	@Column(name ="TL", length = 50)
	public String getTl(){
		return this.tl;
	}
	/**
	 *方法: 设置String
	 *@param: String  通量
	 */
	public void setTl(String tl){
		this.tl = tl;
	}
	/**
	 *方法: 取得String
	 *@return: String  文库浓度（ng/ul）
	 */
	@Column(name ="WKND", length = 50)
	public String getWknd(){
		return this.wknd;
	}
	/**
	 *方法: 设置String
	 *@param: String  文库浓度（ng/ul）
	 */
	public void setWknd(String wknd){
		this.wknd = wknd;
	}
	/**
	 *方法: 取得String
	 *@return: String  各组文库片段长度
	 */
	@Column(name ="WKPDCD", length = 50)
	public String getWkpdcd(){
		return this.wkpdcd;
	}
	/**
	 *方法: 设置String
	 *@param: String  各组文库片段长度
	 */
	public void setWkpdcd(String wkpdcd){
		this.wkpdcd = wkpdcd;
	}
	/**
	 *方法: 取得String
	 *@return: String  测序类型
	 */
	@Column(name ="CXLX", length = 50)
	public String getCxlx(){
		return this.cxlx;
	}
	/**
	 *方法: 设置String
	 *@param: String  测序类型
	 */
	public void setCxlx(String cxlx){
		this.cxlx = cxlx;
	}
	/**
	 *方法: 取得String
	 *@return: String  测序平台
	 */
	@Column(name ="CXPT", length = 50)
	public String getCxpt(){
		return this.cxpt;
	}
	/**
	 *方法: 设置String
	 *@param: String  测序平台
	 */
	public void setCxpt(String cxpt){
		this.cxpt = cxpt;
	}
	/**
	 *方法: 取得String
	 *@return: String  测序读长
	 */
	@Column(name ="CXDC", length = 50)
	public String getCxdc(){
		return this.cxdc;
	}
	/**
	 *方法: 设置String
	 *@param: String  测序读长
	 */
	public void setCxdc(String cxdc){
		this.cxdc = cxdc;
	}
	/**
	 *方法: 取得String
	 *@return: String  下一步流向
	 */
	@Column(name ="NEXT_FLOW", length = 50)
	public String getNextFlow(){
		return this.nextFlow;
	}
	/**
	 *方法: 设置String
	 *@param: String  下一步流向
	 */
	public void setNextFlow(String nextFlow){
		this.nextFlow = nextFlow;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得MbEnrichment
	 *@return: MbEnrichment  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "MB_ENRICHMENT")
	public MbEnrichment getMbEnrichment(){
		return this.mbEnrichment;
	}
	/**
	 *方法: 设置MbEnrichment
	 *@param: MbEnrichment  相关主表
	 */
	public void setMbEnrichment(MbEnrichment mbEnrichment){
		this.mbEnrichment = mbEnrichment;
	}
	/**
	 *方法: 取得String
	 *@return: String  浓度
	 */
	@Column(name ="TZHWKND", length = 50)
	public String getTzhwknd(){
		return this.tzhwknd;
	}
	/**
	 *方法: 设置String
	 *@param: String  浓度
	 */
	public void setTzhwknd(String tzhwknd){
		this.tzhwknd = tzhwknd;
	}
	/**
	 *方法: 取得String
	 *@return: String  体积
	 */
	@Column(name ="TZHWKTJ", length = 50)
	public String getTzhwktj(){
		return this.tzhwktj;
	}
	/**
	 *方法: 设置String
	 *@param: String  体积
	 */
	public void setTzhwktj(String tzhwktj){
		this.tzhwktj = tzhwktj;
	}
	/**
	 *方法: 取得String
	 *@return: String  第一次稀释倍数
	 */
	@Column(name ="DYCXSBS", length = 50)
	public String getDycxsbs(){
		return this.dycxsbs;
	}
	/**
	 *方法: 设置String
	 *@param: String  第一次稀释倍数
	 */
	public void setDycxsbs(String dycxsbs){
		this.dycxsbs = dycxsbs;
	}
	/**
	 *方法: 取得String
	 *@return: String  第一次补水量
	 */
	@Column(name ="DYCBSL", length = 50)
	public String getDycbsl(){
		return this.dycbsl;
	}
	/**
	 *方法: 设置String
	 *@param: String  第一次补水量
	 */
	public void setDycbsl(String dycbsl){
		this.dycbsl = dycbsl;
	}
	/**
	 *方法: 取得String
	 *@return: String  第二次稀释倍数
	 */
	@Column(name ="DECXSBS", length = 50)
	public String getDecxsbs(){
		return this.decxsbs;
	}
	/**
	 *方法: 设置String
	 *@param: String  第二次稀释倍数
	 */
	public void setDecxsbs(String decxsbs){
		this.decxsbs = decxsbs;
	}
	/**
	 *方法: 取得String
	 *@return: String  第二次补水量
	 */
	@Column(name ="DECBSL", length = 50)
	public String getDecbsl(){
		return this.decbsl;
	}
	/**
	 *方法: 设置String
	 *@param: String  第二次补水量
	 */
	public void setDecbsl(String decbsl){
		this.decbsl = decbsl;
	}
	/**
	 *方法: 取得String
	 *@return: String  上机浓度
	 */
	@Column(name ="PMOL", length = 50)
	public String getPmol(){
		return this.pmol;
	}
	/**
	 *方法: 设置String
	 *@param: String  上机浓度
	 */
	public void setPmol(String pmol){
		this.pmol = pmol;
	}
	/**
	 *方法: 取得String
	 *@return: String  临时表id
	 */
	@Column(name ="TEMP_ID", length = 50)
	public String getTempId(){
		return this.tempId;
	}
	/**
	 *方法: 设置String
	 *@param: String  临时表id
	 */
	public void setTempId(String tempId){
		this.tempId = tempId;
	}
	/**
	 *方法: 取得String
	 *@return: String  ot2设备编号
	 */
	@Column(name ="OT_NUM", length = 51)
	public String getOtNum(){
		return this.otNum;
	}
	/**
	 *方法: 设置String
	 *@param: String  ot2设备编号
	 */
	public void setOtNum(String otNum){
		this.otNum = otNum;
	}
	/**
	 *方法: 取得String
	 *@return: String  ot2设备状态
	 */
	@Column(name ="OT_STATE", length = 52)
	public String getOtState(){
		return this.otState;
	}
	/**
	 *方法: 设置String
	 *@param: String  ot2设备状态
	 */
	public void setOtState(String otState){
		this.otState = otState;
	}
	/**
	 *方法: 取得String
	 *@return: String  es设备编号
	 */
	@Column(name ="ES_NUM", length = 53)
	public String getEsNum(){
		return this.esNum;
	}
	/**
	 *方法: 设置String
	 *@param: String  es设备编号
	 */
	public void setEsNum(String esNum){
		this.esNum = esNum;
	}
	/**
	 *方法: 取得String
	 *@return: String  es设备状态
	 */
	@Column(name ="ES_STATE", length = 54)
	public String getEsState(){
		return this.esState;
	}
	/**
	 *方法: 设置String
	 *@param: String  es设备状态
	 */
	public void setEsState(String esState){
		this.esState = esState;
	}
	/**
	 *方法: 取得String
	 *@return: String  chef设备编号
	 */
	@Column(name ="CHEF_NUM", length = 55)
	public String getChefNum(){
		return this.chefNum;
	}
	/**
	 *方法: 设置String
	 *@param: String  chef设备编号
	 */
	public void setChefNum(String chefNum){
		this.chefNum = chefNum;
	}
	/**
	 *方法: 取得String
	 *@return: String  chef设备状态
	 */
	@Column(name ="CHEF_STATE", length = 56)
	public String getChefState(){
		return this.chefState;
	}
	/**
	 *方法: 设置String
	 *@param: String  chef设备状态
	 */
	public void setChefState(String chefState){
		this.chefState = chefState;
	}
	/**
	 *方法: 取得String
	 *@return: String  结果
	 */
	@Column(name ="RESULT", length = 57)
	public String getResult(){
		return this.result;
	}
	/**
	 *方法: 设置String
	 *@param: String  结果
	 */
	public void setResult(String result){
		this.result = result;
	}
}