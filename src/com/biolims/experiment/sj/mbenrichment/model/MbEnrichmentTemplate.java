package com.biolims.experiment.sj.mbenrichment.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.common.model.user.User;
/**   
 * @Title: Model
 * @Description: 执行步骤
 * @author lims-platform
 * @date 2017-08-28 14:18:38
 * @version V1.0   
 *
 */
@Entity
@Table(name = "MB_ENRICHMENT_TEMPLATE")
@SuppressWarnings("serial")
public class MbEnrichmentTemplate extends EntityDao<MbEnrichmentTemplate> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
	/**实验员*/
	private User reciveUser;
	/**模板步骤编id*/
	private String tItem;
	/**步骤编号*/
	private String code;
	/**步骤名称*/
	private String stepName;
	/**开始时间*/
	private String startTime;
	/**结束时间*/
	private String endTime;
	/**状态*/
	private String state;
	/**备注*/
	private String note;
	/**关联样本*/
	private String sampleCodes;
	/**相关主表*/
	private MbEnrichment mbEnrichment;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得User
	 *@return: User  实验员
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "RECIVE_USER")
	public User getReciveUser(){
		return this.reciveUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  实验员
	 */
	public void setReciveUser(User reciveUser){
		this.reciveUser = reciveUser;
	}
	/**
	 *方法: 取得String
	 *@return: String  模板步骤编id
	 */
	@Column(name ="T_ITEM", length = 50)
	public String getTItem(){
		return this.tItem;
	}
	/**
	 *方法: 设置String
	 *@param: String  模板步骤编id
	 */
	public void setTItem(String tItem){
		this.tItem = tItem;
	}
	/**
	 *方法: 取得String
	 *@return: String  步骤编号
	 */
	@Column(name ="CODE", length = 50)
	public String getCode(){
		return this.code;
	}
	/**
	 *方法: 设置String
	 *@param: String  步骤编号
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 *方法: 取得String
	 *@return: String  步骤名称
	 */
	@Column(name ="STEP_NAME", length = 50)
	public String getStepName(){
		return this.stepName;
	}
	/**
	 *方法: 设置String
	 *@param: String  步骤名称
	 */
	public void setStepName(String stepName){
		this.stepName = stepName;
	}
	/**
	 *方法: 取得String
	 *@return: String  开始时间
	 */
	@Column(name ="START_TIME", length = 50)
	public String getStartTime(){
		return this.startTime;
	}
	/**
	 *方法: 设置String
	 *@param: String  开始时间
	 */
	public void setStartTime(String startTime){
		this.startTime = startTime;
	}
	/**
	 *方法: 取得String
	 *@return: String  结束时间
	 */
	@Column(name ="END_TIME", length = 50)
	public String getEndTime(){
		return this.endTime;
	}
	/**
	 *方法: 设置String
	 *@param: String  结束时间
	 */
	public void setEndTime(String endTime){
		this.endTime = endTime;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得String
	 *@return: String  关联样本
	 */
	@Column(name ="SAMPLE_CODES", length = 50)
	public String getSampleCodes(){
		return this.sampleCodes;
	}
	/**
	 *方法: 设置String
	 *@param: String  关联样本
	 */
	public void setSampleCodes(String sampleCodes){
		this.sampleCodes = sampleCodes;
	}
	/**
	 *方法: 取得MbEnrichment
	 *@return: MbEnrichment  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "MB_ENRICHMENT")
	public MbEnrichment getMbEnrichment(){
		return this.mbEnrichment;
	}
	/**
	 *方法: 设置MbEnrichment
	 *@param: MbEnrichment  相关主表
	 */
	public void setMbEnrichment(MbEnrichment mbEnrichment){
		this.mbEnrichment = mbEnrichment;
	}
}