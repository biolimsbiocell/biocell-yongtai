package com.biolims.experiment.sj.mbenrichment.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.service.CommonService;
import com.biolims.experiment.sj.mbenrichment.dao.MbEnrichmentManageDao;
import com.biolims.experiment.sj.mbenrichment.model.MbEnrichmentTemp;
import com.biolims.experiment.sj.mbenrichment.model.MbEnrichmentWaitManage;
import com.biolims.experiment.sj.wklifeblend.dao.WkLifeBlendTaskDao;
import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTaskItem;
import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTaskResult;
import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTaskTemp;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.project.feedback.model.FeedbackDna;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class MbEnrichmentManageService {
	@Resource
	private MbEnrichmentManageDao mbEnrichmentManageDao;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private WkLifeBlendTaskDao wkLifeBlendTaskDao;
	@Resource
	private CommonService commonService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findExperimentDnaManageList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return mbEnrichmentManageDao.selectExperimentDnaManageList(
				mapForQuery, startNum, limitNum, dir, sort);
	}
//	/**
//	 * 样本结果管理明细取消
//	 * @param mapForQuery
//	 * @param startNum
//	 * @param limitNum
//	 * @param dir
//	 * @param sort
//	 * @return
//	 */
//	public void RemoveExperimentDnaManage(String id) {
//		
//		int coutn = mbEnrichmentManageDao.RemoveExperimentDnaManage(id);
//		if(coutn!=0){
//			SampleDnaTaskMoidfyTemp a = new SampleDnaTaskMoidfyTemp();
//			SampleDnaInfo b = commonDAO.get(SampleDnaInfo.class,id);
//			sampleInputService.copy(a, b);
//			a.setState("1");
//			experimentDnaGetDao.saveOrUpdate(a);	
//		}
//	}

	public Map<String, Object> findDnaGetItemList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return mbEnrichmentManageDao.selectDnaTaskItemList(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(MbEnrichmentWaitManage i) throws Exception {
		mbEnrichmentManageDao.saveOrUpdate(i);
	}

	public MbEnrichmentWaitManage get(String id) {
		MbEnrichmentWaitManage mbEnrichmentManage = commonDAO.get(
				MbEnrichmentWaitManage.class, id);
		return mbEnrichmentManage;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(MbEnrichmentWaitManage sc, Map jsonMap) throws Exception {
		if (sc != null) {
			mbEnrichmentManageDao.saveOrUpdate(sc);
			String jsonStr = "";
		}
	}

	/**
	 * 保存中间产物管理
	 * 
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveExperimentDnaInfoManager(String itemDataJson)
			throws Exception {
		List<WkLifeBlendTaskResult> saveItems = new ArrayList<WkLifeBlendTaskResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			WkLifeBlendTaskResult sbi = new WkLifeBlendTaskResult();
			sbi = (WkLifeBlendTaskResult) mbEnrichmentManageDao.Map2Bean(map, sbi);
			if (sbi.getId() != null && sbi.getId().equals(""))
				sbi.setId(null);
			saveItems.add(sbi);
//			if (sbi.getNextFlow() != null && sbi.getSubmit() != null) {
//				if (sbi.getSubmit().equals("1")) {
//					if (sbi.getNextFlow().equals("0")) {
//						// 到文库构建
//						WkTaskTemp wa = new WkTaskTemp();
//						wa.setCode(sbi.getCode());
//						wa.setSampleCode(sbi.getSampleCode());
//						wa.setSequenceFun(sbi.getSequenceFun());
//						wa.setPatientName(sbi.getPatientName());
//						wa.setPhone(sbi.getPhone());
//						wa.setProductId(sbi.getProductId());
//						wa.setProductName(sbi.getProductName());
//						wa.setInspectDate(sbi.getInspectDate());
//						wa.setOrderId(sbi.getOrderId());
//						wa.setIdCard(sbi.getIdCard());
//						wa.setState("1");
//						wa.setVolume(sbi.getVolume());
//						wa.setClassify(sbi.getClassify());
//						mbEnrichmentManageDao.saveOrUpdate(wa);
//						// 样本到建库，改变SampleInfo中原始样本的状态为“完成核酸提取”
//						SampleInfo sf = sampleInfoMainDao.findSampleInfo(sbi
//								.getSampleCode());
//						if (sf != null) {
//							sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_DNA_COMPLETE);
//							sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_DNA_COMPLETE_NAME);
//						}
//						sbi.setState("3");
//					} else if (sbi.getNextFlow().equals("1")) {
//						// 重新提取
//						DnaAbnormal da = new DnaAbnormal();
//						da.setClassify(sbi.getClassify());
//						da.setCode(sbi.getCode());
//						da.setFeedbackTime(DateUtil.dateFormatter(new Date()));
//						da.setIdCard(sbi.getIdCard());
//						da.setInspectDate(sbi.getInspectDate());
//						da.setNextFlow(sbi.getNextFlow());
//						da.setNote(sbi.getNote());
//						da.setOrderId(sbi.getOrderId());
//						da.setPatientName(sbi.getPatientName());
//						da.setPhone(sbi.getPhone());
//						da.setProductId(sbi.getProductId());
//						da.setProductName(sbi.getProductName());
//						da.setReportDate(sbi.getReportDate());
//						da.setResult(sbi.getResult());
//						da.setSampleCode(sbi.getSampleCode());
//						da.setSequenceFun(sbi.getSequenceFun());
//						this.mbEnrichmentManageDao.saveOrUpdate(da);
//						sbi.setState("3");
//					} else if (sbi.getNextFlow().equals("2")) { // 入库
//						SampleInItemTemp st = new SampleInItemTemp();
//						st.setSampleCode(sbi.getSampleCode());
//						st.setNum(sbi.getVolume());
//						st.setState("1");
//						mbEnrichmentManageDao.saveOrUpdate(st);
//						// 入库，改变SampleInfo中原始样本的状态为“待入库”
//						SampleInfo sf = sampleInfoMainDao.findSampleInfo(sbi
//								.getSampleCode());
//						if (sf != null) {
//							sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW);
//							sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW_NAME);
//						}
//						sbi.setState("3");
//					} else if (sbi.getNextFlow().equals("3")) {
//						// 反馈到项目组
//						FeedbackDna df = new FeedbackDna();
//						df.setCode(sbi.getCode());
//						df.setSampleCode(sbi.getSampleCode());
//						df.setSequenceFun(sbi.getSequenceFun());
//						df.setPatientName(sbi.getPatientName());
//						df.setPhone(sbi.getPhone());
//						df.setProductId(sbi.getProductId());
//						df.setProductName(sbi.getProductName());
//						df.setInspectDate(sbi.getInspectDate());
//						df.setReportDate(sbi.getReportDate());
//						df.setOrderId(sbi.getOrderId());
//						df.setIdCard(sbi.getIdCard());
//						df.setNextflow(sbi.getNextFlow());
//						df.setResult(sbi.getResult());
//						df.setClassify(sbi.getClassify());
//						df.setState("0");
//						mbEnrichmentManageDao.saveOrUpdate(df);
//						sbi.setState("3");
//					} else if (sbi.getNextFlow().equals("4")) {
//						// 终止，改变SampleInfo中原始样本的状态为“实验终止”
//						SampleInfo sf = sampleInfoMainDao.findSampleInfo(sbi
//								.getSampleCode());
//						if (sf != null) {
//							sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
//							sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
//						}
//					} else {
//						// 暂停，改变SampleInfo中原始样本的状态为“实验暂停”
//						SampleInfo sf = sampleInfoMainDao.findSampleInfo(sbi
//								.getSampleCode());
//						if (sf != null) {
//							sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
//							sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
//						}
//					}
//				}
//			}
		}
		mbEnrichmentManageDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 保存样本管理(DnaTaskItem)
	 * 
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveDnaGetItemManager(String itemDataJson) throws Exception {
		List<WkLifeBlendTaskItem> saveItems = new ArrayList<WkLifeBlendTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			WkLifeBlendTaskItem sbi = new WkLifeBlendTaskItem();
			sbi = (WkLifeBlendTaskItem) mbEnrichmentManageDao.Map2Bean(map, sbi);
			if (sbi.getId() != null && sbi.getId().equals(""))
				sbi.setId(null);
			saveItems.add(sbi);
//			if (sbi.getNextFlow() != null && sbi.getResult() != null) {
//				if (sbi.getResult().equals("1")) {
//					if (sbi.getNextFlow().equals("0")) {
//						SampleInItemTemp st = new SampleInItemTemp();
//						if (sbi.getCode() == null) {
//							st.setCode(sbi.getSampleCode());
//						} else {
//							st.setCode(sbi.getCode());
//						}
//						st.setSampleCode(sbi.getSampleCode());
//						if (sbi.getSampleNum() != null
//								&& sbi.getSampleConsume() != null) {
//							st.setNum(sbi.getSampleNum()
//									- sbi.getSampleConsume());
//						}
//						st.setState("1");
//						mbEnrichmentManageDao.saveOrUpdate(st);
//						// 入库，改变SampleInfo中原始样本的状态为“待入库”
//						SampleInfo sf = sampleInfoMainDao.findSampleInfo(sbi
//								.getSampleCode());
//						if (sf != null) {
//							sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW);
//							sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW_NAME);
//						}
//						sbi.setState("2");
//					} else {
//						// 终止，改变SampleInfo中原始样本的状态为“实验终止”
//						SampleInfo sf = sampleInfoMainDao.findSampleInfo(sbi
//								.getSampleCode());
//						if (sf != null) {
//							sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
//							sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
//						}
//					}
//				}
//			}
		}
		mbEnrichmentManageDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 核酸样本管理明细入库
	 * 
	 * @param ids
	 */
	public void dnaGetManageItemRuku(String ids) {
		String[] id1 = ids.split(",");
		for (String id : id1) {
			WkLifeBlendTaskItem scp = mbEnrichmentManageDao.get(WkLifeBlendTaskItem.class, id);
			if (scp != null) {
				scp.setState("2");
				SampleInItemTemp st = new SampleInItemTemp();
				st.setSampleCode(scp.getSampleCode());
//				st.setCode(scp.getCode());
				st.setCode(scp.getFjwk());
//				if (scp.getSampleNum() != null
//						&& scp.getSampleConsume() != null) {
//					st.setNum(scp.getSampleNum() - scp.getSampleConsume());
//				}
				st.setState("1");
				mbEnrichmentManageDao.saveOrUpdate(st);
			}
		}
	}

	/**
	 * 核酸样本管理左侧待选
	 * 
	 * @param id
	 */
	public void dnaGetManageItemTiqu(String id) {
		WkLifeBlendTaskItem scp = mbEnrichmentManageDao.get(WkLifeBlendTaskItem.class, id);
		if (scp != null) {
			scp.setState("2");
			WkLifeBlendTaskTemp st = new WkLifeBlendTaskTemp();
//			st.setCode(scp.getCode());
//			st.setSampleCode(scp.getSampleCode());
//			st.setLabCode(scp.getLabCode());
//			st.setProductId(scp.getProductId());
//			st.setProductName(scp.getProductName());
//			st.setSampleType(scp.getSampleType());
//			st.setPatientName(scp.getPatientName());
//			st.setReportDate(scp.getReportDate());
//			st.setSampleType(scp.getSampleType());

//			if (scp.getSampleNum() != null && scp.getSampleConsume() != null) {
//				st.setSampleNum(scp.getSampleNum() - scp.getSampleConsume());
//			}
			st.setState("1");
			mbEnrichmentManageDao.saveOrUpdate(st);
		}
	}
	public Boolean getTempTable(String id, String code) {
		// TODO Auto-generated method stub
		if(!"".equals(id) && id !=null && !"".equals(code) && code !=null ){
			String flag =  mbEnrichmentManageDao.getTempTable(id,code);
			if("1".equals(flag)){
				return true;
			}
		}
		return false;
	}

	/**
	 * 样本结果管理明细取消
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 */
	public void RemoveExperimentDnaManage(String id,String code,String iscz) {
		
//		int coutn = 
//		mbEnrichmentManageDao.RemoveExperimentDnaManage(id,code,iscz);
		if("0".equals(iscz)){
			WkLifeBlendTaskResult scp = mbEnrichmentManageDao.get(WkLifeBlendTaskResult.class, id);
			if(scp!=null){
				MbEnrichmentTemp sill = new MbEnrichmentTemp();
				sampleInputService.copy(sill, scp);
				sill.setState("1");
				mbEnrichmentManageDao.saveOrUpdate(sill);
			}
		}else if("1".equals(iscz)){
			List<MbEnrichmentTemp> sil = commonService.get(MbEnrichmentTemp.class, "hhzh",
					code);
			if(sil.size()>0){
				MbEnrichmentTemp sill = sil.get(0);
				sill.setState("1");
				mbEnrichmentManageDao.saveOrUpdate(sill);
			}
		}
//		if(coutn!=0){
//			SampleDnaTaskMoidfyTemp a = new SampleDnaTaskMoidfyTemp();
//			SampleDnaInfo b = commonDAO.get(SampleDnaInfo.class,id);
//			sampleInputService.copy(a, b);
//			a.setState("1");
//			experimentDnaGetDao.saveOrUpdate(a);	
//		}
	}
	
}
