package com.biolims.experiment.sj.mbenrichment.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.sequencingLife.model.SequencingLifeTaskTemp;
import com.biolims.experiment.sj.mbenrichment.dao.MbEnrichmentDao;
import com.biolims.experiment.sj.mbenrichment.model.MbEnrichment;
import com.biolims.experiment.sj.mbenrichment.model.MbEnrichmentCos;
import com.biolims.experiment.sj.mbenrichment.model.MbEnrichmentItem;
import com.biolims.experiment.sj.mbenrichment.model.MbEnrichmentReagent;
import com.biolims.experiment.sj.mbenrichment.model.MbEnrichmentResult;
import com.biolims.experiment.sj.mbenrichment.model.MbEnrichmentTemp;
import com.biolims.experiment.sj.mbenrichment.model.MbEnrichmentTemplate;
import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTaskItem;
import com.biolims.experiment.wkLife.model.SampleWkLifeInfo;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class MbEnrichmentService {
	@Resource
	private MbEnrichmentDao mbEnrichmentDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findMbEnrichmentList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return mbEnrichmentDao.selectMbEnrichmentList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(MbEnrichment i) throws Exception {

		mbEnrichmentDao.saveOrUpdate(i);

	}
	public MbEnrichment get(String id) {
		MbEnrichment mbEnrichment = commonDAO.get(MbEnrichment.class, id);
		return mbEnrichment;
	}
	public Map<String, Object> findMbEnrichmentItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = mbEnrichmentDao.selectMbEnrichmentItemList(scId, startNum, limitNum, dir, sort);
		List<MbEnrichmentItem> list = (List<MbEnrichmentItem>) result.get("list");
		return result;
	}
	public Map<String, Object> findMbEnrichmentTemplateList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = mbEnrichmentDao.selectMbEnrichmentTemplateList(scId, startNum, limitNum, dir, sort);
		List<MbEnrichmentTemplate> list = (List<MbEnrichmentTemplate>) result.get("list");
		return result;
	}
	public Map<String, Object> findMbEnrichmentReagentList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = mbEnrichmentDao.selectMbEnrichmentReagentList(scId, startNum, limitNum, dir, sort);
		List<MbEnrichmentReagent> list = (List<MbEnrichmentReagent>) result.get("list");
		return result;
	}
	public Map<String, Object> findMbEnrichmentCosList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = mbEnrichmentDao.selectMbEnrichmentCosList(scId, startNum, limitNum, dir, sort);
		List<MbEnrichmentCos> list = (List<MbEnrichmentCos>) result.get("list");
		return result;
	}
	public Map<String, Object> findMbEnrichmentResultList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = mbEnrichmentDao.selectMbEnrichmentResultList(scId, startNum, limitNum, dir, sort);
		List<MbEnrichmentResult> list = (List<MbEnrichmentResult>) result.get("list");
		return result;
	}
	public Map<String, Object> findMbEnrichmentTempList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = mbEnrichmentDao.selectMbEnrichmentTempList(scId, startNum, limitNum, dir, sort);
		List<MbEnrichmentTemp> list = (List<MbEnrichmentTemp>) result.get("list");
		return result;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveMbEnrichmentItem(MbEnrichment sc, String itemDataJson) throws Exception {
		List<MbEnrichmentItem> saveItems = new ArrayList<MbEnrichmentItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			MbEnrichmentItem scp = new MbEnrichmentItem();
			// 将map信息读入实体类
			scp = (MbEnrichmentItem) mbEnrichmentDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setMbEnrichment(sc);

			saveItems.add(scp);
			// 改变左侧样本状态
			MbEnrichmentTemp t = this.commonDAO.get(MbEnrichmentTemp.class,
					scp.getTempId());
			if (t != null) {
				t.setState("2");
			}
			
		}
		mbEnrichmentDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delMbEnrichmentItem(String[] ids) throws Exception {
		for (String id : ids) {
			MbEnrichmentItem scp =  mbEnrichmentDao.get(MbEnrichmentItem.class, id);
			 mbEnrichmentDao.delete(scp);
			 
			 MbEnrichmentTemp t = this.commonDAO.get(MbEnrichmentTemp.class,
						scp.getTempId());
			if (t != null) {
				t.setState("1");
			}
		}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveMbEnrichmentTemplate(MbEnrichment sc, String itemDataJson) throws Exception {
		List<MbEnrichmentTemplate> saveItems = new ArrayList<MbEnrichmentTemplate>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			MbEnrichmentTemplate scp = new MbEnrichmentTemplate();
			// 将map信息读入实体类
			scp = (MbEnrichmentTemplate) mbEnrichmentDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setMbEnrichment(sc);

			saveItems.add(scp);
		}
		mbEnrichmentDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delMbEnrichmentTemplate(String[] ids) throws Exception {
		for (String id : ids) {
			MbEnrichmentTemplate scp =  mbEnrichmentDao.get(MbEnrichmentTemplate.class, id);
			 mbEnrichmentDao.delete(scp);
		}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveMbEnrichmentReagent(MbEnrichment sc, String itemDataJson) throws Exception {
		List<MbEnrichmentReagent> saveItems = new ArrayList<MbEnrichmentReagent>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			MbEnrichmentReagent scp = new MbEnrichmentReagent();
			// 将map信息读入实体类
			scp = (MbEnrichmentReagent) mbEnrichmentDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setMbEnrichment(sc);

			saveItems.add(scp);
		}
		mbEnrichmentDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delMbEnrichmentReagent(String[] ids) throws Exception {
		for (String id : ids) {
			MbEnrichmentReagent scp =  mbEnrichmentDao.get(MbEnrichmentReagent.class, id);
			 mbEnrichmentDao.delete(scp);
		}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveMbEnrichmentCos(MbEnrichment sc, String itemDataJson) throws Exception {
		List<MbEnrichmentCos> saveItems = new ArrayList<MbEnrichmentCos>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			MbEnrichmentCos scp = new MbEnrichmentCos();
			// 将map信息读入实体类
			scp = (MbEnrichmentCos) mbEnrichmentDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setMbEnrichment(sc);

			saveItems.add(scp);
		}
		mbEnrichmentDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delMbEnrichmentCos(String[] ids) throws Exception {
		for (String id : ids) {
			MbEnrichmentCos scp =  mbEnrichmentDao.get(MbEnrichmentCos.class, id);
			 mbEnrichmentDao.delete(scp);
		}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveMbEnrichmentResult(MbEnrichment sc, String itemDataJson) throws Exception {
		List<MbEnrichmentResult> saveItems = new ArrayList<MbEnrichmentResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			MbEnrichmentResult scp = new MbEnrichmentResult();
			// 将map信息读入实体类
			scp = (MbEnrichmentResult) mbEnrichmentDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setMbEnrichment(sc);

			saveItems.add(scp);
		}
		mbEnrichmentDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delMbEnrichmentResult(String[] ids) throws Exception {
		for (String id : ids) {
			MbEnrichmentResult scp =  mbEnrichmentDao.get(MbEnrichmentResult.class, id);
			 mbEnrichmentDao.delete(scp);
		}
	}
	
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void saveMbEnrichmentTemp(MbEnrichment sc, String itemDataJson) throws Exception {
//		List<MbEnrichmentTemp> saveItems = new ArrayList<MbEnrichmentTemp>();
//		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
//		for (Map<String, Object> map : list) {
//			MbEnrichmentTemp scp = new MbEnrichmentTemp();
//			// 将map信息读入实体类
//			scp = (MbEnrichmentTemp) mbEnrichmentDao.Map2Bean(map, scp);
//			if (scp.getId() != null && scp.getId().equals(""))
//				scp.setId(null);
//			scp.setMbEnrichment(sc);
//
//			saveItems.add(scp);
//		}
//		mbEnrichmentDao.saveOrUpdateAll(saveItems);
//	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delMbEnrichmentTemp(String[] ids) throws Exception {
		for (String id : ids) {
			MbEnrichmentTemp scp =  mbEnrichmentDao.get(MbEnrichmentTemp.class, id);
			 mbEnrichmentDao.delete(scp);
		}
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(MbEnrichment sc, Map jsonMap) throws Exception {
		if (sc != null) {
			mbEnrichmentDao.saveOrUpdate(sc);
		
			String jsonStr = "";
			jsonStr = (String)jsonMap.get("mbEnrichmentItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveMbEnrichmentItem(sc, jsonStr);
			}
			jsonStr = (String)jsonMap.get("mbEnrichmentTemplate");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveMbEnrichmentTemplate(sc, jsonStr);
			}
			jsonStr = (String)jsonMap.get("mbEnrichmentReagent");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveMbEnrichmentReagent(sc, jsonStr);
			}
			jsonStr = (String)jsonMap.get("mbEnrichmentCos");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveMbEnrichmentCos(sc, jsonStr);
			}
			jsonStr = (String)jsonMap.get("mbEnrichmentResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveMbEnrichmentResult(sc, jsonStr);
			}
//			jsonStr = (String)jsonMap.get("mbEnrichmentTemp");
//			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
//				saveMbEnrichmentTemp(sc, jsonStr);
//			}
	}
   }
	
	/**
	 * 根据选中的步骤查询相关的试剂明细
	 * 
	 * @param id
	 * @param code
	 * @return
	 */
	public List<Map<String, String>> setReagent(String id, String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = mbEnrichmentDao.setReagent(id, code);
		List<MbEnrichmentReagent> list = (List<MbEnrichmentReagent>) result
				.get("list");
		if (list != null && list.size() > 0) {
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			for (MbEnrichmentReagent ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("code", ti.getCode());
				map.put("name", ti.getName());
				map.put("batch", ti.getBatch());
				map.put("isGood", ti.getIsGood());
				map.put("note", ti.getNote());
//				map.put("tReagent", ti.getReagentCode());
				map.put("tReagent", "");
				if (ti.getOneNum() != null) {
					map.put("oneNum", ti.getOneNum().toString());
				} else {
					map.put("oneNum", "");
				}
				if (ti.getNum() != null) {
					map.put("sampleNum", ti.getSampleNum().toString());
				} else {
					map.put("sampleNum", "");
				}
				if (ti.getNum() != null) {
					map.put("num", ti.getNum().toString());
				} else {
					map.put("num", "");
				}
//				if(ti.getExpireDate()!=null){
//					map.put("expireDate", sdf.format(ti.getExpireDate()));
//				}
//				map.put("sn", ti.getSn());
				map.put("itemId", ti.getItemId());
				map.put("tId", ti.getMbEnrichment().getId());
				map.put("tName", ti.getMbEnrichment().getName());
				mapList.add(map);
			}
		}
		return mapList;
	}

	/**
	 * 根据选中的步骤查询相关的设备明细
	 * 
	 * @param id
	 * @param code
	 * @return
	 */
	public List<Map<String, String>> setCos(String id, String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = mbEnrichmentDao.setCos(id, code);
		List<MbEnrichmentCos> list = (List<MbEnrichmentCos>) result.get("list");
		if (list != null && list.size() > 0) {
			for (MbEnrichmentCos ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("code", ti.getCode());
				map.put("name", ti.getName());
				map.put("isGood", ti.getIsGood());
				if (ti.getTemperature() != null) {
					map.put("temperature", ti.getTemperature().toString());
				} else {
					map.put("temperature", "");
				}
				if (ti.getSpeed() != null) {
					map.put("speed", ti.getSpeed().toString());
				} else {
					map.put("speed", "");
				}
				if (ti.getTime() != null) {
					map.put("time", ti.getTime().toString());
				} else {
					map.put("time", "");
				}
//				if (ti.getType() != null) {
//					map.put("typeId", ti.getType().getId());
//					map.put("typeName", ti.getType().getName());
//				} else {
					map.put("typeId", "");
					map.put("typeName", "");
//				}
//				map.put("state", ti.getState());
				map.put("itemId", ti.getItemId());
				map.put("note", ti.getNote());
				map.put("tCos", ti.getTCos());
				map.put("tId", ti.getMbEnrichment().getId());
				map.put("tName", ti.getMbEnrichment().getName());
				mapList.add(map);
			}
		}
		return mapList;
	}
	
	// 审核完成
		@WriteOperLogTable
		@WriteExOperLog
		@Transactional(rollbackFor = Exception.class)
		public void changeState(String applicationTypeActionId, String id)
				throws Exception {
			MbEnrichment sct = mbEnrichmentDao.get(MbEnrichment.class, id);
			sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
			sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
			User user = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			sct.setConfirmUser(user);
			sct.setConfirmDate(new Date());
			mbEnrichmentDao.update(sct);

			// 将批次信息反馈到模板中
//			List<WkBlendTaskReagent> list1 = mbEnrichmentDao.setReagentList(id);
//			for (WkBlendTaskReagent dt : list1) {
//				String bat1 = dt.getBatch(); // 实验中试剂的批次
//				String drid = dt.getTReagent(); // 实验中保存的试剂ID
//				List<ReagentItem> list2 = mbEnrichmentDao.setReagentListById(drid);
//				for (ReagentItem ri : list2) {
//					if (bat1 != null) {
//						ri.setBatch(bat1);
//					}
//				}
//				// 改变库存主数据试剂数量
//				this.storageService.UpdateStorageAndItemNum(dt.getCode(),
//						dt.getBatch(), dt.getNum());
//			}

			// 获取结果表样本信息
			List<MbEnrichmentResult> list = this.mbEnrichmentDao
					.setWkBlendResultById(id);
			for (MbEnrichmentResult scp : list) {
				if (scp != null) {
					if("0".equals(scp.getResult())){
						if (scp.getNextFlow() != null) {
							if (scp.getNextFlow().equals("0")) {// 上机
								SequencingLifeTaskTemp q = new SequencingLifeTaskTemp();
								sampleInputService.copy(q, scp);
//								q.setPoolingCode(scp.getSjfz());
								q.setPoolingId(scp.getHhzh());
								q.setSequenceType(scp.getCxlx());
								q.setSequencingPlatform(scp.getCxpt());
								q.setSequencingReadLong(scp.getCxdc());
								q.setState("1");
								q.setSampleCode(scp.getSampleCode());
								this.mbEnrichmentDao.saveOrUpdate(q);
								
								
								DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
								// 根据pooling号查询文库信息
								List<WkLifeBlendTaskItem> listItem1 = this.mbEnrichmentDao
										.setWkBlendTaskItemBysjfz(scp.getHhzh());
								for (WkLifeBlendTaskItem w : listItem1) {
									// 根据富集文库查询明细样本
									List<SampleWkLifeInfo> listPoolingTaskItem = this.mbEnrichmentDao
											.getItemListByfjwk(w.getFjwk());
									for (SampleWkLifeInfo p : listPoolingTaskItem) {
										sampleStateService
												.saveSampleState(
														scp.getHhzh(),
														p.getSampleInfo().getCode(),
														p.getProductId(),
														p.getProductName(),
														"",
														format.format(sct.getCreateDate()),
														format.format(new Date()),
														"MbEnrichment",
														"模板富集",
														(User) ServletActionContext
																.getRequest()
																.getSession()
																.getAttribute(
																		SystemConstants.USER_SESSION_KEY),
														id, "上机测序life", "1", null,
														null, null, null, null, null, null,
														null);
									}
								}
							} else {// 终止
								DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
								// 根据pooling号查询文库信息
								List<WkLifeBlendTaskItem> listItem1 = this.mbEnrichmentDao
										.setWkBlendTaskItemBysjfz(scp.getHhzh());
								for (WkLifeBlendTaskItem w : listItem1) {
									// 根据富集文库查询明细样本
									List<SampleWkLifeInfo> listPoolingTaskItem = this.mbEnrichmentDao
											.getItemListByfjwk(w.getFjwk());
									for (SampleWkLifeInfo p : listPoolingTaskItem) {
										sampleStateService
												.saveSampleState(
														scp.getHhzh(),
														p.getSampleInfo().getCode(),
														p.getProductId(),
														p.getProductName(),
														"",
														format.format(sct.getCreateDate()),
														format.format(new Date()),
														"MbEnrichment",
														"模板富集",
														(User) ServletActionContext
																.getRequest()
																.getSession()
																.getAttribute(
																		SystemConstants.USER_SESSION_KEY),
														id, "终止", "0", null,
														null, null, null, null, null, null,
														null);
									}
								}
							}
						}
					}else{
						DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						// 根据pooling号查询文库信息
						List<WkLifeBlendTaskItem> listItem1 = this.mbEnrichmentDao
								.setWkBlendTaskItemBysjfz(scp.getHhzh());
						for (WkLifeBlendTaskItem w : listItem1) {
							// 根据富集文库查询明细样本
							List<SampleWkLifeInfo> listPoolingTaskItem = this.mbEnrichmentDao
									.getItemListByfjwk(w.getFjwk());
							for (SampleWkLifeInfo p : listPoolingTaskItem) {
								sampleStateService
										.saveSampleState(
												scp.getHhzh(),
												p.getSampleInfo().getCode(),
												p.getProductId(),
												p.getProductName(),
												"",
												format.format(sct.getCreateDate()),
												format.format(new Date()),
												"MbEnrichment",
												"模板富集",
												(User) ServletActionContext
														.getRequest()
														.getSession()
														.getAttribute(
																SystemConstants.USER_SESSION_KEY),
												id, "终止", "0", null,
												null, null, null, null, null, null,
												null);
							}
						}
					}
					
					
				}
			}

		}
}
