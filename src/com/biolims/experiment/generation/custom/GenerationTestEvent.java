package com.biolims.experiment.generation.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.generation.service.GenerationTestService;

public class GenerationTestEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		GenerationTestService gtService = (GenerationTestService) ctx.getBean("generationTestService");
		gtService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
