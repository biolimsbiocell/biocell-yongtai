package com.biolims.experiment.generation.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.common.model.user.User;
/**   
 * @Title: Model
 * @Description: 测序样本
 * @author lims-platform
 * @date 2015-11-24 11:47:52
 * @version V1.0   
 *
 */
@Entity
@Table(name = "GENERATION_TASK_ITEM")
@SuppressWarnings("serial")
public class GenerationTaskItem extends EntityDao<GenerationTaskItem> implements java.io.Serializable {
	/**id*/
	private String id;
	/**描述*/
	private String name;
	/**样本编号*/
	private String sampleCode;
	/**实验员*/
	private User reciveUser;
	/**实验时间*/
	private String reciveDate;
	/**检测项目*/
	private String project;
	/**浓度*/
	private Double concentration;
	/**是否合格*/
	private String testQualife;
	/**失败原因*/
	private String failReasons;
	/*状态*/
	private String state;
	/**备注*/
	private String note;
	/**相关主表*/
	private GenerationTask generationTest;
	
	/**临时表Id*/
	private String tempId;
	/**
	 *方法: 取得String
	 *@return: String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本编号
	 */
	@Column(name ="SAMPLE_CODE", length = 50)
	public String getSampleCode(){
		return this.sampleCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本编号
	 */
	public void setSampleCode(String sampleCode){
		this.sampleCode = sampleCode;
	}
	/**
	 *方法: 取得User
	 *@return: User  实验员
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "RECIVE_USER")
	public User getReciveUser(){
		return this.reciveUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  实验员
	 */
	public void setReciveUser(User reciveUser){
		this.reciveUser = reciveUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  实验时间
	 */
	@Column(name ="RECIVE_DATE", length = 50)
	public String getReciveDate(){
		return this.reciveDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  实验时间
	 */
	public void setReciveDate(String reciveDate){
		this.reciveDate = reciveDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  检测项目
	 */
	@Column(name ="PROJECT", length = 50)
	public String getProject(){
		return this.project;
	}
	/**
	 *方法: 设置String
	 *@param: String  检测项目
	 */
	public void setProject(String project){
		this.project = project;
	}
	/**
	 *方法: 取得Double
	 *@return: Double  浓度
	 */
	@Column(name ="CONCENTRATION", length = 50)
	public Double getConcentration(){
		return this.concentration;
	}
	/**
	 *方法: 设置Double
	 *@param: Double  浓度
	 */
	public void setConcentration(Double concentration){
		this.concentration = concentration;
	}
	/**
	 *方法: 取得String
	 *@return: String  是否合格
	 */
	@Column(name ="TEST_QUALIFE", length = 50)
	public String getTestQualife(){
		return this.testQualife;
	}
	/**
	 *方法: 设置String
	 *@param: String  是否合格
	 */
	public void setTestQualife(String testQualife){
		this.testQualife = testQualife;
	}
	/**
	 *方法: 取得String
	 *@return: String  失败原因
	 */
	@Column(name ="FAIL_REASONS", length = 50)
	public String getFailReasons(){
		return this.failReasons;
	}
	/**
	 *方法: 设置String
	 *@param: String  失败原因
	 */
	public void setFailReasons(String failReasons){
		this.failReasons = failReasons;
	}
	@Column(name ="STATE", length = 50)
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得TpTest
	 *@return: GenerationTest  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GENERATION_TEST")
	public GenerationTask getGenerationTest(){
		return this.generationTest;
	}
	/**
	 *方法: 设置TpTest
	 *@param: GenerationTest  相关主表
	 */
	public void setGenerationTest(GenerationTask generationTest){
		this.generationTest = generationTest;
	}
	public String getTempId() {
		return tempId;
	}
	public void setTempId(String tempId) {
		this.tempId = tempId;
	}
	
}