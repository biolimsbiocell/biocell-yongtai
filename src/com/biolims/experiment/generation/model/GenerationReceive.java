package com.biolims.experiment.generation.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.storage.position.model.StoragePosition;
/**   
 * @Title: Model
 * @Description: 血浆接收
 * @author lims-platform
 * @date 2015-11-18 17:25:58
 * @version V1.0   
 *
 */
@Entity
@Table(name = "GENERATION_RECEIVE")
@SuppressWarnings("serial")
public class GenerationReceive extends EntityDao<GenerationReceive> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**名称*/
	private String name;
	/**描述*/
	private String describe;
	/**接收人*/
	private User receiver;
	/**接收日期*/
	private Date receiverDate;
	/**储位提示*/
	private StoragePosition storagePrompt;
	/**工作流状态ID*/
	private String state;
	/*工作流状态*/
	private String stateName;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  名称
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="DESCRIBE_", length = 50)
	public String getDescribe(){
		return this.describe;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setDescribe(String describe){
		this.describe = describe;
	}
	/**
	 *方法: 取得User
	 *@return: User  接收人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "RECEIVER")
	public User getReceiver(){
		return this.receiver;
	}
	/**
	 *方法: 设置User
	 *@param: User  接收人
	 */
	public void setReceiver(User receiver){
		this.receiver = receiver;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  接收日期
	 */
	@Column(name ="RECEIVER_DATE", length = 50)
	public Date getReceiverDate(){
		return this.receiverDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  接收日期
	 */
	public void setReceiverDate(Date receiverDate){
		this.receiverDate = receiverDate;
	}
	/**
	 *方法: 取得StoragePosition
	 *@return: StoragePosition  储位提示
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_PROMPT")
	public StoragePosition getStoragePrompt(){
		return this.storagePrompt;
	}
	/**
	 *方法: 设置StoragePosition
	 *@param: StoragePosition  储位提示
	 */
	public void setStoragePrompt(StoragePosition storagePrompt){
		this.storagePrompt = storagePrompt;
	}
	@Column(name ="STATE", length = 50)
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@Column(name ="STATE_NAME", length = 50)
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	
}