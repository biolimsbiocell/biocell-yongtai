package com.biolims.experiment.generation.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 试剂明细
 * @author lims-platform
 * @date 2015-11-24 11:47:58
 * @version V1.0   
 *
 */
@Entity
@Table(name = "GENERATION_TASK_REAGENT")
@SuppressWarnings("serial")
public class GenerationTaskGeagent extends EntityDao<GenerationTaskGeagent> implements java.io.Serializable {
	/**id*/
	private String id;
	/**试剂编号*/
	private String code;
	/**名称*/
	private String name;
	/**批次*/
	private String batch;
	/**数量*/
	private Integer count;
	//单个用量
	private Double oneNum;
	//样本数量
	private Double sampleNum;
	//用量
	private Double num;	
	//模板试剂ID
	private String tReagent;
	//关联步骤编号
	private String itemId;
	/**是否通过检验*/
	private String isGood;
	/**相关主表*/
	private GenerationTask generationTesk;
	/**
	 *方法: 取得String
	 *@return: String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
//	/**
//	 *方法: 取得String
//	 *@return: String  试剂编号
//	 */
//	@Column(name ="REAGENT_ID", length = 50)
//	public String getReagentId(){
//		return this.reagentId;
//	}
//	/**
//	 *方法: 设置String
//	 *@param: String  试剂编号
//	 */
//	public void setReagentId(String reagentId){
//		this.reagentId = reagentId;
//	}
//	/**
//	 *方法: 取得String
//	 *@return: String  名称
//	 */
//	@Column(name ="DESIGNATION", length = 50)
//	public String getDesignation(){
//		return this.designation;
//	}
//	/**
//	 *方法: 设置String
//	 *@param: String  名称
//	 */
//	public void setDesignation(String designation){
//		this.designation = designation;
//	}
	/**
	 *方法: 取得String
	 *@return: String  批次
	 */
	@Column(name ="BATCH", length = 50)
	public String getBatch(){
		return this.batch;
	}
	/**
	 *方法: 设置String
	 *@param: String  批次
	 */
	public void setBatch(String batch){
		this.batch = batch;
	}
	/**
	 *方法: 取得Integer
	 *@return: Integer  数量
	 */
	@Column(name ="COUNT", length = 50)
	public Integer getCount(){
		return this.count;
	}
	/**
	 *方法: 设置Integer
	 *@param: Integer  数量
	 */
	public void setCount(Integer count){
		this.count = count;
	}
	/**
	 *方法: 取得String
	 *@return: String  是否通过检验
	 */
	@Column(name ="IS_GOOD", length = 50)
	public String getIsGood(){
		return this.isGood;
	}
	/**
	 *方法: 设置String
	 *@param: String  是否通过检验
	 */
	public void setIsGood(String isGood){
		this.isGood = isGood;
	}
	/**
	 *方法: 取得TpTest
	 *@return: GenerationTesk  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GENERATION_TEST")
	public GenerationTask getGenerationTesk(){
		return this.generationTesk;
	}
	/**
	 *方法: 设置TpTest
	 *@param: GenerationTesk  相关主表
	 */
	public void setGenerationTesk(GenerationTask generationTesk){
		this.generationTesk = generationTesk;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Double getOneNum() {
		return oneNum;
	}
	public void setOneNum(Double oneNum) {
		this.oneNum = oneNum;
	}
	public Double getSampleNum() {
		return sampleNum;
	}
	public void setSampleNum(Double sampleNum) {
		this.sampleNum = sampleNum;
	}
	public Double getNum() {
		return num;
	}
	public void setNum(Double num) {
		this.num = num;
	}
	public String gettReagent() {
		return tReagent;
	}
	public void settReagent(String tReagent) {
		this.tReagent = tReagent;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	
}