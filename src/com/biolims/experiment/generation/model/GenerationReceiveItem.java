package com.biolims.experiment.generation.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.storage.position.model.StoragePosition;
/**   
 * @Title: Model
 * @Description: 血浆接收明细
 * @author lims-platform
 * @date 2015-11-18 17:25:44
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SAMPLE_GENERATION_ITEM")
@SuppressWarnings("serial")
public class GenerationReceiveItem extends EntityDao<GenerationReceiveItem> implements java.io.Serializable {
	/**id*/
	private String id;
	/**样本编号*/
	private String code;
	/**原始样本编号*/
	private String sampleCode;
	
	public String getSampleCode() {
		return sampleCode;
	}
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}
	/**描述*/
	private String name;
	/**姓名*/
	private String sname;
	/**检测项目*/
	private String testPro;
	/*处理方式*/
	private String method;
	/*原因*/
	private String reason;
	/*备注*/
	private String note;
	/**储位*/
	private StoragePosition storageLocal;
	/**状态*/
	private String state;
	/**关联主表*/
	private GenerationReceive generationReceiveTask;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GENERATION_RECEIVE_TASK")
	public GenerationReceive getGenerationReceiveTask() {
		return generationReceiveTask;
	}
	public void setGenerationReceiveTask(GenerationReceive generationReceiveTask) {
		this.generationReceiveTask = generationReceiveTask;
	}
	/**
	 *方法: 取得String
	 *@return: String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本编号
	 */
	@Column(name ="CODE", length = 50)
	public String getCode(){
		return this.code;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本编号
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  姓名
	 */
	@Column(name ="SNAME", length = 50)
	public String getSname(){
		return this.sname;
	}
	/**
	 *方法: 设置String
	 *@param: String  姓名
	 */
	public void setSname(String sname){
		this.sname = sname;
	}
	/**
	 *方法: 取得String
	 *@return: String  检测项目
	 */
	@Column(name ="TEST_PRO", length = 50)
	public String getTestPro(){
		return this.testPro;
	}
	/**
	 *方法: 设置String
	 *@param: String  检测项目
	 */
	public void setTestPro(String testPro){
		this.testPro = testPro;
	}
	@Column(name ="METHOD", length = 50)
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	@Column(name ="REASON", length = 50)
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	@Column(name ="NOTE", length = 50)
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	/**
	 *方法: 取得StoragePosition
	 *@return: StoragePosition  储位
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_LOCAL")
	public StoragePosition getStorageLocal(){
		return this.storageLocal;
	}
	/**
	 *方法: 设置StoragePosition
	 *@param: StoragePosition  储位
	 */
	public void setStorageLocal(StoragePosition storageLocal){
		this.storageLocal = storageLocal;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setState(String state){
		this.state = state;
	}
	
	
	
	
}