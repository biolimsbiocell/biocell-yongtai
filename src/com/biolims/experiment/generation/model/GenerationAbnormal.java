package com.biolims.experiment.generation.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 文库异常反馈
 * @author lims-platform
 * @date 2015-11-18 17:26:16
 * @version V1.0   
 *
 */
@Entity
@Table(name = "GENERATION_ABNORMAL")
@SuppressWarnings("serial")
public class GenerationAbnormal extends EntityDao<GenerationAbnormal> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
	/**样本编号*/
	private String sampleCode;
	/**文库编号*/
	private String code;
	/**index*/
	private String indexa;
	/**体积*/
	private Double bulk;
	/**浓度*/
	private Double chorma;
	/**结果判定*/
	private String resultDecide;
	/**下一步流向*/
	private String nextFlow;
	/**处理意见*/
	private String handleIdea;
	/**确认执行*/
	private String isExecute;
	/**反馈时间*/
	private Date backTime;
	/**备注*/
	private String note;
	/**关联主表*/
	private GenerationAbnormal generationAbnormalTask;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GENERATION_ABNORMAL_TASK")
	public GenerationAbnormal getGenerationAbnormalTask() {
		return generationAbnormalTask;
	}
	public void setGenerationAbnormalTask(
			GenerationAbnormal generationAbnormalTask) {
		this.generationAbnormalTask = generationAbnormalTask;
	}
	
	/**处理结果*/
	private String method;
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	/**
	 *方法: 取得String
	 *@return: String  文库编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  文库编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  index
	 */
	@Column(name ="INDEXA", length = 50)
	public String getIndexa(){
		return this.indexa;
	}
	/**
	 *方法: 设置String
	 *@param: String  index
	 */
	public void setIndexa(String indexa){
		this.indexa = indexa;
	}
	/**
	 *方法: 取得Double
	 *@return: Double  体积
	 */
	@Column(name ="BULK", length = 50)
	public Double getBulk(){
		return this.bulk;
	}
	/**
	 *方法: 设置Double
	 *@param: Double  体积
	 */
	public void setBulk(Double bulk){
		this.bulk = bulk;
	}
	/**
	 *方法: 取得Double
	 *@return: Double  浓度
	 */
	@Column(name ="CHORMA", length = 50)
	public Double getChorma(){
		return this.chorma;
	}
	/**
	 *方法: 设置Double
	 *@param: Double  浓度
	 */
	public void setChorma(Double chorma){
		this.chorma = chorma;
	}
	/**
	 *方法: 取得String
	 *@return: String  结果判定
	 */
	@Column(name ="RESULT_DECIDE", length = 50)
	public String getResultDecide(){
		return this.resultDecide;
	}
	/**
	 *方法: 设置String
	 *@param: String  结果判定
	 */
	public void setResultDecide(String resultDecide){
		this.resultDecide = resultDecide;
	}
	/**
	 *方法: 取得String
	 *@return: String  下一步流向
	 */
	@Column(name ="NEXT_FLOW", length = 50)
	public String getNextFlow(){
		return this.nextFlow;
	}
	/**
	 *方法: 设置String
	 *@param: String  下一步流向
	 */
	public void setNextFlow(String nextFlow){
		this.nextFlow = nextFlow;
	}
	/**
	 *方法: 取得String
	 *@return: String  处理意见
	 */
	@Column(name ="HANDLE_IDEA", length = 150)
	public String getHandleIdea(){
		return this.handleIdea;
	}
	/**
	 *方法: 设置String
	 *@param: String  处理意见
	 */
	public void setHandleIdea(String handleIdea){
		this.handleIdea = handleIdea;
	}
	/**
	 *方法: 取得String
	 *@return: String  确认执行
	 */
	@Column(name ="IS_EXECUTE", length = 50)
	public String getIsExecute(){
		return this.isExecute;
	}
	/**
	 *方法: 设置String
	 *@param: String  确认执行
	 */
	public void setIsExecute(String isExecute){
		this.isExecute = isExecute;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  反馈时间
	 */
	public Date getBackTime(){
		return this.backTime;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  反馈时间
	 */
	public void setBackTime(Date backTime){
		this.backTime = backTime;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 150)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 150)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	@Column(name ="SAMPLE_CODE", length = 50)
	public String getSampleCode() {
		return sampleCode;
	}
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  文库编号
	 */
	@Column(name ="CODE", length = 50)
	public String getCode(){
		return this.code;
	}
	/**
	 *方法: 设置String
	 *@param: String  文库编号
	 */
	public void setCode(String code){
		this.code = code;
	}


}