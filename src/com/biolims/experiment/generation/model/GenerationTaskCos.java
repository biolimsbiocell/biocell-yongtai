package com.biolims.experiment.generation.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 设备明细
 * @author lims-platform
 * @date 2015-11-24 11:48:00
 * @version V1.0   
 *
 */
@Entity
@Table(name = "GENERATION_TASK_COS")
@SuppressWarnings("serial")
public class GenerationTaskCos extends EntityDao<GenerationTaskCos> implements java.io.Serializable {
	/**id*/
	private String id;
	/**设备编号*/
	private String code;
	/**设备名称*/
	private String name;
	//温度
	private Double temperature;
	//转速
	private Double speed;
	//时间
	private Double time;
	//关联步骤的编号
	private String itemId;
	/**备注*/
	private String note;
	/**是否通过检测*/
	private String isGood;
	/**相关主表*/
	private GenerationTask generationTest;
	//模板设备ID
	private String tCos;
	/**
	 *方法: 取得String
	 *@return: String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  name
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  name
	 */
	public void setName(String name){
		this.name = name;
	}
//	/**
//	 *方法: 取得String
//	 *@return: String  设备编号
//	 */
//	@Column(name ="DEVICE_ID", length = 50)
//	public String getDeviceId(){
//		return this.deviceId;
//	}
//	/**
//	 *方法: 设置String
//	 *@param: String  设备编号
//	 */
//	public void setDeviceId(String deviceId){
//		this.deviceId = deviceId;
//	}
//	/**
//	 *方法: 取得String
//	 *@return: String  名称
//	 */
//	@Column(name ="DESIGNATION", length = 50)
//	public String getDesignation(){
//		return this.designation;
//	}
//	/**
//	 *方法: 设置String
//	 *@param: String  名称
//	 */
//	public void setDesignation(String designation){
//		this.designation = designation;
//	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得String
	 *@return: String  是否通过检测
	 */
	@Column(name ="IS_GOOD", length = 50)
	public String getIsGood(){
		return this.isGood;
	}
	/**
	 *方法: 设置String
	 *@param: String  是否通过检测
	 */
	public void setIsGood(String isGood){
		this.isGood = isGood;
	}
	/**
	 *方法: 取得TpTest
	 *@return: GenerationTest  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GENERATION_TEST")
	public GenerationTask getGenerationTest(){
		return this.generationTest;
	}
	/**
	 *方法: 设置TpTest
	 *@param: GenerationTest  相关主表
	 */
	public void setGenerationTest(GenerationTask generationTest){
		this.generationTest = generationTest;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Double getTemperature() {
		return temperature;
	}
	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}
	public Double getSpeed() {
		return speed;
	}
	public void setSpeed(Double speed) {
		this.speed = speed;
	}
	public Double getTime() {
		return time;
	}
	public void setTime(Double time) {
		this.time = time;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String gettCos() {
		return tCos;
	}
	public void settCos(String tCos) {
		this.tCos = tCos;
	}
	
}