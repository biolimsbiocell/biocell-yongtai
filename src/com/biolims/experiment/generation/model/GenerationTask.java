package com.biolims.experiment.generation.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.system.template.model.Template;
import com.biolims.common.model.user.User;
import com.biolims.common.model.user.User;
import com.biolims.core.model.user.UserGroup;
/**   
 * @Title: Model
 * @Description: 测序页面
 * @author lims-platform
 * @date 2015-11-24 11:48:22
 * @version V1.0   
 *
 */
@Entity
@Table(name = "GENERATION_TASK")
@SuppressWarnings("serial")
public class GenerationTask extends EntityDao<GenerationTask> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
	/**下达人*/
	private User createUser;
	/**下达时间*/
	private Date createDate;
	/**实验员*/
	private User reciveUser;
	/**实验日期*/
	private String reciveDate;
	/**状态id*/
	private String state;
	/**工作流状态*/
	private String stateName;
	/*模板*/
	private Template template;
	/**备注*/
	private String note;
	
	/**实验组*/
	private UserGroup acceptUser;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得User
	 *@return: User  下达人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser(){
		return this.createUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  下达人
	 */
	public void setCreateUser(User createUser){
		this.createUser = createUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  下达时间
	 */
	@Column(name ="CREATE_DATE", length = 50)
	public Date getCreateDate(){
		return this.createDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  下达时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 *方法: 取得User
	 *@return: User  实验员
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "RECIVE_USER")
	public User getReciveUser(){
		return this.reciveUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  实验员
	 */
	public void setReciveUser(User reciveUser){
		this.reciveUser = reciveUser;
	}
	/**
	 *方法: 取得String
	 *@return: String  实验日期
	 */
	@Column(name ="RECIVE_DATE", length = 50)
	public String getReciveDate(){
		return this.reciveDate;
	}
	/**
	 *方法: 设置String
	 *@param: String  实验日期
	 */
	public void setReciveDate(String reciveDate){
		this.reciveDate = reciveDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态id
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态id
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  工作流状态
	 */
	@Column(name ="STATE_NAME", length = 50)
	public String getStateName(){
		return this.stateName;
	}
	/**
	 *方法: 设置String
	 *@param: String  工作流状态
	 */
	public void setStateName(String stateName){
		this.stateName = stateName;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TEMPLATE")
	public Template getTemplate() {
		return template;
	}
	public void setTemplate(Template template) {
		this.template = template;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "T_USER_GROUP")
	public UserGroup getAcceptUser() {
		return acceptUser;
	}

	public void setAcceptUser(UserGroup acceptUser) {
		this.acceptUser = acceptUser;
	}
}