package com.biolims.experiment.generation.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 执行步骤
 * @author lims-platform
 * @date 2015-11-24 11:47:55
 * @version V1.0   
 *
 */
@Entity
@Table(name = "GENERATION_TASK_TEMPLATE")
@SuppressWarnings("serial")
public class GenerationTaskTemplate extends EntityDao<GenerationTaskTemplate> implements java.io.Serializable {
	/**id*/
	private String id;
	/**描述*/
	private String name;
	/**步骤编号*/
	private String stepNum;
	/**步骤名称*/
	private String stepName;
	/**步骤描述*/
	private String describe;
	/**开始时间*/
	private String startTime;
	/**结束时间*/
	private String endTime;
	/*关联样本*/
	private String sampleCodes;
	/**备注*/
	private String note;
	/**相关主表*/
	private GenerationTask generationTest;
	/**
	 *方法: 取得String
	 *@return: String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  步骤编号
	 */
	@Column(name ="STEP_NUM", length = 50)
	public String getStepNum(){
		return this.stepNum;
	}
	/**
	 *方法: 设置String
	 *@param: String  步骤编号
	 */
	public void setStepNum(String stepNum){
		this.stepNum = stepNum;
	}
	/**
	 *方法: 取得String
	 *@return: String  步骤名称
	 */
	@Column(name ="STEP_NAME", length = 50)
	public String getStepName(){
		return this.stepName;
	}
	/**
	 *方法: 设置String
	 *@param: String  步骤名称
	 */
	public void setStepName(String stepName){
		this.stepName = stepName;
	}
	/**
	 *方法: 取得String
	 *@return: String  步骤描述
	 */
	@Column(name ="DESCRIBE_", length = 50)
	public String getDescribe(){
		return this.describe;
	}
	/**
	 *方法: 设置String
	 *@param: String  步骤描述
	 */
	public void setDescribe(String describe){
		this.describe = describe;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  开始时间
	 */
	@Column(name ="START_TIME", length = 50)
	public String getStartTime(){
		return this.startTime;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  开始时间
	 */
	public void setStartTime(String startTime){
		this.startTime = startTime;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  结束时间
	 */
	@Column(name ="END_TIME", length = 50)
	public String getEndTime(){
		return this.endTime;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  结束时间
	 */
	public void setEndTime(String endTime){
		this.endTime = endTime;
	}
	@Column(name ="SAMPLE_CODES", length = 50)
	public String getSampleCodes() {
		return sampleCodes;
	}
	public void setSampleCodes(String sampleCodes) {
		this.sampleCodes = sampleCodes;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得TpTest
	 *@return: GenerationTest  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GENERATION_TEST")
	public GenerationTask getGenerationTest(){
		return this.generationTest;
	}
	/**
	 *方法: 设置TpTest
	 *@param: GenerationTest  相关主表
	 */
	public void setGenerationTest(GenerationTask generationTest){
		this.generationTest = generationTest;
	}
}