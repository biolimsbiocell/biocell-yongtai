package com.biolims.experiment.generation.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.biolims.dao.EntityDao;

/**   
 * @Title: Model
 * @Description: 待传输样本
 * @author lims-platform
 * @date 2015-11-24 11:47:55
 * @version V1.0   
 *
 */
@Entity
@Table(name = "GENERATION_TASK_TEMP")
@SuppressWarnings("serial")
public class GenerationTaskTemp extends EntityDao<GenerationTaskTemp> implements java.io.Serializable{
	/**编码*/
	private String id;
	/**DNA编号*/
	private String dnaCode;
	/**接收样本号*/
	private String sampleCode;
	/**DNA储位*/
	private String location;
	/**体积*/
	private String volume;
	/**单位*/
	private String unit;
	/**样本名称*/
	private String sampleName;
	/**样本用量*/
	private String sampleNum;
	/**取样体积*/
	private String sampleVolume;
	/**补充体积*/
	private String addVolume;
	/**总体积*/
	private String sumVolume;
	/**Index*/
	private String indexs;
	/**说明*/
	private String note;
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDnaCode() {
		return dnaCode;
	}
	public void setDnaCode(String dnaCode) {
		this.dnaCode = dnaCode;
	}
	public String getSampleCode() {
		return sampleCode;
	}
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	@Column(name="VOLUME",length=20)
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}
	@Column(name="UNIT",length=20)
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getSampleName() {
		return sampleName;
	}
	public void setSampleName(String sampleName) {
		this.sampleName = sampleName;
	}
	public String getSampleNum() {
		return sampleNum;
	}
	public void setSampleNum(String sampleNum) {
		this.sampleNum = sampleNum;
	}
	public String getSampleVolume() {
		return sampleVolume;
	}
	public void setSampleVolume(String sampleVolume) {
		this.sampleVolume = sampleVolume;
	}
	public String getAddVolume() {
		return addVolume;
	}
	public void setAddVolume(String addVolume) {
		this.addVolume = addVolume;
	}
	public String getSumVolume() {
		return sumVolume;
	}
	public void setSumVolume(String sumVolume) {
		this.sumVolume = sumVolume;
	}
	public String getIndexs() {
		return indexs;
	}
	public void setIndexs(String indexs) {
		this.indexs = indexs;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	/**状态*/
	private String state;
	

}
