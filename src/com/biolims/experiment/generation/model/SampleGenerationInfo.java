package com.biolims.experiment.generation.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.system.template.model.Template;
/**   
 * @Title: Model
 * @Description: 测序结果
 * @author lims-platform
 * @date 2015-11-24 11:48:03
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SAMPLE_GENERATION_INFO")
@SuppressWarnings("serial")
public class SampleGenerationInfo extends EntityDao<SampleGenerationInfo> implements java.io.Serializable {
	/**id*/
	private String id;
	/**描述*/
	private String name;
	/**测序编号*/
	private String textNum;
	/**样本编号*/
	private String sampleNum;
	/**体积*/
	private String volume;
	/**单位*/
	private String unit;
	/**结果判定*/
	private String result;
	/**下一步流向*/
	private String flow;
	/**处理意见*/
	private String idea;
	/**备注*/
	private String note;
	/**相关主表*/
	private GenerationTask generationTest;
	/**
	 *方法: 取得String
	 *@return: String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  测序编号
	 */
	@Column(name ="TEXT_NUM", length = 50)
	public String getTextNum(){
		return this.textNum;
	}
	/**
	 *方法: 设置String
	 *@param: String  测序编号
	 */
	public void setTextNum(String textNum){
		this.textNum = textNum;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本编号
	 */
	@Column(name ="SAMPLE_NUM", length = 50)
	public String getSampleNum(){
		return this.sampleNum;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本编号
	 */
	public void setSampleNum(String sampleNum){
		this.sampleNum = sampleNum;
	}
	/**
	 *方法: 取得String
	 *@return: String  体积
	 */
	@Column(name ="VOLUME", length = 50)
	public String getVolume(){
		return this.volume;
	}
	/**
	 *方法: 设置String
	 *@param: String  体积
	 */
	public void setVolume(String volume){
		this.volume = volume;
	}
	/**
	 *方法: 取得String
	 *@return: String  单位
	 */
	@Column(name ="UNIT", length = 50)
	public String getUnit(){
		return this.unit;
	}
	/**
	 *方法: 设置String
	 *@param: String  单位
	 */
	public void setUnit(String unit){
		this.unit = unit;
	}
	/**
	 *方法: 取得String
	 *@return: String  结果判定
	 */
	@Column(name ="RESULT", length = 50)
	public String getResult(){
		return this.result;
	}
	/**
	 *方法: 设置String
	 *@param: String  结果判定
	 */
	public void setResult(String result){
		this.result = result;
	}
	/**
	 *方法: 取得String
	 *@return: String  下一步流向
	 */
	@Column(name ="FLOW", length = 50)
	public String getFlow(){
		return this.flow;
	}
	/**
	 *方法: 设置String
	 *@param: String  下一步流向
	 */
	public void setFlow(String flow){
		this.flow = flow;
	}
	/**
	 *方法: 取得Template
	 *@return: Template  处理意见
	 */
	@Column(name ="IDEA", length = 50)
	public String getIdea(){
		return this.idea;
	}
	/**
	 *方法: 设置Template
	 *@param: Template  处理意见
	 */
	public void setIdea(String idea){
		this.idea = idea;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得TpTest
	 *@return: GenerationTest  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GENERATION_TEST")
	public GenerationTask getGenerationTest(){
		return this.generationTest;
	}
	/**
	 *方法: 设置TpTest
	 *@param: GenerationTest  相关主表
	 */
	public void setGenerationTest(GenerationTask generationTest){
		this.generationTest = generationTest;
	}
}