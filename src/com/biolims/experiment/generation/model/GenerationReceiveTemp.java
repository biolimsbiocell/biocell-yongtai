package com.biolims.experiment.generation.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 样本接收临时表
 * @author lims-platform
 * @date 2015-11-24 11:24:54
 * @version V1.0   
 *
 */
@Entity
@Table(name = "GENERATION_RECEIVE_TEMP")
@SuppressWarnings("serial")
public class GenerationReceiveTemp extends EntityDao<GenerationReceiveTemp> implements java.io.Serializable {
	/**编号*/
	private String id;
	
	/**样本编号*/
	private String code;
	
	/**原始样本编号*/
	private String sampleCode;
	
	public String getSampleCode() {
		return sampleCode;
	}
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}
	/**患者姓名*/
	private String name;
	/**检测项目*/
	private String productId;
	/**检测项目*/
	private String productName;
	/**取样时间*/
	private String sampleTime;
	/**接收日期*/
	private String acceptDate;
	/**体积*/
	private Double volume;
	
	/**单位*/
	private String unit;
	
	/**状态*/
	private String state;
	
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	@Column(name="CODE",length=20)
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	@Column(name="UNIT",length=20)
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	@Column(name="VOLUME",length=20)
	public Double getVolume() {
		return volume;
	}
	public void setVolume(Double volume) {
		this.volume = volume;
	}
	@Column(name="STATE",length=20)
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getSampleTime() {
		return sampleTime;
	}
	public void setSampleTime(String sampleTime) {
		this.sampleTime = sampleTime;
	}
	public String getAcceptDate() {
		return acceptDate;
	}
	public void setAcceptDate(String acceptDate) {
		this.acceptDate = acceptDate;
	}
	
	
	
	
	
}