﻿
package com.biolims.experiment.generation.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.generation.model.GenerationAbnormal;
import com.biolims.experiment.generation.service.GenerationAbnormalTaskService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/experiment/generation/generationAbnormalTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class GenerationAbnormalTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "240705";
	@Autowired
	private GenerationAbnormalTaskService generationAbnormalTaskService;
	private GenerationAbnormal generationAbnormalTask = new GenerationAbnormal();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showGenerationAbnormalTaskList")
	public String showGenerationAbnormalTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/generation/generationAbnormalTask.jsp");
	}

	@Action(value = "showGenerationAbnormalTaskListJson")
	public void showGenerationAbnormalTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = generationAbnormalTaskService.findGenerationAbnormalTaskList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<GenerationAbnormal> list = (List<GenerationAbnormal>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "generationAbnormalTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogGenerationAbnormalTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/generation/generationAbnormalTaskDialog.jsp");
	}

	@Action(value = "showDialogGenerationAbnormalTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogGenerationAbnormalTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = generationAbnormalTaskService.findGenerationAbnormalTaskList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<GenerationAbnormal> list = (List<GenerationAbnormal>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editGenerationAbnormalTask")
	public String editGenerationAbnormalTask() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			generationAbnormalTask = generationAbnormalTaskService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "generationAbnormalTask");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//			generationAbnormalTask.setCreateUser(user);
//			generationAbnormalTask.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/generation/generationAbnormalTask.jsp");
	}

	@Action(value = "copyGenerationAbnormalTask")
	public String copyGenerationAbnormalTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		generationAbnormalTask = generationAbnormalTaskService.get(id);
		generationAbnormalTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/generation/generationAbnormalTaskEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = generationAbnormalTask.getId();
		if(id!=null&&id.equals("")){
			generationAbnormalTask.setId(null);
		}
		Map aMap = new HashMap();
			aMap.put("plasmaAbnormalBack",getParameterFromRequest("plasmaAbnormalBackJson"));
		
			aMap.put("generationAbnormalTask",getParameterFromRequest("generationAbnormalTaskJson"));
		
		generationAbnormalTaskService.save(generationAbnormalTask,aMap);
		return redirect("/experiment/generation/generationAbnormalTask/editGenerationAbnormalTask.action?id=" + generationAbnormalTask.getId());

	}

	@Action(value = "viewGenerationAbnormalTask")
	public String toViewGenerationAbnormalTask() throws Exception {
		String id = getParameterFromRequest("id");
		generationAbnormalTask = generationAbnormalTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/generation/generationAbnormalTask.jsp");
	}
	

	@Action(value = "showPlasmaAbnormalBackList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showPlasmaAbnormalBackList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/generation/plasmaAbnormalBack.jsp");
	}

//	@Action(value = "showPlasmaAbnormalBackListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showPlasmaAbnormalBackListJson() throws Exception {
//		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//		try {
//			String scId = getRequest().getParameter("id");
//			Map<String, Object> result = generationAbnormalTaskService.findPlasmaAbnormalBackList(scId, startNum, limitNum, dir,
//					sort);
//			Long total = (Long) result.get("total");
//			List<PlasmaAbnormalBack> list = (List<PlasmaAbnormalBack>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("code", "");
//			map.put("name", "");
//			map.put("sampleCondition", "");
//			map.put("bulk", "");
//			map.put("resultDecide", "");
//			map.put("nextFlow", "");
//			map.put("handleIdea", "");
//			map.put("isExecute", "");
//			map.put("note", "");
//			map.put("generationAbnormalTask-name", "");
//			map.put("generationAbnormalTask-id", "");
//			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
//	@Action(value = "delPlasmaAbnormalBack")
//	public void delPlasmaAbnormalBack() throws Exception {
//		Map<String, Object> map = new HashMap<String, Object>();
//		try {
//			String[] ids = getRequest().getParameterValues("ids[]");
//			generationAbnormalTaskService.delPlasmaAbnormalBack(ids);
//			map.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			map.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
	

	@Action(value = "showGenerationAbnormalList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showgenerationAbnormalTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/generation/generationAbnormalTask.jsp");
	}

	@Action(value = "showGenerationAbnormalListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showgenerationAbnormalTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		try {
			//String scId = getRequest().getParameter("id");
			Map<String, Object> result = generationAbnormalTaskService.findGenerationAbnormalTask(map2Query, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<GenerationAbnormal> list = (List<GenerationAbnormal>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("indexa", "");
			map.put("bulk", "");
			map.put("chorma", "");
			map.put("resultDecide", "");
			map.put("nextFlow", "");
			map.put("handleIdea", "");
			map.put("isExecute", "");
			map.put("backTime", "yyyy-MM-dd");
			map.put("note", "");
			map.put("method", "");
//			map.put("generationAbnormalTask-name", "");
//			map.put("generationAbnormalTask-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delGenerationAbnormalTask")
	public void delGenerationAbnormalTask() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			generationAbnormalTaskService.delGenerationAbnormalTask(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public GenerationAbnormalTaskService getGenerationAbnormalTaskService() {
		return generationAbnormalTaskService;
	}

	public void setGenerationAbnormalTaskService(GenerationAbnormalTaskService generationAbnormalTaskService) {
		this.generationAbnormalTaskService = generationAbnormalTaskService;
	}

	public GenerationAbnormal getGenerationAbnormalTask() {
		return generationAbnormalTask;
	}

	public void setGenerationAbnormalTask(GenerationAbnormal generationAbnormalTask) {
		this.generationAbnormalTask = generationAbnormalTask;
	}

	//保存文库异常信息
	@Action(value = "saveGenerationAbnormal")
	public void saveGenerationAbnormalTaskBack() throws Exception {
		//String id=getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getRequest().getParameter("itemDataJson");
			generationAbnormalTaskService.saveGenerationAbnormalTask(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
