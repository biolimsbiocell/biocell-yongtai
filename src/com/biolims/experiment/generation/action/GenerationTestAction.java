﻿
package com.biolims.experiment.generation.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.generation.model.GenerationTask;
import com.biolims.experiment.generation.model.GenerationTaskItem;
import com.biolims.experiment.generation.model.GenerationTaskGeagent;
import com.biolims.experiment.generation.model.GenerationTaskCos;
import com.biolims.experiment.generation.model.SampleGenerationInfo;
import com.biolims.experiment.generation.model.GenerationTaskTemplate;
import com.biolims.experiment.generation.model.GenerationTaskTemp;
import com.biolims.experiment.generation.service.GenerationTestService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/experiment/generation")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class GenerationTestAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "240802";
	@Autowired
	private GenerationTestService generationTestService;
	private GenerationTask generationTest = new GenerationTask();
	@Resource
	private FileInfoService fileInfoService;
//	@Resource
//	private ExperimentDnaGetService experimentDnaGetService;
	@Action(value = "showGenerationTestList")
	public String showGenerationTestList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/generation/generationTest.jsp");
	}

	@Action(value = "showGenerationTestListJson")
	public void showGenerationTestListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = generationTestService.findGenerationTestList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<GenerationTask> list = (List<GenerationTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("reciveUser-id", "");
		map.put("reciveUser-name", "");
		map.put("reciveDate", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("note", "");
		//实验组
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "generationTestSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogGenerationTestList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/generation/generationTestDialog.jsp");
	}

	@Action(value = "showDialogGenerationTestListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogGenerationTestListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = generationTestService.findGenerationTestList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<GenerationTask> list = (List<GenerationTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("reciveUser-id", "");
		map.put("reciveUser-name", "");
		map.put("reciveDate", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editGenerationTest")
	public String editGenerationTest() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			generationTest = generationTestService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "generationTest");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			generationTest.setCreateUser(user);
			generationTest.setCreateDate(new Date());
			generationTest.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			generationTest.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(generationTest.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/generation/generationTestEdit.jsp");
	}

	@Action(value = "copyGenerationTest")
	public String copyGenerationTest() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		generationTest = generationTestService.get(id);
		generationTest.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/generation/generationTestEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = generationTest.getId();
		if(id!=null&&id.equals("")){
			generationTest.setId(null);
		}
		Map aMap = new HashMap();
			aMap.put("generationTestDetail",getParameterFromRequest("generationTestDetailJson"));
		
			aMap.put("generationTestStep",getParameterFromRequest("generationTestStepJson"));
		
			aMap.put("generationTestItem",getParameterFromRequest("generationTestItemJson"));
		
			aMap.put("generationTestParticuars",getParameterFromRequest("generationTestParticuarsJson"));
		
			aMap.put("generationTestResult",getParameterFromRequest("generationTestResultJson"));
		
		generationTestService.save(generationTest,aMap);
		return redirect("/experiment/generation/editGenerationTest.action?id=" + generationTest.getId());

	}

	@Action(value = "viewGenerationTest")
	public String toViewGenerationTest() throws Exception {
		String id = getParameterFromRequest("id");
		generationTest = generationTestService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/generation/generationTestEdit.jsp");
	}
	

	@Action(value = "showGenerationTestDetailList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showGenerationTestDetailList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/generation/generationTestDetail.jsp");
	}

	@Action(value = "showGenerationTestDetailListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showGenerationTestDetailListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = generationTestService.findGenerationTestDetailList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<GenerationTaskItem> list = (List<GenerationTaskItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("sampleCode", "");
			map.put("reciveUser-name", "");
			map.put("tempId", "");
			map.put("reciveUser-id", "");
			map.put("reciveDate", "yyyy-MM-dd");
			map.put("project", "");
			map.put("concentration", "");
			map.put("testQualife", "");
			map.put("failReasons", "");
			map.put("state", "");
			map.put("note", "");
			map.put("generationTest-name", "");
			map.put("generationTest-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delGenerationTestDetail")
	public void delGenerationTestDetail() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			generationTestService.delGenerationTestDetail(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	

	@Action(value = "showGenerationTestStepList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showGenerationTestStepList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/generation/generationTestStep.jsp");
	}

	@Action(value = "showGenerationTestStepListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showGenerationTestStepListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = generationTestService.findGenerationTestStepList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<GenerationTaskTemplate> list = (List<GenerationTaskTemplate>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("stepNum", "");
			map.put("stepName", "");
			map.put("describe", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("sampleCodes", "");
			map.put("note", "");
			map.put("generationTest-name", "");
			map.put("generationTest-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delGenerationTestStep")
	public void delGenerationTestStep() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			generationTestService.delGenerationTestStep(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	

	@Action(value = "showGenerationTestItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showGenerationTestItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/generation/generationTestItem.jsp");
	}

	@Action(value = "showGenerationTestItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showGenerationTestItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = generationTestService.findGenerationTestItemList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<GenerationTaskGeagent> list = (List<GenerationTaskGeagent>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("reagentId", "");
			map.put("designation", "");
			map.put("batch", "");
			map.put("count", "");
			map.put("isGood", "");
			map.put("generationTesk-name", "");
			map.put("generationTesk-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delGenerationTestItem")
	public void delGenerationTestItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			generationTestService.delGenerationTestItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	

	@Action(value = "showGenerationTestParticuarsList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showGenerationTestParticuarsList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/generation/generationTestParticuars.jsp");
	}

	@Action(value = "showGenerationTestParticuarsListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showGenerationTestParticuarsListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = generationTestService.findGenerationTestParticuarsList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<GenerationTaskCos> list = (List<GenerationTaskCos>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("deviceId", "");
			map.put("designation", "");
			map.put("note", "");
			map.put("isGood", "");
			map.put("generationTest-name", "");
			map.put("generationTest-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delGenerationTestParticuars")
	public void delGenerationTestParticuars() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			generationTestService.delGenerationTestParticuars(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	

	@Action(value = "showGenerationTestResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showGenerationTestResultList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/generation/generationTestResult.jsp");
	}

	@Action(value = "showGenerationTestResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showGenerationTestResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = generationTestService.findGenerationTestResultList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<SampleGenerationInfo> list = (List<SampleGenerationInfo>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("textNum", "");
			map.put("sampleNum", "");
			map.put("volume", "");
			map.put("unit", "");
			map.put("result", "");
			map.put("flow", "");
			map.put("idea", "");
			
			map.put("note", "");
			map.put("generationTest-name", "");
			map.put("generationTest-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delGenerationTestResult")
	public void delGenerationTestResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			generationTestService.delGenerationTestResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	//去到一代测序样本
	@Action(value = "showExperimentDNAGetReslutList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showExperimentDNAGetReslutList() throws Exception {
		String state=getParameterFromRequest("state");
		putObjToContext("state", state);
		return dispatcher("/WEB-INF/page/experiment/generation/generationTestTemp.jsp");
	}

	@Action(value = "showExperimentDNAGetReslutListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showExperimentDNAGetReslutListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String state = getParameterFromRequest("state");
			/*String stateName="";
			if(state.equals("1")){
				stateName="待提取DNA";
			}else if(state.equals("2")){
				stateName="待血浆分离";
			}else{
				stateName="待文库质检";
			}*/
//			state = com.biolims.workflow.WorkflowConstants.WORKFLOW_COMPLETE_NAME;
//			Map<String, Object> result = experimentDnaGetService.findExperimentDNAGetReslutList(startNum, limitNum, dir, sort,state);
//			Long total = (Long) result.get("total");
//			List<SampleDnaInfo> list = (List<SampleDnaInfo>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("dnaCode", "");
//			map.put("sampleCode", "");
//			//
//			map.put("sampleName", "");
//			map.put("sampleNum", "");
//			map.put("sampleVolume", "");
//			map.put("addVolume", "");
//			map.put("sumVolume", "");
//			map.put("indexs", "");
//			//
//			map.put("location", "");
//			map.put("volume", "");
//			map.put("unit", "");
//			map.put("note", "");
//			map.put("state", "");
//			map.put("experimentDnaGet-name", "");
//			map.put("experimentDnaGet-id", "");
//			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Action(value = "showGenerationTestTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showGenerationTestTempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/generation/generationTestTemp.jsp");
	}

	@Action(value = "showGenerationTestTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showGenerationTestTempListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = generationTestService.findGenerationTestTempList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<GenerationTaskTemp> list = (List<GenerationTaskTemp>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("sampleCode", "");
			map.put("volume", "");
			map.put("unit", "");
			map.put("dnaCode", "");
			map.put("location", "");
			map.put("sampleName", "");
			map.put("sampleNum", "");
			map.put("sampleVolume", "");
			map.put("addVolume", "");
			map.put("sumVolume", "");
			map.put("indexs", "");
			map.put("note", "");
			map.put("state", "");
			
			new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
		
	}


	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public GenerationTestService getGenerationTestService() {
		return generationTestService;
	}

	public void setGenerationTestService(GenerationTestService generationTestService) {
		this.generationTestService = generationTestService;
	}

	public GenerationTask getGenerationTest() {
		return generationTest;
	}

	public void setTpTest(GenerationTask generationTest) {
		this.generationTest = generationTest;
	}


}
