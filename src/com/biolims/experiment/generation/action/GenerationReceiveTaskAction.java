﻿
package com.biolims.experiment.generation.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.generation.model.GenerationReceiveItem;
import com.biolims.experiment.generation.model.GenerationReceive;
import com.biolims.experiment.generation.model.GenerationReceiveTemp;
import com.biolims.experiment.generation.service.GenerationReceiveTaskService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/experiment/generation/generationReceiveTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class GenerationReceiveTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "240803";
	@Autowired
	private GenerationReceiveTaskService generationReceiveTaskService;
	private GenerationReceive generationReceiveTask = new GenerationReceive();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showGenerationReceiveTaskList")
	public String showGenerationReceiveTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/generation/generationReceive.jsp");
	}

	@Action(value = "showGenerationReceiveTaskListJson")
	public void showGenerationReceiveTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = generationReceiveTaskService.findGenerationReceiveTaskList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<GenerationReceive> list = (List<GenerationReceive>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("describe", "");
		map.put("receiver-id", "");
		map.put("receiver-name", "");
		map.put("receiverDate", "yyyy-MM-dd");
		map.put("storagePrompt-id", "");
		map.put("storagePrompt-name", "");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "generationReceiveTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogGenerationReceiveTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/generation/generationReceiveDialog.jsp");
	}

	@Action(value = "showDialogGenerationReceiveTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogGenerationReceiveTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = generationReceiveTaskService.findGenerationReceiveTaskList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<GenerationReceive> list = (List<GenerationReceive>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("describe", "");
		map.put("receiver-id", "");
		map.put("receiver-name", "");
		map.put("receiverDate", "yyyy-MM-dd");
		map.put("storagePrompt-id", "");
		map.put("storagePrompt-name", "");
		map.put("workState", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editGenerationReceiveTask")
	public String editGenerationReceiveTask() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			generationReceiveTask = generationReceiveTaskService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "generationReceiveTask");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			generationReceiveTask.setReceiver(user);
			generationReceiveTask.setReceiverDate(new Date());
			generationReceiveTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			generationReceiveTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(generationReceiveTask.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/generation/generationReceiveTask.jsp");
	}

	@Action(value = "copyGenerationReceiveTask")
	public String copyGenerationReceiveTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		generationReceiveTask = generationReceiveTaskService.get(id);
		generationReceiveTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/generation/generationReceiveTask.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = generationReceiveTask.getId();
		if(id!=null&&id.equals("")){
			generationReceiveTask.setId(null);
		}
		Map aMap = new HashMap();
			aMap.put("generationReceiveInfo",getParameterFromRequest("generationReceiveInfoJson"));
		
		generationReceiveTaskService.save(generationReceiveTask,aMap);
		return redirect("/experiment/generation/generationReceiveTask/editGenerationReceiveTask.action?id=" + generationReceiveTask.getId());

	}

	@Action(value = "viewGenerationReceiveTask")
	public String toViewGenerationReceiveTask() throws Exception {
		String id = getParameterFromRequest("id");
		generationReceiveTask = generationReceiveTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/generation/generationReceiveTask.jsp");
	}
	

	@Action(value = "showGenerationReceiveInfoList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showGenerationReceiveItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/generation/generationReceiveInfo.jsp");
	}

	@Action(value = "showGenerationReceiveInfoListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showGenerationReceiveInfoListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = generationReceiveTaskService.findGenerationReceiveInfoList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<GenerationReceiveItem> list = (List<GenerationReceiveItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("name", "");
			map.put("sname", "");
			map.put("testPro", "");
			map.put("method", "");
			map.put("reason", "");
			map.put("note", "");
			map.put("storageLocal-name", "");
			map.put("storageLocal-id", "");
			map.put("state", "");
			map.put("generationReceiveTask-name", "");
			map.put("generationReceiveTask-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delGenerationReceiveInfo")
	public void delGenerationReceiveInfo() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			generationReceiveTaskService.delGenerationReceiveInfo(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	@Action(value = "showGenerationReceiveTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showGenerationReceiveTempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/generation/generationReceiveTemp.jsp");
	}

	@Action(value = "showGenerationReceiveTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showGenerationReceiveTempListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			//String scId = getRequest().getParameter("id");
			Map<String, Object> result = generationReceiveTaskService.findGenerationReceiveTempList(startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<GenerationReceiveTemp> list = (List<GenerationReceiveTemp>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("volume", "");
			map.put("unit", "");
			map.put("state", "");
			
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public GenerationReceiveTaskService getGenerationReceiveTaskService() {
		return generationReceiveTaskService;
	}

	public void setGenerationReceiveTaskService(GenerationReceiveTaskService generationReceiveTaskService) {
		this.generationReceiveTaskService = generationReceiveTaskService;
	}

	public GenerationReceive getGenerationReceiveTask() {
		return generationReceiveTask;
	}

	public void setGenerationReceiveTask(GenerationReceive generationReceiveTask) {
		this.generationReceiveTask = generationReceiveTask;
	}


}
