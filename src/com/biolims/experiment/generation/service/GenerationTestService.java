package com.biolims.experiment.generation.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.generation.dao.GenerationTestDao;
import com.biolims.experiment.generation.model.GenerationTask;
import com.biolims.experiment.generation.model.GenerationTaskItem;
import com.biolims.experiment.generation.model.GenerationTaskGeagent;
import com.biolims.experiment.generation.model.GenerationTaskCos;
import com.biolims.experiment.generation.model.SampleGenerationInfo;
import com.biolims.experiment.generation.model.GenerationTaskTemplate;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class GenerationTestService {
	@Resource
	private GenerationTestDao generationTestDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findGenerationTestList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return generationTestDao.selectGenerationTestList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(GenerationTask i) throws Exception {

		generationTestDao.saveOrUpdate(i);

	}
	public List<GenerationTaskItem> findGenerationItemList(String scId) throws Exception {
		List<GenerationTaskItem> list = generationTestDao.selectGenerationItemList(scId);
		return list;
	}
	public GenerationTask get(String id) {
		GenerationTask generationTest = commonDAO.get(GenerationTask.class, id);
		return generationTest;
	}
	public Map<String, Object> findGenerationTestDetailList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = generationTestDao.selectGenerationTestDetailList(scId, startNum, limitNum, dir, sort);
		List<GenerationTaskItem> list = (List<GenerationTaskItem>) result.get("list");
		return result;
	}
	public Map<String, Object> findGenerationTestStepList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = generationTestDao.selectGenerationTestStepList(scId, startNum, limitNum, dir, sort);
		List<GenerationTaskTemplate> list = (List<GenerationTaskTemplate>) result.get("list");
		return result;
	}
	public Map<String, Object> findGenerationTestItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = generationTestDao.selectGenerationTestItemList(scId, startNum, limitNum, dir, sort);
		List<GenerationTaskGeagent> list = (List<GenerationTaskGeagent>) result.get("list");
		return result;
	}
	public Map<String, Object> findGenerationTestParticuarsList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = generationTestDao.selectGenerationTestParticuarsList(scId, startNum, limitNum, dir, sort);
		List<GenerationTaskCos> list = (List<GenerationTaskCos>) result.get("list");
		return result;
	}
	public Map<String, Object> findGenerationTestResultList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = generationTestDao.selectGenerationTestResultList(scId, startNum, limitNum, dir, sort);
		List<SampleGenerationInfo> list = (List<SampleGenerationInfo>) result.get("list");
		return result;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveGenerationTestDetail(GenerationTask sc, String itemDataJson) throws Exception {
		List<GenerationTaskItem> saveItems = new ArrayList<GenerationTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			GenerationTaskItem scp = new GenerationTaskItem();
			// 将map信息读入实体类
			scp = (GenerationTaskItem) generationTestDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setGenerationTest(sc);

			saveItems.add(scp);
		}
		generationTestDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delGenerationTestDetail(String[] ids) throws Exception {
		for (String id : ids) {
			GenerationTaskItem scp =  generationTestDao.get(GenerationTaskItem.class, id);
			 generationTestDao.delete(scp);
		}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveGenerationTestStep(GenerationTask sc, String itemDataJson) throws Exception {
		List<GenerationTaskTemplate> saveItems = new ArrayList<GenerationTaskTemplate>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			GenerationTaskTemplate scp = new GenerationTaskTemplate();
			// 将map信息读入实体类
			scp = (GenerationTaskTemplate) generationTestDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setGenerationTest(sc);

			saveItems.add(scp);
		}
		generationTestDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delGenerationTestStep(String[] ids) throws Exception {
		for (String id : ids) {
			GenerationTaskTemplate scp =  generationTestDao.get(GenerationTaskTemplate.class, id);
			 generationTestDao.delete(scp);
		}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveGenerationTestItem(GenerationTask sc, String itemDataJson) throws Exception {
		List<GenerationTaskGeagent> saveItems = new ArrayList<GenerationTaskGeagent>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			GenerationTaskGeagent scp = new GenerationTaskGeagent();
			// 将map信息读入实体类
			scp = (GenerationTaskGeagent) generationTestDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setGenerationTesk(sc);

			saveItems.add(scp);
		}
		generationTestDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delGenerationTestItem(String[] ids) throws Exception {
		for (String id : ids) {
			GenerationTaskGeagent scp =  generationTestDao.get(GenerationTaskGeagent.class, id);
			 generationTestDao.delete(scp);
		}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveGenerationTestParticuars(GenerationTask sc, String itemDataJson) throws Exception {
		List<GenerationTaskCos> saveItems = new ArrayList<GenerationTaskCos>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			GenerationTaskCos scp = new GenerationTaskCos();
			// 将map信息读入实体类
			scp = (GenerationTaskCos) generationTestDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setGenerationTest(sc);

			saveItems.add(scp);
		}
		generationTestDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delGenerationTestParticuars(String[] ids) throws Exception {
		for (String id : ids) {
			GenerationTaskCos scp =  generationTestDao.get(GenerationTaskCos.class, id);
			 generationTestDao.delete(scp);
		}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveGenerationTestResult(GenerationTask sc, String itemDataJson) throws Exception {
		List<SampleGenerationInfo> saveItems = new ArrayList<SampleGenerationInfo>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SampleGenerationInfo scp = new SampleGenerationInfo();
			// 将map信息读入实体类
			scp = (SampleGenerationInfo) generationTestDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setGenerationTest(sc);

			saveItems.add(scp);
		}
		generationTestDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delGenerationTestResult(String[] ids) throws Exception {
		for (String id : ids) {
			SampleGenerationInfo scp =  generationTestDao.get(SampleGenerationInfo.class, id);
			 generationTestDao.delete(scp);
		}
	}
	//审批完成
	public void changeState(String applicationTypeActionId, String id) {
		GenerationTask gt = generationTestDao.get(GenerationTask.class, id);
		gt.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		gt.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);

		generationTestDao.update(gt);
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(GenerationTask sc, Map jsonMap) throws Exception {
		if (sc != null) {
			generationTestDao.saveOrUpdate(sc);
		
			String jsonStr = "";
			jsonStr = (String)jsonMap.get("generationTestDetail");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveGenerationTestDetail(sc, jsonStr);
			}
			jsonStr = (String)jsonMap.get("generationTestStep");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveGenerationTestStep(sc, jsonStr);
			}
			jsonStr = (String)jsonMap.get("generationTestItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveGenerationTestItem(sc, jsonStr);
			}
			jsonStr = (String)jsonMap.get("generationTestParticuars");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveGenerationTestParticuars(sc, jsonStr);
			}
			jsonStr = (String)jsonMap.get("generationTestResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveGenerationTestResult(sc, jsonStr);
			}
		}
   }
	//查询临时表
	public Map<String, Object> findGenerationTestTempList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return generationTestDao.findGenerationTestTempList(mapForQuery, startNum, limitNum, dir, sort);
	}
	//根据选中的步骤查询相关的试剂明细
	public List<Map<String,String>> setReagent(String id,String code){
		List<Map<String,String>> mapList=new ArrayList<Map<String,String>>();
		Map<String, Object> result = generationTestDao.setReagent(id,code);
		List<GenerationTaskGeagent> list = (List<GenerationTaskGeagent>) result.get("list");
		if (list != null && list.size() > 0) {
			for (GenerationTaskGeagent ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id",ti.getId());
				map.put("code",ti.getCode());
				map.put("name",ti.getName());
				map.put("batch",ti.getBatch());
				map.put("isGood",ti.getIsGood());
				//map.put("note",ti.getNote());
				map.put("tReagent",ti.gettReagent());
				
				if(ti.getOneNum()!=null){
					map.put("oneNum",ti.getOneNum().toString());
				}else{
					map.put("oneNum","");
				}
				
				if(ti.getSampleNum()!=null){
					map.put("sampleNum",ti.getSampleNum().toString());
				}else{
					map.put("sampleNum","");
				}
				
				if(ti.getNum()!=null){
					map.put("num",ti.getNum().toString());
				}else{
					map.put("num","");
				}
				map.put("itemId",ti.getItemId());
				map.put("tId",ti.getGenerationTesk().getId());
				map.put("tName",ti.getGenerationTesk().getName());
				mapList.add(map);
			}
		
		}
		return mapList;
	}
	//根据选中的步骤查询相关的设备明细
	public List<Map<String,String>> setCos(String id,String code){
		List<Map<String,String>> mapList=new ArrayList<Map<String,String>>();
		Map<String, Object> result = generationTestDao.setCos(id,code);
		List<GenerationTaskCos> list = (List<GenerationTaskCos>) result.get("list");
		if (list != null && list.size() > 0) {
			for (GenerationTaskCos ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id",ti.getId());
				map.put("code",ti.getCode());
				map.put("name",ti.getName());
				map.put("isGood",ti.getIsGood());
				if(ti.getTemperature()!=null){
					map.put("temperature",ti.getTemperature().toString());	
				}else{
					map.put("temperature","");
				}
				
				if(ti.getSpeed()!=null){
					map.put("speed",ti.getSpeed().toString());
				}else{
					map.put("speed","");
				}
				
				if(ti.getTime()!=null){
					map.put("time",ti.getTime().toString());
				}else{
					map.put("time","");
				}
				
				map.put("itemId",ti.getItemId());
				map.put("note",ti.getNote());
				map.put("tCos",ti.gettCos());
				map.put("tId",ti.getGenerationTest().getId());
				map.put("tName",ti.getGenerationTest().getName());
				mapList.add(map);
			}
		
		}
		return mapList;
	}
}
