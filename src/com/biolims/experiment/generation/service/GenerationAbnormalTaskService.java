package com.biolims.experiment.generation.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.generation.dao.GenerationAbnormalTaskDao;
import com.biolims.experiment.generation.dao.GenerationTestDao;
import com.biolims.experiment.generation.model.GenerationAbnormal;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.sample.storage.dao.SampleInDao;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class GenerationAbnormalTaskService {
	@Resource
	private GenerationAbnormalTaskDao generationAbnormalTaskDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInDao sampleInDao;
	@Resource
	private GenerationTestDao generationTestDao;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findGenerationAbnormalTaskList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return generationAbnormalTaskDao.selectGenerationAbnormalTaskList(mapForQuery, startNum, limitNum, dir, sort);
	}
	public Map<String, Object> findGenerationAbnormalTask(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return generationAbnormalTaskDao.selectGenerationAbnormalTask(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(GenerationAbnormal i,Map jsonMap) throws Exception {

		generationAbnormalTaskDao.saveOrUpdate(i);

	}
	public GenerationAbnormal get(String id) {
		GenerationAbnormal generationAbnormalTask = commonDAO.get(GenerationAbnormal.class, id);
		return generationAbnormalTask;
	}
//	public Map<String, Object> findPlasmaAbnormalBackList(String scId, Integer startNum, Integer limitNum, String dir,
//			String sort) throws Exception {
//		Map<String, Object> result = generationAbnormalTaskDao.selectPlasmaAbnormalBackList(scId, startNum, limitNum, dir, sort);
//		List<PlasmaAbnormalBack> list = (List<PlasmaAbnormalBack>) result.get("list");
//		return result;
//	}
//	public Map<String, Object> findGenerationAbnormalTaskList(String scId, Integer startNum, Integer limitNum, String dir,
//			String sort) throws Exception {
//		Map<String, Object> result = generationAbnormalTaskDao.selectGenerationAbnormalTaskList(scId, startNum, limitNum, dir, sort);
//		List<GenerationAbnormalTask> list = (List<GenerationAbnormalTask>) result.get("list");
//		return result;
//	}
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void savePlasmaAbnormalBack(GenerationAbnormalTask sc, String itemDataJson) throws Exception {
//		List<PlasmaAbnormalBack> saveItems = new ArrayList<PlasmaAbnormalBack>();
//		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
//		for (Map<String, Object> map : list) {
//			PlasmaAbnormalBack scp = new PlasmaAbnormalBack();
//			// 将map信息读入实体类
//			scp = (PlasmaAbnormalBack) generationAbnormalTaskDao.Map2Bean(map, scp);
//			if (scp.getId() != null && scp.getId().equals(""))
//				scp.setId(null);
////			scp.setGenerationAbnormalTask(sc);
//
//			saveItems.add(scp);
//		}
//		generationAbnormalTaskDao.saveOrUpdateAll(saveItems);
//	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void delPlasmaAbnormalBack(String[] ids) throws Exception {
//		for (String id : ids) {
//			PlasmaAbnormalBack scp =  generationAbnormalTaskDao.get(PlasmaAbnormalBack.class, id);
//			 generationAbnormalTaskDao.delete(scp);
//		}
//	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveGenerationAbnormalTask(GenerationAbnormal sc, String itemDataJson) throws Exception {
		List<GenerationAbnormal> saveItems = new ArrayList<GenerationAbnormal>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			GenerationAbnormal scp = new GenerationAbnormal();
			// 将map信息读入实体类
			scp = (GenerationAbnormal) generationAbnormalTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setGenerationAbnormalTask(sc);

			saveItems.add(scp);
		}
		generationAbnormalTaskDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delGenerationAbnormalTask(String[] ids) throws Exception {
		for (String id : ids) {
			GenerationAbnormal scp =  generationAbnormalTaskDao.get(GenerationAbnormal.class, id);
			 generationAbnormalTaskDao.delete(scp);
		}
	}
	
//	@WriteOperLogTable
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void save(GenerationAbnormalTask sc, Map jsonMap) throws Exception {
//		if (sc != null) {
//			generationAbnormalTaskDao.saveOrUpdate(sc);
//		
//			String jsonStr = "";
//			jsonStr = (String)jsonMap.get("plasmaAbnormalTask");
//			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
//				savePlasmaAbnormalTask(sc, jsonStr);
//			}
//			jsonStr = (String)jsonMap.get("generationAbnormalTask");
//			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
//				saveGenerationAbnormalTask(sc, jsonStr);
//			}
//		}
//   }
	//保存文库异常
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveGenerationAbnormalTask(String itemDataJson) throws Exception {
		List<GenerationAbnormal> saveItems = new ArrayList<GenerationAbnormal>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			GenerationAbnormal sbi = new GenerationAbnormal();
			sbi = (GenerationAbnormal) generationAbnormalTaskDao.Map2Bean(map, sbi);
			if (sbi.getId() != null && sbi.getId().equals(""))
				sbi.setId(null);
			
			saveItems.add(sbi);
			/*//更改文库结果子表的判定结果
			String wkid = sbi.getCode();
			List<WKSampleInfo> wkr = wKSampleTaskDao.findWKResult(wkid);
			for(WKSampleInfo wkri : wkr){
				wkri.setResult("0");
			}*/
//			if(sbi!=null&&sbi.getNextFlow()!=null&&sbi.getIsExecute().equals("0")){
//				if(sbi.getNextFlow().equals("0")){
//					//到文库质控
//					WKAcceptTem wqt = new WKAcceptTem();
//					wqt.setCode(sbi.getSampleCode());
//					wqt.setVolume(sbi.getBulk());
//					wqt.setWkId(sbi.getCode());
//					wqt.setWkCode(sbi.getCode());
//					wqt.setState("1");
//					wKSampleTaskDao.saveOrUpdate(wqt);
//				}else if(sbi.getNextFlow().equals("2")){
//					//到文库构建中间表
//					WKTemp wt=new WKTemp();
//					wt.setSampleCode(sbi.getSampleCode());
//					wt.setState("1");
//					wKSampleTaskDao.saveOrUpdate(wt);
//				}else if(sbi.getNextFlow().equals("3")){
//					SampleInItemTemp si=new SampleInItemTemp();
//					si.setSampleCode(sbi.getSampleCode());
//					si.setState("1");
//					sampleInDao.update(si);
//				}else if(sbi.getNextFlow().equals("4")){
//					//异常反馈至项目管理
//					WkFeedback wf=new WkFeedback();
//					wf.setSampleCode(sbi.getSampleCode());
//					wf.setBluk(sbi.getBulk());
//					wf.setIndexs(sbi.getIndexa());
//					wf.setConcentrer(sbi.getChorma());
//					wf.setWKCode(sbi.getCode());
//					sampleInDao.update(wf);
//				}
//			}
//			if(sbi.getNextFlow().equals("0")||sbi.getNextFlow().equals("1")&&sbi.getIsExecute().equals("0")){
//				//将更改后的结果，存放到文库质控临时表里
//				WKAcceptTem wqt = new WKAcceptTem();
//				
//				wqt.setCode(sbi.getSampleCode());
//				wqt.setVolume(sbi.getBulk());
//				wqt.setWkId(sbi.getNextFlow());
//				wqt.setState("1");
//				
//				wKSampleTaskDao.saveOrUpdate(wqt);
//			}else if(sbi.getNextFlow().equals("2")&&sbi.getIsExecute().equals("0")){
//				//将更改后的结果，存放到文库构建临时表
//				List<WKTemp> wkt = wKSampleTaskDao.searchWKTempList();
//				for(WKTemp wkti : wkt){
//					if(wkti.getCode().equals(sbi.getSampleCode())){
//						wkti.setState("1");
//					}
//				}
//			}else if(sbi.getNextFlow().equals("3")&&sbi.getIsExecute().equals("0")){
//				//将更改后的结果，存放到样本入库
//				SampleIn si = new SampleIn();
//				si.setId("SAMPLEIN"+new Date());
//				si.setCode(sbi.getSampleCode());
//				si.setName(sbi.getName());
//				
//				sampleInDao.update(si);
//			}
		}
		generationAbnormalTaskDao.saveOrUpdateAll(saveItems);
	}
}
