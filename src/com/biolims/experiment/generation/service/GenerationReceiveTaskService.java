package com.biolims.experiment.generation.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.generation.dao.GenerationReceiveTaskDao;
import com.biolims.experiment.generation.dao.GenerationTestDao;
import com.biolims.experiment.generation.model.GenerationReceiveItem;
import com.biolims.experiment.generation.model.GenerationReceive;
import com.biolims.experiment.generation.model.GenerationReceiveTemp;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class GenerationReceiveTaskService {
	@Resource
	private GenerationReceiveTaskDao generationReceiveTaskDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private GenerationTestDao generationTestDao;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findGenerationReceiveTaskList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return generationReceiveTaskDao.selectGenerationReceiveTaskList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(GenerationReceive i,Map jsonMap) throws Exception {

		generationReceiveTaskDao.saveOrUpdate(i);

	}
	public GenerationReceive get(String id) {
		GenerationReceive generationReceiveTask = commonDAO.get(GenerationReceive.class, id);
		return generationReceiveTask;
	}
	public Map<String, Object> findGenerationReceiveInfoList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = generationReceiveTaskDao.selectGenerationReceiveInfoList(scId, startNum, limitNum, dir, sort);
		List<GenerationReceiveItem> list = (List<GenerationReceiveItem>) result.get("list");
		return result;
	}
//	//查询文库构建临时表
//	public Map<String, Object> findWKTempList(Integer startNum, Integer limitNum, String dir,
//			String sort) throws Exception {
//		Map<String, Object> result = generationReceiveTaskDao.selectWKTempList( startNum, limitNum, dir, sort);
//		List<WKTemp> list = (List<WKTemp>) result.get("list");
//		return result;
//	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveGenerationReceiveTaskItem(GenerationReceive sc, String itemDataJson) throws Exception {
		List<GenerationReceiveItem> saveItems = new ArrayList<GenerationReceiveItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			GenerationReceiveItem scp = new GenerationReceiveItem();
			// 将map信息读入实体类
			scp = (GenerationReceiveItem) generationReceiveTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setGenerationReceiveTask(sc);
			//将选中的样本状态，修改为无效
			List<GenerationReceiveTemp> tmp = generationReceiveTaskDao.findGenerationReceiveTempList();
			for(GenerationReceiveTemp tmpp : tmp){
				if(tmpp.getCode().equals(scp.getCode())){
					
					tmpp.setState("2");
				}
			}
			saveItems.add(scp);
		}
		generationReceiveTaskDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delGenerationReceiveInfo(String[] ids) throws Exception {
		for (String id : ids) {
			GenerationReceiveItem scp =  generationReceiveTaskDao.get(GenerationReceiveItem.class, id);
			 generationReceiveTaskDao.delete(scp);
		}
	}
//	//审批完成
//	public void changeState(String applicationTypeActionId, String id) {
//		GenerationReceiveTask pr = generationReceiveTaskDao.get(GenerationReceiveTask.class, id);
//		pr.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
//		pr.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
//
//		User user = (User) ServletActionContext.getRequest().getSession()
//				.getAttribute(SystemConstants.USER_SESSION_KEY);
////					sct.setConfirmUser(user);
////					sct.setConfirmDate(new Date());
//
//		generationReceiveTaskDao.update(pr);
//
//		String prid = pr.getId();
//		try {
//			List<GenerationReceiveInfo> pri = generationReceiveTaskDao.findGenerationReceiveInfoList(prid);
//			for(GenerationReceiveInfo prii : pri){
//					//将结果子表的数据，存放到文库构建的临时表
//					WKTemp wkt = new WKTemp();
//					wkt.setCode(prii.getCode());
//					wkt.setSampleCode(prii.getSampleCode());
//					wkt.setName(prii.getName());
//					wkt.setSname(prii.getSname());
//					wkt.setTestPro(prii.getTestPro());
//					wkt.setMethod(prii.getMethod());
//					wkt.setReason(prii.getReason());
//					wkt.setNote(prii.getNote());
//					wkt.setStorageLocal(prii.getStorageLocal());
//					wkt.setState(prii.getState());
//					
//					wKSampleTaskDao.saveOrUpdate(wkt);
//					
//					//{审批完成后，改变SampleInfo中原始样本的状态为“待文库构建”
//					SampleInfo sf=sampleInfoMainDao.findSampleInfo(prii.getSampleCode());
//					sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_WK_NEW);
//					sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_WK_NEW_NAME);
//					//---------------------------------}
//			}
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//	@WriteOperLogTable
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void save(GenerationReceiveTask sc, Map jsonMap) throws Exception {
//		if (sc != null) {
//			generationReceiveTaskDao.saveOrUpdate(sc);
//		
//			String jsonStr = "";
//			jsonStr = (String)jsonMap.get("generationReceiveTaskItem");
//			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
//				saveGenerationReceiveInfo(sc, jsonStr);
//			}
//	}
//   }
	//查询临时表
	public Map<String, Object> findGenerationReceiveTempList(Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		Map<String, Object> result = generationReceiveTaskDao.findGenerationReceiveTempList(startNum, limitNum, dir, sort);
		List<GenerationReceiveTemp> list = (List<GenerationReceiveTemp>) result.get("list");
		result.put("list", list);
		return result;
	}
}
