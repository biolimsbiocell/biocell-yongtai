package com.biolims.experiment.generation.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.generation.model.GenerationTask;
import com.biolims.experiment.generation.model.GenerationTaskItem;
import com.biolims.experiment.generation.model.GenerationTaskGeagent;
import com.biolims.experiment.generation.model.GenerationTaskCos;
import com.biolims.experiment.generation.model.SampleGenerationInfo;
import com.biolims.experiment.generation.model.GenerationTaskTemplate;
import com.biolims.experiment.generation.model.GenerationTaskTemp;
import com.biolims.experiment.plasma.model.PlasmaTaskReagent;

@Repository
@SuppressWarnings("unchecked")
public class GenerationTestDao extends BaseHibernateDao {
	public Map<String, Object> selectGenerationTestList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from GenerationTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<GenerationTask> list = new ArrayList<GenerationTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
		public Map<String, Object> selectGenerationTestDetailList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from GenerationTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and generationTest.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<GenerationTaskItem> list = new ArrayList<GenerationTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectGenerationTestStepList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from GenerationTaskTemplate where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and generationTest.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<GenerationTaskTemplate> list = new ArrayList<GenerationTaskTemplate>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectGenerationTestItemList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from GenerationTaskGeagent where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and generationTesk='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<GenerationTaskGeagent> list = new ArrayList<GenerationTaskGeagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		//根据选中的步骤查询相关的试剂明细
		public Map<String,Object> setReagent(String id,String code){
			String hql="from GenerationTaskGeagent t where 1=1 and t.generationTesk='"+id+"' and t.itemId='"+code+"'";
			Long total=(Long)this.getSession().createQuery("select count(*)"+hql).uniqueResult();
			List<GenerationTaskGeagent> list = this.getSession().createQuery(hql).list();
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("total", total);
			result.put("list", list);
			return result;
		}
		/*
		 * 根据主表ID查询试剂明细
		 */
		public List<GenerationTaskGeagent> setReagentList(String code){
			String hql = "from GenerationTaskGeagent where 1=1 and generationTesk='" + code + "'";
			List<GenerationTaskGeagent> list=this.getSession().createQuery(hql).list();
			return list;
		}
		//根据选中的步骤查询相关的设备明细
		public Map<String,Object> setCos(String id,String code){
			String hql="from GenerationTaskCos t where 1=1 and t.generationTest='"+id+"' and t.itemId='"+code+"'";
			Long total=(Long)this.getSession().createQuery("select count(*)"+hql).uniqueResult();
			List<GenerationTaskCos> list = this.getSession().createQuery(hql).list();
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("total", total);
			result.put("list", list);
			return result;
		}
		public Map<String, Object> selectGenerationTestParticuarsList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from GenerationTaskCos where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and generationTest.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<GenerationTaskCos> list = new ArrayList<GenerationTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectGenerationTestResultList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from SampleGenerationInfo where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and generationTest.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleGenerationInfo> list = new ArrayList<SampleGenerationInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		//查询临时表样本
		public List<GenerationTaskTemp> findGenerationTestTempList() throws Exception {
			String hql = "from GenerationTaskTemp where 1=1 and state='1'";
			List<GenerationTaskTemp> list =  this.getSession().createQuery(hql).list();
			return list;
		}
		//查询临时表样本
		public Map<String, Object> findGenerationTestTempList(Map<String, String> mapForQuery, Integer startNum,
				Integer limitNum, String dir, String sort) {
			String key = " ";
			String hql = " from GenerationTaskTemp where 1=1 ";
			if (mapForQuery.size()>0){
				key = map2where(mapForQuery);
			}else{
				key=" and state='1'";
			}
			Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
			List<GenerationTask> list = new ArrayList<GenerationTask>();
			if (total > 0) {
				if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
					if (sort.indexOf("-") != -1) {
						sort = sort.substring(0, sort.indexOf("-"));
					}
					key = key + " order by " + sort + " " + dir;
				}
				if (startNum == null || limitNum == null) {
					list = this.getSession().createQuery(hql + key).list();
				} else {
					list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
				}
			}
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("total", total);
			result.put("list", list);
			return result;

		}
		public List<GenerationTaskItem> selectGenerationItemList(String scId) throws Exception {
			String hql = "from GenerationTaskItem where 1=1 ";
			String key = "";
			if (scId != null)
				key = key + " and generationTest.id='" + scId + "'";
			Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
			List<GenerationTaskItem> list = new ArrayList<GenerationTaskItem>();
			if (total > 0) {
					list = this.getSession().createQuery(hql + key).list();
			}
			return list;
		}
}