package com.biolims.experiment.generation.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.generation.model.GenerationReceiveItem;
import com.biolims.experiment.generation.model.GenerationReceive;
import com.biolims.experiment.generation.model.GenerationReceiveTemp;

@Repository
@SuppressWarnings("unchecked")
public class GenerationReceiveTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectGenerationReceiveTaskList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from GenerationReceive where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<GenerationReceive> list = new ArrayList<GenerationReceive>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
		public Map<String, Object> selectGenerationReceiveInfoList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from GenerationReceiveItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and generationReceiveTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<GenerationReceiveItem> list = new ArrayList<GenerationReceiveItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
//	//查询文库构建临时表
//	public Map<String, Object> selectWKTempList(Integer startNum, Integer limitNum,
//			String dir, String sort) throws Exception {
//		String hql = "from WKTemp where 1=1 and state=1";
//		String key = "";
//		
//		Long total = (Long) this.getSession().createQuery("select count(*) "+hql).uniqueResult();
//		List<WKTemp> list = new ArrayList<WKTemp>();
//		if (total > 0) {
//			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
//				if (sort.indexOf("-") != -1) {
//					sort = sort.substring(0, sort.indexOf("-"));
//				}
//				key = key + " order by " + sort + " " + dir;
//			} 
//			if (startNum == null || limitNum == null) {
//				list = this.getSession().createQuery(hql + key).list();
//			} else {
//				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
//			}
//		}
//		Map<String, Object> result = new HashMap<String, Object>();
//		result.put("total", total);
//		result.put("list", list);
//		return result;
//	}
//	//查询文库构建临时表，获取所有数据
//	public List<WKTemp> findWKTempList() throws Exception {
//		String hql = "from WKTemp where 1=1 and state=1";
//		List<WKTemp> list =  this.getSession().createQuery(hql).list();
//		return list;
//	}	
	//查询样本子表
	public List<GenerationReceiveItem> findGenerationReceiveInfoList(String scId) throws Exception {
		String hql = "from GenerationReceiveItem where 1=1 and generationReceiveTask.id='" + scId + "'";
		List<GenerationReceiveItem> list =  this.getSession().createQuery(hql).list();
		return list;
	}
	
	//查询临时表样本
	public List<GenerationReceiveTemp> findGenerationReceiveTempList() throws Exception {
		String hql = "from GenerationReceiveTemp where 1=1 and state='1'";
		List<GenerationReceiveTemp> list =  this.getSession().createQuery(hql).list();
		return list;
	}
	//查询临时表样本
	public Map<String, Object> findGenerationReceiveTempList(Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from GenerationReceiveTemp where 1=1 and state='1'";
		String key = "";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<GenerationReceiveTemp> list = new ArrayList<GenerationReceiveTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
}