package com.biolims.experiment.splitsample.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.experiment.splitsample.model.SplitSampleItem;
import com.biolims.experiment.splitsample.service.SplitSampleManageService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/splitsample/splitSampleManage")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SplitSampleManageAction extends BaseActionSupport {

/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "245502";
	@Autowired
	private SplitSampleManageService splitSampleManageService;
	@Resource
	private FileInfoService fileInfoService;

	@Action(value = "showSplitSampleManageEditList")
	public String showSplitSampleManageEditList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/splitsample/splitSampleManageEdit.jsp");
	}

	@Action(value = "showSplitSampleManageList")
	public String showSplitSampleManageList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/splitsample/splitSampleManage.jsp");
	}

	@Action(value = "showSplitSampleManageListJson",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSplitSampleManageListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = splitSampleManageService
					.findSplitSampleManageList(start, length, query, col, sort);
			List<SplitSampleItem> list = (List<SplitSampleItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("splitCode", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("labCode", "");
			map.put("dnaCode", "");
			map.put("sampleType", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("endDate", "");
			map.put("contraction", "");
			map.put("qbContraction", "");
			map.put("od280", "");
			map.put("od260", "");
			map.put("sampleNum", "");
			map.put("volume", "");
			
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("isSplitPlatform", "");
			map.put("isSplitProduct", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("sequencePlatform", "");
			map.put("sampleTypeId", "");
			map.put("splitNum", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	@Action(value = "showSplitSampleManage1List")
	public String showSplitSampleManag1eList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/splitsample/splitSampleManage1.jsp");
	}

	@Action(value = "showSplitSampleManage1ListJson",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSplitSampleManage1ListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = splitSampleManageService
					.findSplitSampleManageList(start, length, query, col, sort);
			List<SplitSampleItem> list = (List<SplitSampleItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("splitCode", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("labCode", "");
			map.put("dnaCode", "");
			map.put("sampleType", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("endDate", "");
			map.put("contraction", "");
			map.put("qbContraction", "");
			map.put("od280", "");
			map.put("od260", "");
			map.put("sampleNum", "");
			map.put("volume", "");
			
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("isSplitPlatform", "");
			map.put("isSplitProduct", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("sequencePlatform", "");
			map.put("sampleTypeId", "");
			map.put("splitNum", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	

	@Action(value = "splitSampleManageSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogSplitSampleManageList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/dna/splitSampleManageDialog.jsp");
	}

//	@Action(value = "showDialogSplitSampleManageListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showDialogSplitSampleManageListJson() throws Exception {
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		String dir = getParameterFromRequest("dir");
//		String sort = getParameterFromRequest("sort");
//		String data = getParameterFromRequest("data");
//		Map<String, String> map2Query = new HashMap<String, String>();
//		if (data != null && data.length() > 0)
//			map2Query = JsonUtils.toObjectByJson(data, Map.class);
//		Map<String, Object> result = splitSampleManageService
//				.findSplitSampleManageList(map2Query, startNum, limitNum,
//						dir, sort);
//		Long count = (Long) result.get("total");
//		List<SampleWaitDnaManage> list = (List<SampleWaitDnaManage>) result
//				.get("list");
//		Map<String, String> map = new HashMap<String, String>();
//		map.put("id", "");
//		map.put("name", "");
//		map.put("volume", "");
//		map.put("note", "");
//		map.put("method", "");
//		map.put("nextFlow", "");
//		map.put("code", "");
//		new SendData().sendDateJson(map, list, count,
//				ServletActionContext.getResponse());
//	}

//	@Action(value = "editSplitSampleManage")
//	public String editSplitSampleManage() throws Exception {
//		String id = getParameterFromRequest("id");
//		long num = 0;
//		if (id != null && !id.equals("")) {
//			splitSampleManage = splitSampleManageService.get(id);
//			putObjToContext("handlemethod",
//					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
//			toToolBar(rightsId, "", "",
//					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
//			num = fileInfoService.findFileInfoCount(id, "splitSampleManage");
//		} else {
//			User user = (User) this
//					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//			putObjToContext("handlemethod",
//					SystemConstants.PAGE_HANDLE_METHOD_ADD);
//			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
//		}
//		putObjToContext("fileNum", num);
//		return dispatcher("/WEB-INF/page/experiment/dna/splitSampleManageEdit.jsp");
//	}

//	@Action(value = "copySplitSampleManage")
//	public String copySplitSampleManage() throws Exception {
//		String id = getParameterFromRequest("id");
//		String handlemethod = getParameterFromRequest("handlemethod");
//		splitSampleManage = splitSampleManageService.get(id);
//		splitSampleManage.setId("");
//		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
//		toToolBar(rightsId, "", "", handlemethod);
//		toSetStateCopy();
//		return dispatcher("/WEB-INF/page/experiment/dna/splitSampleManageEdit.jsp");
//	}

//	@Action(value = "save")
//	public String save() throws Exception {
//		String id = splitSampleManage.getId();
//		if (id != null && id.equals("")) {
//			splitSampleManage.setId(null);
//		}
//		Map aMap = new HashMap();
//		splitSampleManageService.save(splitSampleManage, aMap);
//		return redirect("/experiment/dna/splitSampleManage/editSplitSampleManage.action?id="
//				+ splitSampleManage.getId());
//
//	}

//	@Action(value = "viewSplitSampleManage")
//	public String toViewSplitSampleManage() throws Exception {
//		String id = getParameterFromRequest("id");
//		splitSampleManage = splitSampleManageService.get(id);
//		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
//		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
//		return dispatcher("/WEB-INF/page/experiment/dna/splitSampleManageEdit.jsp");
//	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SplitSampleManageService getSplitSampleManageService() {
		return splitSampleManageService;
	}

	public void setSplitSampleManageService(
			SplitSampleManageService splitSampleManageService) {
		this.splitSampleManageService = splitSampleManageService;
	}

//	public SampleWaitDnaManage getSplitSampleManage() {
//		return splitSampleManage;
//	}
//
//	public void setSplitSampleManage(SampleWaitDnaManage splitSampleManage) {
//		this.splitSampleManage = splitSampleManage;
//	}

//	/**
//	 * 保存中间产物
//	 * @throws Exception
//	 */
//	@Action(value = "saveDNAManage")
//	public void saveDNAAbnormal() throws Exception {
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			String itemDataJson = getParameterFromRequest("itemDataJson");
//			splitSampleManageService
//					.saveSplitSampleInfoManager(itemDataJson);
//			result.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}
//
//	/**
//	 * 保存样本
//	 * @throws Exception
//	 */
//	@Action(value = "saveDnaGetItemManager")
//	public void saveDnaGetItemManager() throws Exception {
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			String itemDataJson = getParameterFromRequest("itemDataJson");
//			splitSampleManageService.saveDnaGetItemManager(itemDataJson);
//			result.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}
//
//	@Action(value = "showDnaGetItemManagerList")
//	public String showDnaGetItemManagerList() throws Exception {
//		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		return dispatcher("/WEB-INF/page/experiment/splitsample/splitSampleManage.jsp");
//	}
//
//	@Action(value = "showDnaGetItemManagerListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showDnaGetItemManagerListJson() throws Exception {
//		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		String dir = getParameterFromRequest("dir");
//		String sort = getParameterFromRequest("sort");
//		String data = getParameterFromRequest("data");
//		// 排序方式
//		Map<String, String> map2Query = new HashMap<String, String>();
//		if (data != null && data.length() > 0)
//			map2Query = JsonUtils.toObjectByJson(data, Map.class);
//		try {
//			Map<String, Object> result = splitSampleManageService
//					.findDnaGetItemList(map2Query, startNum, limitNum, dir,
//							sort);
//			Long total = (Long) result.get("total");
//			List<SplitSampleItem> list = (List<SplitSampleItem>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("splitCode", "");
//			map.put("code", "");
//			map.put("sampleCode", "");
//			map.put("labCode", "");
//			map.put("dnaCode", "");
//			map.put("sampleType", "");
//			map.put("productId", "");
//			map.put("productName", "");
//			map.put("endDate", "");
//			map.put("contraction", "");
//			map.put("qbContraction", "");
//			map.put("od280", "");
//			map.put("od260", "");
//			map.put("sampleNum", "");
//			map.put("volume", "");
//			
//			map.put("nextFlowId", "");
//			map.put("nextFlow", "");
//			map.put("isSplitPlatform", "");
//			map.put("isSplitProduct", "");
//			map.put("state", "");
//			map.put("stateName", "");
//			map.put("sequencePlatform", "");
//			map.put("sampleTypeId", "");
//			new SendData().sendDateJson(map, list, total,
//					ServletActionContext.getResponse());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//	}
//
//	/**
//	 * 样本管理明细入库
//	 * @throws Exception
//	 */
//	@Action(value = "dnaGetManageItemRuku")
//	public void plasmaManageItemRuku() throws Exception {
//		String ids = getRequest().getParameter("ids");
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			splitSampleManageService.dnaGetManageItemRuku(ids);
//			result.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}
//
//	/**
//	 * 样本管理明细提取待办
//	 * @throws Exception
//	 */
//	@Action(value = "dnaGetManageItemTiqu")
//	public void dnaGetManageItemTiqu() throws Exception {
//		String id = getRequest().getParameter("id");
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			splitSampleManageService.dnaGetManageItemTiqu(id);
//			result.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}
}
