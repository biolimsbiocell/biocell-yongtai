package com.biolims.experiment.splitsample.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.splitsample.model.SplitSampleItem;
import com.biolims.experiment.splitsample.service.SplitSampleItemService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/splitsample/splitSampleItem")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SplitSampleItemAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "245501";
	@Autowired
	private SplitSampleItemService splitSampleItemService;
	@Resource
	private CommonDAO commonDAO;
	@Autowired
	private CodingRuleService codingRuleService;

	private SplitSampleItem splitSampleItem = new SplitSampleItem();
	@Resource
	private FileInfoService fileInfoService;

	@Action(value = "showSplitSampleItemList")
	public String showSplitSampleItemList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		String scId = getRequest().getParameter("id");
		putObjToContext("id", scId);
		return dispatcher("/WEB-INF/page/experiment/splitsample/splitSampleItem.jsp");
	}

	@Action(value = "showSplitSampleItemListJson")
	public void showSplitSampleItemListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String classify = getParameterFromRequest("id");
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = splitSampleItemService.findSplitSampleItemList(classify, start, length, query,
					col, sort);
			List<SplitSampleItem> list = (List<SplitSampleItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("splitCode", "");
			map.put("code", "");
			map.put("sampleOrder-id", "");
			map.put("sampleCode", "");
			map.put("labCode", "");
			map.put("dnaCode", "");
			map.put("sampleType", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("endDate", "");
			map.put("contraction", "");
			map.put("qbContraction", "");
			map.put("od280", "");
			map.put("od260", "");
			map.put("sampleNum", "");
			map.put("volume", "");
			map.put("sampleInfo-idCard", "");
			map.put("sampleInfo-orderNum", "");
			map.put("sampleInfo-id", "");

			map.put("barCode", "");
			map.put("orderId", "");
			map.put("orderCode", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("isSplitPlatform", "");
			map.put("isSplitProduct", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("sequencePlatform", "");
			map.put("sampleTypeId", "");

			map.put("splitNum", "");
			map.put("dataTraffic", "");

			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// @Action(value = "showSplitProductItemList")
	// public String showSplitProductItemList() throws Exception {
	// putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// return
	// dispatcher("/WEB-INF/page/experiment/splitsample/splitProductItem.jsp");
	// }
	//
	// @Action(value = "showSplitProductItemListJson")
	// public void showSplitProductItemListJson() throws Exception {
	// int startNum = Integer.parseInt(getParameterFromRequest("start"));
	// int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
	// String dir = getParameterFromRequest("dir");
	// String sort = getParameterFromRequest("sort");
	// String data = getParameterFromRequest("data");
	// Map<String, String> map2Query = new HashMap<String, String>();
	// if (data != null && data.length() > 0)
	// map2Query = JsonUtils.toObjectByJson(data, Map.class);
	// Map<String, Object> result =
	// splitSampleItemService.findSplitSampleItemList2(map2Query, startNum,
	// limitNum, dir, sort);
	// Long count = (Long) result.get("total");
	// List<SplitSampleItem> list = (List<SplitSampleItem>) result.get("list");
	//
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("splitCode", "");
	// map.put("dataTraffic", "");
	// map.put("code", "");
	// map.put("sampleCode", "");
	// map.put("labCode", "");
	// map.put("barCode", "");
	// map.put("dnaCode", "");
	// map.put("sampleType", "");
	// map.put("dicsampleType-name", "");
	// map.put("dicsampleType-id", "");
	// map.put("productId", "");
	// map.put("productName", "");
	// map.put("endDate", "");
	// map.put("contraction", "");
	// map.put("qbContraction", "");
	// map.put("od280", "");
	// map.put("od260", "");
	// map.put("sampleNum", "");
	// map.put("volume", "");
	// map.put("sampleInfo-id", "");
	// map.put("sampleInfo-idCard", "");
	// map.put("orderCode", "");
	// map.put("orderId", "");
	//
	// map.put("nextFlowId", "");
	// map.put("nextFlow", "");
	// map.put("isSplitPlatform", "");
	// map.put("isSplitProduct", "");
	// map.put("state", "");
	// map.put("stateName", "");
	// map.put("sequencePlatform", "");
	// map.put("groupNum", "");
	// map.put("splitNum", "");
	// map.put("sampleTypeId", "");
	//
	// new SendData().sendDateJson(map, list, count,
	// ServletActionContext.getResponse());
	// }
	//

	@Action(value = "showSplitProductItemList1")
	public String showSplitProductItemList1() throws Exception {
		String id = getParameterFromRequest("id");
		putObjToContext("id", id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/splitsample/splitProductItem1.jsp");
	}

	@Action(value = "showSplitProductItemList1Json")
	public void showSplitProductItemList1Json() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = splitSampleItemService.findSplitSampleItemList2(start, length, query, col, sort);
		List<SplitSampleItem> list = (List<SplitSampleItem>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("splitCode", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("labCode", "");
		map.put("dnaCode", "");
		map.put("sampleType", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("endDate", "");
		map.put("contraction", "");
		map.put("qbContraction", "");
		map.put("od280", "");
		map.put("od260", "");
		map.put("sampleNum", "");
		map.put("volume", "");
		map.put("sampleTypeId", "");
		map.put("nextFlowId", "");
		map.put("nextFlow", "");
		map.put("isSplitPlatform", "");
		map.put("isSplitProduct", "");
		map.put("state", "");
		map.put("splitNum", "");
		map.put("stateName", "");
		map.put("sequencePlatform", "");
		map.put("groupNum", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	@Action(value = "showSplitSampleItem1List")
	public String showSplitSampleItem1List() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/splitsample/splitSampleItem1.jsp");
	}

	@Action(value = "showSplitSampleItem1ListJson")
	public void showSplitSampleItem1ListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = splitSampleItemService.findSplitSampleItem1List(map2Query, startNum, limitNum, dir,
				sort);
		Long count = (Long) result.get("total");
		List<SplitSampleItem> list = (List<SplitSampleItem>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("splitCode", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("labCode", "");
		map.put("dnaCode", "");
		map.put("sampleType", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("endDate", "");
		map.put("splitNum", "");
		map.put("contraction", "");
		map.put("qbContraction", "");
		map.put("od280", "");
		map.put("od260", "");
		map.put("sampleNum", "");
		map.put("volume", "");

		map.put("nextFlowId", "");

		map.put("sampleTypeId", "");
		map.put("nextFlow", "");
		map.put("isSplitPlatform", "");
		map.put("isSplitProduct", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("sequencePlatform", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "splitSampleItemSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogSplitSampleItemList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/splitsample/splitSampleItemDialog.jsp");
	}

	@Action(value = "showDialogSplitSampleItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogSplitSampleItemListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = splitSampleItemService.findSplitSampleItemList(null, start, length, query, col,
					sort);
			List<SplitSampleItem> list = (List<SplitSampleItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("splitCode", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("labCode", "");
			map.put("dnaCode", "");
			map.put("sampleType", "");
			map.put("sampleTypeId", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("endDate", "");
			map.put("contraction", "");
			map.put("qbContraction", "");
			map.put("od280", "");
			map.put("od260", "");
			map.put("sampleNum", "");
			map.put("volume", "");
			map.put("splitNum", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Action(value = "editSplitSampleItem")
	public String editSplitSampleItem() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			splitSampleItem = splitSampleItemService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "splitSampleItem");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			// splitSampleItem.setCreateUser(user);
			// splitSampleItem.setCreateDate(new Date());
			// splitSampleItem.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			// splitSampleItem.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/splitsample/splitSampleItemEdit.jsp");
	}

	@Action(value = "copySplitSampleItem")
	public String copySplitSampleItem() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		splitSampleItem = splitSampleItemService.get(id);
		splitSampleItem.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/splitsample/splitSampleItemEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = splitSampleItem.getId();
		if (id != null && id.equals("")) {
			splitSampleItem.setId(null);
		}
		Map aMap = new HashMap();
		splitSampleItemService.save(splitSampleItem, aMap);
		return redirect(
				"/experiment/splitsample/splitSampleItem/editSplitSampleItem.action?id=" + splitSampleItem.getId());

	}

	@Action(value = "viewSplitSampleItem")
	public String toViewSplitSampleItem() throws Exception {
		String id = getParameterFromRequest("id");
		splitSampleItem = splitSampleItemService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/splitsample/splitSampleItemEdit.jsp");
	}

	/**
	 * 列表
	 * 
	 * @throws Exception
	 */
	@Action(value = "saveSampleItem")
	public void saveSampleItem() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String logInfo = getParameterFromRequest("logInfo");
			String itemDataJson = getParameterFromRequest("dataJson");
			splitSampleItemService.saveSampleItem(itemDataJson, logInfo);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 按照检测项目拆分样本---多平台
	 * 
	 * @throws Exception
	 */
	@Action(value = "SplitProductItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void SplitProductItem() throws Exception {
		String splitCode = getRequest().getParameter("splitCode");
		String dnaCode = getRequest().getParameter("dnaCode");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String maxSplitCode = this.splitSampleItemService.splitProductItem(dnaCode, splitCode);
			result.put("data", maxSplitCode);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 按照检测项目拆分样本 --单平台
	 * 
	 * @throws Exception
	 */
	@Action(value = "SplitProductItemTwo", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void SplitProductItemTwo() throws Exception {
		String id = getRequest().getParameter("id"); // 勾选的样本的ID
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.splitSampleItemService.splitProductItemTwo(id);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 查找最大的样本号
	 * 
	 * @throws Exception
	 */
	@Action(value = "findMaxSplitCode", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void findMaxSplitCode() throws Exception {
		String splitCode = getRequest().getParameter("splitCode"); // 勾选的样本的ID
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String code = codingRuleService.getCode1("SplitSampleItem", splitCode, 00, 2, null);
			result.put("data", code);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 提交样本
	 * 
	 * @throws Exception
	 */
	@Action(value = "submitSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void submitSample() throws Exception {
		String [] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.splitSampleItemService.submitSample(ids);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 拆分样本，按数量
	 * 
	 * @throws Exception
	 */
	@Action(value = "splitNum", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void splitNum() throws Exception {
		String id = getRequest().getParameter("id");
		String splitNum = getRequest().getParameter("splitNum");
		Integer splitNums = Integer.valueOf(splitNum);
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<String> splitCodes = splitSampleItemService.splitNum(id, splitNums);
			result.put("success", true);
			result.put("splitCodes", splitCodes);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 查询是否已拆产品code
	 * 
	 * @throws Exception
	 */
	@Action(value = "selIsSplitProductCode", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selIsSplitProductCode() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String IssplitCode = this.splitSampleItemService.selIsSplitProductCode(code);
			result.put("data", IssplitCode);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 查询是否已拆产品splitCode
	 * 
	 * @throws Exception
	 */
	@Action(value = "selIsSplitProductSplitCode", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selIsSplitProductSplitCode() throws Exception {
		String id = getRequest().getParameter("id");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			SplitSampleItem ssi = this.splitSampleItemService.get(id);
			String IssplitCode = ssi.getIsSplitProduct();
			result.put("data", IssplitCode);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SplitSampleItemService getSplitSampleItemService() {
		return splitSampleItemService;
	}

	public void setSplitSampleItemService(SplitSampleItemService splitSampleItemService) {
		this.splitSampleItemService = splitSampleItemService;
	}

	public SplitSampleItem getSplitSampleItem() {
		return splitSampleItem;
	}

	public void setSplitSampleItem(SplitSampleItem splitSampleItem) {
		this.splitSampleItem = splitSampleItem;
	}

}
