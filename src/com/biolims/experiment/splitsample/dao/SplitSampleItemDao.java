package com.biolims.experiment.splitsample.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.splitsample.model.SplitSampleItem;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class SplitSampleItemDao extends BaseHibernateDao {
	public Map<String, Object> selectSplitSampleItemList(String classify, Integer start,
			Integer length, String query, String col,String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SplitSampleItem where 1=1";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		if("1".equals(classify)){
			key+= " and state='1' and techJkServiceTask is null";
		}else{
			key+= " and state='1'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from SplitSampleItem where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<SplitSampleItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}
	
	public Map<String, Object> selectSplitSampleItem1List(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from SplitSampleItem where 1=1 and state='3'";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SplitSampleItem> list = new ArrayList<SplitSampleItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	
	public Map<String, Object> selectSplitSampleItemList2(Integer start, Integer length,
			String query, String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SplitSampleItem where 1=1 and state='5'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from SplitSampleItem where 1=1 and state='5'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<SplitSampleItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}
	
	//查询最大的拆分后样本号
	public String findMaxSplitCode(String dnaCode,String splitCode) {
		String sql = "select  max(t.split_code) from split_sample_item t where 1=1 and t.dna_code='"+dnaCode+"'";
		String maxSplitCode = this.getSession().createSQLQuery(sql).uniqueResult().toString();
		return maxSplitCode;
	}
	
	// 根据拆分后样本号查询是否已拆产品
	public String finIsSplitProductSplitCode(String splitCode) {
		String sql = "select t.is_split_product from split_sample_item t where 1=1 and t.split_code='"
				+ splitCode + "'";
		String isSplitCode = this.getSession().createSQLQuery(sql)
				.uniqueResult().toString();
		return isSplitCode;
	}
	
	// 根据样本编号查询是否已拆产品
		public String finIsSplitProductCode(String code) {
			String sql = "select t.is_split_product from split_sample_item t where 1=1 and t.code='"
					+ code + "'";
			String isSplitCode = this.getSession().createSQLQuery(sql)
					.uniqueResult().toString();
			return isSplitCode;
		}
}
		/*public Map<String, Object> findSplitCode(String code,
				String query, String col, String sort) throws Exception {
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from SplitSampleItem where 1=1 and state='1'";
			String key = "";
			if(query!=null&&!"".equals(query)){
				key=map2Where(query);
			}
			String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
			if(!"all".equals(scopeId)){
				key+=" and scopeId='"+scopeId+"'";
			}
			Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			if(0l!= sumCount){
				Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
				String hql = "from SplitSampleItem where 1=1 and state='1' and code='"+code+"'";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					col=col.replace("-", ".");
					key+=" order by "+col+" "+sort;
				}
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
		}
			return map;
		}
}*/