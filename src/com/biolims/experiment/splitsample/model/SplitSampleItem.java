package com.biolims.experiment.splitsample.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.technology.wk.model.TechJkServiceTask;
/**   
 * @Title: Model
 * @Description: 分管平台
 * @author lims-platform
 * @date 2017-06-21 18:16:22
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SPLIT_SAMPLE_ITEM")
@SuppressWarnings("serial")
public class SplitSampleItem extends EntityDao<SplitSampleItem> implements java.io.Serializable {
	/**编码*/
	private String id;
	/**拆分后编号*/
	private String splitCode;
	/**数据通量*/
	private String dataTraffic;
	/**样本编号*/
	private String code;
	/**条码号*/
	private String barCode;
	/**订单号*/
	private String orderId;
	/**订单号*/
	private String orderCode;
	/**原始样本编号*/
	private String sampleCode;
	/**实验室样本号*/
	private String labCode;
	/**dna编号*/
	private String dnaCode;
	/**样本类型*/
	private String sampleType;
	/**样本类型*/
	private String sampleTypeId;
	/** 中间产物类型 */
	private DicSampleType dicSampleType;
	/**关联订单*/
	private SampleOrder sampleOrder;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_ORDER")
	public SampleOrder getSampleOrder() {
		return sampleOrder;
	}

	public void setSampleOrder(SampleOrder sampleOrder) {
		this.sampleOrder = sampleOrder;
	}
	public String getSampleTypeId() {
		return sampleTypeId;
	}
	public void setSampleTypeId(String sampleTypeId) {
		this.sampleTypeId = sampleTypeId;
	}
	/**检测项目id*/
	private String productId;
	/**检测项目*/
	private String productName;
	/**结束日期*/
	private String endDate;
	/**浓度(ng/ul)*/
	private Double contraction;
	/**qubit浓度(ng/ul)*/
	private String qbContraction;
	/**od260/280*/
	private String od280;
	/**od260/230*/
	private String od260;
	/**总量*/
	private Double sampleNum;
	/**体积*/
	private Double volume;
	//下一步流向ID
	private String nextFlowId;
	//下一步流向
	private String nextFlow;
	//是否已拆平台
	private String isSplitPlatform;
	//是否已拆项目
	private String isSplitProduct;
	//状态ID
	private String state;
	//状态
	private String stateName;
	/** 范围Id */
	private String scopeId;
	/** 范围 */
	private String scopeName;
	
	//测序平台
	private String sequencePlatform;
	
	//分组号
	private String groupNum;
	
	
	// 科技服务
		private TechJkServiceTask techJkServiceTask;
	//产品类型  1科研2临床
	private String sampleStyle;
		
	public String getSampleStyle() {
		return sampleStyle;
	}
	public void setSampleStyle(String sampleStyle) {
		this.sampleStyle = sampleStyle;
	}
		@ManyToOne(fetch = FetchType.LAZY)
		@NotFound(action = NotFoundAction.IGNORE)
		@JoinColumn(name = "TECH_JK_SERVICE_TASK")
		public TechJkServiceTask getTechJkServiceTask() {
			return techJkServiceTask;
		}

		public void setTechJkServiceTask(TechJkServiceTask techJkServiceTask) {
			this.techJkServiceTask = techJkServiceTask;
		}
		
		
	//按此号显示样本
	private String splitNum;
	/** 样本主数据 */
	private SampleInfo sampleInfo;

	/**
	 * 方法: 取得SampleInfo
	 * 
	 * @return: 样本主数据
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}

	/**
	 * 方法: 设置SampleInfo
	 * 
	 * @param: 样本主数据
	 */
	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}
	public String getSplitNum() {
		return splitNum;
	}
	public void setSplitNum(String splitNum) {
		this.splitNum = splitNum;
	}
	public String getGroupNum() {
		return groupNum;
	}
	public void setGroupNum(String groupNum) {
		this.groupNum = groupNum;
	}
	public String getSequencePlatform() {
		return sequencePlatform;
	}
	public void setSequencePlatform(String sequencePlatform) {
		this.sequencePlatform = sequencePlatform;
	}
	public String getNextFlowId() {
		return nextFlowId;
	}
	public void setNextFlowId(String nextFlowId) {
		this.nextFlowId = nextFlowId;
	}
	public String getNextFlow() {
		return nextFlow;
	}
	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}
	public String getIsSplitPlatform() {
		return isSplitPlatform;
	}
	public void setIsSplitPlatform(String isSplitPlatform) {
		this.isSplitPlatform = isSplitPlatform;
	}
	public String getIsSplitProduct() {
		return isSplitProduct;
	}
	public void setIsSplitProduct(String isSplitProduct) {
		this.isSplitProduct = isSplitProduct;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	/**
	 *方法: 取得String
	 *@return: String  编码
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 250)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编码
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  拆分后编号
	 */
	@Column(name ="SPLIT_CODE", length = 255)
	public String getSplitCode(){
		return this.splitCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  拆分后编号
	 */
	public void setSplitCode(String splitCode){
		this.splitCode = splitCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本编号
	 */
	@Column(name ="CODE", length = 255)
	public String getCode(){
		return this.code;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本编号
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 *方法: 取得String
	 *@return: String  原始样本编号
	 */
	@Column(name ="SAMPLE_CODE", length = 255)
	public String getSampleCode(){
		return this.sampleCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  原始样本编号
	 */
	public void setSampleCode(String sampleCode){
		this.sampleCode = sampleCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  实验室样本号
	 */
	@Column(name ="LAB_CODE", length = 255)
	public String getLabCode(){
		return this.labCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  实验室样本号
	 */
	public void setLabCode(String labCode){
		this.labCode = labCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  dna编号
	 */
	@Column(name ="DNA_CODE", length = 255)
	public String getDnaCode(){
		return this.dnaCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  dna编号
	 */
	public void setDnaCode(String dnaCode){
		this.dnaCode = dnaCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本类型
	 */
	@Column(name ="SAMPLE_TYPE", length = 255)
	public String getSampleType(){
		return this.sampleType;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本类型
	 */
	public void setSampleType(String sampleType){
		this.sampleType = sampleType;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_SAMPLE_TYPE")
	public DicSampleType getDicSampleType() {
		return dicSampleType;
	}

	public void setDicSampleType(DicSampleType dicSampleType) {
		this.dicSampleType = dicSampleType;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  检测项目id
	 */
	@Column(name ="PRODUCT_ID", length = 255)
	public String getProductId(){
		return this.productId;
	}
	/**
	 *方法: 设置String
	 *@param: String  检测项目id
	 */
	public void setProductId(String productId){
		this.productId = productId;
	}
	/**
	 *方法: 取得String
	 *@return: String  检测项目
	 */
	@Column(name ="PRODUCT_NAME", length = 255)
	public String getProductName(){
		return this.productName;
	}
	/**
	 *方法: 设置String
	 *@param: String  检测项目
	 */
	public void setProductName(String productName){
		this.productName = productName;
	}
	/**
	 *方法: 取得String
	 *@return: String  结束日期
	 */
	@Column(name ="END_DATE", length = 255)
	public String getEndDate(){
		return this.endDate;
	}
	/**
	 *方法: 设置String
	 *@param: String  结束日期
	 */
	public void setEndDate(String endDate){
		this.endDate = endDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  浓度(ng/ul)
	 */
	@Column(name ="CONTRACTION", length = 255)
	public Double getContraction(){
		return this.contraction;
	}
	/**
	 *方法: 设置String
	 *@param: String  浓度(ng/ul)
	 */
	public void setContraction(Double contraction){
		this.contraction = contraction;
	}
	/**
	 *方法: 取得String
	 *@return: String  qubit浓度(ng/ul)
	 */
	@Column(name ="QB_CONTRACTION", length = 255)
	public String getQbContraction(){
		return this.qbContraction;
	}
	/**
	 *方法: 设置String
	 *@param: String  qubit浓度(ng/ul)
	 */
	public void setQbContraction(String qbContraction){
		this.qbContraction = qbContraction;
	}
	/**
	 *方法: 取得String
	 *@return: String  od260/280
	 */
	@Column(name ="OD280", length = 255)
	public String getOd280(){
		return this.od280;
	}
	/**
	 *方法: 设置String
	 *@param: String  od260/280
	 */
	public void setOd280(String od280){
		this.od280 = od280;
	}
	/**
	 *方法: 取得String
	 *@return: String  od260/230
	 */
	@Column(name ="OD260", length = 255)
	public String getOd260(){
		return this.od260;
	}
	/**
	 *方法: 设置String
	 *@param: String  od260/230
	 */
	public void setOd260(String od260){
		this.od260 = od260;
	}
	/**
	 *方法: 取得String
	 *@return: String  总量
	 */
	@Column(name ="SAMPLE_NUM", length = 255)
	public Double getSampleNum(){
		return this.sampleNum;
	}
	/**
	 *方法: 设置String
	 *@param: String  总量
	 */
	public void setSampleNum(Double sampleNum){
		this.sampleNum = sampleNum;
	}
	/**
	 *方法: 取得String
	 *@return: String  体积
	 */
	@Column(name ="VOLUME", length = 255)
	public Double getVolume(){
		return this.volume;
	}
	/**
	 *方法: 设置String
	 *@param: String  体积
	 */
	public void setVolume(Double volume){
		this.volume = volume;
	}
	/** 
	 * @return barCode
	 * @author  zhiqiang.yang@biolims.cn  
	 */
	public String getBarCode() {
		return barCode;
	}
	/**
	 * @param barCode the barCode to set
	 * @author  zhiqiang.yang@biolims.cn 
	 */
	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}
	/** 
	 * @return orderCode
	 * @author  zhiqiang.yang@biolims.cn  
	 */
	public String getOrderCode() {
		return orderCode;
	}
	/**
	 * @param orderCode the orderCode to set
	 * @author  zhiqiang.yang@biolims.cn 
	 */
	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}
	/** 
	 * @return orderId
	 * @author  zhiqiang.yang@biolims.cn  
	 */
	public String getOrderId() {
		return orderId;
	}
	/**
	 * @param orderId the orderId to set
	 * @author  zhiqiang.yang@biolims.cn 
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	/** 
	 * @return dataTraffic
	 * @author  zhiqiang.yang@biolims.cn  
	 */
	public String getDataTraffic() {
		return dataTraffic;
	}
	/**
	 * @param dataTraffic the dataTraffic to set
	 * @author  zhiqiang.yang@biolims.cn 
	 */
	public void setDataTraffic(String dataTraffic) {
		this.dataTraffic = dataTraffic;
	}
	/**
	 * @return the scopeId
	 */
	public String getScopeId() {
		return scopeId;
	}
	/**
	 * @param scopeId the scopeId to set
	 */
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}
	/**
	 * @return the scopeName
	 */
	public String getScopeName() {
		return scopeName;
	}
	/**
	 * @param scopeName the scopeName to set
	 */
	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}
	
}