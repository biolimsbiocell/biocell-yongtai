package com.biolims.experiment.splitsample.service;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.dna.model.DnaTaskItem;
import com.biolims.experiment.splitsample.dao.SplitSampleManageDao;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.sample.storage.model.SampleInItemTemp;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SplitSampleManageService {
	@Resource
	private SplitSampleManageDao splitSampleManageDao;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findSplitSampleManageList(
			Integer start, Integer length,
			String query, String col, String sort) throws Exception {
		return splitSampleManageDao.selectSplitSampleManageList(start, length, query, col, sort);
	}
	
	public Map<String, Object> findSplitSampleManage1List(
			Integer start, Integer length,
			String query, String col, String sort) throws Exception {
		return splitSampleManageDao.selectSplitSampleManage1List(start, length, query, col, sort);
	}

	public Map<String, Object> findDnaGetItemList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return splitSampleManageDao.selectDnaTaskItemList(mapForQuery,
				startNum, limitNum, dir, sort);
	}

//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void save(SampleWaitDnaManage i) throws Exception {
//		splitSampleManageDao.saveOrUpdate(i);
//	}
//
//	public SampleWaitDnaManage get(String id) {
//		SampleWaitDnaManage splitSampleManage = commonDAO.get(
//				SampleWaitDnaManage.class, id);
//		return splitSampleManage;
//	}
//
//	@WriteOperLogTable
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void save(SampleWaitDnaManage sc, Map jsonMap) throws Exception {
//		if (sc != null) {
//			splitSampleManageDao.saveOrUpdate(sc);
//			String jsonStr = "";
//		}
//	}

	/**
	 * 保存中间产物管理
	 * 
	 * @param itemDataJson
	 * @throws Exception
	 */
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void saveSplitSampleInfoManager(String itemDataJson)
//			throws Exception {
//		List<SampleDnaInfo> saveItems = new ArrayList<SampleDnaInfo>();
//		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
//				itemDataJson, List.class);
//		for (Map<String, Object> map : list) {
//			SampleDnaInfo sbi = new SampleDnaInfo();
//			sbi = (SampleDnaInfo) splitSampleManageDao.Map2Bean(map, sbi);
//			if (sbi.getId() != null && sbi.getId().equals(""))
//				sbi.setId(null);
//			saveItems.add(sbi);
//			if (sbi.getNextFlow() != null && sbi.getSubmit() != null) {
//				if (sbi.getSubmit().equals("1")) {
//					if (sbi.getNextFlow().equals("0")) {
//						// 到文库构建
//						WkTaskTemp wa = new WkTaskTemp();
//						wa.setCode(sbi.getCode());
//						wa.setSampleCode(sbi.getSampleCode());
//						wa.setSequenceFun(sbi.getSequenceFun());
//						wa.setPatientName(sbi.getPatientName());
//						wa.setPhone(sbi.getPhone());
//						wa.setProductId(sbi.getProductId());
//						wa.setProductName(sbi.getProductName());
//						wa.setInspectDate(sbi.getInspectDate());
//						wa.setOrderId(sbi.getOrderId());
//						wa.setIdCard(sbi.getIdCard());
//						wa.setState("1");
//						wa.setVolume(sbi.getVolume());
//						wa.setClassify(sbi.getClassify());
//						splitSampleManageDao.saveOrUpdate(wa);
//						// 样本到建库，改变SampleInfo中原始样本的状态为“完成核酸提取”
//						SampleInfo sf = sampleInfoMainDao.findSampleInfo(sbi
//								.getSampleCode());
//						if (sf != null) {
//							sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_DNA_COMPLETE);
//							sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_DNA_COMPLETE_NAME);
//						}
//						sbi.setState("3");
//					} else if (sbi.getNextFlow().equals("1")) {
//						// 重新提取
//						DnaAbnormal da = new DnaAbnormal();
//						da.setClassify(sbi.getClassify());
//						da.setCode(sbi.getCode());
//						da.setFeedbackTime(DateUtil.dateFormatter(new Date()));
//						da.setIdCard(sbi.getIdCard());
//						da.setInspectDate(sbi.getInspectDate());
//						da.setNextFlow(sbi.getNextFlow());
//						da.setNote(sbi.getNote());
//						da.setOrderId(sbi.getOrderId());
//						da.setPatientName(sbi.getPatientName());
//						da.setPhone(sbi.getPhone());
//						da.setProductId(sbi.getProductId());
//						da.setProductName(sbi.getProductName());
//						da.setReportDate(sbi.getReportDate());
//						da.setResult(sbi.getResult());
//						da.setSampleCode(sbi.getSampleCode());
//						da.setSequenceFun(sbi.getSequenceFun());
//						this.splitSampleManageDao.saveOrUpdate(da);
//						sbi.setState("3");
//					} else if (sbi.getNextFlow().equals("2")) { // 入库
//						SampleInItemTemp st = new SampleInItemTemp();
//						st.setSampleCode(sbi.getSampleCode());
//						st.setNum(sbi.getVolume());
//						st.setState("1");
//						splitSampleManageDao.saveOrUpdate(st);
//						// 入库，改变SampleInfo中原始样本的状态为“待入库”
//						SampleInfo sf = sampleInfoMainDao.findSampleInfo(sbi
//								.getSampleCode());
//						if (sf != null) {
//							sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW);
//							sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW_NAME);
//						}
//						sbi.setState("3");
//					} else if (sbi.getNextFlow().equals("3")) {
//						// 反馈到项目组
//						FeedbackDna df = new FeedbackDna();
//						df.setCode(sbi.getCode());
//						df.setSampleCode(sbi.getSampleCode());
//						df.setSequenceFun(sbi.getSequenceFun());
//						df.setPatientName(sbi.getPatientName());
//						df.setPhone(sbi.getPhone());
//						df.setProductId(sbi.getProductId());
//						df.setProductName(sbi.getProductName());
//						df.setInspectDate(sbi.getInspectDate());
//						df.setReportDate(sbi.getReportDate());
//						df.setOrderId(sbi.getOrderId());
//						df.setIdCard(sbi.getIdCard());
//						df.setNextflow(sbi.getNextFlow());
//						df.setResult(sbi.getResult());
//						df.setClassify(sbi.getClassify());
//						df.setState("0");
//						splitSampleManageDao.saveOrUpdate(df);
//						sbi.setState("3");
//					} else if (sbi.getNextFlow().equals("4")) {
//						// 终止，改变SampleInfo中原始样本的状态为“实验终止”
//						SampleInfo sf = sampleInfoMainDao.findSampleInfo(sbi
//								.getSampleCode());
//						if (sf != null) {
//							sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
//							sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
//						}
//					} else {
//						// 暂停，改变SampleInfo中原始样本的状态为“实验暂停”
//						SampleInfo sf = sampleInfoMainDao.findSampleInfo(sbi
//								.getSampleCode());
//						if (sf != null) {
//							sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
//							sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
//						}
//					}
//				}
//			}
//		}
//		splitSampleManageDao.saveOrUpdateAll(saveItems);
//	}

	/**
	 * 保存样本管理(DnaTaskItem)
	 * 
	 * @param itemDataJson
	 * @throws Exception
	 */
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void saveDnaGetItemManager(String itemDataJson) throws Exception {
//		List<DnaTaskItem> saveItems = new ArrayList<DnaTaskItem>();
//		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
//				itemDataJson, List.class);
//		for (Map<String, Object> map : list) {
//			DnaTaskItem sbi = new DnaTaskItem();
//			sbi = (DnaTaskItem) splitSampleManageDao.Map2Bean(map, sbi);
//			if (sbi.getId() != null && sbi.getId().equals(""))
//				sbi.setId(null);
//			saveItems.add(sbi);
//			if (sbi.getNextFlow() != null && sbi.getResult() != null) {
//				if (sbi.getResult().equals("1")) {
//					if (sbi.getNextFlow().equals("0")) {
//						SampleInItemTemp st = new SampleInItemTemp();
//						if (sbi.getCode() == null) {
//							st.setCode(sbi.getSampleCode());
//						} else {
//							st.setCode(sbi.getCode());
//						}
//						st.setSampleCode(sbi.getSampleCode());
//						if (sbi.getSampleNum() != null
//								&& sbi.getSampleConsume() != null) {
//							st.setNum(sbi.getSampleNum()
//									- sbi.getSampleConsume());
//						}
//						st.setState("1");
//						splitSampleManageDao.saveOrUpdate(st);
//						// 入库，改变SampleInfo中原始样本的状态为“待入库”
//						SampleInfo sf = sampleInfoMainDao.findSampleInfo(sbi
//								.getSampleCode());
//						if (sf != null) {
//							sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW);
//							sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW_NAME);
//						}
//						sbi.setState("2");
//					} else {
//						// 终止，改变SampleInfo中原始样本的状态为“实验终止”
//						SampleInfo sf = sampleInfoMainDao.findSampleInfo(sbi
//								.getSampleCode());
//						if (sf != null) {
//							sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
//							sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
//						}
//					}
//				}
//			}
//		}
//		splitSampleManageDao.saveOrUpdateAll(saveItems);
//	}

	/**
	 * 核酸样本管理明细入库
	 * 
	 * @param ids
	 */
	public void dnaGetManageItemRuku(String ids) {
		String[] id1 = ids.split(",");
		for (String id : id1) {
			DnaTaskItem scp = splitSampleManageDao.get(DnaTaskItem.class, id);
			if (scp != null) {
				scp.setState("2");
				SampleInItemTemp st = new SampleInItemTemp();
				st.setSampleCode(scp.getSampleCode());
				st.setCode(scp.getCode());
				if (scp.getSampleNum() != null
						&& scp.getSampleConsume() != null) {
					st.setNum(scp.getSampleNum() - scp.getSampleConsume());
				}
				st.setState("1");
				splitSampleManageDao.saveOrUpdate(st);
			}
		}
	}

//	/**
//	 * 核酸样本管理明细入库
//	 * 
//	 * @param id
//	 */
//	public void dnaGetManageItemTiqu(String id) {
//		DnaTaskItem scp = splitSampleManageDao.get(DnaTaskItem.class, id);
//		if (scp != null) {
//			scp.setState("2");
//			DnaTaskTemp st = new DnaTaskTemp();
//			st.setCode(scp.getCode());
//			st.setSampleCode(scp.getSampleCode());
//			st.setLabCode(scp.getLabCode());
//			st.setProductId(scp.getProductId());
//			st.setProductName(scp.getProductName());
//			st.setSampleType(scp.getSampleType());
//			st.setPatientName(scp.getPatientName());
//			st.setReportDate(scp.getReportDate());
//			st.setSampleType(scp.getSampleType());
//
//			if (scp.getSampleNum() != null && scp.getSampleConsume() != null) {
//				st.setSampleNum(scp.getSampleNum() - scp.getSampleConsume());
//			}
//			st.setState("1");
//			splitSampleManageDao.saveOrUpdate(st);
//		}
//	}
}
