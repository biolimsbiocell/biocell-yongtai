package com.biolims.experiment.splitsample.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.experiment.check.model.TechCheckServiceTaskTemp;
import com.biolims.experiment.qc.model.Qc2100TaskTemp;
import com.biolims.experiment.qc.model.QcQpcrTaskTemp;
import com.biolims.experiment.splitsample.dao.SplitSampleItemDao;
import com.biolims.experiment.splitsample.model.SplitSampleItem;
import com.biolims.experiment.techreport.model.TechReport;
import com.biolims.experiment.uf.model.UfTaskTemp;
import com.biolims.experiment.wk.model.WkTaskTemp;
import com.biolims.experiment.wkLife.model.WkLifeTaskTemp;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.product.model.Product;
import com.biolims.util.JsonUtils;
import com.opensymphony.xwork2.ActionContext;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SplitSampleItemService {
	@Resource
	private SplitSampleItemDao splitSampleItemDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private NextFlowDao nextFlowDao;

	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private CommonService commonService;

	StringBuffer json = new StringBuffer();

	public Map<String, Object> findSplitSampleItemList(String classify, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return splitSampleItemDao.selectSplitSampleItemList(classify, start, length, query, col, sort);
	}

	public Map<String, Object> findSplitSampleItemList2(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return splitSampleItemDao.selectSplitSampleItemList2(start, length, query, col, sort);
	}

	public Map<String, Object> findSplitSampleItem1List(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return splitSampleItemDao.selectSplitSampleItem1List(mapForQuery, startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SplitSampleItem i) throws Exception {

		splitSampleItemDao.saveOrUpdate(i);

	}

	public SplitSampleItem get(String id) {
		SplitSampleItem splitSampleItem = commonDAO.get(SplitSampleItem.class, id);
		return splitSampleItem;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SplitSampleItem sc, Map jsonMap) throws Exception {
		if (sc != null) {
			splitSampleItemDao.saveOrUpdate(sc);

			String jsonStr = "";
		}
	}

	/**
	 * 保存明细
	 * 
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleItem(String itemDataJson, String logInfo) throws Exception {

		List<SplitSampleItem> saveItems = new ArrayList<SplitSampleItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SplitSampleItem sbi = new SplitSampleItem();
			sbi = (SplitSampleItem) splitSampleItemDao.Map2Bean(map, sbi);
			Double v1 = sbi.getContraction();
			Double v2 = sbi.getVolume();
			//显示设置的状态
			sbi.setState("1");
			if ((v1 != null && !"".equals(v1)) && (v2 != null && !"".equals(v2))) {
				sbi.setSampleNum(v1 * v2);
			}
			sbi.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			sbi.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			saveItems.add(sbi);
		}
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		splitSampleItemDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 按照检测项目拆分样本---多平台
	 * 
	 * @param id
	 * @throws Exception
	 */
	public String splitProductItem(String dnaCode, String splitCode) throws Exception {
		return this.splitSampleItemDao.findMaxSplitCode(dnaCode, splitCode);
	}

	/**
	 * 按照检测项目拆分样本--单平台
	 * 
	 * @param id
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void splitProductItemTwo(String id) throws Exception {
		SplitSampleItem wt = commonDAO.get(SplitSampleItem.class, id);
		if (wt != null) {
			if (wt.getProductId() != null && !wt.getProductId().equals("")) {
				String[] arr = wt.getProductId().split(",");
				String[] arr1 = wt.getProductName().split(",");
				for (int i = 0; i < arr.length; i++) {
					SplitSampleItem wtt = new SplitSampleItem();
					sampleInputService.copy(wtt, wt);
					String markCode = wt.getSplitCode();
					String code = codingRuleService.getCode1("SplitSampleItem", markCode, 00, 2, null);
					wtt.setSplitCode(code);
					wtt.setProductId(arr[i]);
					wtt.setProductName(arr1[i]);
					wtt.setCode(wt.getSplitCode());
					wtt.setState("1");
					wtt.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_PRODUCT_SPLIT_NAME);
					// wtt.setSplitCode(wt.getSplitCode()+"00"+String.valueOf(i+1));
					if(wt.getSampleOrder()!=null) {
						wtt.setSampleOrder(commonDAO.get(SampleOrder.class,
								wt.getSampleOrder().getId()));
					}
					splitSampleItemDao.saveOrUpdate(wtt);
				}
				wt.setState("0");
				splitSampleItemDao.saveOrUpdate(wt);
			}
		}
	}

	/**
	 * 按照检测项目拆分样本--单平台
	 * 
	 * @param id
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitSample(String  [] id) throws Exception {
		for (int i = 0; i < id.length; i++) {
			SplitSampleItem scp = commonDAO.get(SplitSampleItem.class, id[i]);
			if (scp != null) {

				String nextFlowId = scp.getNextFlowId();
				// NextFlow nf = commonService.get(NextFlow.class, nextFlowId);
				// nextFlowId = nf.getSysCode();

				if (nextFlowId.equals("0009")) {// 样本入库
					SampleInItemTemp st = new SampleInItemTemp();
					sampleInputService.copy(st, scp);
					scp.setSplitNum("1");
					scp.setState("1");
					st.setCode(scp.getCode());
					/**
					 * by 吴迪 2017-09-20 样本拆分后入库样本编号取的字段应该是拆分后的编号
					 */
					if (scp.getSplitCode() != null && !"".equals(scp.getSplitCode())) {
						st.setCode(scp.getSplitCode());
					}
					st.setSampleCode(scp.getSampleCode());
					st.setSampleType(scp.getSampleType());
					st.setSampleTypeId(scp.getSampleTypeId());
					st.setDicSampleType(scp.getDicSampleType());
					if (scp.getContraction() != null && !"".equals(scp.getContraction())) {
						st.setConcentration(Double.valueOf(scp.getContraction()));
					}
					if (scp.getVolume() != null && !"".equals(scp.getVolume())) {
						st.setVolume(Double.valueOf(scp.getVolume()));
					}
					if (scp.getSampleNum() != null && !"".equals(scp.getSampleNum())) {
						st.setSumTotal(Double.valueOf(scp.getSampleNum()));
					}
					st.setInfoFrom("SampleDnaInfo");
					// st.setNote("od1:" + scp.getOd260() + " od2:"
					// + scp.getOd280());
					st.setType("2");// 待建库样本
					st.setState("1");
					st.setSampleStyle("2");
					commonDAO.saveOrUpdate(st);
				} else if (nextFlowId.equals("0012")) {// 暂停
				} else if (nextFlowId.equals("0013")) {// 终止
				} else if (nextFlowId.equals("0018")) {// 文库构建
					WkTaskTemp d = new WkTaskTemp();
					scp.setState("2");
					d.setState("1");
					d.setScopeId(scp.getScopeId());
					d.setScopeName(scp.getScopeName());
					d.setProductId(scp.getProductId());
					d.setProductName(scp.getProductName());
					d.setConcentration(scp.getContraction());
					d.setVolume(scp.getVolume());
					d.setSampleCode(scp.getSampleCode());
					d.setCode(scp.getCode());
					DicSampleType t = commonDAO.get(DicSampleType.class, scp
							.getDicSampleType().getId());
					d.setSampleInfo(scp.getSampleInfo());
					d.setSampleType(t.getName());
					if(scp.getSampleOrder()!=null) {
						d.setSampleOrder(commonDAO.get(SampleOrder.class,
								scp.getSampleOrder().getId()));
					}
					commonDAO.saveOrUpdate(scp);
					commonDAO.saveOrUpdate(d);
				} else if (nextFlowId.equals("0066")) {// 文库构建life
					WkLifeTaskTemp d = new WkLifeTaskTemp();
					scp.setState("2");
					sampleInputService.copy(d, scp);
					d.setSampleInfo(scp.getSampleInfo());
					if (scp.getContraction() != null && !"".equals(scp.getContraction())) {
						d.setConcentration(scp.getContraction().toString());
					}
					if (scp.getVolume() != null && !"".equals(scp.getVolume())) {
						d.setVolume(Double.valueOf(scp.getVolume()));
					}
					// d.setIsZkp(scp.getIsZkp());

					if (scp.getQbContraction() != null && !"".equals(scp.getVolume())) {
						d.setQbcontraction(Double.valueOf(scp.getQbContraction()));
					}

					// if (scp.getReportDate() != null && !"".equals(scp.getReportDate())) {
					// d.setReportDate(DateUtil.parse(scp.getReportDate()));
					// }
					if (scp.getProductId() != null) {
						Product p = commonDAO.get(Product.class, scp.getProductId());
						if (p != null && p.getAcceptUserGroup() != null)
							d.setGroupId(p.getAcceptUserGroup().getId());
					}
					commonDAO.saveOrUpdate(d);
				} else if (nextFlowId.equals("0020")) {// 2100质控
					Qc2100TaskTemp qc2100 = new Qc2100TaskTemp();
					scp.setState("2");
					scp.setSplitNum("1");
					sampleInputService.copy(qc2100, scp);
					qc2100.setWkType("2");
					commonDAO.saveOrUpdate(qc2100);
				} else if (nextFlowId.equals("0021")) {// QPCR质控
					QcQpcrTaskTemp qpcr = new QcQpcrTaskTemp();
					scp.setState("2");
					scp.setSplitNum("1");
					sampleInputService.copy(qpcr, scp);
					qpcr.setWkType("1");
					commonDAO.saveOrUpdate(qpcr);
				} else if (nextFlowId.equals("0022")) {// 超声破碎
					UfTaskTemp uf = new UfTaskTemp();
					scp.setState("2");
					scp.setSplitNum("1");
					sampleInputService.copy(uf, scp);
				} else if (nextFlowId.equals("0035")) {// 检测报告
					TechReport tr = new TechReport();
					scp.setState("2");
					scp.setSplitNum("1");
					sampleInputService.copy(tr, scp);
					tr.setDnaCode(scp.getCode());
					commonDAO.saveOrUpdate(tr);
				} else if (nextFlowId.equals("0019")) {// 纯化质检TechCheckServiceTask
					TechCheckServiceTaskTemp d = new TechCheckServiceTaskTemp();
					scp.setState("2");
					scp.setSplitNum("1");
					sampleInputService.copy(d, scp);
					if (scp.getProductId() != null) {
						Product p = commonDAO.get(Product.class, scp.getProductId());
					}
					commonDAO.saveOrUpdate(d);
				}else {
					// 得到下一步流向的相关表单
					List<NextFlow> list_nextFlow = nextFlowDao.seletNextFlowById(nextFlowId);
					for (NextFlow n : list_nextFlow) {
						Object o = Class.forName(n.getApplicationTypeTable().getClassPath()).newInstance();
						scp.setState("1");
						scp.setSplitNum("1");
						sampleInputService.copy(o, scp);
						scp.setState("2");
					}
				}

			}
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			sampleStateService.saveSampleState(scp.getCode(), scp.getSampleCode(), scp.getProductId(),
					scp.getProductName(), "", "", format.format(new Date()), "SplitSampleItem", "分管平台",
					(User) ServletActionContext.getRequest().getSession()
							.getAttribute(SystemConstants.USER_SESSION_KEY),
					"", scp.getNextFlow(), "", null, null, null, null, null, null, null, null);
			scp.setState("2");
			scp.setSplitNum("1");
			commonService.saveOrUpdate(scp);
		}

	}

	/**
	 * 按数量拆分样本
	 * 
	 * @param id
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<String> splitNum(String id, Integer splitNums) throws Exception {
		SplitSampleItem ssi=null;
		SplitSampleItem wt = commonDAO.get(SplitSampleItem.class, id);
		List<String>list=new ArrayList<String>();
		if (wt != null) {
			for (int i = 0; i < splitNums; i++) {
				ssi = new SplitSampleItem();
				sampleInputService.copy(ssi, wt);
				String code = wt.getCode();
				if (splitNums < 8) {
					ssi.setSplitCode(code + "P0" + (i + 1));
					
				} else if (splitNums == 9) {
					ssi.setSplitCode(code + "P" + (i + 1));
				} else {
					ssi.setSplitCode(code + "P" + i);
				}
				ssi.setSplitNum("2");
				splitSampleItemDao.saveOrUpdate(ssi);
				if(ssi!=null&&ssi.getId()!=null) {
					String str=ssi.getId();
					list.add(str);
				}
			}
			wt.setState("0");
			
			splitSampleItemDao.saveOrUpdate(wt);
		}
		return list;
	}

	/**
	 * 查询是否已拆产品
	 * 
	 * @param id
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String selIsSplitProductSplitCode(String splitCode) throws Exception {
		return this.splitSampleItemDao.finIsSplitProductSplitCode(splitCode);
	}

	/**
	 * 查询是否已拆产品
	 * 
	 * @param id
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String selIsSplitProductCode(String code) throws Exception {
		return this.splitSampleItemDao.finIsSplitProductCode(code);
	}

}
