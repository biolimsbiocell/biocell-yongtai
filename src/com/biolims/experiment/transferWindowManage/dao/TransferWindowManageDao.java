package com.biolims.experiment.transferWindowManage.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.equipment.model.InstrumentState;
import com.biolims.experiment.transferWindowManage.model.TransferWindowManage;
import com.biolims.experiment.transferWindowManage.model.TransferWindowState;
import com.biolims.util.JsonUtils;

@Repository
@SuppressWarnings("unchecked")
public class TransferWindowManageDao extends BaseHibernateDao {
	public Map<String, Object> findTransferWindowManageTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from TransferWindowManage where 1=1 ";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from TransferWindowManage where 1=1 ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<TransferWindowManage> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
	//后添加的
	public List<TransferWindowManage> finAllTransferWindowManage(){
		String hql="from TransferWindowManage where 1=1";
		//Query query = this.getSession().createQuery(hql).addEntity(TransferWindowManage.class);	
		
		List<TransferWindowManage> list = new ArrayList<TransferWindowManage>();
		list=getSession().createQuery(hql).list();

		
		return list;
	}

	public Map<String, Object> selectTransferWindowTableJson(Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from TransferWindowManage where 1=1 and transferWindowState='1' ";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from TransferWindowManage where 1=1 and transferWindowState='1' ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<TransferWindowManage> list = new ArrayList<TransferWindowManage>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}
	
	public Map<String, Object> selectTransferWindowTableAllJson(Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from TransferWindowManage where 1=1  ";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from TransferWindowManage where 1=1  ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<TransferWindowManage> list = new ArrayList<TransferWindowManage>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}
	
	public Map<String, Object> findTransferWindowStateTable(Integer start, Integer length, String query, String col,
			String sort, String id) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String key = "";
		String scancode = "";//扫码
		Map<String, String> map2Query = new HashMap<String, String>();
		if (query != null && !"".equals(query)) {
			map2Query = JsonUtils.toObjectByJson(query, Map.class);
			id = (String) map2Query.get("id");
			scancode=(String) map2Query.get("scancode");
			map2Query.remove("scancode");
			map2Query.remove("id");
			if(null!=scancode&&!"".equals(scancode)) {
				if(scancode.equals("0")) {
					key =" and qualityReceiveTime='' and qualityReceiveUser='' ";
				}else if(scancode.equals("1")) {
					key =" and qualityReceiveTime!='' and qualityReceiveUser!=''";
				}
			}
			key += map2where(map2Query);
		}
		String countHql = "select count(*) from TransferWindowState where 1=1 and transferWindowManage.id='" + id + "'";
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from TransferWindowState where 1=1 and transferWindowManage.id='" + id + "' ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<TransferWindowState> list = new ArrayList<TransferWindowState>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
	//后添加的
	public List<TransferWindowManage> checkRegionalManagement(String regionalManagementId){
		String hql="from TransferWindowManage where 1=1 and regionalManagement='"+regionalManagementId+"'";
		
		List<TransferWindowManage> list = new ArrayList<TransferWindowManage>();
		list=getSession().createQuery(hql).list();
		return list;
	}
}