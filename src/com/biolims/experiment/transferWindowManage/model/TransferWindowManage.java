package com.biolims.experiment.transferWindowManage.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.experiment.regionalManagement.model.RegionalManagement;

/**
 * @Title: Model
 * @Description: 房间管理
 * @author lims-platform
 * @date 2016-03-07 11:01:47
 * @version V1.0
 * 
 */
@Entity
@Table(name = "TRANSFER_WINDOW_MANAGE")
@SuppressWarnings("serial")
public class TransferWindowManage extends EntityDao<TransferWindowManage> implements java.io.Serializable, Cloneable {
	/** 传递窗编号 */
	private String id;
	/** 传递窗名称 */
	private String transferWindowName;
	/** 传递窗状态 */
	private String transferWindowState;
	/** 传递窗描述 */
	private String describe;
	/** 创建日期 */
	private Date createDate;
	/** 创建人 */
	private User createUser;
//	/** 审核人 */
//	private User auditor;
	/** 状态 */
	private String state;
	/** 状态名称 */
	private String stateName;
	/** 备注 */
	private String name;
	/** 区域 */
	private RegionalManagement regionalManagement;
//	/** 是否占用 */
//	private String isFull;
//	/** 房间平方米*/
//	private String centiare; 
//	/** 房间立方米*/
//	private String stere;
	/**结果时间的有效期*/
	private String validityPeriod;
//	/**测试时间*/
//	private String testingTime;
//	/**倒计时*/
//	private String cuntdown;

	

	public String getValidityPeriod() {
		return validityPeriod;
	}

	public String getTransferWindowName() {
		return transferWindowName;
	}

	public void setTransferWindowName(String transferWindowName) {
		this.transferWindowName = transferWindowName;
	}

	public String getTransferWindowState() {
		return transferWindowState;
	}

	public void setTransferWindowState(String transferWindowState) {
		this.transferWindowState = transferWindowState;
	}

	public void setValidityPeriod(String validityPeriod) {
		this.validityPeriod = validityPeriod;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	public RegionalManagement getRegionalManagement() {
		return regionalManagement;
	}

	public void setRegionalManagement(RegionalManagement regionalManagement) {
		this.regionalManagement = regionalManagement;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	@Column(name = "NOTE", length = 4000)
	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return createUser;
	}

	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码
	 */

	@Id
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             编码
	 */
	public void setId(String id) {
		this.id = id;
	}

}
