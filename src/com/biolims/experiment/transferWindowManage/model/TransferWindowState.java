package com.biolims.experiment.transferWindowManage.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.system.detecyion.model.SampleDeteyion;

@Entity
@Table(name = "TRANSFER_WINDOW_STATE")
public class TransferWindowState implements Serializable {
	

	private static final long serialVersionUID = -407759837281114436L;

	private String id;// ID
	/** 检验样本编号 */
	private String code;
	/**检测项*/
	private SampleDeteyion sampleDetecyion;
	/** 生产提交时间 */
	private Date qualitySubmitTime;
	/** 生产提交人 */
	private String qualitySubmitUser;
	/** QC扫码时间 */
	private Date qualityReceiveTime;
	/** QC扫码人 */
	private String qualityReceiveUser;
	/** 样本状态  1合格0不合格  是否超过30分钟 */
	private String sampleState;
	/** 数据显示 1显示0隐藏  */
	private String state;
	//生产批号
	private String batch;
	
	
	private TransferWindowManage transferWindowManage;//主表
	
	
	
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getQualitySubmitTime() {
		return qualitySubmitTime;
	}

	public void setQualitySubmitTime(Date qualitySubmitTime) {
		this.qualitySubmitTime = qualitySubmitTime;
	}

	public String getQualitySubmitUser() {
		return qualitySubmitUser;
	}

	public void setQualitySubmitUser(String qualitySubmitUser) {
		this.qualitySubmitUser = qualitySubmitUser;
	}

	public Date getQualityReceiveTime() {
		return qualityReceiveTime;
	}

	public void setQualityReceiveTime(Date qualityReceiveTime) {
		this.qualityReceiveTime = qualityReceiveTime;
	}

	public String getQualityReceiveUser() {
		return qualityReceiveUser;
	}

	public void setQualityReceiveUser(String qualityReceiveUser) {
		this.qualityReceiveUser = qualityReceiveUser;
	}

	public String getSampleState() {
		return sampleState;
	}

	public void setSampleState(String sampleState) {
		this.sampleState = sampleState;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "transfer_window_manage")
	public TransferWindowManage getTransferWindowManage() {
		return transferWindowManage;
	}

	public void setTransferWindowManage(TransferWindowManage transferWindowManage) {
		this.transferWindowManage = transferWindowManage;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_DETECYION")
	public SampleDeteyion getSampleDetecyion() {
		return sampleDetecyion;
	}

	public void setSampleDetecyion(SampleDeteyion sampleDetecyion) {
		this.sampleDetecyion = sampleDetecyion;
	}

	

	/**
	 * @return id
	 * @author zhiqiang.yang@biolims.cn
	 */
	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setId(String id) {
		this.id = id;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	

}
