package com.biolims.experiment.wkLife.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;

import com.biolims.experiment.wkLife.model.SampleWkLifeInfo;
import com.biolims.experiment.wkLife.model.WkLifeManage;
import com.biolims.experiment.wkLife.model.WkLifeTask;
import com.biolims.experiment.wkLife.model.WkLifeTaskModify;
import com.biolims.experiment.wkLife.model.WkLifeTaskTemp;
import com.biolims.experiment.wkLife.service.WKLifeReceiveService;
import com.biolims.experiment.wkLife.service.WKLifeSampleTaskService;
import com.biolims.experiment.wkLife.service.WkLifeManageService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

@Namespace("/experiment/wkLife/wkTastModify")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class WkLifeTastModifyAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "249899";
	@Autowired
	private WKLifeSampleTaskService wkLifeService;
	@Resource
	private WKLifeReceiveService wKLifeReceiveService;
	private WkLifeTaskModify wk = new WkLifeTaskModify();

	@Resource
	private CodingRuleService codingRuleService;
	@Autowired
	private WkLifeManageService wkManageService;
	private WkLifeManage wkManage = new WkLifeManage();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;

	/**
	 * 重建库
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showWKTastModifyList")
	public String showWKTastModifyList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/wkLife/wKLifeTaskModify.jsp");
	}

	/**
	 * 
	 * @Title: showWKRepeatListJson
	 * @Description: TODO(查询对应的取消数据列表信息)
	 * @param @throws Exception    设定文件
	 * @return void    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-8-3 下午3:36:12
	 * @throws
	 */
	@Action(value = "showWKTastModifyListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWKTastModifyListJson() throws Exception {
		int startNum = 0;
		int limitNum =200;
		String ss = getParameterFromRequest("start");
		String sl=getParameterFromRequest("limit");
		if (ss.equals("")) {
			 startNum = Integer.parseInt("0");	
		}else {
			 startNum = Integer.parseInt(ss);
		}
		if (sl.equals("")) {
			limitNum = Integer.parseInt("200");	
		}else {
			 sl=getParameterFromRequest("limit");
			 limitNum = Integer.parseInt(sl);
		}


		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = wkLifeService.findWKTastModifyList(map2Query,
				startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<WkLifeTask> list = (List<WkLifeTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("reciveUser-id", "");
		map.put("reciveUser-name", "");
		map.put("reciveDate", "yyyy-MM-dd");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("indexa", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("qcNum", "");
		// 实验组
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd HH:mm:ss");
		map.put("type", "");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("testUserOneId", "");
		map.put("testUserOneName", "");
		map.put("testUserTwoId", "");
		map.put("testUserTwoName", "");
		map.put("seType", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}
	
	/**
	 * 
	 * @Title: libraryRollbackManage
	 * @Description: TODO(对于错误数据进行回退，且没有执行下一步)
	 * @param @throws Exception    设定文件
	 * @return void    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-8-2 上午10:52:37
	 * @throws
	 */
	@Action(value = "libraryRollbackManage")
	public void libraryRollbackManage() throws Exception {
		// String id=getRequest().getParameter("code");
		Boolean flag;
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String[] id = getRequest().getParameterValues("ID");
			// wkManageService.saveWkManage(itemDataJson);
			flag = wkManageService.changeStateForLibrary(id);
			if (flag) {
				result.put("success", true);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 
	 * @Title: save
	 * @Description: 保存表数据
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return String    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-8-4 下午4:32:26
	 * @throws
	 */
	@Action(value = "save")
	public String save() throws Exception {
		String id = wk.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "WkLifeTaskModify";
			String markCode = "WKGJM";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			wk.setId(autoID);
		}
		Map aMap = new HashMap();

		aMap.put("wKTastModifyItemJson",
				getParameterFromRequest("wKTastModifyItemJson"));

		wkLifeService.save(wk, aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/experiment/wkLife/wkTastModify/showWKTastModifyTempEdit.action?id="
				+ wk.getId() + "&qcNum=" + wk.getQcNum();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);

	}

	/**
	 * 
	 * @Title: showWKTastModifyTempListJson
	 * @Description: TODO(文库构建取消的新建按钮的左侧列表)
	 * @param     设定文件
	 * @return void    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-8-3 下午5:51:49
	 * @throws
	 */
	@Action(value = "showWKTastModifyTempEdit")
	public String showWKTastModifyTempEdit() throws Exception {
		String id = getParameterFromRequest("id");
		String maxNum = getParameterFromRequest("maxNum");
		String qcNum = getParameterFromRequest("qcNum");
		long num = 0;
		long num1 = 0;
		long num2 = 0;
		if (id != null && !id.equals("")) {
			wk = wkLifeService.getWkTastModify(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "wK");
			num1 = fileInfoService.findFileInfoCount(id, "wKdyt");
			num2 = fileInfoService.findFileInfoCount(id, "wK2100");
			String tName = workflowProcessInstanceService
					.findToDoTaskNameByFormId(id);
			putObjToContext("taskName", tName);
		} else {
			wk.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			wk.setCreateUser(user);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stime = format.format(date);
			wk.setCreateDate(stime);
			wk.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			wk.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(wk.getState());
		putObjToContext("maxNum", maxNum);
		putObjToContext("qcNum", qcNum);
		putObjToContext("fileNum", num);
		putObjToContext("fileNum1", num1);
		putObjToContext("fileNum2", num2);
		return dispatcher("/WEB-INF/page/experiment/wkLife/wKLifeTaskModifyEdit.jsp");
	}

	/**
	 * 
	 * @Title: showWKReceiveItemList
	 * @Description: TODO(左侧表查询)
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return String    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-8-4 上午9:38:54
	 * @throws
	 */
	@Action(value = "showWKTastModifyTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWKTastModifyTempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/wkLife/wKLifeTastModifyTemp.jsp");
	}

	/**
	 * 
	 * @Title: showWKTastModifyTempListJson
	 * @Description: TODO(查询左侧列表待处理数据信息)
	 * @param @return    设定文件
	 * @return String    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-8-4 上午10:42:54
	 * @throws
	 */
	@Action(value = "showWKTastModifyTempList1Json", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWKTastModifyTempListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");

		// 当前用所在人员组Id
		String groups = (String) this
				.getObjFromSession(SystemConstants.USER_SESSION_GROUPS);
		User user = (User) this
				.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		try {
			// String scId = getRequest().getParameter("id");
			Map<String, Object> result = wKLifeReceiveService
					.findWKTastModifyTempList(map2Query, startNum, limitNum,
							dir, sort, groups, user.getId());
			Long total = (Long) result.get("total");
			List<WkLifeTaskTemp> list = (List<WkLifeTaskTemp>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("sampleCode", "");
			map.put("splitCode", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("volume", "");
			map.put("unit", "");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("note", "");
			map.put("classify", "");
			map.put("sampleType", "");
			map.put("sampleNum", "");
			map.put("labCode", "");
			map.put("RIN", "");
			map.put("concentration", "");
			map.put("qbcontraction", "");
			map.put("sumVolume", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("techJkServiceTask-sequenceBillDate", "yyyy-MM-dd");
			map.put("techJkServiceTask-sequencePlatform", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("counts", "");
			map.put("isZkp", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			map.put("tjItem-wgcId", "");
			map.put("tjItem-species", "");
			map.put("tjItem-dataNum", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("insertSize", "");
			map.put("indexa", "");
			map.put("pcrRatio", "");
			map.put("dxpdSumTotal", "");
			map.put("loopNum", "");
			map.put("wkSumTotal", "");
			map.put("wkVolume", "");
			map.put("dxpdVolume", "");
			map.put("reason", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: showWKTastModifyItemList
	 * @Description: TODO(查找文库构建取消下的明细页面)
	 * @param @return
	 * @param @throws Exception    设定文件
	 * @return String    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-8-4 下午2:54:37
	 * @throws
	 */
	@Action(value = "showWKTastModifyItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWKTastModifyItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/wkLife/wKLifeTastModifyItem.jsp");
	}

	/**
	 * 
	 * @Title: showWKItemListJson
	 * @Description: TODO(文库构建明细列表查询)
	 * @param @throws Exception    设定文件
	 * @return void    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-8-4 下午3:09:43
	 * @throws
	 */
	@Action(value = "showWKTastModifyItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWKTastModifyItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wkLifeService.findWKTaastModifyItemList(
					scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<SampleWkLifeInfo> list = (List<SampleWkLifeInfo>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("wkCode", "");
			map.put("indexa", "");
			map.put("sampleCode", "");
			map.put("splitCode", "");
			map.put("volume", "");
			map.put("unit", "");
			map.put("result", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("reason", "");
			map.put("submit", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("note", "");
			map.put("state", "");
			map.put("method", "");
			map.put("isExecute", "");
			map.put("wk-name", "");
			map.put("wk-id", "");

			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("counts", "");
			map.put("contractId", "");
			map.put("projectId", "");
			map.put("orderType", "");
			map.put("jkTaskId", "");
			map.put("classify", "");

			map.put("dataType", "");
			map.put("dataNum", "");
			map.put("sampleType", "");
			map.put("i5", "");
			map.put("i7", "");
			map.put("concentration", "");
			map.put("loopNum", "");
			map.put("sumTotal", "");
			map.put("pcrRatio", "");
			map.put("expectNum", "");
			map.put("sampleNum", "");
			map.put("tempId", "");
			map.put("sampleInfo-idCard", "");
			map.put("sampleInfo-orderNum", "");

			map.put("chpsConcentration", "");
			map.put("chVolume", "");
			map.put("chSumTotal", "");
			map.put("indexConcentration", "");
			map.put("dxpdConcentration", "");
			map.put("dxpdVolume", "");
			map.put("dxpdSumTotal", "");
			map.put("wkConcentration", "");
			map.put("wkVolume", "");
			map.put("wkSumTotal", "");
			map.put("labCode", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("isZkp", "");

			// 文库类型
			map.put("techJkServiceTask-libraryType", "");
			// 插入片段
			map.put("techJkServiceTask-insertSize", "");
			// 测序类型
			map.put("techJkServiceTask-sequenceType", "");

			map.put("libType", "");
			map.put("insertSize", "");
			map.put("libNum", "");
			map.put("sampleNowNum", "");
			map.put("takeNum", "");
			map.put("breakNum", "");
			map.put("teOrH20num", "");
			map.put("dataNowNum", "");
			map.put("storageLocation", "");
			map.put("seqType", "");
			map.put("primer", "");
			map.put("isInit", "");
			map.put("species", "");
			map.put("isRna", "");
			map.put("sampleVolume", "");
			map.put("inwardCode", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");

			map.put("unitGroup-id", "");
			map.put("unitGroup-name", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @return wk
	 * @author zhiqiang.yang@biolims.cn
	 */
	public WkLifeTaskModify getWk() {
		return wk;
	}

	/**
	 * @param wk
	 *            the wk to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setWk(WkLifeTaskModify wk) {
		this.wk = wk;
	}
}
