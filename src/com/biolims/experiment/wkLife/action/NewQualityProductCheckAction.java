
package com.biolims.experiment.wkLife.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.project.feedback.model.FeedbackWk;
import com.biolims.project.feedback.service.WkFeedbackService;
import com.biolims.util.DateHelper;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.util.HttpUtils;
import com.biolims.common.model.user.User;
import com.biolims.experiment.wkLife.model.WkLifeTaskItem;
import com.biolims.experiment.wkLife.service.NewQualityProductCheckService;
import com.biolims.file.service.FileInfoService;
@Namespace("/experiment/wkLife/qualityCheck")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class NewQualityProductCheckAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393697L;
	private String rightsId = "240306";
	@Autowired
	private NewQualityProductCheckService qualityProductCheckService;
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showQualityProductList")
	public String showQualityProductList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/wkLife/qualityProductCheck.jsp");
	}

	@Action(value = "showQualityProductListJson")
	public void showQualityProductListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = qualityProductCheckService.findQualityProductList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<WkLifeTaskItem> list = (List<WkLifeTaskItem>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("orderNumber", "");
		map.put("name", "");
		map.put("wkId", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("indexa", "");
		map.put("concentration", "");
		map.put("result", "");
		map.put("reason", "");
		map.put("stepNum", "");
		map.put("note", "");
		map.put("phone", "");
		map.put("orderId", "");
		map.put("patientName", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("inspectDate", "");
		map.put("acceptDate", "");
		map.put("idCard", "");
		map.put("sequenceFun", "");
		map.put("reportDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("rowCode", "");
		map.put("colCode", "");
		map.put("wk-name", "");
		map.put("wk-id", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "saveQualityProduct", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveQualityProduct() throws Exception {

		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String dataJson = getRequest().getParameter("itemDataJson");
			WkLifeTaskItem a = new WkLifeTaskItem();
			qualityProductCheckService.saveQualityProduct(a, dataJson);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}


	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public NewQualityProductCheckService getQualityProductCheckService() {
		return qualityProductCheckService;
	}

	public void setQualityProductCheckService(NewQualityProductCheckService qualityProductCheckService) {
		this.qualityProductCheckService = qualityProductCheckService;
	}



}
