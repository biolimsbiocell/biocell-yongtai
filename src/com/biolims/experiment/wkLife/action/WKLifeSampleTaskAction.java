﻿package com.biolims.experiment.wkLife.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.experiment.wkLife.dao.WKLifeSampleTaskDao;
import com.biolims.experiment.wkLife.model.SampleWkLifeInfo;
import com.biolims.experiment.wkLife.model.SampleWkLifeInfoCtDNA;
import com.biolims.experiment.wkLife.model.SampleWkLifeInfomRNA;
import com.biolims.experiment.wkLife.model.SampleWkLifeInforRNA;
import com.biolims.experiment.wkLife.model.WkLifeTask;
import com.biolims.experiment.wkLife.model.WkLifeTaskCos;
import com.biolims.experiment.wkLife.model.WkLifeTaskItem;
import com.biolims.experiment.wkLife.model.WkLifeTaskReagent;
import com.biolims.experiment.wkLife.model.WkLifeTaskTemp;
import com.biolims.experiment.wkLife.model.WkLifeTaskTemplate;
import com.biolims.experiment.wkLife.service.WKLifeReceiveService;
import com.biolims.experiment.wkLife.service.WKLifeSampleTaskService;
import com.biolims.file.service.FileInfoService;
import com.biolims.storage.main.service.StorageService;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

@Namespace("/experiment/wkLife")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class WKLifeSampleTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "249802";
	@Resource
	private WKLifeSampleTaskService wkLifeService;
	private WkLifeTask wk = new WkLifeTask();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WKLifeSampleTaskDao wKLifeSampleTaskDao;
	@Resource
	private WKLifeReceiveService wKReceiveService;
	@Resource
	private StorageService storageService;
	
	
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;
	@Resource
	private CommonService commonService;

	@Action(value = "showWKList")
	public String showWKList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/wkLife/wKLifeSampleTask.jsp");
	}

	@Action(value = "showWKListJson")
	public void showWKListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = wkLifeService.findWKList(map2Query, startNum,
				limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<WkLifeTask> list = (List<WkLifeTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("reciveUser-id", "");
		map.put("reciveUser-name", "");
		map.put("reciveDate", "yyyy-MM-dd");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("indexa", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("qcNum", "");
		// 实验组
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd HH:mm:ss");
		map.put("type", "");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("testUserOneId", "");
		map.put("testUserOneName", "");
		map.put("testUserTwoId", "");
		map.put("testUserTwoName", "");
		map.put("seType", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "wKSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogWKList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/wkLife/wKLifeDialog.jsp");
	}

	@Action(value = "showDialogWKListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogWKListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = wkLifeService.findWKList(map2Query, startNum,
				limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<WkLifeTask> list = (List<WkLifeTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("reciveUser-id", "");
		map.put("reciveUser-name", "");
		map.put("reciveDate", "yyyy-MM-dd");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("type", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editWK")
	public String editWK() throws Exception {
		String id = getParameterFromRequest("id");
		String maxNum = getParameterFromRequest("maxNum");
		String qcNum = getParameterFromRequest("qcNum");
		long num = 0;
		long num1 = 0;
		long num2 = 0;
		if (id != null && !id.equals("")) {
			wk = wkLifeService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "wK");
			num1 = fileInfoService.findFileInfoCount(id, "wKdyt");
			num2 = fileInfoService.findFileInfoCount(id, "wK2100");
			String tName = workflowProcessInstanceService
					.findToDoTaskNameByFormId(id);
			putObjToContext("taskName", tName);
		} else {
			wk.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			wk.setCreateUser(user);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stime = format.format(date);
			wk.setCreateDate(stime);
			wk.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			wk.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(wk.getState());
		putObjToContext("maxNum", maxNum);
		putObjToContext("qcNum", qcNum);
		putObjToContext("fileNum", num);
		putObjToContext("fileNum1", num1);
		putObjToContext("fileNum2", num2);
		return dispatcher("/WEB-INF/page/experiment/wkLife/wKLifeSampleTaskEdit.jsp");
	}

	@Action(value = "copyWK")
	public String copyWK() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		wk = wkLifeService.get(id);
		wk.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/wkLife/wKLifeSampleTaskEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = wk.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "WkLifeTask";
			String markCode = "WKGL";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			wk.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("wKItem", getParameterFromRequest("wKItemJson"));

		aMap.put("wKTemplate", getParameterFromRequest("wKTemplateJson"));

		aMap.put("wKReagent", getParameterFromRequest("wKReagentJson"));

		aMap.put("wKCos", getParameterFromRequest("wKCosJson"));

		aMap.put("wKSampleInfo", getParameterFromRequest("wKSampleInfoJson"));

		aMap.put("wKSampleInfoCtDNA",
				getParameterFromRequest("wKSampleInfoCtDNAJson"));

		aMap.put("wKSampleInforRNA",
				getParameterFromRequest("wKSampleInforRNAJson"));

		aMap.put("wKSampleInfomRNA",
				getParameterFromRequest("wKSampleInfomRNAJson"));
		wkLifeService.save(wk, aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/experiment/wkLife/editWK.action?id=" + wk.getId()
				+ "&qcNum=" + wk.getQcNum();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);

	}

	@Action(value = "saveAjax")
	public void saveAjax() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String id = getParameterFromRequest("id");
			wk = commonService.get(WkLifeTask.class, id);

			Map aMap = new HashMap();
			aMap.put("wKItem", getParameterFromRequest("wKItemJson"));

			aMap.put("wKTemplate", getParameterFromRequest("wKTemplateJson"));

			aMap.put("wKReagent", getParameterFromRequest("wKReagentJson"));

			aMap.put("wKCos", getParameterFromRequest("wKCosJson"));

			aMap.put("wKSampleInfo",
					getParameterFromRequest("wKSampleInfoJson"));

			aMap.put("wKSampleInfoCtDNA",
					getParameterFromRequest("wKSampleInfoCtDNAJson"));

			aMap.put("wKSampleInforRNA",
					getParameterFromRequest("wKSampleInforRNAJson"));

			aMap.put("wKSampleInfomRNA",
					getParameterFromRequest("wKSampleInfomRNAJson"));
			map =wkLifeService.save(wk, aMap);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));

	}

	@Action(value = "viewWK")
	public String toViewWK() throws Exception {
		String id = getParameterFromRequest("id");
		wk = wkLifeService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/wkLife/wKLifeSampleTaskEdit.jsp");
	}

	@Action(value = "showWKItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWKItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/wkLife/wKLifeItem.jsp");
	}

	@Action(value = "showWKItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWKItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wkLifeService.findWKItemList(scId,
					startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<WkLifeTaskItem> list = (List<WkLifeTaskItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("orderNumber", "");
			map.put("name", "");
			map.put("wkId", "");
			map.put("sampleNum", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("splitCode", "");
			map.put("indexa", "");
			map.put("concentration", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("stepNum", "");
			map.put("note", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("wk-name", "");
			map.put("wk-id", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("counts", "");
			map.put("volume", "");
			map.put("unit", "");
			map.put("addVolume", "");
			map.put("sumVolume", "");
			map.put("contractId", "");
			map.put("projectId", "");
			map.put("orderType", "");
			map.put("jkTaskId", "");
			map.put("classify", "");
			map.put("dataType", "");
			map.put("dataNum", "");
			map.put("productNum", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("sampleType", "");
			map.put("i5", "");
			map.put("i7", "");
			map.put("tag", "");
			map.put("loopNum", "");
			map.put("sumTotal", "");
			map.put("pcrRatio", "");
			map.put("expectNum", "");
			map.put("sampleConsume", "");
			map.put("tempId", "");
			map.put("rin", "");
			map.put("labCode", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			map.put("sampleInfo-patientName", "");
			map.put("sampleInfo-idCard", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("isZkp", "");
			map.put("blx", "");
			map.put("blendCode", "");

			// 文库类型
			map.put("techJkServiceTask-libraryType", "");
			// 插入片段
			map.put("techJkServiceTask-insertSize", "");
			// 测序类型
			map.put("techJkServiceTask-sequenceType", "");

			map.put("libType", "");
			map.put("insertSize", "");
			map.put("libNum", "");
			map.put("sampleNowNum", "");
			map.put("takeNum", "");
			map.put("breakNum", "");
			map.put("teOrH20num", "");
			map.put("dataNowNum", "");
			map.put("storageLocation", "");
			map.put("seqType", "");
			map.put("primer", "");
			map.put("isInit", "");
			map.put("species", "");
			map.put("isRna", "");
			map.put("sampleVolume", "");
			map.put("inwardCode", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			map.put("tjItem-wgcId", "");
			map.put("tjItem-species", "");
			map.put("chipType", "");
			map.put("qubitConcentration", "");
			map.put("NanodropConcentration", "");
			map.put("kit", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWKItem")
	public void delWKItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wkLifeService.delWKItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showWKTemplateList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWKTemplateList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/wkLife/wKLifeTemplate.jsp");
	}

	@Action(value = "showWKTemplateListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWKTemplateListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wkLifeService.findWKTemplateList(scId,
					startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<WkLifeTaskTemplate> list = (List<WkLifeTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("testUserId", "");
			map.put("testUserName", "");
			map.put("sampleCodes", "");
			map.put("tItem", "");
			map.put("state", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("note", "");
			map.put("code", "");
			map.put("wk-name", "");
			map.put("wk-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWKTemplate")
	public void delWKTemplate() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wkLifeService.delWKTemplate(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// ===========2015-12-02 ly===================
	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWKTemplateOne")
	public void delWKTemplateOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			wkLifeService.delWKTemplateOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// ==============================

	@Action(value = "showWKReagentList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWKReagentList() throws Exception {
		// String itemId=getParameterFromRequest("itemId");
		// putObjToContext("itemId",itemId);
		return dispatcher("/WEB-INF/page/experiment/wkLife/wKLifeReagent.jsp");
	}

	@Action(value = "showWKReagentListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWKReagentListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			/*
			 * String scId = getRequest().getParameter("id"); Map<String,
			 * Object> result = wkLifeService.findWKReagentList(scId, startNum,
			 * limitNum, dir, sort);
			 */
			String scId = getRequest().getParameter("id");
			// String itemId=getParameterFromRequest("itemId");
			Map<String, Object> result = new HashMap<String, Object>();
			result = wkLifeService.findWKReagentList(scId, startNum, limitNum, dir,
					sort);
			// if(itemId.equals("")){
			// result = wkLifeService.findWKReagentList(scId, startNum, limitNum,
			// dir,
			// sort);
			// }else{
			// result = wkLifeService.findReagentItemListByItemId(scId, startNum,
			// limitNum, dir,
			// sort,itemId);
			// }
			Long total = (Long) result.get("total");
			List<WkLifeTaskReagent> list = (List<WkLifeTaskReagent>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("batch", "");
			map.put("count", "");
			map.put("oneNum", "");
			map.put("sampleNum", "");
			map.put("num", "");
			map.put("note", "");
			map.put("isGood", "");
			map.put("itemId", "");
			map.put("tReagent", "");
			map.put("wk-name", "");
			map.put("wk-id", "");
			map.put("sn", "");
			map.put("expireDate", "yyyy-MM-dd");
			map.put("reagentCode", "");
			map.put("isRunout", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWKReagent")
	public void delWKReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wkLifeService.delWKReagent(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// ============2015-12-02 ly=================
	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWKReagentOne")
	public void delWKReagentOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			wkLifeService.delWKReagentOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// =============================

	@Action(value = "showWKCosList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWKCosList() throws Exception {
		String itemId = getParameterFromRequest("itemId");
		putObjToContext("itemId", itemId);
		return dispatcher("/WEB-INF/page/experiment/wkLife/wKLifeCos.jsp");
	}

	@Action(value = "showWKCosListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWKCosListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			// String scId = getRequest().getParameter("id");
			// Map<String, Object> result = wkLifeService.findWKCosList(scId,
			// startNum, limitNum, dir, sort);
			String scId = getRequest().getParameter("id");
			String itemId = getParameterFromRequest("itemId");
			Map<String, Object> result = new HashMap<String, Object>();
			if (itemId.equals("")) {
				result = wkLifeService.findWKCosList(scId, startNum, limitNum, dir,
						sort);
			} else {
				result = wkLifeService.findCosItemListByItemId(scId, startNum,
						limitNum, dir, sort, itemId);
			}
			Long total = (Long) result.get("total");
			List<WkLifeTaskCos> list = (List<WkLifeTaskCos>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("state", "");
			map.put("name", "");
			map.put("temperature", "");
			map.put("speed", "");
			map.put("time", "");
			map.put("note", "");
			map.put("itemId", "");
			map.put("tCos", "");
			map.put("isGood", "");
			map.put("wk-name", "");
			map.put("wk-id", "");
			map.put("type-id", "");
			map.put("type-name", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWKCos")
	public void delWKCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wkLifeService.delWKCos(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// ===========2015-12-02 ly=================
	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWKCosOne")
	public void delWKCosOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			wkLifeService.delWKCosOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// FFPE，血液文库结果

	@Action(value = "showWKResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWKResultList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/wkLife/wKLifeSampleInfo.jsp");
	}

	@Action(value = "showWKResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWKResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wkLifeService.findWKResultList(scId,
					startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<SampleWkLifeInfo> list = (List<SampleWkLifeInfo>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("wkCode", "");
			map.put("indexa", "");
			map.put("sampleCode", "");
			map.put("splitCode", "");
			map.put("volume", "");
			map.put("unit", "");
			map.put("result", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("reason", "");
			map.put("submit", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("note", "");
			map.put("state", "");
			map.put("method", "");
			map.put("isExecute", "");
			map.put("wk-name", "");
			map.put("wk-id", "");

			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("counts", "");
			map.put("contractId", "");
			map.put("projectId", "");
			map.put("orderType", "");
			map.put("jkTaskId", "");
			map.put("classify", "");

			map.put("dataType", "");
			map.put("dataNum", "");
			map.put("sampleType", "");
			map.put("i5", "");
			map.put("i7", "");
			map.put("concentration", "");
			map.put("loopNum", "");
			map.put("sumTotal", "");
			map.put("pcrRatio", "");
			map.put("expectNum", "");
			map.put("sampleNum", "");
			map.put("tempId", "");
			map.put("sampleInfo-idCard", "");
			map.put("sampleInfo-orderNum", "");
			
			map.put("chpsConcentration", "");
			map.put("chVolume", "");
			map.put("chSumTotal", "");
			map.put("indexConcentration", "");
			map.put("dxpdConcentration", "");
			map.put("dxpdVolume", "");
			map.put("dxpdSumTotal", "");
			map.put("wkConcentration", "");
			map.put("wkVolume", "");
			map.put("wkSumTotal", "");
			map.put("labCode", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("isZkp", "");

			// 文库类型
			map.put("techJkServiceTask-libraryType", "");
			// 插入片段
			map.put("techJkServiceTask-insertSize", "");
			// 测序类型
			map.put("techJkServiceTask-sequenceType", "");

			map.put("libType", "");
			map.put("insertSize", "");
			map.put("libNum", "");
			map.put("sampleNowNum", "");
			map.put("takeNum", "");
			map.put("breakNum", "");
			map.put("teOrH20num", "");
			map.put("dataNowNum", "");
			map.put("storageLocation", "");
			map.put("seqType", "");
			map.put("primer", "");
			map.put("isInit", "");
			map.put("species", "");
			map.put("isRna", "");
			map.put("sampleVolume", "");
			map.put("inwardCode", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");

			map.put("unitGroup-id", "");
			map.put("unitGroup-name", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// ctDNA文库构建结果
	@Action(value = "showWKResultCtDNAList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWKResultCtDNAList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/wkLife/wKLifeSampleInfoCtDNA.jsp");
	}

	@Action(value = "showWKResultCtDNAListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWKResultCtDNAListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wkLifeService.findWKResultCtDNAList(scId,
					startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<SampleWkLifeInfoCtDNA> list = (List<SampleWkLifeInfoCtDNA>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("indexa", "");
			map.put("sampleCode", "");
			map.put("volume", "");
			map.put("unit", "");
			map.put("result", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("reason", "");
			map.put("submit", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("note", "");
			map.put("state", "");
			map.put("method", "");
			map.put("wkTask-name", "");
			map.put("wkTask-id", "");
			map.put("sampleType", "");
			map.put("i5", "");
			map.put("i7", "");
			map.put("concentration", "");
			map.put("loopNum", "");
			map.put("sumTotal", "");
			map.put("pcrRatio", "");
			map.put("expectNum", "");
			map.put("sampleNum", "");
			map.put("tempId", "");

			map.put("totalNum", "");
			map.put("dpdConcentration", "");
			map.put("dpdVolume", "");
			map.put("dpdSumTotal", "");
			map.put("dpdHsl", "");
			map.put("xpdConcentration", "");
			map.put("xpdVolume", "");
			map.put("xpdSumTotal", "");
			map.put("xpdHsl", "");
			map.put("indexConcentration", "");
			map.put("wkConcentration", "");
			map.put("wkVolume", "");
			map.put("wkSumTotal", "");
			map.put("labCode", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// rRNA去除文库构建结果
	@Action(value = "showWKResultrRNAList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWKResultrRNAList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/wkLife/wKLifeSampleInforRNA.jsp");
	}

	@Action(value = "showWKResultrRNAListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWKResultrRNAListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wkLifeService.findWKResultrRNAList(scId,
					startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<SampleWkLifeInforRNA> list = (List<SampleWkLifeInforRNA>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("indexa", "");
			map.put("sampleCode", "");
			map.put("volume", "");
			map.put("unit", "");
			map.put("result", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("reason", "");
			map.put("submit", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("note", "");
			map.put("state", "");
			map.put("method", "");
			map.put("wkTask-name", "");
			map.put("wkTask-id", "");
			map.put("sampleType", "");
			map.put("i5", "");
			map.put("i7", "");
			map.put("concentration", "");
			map.put("loopNum", "");
			map.put("sumTotal", "");
			map.put("pcrRatio", "");
			map.put("expectNum", "");
			map.put("sampleNum", "");
			map.put("tempId", "");

			map.put("rin", "");
			map.put("nsConcentration", "");
			map.put("nsVolume", "");
			map.put("nsSumTotal", "");
			map.put("qcConcentration", "");
			map.put("qcVolume", "");
			map.put("qcSumTotal", "");
			map.put("qcYield", "");
			map.put("temperature", "");
			map.put("time", "");
			map.put("wkConcentration", "");
			map.put("wkVolume", "");
			map.put("wkSumTotal", "");
			map.put("labCode", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// mRNA去除文库构建结果
	@Action(value = "showWKResultmRNAList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWKResultmRNAList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/wkLife/wKLifeSampleInfomRNA.jsp");
	}

	@Action(value = "showWKResultmRNAListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWKResultmRNAListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wkLifeService.findWKResultmRNAList(scId,
					startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<SampleWkLifeInfomRNA> list = (List<SampleWkLifeInfomRNA>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("indexa", "");
			map.put("sampleCode", "");
			map.put("volume", "");
			map.put("unit", "");
			map.put("result", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("reason", "");
			map.put("submit", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("note", "");
			map.put("state", "");
			map.put("method", "");
			map.put("wkTask-name", "");
			map.put("wkTask-id", "");
			map.put("sampleType", "");
			map.put("i5", "");
			map.put("i7", "");
			map.put("concentration", "");
			map.put("loopNum", "");
			map.put("sumTotal", "");
			map.put("pcrRatio", "");
			map.put("expectNum", "");
			map.put("sampleNum", "");
			map.put("tempId", "");

			map.put("rin", "");
			map.put("nsConcentration", "");
			map.put("nsVolume", "");
			map.put("nsSumTotal", "");
			map.put("qcConcentration", "");
			map.put("qcVolume", "");
			map.put("qcSumTotal", "");
			map.put("qcYield", "");
			map.put("temperature", "");
			map.put("time", "");
			map.put("wkConcentration", "");
			map.put("wkVolume", "");
			map.put("wkSumTotal", "");
			map.put("labCode", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWKResult")
	public void delWKResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wkLifeService.delWKResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWKResultc")
	public void delWKResultc() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wkLifeService.delWKResultc(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWKResultr")
	public void delWKResultr() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wkLifeService.delWKResultr(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWKResultm")
	public void delWKResultm() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wkLifeService.delWKResultm(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	@Action(value = "showWKTempSplitList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWKTempSplitList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/wkLife/wKLifeTempSplit.jsp");
	}

	// 左侧表查询
	@Action(value = "showWKTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWKReceiveItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/wkLife/wKLifeTemp.jsp");
	}

	@Action(value = "showWKTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWKTempListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");

		// 当前用所在人员组Id
		String groups = (String) this
				.getObjFromSession(SystemConstants.USER_SESSION_GROUPS);
		User user = (User) this
				.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		try {
			// String scId = getRequest().getParameter("id");
			Map<String, Object> result = wKReceiveService.findWKTempList(
					map2Query, startNum, limitNum, dir, sort, groups,
					user.getId());
			Long total = (Long) result.get("total");
			List<WkLifeTaskTemp> list = (List<WkLifeTaskTemp>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("sampleCode", "");
			map.put("splitCode", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("volume", "");
			map.put("unit", "");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("note", "");
			map.put("classify", "");
			map.put("sampleType", "");
			map.put("sampleNum", "");
			map.put("labCode", "");
			map.put("RIN", "");
			map.put("concentration", "");
			map.put("qbcontraction", "");
			map.put("sumVolume", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");
			map.put("techJkServiceTask-sequenceBillDate", "yyyy-MM-dd");
			map.put("techJkServiceTask-sequencePlatform", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("counts", "");
			map.put("isZkp", "");
			map.put("tjItem-id", "");
			map.put("tjItem-inwardCode", "");
			map.put("tjItem-wgcId", "");
			map.put("tjItem-species", "");
			map.put("tjItem-dataNum", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 判断样本被使用的次数
	@Action(value = "selectCodeCount")
	public void selectCodeCount() throws Exception {
		String code = getParameterFromRequest("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			Long num = wKLifeSampleTaskDao.selectCount(code);
			int num1 = Integer.valueOf(String.valueOf(num));
			result.put("success", true);
			result.put("data", num1 + 1);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 模板
	@Action(value = "showTemplateWaitList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTemplateWaitList() throws Exception {
		String scId = getRequest().getParameter("id");
		putObjToContext("id", scId);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/wk/showTemplateWaitList.jsp");
	}

	@Action(value = "showTemplateWaitListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTemplateWaitListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wkLifeService.findWKTemplateList(scId,
					startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<WkLifeTaskTemplate> list = (List<WkLifeTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("note", "");
			map.put("tItem", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("reciveUser-id", "");
			map.put("reciveUser-name", "");
			map.put("sampleCodes", "");
			map.put("state", "");
			map.put("wk-name", "");
			map.put("wk-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 保存质控品到文库构建临时表
	@Action(value = "saveQuality")
	public void saveQuality() throws Exception {
		// String id=getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getRequest().getParameter("itemDataJson");
			wkLifeService.saveQuality(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/*
	 * //获得index的最大值
	 * 
	 * @Action(value = "getIndexNum") public void getIndexNum() throws Exception
	 * { // String code = getParameterFromRequest("code"); Map<String, Object>
	 * result = new HashMap<String, Object>(); try { DicType
	 * num=wkDao.getIndexNum(); //int num1=Integer.valueOf(String.valueOf(num));
	 * result.put("success", true); result.put("data", num); } catch (Exception
	 * e) { result.put("success", false); }
	 * HttpUtils.write(JsonUtils.toJsonString(result)); }
	 */

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public WKLifeSampleTaskService getwkLifeService() {
		return wkLifeService;
	}

	public void setwkLifeService(WKLifeSampleTaskService wkLifeService) {
		this.wkLifeService = wkLifeService;
	}

	public WkLifeTask getWk() {
		return wk;
	}

	public void setWk(WkLifeTask wk) {
		this.wk = wk;
	}

	// 根据选中的步骤异步加载相关的试剂数据
	@Action(value = "setReagent")
	public void setReagent() throws Exception {
		String code = getRequest().getParameter("code");
		String id = getRequest().getParameter("tid");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.wkLifeService.setReagent(
					id, code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 根据选中的步骤异步加载相关的设备数据
	@Action(value = "setCos")
	public void setCos() throws Exception {
		String code = getRequest().getParameter("code");
		String id = getRequest().getParameter("tid");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.wkLifeService.setCos(id,
					code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 科技服务-建库任务单
	@Action(value = "showTechJkTaskList")
	public String showTechJkTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/wkLife/techServiceJkTask.jsp");
	}

	@Action(value = "showTechJkTaskListJson")
	public void showTechJkTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = this.wkLifeService.findTechJkServiceTaskList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");

		List<TechJkServiceTask> list = (List<TechJkServiceTask>) result
				.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("projectId-id", "");
		map.put("projectId-name", "");
		map.put("contractId-id", "");
		map.put("contractId-name", "");
		map.put("orderType-id", "");
		map.put("orderType-name", "");
		map.put("productId-id", "");
		map.put("productId-name", "");
		map.put("dataType", "");
		map.put("sequenceBillDate", "yyyy-MM-dd");
		map.put("sequenceBillName", "");
		map.put("sequenceType", "");
		map.put("sequenceLength", "");
		map.put("sequencePlatform", "");
		map.put("sampeNum", "");
		map.put("species", "");
		map.put("experimentPeriod", "");
		map.put("experimentEndTime", "yyyy-MM-dd");
		map.put("experimentRemarks", "");
		map.put("analysisCycle", "");
		map.put("analysisEndTime", "yyyy-MM-dd");
		map.put("anaysisRemarks", "");
		map.put("projectDifference", "");
		map.put("mixRatio", "");
		map.put("coefficient", "");
		map.put("readsOne", "");
		map.put("readsTwo", "");
		map.put("index1", "");
		map.put("index2", "");
		map.put("density", "");
		map.put("orders-id", "");
		map.put("orders-name", "");
		map.put("projectLeader-id", "");
		map.put("projectLeader-name", "");
		map.put("experimentLeader-id", "");
		map.put("experimentLeader-name", "");
		map.put("analysisLeader-id", "");
		map.put("analysisLeader-name", "");
		map.put("techProjectTask-id", "");
		map.put("techProjectTask-name", "");
		map.put("note", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	// 科技服务明细
	// @Action(value = "showTechJkServiceTaskItemList", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public String showTechJkServiceTaskItemList() throws Exception {
	// return "";
	// }
	//
	@Action(value = "showTechJkServiceTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTechJkServiceTaskItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = this.wkLifeService
					.findTechServiceTaskItemList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<TechJkServiceTaskItem> list = (List<TechJkServiceTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("sampleCode", "");
			map.put("state", "");
			map.put("stateName", "");

			map.put("techJkServiceTask-projectId", "");
			map.put("techJkServiceTask-contractId", "");
			map.put("techJkServiceTask-orderType", "");
			map.put("experimentUser", "");
			map.put("endDate", "yyyy-MM-dd");
			map.put("species", "");
			map.put("classify", "");

			map.put("techJkServiceTask-id", "");
			map.put("techJkServiceTask-name", "");

			map.put("dataType", "");
			map.put("dataNum", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 富集查看文库信息
	 * @throws Exception
	 */
	@Action(value = "showWKSplitInfoList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWKSplitInfoList() throws Exception {
		String id = getParameterFromRequest("id");
		String codes = getParameterFromRequest("codes");
		putObjToContext("id", id);
		putObjToContext("codes", codes);
		return dispatcher("/WEB-INF/page/experiment/wkLife/wKLifePoolingSampleInfo.jsp");
	}

	@Action(value = "showWKSplitInfoListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWKSplitInfoListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String id = getParameterFromRequest("id");
		String codes = getParameterFromRequest("codes");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = wkLifeService.showWKSplitInfoList(map2Query, startNum, limitNum, dir, sort, id, codes);
		Long total = (Long) result.get("total");
		List<SampleWkLifeInfo> list = (List<SampleWkLifeInfo>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("code", "");
		map.put("wkCode", "");
		map.put("indexa", "");
		map.put("sampleCode", "");
		map.put("splitCode", "");
		map.put("volume", "");
		map.put("unit", "");
		map.put("result", "");
		map.put("nextFlowId", "");
		map.put("nextFlow", "");
		map.put("reason", "");
		map.put("submit", "");
		map.put("patientName", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("inspectDate", "");
		map.put("acceptDate", "yyyy-MM-dd");
		map.put("idCard", "");
		map.put("phone", "");
		map.put("orderId", "");
		map.put("sequenceFun", "");
		map.put("reportDate", "yyyy-MM-dd");
		map.put("note", "");
		map.put("state", "");
		map.put("method", "");
		map.put("isExecute", "");
		map.put("wk-name", "");
		map.put("wk-id", "");

		map.put("rowCode", "");
		map.put("colCode", "");
		map.put("counts", "");
		map.put("contractId", "");
		map.put("projectId", "");
		map.put("orderType", "");
		map.put("jkTaskId", "");
		map.put("classify", "");

		map.put("dataType", "");
		map.put("dataNum", "");
		map.put("sampleType", "");
		map.put("i5", "");
		map.put("i7", "");
		map.put("concentration", "");
		map.put("loopNum", "");
		map.put("sumTotal", "");
		map.put("pcrRatio", "");
		map.put("expectNum", "");
		map.put("sampleNum", "");
		map.put("tempId", "");
		map.put("sampleInfo-idCard", "");
		map.put("sampleInfo-orderNum", "");
		
		map.put("chpsConcentration", "");
		map.put("chVolume", "");
		map.put("chSumTotal", "");
		map.put("indexConcentration", "");
		map.put("dxpdConcentration", "");
		map.put("dxpdVolume", "");
		map.put("dxpdSumTotal", "");
		map.put("wkConcentration", "");
		map.put("wkVolume", "");
		map.put("wkSumTotal", "");
		map.put("labCode", "");
		map.put("sampleInfo-id", "");
		map.put("sampleInfo-note", "");
		map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
		map.put("sampleInfo-reportDate", "");
		map.put("techJkServiceTask-id", "");
		map.put("techJkServiceTask-name", "");
		map.put("isZkp", "");

		// 文库类型
		map.put("techJkServiceTask-libraryType", "");
		// 插入片段
		map.put("techJkServiceTask-insertSize", "");
		// 测序类型
		map.put("techJkServiceTask-sequenceType", "");

		map.put("libType", "");
		map.put("insertSize", "");
		map.put("libNum", "");
		map.put("sampleNowNum", "");
		map.put("takeNum", "");
		map.put("breakNum", "");
		map.put("teOrH20num", "");
		map.put("dataNowNum", "");
		map.put("storageLocation", "");
		map.put("seqType", "");
		map.put("primer", "");
		map.put("isInit", "");
		map.put("species", "");
		map.put("isRna", "");
		map.put("sampleVolume", "");
		map.put("inwardCode", "");
		map.put("tjItem-id", "");
		map.put("tjItem-inwardCode", "");

		map.put("unitGroup-id", "");
		map.put("unitGroup-name", "");
		map.put("dicSampleType-id", "");
		map.put("dicSampleType-name", "");
		new SendData().sendDateJson(map, list, total,
				ServletActionContext.getResponse());
	}
	
	@Action(value = "showSampleByTechJkTaskId", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleByTechJkTaskId() throws Exception {
		String taskId = getParameterFromRequest("taskId");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, Object>> listMap = this.wkLifeService
					.selectTechTaskItemByTaskId(taskId);
			result.put("success", true);
			result.put("data", listMap);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 提交样本
	@Action(value = "submitSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void submitSample() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.wkLifeService.submitSample(id, ids);
			result.put("success", true);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 提交样本
	@Action(value = "submitSampleCtDNA", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void submitSampleCtDNA() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.wkLifeService.submitSampleCtDNA(id, ids);
			result.put("success", true);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 提交样本
	@Action(value = "submitSamplemRNA", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void submitSamplemRNA() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.wkLifeService.submitSamplemRNA(id, ids);
			result.put("success", true);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 提交样本
	@Action(value = "submitSamplerRNA", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void submitSamplerRNA() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.wkLifeService.submitSamplerRNA(id, ids);
			result.put("success", true);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 查询科研样本
	@Action(value = "selTechJkServicTaskByid", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selTechJkServicTaskByid() throws Exception {
		String id = getParameterFromRequest("id");
		String groups = (String) this
				.getObjFromSession(SystemConstants.USER_SESSION_GROUPS);
		User user = (User) this
				.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		Map<String, String> map = new HashMap<String, String>();
		map.put("techJkServiceTask.id", id);
		Map<String, Object> map2 = wKReceiveService.findWKTempList(map, null,
				null, null, null, groups, user.getId());
		List<WkLifeTaskTemp> list = (List<WkLifeTaskTemp>) map2.get("list");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			result.put("success", true);
			result.put("list", list);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 明细样本入库
	@Action(value = "rukuItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void rukuItem() throws Exception {
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.wkLifeService.rukuItem(ids);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 生成index
	@Action(value = "index", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void index() throws Exception {
//		String id = getParameterFromRequest("id");
		// String indexId = getParameterFromRequest("indexId");
		// String kitId = getParameterFromRequest("kitId");// 盒子id
		String type = getRequest().getParameter("type");
		String id = getRequest().getParameter("id");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
//			this.wkLifeService.index(id);
//			result.put("success", true);
			List<Map<String, Object>> dataListMap = this.storageService
					.selectindex(type, id);
			result.put("success", true);
			result.put("data", dataListMap);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	/**
	 * 按照检测项目拆分样本
	 * @throws Exception
	 */
	@Action(value = "wkTempSplit",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void wkTempSplit() throws Exception {
		String id = getRequest().getParameter("id");					//勾选的样本的ID
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.wkLifeService.wkTempSplit(id);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
