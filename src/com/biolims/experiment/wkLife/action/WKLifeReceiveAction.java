﻿package com.biolims.experiment.wkLife.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;

import com.biolims.experiment.wkLife.model.WkLifeReceive;
import com.biolims.experiment.wkLife.model.WkLifeReceiveItem;
import com.biolims.experiment.wkLife.model.WkLifeReceiveTemp;
import com.biolims.experiment.wkLife.service.WKLifeReceiveService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/wkLife/wKReceive")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class WKLifeReceiveAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "240305";
	@Autowired
	private WKLifeReceiveService wKLifeReceiveService;
	private WkLifeReceive wkReceive = new WkLifeReceive();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;

	@Action(value = "showWKReceiveList")
	public String showWKReceiveList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/wkLife/wKLifeReceive.jsp");
	}

	@Action(value = "showWKReceiveListJson")
	public void showWKReceiveListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = wKLifeReceiveService.findWKReceiveList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<WkLifeReceive> list = (List<WkLifeReceive>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("describe", "");
		map.put("receiver-id", "");
		map.put("receiver-name", "");
		map.put("receiverDate", "yyyy-MM-dd ");
		map.put("storagePrompt-id", "");
		map.put("storagePrompt-name", "");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "wKReceiveSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogWKReceiveList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/wkLife/wKLifeReceiveDialog.jsp");
	}

	@Action(value = "showDialogWKReceiveListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogWKReceiveListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = wKLifeReceiveService.findWKReceiveList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<WkLifeReceive> list = (List<WkLifeReceive>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("describe", "");
		map.put("receiver-id", "");
		map.put("receiver-name", "");
		map.put("receiverDate", "yyyy-MM-dd");
		map.put("storagePrompt-id", "");
		map.put("storagePrompt-name", "");
		map.put("workState", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editWKReceive")
	public String editWKReceive() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			wkReceive = wKLifeReceiveService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "wKReceive");
		} else {
			wkReceive.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			wkReceive.setReceiver(user);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stime = format.format(date);
			wkReceive.setReceiverDate(stime);
			wkReceive.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			wkReceive.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(wkReceive.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/wkLife/wKLifeReceiveEdit.jsp");
	}

	@Action(value = "copyWKReceive")
	public String copyWKReceive() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		wkReceive = wKLifeReceiveService.get(id);
		wkReceive.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/wkLife/wKLifeReceiveEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = wkReceive.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "WkLifeReceive";
			String markCode = "WKJJ";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			wkReceive.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("wKReceiveItem", getParameterFromRequest("wKReceiveItemJson"));

		wKLifeReceiveService.save(wkReceive, aMap);
		return redirect("/experiment/wkLife/wKReceive/editWKReceive.action?id="
				+ wkReceive.getId());

	}

	@Action(value = "viewWKReceive")
	public String toViewWKReceive() throws Exception {
		String id = getParameterFromRequest("id");
		wkReceive = wKLifeReceiveService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/wkLife/wKLifeReceiveEdit.jsp");
	}

	@Action(value = "showWKReceiveItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWKReceiveItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/wkLife/wKLifeReceiveItem.jsp");
	}

	@Action(value = "showWKReceiveItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWKReceiveItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = wKLifeReceiveService
					.findWKReceiveItemList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<WkLifeReceiveItem> list = (List<WkLifeReceiveItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("name", "");
			map.put("method", "");
			map.put("reason", "");
			map.put("note", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("storageLocal-name", "");
			map.put("storageLocal-id", "");
			map.put("state", "");
			map.put("wkReceive-name", "");
			map.put("wkReceive-id", "");
			map.put("classify", "");
			map.put("sampleType", "");
			map.put("sampleNum", "");
			map.put("tempId", "");
			map.put("labCode", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWKReceiveItem")
	public void delWKReceiveItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wKLifeReceiveService.delWKReceiveItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showWkReceiveTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWkReceiveTempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/wkLife/wkLifeReceiveTemp.jsp");
	}

	@Action(value = "showWkReceiveTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWkReceiveTempListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			// String scId = getRequest().getParameter("id");
			Map<String, Object> result = wKLifeReceiveService.findWKAcceptTempList(
					startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<WkLifeReceiveTemp> list = (List<WkLifeReceiveTemp>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("inspectDate", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("volume", "");
			map.put("unit", "");
			map.put("idCard", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("note", "");
			map.put("classify", "");
			map.put("sampleType", "");
			map.put("sampleNum", "");
			map.put("labCode", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-note", "");
			map.put("sampleInfo-receiveDate", "yyyy-MM-dd");
			map.put("sampleInfo-reportDate", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public WKLifeReceiveService getwKLifeReceiveService() {
		return wKLifeReceiveService;
	}

	public void setwKLifeReceiveService(WKLifeReceiveService wKLifeReceiveService) {
		this.wKLifeReceiveService = wKLifeReceiveService;
	}

	public WkLifeReceive getWkReceive() {
		return wkReceive;
	}

	public void setWkReceive(WkLifeReceive wkReceive) {
		this.wkReceive = wkReceive;
	}

}
