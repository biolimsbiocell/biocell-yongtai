
package com.biolims.experiment.wkLife.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;

import com.biolims.experiment.wkLife.model.WkLifeAbnormalBack;
import com.biolims.experiment.wkLife.service.WKLifeRepeatService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/experiment/wkLife/repeat")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class WKLifeRepeatAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "240301";
	@Autowired
	@Resource
	private WKLifeRepeatService wKLifeReceiveService;
	/**
	 * 重建库
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showWKRepeatList")
	public String showWKRepeatList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/wkLife/wkLifeRepeat.jsp");
	}
	@Action(value = "showWKRepeatListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWKRepeatListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		try {
			Map<String, Object> result = wKLifeReceiveService.findSampleWkInfo(map2Query, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<WkLifeAbnormalBack> list = (List<WkLifeAbnormalBack>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("wkCode", "");
			map.put("indexa", "");
			map.put("sampleCode", "");
			map.put("volume", "");
			//map.put("unit", "");
			map.put("result", "");
			map.put("nextFlow", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("note", "");
			map.put("state", "");
			map.put("method", "");
			map.put("isExecute", "");
			
			map.put("techTaskId", "");
			map.put("contractId", "");
			map.put("projectId", "");
			map.put("orderType", "");
			map.put("classify", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "saveWkRepeat", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveWkRepeat() throws Exception {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String dataJson = getRequest().getParameter("itemDataJson");
			wKLifeReceiveService.saveWkRepeat(dataJson);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public WKLifeRepeatService getwKLifeReceiveService() {
		return wKLifeReceiveService;
	}

	public void setwKLifeReceiveService(WKLifeRepeatService wKLifeReceiveService) {
		this.wKLifeReceiveService = wKLifeReceiveService;
	}


}
