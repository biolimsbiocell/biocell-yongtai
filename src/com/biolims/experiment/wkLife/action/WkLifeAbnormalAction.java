﻿package com.biolims.experiment.wkLife.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;

import com.biolims.experiment.wkLife.model.WkLifeAbnormalBack;
import com.biolims.experiment.wkLife.model.WkLifeTaskItem;
import com.biolims.experiment.wkLife.service.WkLifeAbnormalService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/wkLife/wkAbnormal")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class WkLifeAbnormalAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "249803";
	@Autowired
	private WkLifeAbnormalService wkLifeAbnormalService;
	private WkLifeAbnormalBack wkAbnormal = new WkLifeAbnormalBack();
	@Resource
	private FileInfoService fileInfoService;

	@Action(value = "showWkAbnormalMain")
	public String showWkAbnormalMain() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/wkLife/wkLifeAbnormalMain.jsp");
	}

	@Action(value = "showWkAbnormalBackList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWkAbnormalBackList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/wkLife/wkLifeAbnormalBack.jsp");
	}

	/*
	 * @Action(value = "showWkAbnormalBackListJson", interceptorRefs =
	 * 
	 * @InterceptorRef("biolimsDefaultStack")) public void
	 * showWkAbnormalBackListJson() throws Exception { int startNum =
	 * Integer.parseInt(getParameterFromRequest("start")); int limitNum =
	 * Integer.parseInt(getParameterFromRequest("limit")); String dir =
	 * getParameterFromRequest("dir"); String sort =
	 * getParameterFromRequest("sort"); String data =
	 * getParameterFromRequest("data"); Map<String, String> map2Query = new
	 * HashMap<String, String>(); //map2Query.put("code", "%%");
	 * //map2Query.put("sampleCode", "%%"); //map2Query.put("wkCode", "%%");
	 * //map2Query.put("indexa", "%%"); //map2Query.put("isExecute",
	 * "is##@@##null"); if (data != null && data.length() > 0) map2Query =
	 * JsonUtils.toObjectByJson(data, Map.class); try { Map<String, Object>
	 * result = wkLifeAbnormalService.findWkAbnormalBack(map2Query, startNum,
	 * limitNum, dir, sort); Long total = (Long) result.get("total");
	 * List<SampleWkInfo> list = (List<SampleWkInfo>) result.get("list");
	 * Map<String, String> map = new HashMap<String, String>(); map.put("id",
	 * ""); map.put("name", ""); map.put("code", ""); map.put("wkCode", "");
	 * map.put("indexa", ""); map.put("sampleCode", ""); map.put("volume", "");
	 * map.put("unit", ""); map.put("result", ""); map.put("nextFlow", "");
	 * map.put("reason", ""); map.put("submit", ""); map.put("patientName", "");
	 * map.put("productId", ""); map.put("productName", "");
	 * map.put("inspectDate", ""); map.put("acceptDate", "yyyy-MM-dd");
	 * map.put("idCard", ""); map.put("phone", ""); map.put("orderId", "");
	 * map.put("sequenceFun", ""); map.put("reportDate", "yyyy-MM-dd");
	 * map.put("note", ""); map.put("state", ""); map.put("method", "");
	 * map.put("isExecute", ""); map.put("wk-name", ""); map.put("wk-id", "");
	 * new SendData().sendDateJson(map, list, total,
	 * ServletActionContext.getResponse()); } catch (Exception e) {
	 * e.printStackTrace(); } }
	 */

	@Action(value = "showQualityProductList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQualityProductList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/wkLife/qualityProductAbnormal.jsp");
	}

	@Action(value = "showQualityProductListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQualityProductListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		try {
			Map<String, Object> result = wkLifeAbnormalService.findQualityProduct(
					map2Query, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<WkLifeTaskItem> list = (List<WkLifeTaskItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("orderNumber", "");
			map.put("name", "");
			map.put("wkId", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("indexa", "");
			map.put("concentration", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("stepNum", "");
			map.put("note", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "");
			map.put("acceptDate", "");
			map.put("idCard", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("wk-name", "");
			map.put("wk-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWkAbnormalBack")
	public void delWkAbnormalBack() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			wkLifeAbnormalService.delWkAbnormalBack(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public WkLifeAbnormalService getwkLifeAbnormalService() {
		return wkLifeAbnormalService;
	}

	public void setwkLifeAbnormalService(WkLifeAbnormalService wkLifeAbnormalService) {
		this.wkLifeAbnormalService = wkLifeAbnormalService;
	}

	public WkLifeAbnormalBack getWkAbnormalBack() {
		return wkAbnormal;
	}

	public void setWkAbnormalBack(WkLifeAbnormalBack wkAbnormal) {
		this.wkAbnormal = wkAbnormal;
	}

	// 保存文库异常信息
	@Action(value = "saveWkAbnormalBack")
	public void saveWkAbnormalBack() throws Exception {
		// String id=getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getRequest().getParameter("itemDataJson");
			wkLifeAbnormalService.saveWkAbnormalBack(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 根据条件检索异常样本
	// @Action(value = "selectAbnormal")
	// public void selectAbnormal() throws Exception {
	// String code1 = getParameterFromRequest("code");
	// String code2 = getParameterFromRequest("sampleCode");
	// String code3 = getParameterFromRequest("wkCode");
	// String code4 = getParameterFromRequest("indexs");
	// Map<String, Object> result = new HashMap<String, Object>();
	// try {
	// List<Map<String, String>> dataListMap =
	// this.wkLifeAbnormalService.selectAbnormal(code1, code2,code3,code4);
	// result.put("success", true);
	// result.put("data", dataListMap);
	// } catch (Exception e) {
	// result.put("success", false);
	// }
	// HttpUtils.write(JsonUtils.toJsonString(result));
	// }

	// 异常
	@Action(value = "showWkFeedbackListJson")
	public void showWkFeedbackListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = new HashMap<String, Object>();
		result = wkLifeAbnormalService.selectWkAbnormalList(map2Query, startNum,
				limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<WkLifeAbnormalBack> list = (List<WkLifeAbnormalBack>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("sampleCode", "");
		map.put("wkCode", "");
		map.put("code", "");
		map.put("indexa", "");
		map.put("volume", "");
		map.put("concentration", "");
		map.put("result", "");
		map.put("nextFlowId", "");
		map.put("nextFlow", "");
		map.put("method", "");
		map.put("isExecute", "");
		map.put("backTime", "");
		map.put("note", "");
		map.put("state", "");
		map.put("patientName", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("inspectDate", "");
		map.put("acceptDate", "");
		map.put("idCard", "");
		map.put("phone", "");
		map.put("orderId", "");
		map.put("sequenceFun", "");
		map.put("reportDate", "");
		map.put("techTaskId", "");
		map.put("contractId", "");
		map.put("projectId", "");
		map.put("orderType", "");
		map.put("classify", "");
		map.put("sampleType", "");
		map.put("sampleNum", "");
		map.put("tempId", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}
}
