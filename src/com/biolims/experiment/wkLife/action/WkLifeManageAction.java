﻿package com.biolims.experiment.wkLife.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.wkLife.model.SampleWkLifeInfo;
import com.biolims.experiment.wkLife.model.WkLifeManage;
import com.biolims.experiment.wkLife.model.WkLifeTaskItem;
import com.biolims.experiment.wkLife.service.WkLifeManageService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/wkLife/wkManage")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class WkLifeManageAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "249804";
	@Autowired
	private WkLifeManageService wkLifeManageService;
	private WkLifeManage wkManage = new WkLifeManage();
	@Resource
	private FileInfoService fileInfoService;

	@Action(value = "showWkManageEditList")
	public String showWkManageEditList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/wkLife/wKLifeItemManageEdit.jsp");
	}

	@Action(value = "showWkManageList")
	public String showWkManageList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/wkLife/wkLifeManage.jsp");
	}

	@Action(value = "showWkManageListJson")
	public void showWkManageListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = wkLifeManageService.findWkManageList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SampleWkLifeInfo> list = (List<SampleWkLifeInfo>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("code", "");
		map.put("wkCode", "");
		map.put("indexa", "");
		map.put("sampleCode", "");
		map.put("volume", "");
		map.put("unit", "");
		map.put("result", "");
		map.put("nextFlowId", "");
		map.put("nextFlow", "");
		map.put("reason", "");
		map.put("submit", "");
		map.put("patientName", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("inspectDate", "");
		map.put("acceptDate", "yyyy-MM-dd");
		map.put("idCard", "");
		map.put("phone", "");
		map.put("orderId", "");
		map.put("sequenceFun", "");
		map.put("reportDate", "yyyy-MM-dd");
		map.put("note", "");
		map.put("state", "");
		map.put("method", "");
		map.put("isExecute", "");
		map.put("wk-name", "");
		map.put("wk-id", "");

		map.put("rowCode", "");
		map.put("colCode", "");
		map.put("counts", "");
		map.put("contractId", "");
		map.put("projectId", "");
		map.put("orderType", "");
		map.put("jkTaskId", "");
		map.put("classify", "");
		map.put("labCode", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "showWkItemManageList")
	public String showWkItemManageList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/wkLife/wKLifeItemManage.jsp");
	}

	@Action(value = "showWkItemManageListJson")
	public void showWkItemManageListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = wkLifeManageService.findWkItemManageList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<WkLifeTaskItem> list = (List<WkLifeTaskItem>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("orderNumber", "");
		map.put("name", "");
		map.put("wkId", "");
		map.put("sampleNum", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("indexa", "");
		map.put("concentration", "");
		map.put("result", "");
		map.put("reason", "");
		map.put("stepNum", "");
		map.put("note", "");
		map.put("phone", "");
		map.put("orderId", "");
		map.put("patientName", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("inspectDate", "");
		map.put("acceptDate", "yyyy-MM-dd");
		map.put("idCard", "");
		map.put("sequenceFun", "");
		map.put("reportDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("wk-name", "");
		map.put("wk-id", "");
		map.put("rowCode", "");
		map.put("colCode", "");
		map.put("counts", "");
		map.put("volume", "");
		map.put("unit", "");
		map.put("nextFlow", "");
		map.put("addVolume", "");
		map.put("sumVolume", "");
		map.put("contractId", "");
		map.put("projectId", "");
		map.put("orderType", "");
		map.put("jkTaskId", "");
		map.put("classify", "");
		map.put("labCode", "");

		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "wkManageSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogWkManageList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/wkLife/wkLifeManageDialog.jsp");
	}

	@Action(value = "showDialogWkManageListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogWkManageListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = wkLifeManageService.findWkManageList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<WkLifeManage> list = (List<WkLifeManage>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("indexb", "");
		map.put("bulk", "");
		map.put("chroma", "");
		map.put("resultDecide", "");
		map.put("nextFlow", "");
		map.put("note", "");
		map.put("code", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editWkManage")
	public String editWkManage() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			wkManage = wkLifeManageService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "wkManage");
		} else {
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			// wkManage.setCreateUser(user);
			// wkManage.setCreateDate(new Date());
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/wkLife/wkLifeManageEdit.jsp");
	}

	@Action(value = "copyWkManage")
	public String copyWkManage() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		wkManage = wkLifeManageService.get(id);
		wkManage.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/wkLife/wkLifeManageEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = wkManage.getId();
		if (id != null && id.equals("")) {
			wkManage.setId(null);
		}
		Map aMap = new HashMap();
		wkLifeManageService.save(wkManage, aMap);
		return redirect("/experiment/wkLife/wkManage/editWkManage.action?id="
				+ wkManage.getId());

	}

	@Action(value = "viewWkManage")
	public String toViewWkManage() throws Exception {
		String id = getParameterFromRequest("id");
		wkManage = wkLifeManageService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/wkLife/wkLifeManageEdit.jsp");
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public WkLifeManageService getwkLifeManageService() {
		return wkLifeManageService;
	}

	public void setwkLifeManageService(WkLifeManageService wkLifeManageService) {
		this.wkLifeManageService = wkLifeManageService;
	}

	public WkLifeManage getWkManage() {
		return wkManage;
	}

	public void setWkManage(WkLifeManage wkManage) {
		this.wkManage = wkManage;
	}

	// 保存文库管理信息
	@Action(value = "saveWkManage")
	public void saveWkManage() throws Exception {
		// String id=getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getRequest().getParameter("itemDataJson");
			wkLifeManageService.saveWkManage(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 保存文库构建明细信息
	@Action(value = "saveWkItemManage")
	public void saveWkItemManage() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getRequest().getParameter("itemDataJson");
			wkLifeManageService.saveWkItemManage(itemDataJson);

			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 样本管理明细入库
	@Action(value = "wkManageItemRuku")
	public void wkManageItemRuku() throws Exception {
		String ids = getRequest().getParameter("ids");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			wkLifeManageService.wkManageItemRuku(ids);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 样本管理明细建库待办
	@Action(value = "wkManageItemJK")
	public void wkManageItemJK() throws Exception {
		String id = getRequest().getParameter("id");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			wkLifeManageService.wkManageItemJK(id);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
