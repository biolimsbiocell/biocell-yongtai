package com.biolims.experiment.wkLife.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.wkLife.service.WKLifeReceiveService;

public class WKLifeReceiveEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		WKLifeReceiveService prService = (WKLifeReceiveService) ctx.getBean("WKLifeReceiveService");
		prService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
