package com.biolims.experiment.wkLife.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.wkLife.service.WKLifeSampleTaskService;

public class WKLifeTastModifyEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		WKLifeSampleTaskService wkService = (WKLifeSampleTaskService) ctx.getBean("WKLifeSampleTaskService");
		wkService.changeState1(applicationTypeActionId, contentId);

		return "";
	}
}
