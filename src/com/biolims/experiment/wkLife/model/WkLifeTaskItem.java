package com.biolims.experiment.wkLife.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleInfo;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;

/**
 * @Title: Model
 * @Description: 文库明细
 * @author lims-platform
 * @date 2015-11-29 19:25:53
 * @version V1.0
 * 
 */
@Entity
@Table(name = "WK_LIFE_TASK_ITEM")
@SuppressWarnings("serial")
public class WkLifeTaskItem extends EntityDao<WkLifeTaskItem> implements
		java.io.Serializable {
	/** id */
	private String id;
	/* 序号 */
	private Integer orderNumber;
	/** 描述 */
	private String name;
	/* 文库编号 */
	private String wkId;
	/** 样本编号 */
	private String code;
	// 原始样本编号
	private String sampleCode;
	/** 拆分后的样本编号 */
	private String splitCode;
	/* index */
	private String indexa;
	/** 浓度 */
	private Double concentration;
	/** 处理结果 */
	private String result;
	/** 失败原因 */
	private String reason;
	/** 步骤编号 */
	private String stepNum;
	/** 备注 */
	private String note;
	/* 患者姓名 */
	private String patientName;
	/* 检测项目 */
	private String productId;
	/* 检测项目 */
	private String productName;
	/* 取样日期 */
	private String inspectDate;
	/* 接收日期 */
	private Date acceptDate;
	/* 身份证 (改为片段范围) */
	private String idCard;
	/* 手机号 */
	private String phone;
	/* 关联任务单 */
	private String orderId;
	/* 检测方法 */
	private String sequenceFun;
	/* 应出报告日期 */
	private Date reportDate;
	/* 状态（表示是否入库 1：入库） */
	private String state;
	/** 相关主表 */
	private WkLifeTask wk;
	/** 行号 */
	private String rowCode;
	/** 列号 */
	private String colCode;
	/** 板号 */
	private String counts;
	/** 体积 */
	private Double volume;
	/** 样本数量 */
	private Double sampleNum;
	// 样本用量
	private Double sampleConsume;
	/** 临时表Id */
	private String tempId;

	/*
	 * 补充体积
	 */
	private Double addVolume;
	/*
	 * 总体积
	 */
	private Double sumVolume;

	/** 单位 */
	private String unit;
	/** 项目编号 */
	private String projectId;
	/** 合同编号 */
	private String contractId;
	/** 任务单类型 */
	private String orderType;
	/** 下一步流向 */
	private String nextFlow;
	/** 建库任务单编号 */
	private String jkTaskId;
	// 区分临床还是科技服务 0 临床 1 科技服务
	private String classify;
	// 数据类型
	private String dataType;
	// 数据量
	private String dataNum;
	// 中间产物数量
	private String productNum;
	// 中间产物类型
	private DicSampleType dicSampleType;
	// 样本类型
	private String sampleType;
	// i5
	private String i5;
	// i7
	private String i7;
	// 标签
	private String tag;
	// 循环数
	private String loopNum;
	// 总量
	private Double sumTotal;
	// 扩增比例
	private Double pcrRatio;
	// 预期通量
	private Double expectNum;
	// RIN
	private String rin;
	// 实验室样本号
	private String labCode;
	/** 样本主数据 */
	private SampleInfo sampleInfo;
	// 科技服务
	private TechJkServiceTask techJkServiceTask;
	// 是否质控品
	private String isZkp;
	// 变量
	private Double blx;
	// 混合号
	private String blendCode;
	// 内部项目号
	private String inwardCode;

	// 文库类型
	private String libType;
	// 插入大小
	private String insertSize;
	// 文库数量(样本用量（ul）)
	private String libNum;
	// 样品现有量(样本剩余总量)
	private String sampleNowNum;
	// 取样量
	private String takeNum;
	// 打断体积
	private String breakNum;
	// TE或H20体积
	private String teOrH20num;
	// 数据量
	private String dataNowNum;
	// 储位
	private String storageLocation;
	// 测序类型
	private String seqType;
	// 引物
	private String primer;
	// 是否重建库
	private String isInit;
	// 物种
	private String species;
	// 是否有RNA污染
	private String isRna;
	// 取样体积
	private String sampleVolume;
	// 科技服务明细
	private TechJkServiceTaskItem tjItem;

	// 芯片类型
	private String chipType;
	// Qubit浓度
	private Double qubitConcentration;
	// Nanodrop浓度
	private Double NanodropConcentration;
	// index试剂盒id
	private String kit;

	public String getKit() {
		return kit;
	}

	public void setKit(String kit) {
		this.kit = kit;
	}

	public String getChipType() {
		return chipType;
	}

	public void setChipType(String chipType) {
		this.chipType = chipType;
	}

	public Double getQubitConcentration() {
		return qubitConcentration;
	}

	public void setQubitConcentration(Double qubitConcentration) {
		this.qubitConcentration = qubitConcentration;
	}

	public Double getNanodropConcentration() {
		return NanodropConcentration;
	}

	public void setNanodropConcentration(Double nanodropConcentration) {
		NanodropConcentration = nanodropConcentration;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "TJ_ITEM")
	public TechJkServiceTaskItem getTjItem() {
		return tjItem;
	}

	public void setTjItem(TechJkServiceTaskItem tjItem) {
		this.tjItem = tjItem;
	}

	public String getInwardCode() {
		return inwardCode;
	}

	public void setInwardCode(String inwardCode) {
		this.inwardCode = inwardCode;
	}

	public String getSampleVolume() {
		return sampleVolume;
	}

	public void setSampleVolume(String sampleVolume) {
		this.sampleVolume = sampleVolume;
	}

	public String getLibType() {
		return libType;
	}

	public void setLibType(String libType) {
		this.libType = libType;
	}

	public String getInsertSize() {
		return insertSize;
	}

	public void setInsertSize(String insertSize) {
		this.insertSize = insertSize;
	}

	public String getLibNum() {
		return libNum;
	}

	public void setLibNum(String libNum) {
		this.libNum = libNum;
	}

	public String getSampleNowNum() {
		return sampleNowNum;
	}

	public void setSampleNowNum(String sampleNowNum) {
		this.sampleNowNum = sampleNowNum;
	}

	public String getTakeNum() {
		return takeNum;
	}

	public void setTakeNum(String takeNum) {
		this.takeNum = takeNum;
	}

	public String getBreakNum() {
		return breakNum;
	}

	public void setBreakNum(String breakNum) {
		this.breakNum = breakNum;
	}

	public String getTeOrH20num() {
		return teOrH20num;
	}

	public void setTeOrH20num(String teOrH20num) {
		this.teOrH20num = teOrH20num;
	}

	public String getDataNowNum() {
		return dataNowNum;
	}

	public void setDataNowNum(String dataNowNum) {
		this.dataNowNum = dataNowNum;
	}

	public String getStorageLocation() {
		return storageLocation;
	}

	public void setStorageLocation(String storageLocation) {
		this.storageLocation = storageLocation;
	}

	public String getSeqType() {
		return seqType;
	}

	public void setSeqType(String seqType) {
		this.seqType = seqType;
	}

	public String getPrimer() {
		return primer;
	}

	public void setPrimer(String primer) {
		this.primer = primer;
	}

	public String getIsInit() {
		return isInit;
	}

	public void setIsInit(String isInit) {
		this.isInit = isInit;
	}

	public String getSpecies() {
		return species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public String getIsRna() {
		return isRna;
	}

	public void setIsRna(String isRna) {
		this.isRna = isRna;
	}

	public String getBlendCode() {
		return blendCode;
	}

	public void setBlendCode(String blendCode) {
		this.blendCode = blendCode;
	}

	public Double getBlx() {
		return blx;
	}

	public void setBlx(Double blx) {
		this.blx = blx;
	}

	public String getIsZkp() {
		return isZkp;
	}

	public void setIsZkp(String isZkp) {
		this.isZkp = isZkp;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "TECH_JK_SERVICE_TASK")
	public TechJkServiceTask getTechJkServiceTask() {
		return techJkServiceTask;
	}

	public void setTechJkServiceTask(TechJkServiceTask techJkServiceTask) {
		this.techJkServiceTask = techJkServiceTask;
	}

	/**
	 * 方法: 取得SampleInfo
	 * 
	 * @return: 样本主数据
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "SAMPLE_INFO")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}

	/**
	 * 方法: 设置SampleInfo
	 * 
	 * @param: 样本主数据
	 */
	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}

	public String getLabCode() {
		return labCode;
	}

	public void setLabCode(String labCode) {
		this.labCode = labCode;
	}

	public String getRin() {
		return rin;
	}

	public void setRin(String rin) {
		this.rin = rin;
	}

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	public Double getSampleConsume() {
		return sampleConsume;
	}

	public void setSampleConsume(Double sampleConsume) {
		this.sampleConsume = sampleConsume;
	}

	public String getLoopNum() {
		return loopNum;
	}

	public void setLoopNum(String loopNum) {
		this.loopNum = loopNum;
	}

	public Double getSumTotal() {
		return sumTotal;
	}

	public void setSumTotal(Double sumTotal) {
		this.sumTotal = sumTotal;
	}

	public Double getPcrRatio() {
		return pcrRatio;
	}

	public void setPcrRatio(Double pcrRatio) {
		this.pcrRatio = pcrRatio;
	}

	public Double getExpectNum() {
		return expectNum;
	}

	public void setExpectNum(Double expectNum) {
		this.expectNum = expectNum;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getI5() {
		return i5;
	}

	public void setI5(String i5) {
		this.i5 = i5;
	}

	public String getI7() {
		return i7;
	}

	public void setI7(String i7) {
		this.i7 = i7;
	}

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	public String getProductNum() {
		return productNum;
	}

	public void setProductNum(String productNum) {
		this.productNum = productNum;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "DIC_SAMPLE_TYPE")
	public DicSampleType getDicSampleType() {
		return dicSampleType;
	}

	public void setDicSampleType(DicSampleType dicSampleType) {
		this.dicSampleType = dicSampleType;
	}

	public String getRowCode() {
		return rowCode;
	}

	public void setRowCode(String rowCode) {
		this.rowCode = rowCode;
	}

	public String getColCode() {
		return colCode;
	}

	public void setColCode(String colCode) {
		this.colCode = colCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String id
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String id
	 */
	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "ORDER_NUMBER", length = 20)
	public Integer getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 50)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 文库编号
	 */
	@Column(name = "WK_ID", length = 50)
	public String getWkId() {
		return wkId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 文库编号
	 */
	public void setWkId(String wkId) {
		this.wkId = wkId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 原始样本编号
	 */
	@Column(name = "SAMPLE_CODE", length = 100)
	public String getSampleCode() {
		return sampleCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 原始样本编号
	 */
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本编号
	 */
	@Column(name = "CODE", length = 50)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	public String getIndexa() {
		return indexa;
	}

	public void setIndexa(String indexa) {
		this.indexa = indexa;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 浓度
	 */
	@Column(name = "CONCENTRATION", length = 50)
	public Double getConcentration() {
		return this.concentration;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 浓度
	 */
	public void setConcentration(Double concentration) {
		this.concentration = concentration;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 是否合格
	 */
	@Column(name = "RESULT", length = 50)
	public String getResult() {
		return this.result;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 是否合格
	 */
	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 失败原因
	 */
	@Column(name = "REASON", length = 50)
	public String getReason() {
		return this.reason;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 失败原因
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	@Column(name = "STEP_NUM", length = 50)
	public String getStepNum() {
		return stepNum;
	}

	public void setStepNum(String stepNum) {
		this.stepNum = stepNum;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	@Column(name = "PATIENT_NAME", length = 20)
	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	@Column(name = "PRODUCT_ID", length = 20)
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Column(name = "PRODUCT_NAME", length = 20)
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name = "INSPECT_DATE", length = 20)
	public String getInspectDate() {
		return inspectDate;
	}

	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}

	@Column(name = "ACCEPT_DATE", length = 20)
	public Date getAcceptDate() {
		return acceptDate;
	}

	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	@Column(name = "ID_CARD", length = 20)
	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	@Column(name = "SEQUENCE_FUN", length = 20)
	public String getSequenceFun() {
		return sequenceFun;
	}

	public void setSequenceFun(String sequenceFun) {
		this.sequenceFun = sequenceFun;
	}

	@Column(name = "REPORT_DATE", length = 20)
	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	@Column(name = "STATE", length = 20)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得WK
	 * 
	 * @return: WK 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "WK_SAMPLE_TASK")
	public WkLifeTask getWk() {
		return this.wk;
	}

	/**
	 * 方法: 设置WK
	 * 
	 * @param: WK 相关主表
	 */
	public void setWk(WkLifeTask wk) {
		this.wk = wk;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getCounts() {
		return counts;
	}

	public void setCounts(String counts) {
		this.counts = counts;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getContractId() {
		return contractId;
	}

	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public Double getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(Double sampleNum) {
		this.sampleNum = sampleNum;
	}

	public String getJkTaskId() {
		return jkTaskId;
	}

	public void setJkTaskId(String jkTaskId) {
		this.jkTaskId = jkTaskId;
	}

	public Double getAddVolume() {
		return addVolume;
	}

	public void setAddVolume(Double addVolume) {
		this.addVolume = addVolume;
	}

	public Double getSumVolume() {
		return sumVolume;
	}

	public void setSumVolume(Double sumVolume) {
		this.sumVolume = sumVolume;
	}

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

	public String getNextFlow() {
		return nextFlow;
	}

	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getDataNum() {
		return dataNum;
	}

	public void setDataNum(String dataNum) {
		this.dataNum = dataNum;
	}
	public String getSplitCode() {
		return splitCode;
	}

	public void setSplitCode(String splitCode) {
		this.splitCode = splitCode;
	}
}