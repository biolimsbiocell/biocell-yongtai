package com.biolims.experiment.wkLife.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.core.model.user.UserGroup;
import com.biolims.dao.EntityDao;
import com.biolims.system.template.model.Template;

/**
 * @Title: Model
 * @Description: 文库构建
 * @author lims-platform
 * @date 2015-11-29 19:26:25
 * @version V1.0
 * 
 */
@Entity
@Table(name = "WK_LIFE_TASK")
@SuppressWarnings("serial")
public class WkLifeTask extends EntityDao<WkLifeTask> implements java.io.Serializable {
	/** 编号 */
	private String id;
	/** 描述 */
	private String name;
	/** 实验员 */
	private User reciveUser;
	/** 实验时间 */
	private Date reciveDate;
	/** 下达人 */
	private User createUser;
	/** 下达时间 */
	private String createDate;
	// 审核人
	private User confirmUser;
	/** 完成时间 */
	private Date confirmDate;
	/* 模板 */
	private Template template;
	/* index */
	private String indexa;
	/** 状态id */
	private String state;
	/** 工作流状态 */
	private String stateName;
	/** 备注 */
	private String note;
	/** 实验组 */
	private UserGroup acceptUser;
	/** 容器数量 */
	private Integer maxNum;
	// 指控品数量
	private String qcNum;
	// 类型
	private String type;
	// 第一批实验员
	private String testUserOneId;
	private String testUserOneName;
	// 第二批实验员
	private String testUserTwoId;
	private String testUserTwoName;
	// 测序类型 1:illumina 2:lon torrent
	private String seType;

	public String getSeType() {
		return seType;
	}

	public void setSeType(String seType) {
		this.seType = seType;
	}

	public String getTestUserOneId() {
		return testUserOneId;
	}

	public void setTestUserOneId(String testUserOneId) {
		this.testUserOneId = testUserOneId;
	}

	public String getTestUserOneName() {
		return testUserOneName;
	}

	public void setTestUserOneName(String testUserOneName) {
		this.testUserOneName = testUserOneName;
	}

	public String getTestUserTwoId() {
		return testUserTwoId;
	}

	public void setTestUserTwoId(String testUserTwoId) {
		this.testUserTwoId = testUserTwoId;
	}

	public String getTestUserTwoName() {
		return testUserTwoName;
	}

	public void setTestUserTwoName(String testUserTwoName) {
		this.testUserTwoName = testUserTwoName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 50)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 实验员
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "RECIVE_USER")
	public User getReciveUser() {
		return this.reciveUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 实验员
	 */
	public void setReciveUser(User reciveUser) {
		if(reciveUser!=null&&reciveUser.getId()!=null&&reciveUser.getId().equals("")) reciveUser= null; this.reciveUser = reciveUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 实验时间
	 */
	@Column(name = "RECIVE_DATE", length = 50)
	public Date getReciveDate() {
		return this.reciveDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 实验时间
	 */
	public void setReciveDate(Date reciveDate) {
		this.reciveDate = reciveDate;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 下达人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 下达人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 下达时间
	 */
	@Column(name = "CREATE_DATE", length = 50)
	public String getCreateDate() {
		return this.createDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 下达时间
	 */
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "TEMPLATE")
	public Template getTemplate() {
		return template;
	}

	public void setTemplate(Template template) {
		if(template!=null&&template.getId()!=null&&template.getId().equals(""))  template= null; this.template = template;
	}

	@Column(name = "INDEXA", length = 50)
	public String getIndexa() {
		return indexa;
	}

	public void setIndexa(String indexa) {
		this.indexa = indexa;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态id
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态id
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 工作流状态
	 */
	@Column(name = "STATE_NAME", length = 50)
	public String getStateName() {
		return this.stateName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 工作流状态
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "T_USER_GROUP")
	public UserGroup getAcceptUser() {
		return acceptUser;
	}

	public Integer getMaxNum() {
		return maxNum;
	}

	public void setMaxNum(Integer maxNum) {
		this.maxNum = maxNum;
	}

	public void setAcceptUser(UserGroup acceptUser) {
		if(acceptUser!=null&&acceptUser.getId()!=null&&acceptUser.getId().equals("")) acceptUser= null; this.acceptUser = acceptUser;
	}

	public String getQcNum() {
		return qcNum;
	}

	public void setQcNum(String qcNum) {
		this.qcNum = qcNum;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "CONFIRM_USER")
	public User getConfirmUser() {
		return confirmUser;
	}

	public void setConfirmUser(User confirmUser) {
		if(confirmUser!=null&&confirmUser.getId()!=null&&confirmUser.getId().equals(""))  confirmUser= null; this.confirmUser = confirmUser;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

}