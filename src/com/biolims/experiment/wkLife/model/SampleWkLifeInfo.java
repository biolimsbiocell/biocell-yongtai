package com.biolims.experiment.wkLife.model;

import java.util.Date;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleInfo;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;

/**
 * @Title: Model
 * @Description: 文库结果
 * @author lims-platform
 * @date 2015-11-29 19:26:05
 * @version V1.0
 * 
 */
@Entity
@Table(name = "SAMPLE_WK_LIFE_INFO")
@SuppressWarnings("serial")
public class SampleWkLifeInfo extends EntityDao<SampleWkLifeInfo> implements
		java.io.Serializable {
	/** id */
	private String id;
	/** 描述 */
	private String name;
	/** 文库编号 */
	private String wkCode;
	/** 拆分后的样本编号 */
	private String splitCode;
	// 样本编号
	private String code;
	/** 建库任务单编号 */
	private String jkTaskId;
	/** index */
	private String indexa;
	/** 原始样本编号 */
	private String sampleCode;

	/** 单位 */
	private String unit;
	/** 处理结果 */
	private String result;
	/** 下一步流向ID */
	private String nextFlowId;
	/** 下一步流向 */
	private String nextFlow;
	/* 失败原因 */
	private String reason;
	/* 是否提交 */
	private String submit;
	/* 患者姓名 */
	private String patientName;
	/* 检测项目 */
	private String productId;
	/* 检测项目 */
	private String productName;
	/* 取样日期 */
	private String inspectDate;
	/* 接收日期 */
	private Date acceptDate;
	/* 身份证 */
	private String idCard;
	/* 手机号 */
	private String phone;
	/* 关联任务单 */
	private String orderId;
	/* 检测方法 */
	private String sequenceFun;
	/* 应出报告日期 */
	private Date reportDate;
	/* 状态 */
	private String state;
	/** 处理意见 */
	private String method;
	/** 确认执行 */
	private String isExecute;
	/** 备注 */
	private String note;
	/** 相关主表 */
	private WkLifeTask wk;
	/** 行号 */
	private String rowCode;
	/** 列号 */
	private String colCode;
	/** 板号 */
	private String counts;

	// 合同ID
	private String contractId;
	// 项目ID
	private String projectId;
	/** 任务单类型 */
	private String orderType;
	// 区分临床还是科技服务 0 临床 1 科技服务
	private String classify;
	// 数据类型
	private String dataType;
	// 数据量
	private String dataNum;
	/**
	 * 方法: 取得String
	 * 
	 * @return: String id
	 */
	/** 储位 */
	private String location;
	// 样本类型
	private String sampleType;
	// 中间产物类型
	private DicSampleType dicSampleType;
	// i5
	private String i5;
	// i7
	private String i7;

	// 预期通量
	private Double expectNum;
	// 样本数量
	private Double sampleNum;
	/** 临时表Id */
	private String tempId;

	// 纯化破碎后浓度
	private Double chpsConcentration;
	// 纯化体积
	private Double chVolume;
	// 纯化总量
	private Double chSumTotal;
	/** 0.85X纯化浓度 */
	private Double concentration;
	/** 0.85X纯化体积 */
	private Double volume;
	// 0.85X纯化总量
	private Double sumTotal;
	// index浓度
	private Double indexConcentration;
	// 大小片段分离后浓度
	private Double dxpdConcentration;
	// 大小片段体积
	private Double dxpdVolume;
	// PCR前总量
	private Double dxpdSumTotal;
	// 文库浓度
	private Double wkConcentration;
	// 文库体积
	private Double wkVolume;
	// 文库总量
	private Double wkSumTotal;
	// 循环数
	private String loopNum;
	// 扩增比例
	private Double pcrRatio;
	// 实验室样本号
	private String labCode;
	/** 样本主数据 */
	private SampleInfo sampleInfo;
	// 科技服务
	private TechJkServiceTask techJkServiceTask;
	// 是否质控品
	private String isZkp;

	// 文库类型
	private String libType;
	// 插入大小(文库片段大小)
	private String insertSize;
	// 文库数量
	private String libNum;
	// 样品现有量
	private String sampleNowNum;
	// 取样量
	private String takeNum;
	// 打断体积
	private String breakNum;
	// TE或H20体积
	private String teOrH20num;
	// 数据量
	private String dataNowNum;
	// 储位
	private String storageLocation;
	// 测序类型
	private String seqType;
	// 引物
	private String primer;
	// 是否重建库
	private String isInit;
	// 物种
	private String species;
	// 是否有RNA污染
	private String isRna;
	// 取样体积
	private String sampleVolume;
	// 内部项目号
	private String inwardCode;
	// 科技服务明细
	private TechJkServiceTaskItem tjItem;

	/** 单位组 */
	private DicType unitGroup;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "DIC_SAMPLE_TYPE")
	public DicSampleType getDicSampleType() {
		return dicSampleType;
	}

	public void setDicSampleType(DicSampleType dicSampleType) {
		this.dicSampleType = dicSampleType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "UNIT_GROUP")
	public DicType getUnitGroup() {
		return unitGroup;
	}

	public void setUnitGroup(DicType unitGroup) {
		this.unitGroup = unitGroup;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "TJ_ITEM")
	public TechJkServiceTaskItem getTjItem() {
		return tjItem;
	}

	public void setTjItem(TechJkServiceTaskItem tjItem) {
		this.tjItem = tjItem;
	}

	public String getInwardCode() {
		return inwardCode;
	}

	public void setInwardCode(String inwardCode) {
		this.inwardCode = inwardCode;
	}

	public String getLibType() {
		return libType;
	}

	public void setLibType(String libType) {
		this.libType = libType;
	}

	public String getInsertSize() {
		return insertSize;
	}

	public void setInsertSize(String insertSize) {
		this.insertSize = insertSize;
	}

	public String getLibNum() {
		return libNum;
	}

	public void setLibNum(String libNum) {
		this.libNum = libNum;
	}

	public String getSampleNowNum() {
		return sampleNowNum;
	}

	public void setSampleNowNum(String sampleNowNum) {
		this.sampleNowNum = sampleNowNum;
	}

	public String getTakeNum() {
		return takeNum;
	}

	public void setTakeNum(String takeNum) {
		this.takeNum = takeNum;
	}

	public String getBreakNum() {
		return breakNum;
	}

	public void setBreakNum(String breakNum) {
		this.breakNum = breakNum;
	}

	public String getTeOrH20num() {
		return teOrH20num;
	}

	public void setTeOrH20num(String teOrH20num) {
		this.teOrH20num = teOrH20num;
	}

	public String getDataNowNum() {
		return dataNowNum;
	}

	public void setDataNowNum(String dataNowNum) {
		this.dataNowNum = dataNowNum;
	}

	public String getStorageLocation() {
		return storageLocation;
	}

	public void setStorageLocation(String storageLocation) {
		this.storageLocation = storageLocation;
	}

	public String getSeqType() {
		return seqType;
	}

	public void setSeqType(String seqType) {
		this.seqType = seqType;
	}

	public String getPrimer() {
		return primer;
	}

	public void setPrimer(String primer) {
		this.primer = primer;
	}

	public String getIsInit() {
		return isInit;
	}

	public void setIsInit(String isInit) {
		this.isInit = isInit;
	}

	public String getSpecies() {
		return species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public String getIsRna() {
		return isRna;
	}

	public void setIsRna(String isRna) {
		this.isRna = isRna;
	}

	public String getSampleVolume() {
		return sampleVolume;
	}

	public void setSampleVolume(String sampleVolume) {
		this.sampleVolume = sampleVolume;
	}

	public String getIsZkp() {
		return isZkp;
	}

	public void setIsZkp(String isZkp) {
		this.isZkp = isZkp;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "TECH_JK_SERVICE_TASK")
	public TechJkServiceTask getTechJkServiceTask() {
		return techJkServiceTask;
	}

	public void setTechJkServiceTask(TechJkServiceTask techJkServiceTask) {
		this.techJkServiceTask = techJkServiceTask;
	}

	/**
	 * 方法: 取得SampleInfo
	 * 
	 * @return: 样本主数据
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "SAMPLE_INFO")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}

	/**
	 * 方法: 设置SampleInfo
	 * 
	 * @param: 样本主数据
	 */
	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}

	public String getLabCode() {
		return labCode;
	}

	public void setLabCode(String labCode) {
		this.labCode = labCode;
	}

	public Double getChpsConcentration() {
		return chpsConcentration;
	}

	public void setChpsConcentration(Double chpsConcentration) {
		this.chpsConcentration = chpsConcentration;
	}

	public Double getChVolume() {
		return chVolume;
	}

	public void setChVolume(Double chVolume) {
		this.chVolume = chVolume;
	}

	public Double getChSumTotal() {
		return chSumTotal;
	}

	public void setChSumTotal(Double chSumTotal) {
		this.chSumTotal = chSumTotal;
	}

	public Double getIndexConcentration() {
		return indexConcentration;
	}

	public void setIndexConcentration(Double indexConcentration) {
		this.indexConcentration = indexConcentration;
	}

	public Double getDxpdConcentration() {
		return dxpdConcentration;
	}

	public void setDxpdConcentration(Double dxpdConcentration) {
		this.dxpdConcentration = dxpdConcentration;
	}

	public Double getDxpdVolume() {
		return dxpdVolume;
	}

	public void setDxpdVolume(Double dxpdVolume) {
		this.dxpdVolume = dxpdVolume;
	}

	public Double getDxpdSumTotal() {
		return dxpdSumTotal;
	}

	public void setDxpdSumTotal(Double dxpdSumTotal) {
		this.dxpdSumTotal = dxpdSumTotal;
	}

	public Double getWkConcentration() {
		return wkConcentration;
	}

	public void setWkConcentration(Double wkConcentration) {
		this.wkConcentration = wkConcentration;
	}

	public Double getWkVolume() {
		return wkVolume;
	}

	public void setWkVolume(Double wkVolume) {
		this.wkVolume = wkVolume;
	}

	public Double getWkSumTotal() {
		return wkSumTotal;
	}

	public void setWkSumTotal(Double wkSumTotal) {
		this.wkSumTotal = wkSumTotal;
	}

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	public Double getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(Double sampleNum) {
		this.sampleNum = sampleNum;
	}

	public Double getConcentration() {
		return concentration;
	}

	public void setConcentration(Double concentration) {
		this.concentration = concentration;
	}

	public String getLoopNum() {
		return loopNum;
	}

	public void setLoopNum(String loopNum) {
		this.loopNum = loopNum;
	}

	public Double getSumTotal() {
		return sumTotal;
	}

	public void setSumTotal(Double sumTotal) {
		this.sumTotal = sumTotal;
	}

	public Double getPcrRatio() {
		return pcrRatio;
	}

	public void setPcrRatio(Double pcrRatio) {
		this.pcrRatio = pcrRatio;
	}

	public Double getExpectNum() {
		return expectNum;
	}

	public void setExpectNum(Double expectNum) {
		this.expectNum = expectNum;
	}

	public String getI5() {
		return i5;
	}

	public void setI5(String i5) {
		this.i5 = i5;
	}

	public String getI7() {
		return i7;
	}

	public void setI7(String i7) {
		this.i7 = i7;
	}

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 50)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 文库编号
	 */
	@Column(name = "WK_CODE", length = 50)
	public String getWkCode() {
		return wkCode;
	}

	public void setWkCode(String wkCode) {
		this.wkCode = wkCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本编号
	 */
	@Column(name = "CODE", length = 50)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "INDEXA", length = 50)
	public String getIndexa() {
		return indexa;
	}

	public void setIndexa(String indexa) {
		this.indexa = indexa;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 原始样本编号
	 */
	@Column(name = "SAMPLE_CODE", length = 100)
	public String getSampleCode() {
		return this.sampleCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 原始样本编号
	 */
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 体积
	 */
	@Column(name = "VOLUME", length = 50)
	public Double getVolume() {
		return this.volume;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 体积
	 */
	public void setVolume(Double volume) {
		this.volume = volume;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 单位
	 */
	@Column(name = "UNIT", length = 50)
	public String getUnit() {
		return this.unit;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 单位
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 结果判定
	 */
	@Column(name = "RESULT", length = 50)
	public String getResult() {
		return this.result;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 结果判定
	 */
	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 下一步流向
	 */
	@Column(name = "NEXT_FLOW", length = 50)
	public String getNextFlow() {
		return this.nextFlow;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 下一步流向
	 */
	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	@Column(name = "REASON", length = 50)
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@Column(name = "SUBMIT", length = 50)
	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

	@Column(name = "PATIENT_NAME", length = 20)
	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	@Column(name = "PRODUCT_ID", length = 20)
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Column(name = "PRODUCT_NAME", length = 20)
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name = "INSPECT_DATE", length = 20)
	public String getInspectDate() {
		return inspectDate;
	}

	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}

	@Column(name = "ACCEPT_DATE", length = 20)
	public Date getAcceptDate() {
		return acceptDate;
	}

	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	@Column(name = "ID_CARD", length = 20)
	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	@Column(name = "SEQUENCE_FUN", length = 20)
	public String getSequenceFun() {
		return sequenceFun;
	}

	public void setSequenceFun(String sequenceFun) {
		this.sequenceFun = sequenceFun;
	}

	@Column(name = "REPORT_DATE", length = 20)
	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	@Column(name = "STATE", length = 20)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "METHOD", length = 20)
	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	@Column(name = "IS_EXECUTE", length = 20)
	public String getIsExecute() {
		return isExecute;
	}

	public void setIsExecute(String isExecute) {
		this.isExecute = isExecute;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得WK
	 * 
	 * @return: WK 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "WK_SAMPLE_TASK")
	public WkLifeTask getWk() {
		return this.wk;
	}

	/**
	 * 方法: 设置WK
	 * 
	 * @param: WK 相关主表
	 */
	public void setWk(WkLifeTask wk) {
		this.wk = wk;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getRowCode() {
		return rowCode;
	}

	public void setRowCode(String rowCode) {
		this.rowCode = rowCode;
	}

	public String getColCode() {
		return colCode;
	}

	public void setColCode(String colCode) {
		this.colCode = colCode;
	}

	public String getCounts() {
		return counts;
	}

	public void setCounts(String counts) {
		this.counts = counts;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getContractId() {
		return contractId;
	}

	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getJkTaskId() {
		return jkTaskId;
	}

	public void setJkTaskId(String jkTaskId) {
		this.jkTaskId = jkTaskId;
	}

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getDataNum() {
		return dataNum;
	}

	public void setDataNum(String dataNum) {
		this.dataNum = dataNum;
	}

	public String getNextFlowId() {
		return nextFlowId;
	}

	public void setNextFlowId(String nextFlowId) {
		this.nextFlowId = nextFlowId;
	}
	public String getSplitCode() {
		return splitCode;
	}

	public void setSplitCode(String splitCode) {
		this.splitCode = splitCode;
	}
}