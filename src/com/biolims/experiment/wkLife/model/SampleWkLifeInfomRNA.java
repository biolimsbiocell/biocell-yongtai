package com.biolims.experiment.wkLife.model;

import java.util.Date;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.sample.model.SampleInfo;
import com.biolims.technology.wk.model.TechJkServiceTask;

/**
 * @Title: Model
 * @Description: 文库结果 mRNA纯化
 * @author lims-platform
 * @date 2015-11-29 19:26:05
 * @version V1.0
 * 
 */
@Entity
@Table(name = "SAMPLE_WK_LIFE_INFO_MRNA")
@SuppressWarnings("serial")
public class SampleWkLifeInfomRNA extends EntityDao<SampleWkLifeInfomRNA> implements
		java.io.Serializable {
	/** id */
	private String id;
	/** 描述 */
	private String name;
	// 样本编号
	private String code;
	/** index */
	private String indexa;
	/** 原始样本编号 */
	private String sampleCode;
	/** 单位 */
	private String unit;
	/** 处理结果 */
	private String result;
	/** 下一步流向ID */
	private String nextFlowId;
	/** 下一步流向 */
	private String nextFlow;
	/* 失败原因 */
	private String reason;
	/* 是否提交 */
	private String submit;
	/* 患者姓名 */
	private String patientName;
	/* 检测项目 */
	private String productId;
	/* 检测项目 */
	private String productName;
	/* 取样日期 */
	private String inspectDate;
	/* 接收日期 */
	private Date acceptDate;
	/* 身份证 */
	private String idCard;
	/* 手机号 */
	private String phone;
	/* 关联任务单 */
	private String orderId;
	/* 应出报告日期 */
	private Date reportDate;
	/* 状态 */
	private String state;
	/** 处理意见 */
	private String method;
	/** 备注 */
	private String note;
	/** 相关主表 */
	private WkLifeTask wkTask;
	// 样本类型
	private String sampleType;
	// i5
	private String i5;
	// i7
	private String i7;
	// 预期通量
	private Double expectNum;
	// 样本数量
	private Double sampleNum;
	/** 临时表Id */
	private String tempId;

	// RIN
	private String rin;
	// 浓缩浓度
	private Double nsConcentration;
	// 浓缩体积
	private Double nsVolume;
	// 浓缩得量
	private Double nsSumTotal;
	// rRNA纯化浓度
	private Double qcConcentration;
	// rRNA纯化体积
	private Double qcVolume;
	// rRNA纯化得量
	private Double qcSumTotal;
	// rRNA纯化得率
	private Double qcYield;
	// RNA打断温度
	private String temperature;
	// RNA打断时间
	private String time;
	/** cDNA纯化浓度 */
	private Double concentration;
	/** cDNA纯化体积 */
	private Double volume;
	// cDNA纯化得量
	private Double sumTotal;
	// 循环数
	private String loopNum;
	// 文库浓度
	private Double wkConcentration;
	// 文库体积
	private Double wkVolume;
	// 文库总量
	private Double wkSumTotal;
	// 扩增比例
	private Double pcrRatio;
	// 实验室样本号
	private String labCode;
	/** 样本主数据 */
	private SampleInfo sampleInfo;
	// 科技服务
	private TechJkServiceTask techJkServiceTask;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "TECH_JK_SERVICE_TASK")
	public TechJkServiceTask getTechJkServiceTask() {
		return techJkServiceTask;
	}

	public void setTechJkServiceTask(TechJkServiceTask techJkServiceTask) {
		this.techJkServiceTask = techJkServiceTask;
	}

	/**
	 * 方法: 取得SampleInfo
	 * 
	 * @return: 样本主数据
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "SAMPLE_INFO")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}

	/**
	 * 方法: 设置SampleInfo
	 * 
	 * @param: 样本主数据
	 */
	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}

	public String getLabCode() {
		return labCode;
	}

	public void setLabCode(String labCode) {
		this.labCode = labCode;
	}

	public String getRin() {
		return rin;
	}

	public void setRin(String rin) {
		this.rin = rin;
	}

	public Double getNsConcentration() {
		return nsConcentration;
	}

	public void setNsConcentration(Double nsConcentration) {
		this.nsConcentration = nsConcentration;
	}

	public Double getNsVolume() {
		return nsVolume;
	}

	public void setNsVolume(Double nsVolume) {
		this.nsVolume = nsVolume;
	}

	public Double getNsSumTotal() {
		return nsSumTotal;
	}

	public void setNsSumTotal(Double nsSumTotal) {
		this.nsSumTotal = nsSumTotal;
	}

	public Double getQcConcentration() {
		return qcConcentration;
	}

	public void setQcConcentration(Double qcConcentration) {
		this.qcConcentration = qcConcentration;
	}

	public Double getQcVolume() {
		return qcVolume;
	}

	public void setQcVolume(Double qcVolume) {
		this.qcVolume = qcVolume;
	}

	public Double getQcSumTotal() {
		return qcSumTotal;
	}

	public void setQcSumTotal(Double qcSumTotal) {
		this.qcSumTotal = qcSumTotal;
	}

	public Double getQcYield() {
		return qcYield;
	}

	public void setQcYield(Double qcYield) {
		this.qcYield = qcYield;
	}

	public String getTemperature() {
		return temperature;
	}

	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Double getWkConcentration() {
		return wkConcentration;
	}

	public void setWkConcentration(Double wkConcentration) {
		this.wkConcentration = wkConcentration;
	}

	public Double getWkVolume() {
		return wkVolume;
	}

	public void setWkVolume(Double wkVolume) {
		this.wkVolume = wkVolume;
	}

	public Double getWkSumTotal() {
		return wkSumTotal;
	}

	public void setWkSumTotal(Double wkSumTotal) {
		this.wkSumTotal = wkSumTotal;
	}

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	public Double getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(Double sampleNum) {
		this.sampleNum = sampleNum;
	}

	public Double getConcentration() {
		return concentration;
	}

	public void setConcentration(Double concentration) {
		this.concentration = concentration;
	}

	public String getLoopNum() {
		return loopNum;
	}

	public void setLoopNum(String loopNum) {
		this.loopNum = loopNum;
	}

	public Double getSumTotal() {
		return sumTotal;
	}

	public void setSumTotal(Double sumTotal) {
		this.sumTotal = sumTotal;
	}

	public Double getPcrRatio() {
		return pcrRatio;
	}

	public void setPcrRatio(Double pcrRatio) {
		this.pcrRatio = pcrRatio;
	}

	public Double getExpectNum() {
		return expectNum;
	}

	public void setExpectNum(Double expectNum) {
		this.expectNum = expectNum;
	}

	public String getI5() {
		return i5;
	}

	public void setI5(String i5) {
		this.i5 = i5;
	}

	public String getI7() {
		return i7;
	}

	public void setI7(String i7) {
		this.i7 = i7;
	}

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 50)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本编号
	 */
	@Column(name = "CODE", length = 50)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "INDEXA", length = 50)
	public String getIndexa() {
		return indexa;
	}

	public void setIndexa(String indexa) {
		this.indexa = indexa;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 原始样本编号
	 */
	@Column(name = "SAMPLE_CODE", length = 100)
	public String getSampleCode() {
		return this.sampleCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 原始样本编号
	 */
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 体积
	 */
	@Column(name = "VOLUME", length = 50)
	public Double getVolume() {
		return this.volume;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 体积
	 */
	public void setVolume(Double volume) {
		this.volume = volume;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 单位
	 */
	@Column(name = "UNIT", length = 50)
	public String getUnit() {
		return this.unit;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 单位
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 结果判定
	 */
	@Column(name = "RESULT", length = 50)
	public String getResult() {
		return this.result;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 结果判定
	 */
	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 下一步流向
	 */
	@Column(name = "NEXT_FLOW", length = 50)
	public String getNextFlow() {
		return this.nextFlow;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 下一步流向
	 */
	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	@Column(name = "REASON", length = 50)
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@Column(name = "SUBMIT", length = 50)
	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

	@Column(name = "PATIENT_NAME", length = 20)
	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	@Column(name = "PRODUCT_ID", length = 20)
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Column(name = "PRODUCT_NAME", length = 20)
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name = "INSPECT_DATE", length = 20)
	public String getInspectDate() {
		return inspectDate;
	}

	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}

	@Column(name = "ACCEPT_DATE", length = 20)
	public Date getAcceptDate() {
		return acceptDate;
	}

	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	@Column(name = "ID_CARD", length = 20)
	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	@Column(name = "REPORT_DATE", length = 20)
	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	@Column(name = "STATE", length = 20)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "METHOD", length = 20)
	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得WKTask
	 * 
	 * @return: WKTask 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "WK_TASK")
	public WkLifeTask getWkTask() {
		return this.wkTask;
	}

	/**
	 * 方法: 设置WK
	 * 
	 * @param: WKTask 相关主表
	 */
	public void setWkTask(WkLifeTask wkTask) {
		this.wkTask = wkTask;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getNextFlowId() {
		return nextFlowId;
	}

	public void setNextFlowId(String nextFlowId) {
		this.nextFlowId = nextFlowId;
	}

}