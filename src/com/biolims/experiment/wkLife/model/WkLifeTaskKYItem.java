package com.biolims.experiment.wkLife.model;

import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleInfo;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;

/**
 * @Title: Model
 * @Description: 核酸提取科研明细
 * @author lims-platform
 * @date 2015-11-22 17:06:00
 * @version V1.0
 * 
 */
@Entity
@Table(name = "DNA_TASK_KY_ITEM")
@SuppressWarnings("serial")
public class WkLifeTaskKYItem extends EntityDao<WkLifeTaskKYItem> implements
		java.io.Serializable {
	/** 编码 */
	private String id;
	// 科技服务
	private TechJkServiceTask techJkServiceTask;
	// 状态
	private String state;
	/** 相关主表 */
	private WkLifeTask wk;

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得WK
	 * 
	 * @return: WK 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "WK_SAMPLE_TASK")
	public WkLifeTask getWk() {
		return this.wk;
	}

	/**
	 * 方法: 设置WK
	 * 
	 * @param: WK 相关主表
	 */
	public void setWk(WkLifeTask wk) {
		this.wk = wk;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "TECH_JK_SERVICE_TASK")
	public TechJkServiceTask getTechJkServiceTask() {
		return techJkServiceTask;
	}

	public void setTechJkServiceTask(TechJkServiceTask techJkServiceTask) {
		this.techJkServiceTask = techJkServiceTask;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 60)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编码
	 */
	public void setId(String id) {
		this.id = id;
	}

}