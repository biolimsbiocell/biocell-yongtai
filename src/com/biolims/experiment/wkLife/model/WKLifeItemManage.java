package com.biolims.experiment.wkLife.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;

/**
 * @Title: Model
 * @Description: 文库明细管理
 * @author lims-platform
 * @date 2015-11-29 19:25:53
 * @version V1.0
 * 
 */
@Entity
@Table(name = "WK_LIFE_ITEM_MANAGE")
@SuppressWarnings("serial")
public class WKLifeItemManage extends EntityDao<WKLifeItemManage> implements
		java.io.Serializable {
	/** id */
	private String id;
	/** 描述 */
	private String name;
	/* 文库编号 */
	private String wkId;
	/** 样本编号 */
	private String code;
	// 原始样本编号
	private String sampleCode;
	/* index */
	private String indexa;
	/** 浓度 */
	private Double concentration;
	/** 处理结果 */
	private String result;
	/** 下一步流向 */
	private String nextFlow;
	/** 失败原因 */
	private String reason;
	/** 步骤编号 */
	private String stepNum;
	/** 备注 */
	private String note;
	/* 状态 */
	private String state;
	/* 患者姓名 */
	private String patientName;
	/* 检测项目 */
	private String productId;
	/* 检测项目 */
	private String productName;
	/* 取样日期 */
	private String inspectDate;
	/* 接收日期 */
	private Date acceptDate;
	/* 身份证 */
	private String idCard;
	/* 手机号 */
	private String phone;
	/* 关联任务单 */
	private String orderId;
	/* 检测方法 */
	private String sequenceFun;
	/* 应出报告日期 */
	private Date reportDate;
	/** 相关主表 */
	private WkLifeTask wk;
	// 区分临床还是科技服务 0 临床 1 科技服务
	private String classify;

	/**
	 * 方法: 取得String
	 * 
	 * @return: String id
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 50)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 文库编号
	 */
	@Column(name = "WK_ID", length = 50)
	public String getWkId() {
		return wkId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 文库编号
	 */
	public void setWkId(String wkId) {
		this.wkId = wkId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 原始样本编号
	 */
	@Column(name = "SAMPLE_CODE", length = 100)
	public String getSampleCode() {
		return sampleCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 原始样本编号
	 */
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本编号
	 */
	@Column(name = "CODE", length = 50)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	public String getIndexa() {
		return indexa;
	}

	public void setIndexa(String indexa) {
		this.indexa = indexa;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 浓度
	 */
	@Column(name = "CONCENTRATION", length = 50)
	public Double getConcentration() {
		return this.concentration;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 浓度
	 */
	public void setConcentration(Double concentration) {
		this.concentration = concentration;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 是否合格
	 */
	@Column(name = "RESULT", length = 50)
	public String getResult() {
		return this.result;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 是否合格
	 */
	public void setResult(String result) {
		this.result = result;
	}

	@Column(name = "NEXT_FLOW", length = 50)
	public String getNextFlow() {
		return nextFlow;
	}

	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 失败原因
	 */
	@Column(name = "REASON", length = 50)
	public String getReason() {
		return this.reason;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 失败原因
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	@Column(name = "STEP_NUM", length = 50)
	public String getStepNum() {
		return stepNum;
	}

	public void setStepNum(String stepNum) {
		this.stepNum = stepNum;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	@Column(name = "STATE", length = 20)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "PATIENT_NAME", length = 20)
	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	@Column(name = "PRODUCT_ID", length = 20)
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Column(name = "PRODUCT_NAME", length = 20)
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name = "INSPECT_DATE", length = 20)
	public String getInspectDate() {
		return inspectDate;
	}

	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}

	@Column(name = "ACCEPT_DATE", length = 20)
	public Date getAcceptDate() {
		return acceptDate;
	}

	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	@Column(name = "ID_CARD", length = 20)
	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	@Column(name = "SEQUENCE_FUN", length = 20)
	public String getSequenceFun() {
		return sequenceFun;
	}

	public void setSequenceFun(String sequenceFun) {
		this.sequenceFun = sequenceFun;
	}

	@Column(name = "REPORT_DATE", length = 20)
	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	/**
	 * 方法: 取得WK
	 * 
	 * @return: WK 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "WK_SAMPLE_TASK")
	public WkLifeTask getWk() {
		return this.wk;
	}

	/**
	 * 方法: 设置WK
	 * 
	 * @param: WK 相关主表
	 */
	public void setWk(WkLifeTask wk) {
		this.wk = wk;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

}