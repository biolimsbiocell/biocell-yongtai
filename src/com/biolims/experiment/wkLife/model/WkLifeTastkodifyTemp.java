package com.biolims.experiment.wkLife.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.sample.model.SampleInfo;
import com.biolims.storage.position.model.StoragePosition;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;

/**
 * 
 * @ClassName: WkTastkodifyTemp
 * @Description: TODO(文库回退实体创建)
 * @author zhiqiang.yang@biolims.cn
 * @date 2017-8-3 上午10:04:21
 * 
 */
@Entity
@Table(name = "WK_LIFE_TAST_MODIFY_TEMP")
@SuppressWarnings("serial")
public class WkLifeTastkodifyTemp extends EntityDao<WkLifeTastkodifyTemp> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** name */
	private String name;
	/** 样本编号 */
	private String code;
	/** 拆分后的样本编号 */
	private String splitCode;
	/** 原始样本编号 */
	private String sampleCode;
	/** 患者姓名 */
	private String patientName;
	/** 检测项目 */
	private String productId;
	/** 检测项目 */
	private String productName;
	/** 取样日期 */
	private String inspectDate;
	/** 接收日期 */
	private Date acceptDate;
	/** 体积 */
	private Double volume;
	/** 单位 */
	private String unit;
	/** 身份证 (改为片段范围) */
	private String idCard;
	/** 检测方法 */
	private String sequenceFun;
	/** 应出报告日期 */
	private Date reportDate;
	/** 手机号 */
	private String phone;
	// 任务单
	private String orderId;
	/** 状态 */
	private String state;
	/** 备注 */
	private String note;
	// 区分临床还是科技服务 0 临床 1 科技服务
	private String classify;
	// 样本类型
	private String sampleType;
	// 样本数量
	private Double sampleNum;
	// 实验室样本号
	private String labCode;
	// RIN
	private String RIN;
	// 样本浓度
	private String concentration;
	/** 样本主数据 */
	private SampleInfo sampleInfo;
	// 科技服务
	private TechJkServiceTask techJkServiceTask;
	/** 行号 */
	private String rowCode;
	/** 列号 */
	private String colCode;
	/** 板号 */
	private String counts;
	// 是否质控品
	private String isZkp;
	// 科技服务明细
	private TechJkServiceTaskItem tjItem;
	// 负责人id
	private String userId;
	// 负责组id
	private String groupId;
	// 截止日期
	private Date sequenceBillDate;
	/** Qubit浓度 */
	private Double qbcontraction;
	/** 总量 */
	private Double sumVolume;
	/** 是否显示 */
	private String isdisplay;
	/** 处理结果 */
	private String result;
	/** 下一步流向ID */
	private String nextFlowId;
	/** 下一步流向 */
	private String nextFlow;
	/* 失败原因 */
	private String reason;
	/** index */
	private String indexa;
	// 扩增比例
	private Double pcrRatio;
	// PCR前总量
	private Double dxpdSumTotal;
	// 循环数
	private String loopNum;
	// 文库体积
	private Double wkVolume;
	// 文库总量
	private Double wkSumTotal;
	// 大小片段体积
	private Double dxpdVolume;
	// 插入大小(文库片段大小)
	private String insertSize;

	public String getInsertSize() {
		return insertSize;
	}

	public void setInsertSize(String insertSize) {
		this.insertSize = insertSize;
	}

	public Double getDxpdVolume() {
		return dxpdVolume;
	}

	public void setDxpdVolume(Double dxpdVolume) {
		this.dxpdVolume = dxpdVolume;
	}

	public Double getWkVolume() {
		return wkVolume;
	}

	public void setWkVolume(Double wkVolume) {
		this.wkVolume = wkVolume;
	}

	public Double getWkSumTotal() {
		return wkSumTotal;
	}

	public void setWkSumTotal(Double wkSumTotal) {
		this.wkSumTotal = wkSumTotal;
	}

	public String getLoopNum() {
		return loopNum;
	}

	public void setLoopNum(String loopNum) {
		this.loopNum = loopNum;
	}

	public Double getDxpdSumTotal() {
		return dxpdSumTotal;
	}

	public void setDxpdSumTotal(Double dxpdSumTotal) {
		this.dxpdSumTotal = dxpdSumTotal;
	}

	public Double getPcrRatio() {
		return pcrRatio;
	}

	public void setPcrRatio(Double pcrRatio) {
		this.pcrRatio = pcrRatio;
	}

	@Column(name = "INDEXA", length = 50)
	public String getIndexa() {
		return indexa;
	}

	public void setIndexa(String indexa) {
		this.indexa = indexa;
	}

	/**
	 * @return result
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getResult() {
		return result;
	}

	/**
	 * @param result
	 *            the result to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * @return nextFlowId
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getNextFlowId() {
		return nextFlowId;
	}

	/**
	 * @param nextFlowId
	 *            the nextFlowId to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setNextFlowId(String nextFlowId) {
		this.nextFlowId = nextFlowId;
	}

	/**
	 * @return nextFlow
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getNextFlow() {
		return nextFlow;
	}

	/**
	 * @param nextFlow
	 *            the nextFlow to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	/**
	 * @return reason
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * @param reason
	 *            the reason to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	@Column(name = "ISDISPLAY", length = 1)
	public String getIsdisplay() {
		return isdisplay;
	}

	public void setIsdisplay(String isdisplay) {
		this.isdisplay = isdisplay;
	}

	public Date getSequenceBillDate() {
		return sequenceBillDate;
	}

	public void setSequenceBillDate(Date sequenceBillDate) {
		this.sequenceBillDate = sequenceBillDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "TJ_ITEM")
	public TechJkServiceTaskItem getTjItem() {
		return tjItem;
	}

	public void setTjItem(TechJkServiceTaskItem tjItem) {
		this.tjItem = tjItem;
	}

	public String getIsZkp() {
		return isZkp;
	}

	public void setIsZkp(String isZkp) {
		this.isZkp = isZkp;
	}

	public String getRowCode() {
		return rowCode;
	}

	public void setRowCode(String rowCode) {
		this.rowCode = rowCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColCode() {
		return colCode;
	}

	public void setColCode(String colCode) {
		this.colCode = colCode;
	}

	public String getCounts() {
		return counts;
	}

	public void setCounts(String counts) {
		this.counts = counts;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "TECH_JK_SERVICE_TASK")
	public TechJkServiceTask getTechJkServiceTask() {
		return techJkServiceTask;
	}

	public void setTechJkServiceTask(TechJkServiceTask techJkServiceTask) {
		this.techJkServiceTask = techJkServiceTask;
	}

	/**
	 * 方法: 取得SampleInfo
	 * 
	 * @return: 样本主数据
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "SAMPLE_INFO")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}

	/**
	 * 方法: 设置SampleInfo
	 * 
	 * @param: 样本主数据
	 */
	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}

	public String getRIN() {
		return RIN;
	}

	public void setRIN(String rIN) {
		RIN = rIN;
	}

	public String getConcentration() {
		return concentration;
	}

	public void setConcentration(String concentration) {
		this.concentration = concentration;
	}

	public String getLabCode() {
		return labCode;
	}

	public void setLabCode(String labCode) {
		this.labCode = labCode;
	}

	public Double getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(Double sampleNum) {
		this.sampleNum = sampleNum;
	}

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "CODE", length = 20)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "SAMPLE_CODE", length = 20)
	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	@Column(name = "UNIT", length = 20)
	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	@Column(name = "ID_CARD", length = 50)
	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	@Column(name = "SEQUENCE_FUN", length = 50)
	public String getSequenceFun() {
		return sequenceFun;
	}

	public void setSequenceFun(String sequenceFun) {
		this.sequenceFun = sequenceFun;
	}

	@Column(name = "REPORT_DATE", length = 50)
	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	@Column(name = "VOLUME", length = 20)
	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	@Column(name = "STATE", length = 20)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "NOTE", length = 20)
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	@Column(name = "PATIENT_NAME", length = 20)
	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	@Column(name = "PRODUCT_ID", length = 20)
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Column(name = "PRODUCT_NAME", length = 20)
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name = "INSPECT_DATE", length = 20)
	public String getInspectDate() {
		return inspectDate;
	}

	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}

	@Column(name = "ACCEPT_DATE", length = 20)
	public Date getAcceptDate() {
		return acceptDate;
	}

	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

	public Double getQbcontraction() {
		return qbcontraction;
	}

	public void setQbcontraction(Double qbcontraction) {
		this.qbcontraction = qbcontraction;
	}

	public Double getSumVolume() {
		return sumVolume;
	}

	public void setSumVolume(Double sumVolume) {
		this.sumVolume = sumVolume;
	}

	public String getSplitCode() {
		return splitCode;
	}

	public void setSplitCode(String splitCode) {
		this.splitCode = splitCode;
	}
}