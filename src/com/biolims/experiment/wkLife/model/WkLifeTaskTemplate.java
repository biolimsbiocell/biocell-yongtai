package com.biolims.experiment.wkLife.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 执行步骤
 * @author lims-platform
 * @date 2015-11-29 19:25:57
 * @version V1.0   
 *
 */
@Entity
@Table(name = "WK_LIFE_TASK_TEMPLATE")
@SuppressWarnings("serial")
public class WkLifeTaskTemplate extends EntityDao<WkLifeTaskTemplate> implements java.io.Serializable {
	/**id*/
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	private String id;
	/**描述*/
	private String name;
	// 实验员
	private String testUserId;
	private String testUserName;
	/**模板步骤Id*/
	private String tItem;
	/**步骤名称*/
	private String stepName;
	/**步骤编号*/
	private Integer code;
	/**开始时间*/
	private String startTime;
	/**结束时间*/
	private String endTime;
	/**状态*/
	private String state;
	/**备注*/
	private String note;
	/*关联样本*/
	private String sampleCodes;
	/**相关主表*/
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "WK_SAMPLE_TASK")
	private WkLifeTask wk;
	/**
	 *方法: 取得String
	 *@return: String  id
	 */
	
	
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}	
	
	/**
	 * @return the testUserId
	 */
	@Column(name ="TEST_USER_ID", length = 50)
	public String getTestUserId() {
		return testUserId;
	}
	/**
	 * @param testUserId the testUserId to set
	 */
	public void setTestUserId(String testUserId) {
		this.testUserId = testUserId;
	}
	/**
	 * @return the testUserName
	 */
	@Column(name ="TEST_USER_NAME", length = 50)
	public String getTestUserName() {
		return testUserName;
	}
	/**
	 * @param testUserName the testUserName to set
	 */
	public void setTestUserName(String testUserName) {
		this.testUserName = testUserName;
	}
	/**
	 *方法: 取得String
	 *@return: String  步骤编号
	 */
	@Column(name ="T_ITEM_NEW", length = 50)
	public String gettItem() {
		return tItem;
	}
	/**
	 *方法: 设置String
	 *@param: String  步骤编号
	 */
	public void settItem(String tItem) {
		this.tItem = tItem;
	}
	/**
	 *方法: 取得String
	 *@return: String  步骤名称
	 */
	@Column(name ="STEP_NAME", length = 50)
	public String getStepName(){
		return this.stepName;
	}
	/**
	 *方法: 设置String
	 *@param: String  步骤名称
	 */
	public void setStepName(String stepName){
		this.stepName = stepName;
	}
	
	
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  开始时间
	 */
	@Column(name ="START_TIME", length = 50)
	public String getStartTime(){
		return this.startTime;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  开始时间
	 */
	public void setStartTime(String startTime){
		this.startTime = startTime;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  结束时间
	 */
	@Column(name ="END_TIME", length = 50)
	public String getEndTime(){
		return this.endTime;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  结束时间
	 */
	public void setEndTime(String endTime){
		this.endTime = endTime;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE",  columnDefinition="clob")
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	@Column(name ="SAMPLE_CODES", length = 5000)
	public String getSampleCodes() {
		return sampleCodes;
	}
	public void setSampleCodes(String sampleCodes) {
		this.sampleCodes = sampleCodes;
	}
	/**
	 *方法: 取得WK
	 *@return: WK  相关主表
	 */
	
	public WkLifeTask getWk(){
		return this.wk;
	}
	/**
	 *方法: 设置WK
	 *@param: WK  相关主表
	 */
	public void setWk(WkLifeTask wk){
		this.wk = wk;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	
}