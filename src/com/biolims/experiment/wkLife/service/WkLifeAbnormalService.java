package com.biolims.experiment.wkLife.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.pooling.model.PoolingTemp;
import com.biolims.experiment.qc.model.Qc2100TaskTemp;
import com.biolims.experiment.qc.model.QcQpcrTaskTemp;
import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTaskTemp;
import com.biolims.experiment.wkLife.dao.WKLifeSampleTaskDao;
import com.biolims.experiment.wkLife.dao.WkLifeAbnormalDao;
import com.biolims.experiment.wkLife.model.SampleWkLifeInfo;
import com.biolims.experiment.wkLife.model.WkLifeAbnormalBack;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.storage.dao.SampleInDao;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.product.model.Product;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class WkLifeAbnormalService {
	@Resource
	private WkLifeAbnormalDao wkLifeAbnormalDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInDao sampleInDao;
	@Resource
	private WKLifeSampleTaskDao wKLifeSampleTaskDao;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	StringBuffer json = new StringBuffer();

	/**
	 * 查询文库异常
	 * 
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 */
	public Map<String, Object> selectWkAbnormalList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return wkLifeAbnormalDao.selectWkAbnormalList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	public Map<String, Object> findWkAbnormalBack(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return wkLifeAbnormalDao.selectWkAbnormalBack(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	// 查找异常质控品信息
	public Map<String, Object> findQualityProduct(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return wkLifeAbnormalDao.selectQualityProductList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	public WkLifeAbnormalBack get(String id) {
		WkLifeAbnormalBack wkAbnormal = commonDAO.get(WkLifeAbnormalBack.class, id);
		return wkAbnormal;
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWkAbnormalBack(String[] ids) throws Exception {
		for (String id : ids) {
			WkLifeAbnormalBack scp = wkLifeAbnormalDao.get(WkLifeAbnormalBack.class, id);
			wkLifeAbnormalDao.delete(scp);
		}
	}

	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(WkLifeAbnormalBack sc, Map jsonMap) throws Exception {
		if (sc != null) {
			wkLifeAbnormalDao.saveOrUpdate(sc);
		}
	}

	// 保存文库异常
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWkAbnormalBack(String itemDataJson) throws Exception {
		List<WkLifeAbnormalBack> saveItems = new ArrayList<WkLifeAbnormalBack>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			WkLifeAbnormalBack sbi = new WkLifeAbnormalBack();
			sbi = (WkLifeAbnormalBack) wkLifeAbnormalDao.Map2Bean(map, sbi);
			if (sbi.getId() != null && sbi.getId().equals(""))
				sbi.setId(null);

			if (sbi != null && !sbi.getIsExecute().equals("")
					&& !sbi.getResult().equals("")) {
				if (sbi.getIsExecute().equals("1")) {// 确认执行
					if (sbi.getResult().equals("1")) {// 合格
						SampleWkLifeInfo scp = this.commonDAO.get(
								SampleWkLifeInfo.class, sbi.getTempId());
						if (sbi.getNextFlowId().equals("0009")) {// 样本入库
							SampleInItemTemp st = new SampleInItemTemp();
							st.setCode(scp.getCode());
							st.setSampleCode(scp.getSampleCode());
							st.setSampleType(scp.getDicSampleType().getName());
							st.setSampleTypeId(scp.getDicSampleType().getId());
							st.setInfoFrom("SampleWkInfo");
							st.setState("1");
							wKLifeSampleTaskDao.saveOrUpdate(st);
						} else if (sbi.getNextFlowId().equals("0012")) {// 暂停
						} else if (sbi.getNextFlowId().equals("0013")) {// 终止
						} else if (sbi.getNextFlowId().equals("0014")) {// 反馈项目组
						} else if (sbi.getNextFlowId().equals("0020")) {// 2100质控
							Qc2100TaskTemp wqt = new Qc2100TaskTemp();
							sbi.setState("1");
							sampleInputService.copy(wqt, sbi);
							wqt.setWkType("2");
							wKLifeSampleTaskDao.saveOrUpdate(wqt);
						} else if (sbi.getNextFlowId().equals("0021")) {// QPCR质控
							QcQpcrTaskTemp wkqt = new QcQpcrTaskTemp();
							sbi.setState("1");
							sampleInputService.copy(wkqt, sbi);
							wkqt.setWkType("1");
							wKLifeSampleTaskDao.saveOrUpdate(wkqt);
						} else if (sbi.getNextFlowId().equals("0067")) {// 文库混合
							WkLifeBlendTaskTemp q = new WkLifeBlendTaskTemp();
							sampleInputService.copy(q, scp);
							if (scp.getTechJkServiceTask() != null) {
								TechJkServiceTask t = this.commonDAO
										.get(TechJkServiceTask.class, scp
												.getTechJkServiceTask().getId());
								q.setCxlx(t.getSequencingType());
								q.setCxdc(t.getSequenceLength());
								q.setCxpt(t.getSequencePlatform());
							} else {
								Product p = this.commonDAO.get(Product.class,
										scp.getProductId());
								q.setCxlx(p.getSequenceType());
								q.setCxdc(p.getSequenceLength());
								q.setCxpt(p.getSequencePlatform());
							}
							q.setFjwk(scp.getCode());
							q.setSjfz(scp.getSampleCode());
							if (scp.getExpectNum() != null) {

								q.setTl(scp.getExpectNum().toString());
							}
							if (scp.getWkConcentration() != null
									&& !"".equals(scp.getWkConcentration())) {
								q.setConcentration(scp.getWkConcentration()
										.toString());
							}
							if (scp.getWkVolume() != null
									&& !"".equals(scp.getWkVolume())) {
								q.setVolume(scp.getWkVolume().toString());
							}
							if (scp.getWkSumTotal() != null
									&& !"".equals(scp.getWkSumTotal())) {
								q.setSumTotal(scp.getWkSumTotal().toString());
							}
							q.setState("1");
							wKLifeSampleTaskDao.saveOrUpdate(q);
						} else if (sbi.getNextFlowId().equals("0015")) {// 富集
							PoolingTemp ptt = new PoolingTemp();
							sbi.setState("1");
							sampleInputService.copy(ptt, scp);
//							ptt.setWkIndex(scp.getIndexa());
							wKLifeSampleTaskDao.saveOrUpdate(ptt);

						} else {
							// 得到下一步流向的相关表单
							List<NextFlow> list_nextFlow = nextFlowDao
									.seletNextFlowById(sbi.getNextFlowId());
							for (NextFlow n : list_nextFlow) {
								Object o = Class.forName(
										n.getApplicationTypeTable()
												.getClassPath()).newInstance();
								sbi.setState("1");
								sampleInputService.copy(o, sbi);

							}
						}
						sbi.setState("2");

					}
				}
			}
			saveItems.add(sbi);
		}
		wkLifeAbnormalDao.saveOrUpdateAll(saveItems);
	}

}
