package com.biolims.experiment.wkLife.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.dao.CommonDAO;

import com.biolims.experiment.wkLife.dao.NewQualityProductCheckDao;
import com.biolims.experiment.wkLife.model.WkLifeTaskItem;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;

import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class NewQualityProductCheckService {
	@Resource
	private NewQualityProductCheckDao qualityProductCheckDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findQualityProductList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return qualityProductCheckDao.selectQualityProductList(mapForQuery, startNum, limitNum, dir, sort);
	}
	
	/**
	 * 保存
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveQualityProduct(WkLifeTaskItem taskId, String itemDataJson) throws Exception {
		List<WkLifeTaskItem> saveItems = new ArrayList<WkLifeTaskItem>();
		List<Map> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map map : list) {
			WkLifeTaskItem sbti = new WkLifeTaskItem();
			// 将map信息读入实体类
			sbti = (WkLifeTaskItem) qualityProductCheckDao.Map2Bean(map, sbti);
			if(sbti.getResult()!=null&&!sbti.getResult().equals("")){
				if(sbti.getResult().equals("0"))
				//如果处理结果为不合格，显示到异常界面
				sbti.setState("3");
				
			}
			qualityProductCheckDao.saveOrUpdate(sbti);
		}

	}
}
