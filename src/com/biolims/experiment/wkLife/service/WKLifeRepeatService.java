package com.biolims.experiment.wkLife.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.plasma.dao.BloodSplitDao;
import com.biolims.experiment.plasma.model.SamplePlasmaInfo;
import com.biolims.experiment.qc.model.QcReceiveTemp;
import com.biolims.experiment.qc.model.QcQpcrTaskTemp;
import com.biolims.experiment.qc.model.Qc2100TaskTemp;



import com.biolims.experiment.wkLife.dao.WKLifeRepeatDao;
import com.biolims.experiment.wkLife.model.WkLifeAbnormalBack;
import com.biolims.experiment.wkLife.model.WkLifeTaskTemp;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.project.feedback.dao.WkFeedbackDao;
import com.biolims.project.feedback.model.FeedbackWk;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.storage.dao.SampleInDao;
import com.biolims.sample.storage.model.SampleInItem;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.sample.storage.model.SampleOutTemp;
import com.biolims.storage.container.model.StorageContainer;
import com.biolims.system.template.model.CosItem;
import com.biolims.system.template.model.Template;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class WKLifeRepeatService {

	@Resource
	private WKLifeRepeatDao wKLifeRepeatDao;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private BloodSplitDao bloodSplitDao;
	@Resource
	private SampleInDao sampleInDao;
	@Resource
	private SampleInputService sampleInputService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findSampleWkInfo(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return wKLifeRepeatDao.selectSampleWkInfo(mapForQuery, startNum, limitNum,
				dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWkRepeat(String itemDataJson) throws Exception {
		List<WkLifeAbnormalBack> saveItems = new ArrayList<WkLifeAbnormalBack>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			WkLifeAbnormalBack sbti = new WkLifeAbnormalBack();
			// 将map信息读入实体类
			sbti = (WkLifeAbnormalBack) wKLifeRepeatDao.Map2Bean(map, sbti);
			if (sbti.getId() != null && sbti.getId().equals(""))
				sbti.setId(null);

			if (sbti != null && sbti.getResult() != null
					&& sbti.getIsExecute() != null) {
				if (sbti.getIsExecute().equals("1")) {
					if (sbti.getResult().equals("1")) {
						WkLifeTaskTemp wtt = new WkLifeTaskTemp();
						sbti.setState("1");
						sampleInputService.copy(wtt, sbti);
						wKLifeRepeatDao.saveOrUpdate(sbti);
					} else {
						sbti.setState("2");
						wKLifeRepeatDao.saveOrUpdate(sbti);
					}
				}
			}
			saveItems.add(sbti);
		}
		wKLifeRepeatDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 按编号循环出库yq
	 * 
	 * @param scode
	 */
	public void setOutStorage(String scode) {
		for (int i = 0; i < 4; i++) {
			if (i == 0) {
				SampleInItem sit = sampleInDao.getSampleInItemByMark(scode,
						"-2");
				if (sit != null) {
					SampleOutTemp sot = new SampleOutTemp();
					sot.setCode(sit.getCode());
					sot.setSampleCode(sit.getSampleCode());
					sot.setMethod("重建库");
					sot.setState("1");
					commonDAO.saveOrUpdate(sot);
					sit.setState("2");
					// sit.setNum(sit.getNum()-400);
					break;
				}
			} else if (i == 1) {
				SampleInItem sit = sampleInDao.getSampleInItemByMark(scode,
						"-3");
				if (sit != null) {
					SampleOutTemp sot = new SampleOutTemp();
					sot.setCode(sit.getCode());
					sot.setSampleCode(sit.getSampleCode());
					sot.setMethod("重建库");
					sot.setState("1");
					commonDAO.saveOrUpdate(sot);
					sit.setState("2");
					// sit.setNum(sit.getNum()-400);
					break;
				}
			} else if (i == 2) {
				SampleInItem sit = sampleInDao.getSampleInItemByMark(scode,
						"-2");
				if (sit != null) {
					SampleOutTemp sot = new SampleOutTemp();
					sot.setCode(sit.getCode());
					sot.setSampleCode(sit.getSampleCode());
					sot.setMethod("重建库");
					sot.setState("1");
					commonDAO.saveOrUpdate(sot);
					sit.setState("2");
					// sit.setNum(sit.getNum()-400);
					break;
				}
			} else {
				SampleInItem sit = sampleInDao.getSampleInItemByMark(scode,
						"-3");
				if (sit != null) {
					SampleOutTemp sot = new SampleOutTemp();
					sot.setCode(sit.getCode());
					sot.setSampleCode(sit.getSampleCode());
					sot.setMethod("重建库");
					sot.setState("1");
					commonDAO.saveOrUpdate(sot);
					sit.setState("2");
					// sit.setNum(sit.getNum()-400);
					break;
				}
			}
		}
	}
}
