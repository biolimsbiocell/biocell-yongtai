package com.biolims.experiment.wkLife.service;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.equipment.model.Instrument;
import com.biolims.experiment.dna.model.DnaTaskAbnormal;
import com.biolims.experiment.plasma.model.PlasmaAbnormal;
import com.biolims.experiment.pooling.model.PoolingTemp;
import com.biolims.experiment.qc.model.Qc2100TaskTemp;
import com.biolims.experiment.qc.model.QcQpcrTaskTemp;
import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTaskTemp;
import com.biolims.experiment.wkLife.dao.WKLifeReceiveDao;
import com.biolims.experiment.wkLife.dao.WKLifeSampleTaskDao;
import com.biolims.experiment.wkLife.dao.WkLifeAbnormalDao;
import com.biolims.experiment.wkLife.dao.WkLifeManageDao;
import com.biolims.experiment.wkLife.model.SampleWkLifeInfo;
import com.biolims.experiment.wkLife.model.SampleWkLifeInfoCtDNA;
import com.biolims.experiment.wkLife.model.SampleWkLifeInfomRNA;
import com.biolims.experiment.wkLife.model.SampleWkLifeInforRNA;
import com.biolims.experiment.wkLife.model.WkLifeAbnormalBack;
import com.biolims.experiment.wkLife.model.WkLifeTask;
import com.biolims.experiment.wkLife.model.WkLifeTaskCos;
import com.biolims.experiment.wkLife.model.WkLifeTaskItem;
import com.biolims.experiment.wkLife.model.WkLifeTaskModify;
import com.biolims.experiment.wkLife.model.WkLifeTaskModifyInfo;
import com.biolims.experiment.wkLife.model.WkLifeTaskReagent;
import com.biolims.experiment.wkLife.model.WkLifeTaskTemp;
import com.biolims.experiment.wkLife.model.WkLifeTaskTemplate;
import com.biolims.experiment.wkLife.model.WkLifeTastkodifyTemp;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.project.feedback.dao.WkFeedbackDao;
import com.biolims.project.feedback.model.FeedbackWk;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.dao.SampleInDao;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.storage.container.model.StorageContainer;
import com.biolims.storage.main.dao.StorageDao;
import com.biolims.storage.main.service.StorageService;
import com.biolims.storage.model.Storage;
import com.biolims.storage.model.StorageOutItem;
import com.biolims.storage.out.service.StorageOutService;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.product.model.Product;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.template.model.Template;
import com.biolims.system.work.dao.WorkOrderDao;
import com.biolims.system.work.model.WorkOrder;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class WKLifeSampleTaskService {
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private WkLifeAbnormalDao wkLifeAbnormalDao;
	@Resource
	private WkLifeManageDao wkLifeManageDao;
	@Resource
	private WkFeedbackDao wkFeedbackDao;
	@Resource
	private WKLifeReceiveDao wKLifeReceiveDao;
	@Resource
	private SampleInDao sampleInDao;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CommonService commonService;
	@Resource
	private WorkOrderDao workOrderDao;
	@Resource
	private WKLifeSampleTaskDao wKLifeSampleTaskDao;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private DicSampleTypeDao dicSampleTypeDao;
	@Resource
	private StorageService storageService;
	@Resource
	private StorageOutService storageOutService;
	@Resource
	private StorageDao storageDao;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findWKList(Map<String, String> mapForQuery,
			Integer startNum, Integer limitNum, String dir, String sort) {
		return wKLifeSampleTaskDao.selectWKList(mapForQuery, startNum, limitNum,
				dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(WkLifeTask i) throws Exception {

		wKLifeSampleTaskDao.saveOrUpdate(i);

	}

	public WkLifeTask get(String id) {
		WkLifeTask wk = commonDAO.get(WkLifeTask.class, id);
		return wk;
	}

	public Map<String, Object> findWKItemList(String scId, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = wKLifeSampleTaskDao.selectWKItemList(scId,
				startNum, limitNum, dir, sort);
		List<WkLifeTaskItem> list = (List<WkLifeTaskItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findWKTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = wKLifeSampleTaskDao.selectWKTemplateList(scId,
				startNum, limitNum, dir, sort);
		List<WkLifeTaskTemplate> list = (List<WkLifeTaskTemplate>) result.get("list");
		return result;
	}

	public Map<String, Object> findWKReagentList(String scId, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = wKLifeSampleTaskDao.selectWKReagentList(scId,
				startNum, limitNum, dir, sort);
		List<WkLifeTaskReagent> list = (List<WkLifeTaskReagent>) result.get("list");
		return result;
	}

	// 根据关联item查询
	public Map<String, Object> findReagentItemListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		Map<String, Object> result = wKLifeSampleTaskDao
				.selectReagentItemListByItemId(scId, startNum, limitNum, dir,
						sort, itemId);
		List<WkLifeTaskReagent> list = (List<WkLifeTaskReagent>) result.get("list");
		return result;
	}

	public Map<String, Object> findCosItemListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		Map<String, Object> result = wKLifeSampleTaskDao.selectCosItemListByItemId(
				scId, startNum, limitNum, dir, sort, itemId);
		List<WkLifeTaskCos> list = (List<WkLifeTaskCos>) result.get("list");
		return result;
	}

	public Map<String, Object> findWKCosList(String scId, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = wKLifeSampleTaskDao.selectWKCosList(scId,
				startNum, limitNum, dir, sort);
		List<WkLifeTaskCos> list = (List<WkLifeTaskCos>) result.get("list");
		return result;
	}

	public Map<String, Object> findWKResultList(String scId, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = wKLifeSampleTaskDao.selectWKResultList(scId,
				startNum, limitNum, dir, sort);
		List<SampleWkLifeInfo> list = (List<SampleWkLifeInfo>) result.get("list");
		return result;
	}

	/**
	 * 捕获查看文库信息
	 * 
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> showWKSplitInfoList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String id, String codes)
			throws Exception {
		// 获取Pooling明细d的文库编号集合
		List<String> list1 = wKLifeSampleTaskDao.getPoolingTaskItemList(id, codes);
		String wkCodes = "";
		if (list1.size() > 0) {
			for (String str : list1) {
				wkCodes += "'" + str + "',";
			}
		}
		wkCodes = wkCodes.substring(0, wkCodes.length() - 1);
		Map<String, Object> result = wKLifeSampleTaskDao.selectWKInfoList(
				mapForQuery, startNum, limitNum, dir, sort, wkCodes);
		List<SampleWkLifeInfo> list = (List<SampleWkLifeInfo>) result.get("list");
		return result;
	}

	public Map<String, Object> findWKResultCtDNAList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = wKLifeSampleTaskDao.selectWKResultCtDNAList(
				scId, startNum, limitNum, dir, sort);
		List<SampleWkLifeInfo> list = (List<SampleWkLifeInfo>) result.get("list");
		return result;
	}

	public Map<String, Object> findWKResultrRNAList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = wKLifeSampleTaskDao.selectWKResultrRNAList(
				scId, startNum, limitNum, dir, sort);
		List<SampleWkLifeInfo> list = (List<SampleWkLifeInfo>) result.get("list");
		return result;
	}

	public Map<String, Object> findWKResultmRNAList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = wKLifeSampleTaskDao.selectWKResultmRNAList(
				scId, startNum, limitNum, dir, sort);
		List<SampleWkLifeInfo> list = (List<SampleWkLifeInfo>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWKItem(WkLifeTask sc, String itemDataJson) throws Exception {
		List<WkLifeTaskItem> saveItems = new ArrayList<WkLifeTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);

		Template temp = null;
		StorageContainer storageContainer = null;
		Integer rowCode = null;
		Integer colCode = null;
		Integer maxNum = null;
		String counts = "";
		if (sc.getTemplate() != null && !sc.getTemplate().equals("")) {
			temp = wKLifeSampleTaskDao
					.get(Template.class, sc.getTemplate().getId());
			if (temp.getStorageContainer() != null
					&& !temp.getStorageContainer().equals("")) {
				storageContainer = wKLifeSampleTaskDao.get(StorageContainer.class,
						temp.getStorageContainer().getId());
				rowCode = storageContainer.getRowNum();
				colCode = storageContainer.getColNum();

				// counts = codingRuleService.get("WkTaskItem", "JK", 000000, 6,
				// null, "counts");
				//
				counts = codingRuleService.get("counts", "WkTaskItem", "JK",
						00, 2, null);
			}
		}

		for (Map<String, Object> map : list) {
			WkLifeTaskItem scp = new WkLifeTaskItem();
			// 将map信息读入实体类
			scp = (WkLifeTaskItem) wKLifeSampleTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			if (scp.getOrderNumber() != null
					&& !scp.getOrderNumber().equals("")) {
				if (rowCode != null && colCode != null) {
					Double num = Math.floor((scp.getOrderNumber() - 1)
							/ rowCode) + 1;
					scp.setColCode(Integer.toString(num.intValue()));
					if (scp.getOrderNumber() % rowCode == 0) {
						scp.setRowCode(String.valueOf((char) (64 + rowCode)));
					} else {
						scp.setRowCode(String.valueOf((char) (64 + scp
								.getOrderNumber() % rowCode)));
					}
					// scp.setCounts(String.valueOf((scp.getOrderNumber() - 1)
					// / (rowCode * colCode) + 1));
					scp.setCounts(counts);
				}
			}

			// 判断科技任务单明细不为空时赋值通量
			if (scp.getTjItem() != null && !"".equals(scp.getTjItem().getId())) {
				TechJkServiceTaskItem jkItem = this.commonDAO.get(
						TechJkServiceTaskItem.class, scp.getTjItem().getId());
				if (jkItem.getDataNum() != null) {

					scp.setExpectNum(Double.parseDouble(jkItem.getDataNum()));
				}
			}
			scp.setWk(sc);
			Template t = templateDao.get(Template.class, sc.getTemplate()
					.getId());
			if (t.getSampleNum() != null) {
				if (scp.getSampleConsume() == null) {
					scp.setSampleConsume(t.getSampleNum());
				}
			}
			saveItems.add(scp);
			DecimalFormat df = new DecimalFormat("######.000");
			//
			// // 根据文库类型自动计算取样量
			// if (scp.getTakeNum() == null) {
			// if (scp.getLibNum() != null) {
			// if (scp.getLibType() == "1") {
			// scp.setTakeNum("2");
			// } else if (scp.getLibNum() == "2" || scp.getLibNum() == "3"
			// || scp.getLibNum() == "4") {
			// scp.setTakeNum("10");
			// } else if (scp.getLibNum() == "5") {
			// scp.setTakeNum("20");
			// } else if (scp.getLibNum() == "6") {
			// scp.setTakeNum("30");
			// } else if (scp.getLibNum() == "7") {
			// scp.setTakeNum("50");
			// }
			// }
			//
			// }
			// // 根据文库类型自动计算打断体积
			// if (scp.getBreakNum() == null) {
			// if (scp.getLibNum() != null) {
			// if (scp.getLibType() == "1") {
			// scp.setBreakNum("100");
			// } else if (scp.getLibNum() == "2" || scp.getLibNum() == "3"
			// || scp.getLibNum() == "4") {
			// scp.setBreakNum("200");
			// } else if (scp.getLibNum() == "5") {
			// scp.setBreakNum("450");
			// } else if (scp.getLibNum() == "6") {
			// scp.setBreakNum("450");
			// } else if (scp.getLibNum() == "7") {
			// scp.setBreakNum("450");
			// }
			// }
			//
			// }
			//
			// // 自动计算取样体积
			// if (scp.getSampleVolume() == null) {
			// if (scp.getConcentration() != null) {
			// scp.setSampleVolume(String.valueOf(2000 / scp
			// .getConcentration()));
			// }
			// }
			// // 自动计算TE或H20体积
			// if (scp.getTeOrH20num() == null) {
			// scp.setTeOrH20num(String.valueOf(Double.valueOf(scp
			// .getBreakNum()) - Double.valueOf(scp.getSampleVolume())));
			// }

			// if (scp.getConcentration() != null
			// && !"".equals(scp.getConcentration())
			// && scp.getSampleConsume() != null
			// && !"".equals(scp.getSampleConsume())) {
			// Double a;
			// a = scp.getSampleConsume() / scp.getConcentration();
			// scp.setVolume(Double.valueOf(df.format(a)));
			// Double b;
			// if (scp.getBlx() != null && !"".equals(scp.getBlx())) {
			// b = scp.getBlx() - scp.getVolume();
			// scp.setAddVolume(Double.valueOf(df.format(b)));
			// }
			// }

			WkLifeTaskTemp tt = commonDAO.get(WkLifeTaskTemp.class, scp.getTempId());
			if (tt != null) {
				tt.setState("2");
			}
		}

		wKLifeSampleTaskDao.saveOrUpdateAll(saveItems);
		if (sc.getTemplate() != null && !sc.getTemplate().equals("")) {
			temp = wKLifeSampleTaskDao
					.get(Template.class, sc.getTemplate().getId());
			if (temp.getStorageContainer() != null
					&& !temp.getStorageContainer().equals("")) {
				maxNum = Integer.valueOf(wKLifeSampleTaskDao.selectCountMax(
						sc.getId()).toString());
				sc.setMaxNum((maxNum - 1) / (rowCode * colCode) + 1);
			}

		}

	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWKItem(String[] ids) throws Exception {
		for (String id : ids) {
			WkLifeTaskItem scp = wKLifeSampleTaskDao.get(WkLifeTaskItem.class, id);
			wKLifeSampleTaskDao.delete(scp);
			WkLifeTaskTemp tt = commonDAO.get(WkLifeTaskTemp.class, scp.getTempId());
			if (tt != null) {
				tt.setState("1");
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWKTemplate(WkLifeTask sc, String itemDataJson) throws Exception {
		List<WkLifeTaskTemplate> saveItems = new ArrayList<WkLifeTaskTemplate>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			WkLifeTaskTemplate scp = new WkLifeTaskTemplate();
			// 将map信息读入实体类
			scp = (WkLifeTaskTemplate) wKLifeSampleTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setWk(sc);

			saveItems.add(scp);
		}
		wKLifeSampleTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWKTemplate(String[] ids) throws Exception {
		for (String id : ids) {
			WkLifeTaskTemplate scp = wKLifeSampleTaskDao.get(WkLifeTaskTemplate.class, id);
			wKLifeSampleTaskDao.delete(scp);
		}
	}

	// ============================
	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWKTemplateOne(String ids) throws Exception {
		WkLifeTaskTemplate scp = wKLifeSampleTaskDao.get(WkLifeTaskTemplate.class, ids);
		wKLifeSampleTaskDao.delete(scp);
	}

	// ============================
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Integer saveWKReagent(WkLifeTask sc, String itemDataJson)
			throws Exception {
		List<WkLifeTaskItem> item = this.wKLifeSampleTaskDao.setWKItemToManage(sc
				.getId());
		Integer re = 0;
		int n = item.size();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			WkLifeTaskReagent scp = new WkLifeTaskReagent();
			// 将map信息读入实体类
			scp = (WkLifeTaskReagent) wKLifeSampleTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setWk(sc);
			scp.setSampleNum((double) n);
			// 用量 = 单个用量*数量
			if (scp != null && scp.getOneNum() != null
					&& scp.getSampleNum() != null) {
				scp.setNum(scp.getOneNum() * scp.getSampleNum());
			}
			if (scp.getSn() != null && !scp.getSn().equals("")) {
				StorageOutItem soi = storageOutService.findStorageOutItem(scp
						.getSn());
				if (soi != null) {
					if (scp.getCode().equals(soi.getStorage().getId())) {
						scp.setBatch(soi.getCode());
						scp.setExpireDate(soi.getExpireDate());
						re++;
					} else {
						scp.setSn("");
					}
				}
			}
			wKLifeSampleTaskDao.saveOrUpdate(scp);
		}
		return n;
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWKReagent(String[] ids) throws Exception {
		for (String id : ids) {
			WkLifeTaskReagent scp = wKLifeSampleTaskDao.get(WkLifeTaskReagent.class, id);
			wKLifeSampleTaskDao.delete(scp);
		}
	}

	// =========================delWKReagentOne
	/**
	 * 根据ID删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWKReagentOne(String ids) throws Exception {
		WkLifeTaskReagent scp = wKLifeSampleTaskDao.get(WkLifeTaskReagent.class, ids);
		wKLifeSampleTaskDao.delete(scp);
	}

	// =========================

	/**
	 * 审批完成
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String id)
			throws Exception {
		WkLifeTask wk = wKLifeSampleTaskDao.get(WkLifeTask.class, id);
		wk.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		wk.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);

		wKLifeSampleTaskDao.update(wk);

		// 将批次信息反馈到模板中
		List<WkLifeTaskReagent> list1 = wKLifeSampleTaskDao.setReagentList(wk.getId());
		for (WkLifeTaskReagent dt : list1) {
			String bat1 = dt.getBatch(); // 血浆分离实验中试剂的批次
			String drid = dt.gettReagent(); // 血浆分离实验中保存的试剂ID
			List<ReagentItem> list2 = templateDao.setReagentListById(drid);
			for (ReagentItem ri : list2) {
				if (bat1 != null) {
					ri.setBatch(bat1);
				}
			}
			// 改变库存主数据试剂数量
			this.storageService.UpdateStorageAndItemNum(dt.getCode(),
					dt.getBatch(), dt.getNum());
		}
		// 改变文库构建临时表中的状态
		List<WkLifeTaskItem> item = wKLifeReceiveDao.findWkTaskItemmList(wk.getId());
		for (WkLifeTaskItem wri : item) {
			// List<WkTaskTemp> temp =
			// wKLifeReceiveDao.findWKTempList(wri.getCode());
			// if (temp.size() > 0) {
			// for (WkTaskTemp wtt : temp) {
			// wtt.setState("2");
			// }
			// }
			WkLifeTaskTemp temp = commonDAO.get(WkLifeTaskTemp.class, wri.getTempId());
			if (temp != null) {
				temp.setState("2");
			}
		}
		// if (wk.getType().equals("0")) {
		// submitSample(id, null);
		// } else if (wk.getType().equals("1")) {
		// submitSampleCtDNA(id, null);
		// } else if (wk.getType().equals("3")) {
		// submitSamplemRNA(id, null);
		// } else if (wk.getType().equals("2")) {
		// submitSamplerRNA(id, null);
		// }
		submitSample(id, null);
//		submitSampleCtDNA(id, null);
//		submitSamplemRNA(id, null);
//		submitSamplerRNA(id, null);
	}
	
	/**
	 * 审批完成
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState1(String applicationTypeActionId, String id)
			throws Exception {
		WkLifeTaskModify wk = wKLifeSampleTaskDao.get(WkLifeTaskModify.class, id);
		wk.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		wk.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);

		wKLifeSampleTaskDao.update(wk);
		submitSampleModify(id, null);
	}


	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Integer saveWKCos(WkLifeTask sc, String itemDataJson) throws Exception {
		Integer saveItem = 0;
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			WkLifeTaskCos scp = new WkLifeTaskCos();
			// 将map信息读入实体类
			scp = (WkLifeTaskCos) wKLifeSampleTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			Instrument ins = null;
			if (scp.getCode() != null && !scp.equals("")) {
				ins = wKLifeSampleTaskDao.get(Instrument.class, scp.getCode());
			}
			if (ins != null
					&& ins.getType().getId().equals(scp.getType().getId())) {
				scp.setName(ins.getName());
				scp.setState(ins.getState().getName());
				saveItem++;
			} else {
				scp.setName("");
				scp.setCode("");
				scp.setState("");
			}
			scp.setWk(sc);
			wKLifeSampleTaskDao.saveOrUpdate(scp);
		}
		return saveItem;
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWKCos(String[] ids) throws Exception {
		for (String id : ids) {
			WkLifeTaskCos scp = wKLifeSampleTaskDao.get(WkLifeTaskCos.class, id);
			wKLifeSampleTaskDao.delete(scp);
		}
	}

	// ========================
	/**
	 * 根据ID删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWKCosOne(String ids) throws Exception {
		WkLifeTaskCos scp = wKLifeSampleTaskDao.get(WkLifeTaskCos.class, ids);
		wKLifeSampleTaskDao.delete(scp);
	}

	// 文库构建
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWKResult(WkLifeTask sc, String itemDataJson) {

		List<SampleWkLifeInfo> saveItems = new ArrayList<SampleWkLifeInfo>();
		List<Map<String, Object>> list;
		try {
			boolean flag = false;
			list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
			for (Map<String, Object> map : list) {
				SampleWkLifeInfo scp = new SampleWkLifeInfo();
				// 将map信息读入实体类
				scp = (SampleWkLifeInfo) wKLifeSampleTaskDao.Map2Bean(map, scp);
				if (scp.getId() != null && scp.getId().equals(""))
					scp.setId(null);
				// scp.setState("1");
				scp.setWk(sc);
				if (scp.getCode() == null || scp.getCode().equals("")) {
					String markCode = "";
					if (scp.getDicSampleType() != null
							|| !scp.getDicSampleType().equals("")) {
						DicSampleType d = commonDAO.get(DicSampleType.class,
								scp.getDicSampleType().getId());
						String[] sampleCode = scp.getSampleCode().split(",");
						if (sampleCode.length > 1) {
							String str = sampleCode[0].substring(0,
									sampleCode[0].length() - 4);
							for (int i = 0; i < sampleCode.length; i++) {
								str += sampleCode[i].substring(
										sampleCode[i].length() - 4,
										sampleCode[i].length());
							}
							markCode = str + d.getCode();
						} else {
							markCode = scp.getSampleCode() + d.getCode();
						}
						String code = codingRuleService.getCode("SampleWkLifeInfo",
								markCode, 00, 2, null);
						scp.setCode(code);
					}
				}
				if (scp.getChpsConcentration() != null
						&& scp.getChVolume() != null) {
					scp.setChSumTotal(scp.getChpsConcentration()
							* scp.getChVolume());
				}
				if (scp.getConcentration() != null && scp.getVolume() != null) {
					scp.setSumTotal(scp.getConcentration() * scp.getVolume());
				}
				if (scp.getDxpdConcentration() != null
						&& scp.getDxpdVolume() != null) {
					scp.setDxpdSumTotal(scp.getDxpdConcentration()
							* scp.getDxpdVolume());
				}
				if (scp.getWkConcentration() != null
						&& scp.getWkVolume() != null) {
					scp.setWkSumTotal(scp.getWkConcentration()
							* scp.getWkVolume());
				}
				if (scp.getWkSumTotal() != null
						&& scp.getDxpdSumTotal() != null) {
					scp.setPcrRatio(scp.getWkSumTotal() / scp.getDxpdSumTotal());
				}

				wKLifeSampleTaskDao.saveOrUpdate(scp);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// wKLifeSampleTaskDao.saveOrUpdateAll(saveItems);
	}

	// ctDNA文库构建
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWKResultCtDNA(WkLifeTask sc, String itemDataJson) {

		List<SampleWkLifeInfoCtDNA> saveItems = new ArrayList<SampleWkLifeInfoCtDNA>();
		List<Map<String, Object>> list;
		try {
			boolean flag = false;
			list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
			for (Map<String, Object> map : list) {
				SampleWkLifeInfoCtDNA scp = new SampleWkLifeInfoCtDNA();
				// 将map信息读入实体类
				scp = (SampleWkLifeInfoCtDNA) wKLifeSampleTaskDao.Map2Bean(map, scp);
				if (scp.getId() != null && scp.getId().equals(""))
					scp.setId(null);
				// scp.setState("1");
				scp.setWkTask(sc);
				if (scp.getCode() == null || scp.getCode().equals("")) {
					String markCode = "";
					markCode = scp.getSampleCode() + "CL";
					String code = codingRuleService.getCode(
							"SampleWkLifeInfoCtDNA", markCode, 00, 2, null);
					scp.setCode(code);
				}

				if (scp.getDpdConcentration() != null
						&& scp.getDpdVolume() != null) {
					scp.setDpdSumTotal(scp.getDpdConcentration()
							* scp.getDpdVolume());
					scp.setDpdHsl(scp.getDpdSumTotal() / scp.getTotalNum());
				}
				if (scp.getXpdConcentration() != null
						&& scp.getXpdVolume() != null) {
					scp.setXpdSumTotal(scp.getXpdConcentration()
							* scp.getXpdVolume());
					scp.setXpdHsl(scp.getXpdSumTotal() / scp.getTotalNum());
				}
				if (scp.getConcentration() != null && scp.getVolume() != null) {
					scp.setSumTotal(scp.getConcentration() * scp.getVolume());
				}
				if (scp.getWkConcentration() != null
						&& scp.getWkVolume() != null) {
					scp.setWkSumTotal(scp.getWkConcentration()
							* scp.getWkVolume());
				}
				if (scp.getSumTotal() != null && scp.getSumTotal() != null) {
					scp.setPcrRatio(scp.getWkSumTotal() / scp.getSumTotal());
				}
				wKLifeSampleTaskDao.saveOrUpdate(scp);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// wKLifeSampleTaskDao.saveOrUpdateAll(saveItems);
	}

	// rRNA去除+RNA-seq
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWKResultrRNA(WkLifeTask sc, String itemDataJson) {

		List<SampleWkLifeInforRNA> saveItems = new ArrayList<SampleWkLifeInforRNA>();
		List<Map<String, Object>> list;
		try {
			boolean flag = false;
			list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
			for (Map<String, Object> map : list) {
				SampleWkLifeInforRNA scp = new SampleWkLifeInforRNA();
				// 将map信息读入实体类
				scp = (SampleWkLifeInforRNA) wKLifeSampleTaskDao.Map2Bean(map, scp);
				if (scp.getId() != null && scp.getId().equals(""))
					scp.setId(null);
				// scp.setState("1");
				scp.setWkTask(sc);
				if (scp.getCode() == null || scp.getCode().equals("")) {
					String markCode = "";
					markCode = scp.getSampleCode() + "RL";
					String code = codingRuleService.getCode("SampleWkLifeInforRNA",
							markCode, 00, 2, null);
					scp.setCode(code);
				}

				if (scp.getNsConcentration() != null
						&& scp.getNsVolume() != null) {
					scp.setNsSumTotal(scp.getNsConcentration()
							* scp.getNsVolume());
				}
				if (scp.getQcConcentration() != null
						&& scp.getQcVolume() != null) {
					scp.setQcSumTotal(scp.getQcConcentration()
							* scp.getQcVolume());
				}
				if (scp.getSampleNum() != null) {
					scp.setQcYield(scp.getQcSumTotal() / scp.getSampleNum());
				}
				if (scp.getConcentration() != null && scp.getVolume() != null) {
					scp.setSumTotal(scp.getConcentration() * scp.getVolume());
				}
				if (scp.getWkConcentration() != null
						&& scp.getWkVolume() != null) {
					scp.setWkSumTotal(scp.getWkConcentration()
							* scp.getWkVolume());
					if (scp.getSumTotal() != null) {
						scp.setPcrRatio(scp.getWkSumTotal() / scp.getSumTotal());
					}
				}

				wKLifeSampleTaskDao.saveOrUpdate(scp);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// wKLifeSampleTaskDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWKResultmRNA(WkLifeTask sc, String itemDataJson) {

		List<SampleWkLifeInfomRNA> saveItems = new ArrayList<SampleWkLifeInfomRNA>();
		List<Map<String, Object>> list;
		try {
			boolean flag = false;
			list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
			for (Map<String, Object> map : list) {
				SampleWkLifeInfomRNA scp = new SampleWkLifeInfomRNA();
				// 将map信息读入实体类
				scp = (SampleWkLifeInfomRNA) wKLifeSampleTaskDao.Map2Bean(map, scp);
				if (scp.getId() != null && scp.getId().equals(""))
					scp.setId(null);
				// scp.setState("1");
				scp.setWkTask(sc);
				if (scp.getCode() == null || scp.getCode().equals("")) {
					String markCode = "";
					markCode = scp.getSampleCode() + "ML";
					String code = codingRuleService.getCode("SampleWkLifeInfomRNA",
							markCode, 00, 2, null);
					scp.setCode(code);
				}

				if (scp.getNsConcentration() != null
						&& scp.getNsVolume() != null) {
					scp.setNsSumTotal(scp.getNsConcentration()
							* scp.getNsVolume());
				}
				if (scp.getQcConcentration() != null
						&& scp.getQcVolume() != null) {
					scp.setQcSumTotal(scp.getQcConcentration()
							* scp.getQcVolume());
				}
				if (scp.getSampleNum() != null) {
					scp.setQcYield(scp.getQcSumTotal() / scp.getSampleNum());
				}
				if (scp.getConcentration() != null && scp.getVolume() != null) {
					scp.setSumTotal(scp.getConcentration() * scp.getVolume());
				}
				if (scp.getWkConcentration() != null
						&& scp.getWkVolume() != null) {
					scp.setWkSumTotal(scp.getWkConcentration()
							* scp.getWkVolume());
					if (scp.getSumTotal() != null) {
						scp.setPcrRatio(scp.getWkSumTotal() / scp.getSumTotal());
					}
				}

				wKLifeSampleTaskDao.saveOrUpdate(scp);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// wKLifeSampleTaskDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveQuality(String itemDataJson) throws Exception {
		List<WkLifeTaskTemp> saveItems = new ArrayList<WkLifeTaskTemp>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			WkLifeTaskTemp wkt = new WkLifeTaskTemp();
			wkt = (WkLifeTaskTemp) wKLifeSampleTaskDao.Map2Bean(map, wkt);
			if (wkt.getId() != null && wkt.getId().equals(""))
				wkt.setId(null);

			saveItems.add(wkt);
		}
		wKLifeSampleTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWKResult(String[] ids) throws Exception {
		for (String id : ids) {
			SampleWkLifeInfo scp = wKLifeSampleTaskDao.get(SampleWkLifeInfo.class, id);
			if ((scp.getSubmit() == null || (scp.getSubmit() != null && scp
					.getSubmit().equals(""))))
				wKLifeSampleTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWKResultc(String[] ids) throws Exception {
		for (String id : ids) {
			SampleWkLifeInfoCtDNA scp = wKLifeSampleTaskDao.get(
					SampleWkLifeInfoCtDNA.class, id);
			if ((scp.getSubmit() == null || (scp.getSubmit() != null && scp
					.getSubmit().equals(""))))
				wKLifeSampleTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWKResultr(String[] ids) throws Exception {
		for (String id : ids) {
			SampleWkLifeInforRNA scp = wKLifeSampleTaskDao.get(SampleWkLifeInforRNA.class,
					id);
			if ((scp.getSubmit() == null || (scp.getSubmit() != null && scp
					.getSubmit().equals(""))))
				wKLifeSampleTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWKResultm(String[] ids) throws Exception {
		for (String id : ids) {
			SampleWkLifeInfomRNA scp = wKLifeSampleTaskDao.get(SampleWkLifeInfomRNA.class,
					id);
			if ((scp.getSubmit() == null || (scp.getSubmit() != null && scp
					.getSubmit().equals(""))))
				wKLifeSampleTaskDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Map<String, Object> save(WkLifeTask sc, Map jsonMap) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer re = 0;
		Integer in = 0;
		if (sc != null) {
			wKLifeSampleTaskDao.saveOrUpdate(sc);
			Template tem = wKLifeSampleTaskDao.get(Template.class, sc.getTemplate()
					.getId());
			if (tem != null && tem.getQcNum() != null
					&& !tem.getQcNum().equals("")) {
				sc.setQcNum(tem.getQcNum().toString());
			}
			String jsonStr = "";
			jsonStr = (String) jsonMap.get("wKItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveWKItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("wKTemplate");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveWKTemplate(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("wKReagent");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				re = saveWKReagent(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("wKCos");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				in = saveWKCos(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("wKSampleInfo");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveWKResult(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("wKSampleInfoCtDNA");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveWKResultCtDNA(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("wKSampleInforRNA");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveWKResultrRNA(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("wKSampleInformRNA");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveWKResultmRNA(sc, jsonStr);
			}
		}
		map.put("equip", in);
		map.put("reagent", re);
		return map;
	}
	/**
	 * 
	* @Title: save 
	* @Description: TODO(文库构建取消主数据保存) 
	* @param @param sc
	* @param @param jsonMap
	* @param @return
	* @param @throws Exception    设定文件 
	* @return Map<String,Object>    返回类型 
	* @author  zhiqiang.yang@biolims.cn 
	* @date 2017-8-4 下午5:52:21 
	* @throws
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Map<String, Object> save(WkLifeTaskModify sc, Map jsonMap) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer re = 0;
		Integer in = 0;
		if (sc != null) {
			wKLifeSampleTaskDao.saveOrUpdate(sc);
			String jsonStr = "";
			jsonStr = (String) jsonMap.get("wKTastModifyItemJson");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				savewKTastModifyItem(sc, jsonStr);
			}
		}
		map.put("equip", in);
		map.put("reagent", re);
		return map;
	}
/**
 * 
* @Title: savewKTastModifyItem 
* @Description: TODO(保存文库构建取消明细信息) 
* @param @param sc
* @param @param itemDataJson
* @param @throws Exception    设定文件 
* @return void    返回类型 
* @author  zhiqiang.yang@biolims.cn 
* @date 2017-8-4 下午5:57:46 
* @throws
 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savewKTastModifyItem(WkLifeTaskModify sc, String itemDataJson) throws Exception {
		List<WkLifeTaskModifyInfo> saveItems = new ArrayList<WkLifeTaskModifyInfo>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);

		Template temp = null;
		StorageContainer storageContainer = null;
		Integer rowCode = null;
		Integer colCode = null;
		Integer maxNum = null;
		String counts = "";

		for (Map<String, Object> map : list) {
			WkLifeTaskModifyInfo scp = new WkLifeTaskModifyInfo();
			// 将map信息读入实体类
			scp = (WkLifeTaskModifyInfo) wKLifeSampleTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);

			// 判断科技任务单明细不为空时赋值通量
			if (scp.getTjItem() != null && !"".equals(scp.getTjItem().getId())) {
				TechJkServiceTaskItem jkItem = this.commonDAO.get(
						TechJkServiceTaskItem.class, scp.getTjItem().getId());
				if (jkItem.getDataNum() != null) {

					scp.setExpectNum(Double.parseDouble(jkItem.getDataNum()));
				}
			}
			scp.setWk(sc);
			saveItems.add(scp);
			DecimalFormat df = new DecimalFormat("######.000");
			WkLifeTastkodifyTemp tt = commonDAO.get(WkLifeTastkodifyTemp.class, scp.getTempId());
			if (tt != null) {
				tt.setState("2");
			}
		}

		wKLifeSampleTaskDao.saveOrUpdateAll(saveItems);
		if (sc.getTemplate() != null && !sc.getTemplate().equals("")) {
			temp = wKLifeSampleTaskDao
					.get(Template.class, sc.getTemplate().getId());
			if (temp.getStorageContainer() != null
					&& !temp.getStorageContainer().equals("")) {
				maxNum = Integer.valueOf(wKLifeSampleTaskDao.selectCountMax(
						sc.getId()).toString());
				sc.setMaxNum((maxNum - 1) / (rowCode * colCode) + 1);
			}

		}
		
	}
	// 根据选中的步骤查询相关的试剂明细
	public List<Map<String, String>> setReagent(String id, String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = wKLifeSampleTaskDao.setReagent(id, code);
		List<WkLifeTaskReagent> list = (List<WkLifeTaskReagent>) result.get("list");
		if (list != null && list.size() > 0) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			for (WkLifeTaskReagent ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("code", ti.getCode());
				map.put("name", ti.getName());
				map.put("batch", ti.getBatch());
				map.put("isGood", ti.getIsGood());
				map.put("note", ti.getNote());
				map.put("tReagent", ti.gettReagent());

				if (ti.getOneNum() != null) {
					map.put("oneNum", ti.getOneNum().toString());
				} else {
					map.put("oneNum", "");
				}

				if (ti.getSampleNum() != null) {
					map.put("sampleNum", ti.getSampleNum().toString());
				} else {
					map.put("sampleNum", "");
				}

				if (ti.getNum() != null) {
					map.put("num", ti.getNum().toString());
				} else {
					map.put("num", "");
				}
				if (ti.getExpireDate() != null) {
					map.put("expireDate", sdf.format(ti.getExpireDate()));
				}
				map.put("sn", ti.getSn());
				map.put("itemId", ti.getItemId());
				map.put("tId", ti.getWk().getId());
				map.put("tName", ti.getWk().getName());
				mapList.add(map);
			}

		}
		return mapList;
	}

	// 根据选中的步骤查询相关的设备明细
	public List<Map<String, String>> setCos(String id, String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = wKLifeSampleTaskDao.setCos(id, code);
		List<WkLifeTaskCos> list = (List<WkLifeTaskCos>) result.get("list");
		if (list != null && list.size() > 0) {
			for (WkLifeTaskCos ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("code", ti.getCode());
				map.put("name", ti.getName());
				map.put("isGood", ti.getIsGood());
				if (ti.getTemperature() != null) {
					map.put("temperature", ti.getTemperature().toString());
				} else {
					map.put("temperature", "");
				}

				if (ti.getSpeed() != null) {
					map.put("speed", ti.getSpeed().toString());
				} else {
					map.put("speed", "");
				}

				if (ti.getTime() != null) {
					map.put("time", ti.getTime().toString());
				} else {
					map.put("time", "");
				}
				if (ti.getType() != null) {
					map.put("typeId", ti.getType().getId());
					map.put("typeName", ti.getType().getName());
				} else {
					map.put("typeId", "");
					map.put("typeName", "");
				}
				map.put("state", ti.getState());
				map.put("itemId", ti.getItemId());
				map.put("note", ti.getNote());
				map.put("tCos", ti.gettCos());
				map.put("tId", ti.getWk().getId());
				map.put("tName", ti.getWk().getName());
				mapList.add(map);
			}

		}
		return mapList;
	}

	// 科技服务-任务单主表
	public Map<String, Object> findTechJkServiceTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return this.wKLifeSampleTaskDao.selectTechJkServiceTaskList(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	// 科技服务-任务单明细表
	public Map<String, Object> findTechServiceTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = this.wKLifeSampleTaskDao
				.selectTechServiceTaskItemList(scId, startNum, limitNum, dir,
						sort);
		List<TechJkServiceTaskItem> list = (List<TechJkServiceTaskItem>) result
				.get("list");
		return result;
	}

	// 根据科技服务主表id 查询明细信息
	public List<Map<String, Object>> selectTechTaskItemByTaskId(String taskId) {
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		Map<String, Object> result = this.wKLifeSampleTaskDao
				.selectTechTaskItemByTaskId(taskId);
		List<TechJkServiceTaskItem> list = (List<TechJkServiceTaskItem>) result
				.get("list");
		if (list != null && list.size() > 0) {
			for (TechJkServiceTaskItem ti : list) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("id", ti.getId());
				map.put("name", ti.getName());
				map.put("sampleCode", ti.getSampleCode());
				map.put("state", ti.getState());
				map.put("stateName", ti.getStateName());

				map.put("projectId", ti.getTechJkServiceTask().getProjectId());
				map.put("contractId", ti.getTechJkServiceTask().getContractId());
				map.put("orderType", ti.getTechJkServiceTask().getOrderType());
				map.put("experimentUser", ti.getExperimentUser());
				map.put("endDate", ti.getEndDate());
				map.put("species", ti.getSpecies());
				map.put("classify", ti.getClassify());

				map.put("techJkServiceTaskId", ti.getTechJkServiceTask()
						.getId());
				map.put("techJkServiceTaskName", ti.getTechJkServiceTask()
						.getName());
				// if(ti.getSampleCode()!=null){
				// List<TechJkServiceTask> l =
				// commonService.get(TechJkServiceTask.class, "sampleCode",
				// ti.getSampleCode());
				// if(l.size()>0)
				// ti.setTechJkServiceTask(l.get(0));
				// }
				mapList.add(map);
			}
		}
		return mapList;
	}

	public List<WkLifeTaskItem> findWkTaskItemList(String scId) throws Exception {
		List<WkLifeTaskItem> list = wKLifeSampleTaskDao.selectWkTaskItemList(scId);
		return list;
	}
/**
 * 
* @Title: submitSampleModify 
* @Description: TODO(提交样本) 
* @param @param id
* @param @param ids
* @param @throws Exception    设定文件 
* @return void    返回类型 
* @author  zhiqiang.yang@biolims.cn 
* @date 2017-8-8 上午10:27:05 
* @throws
 */
	public void submitSampleModify(String id, String[] ids) throws Exception {
		// 获取主表信息
		WkLifeTaskModify sc = this.wKLifeSampleTaskDao.get(WkLifeTaskModify.class, id);
		// 获取结果表样本信息
		List<WkLifeTaskModifyInfo> list;
		if (ids == null)
			list = this.wKLifeSampleTaskDao.selectWkTastModifyInfoById(id);
		else
			list = this.wKLifeSampleTaskDao.selectWkTastModifyInfoByIds(ids);
		for (WkLifeTaskModifyInfo scp : list) {
			if (scp != null) {
					if (scp.getResult().equals("1")) {
						if (scp.getNextFlowId().equals("0009")) {// 样本入库
							SampleInItemTemp st = new SampleInItemTemp();
							st.setCode(scp.getCode());
							st.setSampleCode(scp.getSampleCode());
							st.setSampleType(scp.getDicSampleType().getName());
							st.setSampleTypeId(scp.getDicSampleType().getId());
							st.setInfoFrom("WkLifeTaskModifyInfo");
							st.setState("1");

							wKLifeSampleTaskDao.saveOrUpdate(st);
						} else if (scp.getNextFlowId().equals("0012")) {// 暂停
						} else if (scp.getNextFlowId().equals("0013")) {// 终止
						} else if (scp.getNextFlowId().equals("0020")) {// 2100质控
							Qc2100TaskTemp wqt = new Qc2100TaskTemp();
							sampleInputService.copy(wqt, scp);
							wqt.setWkType("2");
							wqt.setState("1");
							wqt.setSampleNum(scp.getWkSumTotal());
							wKLifeSampleTaskDao.saveOrUpdate(wqt);
						} else if (scp.getNextFlowId().equals("0032")) {// 文库混合
							WkLifeBlendTaskTemp q = new WkLifeBlendTaskTemp();
							sampleInputService.copy(q, scp);
							q.setState("1");
							wKLifeSampleTaskDao.saveOrUpdate(q);
						} else if (scp.getNextFlowId().equals("0021")) {// QPCR质控
							QcQpcrTaskTemp wkqt = new QcQpcrTaskTemp();
							sampleInputService.copy(wkqt, scp);
							wkqt.setWkType("1");
							wkqt.setState("1");
							wkqt.setSampleNum(scp.getWkSumTotal());
							wKLifeSampleTaskDao.saveOrUpdate(wkqt);
						} else if (scp.getNextFlowId().equals("0015")) {// 富集
							PoolingTemp ptt = new PoolingTemp();
							sampleInputService.copy(ptt, scp);
							ptt.setState("1");
//							ptt.setWkIndex(scp.getIndexa());

							wKLifeSampleTaskDao.saveOrUpdate(ptt);

						}  else {
							// 得到下一步流向的相关表单
							List<NextFlow> list_nextFlow = nextFlowDao
									.seletNextFlowById(scp.getNextFlowId());
							for (NextFlow n : list_nextFlow) {
								Object o = Class.forName(
										n.getApplicationTypeTable()
												.getClassPath()).newInstance();
								scp.setState("1");
								sampleInputService.copy(o, scp);
							}
						}
					} else {
						WkLifeAbnormalBack wka = new WkLifeAbnormalBack();
						scp.setState("1");
						sampleInputService.copy(wka, scp);
					}
					DateFormat format = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");
					sampleStateService.saveSampleState(
							scp.getCode(),
							scp.getSampleCode(),
							scp.getProductId(),
							scp.getProductName(),
							"",
							sc.getCreateDate(),
							format.format(new Date()),
							"WkLifeTaskModify",
							"文库构建修改",
							(User) ServletActionContext
									.getRequest()
									.getSession()
									.getAttribute(
											SystemConstants.USER_SESSION_KEY),
							sc.getId(), scp.getNextFlow(), scp.getResult(),
							null, null, null, null, null, null, null, null);
					scp.setSubmit("1");
					wKLifeSampleTaskDao.saveOrUpdate(scp);
			}
		}
	}

	
	// 提交样本
	public void submitSample(String id, String[] ids) throws Exception {
		// 获取主表信息
		WkLifeTask sc = this.wKLifeSampleTaskDao.get(WkLifeTask.class, id);
		// 获取结果表样本信息
		List<SampleWkLifeInfo> list;
		if (ids == null)
			list = this.wKLifeSampleTaskDao.selectSampleWkInfoById(id);
		else
			list = this.wKLifeSampleTaskDao.selectSampleWkInfoByIds(ids);
		for (SampleWkLifeInfo scp : list) {
			if (scp != null) {
				if (scp.getResult() != null
						&& (scp.getSubmit() == null || (scp.getSubmit() != null && scp
								.getSubmit().equals("")))) {
					if (scp.getResult().equals("1")) {
						if (scp.getNextFlowId().equals("0009")) {// 样本入库
							SampleInItemTemp st = new SampleInItemTemp();
							st.setCode(scp.getCode());
							st.setSampleCode(scp.getSampleCode());
							st.setSampleType(scp.getDicSampleType().getName());
							st.setSampleTypeId(scp.getDicSampleType().getId());
							st.setInfoFrom("SampleWkLifeInfo");
							st.setState("1");

							wKLifeSampleTaskDao.saveOrUpdate(st);
						} else if (scp.getNextFlowId().equals("0012")) {// 暂停
						} else if (scp.getNextFlowId().equals("0013")) {// 终止
						} else if (scp.getNextFlowId().equals("0020")) {// 2100质控
							Qc2100TaskTemp wqt = new Qc2100TaskTemp();
							sampleInputService.copy(wqt, scp);
							wqt.setState("1");
							wqt.setWkType("2");
							wqt.setSampleNum(scp.getWkSumTotal());
							wKLifeSampleTaskDao.saveOrUpdate(wqt);
						} else if (scp.getNextFlowId().equals("0067")) {// 文库混合
							WkLifeBlendTaskTemp q = new WkLifeBlendTaskTemp();
							sampleInputService.copy(q, scp);
							if (scp.getTechJkServiceTask() != null) {
								TechJkServiceTask t = this.commonDAO
										.get(TechJkServiceTask.class, scp
												.getTechJkServiceTask().getId());
								q.setCxlx(t.getSequencingType());
								q.setCxdc(t.getSequenceLength());
								q.setCxpt(t.getSequencePlatform());
							} else {
								Product p = this.commonDAO.get(Product.class,
										scp.getProductId());
								q.setCxlx(p.getSequenceType());
								q.setCxdc(p.getSequenceLength());
								q.setCxpt(p.getSequencePlatform());
							}
							q.setFjwk(scp.getCode());
							q.setSjfz(scp.getSampleCode());
							q.setSampleCode(scp.getSampleCode());
							if (scp.getExpectNum() != null) {

								q.setTl(scp.getExpectNum().toString());
							}
							if (scp.getWkConcentration() != null
									&& !"".equals(scp.getWkConcentration())) {
								q.setConcentration(scp.getWkConcentration()
										.toString());
							}
							if (scp.getWkVolume() != null
									&& !"".equals(scp.getWkVolume())) {
								q.setVolume(scp.getWkVolume().toString());
							}
							if (scp.getWkSumTotal() != null
									&& !"".equals(scp.getWkSumTotal())) {
								q.setSumTotal(scp.getWkSumTotal().toString());
							}
							q.setState("1");
							wKLifeSampleTaskDao.saveOrUpdate(q);
						} else if (scp.getNextFlowId().equals("0021")) {// QPCR质控
							QcQpcrTaskTemp wkqt = new QcQpcrTaskTemp();
							sampleInputService.copy(wkqt, scp);
							wkqt.setWkType("1");
							wkqt.setState("1");
							wkqt.setSampleNum(scp.getWkSumTotal());
							wKLifeSampleTaskDao.saveOrUpdate(wkqt);
						} else if (scp.getNextFlowId().equals("0015")) {// 富集
//							PoolingTaskTemp ptt = new PoolingTaskTemp();
//							sampleInputService.copy(ptt, scp);
//							ptt.setState("1");
//							ptt.setWkIndex(scp.getIndexa());
//
//							wKLifeSampleTaskDao.saveOrUpdate(ptt);

						} else if (scp.getNextFlowId().equals("0037")) {// 富集接收
//							PoolingReceiveTemp ptt = new PoolingReceiveTemp();
//							sampleInputService.copy(ptt, scp);
//							ptt.setState("1");
//							ptt.setWkIndex(scp.getIndexa());
//							wKLifeSampleTaskDao.saveOrUpdate(ptt);

						} else if (scp.getNextFlowId().equals("0042")) {// 模板制备life
							WkLifeBlendTaskTemp wbtt = new WkLifeBlendTaskTemp();
							sampleInputService.copy(wbtt, scp);
							wbtt.setState("1");
							wbtt.setFjwk(scp.getCode());
							if(scp.getWkConcentration()!=null && !"".equals(scp.getWkConcentration()))
								wbtt.setConcentration(scp.getWkConcentration().toString());
							if(scp.getWkVolume()!=null && !"".equals(scp.getWkVolume()))
								wbtt.setVolume(scp.getWkVolume().toString());
							if(scp.getWkSumTotal()!=null && !"".equals(scp.getWkSumTotal()))
								wbtt.setSumTotal(scp.getWkSumTotal().toString());
							wKLifeSampleTaskDao.saveOrUpdate(wbtt);

						}else {
							// 得到下一步流向的相关表单
							List<NextFlow> list_nextFlow = nextFlowDao
									.seletNextFlowById(scp.getNextFlowId());
							for (NextFlow n : list_nextFlow) {
								Object o = Class.forName(
										n.getApplicationTypeTable()
												.getClassPath()).newInstance();
								scp.setState("1");
								sampleInputService.copy(o, scp);
							}
						}
					} else {// 不合格
						// if (scp.getNextFlowId().equals("0014")) {// 反馈项目组
						// // 下一步流向是：反馈至项目组
						// FeedbackWk wf = new FeedbackWk();
						// sampleInputService.copy(wf, scp);
						// wf.setState("1");
						// wf.setSubmit("");
						// wKLifeSampleTaskDao.saveOrUpdate(wf);
						// } else {
						WkLifeAbnormalBack wka = new WkLifeAbnormalBack();
						scp.setState("1");
						sampleInputService.copy(wka, scp);
						wka.setTempId(scp.getId());
						// }

					}
					DateFormat format = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");
					sampleStateService.saveSampleState(
							scp.getCode(),
							scp.getSampleCode(),
							scp.getProductId(),
							scp.getProductName(),
							"",
							sc.getCreateDate(),
							format.format(new Date()),
							"WkLifeTask",
							"文库构建",
							(User) ServletActionContext
									.getRequest()
									.getSession()
									.getAttribute(
											SystemConstants.USER_SESSION_KEY),
							sc.getId(), scp.getNextFlow(), scp.getResult(),
							null, null, null, null, null, null, null, null);
					scp.setSubmit("1");
					wKLifeSampleTaskDao.saveOrUpdate(scp);
				}
			}
		}
	}

	// 提交样本
	public void submitSampleCtDNA(String id, String[] ids) throws Exception {
		// 获取主表信息
		WkLifeTask sc = this.wKLifeSampleTaskDao.get(WkLifeTask.class, id);
		// 获取结果表样本信息
		List<SampleWkLifeInfoCtDNA> list;
		if (ids == null)
			list = this.wKLifeSampleTaskDao.selectSampleWkInfoCtDNAById(id);
		else
			list = this.wKLifeSampleTaskDao.selectSampleWkInfoCtDNAByIds(ids);

		for (SampleWkLifeInfoCtDNA scp : list) {
			if (scp != null) {
				if (scp.getResult() != null
						&& (scp.getSubmit() == null || (scp.getSubmit() != null && scp
								.getSubmit().equals("")))
						&& scp.getNextFlowId() != null) {
					if (scp.getResult().equals("1")) {
						if (scp.getNextFlowId().equals("0009")) {// 样本入库
							SampleInItemTemp st = new SampleInItemTemp();
							st.setCode(scp.getCode());
							st.setSampleCode(scp.getSampleCode());
							st.setSampleType(scp.getSampleType());
							DicSampleType d = dicSampleTypeDao
									.selectDicSampleTypeByName(scp
											.getSampleType());
							st.setSampleTypeId(d.getId());
							st.setInfoFrom("SampleWkLifeInfoCtDNA");
							st.setState("1");
							st.setConcentration(scp.getWkConcentration());
							st.setVolume(scp.getWkVolume());
							st.setSumTotal(scp.getWkSumTotal());
							SampleInfo s = this.sampleInfoMainDao
									.findSampleInfo(scp.getSampleCode());
							st.setPatientName(s.getPatientName());
							wKLifeSampleTaskDao.saveOrUpdate(st);
						} else if (scp.getNextFlowId().equals("0010")) {// 重提取
							DnaTaskAbnormal eda = new DnaTaskAbnormal();
							scp.setState("4");// 状态为4 在重提取中显示
							sampleInputService.copy(eda, scp);

						} else if (scp.getNextFlowId().equals("0011")) {// 重建库
							WkLifeAbnormalBack wka = new WkLifeAbnormalBack();
							scp.setState("4");
							sampleInputService.copy(wka, scp);
						} else if (scp.getNextFlowId().equals("0024")) {// 重抽血
							PlasmaAbnormal wka = new PlasmaAbnormal();
							scp.setState("4");
							sampleInputService.copy(wka, scp);
						} else if (scp.getNextFlowId().equals("0012")) {// 暂停
						} else if (scp.getNextFlowId().equals("0013")) {// 终止
						} else if (scp.getNextFlowId().equals("0020")) {// 2100质控
							Qc2100TaskTemp wqt = new Qc2100TaskTemp();
							scp.setState("1");
							sampleInputService.copy(wqt, scp);
							wqt.setWkType("2");
							wKLifeSampleTaskDao.saveOrUpdate(wqt);
						} else if (scp.getNextFlowId().equals("0021")) {// QPCR质控
							QcQpcrTaskTemp wkqt = new QcQpcrTaskTemp();
							scp.setState("1");
							sampleInputService.copy(wkqt, scp);
							wkqt.setWkType("1");
							wKLifeSampleTaskDao.saveOrUpdate(wkqt);
						} else if (scp.getNextFlowId().equals("0015")) {
//							PoolingTaskTemp ptt = new PoolingTaskTemp();
//							scp.setState("1");
//							sampleInputService.copy(ptt, scp);
//
//							ptt.setWkIndex(scp.getIndexa());
//
//							WorkOrder wo = workOrderDao
//									.findWorkOrderByProduct(scp.getProductId());
//
//							ptt.setSequencingPlatform(wo
//									.getSequencingPlatform());
//							ptt.setSequencingReadLong(wo
//									.getSequencingReadLong());
//							ptt.setWkIndex(scp.getIndexa());
//							wKLifeSampleTaskDao.saveOrUpdate(ptt);

						} else {
							// 得到下一步流向的相关表单
							List<NextFlow> list_nextFlow = nextFlowDao
									.seletNextFlowById(scp.getNextFlowId());
							for (NextFlow n : list_nextFlow) {
								Object o = Class.forName(
										n.getApplicationTypeTable()
												.getClassPath()).newInstance();
								scp.setState("1");
								sampleInputService.copy(o, scp);
							}
						}
					} else {// 不合格
						if (scp.getNextFlowId().equals("0014")) {// 反馈项目组
							// 下一步流向是：反馈至项目组
							FeedbackWk wf = new FeedbackWk();
							sampleInputService.copy(wf, scp);
							wf.setState("1");
							wf.setSubmit("");
							wKLifeSampleTaskDao.saveOrUpdate(wf);
						} else {
							WkLifeAbnormalBack wka = new WkLifeAbnormalBack();
							scp.setState("2");
							sampleInputService.copy(wka, scp);
						}

					}
					DateFormat format = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");
					sampleStateService.saveSampleState(
							scp.getCode(),
							scp.getSampleCode(),
							scp.getProductId(),
							scp.getProductName(),
							"",
							sc.getCreateDate(),
							format.format(new Date()),
							"WkLifeTask",
							"文库构建",
							(User) ServletActionContext
									.getRequest()
									.getSession()
									.getAttribute(
											SystemConstants.USER_SESSION_KEY),
							sc.getId(), scp.getNextFlow(), scp.getResult(),
							null, null, null, null, null, null, null, null);
					scp.setSubmit("1");
					wKLifeSampleTaskDao.saveOrUpdate(scp);
				}
			}
		}
	}

	// 提交样本
	public void submitSamplemRNA(String id, String[] ids) throws Exception {
		// 获取主表信息
		WkLifeTask sc = this.wKLifeSampleTaskDao.get(WkLifeTask.class, id);
		// 获取结果表样本信息
		List<SampleWkLifeInfomRNA> list;
		if (ids == null)
			list = this.wKLifeSampleTaskDao.selectSampleWkInfomRNAById(id);
		else
			list = this.wKLifeSampleTaskDao.selectSampleWkInfomRNAByIds(ids);
		for (SampleWkLifeInfomRNA scp : list) {
			if (scp != null) {
				if (scp.getResult() != null
						&& (scp.getSubmit() == null || (scp.getSubmit() != null && scp
								.getSubmit().equals("")))
						&& scp.getNextFlowId() != null) {
					if (scp.getResult().equals("1")) {
						if (scp.getNextFlowId().equals("0009")) {// 样本入库
							SampleInItemTemp st = new SampleInItemTemp();
							st.setCode(scp.getCode());
							st.setSampleCode(scp.getSampleCode());
							st.setSampleType(scp.getSampleType());
							DicSampleType d = dicSampleTypeDao
									.selectDicSampleTypeByName(scp
											.getSampleType());
							st.setSampleTypeId(d.getId());
							st.setInfoFrom("SampleWkLifeInfomRNA");
							st.setState("1");
							st.setConcentration(scp.getWkConcentration());
							st.setVolume(scp.getWkVolume());
							st.setSumTotal(scp.getWkSumTotal());
							SampleInfo s = this.sampleInfoMainDao
									.findSampleInfo(scp.getSampleCode());
							st.setPatientName(s.getPatientName());
							wKLifeSampleTaskDao.saveOrUpdate(st);
							// 入库，改变SampleInfo中原始样本的状态为“待入库”
							SampleInfo sf = sampleInfoMainDao
									.findSampleInfo(scp.getCode());
							if (sf != null) {
								sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW);
								sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW_NAME);
							}
						} else if (scp.getNextFlowId().equals("0010")) {// 重提取
							DnaTaskAbnormal eda = new DnaTaskAbnormal();
							scp.setState("4");// 状态为4 在重提取中显示    
							sampleInputService.copy(eda, scp);

						} else if (scp.getNextFlowId().equals("0011")) {// 重建库
							WkLifeAbnormalBack wka = new WkLifeAbnormalBack();
							scp.setState("4");
							sampleInputService.copy(wka, scp);
						} else if (scp.getNextFlowId().equals("0024")) {// 重抽血
							PlasmaAbnormal wka = new PlasmaAbnormal();
							scp.setState("4");
							sampleInputService.copy(wka, scp);
						} else if (scp.getNextFlowId().equals("0012")) {// 暂停
							// 暂停，改变SampleInfo中原始样本的状态为“实验暂停”
							SampleInfo sf = sampleInfoMainDao
									.findSampleInfo(scp.getCode());
							if (sf != null) {
								sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
								sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
							}
						} else if (scp.getNextFlowId().equals("0013")) {// 终止
							// 终止，改变SampleInfo中原始样本的状态为“实验终止”
							SampleInfo sf = sampleInfoMainDao
									.findSampleInfo(scp.getCode());
							if (sf != null) {
								sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
								sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
							}
						} else if (scp.getNextFlowId().equals("0020")) {// 2100质控
							Qc2100TaskTemp wqt = new Qc2100TaskTemp();
							scp.setState("1");
							sampleInputService.copy(wqt, scp);
							wqt.setWkType("2");
							wKLifeSampleTaskDao.saveOrUpdate(wqt);
						} else if (scp.getNextFlowId().equals("0021")) {// QPCR质控
							QcQpcrTaskTemp wkqt = new QcQpcrTaskTemp();
							scp.setState("1");
							sampleInputService.copy(wkqt, scp);
							wkqt.setWkType("1");
							wKLifeSampleTaskDao.saveOrUpdate(wkqt);
						} else if (scp.getNextFlowId().equals("0015")) {
//							PoolingTaskTemp ptt = new PoolingTaskTemp();
//							scp.setState("1");
//							sampleInputService.copy(ptt, scp);
//
//							ptt.setWkIndex(scp.getIndexa());
//
//							WorkOrder wo = workOrderDao
//									.findWorkOrderByProduct(scp.getProductId());
//
//							ptt.setSequencingPlatform(wo
//									.getSequencingPlatform());
//							ptt.setSequencingReadLong(wo
//									.getSequencingReadLong());
//							ptt.setWkIndex(scp.getIndexa());
//							wKLifeSampleTaskDao.saveOrUpdate(ptt);

						} else {
							// 得到下一步流向的相关表单
							List<NextFlow> list_nextFlow = nextFlowDao
									.seletNextFlowById(scp.getNextFlowId());
							for (NextFlow n : list_nextFlow) {
								Object o = Class.forName(
										n.getApplicationTypeTable()
												.getClassPath()).newInstance();
								scp.setState("1");
								sampleInputService.copy(o, scp);
							}
						}
					} else {// 不合格
						if (scp.getNextFlowId().equals("0014")) {// 反馈项目组
							// 下一步流向是：反馈至项目组
							FeedbackWk wf = new FeedbackWk();
							sampleInputService.copy(wf, scp);
							wf.setState("1");
							wf.setSubmit("");
							wKLifeSampleTaskDao.saveOrUpdate(wf);
						} else {
							WkLifeAbnormalBack wka = new WkLifeAbnormalBack();
							scp.setState("2");
							sampleInputService.copy(wka, scp);
						}

					}
					DateFormat format = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");
					sampleStateService.saveSampleState(
							scp.getCode(),
							scp.getSampleCode(),
							scp.getProductId(),
							scp.getProductName(),
							"",
							sc.getCreateDate(),
							format.format(new Date()),
							"WkLifeTask",
							"文库构建",

							(User) ServletActionContext
									.getRequest()
									.getSession()
									.getAttribute(
											SystemConstants.USER_SESSION_KEY),
							sc.getId(), scp.getNextFlow(), scp.getResult(),
							null, null, null, null, null, null, null, null);
					scp.setSubmit("1");
					wKLifeSampleTaskDao.saveOrUpdate(scp);
				}
			}
		}
	}

	// 提交样本
	public void submitSamplerRNA(String id, String[] ids) throws Exception {
		// 获取主表信息
		WkLifeTask sc = this.wKLifeSampleTaskDao.get(WkLifeTask.class, id);

		List<SampleWkLifeInforRNA> list;
		if (ids == null)
			list = this.wKLifeSampleTaskDao.selectSampleWkInforRNAById(id);
		else
			list = this.wKLifeSampleTaskDao.selectSampleWkInforRNAByIds(ids);

		for (SampleWkLifeInforRNA scp : list) {
			if (scp != null) {
				if (scp.getResult() != null
						&& (scp.getSubmit() == null || (scp.getSubmit() != null && scp
								.getSubmit().equals("")))
						&& scp.getNextFlowId() != null) {
					if (scp.getResult().equals("1")) {
						if (scp.getNextFlowId().equals("0009")) {// 样本入库
							SampleInItemTemp st = new SampleInItemTemp();
							st.setCode(scp.getCode());
							st.setSampleCode(scp.getSampleCode());
							st.setNum(scp.getSampleNum());
							st.setSampleType(scp.getSampleType());
							DicSampleType d = dicSampleTypeDao
									.selectDicSampleTypeByName(scp
											.getSampleType());
							st.setSampleTypeId(d.getId());
							st.setInfoFrom("SampleWkLifeInforRNA");
							st.setState("1");
							// st.setConcentration(scp.getWkConcentration());
							// st.setVolume(scp.getWkVolume());
							// st.setSumTotal(scp.getWkSumTotal());
							// SampleInfo s = this.sampleInfoMainDao
							// .findSampleInfo(scp.getSampleCode());
							// st.setPatientName(s.getPatientName());
							wKLifeSampleTaskDao.saveOrUpdate(st);
						} else if (scp.getNextFlowId().equals("0010")) {// 重提取
							DnaTaskAbnormal eda = new DnaTaskAbnormal();
							scp.setState("4");// 状态为4 在重提取中显示
							sampleInputService.copy(eda, scp);

						} else if (scp.getNextFlowId().equals("0011")) {// 重建库
							WkLifeAbnormalBack wka = new WkLifeAbnormalBack();
							scp.setState("4");
							sampleInputService.copy(wka, scp);
						} else if (scp.getNextFlowId().equals("0024")) {// 重抽血
							PlasmaAbnormal wka = new PlasmaAbnormal();
							scp.setState("4");
							sampleInputService.copy(wka, scp);
						} else if (scp.getNextFlowId().equals("0012")) {// 暂停
						} else if (scp.getNextFlowId().equals("0013")) {// 终止
						} else if (scp.getNextFlowId().equals("0020")) {// 2100质控
							Qc2100TaskTemp wqt = new Qc2100TaskTemp();
							scp.setState("1");
							sampleInputService.copy(wqt, scp);
							wqt.setWkType("2");
							wKLifeSampleTaskDao.saveOrUpdate(wqt);
						} else if (scp.getNextFlowId().equals("0021")) {// QPCR质控
							QcQpcrTaskTemp wkqt = new QcQpcrTaskTemp();
							scp.setState("1");
							sampleInputService.copy(wkqt, scp);
							wkqt.setWkType("1");
							wKLifeSampleTaskDao.saveOrUpdate(wkqt);
						} else if (scp.getNextFlowId().equals("0015")) {
//							PoolingTaskTemp ptt = new PoolingTaskTemp();
//							scp.setState("1");
//							sampleInputService.copy(ptt, scp);
//
//							ptt.setWkIndex(scp.getIndexa());
//
//							WorkOrder wo = workOrderDao
//									.findWorkOrderByProduct(scp.getProductId());
//
//							ptt.setSequencingPlatform(wo
//									.getSequencingPlatform());
//							ptt.setSequencingReadLong(wo
//									.getSequencingReadLong());
//							ptt.setWkIndex(scp.getIndexa());
//							wKLifeSampleTaskDao.saveOrUpdate(ptt);

						} else {
							// 得到下一步流向的相关表单
							List<NextFlow> list_nextFlow = nextFlowDao
									.seletNextFlowById(scp.getNextFlowId());
							for (NextFlow n : list_nextFlow) {
								Object o = Class.forName(
										n.getApplicationTypeTable()
												.getClassPath()).newInstance();
								scp.setState("1");
								sampleInputService.copy(o, scp);
							}
						}
					} else {// 不合格
						if (scp.getNextFlowId().equals("0014")) {// 反馈项目组
							// 下一步流向是：反馈至项目组
							FeedbackWk wf = new FeedbackWk();
							sampleInputService.copy(wf, scp);
							wf.setState("1");
							wf.setSubmit("");
							wKLifeSampleTaskDao.saveOrUpdate(wf);
						} else {
							WkLifeAbnormalBack wka = new WkLifeAbnormalBack();
							scp.setState("2");
							sampleInputService.copy(wka, scp);
						}

					}
					DateFormat format = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");
					sampleStateService.saveSampleState(
							scp.getCode(),
							scp.getSampleCode(),
							scp.getProductId(),
							scp.getProductName(),
							"",
							sc.getCreateDate(),
							format.format(new Date()),
							"WkLifeTask",
							"文库构建",
							(User) ServletActionContext
									.getRequest()
									.getSession()
									.getAttribute(
											SystemConstants.USER_SESSION_KEY),
							sc.getId(), scp.getNextFlow(), scp.getResult(),
							null, null, null, null, null, null, null, null);
					scp.setSubmit("1");
					wKLifeSampleTaskDao.saveOrUpdate(scp);
				}
			}
		}
	}

	// 明细入库
	public void rukuItem(String[] ids) throws Exception {
		for (String id : ids) {
			WkLifeTaskItem scp = wKLifeSampleTaskDao.get(WkLifeTaskItem.class, id);
			if (!scp.getState().equals("1")) {
				scp.setState("1");// 表示该样本已入库
				SampleInItemTemp st = new SampleInItemTemp();
				sampleInputService.copy(st, scp);
				// if (scp.getSampleNum() != null
				// && scp.getSampleConsume() != null) {
				// st.setSumTotal(scp.getSampleNum() - scp.getSampleConsume());
				// }
				st.setSampleType(scp.getSampleType());
				DicSampleType d = dicSampleTypeDao
						.selectDicSampleTypeByName(scp.getSampleType());
				st.setSampleTypeId(d.getId());
				st.setInfoFrom("PlasmaTaskItem");
				this.wKLifeSampleTaskDao.saveOrUpdate(st);
			}
		}
	}

	// 生成index
	public void index(String id) {
		WkLifeTaskItem w = this.storageDao.get(WkLifeTaskItem.class, id);
		Storage s = this.storageDao.get(Storage.class, w.getTag());
		// 根据试剂盒id查询
		List<Storage> list = this.storageDao.selStorageBykitId(w.getKit());
		// 查询该试剂盒index的最大排序号
		int indexMax = this.storageDao.maxCurrentIndexByKitId(w.getKit());
		// 试剂盒index数量
		int indexNum = list.size();
		// index排序号
		int a = 0;
		for (int i = 0; i < indexNum; i++) {
			if (indexMax == s.getCurrentIndex()) {
				a = indexMax - indexNum + 1;
			} else {
				a = s.getCurrentIndex() + i + 1;
			}
			// 根据index排序号查询index
			Storage s2 = this.storageDao.selStoragebycurrentIndex(a);
			int orderNumber = w.getOrderNumber() + i + 1;
			// 根据主表id和子表实验室排序号赋值
			WkLifeTaskItem w2 = this.wKLifeSampleTaskDao.selWkTaskItem(w.getWk()
					.getId(), orderNumber);
			if (w2 != null) {
				w2.setIndexa(s2.getName());
				w2.setI7(s2.getI5());
				w2.setTag(s2.getId());
				w2.setKit(s2.getKit().getId());
			}
		}
		// // 查询上一次index使用到哪个
		// Storage s = this.storageDao.selStorageGetIndex();
		// int n = 0;
		// if (s == null) {
		// n = 0;
		// } else {
		// n = s.getCurrentIndex();// 排序号
		// s.setIndexState("0");
		// }
		// if (scp.getIndexa() == null) {
		// n++;
		// // 根据排序号查询index
		// Storage s2 = this.storageDao.selStoragebycurrentIndex(n);
		// if (s2 == null) {
		// s2 = this.storageDao.selStoragebycurrentIndex(1);
		// }
		// scp.setTag(s2.getCurrentIndex().toString());
		// scp.setIndexa(s2.getName());
		// scp.setI5(s2.getI5());
		// scp.setI7(s2.getI7());
		//
		// s2.setIndexState("1");
		// } else {
		// // 根据排序号查询index
		// Storage s3 = this.storageDao.selStoragebycurrentIndex(Integer
		// .valueOf(scp.getTag()));
		// s3.setIndexState("1");
		// }

	}

	/**
	 * 按照检测项目拆分样本
	 * 
	 * @param id
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void wkTempSplit(String id) throws Exception {
		WkLifeTaskTemp wt = commonDAO.get(WkLifeTaskTemp.class, id);
		if (wt != null) {
			if (wt.getProductId() != null && !wt.getProductId().equals("")) {
				String[] arr = wt.getProductId().split(",");
				String[] arr1 = wt.getProductName().split(",");
				for (int i = 0; i < arr.length; i++) {
					WkLifeTaskTemp wtt = new WkLifeTaskTemp();
					sampleInputService.copy(wtt, wt);
					wtt.setSplitCode(wt.getSplitCode() + "00"
							+ String.valueOf(i + 1));
					wtt.setProductId(arr[i]);
					wtt.setProductName(arr1[i]);
					wKLifeSampleTaskDao.saveOrUpdate(wtt);
				}
				wt.setState("3");
				wKLifeSampleTaskDao.saveOrUpdate(wt);
			}
		}
	}

	/**
	 * 
	 * @Title: findWKTastModifyList
	 * @Description: TODO(获取取消信息列表)
	 * @param @param map2Query
	 * @param @param startNum
	 * @param @param limitNum
	 * @param @param dir
	 * @param @param sort
	 * @param @return    设定文件
	 * @return Map<String,Object>    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-8-3 下午5:03:00
	 * @throws
	 */
	public Map<String, Object> findWKTastModifyList(
			Map<String, String> map2Query, int startNum, int limitNum,
			String dir, String sort) {
		return wKLifeSampleTaskDao.selectWKTastModifyList(map2Query, startNum,
				limitNum, dir, sort);
	}

	/**
	 * 
	 * @Title: getWkTastModify
	 * @Description: TODO(获取主数据)
	 * @param @param id
	 * @param @return    设定文件
	 * @return WkTaskModify    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-8-3 下午6:00:55
	 * @throws
	 */
	public WkLifeTaskModify getWkTastModify(String id) {
		WkLifeTaskModify wk = commonDAO.get(WkLifeTaskModify.class, id);
		return wk;
	}
/**
 * @throws Exception 
 * 
* @Title: findWKTaastModifyItemList 
* @Description: TODO(文库构建结果数据列表) 
* @param @param scId
* @param @param startNum
* @param @param limitNum
* @param @param dir
* @param @param sort
* @param @return    设定文件 
* @return Map<String,Object>    返回类型 
* @author  zhiqiang.yang@biolims.cn 
* @date 2017-8-4 下午3:16:53 
* @throws
 */
	public Map<String, Object> findWKTaastModifyItemList(String scId,
			int startNum, int limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = wKLifeSampleTaskDao.selectWKTastModifyItemList(scId,
				startNum, limitNum, dir, sort);
		List<WkLifeTaskItem> list = (List<WkLifeTaskItem>) result.get("list");
		return result;
	}
}
