package com.biolims.experiment.wkLife.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.check.model.TechCheckServiceTaskTemp;
import com.biolims.experiment.qc.model.QcReceiveTemp;
import com.biolims.experiment.wkLife.dao.WkLifeAbnormalDao;
import com.biolims.experiment.wkLife.dao.WkLifeManageDao;
import com.biolims.experiment.wkLife.model.SampleWkLifeInfo;
import com.biolims.experiment.wkLife.model.WkLifeAbnormalBack;
import com.biolims.experiment.wkLife.model.WkLifeManage;
import com.biolims.experiment.wkLife.model.WkLifeTaskItem;
import com.biolims.experiment.wkLife.model.WkLifeTaskTemp;
import com.biolims.experiment.wkLife.model.WkLifeTastkodifyTemp;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.project.feedback.model.FeedbackWk;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.storage.dao.SampleInDao;
import com.biolims.sample.storage.model.SampleInItem;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.sample.storage.model.SampleOutTemp;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class WkLifeManageService {
	@Resource
	private WkLifeManageDao wkLifeManageDao;
	@Resource
	private WkLifeAbnormalDao wkLifeAbnormalDao;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInDao sampleInDao;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findWkManageList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return wkLifeManageDao.selectWkManageList(mapForQuery, startNum, limitNum,
				dir, sort);
	}

	public Map<String, Object> findWkItemManageList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return wkLifeManageDao.selectWkItemManageList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(WkLifeManage i) throws Exception {

		wkLifeManageDao.saveOrUpdate(i);

	}

	public WkLifeManage get(String id) {
		WkLifeManage wkManage = commonDAO.get(WkLifeManage.class, id);
		return wkManage;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(WkLifeManage sc, Map jsonMap) throws Exception {
		if (sc != null) {
			wkLifeManageDao.saveOrUpdate(sc);

			String jsonStr = "";
		}
	}

	// 保存文库管理
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWkManage(String itemDataJson) throws Exception {
		List<SampleWkLifeInfo> saveItems = new ArrayList<SampleWkLifeInfo>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SampleWkLifeInfo scp = new SampleWkLifeInfo();
			scp = (SampleWkLifeInfo) wkLifeManageDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			saveItems.add(scp);
			if (scp != null) {
				if (scp.getResult() != null && scp.getSubmit() != null
						&& !scp.getSubmit().equals("")
						&& scp.getSubmit().equals("1")) {
					if (scp.getNextFlow() != null
							&& !scp.getNextFlow().equals("")) {
						// 提交的样本按照下一步流向走
						if (scp.getNextFlow().equals("0")) {
							// 文库质控样本到2100和QPCR

							QcReceiveTemp qt = new QcReceiveTemp();
							qt.setCode(scp.getCode());
							qt.setSampleCode(scp.getSampleCode());
							qt.setPatientName(scp.getPatientName());
							qt.setProductId(scp.getProductId());
							qt.setProductName(scp.getProductName());
							qt.setInspectDate(scp.getInspectDate());
							qt.setAcceptDate(scp.getAcceptDate());
							qt.setIdCard(scp.getIdCard());
							qt.setPhone(scp.getPhone());
							qt.setOrderId(scp.getOrderId());
							qt.setReportDate(scp.getReportDate());
							qt.setWkCode(scp.getWkCode());
							qt.setWkType("0");
							qt.setState("1");

							qt.setRowCode(scp.getRowCode());
							qt.setColCode(scp.getColCode());
							qt.setCounts(scp.getCounts());
							qt.setTechTaskId(scp.getJkTaskId());
							qt.setProjectId(scp.getProjectId());
							qt.setContractId(scp.getContractId());
							qt.setOrderType(scp.getOrderType());

							qt.setClassify(scp.getClassify());
							commonDAO.saveOrUpdate(qt);

							// 样本到质控后，改变SampleInfo中原始样本的状态为“完成文库构建”
							SampleInfo sf = sampleInfoMainDao
									.findSampleInfo(scp.getSampleCode());
							if (sf != null) {
								sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_WK_COMPLETE);
								sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_WK_COMPLETE_NAME);
							}
							scp.setState("3");
						} else if (scp.getNextFlow().equals("1")) {
							// 2100 or Caliper
							QcReceiveTemp qt = new QcReceiveTemp();
							qt.setCode(scp.getCode());
							qt.setSampleCode(scp.getSampleCode());
							qt.setPatientName(scp.getPatientName());
							qt.setProductId(scp.getProductId());
							qt.setProductName(scp.getProductName());
							qt.setInspectDate(scp.getInspectDate());
							qt.setAcceptDate(scp.getAcceptDate());
							qt.setIdCard(scp.getIdCard());
							qt.setPhone(scp.getPhone());
							qt.setOrderId(scp.getOrderId());
							qt.setReportDate(scp.getReportDate());
							qt.setWkCode(scp.getWkCode());
							qt.setWkType("2");

							qt.setRowCode(scp.getRowCode());
							qt.setColCode(scp.getColCode());
							qt.setCounts(scp.getCounts());
							qt.setState("1");

							qt.setTechTaskId(scp.getJkTaskId());
							qt.setProjectId(scp.getProjectId());
							qt.setContractId(scp.getContractId());
							qt.setOrderType(scp.getOrderType());

							qt.setClassify(scp.getClassify());
							commonDAO.saveOrUpdate(qt);

							// 合格样本到质控后，改变SampleInfo中原始样本的状态为“完成文库构建”
							SampleInfo sf = sampleInfoMainDao
									.findSampleInfo(scp.getSampleCode());
							if (sf != null) {
								sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_WK_COMPLETE);
								sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_WK_COMPLETE_NAME);
							}
							scp.setState("3");
						} else if (scp.getNextFlow().equals("2")) {
							// 终止，到重建库
							WkLifeAbnormalBack wka = new WkLifeAbnormalBack();
							wka.setName(scp.getName());
							wka.setSampleCode(scp.getSampleCode());
							wka.setWkCode(scp.getWkCode());
							wka.setCode(scp.getCode());
							wka.setIndexa(scp.getIndexa());
							wka.setVolume(scp.getVolume());
							wka.setResult(scp.getResult());
							wka.setNextFlow(scp.getNextFlow());
							wka.setBackTime(new Date());
							wka.setNote(scp.getNote());
							wka.setPatientName(scp.getPatientName());
							wka.setProductId(scp.getProductId());
							wka.setProductName(scp.getProductName());
							wka.setInspectDate(scp.getInspectDate());
							wka.setAcceptDate(scp.getAcceptDate());
							wka.setIdCard(scp.getIdCard());
							wka.setPhone(scp.getPhone());
							wka.setOrderId(scp.getOrderId());
							wka.setSequenceFun(scp.getSequenceFun());
							wka.setReportDate(scp.getReportDate());

							wka.setTechTaskId(scp.getJkTaskId());
							wka.setProjectId(scp.getProjectId());
							wka.setContractId(scp.getContractId());
							wka.setOrderType(scp.getOrderType());
							wka.setClassify(scp.getClassify());
							wkLifeAbnormalDao.saveOrUpdate(wka);
							scp.setState("3");
						} else if (scp.getNextFlow().equals("3")) {
							// 入库
							SampleInItemTemp st = new SampleInItemTemp();
							st.setCode(scp.getWkCode());
							st.setSampleCode(scp.getSampleCode());
							st.setNum(scp.getVolume());
							st.setState("1");
							commonDAO.saveOrUpdate(st);
							// 入库，改变SampleInfo中原始样本的状态为“待入库”
							SampleInfo sf = sampleInfoMainDao
									.findSampleInfo(scp.getSampleCode());
							if (sf != null) {
								sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW);
								sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW_NAME);
							}
							scp.setState("3");
						} else if (scp.getNextFlow().equals("4")) {
							// 异常反馈至项目管理
							FeedbackWk wf = new FeedbackWk();
							wf.setName(scp.getName());
							wf.setSampleCode(scp.getSampleCode());
							wf.setWkCode(scp.getWkCode());
							wf.setCode(scp.getCode());
							wf.setIndexs(scp.getIndexa());
							wf.setVolume(scp.getVolume());
							wf.setResult(scp.getResult());
							wf.setNextflow(scp.getNextFlow());
							wf.setPatientName(scp.getPatientName());
							wf.setProductId(scp.getProductId());
							wf.setProductName(scp.getProductName());
							wf.setInspectDate(scp.getInspectDate());
							wf.setAcceptDate(scp.getAcceptDate());
							wf.setIdCard(scp.getIdCard());
							wf.setPhone(scp.getPhone());
							wf.setOrderId(scp.getOrderId());
							wf.setSequenceFun(scp.getSequenceFun());
							wf.setReportDate(scp.getReportDate());
							wf.setState("1");
							wf.setRowCode(scp.getRowCode());
							wf.setColCode(scp.getColCode());
							wf.setCounts(scp.getCounts());

							wf.setTechTaskId(scp.getJkTaskId());
							wf.setProjectId(scp.getProjectId());
							wf.setContractId(scp.getContractId());
							wf.setOrderType(scp.getOrderType());
							wf.setClassify(scp.getClassify());
							commonDAO.saveOrUpdate(wf);
							scp.setState("3");
						} else if (scp.getNextFlow().equals("5")) {
							// 暂停，改变SampleInfo中原始样本的状态为“实验暂停”
							SampleInfo sf = sampleInfoMainDao
									.findSampleInfo(scp.getSampleCode());
							if (sf != null) {
								sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
								sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
							}
						} else if (scp.getNextFlow().equals("6")) {
							// QPCR质控
							QcReceiveTemp qt1 = new QcReceiveTemp();
							qt1.setCode(scp.getCode());
							qt1.setSampleCode(scp.getSampleCode());
							qt1.setPatientName(scp.getPatientName());
							qt1.setProductId(scp.getProductId());
							qt1.setProductName(scp.getProductName());
							qt1.setInspectDate(scp.getInspectDate());
							qt1.setAcceptDate(scp.getAcceptDate());
							qt1.setIdCard(scp.getIdCard());
							qt1.setPhone(scp.getPhone());
							qt1.setOrderId(scp.getOrderId());
							qt1.setReportDate(scp.getReportDate());
							qt1.setWkCode(scp.getWkCode());
							qt1.setWkType("1");
							qt1.setState("1");
							qt1.setRowCode(scp.getRowCode());
							qt1.setColCode(scp.getColCode());
							qt1.setCounts(scp.getCounts());

							qt1.setTechTaskId(scp.getJkTaskId());
							qt1.setProjectId(scp.getProjectId());
							qt1.setContractId(scp.getContractId());
							qt1.setOrderType(scp.getOrderType());
							qt1.setClassify(scp.getClassify());
							commonDAO.saveOrUpdate(qt1);
							// 到质控后，改变SampleInfo中原始样本的状态为“完成文库构建”
							SampleInfo sf = sampleInfoMainDao
									.findSampleInfo(scp.getSampleCode());
							if (sf != null) {
								sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_WK_COMPLETE);
								sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_WK_COMPLETE_NAME);
							}
							scp.setState("3");
						} else if (scp.getNextFlow().equals("8")) {// 核酸检测
							if (scp.getOrderType() != null) {
								if (scp.getOrderType().equals("3")) {
									// 任务单类型为：测序任务单
									TechCheckServiceTaskTemp tc = new TechCheckServiceTaskTemp();
//									tc.setAcceptDate(scp.getAcceptDate());
//									tc.setContractId(scp.getContractId());
//									tc.setIdCard(scp.getIdCard());
//									tc.setInspectDate(scp.getInspectDate());
									tc.setNote(scp.getNote());

//									tc.setOrderType(scp.getOrderType());
//									tc.setPatient(scp.getPatientName());
//									tc.setPhone(scp.getPhone());
									tc.setProductId(scp.getProductId());
									tc.setOrderId(scp.getJkTaskId());

									tc.setProductName(scp.getProductName());
//									tc.setProjectId(scp.getProductId());
//									tc.setReason(scp.getReason());
//									tc.setReportDate(scp.getReportDate());
									tc.setSampleCode(scp.getSampleCode());

									tc.setState("1");
									tc.setClassify(scp.getClassify());
									this.commonDAO.saveOrUpdate(tc);
									scp.setState("3");
								}
							}
						} else {
							// 重建库
							WkLifeAbnormalBack wka = new WkLifeAbnormalBack();
							wka.setName(scp.getName());
							wka.setSampleCode(scp.getSampleCode());
							wka.setWkCode(scp.getWkCode());
							wka.setCode(scp.getCode());
							wka.setIndexa(scp.getIndexa());
							wka.setVolume(scp.getVolume());
							wka.setResult(scp.getResult());
							wka.setNextFlow(scp.getNextFlow());
							wka.setBackTime(new Date());
							wka.setNote(scp.getNote());
							wka.setPatientName(scp.getPatientName());
							wka.setProductId(scp.getProductId());
							wka.setProductName(scp.getProductName());
							wka.setInspectDate(scp.getInspectDate());
							wka.setAcceptDate(scp.getAcceptDate());
							wka.setIdCard(scp.getIdCard());
							wka.setPhone(scp.getPhone());
							wka.setOrderId(scp.getOrderId());
							wka.setSequenceFun(scp.getSequenceFun());
							wka.setReportDate(scp.getReportDate());

							wka.setTechTaskId(scp.getJkTaskId());
							wka.setProjectId(scp.getProjectId());
							wka.setContractId(scp.getContractId());
							wka.setOrderType(scp.getOrderType());
							wka.setClassify(scp.getClassify());
							wkLifeAbnormalDao.saveOrUpdate(wka);
							scp.setState("3");
						}
					}
				}
			}
		}
		wkLifeManageDao.saveOrUpdateAll(saveItems);
	}

	// 保存文库明细
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWkItemManage(String itemDataJson) throws Exception {
		List<WkLifeTaskItem> saveItems = new ArrayList<WkLifeTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			WkLifeTaskItem sbi = new WkLifeTaskItem();
			sbi = (WkLifeTaskItem) wkLifeManageDao.Map2Bean(map, sbi);
			if (sbi.getId() != null && sbi.getId().equals(""))
				sbi.setId(null);

			saveItems.add(sbi);
			if (sbi != null && sbi.getNextFlow() != null
					&& sbi.getResult() != null) {
				// 判定合格的执行下一步
				if (sbi.getResult().equals("1")) {
					if (sbi.getNextFlow().equals("0")) {
						// 入库
						// Qc2100TaskTemp wt=new Qc2100TaskTemp();
						// wt.setCode(sbi.getCode());
						// wt.setWkCode(sbi.getWkId());
						// wt.setName(sbi.getName());
						// wt.setSampleCode(sbi.getSampleCode());
						// wt.setWkType("0");
						// wt.setWkVolume(sbi.getVolume());
						// wt.setResult(sbi.getResult());
						// wt.setNote(sbi.getNote());
						// wt.setPatientName(sbi.getPatientName());
						// wt.setProductId(sbi.getProductId());
						// wt.setProductName(sbi.getProductName());
						// wt.setInspectDate(sbi.getInspectDate());
						// wt.setAcceptDate(sbi.getAcceptDate());
						// wt.setIdCard(sbi.getIdCard());
						// wt.setSequenceFun(sbi.getSequenceFun());
						// wt.setReportDate(sbi.getReportDate());
						// wt.setState("1");
						// wkLifeManageDao.saveOrUpdate(wt);
						// QcReceiveTemp wqt = new QcReceiveTemp();
						// wqt.setCode(sbi.getCode());
						// wqt.setSampleCode(sbi.getSampleCode());
						// wqt.setWkCode(sbi.getWkId());
						// wqt.setWkType("0");
						// wqt.setVolume(sbi.getVolume());
						// wqt.setPatientName(sbi.getPatientName());
						// wqt.setProductId(sbi.getProductId());
						// wqt.setProductName(sbi.getProductName());
						// wqt.setInspectDate(sbi.getInspectDate());
						// wqt.setAcceptDate(sbi.getAcceptDate());
						// wqt.setIdCard(sbi.getIdCard());
						// wqt.setPhone(sbi.getPhone());
						// wqt.setOrderId(sbi.getOrderId());
						// wqt.setReportDate(sbi.getReportDate());
						// wqt.setState("1");
						// wqt.setClassify(sbi.getClassify());
						// wkLifeAbnormalDao.saveOrUpdate(wqt);
						// 合格样本到质控后，改变SampleInfo中原始样本的状态为“完成文库构建”
						// SampleInfo
						// sf=sampleInfoMainDao.findSampleInfo(sbi.getSampleCode());
						// if(sf!=null){
						// sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_WK_COMPLETE);
						// sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_WK_COMPLETE_NAME);
						// }
						// }else if(sbi.getNextFlow().equals("1")){ //QPCR质控
						// QcQpcrTaskTemp qd=new QcQpcrTaskTemp();
						// qd.setCode(sbi.getCode());
						// qd.setWkCode(sbi.getWkId());
						// qd.setName(sbi.getName());
						// qd.setSampleCode(sbi.getSampleCode());
						// qd.setWkType("1");
						// qd.setWkVolume(sbi.getVolume());
						// qd.setResult(sbi.getResult());
						// qd.setNote(sbi.getNote());
						// qd.setPatientName(sbi.getPatientName());
						// qd.setProductId(sbi.getProductId());
						// qd.setProductName(sbi.getProductName());
						// qd.setInspectDate(sbi.getInspectDate());
						// qd.setAcceptDate(sbi.getAcceptDate());
						// qd.setIdCard(sbi.getIdCard());
						// qd.setSequenceFun(sbi.getSequenceFun());
						// qd.setReportDate(sbi.getReportDate());
						// qd.setState("1");
						// wkLifeManageDao.saveOrUpdate(qd);
						// QcReceiveTemp wqt = new QcReceiveTemp();
						// wqt.setCode(sbi.getCode());
						// wqt.setSampleCode(sbi.getSampleCode());
						// wqt.setWkCode(sbi.getWkId());
						// wqt.setWkType("0");
						// wqt.setVolume(sbi.getVolume());
						// wqt.setPatientName(sbi.getPatientName());
						// wqt.setProductId(sbi.getProductId());
						// wqt.setProductName(sbi.getProductName());
						// wqt.setInspectDate(sbi.getInspectDate());
						// wqt.setAcceptDate(sbi.getAcceptDate());
						// wqt.setIdCard(sbi.getIdCard());
						// wqt.setPhone(sbi.getPhone());
						// wqt.setOrderId(sbi.getOrderId());
						// wqt.setReportDate(sbi.getReportDate());
						// wqt.setState("1");
						// wqt.setClassify(sbi.getClassify());
						// wkLifeAbnormalDao.saveOrUpdate(wqt);
						// 合格样本到质控后，改变SampleInfo中原始样本的状态为“完成文库构建”
						// SampleInfo
						// sf=sampleInfoMainDao.findSampleInfo(sbi.getSampleCode());
						// if(sf!=null){
						// sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_WK_COMPLETE);
						// sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_WK_COMPLETE_NAME);
						// }
						// }else if(sbi.getNextFlow().equals("2")){ //终止
						// List<SampleInfo>
						// sList=sampleInfoMainDao.setSampleInfoBySampleCode(sbi.getCode());
						// if(sList.size()>0){
						// for(SampleInfo si:sList){
						// si.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
						// si.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
						// }
						// }
						// //终止，改变SampleInfo中原始样本的状态为“实验终止”
						// SampleInfo
						// sf=sampleInfoMainDao.findSampleInfo(sbi.getSampleCode());
						// if(sf!=null){
						// sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
						// sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
						// }
						// }else{
						// 入库
						SampleInItemTemp st = new SampleInItemTemp();
						if (sbi.getCode() == null) {
							st.setCode(sbi.getSampleCode());
						} else {
							st.setCode(sbi.getCode());
						}
						st.setSampleCode(sbi.getSampleCode());

						if (sbi.getSampleNum() != null
								&& sbi.getSampleConsume() != null) {
							st.setNum(sbi.getSampleNum()
									- sbi.getSampleConsume());
						}
						st.setState("1");
						wkLifeManageDao.saveOrUpdate(st);
						// 入库，改变SampleInfo中原始样本的状态为“待入库”
						SampleInfo sf = sampleInfoMainDao.findSampleInfo(sbi
								.getSampleCode());
						if (sf != null) {
							sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW);
							sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW_NAME);
						}
						sbi.setState("3");
					} else {
						// 出库
						// 先判断有没有入库
						List<SampleInItem> listsit = sampleInDao
								.getSampleInState(sbi.getSampleCode());
						if (listsit.size() > 0) {
							// 已完成入库的样本，判断其量是不是0
							if (listsit.get(0).getNum() > 0) {
								SampleOutTemp sot = new SampleOutTemp();
								sot.setCode(sbi.getCode());
								sot.setSampleCode(sbi.getSampleCode());
								// sot.setMethod("重建库");
								sot.setState("1");
								commonDAO.saveOrUpdate(sot);
								sbi.setState("3");
							}
						}
					}
				}
			}
		}
		wkLifeManageDao.saveOrUpdateAll(saveItems);
	}

	// 样本管理明细入库
	public void wkManageItemRuku(String ids) {
		String[] id1 = ids.split(",");
		for (String id : id1) {
			WkLifeTaskItem scp = wkLifeManageDao.get(WkLifeTaskItem.class, id);
			if (scp != null) {
				scp.setState("2");
				SampleInItemTemp st = new SampleInItemTemp();
				if (scp.getCode() == null) {
					st.setCode(scp.getCode());
				} else {
					st.setCode(scp.getCode());
				}
				st.setSampleCode(scp.getSampleCode());
				if (scp.getSampleNum() != null
						&& scp.getSampleConsume() != null) {
					st.setNum(scp.getSampleNum() - scp.getSampleConsume());
				}
				st.setState("1");
				wkLifeManageDao.saveOrUpdate(st);
			}

		}
	}

	// 样本管理明细建库待办
	public void wkManageItemJK(String id) {
		WkLifeTaskItem scp = wkLifeManageDao.get(WkLifeTaskItem.class, id);
		if (scp != null) {
			scp.setState("2");
			WkLifeTaskTemp st = new WkLifeTaskTemp();
			st.setCode(scp.getCode());
			st.setSampleCode(scp.getSampleCode());
			st.setLabCode(scp.getLabCode());
			st.setSampleType(scp.getSampleType());
			st.setProductId(scp.getProductId());
			st.setProductName(scp.getProductName());
			st.setReportDate(scp.getReportDate());
			st.setSampleType(scp.getSampleType());
			st.setPatientName(scp.getPatientName());
			st.setSampleType(scp.getSampleType());
			if (scp.getSampleNum() != null && scp.getSampleConsume() != null) {
				st.setSampleNum(scp.getSampleNum() - scp.getSampleConsume());
			}
			st.setState("1");
			wkLifeManageDao.saveOrUpdate(st);

		}
	}

	/**
	 * @return
	 * @throws Exception
	 * 
	 * @Title: changeStateForLibrary
	 * @Description: TODO(改变文库结果数据信息的state)
	 * @param @param itemDataJson    设定文件
	 * @return void    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-8-2 上午11:35:56
	 * @throws
	 */
	public Boolean changeStateForLibrary(String[] idS) {
		
		Boolean f = true;
		int num = 0;
		for (int i = 0; i < idS.length; i++) {
			String id = idS[i];
			// 查找数据结果信息
			SampleWkLifeInfo scp = wkLifeManageDao.get(SampleWkLifeInfo.class, id);
			// 1. 查找对应结果数据的下一步流向ID
			String nextFlowId = scp.getNextFlowId();
			// 2. 查找对应的结果数据编号
			String libCode = scp.getCode();
			// 3. 对应模块查找相应的文库编号
			// 3.1. 去富集查找是否有对应的数据
			if (null == nextFlowId) {
				return f == false;
			}
			if ("0037".equals(nextFlowId)) {
				num = wkLifeManageDao.serchPollingTastTemp(libCode);
				if (num == 1) {
					// 对应的结果数据的state设置为“0”
					scp.setState("0");
					wkLifeManageDao.saveOrUpdate(scp);
					// 将数据插入返回左侧列表中
					setIntolibraryRollback(scp);
					return f;
				} else {
					return f == false;
				}
			}
			// 3.2. 去富集接受查找是否有对应的数据
			if ("0015".equals(nextFlowId)) {
				num = wkLifeManageDao.serchPollingReceiveTemp(libCode);
				if (num == 1) {
					// 对应的结果数据的state设置为“0”
					scp.setState("0");
					wkLifeManageDao.saveOrUpdate(scp);
					setIntolibraryRollback(scp);
					return f;
				} else {
					return f == false;
				}
			}
			// 3.3. 去文库混合查找是否有对应的数据
			if ("0032".equals(nextFlowId)) {
				num = wkLifeManageDao.serchWkBlendTastTemp(libCode);
				if (num == 1) {
					// 对应的结果数据的state设置为“0”
					scp.setState("0");
					wkLifeManageDao.saveOrUpdate(scp);
					setIntolibraryRollback(scp);
					return f;
				} else {
					return f == false;
				}
			}

		}
		return f;
		
	}

	/**
	 * @param scp
	 * 
	 * @Title: setIntolibraryRollback
	 * @Description: TODO(将回退后的信息插入到文库回退表里)
	 * @param     设定文件
	 * @return void    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-8-2 下午4:07:40
	 * @throws
	 */
	private void setIntolibraryRollback(SampleWkLifeInfo scp) {
		WkLifeTastkodifyTemp wkTastkodifyTemp = new WkLifeTastkodifyTemp();
		wkTastkodifyTemp.setAcceptDate(scp.getAcceptDate());
		wkTastkodifyTemp.setClassify(scp.getClassify());
		wkTastkodifyTemp.setCode(scp.getCode());
		wkTastkodifyTemp.setColCode(scp.getColCode());
		wkTastkodifyTemp.setCounts(scp.getCounts());
		wkTastkodifyTemp.setIdCard(scp.getIdCard());
		wkTastkodifyTemp.setInspectDate(scp.getInspectDate());
		wkTastkodifyTemp.setIsZkp(scp.getIsZkp());
		wkTastkodifyTemp.setLabCode(scp.getLabCode());
		wkTastkodifyTemp.setName(scp.getName());
		wkTastkodifyTemp.setNote(scp.getNote());
		wkTastkodifyTemp.setOrderId(scp.getOrderId());
		wkTastkodifyTemp.setPatientName(scp.getPatientName());
		wkTastkodifyTemp.setPhone(scp.getPhone());
		wkTastkodifyTemp.setProductId(scp.getProductId());
		wkTastkodifyTemp.setProductName(scp.getProductName());
		wkTastkodifyTemp.setReportDate(scp.getReportDate());
		wkTastkodifyTemp.setRowCode(scp.getRowCode());
		wkTastkodifyTemp.setSampleCode(scp.getSampleCode());
		wkTastkodifyTemp.setSampleInfo(scp.getSampleInfo());
		wkTastkodifyTemp.setSampleNum(scp.getSampleNum());
		wkTastkodifyTemp.setSampleType(scp.getSampleType());
		wkTastkodifyTemp.setSequenceFun(scp.getSequenceFun());
		wkTastkodifyTemp.setSplitCode(scp.getSplitCode());
		wkTastkodifyTemp.setState(scp.getState());
		wkTastkodifyTemp.setTechJkServiceTask(scp.getTechJkServiceTask());
		wkTastkodifyTemp.setTjItem(scp.getTjItem());
		wkTastkodifyTemp.setUnit(scp.getUnit());
		wkTastkodifyTemp.setVolume(scp.getVolume());
		wkTastkodifyTemp.setNextFlow(scp.getNextFlow());
		wkTastkodifyTemp.setNextFlowId(scp.getNextFlowId());
		wkTastkodifyTemp.setInsertSize(scp.getInsertSize());
		wkTastkodifyTemp.setIndexa(scp.getIndexa());
		wkTastkodifyTemp.setPcrRatio(scp.getPcrRatio());
		wkTastkodifyTemp.setDxpdSumTotal(scp.getDxpdSumTotal());
		wkTastkodifyTemp.setLoopNum(scp.getLoopNum());
		wkTastkodifyTemp.setWkSumTotal(scp.getWkSumTotal());
		wkTastkodifyTemp.setWkVolume(scp.getWkVolume());
		wkTastkodifyTemp.setDxpdVolume(scp.getDxpdVolume());
		wkTastkodifyTemp.setReason(scp.getReason());
		wkTastkodifyTemp.setState("1");
		wkLifeManageDao.saveOrUpdate(wkTastkodifyTemp);
	}
}
