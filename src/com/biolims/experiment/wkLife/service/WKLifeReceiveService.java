package com.biolims.experiment.wkLife.service;

import java.util.ArrayList;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;


import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;



import com.biolims.experiment.wkLife.dao.WKLifeReceiveDao;
import com.biolims.experiment.wkLife.dao.WKLifeSampleTaskDao;
import com.biolims.experiment.wkLife.model.WkLifeReceive;
import com.biolims.experiment.wkLife.model.WkLifeReceiveItem;
import com.biolims.experiment.wkLife.model.WkLifeReceiveTemp;
import com.biolims.experiment.wkLife.model.WkLifeTaskTemp;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.dao.SampleReceiveDao;
import com.biolims.sample.model.SampleInfo;

import com.biolims.sample.service.SampleInputService;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class WKLifeReceiveService {
	@Resource
	private WKLifeReceiveDao wKLifeReceiveDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private WKLifeSampleTaskDao wKLifeSampleTaskDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleReceiveDao sampleReceiveDao;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findWKReceiveList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return wKLifeReceiveDao.selectWKReceiveList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(WkLifeReceive i) throws Exception {

		wKLifeReceiveDao.saveOrUpdate(i);

	}

	public WkLifeReceive get(String id) {
		WkLifeReceive wKReceive = commonDAO.get(WkLifeReceive.class, id);
		return wKReceive;
	}

	public Map<String, Object> findWKReceiveItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = wKLifeReceiveDao.selectWKReceiveItemList(scId,
				startNum, limitNum, dir, sort);
		List<WkLifeReceiveItem> list = (List<WkLifeReceiveItem>) result.get("list");
		return result;
	}

	// 查询文库构建临时表
	public Map<String, Object> findWKTempList(Map<String, String> map2Query,
			Integer startNum, Integer limitNum, String dir, String sort,
			String groups, String userId) throws Exception {
		Map<String, Object> result = wKLifeReceiveDao.selectWKTempList(map2Query,
				startNum, limitNum, dir, sort, groups, userId);
		List<WkLifeTaskTemp> list = (List<WkLifeTaskTemp>) result.get("list");
		// Long total = (Long) result.get("total");
		// Map<String, Object> map = new HashMap<String, Object>();
		// User user = (User) ServletActionContext.getRequest().getSession()
		// .getAttribute(SystemConstants.USER_SESSION_KEY);
		// List<WkTaskTemp> List = new ArrayList<WkTaskTemp>();// 存放总的样本
		// List<WkTaskTemp> list2 = new ArrayList<WkTaskTemp>();// 存放科研样本
		// List<WkTaskTemp> list3 = new ArrayList<WkTaskTemp>();// 存放临床产品样本
		// for (WkTaskTemp w : list) {
		// if (w.getIsZkp() != null && w.getIsZkp().equals("1")) {
		// List.add(w);
		// } else {
		// if (w.getTechJkServiceTask() != null) {// 科研任务单
		// // 获取到分配实验负责人
		// User tuser = w.getTechJkServiceTask().getExperimentLeader();
		// // 当前登录用户是否为实验负责人
		// if (user.getId().equals(tuser.getId())) {
		// list2.add(w);
		// }
		// } else {// 临床
		// SampleReceiveItem s = sampleReceiveDao.selectItemByCode(w
		// .getSampleCode());
		// if (s != null) {
		// if (s.getSampleReceive().getAcceptUserGroup() != null) {
		// // 获取到分配实验组的id
		// CharSequence c = s.getSampleReceive()
		// .getAcceptUserGroup().getId();
		// // 当前登录用户所在实验组是否包含分配的实验组
		// boolean b = groups.contains(c);
		// if (b) {
		// list3.add(w);
		// }
		// }
		// }
		// }
		// }
		// }
		// // list2排序
		// ComparatorChain chain = new ComparatorChain();
		// // 根据某个字段把list排序
		// chain.addComparator(new BeanComparator(
		// "techJkServiceTask.sequenceBillDate"), false);// true,fase正序反序
		// Collections.sort(list2, chain);
		// List.addAll(list2);
		// List.addAll(list3);
		// map.put("list", List);
		// map.put("total", total);
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWKReceiveItem(WkLifeReceive sc, String itemDataJson)
			throws Exception {
		List<WkLifeReceiveItem> saveItems = new ArrayList<WkLifeReceiveItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			WkLifeReceiveItem scp = new WkLifeReceiveItem();
			// 将map信息读入实体类
			scp = (WkLifeReceiveItem) wKLifeReceiveDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setWkReceive(sc);
			// 将选中的样本状态，修改为无效
			// List<WkReceiveTemp> tmp = wKLifeReceiveDao.findWKAcceptTempList();
			// for (WkReceiveTemp tmpp : tmp) {
			// if (tmpp.getCode().equals(scp.getCode())) {
			// tmpp.setState("2");
			// wKLifeReceiveDao.saveOrUpdate(tmpp);
			// }
			// }
			saveItems.add(scp);
		}
		wKLifeReceiveDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWKReceiveItem(String[] ids) throws Exception {
		for (String id : ids) {
			WkLifeReceiveItem scp = wKLifeReceiveDao.get(WkLifeReceiveItem.class, id);
			wKLifeReceiveDao.delete(scp);
		}
	}

	// 审批完成
	public void changeState(String applicationTypeActionId, String id) {
		WkLifeReceive pr = wKLifeReceiveDao.get(WkLifeReceive.class, id);
		pr.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		pr.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		// sct.setConfirmUser(user);
		// sct.setConfirmDate(new Date());

		wKLifeReceiveDao.update(pr);

		String prid = pr.getId();
		try {
			List<WkLifeReceiveItem> pri = wKLifeReceiveDao.findWKReceiveItemList(prid);
			for (WkLifeReceiveItem prii : pri) {
				if (prii.getMethod().equals("1")) {
					WkLifeTaskTemp wtt = new WkLifeTaskTemp();
					prii.setState("1");
					sampleInputService.copy(wtt, prii);
					// UfTaskTemp uft = new UfTaskTemp();
					// uft.setCode(prii.getCode());
					// uft.setSampleCode(prii.getSampleCode());
					// uft.setPatientName(prii.getPatientName());
					// uft.setProductId(prii.getProductId());
					// uft.setProductName(prii.getProductName());
					// if (prii.getInspectDate() != null
					// && !prii.getInspectDate().equals("")) {
					// SimpleDateFormat sim = new SimpleDateFormat(
					// "yyyy-MM-dd");
					// String str = prii.getInspectDate();
					// Date d = null;
					// try {
					// d = sim.parse(str);
					// } catch (Exception e) {
					// e.printStackTrace();
					// }
					// uft.setInspectDate(d);
					// } else {
					// uft.setInspectDate(null);
					// }
					//
					// uft.setAcceptDate(prii.getAcceptDate());
					// uft.setIdCard(prii.getIdCard());
					// uft.setPhone(prii.getPhone());
					// uft.setOrderId(prii.getOrderId());
					// uft.setSequenceFun(prii.getSequenceFun());
					// uft.setNote(prii.getNote());
					// uft.setState("1");
					// uft.setClassify(prii.getClassify());
					// wKLifeSampleTaskDao.saveOrUpdate(uft);

					// List<WkReceiveTemp> list = wKLifeReceiveDao
					// .findWKAcceptTempBycode(prii.getCode());
					// for (WkReceiveTemp temp : list) {
					// temp.setState("2");
					// }
					WkLifeReceiveTemp temp = commonDAO.get(WkLifeReceiveTemp.class,
							prii.getTempId());
					if (temp != null) {
						temp.setState("2");
					}
				}

				// 审批完成后，改变SampleInfo中原始样本的状态为“待文库构建”
				SampleInfo sf = sampleInfoMainDao
						.findSampleInfo(prii.getCode());
				if (sf != null) {
					sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_WK_NEW);
					sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_WK_NEW_NAME);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(WkLifeReceive sc, Map jsonMap) throws Exception {
		if (sc != null) {
			wKLifeReceiveDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("wKReceiveItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveWKReceiveItem(sc, jsonStr);
			}
		}
	}

	// 查询临时表
	public Map<String, Object> findWKAcceptTempList(Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = wKLifeReceiveDao.findWKAcceptTempList(
				startNum, limitNum, dir, sort);
		List<WkLifeReceiveTemp> list = (List<WkLifeReceiveTemp>) result.get("list");
		result.put("list", list);
		return result;
	}
/**
 * 
* @Title: findWKTastModifyTempList 
* @Description: TODO(查询文库取消左侧列表信息) 
* @param @param map2Query
* @param @param startNum
* @param @param limitNum
* @param @param dir
* @param @param sort
* @param @param groups
* @param @param userId
* @param @return
* @param @throws Exception    设定文件 
* @return Map<String,Object>    返回类型 
* @author  zhiqiang.yang@biolims.cn 
* @date 2017-8-4 上午10:57:50 
* @throws
 */
	public Map<String, Object> findWKTastModifyTempList(
			Map<String, String> map2Query, int startNum, int limitNum,
			String dir, String sort, String groups, String userId)
			throws Exception {
		Map<String, Object> result = wKLifeReceiveDao.selectWKTastModifyTempList(map2Query,
				startNum, limitNum, dir, sort, groups, userId);
		List<WkLifeTaskTemp> list = (List<WkLifeTaskTemp>) result.get("list");
		return result;
	}
}
