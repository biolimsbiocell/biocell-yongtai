package com.biolims.experiment.wkLife.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.dna.model.DnaTaskItem;

import com.biolims.experiment.wkLife.model.WkLifeTask;
import com.biolims.experiment.wkLife.model.WkLifeTaskCos;
import com.biolims.experiment.wkLife.model.WkLifeTaskItem;
import com.biolims.experiment.wkLife.model.WkLifeTaskReagent;
import com.biolims.experiment.wkLife.model.WkLifeTaskTemplate;


@Repository
public class NewWKLifeSampleTaskDao extends CommonDAO {

	

	
	//查询实验模板ID
	public List<WkLifeTask> selTemplat(String formId) throws Exception {
		String hql = "from WkLifeTask t where 1=1 and id = '" + formId
				+ "'";
		List<WkLifeTask> list =  this.getSession().createQuery(hql).list();
		return list;
	}

	//进入页面后查询明细
	public List<WkLifeTaskItem> selTaskItem(String formId) throws Exception {
		String hql = "from WkLifeTaskItem t where 1=1 and wk.id = '" + formId
				+ "'";
		List<WkLifeTaskItem> list =  this.getSession().createQuery(hql).list();
		return list;
	}
	
	//根据主表ID查询模板步骤明细
	public List<WkLifeTaskTemplate> selTemplateItem(String formId) throws Exception {
		String hql = "from WkLifeTaskTemplate t where 1=1 and t.wk = '" + formId
				+ "' order by t.code";
		List<WkLifeTaskTemplate> list =  this.getSession().createQuery(hql).list();
		return list;
	}
	//根据主表ID查询模板试剂明细
	public List<WkLifeTaskReagent> selReagent(String formId) throws Exception {
		String hql = "from WkTaskReagent t where 1=1 and t.wk = '" + formId
				+ "'";
		List<WkLifeTaskReagent> list =  this.getSession().createQuery(hql).list();
		return list;
	}
	//根据主表ID查询模板设备明细
	public List<WkLifeTaskCos> selCos(String formId) throws Exception {
		String hql = "from WkTaskCos t where 1=1 and t.wk = '" + formId
				+ "'";
		List<WkLifeTaskCos> list =  this.getSession().createQuery(hql).list();
		return list;
	}
	//根据主表id判断结果是否已经生成
	public List selResult(String formId) throws Exception {
		String hql = "from SampleWkLifeInfo t where 1=1 and t.wk = '" + formId
				+ "'";
		List list =  this.getSession().createQuery(hql).list();
		return list;
	}
	
	// 根木样本编号查询待入库样本findSampleInTempByCode
	public List<WkLifeTaskItem> findTaskItemByCode(String code, String formId)
			throws Exception {
		String hql = "from WkLifeTaskItem t where 1=1 and t.code='" + code
				+ "' and t.wk= '" + formId + "'";
		List<WkLifeTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

}
