package com.biolims.experiment.wkLife.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.dao.CommonDAO;
import com.biolims.dao.BaseHibernateDao;

import com.biolims.experiment.wkLife.model.SampleWkLifeInfo;
import com.biolims.experiment.wkLife.model.WkLifeTaskItem;

@Repository
@SuppressWarnings("unchecked")
public class WkLifeManageDao extends BaseHibernateDao {
	public Map<String, Object> selectWkManageList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from SampleWkLifeInfo where 1=1 ";
		if (mapForQuery.size() > 0) {
			key = map2where(mapForQuery);
		} else {
			key = " and state is null";
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SampleWkLifeInfo> list = new ArrayList<SampleWkLifeInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectWkItemManageList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from WkLifeTaskItem where 1=1 and state='1'";
		if (mapForQuery.size() > 0) {
			key = map2where(mapForQuery);
		}
		// else{
		// key=" and state is null";
		// }
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<WkLifeTaskItem> list = new ArrayList<WkLifeTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	/**
	 * @return
	 * 
	 * @Title: serchPollingTastTemp
	 * @Description: TODO(查找富集对应的数据)
	 * @param @param libCode    设定文件
	 * @return int    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-8-2 下午3:31:04
	 * @throws
	 */
	public int serchPollingTastTemp(String libCode) {
		String sql = "SELECT COUNT(*) FROM pooling_receive_temp t WHERE t.code='"
				+ libCode + "'";
		Query query = this.getSession().createSQLQuery(sql);
		if (query.list().size() > 0) {
			return 1;
		}
		return 0;

	}

	/**
	 * 
	 * @Title: serchPollingReceiveTemp
	 * @Description: TODO(查找富集接受对应的数据)
	 * @param @param libCode
	 * @param @return    设定文件
	 * @return int    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-8-2 下午3:45:30
	 * @throws
	 */
	public int serchPollingReceiveTemp(String libCode) {
		String sql = "SELECT COUNT(*)  FROM pooling_task_temp  t WHERE t.code='"
				+ libCode + "'";
		Query query = this.getSession().createSQLQuery(sql);
		if (query.list().size() > 0) {
			return 1;
		}
		return 0;
	}

	/**
	 * 
	 * @Title: serchWkBlendTastTemp
	 * @Description: TODO(查找文库混合对应的数据)
	 * @param @param libCode
	 * @param @return    设定文件
	 * @return int    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-8-2 下午3:56:26
	 * @throws
	 */
	public int serchWkBlendTastTemp(String libCode) {
		String sql = "SELECT COUNT(*)  FROM wkLifeblend_task_temp  t WHERE t.code='"
				+ libCode + "'";
		Query query = this.getSession().createSQLQuery(sql);
		if (query.list().size() > 0) {
			return 1;
		}
		return 0;
	}
}