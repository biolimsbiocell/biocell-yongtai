package com.biolims.experiment.wkLife.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;

import com.biolims.experiment.wkLife.model.WkLifeReceive;
import com.biolims.experiment.wkLife.model.WkLifeReceiveItem;
import com.biolims.experiment.wkLife.model.WkLifeReceiveTemp;
import com.biolims.experiment.wkLife.model.WkLifeTaskItem;
import com.biolims.experiment.wkLife.model.WkLifeTaskTemp;

@Repository
@SuppressWarnings("unchecked")
public class WKLifeReceiveDao extends BaseHibernateDao {
	public Map<String, Object> selectWKReceiveList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from WkLifeReceive where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<WkLifeReceive> list = new ArrayList<WkLifeReceive>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectWKReceiveItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from WkLifeReceiveItem t where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and wkLifeReceive.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<WkLifeReceiveItem> list = new ArrayList<WkLifeReceiveItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 查询文库构建临时表
	public Map<String, Object> selectWKTempList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String groupId,
			String userId) throws Exception {
		// String hql = "from WkLifeTaskTemp where 1=1 and state='1'  and (userId='"
		// + userId + "' or  instr('" + groupId + "',groupId)>0) ";
		String hql = "from WkLifeTaskTemp where 1=1 and state='1' ";
		String key = "";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql).uniqueResult();
		List<WkLifeTaskTemp> list = new ArrayList<WkLifeTaskTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by  " + sort + " " + dir;
			} else {
				key = key + " order by sequenceBillDate desc";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 查询文库构建临时表，获取所有数据
	public List<WkLifeTaskTemp> findWKTempList(String code) throws Exception {
		String hql = "from WkLifeTaskTemp where 1=1 and state='1' and code='"
				+ code + "'";
		List<WkLifeTaskTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 查询样本子表
	public List<WkLifeReceiveItem> findWKReceiveItemList(String scId)
			throws Exception {
		String hql = "from WkLifeReceiveItem where 1=1 and method='1' and wkReceive.id='"
				+ scId + "'";
		List<WkLifeReceiveItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<WkLifeTaskItem> findWkTaskItemmList(String scId) throws Exception {
		String hql = "from WkLifeTaskItem where 1=1 and wk.id='" + scId + "'";
		List<WkLifeTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 查询临时表样本
	public List<WkLifeReceiveTemp> findWKAcceptTempList() throws Exception {
		String hql = "from WkLifeReceiveTemp where 1=1 and state='1'";
		List<WkLifeReceiveTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 查询临时表样本
	public Map<String, Object> findWKAcceptTempList(Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = "from WkLifeReceiveTemp where 1=1 and state='1'";
		String key = "";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<WkLifeReceiveTemp> list = new ArrayList<WkLifeReceiveTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by code DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据样本编号查询临时表样本 改变状态
	public List<WkLifeReceiveTemp> findWKAcceptTempBycode(String code)
			throws Exception {
		String hql = "from WkLifeReceiveTemp where 1=1 and state='1' and code='"
				+ code + "'";
		List<WkLifeReceiveTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 
	 * @Title: selectWKTastModifyTempList
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param @param map2Query
	 * @param @param startNum
	 * @param @param limitNum
	 * @param @param dir
	 * @param @param sort
	 * @param @param groups
	 * @param @param userId
	 * @param @return    设定文件
	 * @return Map<String,Object>    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-8-4 上午11:03:06
	 * @throws
	 */
	public Map<String, Object> selectWKTastModifyTempList(
			Map<String, String> map2Query, Integer startNum, Integer limitNum,
			String dir, String sort, String groups, String userId) throws Exception {
		// String hql = "from WkLifeTaskTemp where 1=1 and state='1'  and (userId='"
		// + userId + "' or  instr('" + groupId + "',groupId)>0) ";
		String hql = "from WkLifeTastkodifyTemp where 1=1 and state='1' ";
		String key = "";
		if (map2Query != null)
			key = map2where(map2Query);
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql).uniqueResult();
		List<WkLifeTaskTemp> list = new ArrayList<WkLifeTaskTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by  " + sort + " " + dir;
			} else {
				key = key + " order by sequenceBillDate desc";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

}