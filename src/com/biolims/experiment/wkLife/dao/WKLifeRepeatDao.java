package com.biolims.experiment.wkLife.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;

import com.biolims.experiment.wkLife.model.WkLifeAbnormalBack;


@Repository
@SuppressWarnings("unchecked")
public class WKLifeRepeatDao extends BaseHibernateDao {

	/*
	 * public Map<String, Object> selectWKRepeatList(String scId, Integer
	 * startNum, Integer limitNum, String dir, String sort) throws Exception {
	 * String hql = "from SampleWkInfo where 1=1 and method = 2"; String key =
	 * ""; if (scId != null) key = key + " and wk.id='" + scId + "'"; Long total
	 * = (Long) this.getSession().createQuery("select count(*) " + hql +
	 * key).uniqueResult(); List<SampleWkInfo> list = new
	 * ArrayList<SampleWkInfo>(); if (total > 0) { if (dir != null &&
	 * dir.length() > 0 && sort != null && sort.length() > 0) { if
	 * (sort.indexOf("-") != -1) { sort = sort.substring(0, sort.indexOf("-"));
	 * } key = key + " order by " + sort + " " + dir; } if (startNum == null ||
	 * limitNum == null) { list = this.getSession().createQuery(hql +
	 * key).list(); } else { list = this.getSession().createQuery(hql +
	 * key).setFirstResult(startNum).setMaxResults(limitNum).list(); } }
	 * Map<String, Object> result = new HashMap<String, Object>();
	 * result.put("total", total); result.put("list", list); return result; }
	 */
	public Map<String, Object> selectSampleWkInfo(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String hql = "from WkLifeAbnormalBack t where 1=1 and state='4'";
		String key = "";
		if (mapForQuery.size() > 0) {
			key = map2where(mapForQuery);
		}
		// else {
		// key = " and (t.isExecute is null or t.isExecute='0')";
		// }
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<WkLifeAbnormalBack> list = new ArrayList<WkLifeAbnormalBack>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
}