package com.biolims.experiment.wkLife.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;

import com.biolims.experiment.wkLife.model.SampleWkLifeInfo;
import com.biolims.experiment.wkLife.model.SampleWkLifeInfoCtDNA;
import com.biolims.experiment.wkLife.model.SampleWkLifeInfomRNA;
import com.biolims.experiment.wkLife.model.SampleWkLifeInforRNA;
import com.biolims.experiment.wkLife.model.WkLifeTask;
import com.biolims.experiment.wkLife.model.WkLifeTaskCos;
import com.biolims.experiment.wkLife.model.WkLifeTaskItem;
import com.biolims.experiment.wkLife.model.WkLifeTaskModifyInfo;
import com.biolims.experiment.wkLife.model.WkLifeTaskReagent;
import com.biolims.experiment.wkLife.model.WkLifeTaskTemp;
import com.biolims.experiment.wkLife.model.WkLifeTaskTemplate;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;

@Repository
@SuppressWarnings("unchecked")
public class WKLifeSampleTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectWKList(Map<String, String> mapForQuery,
			Integer startNum, Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from WkLifeTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<WkLifeTask> list = new ArrayList<WkLifeTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id desc";

			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public List<WkLifeTaskItem> selectWkTaskItemList(String scId) throws Exception {
		String hql = "from WkLifeTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and wk.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<WkLifeTaskItem> list = new ArrayList<WkLifeTaskItem>();
		list = this.getSession().createQuery(hql + key).list();
		return list;
	}

	public Map<String, Object> selectWKItemList(String scId, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = "from WkLifeTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and wk.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<WkLifeTaskItem> list = new ArrayList<WkLifeTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by orderNumber asc ";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectWKTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from WkLifeTaskTemplate where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and wk.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<WkLifeTaskTemplate> list = new ArrayList<WkLifeTaskTemplate>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by code ASC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	
	public Map<String, Object> selectWKReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from WkLifeTaskReagent where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and wk.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<WkLifeTaskReagent> list = new ArrayList<WkLifeTaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据关联itemId查询Reagent
	public Map<String, Object> selectReagentItemListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		String hql = "from WkLifeTaskReagent where 1=1 and itemId='" + itemId + "'";
		String key = "";
		if (scId != null)
			key = key + " and wk.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<ReagentItem> list = new ArrayList<ReagentItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据选中的步骤查询相关的试剂明细
	public Map<String, Object> setReagent(String id, String code) {
		String hql = "from WkLifeTaskReagent t where 1=1 and t.wk='" + id
				+ "' and t.itemId='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<WkLifeTaskReagent> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/*
	 * 根据主表ID查询试剂明细
	 */
	public List<WkLifeTaskReagent> setReagentList(String code) {
		String hql = "from WkLifeTaskReagent where 1=1 and wk='" + code + "'";
		List<WkLifeTaskReagent> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据选中的步骤查询相关的设备明细
	public Map<String, Object> setCos(String id, String code) {
		String hql = "from WkLifeTaskCos t where 1=1 and t.wk='" + id
				+ "' and t.itemId='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<WkLifeTaskCos> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据关联的item查询
	public Map<String, Object> selectCosItemListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		String hql = "from WkLifeTaskCos where 1=1 and itemId='" + itemId + "'";
		String key = "";
		if (scId != null)
			key = key + " and wk.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<WkLifeTaskCos> list = new ArrayList<WkLifeTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectWKCosList(String scId, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = "from WkLifeTaskCos where 1=1 ";

		String key = "";

		if (scId != null)
			key = key + " and wk.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<WkLifeTaskCos> list = new ArrayList<WkLifeTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectWKResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SampleWkLifeInfo where 1=1";
		String key = "";
		if (scId != null)
			key = key + " and wk.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleWkLifeInfo> list = new ArrayList<SampleWkLifeInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by productId asc ";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectWKResultCtDNAList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SampleWkLifeInfoCtDNA where 1=1";
		String key = "";
		if (scId != null)
			key = key + " and wkTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleWkLifeInfoCtDNA> list = new ArrayList<SampleWkLifeInfoCtDNA>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by productId asc ";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectWKResultrRNAList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SampleWkLifeInforRNA where 1=1";
		String key = "";
		if (scId != null)
			key = key + " and wkTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleWkLifeInforRNA> list = new ArrayList<SampleWkLifeInforRNA>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by productId asc ";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectWKResultmRNAList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SampleWkLifeInfomRNA where 1=1";
		String key = "";
		if (scId != null)
			key = key + " and wkTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleWkLifeInfomRNA> list = new ArrayList<SampleWkLifeInfomRNA>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by productId asc ";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 查询文库构建临时表
	public List<WkLifeTaskTemp> searchWKTempList() {
		String hql = "from WkLifeTaskTemp where 1=1";
		List<WkLifeTaskTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据ID相关样本的数量
	public Long selectCountMax(String code) {
		String hql = "from WkLifeTaskItem t where 1=1 and t.wk.id='" + code + "'";
	
		Long list = (Long) this.getSession()
				.createQuery("select count(t.id) " + hql).uniqueResult();
		return list;
	}
     
	// 查询结果子表中下一步是样本入库的数据
	public List<SampleWkLifeInfo> searchWKSampleInfoList(String id) {
		String hql = "from SampleWkLifeInfo where 1=1 and next='3'  and wk.id='"
				+ id + "'";
		List<SampleWkLifeInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 查询结果子表中合格的数据
	public List<SampleWkLifeInfo> searchWKResultList(String id) {
		String hql = "from SampleWkInfo where 1=1 and result='0' and wk.id='"
				+ id + "'";
		List<SampleWkLifeInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 查询结果表中的所有的数据
	public List<SampleWkLifeInfo> findWKResultList(String id) {
		String hql = "from SampleWkLifeInfo where 1=1 and  wk.id='" + id + "'";
		List<SampleWkLifeInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 查询结果表中的异常数据
	public List<SampleWkLifeInfo> selectWKResultList(String id) {
		String hql = "from SampleWkLifeInfo where 1=1 and result='1' and  wk.id='"
				+ id + "'";
		List<SampleWkLifeInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 查询结果表中的异常数据，进行修改
	public List<SampleWkLifeInfo> findWKResult(String id) {
		String hql = "from SampleWkLifeInfo where 1=1 and result='1' and  code='"
				+ id + "'";
		List<SampleWkLifeInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据ID相关样本的提取次数
	public Long selectCount(String code) {
		String hql = "from WkLifeTaskItem  where 1=1 and code='" + code + "'";
		Long list = (Long) this.getSession()
				.createQuery("select count(code)" + hql).uniqueResult();
		return list;
	}

	// 获取主表index的值
	public List<WkLifeTask> getIndexValue(String id) {
		String hql = "from WkLifeTask  where 1=1 and id='" + id + "'";
		List<WkLifeTask> list = this.getSession().createQuery(hql).list();
		// System.out.print(list);
		return list;
	}

	/**
	 * 获取最近一次创建的文库构建明细表index的最大值
	 * 
	 * @return
	 */
	public Integer getIndexNum() {
		String hql = "from WkLifeTaskItem t where t.wk=(select k.id from WkLifeTask k where k.createDate=(select max(createDate) from WkLifeTask))";
		// String hql = "from WkTaskItem  where 1=1";
		Integer list = (Integer) this.getSession()
				.createQuery("select max(indexa)" + hql).uniqueResult();
		// System.out.print(list);
		return list;
	}

	// 获取字典表index的最大值
	public String getIndexMax() {
		String hql = "from DicType  where 1=1 and type = 'indexa'";
		String list = (String) this.getSession()
				.createQuery("select max(name)" + hql).uniqueResult();
		// System.out.print(list);
		return list;
	}

	// 根据主数据ID查询明细数据
	public List<WkLifeTaskItem> setWKItemToManage(String code) {
		String hql = "from WkLifeTaskItem t where 1=1 and t.wk='" + code + "'";
		List<WkLifeTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据样本编号获取相关文库构建结果的集合
	 * 
	 * @param scode
	 * @return
	 */
	public List<SampleWkLifeInfo> getWKSampleInfoBySampleCode(String scode) {
		String hql = "from SampleWkLifeInfo where 1=1 and wkCode='" + scode + "'";
		List<SampleWkLifeInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据文库号查询
	 * 
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 */
	public SampleWkLifeInfo getWKSampleInfoByWkCode(String scode) {
		String hql = "from SampleWkLifeInfo where 1=1 and wkCode='" + scode + "'";
		List<SampleWkLifeInfo> list = this.getSession().createQuery(hql).list();
		if (list.size() > 0)
			return list.get(0);
		return null;
	}

	// 科技服务
	public Map<String, Object> selectTechJkServiceTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		// String hql =
		// " from TechJkServiceTask t where 1=1 and t.orderType = '0'";//任务单类型为：建库
		String hql = " from TechJkServiceTask t where 1=1";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<TechJkServiceTask> list = new ArrayList<TechJkServiceTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectTechServiceTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from TechJkServiceTaskItem where 1=1 and state = '1'";
		String key = "";
		if (scId != null)
			key = key + " and techJkServiceTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<TechJkServiceTaskItem> list = new ArrayList<TechJkServiceTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据科技服务主表id获取明细表信息
	public Map<String, Object> selectTechTaskItemByTaskId(String taskId) {
		String hql = "from TechJkServiceTaskItem t where 1=1 and t.techJkServiceTask='"
				+ taskId + "' and t.state = '1'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<TechJkServiceTaskItem> list = this.getSession().createQuery(hql)
				.list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 改变建库样本明细表的状态
	public List<TechJkServiceTaskItem> changForState() {
		String hql = "from TechJkServiceTaskItem";
		List<TechJkServiceTaskItem> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	// 根据项目号查询任务单
	public TechJkServiceTask selectTechTaskByProject(String projectId) {
		String hql = "from TechJkServiceTask where projectId='" + projectId
				+ "'";
		List<TechJkServiceTask> list = this.getSession().createQuery(hql)
				.list();
		if (list.size() > 0)
			return list.get(0);
		return null;
	}

	// 根据主表编号查询结果表信息
	public List<SampleWkLifeInfo> selectSampleWkInfo(String id) {
		String hql = "from SampleWkLifeInfo where 1=1 and wk.id='" + id + "'";
		List<SampleWkLifeInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<SampleWkLifeInfo> selectSampleWkInfoById(String id) {
		String hql = "from SampleWkLifeInfo where (submit is null or submit='') and wk.id='"
				+ id + "'";
		List<SampleWkLifeInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	public List<WkLifeTaskModifyInfo> selectWkTastModifyInfoById(String id) {
		String hql = "from WkLifeTaskModifyInfo where  1=1 and wk.id='"
				+ id + "'";
		List<WkLifeTaskModifyInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	// 根据样本编号查询
	public List<WkLifeTaskModifyInfo> selectWkTastModifyInfoByIds(String[] codes) {

		String insql = "''";
		for (String code : codes) {

			insql += ",'" + code + "'";

		}

		String hql = "from WkLifeTaskModifyInfo t where 1=1 and id in ("
				+ insql + ")";
		List<WkLifeTaskModifyInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据样本编号查询
	public List<SampleWkLifeInfo> selectSampleWkInfoByIds(String[] codes) {

		String insql = "''";
		for (String code : codes) {

			insql += ",'" + code + "'";

		}

		String hql = "from SampleWkLifeInfo t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<SampleWkLifeInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据主表编号查询结果表信息
	public List<SampleWkLifeInfoCtDNA> selectSampleWkInfoCtDNA(String id) {
		String hql = "from SampleWkLifeInfoCtDNA where 1=1 and wkTask.id='" + id
				+ "'";
		List<SampleWkLifeInfoCtDNA> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	public List<SampleWkLifeInfoCtDNA> selectSampleWkInfoCtDNAById(String id) {
		String hql = "from SampleWkLifeInfoCtDNA where (submit is null or submit='') and wkTask.id='"
				+ id + "'";
		List<SampleWkLifeInfoCtDNA> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	public List<SampleWkLifeInfoCtDNA> selectSampleWkInfoCtDNAByIds(String[] codes) {
		String insql = "''";
		for (String code : codes) {

			insql += ",'" + code + "'";

		}
		String hql = "from SampleWkLifeInfoCtDNA where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<SampleWkLifeInfoCtDNA> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	// 根据主表编号查询结果表信息
	public List<SampleWkLifeInforRNA> selectSampleWkInforRNA(String id) {
		String hql = "from SampleWkLifeInforRNA where 1=1 and wkTask.id='" + id
				+ "'";
		List<SampleWkLifeInforRNA> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<SampleWkLifeInforRNA> selectSampleWkInforRNAById(String id) {
		String hql = "from SampleWkLifeInforRNA where submit is null  and wkTask.id='"
				+ id + "'";
		List<SampleWkLifeInforRNA> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<SampleWkLifeInforRNA> selectSampleWkInforRNAByIds(String[] codes) {
		String insql = "''";
		for (String code : codes) {

			insql += ",'" + code + "'";

		}
		String hql = "from SampleWkLifeInforRNA where submit is null  and wkTask.id in ("
				+ insql + ")";
		List<SampleWkLifeInforRNA> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据主表编号查询结果表信息
	public List<SampleWkLifeInfomRNA> selectSampleWkInfomRNA(String id) {
		String hql = "from SampleWkLifeInfomRNA where 1=1 and wkTask.id='" + id
				+ "'";
		List<SampleWkLifeInfomRNA> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<SampleWkLifeInfomRNA> selectSampleWkInfomRNAById(String id) {
		String hql = "from SampleWkLifeInfomRNA where  submit is null and wkLifeTask.id='"
				+ id + "'";
		List<SampleWkLifeInfomRNA> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<SampleWkLifeInfomRNA> selectSampleWkInfomRNAByIds(String[] codes) {
		String insql = "''";
		for (String code : codes) {

			insql += ",'" + code + "'";

		}
		String hql = "from SampleWkLifeInfomRNA where  submit is null and wkTask.id in ("
				+ insql + ")";
		List<SampleWkLifeInfomRNA> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据主表id和实验室排序号查询明细信息
	public WkLifeTaskItem selWkTaskItem(String id, Integer orderNumber) {
		String hql = "from WkLifeTaskItem where wk.id='" + id
				+ "' and orderNumber=" + orderNumber + "";
		WkLifeTaskItem w = (WkLifeTaskItem) this.getSession().createQuery(hql)
				.uniqueResult();
		return w;
	}

	/**
	 * 富集查看的文库信息
	 * 
	 * @return
	 */
	public Map<String, Object> selectWKInfoList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String codes) {
		String key = " ";
		String hql = " from SampleWkLifeInfo where 1=1 ";
		if (mapForQuery.size() > 0) {
			key = map2where(mapForQuery);
		} else {
			key = " and code in (" + codes + ")";
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SampleWkLifeInfo> list = new ArrayList<SampleWkLifeInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by code desc";

			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	/**
	 * 根据富集ID和富集文库号查询富集前的文库号
	 * 
	 * @param id
	 * @param orderNumber
	 * @return
	 */
	public List<String> getPoolingTaskItemList(String id, String codes) {
		String hql = "from PoolingTaskItem where poolingTask='" + id
				+ "' and fjWkCode in (" + codes + ")";
		List<String> list = this.getSession()
				.createQuery("select distinct code " + hql).list();
		return list;
	}

	/**
	 * 
	 * @Title: selectWKTastModifyList
	 * @Description: TODO(获取文库取消数据信息列表)
	 * @param @param map2Query
	 * @param @param startNum
	 * @param @param limitNum
	 * @param @param dir
	 * @param @param sort
	 * @param @return    设定文件
	 * @return Map<String,Object>    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-8-3 下午5:07:29
	 * @throws
	 */

	@SuppressWarnings("unchecked")
	public Map<String, Object> selectWKTastModifyList(
			Map<String, String> map2Query, Integer startNum, Integer limitNum,
			String dir, String sort) {
		String key = " ";
		String hql = " from WkLifeTaskModify where 1=1";
		if (map2Query != null)
			key = map2where(map2Query);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<WkLifeTask> list = new ArrayList<WkLifeTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id desc";

			}
			if (startNum == null || startNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
/**
 * 
* @Title: selectWKTastModifyItemList 
* @Description: TODO(文库构建取消明细信息查询列表) 
* @param @param scId
* @param @param startNum
* @param @param limitNum
* @param @param dir
* @param @param sort
* @param @return    设定文件 
* @return Map<String,Object>    返回类型 
* @author  zhiqiang.yang@biolims.cn 
* @date 2017-8-4 下午3:19:41 
* @throws
 */
	public Map<String, Object> selectWKTastModifyItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort) {
		String hql = "from WkLifeTaskModifyInfo where 1=1";
		String key = "";
		if (scId != null)
			key = key + " and wk.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<WkLifeTaskItem> list = new ArrayList<WkLifeTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by acceptDate asc ";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
}