package com.biolims.experiment.production.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Repository;

import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.deviation.model.InfluenceProduct;
import com.biolims.equipment.model.Instrument;
import com.biolims.experiment.cell.passage.model.CellPassage;

import com.biolims.experiment.cell.passage.model.CellPassageTemplate;
import com.biolims.experiment.cell.passage.model.CellProducOperation;
import com.biolims.experiment.cell.passage.model.CellPassageItem;

import com.biolims.experiment.cell.passage.model.CellProductionRecord;
import com.biolims.experiment.cell.passage.model.CellproductCos;
import com.biolims.sample.model.SampleOrder;
import com.biolims.tra.sampletransport.model.SampleTransportItem;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class ProductionPlanDao extends BaseHibernateDao {
	public Map<String, Object> findCellPrimaryCultureTemplateList(Integer start, Integer length, String query,
			String col, String sort, String id) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CellProductionRecord where 1=1 and cellPassage.id='" + id + "'";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = " from CellProductionRecord where 1=1 and cellPassage.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by orderNum";
			}
			List<CellProductionRecord> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", "1");
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> newFindCellPrimaryCultureTemplateList(Integer start, Integer length, String query,
			String col, String sort, String id, String bottleOrBag, String infectionType, String dataS, String dataE,
			String[] dbbacx, String produceId, String pici) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String key = "";
		if ("".equals("produceId") || produceId != null) {
			key = "and endTime is null ";
		}
		// 已生产
		if (produceId.equals("1")) {
			key = "and endTime is not null ";

		}
		// 待生产
		if (produceId.equals("2")) {
			key = "and endTime is null ";
		}
		// 全部生产
		if (produceId.equals("3")) {
			key = "";
		}

		String countHql = "select count(*) from CellProductionRecord where 1=1 and cellPassage.state<>'1' ";
//		Long sumCount =(Long) getSession().createQuery(countHql+key).uniqueResult();
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}

		if (bottleOrBag != null && !"".equals(bottleOrBag)) {
			key += "and templeItem.bottleOrBag='" + bottleOrBag + "' ";
		}
		if (infectionType != null && !"".equals(infectionType)) {
			key += "and cellPassage.infectionType='" + infectionType + "' ";
		}
		if (pici != null && !"".equals(pici)) {
			key += "and cellPassage.batch='" + pici + "' ";
		}
		if (dataS != null && !"".equals(dataS) && dataE != null && !"".equals(dataE)) {
			key += " and str_to_date(planWorkDate,'%Y-%m-%d') >= str_to_date('" + dataS + "','%Y-%m-%d') "
					+ "and str_to_date(planWorkDate,'%Y-%m-%d') <= str_to_date('" + dataE + "','%Y-%m-%d') ";
		}
		if (dbbacx != null && dbbacx.length > 0) {
			String a = "";
			for (int d = 0; d < dbbacx.length; d++) {
				if ("".equals(a)) {
					a = " templeItem.name='" + dbbacx[d] + "' ";
				} else {
					a += "or templeItem.name='" + dbbacx[d] + "'";
				}
			}
			key += " and (" + a + ") ";
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();

		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();

			String hql = "from CellProductionRecord where 1=1  and cellPassage.state<>'1' ";
//			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
//				col = col.replace("-", ".");
//				key += " order by cellPassage.createDate desc,orderNum asc";
//			}
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;

			}

			List<CellProductionRecord> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> findCellPrimaryCultureTemplate1List(Integer start, Integer length, String query,
			String col, String sort, String id) throws Exception {
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CellProductionRecord where 1=1 and cellPassage.id='" + id
				+ "' and templeOperator.id='" + user.getId() + "' and (endTime is null or endTime='') ";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = " from CellProductionRecord where 1=1 and cellPassage.id='" + id + "' and templeOperator.id='"
					+ user.getId() + "' and (endTime is null or endTime='') ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by orderNum";
			}
			List<CellProductionRecord> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", "1");
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> NewFindCellPrimaryCultureTemplate1List(Integer start, Integer length, String query,
			String col, String sort, String id, String pici) throws Exception {
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CellProductionRecord where 1=1  and templeOperator.id='" + user.getId()
				+ "' and (endTime is null or endTime='') ";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		if (pici != null && !"".equals(pici)) {

			key += "and cellPassage.batch like '%" + pici + "%' ";
		}

		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = " from CellProductionRecord where 1=1 and templeOperator.id='" + user.getId()
					+ "' and (endTime is null or endTime='') ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
//				key += " order by orderNum,cellPassage.createDate desc";
				key += " order by " + col + " " + sort;

			}
			List<CellProductionRecord> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> findCellPrimaryCultureTemplateerList(Integer start, Integer length, String query,
			String col, String sort, String id, String inputDate, String inputDateEnd, String[] bzcx) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		/*
		 * Calendar calendar1 = Calendar.getInstance(); SimpleDateFormat sdf1 = new
		 * SimpleDateFormat("yyyy-MM-dd"); Date nowdate = new Date();
		 * calendar1.setTime(sdf1.parse(sdf1.format(nowdate)));
		 * calendar1.add(Calendar.DATE, 1); String one_days_after =
		 * sdf1.format(calendar1.getTime());
		 */
		String countHql = "select count(*) from CellProductionRecord cpr where 1=1 and cellPassage.id='" + id + "'";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		if (inputDate != null && !"".equals(inputDate) && inputDateEnd != null && !"".equals(inputDateEnd)
				&& bzcx != null && bzcx.length > 0) {
			key += " and str_to_date(cpr.planWorkDate, '%Y-%m-%d')>=str_to_date('" + inputDate + "', '%Y-%m-%d')  ";
			key += " and str_to_date(cpr.planWorkDate, '%Y-%m-%d')<=str_to_date('" + inputDateEnd + "', '%Y-%m-%d')  ";

			if (bzcx != null && bzcx.length > 0) {
				String a = "";
				for (int d = 0; d < bzcx.length; d++) {
					if ("".equals(a)) {
						a = " cpr.templeItem.name='" + bzcx[d] + "' ";
					} else {
						a += "or cpr.templeItem.name='" + bzcx[d] + "'";
					}
				}
				key += " and (" + a + ") ";
			}
		} else {
			key += " and str_to_date(cpr.planWorkDate, '%Y-%m-%d')>=str_to_date('" + inputDate + "', '%Y-%m-%d')  ";
			key += " and str_to_date(cpr.planWorkDate, '%Y-%m-%d')<=str_to_date('" + inputDateEnd + "', '%Y-%m-%d')  ";
		}

		if (bzcx != null && bzcx.length > 0) {
			String a = "";
			for (int d = 0; d < bzcx.length; d++) {
				if ("".equals(a)) {
					a = " cpr.templeItem.name='" + bzcx[d] + "' ";
				} else {
					a += "or cpr.templeItem.name='" + bzcx[d] + "'";
				}
			}
			key += " and (" + a + ") ";
		}

		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = " from CellProductionRecord cpr where 1=1 and cellPassage.id='" + id
					+ "' and (endTime is null or endTime='')";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by orderNum";
			}
			List<CellProductionRecord> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", "1");
			map.put("list", list);
		}
		return map;
	}
	// 生产

	public Map<String, Object> findList(Integer start, Integer length, String query, String col, String sort, String id)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
//		String hl = " from CellPassageTemplate where 1=1 and cellPassage.id = '" + id + "'and (state='1'or state=null)";
		String countHql = "select count(*) from CellProductionRecord where 1=1 and cellPassage.id='" + id + "'";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = " from CellProductionRecord where 1=1 and cellPassage.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by orderNum";
			}
			List<CellProductionRecord> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", "1");
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> findBuHeGe(Integer start, Integer length, String query, String col, String sort,
			String buzou, String id) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();

		String countHql = "select count(*) from CellProductionRecord where 1=1 and cellPassage.id='" + id
				+ "' and orderNum in (" + buzou + ") ";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = " from CellProductionRecord where 1=1 and cellPassage.id='" + id + "'and orderNum in (" + buzou
					+ ")  ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by orderNum";
			}
			List<CellProductionRecord> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", "1");
			map.put("list", list);
		}
		return map;

	}

	public List<CellProductionRecord> getTemplateItemListPage(String id, String orderNum) {
		List<CellProductionRecord> list = new ArrayList<CellProductionRecord>();
		String hql = "from CellProductionRecord where 1=1 and cellPassage.id='" + id + "' and orderNum>=" + orderNum
				+ " order by orderNum";
		String key = "";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	// 根据日期查询
	public List<CellProductionRecord> findProductState(String planWorkDate) {
		List<CellProductionRecord> list = new ArrayList<CellProductionRecord>();
		String hql = "from CellProductionRecord where 1=1 and planWorkDate='" + planWorkDate + "'";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<CellPassage> findCellPassageList() {
		List<CellPassage> list = new ArrayList<CellPassage>();
		String hql = " from CellPassage where 1=1 and state<>'1' order by createDate desc";
		list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 生产计划查询
	public List<CellPassage> findQuProductionList(Integer start, Integer length, String query, String col, String sort,
			String type) throws Exception {

		List<CellPassage> list = new ArrayList<CellPassage>();
		String hql = " from CellPassage where 1=1";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
		return list;
//		Map<String, Object> map = new HashMap<String, Object>();
//		String countHql = "select count(*) from CellPassage where 1=1";
//		String key = "";
//		if (query != null && !"".equals(query)) {
//			key = map2Where(query);
//		} else {
//			
//		}
//		
//		if(type!=null
//				&&!"".equals(type)){
//			if("1".equals(type)){
//				key = key + " and state='1' ";
//			}else if("2".equals(type)){
//				key = key + " and state='2' ";
//			}
//		}
//		String scopeId = (String) ActionContext.getContext().getSession()
//				.get("scopeId");
//		if (!"all".equals(scopeId)) {
//			key += " and scopeId='" + scopeId + "'";
//		}
//
//		Long sumCount = (Long) getSession().createQuery(countHql + key)
//				.uniqueResult();
//		if (0l != sumCount) {
//			Long filterCount = (Long) getSession().createQuery(countHql + key)
//					.uniqueResult();
//			String hql = "from TransportOrderCell where 1=1 and submitData = '是' ";
//			if (col != null && !"".equals(col) && !"".equals(sort)
//					&& sort != null) {
//				if (col.indexOf("-") != -1) {
//					col = col.substring(0, col.indexOf("-"));
//				}
//				if("id".equals(col)){
//					key += " order by id desc ";
//				}else{
//					key += " order by " + col + " " + sort;
//				}
//			}else{
//				key += " order by id desc ";
//			}
//			List<SampleTransportItem> list = getSession()
//					.createQuery(hql + key).setFirstResult(start)
//					.setMaxResults(length).list();
//			map.put("recordsTotal", sumCount);
//			map.put("recordsFiltered", filterCount);
//			map.put("list", list);
//		}
	}

	public List<CellPassage> alreadyProduced() {
		List<CellPassage> list = new ArrayList<CellPassage>();
		String hql = " from CellPassage where 1=1 and state='1' order by createDate desc";
		list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<CellPassage> findCellPassageList1(String pici) {
		List<CellPassage> list = new ArrayList<CellPassage>();
		String hql = " from CellPassage where 1=1 and state <> '1' and batch='" + pici + "' order by createDate desc";
		list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<CellProductionRecord> findTemplateList(String id) {
		List<CellProductionRecord> list = new ArrayList<CellProductionRecord>();
		String hql = " from CellProductionRecord where 1=1 and cellPassage.id = '" + id + "'";
		list = this.getSession().createQuery(hql).list();
		return list;

	}

	// 已生产

	public List<CellPassageTemplate> findCellPassageTemplate(String id) {
		List<CellPassageTemplate> list = new ArrayList<CellPassageTemplate>();
		String hql = " from CellPassageTemplate where 1=1 and cellPassage.id = '" + id
				+ "'and (state='1'or state=null)";
		list = this.getSession().createQuery(hql).list();
		return list;

	}

//	/**
//	 * 待生产
//	 * */
//	public List<CellProductionRecord> daiShengChan(String id) {
//		List<CellProductionRecord> list = new ArrayList<CellProductionRecord>();
//		String hql = " from CellProductionRecord where 1=1 and cellPassage.id = '" + id + "'";
//		list = this.getSession().createQuery(hql).list();
//		return list;
//	}
//	
//	/**
//	 * 已经生产
//	 * */
//	public List<CellProductionRecord> alreadyProduced(String id) {
//		List<CellProductionRecord> list = new ArrayList<CellProductionRecord>();
//		String hql = " from CellProductionRecord where 1=1 and cellPassage.id = '" + id + "'";
//		list = this.getSession().createQuery(hql).list();
//		return list;
//	}

	public List<CellPassage> findCellPassageListByUser(String name) {
		List<CellPassage> list = new ArrayList<CellPassage>();
		String hql = " from CellPassage where 1=1 and state <> '1' ";// and workUser like '%"+name+"%'";
		list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<CellProductionRecord> findAllStepItem(String id) {
		List<CellProductionRecord> list = new ArrayList<CellProductionRecord>();
		String hql = "from CellProductionRecord where 1=1 and cellPassage.id = '" + id + "'";
		String key = "";
		list = getSession().createQuery(hql + key).list();
		return list;
	}

	public List<CellPassage> findCellPassageList(String type) {
		List<CellPassage> list = new ArrayList<CellPassage>();
		String hql = " from CellPassage cp " + "where cp.id in ( "
				+ "SELECT cpt.cellPassage.id from CellPassageTemplate cpt "
				+ "GROUP BY cpt.cellPassage.id HAVING COUNT(cpt.cellPassage.id)>0 "
				+ ") and cp.state <>'1' order by cp.createDate desc ";
		list = this.getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> findCellPassageList(String type, Integer start, Integer length) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from CellPassage cp " + "where cp.id in ( "
				+ "SELECT cpt.cellPassage.id from CellPassageTemplate cpt "
				+ "GROUP BY cpt.cellPassage.id HAVING COUNT(cpt.cellPassage.id)>0 "
				+ ") and cp.state <>'1' order by cp.createDate desc ";
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql).uniqueResult();
			String hql = " from CellPassage cp " + "where cp.id in ( "
					+ "SELECT cpt.cellPassage.id from CellPassageTemplate cpt "
					+ "GROUP BY cpt.cellPassage.id HAVING COUNT(cpt.cellPassage.id)>0 "
					+ ") and cp.state <>'1' order by cp.createDate desc ";
			List<CellPassage> list = this.getSession().createQuery(hql).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<CellPassage> findCellPassageListByDate(String inputDate, String inputDateEnd) {
		List<CellPassage> list = new ArrayList<CellPassage>();
		String hql = " from CellPassage cp where 1=1 and cp.state <> '1' ";
		String key = "";
		if (inputDate != null && !"".equals(inputDate) && inputDateEnd != null && !"".equals(inputDateEnd)) {
			key = " and cp.id in ( " + "SELECT cpt.cellPassage.id from CellPassageTemplate cpt where "
					+ "str_to_date(cpt.planWorkDate,'%Y-%m-%d') >= str_to_date('" + inputDate + "','%Y-%m-%d') "
					+ "and str_to_date(cpt.planWorkDate,'%Y-%m-%d') <= str_to_date('" + inputDateEnd + "','%Y-%m-%d')"
					+ "GROUP BY cpt.cellPassage.id HAVING COUNT(cpt.cellPassage.id)>0 "
					+ ") order by cp.createDate desc";
		}
		// else if (inputDate != null && !"".equals(inputDate)) {
		// key = " and cp.id in ( "
		// + "SELECT cpt.cellPassage.id from CellPassageTemplate cpt where
		// cpt.planWorkDate like '%"
		// + inputDate + "%' " + "GROUP BY cpt.cellPassage.id HAVING
		// COUNT(cpt.cellPassage.id)>0 " + ") order by cp.createDate desc";
		// // key = " and FIND_IN_SET('"+inputDate+"',planWorkDate) > 0";
		// }
		list = this.getSession().createQuery(hql + key).list();
		return list;
	}

	public Map<String, Object> findCellPassageListByDate(String inputDate, String inputDateEnd, Integer start,
			Integer length, String[] bzcx) {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*)  from CellPassage cp where 1=1 and cp.state <> '1' ";
		String key = "";
		if (inputDate != null && !"".equals(inputDate) && inputDateEnd != null && !"".equals(inputDateEnd)
				&& bzcx != null && bzcx.length > 0) {
			String a = "";
			if (bzcx != null && bzcx.length > 0) {
				for (int d = 0; d < bzcx.length; d++) {
					if ("".equals(a)) {
						a = " cpt.name='" + bzcx[d] + "' ";
					} else {
						a += "or cpt.name='" + bzcx[d] + "'";
					}
				}
				a = " and (" + a + ") ";
			}
			key = " and cp.id in ( " + "SELECT cpt.cellPassage.id from CellPassageTemplate cpt where "
					+ "str_to_date(cpt.planWorkDate,'%Y-%m-%d') >= str_to_date('" + inputDate + "','%Y-%m-%d') "
					+ "and str_to_date(cpt.planWorkDate,'%Y-%m-%d') <= str_to_date('" + inputDateEnd + "','%Y-%m-%d')"
//					+"and cpt.name='"+bzcx+"'"
					+ a + "GROUP BY cpt.cellPassage.id HAVING COUNT(cpt.cellPassage.id)>0 "
					+ ") order by cp.createDate desc";

		} else {
			key = " and cp.id in ( " + "SELECT cpt.cellPassage.id from CellPassageTemplate cpt where "
					+ "str_to_date(cpt.planWorkDate,'%Y-%m-%d') >= str_to_date('" + inputDate + "','%Y-%m-%d') "
					+ "and str_to_date(cpt.planWorkDate,'%Y-%m-%d') <= str_to_date('" + inputDateEnd + "','%Y-%m-%d')"
//					+"and cpt.name='"+bzcx+"'"
					+ "GROUP BY cpt.cellPassage.id HAVING COUNT(cpt.cellPassage.id)>0 "
					+ ") order by cp.createDate desc";
		}

		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = " from CellPassage cp where 1=1 and cp.state <> '1' ";
			List<CellPassage> list = this.getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	// 根据时间查找完成的
	public List<CellPassage> findByDate(String inputDate, String inputDateEnd) {
		List<CellPassage> list = new ArrayList<CellPassage>();
		String hql = " from CellPassage cp where 1=1 and cp.state = '1' ";
		String key = "";
		if (inputDate != null && !"".equals(inputDate) && inputDateEnd != null && !"".equals(inputDateEnd)) {
			key = " and cp.id in ( " + "SELECT cpt.cellPassage.id from CellPassageTemplate cpt where "
					+ "str_to_date(cpt.planWorkDate,'%Y-%m-%d') >= str_to_date('" + inputDate + "','%Y-%m-%d') "
					+ "and str_to_date(cpt.planWorkDate,'%Y-%m-%d') <= str_to_date('" + inputDateEnd + "','%Y-%m-%d')"
					+ "GROUP BY cpt.cellPassage.id HAVING COUNT(cpt.cellPassage.id)>0 "
					+ ") order by cp.createDate desc";
		}
		list = this.getSession().createQuery(hql + key).list();
		return list;
	}

	// 根据时间查找完成的
	public Map<String, Object> findByDate(String inputDate, String inputDateEnd, Integer start, Integer length,
			String bzcx[]) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*)  from CellPassage cp where 1=1 and cp.state = '1' ";
		String key = "";
		if (inputDate != null && !"".equals(inputDate) && inputDateEnd != null && !"".equals(inputDateEnd)) {
			key = " and cp.id in ( " + "SELECT cpt.cellPassage.id from CellPassageTemplate cpt where "
					+ "str_to_date(cpt.planWorkDate,'%Y-%m-%d') >= str_to_date('" + inputDate + "','%Y-%m-%d') "
					+ "and str_to_date(cpt.planWorkDate,'%Y-%m-%d') <= str_to_date('" + inputDateEnd + "','%Y-%m-%d')"
					+ "GROUP BY cpt.cellPassage.id HAVING COUNT(cpt.cellPassage.id)>0 "
					+ ") order by cp.createDate desc";
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = " from CellPassage cp where 1=1 and cp.state = '1' ";
			List<CellPassage> list = this.getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public SampleOrder getSampleOrderBybatch(String pc) {
		SampleOrder so = new SampleOrder();
		String hql = "from SampleOrder where 1=1 and barcode='" + pc + "'";
		so = (SampleOrder) getSession().createQuery(hql).uniqueResult();
		return so;
	}

	public List<InfluenceProduct> findInfluenceProductList(String ordercode) {
		List<InfluenceProduct> list = new ArrayList<InfluenceProduct>();
		String hql = "from InfluenceProduct where 1=1 and orderCode='" + ordercode
				+ "' and (deviationHandlingReport.approvalStatus is null or deviationHandlingReport.approvalStatus!='1') ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> showInfluenceProductTable(Integer start, Integer length, String query, String col,
			String sort, String flag) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from InfluenceProduct where 1=1 and orderCode='" + flag + "'";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from InfluenceProduct where 1=1 and orderCode='" + flag + "' ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				if (col.indexOf("-") != -1) {
					col = col.replace("-", ".");
				}
				key += " order by " + col + " " + sort;
			}
			List<InfluenceProduct> list = new ArrayList<InfluenceProduct>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<CellPassageItem> getCellPassageItems(CellPassage cellPassage, String orderNum) {
		List<CellPassageItem> list = new ArrayList<CellPassageItem>();
		String hql = "from CellPassageItem where 1=1 and stepNum='" + orderNum + "' and cellPassage.id='"
				+ cellPassage.getId() + "' and blendState='0' ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public Instrument findMicroscopeNo(String microscopeNo) {
		Instrument list = new Instrument();
		String hql = "from Instrument where 1=1 and id='" + microscopeNo + "'";
		list = (Instrument) getSession().createQuery(hql).uniqueResult();
		return list;
	}

	public List<CellPassageItem> findSampleId(String sampleId, String id, String ordernum) {
		String hql = "from CellPassageItem where 1=1 and code='" + sampleId + "'and cellPassage='" + id
				+ "'and stepNum='" + ordernum + "'";
		List<CellPassageItem> list = getSession().createQuery(hql).list();
		return list;
	}

	public List<Instrument> getRoomInstruments(String roomId) {
		List<Instrument> list = new ArrayList<Instrument>();
		String hql = "from Instrument where 1=1 and roomManagement.id='" + roomId + "' and state.id='1r' ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<CellProducOperation> getCellProducOperations(String id, String orderNum) {
		List<CellProducOperation> list = new ArrayList<CellProducOperation>();
		String hql = "from CellProducOperation where 1=1 and cellPassage.id='" + id + "' and orderNum='" + orderNum
				+ "' ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<CellproductCos> getCellproductCosinstruments(String id) {
		List<CellproductCos> list = new ArrayList<CellproductCos>();
		String hql = "from CellproductCos where 1=1 and cellProduc.id='" + id + "' ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<CellPassageItem> findIncubator(String incubator, String id, String ordernum) {
		String hql = "from CellPassageItem where 1=1 and instrument.id='"+incubator+"'and cellPassage.id='"+id+"'and stepNum='"+ordernum+"'";
		List<CellPassageItem> list = getSession().createQuery(hql).list();
		return list;
	}

}