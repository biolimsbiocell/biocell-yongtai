package com.biolims.experiment.production.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.deviation.model.InfluenceProduct;
import com.biolims.equipment.model.Instrument;
import com.biolims.experiment.cell.passage.dao.CellPassageDao;
import com.biolims.experiment.cell.passage.model.CellPassage;
import com.biolims.experiment.cell.passage.model.CellPassageCos;
import com.biolims.experiment.cell.passage.model.CellPassageTemplate;
import com.biolims.experiment.cell.passage.model.CellProducOperation;
import com.biolims.experiment.cell.passage.model.CellPassageItem;
import com.biolims.experiment.cell.passage.model.CellPassageObservation;
import com.biolims.experiment.cell.passage.model.CellProductionRecord;
import com.biolims.experiment.cell.passage.model.CellproductCos;
import com.biolims.experiment.production.dao.ProductionPlanDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.stamp.birtVersion.model.BirtVersion;
import com.biolims.stamp.birtVersion.model.BirtVersionItem;
import com.biolims.system.template.model.TemplateItem;

import net.sf.json.JSONObject;

@Service
@Transactional
public class ProductionPlanService {

	@Resource
	private CellPassageDao cellPassageDao;
	@Resource
	private ProductionPlanDao productionPlanDao;
	@Resource
	private CommonDAO commonDAO;

	@SuppressWarnings("unchecked")
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Map<String, Object> findAllProductionList(Integer start, Integer length, String query, String col,
			String sort, String type, String inputDate, String inputDateEnd, String[] bzcx) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		List<Object> list = new ArrayList<Object>();
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);

		// 原代
		// 除完成的数据
		// List<CellPrimaryCulture> l1 =
		// cellPrimaryCultureDao.findCellPrimaryCultureList();
		//
		// if(l1.size()>0) {
		// for (CellPrimaryCulture cellPrimaryCulture : l1) {
		// String workUser1 = "";
		// List<CellPrimaryCultureTemplate> tl1 =
		// cellPrimaryCultureDao.findTemplateList(cellPrimaryCulture.getId());
		// if(tl1.size()>0) {
		// for (CellPrimaryCultureTemplate t1 : tl1) {
		// if(t1.getTestUserList()!=null&&!"".equals(t1.getTestUserList())) {
		// workUser1 += t1.getTestUserList() + ",";
		// }
		// }
		// }
		// if(!"".equals(workUser1)) {
		// workUser1 = workUser1.substring(0, workUser1.length()-1);
		// }
		// cellPrimaryCulture.setWorkUser(workUser1);
		// commonDAO.saveOrUpdate(cellPrimaryCulture);
		// }
		// }
		// List<CellPrimaryCulture> list1 =
		// cellPrimaryCultureDao.findCellPrimaryCultureListByUser(user.getName());

		// 传代
		List<CellPassage> l2 = productionPlanDao.findCellPassageList();
		// List<CellPassage> l2 =
		// productionPlanDao.findCellPassageListByDate(inputDate);
		if (l2.size() > 0) {
			for (CellPassage cellPassage : l2) {
				String workUser2 = "";
				List<CellProductionRecord> tl2 = productionPlanDao.findTemplateList(cellPassage.getId());
				for (CellProductionRecord t2 : tl2) {
					if (t2.getTempleOperator() != null && !"".equals(t2.getTempleOperator())) {
						if (workUser2.indexOf(t2.getTempleOperator().getName()) != -1) {

						} else {
							workUser2 += t2.getTempleOperator().getName() + ",";
						}
					}
				}
				if (!"".equals(workUser2)) {
					workUser2 = workUser2.substring(0, workUser2.length() - 1);
				}
				cellPassage.setWorkUser(workUser2);
				commonDAO.saveOrUpdate(cellPassage);
			}
		}

		// List<CellPassage> list2 =
		// productionPlanDao.findCellPassageListByUser(user.getName());
		List<CellPassage> list2 = null;
		if ("1".equals(type)) {
			map = productionPlanDao.findCellPassageList(type, start, length);
			list2 = (List<CellPassage>) map.get("list");
//			list2 = productionPlanDao.findCellPassageList(type);
		}
		if ("2".equals(type)) {
			map = productionPlanDao.findCellPassageListByDate(inputDate, inputDateEnd, start, length, bzcx);
			list2 = (List<CellPassage>) map.get("list");
//			list2 = productionPlanDao.findCellPassageListByDate(inputDate, inputDateEnd);
		}
		if ("3".equals(type)) {
			map = productionPlanDao.findByDate(inputDate, inputDateEnd, start, length, bzcx);
			list2 = (List<CellPassage>) map.get("list");
//			list2 = productionPlanDao.findByDate(inputDate, inputDateEnd);
		}
		// 产品干
		// List<PlasmaTask> l3 = bloodSplitDao.findBloodSplitList();
		// if(l3.size()>0) {
		// for (PlasmaTask plasmaTask : l3) {
		// String workUser3 = "";
		// List<PlasmaTaskTemplate> tl3 =
		// bloodSplitDao.findTemplateByzId(plasmaTask.getId());
		// if(tl3.size()>0) {
		// for (PlasmaTaskTemplate t3 : tl3) {
		// if(t3.getTestUserList()!=null&&!"".equals(t3.getTestUserList())) {
		// workUser3 += t3.getTestUserList() + ",";
		// }
		// }
		// if(!"".equals(workUser3)) {
		// workUser3 = workUser3.substring(0, workUser3.length()-1);
		// }
		// plasmaTask.setWorkUser(workUser3);
		// commonDAO.saveOrUpdate(plasmaTask);
		// }
		// }
		// }
		// List<PlasmaTask> list3 =
		// bloodSplitDao.findBloodSplitTaskListByUser(user.getName());

		// 免疫细胞
		// List<ImmuneCellProduction> l4 = cellProductionDao.findImmuneCellList();
		// if(l4.size()>0) {
		// for (ImmuneCellProduction immuneCellProduction : l4) {
		// String workUser4 = "";
		// List<ImmuneCellProductionTemplate> tl4 =
		// cellProductionDao.findTemplateList(immuneCellProduction.getId());
		// if(tl4.size()>0) {
		// for (ImmuneCellProductionTemplate t4 : tl4) {
		// if(t4.getTestUserList()!=null&&!"".equals(t4.getTestUserList())) {
		// workUser4 += t4.getTestUserList() + ",";
		// }
		// }
		// }
		// if(!"".equals(workUser4)) {
		// workUser4 = workUser4.substring(0, workUser4.length()-1);
		// }
		// immuneCellProduction.setWorkUser(workUser4);
		// commonDAO.saveOrUpdate(immuneCellProduction);
		// }
		// }
		//
		// List<ImmuneCellProduction> list4 =
		// cellProductionDao.findImmuneCellListByUser(user.getName());

		// 细胞因子
		// List<CytokineProduction> l5 = cytokineProductionDao.findCytokineList();
		// if(l5.size()>0) {
		// for (CytokineProduction cytokineProduction : l5) {
		// String workUser5 = "";
		// List<CytokineProductionTemplate> tl5 =
		// cytokineProductionDao.findTemplateList(cytokineProduction.getId());
		// if(tl5.size()>0) {
		// for (CytokineProductionTemplate t5 : tl5) {
		// if(t5.getTestUserList()!=null&&!"".equals(t5.getTestUserList())) {
		// workUser5 += t5.getTestUserList() + ",";
		// }
		// }
		// }
		// if(!"".equals(workUser5)) {
		// workUser5 = workUser5.substring(0, workUser5.length()-1);
		// }
		// cytokineProduction.setWorkUser(workUser5);
		// commonDAO.saveOrUpdate(cytokineProduction);
		// }
		// }
		// List<CytokineProduction> list5 =
		// cytokineProductionDao.findCytokineListByUser(user.getName());
		// list.addAll(list1);
		if (list2 != null) {
			if (!list2.isEmpty()) {
				for (int i = 0; i < list2.size(); i++) {
					String pc = list2.get(i).getBatch();
					SampleOrder so = new SampleOrder();
					so = productionPlanDao.getSampleOrderBybatch(pc);
					if (so != null) {
						list2.get(i).setPatientName(so.getName());
						list2.get(i).setFiltrateCode(so.getFiltrateCode());
						commonDAO.saveOrUpdate(list2.get(i));
					}
				}
			}
			list.addAll(list2);
		}
		// list.addAll(list3);
		// list.addAll(list4);
		// list.addAll(list5);
		map.put("list", list);
//		map.put("recordsTotal", list.size());
//		// map.put("recordsFiltered", list.size());
//		map.put("recordsFiltered", "1");
		return map;
	}

	@SuppressWarnings("unchecked")
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Map<String, Object> findQuProductionList(Integer start, Integer length, String query, String col,
			String sort, String type, String inputDate, String inputDateEnd, String[] bzcx) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		List<Object> list = new ArrayList<Object>();
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		List<CellPassage> l2 = productionPlanDao.findQuProductionList(start, length, query, col, sort, type);
		if (l2.size() > 0) {
			for (CellPassage cellPassage : l2) {
				String workUser2 = "";
				List<CellProductionRecord> tl2 = productionPlanDao.findTemplateList(cellPassage.getId());
				for (CellProductionRecord t2 : tl2) {
					if (t2.getTempleOperator() != null && !"".equals(t2.getTempleOperator())) {
						if (workUser2.indexOf(t2.getTempleOperator().getName()) != -1) {

						} else {
							workUser2 += t2.getTempleOperator().getName() + ",";
						}
					}
				}
				if (!"".equals(workUser2)) {
					workUser2 = workUser2.substring(0, workUser2.length() - 1);
				}
				cellPassage.setWorkUser(workUser2);
				commonDAO.saveOrUpdate(cellPassage);
			}

		}

		if (!l2.isEmpty()) {
			for (int i = 0; i < l2.size(); i++) {
				String pc = l2.get(i).getBatch();
				SampleOrder so = new SampleOrder();
				so = productionPlanDao.getSampleOrderBybatch(pc);
				if (so != null) {
					l2.get(i).setPatientName(so.getName());
					l2.get(i).setFiltrateCode(so.getFiltrateCode());
					commonDAO.saveOrUpdate(l2.get(i));
				}
			}
		}
		list.addAll(l2);
		// list.addAll(list3);
		// list.addAll(list4);
		// list.addAll(list5);
		map.put("list", list);
		map.put("recordsTotal", list.size());
		// map.put("recordsFiltered", list.size());
		map.put("recordsFiltered", "1");
		return map;
	}
//	/**
//	 * 已生产
//	 * 
//	 * */
//	@SuppressWarnings("unchecked")
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public Map<String, Object> alreadyProduced(Integer start, Integer length, String query, String col,
//			String sort, String type, String inputDate, String inputDateEnd) throws Exception {
//		Map<String, Object> map = new HashMap<String, Object>();
//		List<Object> list = new ArrayList<Object>();
//		User user = (User) ServletActionContext.getRequest().getSession()
//				.getAttribute(SystemConstants.USER_SESSION_KEY);
//
//		// 传代
//		//查询出状态完成的所有数据
//		List<CellPassage> l2 = productionPlanDao.alreadyProduced();
//
//		if (l2.size() > 0) {
//			for (CellPassage cellPassage : l2) {
//				String workUser2 = "";
//	
//				List<CellProductionRecord> tl2 = productionPlanDao.findTemplateList(cellPassage.getId());
//
//				for (CellProductionRecord t2 : tl2) {
//					if (t2.getTempleOperator() != null && !"".equals(t2.getTempleOperator())) {
//						if (workUser2.indexOf(t2.getTempleOperator().getName()) != -1) {
//
//						} else {
//							workUser2 += t2.getTempleOperator().getName() + ",";
//						}
//					}
//				}
//				if (!"".equals(workUser2)) {
//					workUser2 = workUser2.substring(0, workUser2.length() - 1);
//				}
//				
//				cellPassage.setWorkUser(workUser2);
//				commonDAO.saveOrUpdate(cellPassage);
//			
//			}
//		}
//
//		List<CellPassage> list2 = null;
//		if ("1".equals(type)) {
//			list2 = productionPlanDao.findCellPassageList(type);
//		} else {
//			list2 = productionPlanDao.findByDate(inputDate, inputDateEnd);
//		}
//		for (CellPassage cellPassage : list2) {
//			if(cellPassage.getStateName().equals("不合格终止")) {
//				List<CellPassageTemplate> cellPassageTemplates =productionPlanDao.findCellPassageTemplate(cellPassage.getId());
//				for (CellPassageTemplate cellPassageTemplate : cellPassageTemplates) {
//					
//					
//				SampleOrder sampleOrder=new SampleOrder();
//				sampleOrder=productionPlanDao.getSampleOrderBybatch(cellPassage.getBatch());	
//				if (sampleOrder != null) {
//					cellPassage.setPatientName(sampleOrder.getName());
//					cellPassage.setFiltrateCode(sampleOrder.getFiltrateCode());
//					commonDAO.saveOrUpdate(cellPassage);
//				}
//				}
//			
//			}else {
//				SampleOrder sampleOrder=new SampleOrder();
//				sampleOrder=productionPlanDao.getSampleOrderBybatch(cellPassage.getBatch());	
//				if (sampleOrder != null) {
//					cellPassage.setPatientName(sampleOrder.getName());
//					cellPassage.setFiltrateCode(sampleOrder.getFiltrateCode());
//					commonDAO.saveOrUpdate(cellPassage);
//				}
//			}
//	
//			
//		}
//
////		if (!list2.isEmpty()) {
////			for (int i = 0; i < list2.size(); i++) {
////				String pc = list2.get(i).getBatch();
////				SampleOrder so = new SampleOrder();
////				so = productionPlanDao.getSampleOrderBybatch(pc);
////				if (so != null) {
////					list2.get(i).setPatientName(so.getName());
////					list2.get(i).setFiltrateCode(so.getFiltrateCode());
////					commonDAO.saveOrUpdate(list2.get(i));
////				}
////			}
////		}
////		
//
//		list.addAll(list2);
//		map.put("list", list);
//		map.put("recordsTotal", list.size());
//		map.put("recordsFiltered", "1");
//		return map;
//	}
//	

	/**
	 * 保存细胞观察
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void SaveCellObservation(String scjhid, String observationResult, String cellNum, String cellNumber,
			String cellDensity, String observationNote, String operationState, String microscopeNo,String bh,String mc,
			String zt,String sampleId,String sampleCheck)
			throws ParseException {
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		CellProductionRecord cpct = commonDAO.get(CellProductionRecord.class, scjhid);
		if (cpct != null) {
			cpct.setObservationDate(new Date());
			cpct.setObservationResult(observationResult);
			cpct.setOperationState(operationState);
			cpct.setObservationUser(user);
			cpct.setCellNum(cellNum);
			cpct.setCellNumber(cellNumber);
			cpct.setCellDensity(cellDensity);
			cpct.setObservationNote(observationNote);
			if (microscopeNo == null) {
				cpct.setMicroscopeNo("");
			} else {
				cpct.setMicroscopeNo(microscopeNo);
				cpct.setMicroscopeId(bh);
				cpct.setMicroscopeName(mc);
				cpct.setMicroscopeStateName(zt);
			}
			cpct.setSampleId(sampleId);
			cpct.setSampleCheck(sampleCheck);
			commonDAO.saveOrUpdate(cpct);

			CellPassageObservation cpo = new CellPassageObservation();
			cpo.setId(null);
			cpo.setCellPassage(cpct.getCellPassage());
			cpo.setStepNum(cpct.getOrderNum());
			cpo.setObservationDate(cpct.getObservationDate());
			cpo.setObservationResult(cpct.getObservationResult());
			cpo.setOperationState(cpct.getOperationState());
			cpo.setObservationUser(cpct.getObservationUser());
			cpo.setNote(cpct.getObservationNote());
			cpo.setCellNum(cpct.getCellNum());
			cpo.setCellNumber(cpct.getCellNumber());
			cpo.setCellDensity(cpct.getCellDensity());
			if (cpct.getMicroscopeNo() == null) {
				cpo.setMicroscopeNo("");
			} else {
				cpo.setMicroscopeNo(cpct.getMicroscopeNo());
			}
			commonDAO.saveOrUpdate(cpo);
		}

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCPtemplateitem(String id, String userid, String operationdate, String templateid, String room,
			String roomId, String note) throws ParseException {
		
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		// 保存操作人，预计操作时间，根据当前步骤的操作时间更新后面步骤的操作时间
		CellProductionRecord cellProductionRecord2 = commonDAO.get(CellProductionRecord.class, templateid);
		CellProductionRecord cellProductionRecord = cellProductionRecord2;
		CellProductionRecord cpct = cellProductionRecord;
		if (cpct != null) {
			if (userid != null && !"".equals(userid)) {
				User uu = commonDAO.get(User.class, userid);
				if (uu != null) {
					cpct.setTempleOperator(uu);
				} else {
					cpct.setTempleOperator(null);
				}
			} else {
				cpct.setTempleOperator(null);
			}
			cpct.setPlanWorkDate(operationdate);
			cpct.setOperatingRoomName(room);
			cpct.setOperatingRoomId(roomId);
			cpct.setNote(note);
			if (cpct.getContent() != null) {
				JSONObject jsonCon = JSONObject.fromObject(cpct.getContent());
				jsonCon.put("operatingRoomName", room);
				jsonCon.put("templeOperator", userid);
				jsonCon.put("operatingRoomId", roomId);
				jsonCon.put("noet", note);
				cpct.setContent(jsonCon.toString());
			} else {
				JSONObject jsonCon = new JSONObject();
				jsonCon.put("operatingRoomName", room);
				jsonCon.put("templeOperator", userid);
				jsonCon.put("operatingRoomId", roomId);
				jsonCon.put("noet", note);
				cpct.setContent(jsonCon.toString());
			}
			
			//选择房间保存应该将该房间的仪器放入工前准备的仪器中
			if(roomId!=null
					&&!"".equals(roomId)) {
				//查询该房间下仪器
				List<Instrument> its = productionPlanDao.getRoomInstruments(roomId);
				if(its.size()>0) {
					//循环该步骤是否配置仪器  存在仪器 将仪器放到该步骤
					List<CellProducOperation> cpos = productionPlanDao.getCellProducOperations(cpct.getCellPassage().getId(), cpct.getOrderNum());
					if(cpos.size()>0) {
						for(CellProducOperation cpo:cpos) {
							List<CellproductCos> cpss = productionPlanDao.getCellproductCosinstruments(cpo.getId());
							if(cpss.size()>0) {
								for(CellproductCos cps:cpss) {
									for(Instrument it:its) {
										if(it.getMainType().getId().equals(cps.getTemplateCos().getTypeId())) {
											cps.setInstrumentCode(it.getId());
											cps.setInstrumentName(it.getName());
											cps.setInstrumentId(it.getMainType().getId());
											commonDAO.saveOrUpdate(cps);
											break;
										}
									}
								}
							}
						}
					}
				}
				
			}

			commonDAO.saveOrUpdate(cpct);

			int amount = 0;
			// 算预计时间 第一次创建的生产的时候固定 计划完成时间 创建第一步
			List<CellProductionRecord> temList = productionPlanDao.getTemplateItemListPage(id, cpct.getOrderNum());
			if (temList.size() > 0) {
				for (int n = 0; n < temList.size(); n++) {
					CellProductionRecord cellProduc = temList.get(n);
					if (!"".equals(cellProduc.getEstimatedDate())) {
						if (n > 0) {
							String templateItemId = cellProduc.getTempleItem().getId();
							TemplateItem ti = new TemplateItem();
							ti = commonDAO.get(TemplateItem.class, templateItemId);
							if ("1".equals(ti.getHarvest())) {
								break;
							}
						}
						if (n == 0) {
							Calendar calendar1 = Calendar.getInstance();
							SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
							calendar1.setTime(sdf1.parse(temList.get(0).getPlanWorkDate()));
							String three_days_ago = sdf1.format(calendar1.getTime());
							cellProduc.setPlanWorkDate(three_days_ago);//
						} else {
							Calendar calendar1 = Calendar.getInstance();
							SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
							calendar1.setTime(sdf1.parse(temList.get(0).getPlanWorkDate()));
							calendar1.add(Calendar.DATE, amount);
							String three_days_ago = sdf1.format(calendar1.getTime());
							cellProduc.setPlanWorkDate(three_days_ago);//
						}
						amount += Integer.parseInt(cellProduc.getEstimatedDate());
					}
					commonDAO.saveOrUpdate(cellProduc);
				}
			}

			List<CellProductionRecord> cprList = productionPlanDao.findAllStepItem(id);
			CellPassage cellPassage = commonDAO.get(CellPassage.class, id);
			String planTime = "";
			for (int i = 0; i < cprList.size(); i++) {
				String planWorkDate = cprList.get(i).getPlanWorkDate();
				if (i < cprList.size() - 1) {
					planTime += planWorkDate + ",";
				} else {
					planTime += planWorkDate;
				}
			}
			cellPassage.setPlanWorkDate(planTime);
			commonDAO.saveOrUpdate(cellPassage);
		}
		CellProductionRecord cpr=commonDAO.get(CellProductionRecord.class, templateid);
		LogInfo li = new LogInfo();
		li.setLogDate(new Date());
		li.setFileId(cpr.getCellPassage().getBatch());
		li.setUserId(user.getId());
		li.setClassName("CellProductionRecord");
		li.setState("3");
		li.setStateName("数据修改");
		li.setModifyContent("生产计划：产品批号为："+cpr.getCellPassage().getBatch()+"的数据已经保存");
		commonDAO.saveOrUpdate(li);
		
		
		
		
	}

	public Map<String, Object> findCellPrimaryCultureTemplateList(Integer start, Integer length, String query,
			String col, String sort, String ids) throws Exception {
		Map<String, Object> result = productionPlanDao.findCellPrimaryCultureTemplateList(start, length, query, col,
				sort, ids);
		return result;
	}

	public Map<String, Object> newFindCellPrimaryCultureTemplateList(Integer start, Integer length, String query,
			String col, String sort, String ids, String bottleOrBag, String infectionType, String dataS, String dataE,
			String[] dbbacx, String produceId, String pici) throws Exception {
		Map<String, Object> result = productionPlanDao.newFindCellPrimaryCultureTemplateList(start, length, query, col,
				sort, ids, bottleOrBag, infectionType, dataS, dataE, dbbacx, produceId, pici);
		return result;
	}

	public Map<String, Object> findCellPrimaryCultureTemplate1List(Integer start, Integer length, String query,
			String col, String sort, String ids) throws Exception {
		// TODO Auto-generated method stub
		return productionPlanDao.findCellPrimaryCultureTemplate1List(start, length, query, col, sort, ids);
	}

	public Map<String, Object> NewFindCellPrimaryCultureTemplate1List(Integer start, Integer length, String query,
			String col, String sort, String ids, String pici) throws Exception {
		// TODO Auto-generated method stub
		return productionPlanDao.NewFindCellPrimaryCultureTemplate1List(start, length, query, col, sort, ids, pici);
	}

	public Map<String, Object> findCellPrimaryCultureTemplateerList(Integer start, Integer length, String query,
			String col, String sort, String id, String inputDate, String inputDateEnd, String[] bzcx) throws Exception {
		Map<String, Object> result = productionPlanDao.findCellPrimaryCultureTemplateerList(start, length, query, col,
				sort, id, inputDate, inputDateEnd, bzcx);
//		List<CellProductionRecord> list = (List<CellProductionRecord>) result.get("list");
//		if(list.size()>0) {
//			for(CellProductionRecord cpt:list) {
//				if(cpt.getEndTime()==null||"".equals(cpt.getEndTime())) {
//					List<CellPassageItem> cpis = productionPlanDao.getCellPassageItems(cpt.getCellPassage(),cpt.getOrderNum());
//					if(cpis.size()>0) {
//						CellPassageItem cpi = cpis.get(0);
//						cpt.setCounts(cpi.getCounts());
//						cpt.setPosId(cpi.getPosId());
//					}else {
//						cpt.setCounts("");
//						cpt.setPosId("");
//					}
//				}else {
//					cpt.setCounts("");
//					cpt.setPosId("");
//				}
//			}
//			result.put("list", list);
//		}
		return result;
	}

	// 完成
	public Map<String, Object> findList(Integer start, Integer length, String query, String col, String sort, String id)
			throws Exception {
//		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Map<String, Object> pro = new HashMap<String, Object>();
		CellPassage cellPassage = productionPlanDao.get(CellPassage.class, id);
		if (cellPassage.getStateName().equals("不合格终止")) {
			List<CellPassageTemplate> cellPassageTemplates = productionPlanDao.findCellPassageTemplate(id);
			String orderNum = "";
			for (CellPassageTemplate cellPassageTemplate : cellPassageTemplates) {
				if (cellPassageTemplate.getOrderNum() != null && !"".equals(cellPassageTemplate.getOrderNum())) {
					orderNum = orderNum + "'" + cellPassageTemplate.getOrderNum() + "',";
				}
			}
			orderNum = orderNum.substring(0, orderNum.length() - 1);
			pro = productionPlanDao.findBuHeGe(start, length, query, col, sort, orderNum, id);
//			    list.add( pro);

		} else {
			pro = productionPlanDao.findList(start, length, query, col, sort, id);
//			list.add( pro);
		}

//		List<CellProductionRecord> list = (List<CellProductionRecord>) pro.get("list");
//		if(list.size()>0) {
//			for(CellProductionRecord cpt:list) {
//				if(cpt.getEndTime()==null||"".equals(cpt.getEndTime())) {
//					List<CellPassageItem> cpis = productionPlanDao.getCellPassageItems(cpt.getCellPassage(),cpt.getOrderNum());
//					if(cpis.size()>0) {
//						CellPassageItem cpi = cpis.get(0);
//						cpt.setCounts(cpi.getCounts());
//						cpt.setPosId(cpi.getPosId());
//					}else {
//						cpt.setCounts("");
//						cpt.setPosId("");
//					}
//				}else {
//					cpt.setCounts("");
//					cpt.setPosId("");
//				}
//			}
//			pro.put("list", list);
//		}
		return pro;
	}

	public List<InfluenceProduct> findInfluenceProducts(String ordercode) {
		return productionPlanDao.findInfluenceProductList(ordercode);
	}

	// 根据日期查找
	public List<CellProductionRecord> findProductState(String planWorkDate) {
		List<CellProductionRecord> cellProductionRecords = productionPlanDao.findProductState(planWorkDate);
		if (!cellProductionRecords.isEmpty()) {
			for (CellProductionRecord cellProductionRecord : cellProductionRecords) {
				if (cellProductionRecord.getTempleOperator() != null) {
					if (!"".equals(cellProductionRecord.getTempleOperator().getId())
							&& cellProductionRecord.getTempleOperator().getId() != null) {
						cellProductionRecord.getTempleOperator();

					}

				}
			}

		}
		return null;

	}

	public Map<String, Object> showInfluenceProductTable(Integer start, Integer length, String query, String col,
			String sort, String flag) throws Exception {
		return productionPlanDao.showInfluenceProductTable(start, length, query, col, sort, flag);
	}

	public List<CellPassageItem> getCellPassageItems(CellPassage cellPassage, String orderNum) {
		return productionPlanDao.getCellPassageItems(cellPassage, orderNum);
	}

	public Instrument findMicroscopeNo(String microscopeNo) {
		return productionPlanDao.findMicroscopeNo(microscopeNo);
	}

	public List<CellPassageItem> findSampleId(String sampleId,String id,String ordernum) {
		return productionPlanDao.findSampleId(sampleId,id,ordernum);
	}

	public List<CellPassageItem> findIncubator(String incubator, String id, String ordernum) {
		return productionPlanDao.findIncubator(incubator,id,ordernum);
	}
}
