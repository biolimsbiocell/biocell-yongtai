﻿package com.biolims.experiment.production.action;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.deviation.model.InfluenceProduct;
import com.biolims.equipment.model.Instrument;
import com.biolims.experiment.cell.passage.model.CellPassage;
import com.biolims.experiment.cell.passage.model.CellPassageItem;
import com.biolims.experiment.cell.passage.model.CellProductionRecord;
import com.biolims.experiment.production.service.ProductionPlanService;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.system.newsyscode.model.CodeMainNew;
import com.biolims.system.newsyscode.service.CodeMainNewService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.ObjectToMapUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/productionPlan/productionPlan")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class ProductionPlanAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "210000101";
	@Autowired
	private ProductionPlanService productionPlanService;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodeMainNewService codeMainNewService;

	@Action(value = "showDateInputDialog")
	public String showDateInputDialog() {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/productionPlan/dateInputDialog.jsp");
	}

	@Action(value = "showAllProductionTable")
	public String showAllProductionTable() throws Exception {
		String type = getParameterFromRequest("type");
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		putObjToContext("type", type);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/productionPlan/productionPlanTable.jsp");
	}

	/**
	 * 查询所有的生产任务template中有结束时间的,并显示对应的操作员生产待办上面 @Title:
	 * showAllProductionList @Description: TODO @param @return
	 * void @author @date @throws
	 */
	@Action(value = "showAllProductionTableList")
	public void showAllProductionTableList() {

		String type = getParameterFromRequest("type");
		String inputDate = getParameterFromRequest("inputDate");
		String inputDateEnd = getParameterFromRequest("inputDateEnd");
		String bzcx = getParameterFromRequest("bzcx");
		String dbbacx[] = null;
		if (bzcx != null && !"".equals(bzcx)) {
			dbbacx = bzcx.split(",");
		}

		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			if (!"".equals(query) && query != null) {
				result = productionPlanService.findQuProductionList(start, length, query, col, sort, type, inputDate,
						inputDateEnd, dbbacx);
			} else {
				result = productionPlanService.findAllProductionList(start, length, query, col, sort, type, inputDate,
						inputDateEnd, dbbacx);
			}
			List<Object> list = (List<Object>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("confirmDate", "yyyy-MM-dd");
			map.put("template-name", "");
			map.put("testUserOneName", "");
			map.put("stateName", "");
			map.put("state", "");
			map.put("scopeName", "");
			map.put("batch", "");
			map.put("filtrateCode", "");
			map.put("patientName", "");
			map.put("place", "");
			map.put("infectionType", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 保存细胞观察
	 * 
	 * @throws Exception
	 */
	@Action(value = "SaveCellObservation")
	public void SaveCellObservation() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String scjhid = getParameterFromRequest("scjhid");
			String observationResult = getParameterFromRequest("observationResult");
			String cellNum = getParameterFromRequest("cellNum");
			String cellNumber = getParameterFromRequest("cellNumber");
			String cellDensity = getParameterFromRequest("cellDensity");
			String observationNote = getParameterFromRequest("observationNote");
			String operationState = getParameterFromRequest("operationState");
			String microscopeNo = getParameterFromRequest("microscopeNo");
			String bh = getParameterFromRequest("bh");
			String zt = getParameterFromRequest("zt");
			String mc = getParameterFromRequest("mc");
			String sampleId = getParameterFromRequest("sampleId");
			String sampleCheck = getParameterFromRequest("sampleCheck");
			productionPlanService.SaveCellObservation(scjhid, observationResult, cellNum, cellNumber, cellDensity,
					observationNote, operationState, microscopeNo, bh, mc, zt, sampleId, sampleCheck);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 保存细胞观察
	 * 
	 * @throws Exception
	 */
	@Action(value = "findProductState")
	public void findProductState() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String planWorkDate = getParameterFromRequest("planWorkDate");

			productionPlanService.findProductState(planWorkDate);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "findCprPossibleModify")
	public void findCprPossibleModify() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String cprId = getParameterFromRequest("cprId");
		if (!"".equals(cprId) && cprId != null) {
			CellProductionRecord cellProductionRecord = commonDAO.get(CellProductionRecord.class, cprId);
			if (cellProductionRecord != null) {
				if ("1".equals(cellProductionRecord.getPossibleModify())) {
					map.put("success", true);

				} else {
					map.put("success", false);
				}
			} else {
				map.put("success", false);

			}
		} else {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 保存PC原代实验模板
	 * 
	 * @throws Exception
	 */
	@Action(value = "saveCPtemplateitem")
	public void saveCPtemplateitem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String id = getParameterFromRequest("id");
			String userid = getParameterFromRequest("userid");
			String operationdate = getParameterFromRequest("operationdate");
			String templateid = getParameterFromRequest("templateid");
			String room = getParameterFromRequest("room");
			String roomId = getParameterFromRequest("roomId");
			String note = getParameterFromRequest("note");
			productionPlanService.saveCPtemplateitem(id, userid, operationdate, templateid, room, roomId, note);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 查询该样本是否存在为解决的偏差
	 * 
	 * @throws Exception
	 */
	@Action(value = "findDeviations")
	public void findDeviations() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
//			String ordercode = getParameterFromRequest("ordercode");
			String ordercode = getParameterFromRequest("cellPassageBatch");
			List<InfluenceProduct> ips = productionPlanService.findInfluenceProducts(ordercode);
			if (ips.size() > 0) {
				map.put("success", true);
			} else {
				map.put("success", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "findCellPassageTemplate1List")
	public void findCellPassageTemplate1List() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String id = getParameterFromRequest("id");

		Map<String, Object> result = new HashMap<String, Object>();
		result = productionPlanService.findCellPrimaryCultureTemplate1List(start, length, query, col, sort, id);
		List<CellProductionRecord> list = (List<CellProductionRecord>) result.get("list");
//		if(list.size()>0){
//			for(CellProductionRecord cpr:list){
//				List<CellPassageItem> cpis = productionPlanService.getCellPassageItems(cpr.getCellPassage(),cpr.getOrderNum());
//				if(cpis.size()>0){
//					CellPassageItem cpi = cpis.get(0);
//					cpr.setCounts(cpi.getCounts());
//					cpr.setPosId(cpi.getPosId());
//				}else{
//					cpr.setCounts("");
//					cpr.setPosId("");
//				}
//			}
//		}
//		List<CellProductionRecord> list = (List<CellProductionRecord>) result.get("list");
		if (list.size() > 0) {
			for (CellProductionRecord cpt : list) {
				if (cpt.getEndTime() == null || "".equals(cpt.getEndTime())) {
					List<CellPassageItem> cpis = productionPlanService.getCellPassageItems(cpt.getCellPassage(),
							cpt.getOrderNum());
					if (cpis.size() > 0) {
						CellPassageItem cpi = cpis.get(0);
						cpt.setCounts(cpi.getCounts());
						cpt.setPosId(cpi.getPosId());
					} else {
						cpt.setCounts("");
						cpt.setPosId("");
					}
				} else {
					cpt.setCounts("");
					cpt.setPosId("");
				}
			}
		}
		Map<String, String> map = new HashMap<String, String>();
		CellProductionRecord cpr = new CellProductionRecord();
		map = (Map<String, String>) ObjectToMapUtils.getMapKey(cpr);
		map.put("templeItem-name", "");
		map.put("cellPassage-id", "");
		map.put("templeOperator-name", "");

		map.put("counts", "");
		map.put("posId", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	@Action(value = "NewFindCellPassageTemplate1List")
	public void NewFindCellPassageTemplate1List() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String id = getParameterFromRequest("id");
		String pici = getParameterFromRequest("pici");

		Map<String, Object> result = new HashMap<String, Object>();
		result = productionPlanService.NewFindCellPrimaryCultureTemplate1List(start, length, query, col, sort, id,
				pici);
		List<CellProductionRecord> list = (List<CellProductionRecord>) result.get("list");
		if (list != null && list.size() > 0) {
			for (CellProductionRecord cpt : list) {
				if (cpt.getEndTime() == null || "".equals(cpt.getEndTime())) {
					List<CellPassageItem> cpis = productionPlanService.getCellPassageItems(cpt.getCellPassage(),
							cpt.getOrderNum());
					if (cpis.size() > 0) {
						CellPassageItem cpi = cpis.get(0);
						cpt.setCounts(cpi.getCounts());
						cpt.setPosId(cpi.getPosId());
					} else {
						cpt.setCounts("");
						cpt.setPosId("");
					}
				} else {
					cpt.setCounts("");
					cpt.setPosId("");
				}
			}
		}
		Map<String, String> map = new HashMap<String, String>();
		CellProductionRecord cpr = new CellProductionRecord();
		map = (Map<String, String>) ObjectToMapUtils.getMapKey(cpr);
		map.put("cellPassage-id", "");
		map.put("cellPassage-batch", "");
		map.put("cellPassage-patientName", "");
		map.put("cellPassage-filtrateCode", "");
		map.put("orderNum", "");
		map.put("templeItem-name", "");
		map.put("estimatedDate", "");
		map.put("cellPassage-infectionType", "");
		map.put("cellPassage-place", "");
		map.put("templeItem-bottleOrBag", "");
		map.put("cellPassage-template-name", "");

		map.put("endTime", "yyyy-MM-dd");
		map.put("planWorkDate", "yyyy-MM-dd");
		map.put("cellPassage-id", "");

		map.put("templeOperator-id", "");
		map.put("templeOperator-name", "");
		map.put("operatingRoomId", "");
		map.put("note", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	@Action(value = "findCellPassageTemplateList")
	public void findCellPassageTemplateList() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String id = getParameterFromRequest("id");
		Map<String, Object> result = new HashMap<String, Object>();
		result = productionPlanService.findCellPrimaryCultureTemplateList(start, length, query, col, sort, id);
		List<CellProductionRecord> list = (List<CellProductionRecord>) result.get("list");

		if (list.size() > 0) {
			for (CellProductionRecord cpt : list) {
				if (cpt.getEndTime() == null || "".equals(cpt.getEndTime())) {
					List<CellPassageItem> cpis = productionPlanService.getCellPassageItems(cpt.getCellPassage(),
							cpt.getOrderNum());
					if (cpis.size() > 0) {
						CellPassageItem cpi = cpis.get(0);
						cpt.setCounts(cpi.getCounts());
						cpt.setPosId(cpi.getPosId());
					} else {
						cpt.setCounts("");
						cpt.setPosId("");
					}
				} else {
					cpt.setCounts("");
					cpt.setPosId("");
				}
			}
		}

		Map<String, String> map = new HashMap<String, String>();
		CellProductionRecord cpr = new CellProductionRecord();
		map = (Map<String, String>) ObjectToMapUtils.getMapKey(cpr);
		map.put("templeItem-name", "");
		map.put("endTime", "yyyy-MM-dd");
		map.put("planWorkDate", "yyyy-MM-dd");
		map.put("cellPassage-id", "");

		map.put("templeOperator-id", "");
		map.put("templeOperator-name", "");
		map.put("operatingRoomId", "");
		map.put("note", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	@Action(value = "newFindCellPassageTemplateList")
	public void newFindCellPassageTemplateList() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
//		String col = getParameterFromRequest("columns[4][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String bottleOrBag = getParameterFromRequest("bottleOrBag");
		String infectionType = getParameterFromRequest("infectionType");
		String dataS = getParameterFromRequest("date_input");
		String dataE = getParameterFromRequest("date_inputEnd");
		String productHideFields = getParameterFromRequest("productHideFields");
		String produceId = getParameterFromRequest("produceId");
		String pici = getParameterFromRequest("pici");

		String dbbacx[] = null;
		if (productHideFields != null && !"".equals(productHideFields)) {
			dbbacx = productHideFields.split(",");
		}
		String id = getParameterFromRequest("id");
		Map<String, Object> result = new HashMap<String, Object>();
		result = productionPlanService.newFindCellPrimaryCultureTemplateList(start, length, query, col, sort, id,
				bottleOrBag, infectionType, dataS, dataE, dbbacx, produceId, pici);
		List<CellProductionRecord> list = (List<CellProductionRecord>) result.get("list");

		if (list != null && list.size() > 0) {
//		if(!list.isEmpty()) {
			for (CellProductionRecord cpt : list) {
				if (cpt.getEndTime() == null || "".equals(cpt.getEndTime())) {
					List<CellPassageItem> cpis = productionPlanService.getCellPassageItems(cpt.getCellPassage(),
							cpt.getOrderNum());
					if (cpis.size() > 0) {
						CellPassageItem cpi = cpis.get(0);
						cpt.setCounts(cpi.getCounts());
						cpt.setPosId(cpi.getPosId());
					} else {
						cpt.setCounts("");
						cpt.setPosId("");
					}
				} else {
					cpt.setCounts("");
					cpt.setPosId("");
				}
			}
		}

		Map<String, String> map = new HashMap<String, String>();
		CellProductionRecord cpr = new CellProductionRecord();
		map = (Map<String, String>) ObjectToMapUtils.getMapKey(cpr);

		map.put("id", "");
		map.put("cellPassage-id", "");
		map.put("cellPassage-batch", "");
		map.put("cellPassage-patientName", "");
		map.put("cellPassage-filtrateCode", "");
		map.put("orderNum", "");
		map.put("templeItem-name", "");
		map.put("estimatedDate", "");
		map.put("cellPassage-infectionType", "");
		map.put("cellPassage-place", "");
		map.put("templeItem-bottleOrBag", "");
		map.put("cellPassage-template-name", "");

		map.put("endTime", "yyyy-MM-dd");
		map.put("planWorkDate", "yyyy-MM-dd");
		map.put("cellPassage-id", "");

		map.put("templeOperator-id", "");
		map.put("templeOperator-name", "");
		map.put("operatingRoomId", "");
		map.put("note", "");

		map.put("templeItem-cellObservation", "");
		map.put("operationState", "");
		map.put("observationDate", "yyyy-MM-dd");
		map.put("observationResult", "");
		map.put("observationUser-name", "");

		map.put("templeItem-reinfusionPlan", "");

		map.put("microscopeNo", "");
		map.put("microscopeName", "");
		map.put("microscopeStateName", "");
		map.put("sampleId", "");
		map.put("sampleCheck", "");
		map.put("incubator", "");
		map.put("incubatorState", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	// 打印机
	@Action(value = "dyMakeCode", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void SmakeCode() throws Exception {

		String[] ids = getRequest().getParameterValues("ids[]");
		String printCode = getParameterFromRequest("printCode");
		Map<String, Object> map = new HashMap<String, Object>();
		for (String id : ids) {
			CellProductionRecord item = commonDAO.get(CellProductionRecord.class, id);
			String batchA = item.getCellPassage().getBatch();
			String batchB = item.getCellPassage().getBatch();
			;
			int printNum = Integer.parseInt("1");
			for (int i = 0; i < printNum; i++) {
				CodeMainNew codeMain = codeMainNewService.get(printCode);
				if (codeMain != null) {
					String codeFull = batchA;
					String code1 = batchA.substring(0, id.length() - 13);
					String code2 = batchA.substring(id.length() - 13);
					String printStr = codeMain.getCode();
					printStr = printStr.replaceAll("@@code1@@", code1);
					printStr = printStr.replaceAll("@@code2@@", code2);
					printStr = printStr.replaceAll("@@code@@", codeFull);
					printStr = printStr.replaceAll("@@name@@", batchB);
					String ip = codeMain.getIp();
					Socket socket = null;
					OutputStream os;
					try {
						System.out.println(printStr);
						socket = new Socket();
						SocketAddress sa = new InetSocketAddress(ip, 9100);
						socket.connect(sa);
						os = socket.getOutputStream();
						os.write(printStr.getBytes("UTF-8"));
						os.flush();
						map.put("success", true);
					} catch (UnknownHostException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					} finally {
						if (socket != null) {
							try {
								socket.close();
							} catch (IOException e) {
							}
						}
					}
				}
			}
		}

		HttpUtils.write(JsonUtils.toJsonString(map));

	}

	@Action(value = "findCellPassageTemplateerList")
	public void findCellPassageTemplateerList() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String id = getParameterFromRequest("id");
		String inputDate = getParameterFromRequest("inputDate");
		String inputDateEnd = getParameterFromRequest("inputDateEnd");
		String bzcx = getParameterFromRequest("bzcx");
		String[] dbbacx = null;
		if (bzcx != null && !"".equals(bzcx)) {
			dbbacx = bzcx.split(",");
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result = productionPlanService.findCellPrimaryCultureTemplateerList(start, length, query, col, sort, id,
				inputDate, inputDateEnd, dbbacx);
		List<CellProductionRecord> list = (List<CellProductionRecord>) result.get("list");

		if (list.size() > 0) {
			for (CellProductionRecord cpt : list) {
				if (cpt.getEndTime() == null || "".equals(cpt.getEndTime())) {
					List<CellPassageItem> cpis = productionPlanService.getCellPassageItems(cpt.getCellPassage(),
							cpt.getOrderNum());
					if (cpis.size() > 0) {
						CellPassageItem cpi = cpis.get(0);
						cpt.setCounts(cpi.getCounts());
						cpt.setPosId(cpi.getPosId());
					} else {
						cpt.setCounts("");
						cpt.setPosId("");
					}
				} else {
					cpt.setCounts("");
					cpt.setPosId("");
				}
			}
		}

		Map<String, String> map = new HashMap<String, String>();
		CellProductionRecord cpr = new CellProductionRecord();
		map = (Map<String, String>) ObjectToMapUtils.getMapKey(cpr);
		map.put("templeItem-name", "");
		map.put("cellPassage-id", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	// 已经完成
	@Action(value = "findList")
	public void findList() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String id = getParameterFromRequest("id");
//			String inputDate = getParameterFromRequest("inputDate");
//			String inputDateEnd = getParameterFromRequest("inputDateEnd");

		Map<String, Object> result = new HashMap<String, Object>();
		result = productionPlanService.findList(start, length, query, col, sort, id);
		List<CellProductionRecord> list = (List<CellProductionRecord>) result.get("list");

		if (list.size() > 0) {
			for (CellProductionRecord cpt : list) {
				if (cpt.getEndTime() == null || "".equals(cpt.getEndTime())) {
					List<CellPassageItem> cpis = productionPlanService.getCellPassageItems(cpt.getCellPassage(),
							cpt.getOrderNum());
					if (cpis.size() > 0) {
						CellPassageItem cpi = cpis.get(0);
						cpt.setCounts(cpi.getCounts());
						cpt.setPosId(cpi.getPosId());
					} else {
						cpt.setCounts("");
						cpt.setPosId("");
					}
				} else {
					cpt.setCounts("");
					cpt.setPosId("");
				}
			}
		}

		Map<String, String> map = new HashMap<String, String>();
		CellProductionRecord cpr = new CellProductionRecord();
		map = (Map<String, String>) ObjectToMapUtils.getMapKey(cpr);
		map.put("templeItem-name", "");
		map.put("cellPassage-id", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	@Action(value = "showInfluenceProductList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String dicTypeSelectTable() throws Exception {
		String zid = getParameterFromRequest("id");
		CellProductionRecord cpr = new CellProductionRecord();
		cpr = commonDAO.get(CellProductionRecord.class, zid);
		putObjToContext("id", cpr.getCellPassage().getBatch());
		return dispatcher("/WEB-INF/page/experiment/productionPlan/showInfluenceProductList.jsp");
	}

	@Action(value = "showInfluenceProductTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void dicTypeSelectTableJson() throws Exception {
		String flag = getParameterFromRequest("flag");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = productionPlanService.showInfluenceProductTable(start, length, query, col, sort,
				flag);
		List<InfluenceProduct> list = (List<InfluenceProduct>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("orderCode", "");
		map.put("note", "");
		map.put("deviationHandlingReport-name", "");
		map.put("deviationHandlingReport-id", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * 显微镜编号
	 * 
	 * @throws Exception
	 */
	@Action(value = "findMicroscopeNo")
	public void findMicroscopeNo() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String microscopeNo = getParameterFromRequest("microscopeNo");
			Instrument in = productionPlanService.findMicroscopeNo(microscopeNo);
			if (in != null) {
				if (in.getState().getId().equals("1r")) {
					map.put("zy", true);
					map.put("name", in.getName());
					map.put("bh", in.getId());
					map.put("stateName", in.getState().getName());
				} else {
					map.put("zy", false);
					map.put("name", in.getName());
					map.put("bh", in.getId());
					map.put("stateName", in.getState().getName());
				}
				map.put("success", true);
			} else {
				map.put("success", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 样本编号
	 * 
	 * @throws Exception
	 */
	@Action(value = "findSampleId")
	public void findSampleId() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String id = getParameterFromRequest("id");
			String sampleId = getParameterFromRequest("sampleId");
			String ordernum = getParameterFromRequest("ordernum");
			List<CellPassageItem> in = productionPlanService.findSampleId(sampleId, id, ordernum);
			if (!in.isEmpty()) {
				map.put("sampleCheck", "已确认");
				map.put("success", true);
			} else {
				map.put("success", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 培养箱
	 * 
	 * @throws Exception
	 */
	@Action(value = "findIncubator")
	public void findIncubator() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String id = getParameterFromRequest("id");
			String incubator = getParameterFromRequest("incubator");
			String ordernum = getParameterFromRequest("ordernum");
			List<CellPassageItem>  in = productionPlanService.findIncubator(incubator, id, ordernum);
			if (!in.isEmpty()) {
				map.put("pyx", in.get(0).getInstrument().getId());
				map.put("pyxState", "已匹配");
				map.put("success", true);
			} else {
				map.put("success", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

}
