package com.biolims.experiment.producer.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.producer.service.ProducerTaskService;

public class ProducerTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		ProducerTaskService mbService = (ProducerTaskService) ctx
				.getBean("producerTaskService");
		mbService.chengeState(applicationTypeActionId, contentId);

		return "";
	}
}
