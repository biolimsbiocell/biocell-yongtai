package com.biolims.experiment.producer.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 制片上机结果
 * @author lims-platform
 * @date 2016-03-25 10:16:50
 * @version V1.0
 * 
 */
@Entity
@Table(name = "PRODUCER_TASK_RESULT")
@SuppressWarnings("serial")
public class ProducerTaskResult extends EntityDao<ProducerTaskResult> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 描述 */
	private String name;
	/** 样本编号 */
	private String code;
	/** 玻片编号 */
	private String slideCode;
	/** 接种收获编号 */
	private String jzshCode;
	/** 原始样本编号 */
	private String sampleCode;
	/** 建库任务单编号 */
	private String jkTaskId;
	/** index */
	private String indexa;
	/** 体积 */
	private Double volume;
	/** 单位 */
	private String unit;
	/** 处理结果 */
	private String result;
	/** 失败原因 */
	private String reason;
	/** 是否提交 */
	private String submit;
	/** 患者姓名 */
	private String patientName;
	/** 检测项目编号 */
	private String productId;
	/** 检测项目名称 */
	private String productName;
	/** 取样时间 */
	private Date inspectDate;
	/** 接收日期 */
	private Date acceptDate;
	/**制片日期 */
	private Date resultDate;
	/** 身份证 */
	private String idCard;
	/** 手机号 */
	private String phone;
	/** 关联任务单 */
	private String orderId;
	/** 检测方法 */
	private String sequenceFun;
	/** 应出报告时间 */
	private Date reportDate;
	/** 状态 */
	private String state;
	/** 处理意见 */
	private String method;
	/** 确定执行 */
	private String isExecute;
	/** 备注 */
	private String note;
	/** 相关主表 */
	private ProducerTask producerTask;
	/** 行号 */
	private String rowCode;
	/** 列号 */
	private String colCode;
	/** 板号 */
	private String counts;
	/** 合同id */
	private String contractId;
	/** 项目id */
	private String projectId;
	/** 任务单类型 */
	private String orderType;
	/** 区分临床还是科技服务 0 临床 1 科技服务 */
	private String classify;
	/** 数据类型 */
	private String dataType;
	/** 数据量 */
	private String dataNum;
	/** 下一步流向ID */
	private String nextFlowId;
	/** 下一步流向 */
	private String nextFlow;
	// 样本类型
	private String sampleType;
	/** 临时表Id */
	private String tempId;

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	public String getNextFlowId() {
		return nextFlowId;
	}

	public void setNextFlowId(String nextFlowId) {
		this.nextFlowId = nextFlowId;
	}

	public String getNextFlow() {
		return nextFlow;
	}

	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 100)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本编号
	 */
	@Column(name = "CODE", length = 50)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 原始样本编号
	 */
	@Column(name = "SAMPLE_CODE", length = 50)
	public String getSampleCode() {
		return this.sampleCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 原始样本编号
	 */
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 建库任务单编号
	 */
	@Column(name = "JK_TASK_ID", length = 50)
	public String getJkTaskId() {
		return this.jkTaskId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 建库任务单编号
	 */
	public void setJkTaskId(String jkTaskId) {
		this.jkTaskId = jkTaskId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String index
	 */
	@Column(name = "INDEXA", length = 50)
	public String getIndexa() {
		return this.indexa;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String index
	 */
	public void setIndexa(String indexa) {
		this.indexa = indexa;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 体积
	 */
	@Column(name = "VOLUME", length = 50)
	public Double getVolume() {
		return this.volume;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 体积
	 */
	public void setVolume(Double volume) {
		this.volume = volume;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 单位
	 */
	@Column(name = "UNIT", length = 50)
	public String getUnit() {
		return this.unit;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 单位
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 处理结果
	 */
	@Column(name = "RESULT", length = 50)
	public String getResult() {
		return this.result;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 处理结果
	 */
	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 失败原因
	 */
	@Column(name = "REASON", length = 50)
	public String getReason() {
		return this.reason;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 失败原因
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 是否提交
	 */
	@Column(name = "SUBMIT", length = 50)
	public String getSubmit() {
		return this.submit;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 是否提交
	 */
	public void setSubmit(String submit) {
		this.submit = submit;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 患者姓名
	 */
	@Column(name = "PATIENT_NAME", length = 50)
	public String getPatientName() {
		return this.patientName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 患者姓名
	 */
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 检测项目编号
	 */
	@Column(name = "PRODUCT_ID", length = 50)
	public String getProductId() {
		return this.productId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 检测项目编号
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 检测项目名称
	 */
	@Column(name = "PRODUCT_NAME", length = 50)
	public String getProductName() {
		return this.productName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 检测项目名称
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 取样时间
	 */
	@Column(name = "INSPECT_DATE", length = 50)
	public Date getInspectDate() {
		return this.inspectDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 取样时间
	 */
	public void setInspectDate(Date inspectDate) {
		this.inspectDate = inspectDate;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 接收日期
	 */
	@Column(name = "ACCEPT_DATE", length = 50)
	public Date getAcceptDate() {
		return this.acceptDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 接收日期
	 */
	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 身份证
	 */
	@Column(name = "ID_CARD", length = 50)
	public String getIdCard() {
		return this.idCard;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 身份证
	 */
	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 手机号
	 */
	@Column(name = "PHONE", length = 50)
	public String getPhone() {
		return this.phone;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 手机号
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 关联任务单
	 */
	@Column(name = "ORDER_ID", length = 50)
	public String getOrderId() {
		return this.orderId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 关联任务单
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 检测方法
	 */
	@Column(name = "SEQUENCE_FUN", length = 50)
	public String getSequenceFun() {
		return this.sequenceFun;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 检测方法
	 */
	public void setSequenceFun(String sequenceFun) {
		this.sequenceFun = sequenceFun;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 应出报告时间
	 */
	@Column(name = "REPORT_DATE", length = 50)
	public Date getReportDate() {
		return this.reportDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 应出报告时间
	 */
	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 处理意见
	 */
	@Column(name = "METHOD", length = 50)
	public String getMethod() {
		return this.method;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 处理意见
	 */
	public void setMethod(String method) {
		this.method = method;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 确定执行
	 */
	@Column(name = "IS_EXECUTE", length = 50)
	public String getIsExecute() {
		return this.isExecute;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 确定执行
	 */
	public void setIsExecute(String isExecute) {
		this.isExecute = isExecute;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得SnpTask
	 * 
	 * @return: SnpTask 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PRODUCER_TASK")
	public ProducerTask getProducerTask() {
		return this.producerTask;
	}

	/**
	 * 方法: 设置SnpTask
	 * 
	 * @param: SnpTask 相关主表
	 */
	public void setProducerTask(ProducerTask producerTask) {
		this.producerTask = producerTask;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 行号
	 */
	@Column(name = "ROW_CODE", length = 50)
	public String getRowCode() {
		return this.rowCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 行号
	 */
	public void setRowCode(String rowCode) {
		this.rowCode = rowCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 列号
	 */
	@Column(name = "COL_CODE", length = 50)
	public String getColCode() {
		return this.colCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 列号
	 */
	public void setColCode(String colCode) {
		this.colCode = colCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 板号
	 */
	@Column(name = "COUNTS", length = 50)
	public String getCounts() {
		return this.counts;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 板号
	 */
	public void setCounts(String counts) {
		this.counts = counts;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 合同id
	 */
	@Column(name = "CONTRACT_ID", length = 50)
	public String getContractId() {
		return this.contractId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 合同id
	 */
	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 项目id
	 */
	@Column(name = "PROJECT_ID", length = 50)
	public String getProjectId() {
		return this.projectId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 项目id
	 */
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 任务单类型
	 */
	@Column(name = "ORDER_TYPE", length = 50)
	public String getOrderType() {
		return this.orderType;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 任务单类型
	 */
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 区分临床还是科技服务 0 临床 1 科技服务
	 */
	@Column(name = "CLASSIFY", length = 50)
	public String getClassify() {
		return this.classify;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 区分临床还是科技服务 0 临床 1 科技服务
	 */
	public void setClassify(String classify) {
		this.classify = classify;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 数据类型
	 */
	@Column(name = "DATA_TYPE", length = 50)
	public String getDataType() {
		return this.dataType;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 数据类型
	 */
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 数据量
	 */
	@Column(name = "DATA_NUM", length = 50)
	public String getDataNum() {
		return this.dataNum;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 数据量
	 */
	public void setDataNum(String dataNum) {
		this.dataNum = dataNum;
	}

	public String getJzshCode() {
		return jzshCode;
	}

	public void setJzshCode(String jzshCode) {
		this.jzshCode = jzshCode;
	}

	public String getSlideCode() {
		return slideCode;
	}

	public void setSlideCode(String slideCode) {
		this.slideCode = slideCode;
	}

	public Date getResultDate() {
		return resultDate;
	}

	public void setResultDate(Date resultDate) {
		this.resultDate = resultDate;
	}
	
}