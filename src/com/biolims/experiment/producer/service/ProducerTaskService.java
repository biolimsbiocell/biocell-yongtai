package com.biolims.experiment.producer.service;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.karyoabnormal.model.KaryoAbnormal;
import com.biolims.experiment.karyotyping.model.KaryotypingTaskTemp;
import com.biolims.experiment.producer.dao.ProducerTaskDao;
import com.biolims.experiment.producer.model.ProducerTask;
import com.biolims.experiment.producer.model.ProducerTaskCos;
import com.biolims.experiment.producer.model.ProducerTaskItem;
import com.biolims.experiment.producer.model.ProducerTaskReagent;
import com.biolims.experiment.producer.model.ProducerTaskResult;
import com.biolims.experiment.producer.model.ProducerTaskTemp;
import com.biolims.experiment.producer.model.ProducerTaskTemplate;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.report.model.EndReport;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class ProducerTaskService {
	@Resource
	private ProducerTaskDao producerTaskDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private NextFlowDao nextFlowDao;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findProducerTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return producerTaskDao.selectProducerTaskList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	public Map<String, Object> findProducerTaskListByState(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return producerTaskDao.selectProducerTaskListByState(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(ProducerTask i) throws Exception {

		producerTaskDao.saveOrUpdate(i);

	}

	public ProducerTask get(String id) {
		ProducerTask ProducerTask = commonDAO.get(ProducerTask.class, id);
		return ProducerTask;
	}

	public Map<String, Object> findProducerTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = producerTaskDao
				.selectProducerTaskItemList(scId, startNum, limitNum, dir, sort);
		List<ProducerTaskItem> list = (List<ProducerTaskItem>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findProducerTaskTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = producerTaskDao
				.selectProducerTaskTemplateList(scId, startNum, limitNum, dir,
						sort);
		List<ProducerTaskTemplate> list = (List<ProducerTaskTemplate>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findProducerTaskReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = producerTaskDao
				.selectProducerTaskReagentList(scId, startNum, limitNum, dir,
						sort);
		List<ProducerTaskReagent> list = (List<ProducerTaskReagent>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findProducerTaskCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = producerTaskDao.selectProducerTaskCosList(
				scId, startNum, limitNum, dir, sort);
		List<ProducerTaskCos> list = (List<ProducerTaskCos>) result.get("list");
		return result;
	}

	public Map<String, Object> findProducerTaskResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = producerTaskDao
				.selectProducerTaskResultList(scId, startNum, limitNum, dir,
						sort);
		List<ProducerTaskResult> list = (List<ProducerTaskResult>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findProducerTaskTempList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort)
			throws Exception {
		/*Map<String, Object> result = producerTaskDao
				.selectProducerTaskTempList(mapForQuery, startNum, limitNum, dir, sort);
		List<ProducerTaskTemp> list = (List<ProducerTaskTemp>) result
				.get("list");
		return result;*/
		Map<String, Object> result2 = new HashMap<String, Object>();
		List<ProducerTaskTemp> list2=new ArrayList<ProducerTaskTemp>();
		Map<String, Object> result =  producerTaskDao
				.selectProducerTaskTempList(mapForQuery,
				startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<ProducerTaskTemp> list = (List<ProducerTaskTemp>) result.get("list");
		for(ProducerTaskTemp t:list){
			SampleInfo s=this.sampleInfoMainDao.findSampleInfo(t.getSampleCode());
			if(s!=null && s.getSampleOrder()!=null){
				t.setChargeNote(s.getSampleOrder().getChargeNote());
			}
			list2.add(t);
		}
		result2.put("total", count);
		result2.put("list", list2);
		return result2;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveProducerTaskItem(ProducerTask sc, String itemDataJson)
			throws Exception {
		List<ProducerTaskItem> saveItems = new ArrayList<ProducerTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			ProducerTaskItem scp = new ProducerTaskItem();
			// 将map信息读入实体类
			scp = (ProducerTaskItem) producerTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setProducerTask(sc);

			saveItems.add(scp);
			// 改变左侧样本状态
			ProducerTaskTemp dt = commonDAO.get(ProducerTaskTemp.class, scp.getTempId());
			if (dt != null) {
				dt.setState("2");
			}
			commonDAO.saveOrUpdate(dt);
		}
		producerTaskDao.saveOrUpdateAll(saveItems);
		List<ProducerTaskItem> snlist=producerTaskDao.getItem(sc.getId());
		List<ProducerTaskReagent> rglist=producerTaskDao.getReagent(sc.getId());
		if(snlist.size()>0){
			if(rglist.size()>0){
				//把样本数量传到试剂明细中
				for(ProducerTaskReagent sr:rglist){
					sr.setSampleNum(Double.valueOf(snlist.size()));
				}
			}
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delProducerTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			ProducerTaskItem scp = producerTaskDao.get(ProducerTaskItem.class,
					id);
			producerTaskDao.delete(scp);
			
			// 改变左侧样本状态
			ProducerTaskTemp dt = commonDAO.get(ProducerTaskTemp.class, scp.getTempId());
			if (dt != null) {
				dt.setState("1");
			}
			commonDAO.saveOrUpdate(dt);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveProducerTaskTemplate(ProducerTask sc, String itemDataJson)
			throws Exception {
		List<ProducerTaskTemplate> saveItems = new ArrayList<ProducerTaskTemplate>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			ProducerTaskTemplate scp = new ProducerTaskTemplate();
			// 将map信息读入实体类
			scp = (ProducerTaskTemplate) producerTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setProducerTask(sc);

			saveItems.add(scp);
		}
		producerTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delProducerTaskTemplate(String[] ids) throws Exception {
		for (String id : ids) {
			ProducerTaskTemplate scp = producerTaskDao.get(
					ProducerTaskTemplate.class, id);
			producerTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delProducerTaskTemplateOne(String ids) throws Exception {
		ProducerTaskTemplate scp = producerTaskDao.get(
				ProducerTaskTemplate.class, ids);
		producerTaskDao.delete(scp);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveProducerTaskReagent(ProducerTask sc, String itemDataJson)
			throws Exception {
		List<ProducerTaskReagent> saveItems = new ArrayList<ProducerTaskReagent>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			ProducerTaskReagent scp = new ProducerTaskReagent();
			// 将map信息读入实体类
			scp = (ProducerTaskReagent) producerTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setProducerTask(sc);

			saveItems.add(scp);
		}
		producerTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delProducerTaskReagent(String[] ids) throws Exception {
		for (String id : ids) {
			ProducerTaskReagent scp = producerTaskDao.get(
					ProducerTaskReagent.class, id);
			producerTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delProducerTaskReagentOne(String ids) throws Exception {
		ProducerTaskReagent scp = producerTaskDao.get(
				ProducerTaskReagent.class, ids);
		producerTaskDao.delete(scp);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveProducerTaskCos(ProducerTask sc, String itemDataJson)
			throws Exception {
		List<ProducerTaskCos> saveItems = new ArrayList<ProducerTaskCos>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			ProducerTaskCos scp = new ProducerTaskCos();
			// 将map信息读入实体类
			scp = (ProducerTaskCos) producerTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setProducerTask(sc);

			saveItems.add(scp);
		}
		producerTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delProducerTaskCos(String[] ids) throws Exception {
		for (String id : ids) {
			ProducerTaskCos scp = producerTaskDao
					.get(ProducerTaskCos.class, id);
			producerTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delProducerTaskCosOne(String ids) throws Exception {
		ProducerTaskCos scp = producerTaskDao.get(ProducerTaskCos.class, ids);
		producerTaskDao.delete(scp);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveProducerTaskResult(ProducerTask sc, String itemDataJson)
			throws Exception {
		List<ProducerTaskResult> saveItems = new ArrayList<ProducerTaskResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			ProducerTaskResult scp = new ProducerTaskResult();
			// 将map信息读入实体类
			scp = (ProducerTaskResult) producerTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setProducerTask(sc);
			// if (scp.getCode() == null || scp.getCode().equals("")) {
			// String markCode = scp.getSampleCode() + "ZP";
			// String code = codingRuleService.getCode("ProducerTaskResult",
			// markCode, 00, 2, null);
			// scp.setCode(code);
			// }
			producerTaskDao.saveOrUpdate(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delProducerTaskResult(String[] ids) throws Exception {
		for (String id : ids) {
			ProducerTaskResult scp = producerTaskDao.get(
					ProducerTaskResult.class, id);
			producerTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delProducerTaskTemp(String[] ids) throws Exception {
		for (String id : ids) {
			ProducerTaskTemp scp = producerTaskDao.get(ProducerTaskTemp.class,
					id);
			producerTaskDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(ProducerTask sc, Map jsonMap) throws Exception {
		if (sc != null) {
			producerTaskDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("producerTaskItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveProducerTaskItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("producerTaskTemplate");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveProducerTaskTemplate(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("producerTaskReagent");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveProducerTaskReagent(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("producerTaskCos");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveProducerTaskCos(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("producerTaskResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveProducerTaskResult(sc, jsonStr);
			}
			List<ProducerTaskItem> snlist=producerTaskDao.getItem(sc.getId());
			List<ProducerTaskReagent> rglist=producerTaskDao.getReagent(sc.getId());
			 DecimalFormat df = new DecimalFormat("#.00");
			 Double nums = 0.00;
			if(snlist.size()>0){
				if(rglist.size()>0){
					for(ProducerTaskReagent sr:rglist){
						sr.setSampleNum(Double.valueOf(snlist.size()));
						if(sr.getOneNum()!=null && 
								sr.getSampleNum()!=null){
							nums =Double.valueOf(df.format(sr.getOneNum()*snlist.size()));
							sr.setNum(nums);
						}
					}
				}
			}
		}
	}

	/**
	 * 审核完成
	 * 
	 * @param applicationTypeActionId
	 * @param id
	 * @throws Exception
	 */
	public void chengeState(String applicationTypeActionId, String id)
			throws Exception {
		ProducerTask sct = producerTaskDao.get(ProducerTask.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		Date d = new Date();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String stime = format.format(d);
		sct.setConfirmDate(stime);

		commonDAO.update(sct);
		// 实验完成的时候改变左侧表的状态
		List<ProducerTaskResult> item = producerTaskDao
				.getProducerTaskResultList(sct.getId());
		for (ProducerTaskResult c : item) {
			ProducerTaskTemp temp = commonDAO.get(ProducerTaskTemp.class,
					c.getTempId());
			if (temp != null) {
				temp.setState("2");
			}
		}
		submitSample(id, null);
	}

	/**
	 * 根据制片上机加载info子表
	 * 
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, String>> showProducerTaskResultList(String code)
			throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = producerTaskDao
				.setProducerTaskResultList(code);
		List<ProducerTaskResult> list = (List<ProducerTaskResult>) result
				.get("list");

		if (list != null && list.size() > 0) {
			for (ProducerTaskResult pt : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", pt.getId());
				map.put("code", pt.getCode());
				map.put("slideCode", pt.getSlideCode());
				map.put("sampleCode", pt.getSampleCode());
				map.put("productId", pt.getProductId());
				map.put("productName", pt.getProductName());
				map.put("result", pt.getResult());
				map.put("note", pt.getNote());
				map.put("submit", pt.getSubmit());
				map.put("nextId", pt.getNextFlowId());
				map.put("next", pt.getNextFlow());
				mapList.add(map);
			}
		}
		return mapList;
	}

	// 提交样本
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitSample(String id, String[] ids) throws Exception {
		// 获取主表信息
		ProducerTask sc = this.producerTaskDao.get(ProducerTask.class, id);
		// 获取结果表样本信息

		List<ProducerTaskResult> list;
		if (ids == null)
			list = this.producerTaskDao.setResultById(id);
		else
			list = this.producerTaskDao.setResultByIds(ids);
		for (ProducerTaskResult scp : list) {
			if (scp != null) {
				if (scp.getResult() != null && (scp.getSubmit() == null||"".equals(scp.getSubmit()))) {
					String isOk = scp.getResult();
					if (isOk.equals("1")) {
						KaryotypingTaskTemp k = new KaryotypingTaskTemp();
						k.setCode(scp.getCode());
						k.setSampleCode(scp.getSampleCode());
						k.setProductId(scp.getProductId());
						k.setProductName(scp.getProductName());
						k.setSampleType(scp.getSampleType());
						k.setSlideCode(scp.getSlideCode());
						k.setSeqDate(new Date());
						k.setState("1");
						producerTaskDao.saveOrUpdate(k);
						DateFormat format = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss");
						sampleStateService
								.saveSampleState(
										scp.getCode(),
										scp.getSampleCode(),
										scp.getProductId(),
										scp.getProductName(),
										"",
										DateUtil.format(sc.getCreateDate()),
										format.format(new Date()),
										"ProducerTask",
										"核型检测上机扫描",
										(User) ServletActionContext
												.getRequest()
												.getSession()
												.getAttribute(
														SystemConstants.USER_SESSION_KEY),
										sc.getId(), "核型分析", scp.getResult(),
										null, null, null, null, null, null,
										null, null);
						scp.setSubmit("1");
						producerTaskDao.saveOrUpdate(scp);
					} else {
						if(scp.getNextFlowId().equals("0038")){
							//提交不和合格的到报告终止
					    	EndReport cr = new EndReport();
							cr.setCode(scp.getCode());
							cr.setSampleCode(scp.getSampleCode());
							cr.setSampleType(scp.getSampleType());
							cr.setProductId(scp.getProductId());
							cr.setProductName(scp.getProductName());
							cr.setAdvice(scp.getNote());
							cr.setTaskType("上机扫描");
							cr.setTaskId(scp.getId());
							cr.setTaskResultId(scp.getProducerTask().getId());
							cr.setWaitDate(new Date());
					    	commonDAO.saveOrUpdate(cr);
						}else{
								// 不合格的到异常
								KaryoAbnormal ka = new KaryoAbnormal();
								ka.setCode(scp.getCode());
								ka.setSampleCode(scp.getSampleCode());
								ka.setProductId(scp.getProductId());
								ka.setProductName(scp.getProductName());
								ka.setOrderId(scp.getOrderId());
								ka.setAcceptDate(scp.getAcceptDate());
								ka.setReportDate(scp.getReportDate());
								ka.setSampleType(scp.getSampleType());
								ka.setTaskId(scp.getProducerTask().getId());
								ka.setNote(scp.getNote());
								ka.setTaskName("上机扫描");
								ka.setState("1");
								producerTaskDao.saveOrUpdate(ka);
						}
						scp.setSubmit("1");
					}
				}
			}
		}
	}
}