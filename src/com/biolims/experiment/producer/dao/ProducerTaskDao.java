package com.biolims.experiment.producer.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.producer.model.ProducerTask;
import com.biolims.experiment.producer.model.ProducerTaskCos;
import com.biolims.experiment.producer.model.ProducerTaskItem;
import com.biolims.experiment.producer.model.ProducerTaskReagent;
import com.biolims.experiment.producer.model.ProducerTaskResult;
import com.biolims.experiment.producer.model.ProducerTaskTemp;
import com.biolims.experiment.producer.model.ProducerTaskTemplate;
import com.biolims.experiment.snpjc.cross.model.SnpCrossItem;
import com.biolims.experiment.snpjc.cross.model.SnpCrossReagent;
import com.biolims.goods.mate.model.GoodsMaterialsApplyItem;

@Repository
@SuppressWarnings("unchecked")
public class ProducerTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectProducerTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from ProducerTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<ProducerTask> list = new ArrayList<ProducerTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id desc";

			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	/**
	 * 根据状态查询
	 */
	public Map<String, Object> selectProducerTaskListByState(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from ProducerTask where 1=1";
		if (mapForQuery.size() > 0) {
			key = map2where(mapForQuery);
		} else {
			key = " and state='1'";
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<ProducerTask> list = new ArrayList<ProducerTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id desc";

			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectProducerTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from ProducerTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and producerTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<ProducerTaskItem> list = new ArrayList<ProducerTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectProducerTaskTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from ProducerTaskTemplate where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and producerTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<ProducerTaskTemplate> list = new ArrayList<ProducerTaskTemplate>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectProducerTaskReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from ProducerTaskReagent where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and producerTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<ProducerTaskReagent> list = new ArrayList<ProducerTaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectProducerTaskCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from ProducerTaskCos where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and producerTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<ProducerTaskCos> list = new ArrayList<ProducerTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectProducerTaskResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from ProducerTaskResult where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and producerTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<ProducerTaskResult> list = new ArrayList<ProducerTaskResult>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectProducerTaskTempList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from ProducerTaskTemp where 1=1 and state='1'";
		String key = "";

		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql).uniqueResult();

		List<ProducerTaskTemp> list = new ArrayList<ProducerTaskTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据主表ID查询相关子表信息
	 * 
	 * @param id
	 * @return
	 */
	public List<ProducerTaskItem> setItemList(String id) {
		String hql = "from ProducerTaskItem where 1=1 and producerTask.id='"
				+ id + "'";
		List<ProducerTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据主表ID查询相关结果子表信息
	 * 
	 * @param id
	 * @return
	 */
	public List<ProducerTaskResult> getProducerTaskResultList(String id) {
		String hql = "from ProducerTaskResult where 1=1 and producerTask='"
				+ id + "'";
		List<ProducerTaskResult> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	public List<ProducerTaskTemp> setTempList(String code) {
		String hql = "from ProducerTaskTemp where 1=1 and code='" + code + "'";
		List<ProducerTaskTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 选择制片上机加载info子表信息
	 * 
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> setProducerTaskResultList(String code)
			throws Exception {
		String hql = "from ProducerTaskResult t where 1=1 ";
		String key = "";
		if (code != null)
			key = key + " and  t.producerTask='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<ProducerTaskResult> list = this.getSession()
				.createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据样本编号查询
	public List<ProducerTaskResult> setResultById(String code) {
		String hql = "from ProducerTaskResult t where (submit is null or submit='') and producerTask.id='"
				+ code + "'";
		List<ProducerTaskResult> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	// 根据样本编号查询
	public List<ProducerTaskResult> setResultByIds(String[] codes) {

		String insql = "''";
		for (String code : codes) {

			insql += ",'" + code + "'";

		}

		String hql = "from ProducerTaskResult t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<ProducerTaskResult> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}
	/**
	 * 根据主表编号Item信息
	 * @param code
	 * @return
	 */
	public List<ProducerTaskItem> getItem(String id) {
		String hql = "from ProducerTaskItem t where 1=1  and producerTask='"+ id + "'";
		List<ProducerTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据主表编号Reagent信息
	 * @param code
	 * @return
	 */
	public List<ProducerTaskReagent> getReagent(String id) {
		String hql = "from ProducerTaskReagent t where 1=1  and producerTask='"+ id + "'";
		List<ProducerTaskReagent> list = this.getSession().createQuery(hql).list();
		return list;
	}
}