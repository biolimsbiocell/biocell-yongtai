package com.biolims.experiment.producer.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.comsearch.dao.ComSearchDao;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.karyoship.model.KaryoShipTask;
import com.biolims.experiment.producer.model.ProducerTask;
import com.biolims.experiment.producer.model.ProducerTaskCos;
import com.biolims.experiment.producer.model.ProducerTaskItem;
import com.biolims.experiment.producer.model.ProducerTaskReagent;
import com.biolims.experiment.producer.model.ProducerTaskResult;
import com.biolims.experiment.producer.model.ProducerTaskTemp;
import com.biolims.experiment.producer.model.ProducerTaskTemplate;
import com.biolims.experiment.producer.service.ProducerTaskService;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.template.model.Template;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/producer/producerTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class ProducerTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "249004";
	@Autowired
	private ProducerTaskService producerTaskService;
	private ProducerTask producerTask = new ProducerTask();
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private ComSearchDao comSearchDao;

	@Action(value = "showProducerTaskList")
	public String showProducerTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/producer/producerTask.jsp");
	}

	@Action(value = "showProducerTaskListJson")
	public void showProducerTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = producerTaskService.findProducerTaskList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<ProducerTask> list = (List<ProducerTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("reciveUser-id", "");
		map.put("reciveUser-name", "");
		map.put("reciveDate", "yyyy-MM-dd");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("indexa", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("maxNum", "");
		map.put("qcNum", "");
		map.put("confirmDate", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "producerTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogProducerTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/producer/producerTaskDialog.jsp");
	}

	@Action(value = "showDialogProducerTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogProducerTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0) {
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		}
		// else{
		// map2Query.put("state", "1");
		// }
		Map<String, Object> result = producerTaskService
				.findProducerTaskListByState(map2Query, startNum, limitNum,
						dir, sort);
		Long count = (Long) result.get("total");
		List<ProducerTask> list = (List<ProducerTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("reciveUser-id", "");
		map.put("reciveUser-name", "");
		map.put("reciveDate", "yyyy-MM-dd");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("indexa", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("maxNum", "");
		map.put("qcNum", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editProducerTask")
	public String editProducerTask() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			producerTask = producerTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "ProducerTask");
		} else {
			producerTask.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			producerTask.setCreateUser(user);
			producerTask.setCreateDate(new Date());
			producerTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			producerTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
//			List<Template> tlist=comSearchDao.getTemplateByType("doProducer");
//			Template t=tlist.get(0);
//			producerTask.setTemplate(t);
//			//UserGroup u=comSearchDao.get(UserGroup.class,t.getAcceptUser().getId());
//			if(t.getAcceptUser()!=null){
//				producerTask.setAcceptUser(t.getAcceptUser());
//			}
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(producerTask.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/producer/producerTaskEdit.jsp");
	}

	@Action(value = "copyProducerTask")
	public String copyProducerTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		producerTask = producerTaskService.get(id);
		producerTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/producer/producerTaskEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = producerTask.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "ProducerTask";
			String markCode = "ZPSJ";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			producerTask.setId(autoID);
//			producerTask.setName("上机扫描-"+autoID);
		}
		Map aMap = new HashMap();
		aMap.put("producerTaskItem",
				getParameterFromRequest("producerTaskItemJson"));

		aMap.put("producerTaskTemplate",
				getParameterFromRequest("producerTaskTemplateJson"));

		aMap.put("producerTaskReagent",
				getParameterFromRequest("producerTaskReagentJson"));

		aMap.put("producerTaskCos",
				getParameterFromRequest("producerTaskCosJson"));

		aMap.put("producerTaskResult",
				getParameterFromRequest("producerTaskResultJson"));

		producerTaskService.save(producerTask, aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/experiment/producer/producerTask/editProducerTask.action?id="
				+ producerTask.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);
		// return
		// redirect("/experiment/producer/producerTask/editProducerTask.action?id="
		// + producerTask.getId());

	}

	@Action(value = "viewProducerTask")
	public String toViewProducerTask() throws Exception {
		String id = getParameterFromRequest("id");
		producerTask = producerTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/producer/producerTaskEdit.jsp");
	}

	@Action(value = "showProducerTaskItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showProducerTaskItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/producer/producerTaskItem.jsp");
	}

	@Action(value = "showProducerTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showProducerTaskItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = producerTaskService
					.findProducerTaskItemList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<ProducerTaskItem> list = (List<ProducerTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("orderNumber", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("indexa", "");
			map.put("concentration", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("stepNum", "");
			map.put("note", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("resultDate", "yyyy-MM-dd");
			map.put("inspectDate", "yyyy-MM-dd");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("producerTask-name", "");
			map.put("producerTask-id", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("volume", "");
			map.put("sampleNum", "");
			map.put("addVolume", "");
			map.put("sumVolume", "");
			map.put("counts", "");
			map.put("unit", "");
			map.put("projectId", "");
			map.put("contractId", "");
			map.put("orderType", "");
			map.put("jkTaskId", "");
			map.put("classify", "");
			map.put("dataType", "");
			map.put("dataNum", "");
			map.put("productNum", "");
			map.put("sampleType", "");
			map.put("tempId", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delProducerTaskItem")
	public void delProducerTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			producerTaskService.delProducerTaskItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showProducerTaskTemplateList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showProducerTaskTemplateList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/producer/producerTaskTemplate.jsp");
	}

	@Action(value = "showProducerTaskTemplateListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showProducerTaskTemplateListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = producerTaskService
					.findProducerTaskTemplateList(scId, startNum, limitNum,
							dir, sort);
			Long total = (Long) result.get("total");
			List<ProducerTaskTemplate> list = (List<ProducerTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("reciveUser-name", "");
			map.put("reciveUser-id", "");
			map.put("tItem", "");
			map.put("code", "");
			map.put("stepName", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("state", "");
			map.put("note", "");
			map.put("sampleCodes", "");
			map.put("producerTask-name", "");
			map.put("producerTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delProducerTaskTemplate")
	public void delProducerTaskTemplate() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			producerTaskService.delProducerTaskTemplate(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据id删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delProducerTaskTemplateOne")
	public void delProducerTaskTemplateOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			producerTaskService.delProducerTaskTemplateOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showProducerTaskReagentList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showProducerTaskReagentList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/producer/producerTaskReagent.jsp");
	}

	@Action(value = "showProducerTaskReagentListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showProducerTaskReagentListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = producerTaskService
					.findProducerTaskReagentList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<ProducerTaskReagent> list = (List<ProducerTaskReagent>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("batch", "");
			map.put("count", "");
			map.put("oneNum", "");
			map.put("sampleNum", "");
			map.put("num", "");
			map.put("isGood", "");
			map.put("note", "");
			map.put("producerTask-name", "");
			map.put("producerTask-id", "");
			map.put("itemId", "");
			map.put("tReagent", "");
			map.put("sn", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delProducerTaskReagent")
	public void delProducerTaskReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			producerTaskService.delProducerTaskReagent(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据id删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delProducerTaskReagentOne")
	public void delProducerTaskReagentOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			producerTaskService.delProducerTaskReagentOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showProducerTaskCosList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showProducerTaskCosList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/producer/producerTaskCos.jsp");
	}

	@Action(value = "showProducerTaskCosListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showProducerTaskCosListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = producerTaskService
					.findProducerTaskCosList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<ProducerTaskCos> list = (List<ProducerTaskCos>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("temperature", "");
			map.put("speed", "");
			map.put("time", "");
			map.put("note", "");
			map.put("isGood", "");
			map.put("producerTask-name", "");
			map.put("producerTask-id", "");
			map.put("itemId", "");
			map.put("tCos", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delProducerTaskCos")
	public void delProducerTaskCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			producerTaskService.delProducerTaskCos(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据id删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delProducerTaskCosOne")
	public void delProducerTaskCosOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			producerTaskService.delProducerTaskCosOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showProducerTaskResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showProducerTaskResultList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/producer/producerTaskResult.jsp");
	}

	@Action(value = "showProducerTaskResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showProducerTaskResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = producerTaskService
					.findProducerTaskResultList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<ProducerTaskResult> list = (List<ProducerTaskResult>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("jzshCode", "");
			map.put("slideCode", "");
			map.put("sampleCode", "");
			map.put("jkTaskId", "");
			map.put("indexa", "");
			map.put("volume", "");
			map.put("unit", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("submit", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("resultDate", "yyyy-MM-dd");
			map.put("inspectDate", "yyyy-MM-dd");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("method", "");
			map.put("isExecute", "");
			map.put("note", "");
			map.put("producerTask-name", "");
			map.put("producerTask-id", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("counts", "");
			map.put("contractId", "");
			map.put("projectId", "");
			map.put("orderType", "");
			map.put("classify", "");
			map.put("dataType", "");
			map.put("dataNum", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("sampleType", "");
			map.put("tempId", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delProducerTaskResult")
	public void delProducerTaskResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			producerTaskService.delProducerTaskResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showProducerTaskTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showProducerTaskTempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/producer/producerTaskTemp.jsp");
	}

	@Action(value = "showProducerTaskTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showProducerTaskTempListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = producerTaskService
				.findProducerTaskTempList(map2Query, startNum, limitNum, dir,
						sort);
		Long total = (Long) result.get("total");
		List<ProducerTaskTemp> list = (List<ProducerTaskTemp>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("patientName", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("inspectDate", "yyyy-MM-dd");
		map.put("acceptDate", "yyyy-MM-dd");
		map.put("volume", "");
		map.put("unit", "");
		map.put("idCard", "");
		map.put("sequenceFun", "");
		map.put("reportDate", "yyyy-MM-dd");
		map.put("phone", "");
		map.put("orderId", "");
		map.put("state", "");
		map.put("note", "");
		map.put("classify", "");
		map.put("sampleType", "");
		map.put("resultDate", "yyyy-MM-dd");
		//缴费状态
		map.put("chargeNote", "");
		new SendData().sendDateJson(map, list, total,
				ServletActionContext.getResponse());
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delProducerTaskTemp")
	public void delProducerTaskTemp() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			producerTaskService.delProducerTaskTemp(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public ProducerTaskService getProducerTaskService() {
		return producerTaskService;
	}

	public void setProducerTaskService(ProducerTaskService producerTaskService) {
		this.producerTaskService = producerTaskService;
	}

	public ProducerTask getProducerTask() {
		return producerTask;
	}

	public void setProducerTask(ProducerTask producerTask) {
		this.producerTask = producerTask;
	}

	/**
	 * 根据制片上机加载info子表明细
	 * 
	 * @throws Exception
	 */
	@Action(value = "setItemToKaryotypingTask", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void setItemToKaryotypingTask() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.producerTaskService
					.showProducerTaskResultList(code);
			result.put("success", true);
			result.put("data", dataListMap);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 提交样本
	@Action(value = "submitSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void submitSample() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.producerTaskService.submitSample(id, ids);

			result.put("success", true);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	 /**
		 * 保存上机扫描明细
		 * @throws Exception
		 */
		@Action(value = "saveProducerTaskItem",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
		public void saveProducerTaskItem() throws Exception {
			String id = getRequest().getParameter("id");
			String itemDataJson = getRequest().getParameter("itemDataJson");
			Map<String, Object> result = new HashMap<String, Object>();
			try {
				ProducerTask sc=commonDAO.get(ProducerTask.class, id);
				Map aMap = new HashMap();
				aMap.put("producerTaskItem",
						itemDataJson);
				if(sc!=null){
					producerTaskService.save(sc,aMap);
				}
				result.put("success", true);
			} catch (Exception e) {
				e.printStackTrace();
				result.put("success", false);
			}
			HttpUtils.write(JsonUtils.toJsonString(result));
		}
		
		 /**
		 * 保存上机扫描结果
		 * @throws Exception
		 */
		@Action(value = "saveProducerTaskResult",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
		public void saveProducerTaskResult() throws Exception {
			String id = getRequest().getParameter("id");
			String itemDataJson = getRequest().getParameter("itemDataJson");
			Map<String, Object> result = new HashMap<String, Object>();
			try {
				ProducerTask sc=commonDAO.get(ProducerTask.class, id);
				Map aMap = new HashMap();
				aMap.put("producerTaskResult",
						itemDataJson);
				if(sc!=null){
					producerTaskService.save(sc,aMap);
				}
				result.put("success", true);
			} catch (Exception e) {
				e.printStackTrace();
				result.put("success", false);
			}
			HttpUtils.write(JsonUtils.toJsonString(result));
		}

}
