package com.biolims.experiment.freeze.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.freeze.service.FreezeTaskService;

public class FreezeTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		FreezeTaskService mbService = (FreezeTaskService) ctx
				.getBean("freezeTaskService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
