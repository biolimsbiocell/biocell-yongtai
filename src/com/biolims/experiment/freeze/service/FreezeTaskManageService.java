package com.biolims.experiment.freeze.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.freeze.dao.FreezeTaskManageDao;
import com.biolims.experiment.freeze.model.FreezeTaskItem;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.util.JsonUtils;

@Service
@Transactional
public class FreezeTaskManageService {
	@Resource
	private FreezeTaskManageDao freezeTaskManageDao;
	@Resource
	private CommonDAO commonDAO;



	/**
	 * 
	 * @Title: freezeTaskManageItemRuku
	 * @Description: 入库
	 * @author : 
	 * @date 
	 * @param ids
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void freezeTaskManageItemRuku(String[] ids) {
		for (String id : ids) {
			FreezeTaskItem scp = freezeTaskManageDao.get(FreezeTaskItem.class, id);
			if (scp != null) {
				scp.setState("2");
				SampleInItemTemp st = new SampleInItemTemp();
				if (scp.getCode() == null) {
					st.setCode(scp.getSampleCode());
				} else {
					st.setCode(scp.getCode());
				}
				st.setSampleCode(scp.getSampleCode());
				if (scp.getSampleNum() != null
						&& scp.getSampleConsume() != null) {
					st.setNum(scp.getSampleNum() - scp.getSampleConsume());
				}
				st.setState("1");
				freezeTaskManageDao.saveOrUpdate(st);
			}

		}
	}

	/**
	 * 
	 * @Title: showFreezeTaskManageJson
	 * @Description: 展示样本管理明细
	 * @author : 
	 * @date 
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> showFreezeTaskManageJson(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		return freezeTaskManageDao.showFreezeTaskManageJson(start, length, query,
				col, sort);
	}
}

