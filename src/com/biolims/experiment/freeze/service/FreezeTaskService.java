package com.biolims.experiment.freeze.service;


import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.dic.model.DicType;
import com.biolims.experiment.cell.revive.model.CellReviveTemp;
import com.biolims.experiment.freeze.dao.FreezeTaskDao;
import com.biolims.experiment.freeze.model.FreezeTask;
import com.biolims.experiment.freeze.model.FreezeTaskAbnormal;
import com.biolims.experiment.freeze.model.FreezeTaskCos;
import com.biolims.experiment.freeze.model.FreezeTaskInfo;
import com.biolims.experiment.freeze.model.FreezeTaskItem;
import com.biolims.experiment.freeze.model.FreezeTaskReagent;
import com.biolims.experiment.freeze.model.FreezeTaskTemp;
import com.biolims.experiment.freeze.model.FreezeTaskTemplate;
import com.biolims.file.model.FileInfo;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.sample.dao.SampleReceiveDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.storage.main.service.StorageService;
import com.biolims.storage.out.service.StorageOutService;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.primers.model.MappingPrimersLibraryItem;
import com.biolims.system.primers.service.MappingPrimersLibraryService;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.CosItem;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.model.TemplateItem;
import com.biolims.system.template.service.TemplateService;
import com.biolims.util.JsonUtils;
import com.csvreader.CsvReader;
import com.opensymphony.xwork2.ActionContext;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
@Service
@Transactional
public class FreezeTaskService {
	
	@Resource
	private FreezeTaskDao freezeTaskDao;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private TemplateService templateService;
	@Resource
	private MappingPrimersLibraryService mappingPrimersLibraryService;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private StorageService storageService;
	@Resource
	private StorageOutService storageOutService;
	@Resource
	private SampleReceiveDao sampleReceiveDao;
	@Resource
	private DicSampleTypeDao dicSampleTypeDao;

	/**
	 * 
	 * @Title: get
	 * @Description:根据ID获取主表
	 * @author : 
	 * @date 
	 * @param id
	 * @return FreezeTask
	 * @throws
	 */
	public FreezeTask get(String id) {
		FreezeTask freezeTask = commonDAO.get(FreezeTask.class, id);
		return freezeTask;
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delFreezeTaskItem(String delStr, String[] ids, User user, String freezeTask_id) throws Exception {
		String delId="";
		for (String id : ids) {
			FreezeTaskItem scp = freezeTaskDao.get(FreezeTaskItem.class, id);
			if (scp.getId() != null) {
				FreezeTask pt = freezeTaskDao.get(FreezeTask.class, scp
						.getFreezeTask().getId());
				pt.setSampleNum(pt.getSampleNum() - 1);
				// 改变左侧样本状态
				FreezeTaskTemp freezeTaskTemp = this.commonDAO.get(
						FreezeTaskTemp.class, scp.getTempId());
				if (freezeTaskTemp != null) {
				pt.setSampleNum(pt.getSampleNum() - 1);
					freezeTaskTemp.setState("1");
					freezeTaskDao.update(freezeTaskTemp);
				}
				freezeTaskDao.update(pt);
				freezeTaskDao.delete(scp);
			}
		delId+=scp.getSampleCode()+",";
		}
		if (ids.length>0) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(freezeTask_id);
			li.setModifyContent(delStr+delId);
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 
	 * @Title: delFreezeTaskItemAf
	 * @Description: 重新排板
	 * @author : 
	 * @date 
	 * @param ids
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delFreezeTaskItemAf(String[] ids) throws Exception {
		for (String id : ids) {
			FreezeTaskItem scp = freezeTaskDao.get(FreezeTaskItem.class, id);
			if (scp.getId() != null) {
				scp.setState("1");
				freezeTaskDao.saveOrUpdate(scp);
			}

		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delFreezeTaskResult(String[] ids, String delStr, String mainId, User user) throws Exception {
		String idStr="";
		for (String id : ids) {
			FreezeTaskInfo scp = freezeTaskDao
					.get(FreezeTaskInfo.class, id);
			if ((scp.getSubmit() == null || (scp.getSubmit() != null && scp
					.getSubmit().equals(""))))
				freezeTaskDao.delete(scp);
			idStr+=scp.getSampleCode()+",";
		}
		LogInfo li = new LogInfo();
		li.setLogDate(new Date());
		User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
		li.setFileId(mainId);
		li.setModifyContent(delStr+idStr);
		commonDAO.saveOrUpdate(li);
	}

	/**
	 * 删除试剂明细
	 * 
	 * @param id2
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delFreezeTaskReagent(String id, String delStr, String mainId, User user) throws Exception {
		if (id != null && !"".equals(id)) {
			FreezeTaskReagent scp = freezeTaskDao.get(FreezeTaskReagent.class,
					id);
			freezeTaskDao.delete(scp);
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(mainId);
			li.setModifyContent(delStr+scp.getName());
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 删除设备明细
	 * 
	 * @param id
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delFreezeTaskCos(String id, String delStr, String mainId, User user) throws Exception {
		if (id != null && !"".equals(id)) {
			FreezeTaskCos scp = freezeTaskDao.get(FreezeTaskCos.class, id);
			freezeTaskDao.delete(scp);
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(mainId);
			li.setModifyContent(delStr+scp.getName());
		}
	}

	/**
	 * 
	 * @Title: saveFreezeTaskTemplate
	 * @Description: 保存模板
	 * @author : 
	 * @date 
	 * @param sc
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveFreezeTaskTemplate(FreezeTask sc) {
		List<FreezeTaskTemplate> tlist2 = freezeTaskDao.delTemplateItem(sc
				.getId());
		List<FreezeTaskReagent> rlist2 = freezeTaskDao.delReagentItem(sc
				.getId());
		List<FreezeTaskCos> clist2 = freezeTaskDao.delCosItem(sc.getId());
		commonDAO.deleteAll(tlist2);
		commonDAO.deleteAll(rlist2);
		commonDAO.deleteAll(clist2);
		List<TemplateItem> tlist = templateService.getTemplateItem(sc
				.getTemplate().getId(), null);
		List<ReagentItem> rlist = templateService.getReagentItem(sc
				.getTemplate().getId(), null);
		List<CosItem> clist = templateService.getCosItem(sc.getTemplate()
				.getId(), null);
		for (TemplateItem t : tlist) {
			FreezeTaskTemplate ptt = new FreezeTaskTemplate();
			ptt.setFreezeTask(sc);
			ptt.setOrderNum(t.getOrderNum());
			ptt.setName(t.getName());
			ptt.setNote(t.getNote());
			ptt.setContent(t.getContent());
			freezeTaskDao.saveOrUpdate(ptt);
		}
		for (ReagentItem t : rlist) {
			FreezeTaskReagent ptr = new FreezeTaskReagent();
			ptr.setFreezeTask(sc);
			ptr.setCode(t.getCode());
			ptr.setName(t.getName());
			ptr.setItemId(t.getItemId());
			ptr.setName(t.getName());
			ptr.setNote(t.getNote());
			freezeTaskDao.saveOrUpdate(ptr);
		}
		for (CosItem t : clist) {
			FreezeTaskCos ptc = new FreezeTaskCos();
			ptc.setFreezeTask(sc);
			ptc.setItemId(t.getItemId());
			ptc.setName(t.getName());
			ptc.setNote(t.getNote());
			ptc.setType(t.getType());
			freezeTaskDao.saveOrUpdate(ptc);
		}
	}

	/**
	 * 
	 * @Title: changeState
	 * @Description:改变状态
	 * @author : 
	 * @date 
	 * @param applicationTypeActionId
	 * @param id
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String id)
			throws Exception {
		FreezeTask sct = freezeTaskDao.get(FreezeTask.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		freezeTaskDao.update(sct);
		if (sct.getTemplate() != null) {
			String iid = sct.getTemplate().getId();
			if (iid != null) {
				templateService.setCosIsUsedOver(iid);
			}
		}

		submitSample(id, null);

	}

	/**
	 * 
	 * @Title: submitSample
	 * @Description: 提交样本
	 * @author :
	 * @date 
	 * @param id
	 * @param ids
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitSample(String id, String[] ids) throws Exception {
		// 获取主表信息
		FreezeTask sc = this.freezeTaskDao.get(FreezeTask.class, id);
		// 获取结果表样本信息
		List<FreezeTaskInfo> list;
		if (ids == null)
			list = this.freezeTaskDao.selectAllResultListById(id);
		else
			list = this.freezeTaskDao.selectAllResultListByIds(ids);

		for (FreezeTaskInfo scp : list) {
			if (scp != null
					&& scp.getResult() != null
					&& (scp.getSubmit() == null || (scp.getSubmit() != null && scp
							.getSubmit().equals("")))) {
				if (scp.getResult().equals("1")) {// 合格
					String next = scp.getNextFlowId();

					if (next.equals("0009")) {// 样本入库
						// 样本入库
						SampleInItemTemp st = new SampleInItemTemp();
						st.setSampleState("1");
						st.setParentId(scp.getParentId());
						st.setPronoun(scp.getPronoun());
						st.setScopeId(sc.getScopeId());
						st.setScopeName(sc.getScopeName());
						st.setProductId(scp.getProductId());
						st.setProductName(scp.getProductName());
						st.setCode(scp.getCode());
						st.setSampleCode(scp.getSampleCode());
						st.setConcentration(scp.getConcentration());
						st.setVolume(scp.getVolume());
						DicSampleType t = commonDAO.get(DicSampleType.class, scp
								.getDicSampleType().getId());
						st.setSampleType(t.getName());
						st.setDicSampleType(scp.getDicSampleType());
						st.setState("1");
						st.setPatientName(scp.getPatientName());
						st.setSampleTypeId(scp.getDicSampleType().getId());
						st.setInfoFrom("FreezeTaskInfo");
						if(scp.getSampleOrder()!=null) {
							st.setSampleOrder(commonDAO.get(SampleOrder.class,
									scp.getSampleOrder().getId()));
						}
						freezeTaskDao.saveOrUpdate(st);
					
					} else if (next.equals("0012")) {// 暂停
					} else if (next.equals("0013")) {// 终止
					} else if(next.equals("0072")) {//细胞复苏
						CellReviveTemp cpt=new CellReviveTemp();
						scp.setState("1");
						cpt.setState("1");
						cpt.setParentId(scp.getParentId());
						cpt.setSampleCode(scp.getSampleCode());
						cpt.setCode(scp.getCode());
						cpt.setProductId(scp.getProductId());
						cpt.setProductName(scp.getProductName());
						cpt.setConcentration(scp.getConcentration());
						cpt.setVolume(scp.getVolume());
						cpt.setOrderId(sc.getId());
						cpt.setSampleType(scp.getSampleType());
						cpt.setSampleInfo(scp.getSampleInfo());
						cpt.setScopeId(scp.getScopeId());
						cpt.setScopeName(scp.getScopeName());
						cpt.setPronoun(scp.getPronoun());
						if(scp.getSampleOrder()!=null) {
							cpt.setSampleOrder(commonDAO.get(SampleOrder.class,
									scp.getSampleOrder().getId()));
						}
						freezeTaskDao.saveOrUpdate(cpt);
					}else {
					    scp.setState("1");
						scp.setScopeId(sc.getScopeId());
						scp.setScopeName(sc.getScopeName());
						// 得到下一步流向的相关表单
						List<NextFlow> list_nextFlow = nextFlowDao
								.seletNextFlowById(next);
						for (NextFlow n : list_nextFlow) {
							Object o = Class.forName(
									n.getApplicationTypeTable().getClassPath())
									.newInstance();
							sampleInputService.copy(o, scp);
						}
					}
				} else {// 不合格
					FreezeTaskAbnormal pa = new FreezeTaskAbnormal();// 样本异常
					pa.setOrderId(sc.getId());
					pa.setState("1");
					sampleInputService.copy(pa, scp);
				}
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				sampleStateService
						.saveSampleState(
								scp.getCode(),
								scp.getSampleCode(),
								scp.getProductId(),
								scp.getProductName(),
								"",
								sc.getCreateDate(),
								format.format(new Date()),
								"FreezeTask",
								"冻存实验",
								(User) ServletActionContext
										.getRequest()
										.getSession()
										.getAttribute(
												SystemConstants.USER_SESSION_KEY),
								id, scp.getNextFlow(), scp.getResult(), null,
								null, null, null, null, null, null, null);
				
				scp.setSubmit("1");
				freezeTaskDao.saveOrUpdate(scp);
			}
		}
	}

	/**
	 * 
	 * @Title: findFreezeTaskTable
	 * @Description: 展示主表
	 * @author :
	 * @date 
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> findFreezeTaskTable(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		return freezeTaskDao.findFreezeTaskTable(start, length, query, col,
				sort);
	}

	/**
	 * 
	 * @Title: selectFreezeTaskTempTable
	 * @Description: 展示临时表
	 * @author : 
	 * @date 
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> selectFreezeTaskTempTable(String[] codes, Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		return freezeTaskDao.selectFreezeTaskTempTable(codes,start, length, query,
				col, sort);
	}
	/**
	 * 
	 * @Title: saveAllot
	 * @Description: 保存任务分配
	 * @author : 
	 * @date 
	 * @param main
	 * @param tempId
	 * @param userId
	 * @param templateId
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String saveAllot(String main, String[] tempId, String userId,
			String templateId, String logInfo) throws Exception {
		String id = "";
		if (main != null) {
			String mainJson = "[" + main + "]";
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					mainJson, List.class);
			FreezeTask pt = new FreezeTask();
			pt = (FreezeTask) commonDAO.Map2Bean(list.get(0), pt);
			String name=pt.getName();
			Integer sampleNum=pt.getSampleNum();
			Integer maxNum=pt.getMaxNum();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if ((pt.getId() != null && pt.getId().equals(""))
					|| pt.getId().equals("NEW")) {
				String modelName = "FreezeTask";
				String markCode = "FREE";
				id = codingRuleService.genTransID(modelName, markCode);
				pt.setId(id);
				pt.setCreateDate(sdf.format(new Date()));
				pt.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				pt.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
				
			} else {
				id = pt.getId();
				pt=commonDAO.get(FreezeTask.class, id);
				pt.setName(name);
				pt.setSampleNum(sampleNum);
				pt.setMaxNum(maxNum);
			}
			Template t = commonDAO.get(Template.class, templateId);
			pt.setTemplate(t);
			pt.setTestUserOneId(userId);
			String [] users=userId.split(",");
			String userName="";
			for(int i=0;i<users.length;i++){
				if(i==users.length-1){
					User user=commonDAO.get(User.class, users[i]);
					userName+=user.getName();
				}else{
					User user=commonDAO.get(User.class, users[i]);
					userName+=user.getName()+",";
				}
			}
			pt.setTestUserOneName(userName);
			freezeTaskDao.saveOrUpdate(pt);			
            if (pt.getTemplate() != null) {
				saveFreezeTaskTemplate(pt);
			}
			if (tempId != null) {
				if (pt.getTemplate() != null) {
						for (String temp : tempId) {
							FreezeTaskTemp ptt = freezeTaskDao.get(
									FreezeTaskTemp.class, temp);
							if (t.getIsSeparate().equals("1")) {
								List<MappingPrimersLibraryItem> listMPI = mappingPrimersLibraryService
										.getMappingPrimersLibraryItem(ptt
												.getProductId());
								for (MappingPrimersLibraryItem mpi : listMPI) {
									FreezeTaskItem pti = new FreezeTaskItem();
									pti.setParentId(ptt.getParentId());
									pti.setPronoun(ptt.getPronoun());
									pti.setSampleCode(ptt.getSampleCode());
									pti.setProductId(ptt.getProductId());
									pti.setProductName(ptt.getProductName());
									pti.setDicSampleTypeId(t.getDicSampleType() != null
											&& !"".equals(t.getDicSampleType()) ? t
											.getDicSampleType().getId() : t
											.getDicSampleTypeId());
									pti.setDicSampleTypeName(t
											.getDicSampleType() != null
											&& !"".equals(t.getDicSampleType()) ? t
											.getDicSampleType().getName() : t
											.getDicSampleTypeName());
									pti.setProductName(ptt.getProductName());
									pti.setProductNum(t.getProductNum() != null
											&& !"".equals(t.getProductNum()) ? t
											.getProductNum() : t
											.getProductNum1());
									pti.setCode(ptt.getCode());
									if (t.getStorageContainer() != null) {
										pti.setState("1");
									}else{
										pti.setState("2");
									}
									ptt.setState("2");
									pti.setTempId(temp);
									pti.setFreezeTask(pt);
									pti.setChromosomalLocation(mpi
											.getChromosomalLocation());
									pti.setSampleInfo(ptt.getSampleInfo());
									if(ptt.getSampleOrder()!=null) {
										pti.setSampleOrder(commonDAO.get(SampleOrder.class,
												ptt.getSampleOrder().getId()));
									}
									freezeTaskDao.saveOrUpdate(pti);
								}
							} else {
								FreezeTaskItem pti = new FreezeTaskItem();
								pti.setParentId(ptt.getParentId());
								pti.setPronoun(ptt.getPronoun());
								pti.setSampleCode(ptt.getSampleCode());
								pti.setProductId(ptt.getProductId());
								pti.setProductName(ptt.getProductName());
								pti.setDicSampleTypeId(t.getDicSampleType() != null
										&& !"".equals(t.getDicSampleType()) ? t
										.getDicSampleType().getId() : t
										.getDicSampleTypeId());
								pti.setProductNum(t.getProductNum() != null
										&& !"".equals(t.getProductNum()) ? t
										.getProductNum() : t.getProductNum1());
								pti.setDicSampleTypeName(t.getDicSampleType() != null
										&& !"".equals(t.getDicSampleType()) ? t
										.getDicSampleType().getName() : t
										.getDicSampleTypeName());
								pti.setCode(ptt.getCode());
								pti.setSampleInfo(ptt.getSampleInfo());
								if (t.getStorageContainer() != null) {
										pti.setState("1");
									}else{
										pti.setState("2");
									}
								ptt.setState("2");
								pti.setTempId(temp);
								pti.setFreezeTask(pt);
								if(ptt.getSampleOrder()!=null) {
									pti.setSampleOrder(commonDAO.get(SampleOrder.class,
											ptt.getSampleOrder().getId()));
								}
								freezeTaskDao.saveOrUpdate(pti);
							}
							freezeTaskDao.saveOrUpdate(ptt);
						}
				}

			
			}
			
			if(logInfo!=null&&!"".equals(logInfo)){
				LogInfo li=new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(pt.getId());
				li.setModifyContent(logInfo);
				commonDAO.saveOrUpdate(li);
			}
		}
		return id;

	}

	/**
	 * 
	 * @Title: findFreezeTaskItemTable
	 * @Description:展示未排版样本
	 * @author : 
	 * @date 
	 * @param id
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> findFreezeTaskItemTable(String id,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return freezeTaskDao.findFreezeTaskItemTable(id, start, length, query,
				col, sort);
	}

	/**
	 * 
	 * @Title: saveMakeUp
	 * @Description: 保存排板数据
	 * @author : 
	 * @date 
	 * @param id
	 * @param item
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveMakeUp(String id, String item, String logInfo) throws Exception {
		List<FreezeTaskItem> saveItems = new ArrayList<FreezeTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(item,
				List.class);
		FreezeTask pt = commonDAO.get(FreezeTask.class, id);
		FreezeTaskItem scp = new FreezeTaskItem();
		for (Map<String, Object> map : list) {
			// 将map信息读入实体类
			scp = (FreezeTaskItem) freezeTaskDao.Map2Bean(map, scp);
			FreezeTaskItem pti = commonDAO.get(FreezeTaskItem.class,
					scp.getId());
			pti.setDicSampleTypeId(scp.getDicSampleTypeId());
			pti.setDicSampleTypeName(scp.getDicSampleTypeName());
			pti.setProductNum(scp.getProductNum());
			pti.setBlendCode(scp.getBlendCode());
			pti.setColor(scp.getColor());    
			scp.setFreezeTask(pt);
			saveItems.add(pti);
		}
		if(logInfo!=null&&!"".equals(logInfo)){
			LogInfo li=new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		freezeTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 
	 * @Title: showWellPlate
	 * @Description: 展示孔板
	 * @author : 
	 * @date 
	 * @param id
	 * @return
	 * @throws Exception
	 *             List<FreezeTaskItem>
	 * @throws
	 */
	public List<FreezeTaskItem> showWellPlate(String id) throws Exception {
		List<FreezeTaskItem> list = freezeTaskDao.showWellPlate(id);
		return list;
	}

	/**
	 * 
	 * @Title: findFreezeTaskItemAfTable
	 * @Description: 展示排板后
	 * @author :
	 * @date 
	 * @param scId
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> findFreezeTaskItemAfTable(String scId,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return freezeTaskDao.findFreezeTaskItemAfTable(scId, start, length,
				query, col, sort);
	}

	/**
	 * 
	 * @Title: plateLayout
	 * @Description: 排板
	 * @author : 
	 * @date 
	 * @param data
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void plateLayout(String[] data) {
		for (String da : data) {
			String[] d = da.split(",");
			FreezeTaskItem pti = commonDAO.get(FreezeTaskItem.class, d[0]);
			pti.setPosId(d[1]);
			pti.setCounts(d[2]);
			pti.setState("2");
			freezeTaskDao.saveOrUpdate(pti);
		}
	}

	/**
	 * 
	 * @Title: showFreezeTaskStepsJson
	 * @Description: 展示实验步骤
	 * @author :
	 * @date 
	 * @param id
	 * @param code
	 * @return Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> showFreezeTaskStepsJson(String id, String code) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<FreezeTaskTemplate> pttList = freezeTaskDao
				.showFreezeTaskStepsJson(id, code);
		List<FreezeTaskReagent> ptrList = freezeTaskDao
				.showFreezeTaskReagentJson(id, code);
		List<FreezeTaskCos> ptcList = freezeTaskDao.showFreezeTaskCosJson(id,
				code);
		List<Object> plate = freezeTaskDao.showWellList(id);
		map.put("template", pttList);
		map.put("reagent", ptrList);
		map.put("cos", ptcList);
		map.put("plate", plate);
		return map;
	}

	/**
	 * 
	 * @Title: showFreezeTaskResultTableJson
	 * @Description: 展示结果
	 * @author : 
	 * @date
	 * @param id
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> showFreezeTaskResultTableJson(String id,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return freezeTaskDao.showFreezeTaskResultTableJson(id, start, length,
				query, col, sort);
	}

	/**
	 * 
	 * @Title: plateSample
	 * @Description: 孔板样本
	 * @author : 
	 * @date
	 * @param id
	 * @param counts
	 * @return Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> plateSample(String id, String counts) {
		FreezeTask pt = commonDAO.get(FreezeTask.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		List<FreezeTaskItem> list = freezeTaskDao.plateSample(id, counts);
		Integer row = null;
		Integer col = null;
		if (pt.getTemplate().getStorageContainer() != null) {
		row = pt.getTemplate().getStorageContainer().getRowNum();
		col = pt.getTemplate().getStorageContainer().getColNum();
		}
		map.put("list", list);
		map.put("rowNum", row);
		map.put("colNum", col);
		return map;
	}

	/**
	 * 
	 * @Title: getCsvContent
	 * @Description: 上传结果
	 * @author : 
	 * @date 
	 * @param id
	 * @param fileId
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void getCsvContent(String id, String fileId) throws Exception {

		FileInfo fileInfo = freezeTaskDao.get(FileInfo.class, fileId);
		String filepath = fileInfo.getFilePath();
		File a = new File(filepath);
		if (a.isFile()) {
			FreezeTask pt = freezeTaskDao.get(FreezeTask.class, id);
			InputStream is = new FileInputStream(filepath);
			if (is != null) {
				CsvReader reader = new CsvReader(is, Charset.forName("GBK"));
				reader.readHeaders();// 去除表头
				while (reader.readRecord()) {
					String code = reader.get(0);
					if (code != null && !"".equals(code)) {
						List<FreezeTaskInfo> listPTI = freezeTaskDao
								.findFreezeTaskInfoByCode(code);
						if (listPTI.size() > 0) {
							FreezeTaskInfo spi = listPTI.get(0);
							spi.setConcentration(Double.parseDouble(reader
									.get(4)));
							spi.setVolume(Double.parseDouble(reader.get(5)));
							freezeTaskDao.saveOrUpdate(spi);
						}
					
					}
				}
			}
		}
	}

	/**
	 * 
	 * @Title: saveSteps
	 * @Description: 保存实验步骤
	 * @author : 
	 * @date 
	 * @param id
	 * @param templateJson
	 * @param reagentJson
	 * @param cosJson
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSteps(String id, String templateJson, String reagentJson,
			String cosJson, String logInfo) {
		if(logInfo!=null&&!"".equals(logInfo)){
			LogInfo li=new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}	
		if (templateJson != null && !templateJson.equals("{}")
				&& !templateJson.equals("")) {
			saveTemplate(id, templateJson);
		}
		if (reagentJson != null && !reagentJson.equals("{}")
				&& !reagentJson.equals("")) {
			saveReagent(id, reagentJson);
		}
		if (cosJson != null && !cosJson.equals("{}") && !cosJson.equals("")) {
			saveCos(id, cosJson);
		}
	}

	/**
	 * 
	 * @Title: saveTemplate
	 * @Description: 保存模板明细
	 * @author :
	 * @date 
	 * @param id
	 * @param templateJson
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveTemplate(String id, String templateJson) {
		JSONArray array = JSONArray.fromObject("[" + templateJson + "]");
		JSONObject object = JSONObject.fromObject(array.get(0));
		String templateId = (String) object.get("id");
		if (templateId != null && !"".equals(templateId)) {
			FreezeTaskTemplate ptt = freezeTaskDao.get(
					FreezeTaskTemplate.class, templateId);
			String contentData = object.get("contentData").toString();
			if (contentData != null && !"".equals(contentData)) {
				ptt.setContentData(contentData);
			}
			String startTime = (String) object.get("startTime");
			if (startTime != null && !"".equals(startTime)) {
				ptt.setStartTime(startTime);
			}
			String endTime = (String) object.get("endTime");
			if (endTime != null && !"".equals(endTime)) {
				ptt.setEndTime(endTime);
			}
			freezeTaskDao.saveOrUpdate(ptt);
		}
	}

	/**
	 * 
	 * @Title: saveReagent
	 * @Description: 保存试剂
	 * @author : 
	 * @date 
	 * @param id
	 * @param reagentJson
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveReagent(String id, String reagentJson) {
	
		JSONArray array = JSONArray.fromObject(reagentJson);
		for (int i = 0; i < array.size(); i++) {
			JSONObject object = JSONObject.fromObject(array.get(i));
			String reagentId = (String) object.get("id");
			FreezeTaskReagent ptr = null;
			if (reagentId != null && !"".equals(reagentId)) {
				ptr = commonDAO.get(FreezeTaskReagent.class, reagentId);
				String batch = (String) object.get("batch");
				if (batch != null && !"".equals(batch)) {
					ptr.setBatch(batch);
				}
				String sn = (String) object.get("sn");
				if (sn != null && !"".equals(sn)) {
					ptr.setSn(sn);
				}

			} else {
				ptr = new FreezeTaskReagent();
				String code = (String) object.get("code");
				if (code != null && !"".equals(code)) {
					ptr.setCode(code);
					String name = (String) object.get("name");
					if (name != null && !"".equals(name)) {
						ptr.setName(name);
					}
					String itemid = (String) object.get("itemId");
					if (itemid != null && !"".equals(itemid)) {
						ptr.setItemId(itemid);
					}
					String batch = (String) object.get("batch");
					if (batch != null && !"".equals(batch)) {
						ptr.setBatch(batch);
					}
					String sn = (String) object.get("sn");
					if (sn != null && !"".equals(sn)) {
						ptr.setSn(sn);
					}
					ptr.setFreezeTask(commonDAO.get(FreezeTask.class, id));
				}

			}
			freezeTaskDao.saveOrUpdate(ptr);
		}
	
	}

	/**
	 * 
	 * @Title: saveCos
	 * @Description: 保存设备
	 * @author :
	 * @date
	 * @param id
	 * @param cosJson
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveCos(String id, String cosJson) {
	
		JSONArray array = JSONArray.fromObject(cosJson);
		for (int j = 0; j < array.size(); j++) {
			JSONObject object = JSONObject.fromObject(array.get(j));
			String cosId = (String) object.get("id");
			FreezeTaskCos ptc=null;
			if (cosId != null && !"".equals(cosId)) {
				ptc = commonDAO.get(FreezeTaskCos.class, cosId);
				String code = (String) object.get("code");
				if (code != null && !"".equals(code)) {
					ptc.setCode(code);
				}
				String name = (String) object.get("name");
				if (name != null && !"".equals(name)) {
					ptc.setName(name);
				}
			} else {
				ptc = new FreezeTaskCos();
				String typeId = (String) object.get("typeId");
				if (typeId != null && !"".equals(typeId)) {
					DicType dt = commonDAO.get(DicType.class, typeId);
					ptc.setType(dt);
					String code = (String) object.get("code");
					if (code != null && !"".equals(code)) {
						ptc.setCode(code);
					}
					String name = (String) object.get("name");
					if (name != null && !"".equals(name)) {
						ptc.setName(name);
					}
					String itemid = (String) object.get("itemId");
					if (itemid != null && !"".equals(itemid)) {
						ptc.setItemId(itemid);
					}
					ptc.setFreezeTask(commonDAO.get(FreezeTask.class, id));
				}
			}
			freezeTaskDao.saveOrUpdate(ptc);
		}
	
	
	}

	/**
	 * 
	 * @Title: plateSampleTable
	 * @Description: 孔板样本列表
	 * @author : 
	 * @date 
	 * @param id
	 * @param counts
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> plateSampleTable(String id, String counts,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return freezeTaskDao.plateSampleTable(id, counts, start, length, query,
				col, sort);
	}

	/**
	 * 
	 * @Title: bringResult
	 * @Description: 生成结果
	 * @author :
	 * @date
	 * @param id
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void bringResult(String id) throws Exception {
	
	
		List<FreezeTaskItem> list = freezeTaskDao.showWellPlate(id);
		Map<String, Object> map = new HashMap<String, Object>();
		List<FreezeTaskInfo> spiList = freezeTaskDao.selectResultListById(id);
		for (FreezeTaskItem pti : list) {
			boolean b = true;
			for (FreezeTaskInfo spi : spiList) {
				if (spi.getSampleCode().indexOf(pti.getSampleCode()) > -1) {
					b = false;
				}
			}
			if (b) {
				if (pti.getBlendCode() == null || "".equals(pti.getBlendCode())) {
					if (pti.getProductNum() != null
							&& !"".equals(pti.getProductNum())) {
						Integer pn = Integer.parseInt(pti.getProductNum());
						for (int i = 0; i < pn; i++) {
							FreezeTaskInfo scp = new FreezeTaskInfo();
							scp.setParentId(pti.getParentId());
							DicSampleType ds = commonDAO.get(
									DicSampleType.class,
									pti.getDicSampleTypeId());
							scp.setDicSampleType(ds);
							scp.setSampleCode(pti.getSampleCode());
							scp.setProductId(pti.getProductId());
							scp.setProductName(pti.getProductName());
							scp.setSampleInfo(pti.getSampleInfo());
							scp.setFreezeTask(pti.getFreezeTask());
							scp.setResult("1");
							String markCode = "";
							if (scp.getDicSampleType() != null) {
								DicSampleType d = commonDAO.get(
										DicSampleType.class, scp
												.getDicSampleType().getId());
								String[] sampleCode = scp.getSampleCode()
										.split(",");
								if (sampleCode.length > 1) {
									String str = sampleCode[0].substring(0,
											sampleCode[0].length() - 4);
									for (int j = 0; j < sampleCode.length; j++) {
										str += sampleCode[j].substring(
												sampleCode[j].length() - 4,
												sampleCode[j].length());
									}
									if (d == null) {
										markCode = "F"+str;
									} else {
										markCode = "F"+str + d.getCode();
									}
								} else {
									if (d == null) {
										markCode = "F"+scp.getSampleCode();
									} else {
										markCode = "F"+scp.getSampleCode()
												+ d.getCode();
									}
								}

							}
							String code = codingRuleService.getCode(
									"FreezeTaskInfo", markCode, 00, 2, null);
							scp.setCode(code);
							if(pti.getSampleOrder()!=null) {
								scp.setSampleOrder(commonDAO.get(SampleOrder.class,
										pti.getSampleOrder().getId()));
							}
							freezeTaskDao.saveOrUpdate(scp);
						}
					}
				} else {
					if (!map.containsKey(String.valueOf(pti.getBlendCode()))) {
						if (pti.getProductNum() != null
								&& !"".equals(pti.getProductNum())) {
							Integer pn = Integer.parseInt(pti.getProductNum());
							for (int i = 0; i < pn; i++) {
								FreezeTaskInfo scp = new FreezeTaskInfo();
								DicSampleType ds = commonDAO.get(
										DicSampleType.class,
										pti.getDicSampleTypeId());
								scp.setDicSampleType(ds);
								scp.setSampleCode(pti.getSampleCode());
								scp.setProductId(pti.getProductId());
								scp.setProductName(pti.getProductName());
								scp.setSampleInfo(pti.getSampleInfo());
								scp.setFreezeTask(pti.getFreezeTask());
								scp.setResult("1");
								String markCode = "";
								if (scp.getDicSampleType() != null) {
									DicSampleType d = commonDAO
											.get(DicSampleType.class, scp
													.getDicSampleType().getId());
									String[] sampleCode = scp.getSampleCode()
											.split(",");
									if (sampleCode.length > 1) {
										String str = sampleCode[0].substring(0,
												sampleCode[0].length() - 4);
										for (int j = 0; j < sampleCode.length; j++) {
											str += sampleCode[j].substring(
													sampleCode[j].length() - 4,
													sampleCode[j].length());
										}
										if (d == null) {
											markCode = str;
										} else {
											markCode = str + d.getCode();
										}
									} else {
										if (d == null) {
											markCode = scp.getSampleCode();
										} else {
											markCode = scp.getSampleCode()
													+ d.getCode();
										}
									}

								}
								String code = codingRuleService.getCode(
										"FreezeTaskInfo", markCode, 00, 2,
										null);
								scp.setCode(code);
								map.put(String.valueOf(pti.getBlendCode()),
										code);
								if(pti.getSampleOrder()!=null) {
									scp.setSampleOrder(commonDAO.get(SampleOrder.class,
											pti.getSampleOrder().getId()));
								}
								freezeTaskDao.saveOrUpdate(scp);
							}
						}
					} else {
						String code = (String) map.get(String.valueOf(pti
								.getBlendCode()));
						FreezeTaskInfo spi = freezeTaskDao
								.getResultByCode(code);
						spi.setSampleCode(spi.getSampleCode() + ","
								+ pti.getSampleCode());
						freezeTaskDao.saveOrUpdate(spi);
					}
				}
			}
		}
	
	
	}

	/**
	 * 
	 * @Title: saveResult
	 * @Description: 保存结果
	 * @author : 
	 * @date 
	 * @param id
	 * @param dataJson
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveResult(String id, String dataJson, String logInfo,String confirmUser) throws Exception {

		List<FreezeTaskInfo> saveItems = new ArrayList<FreezeTaskInfo>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(dataJson,
				List.class);
		FreezeTask pt = commonDAO.get(FreezeTask.class, id);		
		for (Map<String, Object> map : list) {
		FreezeTaskInfo scp = new FreezeTaskInfo();
			// 将map信息读入实体类
			scp = (FreezeTaskInfo) freezeTaskDao.Map2Bean(map, scp);
			scp.setFreezeTask(pt);
			saveItems.add(scp);
		}
		if(confirmUser!=null&&!"".equals(confirmUser)){
			User confirm=commonDAO.get(User.class, confirmUser);
			pt.setConfirmUser(confirm);
			freezeTaskDao.saveOrUpdate(pt);
		}
		if(logInfo!=null&&!"".equals(logInfo)){
			LogInfo li=new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		freezeTaskDao.saveOrUpdateAll(saveItems);

	}

	public List<FreezeTaskItem> findFreezeTaskItemList(String scId)
			throws Exception {
		List<FreezeTaskItem> list = freezeTaskDao.selectFreezeTaskItemList(scId);
		return list;
	}
	public Integer generateBlendCode(String id) {
		return freezeTaskDao.generateBlendCode(id);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveLeftQuality(FreezeTask pt,String []ids,String logInfo) throws Exception {
		if(ids!=null&&!"".equals(ids)){
			for (int i = 0; i < ids.length; i++) {
				FreezeTaskItem pti = new FreezeTaskItem();
				pti.setFreezeTask(pt);
				pti.setCode(ids[i]);
				pti.setSampleCode(ids[i]);
				pti.setState("1");
				freezeTaskDao.save(pti);
			}
		}
		
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(pt.getId());
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		
	}

@WriteOperLog
@WriteExOperLog
@Transactional(rollbackFor = Exception.class)
public void saveRightQuality(FreezeTask pt,String []ids,String logInfo) throws Exception {
	if(ids!=null&&!"".equals(ids)){
			for (int i = 0; i < ids.length; i++) {
				FreezeTaskItem pti = new FreezeTaskItem();
				pti.setFreezeTask(pt);
				pti.setCode(ids[i]);
				pti.setSampleCode(ids[i]);
				pti.setState("1");
				commonDAO.save(pti);
			}
		}
		
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
				.getRequest()
				.getSession()
				.getAttribute(
						SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(pt.getId());
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
	
}
public FreezeTaskInfo getInfoById(String idc) {
	return freezeTaskDao.get(FreezeTaskInfo.class,idc);
	
}
}

