package com.biolims.experiment.freeze.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import com.biolims.common.constants.SystemConstants;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.freeze.model.FreezeTask;
import com.biolims.experiment.freeze.model.FreezeTaskCos;
import com.biolims.experiment.freeze.model.FreezeTaskItem;
import com.biolims.experiment.freeze.model.FreezeTaskReagent;
import com.biolims.experiment.freeze.model.FreezeTaskTemp;
import com.biolims.experiment.freeze.model.FreezeTaskTemplate;
import com.biolims.experiment.freeze.model.FreezeTaskInfo;
import com.opensymphony.xwork2.ActionContext;


@Repository
@SuppressWarnings("unchecked")
public class FreezeTaskDao extends BaseHibernateDao {

	public Map<String, Object> findFreezeTaskTable(Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from FreezeTask where 1=1";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from FreezeTask where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<FreezeTask> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public Map<String, Object> selectFreezeTaskTempTable(String[] codes, Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
			
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from FreezeTaskTemp where 1=1 and state='1'";
			String key = "";
			if(query!=null){
				key=map2Where(query);
			}
			if(codes!=null&&!codes.equals("")){
				key+=" and code in (:alist)";
			}
			String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
			if(!"all".equals(scopeId)){
				key+=" and scopeId='"+scopeId+"'";
			}
			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				List<FreezeTaskTemp> list=null;
				Long filterCount = (Long) getSession().createQuery(countHql).uniqueResult();
				String hql = "from FreezeTaskTemp  where 1=1 and state='1'";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					col=col.replace("-", ".");
					key+=" order by "+col+" "+sort;
				}
				if(codes!=null&&!codes.equals("")){
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).setParameterList("alist", codes).list();
				}else{
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
				}
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
				}
			return map;
		
			
	}

	public Map<String, Object> findFreezeTaskItemTable(String id,Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from FreezeTaskItem where 1=1 and state='1' and freezeTask.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from FreezeTaskItem  where 1=1 and state='1' and freezeTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<FreezeTaskItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public List<FreezeTaskItem> showWellPlate(String id) {
		String hql="from FreezeTaskItem  where 1=1 and state='2' and  freezeTask.id='"+id+"'";
		List<FreezeTaskItem> list = getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> findFreezeTaskItemAfTable(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from FreezeTaskItem where 1=1 and state='2' and freezeTask.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from FreezeTaskItem  where 1=1 and state='2' and freezeTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<FreezeTaskItem> list =this.getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
		}

	public List<String> getCountGroup(String id) {

		String countHql = "select count(*) from FreezeTaskItem where 1=1 and state='2' and freezeTask.id='"+id+"'";
		String key = "";
		List<String> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "select counts from FreezeTaskItem where 1=1 and state='2' and freezeTask.id='"+id+"' group by counts";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
		
	}

	public List<FreezeTaskTemplate> showFreezeTaskStepsJson(String id,String code) {

		String countHql = "select count(*) from FreezeTaskTemplate where 1=1 and freezeTask.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and orderNum ='"+code+"'";
		}else{
		}
		List<FreezeTaskTemplate> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from FreezeTaskTemplate where 1=1 and freezeTask.id='"+id+"'";
			key+=" order by orderNum asc";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<FreezeTaskReagent> showFreezeTaskReagentJson(String id,String code) {
		String countHql = "select count(*) from FreezeTaskReagent where 1=1 and freezeTask.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<FreezeTaskReagent> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from FreezeTaskReagent where 1=1 and freezeTask.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<FreezeTaskCos> showFreezeTaskCosJson(String id,String code) {
		String countHql = "select count(*) from FreezeTaskCos where 1=1 and freezeTask.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<FreezeTaskCos> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from FreezeTaskCos where 1=1 and freezeTask.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<FreezeTaskTemplate> delTemplateItem(String id) {
		List<FreezeTaskTemplate> list=new ArrayList<FreezeTaskTemplate>();
		String hql = "from FreezeTaskTemplate where 1=1 and freezeTask.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<FreezeTaskReagent> delReagentItem(String id) {
		List<FreezeTaskReagent> list=new ArrayList<FreezeTaskReagent>();
		String hql = "from FreezeTaskReagent where 1=1 and freezeTask.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<FreezeTaskCos> delCosItem(String id) {
		List<FreezeTaskCos> list=new ArrayList<FreezeTaskCos>();
		String hql = "from FreezeTaskCos where 1=1 and freezeTask.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> showFreezeTaskResultTableJson(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from FreezeTaskInfo where 1=1 and freezeTask.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from FreezeTaskInfo  where 1=1 and freezeTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<FreezeTaskInfo> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	}

	public List<Object> showWellList(String id) {
		
		String hql="select counts,count(*) from FreezeTaskItem where 1=1 and freezeTask.id='"+id+"' group by counts";
		List<Object> list=getSession().createQuery(hql).list();
	
		return list;
	}

	public List<FreezeTaskItem> plateSample(String id, String counts) {
		String hql="from FreezeTaskItem where 1=1 and freezeTask.id='"+id+"'";
		String key="";
		if(counts==null||counts.equals("")){
			key=" and counts is null";
		}else{
			key="and counts='"+counts+"'";
		}
		List<FreezeTaskItem> list=getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> plateSampleTable(String id, String counts,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from FreezeTaskItem where 1=1 and freezeTask.id='"+id+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		if(counts==null||counts.equals("")){
			key+=" and counts is null";
		}else{
			key+="and counts='"+counts+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from FreezeTaskItem  where 1=1 and freezeTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<FreezeTaskItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	
	}

	public List<FreezeTaskInfo> findFreezeTaskInfoByCode(String code) {
		String hql="from FreezeTaskInfo where 1=1 and code='"+code+"'";
		List<FreezeTaskInfo> list=new ArrayList<FreezeTaskInfo>();
		list=getSession().createQuery(hql).list();
		return list;
	}
	
	public List<FreezeTaskInfo> selectAllResultListById(String code) {
		String hql = "from FreezeTaskInfo  where (submit is null or submit='') and freezeTask.id='"
				+ code + "'";
		List<FreezeTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	public List<FreezeTaskInfo> selectResultListById(String id) {
		String hql = "from FreezeTaskInfo  where freezeTask.id='"
				+ id + "'";
		List<FreezeTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<FreezeTaskInfo> selectAllResultListByIds(String[] codes) {
		String insql = "''";
		for (String code : codes) {
			insql += ",'" + code + "'";
		}
		String hql = "from FreezeTaskInfo t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<FreezeTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

		public List<FreezeTaskItem> selectFreezeTaskItemList(String scId)
			throws Exception {
		String hql = "from FreezeTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and freezeTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<FreezeTaskItem> list = new ArrayList<FreezeTaskItem>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}
	
		public Integer generateBlendCode(String id) {
			String hql="select max(blendCode) from FreezeTaskItem where 1=1 and freezeTask.id='"+id+"'";
			Integer blendCode=(Integer) this.getSession().createQuery(hql).uniqueResult();
			return blendCode;
		}

		public FreezeTaskInfo getResultByCode(String code) {
			String hql=" from FreezeTaskInfo where 1=1 and code='"+code+"'";
			FreezeTaskInfo spi=(FreezeTaskInfo) this.getSession().createQuery(hql).uniqueResult();
			return spi;
		}
	
}