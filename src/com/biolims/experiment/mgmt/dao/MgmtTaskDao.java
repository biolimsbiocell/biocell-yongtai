package com.biolims.experiment.mgmt.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.mgmt.model.MgmtTask;
import com.biolims.experiment.mgmt.model.MgmtTaskCos;
import com.biolims.experiment.mgmt.model.MgmtTaskItem;
import com.biolims.experiment.mgmt.model.MgmtTaskReagent;
import com.biolims.experiment.mgmt.model.MgmtTaskTemp;
import com.biolims.experiment.mgmt.model.MgmtTaskTemplate;
import com.biolims.experiment.mgmt.model.MgmtTaskInfo;



@Repository
@SuppressWarnings("unchecked")
public class MgmtTaskDao extends BaseHibernateDao {

	public Map<String, Object> findMgmtTaskTable(Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from MgmtTask where 1=1";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from MgmtTask where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<MgmtTask> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public Map<String, Object> selectMgmtTaskTempTable(String[] codes, Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
			
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from MgmtTaskTemp where 1=1 and state='1'";
			String key = "";
			if(query!=null&&!"".equals(query)){
				key=map2Where(query);
			}
			if(codes!=null&&!codes.equals("")){
				key+=" and code in (:alist)";
			}
			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				List<MgmtTaskTemp> list=null;
				Long filterCount = (Long) getSession().createQuery(countHql).uniqueResult();
				String hql = "from MgmtTaskTemp  where 1=1 and state='1'";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					col=col.replace("-", ".");
					key+=" order by "+col+" "+sort;
				}
				if(codes!=null&&!codes.equals("")){
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).setParameterList("alist", codes).list();
				}else{
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
				}
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
				}
			return map;
		
			
	}

	public Map<String, Object> findMgmtTaskItemTable(String id,Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from MgmtTaskItem where 1=1 and state='1' and mgmtTask.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from MgmtTaskItem  where 1=1 and state='1' and mgmtTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<MgmtTaskItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public List<MgmtTaskItem> showWellPlate(String id) {
		String hql="from MgmtTaskItem  where 1=1 and state='2' and  mgmtTask.id='"+id+"'";
		List<MgmtTaskItem> list = getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> findMgmtTaskItemAfTable(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from MgmtTaskItem where 1=1 and state='2' and mgmtTask.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from MgmtTaskItem  where 1=1 and state='2' and mgmtTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<MgmtTaskItem> list =this.getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
		}

	public List<String> getCountGroup(String id) {

		String countHql = "select count(*) from MgmtTaskItem where 1=1 and state='2' and mgmtTask.id='"+id+"'";
		String key = "";
		List<String> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "select counts from MgmtTaskItem where 1=1 and state='2' and mgmtTask.id='"+id+"' group by counts";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
		
	}

	public List<MgmtTaskTemplate> showMgmtTaskStepsJson(String id,String code) {

		String countHql = "select count(*) from MgmtTaskTemplate where 1=1 and mgmtTask.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and code='"+code+"'";
		}else{
		}
		List<MgmtTaskTemplate> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from MgmtTaskTemplate where 1=1 and mgmtTask.id='"+id+"'";
			key+=" order by orderNum asc";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<MgmtTaskReagent> showMgmtTaskReagentJson(String id,String code) {
		String countHql = "select count(*) from MgmtTaskReagent where 1=1 and mgmtTask.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<MgmtTaskReagent> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from MgmtTaskReagent where 1=1 and mgmtTask.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<MgmtTaskCos> showMgmtTaskCosJson(String id,String code) {
		String countHql = "select count(*) from MgmtTaskCos where 1=1 and mgmtTask.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<MgmtTaskCos> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from MgmtTaskCos where 1=1 and mgmtTask.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<MgmtTaskTemplate> delTemplateItem(String id) {
		List<MgmtTaskTemplate> list=new ArrayList<MgmtTaskTemplate>();
		String hql = "from MgmtTaskTemplate where 1=1 and mgmtTask.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<MgmtTaskReagent> delReagentItem(String id) {
		List<MgmtTaskReagent> list=new ArrayList<MgmtTaskReagent>();
		String hql = "from MgmtTaskReagent where 1=1 and mgmtTask.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<MgmtTaskCos> delCosItem(String id) {
		List<MgmtTaskCos> list=new ArrayList<MgmtTaskCos>();
		String hql = "from MgmtTaskCos where 1=1 and mgmtTask.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> showMgmtTaskResultTableJson(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from MgmtTaskInfo where 1=1 and mgmtTask.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from MgmtTaskInfo  where 1=1 and mgmtTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<MgmtTaskInfo> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	}

	public List<Object> showWellList(String id) {
		String hql="select counts,count(*) from mgmt_task_item where 1=1 and mgmt_task='"+id+"' group by counts";
		List<Object> list=getSession().createSQLQuery(hql).list();
		return list;
	}

	public List<MgmtTaskItem> plateSample(String id, String counts) {
		String hql="from MgmtTaskItem where 1=1 and mgmtTask.id='"+id+"' and counts='"+counts+"'";
		List<MgmtTaskItem> list=getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> plateSampleTable(String id, String counts,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from MgmtTaskItem where 1=1 and mgmtTask.id='"+id+"' and counts='"+counts+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from MgmtTaskItem  where 1=1 and mgmtTask.id='"+id+"' and counts='"+counts+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<MgmtTaskItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	
	}

	public List<MgmtTaskItem> findMgmtTaskItemByCode(String code) {
		String hql="from MgmtTaskItem where 1=1 and code='"+code+"'";
		List<MgmtTaskItem> list=new ArrayList<MgmtTaskItem>();
		list=getSession().createQuery(hql).list();
		return list;
	}
	public List<MgmtTaskInfo> selectAllResultListById(String code) {
		String hql = "from MgmtTaskInfo  where (submit is null or submit='') and mgmtTask.id='"
				+ code + "'";
		List<MgmtTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<MgmtTaskInfo> selectAllResultListByIds(String[] codes) {
		String insql = "''";
		for (String code : codes) {
			insql += ",'" + code + "'";
		}
		String hql = "from MgmtTaskInfo t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<MgmtTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

		public List<MgmtTaskItem> selectMgmtTaskItemList(String scId)
			throws Exception {
		String hql = "from MgmtTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and mgmtTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<MgmtTaskItem> list = new ArrayList<MgmtTaskItem>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}
}