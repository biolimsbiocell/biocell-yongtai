﻿package com.biolims.experiment.mgmt.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.mgmt.model.MgmtTaskAbnormal;
import com.biolims.experiment.mgmt.service.MgmtTaskAbnormalService;
import com.biolims.experiment.mgmt.service.MgmtTaskService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.common.PushData;
@Namespace("/experiment/mgmt/mgmtTaskAbnormal")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class MgmtTaskAbnormalAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "24121";// 240102
	@Autowired
	private MgmtTaskAbnormalService mgmtTaskAbnormalService;
	@Resource
	private FileInfoService fileInfoService;
	@Autowired
	private MgmtTaskService mgmtTaskService;
	
	/**
	 * 
	 * @Title: showMgmtTaskAbnormalTable
	 * @Description: 展示异常样本列表
	 * @author 
	 * @date 
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showMgmtTaskAbnormalTable")
	public String showMgmtTaskAbnormalTable() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/mgmt/mgmtTaskAbnormal.jsp");
	}

	@Action(value = "showMgmtTaskAbnormalTableJson")
	public void showMgmtTaskAbnormalTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = mgmtTaskAbnormalService
				.showMgmtTaskAbnormalTableJson(start, length, query, col, sort);
		List<MgmtTaskAbnormal> list = (List<MgmtTaskAbnormal>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("sampleCode", "");
		map.put("sampleType", "");
		map.put("nextFlowId", "");
		map.put("nextFlow", "");
		map.put("isExecute", "");
		map.put("note", "");
		map.put("code", "");
		map.put("method", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("result", "");
		map.put("volume", "");
		map.put("unit", "");
		map.put("patient", "");
		map.put("reportDate", "yyyy-MM-dd");
		map.put("reason", "");
		map.put("concentration", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("orderId", "");
		map.put("classify", "");
		map.put("sampleNum", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * 
	 * @Title: executeAbnormal
	 * @Description:执行异常列表
	 * @author 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "executeAbnormal")
	public void executeAbnormal() throws Exception {
		String[] ids = getRequest().getParameterValues("ids[]");
		String dataJson = getParameterFromRequest("dataJson");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			mgmtTaskAbnormalService.executeAbnormal(ids, dataJson);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public MgmtTaskAbnormalService getMgmtTaskAbnormalService() {
		return mgmtTaskAbnormalService;
	}

	public void setMgmtTaskAbnormalService(
			MgmtTaskAbnormalService mgmtTaskAbnormalService) {
		this.mgmtTaskAbnormalService =mgmtTaskAbnormalService;
	}

}
