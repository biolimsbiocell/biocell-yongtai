package com.biolims.experiment.karyoship.custom;


import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.karyoship.service.KaryoShipTaskService;

public class KaryoShipTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		KaryoShipTaskService mbService = (KaryoShipTaskService) ctx.getBean("karyoShipTaskService");
		mbService.changeState(applicationTypeActionId, contentId);
		return "";
	}
}
