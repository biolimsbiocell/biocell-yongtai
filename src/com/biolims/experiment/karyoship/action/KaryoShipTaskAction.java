package com.biolims.experiment.karyoship.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.comsearch.dao.ComSearchDao;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.karyoget.model.KaryoGetTask;
import com.biolims.experiment.karyoship.model.KaryoShipTask;
import com.biolims.experiment.karyoship.model.KaryoShipTaskCos;
import com.biolims.experiment.karyoship.model.KaryoShipTaskItem;
import com.biolims.experiment.karyoship.model.KaryoShipTaskReagent;
import com.biolims.experiment.karyoship.model.KaryoShipTaskResult;
import com.biolims.experiment.karyoship.model.KaryoShipTaskTemp;
import com.biolims.experiment.karyoship.model.KaryoShipTaskTemplate;
import com.biolims.experiment.karyoship.service.KaryoShipTaskService;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.template.model.Template;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/karyoship/karyoShipTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class KaryoShipTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "249003";
	@Autowired
	private KaryoShipTaskService karyoShipTaskService;
	private KaryoShipTask karyoShipTask = new KaryoShipTask();
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private ComSearchDao comSearchDao;

	@Action(value = "showKaryoShipTaskList")
	public String showKaryoShipTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/karyoship/karyoShipTask.jsp");
	}

	@Action(value = "showKaryoShipTaskListJson")
	public void showKaryoShipTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = karyoShipTaskService
				.findKaryoShipTaskList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<KaryoShipTask> list = (List<KaryoShipTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("sampleType-id", "");
		map.put("sampleType-name", "");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("confirmDate", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "karyoShipTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogKaryoShipTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/karyoship/karyoShipTaskDialog.jsp");
	}

	@Action(value = "showDialogKaryoShipTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogKaryoShipTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = karyoShipTaskService
				.findKaryoShipTaskList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<KaryoShipTask> list = (List<KaryoShipTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("sampleType-id", "");
		map.put("sampleType-name", "");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("confirmDate", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editKaryoShipTask")
	public String editKaryoShipTask() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			karyoShipTask = karyoShipTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "karyoShipTask");
		} else {
			karyoShipTask.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			karyoShipTask.setCreateUser(user);
			karyoShipTask.setCreateDate(new Date());
			karyoShipTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			karyoShipTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
//			List<Template> tlist=comSearchDao.getTemplateByType("doCkaShip");
//			Template t=tlist.get(0);
//			karyoShipTask.setTemplate(t);
//			//UserGroup u=comSearchDao.get(UserGroup.class,t.getAcceptUser().getId());
//			if(t.getAcceptUser()!=null){
//				karyoShipTask.setAcceptUser(t.getAcceptUser());
//			}
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(karyoShipTask.getState());
		putObjToContext("fileNum", num);
		User user2 = (User) this
				.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		putObjToContext("user2", user2.getName());
		return dispatcher("/WEB-INF/page/experiment/karyoship/karyoShipTaskEdit.jsp");
	}

	@Action(value = "copyKaryoShipTask")
	public String copyKaryoShipTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		karyoShipTask = karyoShipTaskService.get(id);
		karyoShipTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/karyoship/karyoShipTaskEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = karyoShipTask.getId();
		/*
		 * if(id!=null&&id.equals("")){ karyoShipTask.setId(null); }
		 */
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "KaryoShipTask";
			String markCode = "YBZP";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			karyoShipTask.setId(autoID);
//			karyoShipTask.setName("样本制片-"+autoID);
		}
		Map aMap = new HashMap();
		aMap.put("karyoShipTaskItem",
				getParameterFromRequest("karyoShipTaskItemJson"));

		aMap.put("karyoShipTaskTemplate",
				getParameterFromRequest("karyoShipTaskTemplateJson"));

		aMap.put("karyoShipTaskReagent",
				getParameterFromRequest("karyoShipTaskReagentJson"));

		aMap.put("karyoShipTaskCos",
				getParameterFromRequest("karyoShipTaskCosJson"));

		aMap.put("karyoShipTaskResult",
				getParameterFromRequest("karyoShipTaskResultJson"));

		karyoShipTaskService.save(karyoShipTask, aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/experiment/karyoship/karyoShipTask/editKaryoShipTask.action?id="
				+ karyoShipTask.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);
		// return
		// redirect("/experiment/karyoship/karyoShipTask/editKaryoShipTask.action?id="
		// + karyoShipTask.getId());

	}

	@Action(value = "viewKaryoShipTask")
	public String toViewKaryoShipTask() throws Exception {
		String id = getParameterFromRequest("id");
		karyoShipTask = karyoShipTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/karyoship/karyoShipTaskEdit.jsp");
	}

	@Action(value = "showKaryoShipTaskItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showKaryoShipTaskItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/karyoship/karyoShipTaskItem.jsp");
	}

	@Action(value = "showKaryoShipTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showKaryoShipTaskItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = karyoShipTaskService
					.findKaryoShipTaskItemList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<KaryoShipTaskItem> list = (List<KaryoShipTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("experimentCode", "");
			map.put("checkCode", "");
			map.put("preReapDate", "yyyy-MM-dd");
			map.put("harvestDate", "yyyy-MM-dd");
			map.put("inoculateDate", "yyyy-MM-dd");
			map.put("shipDate", "yyyy-MM-dd");
			map.put("note", "");
			map.put("tempId", "");
			map.put("state", "");
			map.put("sampleType", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("productNum", "");
			map.put("orderId", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("karyoShipTask-name", "");
			map.put("karyoShipTask-id", "");

			map.put("productId", "");
			map.put("productName", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("reportDate", "yyyy-MM-dd");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKaryoShipTaskItem")
	public void delKaryoShipTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			karyoShipTaskService.delKaryoShipTaskItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showKaryoShipTaskTemplateList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showKaryoShipTaskTemplateList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/karyoship/karyoShipTaskTemplate.jsp");
	}

	@Action(value = "showKaryoShipTaskTemplateListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showKaryoShipTaskTemplateListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = karyoShipTaskService
					.findKaryoShipTaskTemplateList(scId, startNum, limitNum,
							dir, sort);
			Long total = (Long) result.get("total");
			List<KaryoShipTaskTemplate> list = (List<KaryoShipTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("stepNum", "");
			map.put("stepDescribe", "");
			map.put("experimentUser-id", "");
			map.put("experimentUser-name", "");
			map.put("startDate", "");
			map.put("endDate", "");
			map.put("patientName", "");
			map.put("state", "");
			map.put("karyoShipTask-name", "");
			map.put("karyoShipTask-id", "");
			map.put("note", "");
			map.put("sampleCodes", "");
			map.put("tItem", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKaryoShipTaskTemplate")
	public void delKaryoShipTaskTemplate() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			karyoShipTaskService.delKaryoShipTaskTemplate(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showKaryoShipTaskReagentList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showKaryoShipTaskReagentList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/karyoship/karyoShipTaskReagent.jsp");
	}

	@Action(value = "showKaryoShipTaskReagentListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showKaryoShipTaskReagentListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = karyoShipTaskService
					.findKaryoShipTaskReagentList(scId, startNum, limitNum,
							dir, sort);
			Long total = (Long) result.get("total");
			List<KaryoShipTaskReagent> list = (List<KaryoShipTaskReagent>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("agentiaCode", "");
			map.put("agentiaName", "");
			map.put("batch", "");
			map.put("isCheck", "");
			map.put("singleDosage", "");
			map.put("reactionDosage", "");
			map.put("dosage", "");
			map.put("karyoShipTask-name", "");
			map.put("karyoShipTask-id", "");
			map.put("note", "");
			map.put("tReagent", "");
			map.put("itemId", "");
			map.put("sampleNum", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKaryoShipTaskReagent")
	public void delKaryoShipTaskReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			karyoShipTaskService.delKaryoShipTaskReagent(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showKaryoShipTaskCosList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showKaryoShipTaskCosList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/karyoship/karyoShipTaskCos.jsp");
	}

	@Action(value = "showKaryoShipTaskCosListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showKaryoShipTaskCosListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = karyoShipTaskService
					.findKaryoShipTaskCosList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<KaryoShipTaskCos> list = (List<KaryoShipTaskCos>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("instrumentCode", "");
			map.put("instrumentName", "");
			map.put("isCheck", "");
			map.put("temperature", "");
			map.put("speed", "");
			map.put("time", "yyyy-MM-dd");
			map.put("karyoShipTask-name", "");
			map.put("karyoShipTask-id", "");
			map.put("note", "");
			map.put("tCos", "");
			map.put("itemId", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKaryoShipTaskCos")
	public void delKaryoShipTaskCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			karyoShipTaskService.delKaryoShipTaskCos(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showKaryoShipTaskResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showKaryoShipTaskResultList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/karyoship/karyoShipTaskResult.jsp");
	}

	@Action(value = "showKaryoShipTaskResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showKaryoShipTaskResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = karyoShipTaskService
					.findKaryoShipTaskResultList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<KaryoShipTaskResult> list = (List<KaryoShipTaskResult>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("sampleCode", "");
			map.put("experimentCode", "");
			map.put("code", "");
			map.put("zjCode", "");
			map.put("inoculateDate", "yyyy-MM-dd");
			map.put("shipDate", "yyyy-MM-dd");
			map.put("harvestDate", "yyyy-MM-dd");
			map.put("harversUser", "");
			map.put("nextFlow", "");
			map.put("nextFlowId", "");
			map.put("isrStandard", "");
			map.put("isCommit", "");
			map.put("karyoShipTask-name", "");
			map.put("karyoShipTask-id", "");
			map.put("note", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("tempId", "");
			map.put("state", "");
			map.put("orderId", "");
			map.put("sampleType", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("reportDate", "yyyy-MM-dd");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKaryoShipTaskResult")
	public void delKaryoShipTaskResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			karyoShipTaskService.delKaryoShipTaskResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showKaryoShipTaskTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showKaryoShipTaskTempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/karyoship/karyoShipTaskTemp.jsp");
	}

	@Action(value = "showKaryoShipTaskTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showKaryoShipTaskTempListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = karyoShipTaskService
				.findKaryoShipTaskTempList(map2Query, startNum, limitNum, dir,
						sort);
		Long total = (Long) result.get("total");
		List<KaryoShipTaskTemp> list = (List<KaryoShipTaskTemp>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("patientName", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("acceptDate", "yyyy-MM-dd");
		map.put("reportDate", "yyyy-MM-dd");
		map.put("orderId", "");
		map.put("state", "");
		map.put("note", "");
		map.put("sampleType", "");
		map.put("preReapDate", "yyyy-MM-dd");
		//收费状态
		map.put("chargeNote","");
		map.put("inoculateDate", "yyyy-MM-dd");//接种日期
		map.put("harvestDate", "yyyy-MM-dd");//收获日期
		new SendData().sendDateJson(map, list, total,
				ServletActionContext.getResponse());
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKaryoShipTaskTemp")
	public void delKaryoShipTaskTemp() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			karyoShipTaskService.delKaryoShipTaskTemp(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public KaryoShipTaskService getKaryoShipTaskService() {
		return karyoShipTaskService;
	}

	public void setKaryoShipTaskService(
			KaryoShipTaskService karyoShipTaskService) {
		this.karyoShipTaskService = karyoShipTaskService;
	}

	public KaryoShipTask getKaryoShipTask() {
		return karyoShipTask;
	}

	public void setKaryoShipTask(KaryoShipTask karyoShipTask) {
		this.karyoShipTask = karyoShipTask;
	}

	/**
	 * 根据ID删除
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKaryoShipTaskReagentOne")
	public void delKaryoShipTaskReagentOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			karyoShipTaskService.delKaryoShipTaskReagentOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKaryoShipTaskTemplateOne")
	public void delKaryoShipTaskTemplateOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			karyoShipTaskService.delKaryoShipTaskTemplateOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKaryoShipTaskCosOne")
	public void delKaryoShipTaskCosOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			karyoShipTaskService.delKaryoShipTaskCosOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	// 提交样本
		@Action(value = "submitSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
		public void submitSample() throws Exception {
			String id = getParameterFromRequest("id");
			String[] ids = getRequest().getParameterValues("ids[]");
			Map<String, Object> result = new HashMap<String, Object>();
			try {
				this.karyoShipTaskService.submitSample(id, ids);

				result.put("success", true);

			} catch (Exception e) {
				result.put("success", false);
			}
			HttpUtils.write(JsonUtils.toJsonString(result));
		}
		
	 /**
	 * 保存样本制片明细
	 * @throws Exception
	 */
	@Action(value = "saveKaryoShipTaskItem",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveKaryoShipTaskItem() throws Exception {
		String id = getRequest().getParameter("id");
		String itemDataJson = getRequest().getParameter("itemDataJson");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			KaryoShipTask sc=commonDAO.get(KaryoShipTask.class, id);
			Map aMap = new HashMap();
			aMap.put("karyoShipTaskItem",
					itemDataJson);
			if(sc!=null){
				karyoShipTaskService.save(sc,aMap);
			}
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	 /**
	 * 保存制片结果
	 * @throws Exception
	 */
	@Action(value = "saveKaryoShipTaskResult",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveKaryoShipTaskResult() throws Exception {
		String id = getRequest().getParameter("id");
		String itemDataJson = getRequest().getParameter("itemDataJson");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			KaryoShipTask sc=commonDAO.get(KaryoShipTask.class, id);
			Map aMap = new HashMap();
			aMap.put("karyoShipTaskResult",
					itemDataJson);
			if(sc!=null){
				karyoShipTaskService.save(sc,aMap);
			}
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}


	}

