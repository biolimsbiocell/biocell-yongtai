package com.biolims.experiment.karyoship.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.fish.model.SamplePreTaskResult;
import com.biolims.experiment.karyo.model.KaryoTypeTaskAgentia;
import com.biolims.experiment.karyo.model.KaryoTypeTaskItem;
import com.biolims.experiment.karyoship.model.KaryoShipTask;
import com.biolims.experiment.karyoship.model.KaryoShipTaskReagent;
import com.biolims.experiment.karyoship.model.KaryoShipTaskCos;
import com.biolims.experiment.karyoship.model.KaryoShipTaskResult;
import com.biolims.experiment.karyoship.model.KaryoShipTaskItem;
import com.biolims.experiment.karyoship.model.KaryoShipTaskTemp;
import com.biolims.experiment.karyoship.model.KaryoShipTaskTemplate;

@Repository
@SuppressWarnings("unchecked")
public class KaryoShipTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectKaryoShipTaskList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from KaryoShipTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<KaryoShipTask> list = new ArrayList<KaryoShipTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
		public Map<String, Object> selectKaryoShipTaskItemList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from KaryoShipTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and karyoShipTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<KaryoShipTaskItem> list = new ArrayList<KaryoShipTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectKaryoShipTaskTemplateList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from KaryoShipTaskTemplate where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and karyoShipTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<KaryoShipTaskTemplate> list = new ArrayList<KaryoShipTaskTemplate>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectKaryoShipTaskAgentiaList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from KaryoShipTaskReagent where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and karyoShipTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<KaryoShipTaskReagent> list = new ArrayList<KaryoShipTaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectKaryoShipTaskCosList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from KaryoShipTaskCos where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and karyoShipTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<KaryoShipTaskCos> list = new ArrayList<KaryoShipTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectKaryoShipTaskHarvestList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from KaryoShipTaskResult where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and karyoShipTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<KaryoShipTaskResult> list = new ArrayList<KaryoShipTaskResult>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		/*public Map<String, Object> selectKaryoShipTaskTempList(String scId, Integer startNum, Integer limitNum,
				String dir, String sort) throws Exception {
			String hql = "from KaryoShipTaskTemp where 1=1 ";
			String key = "";
			Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
			List<KaryoShipTaskTemp> list = new ArrayList<KaryoShipTaskTemp>();
			if (total > 0) {
				if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
					if (sort.indexOf("-") != -1) {
						sort = sort.substring(0, sort.indexOf("-"));
					}
					key = key + " order by " + sort + " " + dir;
				} 
				if (startNum == null || limitNum == null) {
					list = this.getSession().createQuery(hql + key).list();
				} else {
					list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
				}
			}
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("total", total);
			result.put("list", list);
			return result;
		}*/
	public Map<String, Object> selectKaryoShipTaskTempList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from KaryoShipTaskTemp where 1=1 and state='1'";
		if (mapForQuery != null){
			key = map2where(mapForQuery);
		}
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<KaryoShipTaskTemp> list = new ArrayList<KaryoShipTaskTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	
	/**
	 * 根据主表ID查询结果明细
	 * @param id
	 * @return
	 */
	public List<KaryoShipTaskResult> getKaryoShipTaskResultById(String id){
		String hql = " from KaryoShipTaskResult where 1=1 and karyoShipTask='"+id+"'";
		List<KaryoShipTaskResult> list=this.getSession().createQuery(hql).list();
		return list;
	}
	
	// 根据样本编号查询
		public List<KaryoShipTaskResult> setResultById(String code) {
			String hql = "from KaryoShipTaskResult t where (isCommit is null or isCommit='') and karyoShipTask.id='"
					+ code + "'";
			List<KaryoShipTaskResult> list = this.getSession().createQuery(hql).list();
			return list;
		}

		// 根据样本编号查询
		public List<KaryoShipTaskResult> setResultByIds(String[] codes) {

			String insql = "''";
			for (String code : codes) {

				insql += ",'" + code + "'";

			}

			String hql = "from KaryoShipTaskResult t where (isCommit is null or isCommit='') and id in ("
					+ insql + ")";
			List<KaryoShipTaskResult> list = this.getSession().createQuery(hql).list();
			return list;
		}
		
	/**
	 * 根据主表编号Item信息
	 * @param code
	 * @return
	 */
	public List<KaryoShipTaskItem> getItem(String id) {
		String hql = "from KaryoShipTaskItem t where 1=1  and karyoShipTask='"+ id + "'";
		List<KaryoShipTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据主表编号Reagent信息
	 * @param code
	 * @return
	 */
	public List<KaryoShipTaskReagent> getReagent(String id) {
		String hql = "from KaryoShipTaskReagent t where 1=1  and karyoShipTask='"+ id + "'";
		List<KaryoShipTaskReagent> list = this.getSession().createQuery(hql).list();
		return list;
	}
}