package com.biolims.experiment.karyoship.service;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.dna.model.DnaTaskTemp;
import com.biolims.experiment.karyo.model.KaryoTypeTaskAgentia;
import com.biolims.experiment.karyo.model.KaryoTypeTaskItem;
import com.biolims.experiment.karyo.model.KaryoTypeTaskTemp;
import com.biolims.experiment.karyoabnormal.model.KaryoAbnormal;
import com.biolims.experiment.karyoship.dao.KaryoShipTaskDao;
import com.biolims.experiment.karyoship.model.KaryoShipTask;
import com.biolims.experiment.karyoship.model.KaryoShipTaskCos;
import com.biolims.experiment.karyoship.model.KaryoShipTaskItem;
import com.biolims.experiment.karyoship.model.KaryoShipTaskReagent;
import com.biolims.experiment.karyoship.model.KaryoShipTaskResult;
import com.biolims.experiment.karyoship.model.KaryoShipTaskTemp;
import com.biolims.experiment.karyoship.model.KaryoShipTaskTemplate;
import com.biolims.experiment.producer.model.ProducerTaskTemp;
import com.biolims.experiment.snpjc.abnormal.model.SnpAbnormal;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.report.model.EndReport;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class KaryoShipTaskService {
	@Resource
	private KaryoShipTaskDao karyoShipTaskDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private NextFlowDao nextFlowDao;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findKaryoShipTaskList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return karyoShipTaskDao.selectKaryoShipTaskList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(KaryoShipTask i) throws Exception {

		karyoShipTaskDao.saveOrUpdate(i);

	}
	public KaryoShipTask get(String id) {
		KaryoShipTask karyoShipTask = commonDAO.get(KaryoShipTask.class, id);
		return karyoShipTask;
	}
	public Map<String, Object> findKaryoShipTaskItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = karyoShipTaskDao.selectKaryoShipTaskItemList(scId, startNum, limitNum, dir, sort);
		List<KaryoShipTaskItem> list = (List<KaryoShipTaskItem>) result.get("list");
		return result;
	}
	public Map<String, Object> findKaryoShipTaskTemplateList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = karyoShipTaskDao.selectKaryoShipTaskTemplateList(scId, startNum, limitNum, dir, sort);
		List<KaryoShipTaskTemplate> list = (List<KaryoShipTaskTemplate>) result.get("list");
		return result;
	}
	public Map<String, Object> findKaryoShipTaskReagentList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = karyoShipTaskDao.selectKaryoShipTaskAgentiaList(scId, startNum, limitNum, dir, sort);
		List<KaryoShipTaskReagent> list = (List<KaryoShipTaskReagent>) result.get("list");
		return result;
	}
	public Map<String, Object> findKaryoShipTaskCosList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = karyoShipTaskDao.selectKaryoShipTaskCosList(scId, startNum, limitNum, dir, sort);
		List<KaryoShipTaskCos> list = (List<KaryoShipTaskCos>) result.get("list");
		return result;
	}
	public Map<String, Object> findKaryoShipTaskResultList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = karyoShipTaskDao.selectKaryoShipTaskHarvestList(scId, startNum, limitNum, dir, sort);
		List<KaryoShipTaskResult> list = (List<KaryoShipTaskResult>) result.get("list");
		return result;
	}
	public Map<String, Object> findKaryoShipTaskTempList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		/*Map<String, Object> result = karyoShipTaskDao.selectKaryoShipTaskTempList(mapForQuery,startNum,
				limitNum, dir,sort);
		List<KaryoShipTaskTemp> list = (List<KaryoShipTaskTemp>) result.get("list");
		return result;*/
		Map<String, Object> result2 = new HashMap<String, Object>();
		List<KaryoShipTaskTemp> list2=new ArrayList<KaryoShipTaskTemp>();
		Map<String, Object> result =  karyoShipTaskDao.selectKaryoShipTaskTempList(mapForQuery,
				startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<KaryoShipTaskTemp> list = (List<KaryoShipTaskTemp>) result.get("list");
		for(KaryoShipTaskTemp t:list){
			SampleInfo s=this.sampleInfoMainDao.findSampleInfo(t.getSampleCode());
			if(s!=null && s.getSampleOrder()!=null){
				t.setChargeNote(s.getSampleOrder().getChargeNote());
			}
			list2.add(t);
		}
		result2.put("total", count);
		result2.put("list", list2);
		return result2;
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveKaryoShipTaskItem(KaryoShipTask sc, String itemDataJson) throws Exception {
		List<KaryoShipTaskItem> saveItems = new ArrayList<KaryoShipTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			KaryoShipTaskItem scp = new KaryoShipTaskItem();
			// 将map信息读入实体类
			scp = (KaryoShipTaskItem) karyoShipTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setKaryoShipTask(sc);
			saveItems.add(scp);
			
			// 改变左侧样本状态
			KaryoShipTaskTemp dt = commonDAO.get(KaryoShipTaskTemp.class, scp.getTempId());
			if (dt != null) {
				dt.setState("2");
			}
			commonDAO.saveOrUpdate(dt);
			/*if(scp!=null){
				Template t=commonDAO.get(Template.class, sc.getTemplate().getId());
				//DicSampleType d=new DicSampleType();
				if(t!=null){
					if(t.getDicSampleType()!=null){
						DicSampleType d=commonDAO.get(DicSampleType.class,
								t.getDicSampleType().getId());
						if(scp.getDicSampleType()==null || 
								(scp.getDicSampleType()!=null && scp.getDicSampleType().equals(""))){
							if(d!=null){
								scp.setDicSampleType(d);
							}
						}
					}
					if(t.getProductNum()!=null){
						if(scp.getProductNum()==null || 
								(scp.getProductNum()!=null && scp.getProductNum().equals(""))){
							scp.setProductNum(t.getProductNum());
						}
					}
				}
			}*/
		}
		karyoShipTaskDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKaryoShipTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			KaryoShipTaskItem scp =  karyoShipTaskDao.get(KaryoShipTaskItem.class, id);
			 karyoShipTaskDao.delete(scp);
			 
			// 改变左侧样本状态
			KaryoShipTaskTemp dt = commonDAO.get(KaryoShipTaskTemp.class, scp.getTempId());
			if (dt != null) {
				dt.setState("1");
			}
			commonDAO.saveOrUpdate(dt);
		}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveKaryoShipTaskTemplate(KaryoShipTask sc, String itemDataJson) throws Exception {
		List<KaryoShipTaskTemplate> saveItems = new ArrayList<KaryoShipTaskTemplate>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			KaryoShipTaskTemplate scp = new KaryoShipTaskTemplate();
			// 将map信息读入实体类
			scp = (KaryoShipTaskTemplate) karyoShipTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setKaryoShipTask(sc);

			saveItems.add(scp);
		}
		karyoShipTaskDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKaryoShipTaskTemplate(String[] ids) throws Exception {
		for (String id : ids) {
			KaryoShipTaskTemplate scp =  karyoShipTaskDao.get(KaryoShipTaskTemplate.class, id);
			 karyoShipTaskDao.delete(scp);
		}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveKaryoShipTaskReagent(KaryoShipTask sc, String itemDataJson) throws Exception {
		List<KaryoShipTaskReagent> saveItems = new ArrayList<KaryoShipTaskReagent>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			KaryoShipTaskReagent scp = new KaryoShipTaskReagent();
			// 将map信息读入实体类
			scp = (KaryoShipTaskReagent) karyoShipTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setKaryoShipTask(sc);

			saveItems.add(scp);
		}
		karyoShipTaskDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKaryoShipTaskReagent(String[] ids) throws Exception {
		for (String id : ids) {
			KaryoShipTaskReagent scp =  karyoShipTaskDao.get(KaryoShipTaskReagent.class, id);
			 karyoShipTaskDao.delete(scp);
		}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveKaryoShipTaskCos(KaryoShipTask sc, String itemDataJson) throws Exception {
		List<KaryoShipTaskCos> saveItems = new ArrayList<KaryoShipTaskCos>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			KaryoShipTaskCos scp = new KaryoShipTaskCos();
			// 将map信息读入实体类
			scp = (KaryoShipTaskCos) karyoShipTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setKaryoShipTask(sc);

			saveItems.add(scp);
		}
		karyoShipTaskDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKaryoShipTaskCos(String[] ids) throws Exception {
		for (String id : ids) {
			KaryoShipTaskCos scp =  karyoShipTaskDao.get(KaryoShipTaskCos.class, id);
			 karyoShipTaskDao.delete(scp);
		}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveKaryoShipTaskResult(KaryoShipTask sc, String itemDataJson) throws Exception {
		List<KaryoShipTaskResult> saveItems = new ArrayList<KaryoShipTaskResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for (Map<String, Object> map : list) {
			KaryoShipTaskResult scp = new KaryoShipTaskResult();
			// 将map信息读入实体类
			scp = (KaryoShipTaskResult) karyoShipTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setKaryoShipTask(sc);

			/*if (scp.getCode() == null || scp.getCode().equals("")) {
				String markCode = "";
				if (scp.getDicSampleType() != null
						&& !scp.getDicSampleType().equals("")) {
					DicSampleType d = commonDAO.get(DicSampleType.class, scp
							.getDicSampleType().getId());

					markCode = scp.getSampleCode() + d.getCode();
				}else{
					markCode = scp.getSampleCode();
				}
				String code = codingRuleService.getCode("KaryoShipTaskResult",
						markCode, 00, 2, null);
				scp.setCode(code);
			}*/
			saveItems.add(scp);
			karyoShipTaskDao.saveOrUpdate(scp);
			if (scp != null) {
				if (scp.getIsrStandard() != null && scp.getIsCommit() != null &&
						!scp.getIsrStandard().equals("") && !scp.getIsCommit().equals("") ){
					if(scp.getIsCommit().equals("1")){
						if(scp.getIsrStandard().equals("1")){
							if(scp.getNextFlowId()!=null && !scp.getNextFlowId().equals("")){
								if (scp.getNextFlowId().equals("0009")) {// 样本入库
									SampleInItemTemp st = new SampleInItemTemp();
									st.setCode(scp.getCode());
									st.setSampleCode(scp.getSampleCode());
									st.setState("1");
									commonDAO.saveOrUpdate(st);
									// 入库，改变SampleInfo中原始样本的状态为“待入库”
									SampleInfo sf = sampleInfoMainDao
											.findSampleInfo(scp.getCode());
									if (sf != null) {
										sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW);
										sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW_NAME);
									}
								} else if (scp.getNextFlowId().equals("0012")) {// 暂停
									// 暂停，改变SampleInfo中原始样本的状态为“实验暂停”
									SampleInfo sf = sampleInfoMainDao
											.findSampleInfo(scp.getCode());
									if (sf != null) {
										sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
										sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
									}
								} else if (scp.getNextFlowId().equals("0013")) {// 终止
									// 终止，改变SampleInfo中原始样本的状态为“实验终止”
									SampleInfo sf = sampleInfoMainDao
											.findSampleInfo(scp.getCode());
									if (sf != null) {
										sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
										sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
									}
								}else {
									// 得到下一步流向的相关表单
									List<NextFlow> list_nextFlow = nextFlowDao
											.seletNextFlowById(scp.getNextFlowId());
									for (NextFlow n : list_nextFlow) {
										Object o = Class.forName(
												n.getApplicationTypeTable()
														.getClassPath())
												.newInstance();
										scp.setState("1");
										sampleInputService.copy(o, scp);
									}
								}
							}
						}else{
							KaryoAbnormal ka=new KaryoAbnormal();
							ka.setCode(scp.getCode());
							ka.setSampleCode(scp.getSampleCode());
							ka.setProductId(scp.getProductId());
							ka.setProductName(scp.getProductName());
							ka.setOrderId(scp.getOrderId());
							ka.setAcceptDate(scp.getAcceptDate());
							ka.setReportDate(scp.getReportDate());
							ka.setSampleType(scp.getSampleType());
							ka.setTaskId(scp.getKaryoShipTask().getId());
							ka.setNote(scp.getNote());
							ka.setTaskName("样本制片");
							ka.setState("1");
							
							commonDAO.saveOrUpdate(ka);
						}
						sampleStateService
						.saveSampleState(
							scp.getCode(),
							scp.getSampleCode(),
							scp.getProductId(),
							scp.getProductName(),
							"",
							format.format(sc.getCreateDate()),
							format.format(new Date()),
							"KaryoShipTask",
							"样本制片",
							(User) ServletActionContext
									.getRequest()
									.getSession()
									.getAttribute(
											SystemConstants.USER_SESSION_KEY),
							sc.getId(), scp.getNextFlow(),
							scp.getIsrStandard(), null, null, null,
							null, null, null, null, null);
					}
				}
			}
		}
		karyoShipTaskDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKaryoShipTaskResult(String[] ids) throws Exception {
		for (String id : ids) {
			KaryoShipTaskResult scp =  karyoShipTaskDao.get(KaryoShipTaskResult.class, id);
			 karyoShipTaskDao.delete(scp);
		}
	}
	
	
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKaryoShipTaskTemp(String[] ids) throws Exception {
		for (String id : ids) {
			KaryoShipTaskTemp scp =  karyoShipTaskDao.get(KaryoShipTaskTemp.class, id);
			 karyoShipTaskDao.delete(scp);
		}
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(KaryoShipTask sc, Map jsonMap) throws Exception {
		if (sc != null) {
			karyoShipTaskDao.saveOrUpdate(sc);
		
			String jsonStr = "";
			jsonStr = (String)jsonMap.get("karyoShipTaskItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveKaryoShipTaskItem(sc, jsonStr);
			}
			jsonStr = (String)jsonMap.get("karyoShipTaskTemplate");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveKaryoShipTaskTemplate(sc, jsonStr);
			}
			jsonStr = (String)jsonMap.get("karyoShipTaskReagent");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveKaryoShipTaskReagent(sc, jsonStr);
			}
			jsonStr = (String)jsonMap.get("karyoShipTaskCos");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveKaryoShipTaskCos(sc, jsonStr);
			}
			jsonStr = (String)jsonMap.get("karyoShipTaskResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveKaryoShipTaskResult(sc, jsonStr);
			}
			List<KaryoShipTaskItem> snlist=karyoShipTaskDao.getItem(sc.getId());
			List<KaryoShipTaskReagent> rglist=karyoShipTaskDao.getReagent(sc.getId());
			 DecimalFormat df = new DecimalFormat("#.00");
			 Double nums = 0.00;
			if(snlist.size()>0){
				if(rglist.size()>0){
					for(KaryoShipTaskReagent sr:rglist){
						sr.setSampleNum(String.valueOf(snlist.size()));
						if(sr.getSingleDosage()!=null && 
								sr.getSampleNum()!=null){
							nums =Double.valueOf(df.format(sr.getSingleDosage()*snlist.size()));
							sr.setDosage(nums);
						}
					}
				}
			}
		}
   }
	/**
	 * 审批完成
	 */
	public void changeState(String applicationTypeActionId, String id)
			throws Exception {
		KaryoShipTask kt = karyoShipTaskDao.get(KaryoShipTask.class, id);
		kt.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		kt.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		Date date = new Date();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String stime = format.format(date);
		kt.setConfirmDate(stime);
		karyoShipTaskDao.update(kt);
		
		//完成后改变左侧表的状态
		List<KaryoShipTaskResult> list=karyoShipTaskDao
				.getKaryoShipTaskResultById(kt.getId());
		if(list.size()>0){
			for(KaryoShipTaskResult k:list){
				KaryoShipTaskTemp tt=commonDAO
						.get(KaryoShipTaskTemp.class, k.getTempId());
				if(tt!=null){
					tt.setState("2");
				}
			}
		}
		submitSample(id,null);
	}
	
	/**
	 * 根据ID删除
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKaryoShipTaskReagentOne(String ids) throws Exception {
		KaryoShipTaskReagent scp =  karyoShipTaskDao.get(KaryoShipTaskReagent.class, ids);
		karyoShipTaskDao.delete(scp);
	}
	/**
	 * 根据ID删除
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKaryoShipTaskTemplateOne(String ids) throws Exception {
		KaryoShipTaskTemplate scp =  karyoShipTaskDao.get(KaryoShipTaskTemplate.class, ids);
		karyoShipTaskDao.delete(scp);
	}
	/**
	 * 根据ID删除
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKaryoShipTaskCosOne(String ids) throws Exception {
		KaryoShipTaskCos scp =  karyoShipTaskDao.get(KaryoShipTaskCos.class, ids);
		karyoShipTaskDao.delete(scp);
	}

	
	// 提交样本
		@WriteOperLog
		@WriteExOperLog
		@Transactional(rollbackFor = Exception.class)
		public void submitSample(String id, String[] ids) throws Exception {
			// 获取主表信息
			KaryoShipTask sc = this.karyoShipTaskDao.get(KaryoShipTask.class, id);
			// 获取结果表样本信息

			List<KaryoShipTaskResult> list;
			if (ids == null)
				list = this.karyoShipTaskDao.setResultById(id);
			else
				list = this.karyoShipTaskDao.setResultByIds(ids);
			for (KaryoShipTaskResult scp : list) {
				if (scp != null) {
					if (scp.getIsrStandard() != null && (scp.getIsCommit() == null||"".equals(scp.getIsCommit()))) {
						String isOk = scp.getIsrStandard();
						if (isOk.equals("1")) {
							ProducerTaskTemp k = new ProducerTaskTemp();
							k.setCode(scp.getCode());
							k.setSampleCode(scp.getSampleCode());
							k.setProductId(scp.getProductId());
							k.setProductName(scp.getProductName());
							k.setSampleType(scp.getSampleType());
//							k.setChipNum(scp.getChipNum());
							k.setState("1");
							k.setAcceptDate(scp.getAcceptDate());
							k.setResultDate(scp.getShipDate());
							karyoShipTaskDao.saveOrUpdate(k);
							DateFormat format = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss");
							sampleStateService
									.saveSampleState(
											scp.getCode(),
											scp.getSampleCode(),
											scp.getProductId(),
											scp.getProductName(),
											"",
											DateUtil.format(sc.getCreateDate()),
											format.format(new Date()),
											"KaryoShipTask",
											"样本制片",
											(User) ServletActionContext
													.getRequest()
													.getSession()
													.getAttribute(
															SystemConstants.USER_SESSION_KEY),
											sc.getId(), "上机扫描",
											scp.getIsrStandard(), null, null, null,
											null, null, null, null, null);
							scp.setIsCommit("1");
							karyoShipTaskDao.saveOrUpdate(scp);
						} else {
							
							if(scp.getNextFlowId().equals("0038")){
								//提交不和合格的到报告终止
						    	EndReport cr = new EndReport();
								cr.setCode(scp.getCode());
								cr.setSampleCode(scp.getSampleCode());
								cr.setSampleType(scp.getSampleType());
								cr.setProductId(scp.getProductId());
								cr.setProductName(scp.getProductName());
								cr.setAdvice(scp.getNote());
								cr.setTaskType("样本制片");
								cr.setTaskId(scp.getId());
								cr.setTaskResultId(scp.getKaryoShipTask().getId());
								cr.setWaitDate(new Date());
						    	commonDAO.saveOrUpdate(cr);
							}else{
									// 不合格的到异常
									KaryoAbnormal ka = new KaryoAbnormal();
									ka.setCode(scp.getCode());
									ka.setSampleCode(scp.getSampleCode());
									ka.setProductId(scp.getProductId());
									ka.setProductName(scp.getProductName());
									ka.setSampleType(scp.getSampleType());
									ka.setTaskId(scp.getKaryoShipTask().getId());
									ka.setNote(scp.getNote());
									ka.setTaskName("样本制片");
									ka.setState("1");
									karyoShipTaskDao.saveOrUpdate(ka);
							}
							scp.setIsCommit("1");
						}
					}
				}
			}
		}
	}




