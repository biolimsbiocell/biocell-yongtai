package com.biolims.experiment.sequencing.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.sequencing.service.SequencingTaskService;

public class SequencingTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		SequencingTaskService mbService = (SequencingTaskService) ctx
				.getBean("sequencingTaskService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
