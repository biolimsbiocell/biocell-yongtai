package com.biolims.experiment.sequencing.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import com.biolims.common.constants.SystemConstants;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.sequencing.model.SequencingTask;
import com.biolims.experiment.sequencing.model.SequencingTaskCos;
import com.biolims.experiment.sequencing.model.SequencingTaskItem;
import com.biolims.experiment.sequencing.model.SequencingTaskReagent;
import com.biolims.experiment.sequencing.model.SequencingTaskTemp;
import com.biolims.experiment.sequencing.model.SequencingTaskTemplate;
import com.biolims.experiment.sequencing.model.SequencingTaskInfo;
import com.opensymphony.xwork2.ActionContext;


@Repository
@SuppressWarnings("unchecked")
public class SequencingTaskDao extends BaseHibernateDao {

	public Map<String, Object> findSequencingTaskTable(Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SequencingTask where 1=1";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from SequencingTask where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<SequencingTask> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public Map<String, Object> selectSequencingTaskTempTable(String[] codes, Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
			
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from SequencingTaskTemp where 1=1 and state='1'";
			String key = "";
			if(query!=null&&!"".equals(query)){
				key=map2Where(query);
			}
			if(codes!=null&&!codes.equals("")){
				key+=" and code in (:alist)";
			}
			String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
			if(!"all".equals(scopeId)){
				key+=" and scopeId='"+scopeId+"'";
			}
			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				List<SequencingTaskTemp> list=null;
				Long filterCount = (Long) getSession().createQuery(countHql).uniqueResult();
				String hql = "from SequencingTaskTemp  where 1=1 and state='1'";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					col=col.replace("-", ".");
					key+=" order by "+col+" "+sort;
				}
				if(codes!=null&&!codes.equals("")){
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).setParameterList("alist", codes).list();
				}else{
					list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
				}
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
				}
			return map;
		
			
	}

	public Map<String, Object> findSequencingTaskItemTable(String id,Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SequencingTaskItem where 1=1 and state='1' and sequencingTask.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from SequencingTaskItem  where 1=1 and state='1' and sequencingTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<SequencingTaskItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public List<SequencingTaskItem> showWellPlate(String id) {
		String hql="from SequencingTaskItem  where 1=1 and state='2' and  sequencingTask.id='"+id+"'";
		List<SequencingTaskItem> list = getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> findSequencingTaskItemAfTable(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SequencingTaskItem where 1=1 and state='2' and sequencingTask.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from SequencingTaskItem  where 1=1 and state='2' and sequencingTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<SequencingTaskItem> list =this.getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
		}

	public List<String> getCountGroup(String id) {

		String countHql = "select count(*) from SequencingTaskItem where 1=1 and state='2' and sequencingTask.id='"+id+"'";
		String key = "";
		List<String> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "select counts from SequencingTaskItem where 1=1 and state='2' and sequencingTask.id='"+id+"' group by counts";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
		
	}

	public List<SequencingTaskTemplate> showSequencingTaskStepsJson(String id,String code) {

		String countHql = "select count(*) from SequencingTaskTemplate where 1=1 and sequencingTask.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and orderNum ='"+code+"'";
		}else{
		}
		List<SequencingTaskTemplate> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from SequencingTaskTemplate where 1=1 and sequencingTask.id='"+id+"'";
			key+=" order by orderNum asc";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<SequencingTaskReagent> showSequencingTaskReagentJson(String id,String code) {
		String countHql = "select count(*) from SequencingTaskReagent where 1=1 and sequencingTask.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<SequencingTaskReagent> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from SequencingTaskReagent where 1=1 and sequencingTask.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<SequencingTaskCos> showSequencingTaskCosJson(String id,String code) {
		String countHql = "select count(*) from SequencingTaskCos where 1=1 and sequencingTask.id='"+id+"'";
		String key = "";
		if(code!=null&&!"".equals(code)){
			key+=" and itemId='"+code+"'";
		}else{
			key+=" and itemId='1'";
		}
		List<SequencingTaskCos> list=null;
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			String hql = "from SequencingTaskCos where 1=1 and sequencingTask.id='"+id+"'";
			list = getSession().createQuery(hql+key).list();
			}
		return list;
	}

	public List<SequencingTaskTemplate> delTemplateItem(String id) {
		List<SequencingTaskTemplate> list=new ArrayList<SequencingTaskTemplate>();
		String hql = "from SequencingTaskTemplate where 1=1 and sequencingTask.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<SequencingTaskReagent> delReagentItem(String id) {
		List<SequencingTaskReagent> list=new ArrayList<SequencingTaskReagent>();
		String hql = "from SequencingTaskReagent where 1=1 and sequencingTask.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
		}

	public List<SequencingTaskCos> delCosItem(String id) {
		List<SequencingTaskCos> list=new ArrayList<SequencingTaskCos>();
		String hql = "from SequencingTaskCos where 1=1 and sequencingTask.id='"+id+"'";
		String key="";
		list = getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> showSequencingTaskResultTableJson(String id,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SequencingTaskInfo where 1=1 and sequencingTask.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from SequencingTaskInfo  where 1=1 and sequencingTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<SequencingTaskInfo> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	}

	public List<Object> showWellList(String id) {
		
		String hql="select counts,count(*) from SequencingTaskItem where 1=1 and sequencingTask.id='"+id+"' group by counts";
		List<Object> list=getSession().createQuery(hql).list();
	
		return list;
	}

	public List<SequencingTaskItem> plateSample(String id, String counts) {
		String hql="from SequencingTaskItem where 1=1 and sequencingTask.id='"+id+"'";
		String key="";
		if(counts==null||counts.equals("")){
			key=" and counts is null";
		}else{
			key="and counts='"+counts+"'";
		}
		List<SequencingTaskItem> list=getSession().createQuery(hql+key).list();
		return list;
	}

	public Map<String, Object> plateSampleTable(String id, String counts,
			Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SequencingTaskItem where 1=1 and sequencingTask.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		if(counts==null||counts.equals("")){
			key+=" and counts is null";
		}else{
			key+="and counts='"+counts+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from SequencingTaskItem  where 1=1 and sequencingTask.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<SequencingTaskItem> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	
	
	}

	public List<SequencingTaskItem> findSequencingTaskItemByCode(String code) {
		String hql="from SequencingTaskItem where 1=1 and code='"+code+"'";
		List<SequencingTaskItem> list=new ArrayList<SequencingTaskItem>();
		list=getSession().createQuery(hql).list();
		return list;
	}
	
	public List<SequencingTaskInfo> selectAllResultListById(String code) {
		String hql = "from SequencingTaskInfo  where (submit is null or submit='') and sequencingTask.id='"
				+ code + "'";
		List<SequencingTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	public List<SequencingTaskInfo> selectResultListById(String id) {
		String hql = "from SequencingTaskInfo  where sequencingTask.id='"
				+ id + "'";
		List<SequencingTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<SequencingTaskInfo> selectAllResultListByIds(String[] codes) {
		String insql = "''";
		for (String code : codes) {
			insql += ",'" + code + "'";
		}
		String hql = "from SequencingTaskInfo t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<SequencingTaskInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

		public List<SequencingTaskItem> selectSequencingTaskItemList(String scId)
			throws Exception {
		String hql = "from SequencingTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sequencingTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SequencingTaskItem> list = new ArrayList<SequencingTaskItem>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}
	
		public Integer generateBlendCode(String id) {
			String hql="select max(blendCode) from SequencingTaskItem where 1=1 and sequencingTask.id='"+id+"'";
			Integer blendCode=(Integer) this.getSession().createQuery(hql).uniqueResult();
			return blendCode;
		}

		public SequencingTaskInfo getResultByCode(String code) {
			String hql=" from SequencingTaskInfo where 1=1 and code='"+code+"'";
			SequencingTaskInfo spi=(SequencingTaskInfo) this.getSession().createQuery(hql).uniqueResult();
			return spi;
		}

		public List<SequencingTaskInfo> findSequencingTaskInfoByFcCode(String fileName) {
			String hql=" from SequencingTaskInfo where 1=1 and fc_code='"+fileName+"'";
			List<SequencingTaskInfo> list = this.getSession().createQuery(hql).list();
			return list;
		}

		public List<SequencingTaskInfo> findSequencingTaskInfoByCode(String code) {
			String hql=" from SequencingTaskInfo where 1=1 and code='"+code+"'";
			List<SequencingTaskInfo> list = this.getSession().createQuery(hql).list();
			return list;
		}
	
}