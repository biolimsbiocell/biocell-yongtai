package com.biolims.experiment.sequencing.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.dic.model.DicType;
import com.biolims.experiment.sequencing.dao.SequencingTaskDao;
import com.biolims.experiment.sequencing.model.SequencingTask;
import com.biolims.experiment.sequencing.model.SequencingTaskAbnormal;
import com.biolims.experiment.sequencing.model.SequencingTaskCos;
import com.biolims.experiment.sequencing.model.SequencingTaskInfo;
import com.biolims.experiment.sequencing.model.SequencingTaskItem;
import com.biolims.experiment.sequencing.model.SequencingTaskReagent;
import com.biolims.experiment.sequencing.model.SequencingTaskTemp;
import com.biolims.experiment.sequencing.model.SequencingTaskTemplate;
import com.biolims.experiment.wk.model.WkTaskInfo;
import com.biolims.file.model.FileInfo;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.sample.dao.SampleReceiveDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.storage.main.service.StorageService;
import com.biolims.storage.out.service.StorageOutService;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.primers.model.MappingPrimersLibraryItem;
import com.biolims.system.primers.service.MappingPrimersLibraryService;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.CosItem;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.model.TemplateItem;
import com.biolims.system.template.service.TemplateService;
import com.biolims.util.JsonUtils;
import com.csvreader.CsvReader;
import com.opensymphony.xwork2.ActionContext;

@Service
@Transactional
public class SequencingTaskService {

	@Resource
	private SequencingTaskDao sequencingTaskDao;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private TemplateService templateService;
	@Resource
	private MappingPrimersLibraryService mappingPrimersLibraryService;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private StorageService storageService;
	@Resource
	private StorageOutService storageOutService;
	@Resource
	private SampleReceiveDao sampleReceiveDao;
	@Resource
	private DicSampleTypeDao dicSampleTypeDao;

	/**
	 * 
	 * @Title: get
	 * @Description:根据ID获取主表
	 * @author :
	 * @date
	 * @param id
	 * @return SequencingTask
	 * @throws
	 */
	public SequencingTask get(String id) {
		SequencingTask sequencingTask = commonDAO.get(SequencingTask.class, id);
		return sequencingTask;
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSequencingTaskItem(String delStr, String[] ids, User user,
			String sequencingTask_id) throws Exception {
		String delId = "";
		for (String id : ids) {
			SequencingTaskItem scp = sequencingTaskDao.get(
					SequencingTaskItem.class, id);
			if (scp.getId() != null) {
				SequencingTask pt = sequencingTaskDao.get(SequencingTask.class,
						scp.getSequencingTask().getId());
				pt.setSampleNum(pt.getSampleNum() - 1);
				// 改变左侧样本状态
				SequencingTaskTemp sequencingTaskTemp = this.commonDAO.get(
						SequencingTaskTemp.class, scp.getTempId());
				if (sequencingTaskTemp != null) {
					sequencingTaskTemp.setState("1");
					sequencingTaskDao.update(sequencingTaskTemp);
				}
				sequencingTaskDao.update(pt);
				sequencingTaskDao.delete(scp);
			}
			delId += scp.getSampleCode() + ",";
		}
		if (ids.length > 0) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setUserId(user.getId());
			li.setFileId(sequencingTask_id);
			li.setModifyContent(delStr + delId);
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 
	 * @Title: delSequencingTaskItemAf
	 * @Description: 重新排板
	 * @author :
	 * @date
	 * @param ids
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSequencingTaskItemAf(String[] ids) throws Exception {
		for (String id : ids) {
			SequencingTaskItem scp = sequencingTaskDao.get(
					SequencingTaskItem.class, id);
			if (scp.getId() != null) {
				scp.setState("1");
				sequencingTaskDao.saveOrUpdate(scp);
			}

		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSequencingTaskResult(String[] ids) throws Exception {
		for (String id : ids) {
			SequencingTaskInfo scp = sequencingTaskDao.get(
					SequencingTaskInfo.class, id);
			if ((scp.getSubmit() == null || (scp.getSubmit() != null && scp
					.getSubmit().equals(""))))
				sequencingTaskDao.delete(scp);
		}
	}

	/**
	 * 删除试剂明细
	 * 
	 * @param id2
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSequencingTaskReagent(String id) throws Exception {
		if (id != null && !"".equals(id)) {
			SequencingTaskReagent scp = sequencingTaskDao.get(
					SequencingTaskReagent.class, id);
			sequencingTaskDao.delete(scp);
		}
	}

	/**
	 * 删除设备明细
	 * 
	 * @param id
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSequencingTaskCos(String id) throws Exception {
		if (id != null && !"".equals(id)) {
			SequencingTaskCos scp = sequencingTaskDao.get(
					SequencingTaskCos.class, id);
			sequencingTaskDao.delete(scp);
		}
	}

	/**
	 * 
	 * @Title: saveSequencingTaskTemplate
	 * @Description: 保存模板
	 * @author :
	 * @date
	 * @param sc
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveSequencingTaskTemplate(SequencingTask sc) {
		List<SequencingTaskTemplate> tlist2 = sequencingTaskDao
				.delTemplateItem(sc.getId());
		List<SequencingTaskReagent> rlist2 = sequencingTaskDao
				.delReagentItem(sc.getId());
		List<SequencingTaskCos> clist2 = sequencingTaskDao.delCosItem(sc
				.getId());
		commonDAO.deleteAll(tlist2);
		commonDAO.deleteAll(rlist2);
		commonDAO.deleteAll(clist2);
		List<TemplateItem> tlist = templateService.getTemplateItem(sc
				.getTemplate().getId(), null);
		List<ReagentItem> rlist = templateService.getReagentItem(sc
				.getTemplate().getId(), null);
		List<CosItem> clist = templateService.getCosItem(sc.getTemplate()
				.getId(), null);
		for (TemplateItem t : tlist) {
			SequencingTaskTemplate ptt = new SequencingTaskTemplate();
			ptt.setSequencingTask(sc);
			ptt.setOrderNum(t.getOrderNum());
			ptt.setName(t.getName());
			ptt.setNote(t.getNote());
			ptt.setContent(t.getContent());
			sequencingTaskDao.saveOrUpdate(ptt);
		}
		for (ReagentItem t : rlist) {
			SequencingTaskReagent ptr = new SequencingTaskReagent();
			ptr.setSequencingTask(sc);
			ptr.setCode(t.getCode());
			ptr.setName(t.getName());
			ptr.setItemId(t.getItemId());
			ptr.setName(t.getName());
			ptr.setNote(t.getNote());
			sequencingTaskDao.saveOrUpdate(ptr);
		}
		for (CosItem t : clist) {
			SequencingTaskCos ptc = new SequencingTaskCos();
			ptc.setSequencingTask(sc);
			ptc.setItemId(t.getItemId());
			ptc.setName(t.getName());
			ptc.setNote(t.getNote());
			ptc.setType(t.getType());
			sequencingTaskDao.saveOrUpdate(ptc);
		}
	}

	/**
	 * 
	 * @Title: changeState
	 * @Description:改变状态
	 * @author :
	 * @date
	 * @param applicationTypeActionId
	 * @param id
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String id)
			throws Exception {
		SequencingTask sct = sequencingTaskDao.get(SequencingTask.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		sequencingTaskDao.update(sct);
		if (sct.getTemplate() != null) {
			String iid = sct.getTemplate().getId();
			if (iid != null) {
				templateService.setCosIsUsedOver(iid);
			}
		}

		submitSample(id, null);

	}

	/**
	 * 
	 * @Title: submitSample
	 * @Description: 提交样本
	 * @author :
	 * @date
	 * @param id
	 * @param ids
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitSample(String id, String[] ids) throws Exception {
		// 获取主表信息
		SequencingTask sc = this.sequencingTaskDao
				.get(SequencingTask.class, id);
		// 获取结果表样本信息
		List<SequencingTaskInfo> list;
		if (ids == null)
			list = this.sequencingTaskDao.selectAllResultListById(id);
		else
			list = this.sequencingTaskDao.selectAllResultListByIds(ids);

		for (SequencingTaskInfo scp : list) {
			if (scp != null
					&& scp.getResult() != null
					&& (scp.getSubmit() == null || (scp.getSubmit() != null && scp
							.getSubmit().equals("")))) {
				if (scp.getResult().equals("1")) {// 合格
					String next = scp.getNextFlowId();
					if(next!=null&&!next.equals("")){
						
					
					if (next.equals("0009")) {// 样本入库
						SampleInItemTemp st = new SampleInItemTemp();
						st.setSampleState("1");
						st.setScopeId(sc.getScopeId());
						st.setScopeName(sc.getScopeName());
						st.setProductId(scp.getProductId());
						st.setProductName(scp.getProductName());
						st.setCode(scp.getCode());
						st.setSampleCode(scp.getSampleCode());
						st.setConcentration(scp.getConcentration());
						st.setVolume(scp.getVolume());
						DicSampleType t = commonDAO.get(DicSampleType.class, scp
								.getDicSampleType().getId());
						st.setSampleType(t.getName());
						st.setDicSampleType(scp.getDicSampleType());
						st.setPatientName(scp.getPatientName());
						st.setSampleTypeId(scp.getDicSampleType().getId());
						st.setInfoFrom("SequencingTaskInfo");
						st.setState("1");
						sequencingTaskDao.saveOrUpdate(st);
					} else if (next.equals("0012")) {// 暂停
					} else if (next.equals("0013")) {// 终止
					} else if (next.equals("0034")) {// 下机质控
					} else {
						scp.setState("1");
						scp.setScopeId(sc.getScopeId());
						scp.setScopeName(sc.getScopeName());
						// 得到下一步流向的相关表单
						List<NextFlow> list_nextFlow = nextFlowDao
								.seletNextFlowById(next);
						for (NextFlow n : list_nextFlow) {
							Object o = Class.forName(
									n.getApplicationTypeTable().getClassPath())
									.newInstance();
							sampleInputService.copy(o, scp);
						}
					}}
				} else {// 不合格
					SequencingTaskAbnormal pa = new SequencingTaskAbnormal();// 样本异常
					sampleInputService.copy(pa, scp);
					pa.setOrderId(sc.getId());
					pa.setState("1");
					pa.setScopeId(sc.getScopeId());
					pa.setScopeName(sc.getScopeName());
					commonDAO.saveOrUpdate(pa);
				}
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				sampleStateService
						.saveSampleState(
								scp.getCode(),
								scp.getSampleCode(),
								scp.getProductId(),
								scp.getProductName(),
								"",
								sc.getCreateDate(),
								format.format(new Date()),
								"SequencingTask",
								"上机测序",
								(User) ServletActionContext
										.getRequest()
										.getSession()
										.getAttribute(
												SystemConstants.USER_SESSION_KEY),
								id, scp.getNextFlow(), scp.getResult(), null,
								null, null, null, null, null, null, null);

				scp.setSubmit("1");
				sequencingTaskDao.saveOrUpdate(scp);
			}
		}
	}

	/**
	 * 
	 * @Title: findSequencingTaskTable
	 * @Description: 展示主表
	 * @author :
	 * @date
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> findSequencingTaskTable(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		return sequencingTaskDao.findSequencingTaskTable(start, length, query,
				col, sort);
	}

	/**
	 * 
	 * @Title: selectSequencingTaskTempTable
	 * @Description: 展示临时表
	 * @author :
	 * @date
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> selectSequencingTaskTempTable(String[] codes,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return sequencingTaskDao.selectSequencingTaskTempTable(codes, start,
				length, query, col, sort);
	}

	/**
	 * 
	 * @Title: saveAllot
	 * @Description: 保存任务分配
	 * @author :
	 * @date
	 * @param main
	 * @param tempId
	 * @param userId
	 * @param templateId
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String saveAllot(String main, String[] tempId, String userId,
			String templateId, String logInfo) throws Exception {
		String id = "";
		if (main != null) {
			String mainJson = "[" + main + "]";
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					mainJson, List.class);
			SequencingTask pt = new SequencingTask();
			pt = (SequencingTask) commonDAO.Map2Bean(list.get(0), pt);
			String name = pt.getName();
			Integer sampleNum = pt.getSampleNum();
			Integer maxNum = pt.getMaxNum();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if ((pt.getId() != null && pt.getId().equals(""))
					|| pt.getId().equals("NEW")) {
				String modelName = "SequencingTask";
				String markCode = "SJCX";
				id = codingRuleService.genTransID(modelName, markCode);
				pt.setId(id);
				pt.setCreateDate(sdf.format(new Date()));
				pt.setScopeId((String) ActionContext.getContext().getSession()
						.get("scopeId"));
				pt.setScopeName((String) ActionContext.getContext()
						.getSession().get("scopeName"));

			} else {
				id = pt.getId();
				pt = commonDAO.get(SequencingTask.class, id);
				pt.setName(name);
				pt.setSampleNum(sampleNum);
				pt.setMaxNum(maxNum);
			}
			Template t = commonDAO.get(Template.class, templateId);
			t.setId(templateId);
			pt.setTemplate(t);
			pt.setTestUserOneId(userId);
			String [] users=userId.split(",");
			String userName="";
			for(int i=0;i<users.length;i++){
				if(i==users.length-1){
					User user=commonDAO.get(User.class, users[i]);
					userName+=user.getName();
				}else{
					User user=commonDAO.get(User.class, users[i]);
					userName+=user.getName()+",";
				}
			}
			pt.setTestUserOneName(userName);
			sequencingTaskDao.saveOrUpdate(pt);
			if (pt.getTemplate() != null) {
				saveSequencingTaskTemplate(pt);
			}
			if (tempId != null) {
				if (pt.getTemplate() != null) {
						for (String temp : tempId) {
							SequencingTaskTemp ptt = sequencingTaskDao.get(
									SequencingTaskTemp.class, temp);
							if (t.getIsSeparate().equals("1")) {
								List<MappingPrimersLibraryItem> listMPI = mappingPrimersLibraryService
										.getMappingPrimersLibraryItem(ptt
												.getProductId());
								for (MappingPrimersLibraryItem mpi : listMPI) {
									SequencingTaskItem pti = new SequencingTaskItem();
									pti.setSampleCode(ptt.getSampleCode());
									pti.setProductId(ptt.getProductId());
									pti.setProductName(ptt.getProductName());
									pti.setDicSampleTypeId(t.getDicSampleType() != null
											&& !"".equals(t.getDicSampleType()) ? t
											.getDicSampleType().getId() : t
											.getDicSampleTypeId());
									pti.setDicSampleTypeName(t
											.getDicSampleType() != null
											&& !"".equals(t.getDicSampleType()) ? t
											.getDicSampleType().getName() : t
											.getDicSampleTypeName());
									pti.setProductName(ptt.getProductName());
									pti.setProductNum(t.getProductNum() != null
											&& !"".equals(t.getProductNum()) ? t
											.getProductNum() : t
											.getProductNum1());
									pti.setCode(ptt.getCode());
									pti.setConcentration(ptt.getConcentration());
									pti.setVolume(ptt.getVolume());
									pti.setSumTotal(ptt.getSumTotal());
									pti.setLane(ptt.getLane());
									if (t.getStorageContainer() != null) {
										pti.setState("1");
									}else{
										pti.setState("2");
									}
									ptt.setState("2");
									pti.setTempId(temp);
									pti.setSequencingTask(pt);
									pti.setChromosomalLocation(mpi
											.getChromosomalLocation());
									sequencingTaskDao.saveOrUpdate(pti);
								}
							} else {
								SequencingTaskItem pti = new SequencingTaskItem();
								pti.setSampleCode(ptt.getSampleCode());
								pti.setProductId(ptt.getProductId());
								pti.setProductName(ptt.getProductName());
								pti.setDicSampleTypeId(t.getDicSampleType() != null
										&& !"".equals(t.getDicSampleType()) ? t
										.getDicSampleType().getId() : t
										.getDicSampleTypeId());
								pti.setProductNum(t.getProductNum() != null
										&& !"".equals(t.getProductNum()) ? t
										.getProductNum() : t.getProductNum1());
								pti.setDicSampleTypeName(t.getDicSampleType() != null
										&& !"".equals(t.getDicSampleType()) ? t
										.getDicSampleType().getName() : t
										.getDicSampleTypeName());
								pti.setCode(ptt.getCode());
								pti.setConcentration(ptt.getConcentration());
								pti.setVolume(ptt.getVolume());
								pti.setSumTotal(ptt.getSumTotal());
								pti.setLane(ptt.getLane());
								if (t.getStorageContainer() != null) {
									pti.setState("1");
								}else{
									pti.setState("2");
								}
								ptt.setState("2");
								pti.setTempId(temp);
								pti.setSequencingTask(pt);
								sequencingTaskDao.saveOrUpdate(pti);
							}
							sequencingTaskDao.saveOrUpdate(ptt);
						}
				}

			}

			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(pt.getId());
				li.setModifyContent(logInfo);
				commonDAO.saveOrUpdate(li);
			}
		}
		return id;

	}

	/**
	 * 
	 * @Title: findSequencingTaskItemTable
	 * @Description:展示未排版样本
	 * @author :
	 * @date
	 * @param id
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> findSequencingTaskItemTable(String id,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return sequencingTaskDao.findSequencingTaskItemTable(id, start, length,
				query, col, sort);
	}

	/**
	 * 
	 * @Title: saveMakeUp
	 * @Description: 保存排板数据
	 * @author :
	 * @date
	 * @param id
	 * @param item
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveMakeUp(String id, String item, String logInfo)
			throws Exception {
		List<SequencingTaskItem> saveItems = new ArrayList<SequencingTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(item,
				List.class);
		SequencingTask pt = commonDAO.get(SequencingTask.class, id);
		SequencingTaskItem scp = new SequencingTaskItem();
		for (Map<String, Object> map : list) {
			// 将map信息读入实体类
			scp = (SequencingTaskItem) sequencingTaskDao.Map2Bean(map, scp);
			SequencingTaskItem pti = commonDAO.get(SequencingTaskItem.class,
					scp.getId());
			pti.setDicSampleTypeId(scp.getDicSampleTypeId());
			pti.setDicSampleTypeName(scp.getDicSampleTypeName());
			pti.setProductNum(scp.getProductNum());
			pti.setLane(scp.getLane());
			pti.setRunId(scp.getRunId());
			pti.setFcCode(scp.getFcCode());
			pti.setBlendCode(scp.getBlendCode());
			pti.setColor(scp.getColor());
			scp.setSequencingTask(pt);
			saveItems.add(pti);
		}
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		sequencingTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 
	 * @Title: showWellPlate
	 * @Description: 展示孔板
	 * @author :
	 * @date
	 * @param id
	 * @return
	 * @throws Exception
	 *             List<SequencingTaskItem>
	 * @throws
	 */
	public List<SequencingTaskItem> showWellPlate(String id) throws Exception {
		List<SequencingTaskItem> list = sequencingTaskDao.showWellPlate(id);
		return list;
	}

	/**
	 * 
	 * @Title: findSequencingTaskItemAfTable
	 * @Description: 展示排板后
	 * @author :
	 * @date
	 * @param scId
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> findSequencingTaskItemAfTable(String scId,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return sequencingTaskDao.findSequencingTaskItemAfTable(scId, start,
				length, query, col, sort);
	}

	/**
	 * 
	 * @Title: plateLayout
	 * @Description: 排板
	 * @author :
	 * @date
	 * @param data
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void plateLayout(String[] data) {
		for (String da : data) {
			String[] d = da.split(",");
			SequencingTaskItem pti = commonDAO.get(SequencingTaskItem.class,
					d[0]);
			pti.setPosId(d[1]);
			pti.setCounts(d[2]);
			pti.setState("2");
			sequencingTaskDao.saveOrUpdate(pti);
		}
	}

	/**
	 * 
	 * @Title: showSequencingTaskStepsJson
	 * @Description: 展示实验步骤
	 * @author :
	 * @date
	 * @param id
	 * @param code
	 * @return Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> showSequencingTaskStepsJson(String id,
			String code) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<SequencingTaskTemplate> pttList = sequencingTaskDao
				.showSequencingTaskStepsJson(id, code);
		List<SequencingTaskReagent> ptrList = sequencingTaskDao
				.showSequencingTaskReagentJson(id, code);
		List<SequencingTaskCos> ptcList = sequencingTaskDao
				.showSequencingTaskCosJson(id, code);
		List<Object> plate = sequencingTaskDao.showWellList(id);
		map.put("template", pttList);
		map.put("reagent", ptrList);
		map.put("cos", ptcList);
		map.put("plate", plate);
		return map;
	}

	/**
	 * 
	 * @Title: showSequencingTaskResultTableJson
	 * @Description: 展示结果
	 * @author :
	 * @date
	 * @param id
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> showSequencingTaskResultTableJson(String id,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return sequencingTaskDao.showSequencingTaskResultTableJson(id, start,
				length, query, col, sort);
	}

	/**
	 * 
	 * @Title: plateSample
	 * @Description: 孔板样本
	 * @author :
	 * @date
	 * @param id
	 * @param counts
	 * @return Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> plateSample(String id, String counts) {
		SequencingTask pt = commonDAO.get(SequencingTask.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		List<SequencingTaskItem> list = sequencingTaskDao.plateSample(id,
				counts);
		Integer row = null;
		Integer col = null;
		if (pt.getTemplate().getStorageContainer() != null) {
			row = pt.getTemplate().getStorageContainer().getRowNum();
			col = pt.getTemplate().getStorageContainer().getColNum();
		}
		map.put("list", list);
		map.put("rowNum", row);
		map.put("colNum", col);
		return map;
	}

	/**
	 * 
	 * @Title: getCsvContent
	 * @Description: 上传结果
	 * @author :
	 * @date
	 * @param id
	 * @param fileId
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void getCsvContent(String id, String fileId) throws Exception {

		FileInfo fileInfo = sequencingTaskDao.get(FileInfo.class, fileId);
		String filepath = fileInfo.getFilePath();
		File a = new File(filepath);
		if (a.isFile()) {
			SequencingTask pt = sequencingTaskDao.get(SequencingTask.class, id);
			InputStream is = new FileInputStream(filepath);
			if (is != null) {
				CsvReader reader = new CsvReader(is, Charset.forName("GBK"));
				reader.readHeaders();// 去除表头
				while (reader.readRecord()) {
					String code = reader.get(1);
					if (code != null && !"".equals(code)) {
						List<SequencingTaskInfo> listPTI = sequencingTaskDao
								.findSequencingTaskInfoByCode(code);
						if (listPTI.size() > 0) {
							SequencingTaskInfo spi = listPTI.get(0);
							spi.setFcLocation(reader.get(5));
							spi.setLane(reader.get(6));
							spi.setRunId(reader.get(7));
							spi.setMachineCode(reader.get(8));
							sequencingTaskDao.saveOrUpdate(spi);
						}
					}
					/*String code = reader.get(0);
					if (code != null && !"".equals(code)) {
						List<SequencingTaskItem> listPTI = sequencingTaskDao
								.findSequencingTaskItemByCode(code);
						if (listPTI.size() > 0) {
							SequencingTaskInfo spi = new SequencingTaskInfo();
							spi.setSampleCode(listPTI.get(0).getSampleCode());
							spi.setCode(reader.get(0));
							spi.setConcentration(Double.parseDouble(reader
									.get(1)));
							spi.setVolume(Double.parseDouble(reader.get(2)));
							spi.setResult(reader.get(3).equals("合格") ? "1"
									: "0");
							spi.setSequencingTask(pt);
							spi.setProductId(listPTI.get(0).getProductId());
							spi.setProductName(listPTI.get(0).getProductName());
							sequencingTaskDao.saveOrUpdate(spi);
						}
					}*/
				}
			}
		}
	}

	/**
	 * 
	 * @Title: saveSteps
	 * @Description: 保存实验步骤
	 * @author :
	 * @date
	 * @param id
	 * @param templateJson
	 * @param reagentJson
	 * @param cosJson
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSteps(String id, String templateJson, String reagentJson,
			String cosJson, String logInfo) {
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		if (templateJson != null && !templateJson.equals("{}")
				&& !templateJson.equals("")) {
			saveTemplate(id, templateJson);
		}
		if (reagentJson != null && !reagentJson.equals("{}")
				&& !reagentJson.equals("")) {
			saveReagent(id, reagentJson);
		}
		if (cosJson != null && !cosJson.equals("{}") && !cosJson.equals("")) {
			saveCos(id, cosJson);
		}
	}

	/**
	 * 
	 * @Title: saveTemplate
	 * @Description: 保存模板明细
	 * @author :
	 * @date
	 * @param id
	 * @param templateJson
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveTemplate(String id, String templateJson) {
		JSONArray array = JSONArray.fromObject("[" + templateJson + "]");
		JSONObject object = JSONObject.fromObject(array.get(0));
		String templateId = (String) object.get("id");
		if (templateId != null && !"".equals(templateId)) {
			SequencingTaskTemplate ptt = sequencingTaskDao.get(
					SequencingTaskTemplate.class, templateId);
			String contentData = object.get("contentData").toString();
			if (contentData != null && !"".equals(contentData)) {
				ptt.setContentData(contentData);
			}
			String startTime = (String) object.get("startTime");
			if (startTime != null && !"".equals(startTime)) {
				ptt.setStartTime(startTime);
			}
			String endTime = (String) object.get("endTime");
			if (endTime != null && !"".equals(endTime)) {
				ptt.setEndTime(endTime);
			}
			sequencingTaskDao.saveOrUpdate(ptt);
		}
	}

	/**
	 * 
	 * @Title: saveReagent
	 * @Description: 保存试剂
	 * @author :
	 * @date
	 * @param id
	 * @param reagentJson
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveReagent(String id, String reagentJson) {

		JSONArray array = JSONArray.fromObject(reagentJson);
		for (int i = 0; i < array.size(); i++) {
			JSONObject object = JSONObject.fromObject(array.get(i));
			String reagentId = (String) object.get("id");
			SequencingTaskReagent ptr = null;
			if (reagentId != null && !"".equals(reagentId)) {
				ptr = commonDAO.get(SequencingTaskReagent.class, reagentId);
				String batch = (String) object.get("batch");
				if (batch != null && !"".equals(batch)) {
					ptr.setBatch(batch);
				}
				String sn = (String) object.get("sn");
				if (sn != null && !"".equals(sn)) {
					ptr.setSn(sn);
				}

			} else {
				ptr = new SequencingTaskReagent();
				String code = (String) object.get("code");
				if (code != null && !"".equals(code)) {
					ptr.setCode(code);
					String name = (String) object.get("name");
					if (name != null && !"".equals(name)) {
						ptr.setName(name);
					}
					String itemid = (String) object.get("itemId");
					if (itemid != null && !"".equals(itemid)) {
						ptr.setItemId(itemid);
					}
					String batch = (String) object.get("batch");
					if (batch != null && !"".equals(batch)) {
						ptr.setBatch(batch);
					}
					String sn = (String) object.get("sn");
					if (sn != null && !"".equals(sn)) {
						ptr.setSn(sn);
					}
					ptr.setSequencingTask(commonDAO.get(SequencingTask.class,
							id));
				}

			}
			sequencingTaskDao.saveOrUpdate(ptr);
		}

	}

	/**
	 * 
	 * @Title: saveCos
	 * @Description: 保存设备
	 * @author :
	 * @date
	 * @param id
	 * @param cosJson
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveCos(String id, String cosJson) {

		JSONArray array = JSONArray.fromObject(cosJson);
		for (int j = 0; j < array.size(); j++) {
			JSONObject object = JSONObject.fromObject(array.get(j));
			String cosId = (String) object.get("id");
			SequencingTaskCos ptc = null;
			if (cosId != null && !"".equals(cosId)) {
				ptc = commonDAO.get(SequencingTaskCos.class, cosId);
				String code = (String) object.get("code");
				if (code != null && !"".equals(code)) {
					ptc.setCode(code);
				}
				String name = (String) object.get("name");
				if (name != null && !"".equals(name)) {
					ptc.setName(name);
				}
			} else {
				ptc = new SequencingTaskCos();
				String typeId = (String) object.get("typeId");
				if (typeId != null && !"".equals(typeId)) {
					DicType dt = commonDAO.get(DicType.class, typeId);
					ptc.setType(dt);
					String code = (String) object.get("code");
					if (code != null && !"".equals(code)) {
						ptc.setCode(code);
					}
					String name = (String) object.get("name");
					if (name != null && !"".equals(name)) {
						ptc.setName(name);
					}
					String itemid = (String) object.get("itemId");
					if (itemid != null && !"".equals(itemid)) {
						ptc.setItemId(itemid);
					}
					ptc.setSequencingTask(commonDAO.get(SequencingTask.class,
							id));
				}
			}
			sequencingTaskDao.saveOrUpdate(ptc);
		}

	}

	/**
	 * 
	 * @Title: plateSampleTable
	 * @Description: 孔板样本列表
	 * @author :
	 * @date
	 * @param id
	 * @param counts
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return
	 * @throws Exception
	 *             Map<String,Object>
	 * @throws
	 */
	public Map<String, Object> plateSampleTable(String id, String counts,
			Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return sequencingTaskDao.plateSampleTable(id, counts, start, length,
				query, col, sort);
	}

	/**
	 * 
	 * @Title: bringResult
	 * @Description: 生成结果
	 * @author :
	 * @date
	 * @param id
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void bringResult(String id) throws Exception {

		List<SequencingTaskItem> list = sequencingTaskDao.showWellPlate(id);
		Map<String, Object> map = new HashMap<String, Object>();
		List<SequencingTaskInfo> spiList = sequencingTaskDao
				.selectResultListById(id);
		for (SequencingTaskItem pti : list) {
			boolean b = true;
			for (SequencingTaskInfo spi : spiList) {
				if (spi.getSampleCode().indexOf(pti.getSampleCode()) > -1) {
					b = false;
				}
			}
			if (b) {
				SequencingTaskInfo scp = new SequencingTaskInfo();
				scp.setSampleCode(pti.getSampleCode());
				scp.setProductId(pti.getProductId());
				scp.setLane(pti.getLane());
				scp.setFcCode(pti.getFcCode());
				scp.setRunId(pti.getRunId());
				scp.setProductName(pti.getProductName());
				scp.setSequencingTask(pti.getSequencingTask());
				scp.setResult("1");
				scp.setCode(pti.getCode());
				sequencingTaskDao.saveOrUpdate(scp);
			}
		}

	}

	/**
	 * 
	 * @Title: saveResult
	 * @Description: 保存结果
	 * @author :
	 * @date
	 * @param id
	 * @param dataJson
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveResult(String id, String dataJson, String logInfo,String confirmUser)
			throws Exception {

		List<SequencingTaskInfo> saveItems = new ArrayList<SequencingTaskInfo>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(dataJson,
				List.class);
		SequencingTask pt = commonDAO.get(SequencingTask.class, id);
		for (Map<String, Object> map : list) {
			SequencingTaskInfo scp = new SequencingTaskInfo();
			// 将map信息读入实体类
			scp = (SequencingTaskInfo) sequencingTaskDao.Map2Bean(map, scp);
			scp.setSequencingTask(pt);
			saveItems.add(scp);
		}
		if(confirmUser!=null&&!"".equals(confirmUser)){
			User confirm=commonDAO.get(User.class, confirmUser);
			pt.setConfirmUser(confirm);
			sequencingTaskDao.saveOrUpdate(pt);
		}
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		sequencingTaskDao.saveOrUpdateAll(saveItems);

	}

	public List<SequencingTaskItem> findSequencingTaskItemList(String scId)
			throws Exception {
		List<SequencingTaskItem> list = sequencingTaskDao
				.selectSequencingTaskItemList(scId);
		return list;
	}

	public Integer generateBlendCode(String id) {
		return sequencingTaskDao.generateBlendCode(id);
	}

	public SequencingTaskInfo getInfoById(String id) {
		return sequencingTaskDao.get(SequencingTaskInfo.class, id);
	}
}
