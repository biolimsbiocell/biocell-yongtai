package com.biolims.experiment.fish.custom;


import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.fish.service.FishSjTaskService;
import com.biolims.experiment.plasma.service.BloodSplitService;

public class FishSjTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		FishSjTaskService mbService = (FishSjTaskService) ctx.getBean("fishSjTaskService");
		mbService.chengeState(applicationTypeActionId, contentId);
		return "";
	}
}
