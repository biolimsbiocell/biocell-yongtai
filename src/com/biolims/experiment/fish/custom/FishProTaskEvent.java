package com.biolims.experiment.fish.custom;


import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.fish.service.FishProTaskService;

public class FishProTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		FishProTaskService mbService = (FishProTaskService) ctx.getBean("fishProTaskService");
		mbService.chengeState(applicationTypeActionId, contentId);

		return "";
	}
}
