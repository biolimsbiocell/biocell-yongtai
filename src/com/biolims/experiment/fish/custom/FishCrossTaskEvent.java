package com.biolims.experiment.fish.custom;


import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.fish.service.FishCrossTaskService;

public class FishCrossTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		FishCrossTaskService mbService = (FishCrossTaskService) ctx.getBean("fishCrossTaskService");
		mbService.chengeState(applicationTypeActionId, contentId);

		return "";
	}
}
