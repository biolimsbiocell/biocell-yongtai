package com.biolims.experiment.fish.second.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.fish.second.model.FishSecondInstance;
import com.biolims.sample.model.SampleOrder;
@Repository
@SuppressWarnings("unchecked")
public class FishSecondDao extends BaseHibernateDao {
	public Map<String, Object> selectFishSecondInstanceList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from FishSecondInstance where 1=1 ";
		if (mapForQuery.size()>0){
			key = map2where(mapForQuery);
		}else{
			key = " and state='1'";
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<FishSecondInstance> list = new ArrayList<FishSecondInstance>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	
/**
 * 根据样本编号查询
 * @param codes
 * @return
 */
public List<FishSecondInstance> setSecondInstanceByIds(String[] codes) {
	String insql = "''";
	for (String code : codes) {
		insql += ",'" + code + "'";
	}
	String hql = "from FishSecondInstance t where isCommit is null and id in ("
			+ insql + ")";
	List<FishSecondInstance> list = this.getSession().createQuery(hql).list();
	return list;
   }	
/**
 * 根据原始样本编号查询订单
 * @param codes
 * @return
 */
	public List<SampleOrder> setSampleOrderById(String codes) {
		String hql = " from  SampleOrder where 1=1 and id = '" + codes + "'";
		return this.getSession()
				.createQuery( hql).list();
	}
	
}