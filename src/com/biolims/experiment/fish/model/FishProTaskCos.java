package com.biolims.experiment.fish.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.experiment.plasma.model.PlasmaTask;

/**
 * @Title: Model
 * @Description: 设备明细
 * @author lims-platform
 * @date 2016-03-25 10:16:48
 * @version V1.0
 * 
 */
@Entity
@Table(name = "FISH_PRO_TASK_COS")
@SuppressWarnings("serial")
public class FishProTaskCos extends EntityDao<FishProTaskCos> implements
		java.io.Serializable {
	/** 设备id */
	private String id;
	/** 设备编号 */
	private String code;
	/** 设备名称 */
	private String name;

	/**是否通过检验*/
	private String isGood;
	// 关联步骤的id
	private String itemId;

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	// 温度
	private Double temperature;
	// 转速
	private String speed;
	// 时间
	private Double time;
	// 备注
	private String note;

	public Double getTemperature() {
		return temperature;
	}

	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}

	public String getSpeed() {
		return speed;
	}

	public void setSpeed(String speed) {
		this.speed = speed;
	}

	public Double getTime() {
		return time;
	}

	public void setTime(Double time) {
		this.time = time;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	/** 相关主表 */
	private FishProTask fishProTask;
	/**
	 * 方法: 取得FishProTask
	 * 
	 * @return: FishProTask 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "FISH_PRO_TASK")
	public FishProTask getFishProTask() {
		return this.fishProTask;
	}
	
	/**
	 * 方法: 设置FishProTask
	 * 
	 * @param: FishProTask 相关主表
	 */
	public void setFishProTask(FishProTask fishProTask) {
		this.fishProTask = fishProTask;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 设备id
	 */
	/*
	 * Cos id
	 */
	private String tCos;

	public String gettCos() {
		return tCos;
	}

	public void settCos(String tCos) {
		this.tCos = tCos;
	}

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 设备id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 设备编号
	 */
	@Column(name = "CODE", length = 50)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 设备编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 设备名称
	 */
	@Column(name = "NAME", length = 50)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 设备名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 是否通过检验
	 */
	@Column(name = "IS_GOOD", length = 50)
	public String getIsGood() {
		return this.isGood;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 是否通过检验
	 */
	public void setIsGood(String isGood) {
		this.isGood = isGood;
	}
}