package com.biolims.experiment.fish.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 分析明细
 * @author lims-platform
 * @date 2016-06-03 19:50:08
 * @version V1.0   
 *
 */
@Entity
@Table(name = "FISH_SJ_TASK_RESULT")
@SuppressWarnings("serial")
public class FishSjTaskResult extends EntityDao<FishSjTaskResult> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**样本编号*/
	private String code;
	/**原始样本编号*/
	private String sampleCode;
	/**玻片编号*/
	private String slideCode;
	/**18*/
	private String f18;
	/**x*/
	private String x;
	/**y*/
	private String y;
	/**13*/
	private String f13;
	/**21*/
	private String f21;
	/**其他位点*/
	private String otherSit;
	/**是否合格*/
	private String isGood;
	/**是否提交*/
	private String isCommit;
	/**备注*/
	private String note;
	/**状态*/
	private String state;
	/**相关主表*/
	private FishSjTask fishSjTask;
	/** 检测项目 */
	private String productId;
	/** 检测项目 */
	private String productName;
	/**样本类型*/
	private String sampleType;
	/**细胞计数*/
	private String cellsNum;
	/**下一步流向*/
	private String nextFlow;
	/**下一步流向ID*/
	private String nextFlowId;
	
	
	@Column(name ="CELLS_NUM", length = 50)
	public String getCellsNum() {
		return cellsNum;
	}
	public void setCellsNum(String cellsNum) {
		this.cellsNum = cellsNum;
	}
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本编号
	 */
	@Column(name ="CODE", length = 50)
	public String getCode(){
		return this.code;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本编号
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 *方法: 取得String
	 *@return: String  原始样本编号
	 */
	@Column(name ="SAMPLE_CODE", length = 50)
	public String getSampleCode(){
		return this.sampleCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  原始样本编号
	 */
	public void setSampleCode(String sampleCode){
		this.sampleCode = sampleCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  玻片编号
	 */
	@Column(name ="SLIDE_CODE", length = 50)
	public String getSlideCode(){
		return this.slideCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  玻片编号
	 */
	public void setSlideCode(String slideCode){
		this.slideCode = slideCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  18
	 */
	@Column(name ="F18", length = 50)
	public String getF18(){
		return this.f18;
	}
	/**
	 *方法: 设置String
	 *@param: String  18
	 */
	public void setF18(String f18){
		this.f18 = f18;
	}
	/**
	 *方法: 取得String
	 *@return: String  x
	 */
	@Column(name ="X", length = 50)
	public String getX(){
		return this.x;
	}
	/**
	 *方法: 设置String
	 *@param: String  x
	 */
	public void setX(String x){
		this.x = x;
	}
	/**
	 *方法: 取得String
	 *@return: String  y
	 */
	@Column(name ="Y", length = 50)
	public String getY(){
		return this.y;
	}
	/**
	 *方法: 设置String
	 *@param: String  y
	 */
	public void setY(String y){
		this.y = y;
	}
	/**
	 *方法: 取得String
	 *@return: String  13
	 */
	@Column(name ="F13", length = 50)
	public String getF13(){
		return this.f13;
	}
	/**
	 *方法: 设置String
	 *@param: String  13
	 */
	public void setF13(String f13){
		this.f13 = f13;
	}
	/**
	 *方法: 取得String
	 *@return: String  21
	 */
	@Column(name ="F21", length = 50)
	public String getF21(){
		return this.f21;
	}
	/**
	 *方法: 设置String
	 *@param: String  21
	 */
	public void setF21(String f21){
		this.f21 = f21;
	}
	/**
	 *方法: 取得String
	 *@return: String  是否合格
	 */
	@Column(name ="IS_GOOD", length = 100)
	public String getIsGood(){
		return this.isGood;
	}
	/**
	 *方法: 设置String
	 *@param: String  是否合格
	 */
	public void setIsGood(String isGood){
		this.isGood = isGood;
	}
	/**
	 *方法: 取得String
	 *@return: String  是否提交
	 */
	@Column(name ="IS_COMMIT", length = 50)
	public String getIsCommit(){
		return this.isCommit;
	}
	/**
	 *方法: 设置String
	 *@param: String  是否提交
	 */
	public void setIsCommit(String isCommit){
		this.isCommit = isCommit;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得FishSjTask
	 *@return: FishSjTask  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "FISH_SJ_TASK")
	public FishSjTask getFishSjTask(){
		return this.fishSjTask;
	}
	/**
	 *方法: 设置FishSjTask
	 *@param: FishSjTask  相关主表
	 */
	public void setFishSjTask(FishSjTask fishSjTask){
		this.fishSjTask = fishSjTask;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getSampleType() {
		return sampleType;
	}
	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}
	public String getOtherSit() {
		return otherSit;
	}
	public void setOtherSit(String otherSit) {
		this.otherSit = otherSit;
	}
	public String getNextFlow() {
		return nextFlow;
	}
	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}
	public String getNextFlowId() {
		return nextFlowId;
	}
	public void setNextFlowId(String nextFlowId) {
		this.nextFlowId = nextFlowId;
	}
	
}