package com.biolims.experiment.fish.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.experiment.plasma.model.PlasmaTask;
import com.biolims.common.model.user.User;

/**
 * @Title: Model
 * @Description: 实验步骤
 * @author lims-platform
 * @date 2016-03-25 10:16:46
 * @version V1.0
 * 
 */
@Entity
@Table(name = "FISH_CROSS_TASK_TEMPLATE")
@SuppressWarnings("serial")
public class FishCrossTaskTemplate extends EntityDao<FishCrossTaskTemplate> implements
		java.io.Serializable {
	/**步骤id*/
	private String id;
	/**步骤编号*/
	private String code;
	/**步骤名称*/
	private String name;
	/**备注*/
	private String note;
	/**关联样本*/
	private String sampleCodes;
	/*
	 * Item id模板步骤ID
	 */
	private String tItem;
	
	//开始时间
	private String startTime;
	//结束时间
	private String endTime;
	//实验员
	private User testUser;
	//状态
	private String state;
	/*关联样本*/
	private String codes;
	public String gettItem() {
		return tItem;
	}
	public void settItem(String tItem) {
		this.tItem = tItem;
	}
	/**相关主表*/
	private FishCrossTask fishCrossTask;
	
	/**
	 *方法: 取得String
	 *@return: String  步骤id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  步骤id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  步骤编号
	 */
	@Column(name ="CODE", length = 50)
	public String getCode(){
		return this.code;
	}
	/**
	 *方法: 设置String
	 *@param: String  步骤编号
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 *方法: 取得String
	 *@return: String  步骤名称
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  步骤名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	@Column(name ="SAMPLE_CODES", length = 5000)
	public String getSampleCodes() {
		return sampleCodes;
	}
	public void setSampleCodes(String sampleCodes) {
		this.sampleCodes = sampleCodes;
	}
	@Column(name="START_TIME",length=50)
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	@Column(name="END_TIME",length=50)
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TEST_USER")
	public User getTestUser() {
		return testUser;
	}
	public void setTestUser(User testUser) {
		this.testUser = testUser;
	}
	@Column(name="STATE",length=20)
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@Column(name="CODES",length=2000)
	public String getCodes() {
		return codes;
	}
	public void setCodes(String codes) {
		this.codes = codes;
	}

	/**
	 * 方法: 取得FishCrossTask
	 * 
	 * @return: FishCrossTask 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "FISH_CROSS_TASK")
	public FishCrossTask getFishCrossTask() {
		return this.fishCrossTask;
	}

	/**
	 * 方法: 设置FishCrossTask
	 * 
	 * @param: FishCrossTask 相关主表
	 */
	public void setFishCrossTask(FishCrossTask fishCrossTask) {
		this.fishCrossTask = fishCrossTask;
	}
}