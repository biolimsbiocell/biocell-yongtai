package com.biolims.experiment.fish.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.experiment.plasma.model.PlasmaTask;
import com.biolims.sample.model.DicSampleType;

/**
 * @Title: Model
 * @Description: 杂交洗脱结果
 * @author lims-platform
 * @date 2016-03-25 10:16:50
 * @version V1.0
 * 
 */
@Entity
@Table(name = "FISH_CROSS_TASK_RESULT")
@SuppressWarnings("serial")
public class FishCrossTaskResult extends EntityDao<FishCrossTaskResult> implements
		java.io.Serializable {
	/** 编码 */
	private String id;
	/** 临时表id */
	private String tempId;
	/** 样本编号 */
	private String code;
	/** 接收样本编号 */
	private String sampleCode;
	/** 制片样本编号 */
	private String zpCode;
	/** 样本编号 */
	private String slideCode;
	/** 体积 */
	private Double volume;
	/** 单位 */
	private String unit;
	/** 结果 */
	private String result;
	// 下一步流向Id
	private String nextFlowId;
	/** 下一步流向 */
	private String nextFlow;
	/** 处理意见 */
	private String method;
	/* 状态 */
	private String state;
	/** 备注 */
	private String note;
	/** 检测项目 */
	private String productId;
	/** 检测项目 */
	private String productName;
	// 是否提交
	private String submit;
	/** 患者姓名 */
	private String patientName;
	/** 身份证号 */
	private String idCard;
	/** 检测方法 */
	private String sequencingFun;
	/** 取样日期 */
	private String inspectDate;
	/** 接收日期 */
	private Date acceptDate;
	/** 应出报告日期 */
	private Date reportDate;
	// 原因
	private String reason;
	/** 浓度 */
	private Double concentration;
	/** 任务单Id */
	private String orderId;
	/** 手机号 */
	private String phone;

	/** 确认执行 */
	private String isExecute;
	private String location;

	// 区分临床还是科技服务 0 临床 1 科技服务
	private String classify;
	/** 中间产物类型 */
	private DicSampleType dicSampleType;
	// 样本类型
	private String sampleType;
	/** 样本数量 */
	private Double sampleNum;
	
	private FishCrossTask fishCrossTask;

	public Double getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(Double sampleNum) {
		this.sampleNum = sampleNum;
	}

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 60)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 血浆编号
	 */
	@Column(name = "CODE", length = 60)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 血浆编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 体积
	 */
	@Column(name = "VOLUME", length = 36)
	public Double getVolume() {
		return this.volume;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 体积
	 */
	public void setVolume(Double volume) {
		this.volume = volume;
	}

	/**
	 * 方法: 取得DicUnit
	 * 
	 * @return: DicUnit 单位
	 */
	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 60)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	@Column(name = "STATE", length = 60)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getNextFlow() {
		return nextFlow;
	}

	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public String getSequencingFun() {
		return sequencingFun;
	}

	public void setSequencingFun(String sequencingFun) {
		this.sequencingFun = sequencingFun;
	}

	public String getInspectDate() {
		return inspectDate;
	}

	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}

	public Date getAcceptDate() {
		return acceptDate;
	}

	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Double getConcentration() {
		return concentration;
	}

	public void setConcentration(Double concentration) {
		this.concentration = concentration;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	public String getIsExecute() {
		return isExecute;
	}

	public void setIsExecute(String isExecute) {
		this.isExecute = isExecute;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_SAMPLE_TYPE")
	public DicSampleType getDicSampleType() {
		return dicSampleType;
	}

	public void setDicSampleType(DicSampleType dicSampleType) {
		this.dicSampleType = dicSampleType;
	}

	public String getNextFlowId() {
		return nextFlowId;
	}

	public void setNextFlowId(String nextFlowId) {
		this.nextFlowId = nextFlowId;
	}
	/**
	 * 方法: 取得FishCrossTask
	 * 
	 * @return: FishCrossTask 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "FISH_CROSS_TASK")
	public FishCrossTask getFishCrossTask() {
		return this.fishCrossTask;
	}

	/**
	 * 方法: 设置FishCrossTask
	 * 
	 * @param: FishCrossTask 相关主表
	 */
	public void setFishCrossTask(FishCrossTask fishCrossTask) {
		this.fishCrossTask = fishCrossTask;
	}

	public String getZpCode() {
		return zpCode;
	}

	public void setZpCode(String zpCode) {
		this.zpCode = zpCode;
	}

	public String getSlideCode() {
		return slideCode;
	}

	public void setSlideCode(String slideCode) {
		this.slideCode = slideCode;
	}
}