package com.biolims.experiment.fish.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.core.model.user.UserGroup;
import com.biolims.dao.EntityDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.system.template.model.Template;
import com.biolims.system.work.model.WorkOrder;

/**
 * @Title: Model
 * @Description: 样本预处理
 * @author lims-platform
 * @date 2016-03-25 10:16:58
 * @version V1.0
 * 
 */
@Entity
@Table(name = "SAMPLE_PRE_TASK")
@SuppressWarnings("serial")
public class SamplePreTask extends EntityDao<SamplePreTask> implements java.io.Serializable {
	/** 编号 */
	private String id;
	/** 名称 */
	private String name;
	/** 下达人 */
	private User createUser;
	/** 下达时间 */
	private String createDate;
	/** 完成时间 */
	private String confirmDate;
	/** 所属任务 */
	private WorkOrder workOrder;
	/** 实验员 */
	private User testUser;
	/** 实验时间 */
	private Date testDate;
	/** 选择模板 */
	private Template template;
	/** 工作流状态 */
	private String state;
	/** 工作流状态 */
	private String stateName;

	/** 实验组 */
	private UserGroup acceptUser;
	/** 容器数量 */
	private Integer maxNum;
	/** 样本类型 */
	private DicSampleType dicSampleType;
	/** 类型 */
	private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 名称
	 */
	@Column(name = "NAME", length = 120)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 下达人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 下达人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 下达时间
	 */
	@Column(name = "CREATE_DATE", length = 255)
	public String getCreateDate() {
		return this.createDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 下达时间
	 */
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	/**
	 * 方法: 取得WorkOrder
	 * 
	 * @return: WorkOrder 所属任务
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "WORK_ORDER")
	public WorkOrder getWorkOrder() {
		return this.workOrder;
	}

	/**
	 * 方法: 设置WorkOrder
	 * 
	 * @param: WorkOrder 所属任务
	 */
	public void setWorkOrder(WorkOrder workOrder) {
		this.workOrder = workOrder;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 实验员
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TEST_USER")
	public User getTestUser() {
		return this.testUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 实验员
	 */
	public void setTestUser(User testUser) {
		this.testUser = testUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 实验时间
	 */
	@Column(name = "TEST_DATE", length = 255)
	public Date getTestDate() {
		return this.testDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 实验时间
	 */
	public void setTestDate(Date testDate) {
		this.testDate = testDate;
	}

	/**
	 * 方法: 取得Template
	 * 
	 * @return: Template 选择模板
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TEMPLATE")
	public Template getTemplate() {
		return this.template;
	}

	/**
	 * 方法: 设置Template
	 * 
	 * @param: Template 选择模板
	 */
	public void setTemplate(Template template) {
		this.template = template;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 工作流状态
	 */
	@Column(name = "STATE", length = 60)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 工作流状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 工作流状态
	 */
	@Column(name = "STATE_NAME", length = 60)
	public String getStateName() {
		return this.stateName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 工作流状态
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "T_USER_GROUP")
	public UserGroup getAcceptUser() {
		return acceptUser;
	}

	public Integer getMaxNum() {
		return maxNum;
	}

	public void setMaxNum(Integer maxNum) {
		this.maxNum = maxNum;
	}

	public void setAcceptUser(UserGroup acceptUser) {
		this.acceptUser = acceptUser;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_SAMPLE_TYPE")
	public DicSampleType getDicSampleType() {
		return dicSampleType;
	}

	public void setDicSampleType(DicSampleType dicSampleType) {
		this.dicSampleType = dicSampleType;
	}

	public String getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(String confirmDate) {
		this.confirmDate = confirmDate;
	}

}