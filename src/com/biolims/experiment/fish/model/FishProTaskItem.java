package com.biolims.experiment.fish.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.experiment.plasma.model.PlasmaTask;
import com.biolims.sample.model.DicSampleType;
import com.biolims.storage.position.model.StoragePosition;

/**
 * @Title: Model
 * @Description: fish制片样本明细
 * @author lims-platform
 * @date 2016-03-25 10:16:44
 * @version V1.0
 * 
 */
@Entity
@Table(name = "FISH_PRO_TASK_ITEM")
@SuppressWarnings("serial")
public class FishProTaskItem extends EntityDao<FishProTaskItem> implements
		java.io.Serializable {
	/** 编码 */
	private String id;
	/** 临时表id */
	private String tempId;
	/** 血浆编号 */
	private String code;
	/** 样本编号 */
	private String sampleCode;
	/** 样本名称 */
	private String name;
	/** 患者姓名 */
	private String patientName;
	/** 核对 */
	private String checked;
	/** 检测项目 */
	private String productId;
	/** 检测项目 */
	private String productName;
	/** 状态 */
	private String state;
	/** 备注 */
	private String note;
	/** 储位 */
	private StoragePosition storage;
	/** 浓度 */
	private Double concentration;
	/** 结果 */
	private String result;
	/** 失败原因 */
	private String reason;
	/** 步骤编号 */
	private String stepNum;
	/** 样本数量 */
	private Double sampleNum;
	// 样本用量
	private Double sampleConsume;
	/** 体积 */
	private Double volume;
	/** 身份证号 */
	private String idCard;
	/** 检测方法 */
	private String sequencingFun;
	/** 取样日期 */
	private String inspectDate;
	/** 接收日期 */
	private Date acceptDate;
	/** 应出报告日期 */
	private Date reportDate;
	/** 任务单id */
	private String orderId;
	/** 手机号 */
	private String phone;
	// 排序号
	private Integer orderNumber;
	/** 行号 */
	private String rowCode;
	/** 列号 */
	private String colCode;
	/** 板号 */
	private String counts;

	// 区分临床还是科技服务 0 临床 1 科技服务
	private String classify;
	// 中间产物数量
	private String productNum;
	// 中间产物类型
	private DicSampleType dicSampleType;
	// 样本类型
	private String sampleType;

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}
	/** 相关主表 */
	private FishProTask fishProTask;
	/**
	 * 方法: 取得FishProTask
	 * 
	 * @return: FishProTask 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "FISH_PRO_TASK")
	public FishProTask getFishProTask() {
		return this.fishProTask;
	}
	
	/**
	 * 方法: 设置FishProTask
	 * 
	 * @param: FishProTask 相关主表
	 */
	public void setFishProTask(FishProTask fishProTask) {
		this.fishProTask = fishProTask;
	}
	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 60)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本编号
	 */
	@Column(name = "CODE", length = 60)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 核对
	 */
	@Column(name = "CHECKED", length = 60)
	public String getChecked() {
		return this.checked;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 核对
	 */
	public void setChecked(String checked) {
		this.checked = checked;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 36)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得Sting
	 * 
	 * @return: Sting 备注
	 */
	@Column(name = "NOTE", length = 36)
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}


	@Column(name = "NAME", length = 50)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "SAMPLE_CODE", length = 50)
	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	/**
	 * 方法: 取得StoragePosition
	 * 
	 * @return: StoragePosition 储位
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE")
	public StoragePosition getStorage() {
		return this.storage;
	}

	/**
	 * 方法: 设置StoragePosition
	 * 
	 * @param: StoragePosition 储位
	 */
	public void setStorage(StoragePosition storage) {
		this.storage = storage;
	}

	@Column(name = "CONCENTRATION", length = 100)
	public Double getConcentration() {
		return concentration;
	}

	public void setConcentration(Double concentration) {
		this.concentration = concentration;
	}

	@Column(name = "RESULT", length = 50)
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	@Column(name = "REASON", length = 100)
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@Column(name = "STEP_NUM", length = 50)
	public String getStepNum() {
		return stepNum;
	}

	public void setStepNum(String stepNum) {
		this.stepNum = stepNum;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public String getSequencingFun() {
		return sequencingFun;
	}

	public void setSequencingFun(String sequencingFun) {
		this.sequencingFun = sequencingFun;
	}

	public String getInspectDate() {
		return inspectDate;
	}

	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}

	public Date getAcceptDate() {
		return acceptDate;
	}

	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	public Integer getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getRowCode() {
		return rowCode;
	}

	public void setRowCode(String rowCode) {
		this.rowCode = rowCode;
	}

	public String getColCode() {
		return colCode;
	}

	public void setColCode(String colCode) {
		this.colCode = colCode;
	}

	public String getCounts() {
		return counts;
	}

	public void setCounts(String counts) {
		this.counts = counts;
	}

	public Double getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(Double sampleNum) {
		this.sampleNum = sampleNum;
	}

	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}

	public String getProductNum() {
		return productNum;
	}

	public void setProductNum(String productNum) {
		this.productNum = productNum;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_SAMPLE_TYPE")
	public DicSampleType getDicSampleType() {
		return dicSampleType;
	}

	public void setDicSampleType(DicSampleType dicSampleType) {
		this.dicSampleType = dicSampleType;
	}

	public Double getSampleConsume() {
		return sampleConsume;
	}

	public void setSampleConsume(Double sampleConsume) {
		this.sampleConsume = sampleConsume;
	}
}