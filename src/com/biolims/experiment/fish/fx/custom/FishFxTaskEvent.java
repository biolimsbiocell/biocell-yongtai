package com.biolims.experiment.fish.fx.custom;


import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.fish.fx.service.FishFxTaskService;
import com.biolims.experiment.plasma.service.BloodSplitService;

public class FishFxTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		FishFxTaskService mbService = (FishFxTaskService) ctx.getBean("fishFxTaskService");
		mbService.chengeState(applicationTypeActionId, contentId);
		return "";
	}
}
