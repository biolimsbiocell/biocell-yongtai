package com.biolims.experiment.fish.fx.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.experiment.fish.first.model.FishFirstInstance;
import com.biolims.experiment.fish.first.service.FishFirstService;
import com.biolims.experiment.fish.fx.model.FishAgainInstance;
import com.biolims.experiment.fish.fx.service.FishAgainService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

@Namespace("/experiment/fish/fx/again")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class FishAgainAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "248906";
	@Autowired
	private FishAgainService fishAgainService;
	private FishAgainInstance fishAgainInstance = new FishAgainInstance();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;

	@Action(value = "showFishAgainInstanceList")
	public String showFishAgainInstanceList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/fish/fx/fishAgainInstance.jsp");
	}

	@Action(value = "showFishAgainInstanceListJson")
	public void showFishAgainInstanceListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = fishAgainService.findFishAgainInstanceList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<FishAgainInstance> list = (List<FishAgainInstance>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("sampleType", "");
		map.put("slideCode", "");
		map.put("lcjy", "");
		map.put("ycbg", "");
		map.put("cellNum", "");
		map.put("result", "");
		map.put("resultDetail", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("nextFlowId", "");
		map.put("nextFlow", "");
		map.put("isGood", "");
		map.put("isCommit", "");
		map.put("note", "");
		map.put("upTime", "");
		map.put("state", "");
		map.put("probeSite", "");
		map.put("template-id", "");
		map.put("template-fileName", "");
		map.put("reportInfo-id", "");
		map.put("reportInfo-name", "");
		map.put("reportInfoId", "");
		map.put("reportInfoName", "");
		map.put("taskType", "");
		map.put("taskResultId", "");
		map.put("taskId", "");
		map.put("fileNum", "");
		map.put("formerDate", "yyyy-MM-dd");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}
/**
 * 提交样本
 * @throws Exception
 */
@Action(value = "submitSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
public void submitSample() throws Exception {
	String[] ids = getRequest().getParameterValues("ids[]");
	Map<String, Object> result = new HashMap<String, Object>();
	try {
		this.fishAgainService.submitSample(ids);

		result.put("success", true);

	} catch (Exception e) {
		result.put("success", false);
	}
	HttpUtils.write(JsonUtils.toJsonString(result));
}

/**
 * 删除明细信息
 * 
 * @throws Exception
 */
@Action(value = "delFishAgainInstance")
public void delFishAgainInstance() throws Exception {
	Map<String, Object> map = new HashMap<String, Object>();
	try {
		String[] ids = getRequest().getParameterValues("ids[]");
		fishAgainService.delFishAgainInstance(ids);
		map.put("success", true);
	} catch (Exception e) {
		e.printStackTrace();
		map.put("success", false);
	}
	HttpUtils.write(JsonUtils.toJsonString(map));
}

public String getRightsId() {
	return rightsId;
}

public void setRightsId(String rightsId) {
	this.rightsId = rightsId;
}

public FishAgainService getFishAgainService() {
	return fishAgainService;
}

public void setFishAgainService(FishAgainService fishAgainService) {
	this.fishAgainService = fishAgainService;
}

public FishAgainInstance getFishAgainInstance() {
	return fishAgainInstance;
}

public void setFishAgainInstance(FishAgainInstance fishAgainInstance) {
	this.fishAgainInstance = fishAgainInstance;
}

/**
 * 保存复核结果
 */
@Action(value = "savefishAgainInstance",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
public void savefishAgainInstance() throws Exception {
	Map<String, Object> result = new HashMap<String, Object>();
	try {
		String itemDataJson = getRequest().getParameter("itemDataJson");
		fishAgainService.saveFishAgainInstance(itemDataJson);
		result.put("success", true);
	} catch (Exception e) {
		e.printStackTrace();
		result.put("success", false);
	}
	HttpUtils.write(JsonUtils.toJsonString(result));
}

/**
 * 生成PDF报告文件
 * 
 * @throws Exception
 */
@Action(value = "createReportFile", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
public void createReportFile() throws Exception {
	Map<String, Boolean> map = new HashMap<String, Boolean>();
	try {
		String[] ids = getRequest().getParameterValues("ids[]");
		fishAgainService.createReportFilePDF(ids);
		map.put("success", true);
	} catch (Exception e) {
		e.printStackTrace();
		map.put("success", false);
	}

	HttpUtils.write(JsonUtils.toJsonString(map));
}
}
