package com.biolims.experiment.fish.fx.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.comsearch.dao.ComSearchDao;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.core.model.user.UserGroup;
import com.biolims.experiment.fish.fx.model.FishFxTask;
import com.biolims.experiment.fish.fx.model.FishFxTaskResult;
import com.biolims.experiment.fish.fx.model.FishFxTaskTemp;
import com.biolims.experiment.fish.fx.service.FishFxTaskService;
import com.biolims.experiment.fish.model.FishCrossTask;
import com.biolims.file.model.FileInfo;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

@Namespace("/experiment/fish/fx/fishFxTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class FishFxTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "248905";
	@Autowired
	private FishFxTaskService fishFxTaskService;
	private FishFxTask fishFxTask = new FishFxTask();
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;
	@Resource
	private ComSearchDao comSearchDao;

	@Action(value = "showFishFxTaskList")
	public String showFishFxTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/fish/fx/fishFxTask.jsp");
	}

	@Action(value = "showFishFxTaskListJson")
	public void showFishFxTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = fishFxTaskService.findFishFxTaskList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<FishFxTask> list = (List<FishFxTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "fishFxTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogFishFxTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/fish/fx/fishFxTaskDialog.jsp");
	}

	@Action(value = "showDialogFishFxTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogFishFxTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = fishFxTaskService
				.findFishFxTaskListByState(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<FishFxTask> list = (List<FishFxTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editFishFxTask")
	public String editFishFxTask() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			fishFxTask = fishFxTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "fishFxTask");
			String tName = workflowProcessInstanceService
					.findToDoTaskNameByFormId(id);
			putObjToContext("taskName", tName);
		} else {
			fishFxTask.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			fishFxTask.setCreateUser(user);
			fishFxTask.setCreateDate(new Date());
			fishFxTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			fishFxTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			UserGroup u=comSearchDao.get(UserGroup.class,"P013");
			//if(fishFxTask.getAcceptUser()!=null){
				fishFxTask.setAcceptUser(u);
			//}
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(fishFxTask.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/fish/fx/fishFxTaskEdit.jsp");
	}

	@Action(value = "copyFishFxTask")
	public String copyFishFxTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		fishFxTask = fishFxTaskService.get(id);
		fishFxTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/fish/fx/fishFxTaskEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = fishFxTask.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "FishFxTask";
			String markCode = "FFX";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			fishFxTask.setId(autoID);
			fishFxTask.setName("Fish分析-"+autoID);
		}
		Map aMap = new HashMap();
		aMap.put("fishFxTaskResult",
				getParameterFromRequest("fishFxTaskResultJson"));

		fishFxTaskService.save(fishFxTask, aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/experiment/fish/fx/fishFxTask/editFishFxTask.action?id="
				+ fishFxTask.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);
		// return
		// redirect("/experiment/fish/fx/fishFxTask/editFishFxTask.action?id="
		// + fishFxTask.getId());

	}

	@Action(value = "viewFishFxTask")
	public String toViewFishFxTask() throws Exception {
		String id = getParameterFromRequest("id");
		fishFxTask = fishFxTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/fish/fx/fishFxTaskEdit.jsp");
	}

	@Action(value = "showFishFxTaskResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showFishFxTaskResultList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/fish/fx/fishFxTaskResult.jsp");
	}

	@Action(value = "showFishFxTaskResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showFishFxTaskResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = fishFxTaskService
					.findFishFxTaskResultList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<FishFxTaskResult> list = (List<FishFxTaskResult>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("sampleType", "");
			map.put("slideCode", "");
			map.put("lcjy", "");
			map.put("ycbg", "");
			map.put("cellNum", "");
			map.put("result", "");
			map.put("resultDetail", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("isGood", "");
			map.put("isCommit", "");
			map.put("note", "");
			map.put("state", "");
			map.put("fishFxTask-name", "");
			map.put("fishFxTask-id", "");
			map.put("probeSite", "");
			map.put("template-id", "");
			map.put("template-fileName", "");
			map.put("reportInfo-id", "");
			map.put("reportInfo-name", "");
			map.put("reportInfoId", "");
			map.put("reportInfoName", "");
			map.put("fileNum", "");
			map.put("upTime", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delFishFxTaskResult")
	public void delFishFxTaskResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			fishFxTaskService.delFishFxTaskResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除
	 * 
	 * @throws Exception
	 */
	@Action(value = "delFishFxTaskResultOne")
	public void delFishSjTaskResultOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			fishFxTaskService.delFishFxTaskResultOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showFishFxTaskTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showFishFxTaskTempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/fish/fx/fishFxTaskTemp.jsp");
	}

	@Action(value = "showFishFxTaskTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showFishFxTaskTempListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = fishFxTaskService
				.findFishFxTaskTempList(map2Query, startNum, limitNum, dir, sort);
		Long total = (Long) result.get("total");
		List<FishFxTaskTemp> list = (List<FishFxTaskTemp>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("cellNum", "");
		map.put("sampleCode", "");
		map.put("sampleType", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("state", "");
		map.put("note", "");
		map.put("patientName", "");
		map.put("reason", "");
		map.put("gender", "");
		map.put("doctor", "");
		map.put("hospital", "");
		map.put("slideCode", "");
		map.put("chargeNote", "");//缴费状态
		new SendData().sendDateJson(map, list, total,
				ServletActionContext.getResponse());
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delFishFxTaskTemp")
	public void delFishFxTaskTemp() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			fishFxTaskService.delFishFxTaskTemp(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public FishFxTaskService getFishFxTaskService() {
		return fishFxTaskService;
	}

	public void setFishFxTaskService(FishFxTaskService fishFxTaskService) {
		this.fishFxTaskService = fishFxTaskService;
	}

	public FishFxTask getFishFxTask() {
		return fishFxTask;
	}

	public void setFishFxTask(FishFxTask fishFxTask) {
		this.fishFxTask = fishFxTask;
	}

	/**
	 * 生成PDF报告文件
	 * 
	 * @throws Exception
	 */
	@Action(value = "createReportFile", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void createReportFile() throws Exception {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			fishFxTaskService.createReportFilePDF(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}

		HttpUtils.write(JsonUtils.toJsonString(map));
	}
/**
 * 提交样本
 * @throws Exception
 */
@Action(value = "submitSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
public void submitSample() throws Exception {
	String id = getParameterFromRequest("id");
	String[] ids = getRequest().getParameterValues("ids[]");
	Map<String, Object> result = new HashMap<String, Object>();
	try {
		this.fishFxTaskService.submitSample(id, ids);

		result.put("success", true);

	} catch (Exception e) {
		result.put("success", false);
	}
	HttpUtils.write(JsonUtils.toJsonString(result));
   }
   /**
	 * 保存FISH分析明细
	 * @throws Exception
	 */
	@Action(value = "saveFishFxTaskResult",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveFishFxTaskResult() throws Exception {
		String id = getRequest().getParameter("id");
		String itemDataJson = getRequest().getParameter("itemDataJson");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			FishFxTask sc=commonDAO.get(FishFxTask.class, id);
			Map aMap = new HashMap();
			aMap.put("fishFxTaskResult",
					itemDataJson);
			if(sc!=null){
				fishFxTaskService.save(sc,aMap);
			}
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
