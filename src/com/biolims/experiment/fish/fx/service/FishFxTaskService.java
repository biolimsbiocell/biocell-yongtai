package com.biolims.experiment.fish.fx.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.comsearch.dao.ComSearchDao;
import com.biolims.common.comsearch.service.ComSearchService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.experiment.fish.fishabnormal.model.FishAbnormal;
import com.biolims.experiment.fish.fx.dao.FishFxTaskDao;
import com.biolims.experiment.fish.fx.model.FishFxTask;
import com.biolims.experiment.fish.fx.model.FishFxTaskResult;
import com.biolims.experiment.fish.fx.model.FishFxTaskTemp;
import com.biolims.experiment.karyotyping.dao.KaryotypingTaskDao;
import com.biolims.experiment.karyotyping.service.KaryotypingTaskService;
import com.biolims.file.model.FileInfo;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.report.model.EndReport;
import com.biolims.report.model.SampleReportTemp;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.sysreport.model.ReportTemplateInfo;
import com.biolims.util.ConfigFileUtil;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class FishFxTaskService {
	@Resource
	private FishFxTaskDao fishFxTaskDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private ComSearchDao comSearchDao;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private CommonService commonService;
	@Resource
	private KaryotypingTaskDao karyotypingTaskDao;
	@Resource
	private KaryotypingTaskService karyotypingTaskService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private ComSearchService comSearchService;

	StringBuffer json = new StringBuffer();

	public Map<String, Object> findFishFxTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return fishFxTaskDao.selectFishFxTaskList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	public Map<String, Object> findFishFxTaskListByState(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return fishFxTaskDao.selectFishFxTaskListByState(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FishFxTask i) throws Exception {

		fishFxTaskDao.saveOrUpdate(i);

	}

	public FishFxTask get(String id) {
		FishFxTask fishFxTask = commonDAO.get(FishFxTask.class, id);
		return fishFxTask;
	}

	public Map<String, Object> findFishFxTaskResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = fishFxTaskDao.selectFishFxTaskResultList(
				scId, startNum, limitNum, dir, sort);
		List<FishFxTaskResult> list = (List<FishFxTaskResult>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findFishFxTaskTempList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = fishFxTaskDao.
				selectFishFxTaskTempList(mapForQuery, startNum, limitNum, dir, sort);
		List<FishFxTaskTemp> list = (List<FishFxTaskTemp>) result.get("list");
		Long count = (Long) result.get("total");
		Map<String, Object> result2 = new HashMap<String, Object>();
		List<FishFxTaskTemp> list2=new ArrayList<FishFxTaskTemp>();
		for(FishFxTaskTemp t:list){
			SampleInfo s=this.sampleInfoMainDao.findSampleInfo(t.getSampleCode());
			if(s!=null && s.getSampleOrder()!=null){
				t.setChargeNote(s.getSampleOrder().getChargeNote());
				t.setPatientName(s.getSampleOrder().getName());
				if(s.getSampleOrder().getCrmDoctor()!=null){
					t.setDoctor(s.getSampleOrder().getCrmDoctor().getName());
				}else{
					t.setDoctor("");
				}
				if(s.getSampleOrder().getCrmCustomer()!=null){
					t.setHospital(s.getSampleOrder().getCrmCustomer().getName());
				}else{
					t.setHospital("");
				}
				t.setGender(s.getSampleOrder().getGender());
				t.setReason(s.getSampleOrder().getSubmitReasonName());
			}
			list2.add(t);
		}
		result2.put("total", count);
		result2.put("list", list2);
		return result2;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveFishFxTaskResult(FishFxTask sc, String itemDataJson)
			throws Exception {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		List<FishFxTaskResult> saveItems = new ArrayList<FishFxTaskResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			FishFxTaskResult scp = new FishFxTaskResult();
			// 将map信息读入实体类
			scp = (FishFxTaskResult) fishFxTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setFishFxTask(sc);

			saveItems.add(scp);
			fishFxTaskDao.saveOrUpdate(scp);
			//获取相关图片信息
			List<FileInfo> picList=comSearchDao.findPicture(scp.getId(), "fishFxTaskResult");
			if(picList.size()>0){
				if(picList.size()<=2){
					List<ReportTemplateInfo> rlist=comSearchDao.getReportTemplateInfo(scp.getProductId(),"1");
					scp.setReportInfo(rlist.get(0));
					scp.setTemplate(rlist.get(0).getAttach());
				}else{
					int n=(picList.size()-2)%4;
					int a=0;//打印页数
					if(n==0){
						a=(picList.size()-2)/4;
					}else{
						a=(picList.size()-2)/4+1;
					}
					
					ReportTemplateInfo rt=new ReportTemplateInfo();//报告模板
					for(int i=0;i<a;i++){
						//获取对应报告页数的模板
						rt=comSearchDao.getReportTemplateInfo
								(scp.getProductId(),String.valueOf(i+2)).get(0);
					}
					scp.setReportInfo(rt);
					scp.setTemplate(rt.getAttach());
				}
			}else{
				scp.setReportInfo(null);
				scp.setTemplate(null);
			}
//			if(scp!=null){
//				if(scp.getIsCommit()!=null && !scp.getIsCommit().equals("")
//					&& scp.getIsGood()!=null && !scp.getIsGood().equals("")){
//					if(scp.getIsCommit().equals("1")){
//						if(scp.getIsGood().equals("1")){
//							//提交合格的到复核
//					    	FishAgainInstance sf=new FishAgainInstance();
//					    	sf.setCode(scp.getCode());
//					    	sf.setSampleCode(scp.getSampleCode());
//					    	sf.setSampleType(scp.getSampleType());
//					    	sf.setSlideCode(scp.getSlideCode());
//					    	sf.setResult(scp.getResult());
//					    	sf.setResultDetail(scp.getResultDetail());
//					    	sf.setLcjy(scp.getLcjy());
//					    	sf.setProbeSite(scp.getProbeSite());
//					    	sf.setProductId(scp.getProductId());
//					    	sf.setProductName(scp.getProductName());
//					    	if(scp.getTemplate()!=null){
//					    		sf.setTemplate(scp.getTemplate());
//					    	}
//					    	if(scp.getReportInfo()!=null){
//					    		sf.setReportInfo(scp.getReportInfo());
//					    	}
//					    	sf.setReportInfoId(scp.getReportInfoId());
//					    	sf.setReportInfoName(scp.getReportInfoName());
//					    	sf.setCellNum(scp.getCellNum());
//					    	sf.setYcbg(scp.getYcbg());
//					    	sf.setTaskId(sc.getId());
//					    	sf.setTaskResultId(scp.getId());
//					    	sf.setTaskType("Fish分析");
//					    	sf.setState("1");
//					    	sf.setFormerDate(new Date());
//					    	commonDAO.saveOrUpdate(sf);
//					    	
//					    	//提交后改变左侧状态
//					    	if(scp.getFileNum()!=null){
//					    		FishFxTaskTemp ft=commonDAO.get(FishFxTaskTemp.class, scp.getFileNum());
//								if(ft!=null){
//									ft.setState("2");
//								}
//					    	}
//						}else{
//							FishAbnormal ka=new FishAbnormal();
//							ka.setCode(scp.getCode());
//							ka.setSampleCode(scp.getSampleCode());
//							ka.setProductId(scp.getProductId());
//							ka.setProductName(scp.getProductName());
//							//ka.setSampleType(scp.getSampleType());
//							ka.setTaskId(scp.getFishFxTask().getId());
//							ka.setNote(scp.getNote());
//							ka.setTaskName("Fish分析");
//							ka.setState("1");
//						}
//						sampleStateService
//						.saveSampleState1(
//								scp.getCode(),
//								scp.getSampleCode(),
//								scp.getProductId(),
//								scp.getProductName(),
//								"",
//								format.format(sc.getCreateDate()),
//								format.format(new Date()),
//								"FishFxTask",
//								"Fish分析",
//								(User) ServletActionContext
//										.getRequest()
//										.getSession()
//										.getAttribute(
//												SystemConstants.USER_SESSION_KEY),
//								sc.getId(), "Fish数据复核",
//								scp.getIsGood(), null, null, null, null, null,
//								null, null, null,scp.getResult(),scp.getResultDetail(),scp.getLcjy(),scp.getYcbg());
//					}
//				}
//			}

		}
		fishFxTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delFishFxTaskResult(String[] ids) throws Exception {
		for (String id : ids) {
			FishFxTaskResult scp = fishFxTaskDao
					.get(FishFxTaskResult.class, id);
			fishFxTaskDao.delete(scp);
		}
	}

	/**
	 * 根据ID删除
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delFishFxTaskResultOne(String ids) throws Exception {
		FishFxTaskResult scp = fishFxTaskDao.get(FishFxTaskResult.class, ids);
		fishFxTaskDao.delete(scp);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delFishFxTaskTemp(String[] ids) throws Exception {
		for (String id : ids) {
			FishFxTaskTemp scp = fishFxTaskDao.get(FishFxTaskTemp.class, id);
			fishFxTaskDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FishFxTask sc, Map jsonMap) throws Exception {
		if (sc != null) {
			fishFxTaskDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("fishFxTaskResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveFishFxTaskResult(sc, jsonStr);
			}

		}
	}

	/**
	 * 审批完成
	 * 
	 * @param applicationTypeActionId
	 * @param id
	 * @throws Exception
	 */
	public void chengeState(String applicationTypeActionId, String id)
			throws Exception {
		FishFxTask sct = commonDAO.get(FishFxTask.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);

		fishFxTaskDao.update(sct);
		
		submitSample(id,null);
		// 完成后改变杂交洗脱实验的状态
		// if(sct.getFishSjTask()!=null){
		// FishSjTask fct = commonDAO.get(FishSjTask.class,
		// sct.getFishSjTask().getId());
		// if(fct!=null){
		// fct.setState("2");
		// }
		// }
//		List<FishFxTaskResult> list = this.fishFxTaskDao.getResult(id);
//		if(list.size()>0){
//			for (FishFxTaskResult scp : list) {
//				if (scp.getIsCommit().equals("1")) {
//					if (scp.getIsGood().equals("1")) {
//						if (scp.getNextFlowId().equals("0030")) {// 到报告生成模块
//							SampleInfo sf = sampleInfoMainDao.findSampleInfo(scp
//									.getCode());
//							if (sf != null) {
//								sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_REPORT_NEW);
//								sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_REPORT_NEW_NAME);
//							}
//							CreateReport cr = new CreateReport();
//							cr.setCode(scp.getCode());
//							cr.setSampleCode(scp.getSampleCode());
//							cr.setSlideCode(scp.getSlideCode());
//							cr.setProductId(scp.getProductId());
//							cr.setProductName(scp.getProductName());
//							cr.setResult(scp.getResult());
//							cr.setResultDescription(scp.getResultDetail());
//							cr.setAdvice(scp.getLcjy());
//							cr.setTaskId(sct.getId());
//							cr.setTaskType("Fish分析");
//							cr.setTaskResultId(scp.getId());
//							if(scp.getReportInfo()!=null && scp.getTemplate()!=null){
//								cr.setReportInfo(scp.getReportInfo());
//								cr.setTemplate(scp.getTemplate());
//							}
//							cr.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_REPORT_NEW);
//							cr.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_REPORT_NEW_NAME);
//	
//							commonDAO.saveOrUpdate(cr);
//						}
//					}else{
//						FishAbnormal ka=new FishAbnormal();
//						ka.setCode(scp.getCode());
//						ka.setSampleCode(scp.getSampleCode());
//						ka.setProductId(scp.getProductId());
//						ka.setProductName(scp.getProductName());
//						//ka.setSampleType(scp.getSampleType());
//						ka.setTaskId(scp.getFishFxTask().getId());
//						ka.setNote(scp.getNote());
//						ka.setTaskName("Fish分析");
//						ka.setState("1");
//					}
//	
//					DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//					sampleStateService
//							.saveSampleState(
//									scp.getCode(),
//									scp.getSampleCode(),
//									scp.getProductId(),
//									scp.getProductName(),
//									"",
//									format.format(sct.getCreateDate()),
//									format.format(new Date()),
//									"FishFxTask",
//									"Fish分析",
//									(User) ServletActionContext
//											.getRequest()
//											.getSession()
//											.getAttribute(
//													SystemConstants.USER_SESSION_KEY),
//									sct.getId(), scp.getNextFlow(),
//									scp.getIsGood(), null, null, null, null, null,
//									null, null, null);
//				}
//			}
//		}
//		List<FishFxTaskResult> list_item = fishFxTaskDao
//				.selectItem(sct.getId());
//		for (FishFxTaskResult t : list_item) {
//			String tempId = t.getId();
//			FishFxTaskTemp temp = commonDAO.get(
//					FishFxTaskTemp.class, tempId);
//			if (temp != null) {
//				temp.setState("2");
//			}
//		}
	}
	
	
	/**
	 * 提交样本
	 * @param id
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitSample(String id, String[] ids) throws Exception {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// 获取主表信息
		FishFxTask sc = this.fishFxTaskDao.get(FishFxTask.class, id);
		// 获取结果表样本信息
		List<FishFxTaskResult> list=new ArrayList<FishFxTaskResult>();
		if (ids == null)
			list = this.fishFxTaskDao.setResultById(id);
		else
			list = this.fishFxTaskDao.setResultByIds(ids);
		for (FishFxTaskResult scp : list) {
			if (scp != null) {
				if(scp.getIsGood().equals("1")){
					if("0031".equals(scp.getNextFlowId())||"0035".equals(scp.getNextFlowId())){ //报告模块

						SampleInfo si = new SampleInfo();

						List<SampleInfo> sil = commonService.get(SampleInfo.class, "code",
								scp.getSampleCode());
						if (sil.size() > 0)
							si = sil.get(0);

						if (si.getOrderNum() != null) {
							List<SampleReportTemp> ab = commonService.get(SampleReportTemp.class, "orderNum",
									si.getOrderNum());
							if (ab.size() > 0){
								boolean a = false;
								SampleReportTemp st = new SampleReportTemp();
								for(SampleReportTemp abc : ab){
									if("fish".equals(abc.getTestCode())&&scp.getCode().equals(abc.getCode())&&scp.getProductId().equals(abc.getProductId())){
										a=true;
										st=abc;
										break;
									}
								}
								if(a){
									st.setState("1");
									commonService.saveOrUpdate(st);
								}else{
									st.setCode(scp.getCode());
									st.setPatientName(si.getPatientName());
									st.setSampleCode(scp.getSampleCode());
									st.setProductId(scp.getProductId());
									st.setProductName(scp.getProductName());
									st.setOrderNum(si.getOrderNum());
									st.setState(scp.getState());
									st.setNote(scp.getNote());
									st.setState("1");
									/*
									 * 赋值订单实体
									 */
									SampleOrder od = commonDAO.get(SampleOrder.class,
											si.getOrderNum());
									if (od!=null||"".equals(od)){
										st.setSampleOrder(od);
									}
									st.setTestCode("fish");
									st.setTestFroms("Fish实验");
									commonService.saveOrUpdate(st);
								}
							}else{
								SampleReportTemp st = new SampleReportTemp();

								st.setCode(scp.getCode());
								st.setPatientName(si.getPatientName());
								st.setSampleCode(scp.getSampleCode());
								st.setProductId(scp.getProductId());
								st.setProductName(scp.getProductName());
								st.setOrderNum(si.getOrderNum());
								st.setState(scp.getState());
								st.setNote(scp.getNote());
								st.setState("1");
								/*
								 * 赋值订单实体
								 */
								SampleOrder od = commonDAO.get(SampleOrder.class,
										si.getOrderNum());
								if (od!=null||"".equals(od)){
									st.setSampleOrder(od);
								}
								st.setTestCode("fish");
								st.setTestFroms("Fish实验");
								commonService.saveOrUpdate(st);
//								orderList.add(si.getOrderNum());
//								qtTaskDao.saveOrUpdate(st);
							}
						}
					}
//					//提交合格的到复核
//			    	FishAgainInstance sf=new FishAgainInstance();
//			    	sf.setCode(scp.getCode());
//			    	sf.setSampleCode(scp.getSampleCode());
//			    	sf.setSampleType(scp.getSampleType());
//			    	sf.setSlideCode(scp.getSlideCode());
//			    	sf.setResult(scp.getResult());
//			    	sf.setResultDetail(scp.getResultDetail());
//			    	sf.setLcjy(scp.getLcjy());
//			    	sf.setProbeSite(scp.getProbeSite());
//			    	sf.setProductId(scp.getProductId());
//			    	sf.setProductName(scp.getProductName());
//			    	if(scp.getTemplate()!=null){
//			    		sf.setTemplate(scp.getTemplate());
//			    	}
//			    	if(scp.getReportInfo()!=null){
//			    		sf.setReportInfo(scp.getReportInfo());
//			    	}
//			    	sf.setReportInfoId(scp.getReportInfoId());
//			    	sf.setReportInfoName(scp.getReportInfoName());
//			    	sf.setCellNum(scp.getCellNum());
//			    	sf.setYcbg(scp.getYcbg());
//			    	sf.setTaskId(sc.getId());
//			    	sf.setTaskResultId(scp.getId());
//			    	sf.setTaskType("Fish分析");
//			    	sf.setState("1");
//			    	sf.setFormerDate(new Date());
//			    	commonDAO.saveOrUpdate(sf);
			    	
			    	//提交后改变左侧状态
			    	if(scp.getFileNum()!=null){
			    		FishFxTaskTemp ft=commonDAO.get(FishFxTaskTemp.class, scp.getFileNum());
						if(ft!=null){
							ft.setState("2");
						}
			    	}
					sampleStateService
					.saveSampleState1(
							scp.getCode(),
							scp.getSampleCode(),
							scp.getProductId(),
							scp.getProductName(),
							"",
							format.format(sc.getCreateDate()),
							format.format(new Date()),
							"FishFxTask",
							"Fish分析",
							(User) ServletActionContext
									.getRequest()
									.getSession()
									.getAttribute(
											SystemConstants.USER_SESSION_KEY),
							sc.getId(), scp.getNextFlow(),
							scp.getIsGood(), null, null, null, null, null,
							null, null, null,scp.getResult(),scp.getResultDetail(),scp.getLcjy(),scp.getYcbg());
							scp.setIsCommit("1");
					} else {
						
						if(scp.getNextFlowId().equals("0038")){
							//提交不和合格的到报告终止
					    	EndReport cr = new EndReport();
							cr.setCode(scp.getCode());
							cr.setSampleCode(scp.getSampleCode());
							cr.setSampleType(scp.getSampleType());
							cr.setProductId(scp.getProductId());
							cr.setProductName(scp.getProductName());
							cr.setAdvice(scp.getNote());
							cr.setTaskType("Fish分析");
							cr.setTaskId(scp.getId());
							cr.setTaskResultId(scp.getFishFxTask().getId());
							cr.setWaitDate(new Date());
					    	commonDAO.saveOrUpdate(cr);
						}else{
								// 不合格的到异常
								FishAbnormal ka = new FishAbnormal();
								ka.setCode(scp.getCode());
								ka.setSampleCode(scp.getSampleCode());
								ka.setProductId(scp.getProductId());
								ka.setProductName(scp.getProductName());
								ka.setSampleType(scp.getSampleType());
								ka.setTaskId(sc.getId());
								ka.setNote(scp.getNote());
								ka.setTaskName("Fish分析");
								ka.setState("1");
								commonDAO.saveOrUpdate(ka);
						}
						scp.setIsCommit("1");
					}
				}
			}
		}
	/**
	 * 生成PDF报告文件
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@SuppressWarnings("static-access")
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void createReportFilePDF(String[] ids) throws Exception {
		// 临时地址
		String tempFile = ConfigFileUtil
				.getValueByKey("file.report.temp.path");
		// 正式地址
		String formFile = ConfigFileUtil
				.getValueByKey("file.report.form.path");
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH");
		DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		for (String id : ids) {
			FishFxTaskResult sri = commonDAO.get(FishFxTaskResult.class, id);
			SampleInfo sf = sampleInfoMainDao.findSampleInfo(sri
					.getSampleCode());
			/*SampleOrder so = new SampleOrder();
			if (sf != null) {
				if (sf.getSampleOrder() != null) {
					so = sf.getSampleOrder();
				}
			}*/
			SampleOrder so=commonDAO.get(SampleOrder.class, sri
					.getSampleCode());
			//获取相关图片
			List<FileInfo> picList=comSearchDao.findPictureOrderByName(id, "fishFxTaskResult");
			//报告模板
			ReportTemplateInfo reportInfo=sri.getReportInfo();
			//PDF文件
			FileInfo fileInfo = sri.getTemplate();
			if(fileInfo!=null){
				FileInputStream in = new FileInputStream(
						fileInfo.getFilePath());
				PdfReader reader = new PdfReader(in);
				String root = ConfigFileUtil.getRootPath() + File.separator
						+ DateUtil.format(new Date(), "yyyyMMdd");
				if (!new File(root).exists()) {
					new File(root).mkdirs();
				}
				File deskFile = new File(tempFile, "PDF" + sri.getCode()
						+ ".pdf");
				PdfStamper ps = new PdfStamper(reader,
						new FileOutputStream(deskFile)); //生成的输出流
				AcroFields s = ps.getAcroFields();//生成的新PDF文件
				//判断打印多少页
				int a=Integer.valueOf(reportInfo.getType());//打印页数
				/*if(picList.size()>2){
					int n=(picList.size()-2)%4;
					if(n==0){
						a=(picList.size()-2)/4;
					}else{
						a=(picList.size()-2)/4+1;
					}
				}else{
					a=1;
				}*/
				if(a==1){
					insertData(so,sri,s);
					if(picList.size()==1){
						insertImage2(ps,picList.get(0).getFilePath(),s,"Text1");
					}else{
						insertImage2(ps,picList.get(0).getFilePath(),s,"Text1");
						insertImage2(ps,picList.get(1).getFilePath(),s,"Text2");	
					}
				}else{
					for(int i=1;i<a+1;i++){
						String k=String.valueOf(i);
						if(i==1){
							insertImage2(ps,picList.get(0).getFilePath(),s,"Text1");
							insertImage2(ps,picList.get(1).getFilePath(),s,"Text2");
						}else{
							for(int j=0;j<(picList.size()-(i*(i-1)));j++){
								if((i*(i-1)+j)<=picList.size() && picList.get(i*(i-1)+j)!=null){
									insertImage2(ps,picList.get(i*(i-1)+j).getFilePath(),s,"Text"+String.valueOf(i*(i-1)+j+1));
								}
							}
						}
						insertDatas(so,sri,s,k,String.valueOf(a));
						String jg=sri.getResult();
						jg=jg.replace("\r","");
						jg=jg.replace("\n","");
						int num0=jg.getBytes("UTF-8").length;	//字节长度
						String jgjs=sri.getResultDetail();	//结果解释
						jgjs=jgjs.replace("\r","");
						jgjs=jgjs.replace("\n","");
						int num1=jgjs.getBytes("UTF-8").length;	//字节长度
						int n0=0;
						int n1=0;
						String jg1="";
						String jg2="";
						String jg3="";
						
						String jgjs1="";
						String jgjs2="";
						String jgjs3="";
						//结果
						if(num0>0){
							if(num0<=88){
								jg1=jg;
							}else{
						        for (int i1=1; i1 <=(num0/88); i1++) {
					        		jg2=jg.replace(jg3, "");
					        		n0=jg2.getBytes("UTF-8").length;
					        		if(n0>=88){
					        			jg1+=comSearchService.bSubstring(jg2, 88)+"\r";
					        			jg3+=comSearchService.bSubstring(jg2, 88);
					        		}else if(n0<90 && n0>0){
					        			jg1+=comSearchService.bSubstring(jg2, 88)+"\r";
					        			jg3+=comSearchService.bSubstring(jg2, 88);
					        		}
						        }
							}
						}
						//jg1+=jg1+bSubstring(jg.replace(jg3, ""), n0);
						System.out.println(jg1+jg.replace(jg3, ""));
						if(!jg.replace(jg3, "").equals("")){
							if(sri.getResult()!=null){
								s.setField("result", jg1+jg.replace(jg3, ""));
							}else{
								s.setField("result", "");
							}
						}else{
							if(sri.getResult()!=null){
								s.setField("result", jg1);
							}else{
								s.setField("result", "");
							}
						}
						
						//结果解释
						if(num1>0){
							if(num1<=84){
								jgjs1=jgjs;
							}else{
						        for (int i1=0; i1 <=(num1/84); i1++) {
					        		jgjs2=jgjs.replace(jgjs3, "");
					        		n1=jgjs2.getBytes("UTF-8").length;
					        		if(n1>=84){
					        			jgjs1+=comSearchService.bSubstring(jgjs2, 84)+"\r";
					        			jgjs3+=comSearchService.bSubstring(jgjs2, 84);
					        		}else if(n1<84 && n1>0){
					        			jgjs1+=comSearchService.bSubstring(jgjs2, n1)+"\r";
					        			jgjs3+=comSearchService.bSubstring(jgjs2, n1);
					        		}
						        }
							}
						}
						System.out.println(jgjs1+jgjs.replace(jgjs3, ""));
						if(!jgjs.replace(jgjs3, "").equals("")){
							//jgjs1+=jgjs1+jgjs.replace(jgjs3, "");
							if(sri.getResultDetail()!=null){
								s.setField("resultDescription", jgjs1+jgjs.replace(jgjs3, ""));
							}else{
								s.setField("resultDescription", "");
							}
						}else{
							if(sri.getResultDetail()!=null){
								s.setField("resultDescription", jgjs1);
							}else{
								s.setField("resultDescription", "");
							}
						}
						if(sri.getLcjy()!=null){
							if(sri.getLcjy().equals("1")){
								s.setField("advice", "门诊随访");
							}else{
								s.setField("advice", "遗传门诊随访");
							}
						}else{
							s.setField("advice", "");
						}
						//临床症状描述
						if(so.getSubmitReasonName()!=null){
							s.setField("lczz", so.getSubmitReasonName());
						}else{
							s.setField("lczz", "");
						}
						if(sri.getCellNum()!=null){
							s.setField("cellNum", sri.getCellNum());
						}else{
							s.setField("cellNum", "");
						}
						s.setField("reportDate", format1.format(new Date()));
					}
				}
				ps.setFormFlattening(true);// 这句不能少
				ps.close();
				reader.close();
				in.close();
				
				commonDAO.update(sri);
			}
				//ReportTemplateInfo reportInfo=commonDAO.get(ReportTemplateInfo.class, sri.getId());
//			if(picList.size()>2){
//				//第一张
//				//String [] rid=sri.getReportInfoId().split(",");
//				ReportTemplateInfo reportInfo1=sri.getReportInfo();
//						//commonDAO.get(ReportTemplateInfo.class, rid[0]);
//				FileInfo fileInfo1 = sri.getTemplate();
//				FileInputStream in1 = new FileInputStream(
//						fileInfo1.getFilePath());
//				PdfReader reader1 = new PdfReader(in1);
//				String root = ConfigFileUtil.getRootPath() + File.separator
//						+ DateUtil.format(new Date(), "yyyyMMdd");
//				if (!new File(root).exists()) {
//					new File(root).mkdirs();
//				}
//				File deskFile1 = new File(tempFile, "PDF" + sri.getCode()
//						+ "-1.pdf");
//				PdfStamper ps1 = new PdfStamper(reader1,
//						new FileOutputStream(deskFile1)); // 生成的输出流
//				AcroFields s1 = ps1.getAcroFields();
//				int n=(picList.size()-2)%4;
//				int a=0;//打印张数
//				if(n==0){
//					a=(picList.size()-2)/4;
//				}else{
//					a=(picList.size()-2)/4+1;
//				}
//				//打印第一张
//				insertData(so,sri,s1,"1",String.valueOf(a+1));
//				for(int k=0;k<2;k++){
//					insertImage1(ps1,picList.get(k).getFilePath(),s1,"Text"+(String.valueOf(k+1)));
//				}
//				ps1.setFormFlattening(true);
//				ps1.close();
//				reader1.close();
//				in1.close();
//				
//				for(int i=1;i<=a;i++){
//					ReportTemplateInfo reportInfo2=commonDAO.get(ReportTemplateInfo.class, rid[i]);
//					FileInfo fileInfo2 = reportInfo2.getAttach();
//					FileInputStream in2 = new FileInputStream(
//							fileInfo2.getFilePath());
//					PdfReader reader2 = new PdfReader(in2);
//					File deskFile2 = new File(tempFile, "PDF" + sri.getCode()+"-"+String.valueOf(i+1)+".pdf");
//					PdfStamper ps2 = new PdfStamper(reader2,
//							new FileOutputStream(deskFile2)); // 生成的输出流
//					AcroFields s = ps2.getAcroFields();
//					if((picList.size()-2)-4*i>0){
//						insertData(so,sri,s,String.valueOf(i+1),String.valueOf(a+1));
//						for(int j=0;j<4;j++){
//							insertImage1(ps2,picList.get((j+2)+((i-1)*4)).getFilePath(),s,
//									"Text"+(String.valueOf(j+1)));
//						}
//						ps2.setFormFlattening(true);
//						ps2.close();
//						reader2.close();
//						in2.close();
//					}else{
//						insertData(so,sri,s,String.valueOf(i+1),String.valueOf(a+1));
//						for(int k=0;k<(picList.size()-2)-4*(i-1);k++){
//							insertImage1(ps2,picList.get(k+2+(i-1)*4).getFilePath(),
//									s,"Text"+(String.valueOf(k+1)));
//						}
//						ps2.setFormFlattening(true);
//						ps2.close();
//						reader2.close();
//						in2.close();
//					}
//				}
//			}else{
//				ReportTemplateInfo reportInfo=commonDAO.get(ReportTemplateInfo.class, sri.getReportInfoId());
//				FileInfo fileInfo = reportInfo.getAttach();
//				FileInputStream in = new FileInputStream(
//						fileInfo.getFilePath());
//				PdfReader reader = new PdfReader(in);
//				String root = ConfigFileUtil.getRootPath() + File.separator
//						+ DateUtil.format(new Date(), "yyyyMMdd");
//				if (!new File(root).exists()) {
//					new File(root).mkdirs();
//				}
//				File deskFile = new File(tempFile, "PDF" + sri.getCode()
//						+ "-1.pdf");
//				PdfStamper ps = new PdfStamper(reader,
//						new FileOutputStream(deskFile)); // 生成的输出流
//				AcroFields s = ps.getAcroFields();//PDF
//				insertData(so,sri,s,"1","1");
//				if(picList.size()==1){
//					insertImage1(ps,picList.get(0).getFilePath(),s,"Text1");
//				}else if(picList.size()==2){
//					insertImage1(ps,picList.get(0).getFilePath(),s,"Text1");
//					insertImage1(ps,picList.get(1).getFilePath(),s,"Text2");
//				}
//				ps.setFormFlattening(true);// 这句不能少
//				ps.close();
//				reader.close();
//				in.close();
//			}
		}
	}

	public List<FishFxTaskResult> findItemList(String scId) throws Exception {
		List<FishFxTaskResult> list = this.fishFxTaskDao.getResult(scId);
		return list;
	}
	/**
	 * 向PDF插入数据
	 * @param ps
	 * @param s
	 * @param path
	 * @param text
	 */
	@SuppressWarnings("static-access")
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void insertData(SampleOrder so,FishFxTaskResult sri,
			AcroFields s) throws Exception{
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		if(so.getName()!=null){
			String names=so.getName();
			s.setField("names", names);
		}else{
			s.setField("names", "");
		}
		System.out.println("姓名"+so.getName());
		if(so.getGender()!=null){
			String m="男";
			String w="女";
			if(so.getGender().equals("1")){
				s.setField("genders", m);
			}else{
				s.setField("genders", w);
			}
		}else{
			s.setField("genders", "");
		}
		System.out.println("性别"+so.getGender());
		if (so.getAge() != null) {
			s.setField("age", so.getAge().toString());
		} else {
			s.setField("age", "");
		}
		if (so.getBirthDate() != null) {
			s.setField("birthDay",format.format(so.getBirthDate()));
		} else {
			s.setField("birthDay", "");
		}
		if(sri.getSampleCode()!=null){
			s.setField("slideCode", sri.getSampleCode());
		}else{
			s.setField("slideCode", "");
		}
		s.setField("medicalNum", so.getMedicalNumber());
		if(sri.getSampleType()!=null){
			String t=sri.getSampleType();
			s.setField("sampleTypes", t);
		}else{
			s.setField("sampleTypes", "");
		}
	
		System.out.println("样本类型"+sri.getSampleType());
		if(so.getCrmCustomer()!=null){
			s.setField("crmCustomer", so.getCrmCustomer().getName());
		}else{
			s.setField("crmCustomer", "");
		}
		if (so.getSamplingDate() != null) {
			s.setField("samplingDate",
					format.format(so.getSamplingDate()));
		} else {
			s.setField("samplingDate", "");
		}
		String jg=sri.getResult();
		jg=jg.replace("\r","");
		jg=jg.replace("\n","");
		int num0=jg.getBytes("UTF-8").length;	//字节长度
		String jgjs=sri.getResultDetail();	//结果解释
		jgjs=jgjs.replace("\r","");
		jgjs=jgjs.replace("\n","");
		int num1=jgjs.getBytes("UTF-8").length;	//字节长度
		int n0=0;
		int n1=0;
		String jg1="";
		String jg2="";
		String jg3="";
		
		String jgjs1="";
		String jgjs2="";
		String jgjs3="";
		//结果
		if(num0>0){
			if(num0<=88){
				jg1=jg;
			}else{
		        for (int i1=1; i1 <=(num0/88); i1++) {
	        		jg2=jg.replace(jg3, "");
	        		n0=jg2.getBytes("UTF-8").length;
	        		if(n0>=88){
	        			jg1+=comSearchService.bSubstring(jg2, 88)+"\r";
	        			jg3+=comSearchService.bSubstring(jg2, 88);
	        		}else if(n0<90 && n0>0){
	        			jg1+=comSearchService.bSubstring(jg2, 88)+"\r";
	        			jg3+=comSearchService.bSubstring(jg2, 88);
	        		}
		        }
			}
		}
		//jg1+=jg1+bSubstring(jg.replace(jg3, ""), n0);
		System.out.println(jg1+jg.replace(jg3, ""));
		if(!jg.replace(jg3, "").equals("")){
			if(sri.getResult()!=null){
				s.setField("result", jg1+jg.replace(jg3, ""));
			}else{
				s.setField("result", "");
			}
		}else{
			if(sri.getResult()!=null){
				s.setField("result", jg1);
			}else{
				s.setField("result", "");
			}
		}
		
		//结果解释
		if(num1>0){
			if(num1<=84){
				jgjs1=jgjs;
			}else{
		        for (int i1=0; i1 <=(num1/84); i1++) {
	        		jgjs2=jgjs.replace(jgjs3, "");
	        		n1=jgjs2.getBytes("UTF-8").length;
	        		if(n1>=84){
	        			jgjs1+=comSearchService.bSubstring(jgjs2, 84)+"\r";
	        			jgjs3+=comSearchService.bSubstring(jgjs2, 84);
	        		}else if(n1<84 && n1>0){
	        			jgjs1+=comSearchService.bSubstring(jgjs2, n1)+"\r";
	        			jgjs3+=comSearchService.bSubstring(jgjs2, n1);
	        		}
		        }
			}
		}
		System.out.println(jgjs1+jgjs.replace(jgjs3, ""));
		if(!jgjs.replace(jgjs3, "").equals("")){
			//jgjs1+=jgjs1+jgjs.replace(jgjs3, "");
			if(sri.getResultDetail()!=null){
				s.setField("resultDescription", jgjs1+jgjs.replace(jgjs3, ""));
			}else{
				s.setField("resultDescription", "");
			}
		}else{
			if(sri.getResultDetail()!=null){
				s.setField("resultDescription", jgjs1);
			}else{
				s.setField("resultDescription", "");
			}
		}
		if(sri.getLcjy()!=null){
			if(sri.getLcjy().equals("1")){
				s.setField("advice", "门诊随访");
			}else{
				s.setField("advice", "遗传门诊随访");
			}
		}else{
			s.setField("advice", "");
		}
		//临床症状描述
		if(so.getSubmitReasonName()!=null){
			s.setField("lczz", so.getSubmitReasonName());
		}else{
			s.setField("lczz", "");
		}
		if(sri.getCellNum()!=null){
			s.setField("cellNum", sri.getCellNum());
		}else{
			s.setField("cellNum", "");
		}
		/*s.setField("reportDate", format.format(new Date()));
		s.setField("checkUser", sri.getFishFxTask()
				.getAcceptUser().getName());
		s.setField("confirmUser", sri.getFishFxTask()
				.getCreateUser().getName());*/
		s.setField("pageNum", "1/1");
		commonDAO.update(sri);
	}
	/**
	 * 向PDF插入数据
	 * @param ps
	 * @param s
	 * @param path
	 * @param text
	 */
	@SuppressWarnings("static-access")
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void insertDatas(SampleOrder so,FishFxTaskResult sri,
			AcroFields s,String i,String a) throws Exception{
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		if(so.getName()!=null){
			String names=so.getName();
			s.setField("names"+i, names);
		}else{
			s.setField("names"+i, "");
		}
		System.out.println("姓名"+so.getName());
		if(so.getGender()!=null){
			String m="男";
			String w="女";
			if(so.getGender().equals("1")){
				s.setField("genders"+i, m);
			}else{
				s.setField("genders"+i, w);
			}
		}else{
			s.setField("genders"+i, "");
		}
		System.out.println("性别"+so.getGender());
		if (so.getAge() != null) {
			s.setField("age"+i, so.getAge().toString());
		} else {
			s.setField("age"+i, "");
		}
		if (so.getBirthDate() != null) {
			s.setField("birthDay"+i, format.format(so.getBirthDate()));
		} else {
			s.setField("birthDay"+i, "");
		}
		if(sri.getSampleCode()!=null){
			s.setField("slideCode"+i, sri.getSampleCode());
		}else{
			s.setField("slideCode"+i, "");
		}
		s.setField("medicalNum"+i, so.getMedicalNumber());
		if(sri.getSampleType()!=null){
			String t=sri.getSampleType();
			s.setField("sampleTypes"+i, t);
		}else{
			s.setField("sampleTypes"+i, "");
		}
	
		System.out.println("样本类型"+sri.getSampleType());
		if(so.getCrmCustomer()!=null){
			s.setField("crmCustomer"+i, so.getCrmCustomer().getName());
		}else{
			s.setField("crmCustomer"+i, "");
		}
		if (so.getSamplingDate() != null) {
			s.setField("samplingDate"+i,
					format.format(so.getSamplingDate()));
		} else {
			s.setField("samplingDate"+i, "");
		}
		s.setField("pageNum"+i, i+"/"+a);
		commonDAO.update(sri);
	}
//	/**
//	 * 向PDF插入图片
//	 * @param ps
//	 * @param s
//	 * @param path
//	 * @param text
//	 */
//	@SuppressWarnings("static-access")
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void insertImage(SampleOrder so,FishFxTaskResult sri,
//			PdfStamper ps,String path,String text,String num) throws Exception{
//		try { 
//			AcroFields s = ps.getAcroFields();
//			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//			s.setField("names", so.getName());
//			s.setField("genders", so.getGender());
//			if (so.getAge() != null) {
//				s.setField("age", so.getAge().toString());
//			} else {
//				s.setField("age", "");
//			}
//			if (so.getBirthDate() != null) {
//				s.setField("birthDay", format.format(so.getBirthDate()));
//			} else {
//				s.setField("birthDay", "");
//			}
//			if(sri.getSampleCode()!=null){
//				s.setField("slideCode", sri.getSampleCode());
//			}else{
//				s.setField("slideCode", "");
//			}
//			s.setField("medicalNum", so.getMedicalNumber());
//			s.setField("pageNum", "1/"+num);
//			s.setField("sampleTypes", sri.getSampleType());
//			if(so.getCrmCustomer()!=null){
//				s.setField("crmCustomer", so.getCrmCustomer().getName());
//			}else{
//				s.setField("crmCustomer", "");
//			}
//			if (so.getSamplingDate() != null) {
//				s.setField("samplingDate",
//						format.format(so.getSamplingDate()));
//			} else {
//				s.setField("samplingDate", "");
//			}
//			if(sri.getResult()!=null){
//				s.setField("result", sri.getResult());
//			}else{
//				s.setField("result", "");
//			}
//			if(sri.getResultDetail()!=null){
//				s.setField("resultDescription", sri.getResultDetail());
//			}else{
//				s.setField("resultDescription", "");
//			}
//			if(sri.getLcjy()!=null){
//				s.setField("advice", sri.getLcjy());
//			}else{
//				s.setField("advice", "");
//			}
//			s.setField("reportDate", format.format(new Date()));
//
//			s.setField("checkUser", sri.getFishFxTask()
//					.getAcceptUser().getName());
//			s.setField("confirmUser", sri.getFishFxTask()
//					.getCreateUser().getName());
//			fishFxTaskDao.update(sri);
//			
//			
//			Image image = Image.getInstance(path);  
//			List<AcroFields.FieldPosition> list = s.getFieldPositions(text);  
//			Rectangle signRect = list.get(0).position;  
//			PdfContentByte under = ps.getOverContent(1);  
//			float x = signRect.getLeft();  
//			float y = signRect.getBottom();  
//			//System.out.println(x);  
//			//System.out.println(y);  
//			image.setAbsolutePosition(x, y);  
//			image.scaleToFit(200, 200);  
//			  
//			under.addImage(image); 
//			
//		}catch (Exception e){  
//			// TODO Auto-generated catch block  
//			e.printStackTrace();  
//		}  
//	}
	/**
	 * 向第PDF插入图片
	 * @param ps
	 * @param s
	 * @param path
	 * @param text
	 */
	@SuppressWarnings("static-access")
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void insertImage1(PdfStamper ps,String path,AcroFields s,String text) throws Exception{
		try { 
			
//			Image image = Image.getInstance(path);  
//			List<AcroFields.FieldPosition> list = s.getFieldPositions(text);  
//			Rectangle signRect = list.get(0).position;  
//			PdfContentByte under = ps.getOverContent(1);  
//			float x = signRect.getLeft();  
//			float y = signRect.getBottom();  
//			System.out.println(x);  
//			System.out.println(y);  
//			image.setAbsolutePosition(x, y);  
//			image.scaleToFit(290, 412);  
//			  
//			under.addImage(image); 
		    int pageNo = s.getFieldPositions(text).get(0).page;
		    Rectangle signRect = s.getFieldPositions(text).get(0).position;
	        float x = signRect.getLeft();
	        float y = signRect.getBottom();

	        // 读图片
	        Image image = Image.getInstance(path);
	        // 获取操作的页面
	        PdfContentByte under = ps.getOverContent(pageNo);
	        // 根据域的大小缩放图片
	        image.scaleToFit(signRect.getWidth(), signRect.getHeight());
	        System.out.println(x);  
			System.out.println(y);  
	        // 添加图片
	        image.setAbsolutePosition(x, 460);
	        under.addImage(image);

		}catch (Exception e){  
			// TODO Auto-generated catch block  
			e.printStackTrace();  
		}  
	}
	/**
	 * 向第PDF插入图片
	 * @param ps
	 * @param s
	 * @param path
	 * @param text
	 */
	@SuppressWarnings("static-access")
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void insertImage2(PdfStamper ps,String path,AcroFields s,String text) throws Exception{
		try { 
			
//			Image image = Image.getInstance(path);  
//			List<AcroFields.FieldPosition> list = s.getFieldPositions(text);  
//			Rectangle signRect = list.get(0).position;  
//			PdfContentByte under = ps.getOverContent(1);  
//			float x = signRect.getLeft();  
//			float y = signRect.getBottom();  
//			System.out.println(x);  
//			System.out.println(y);  
//			image.setAbsolutePosition(x, y);  
//			image.scaleToFit(290, 412);  
//			  
//			under.addImage(image); 
		    int pageNo = s.getFieldPositions(text).get(0).page;
		    Rectangle signRect = s.getFieldPositions(text).get(0).position;
	        float x = signRect.getLeft();
	        float y = signRect.getBottom();

	        // 读图片
	        Image image = Image.getInstance(path);
	        // 获取操作的页面
	        PdfContentByte under = ps.getOverContent(pageNo);
	        // 根据域的大小缩放图片
	        image.scaleToFit(signRect.getWidth(), signRect.getHeight());
	        System.out.println(x);  
			System.out.println(y);  
	        // 添加图片
	        image.setAbsolutePosition(x, y+40);
	        under.addImage(image);

		}catch (Exception e){  
			// TODO Auto-generated catch block  
			e.printStackTrace();  
		}  
	}
}
