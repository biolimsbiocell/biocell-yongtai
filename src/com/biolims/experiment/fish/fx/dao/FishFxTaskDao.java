package com.biolims.experiment.fish.fx.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.fish.fx.model.FishFxTask;
import com.biolims.experiment.fish.fx.model.FishFxTaskResult;
import com.biolims.experiment.fish.fx.model.FishFxTaskTemp;

@Repository
@SuppressWarnings("unchecked")
public class FishFxTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectFishFxTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from FishFxTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<FishFxTask> list = new ArrayList<FishFxTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectFishFxTaskListByState(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from FishFxTask where 1=1 and state='1'";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<FishFxTask> list = new ArrayList<FishFxTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectFishFxTaskResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from FishFxTaskResult where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and fishFxTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<FishFxTaskResult> list = new ArrayList<FishFxTaskResult>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectFishFxTaskTempList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from FishFxTaskTemp where 1=1 and state='1'";
		String key = "";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<FishFxTaskTemp> list = new ArrayList<FishFxTaskTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public List<FishFxTaskResult> getResult(String id) {
		String hql = "from FishFxTaskResult where 1=1 and fishFxTask.id='" + id
				+ "'";
		List<FishFxTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}
	public List<FishFxTaskResult> selectItem(String id) {
		String hql = "from FishFxTaskResult where 1=1 and fishFxTask.id='"
				+ id + "'";
		List<FishFxTaskResult> list = this.getSession().createQuery(hql)
				.list();

		return list;
	}
/**
 * 根据样本编号查询
 * @param code
 * @return
 */
public List<FishFxTaskResult> setResultById(String code) {
	String hql = "from FishFxTaskResult t where (isCommit is null or isCommit='') and fishFxTask.id='"
			+ code + "'";
	List<FishFxTaskResult> list = this.getSession().createQuery(hql).list();
	return list;
}

/**
 * 根据样本编号查询
 * @param codes
 * @return
 */
public List<FishFxTaskResult> setResultByIds(String[] codes) {

	String insql = "''";
	for (String code : codes) {

		insql += ",'" + code + "'";

	}

	String hql = "from FishFxTaskResult t where (isCommit is null or isCommit='') and id in ("
			+ insql + ")";
	List<FishFxTaskResult> list = this.getSession().createQuery(hql).list();
	return list;
}
}