package com.biolims.experiment.fish.fx.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.fish.fx.model.FishAgainInstance;
import com.biolims.experiment.fish.fx.model.FishFxTaskResult;
@Repository
@SuppressWarnings("unchecked")
public class FishAgainDao extends BaseHibernateDao {
	public Map<String, Object> selectFishAgainInstanceList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from FishAgainInstance where 1=1 ";
		if (mapForQuery.size()>0){
			key = map2where(mapForQuery);
		}else{
			key = " and state='1'";
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<FishAgainInstance> list = new ArrayList<FishAgainInstance>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	
/**
 * 根据样本编号查询
 * @param codes
 * @return
 */
public List<FishAgainInstance> setInstanceByIds(String[] codes) {
	String insql = "''";
	for (String code : codes) {
		insql += ",'" + code + "'";
	}
	String hql = "from FishAgainInstance t where (isCommit is null or isCommit='') and id in ("
			+ insql + ")";
	List<FishAgainInstance> list = this.getSession().createQuery(hql).list();
	return list;
   }

}