package com.biolims.experiment.fish.fishabnormal.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.bobs.sample.model.BobsSampleTemp;
import com.biolims.experiment.fish.dao.SamplePreTaskDao;
import com.biolims.experiment.fish.fishabnormal.dao.FishAbnormalDao;
import com.biolims.experiment.fish.fishabnormal.model.FishAbnormal;
import com.biolims.experiment.fish.model.FishProTaskTemp;
import com.biolims.experiment.fish.model.SamplePreTaskTemp;
import com.biolims.experiment.karyoabnormal.model.KaryoAbnormal;
import com.biolims.experiment.karyoget.model.KaryoGetTaskTemp;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.template.model.Template;
import com.biolims.util.JsonUtils;
import com.opensymphony.xwork2.ActionContext;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class FishAbnormalService {
	@Resource
	private SamplePreTaskDao samplePreTaskDao;
	@Resource
	private FishAbnormalDao fishAbnormalDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private NextFlowDao nextFlowDao;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findFishAbnormalList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = fishAbnormalDao.selectFishAbnormalList(mapForQuery,startNum,
				limitNum, dir,sort);
		List<FishAbnormal> list = (List<FishAbnormal>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveFishAbnormal(String itemDataJson) throws Exception {
		List<FishAbnormal> saveItems = new ArrayList<FishAbnormal>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for (Map<String, Object> map : list) {
			FishAbnormal scp = new FishAbnormal();
			// 将map信息读入实体类
			scp = (FishAbnormal) fishAbnormalDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);

			saveItems.add(scp);
			fishAbnormalDao.saveOrUpdate(scp);
	/*		if (scp != null) {
				if (scp.getResult() != null && scp.getIsSubmit() != null &&
					scp.getNextFlowId()!=null && !scp.getResult().equals("") 
					&& !scp.getIsSubmit().equals("") && !scp.getNextFlowId().equals("")){
					if(scp.getIsSubmit().equals("1")){
						if(scp.getResult().equals("1")){
							if (scp.getNextFlowId().equals("0009")) {// 样本入库
								SampleInItemTemp st = new SampleInItemTemp();
								st.setCode(scp.getCode());
								st.setSampleCode(scp.getSampleCode());
								st.setState("1");
								commonDAO.saveOrUpdate(st);
								// 入库，改变SampleInfo中原始样本的状态为“待入库”
								SampleInfo sf = sampleInfoMainDao
										.findSampleInfo(scp.getCode());
								if (sf != null) {
									sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW);
									sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW_NAME);
								}
								scp.setState("2");
							} else if (scp.getNextFlowId().equals("0012")) {// 暂停
								// 暂停，改变SampleInfo中原始样本的状态为“实验暂停”
								SampleInfo sf = sampleInfoMainDao
										.findSampleInfo(scp.getCode());
								if (sf != null) {
									sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
									sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
								}
								
							} else if (scp.getNextFlowId().equals("0013")) {// 终止
								// 终止，改变SampleInfo中原始样本的状态为“实验终止”
								SampleInfo sf = sampleInfoMainDao
										.findSampleInfo(scp.getCode());
								if (sf != null) {
									sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
									sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
								}
							}else {
								// 得到下一步流向的相关表单
								List<NextFlow> list_nextFlow = nextFlowDao
										.seletNextFlowById(scp.getNextFlowId());
								for (NextFlow n : list_nextFlow) {
									Object o = Class.forName(
											n.getApplicationTypeTable()
													.getClassPath())
											.newInstance();
									scp.setState("1");
									sampleInputService.copy(o, scp);
								}
							}
							scp.setState("2");
						}
					}
				}
			}*/
		}
		fishAbnormalDao.saveOrUpdateAll(saveItems);
	}
	
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delFishAbnormal(String[] ids) throws Exception {
		for (String id : ids) {
			FishAbnormal scp = fishAbnormalDao.get(FishAbnormal.class, id);
			fishAbnormalDao.delete(scp);
		}
	}
	
	/**
	 * 提交样本
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitSample(String[] ids) throws Exception {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// 获取结果表样本信息
		List<FishAbnormal> list = this.fishAbnormalDao.setResultByIds(ids);
		for (FishAbnormal scp : list) {
			
			
			if (scp != null) {
				if (scp.getResult() != null  &&
					scp.getNextFlowId()!=null && !scp.getResult().equals("") 
					&& !scp.getNextFlowId().equals("")){
				//	if(scp.getIsSubmit().equals("1")){
						if(scp.getResult().equals("1")){
							if (scp.getNextFlowId().equals("0009")) {// 样本入库
								SampleInItemTemp st = new SampleInItemTemp();
								st.setCode(scp.getCode());
								st.setSampleCode(scp.getSampleCode());
								st.setState("1");
								commonDAO.saveOrUpdate(st);
								// 入库，改变SampleInfo中原始样本的状态为“待入库”
								SampleInfo sf = sampleInfoMainDao
										.findSampleInfo(scp.getCode());
								if (sf != null) {
									sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW);
									sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW_NAME);
								}
								scp.setState("2");
							} else if (scp.getNextFlowId().equals("0012")) {// 暂停
								// 暂停，改变SampleInfo中原始样本的状态为“实验暂停”
								SampleInfo sf = sampleInfoMainDao
										.findSampleInfo(scp.getCode());
								if (sf != null) {
									sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
									sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
								}
								
							} else if (scp.getNextFlowId().equals("0013")) {// 终止
								// 终止，改变SampleInfo中原始样本的状态为“实验终止”
								SampleInfo sf = sampleInfoMainDao
										.findSampleInfo(scp.getCode());
								if (sf != null) {
									sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
									sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
								}
							}else if (scp.getNextFlowId().equals("0039")) {//Fish样本预处理
								List<SamplePreTaskTemp> sptList=samplePreTaskDao.getTempByCode(scp.getCode());
								if(sptList.size()>0){
									SamplePreTaskTemp bs=sptList.get(0);
									bs.setState("1");
								}
							}else {
								// 得到下一步流向的相关表单
								List<NextFlow> list_nextFlow = nextFlowDao
										.seletNextFlowById(scp.getNextFlowId());
								for (NextFlow n : list_nextFlow) {
									Object o = Class.forName(
											n.getApplicationTypeTable()
													.getClassPath())
											.newInstance();
									scp.setState("1");
									sampleInputService.copy(o, scp);
								}
							}
							scp.setState("2");
						}
					//}
				}
			}scp.setIsSubmit("1");
			
			
//	if (scp != null) {
//		FishProTaskTemp ft= new FishProTaskTemp();
//			ft.setCode(scp.getCode());
//			ft.setSampleCode(scp.getSampleCode());
//			ft.setProductId(scp.getProductId());
//			ft.setProductName(scp.getProductName());
//			ft.setSampleType(scp.getSampleType());
//			ft.setState("1");
//			ft.setAcceptDate(scp.getAcceptDate());
//			fishAbnormalDao.saveOrUpdate(ft);
//			
//			scp.setIsSubmit("1");
//			fishAbnormalDao.saveOrUpdate(scp);
//	         }scp.setIsSubmit("1");
		}
	}
}
