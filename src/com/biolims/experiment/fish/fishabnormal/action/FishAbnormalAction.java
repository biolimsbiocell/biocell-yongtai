package com.biolims.experiment.fish.fishabnormal.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.fish.fishabnormal.model.FishAbnormal;
import com.biolims.experiment.fish.fishabnormal.service.FishAbnormalService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/experiment/fishAbnormal")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class FishAbnormalAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "248907";
	@Autowired
	private FishAbnormalService fishAbnormalService;
	private FishAbnormal fishAbnormal = new FishAbnormal();
	
	@Action(value = "showFishAbnormalList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showFishAbnormalList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/fish/fishabnormal/fishAbnormal.jsp");
	}

	@Action(value = "showFishAbnormalListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showFishAbnormalListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = fishAbnormalService.findFishAbnormalList(map2Query, startNum, limitNum, dir, sort);
		Long total = (Long) result.get("total");
		List<FishAbnormal> list = (List<FishAbnormal>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("patientName", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("acceptDate", "yyyy-MM-dd");
		map.put("reportDate", "yyyy-MM-dd");
		map.put("orderId", "");
		map.put("nextFlow", "");
		map.put("nextFlowId", "");
		map.put("result", "");
		map.put("isSubmit", "");
		map.put("taskId", "");
		map.put("taskName", "");
		map.put("sampleType", "");
		map.put("state", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
	}
	
	// 提交样本
	@Action(value = "submitSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void submitSample() throws Exception {
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.fishAbnormalService.submitSample(ids);

			result.put("success", true);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}	
	
	
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delFishAbnormal")
	public void delFishAbnormal() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			fishAbnormalService.delFishAbnormal(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	
	/**
	 * 保存异常样本
	 * @throws Exception
	 */
	@Action(value = "saveAbnormal")
	public void saveAbnormal() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getRequest().getParameter("itemDataJson");
			fishAbnormalService.saveFishAbnormal(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public FishAbnormalService getFishAbnormalService() {
		return fishAbnormalService;
	}

	public void setFishAbnormalService(FishAbnormalService fishAbnormalService) {
		this.fishAbnormalService = fishAbnormalService;
	}

	public FishAbnormal getFishAbnormal() {
		return fishAbnormal;
	}

	public void setFishAbnormal(FishAbnormal fishAbnormal) {
		this.fishAbnormal = fishAbnormal;
	}

}
