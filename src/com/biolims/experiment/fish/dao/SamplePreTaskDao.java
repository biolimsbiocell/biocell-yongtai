package com.biolims.experiment.fish.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.bobs.sample.model.BobsSampleTemp;
import com.biolims.experiment.fish.model.SamplePreTask;
import com.biolims.experiment.fish.model.SamplePreTaskCos;
import com.biolims.experiment.fish.model.SamplePreTaskItem;
import com.biolims.experiment.fish.model.SamplePreTaskReagent;
import com.biolims.experiment.fish.model.SamplePreTaskResult;
import com.biolims.experiment.fish.model.SamplePreTaskTemp;
import com.biolims.experiment.fish.model.SamplePreTaskTemplate;
import com.biolims.experiment.producer.model.ProducerTaskItem;
import com.biolims.experiment.producer.model.ProducerTaskReagent;
import com.biolims.experiment.snpjc.sample.model.SnpSamplePdhResult;
@Repository
@SuppressWarnings("unchecked")
public class SamplePreTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectSamplePreTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from SamplePreTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SamplePreTask> list = new ArrayList<SamplePreTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}else{
				
				key = key + " order by id desc";
				
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	public List<SamplePreTaskItem> selectPlasmaItemList(String scId) throws Exception {
		String hql = "from SamplePreTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and samplePreTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<SamplePreTaskItem> list = new ArrayList<SamplePreTaskItem>();
		if (total > 0) {
				list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}
	public Map<String, Object> selectSamplePreTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SamplePreTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and samplePreTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SamplePreTaskItem> list = new ArrayList<SamplePreTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by  " + sort + " " + dir;
			} else {
				key = key + " order by orderNumber asc ";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectBloodSampleInfoList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SamplePreTaskResult where 1=1";
		String key = "";
		if (scId != null)
			key = key + " and samplePreTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SamplePreTaskResult> list = new ArrayList<SamplePreTaskResult>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 模板对应的子表
	public Map<String, Object> selectSamplePreTaskTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SamplePreTaskTemplate where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and samplePreTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SamplePreTaskTemplate> list = new ArrayList<SamplePreTaskTemplate>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectSamplePreTaskReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SamplePreTaskReagent where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and samplePreTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SamplePreTaskReagent> list = new ArrayList<SamplePreTaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectSamplePreTaskReagentListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		String hql = "from SamplePreTaskReagent where 1=1 and itemId='" + itemId
				+ "'";
		String key = "";
		if (scId != null)
			key = key + " and samplePreTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SamplePreTaskReagent> list = new ArrayList<SamplePreTaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectSamplePreTaskCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SamplePreTaskCos where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and samplePreTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SamplePreTaskCos> list = new ArrayList<SamplePreTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据选中的步骤查询相关的试剂明细
	 * @param id
	 * @param code
	 * @return
	 */
	public Map<String, Object> setReagent(String id, String code) {
		String hql = "from SamplePreTaskReagent t where 1=1 and t.samplePreTask='"
				+ id + "' and t.itemId='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<SamplePreTaskReagent> list = this.getSession().createQuery(hql)
				.list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/*
	 * 根据主表ID查询试剂明细
	 */
	public List<SamplePreTaskReagent> setReagentList(String code) {
		String hql = "from SamplePreTaskReagent where 1=1 and samplePreTask='"
				+ code + "'";
		List<SamplePreTaskReagent> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	/**
	 * 根据选中的步骤查询相关的设备明细
	 * @param id
	 * @param code
	 * @return
	 */
	public Map<String, Object> setCos(String id, String code) {
		String hql = "from SamplePreTaskCos t where 1=1 and t.samplePreTask='"
				+ id + "' and t.itemId='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<SamplePreTaskCos> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据明细查询设备明细
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param itemId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectSamplePreTaskCosListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		String hql = "from SamplePreTaskCos t where 1=1 and itemId='" + itemId
				+ "'";
		String key = "";
		if (scId != null)
			key = key + " and samplePreTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SamplePreTaskCos> list = new ArrayList<SamplePreTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// public List<BloodSampleTask> selectBloodList(){
	// String hql = "from BloodSampleTask t where 1=1 and t.state=1";
	// List<BloodSampleTask> list = this.getSession().createQuery(hql).list();
	// return list;
	// }

	public List<SamplePreTaskResult> selectResultList(String id) {
		String hql = "from SamplePreTaskResult where 1=1 and resultJudge='1' and samplePreTask='"
				+ id + "'";
		List<SamplePreTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/*// 根据状态获取样本
	public Map<String, Object> selectWKAcceptTempList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from WKAcceptTemp  where 1=1 and state is null";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql).uniqueResult();
		List<SamplePreTaskReagent> list = new ArrayList<SamplePreTaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql).list();
			} else {
				list = this.getSession().createQuery(hql)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}*/

	/*// 合格血浆流到血浆接收
	public Map<String, Object> selectSuccessPlasmaList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SamplePlasmaInfo  where 1=1 and resultJudge='1'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql).uniqueResult();
		List<SamplePlasmaInfo> list = new ArrayList<SamplePlasmaInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql).list();
			} else {
				list = this.getSession().createQuery(hql)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}*/

	/**
	 * 修改结果子表的样本状态
	 * @param code
	 * @return
	 */
	public List<SamplePreTaskResult> findSamplePreTaskResult(String code) {
		String hql = "from SamplePreTaskResult  where 1=1 and samplePreTask.id='"
				+ code + "'";
		List<SamplePreTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询从样本接收状态完成的的样本
	 * @return
	 */
	public List<SamplePreTaskTemp> findBeforeResultList() {
		String hql = "from SamplePreTaskTemp  where 1=1 and state='1' ";
		List<SamplePreTaskTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询不通过的异常样本
	 * @param code
	 * @return
	 */
	public List<SamplePreTaskResult> selectResultListByMethod(String code) {
		String hql = "from SamplePreTaskResult  where 1=1 and method=0 and samplePreTask='"
				+ code + "'";
		List<SamplePreTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询全部样本
	 * @param code
	 * @return
	 */
	public List<SamplePreTaskResult> selectAllResultList(String code) {
		String hql = "from SamplePreTaskResult  where 1=1 and samplePreTask='"
				+ code + "'";
		List<SamplePreTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 *  获取样本接收的样本
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectSamplePreTaskTempList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from SamplePreTaskTemp where 1=1 and state='1'";
		if (mapForQuery != null){
			key = map2where(mapForQuery);
		}
//		else{
//			key=" and state='1'";
//		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SamplePreTaskTemp> list = new ArrayList<SamplePreTaskTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}else{
				
				key = key + " order by id desc";
				
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	// //下一步流向
	// public List<BloodSampleInfo> selectNextFlowList(String code){
	// String hql =
	// "from BloodSampleInfo  where 1=1 and resultJudge=1 and bloodSampleTask='"+code+"'";
	// List<BloodSampleInfo> list = this.getSession().createQuery(hql).list();
	// return list;
	// }
	/**
	 * 根据ID相关样本的数量
	 * @param code
	 * @return
	 */
	public Long selectCountMax(String code) {
		String hql = "from SamplePreTaskItem t where 1=1 and t.samplePreTask.id='"
				+ code + "'";
		Long list = (Long) this.getSession()
				.createQuery("select count(t.id) " + hql).uniqueResult();
		return list;
	}

	/**
	 * 根据ID相关样本的提取次数
	 * @param code
	 * @return
	 */
	public Long selectCount(String code) {
		String hql = "from SamplePreTaskItem t where 1=1 and t.code='" + code
				+ "'";
		Long list = (Long) this.getSession()
				.createQuery("select count(t.code)" + hql).uniqueResult();
		return list;
	}

	
	public List<SamplePreTaskItem> selectBloodItemToLeftList() {
		String hql = "from SamplePreTaskItem  where 1=1 and code like '%-1%' and resultJudge=0";
		List<SamplePreTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 不合格的样本 在完成的状态下 回到左侧页面
	 * @return
	 */
	public List<SamplePreTaskResult> selectBloodSampleInfoList() {
		String hql = "from SamplePreTaskResult  where 1=1 and code like '%-1%' and resultJudge=0";
		List<SamplePreTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据样本编号查询info子表信息
	 * @param code
	 * @return
	 */
	public List<SamplePreTaskResult> findBloodSample(String code) {
		String hql = "from SamplePreTaskResult t where 1=1 and t.sampleCode='"
				+ code + "'";
		List<SamplePreTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据临时表id查询样本
	 * @param tempId
	 * @return
	 */
	public String findTemp(String tempId) {
		String hql = "from SamplePreTaskResult  where 1=1 and tempId='" + tempId
				+ "'";
		String str = (String) this.getSession()
				.createQuery("select distinct tempId " + hql).uniqueResult();
		return str;
	}

	/**
	 * 查询同一主表，同一个样本
	 * 
	 * @param code
	 * @return
	 */
	public List<SamplePreTaskResult> getBloodSampleList(String code, String id) {
		String hql = "from SamplePreTaskResult t where 1=1 and t.sampleCode='"
				+ code + "' and nextFlow='0' and bloodSampleTask='" + id + "'";
		List<SamplePreTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据样本编号查询
	 * @param code
	 * @return
	 */
	public List<SamplePreTaskResult> setResultById(String code) {
		String hql = "from SamplePreTaskResult t where (submit is null or submit='') and samplePreTask.id='"
				+ code + "'";
		List<SamplePreTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据样本编号查询
	 * @param codes
	 * @return
	 */
	public List<SamplePreTaskResult> setResultByIds(String[] codes) {

		String insql = "''";
		for (String code : codes) {

			insql += ",'" + code + "'";

		}

		String hql = "from SamplePreTaskResult t where (submit is null or submit='') and id in ("
				+ insql + ")";
		List<SamplePreTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据主表编号Item信息
	 * @param code
	 * @return
	 */
	public List<SamplePreTaskItem> getItem(String id) {
		String hql = "from SamplePreTaskItem t where 1=1  and samplePreTask='"+ id + "'";
		List<SamplePreTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据主表编号Reagent信息
	 * @param code
	 * @return
	 */
	public List<SamplePreTaskReagent> getReagent(String id) {
		String hql = "from SamplePreTaskReagent t where 1=1  and samplePreTask='"+ id + "'";
		List<SamplePreTaskReagent> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	/**
	 * 根据样本编号查询临时表样本信息
	 * @param code
	 * @return
	 */
	public List<SamplePreTaskTemp> getTempByCode(String code) {
		String hql = "from SamplePreTaskTemp t where 1=1  and state='2' and code='"+ code + "'";
		List<SamplePreTaskTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}
}