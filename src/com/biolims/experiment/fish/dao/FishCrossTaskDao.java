package com.biolims.experiment.fish.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.fish.model.FishCrossTask;
import com.biolims.experiment.fish.model.FishCrossTaskCos;
import com.biolims.experiment.fish.model.FishCrossTaskItem;
import com.biolims.experiment.fish.model.FishCrossTaskReagent;
import com.biolims.experiment.fish.model.FishCrossTaskResult;
import com.biolims.experiment.fish.model.FishCrossTaskTemp;
import com.biolims.experiment.fish.model.FishCrossTaskTemplate;
import com.biolims.experiment.fish.model.FishProTaskItem;
import com.biolims.experiment.fish.model.FishProTaskReagent;
import com.biolims.experiment.fish.model.FishProTaskResult;
import com.biolims.experiment.fish.model.FishSjTaskResult;
@Repository
@SuppressWarnings("unchecked")
public class FishCrossTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectFishCrossTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from FishCrossTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<FishCrossTask> list = new ArrayList<FishCrossTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}else{
				
				key = key + " order by id desc";
				
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	public Map<String, Object> selectFishCrossTaskListByState(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from FishCrossTask where 1=1";
		if (mapForQuery.size()>0){
			key = map2where(mapForQuery);
		}else{
			key="  and state='1'";
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<FishCrossTask> list = new ArrayList<FishCrossTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}else{
				
				key = key + " order by id desc";
				
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	
	public List<FishCrossTaskItem> selectfishCrossTaskItemList(String scId) throws Exception {
		String hql = "from FishCrossTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and fishCrossTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<FishCrossTaskItem> list = new ArrayList<FishCrossTaskItem>();
		if (total > 0) {
				list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}
	public Map<String, Object> selectFishCrossTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from FishCrossTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and fishCrossTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<FishCrossTaskItem> list = new ArrayList<FishCrossTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by  " + sort + " " + dir;
			} else {
				key = key + " order by orderNumber asc ";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectFishCrossTaskResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from FishCrossTaskResult where 1=1";
		String key = "";
		if (scId != null)
			key = key + " and fishCrossTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<FishCrossTaskResult> list = new ArrayList<FishCrossTaskResult>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 模板对应的子表
	public Map<String, Object> selectFishCrossTaskTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from FishCrossTaskTemplate where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and fishCrossTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<FishCrossTaskTemplate> list = new ArrayList<FishCrossTaskTemplate>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectFishCrossTaskReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from FishCrossTaskReagent where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and fishCrossTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<FishCrossTaskReagent> list = new ArrayList<FishCrossTaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectFishCrossTaskReagentListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		String hql = "from FishCrossTaskReagent where 1=1 and itemId='" + itemId
				+ "'";
		String key = "";
		if (scId != null)
			key = key + " and fishCrossTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<FishCrossTaskReagent> list = new ArrayList<FishCrossTaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectFishCrossTaskCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from FishCrossTaskCos where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and fishCrossTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<FishCrossTaskCos> list = new ArrayList<FishCrossTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据选中的步骤查询相关的试剂明细
	 * @param id
	 * @param code
	 * @return
	 */
	public Map<String, Object> setReagent(String id, String code) {
		String hql = "from FishCrossTaskReagent t where 1=1 and t.fishCrossTask='"
				+ id + "' and t.itemId='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<FishCrossTaskReagent> list = this.getSession().createQuery(hql)
				.list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/*
	 * 根据主表ID查询试剂明细
	 */
	public List<FishCrossTaskReagent> setReagentList(String code) {
		String hql = "from FishCrossTaskReagent where 1=1 and fishCrossTask='"
				+ code + "'";
		List<FishCrossTaskReagent> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	/**
	 * 根据选中的步骤查询相关的设备明细
	 * @param id
	 * @param code
	 * @return
	 */
	public Map<String, Object> setCos(String id, String code) {
		String hql = "from FishCrossTaskCos t where 1=1 and t.fishCrossTask='"
				+ id + "' and t.itemId='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<FishCrossTaskCos> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据明细查询设备明细
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param itemId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectFishCrossTaskCosListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		String hql = "from FishCrossTaskCos t where 1=1 and itemId='" + itemId
				+ "'";
		String key = "";
		if (scId != null)
			key = key + " and fishCrossTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<FishCrossTaskCos> list = new ArrayList<FishCrossTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public List<FishCrossTaskResult> selectResultList(String id) {
		String hql = "from FishCrossTaskResult where 1=1 and resultJudge='1' and fishCrossTask='"
				+ id + "'";
		List<FishCrossTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 修改结果子表的样本状态
	 * @param code
	 * @return
	 */
	public List<FishCrossTaskResult> findFishCrossTaskResult(String code) {
		String hql = "from FishCrossTaskResult  where 1=1 and fishCrossTask.id='"
				+ code + "'";
		List<FishCrossTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询从样本接收状态完成的的样本
	 * @return
	 */
	public List<FishCrossTaskTemp> findBeforeResultList() {
		String hql = "from FishCrossTaskTemp  where 1=1 and state='1' ";
		List<FishCrossTaskTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询不通过的异常样本
	 * @param code
	 * @return
	 */
	public List<FishCrossTaskResult> selectResultListByMethod(String code) {
		String hql = "from FishCrossTaskResult  where 1=1 and method=0 and fishCrossTask='"
				+ code + "'";
		List<FishCrossTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询全部样本
	 * @param code
	 * @return
	 */
	public List<FishCrossTaskResult> selectAllResultList(String code) {
		String hql = "from FishCrossTaskResult  where 1=1 and fishCrossTask='"
				+ code + "'";
		List<FishCrossTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 *  获取样本接收的样本
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectFishCrossTaskTempList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from FishCrossTaskTemp where 1=1 and state='1'";
		if (mapForQuery != null){
			key = map2where(mapForQuery);
		}
//		else{
//			key=" and state='1'";
//		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<FishCrossTaskTemp> list = new ArrayList<FishCrossTaskTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}else{
				
				key = key + " order by id desc";
				
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	
	/**
	 * 根据ID相关样本的数量
	 * @param code
	 * @return
	 */
	public Long selectCountMax(String code) {
		String hql = "from FishCrossTaskItem t where 1=1 and t.fishCrossTask.id='"
				+ code + "'";
		Long list = (Long) this.getSession()
				.createQuery("select count(t.id) " + hql).uniqueResult();
		return list;
	}

	/**
	 * 根据ID相关样本的提取次数
	 * @param code
	 * @return
	 */
	public Long selectCount(String code) {
		String hql = "from FishCrossTaskItem t where 1=1 and t.code='" + code
				+ "'";
		Long list = (Long) this.getSession()
				.createQuery("select count(t.code)" + hql).uniqueResult();
		return list;
	}

	
	public List<FishCrossTaskItem> selectFishCrossTaskResultToLeftList() {
		String hql = "from FishCrossTaskItem  where 1=1 and code like '%-1%' and resultJudge=0";
		List<FishCrossTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 不合格的样本 在完成的状态下 回到左侧页面
	 * @return
	 */
	public List<FishCrossTaskResult> selectFishCrossTaskResultList() {
		String hql = "from FishCrossTaskResult  where 1=1 and code like '%-1%' and resultJudge=0";
		List<FishCrossTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据样本编号查询info子表信息
	 * @param code
	 * @return
	 */
	public List<FishCrossTaskResult> findBloodSample(String code) {
		String hql = "from FishCrossTaskResult t where 1=1 and t.sampleCode='"
				+ code + "'";
		List<FishCrossTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据临时表id查询样本
	 * @param tempId
	 * @return
	 */
	public String findTemp(String tempId) {
		String hql = "from FishCrossTaskResult  where 1=1 and tempId='" + tempId
				+ "'";
		String str = (String) this.getSession()
				.createQuery("select distinct tempId " + hql).uniqueResult();
		return str;
	}

	/**
	 * 查询同一主表，同一个样本
	 * 
	 * @param code
	 * @return
	 */
	public List<FishCrossTaskResult> getBloodSampleList(String code, String id) {
		String hql = "from FishCrossTaskResult t where 1=1 and t.sampleCode='"
				+ code + "' and nextFlow='0' and fishCrossTask='" + id + "'";
		List<FishCrossTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	/**
	 * 选择杂交洗脱加载info子表信息
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> setFishCrossTaskResultList(String code) throws Exception {
		String hql = "from FishCrossTaskResult t where 1=1 and submit='1' and result='1'";
		String key = "";
		if (code != null)
			key = key + " and  t.fishCrossTask in(" + code + ")";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<FishCrossTaskResult> list = this.getSession().createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	
//	public Map<String, Object> setFishCrossTaskResultList(String code) throws Exception {
//		String hql = "from FishCrossTaskResult t where 1=1 and submit='1' and result='1'";
//		String key = "";
//		if (code != null)
//			key = key + " and  t.fishCrossTask in (" + code + ")";
//		
//		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
//		List<FishCrossTaskResult> list = this.getSession().createQuery(hql + key).list();
//		Map<String, Object> result = new HashMap<String, Object>();
//		result.put("total", total);
//		result.put("list", list);
//		return result;
//	}
	
	/**
	 * 根据主表编号Item信息
	 * @param code
	 * @return
	 */
	public List<FishCrossTaskItem> getItem(String id) {
		String hql = "from FishCrossTaskItem t where 1=1  and fishCrossTask='"+ id + "'";
		List<FishCrossTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据主表编号Reagent信息
	 * @param code
	 * @return
	 */
	public List<FishCrossTaskReagent> getReagent(String id) {
		String hql = "from FishCrossTaskReagent t where 1=1  and fishCrossTask='"+ id + "'";
		List<FishCrossTaskReagent> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据样本编号查询
	 * @param code
	 * @return
	 */
	public List<FishCrossTaskResult> setResultById(String code) {
		String hql = "from FishCrossTaskResult t where (submit is null or submit = '') and fishCrossTask.id='"
				+ code + "'";
		List<FishCrossTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据样本编号查询
	 * @param codes
	 * @return
	 */
	public List<FishCrossTaskResult> setResultByIds(String[] codes) {
		String insql = "''";
		for (String code : codes) {
			insql += ",'" + code + "'";
		}
		String hql = "from FishCrossTaskResult t where (submit is null or submit = '') and id in ("
				+ insql + ")";
		List<FishCrossTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}
}