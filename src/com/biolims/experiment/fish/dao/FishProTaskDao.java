package com.biolims.experiment.fish.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.fish.model.FishProTask;
import com.biolims.experiment.fish.model.FishProTaskCos;
import com.biolims.experiment.fish.model.FishProTaskItem;
import com.biolims.experiment.fish.model.FishProTaskReagent;
import com.biolims.experiment.fish.model.FishProTaskResult;
import com.biolims.experiment.fish.model.FishProTaskTemp;
import com.biolims.experiment.fish.model.FishProTaskTemplate;
import com.biolims.experiment.fish.model.SamplePreTaskItem;
import com.biolims.experiment.fish.model.SamplePreTaskReagent;
import com.biolims.experiment.fish.model.SamplePreTaskResult;
@Repository
@SuppressWarnings("unchecked")
public class FishProTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectFishProTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from FishProTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<FishProTask> list = new ArrayList<FishProTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}else{
				
				key = key + " order by id desc";
				
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	public List<FishProTaskItem> selectfishProTaskItemList(String scId) throws Exception {
		String hql = "from FishProTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and fishProTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<FishProTaskItem> list = new ArrayList<FishProTaskItem>();
		if (total > 0) {
				list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}
	public Map<String, Object> selectFishProTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from FishProTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and fishProTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<FishProTaskItem> list = new ArrayList<FishProTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by  " + sort + " " + dir;
			} else {
				key = key + " order by orderNumber asc ";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectFishProTaskResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from FishProTaskResult where 1=1";
		String key = "";
		if (scId != null)
			key = key + " and fishProTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<FishProTaskResult> list = new ArrayList<FishProTaskResult>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 模板对应的子表
	public Map<String, Object> selectFishProTaskTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from FishProTaskTemplate where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and fishProTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<FishProTaskTemplate> list = new ArrayList<FishProTaskTemplate>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectFishProTaskReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from FishProTaskReagent where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and fishProTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<FishProTaskReagent> list = new ArrayList<FishProTaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectFishProTaskReagentListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		String hql = "from FishProTaskReagent where 1=1 and itemId='" + itemId
				+ "'";
		String key = "";
		if (scId != null)
			key = key + " and fishProTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<FishProTaskReagent> list = new ArrayList<FishProTaskReagent>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectFishProTaskCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from FishProTaskCos where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and fishProTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<FishProTaskCos> list = new ArrayList<FishProTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据选中的步骤查询相关的试剂明细
	 * @param id
	 * @param code
	 * @return
	 */
	public Map<String, Object> setReagent(String id, String code) {
		String hql = "from FishProTaskReagent t where 1=1 and t.fishProTask='"
				+ id + "' and t.itemId='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<FishProTaskReagent> list = this.getSession().createQuery(hql)
				.list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/*
	 * 根据主表ID查询试剂明细
	 */
	public List<FishProTaskReagent> setReagentList(String code) {
		String hql = "from FishProTaskReagent where 1=1 and fishProTask='"
				+ code + "'";
		List<FishProTaskReagent> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	/**
	 * 根据选中的步骤查询相关的设备明细
	 * @param id
	 * @param code
	 * @return
	 */
	public Map<String, Object> setCos(String id, String code) {
		String hql = "from FishProTaskCos t where 1=1 and t.fishProTask='"
				+ id + "' and t.itemId='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<FishProTaskCos> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据明细查询设备明细
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param itemId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectFishProTaskCosListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		String hql = "from FishProTaskCos t where 1=1 and itemId='" + itemId
				+ "'";
		String key = "";
		if (scId != null)
			key = key + " and fishProTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<FishProTaskCos> list = new ArrayList<FishProTaskCos>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public List<FishProTaskResult> selectResultList(String id) {
		String hql = "from FishProTaskResult where 1=1 and resultJudge='1' and fishProTask='"
				+ id + "'";
		List<FishProTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 修改结果子表的样本状态
	 * @param code
	 * @return
	 */
	public List<FishProTaskResult> findFishProTaskResult(String code) {
		String hql = "from FishProTaskResult  where 1=1 and fishProTask.id='"
				+ code + "'";
		List<FishProTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询从样本接收状态完成的的样本
	 * @return
	 */
	public List<FishProTaskTemp> findBeforeResultList() {
		String hql = "from FishProTaskTemp  where 1=1 and state='1' ";
		List<FishProTaskTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询不通过的异常样本
	 * @param code
	 * @return
	 */
	public List<FishProTaskResult> selectResultListByMethod(String code) {
		String hql = "from FishProTaskResult  where 1=1 and method=0 and fishProTask='"
				+ code + "'";
		List<FishProTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 查询全部样本
	 * @param code
	 * @return
	 */
	public List<FishProTaskResult> selectAllResultList(String code) {
		String hql = "from FishProTaskResult  where 1=1 and fishProTask='"
				+ code + "'";
		List<FishProTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 *  获取样本接收的样本
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectFishProTaskTempList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from FishProTaskTemp where 1=1 and state='1'";
		if (mapForQuery != null){
			key = map2where(mapForQuery);
		}
//		else{
//			key=" and state='1'";
//		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<FishProTaskTemp> list = new ArrayList<FishProTaskTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}else{
				
				key = key + " order by id desc";
				
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	
	/**
	 * 根据ID相关样本的数量
	 * @param code
	 * @return
	 */
	public Long selectCountMax(String code) {
		String hql = "from FishProTaskItem t where 1=1 and t.fishProTask.id='"
				+ code + "'";
		Long list = (Long) this.getSession()
				.createQuery("select count(t.id) " + hql).uniqueResult();
		return list;
	}

	/**
	 * 根据ID相关样本的提取次数
	 * @param code
	 * @return
	 */
	public Long selectCount(String code) {
		String hql = "from FishProTaskItem t where 1=1 and t.code='" + code
				+ "'";
		Long list = (Long) this.getSession()
				.createQuery("select count(t.code)" + hql).uniqueResult();
		return list;
	}

	
	public List<FishProTaskItem> selectFishProTaskResultToLeftList() {
		String hql = "from FishProTaskItem  where 1=1 and code like '%-1%' and resultJudge=0";
		List<FishProTaskItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 不合格的样本 在完成的状态下 回到左侧页面
	 * @return
	 */
	public List<FishProTaskResult> selectFishProTaskResultList() {
		String hql = "from FishProTaskResult  where 1=1 and code like '%-1%' and resultJudge=0";
		List<FishProTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据样本编号查询info子表信息
	 * @param code
	 * @return
	 */
	public List<FishProTaskResult> findBloodSample(String code) {
		String hql = "from FishProTaskResult t where 1=1 and t.sampleCode='"
				+ code + "'";
		List<FishProTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据临时表id查询样本
	 * @param tempId
	 * @return
	 */
	public String findTemp(String tempId) {
		String hql = "from FishProTaskResult  where 1=1 and tempId='" + tempId
				+ "'";
		String str = (String) this.getSession()
				.createQuery("select distinct tempId " + hql).uniqueResult();
		return str;
	}

	/**
	 * 查询同一主表，同一个样本
	 * 
	 * @param code
	 * @return
	 */
	public List<FishProTaskResult> getBloodSampleList(String code, String id) {
		String hql = "from FishProTaskResult t where 1=1 and t.sampleCode='"
				+ code + "' and nextFlow='0' and fishProTask='" + id + "'";
		List<FishProTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	// 根据样本编号查询
		public List<FishProTaskResult> setResultById(String code) {
			String hql = "from FishProTaskResult t where (submit is null or submit='')  and fishProTask.id='"
					+ code + "'";
			List<FishProTaskResult> list = this.getSession().createQuery(hql).list();
			return list;
		}

		// 根据样本编号查询
		public List<FishProTaskResult> setResultByIds(String[] codes) {

			String insql = "''";
			for (String code : codes) {

				insql += ",'" + code + "'";

			}

			String hql = "from FishProTaskResult t where (submit is null or submit='') and id in ("
					+ insql + ")";
			List<FishProTaskResult> list = this.getSession().createQuery(hql).list();
			return list;
		}
		
		/**
		 * 根据主表编号Item信息
		 * @param code
		 * @return
		 */
		public List<FishProTaskItem> getItem(String id) {
			String hql = "from FishProTaskItem t where 1=1  and fishProTask='"+ id + "'";
			List<FishProTaskItem> list = this.getSession().createQuery(hql).list();
			return list;
		}
		/**
		 * 根据主表编号Reagent信息
		 * @param code
		 * @return
		 */
		public List<FishProTaskReagent> getReagent(String id) {
			String hql = "from FishProTaskReagent t where 1=1  and fishProTask='"+ id + "'";
			List<FishProTaskReagent> list = this.getSession().createQuery(hql).list();
			return list;
		}
	}
