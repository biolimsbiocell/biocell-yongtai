package com.biolims.experiment.fish.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.fish.model.FishCrossTaskResult;
import com.biolims.experiment.fish.model.FishSjTask;
import com.biolims.experiment.fish.model.FishSjTaskResult;
import com.biolims.experiment.producer.model.ProducerTaskResult;

@Repository
@SuppressWarnings("unchecked")
public class FishSjTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectFishSjTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from FishSjTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<FishSjTask> list = new ArrayList<FishSjTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectFishSjTaskListByState(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from FishSjTask where 1=1";
		if (mapForQuery.size() > 0) {
			key = map2where(mapForQuery);
		} else {
			key = "  and state='1'";
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<FishSjTask> list = new ArrayList<FishSjTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectFishSjTaskResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from FishSjTaskResult where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and fishSjTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<FishSjTaskResult> list = new ArrayList<FishSjTaskResult>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	/**
	 * 数据审核查看上机阅片结果
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 */
	public Map<String, Object> gettFishSjTaskResultList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort,String code) {
		String key = " ";
		String hql = " from FishSjTaskResult where 1=1 and code in (" + code + ") and isCommit='1'";
//		if (mapForQuery.size()>0){
//			key = map2where(mapForQuery);
//		}else{
//			key = " and code in (" + code + ") and isCommit='1'";
//		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<FishSjTaskResult> list = new ArrayList<FishSjTaskResult>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	/**
	 * 选择上机阅片加载info子表信息
	 * 
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> setFishSjTaskResultList(String code)
			throws Exception {
		String hql = "from FishSjTaskResult t where 1=1 ";
		String key = "";
		if (code != null)
			key = key + " and  t.fishSjTask='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<FishSjTaskResult> list = this.getSession().createQuery(hql + key)
				.list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 根据样本编号查询
	 * @param code
	 * @return
	 */
	public List<FishSjTaskResult> setResultById(String code) {
		String hql = "from FishSjTaskResult t where (isCommit is null or isCommit='') and fishSjTask.id='"
				+ code + "'";
		List<FishSjTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据样本编号查询
	 * @param codes
	 * @return
	 */
	public List<FishSjTaskResult> setResultByIds(String[] codes) {

		String insql = "''";
		for (String code : codes) {

			insql += ",'" + code + "'";

		}

		String hql = "from FishSjTaskResult t where (isCommit is null or isCommit='') and id in ("
				+ insql + ")";
		List<FishSjTaskResult> list = this.getSession().createQuery(hql).list();
		return list;
	}
}