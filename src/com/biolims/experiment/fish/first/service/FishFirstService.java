package com.biolims.experiment.fish.first.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.comsearch.dao.ComSearchDao;
import com.biolims.common.comsearch.service.ComSearchService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.fish.first.dao.FishFirstDao;
import com.biolims.experiment.fish.first.model.FishFirstInstance;
import com.biolims.experiment.fish.fx.model.FishAgainInstance;
import com.biolims.experiment.fish.fx.model.FishFxTask;
import com.biolims.experiment.fish.fx.model.FishFxTaskResult;
import com.biolims.experiment.fish.fx.model.FishFxTaskTemp;
import com.biolims.experiment.fish.second.model.FishSecondInstance;
import com.biolims.experiment.karyotyping.dao.KaryotypingTaskDao;
import com.biolims.experiment.karyotyping.model.KaryotypingSecond;
import com.biolims.file.model.FileInfo;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.report.model.CreateReport;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.sysreport.model.ReportTemplateInfo;
import com.biolims.util.ConfigFileUtil;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class FishFirstService {
	@Resource
	private FishFirstDao fishFirstDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private KaryotypingTaskDao karyotypingTaskDao;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private ComSearchDao comSearchDao;
	@Resource
	private ComSearchService comSearchService;

	StringBuffer json = new StringBuffer();

	public Map<String, Object> findFishFirstInstanceList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return fishFirstDao.selectFishFirstInstanceList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FishFirstInstance i) throws Exception {

		fishFirstDao.saveOrUpdate(i);

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveFishFirstInstance(String itemDataJson)
			throws Exception {
		List<FishFirstInstance> saveItems = new ArrayList<FishFirstInstance>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			FishFirstInstance scp = new FishFirstInstance();
			// 将map信息读入实体类
			scp = (FishFirstInstance) fishFirstDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			saveItems.add(scp);
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			fishFirstDao.saveOrUpdate(scp);
			//获取相关图片信息
			List<FileInfo> picList=comSearchDao.findPicture(scp.getTaskResultId(), "fishFxTaskResult");
			if(picList.size()>0){
				if(picList.size()<=2){
					List<ReportTemplateInfo> rlist=comSearchDao.getReportTemplateInfo(scp.getProductId(),"1");
					scp.setReportInfo(rlist.get(0));
					scp.setTemplate(rlist.get(0).getAttach());
				}else{
					int n=(picList.size()-2)%4;
					int a=0;//打印页数
					if(n==0){
						a=(picList.size()-2)/4;
					}else{
						a=(picList.size()-2)/4+1;
					}
					
					ReportTemplateInfo rt=new ReportTemplateInfo();//报告模板
					for(int i=0;i<a;i++){
						//获取对应报告页数的模板
						rt=comSearchDao.getReportTemplateInfo
								(scp.getProductId(),String.valueOf(i+2)).get(0);
					}
					scp.setReportInfo(rt);
					scp.setTemplate(rt.getAttach());
				}
			}else{
				scp.setReportInfo(null);
				scp.setTemplate(null);
			}
			if (scp != null) {
				if (scp.getIsCommit() != null
					 //&& scp.getIsGood() != null
					 //&& !scp.getIsGood().equals("")
					 && !scp.getIsCommit().equals("")) {
					 if (scp.getIsCommit().equals("1")) {
					    //if (scp.getIsGood().equals("1")) {
					    	//提交合格的到二审
					    	FishSecondInstance sf=new FishSecondInstance();
					    	sf.setCode(scp.getCode());
					    	sf.setSampleCode(scp.getSampleCode());
					    	sf.setSampleType(scp.getSampleType());
					    	sf.setSlideCode(scp.getSlideCode());
					    	sf.setResult(scp.getResult());
					    	sf.setResultDetail(scp.getResultDetail());
					    	sf.setLcjy(scp.getLcjy());
					    	sf.setProbeSite(scp.getProbeSite());
					    	sf.setProductId(scp.getProductId());
					    	sf.setProductName(scp.getProductName());
					    	if(scp.getTemplate()!=null){
					    		sf.setTemplate(scp.getTemplate());
					    	}
					    	if(scp.getReportInfo()!=null){
					    		sf.setReportInfo(scp.getReportInfo());
					    	}
					    	sf.setReportInfoId(scp.getReportInfoId());
					    	sf.setReportInfoName(scp.getReportInfoName());
					    	sf.setCellNum(scp.getCellNum());
					    	sf.setYcbg(scp.getYcbg());
					    	sf.setTaskId(scp.getTaskId());
					    	sf.setTaskResultId(scp.getTaskResultId());
					    	sf.setTaskType(scp.getTaskType());
					    	sf.setState("1");
					    	sf.setFormerDate(new Date());
					    	commonDAO.saveOrUpdate(sf);
					    	
					    	//完成后改变一审的状态
							scp.setState("2");
					    //}
							sampleStateService
							.saveSampleState1(
									scp.getCode(),
									scp.getSampleCode(),
									scp.getProductId(),
									scp.getProductName(),
									"",
									format.format(new Date()),
									format.format(new Date()),
									"FishFirstInstance",
									"Fish分析数据审核",
									(User) ServletActionContext
											.getRequest()
											.getSession()
											.getAttribute(
													SystemConstants.USER_SESSION_KEY),
									null, "Fish报告审核","1", null, null, null, null, null,
									null, null, null,scp.getResult(),scp.getResultDetail(),scp.getLcjy(),scp.getYcbg());
					 }
				 }
			}
		}
		fishFirstDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delFishFirstInstance(String[] ids) throws Exception {
		for (String id : ids) {
			FishFirstInstance scp = fishFirstDao.get(FishFirstInstance.class, id);
			fishFirstDao.delete(scp);
		}
	}
	/**
	 * 生成PDF报告文件
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void createReportFilePDF(String[] ids) throws Exception {
		String tempFile = ConfigFileUtil
				.getValueByKey("file.report.temp.path");
		// 正式地址
		String formFile = ConfigFileUtil
				.getValueByKey("file.report.form.path");
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		for (String id : ids) {
			FishFirstInstance sri = commonDAO.get(FishFirstInstance.class, id);
			SampleInfo sf = sampleInfoMainDao.findSampleInfo(sri
					.getSampleCode());
			/*SampleOrder so = new SampleOrder();
			if (sf != null) {
				if (sf.getSampleOrder() != null) {
					so = sf.getSampleOrder();
				}
			}*/
			SampleOrder so=commonDAO.get(SampleOrder.class, sri
					.getSampleCode());
			//获取相关图片
			List<FileInfo> picList=comSearchDao.findPictureOrderByName(sri.getTaskResultId(), "fishFxTaskResult");
			//报告模板
			ReportTemplateInfo reportInfo=sri.getReportInfo();
			//PDF文件
			FileInfo fileInfo = sri.getTemplate();
			if(fileInfo!=null){
				FileInputStream in = new FileInputStream(
						fileInfo.getFilePath());
				PdfReader reader = new PdfReader(in);
				String root = ConfigFileUtil.getRootPath() + File.separator
						+ DateUtil.format(new Date(), "yyyyMMdd");
				if (!new File(root).exists()) {
					new File(root).mkdirs();
				}
				File deskFile = new File(tempFile, "PDF" + sri.getCode()
						+ ".pdf");
				PdfStamper ps = new PdfStamper(reader,
						new FileOutputStream(deskFile)); //生成的输出流
				AcroFields s = ps.getAcroFields();//生成的新PDF文件
				//判断打印多少页
				int a=Integer.valueOf(reportInfo.getType());//打印页数
				/*if(picList.size()>2){
					int n=(picList.size()-2)%4;
					if(n==0){
						a=(picList.size()-2)/4;
					}else{
						a=(picList.size()-2)/4+1;
					}
				}else{
					a=1;
				}*/
				if(a==1){
					insertData(so,sri,s);
					if(picList.size()==1){
						insertImage2(ps,picList.get(0).getFilePath(),s,"Text1");
					}else{
						insertImage2(ps,picList.get(0).getFilePath(),s,"Text1");
						insertImage2(ps,picList.get(1).getFilePath(),s,"Text2");	
					}
				}else{
					for(int i=1;i<a+1;i++){
						String k=String.valueOf(i);
						if(i==1){
							insertImage2(ps,picList.get(0).getFilePath(),s,"Text1");
							insertImage2(ps,picList.get(1).getFilePath(),s,"Text2");
						}else{
							for(int j=0;j<(picList.size()-(i*(i-1)));j++){
								if((i*(i-1)+j)<=picList.size() && picList.get(i*(i-1)+j)!=null){
									insertImage2(ps,picList.get(i*(i-1)+j).getFilePath(),s,"Text"+String.valueOf(i*(i-1)+j+1));
								}
							}
						}
						insertDatas(so,sri,s,k,String.valueOf(a));
						/*if(sri.getResult()!=null){
							s.setField("result", sri.getResult());
						}else{
							s.setField("result", "");
						}
						if(sri.getResultDetail()!=null){
							s.setField("resultDescription", sri.getResultDetail());
						}else{
							s.setField("resultDescription", "");
						}*/
						String jg=sri.getResult();
						jg=jg.replace("\r","");
						jg=jg.replace("\n","");
						int num0=jg.getBytes("UTF-8").length;	//字节长度
						String jgjs=sri.getResultDetail();	//结果解释
						jgjs=jgjs.replace("\r","");
						jgjs=jgjs.replace("\n","");
						int num1=jgjs.getBytes("UTF-8").length;	//字节长度
						int n0=0;
						int n1=0;
						String jg1="";
						String jg2="";
						String jg3="";
						
						String jgjs1="";
						String jgjs2="";
						String jgjs3="";
						//结果
						if(num0>0){
							if(num0<=88){
								jg1=jg;
							}else{
						        for (int i1=1; i1 <=(num0/88); i1++) {
					        		jg2=jg.replace(jg3, "");
					        		n0=jg2.getBytes("UTF-8").length;
					        		if(n0>=88){
					        			jg1+=comSearchService.bSubstring(jg2, 88)+"\r";
					        			jg3+=comSearchService.bSubstring(jg2, 88);
					        		}else if(n0<90 && n0>0){
					        			jg1+=comSearchService.bSubstring(jg2, 88)+"\r";
					        			jg3+=comSearchService.bSubstring(jg2, 88);
					        		}
						        }
							}
						}
						//jg1+=jg1+bSubstring(jg.replace(jg3, ""), n0);
						System.out.println(jg1+jg.replace(jg3, ""));
						if(!jg.replace(jg3, "").equals("")){
							if(sri.getResult()!=null){
								s.setField("result", jg1+jg.replace(jg3, ""));
							}else{
								s.setField("result", "");
							}
						}else{
							if(sri.getResult()!=null){
								s.setField("result", jg1);
							}else{
								s.setField("result", "");
							}
						}
						
						//结果解释
						if(num1>0){
							if(num1<=84){
								jgjs1=jgjs;
							}else{
						        for (int i1=0; i1 <=(num1/84); i1++) {
					        		jgjs2=jgjs.replace(jgjs3, "");
					        		n1=jgjs2.getBytes("UTF-8").length;
					        		if(n1>=84){
					        			jgjs1+=comSearchService.bSubstring(jgjs2, 84)+"\r";
					        			jgjs3+=comSearchService.bSubstring(jgjs2, 84);
					        		}else if(n1<84 && n1>0){
					        			jgjs1+=comSearchService.bSubstring(jgjs2, n1)+"\r";
					        			jgjs3+=comSearchService.bSubstring(jgjs2, n1);
					        		}
						        }
							}
						}
						System.out.println(jgjs1+jgjs.replace(jgjs3, ""));
						if(!jgjs.replace(jgjs3, "").equals("")){
							//jgjs1+=jgjs1+jgjs.replace(jgjs3, "");
							if(sri.getResultDetail()!=null){
								s.setField("resultDescription", jgjs1+jgjs.replace(jgjs3, ""));
							}else{
								s.setField("resultDescription", "");
							}
						}else{
							if(sri.getResultDetail()!=null){
								s.setField("resultDescription", jgjs1);
							}else{
								s.setField("resultDescription", "");
							}
						}
						if(sri.getLcjy()!=null){
							if(sri.getLcjy().equals("1")){
								s.setField("advice", "门诊随访");
							}else{
								s.setField("advice", "遗传门诊随访");
							}
						}else{
							s.setField("advice", "");
						}
						//临床症状描述
						if(so.getSubmitReasonName()!=null){
							s.setField("lczz", so.getSubmitReasonName());
						}else{
							s.setField("lczz", "");
						}
						if(sri.getCellNum()!=null){
							s.setField("cellNum", sri.getCellNum());
						}else{
							s.setField("cellNum", "");
						}
						s.setField("reportDate", format1.format(new Date()));
						FishFxTask ft=commonDAO.get(FishFxTask.class, sri.getTaskId());
						s.setField("checkUser", ft
								.getAcceptUser().getName());
						s.setField("confirmUser", ft
								.getCreateUser().getName());
						
						commonDAO.update(sri);
					}
				}
				ps.setFormFlattening(true);// 这句不能少
				ps.close();
				reader.close();
				in.close();
			}
		}
	}
	/**
	 * 向PDF插入数据
	 * @param ps
	 * @param s
	 * @param path
	 * @param text
	 */
	@SuppressWarnings("static-access")
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void insertData(SampleOrder so,FishFirstInstance sri,
			AcroFields s) throws Exception{
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		if(so.getName()!=null){
			String names=so.getName();
			s.setField("names", names);
		}else{
			s.setField("names", "");
		}
		System.out.println("姓名"+so.getName());
		if(so.getGender()!=null){
			String m="男";
			String w="女";
			if(so.getGender().equals("1")){
				s.setField("genders", m);
			}else{
				s.setField("genders", w);
			}
		}else{
			s.setField("genders", "");
		}
		System.out.println("性别"+so.getGender());
		if (so.getAge() != null) {
			s.setField("age", so.getAge().toString());
		} else {
			s.setField("age", "");
		}
		if (so.getBirthDate() != null) {
			s.setField("birthDay", format.format(so.getBirthDate()));
		} else {
			s.setField("birthDay", "");
		}
		if(sri.getSampleCode()!=null){
			s.setField("slideCode", sri.getSampleCode());
		}else{
			s.setField("slideCode", "");
		}
		s.setField("medicalNum", so.getMedicalNumber());
		if(sri.getSampleType()!=null){
			String t=sri.getSampleType();
			s.setField("sampleTypes", t);
		}else{
			s.setField("sampleTypes", "");
		}
	
		System.out.println("样本类型"+sri.getSampleType());
		if(so.getCrmCustomer()!=null){
			s.setField("crmCustomer", so.getCrmCustomer().getName());
		}else{
			s.setField("crmCustomer", "");
		}
		if (so.getSamplingDate() != null) {
			s.setField("samplingDate",
					format.format(so.getSamplingDate()));
		} else {
			s.setField("samplingDate", "");
		}
		String jg=sri.getResult();
		jg=jg.replace("\r","");
		jg=jg.replace("\n","");
		int num0=jg.getBytes("UTF-8").length;	//字节长度
		String jgjs=sri.getResultDetail();	//结果解释
		jgjs=jgjs.replace("\r","");
		jgjs=jgjs.replace("\n","");
		int num1=jgjs.getBytes("UTF-8").length;	//字节长度
		int n0=0;
		int n1=0;
		String jg1="";
		String jg2="";
		String jg3="";
		
		String jgjs1="";
		String jgjs2="";
		String jgjs3="";
		//结果
		if(num0>0){
			if(num0<=88){
				jg1=jg;
			}else{
		        for (int i1=1; i1 <=(num0/88); i1++) {
	        		jg2=jg.replace(jg3, "");
	        		n0=jg2.getBytes("UTF-8").length;
	        		if(n0>=88){
	        			jg1+=comSearchService.bSubstring(jg2, 88)+"\r";
	        			jg3+=comSearchService.bSubstring(jg2, 88);
	        		}else if(n0<90 && n0>0){
	        			jg1+=comSearchService.bSubstring(jg2, 88)+"\r";
	        			jg3+=comSearchService.bSubstring(jg2, 88);
	        		}
		        }
			}
		}
		//jg1+=jg1+bSubstring(jg.replace(jg3, ""), n0);
		System.out.println(jg1+jg.replace(jg3, ""));
		if(!jg.replace(jg3, "").equals("")){
			if(sri.getResult()!=null){
				s.setField("result", jg1+jg.replace(jg3, ""));
			}else{
				s.setField("result", "");
			}
		}else{
			if(sri.getResult()!=null){
				s.setField("result", jg1);
			}else{
				s.setField("result", "");
			}
		}
		
		//结果解释
		if(num1>0){
			if(num1<=84){
				jgjs1=jgjs;
			}else{
		        for (int i1=0; i1 <=(num1/84); i1++) {
	        		jgjs2=jgjs.replace(jgjs3, "");
	        		n1=jgjs2.getBytes("UTF-8").length;
	        		if(n1>=84){
	        			jgjs1+=comSearchService.bSubstring(jgjs2, 84)+"\r";
	        			jgjs3+=comSearchService.bSubstring(jgjs2, 84);
	        		}else if(n1<84 && n1>0){
	        			jgjs1+=comSearchService.bSubstring(jgjs2, n1)+"\r";
	        			jgjs3+=comSearchService.bSubstring(jgjs2, n1);
	        		}
		        }
			}
		}
		System.out.println(jgjs1+jgjs.replace(jgjs3, ""));
		if(!jgjs.replace(jgjs3, "").equals("")){
			//jgjs1+=jgjs1+jgjs.replace(jgjs3, "");
			if(sri.getResultDetail()!=null){
				s.setField("resultDescription", jgjs1+jgjs.replace(jgjs3, ""));
			}else{
				s.setField("resultDescription", "");
			}
		}else{
			if(sri.getResultDetail()!=null){
				s.setField("resultDescription", jgjs1);
			}else{
				s.setField("resultDescription", "");
			}
		}
		if(sri.getLcjy()!=null){
			if(sri.getLcjy().equals("1")){
				s.setField("advice", "门诊随访");
			}else{
				s.setField("advice", "遗传门诊随访");
			}
		}else{
			s.setField("advice", "");
		}
		//临床症状描述
		if(so.getSubmitReasonName()!=null){
			s.setField("lczz", so.getSubmitReasonName());
		}else{
			s.setField("lczz", "");
		}
		if(sri.getCellNum()!=null){
			s.setField("cellNum", sri.getCellNum());
		}else{
			s.setField("cellNum", "");
		}
		s.setField("reportDate", format.format(new Date()));
		/*FishFxTask ft=commonDAO.get(FishFxTask.class, sri.getTaskId());
		s.setField("checkUser", ft
				.getAcceptUser().getName());
		s.setField("confirmUser", ft
				.getCreateUser().getName());*/
		s.setField("pageNum", "1/1");
		commonDAO.update(sri);
	}
	/**
	 * 向PDF插入数据
	 * @param ps
	 * @param s
	 * @param path
	 * @param text
	 */
	@SuppressWarnings("static-access")
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void insertDatas(SampleOrder so,FishFirstInstance sri,
			AcroFields s,String i,String a) throws Exception{
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		if(so.getName()!=null){
			String names=so.getName();
			s.setField("names"+i, names);
		}else{
			s.setField("names"+i, "");
		}
		System.out.println("姓名"+so.getName());
		if(so.getGender()!=null){
			String m="男";
			String w="女";
			if(so.getGender().equals("1")){
				s.setField("genders"+i, m);
			}else{
				s.setField("genders"+i, w);
			}
		}else{
			s.setField("genders"+i, "");
		}
		System.out.println("性别"+so.getGender());
		if (so.getAge() != null) {
			s.setField("age"+i, so.getAge().toString());
		} else {
			s.setField("age"+i, "");
		}
		if (so.getBirthDate() != null) {
			s.setField("birthDay"+i, format.format(so.getBirthDate()));
		} else {
			s.setField("birthDay"+i, "");
		}
		if(sri.getSampleCode()!=null){
			s.setField("slideCode"+i, sri.getSampleCode());
		}else{
			s.setField("slideCode"+i, "");
		}
		s.setField("medicalNum"+i, so.getMedicalNumber());
		if(sri.getSampleType()!=null){
			String t=sri.getSampleType();
			s.setField("sampleTypes"+i, t);
		}else{
			s.setField("sampleTypes"+i, "");
		}
	
		System.out.println("样本类型"+sri.getSampleType());
		if(so.getCrmCustomer()!=null){
			s.setField("crmCustomer"+i, so.getCrmCustomer().getName());
		}else{
			s.setField("crmCustomer"+i, "");
		}
		if (so.getSamplingDate() != null) {
			s.setField("samplingDate"+i,
					format.format(so.getSamplingDate()));
		} else {
			s.setField("samplingDate"+i, "");
		}
		s.setField("pageNum"+i, i+"/"+a);
		commonDAO.update(sri);
	}
	
	/**
	 * 向第PDF插入图片
	 * @param ps
	 * @param s
	 * @param path
	 * @param text
	 */
	@SuppressWarnings("static-access")
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void insertImage2(PdfStamper ps,String path,AcroFields s,String text) throws Exception{
		try { 
			
//			Image image = Image.getInstance(path);  
//			List<AcroFields.FieldPosition> list = s.getFieldPositions(text);  
//			Rectangle signRect = list.get(0).position;  
//			PdfContentByte under = ps.getOverContent(1);  
//			float x = signRect.getLeft();  
//			float y = signRect.getBottom();  
//			System.out.println(x);  
//			System.out.println(y);  
//			image.setAbsolutePosition(x, y);  
//			image.scaleToFit(290, 412);  
//			  
//			under.addImage(image); 
		    int pageNo = s.getFieldPositions(text).get(0).page;
		    Rectangle signRect = s.getFieldPositions(text).get(0).position;
	        float x = signRect.getLeft();
	        float y = signRect.getBottom();

	        // 读图片
	        Image image = Image.getInstance(path);
	        // 获取操作的页面
	        PdfContentByte under = ps.getOverContent(pageNo);
	        // 根据域的大小缩放图片
	        image.scaleToFit(signRect.getWidth(), signRect.getHeight());
	        System.out.println(x);  
			System.out.println(y);  
	        // 添加图片
	        image.setAbsolutePosition(x, y+40);
	        under.addImage(image);

		}catch (Exception e){  
			// TODO Auto-generated catch block  
			e.printStackTrace();  
		}  
	}
	
/**
 * 提交样本
 * @param id
 * @param ids
 * @throws Exception
 */
@WriteOperLog
@WriteExOperLog
@Transactional(rollbackFor = Exception.class)
public void submitSample( String[] ids) throws Exception {
DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
// 获取结果表样本信息
List<FishFirstInstance> list = this.fishFirstDao.setFirstInstanceByIds(ids);
for (FishFirstInstance scp : list) {
if (scp != null) {
    	FishSecondInstance sf=new FishSecondInstance();
    	sf.setCode(scp.getCode());
    	sf.setSampleCode(scp.getSampleCode());
    	sf.setSampleType(scp.getSampleType());
    	sf.setSlideCode(scp.getSlideCode());
    	sf.setResult(scp.getResult());
    	sf.setResultDetail(scp.getResultDetail());
    	sf.setLcjy(scp.getLcjy());
    	sf.setProbeSite(scp.getProbeSite());
    	sf.setProductId(scp.getProductId());
    	sf.setProductName(scp.getProductName());
    	if(scp.getTemplate()!=null){
    		sf.setTemplate(scp.getTemplate());
    	}
    	if(scp.getReportInfo()!=null){
    		sf.setReportInfo(scp.getReportInfo());
    	}
    	sf.setReportInfoId(scp.getReportInfoId());
    	sf.setReportInfoName(scp.getReportInfoName());
    	sf.setCellNum(scp.getCellNum());
    	sf.setYcbg(scp.getYcbg());
    	sf.setTaskId(scp.getTaskId());
    	sf.setTaskResultId(scp.getTaskResultId());
    	sf.setTaskType(scp.getTaskType());
    	sf.setState("1");
    	sf.setFormerDate(new Date());
    	commonDAO.saveOrUpdate(sf);
    	
    	//完成后改变一审的状态
		scp.setState("2");
    //}
		sampleStateService
		.saveSampleState1(
				scp.getCode(),
				scp.getSampleCode(),
				scp.getProductId(),
				scp.getProductName(),
				"",
				format.format(new Date()),
				format.format(new Date()),
				"FishFirstInstance",
				"Fish分析数据审核",
				(User) ServletActionContext
						.getRequest()
						.getSession()
						.getAttribute(
								SystemConstants.USER_SESSION_KEY),
				null, "Fish报告审核","1", null, null, null, null, null,
				null, null, null,scp.getResult(),scp.getResultDetail(),scp.getLcjy(),scp.getYcbg());
 }scp.setIsCommit("1");
		}
	}
	
}
