﻿package com.biolims.experiment.fish.action;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.comsearch.dao.ComSearchDao;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.fish.dao.SamplePreTaskDao;
import com.biolims.experiment.fish.model.SamplePreTask;
import com.biolims.experiment.fish.model.SamplePreTaskCos;
import com.biolims.experiment.fish.model.SamplePreTaskItem;
import com.biolims.experiment.fish.model.SamplePreTaskReagent;
import com.biolims.experiment.fish.model.SamplePreTaskResult;
import com.biolims.experiment.fish.model.SamplePreTaskTemp;
import com.biolims.experiment.fish.model.SamplePreTaskTemplate;
import com.biolims.experiment.fish.service.SamplePreTaskService;
import com.biolims.experiment.karyoship.model.KaryoShipTask;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.syscode.model.CodeMain;
import com.biolims.system.syscode.service.CodeMainService;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.service.TemplateService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/fish/samplePreTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SamplePreTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "248901";
	@Autowired
	private SamplePreTaskService samplePreTaskService;
	private SamplePreTask samplePreTask = new SamplePreTask();
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private SamplePreTaskDao samplePreTaskDao;
	@Resource
	private TemplateService templateService;
	@Resource
	private CodeMainService codeMainService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private ComSearchDao comSearchDao;

	@Action(value = "showSamplePreTaskList")
	public String showSamplePreTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/fish/samplePreTask.jsp");
	}

	@Action(value = "showSamplePreTaskListJson")
	public void showSamplePreTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = samplePreTaskService.findSamplePreTaskList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SamplePreTask> list = (List<SamplePreTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "");
		map.put("workOrder-id", "");
		map.put("workOrder-name", "");
		map.put("testUser-id", "");
		map.put("testUser-name", "");
		map.put("testDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("state", "");
		map.put("stateName", "");

		// 实验组
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("dicSampleType-id", "");
		map.put("dicSampleType-name", "");

		map.put("confirmDate", "");
		map.put("type", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "SamplePreTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogSamplePreTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/fish/samplePreTaskDialog.jsp");
	}

	@Action(value = "showDialogSamplePreTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogSamplePreTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = samplePreTaskService.findSamplePreTaskList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SamplePreTask> list = (List<SamplePreTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("workOrder-id", "");
		map.put("workOrder-name", "");
		map.put("testUser-id", "");
		map.put("testUser-name", "");
		map.put("testDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("state", "");
		map.put("stateName", "");

		map.put("dicSampleType-id", "");
		map.put("dicSampleType-name", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editSamplePreTask")
	public String editSamplePreTask() throws Exception {
		String id = getParameterFromRequest("id");
		String maxNum = getParameterFromRequest("maxNum");
		long num = 0;
		if (id != null && !id.equals("")) {
			samplePreTask = samplePreTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "samplePreTask");
		} else {
			samplePreTask.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			samplePreTask.setCreateUser(user);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stime = format.format(date);
			samplePreTask.setCreateDate(stime);
			samplePreTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			samplePreTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
//			List<Template> tlist=comSearchDao.getTemplateByType("doSamplePre");
//			Template t=tlist.get(0);
//			samplePreTask.setTemplate(t);
//			//UserGroup u=comSearchDao.get(UserGroup.class,t.getAcceptUser().getId());
//			if(t.getAcceptUser()!=null){
//				samplePreTask.setAcceptUser(t.getAcceptUser());
//			}
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(samplePreTask.getState());
		putObjToContext("fileNum", num);
		putObjToContext("maxNum", maxNum);
		return dispatcher("/WEB-INF/page/experiment/fish/samplePreTaskEdit.jsp");
	}

	@Action(value = "copySamplePreTask")
	public String copySamplePreTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		samplePreTask = samplePreTaskService.get(id);
		samplePreTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/fish/samplePreTaskEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = samplePreTask.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "SamplePreTask";
			String markCode = "YBYCL";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			samplePreTask.setId(autoID);
//			samplePreTask.setName("Fish样本预处理-"+autoID);
		}
		Map aMap = new HashMap();
		aMap.put("samplePreTaskItem",
				getParameterFromRequest("samplePreTaskItemJson"));

		aMap.put("samplePreTaskResult",
				getParameterFromRequest("samplePreTaskResultJson"));

		aMap.put("samplePreTaskTemplate",
				getParameterFromRequest("samplePreTaskTemplateJson"));
		aMap.put("samplePreTaskReagent",
				getParameterFromRequest("samplePreTaskReagentJson"));
		aMap.put("samplePreTaskCos", getParameterFromRequest("samplePreTaskCosJson"));

		samplePreTaskService.save(samplePreTask, aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/experiment/fish/samplePreTask/editSamplePreTask.action?id=" + samplePreTask.getId()+"&maxNum="+samplePreTask.getMaxNum();
		if (bpmTaskId != null && !bpmTaskId.equals("") && !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);
	}

	@Action(value = "viewSamplePreTask")
	public String toViewSamplePreTask() throws Exception {
		String id = getParameterFromRequest("id");
		samplePreTask = samplePreTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/fish/samplePreTaskEdit.jsp");
	}

	@Action(value = "showSamplePreTaskItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSamplePreTaskItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/fish/samplePreTaskItem.jsp");
	}

	@Action(value = "showSamplePreTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSamplePreTaskItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = samplePreTaskService
					.findSamplePreTaskItemList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<SamplePreTaskItem> list = (List<SamplePreTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("tempId", "");
			map.put("sampleCode", "");
			map.put("name", "");
			map.put("patientName", "");
			map.put("checked", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("state", "");
			map.put("note", "");
			map.put("concentration", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("stepNum", "");
			map.put("samplePreTask-name", "");
			map.put("samplePreTask-id", "");
			map.put("storage-name", "");
			map.put("storage-id", "");
			map.put("volume", "");
			map.put("idCard", "");
			map.put("sequencingFun", "");
			map.put("inspectDate", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("orderId", "");
			map.put("phone", "");
			map.put("orderNumber", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("counts", "");
			map.put("classify", "");
			map.put("productNum", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("sampleNum", "");
			map.put("sampleConsume", "");
			map.put("sampleType", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSamplePreTaskItem")
	public void delSamplePreTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			samplePreTaskService.delSamplePreTaskItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showSamplePreTaskResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSamplePreTaskResultList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/fish/samplePreTaskResult.jsp");
	}

	@Action(value = "showSamplePreTaskResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSamplePreTaskResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = samplePreTaskService
					.findSamplePreTaskResultList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<SamplePreTaskResult> list = (List<SamplePreTaskResult>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("tempId", "");
			map.put("code", "");
			map.put("zjCode", "");
			//
			map.put("sampleCode", "");
			map.put("suspensionCode", "");
			map.put("volume", "");
			map.put("unit", "");

			map.put("result", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("method", "");
			map.put("state", "");
			map.put("note", "");

			map.put("samplePreTask-id", "");
			map.put("samplePreTask-name", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("submit", "");

			map.put("patientName", "");
			map.put("idCard", "");
			map.put("sequencingFun", "");
			map.put("inspectDate", "");
			map.put("acceptDate", "yyyy-MM-dd");

			map.put("reportDate", "yyyy-MM-dd");
			map.put("reason", "");
			map.put("concentration", "");
			map.put("orderId", "");
			map.put("phone", "");
			map.put("classify", "");

			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("sampleType", "");
			map.put("sampleNum", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSamplePreTaskResult")
	public void delSamplePreTaskResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			samplePreTaskService.delSamplePreTaskResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showSamplePreTaskTemplateList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSamplePreTaskTemplateList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/fish/samplePreTaskTemplate.jsp");
	}

	@Action(value = "showSamplePreTaskTemplateListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSamplePreTaskTemplateListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = samplePreTaskService
					.findSamplePreTaskTemplateList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<SamplePreTaskTemplate> list = (List<SamplePreTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("testUser-id", "");
			map.put("testUser-name", "");
			map.put("note", "");
			map.put("tItem", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("state", "");
			map.put("codes", "");
			map.put("samplePreTask-name", "");
			map.put("samplePreTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSamplePreTaskTemplate")
	public void delSamplePreTaskTemplate() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			samplePreTaskService.delSamplePreTaskTemplate(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSamplePreTaskTemplateOne")
	public void delSamplePreTaskTemplateOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			samplePreTaskService.delSamplePreTaskTemplateOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showSamplePreTaskReagentList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSamplePreTaskReagentList() throws Exception {
		// String itemId = getParameterFromRequest("itemId");
		// putObjToContext("itemId",itemId);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/fish/samplePreTaskReagent.jsp");
	}

	@Action(value = "showSamplePreTaskReagentListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSamplePreTaskReagentListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = new HashMap<String, Object>();
			// String itemId = getParameterFromRequest("itemId");
			result = samplePreTaskService.findSamplePreTaskReagentList(scId,
					startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<SamplePreTaskReagent> list = (List<SamplePreTaskReagent>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("batch", "");
			map.put("isGood", "");
			map.put("note", "");
			//
			map.put("oneNum", "");
			map.put("sampleNum", "");
			map.put("num", "");
			map.put("count", "");
			//
			map.put("tReagent", "");
			map.put("itemId", "");
			map.put("samplePreTask-name", "");
			map.put("samplePreTask-id", "");
			map.put("sn", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSamplePreTaskReagent")
	public void delSamplePreTaskReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			samplePreTaskService.delSamplePreTaskReagent(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSamplePreTaskReagentOne")
	public void delSamplePreTaskReagentOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			samplePreTaskService.delSamplePreTaskReagentOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showSamplePreTaskCosList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSamplePreTaskCosList() throws Exception {
		// String itemId = getParameterFromRequest("itemId");
		// putObjToContext("itemId",itemId);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/fish/samplePreTaskCos.jsp");
	}

	@Action(value = "showSamplePreTaskCosListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSamplePreTaskCosListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = new HashMap<String, Object>();
			result = samplePreTaskService.findSamplePreTaskCosList(scId, startNum,
					limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<SamplePreTaskCos> list = (List<SamplePreTaskCos>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("isGood", "");
			map.put("tCos", "");
			map.put("itemId", "");
			//
			map.put("temperature", "");
			map.put("speed", "");
			map.put("time", "");
			map.put("note", "");
			//
			map.put("samplePreTask-name", "");
			map.put("samplePreTask-id", "");

			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSamplePreTaskCos")
	public void delSamplePreTaskCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			samplePreTaskService.delSamplePreTaskCos(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSamplePreTaskCosOne")
	public void delSamplePreTaskCosOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			samplePreTaskService.delSamplePreTaskCosOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 打印条码
	@Action(value = "makeCode", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void makeCode() throws Exception {

		String ip = getParameterFromRequest("ip");
		String id = getParameterFromRequest("id");
		String code = getParameterFromRequest("code");
		String name = getParameterFromRequest("name");
		Socket socket = null;
		Template template = null;
		CodeMain codeMain = null;
		OutputStream os;
		try {
			socket = new Socket();
			// 向服务器端第一次发送字符串

			// 向服务器端第二次发送字符串
			SocketAddress sa = new InetSocketAddress(ip, 9100);
			socket.connect(sa);

			os = socket.getOutputStream();

			char c = 13;
			char b = 10;
			String cb = String.valueOf(c) + String.valueOf(b);
			SamplePreTask pt = samplePreTaskService.get(id);
			if (pt != null && pt.getTemplate() != null
					&& pt.getTemplate().getId() != null) {
				template = templateService.get(pt.getTemplate().getId());
			}
			if (template != null && template.getCodeMain() != null
					&& template.getCodeMain().getId() != null) {
				codeMain = codeMainService.get(template.getCodeMain().getId());
			}
			if (codeMain != null) {
				String codeidx = codeMain.getCodex();
				String codeidy = codeMain.getCodey();
				String codenamex = codeMain.getNamex();
				String codenamey = codeMain.getNamey();
				String qrx = codeMain.getQrx();
				String qry = codeMain.getQry();
				String markCode = codeMain.getCode();
				String printStr = "";
				// for(int i=1;i<5;i++){
				printStr = "e PCX;*"
						+ cb
						+ "e IMG;*"
						+ cb
						+ "mm"
						+ cb
						+ "zO"
						+ cb
						+ "J"
						+ cb
						+ "O R,P"
						+ cb
						+ "H75,0,T"
						+ cb
						+ "D 0.0,0.0"
						+ cb
						+ "M l FNT;/iffs/gb2312"
						+ cb
						+ "F90;gb2312"
						+ cb
						+ "Sl1;0.0,0.0,9.53,9.53,50.90,50.90,1"
						+ cb
						// + "T4.2,4.0,0,90,3.5,q70;" + name + cb +
						// "T4.2,7.6,0,3,2.84,q90;" + id + cb
						+ "T" + codenamex + "," + codenamey + ",0,90,3.5,q70;"
						+ name + cb + "T" + codeidx + "," + codeidy
						+ ",0,3,2.84,q90;" + code + cb + "B" + qrx + "," + qry
						+ ",0,DATAMATRIX,0.33;" + code + cb + "T"
						+ (Float.valueOf(codenamex) + 27) + "," + codenamey
						+ ",0,90,3.5,q70;" + name + cb + "T"
						+ (Float.valueOf(codeidx) + 27) + "," + codeidy
						+ ",0,3,2.84,q90;" + code + cb + "B"
						+ (Float.valueOf(qrx) + 27) + "," + qry
						+ ",0,DATAMATRIX,0.33;" + code + cb + "A 1" + cb;
				// 31.6,2.5
				// }
				os.write(printStr.getBytes("UTF-8"));
			}

			os.flush();

		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
				}
			}
		}
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SamplePreTaskService getSamplePreTaskService() {
		return samplePreTaskService;
	}

	public void setSamplePreTaskService(SamplePreTaskService samplePreTaskService) {
		this.samplePreTaskService = samplePreTaskService;
	}

	public SamplePreTask getSamplePreTask() {
		return samplePreTask;
	}

	public void setSamplePreTask(SamplePreTask samplePreTask) {
		this.samplePreTask = samplePreTask;
	}

	
	@Action(value = "showSamplePreTasktempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSamplePreTasktempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/fish/samplePreTaskTemp.jsp");
	}

	@Action(value = "showSamplePreTasktempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSamplePreTasktempListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = samplePreTaskService.selectSamplePreTaskTempList(map2Query, startNum, limitNum, dir, sort);
		Long total = (Long) result.get("total");
		List<SamplePreTaskTemp> list = (List<SamplePreTaskTemp>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("name", "");
		map.put("patientName", "");
		map.put("reason", "");
		map.put("gender", "");
		map.put("doctor", "");
		map.put("hospital", "");
		map.put("note", "");
		map.put("state", "");
		map.put("stateName", "");

		map.put("productId", "");
		map.put("productName", "");
		map.put("method", "");
		
		map.put("concentration", "");
		map.put("result", "");
		map.put("stepNum", "");

		map.put("volume", "");
		map.put("idCard", "");
		map.put("sequencingFun", "");
		map.put("inspectDate", "");
		map.put("acceptDate", "yyyy-MM-dd");

		map.put("reportDate", "yyyy-MM-dd");
		map.put("orderId", "");
		map.put("phone", "");
		map.put("classify", "");
		map.put("sampleNum", "");
		map.put("sampleType", "");
		//缴费状态
		map.put("chargeNote", "");
		new SendData().sendDateJson(map, list, total,
				ServletActionContext.getResponse());
	}

	// 判断样本做血浆提取的次数
	@Action(value = "selectCodeCount")
	public void selectCodeCount() throws Exception {
		String code = getParameterFromRequest("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			Long num = samplePreTaskDao.selectCount(code);
			int num1 = Integer.valueOf(String.valueOf(num));
			result.put("success", true);
			result.put("data", num1);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 模板
	@Action(value = "showTemplateWaitList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTemplateWaitList() throws Exception {
		String scId = getRequest().getParameter("id");
		putObjToContext("id", scId);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/fish/showTemplateWaitList.jsp");
	}

	@Action(value = "showTemplateWaitListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTemplateWaitListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = samplePreTaskService
					.findSamplePreTaskTemplateList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<SamplePreTaskTemplate> list = (List<SamplePreTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("note", "");
			map.put("tItem", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("testUser-id", "");
			map.put("testUser-name", "");
			map.put("sampleCodes", "");
			map.put("state", "");
			map.put("samplePreTask-name", "");
			map.put("samplePreTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public SamplePreTaskDao getSamplePreTaskDao() {
		return samplePreTaskDao;
	}

	public void setSamplePreTaskDao(SamplePreTaskDao SamplePreTaskDao) {
		this.samplePreTaskDao = SamplePreTaskDao;
	}

	// 根据选中的步骤异步加载相关的试剂数据
	@Action(value = "setReagent")
	public void setReagent() throws Exception {
		String code = getRequest().getParameter("code");
		String id = getRequest().getParameter("tid");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.samplePreTaskService
					.setReagent(id, code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 根据选中的步骤异步加载相关的设备数据
	@Action(value = "setCos")
	public void setCos() throws Exception {
		String code = getRequest().getParameter("code");
		String id = getRequest().getParameter("tid");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.samplePreTaskService
					.setCos(id, code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	// 提交样本
	@Action(value = "submitSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void submitSample() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.samplePreTaskService.submitSample(id, ids);

			result.put("success", true);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	 /**
	 * 保存样本预处理明细
	 * @throws Exception
	 */
	@Action(value = "saveSamplePreTaskItem",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveSamplePreTaskItem() throws Exception {
		String id = getRequest().getParameter("id");
		String itemDataJson = getRequest().getParameter("itemDataJson");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			SamplePreTask sc=commonDAO.get(SamplePreTask.class, id);
			Map aMap = new HashMap();
			aMap.put("samplePreTaskItem",
					itemDataJson);
			if(sc!=null){
				samplePreTaskService.save(sc,aMap);
			}
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	 /**
	 * 保存样本预处理结果
	 * @throws Exception
	 */
	@Action(value = "saveSamplePreTaskResult",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveSamplePreTaskResult() throws Exception {
		String id = getRequest().getParameter("id");
		String itemDataJson = getRequest().getParameter("itemDataJson");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			SamplePreTask sc=commonDAO.get(SamplePreTask.class, id);
			Map aMap = new HashMap();
			aMap.put("samplePreTaskResult",
					itemDataJson);
			if(sc!=null){
				samplePreTaskService.save(sc,aMap);
			}
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

}
