package com.biolims.experiment.fish.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.comsearch.dao.ComSearchDao;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.core.model.user.UserGroup;
import com.biolims.experiment.fish.model.FishCrossTask;
import com.biolims.experiment.fish.model.FishSjTask;
import com.biolims.experiment.fish.model.FishSjTaskResult;
import com.biolims.experiment.fish.service.FishSjTaskService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/fish/fishSjTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class FishSjTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "248904";
	@Autowired
	private FishSjTaskService fishSjTaskService;
	private FishSjTask fishSjTask = new FishSjTask();
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private ComSearchDao comSearchDao;

	@Action(value = "showFishSjTaskList")
	public String showFishSjTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/fish/fishSjTask.jsp");
	}

	@Action(value = "showFishSjTaskListJson")
	public void showFishSjTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = fishSjTaskService.findFishSjTaskList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<FishSjTask> list = (List<FishSjTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		//新加的string的字段
		map.put("fishCrossTaskId", "");
		map.put("fishCrossTaskName", "");
		
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "fishSjTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogFishSjTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/fish/fishSjTaskDialog.jsp");
	}

	@Action(value = "showDialogFishSjTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogFishSjTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = fishSjTaskService
				.findFishSjTaskListByState(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<FishSjTask> list = (List<FishSjTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		
		//新加的string型的字段
		map.put("fishCrossTaskId", "");
		map.put("fishCrossTaskName", "");
		
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editFishSjTask")
	public String editFishSjTask() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			fishSjTask = fishSjTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "fishSjTask");
		} else {
			fishSjTask.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			fishSjTask.setCreateUser(user);
			fishSjTask.setCreateDate(new Date());
			fishSjTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			fishSjTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			//P012
			UserGroup u=comSearchDao.get(UserGroup.class,"P012");
			fishSjTask.setAcceptUser(u);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(fishSjTask.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/fish/fishSjTaskEdit.jsp");
	}

	@Action(value = "copyFishSjTask")
	public String copyFishSjTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		fishSjTask = fishSjTaskService.get(id);
		fishSjTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/fish/fishSjTaskEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = fishSjTask.getId();
		// if(id!=null&&id.equals("")){
		// fishSjTask.setId(null);
		// }
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "FishSjTask";
			String markCode = "SJYP";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			fishSjTask.setId(autoID);
//			fishSjTask.setName("Fish上机阅片-"+autoID);
		}
		Map aMap = new HashMap();
		aMap.put("fishSjTaskResult",
				getParameterFromRequest("fishSjTaskResultJson"));

		fishSjTaskService.save(fishSjTask, aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/experiment/fish/fishSjTask/editFishSjTask.action?id="
				+ fishSjTask.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);
		// return
		// redirect("/experiment/fish/fishSjTask/editFishSjTask.action?id=" +
		// fishSjTask.getId());

	}

	@Action(value = "viewFishSjTask")
	public String toViewFishSjTask() throws Exception {
		String id = getParameterFromRequest("id");
		fishSjTask = fishSjTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/fish/fishSjTaskEdit.jsp");
	}

	@Action(value = "showFishSjTaskResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showFishSjTaskResultList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/fish/fishSjTaskResult.jsp");
	}

	@Action(value = "showFishSjTaskResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showFishSjTaskResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = fishSjTaskService
					.findFishSjTaskResultList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<FishSjTaskResult> list = (List<FishSjTaskResult>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("slideCode", "");
			map.put("f18", "");
			map.put("x", "");
			map.put("y", "");
			map.put("f13", "");
			map.put("f21", "");
			map.put("isGood", "");
			map.put("otherSit", "");
			map.put("isCommit", "");
			map.put("note", "");
			map.put("state", "");
			map.put("fishSjTask-name", "");
			map.put("fishSjTask-id", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("sampleType", "");
			map.put("cellsNum", "");
			map.put("nextFlow", "");
			map.put("nextFlowId", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 数据审核查看上机阅片结果
	 */
	@Action(value = "getFishSjTaskResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String getFishSjTaskResultList() throws Exception {
		String code=getParameterFromRequest("code");
		putObjToContext("code", code);
		return dispatcher("/WEB-INF/page/experiment/fish/getSjTaskResult.jsp");
	}

	@Action(value = "getFishSjTaskResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void getFishSjTaskResultListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		String code = getParameterFromRequest("code");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = fishSjTaskService.getFishSjTaskResultList(map2Query, startNum, limitNum, dir, sort, code);
		Long total = (Long) result.get("total");
		List<FishSjTaskResult> list = (List<FishSjTaskResult>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("slideCode", "");
		map.put("f18", "");
		map.put("x", "");
		map.put("y", "");
		map.put("f13", "");
		map.put("f21", "");
		map.put("isGood", "");
		map.put("otherSit", "");
		map.put("isCommit", "");
		map.put("note", "");
		map.put("state", "");
		map.put("fishSjTask-name", "");
		map.put("fishSjTask-id", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("sampleType", "");
		map.put("cellsNum", "");
		new SendData().sendDateJson(map, list, total,
				ServletActionContext.getResponse());
	}
	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delFishSjTaskResult")
	public void delFishSjTaskResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			fishSjTaskService.delFishSjTaskResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除
	 * 
	 * @throws Exception
	 */
	@Action(value = "delFishSjTaskResultOne")
	public void delFishSjTaskResultOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			fishSjTaskService.delFishSjTaskResultOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public FishSjTaskService getFishSjTaskService() {
		return fishSjTaskService;
	}

	public void setFishSjTaskService(FishSjTaskService fishSjTaskService) {
		this.fishSjTaskService = fishSjTaskService;
	}

	public FishSjTask getFishSjTask() {
		return fishSjTask;
	}

	public void setFishSjTask(FishSjTask fishSjTask) {
		this.fishSjTask = fishSjTask;
	}

	/**
	 * 根据FishSjTask加载info子表明细
	 * 
	 * @throws Exception
	 */
	@Action(value = "setResultToFishFx", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void setResultToFishFx() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.fishSjTaskService
					.showFishSjTaskResultList(code);
			result.put("success", true);
			result.put("data", dataListMap);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 提交样本
	 * @throws Exception
	 */
	@Action(value = "submitSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void submitSample() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.fishSjTaskService.submitSample(id, ids);

			result.put("success", true);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	 /**
	 * 保存上机阅片明细
	 * @throws Exception
	 */
	@Action(value = "saveFishSjTaskResult",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveFishSjTaskResult() throws Exception {
		String id = getRequest().getParameter("id");
		String itemDataJson = getRequest().getParameter("itemDataJson");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			FishSjTask sc=commonDAO.get(FishSjTask.class, id);
			Map aMap = new HashMap();
			aMap.put("fishSjTaskResult",
					itemDataJson);
			if(sc!=null){
				fishSjTaskService.save(sc,aMap);
			}
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

}
