﻿package com.biolims.experiment.fish.action;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.comsearch.dao.ComSearchDao;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.dic.model.DicType;
import com.biolims.experiment.fish.dao.FishCrossTaskDao;
import com.biolims.experiment.fish.model.FishCrossTask;
import com.biolims.experiment.fish.model.FishCrossTaskCos;
import com.biolims.experiment.fish.model.FishCrossTaskItem;
import com.biolims.experiment.fish.model.FishCrossTaskReagent;
import com.biolims.experiment.fish.model.FishCrossTaskResult;
import com.biolims.experiment.fish.model.FishCrossTaskTemp;
import com.biolims.experiment.fish.model.FishCrossTaskTemplate;
import com.biolims.experiment.fish.model.FishSjTask;
import com.biolims.experiment.fish.model.SamplePreTask;
import com.biolims.experiment.fish.service.FishCrossTaskService;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.syscode.model.CodeMain;
import com.biolims.system.syscode.service.CodeMainService;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.service.TemplateService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/fish/fishCrossTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class FishCrossTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "248903";
	@Autowired
	private FishCrossTaskService fishCrossTaskService;
	private FishCrossTask fishCrossTask = new FishCrossTask();
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private FishCrossTaskDao fishCrossTaskDao;
	@Resource
	private TemplateService templateService;
	@Resource
	private CodeMainService codeMainService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private ComSearchDao comSearchDao;

	@Action(value = "showFishCrossTaskList")
	public String showFishCrossTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/fish/fishCrossTask.jsp");
	}

	@Action(value = "showFishCrossTaskListJson")
	public void showFishCrossTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = fishCrossTaskService.findFishCrossTaskList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<FishCrossTask> list = (List<FishCrossTask>) result.get("list");
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "");
		map.put("workOrder-id", "");
		map.put("workOrder-name", "");
		map.put("testUser-id", "");
		map.put("testUser-name", "");
		map.put("testDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("state", "");
		map.put("stateName", "");

		// 实验组
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("dicSampleType-id", "");
		map.put("dicSampleType-name", "");

		map.put("confirmDate", "");
		map.put("type", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "FishCrossTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogFishCrossTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/fish/fishCrossTaskDialog.jsp");
	}

	@Action(value = "showDialogFishCrossTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogFishCrossTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = fishCrossTaskService.findFishCrossTaskListByState(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<FishCrossTask> list = (List<FishCrossTask>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("workOrder-id", "");
		map.put("workOrder-name", "");
		map.put("testUser-id", "");
		map.put("testUser-name", "");
		map.put("testDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("state", "");
		map.put("stateName", "");

		map.put("dicSampleType-id", "");
		map.put("dicSampleType-name", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}
	
	//展示要修改的框新加
	@Action(value = "finshSjReason", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSubmitReason() throws Exception {
		String type = getRequest().getParameter("type");
		putObjToContext("type", type);
		return dispatcher("/WEB-INF/page/experiment/fish/showfinshSjReason.jsp");
	}
	
	@Action(value = "showfinshSjReason", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSubmitReasonJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0) {
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		}
		//String type = getParameterFromRequest("type");
		Map<String, Object> result = fishCrossTaskService.findFishCrossTaskListByState(
				map2Query, startNum, limitNum, dir, sort);
		Long total = (Long) result.get("total");
		List<FishCrossTask> list = (List<FishCrossTask>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "");
		map.put("workOrder-id", "");
		map.put("workOrder-name", "");
		map.put("testUser-id", "");
		map.put("testUser-name", "");
		map.put("testDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("state", "");
		map.put("stateName", "");
		// 实验组
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("dicSampleType-id", "");
		map.put("dicSampleType-name", "");

		map.put("confirmDate", "");
		map.put("type", "");
		new SendData().sendDateJson(map, list, total,
				ServletActionContext.getResponse());
	}

	@Action(value = "editFishCrossTask")
	public String editFishCrossTask() throws Exception {
		String id = getParameterFromRequest("id");
		String maxNum = getParameterFromRequest("maxNum");
		long num = 0;
		if (id != null && !id.equals("")) {
			fishCrossTask = fishCrossTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "fishCrossTask");
		} else {
			fishCrossTask.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			fishCrossTask.setCreateUser(user);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stime = format.format(date);
			fishCrossTask.setCreateDate(stime);
			fishCrossTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			fishCrossTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
//			List<Template> tlist=comSearchDao.getTemplateByType("doFishCross");
//			Template t=tlist.get(0);
//			fishCrossTask.setTemplate(t);
			//UserGroup u=comSearchDao.get(UserGroup.class,t.getAcceptUser().getId());
//			if(t.getAcceptUser()!=null){
//				fishCrossTask.setAcceptUser(t.getAcceptUser());
//			}
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(fishCrossTask.getState());
		putObjToContext("fileNum", num);
		putObjToContext("maxNum", maxNum);
		return dispatcher("/WEB-INF/page/experiment/fish/fishCrossTaskEdit.jsp");
	}

	@Action(value = "copyFishCrossTask")
	public String copyFishCrossTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		fishCrossTask = fishCrossTaskService.get(id);
		fishCrossTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/fish/fishCrossTaskEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = fishCrossTask.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "FishCrossTask";
			String markCode = "ZJXT";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			fishCrossTask.setId(autoID);
//			fishCrossTask.setName("Fish杂交洗脱-"+autoID);
		}
		Map aMap = new HashMap();
		aMap.put("fishCrossTaskItem",
				getParameterFromRequest("fishCrossTaskItemJson"));

		aMap.put("fishCrossTaskResult",
				getParameterFromRequest("fishCrossTaskResultJson"));

		aMap.put("fishCrossTaskTemplate",
				getParameterFromRequest("fishCrossTaskTemplateJson"));
		aMap.put("fishCrossTaskReagent",
				getParameterFromRequest("fishCrossTaskReagentJson"));
		aMap.put("fishCrossTaskCos", getParameterFromRequest("fishCrossTaskCosJson"));

		fishCrossTaskService.save(fishCrossTask, aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/experiment/fish/fishCrossTask/editFishCrossTask.action?id=" + fishCrossTask.getId()+"&maxNum="+fishCrossTask.getMaxNum();
		if (bpmTaskId != null && !bpmTaskId.equals("") && !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);
	}

	@Action(value = "viewFishCrossTask")
	public String toViewFishCrossTask() throws Exception {
		String id = getParameterFromRequest("id");
		fishCrossTask = fishCrossTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/fish/fishCrossTaskEdit.jsp");
	}

	@Action(value = "showFishCrossTaskItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showFishCrossTaskItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/fish/fishCrossTaskItem.jsp");
	}

	@Action(value = "showFishCrossTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showFishCrossTaskItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = fishCrossTaskService
					.findFishCrossTaskItemList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<FishCrossTaskItem> list = (List<FishCrossTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("tempId", "");
			map.put("sampleCode", "");
			map.put("name", "");
			map.put("patientName", "");
			map.put("checked", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("state", "");
			map.put("note", "");
			map.put("concentration", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("stepNum", "");
			map.put("fishCrossTask-name", "");
			map.put("fishCrossTask-id", "");
			map.put("storage-name", "");
			map.put("storage-id", "");
			map.put("volume", "");
			map.put("idCard", "");
			map.put("sequencingFun", "");
			map.put("inspectDate", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("orderId", "");
			map.put("phone", "");
			map.put("orderNumber", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("counts", "");
			map.put("classify", "");
			map.put("productNum", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("sampleNum", "");
			map.put("sampleConsume", "");
			map.put("sampleType", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delFishCrossTaskItem")
	public void delFishCrossTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			fishCrossTaskService.delFishCrossTaskItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showFishCrossTaskResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showFishCrossTaskResultList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/fish/fishCrossTaskResult.jsp");
	}

	@Action(value = "showFishCrossTaskResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showFishCrossTaskResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = fishCrossTaskService
					.findFishCrossTaskResultList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<FishCrossTaskResult> list = (List<FishCrossTaskResult>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("tempId", "");
			map.put("code", "");
			map.put("zpCode", "");
			map.put("slideCode", "");
			map.put("sampleCode", "");
			map.put("volume", "");
			map.put("unit", "");

			map.put("result", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("method", "");
			map.put("state", "");
			map.put("note", "");

			map.put("fishCrossTask-id", "");
			map.put("fishCrossTask-name", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("submit", "");

			map.put("patientName", "");
			map.put("idCard", "");
			map.put("sequencingFun", "");
			map.put("inspectDate", "");
			map.put("acceptDate", "yyyy-MM-dd");

			map.put("reportDate", "yyyy-MM-dd");
			map.put("reason", "");
			map.put("concentration", "");
			map.put("orderId", "");
			map.put("phone", "");
			map.put("classify", "");

			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("sampleType", "");
			map.put("sampleNum", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delFishCrossTaskResult")
	public void delFishCrossTaskResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			fishCrossTaskService.delFishCrossTaskResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showFishCrossTaskTemplateList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showFishCrossTaskTemplateList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/fish/fishCrossTaskTemplate.jsp");
	}

	@Action(value = "showFishCrossTaskTemplateListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showFishCrossTaskTemplateListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = fishCrossTaskService
					.findFishCrossTaskTemplateList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<FishCrossTaskTemplate> list = (List<FishCrossTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("testUser-id", "");
			map.put("testUser-name", "");
			map.put("note", "");
			map.put("tItem", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("state", "");
			map.put("codes", "");
			map.put("fishCrossTask-name", "");
			map.put("fishCrossTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delFishCrossTaskTemplate")
	public void delFishCrossTaskTemplate() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			fishCrossTaskService.delFishCrossTaskTemplate(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delFishCrossTaskTemplateOne")
	public void delFishCrossTaskTemplateOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			fishCrossTaskService.delFishCrossTaskTemplateOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showFishCrossTaskReagentList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showFishCrossTaskReagentList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/fish/fishCrossTaskReagent.jsp");
	}

	@Action(value = "showFishCrossTaskReagentListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showFishCrossTaskReagentListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = new HashMap<String, Object>();
			result = fishCrossTaskService.findFishCrossTaskReagentList(scId,
					startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<FishCrossTaskReagent> list = (List<FishCrossTaskReagent>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("batch", "");
			map.put("isGood", "");
			map.put("note", "");
			//
			map.put("oneNum", "");
			map.put("sampleNum", "");
			map.put("num", "");
			map.put("count", "");
			//
			map.put("tReagent", "");
			map.put("itemId", "");
			map.put("fishCrossTask-name", "");
			map.put("fishCrossTask-id", "");
			map.put("sn", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delFishCrossTaskReagent")
	public void delFishCrossTaskReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			fishCrossTaskService.delFishCrossTaskReagent(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delFishCrossTaskReagentOne")
	public void delFishCrossTaskReagentOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			fishCrossTaskService.delFishCrossTaskReagentOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showFishCrossTaskCosList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showFishCrossTaskCosList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/fish/fishCrossTaskCos.jsp");
	}

	@Action(value = "showFishCrossTaskCosListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showFishCrossTaskCosListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = new HashMap<String, Object>();
			result = fishCrossTaskService.findFishCrossTaskCosList(scId, startNum,
					limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<FishCrossTaskCos> list = (List<FishCrossTaskCos>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("isGood", "");
			map.put("tCos", "");
			map.put("itemId", "");
			//
			map.put("temperature", "");
			map.put("speed", "");
			map.put("time", "");
			map.put("note", "");
			//
			map.put("fishCrossTask-name", "");
			map.put("fishCrossTask-id", "");

			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delFishCrossTaskCos")
	public void delFishCrossTaskCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			fishCrossTaskService.delFishCrossTaskCos(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delFishCrossTaskCosOne")
	public void delFishCrossTaskCosOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			fishCrossTaskService.delFishCrossTaskCosOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 打印条码
	@Action(value = "makeCode", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void makeCode() throws Exception {

		String ip = getParameterFromRequest("ip");
		String id = getParameterFromRequest("id");
		String code = getParameterFromRequest("code");
		String name = getParameterFromRequest("name");
		Socket socket = null;
		Template template = null;
		CodeMain codeMain = null;
		OutputStream os;
		try {
			socket = new Socket();
			// 向服务器端第一次发送字符串

			// 向服务器端第二次发送字符串
			SocketAddress sa = new InetSocketAddress(ip, 9100);
			socket.connect(sa);

			os = socket.getOutputStream();

			char c = 13;
			char b = 10;
			String cb = String.valueOf(c) + String.valueOf(b);
			FishCrossTask pt = fishCrossTaskService.get(id);
			if (pt != null && pt.getTemplate() != null
					&& pt.getTemplate().getId() != null) {
				template = templateService.get(pt.getTemplate().getId());
			}
			if (template != null && template.getCodeMain() != null
					&& template.getCodeMain().getId() != null) {
				codeMain = codeMainService.get(template.getCodeMain().getId());
			}
			if (codeMain != null) {
				String codeidx = codeMain.getCodex();
				String codeidy = codeMain.getCodey();
				String codenamex = codeMain.getNamex();
				String codenamey = codeMain.getNamey();
				String qrx = codeMain.getQrx();
				String qry = codeMain.getQry();
				String markCode = codeMain.getCode();
				String printStr = "";
				// for(int i=1;i<5;i++){
				printStr = "e PCX;*"
						+ cb
						+ "e IMG;*"
						+ cb
						+ "mm"
						+ cb
						+ "zO"
						+ cb
						+ "J"
						+ cb
						+ "O R,P"
						+ cb
						+ "H75,0,T"
						+ cb
						+ "D 0.0,0.0"
						+ cb
						+ "M l FNT;/iffs/gb2312"
						+ cb
						+ "F90;gb2312"
						+ cb
						+ "Sl1;0.0,0.0,9.53,9.53,50.90,50.90,1"
						+ cb
						// + "T4.2,4.0,0,90,3.5,q70;" + name + cb +
						// "T4.2,7.6,0,3,2.84,q90;" + id + cb
						+ "T" + codenamex + "," + codenamey + ",0,90,3.5,q70;"
						+ name + cb + "T" + codeidx + "," + codeidy
						+ ",0,3,2.84,q90;" + code + cb + "B" + qrx + "," + qry
						+ ",0,DATAMATRIX,0.33;" + code + cb + "T"
						+ (Float.valueOf(codenamex) + 27) + "," + codenamey
						+ ",0,90,3.5,q70;" + name + cb + "T"
						+ (Float.valueOf(codeidx) + 27) + "," + codeidy
						+ ",0,3,2.84,q90;" + code + cb + "B"
						+ (Float.valueOf(qrx) + 27) + "," + qry
						+ ",0,DATAMATRIX,0.33;" + code + cb + "A 1" + cb;
				// 31.6,2.5
				// }
				os.write(printStr.getBytes("UTF-8"));
			}

			os.flush();

		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
				}
			}
		}
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public FishCrossTaskService getFishCrossTaskService() {
		return fishCrossTaskService;
	}

	public void setFishCrossTaskService(FishCrossTaskService fishCrossTaskService) {
		this.fishCrossTaskService = fishCrossTaskService;
	}

	public FishCrossTask getFishCrossTask() {
		return fishCrossTask;
	}

	public void setFishCrossTask(FishCrossTask fishCrossTask) {
		this.fishCrossTask = fishCrossTask;
	}
	
	public FishCrossTaskDao getFishCrossTaskDao() {
		return fishCrossTaskDao;
	}

	public void setFishCrossTaskDao(FishCrossTaskDao fishCrossTaskDao) {
		this.fishCrossTaskDao = fishCrossTaskDao;
	}
	
	@Action(value = "showFishCrossTasktempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showFishCrossTasktempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/fish/fishCrossTaskTemp.jsp");
	}

	@Action(value = "showFishCrossTasktempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showFishCrossTasktempListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = fishCrossTaskService.selectFishCrossTaskTempList(map2Query, startNum, limitNum, dir, sort);
		Long total = (Long) result.get("total");
		List<FishCrossTaskTemp> list = (List<FishCrossTaskTemp>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("name", "");
		map.put("patientName", "");
		map.put("reason", "");
		map.put("gender", "");
		map.put("doctor", "");
		map.put("hospital", "");
		map.put("note", "");
		map.put("state", "");
		map.put("stateName", "");

		map.put("productId", "");
		map.put("productName", "");
		map.put("method", "");
		map.put("concentration", "");
		map.put("result", "");
		map.put("stepNum", "");

		map.put("volume", "");
		map.put("idCard", "");
		map.put("sequencingFun", "");
		map.put("inspectDate", "");
		map.put("acceptDate", "yyyy-MM-dd");

		map.put("reportDate", "yyyy-MM-dd");
		map.put("orderId", "");
		map.put("phone", "");
		map.put("classify", "");
		map.put("sampleNum", "");
		map.put("sampleType", "");
		map.put("chargeNote", "");//缴费状态
		new SendData().sendDateJson(map, list, total,
				ServletActionContext.getResponse());
	}

	// 判断样本做血浆提取的次数
	@Action(value = "selectCodeCount")
	public void selectCodeCount() throws Exception {
		String code = getParameterFromRequest("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			Long num = fishCrossTaskDao.selectCount(code);
			int num1 = Integer.valueOf(String.valueOf(num));
			result.put("success", true);
			result.put("data", num1);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 模板
	@Action(value = "showTemplateWaitList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTemplateWaitList() throws Exception {
		String scId = getRequest().getParameter("id");
		putObjToContext("id", scId);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/fish/showTemplateWaitList.jsp");
	}

	@Action(value = "showTemplateWaitListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTemplateWaitListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = fishCrossTaskService
					.findFishCrossTaskTemplateList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<FishCrossTaskTemplate> list = (List<FishCrossTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("note", "");
			map.put("tItem", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("testUser-id", "");
			map.put("testUser-name", "");
			map.put("sampleCodes", "");
			map.put("state", "");
			map.put("FishCrossTask-name", "");
			map.put("FishCrossTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 根据选中的步骤异步加载相关的试剂数据
	@Action(value = "setReagent", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void setReagent() throws Exception {
		String code = getRequest().getParameter("code");
		String id = getRequest().getParameter("tid");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.fishCrossTaskService
					.setReagent(id, code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 根据选中的步骤异步加载相关的设备数据
	@Action(value = "setCos", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void setCos() throws Exception {
		String code = getRequest().getParameter("code");
		String id = getRequest().getParameter("tid");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.fishCrossTaskService
					.setCos(id, code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	/**
	 * 根据FishCrossTask加载info子表明细
	 * @throws Exception
	 */
	@Action(value = "setResultToFishSJ", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void setResultToFishSJ() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.fishCrossTaskService
					.showFishCrossTaskResultList(code);
			result.put("success", true);
			result.put("data", dataListMap);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	/**
	 *  提交样本
	 * @throws Exception
	 */
	@Action(value = "submitSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void submitSample() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.fishCrossTaskService.submitSample(id, ids);

			result.put("success", true);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	 /**
		 * 保存杂交洗脱样本明细
		 * @throws Exception
		 */
		@Action(value = "saveFishCrossTaskItem",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
		public void saveFishCrossTaskItem() throws Exception {
			String id = getRequest().getParameter("id");
			String itemDataJson = getRequest().getParameter("itemDataJson");
			Map<String, Object> result = new HashMap<String, Object>();
			try {
				FishCrossTask sc=commonDAO.get(FishCrossTask.class, id);
				Map aMap = new HashMap();
				aMap.put("fishCrossTaskItem",
						itemDataJson);
				if(sc!=null){
					fishCrossTaskService.save(sc,aMap);
				}
				result.put("success", true);
			} catch (Exception e) {
				e.printStackTrace();
				result.put("success", false);
			}
			HttpUtils.write(JsonUtils.toJsonString(result));
		}
		
		 /**
		 * 保存杂交洗脱结果
		 * @throws Exception
		 */
		@Action(value = "saveFishCrossTaskResult",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
		public void saveFishCrossTaskResult() throws Exception {
			String id = getRequest().getParameter("id");
			String itemDataJson = getRequest().getParameter("itemDataJson");
			Map<String, Object> result = new HashMap<String, Object>();
			try {
				FishCrossTask sc=commonDAO.get(FishCrossTask.class, id);
				Map aMap = new HashMap();
				aMap.put("fishCrossTaskResult",
						itemDataJson);
				if(sc!=null){
					fishCrossTaskService.save(sc,aMap);
				}
				result.put("success", true);
			} catch (Exception e) {
				e.printStackTrace();
				result.put("success", false);
			}
			HttpUtils.write(JsonUtils.toJsonString(result));
		}
}
