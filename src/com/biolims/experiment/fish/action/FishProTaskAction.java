﻿package com.biolims.experiment.fish.action;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.comsearch.dao.ComSearchDao;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.fish.dao.FishProTaskDao;
import com.biolims.experiment.fish.model.FishProTask;
import com.biolims.experiment.fish.model.FishProTaskCos;
import com.biolims.experiment.fish.model.FishProTaskItem;
import com.biolims.experiment.fish.model.FishProTaskReagent;
import com.biolims.experiment.fish.model.FishProTaskResult;
import com.biolims.experiment.fish.model.FishProTaskTemp;
import com.biolims.experiment.fish.model.FishProTaskTemplate;
import com.biolims.experiment.fish.model.SamplePreTask;
import com.biolims.experiment.fish.service.FishProTaskService;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.syscode.model.CodeMain;
import com.biolims.system.syscode.service.CodeMainService;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.service.TemplateService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/fish/fishProTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class FishProTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "248902";
	@Autowired
	private FishProTaskService fishProTaskService;
	private FishProTask fishProTask = new FishProTask();
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private FishProTaskDao fishProTaskDao;
	@Resource
	private TemplateService templateService;
	@Resource
	private CodeMainService codeMainService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private ComSearchDao comSearchDao;

	@Action(value = "showFishProTaskList")
	public String showFishProTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/fish/fishProTask.jsp");
	}

	@Action(value = "showFishProTaskListJson")
	public void showFishProTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = fishProTaskService.findFishProTaskList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<FishProTask> list = (List<FishProTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "");
		map.put("workOrder-id", "");
		map.put("workOrder-name", "");
		map.put("testUser-id", "");
		map.put("testUser-name", "");
		map.put("testDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("state", "");
		map.put("stateName", "");

		// 实验组
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("dicSampleType-id", "");
		map.put("dicSampleType-name", "");

		map.put("confirmDate", "");
		map.put("type", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "FishProTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogFishProTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/fish/fishProTaskDialog.jsp");
	}

	@Action(value = "showDialogFishProTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogFishProTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = fishProTaskService.findFishProTaskList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<FishProTask> list = (List<FishProTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("workOrder-id", "");
		map.put("workOrder-name", "");
		map.put("testUser-id", "");
		map.put("testUser-name", "");
		map.put("testDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("state", "");
		map.put("stateName", "");

		map.put("dicSampleType-id", "");
		map.put("dicSampleType-name", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editFishProTask")
	public String editFishProTask() throws Exception {
		String id = getParameterFromRequest("id");
		String maxNum = getParameterFromRequest("maxNum");
		long num = 0;
		if (id != null && !id.equals("")) {
			fishProTask = fishProTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "fishProTask");
		} else {
			fishProTask.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			fishProTask.setCreateUser(user);
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stime = format.format(date);
			fishProTask.setCreateDate(stime);
			fishProTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			fishProTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
//			List<Template> tlist=comSearchDao.getTemplateByType("doFishPro");
//			Template t=tlist.get(0);
//			fishProTask.setTemplate(t);
//			//UserGroup u=comSearchDao.get(UserGroup.class,t.getAcceptUser().getId());
//			if(t.getAcceptUser()!=null){
//				fishProTask.setAcceptUser(t.getAcceptUser());
//			}
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(fishProTask.getState());
		putObjToContext("fileNum", num);
		putObjToContext("maxNum", maxNum);
		return dispatcher("/WEB-INF/page/experiment/fish/fishProTaskEdit.jsp");
	}

	@Action(value = "copyFishProTask")
	public String copyFishProTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		fishProTask = fishProTaskService.get(id);
		fishProTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/fish/fishProTaskEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = fishProTask.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "FishProTask";
			String markCode = "FSZP";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			fishProTask.setId(autoID);
//			fishProTask.setName("Fish制片实验-"+autoID);
		}
		Map aMap = new HashMap();
		aMap.put("fishProTaskItem",
				getParameterFromRequest("fishProTaskItemJson"));

		aMap.put("fishProTaskResult",
				getParameterFromRequest("fishProTaskResultJson"));

		aMap.put("fishProTaskTemplate",
				getParameterFromRequest("fishProTaskTemplateJson"));
		aMap.put("fishProTaskReagent",
				getParameterFromRequest("fishProTaskReagentJson"));
		aMap.put("fishProTaskCos", getParameterFromRequest("fishProTaskCosJson"));

		fishProTaskService.save(fishProTask, aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/experiment/fish/fishProTask/editFishProTask.action?id=" + fishProTask.getId()+"&maxNum="+fishProTask.getMaxNum();
		if (bpmTaskId != null && !bpmTaskId.equals("") && !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);
	}

	@Action(value = "viewFishProTask")
	public String toViewFishProTask() throws Exception {
		String id = getParameterFromRequest("id");
		fishProTask = fishProTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/fish/fishProTaskEdit.jsp");
	}

	@Action(value = "showFishProTaskItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showFishProTaskItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/fish/fishProTaskItem.jsp");
	}

	@Action(value = "showFishProTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showFishProTaskItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = fishProTaskService
					.findFishProTaskItemList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<FishProTaskItem> list = (List<FishProTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("tempId", "");
			map.put("sampleCode", "");
			map.put("name", "");
			map.put("patientName", "");
			map.put("checked", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("state", "");
			map.put("note", "");
			map.put("concentration", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("stepNum", "");
			map.put("fishProTask-name", "");
			map.put("fishProTask-id", "");
			map.put("storage-name", "");
			map.put("storage-id", "");
			map.put("volume", "");
			map.put("idCard", "");
			map.put("sequencingFun", "");
			map.put("inspectDate", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("orderId", "");
			map.put("phone", "");
			map.put("orderNumber", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("counts", "");
			map.put("classify", "");
			map.put("productNum", "");
			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("sampleNum", "");
			map.put("sampleConsume", "");
			map.put("sampleType", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delFishProTaskItem")
	public void delFishProTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			fishProTaskService.delFishProTaskItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showFishProTaskResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showFishProTaskResultList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/fish/fishProTaskResult.jsp");
	}

	@Action(value = "showFishProTaskResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showFishProTaskResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = fishProTaskService
					.findFishProTaskResultList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<FishProTaskResult> list = (List<FishProTaskResult>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("tempId", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("yclCode", "");
			map.put("volume", "");
			map.put("unit", "");
			
			map.put("slideCode", "");
			map.put("crossPlace", "");
			map.put("probe", "");
			
			map.put("result", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("method", "");
			map.put("state", "");
			map.put("note", "");

			map.put("fishProTask-id", "");
			map.put("fishProTask-name", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("submit", "");

			map.put("patientName", "");
			map.put("idCard", "");
			map.put("sequencingFun", "");
			map.put("inspectDate", "");
			map.put("acceptDate", "yyyy-MM-dd");

			map.put("reportDate", "yyyy-MM-dd");
			map.put("reason", "");
			map.put("concentration", "");
			map.put("orderId", "");
			map.put("phone", "");
			map.put("classify", "");

			map.put("dicSampleType-id", "");
			map.put("dicSampleType-name", "");
			map.put("sampleType", "");
			map.put("sampleNum", "");
			map.put("probes-id", "");
			map.put("probes-name","");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delFishProTaskResult")
	public void delFishProTaskResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			fishProTaskService.delFishProTaskResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showFishProTaskTemplateList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showFishProTaskTemplateList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/fish/fishProTaskTemplate.jsp");
	}

	@Action(value = "showFishProTaskTemplateListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showFishProTaskTemplateListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = fishProTaskService
					.findFishProTaskTemplateList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<FishProTaskTemplate> list = (List<FishProTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("testUser-id", "");
			map.put("testUser-name", "");
			map.put("note", "");
			map.put("tItem", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("state", "");
			map.put("codes", "");
			map.put("fishProTask-name", "");
			map.put("fishProTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delFishProTaskTemplate")
	public void delFishProTaskTemplate() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			fishProTaskService.delFishProTaskTemplate(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delFishProTaskTemplateOne")
	public void delFishProTaskTemplateOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			fishProTaskService.delFishProTaskTemplateOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showFishProTaskReagentList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showFishProTaskReagentList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/fish/fishProTaskReagent.jsp");
	}

	@Action(value = "showFishProTaskReagentListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showFishProTaskReagentListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = new HashMap<String, Object>();
			result = fishProTaskService.findFishProTaskReagentList(scId,
					startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<FishProTaskReagent> list = (List<FishProTaskReagent>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("batch", "");
			map.put("isGood", "");
			map.put("note", "");
			//
			map.put("oneNum", "");
			map.put("sampleNum", "");
			map.put("num", "");
			map.put("count", "");
			//
			map.put("tReagent", "");
			map.put("itemId", "");
			map.put("fishProTask-name", "");
			map.put("fishProTask-id", "");
			map.put("sn", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delFishProTaskReagent")
	public void delFishProTaskReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			fishProTaskService.delFishProTaskReagent(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据ID删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delFishProTaskReagentOne")
	public void delFishProTaskReagentOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			fishProTaskService.delFishProTaskReagentOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showFishProTaskCosList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showFishProTaskCosList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/fish/fishProTaskCos.jsp");
	}

	@Action(value = "showFishProTaskCosListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showFishProTaskCosListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = new HashMap<String, Object>();
			result = fishProTaskService.findFishProTaskCosList(scId, startNum,
					limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<FishProTaskCos> list = (List<FishProTaskCos>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("isGood", "");
			map.put("tCos", "");
			map.put("itemId", "");
			//
			map.put("temperature", "");
			map.put("speed", "");
			map.put("time", "");
			map.put("note", "");
			//
			map.put("fishProTask-name", "");
			map.put("fishProTask-id", "");

			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delFishProTaskCos")
	public void delFishProTaskCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			fishProTaskService.delFishProTaskCos(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delFishProTaskCosOne")
	public void delFishProTaskCosOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			fishProTaskService.delFishProTaskCosOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 打印条码
	@Action(value = "makeCode", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void makeCode() throws Exception {

		String ip = getParameterFromRequest("ip");
		String id = getParameterFromRequest("id");
		String code = getParameterFromRequest("code");
		String name = getParameterFromRequest("name");
		Socket socket = null;
		Template template = null;
		CodeMain codeMain = null;
		OutputStream os;
		try {
			socket = new Socket();
			// 向服务器端第一次发送字符串

			// 向服务器端第二次发送字符串
			SocketAddress sa = new InetSocketAddress(ip, 9100);
			socket.connect(sa);

			os = socket.getOutputStream();

			char c = 13;
			char b = 10;
			String cb = String.valueOf(c) + String.valueOf(b);
			FishProTask pt = fishProTaskService.get(id);
			if (pt != null && pt.getTemplate() != null
					&& pt.getTemplate().getId() != null) {
				template = templateService.get(pt.getTemplate().getId());
			}
			if (template != null && template.getCodeMain() != null
					&& template.getCodeMain().getId() != null) {
				codeMain = codeMainService.get(template.getCodeMain().getId());
			}
			if (codeMain != null) {
				String codeidx = codeMain.getCodex();
				String codeidy = codeMain.getCodey();
				String codenamex = codeMain.getNamex();
				String codenamey = codeMain.getNamey();
				String qrx = codeMain.getQrx();
				String qry = codeMain.getQry();
				String markCode = codeMain.getCode();
				String printStr = "";
				// for(int i=1;i<5;i++){
				printStr = "e PCX;*"
						+ cb
						+ "e IMG;*"
						+ cb
						+ "mm"
						+ cb
						+ "zO"
						+ cb
						+ "J"
						+ cb
						+ "O R,P"
						+ cb
						+ "H75,0,T"
						+ cb
						+ "D 0.0,0.0"
						+ cb
						+ "M l FNT;/iffs/gb2312"
						+ cb
						+ "F90;gb2312"
						+ cb
						+ "Sl1;0.0,0.0,9.53,9.53,50.90,50.90,1"
						+ cb
						// + "T4.2,4.0,0,90,3.5,q70;" + name + cb +
						// "T4.2,7.6,0,3,2.84,q90;" + id + cb
						+ "T" + codenamex + "," + codenamey + ",0,90,3.5,q70;"
						+ name + cb + "T" + codeidx + "," + codeidy
						+ ",0,3,2.84,q90;" + code + cb + "B" + qrx + "," + qry
						+ ",0,DATAMATRIX,0.33;" + code + cb + "T"
						+ (Float.valueOf(codenamex) + 27) + "," + codenamey
						+ ",0,90,3.5,q70;" + name + cb + "T"
						+ (Float.valueOf(codeidx) + 27) + "," + codeidy
						+ ",0,3,2.84,q90;" + code + cb + "B"
						+ (Float.valueOf(qrx) + 27) + "," + qry
						+ ",0,DATAMATRIX,0.33;" + code + cb + "A 1" + cb;
				// 31.6,2.5
				// }
				os.write(printStr.getBytes("UTF-8"));
			}

			os.flush();

		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
				}
			}
		}
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public FishProTaskService getFishProTaskService() {
		return fishProTaskService;
	}

	public void setFishProTaskService(FishProTaskService fishProTaskService) {
		this.fishProTaskService = fishProTaskService;
	}

	public FishProTask getFishProTask() {
		return fishProTask;
	}

	public void setFishProTask(FishProTask fishProTask) {
		this.fishProTask = fishProTask;
	}
	
	public FishProTaskDao getFishProTaskDao() {
		return fishProTaskDao;
	}

	public void setFishProTaskDao(FishProTaskDao fishProTaskDao) {
		this.fishProTaskDao = fishProTaskDao;
	}
	
	@Action(value = "showFishProTasktempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showFishProTasktempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/fish/fishProTaskTemp.jsp");
	}

	@Action(value = "showFishProTasktempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showFishProTasktempListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = fishProTaskService.selectFishProTaskTempList(map2Query, startNum, limitNum, dir, sort);
		Long total = (Long) result.get("total");
		List<FishProTaskTemp> list = (List<FishProTaskTemp>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("sampleCode", "");
		map.put("code", "");
		map.put("name", "");
		map.put("patientName", "");
		map.put("reason", "");
		map.put("gender", "");
		map.put("doctor", "");
		map.put("hospital", "");
		map.put("note", "");
		map.put("state", "");
		map.put("stateName", "");

		map.put("productId", "");
		map.put("productName", "");
		map.put("method", "");
		map.put("concentration", "");
		map.put("result", "");
		map.put("stepNum", "");

		map.put("volume", "");
		map.put("idCard", "");
		map.put("sequencingFun", "");
		map.put("inspectDate", "");
		map.put("acceptDate", "yyyy-MM-dd");

		map.put("reportDate", "yyyy-MM-dd");
		map.put("orderId", "");
		map.put("phone", "");
		map.put("classify", "");
		map.put("sampleNum", "");
		map.put("sampleType", "");
		map.put("chargeNote", "");//缴费状态
		new SendData().sendDateJson(map, list, total,
				ServletActionContext.getResponse());
	}

	// 判断样本做血浆提取的次数
	@Action(value = "selectCodeCount")
	public void selectCodeCount() throws Exception {
		String code = getParameterFromRequest("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			Long num = fishProTaskDao.selectCount(code);
			int num1 = Integer.valueOf(String.valueOf(num));
			result.put("success", true);
			result.put("data", num1);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 模板
	@Action(value = "showTemplateWaitList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTemplateWaitList() throws Exception {
		String scId = getRequest().getParameter("id");
		putObjToContext("id", scId);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/fish/showTemplateWaitList.jsp");
	}

	@Action(value = "showTemplateWaitListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTemplateWaitListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = fishProTaskService
					.findFishProTaskTemplateList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<FishProTaskTemplate> list = (List<FishProTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("name", "");
			map.put("note", "");
			map.put("tItem", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("testUser-id", "");
			map.put("testUser-name", "");
			map.put("sampleCodes", "");
			map.put("state", "");
			map.put("FishProTask-name", "");
			map.put("FishProTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 根据选中的步骤异步加载相关的试剂数据
	@Action(value = "setReagent")
	public void setReagent() throws Exception {
		String code = getRequest().getParameter("code");
		String id = getRequest().getParameter("tid");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.fishProTaskService
					.setReagent(id, code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 根据选中的步骤异步加载相关的设备数据
	@Action(value = "setCos")
	public void setCos() throws Exception {
		String code = getRequest().getParameter("code");
		String id = getRequest().getParameter("tid");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.fishProTaskService
					.setCos(id, code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	/**
	 *  提交样本
	 * @throws Exception
	 */
	@Action(value = "submitSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void submitSample() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.fishProTaskService.submitSample(id, ids);

			result.put("success", true);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	/**
	 * 保存制片样本明细
	 * @throws Exception
	 */
	@Action(value = "saveFishProTaskItem",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveFishProTaskItem() throws Exception {
		String id = getRequest().getParameter("id");
		String itemDataJson = getRequest().getParameter("itemDataJson");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			FishProTask sc=commonDAO.get(FishProTask.class, id);
			Map aMap = new HashMap();
			aMap.put("fishProTaskItem",
					itemDataJson);
			if(sc!=null){
				fishProTaskService.save(sc,aMap);
			}
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	 /**
	 * 保存制片结果
	 * @throws Exception
	 */
	@Action(value = "saveFishProTaskResult",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveFishProTaskResult() throws Exception {
		String id = getRequest().getParameter("id");
		String itemDataJson = getRequest().getParameter("itemDataJson");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			FishProTask sc=commonDAO.get(FishProTask.class, id);
			Map aMap = new HashMap();
			aMap.put("fishProTaskResult",
					itemDataJson);
			if(sc!=null){
				fishProTaskService.save(sc,aMap);
			}
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

}


