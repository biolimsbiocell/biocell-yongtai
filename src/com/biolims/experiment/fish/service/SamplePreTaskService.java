package com.biolims.experiment.fish.service;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.dna.model.DnaTaskTemp;
import com.biolims.experiment.fish.dao.SamplePreTaskDao;
import com.biolims.experiment.fish.fishabnormal.model.FishAbnormal;
import com.biolims.experiment.fish.model.FishProTaskTemp;
import com.biolims.experiment.fish.model.SamplePreTask;
import com.biolims.experiment.fish.model.SamplePreTaskCos;
import com.biolims.experiment.fish.model.SamplePreTaskItem;
import com.biolims.experiment.fish.model.SamplePreTaskReagent;
import com.biolims.experiment.fish.model.SamplePreTaskResult;
import com.biolims.experiment.fish.model.SamplePreTaskTemp;
import com.biolims.experiment.fish.model.SamplePreTaskTemplate;
import com.biolims.experiment.karyoabnormal.model.KaryoAbnormal;
import com.biolims.experiment.karyoget.model.KaryoGetTaskTemp;
import com.biolims.experiment.karyoship.model.KaryoShipTaskItem;
import com.biolims.experiment.karyoship.model.KaryoShipTaskReagent;
import com.biolims.experiment.producer.model.ProducerTaskItem;
import com.biolims.experiment.producer.model.ProducerTaskReagent;
import com.biolims.experiment.snpjc.abnormal.model.SnpAbnormal;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.report.model.EndReport;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.storage.container.model.StorageContainer;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.service.TemplateService;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SamplePreTaskService {
	@Resource
	private SamplePreTaskDao samplePreTaskDao;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private TemplateService templateService;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findSamplePreTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return samplePreTaskDao.selectSamplePreTaskList(mapForQuery, startNum,
				limitNum, dir, sort);
	}
	/**
	 * 获取样本接收的样本
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectSamplePreTaskTempList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort)
			throws Exception {
		/*Map<String, Object> result = samplePreTaskDao
				.selectSamplePreTaskTempList(mapForQuery, startNum,
						limitNum, dir, sort);
		List<SamplePreTaskTemp> list = (List<SamplePreTaskTemp>) result.get("list");
		return result;*/
		Map<String, Object> result2 = new HashMap<String, Object>();
		List<SamplePreTaskTemp> list2=new ArrayList<SamplePreTaskTemp>();
		Map<String, Object> result = samplePreTaskDao
				.selectSamplePreTaskTempList(mapForQuery,
				startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<SamplePreTaskTemp> list = (List<SamplePreTaskTemp>) result.get("list");
		for(SamplePreTaskTemp t:list){
			SampleOrder s=commonDAO.get(SampleOrder.class, t.getSampleCode());
			if(s!=null){
				t.setChargeNote(s.getChargeNote());
				t.setPatientName(s.getName());
				if(s.getAttendingDoctor()!=null){
					t.setDoctor(s.getAttendingDoctor());
				}else{
					t.setDoctor("");
				}
				if(s.getCrmCustomer()!=null){
					t.setHospital(s.getCrmCustomer().getName());
				}else{
					t.setHospital("");
				}
				t.setGender(s.getGender());
				t.setReason(s.getSubmitReasonName());
				t.setAcceptDate(s.getCreateDate());
			}
			list2.add(t);
		}
		result2.put("total", count);
		result2.put("list", list2);
		return result2;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SamplePreTask i) throws Exception {

		samplePreTaskDao.saveOrUpdate(i);

	}

	public SamplePreTask get(String id) {
		SamplePreTask samplePreTask = commonDAO.get(SamplePreTask.class, id);
		return samplePreTask;
	}

	public Map<String, Object> findSamplePreTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = samplePreTaskDao.selectSamplePreTaskItemList(
				scId, startNum, limitNum, dir, sort);
		List<SamplePreTaskItem> list = (List<SamplePreTaskItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findSamplePreTaskResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = samplePreTaskDao.selectBloodSampleInfoList(
				scId, startNum, limitNum, dir, sort);
		List<SamplePreTaskResult> list = (List<SamplePreTaskResult>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findSamplePreTaskTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = samplePreTaskDao
				.selectSamplePreTaskTemplateList(scId, startNum, limitNum, dir,
						sort);
		List<SamplePreTaskTemplate> list = (List<SamplePreTaskTemplate>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findSamplePreTaskReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = samplePreTaskDao.selectSamplePreTaskReagentList(
				scId, startNum, limitNum, dir, sort);
		List<SamplePreTaskReagent> list = (List<SamplePreTaskReagent>) result
				.get("list");
		return result;
	}

	// 根据步骤加载试剂明细
	public Map<String, Object> findSamplePreTaskReagentListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		Map<String, Object> result = samplePreTaskDao
				.selectSamplePreTaskReagentListByItemId(scId, startNum, limitNum,
						dir, sort, itemId);
		List<SamplePreTaskReagent> list = (List<SamplePreTaskReagent>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findSamplePreTaskCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = samplePreTaskDao.selectSamplePreTaskCosList(
				scId, startNum, limitNum, dir, sort);
		List<SamplePreTaskCos> list = (List<SamplePreTaskCos>) result.get("list");
		return result;
	}

	public Map<String, Object> findCosItemListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		Map<String, Object> result = samplePreTaskDao
				.selectSamplePreTaskCosListByItemId(scId, startNum, limitNum, dir,
						sort, itemId);
		List<SamplePreTaskCos> list = (List<SamplePreTaskCos>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSamplePreTaskItem(SamplePreTask sc, String itemDataJson)
			throws Exception {
		List<SamplePreTaskItem> saveItems = new ArrayList<SamplePreTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		Template temp = null;
		StorageContainer storageContainer = null;
		Integer rowCode = null;
		Integer colCode = null;
		Integer maxNum = null;
		if (sc.getTemplate() != null && !sc.getTemplate().equals("")) {
			temp = templateDao.get(Template.class, sc.getTemplate().getId());
			if (temp.getStorageContainer() != null
					&& !temp.getStorageContainer().equals("")) {
				storageContainer = samplePreTaskDao.get(StorageContainer.class,
						temp.getStorageContainer().getId());
				rowCode = storageContainer.getRowNum();
				colCode = storageContainer.getColNum();

			}
		}

		for (Map<String, Object> map : list) {
			SamplePreTaskItem scp = new SamplePreTaskItem();
			// 将map信息读入实体类
			scp = (SamplePreTaskItem) samplePreTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			if (scp.getOrderNumber() != null
					&& !scp.getOrderNumber().equals("")) {
				if (rowCode != null && colCode != null) {
					Double num;
					if ((scp.getOrderNumber() - 1) / (rowCode * colCode) > 0) {
						int count = scp.getOrderNumber() / (rowCode * colCode);
						num = Math.floor((scp.getOrderNumber() - count
								* rowCode * colCode - 1)
								/ rowCode) + 1;
					} else {
						num = Math.floor((scp.getOrderNumber() - 1) / rowCode) + 1;
					}
					scp.setColCode(Integer.toString(num.intValue()));
					if (scp.getOrderNumber() % rowCode == 0) {
						scp.setRowCode(String.valueOf((char) (64 + rowCode)));
					} else {
						scp.setRowCode(String.valueOf((char) (64 + scp
								.getOrderNumber() % rowCode)));
					}
					scp.setCounts(String.valueOf((scp.getOrderNumber() - 1)
							/ (rowCode * colCode) + 1));
				}
			}

			scp.setSamplePreTask(sc);
			saveItems.add(scp);
			if(scp!=null){
				Template t=commonDAO.get(Template.class, sc.getTemplate().getId());
				//DicSampleType d=new DicSampleType();
				if(t!=null){
					if(t.getDicSampleType()!=null){
						DicSampleType d=commonDAO.get(DicSampleType.class,
								t.getDicSampleType().getId());
						if(scp.getDicSampleType()==null ||
								(scp.getDicSampleType()!=null && scp.getDicSampleType().equals(""))){
							if(d!=null){
								scp.setDicSampleType(d);
							}
						}
					}
					if(t.getProductNum()!=null){
						if(scp.getProductNum()==null || 
								(scp.getProductNum()!=null && scp.getProductNum().equals(""))){
							scp.setProductNum(t.getProductNum());
						}
					}
				}
			}
			// 改变左侧样本状态
			SamplePreTaskTemp dt = commonDAO.get(SamplePreTaskTemp.class, scp.getTempId());
			if (dt != null) {
				dt.setState("2");
			}
		}
		samplePreTaskDao.saveOrUpdateAll(saveItems);
//		List<SamplePreTaskItem> snlist=samplePreTaskDao.getItem(sc.getId());
//		List<SamplePreTaskReagent> rglist=samplePreTaskDao.getReagent(sc.getId());
//		if(snlist.size()>0){
//			if(rglist.size()>0){
//				//把样本数量传到试剂明细中
//				for(SamplePreTaskReagent sr:rglist){
//					sr.setSampleNum(Double.valueOf(snlist.size()));
//				}
//			}
//		}
		if (sc.getTemplate() != null && !sc.getTemplate().equals("")) {
			temp = templateDao.get(Template.class, sc.getTemplate().getId());
			if (temp.getStorageContainer() != null
					&& !temp.getStorageContainer().equals("")) {
				maxNum = Integer.valueOf(samplePreTaskDao.selectCountMax(
						sc.getId()).toString());
				sc.setMaxNum((maxNum - 1) / (rowCode * colCode) + 1);
			}
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSamplePreTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			SamplePreTaskItem scp = samplePreTaskDao.get(SamplePreTaskItem.class, id);
			if (scp.getId() != null) {
				samplePreTaskDao.delete(scp);
			}
			// 改变左侧样本状态
			SamplePreTaskTemp dt = commonDAO.get(SamplePreTaskTemp.class, scp.getTempId());
			if (dt != null) {
				dt.setState("1");
			}
			commonDAO.update(dt);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSamplePreTaskResult(SamplePreTask sc, String itemDataJson)
			throws Exception {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		List<SamplePreTaskResult> saveItems = new ArrayList<SamplePreTaskResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SamplePreTaskResult scp = new SamplePreTaskResult();
			// 将map信息读入实体类
			scp = (SamplePreTaskResult) samplePreTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSamplePreTask(sc);
			if (scp.getCode() == null || scp.getCode().equals("")) {
				String markCode = "";
				if (scp.getDicSampleType() != null
						|| !scp.getDicSampleType().equals("")) {
					DicSampleType d = commonDAO.get(DicSampleType.class, scp
							.getDicSampleType().getId());

					markCode = scp.getSampleCode() + d.getCode();
				}
				String code = codingRuleService.getCode("SamplePreTaskResult",
						markCode, 00, 2, null);
				scp.setCode(code);

			}
			saveItems.add(scp);
			SampleInfo sf = sampleInfoMainDao.findSampleInfo(scp.getCode());
//			if (scp != null && scp.getSubmit() != null
//					&& scp.getResult() != null && scp.getSubmit().equals("1")) {
//				if (scp.getResult().equals("1")) {// 合格
//					String next = scp.getNextFlowId();
			if (scp != null){
			if (scp.getResult() != null && scp.getSubmit() != null &&
					!scp.getResult().equals("") && !scp.getSubmit().equals("") ){
			if(scp.getSubmit().equals("1")){
				if(scp.getResult().equals("1")){
					if(scp.getNextFlowId()!=null && !scp.getNextFlowId().equals("")){
							String next=scp.getNextFlowId();
						if (next.equals("0009")) {// 样本入库
							SampleInItemTemp st = new SampleInItemTemp();
							st.setCode(scp.getCode());
							st.setSampleCode(scp.getSampleCode());
							st.setNum(scp.getSampleNum());
							st.setState("1");
							samplePreTaskDao.saveOrUpdate(st);
							// 入库，改变SampleInfo中原始样本的状态为“待入库”
							if (sf != null) {
								sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW);
								sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW_NAME);
							}
						} else if (next.equals("0012")) {// 暂停
							// 暂停，改变SampleInfo中原始样本的状态为“实验暂停”
							if (sf != null) {
								sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
								sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
							}
						} else if (next.equals("0013")) {// 终止
							// 终止，改变SampleInfo中原始样本的状态为“实验终止”
							if (sf != null) {
								sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
								sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
							}
						} else {
							// 得到下一步流向的相关表单
							List<NextFlow> list_nextFlow = nextFlowDao
									.seletNextFlowById(next);
							for (NextFlow n : list_nextFlow) {
								Object o = Class.forName(
										n.getApplicationTypeTable().getClassPath())
										.newInstance();
								scp.setState("1");
								sampleInputService.copy(o, scp);
							}
						}
					}
				}else{
					FishAbnormal ka=new FishAbnormal();
					ka.setCode(scp.getCode());
					ka.setSampleCode(scp.getSampleCode());
					ka.setProductId(scp.getProductId());
					ka.setProductName(scp.getProductName());
					ka.setOrderId(scp.getOrderId());
					ka.setAcceptDate(scp.getAcceptDate());
					ka.setReportDate(scp.getReportDate());
					ka.setSampleType(scp.getSampleType());
					ka.setTaskId(scp.getSamplePreTask().getId());
					ka.setNote(scp.getNote());
					ka.setTaskName("Fish样本预处理");
					ka.setState("1");
					
					commonDAO.saveOrUpdate(ka);
				}
				sampleStateService
						.saveSampleState(
								scp.getCode(),
								scp.getSampleCode(),
								scp.getProductId(),
								scp.getProductName(),
								"",
								sc.getCreateDate(),
								format.format(new Date()),
								"SamplePreTask",
								"样本预处理",
								(User) ServletActionContext
										.getRequest()
										.getSession()
										.getAttribute(
												SystemConstants.USER_SESSION_KEY),
								sc.getId(), scp.getNextFlow(), scp.getResult(),
								null, null, null, null, null, null, null, null);
					}
				}
			}
			samplePreTaskDao.saveOrUpdateAll(saveItems);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSamplePreTaskResult(String[] ids) throws Exception {
		for (String id : ids) {
			SamplePreTaskResult scp = samplePreTaskDao
					.get(SamplePreTaskResult.class, id);
			samplePreTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSamplePreTaskTemplate(SamplePreTask sc, String itemDataJson)
			throws Exception {
		List<SamplePreTaskTemplate> saveItems = new ArrayList<SamplePreTaskTemplate>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SamplePreTaskTemplate scp = new SamplePreTaskTemplate();
			// 将map信息读入实体类
			scp = (SamplePreTaskTemplate) samplePreTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSamplePreTask(sc);
			saveItems.add(scp);
		}
		samplePreTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSamplePreTaskTemplate(String[] ids) throws Exception {
		for (String id : ids) {
			SamplePreTaskTemplate scp = samplePreTaskDao.get(
					SamplePreTaskTemplate.class, id);
			samplePreTaskDao.delete(scp);
		}
	}

	// ============2015-12-02 ly================
	/**
	 * 根据ID删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSamplePreTaskTemplateOne(String ids) throws Exception {
		SamplePreTaskTemplate scp = samplePreTaskDao.get(SamplePreTaskTemplate.class,
				ids);
		samplePreTaskDao.delete(scp);
	}

	// ============================
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSamplePreTaskReagent(SamplePreTask sc, String itemDataJson)
			throws Exception {
		List<SamplePreTaskReagent> saveItems = new ArrayList<SamplePreTaskReagent>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SamplePreTaskReagent scp = new SamplePreTaskReagent();
			// 将map信息读入实体类
			scp = (SamplePreTaskReagent) samplePreTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			// 用量 = 单个用量*数量
			/*if (scp != null && scp.getOneNum() != null
					&& scp.getCount() != null) {
				scp.setNum(scp.getOneNum() * scp.getCount());
			}*/
			scp.setSamplePreTask(sc);
			saveItems.add(scp);
		}
		samplePreTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSamplePreTaskReagent(String[] ids) throws Exception {
		for (String id : ids) {
			SamplePreTaskReagent scp = samplePreTaskDao.get(SamplePreTaskReagent.class,
					id);
			samplePreTaskDao.delete(scp);
		}
	}

	// ================2015-12-02 ly=====================
	/**
	 * 根据ID删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSamplePreTaskReagentOne(String ids) throws Exception {
		SamplePreTaskReagent scp = samplePreTaskDao.get(SamplePreTaskReagent.class, ids);
		samplePreTaskDao.delete(scp);
	}

	// =====================================
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSamplePreTaskCos(SamplePreTask sc, String itemDataJson)
			throws Exception {
		List<SamplePreTaskCos> saveItems = new ArrayList<SamplePreTaskCos>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SamplePreTaskCos scp = new SamplePreTaskCos();
			// 将map信息读入实体类
			scp = (SamplePreTaskCos) samplePreTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSamplePreTask(sc);
			saveItems.add(scp);
		}
		samplePreTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSamplePreTaskCos(String[] ids) throws Exception {
		for (String id : ids) {
			SamplePreTaskCos scp = samplePreTaskDao.get(SamplePreTaskCos.class, id);
			samplePreTaskDao.delete(scp);
		}
	}

	// ============2015-12-02 ly====================
	/**
	 * 根据ID删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSamplePreTaskCosOne(String ids) throws Exception {
		SamplePreTaskCos scp = samplePreTaskDao.get(SamplePreTaskCos.class, ids);
		samplePreTaskDao.delete(scp);
	}

	// ================================
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SamplePreTask sc, Map jsonMap) throws Exception {
		if (sc != null) {
			samplePreTaskDao.saveOrUpdate(sc);
			if (sc.getTemplate() != null) {
				String id = sc.getTemplate().getId();
				templateService.setCosIsUsedSave(id);
			}
			String jsonStr = "";
			jsonStr = (String) jsonMap.get("samplePreTaskItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSamplePreTaskItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("samplePreTaskResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSamplePreTaskResult(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("samplePreTaskTemplate");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSamplePreTaskTemplate(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("samplePreTaskReagent");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSamplePreTaskReagent(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("samplePreTaskCos");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSamplePreTaskCos(sc, jsonStr);
			}
			List<SamplePreTaskItem> snlist=samplePreTaskDao.getItem(sc.getId());
			List<SamplePreTaskReagent> rglist=samplePreTaskDao.getReagent(sc.getId());
			DecimalFormat df = new DecimalFormat("#.00");
			if(snlist.size()>0){
				if(rglist.size()>0){
					for(SamplePreTaskReagent sr:rglist){
						sr.setSampleNum(Double.valueOf(snlist.size()));
						if(sr.getOneNum()!=null && 
								sr.getSampleNum()!=null){
							sr.setNum(df.format(String.valueOf(sr.getOneNum()*snlist.size())));
							System.out.println(String.valueOf(sr.getOneNum()*snlist.size())+"9999999999999999999999999999999999999999");
						}
					}
				}
			}
		}
	}

	// 审批完成
	public void chengeState(String applicationTypeActionId, String id) throws Exception {
		SamplePreTask sct = samplePreTaskDao.get(SamplePreTask.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		DateFormat format = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		sct.setConfirmDate(format.format(new Date()));
		samplePreTaskDao.update(sct);
		if (sct.getTemplate() != null) {
			String iid = sct.getTemplate().getId();
			if (iid != null) {
				templateService.setCosIsUsedOver(iid);
			}
		}
		// 将批次信息反馈到模板中
		List<SamplePreTaskReagent> list1 = samplePreTaskDao.setReagentList(sct
				.getId());
		for (SamplePreTaskReagent dt : list1) {
			String bat1 = dt.getBatch(); // 血浆分离实验中试剂的批次
			String drid = dt.gettReagent(); // 血浆分离实验中保存的试剂ID
			List<ReagentItem> list2 = templateDao.setReagentListById(drid);
			for (ReagentItem ri : list2) {
				if (bat1 != null) {
					ri.setBatch(bat1);
				}
			}
		}

		// 修改样本接收子表的样本状态
		List<SamplePreTaskResult> bsr1 = samplePreTaskDao.findSamplePreTaskResult(sct
				.getId());
		for (SamplePreTaskResult bsr : bsr1) {
			changeStateForTemp(bsr.getTempId());
		}
		submitSample(id,null);
	}

	/**
	 * 结果表被保存时 修改临时表的状态
	 * @param code
	 */
	public void changeStateForTemp(String code) {
		SamplePreTaskTemp SamplePreTaskTemp = this.commonDAO.get(
				SamplePreTaskTemp.class, code);
		if (SamplePreTaskTemp != null) {
			SamplePreTaskTemp.setState("2");
		}
	}

	/**
	 * 根据选中的步骤查询相关的试剂明细
	 * 
	 * @param id
	 * @param code
	 * @return
	 */
	public List<Map<String, String>> setReagent(String id, String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = samplePreTaskDao.setReagent(id, code);
		List<SamplePreTaskReagent> list = (List<SamplePreTaskReagent>) result
				.get("list");
		if (list != null && list.size() > 0) {
			for (SamplePreTaskReagent ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("code", ti.getCode());
				map.put("name", ti.getName());
				map.put("batch", ti.getBatch());
				map.put("isGood", ti.getIsGood());
				map.put("note", ti.getNote());
				map.put("tReagent", ti.gettReagent());

				if (ti.getOneNum() != null) {
					map.put("oneNum", ti.getOneNum().toString());
				} else {
					map.put("oneNum", "");
				}

				if (ti.getSampleNum() != null) {
					map.put("sampleNum", ti.getSampleNum().toString());
				} else {
					map.put("sampleNum", "");
				}

				if (ti.getNum() != null) {
					map.put("num", ti.getNum().toString());
				} else {
					map.put("num", "");
				}
				map.put("itemId", ti.getItemId());
				map.put("tId", ti.getSamplePreTask().getId());
				map.put("tName", ti.getSamplePreTask().getName());
				mapList.add(map);
			}

		}
		return mapList;
	}

	/**
	 * 根据选中的步骤查询相关的设备明细
	 * 
	 * @param id
	 * @param code
	 * @return
	 */
	public List<Map<String, String>> setCos(String id, String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = samplePreTaskDao.setCos(id, code);
		List<SamplePreTaskCos> list = (List<SamplePreTaskCos>) result.get("list");
		if (list != null && list.size() > 0) {
			for (SamplePreTaskCos ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("code", ti.getCode());
				map.put("name", ti.getName());
				map.put("isGood", ti.getIsGood());
				if (ti.getTemperature() != null) {
					map.put("temperature", ti.getTemperature().toString());
				} else {
					map.put("temperature", "");
				}

				if (ti.getSpeed() != null) {
					map.put("speed", ti.getSpeed().toString());
				} else {
					map.put("speed", "");
				}

				if (ti.getTime() != null) {
					map.put("time", ti.getTime().toString());
				} else {
					map.put("time", "");
				}

				map.put("itemId", ti.getItemId());
				map.put("note", ti.getNote());
				map.put("tCos", ti.gettCos());
				map.put("tId", ti.getSamplePreTask().getId());
				map.put("tName", ti.getSamplePreTask().getName());
				mapList.add(map);
			}

		}
		return mapList;
	}

	public List<SamplePreTaskItem> findPlasmaItemList(String scId)
			throws Exception {
		List<SamplePreTaskItem> list = samplePreTaskDao.selectPlasmaItemList(scId);
		return list;
	}
	
	// 提交样本
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitSample(String id, String[] ids) throws Exception {
		// 获取主表信息
		SamplePreTask sc = this.samplePreTaskDao.get(SamplePreTask.class, id);
		// 获取结果表样本信息

		List<SamplePreTaskResult> list;
		if (ids == null)
			list = this.samplePreTaskDao.setResultById(id);
		else
			list = this.samplePreTaskDao.setResultByIds(ids);
		for (SamplePreTaskResult scp : list) {
			if (scp != null) {
				if (scp.getResult() != null && scp.getSubmit() == null) {
					String isOk = scp.getResult();
					if (isOk.equals("1")) {
						FishProTaskTemp k = new FishProTaskTemp();
						k.setCode(scp.getCode());
						k.setSampleCode(scp.getSampleCode());
						k.setProductId(scp.getProductId());
						k.setProductName(scp.getProductName());
						k.setSampleType(scp.getSampleType());
//						k.setChipNum(scp.getChipNum());
						k.setState("1");
						samplePreTaskDao.saveOrUpdate(k);
						DateFormat format = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");
						sampleStateService
								.saveSampleState(
										scp.getCode(),
										scp.getSampleCode(),
										scp.getProductId(),
										scp.getProductName(),
										"",
										sc.getCreateDate(),
										format.format(new Date()),
										"SamplePreTask",
										"Fish样本预处理",
										(User) ServletActionContext
												.getRequest()
												.getSession()
												.getAttribute(
														SystemConstants.USER_SESSION_KEY),
										sc.getId(), "Fish制片",
										scp.getResult(), null, null, null,
										null, null, null, null, null);
						scp.setSubmit("1");
						samplePreTaskDao.saveOrUpdate(scp);
					} else {
						
						if(scp.getNextFlowId().equals("0038")){
							//提交不和合格的到报告终止
					    	EndReport cr = new EndReport();
							cr.setCode(scp.getCode());
							cr.setSampleCode(scp.getSampleCode());
							cr.setSampleType(scp.getSampleType());
							cr.setProductId(scp.getProductId());
							cr.setProductName(scp.getProductName());
							cr.setAdvice(scp.getNote());
							cr.setTaskType("Fish样本预处理");
							cr.setTaskId(scp.getId());
							cr.setTaskResultId(scp.getSamplePreTask().getId());
							cr.setWaitDate(new Date());
					    	commonDAO.saveOrUpdate(cr);
						}else{
								// 不合格的到异常
								FishAbnormal ka = new FishAbnormal();
								ka.setCode(scp.getCode());
								ka.setSampleCode(scp.getSampleCode());
								ka.setProductId(scp.getProductId());
								ka.setProductName(scp.getProductName());
								ka.setSampleType(scp.getSampleType());
								ka.setTaskId(scp.getSamplePreTask().getId());
								ka.setNote(scp.getNote());
								ka.setTaskName("Fish样本预处理");
								ka.setState("1");
								samplePreTaskDao.saveOrUpdate(ka);
						}
						scp.setSubmit("1");
					}
				}
			}
		}
	}
}



