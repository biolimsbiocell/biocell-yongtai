package com.biolims.experiment.fish.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.fish.dao.FishSjTaskDao;
import com.biolims.experiment.fish.fishabnormal.model.FishAbnormal;
import com.biolims.experiment.fish.fx.model.FishFxTaskTemp;
import com.biolims.experiment.fish.model.FishCrossTask;
import com.biolims.experiment.fish.model.FishSjTask;
import com.biolims.experiment.fish.model.FishSjTaskResult;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.report.model.EndReport;
import com.biolims.sample.service.SampleStateService;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class FishSjTaskService {
	@Resource
	private FishSjTaskDao fishSjTaskDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleStateService sampleStateService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findFishSjTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return fishSjTaskDao.selectFishSjTaskList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	public Map<String, Object> findFishSjTaskListByState(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return fishSjTaskDao.selectFishSjTaskListByState(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	/**
	 * 数据审核查看上机阅片结果
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 */
	public Map<String, Object> getFishSjTaskResultList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort,String code) {
		return fishSjTaskDao.gettFishSjTaskResultList(mapForQuery, startNum, limitNum, dir, sort,code);
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FishSjTask i) throws Exception {

		fishSjTaskDao.saveOrUpdate(i);

	}

	public FishSjTask get(String id) {
		FishSjTask fishSjTask = commonDAO.get(FishSjTask.class, id);
		return fishSjTask;
	}

	public Map<String, Object> findFishSjTaskResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = fishSjTaskDao.selectFishSjTaskResultList(
				scId, startNum, limitNum, dir, sort);
		List<FishSjTaskResult> list = (List<FishSjTaskResult>) result
				.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveFishSjTaskResult(FishSjTask sc, String itemDataJson)
			throws Exception {
		List<FishSjTaskResult> saveItems = new ArrayList<FishSjTaskResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			FishSjTaskResult scp = new FishSjTaskResult();
			// 将map信息读入实体类
			scp = (FishSjTaskResult) fishSjTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setFishSjTask(sc);

			saveItems.add(scp);
			// if (scp != null){
			// if (scp.getIsGood() != null && scp.getIsCommit() != null &&
			// !scp.getIsGood().equals("") && !scp.getIsCommit().equals("") ){
			// if(scp.getIsCommit().equals("1")){
			// if(scp.getIsGood().equals("0")){
			// FishAbnormal ka=new FishAbnormal();
			// ka.setCode(scp.getCode());
			// ka.setSampleCode(scp.getSampleCode());
			// ka.setProductId(scp.getProductId());
			// ka.setProductName(scp.getProductName());
			// ka.setSampleType(scp.getSampleType());
			// ka.setTaskId(scp.getFishSjTask().getId());
			// ka.setNote(scp.getNote());
			// ka.setTaskName("Fish上机阅片");
			// ka.setState("1");
			//
			// commonDAO.saveOrUpdate(ka);
			// }
			// }
			// }
			// }
		}
		fishSjTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delFishSjTaskResult(String[] ids) throws Exception {
		for (String id : ids) {
			FishSjTaskResult scp = fishSjTaskDao
					.get(FishSjTaskResult.class, id);
			fishSjTaskDao.delete(scp);
		}
	}

	/**
	 * 根据ID删除
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delFishSjTaskResultOne(String ids) throws Exception {
		FishSjTaskResult scp = fishSjTaskDao.get(FishSjTaskResult.class, ids);
		fishSjTaskDao.delete(scp);
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FishSjTask sc, Map jsonMap) throws Exception {
		if (sc != null) {
			fishSjTaskDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("fishSjTaskResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveFishSjTaskResult(sc, jsonStr);
			}
		}
	}

	/**
	 * 根据FishSjTask加载info子表
	 * 
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, String>> showFishSjTaskResultList(String code)
			throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = fishSjTaskDao
				.setFishSjTaskResultList(code);
		List<FishSjTaskResult> list = (List<FishSjTaskResult>) result
				.get("list");

		if (list != null && list.size() > 0) {
			for (FishSjTaskResult pt : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", pt.getId());
				map.put("code", pt.getCode());
				map.put("sampleCode", pt.getSampleCode());
				map.put("slideCode", pt.getSlideCode());
				map.put("productId", pt.getProductId());
				map.put("productName", pt.getProductName());
				map.put("note", pt.getNote());
				mapList.add(map);
			}
		}
		return mapList;
	}

	/**
	 * 审批完成
	 * 
	 * @param applicationTypeActionId
	 * @param id
	 * @throws Exception
	 */
	public void chengeState(String applicationTypeActionId, String id)
			throws Exception {
		FishSjTask sct = commonDAO.get(FishSjTask.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);

		fishSjTaskDao.update(sct);

		
//		if (sct.getFishCrossTask() != null) {
//			FishCrossTask fct = commonDAO.get(FishCrossTask.class, sct
//					.getFishCrossTask().getId());
//			if (fct != null) {
//				fct.setState("2");
//			}
//		}
		// 完成后改变杂交洗脱实验的状态
		String ids =sct.getFishCrossTaskId();
		String [] ids1= ids.split(",");
		for (int i = 0; i < ids1.length; i++) {
			FishCrossTask fct = commonDAO.get(FishCrossTask.class, ids1[i]);
			if (fct != null) {
			fct.setState("2");
		      }
		}
		submitSample(id, null);
	}

	// 提交样本
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitSample(String id, String[] ids) throws Exception {
		// 获取主表信息
		FishSjTask sc = this.fishSjTaskDao.get(FishSjTask.class, id);
		// 获取结果表样本信息

		List<FishSjTaskResult> list;
		if (ids == null)
			list = this.fishSjTaskDao.setResultById(id);
		else
			list = this.fishSjTaskDao.setResultByIds(ids);
		for (FishSjTaskResult scp : list) {
			if (scp != null) {
				if (scp.getIsGood() != null && scp.getIsCommit() == null) {
					String isOk = scp.getIsGood();
					if (isOk.equals("1")) {
						if(scp.getNextFlowId().equals("0051")){
							FishFxTaskTemp k = new FishFxTaskTemp();
							k.setCode(scp.getCode());
							k.setSampleCode(scp.getSampleCode());
							k.setProductId(scp.getProductId());
							k.setProductName(scp.getProductName());
							k.setSampleType(scp.getSampleType());
							k.setSlideCode(scp.getSlideCode());
							k.setCellNum(scp.getCellsNum());
							k.setState("1");
							fishSjTaskDao.saveOrUpdate(k);
							DateFormat format = new SimpleDateFormat(
									"yyyy-MM-dd HH:mm:ss");
							sampleStateService
									.saveSampleState(
											scp.getCode(),
											scp.getSampleCode(),
											scp.getProductId(),
											scp.getProductName(),
											"",
											DateUtil.format(sc.getCreateDate()),
											format.format(new Date()),
											"FishSjTask",
											"Fish检测上机阅片",
											(User) ServletActionContext
													.getRequest()
													.getSession()
													.getAttribute(
															SystemConstants.USER_SESSION_KEY),
											sc.getId(), "核型分析", scp.getIsGood(),
											null, null, null, null, null, null,
											null, null);
							fishSjTaskDao.saveOrUpdate(scp);
						}
						scp.setIsCommit("1");
					} else {
						
						if(scp.getNextFlowId().equals("0038")){
							//提交不和合格的到报告终止
					    	EndReport cr = new EndReport();
							cr.setCode(scp.getCode());
							cr.setSampleCode(scp.getSampleCode());
							cr.setSampleType(scp.getSampleType());
							cr.setProductId(scp.getProductId());
							cr.setProductName(scp.getProductName());
							cr.setAdvice(scp.getNote());
							cr.setTaskType("Fish上机阅片");
							cr.setTaskId(scp.getId());
							cr.setTaskResultId(scp.getFishSjTask().getId());
							cr.setWaitDate(new Date());
					    	commonDAO.saveOrUpdate(cr);
						}else{
								// 不合格的到异常
								FishAbnormal ka = new FishAbnormal();
								ka.setCode(scp.getCode());
								ka.setSampleCode(scp.getSampleCode());
								ka.setProductId(scp.getProductId());
								ka.setProductName(scp.getProductName());
								ka.setSampleType(scp.getSampleType());
								ka.setTaskId(sc.getId());
								ka.setNote(scp.getNote());
								ka.setTaskName("Fish上机阅片");
								ka.setState("1");
								commonDAO.saveOrUpdate(ka);
						}
						scp.setIsCommit("1");
					}
				}
			}
		}
	}
}
