package com.biolims.experiment.fish.service;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.fish.dao.FishProTaskDao;
import com.biolims.experiment.fish.fishabnormal.model.FishAbnormal;
import com.biolims.experiment.fish.model.FishCrossTaskTemp;
import com.biolims.experiment.fish.model.FishProTask;
import com.biolims.experiment.fish.model.FishProTaskCos;
import com.biolims.experiment.fish.model.FishProTaskItem;
import com.biolims.experiment.fish.model.FishProTaskReagent;
import com.biolims.experiment.fish.model.FishProTaskResult;
import com.biolims.experiment.fish.model.FishProTaskTemp;
import com.biolims.experiment.fish.model.FishProTaskTemplate;
import com.biolims.experiment.fish.model.SamplePreTaskItem;
import com.biolims.experiment.fish.model.SamplePreTaskReagent;
import com.biolims.experiment.fish.model.SamplePreTaskTemp;
import com.biolims.experiment.karyoget.model.KaryoGetTaskTemp;
import com.biolims.experiment.snpjc.abnormal.model.SnpAbnormal;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.report.model.EndReport;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.storage.container.model.StorageContainer;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.service.TemplateService;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class FishProTaskService {
	@Resource
	private FishProTaskDao fishProTaskDao;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private TemplateService templateService;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findFishProTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return fishProTaskDao.selectFishProTaskList(mapForQuery, startNum,
				limitNum, dir, sort);
	}
	/**
	 * 获取样本接收的样本
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectFishProTaskTempList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort)
			throws Exception {
		/*Map<String, Object> result = fishProTaskDao
				.selectFishProTaskTempList(mapForQuery, startNum,
						limitNum, dir, sort);
		List<FishProTaskTemp> list = (List<FishProTaskTemp>) result.get("list");
		return result;*/
		Map<String, Object> result = fishProTaskDao
				.selectFishProTaskTempList(mapForQuery,
				startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<FishProTaskTemp> list = (List<FishProTaskTemp>) result.get("list");
		
		Map<String, Object> result2 = new HashMap<String, Object>();
		List<FishProTaskTemp> list2=new ArrayList<FishProTaskTemp>();
		for(FishProTaskTemp t:list){
			SampleInfo s=this.sampleInfoMainDao.findSampleInfo(t.getSampleCode());
			if(s!=null && s.getSampleOrder()!=null){
				t.setChargeNote(s.getSampleOrder().getChargeNote());
				t.setPatientName(s.getSampleOrder().getName());
				if(s.getSampleOrder().getCrmDoctor()!=null){
					t.setDoctor(s.getSampleOrder().getCrmDoctor().getName());
				}else{
					t.setDoctor("");
				}
				if(s.getSampleOrder().getCrmCustomer()!=null){
					t.setHospital(s.getSampleOrder().getCrmCustomer().getName());
				}else{
					t.setHospital("");
				}
				t.setGender(s.getSampleOrder().getGender());
				t.setReason(s.getSampleOrder().getSubmitReasonName());
			}
			list2.add(t);
		}
		result2.put("total", count);
		result2.put("list", list2);
		return result2;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FishProTask i) throws Exception {

		fishProTaskDao.saveOrUpdate(i);

	}

	public FishProTask get(String id) {
		FishProTask FishProTask = commonDAO.get(FishProTask.class, id);
		return FishProTask;
	}

	public Map<String, Object> findFishProTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = fishProTaskDao.selectFishProTaskItemList(
				scId, startNum, limitNum, dir, sort);
		List<FishProTaskItem> list = (List<FishProTaskItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findFishProTaskResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = fishProTaskDao.selectFishProTaskResultList(
				scId, startNum, limitNum, dir, sort);
		List<FishProTaskResult> list = (List<FishProTaskResult>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findFishProTaskTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = fishProTaskDao
				.selectFishProTaskTemplateList(scId, startNum, limitNum, dir,
						sort);
		List<FishProTaskTemplate> list = (List<FishProTaskTemplate>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findFishProTaskReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = fishProTaskDao.selectFishProTaskReagentList(
				scId, startNum, limitNum, dir, sort);
		List<FishProTaskReagent> list = (List<FishProTaskReagent>) result
				.get("list");
		return result;
	}

	// 根据步骤加载试剂明细
	public Map<String, Object> findFishProTaskReagentListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		Map<String, Object> result = fishProTaskDao
				.selectFishProTaskReagentListByItemId(scId, startNum, limitNum,
						dir, sort, itemId);
		List<FishProTaskReagent> list = (List<FishProTaskReagent>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findFishProTaskCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = fishProTaskDao.selectFishProTaskCosList(
				scId, startNum, limitNum, dir, sort);
		List<FishProTaskCos> list = (List<FishProTaskCos>) result.get("list");
		return result;
	}

	public Map<String, Object> findCosItemListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		Map<String, Object> result = fishProTaskDao
				.selectFishProTaskCosListByItemId(scId, startNum, limitNum, dir,
						sort, itemId);
		List<FishProTaskCos> list = (List<FishProTaskCos>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveFishProTaskItem(FishProTask sc, String itemDataJson)
			throws Exception {
		List<FishProTaskItem> saveItems = new ArrayList<FishProTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		Template temp = null;
		StorageContainer storageContainer = null;
		Integer rowCode = null;
		Integer colCode = null;
		Integer maxNum = null;
		if (sc.getTemplate() != null && !sc.getTemplate().equals("")) {
			temp = templateDao.get(Template.class, sc.getTemplate().getId());
			if (temp.getStorageContainer() != null
					&& !temp.getStorageContainer().equals("")) {
				storageContainer = fishProTaskDao.get(StorageContainer.class,
						temp.getStorageContainer().getId());
				rowCode = storageContainer.getRowNum();
				colCode = storageContainer.getColNum();

			}
		}

		for (Map<String, Object> map : list) {
			FishProTaskItem scp = new FishProTaskItem();
			// 将map信息读入实体类
			scp = (FishProTaskItem) fishProTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			if (scp.getOrderNumber() != null
					&& !scp.getOrderNumber().equals("")) {
				if (rowCode != null && colCode != null) {
					Double num;
					if ((scp.getOrderNumber() - 1) / (rowCode * colCode) > 0) {
						int count = scp.getOrderNumber() / (rowCode * colCode);
						num = Math.floor((scp.getOrderNumber() - count
								* rowCode * colCode - 1)
								/ rowCode) + 1;
					} else {
						num = Math.floor((scp.getOrderNumber() - 1) / rowCode) + 1;
					}
					scp.setColCode(Integer.toString(num.intValue()));
					if (scp.getOrderNumber() % rowCode == 0) {
						scp.setRowCode(String.valueOf((char) (64 + rowCode)));
					} else {
						scp.setRowCode(String.valueOf((char) (64 + scp
								.getOrderNumber() % rowCode)));
					}
					scp.setCounts(String.valueOf((scp.getOrderNumber() - 1)
							/ (rowCode * colCode) + 1));
				}
			}
			// 改变左侧样本状态
			FishProTaskTemp dt = commonDAO.get(FishProTaskTemp.class, scp.getTempId());
			if (dt != null) {
				dt.setState("2");
			}
			commonDAO.update(dt);
			scp.setFishProTask(sc);
			// if (temp != null && !temp.equals(""))
			// scp.setSampleNum(temp.getSampleNum());
			saveItems.add(scp);
		}
		fishProTaskDao.saveOrUpdateAll(saveItems);
		if (sc.getTemplate() != null && !sc.getTemplate().equals("")) {
			temp = templateDao.get(Template.class, sc.getTemplate().getId());
			if (temp.getStorageContainer() != null
					&& !temp.getStorageContainer().equals("")) {
				maxNum = Integer.valueOf(fishProTaskDao.selectCountMax(
						sc.getId()).toString());
				sc.setMaxNum((maxNum - 1) / (rowCode * colCode) + 1);
			}
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delFishProTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			FishProTaskItem scp = fishProTaskDao.get(FishProTaskItem.class, id);
			if (scp.getId() != null) {
				fishProTaskDao.delete(scp);
			}
			// 改变左侧样本状态
			FishProTaskTemp dt = commonDAO.get(FishProTaskTemp.class, scp.getTempId());
			if (dt != null) {
				dt.setState("1");
			}
			commonDAO.update(dt);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveFishProTaskResult(FishProTask sc, String itemDataJson)
			throws Exception {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		List<FishProTaskResult> saveItems = new ArrayList<FishProTaskResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			FishProTaskResult scp = new FishProTaskResult();
			// 将map信息读入实体类
			scp = (FishProTaskResult) fishProTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setFishProTask(sc);
			/*if (scp.getCode() == null || scp.getCode().equals("")) {
				String markCode = "";
				if (scp.getDicSampleType() != null
						|| !scp.getDicSampleType().equals("")) {
					DicSampleType d = commonDAO.get(DicSampleType.class, scp
							.getDicSampleType().getId());
					if(d!=null){
						markCode = scp.getSampleCode() + d.getCode();
					}else{
						markCode = scp.getSampleCode()+"FP";
					}
				}
				String code = codingRuleService.getCode("FishProTaskResult",
						markCode, 00, 2, null);
				scp.setCode(code);

			}*/
			saveItems.add(scp);
//			SampleInfo sf = sampleInfoMainDao.findSampleInfo(scp.getCode());
//			if (scp != null){
//				if (scp.getResult() != null && scp.getSubmit() != null &&
//						!scp.getResult().equals("") && !scp.getSubmit().equals("") ){
//				if(scp.getSubmit().equals("1")){
//					if(scp.getResult().equals("1")){
//					if(scp.getNextFlowId()!=null && !scp.getNextFlowId().equals("")){
//						String next=scp.getNextFlowId();
//						if (next.equals("0009")) {// 样本入库
//							SampleInItemTemp st = new SampleInItemTemp();
//							st.setCode(scp.getCode());
//							st.setSampleCode(scp.getSampleCode());
//							st.setNum(scp.getSampleNum());
//							st.setState("1");
//							fishProTaskDao.saveOrUpdate(st);
//							// 入库，改变SampleInfo中原始样本的状态为“待入库”
//							if (sf != null) {
//								sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW);
//								sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_IN_NEW_NAME);
//							}
//						} else if (next.equals("0012")) {// 暂停
//							// 暂停，改变SampleInfo中原始样本的状态为“实验暂停”
//							if (sf != null) {
//								sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE);
//								sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_PAUSE_NAME);
//							}
//						} else if (next.equals("0013")) {// 终止
//							// 终止，改变SampleInfo中原始样本的状态为“实验终止”
//							if (sf != null) {
//								sf.setState(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP);
//								sf.setStateName(com.biolims.workflow.WorkflowConstants.EXPERIMENT_STOP_NAME);
//							}
//						} else {
//							// 得到下一步流向的相关表单
//							List<NextFlow> list_nextFlow = nextFlowDao
//									.seletNextFlowById(next);
//							for (NextFlow n : list_nextFlow) {
//								Object o = Class.forName(
//										n.getApplicationTypeTable().getClassPath())
//										.newInstance();
//								scp.setState("1");
//								sampleInputService.copy(o, scp);
//							}
//						}
//					}
//				}else{
//					FishAbnormal ka=new FishAbnormal();
//					ka.setCode(scp.getCode());
//					ka.setSampleCode(scp.getSampleCode());
//					ka.setProductId(scp.getProductId());
//					ka.setProductName(scp.getProductName());
//					ka.setOrderId(scp.getOrderId());
//					ka.setAcceptDate(scp.getAcceptDate());
//					ka.setReportDate(scp.getReportDate());
//					ka.setSampleType(scp.getSampleType());
//					ka.setTaskId(scp.getFishProTask().getId());
//					ka.setNote(scp.getNote());
//					ka.setTaskName("Fish制片");
//					ka.setState("1");
//					
//					commonDAO.saveOrUpdate(ka);
//				}
//				sampleStateService
//						.saveSampleState(
//								scp.getCode(),
//								scp.getSampleCode(),
//								scp.getProductId(),
//								scp.getProductName(),
//								"",
//								sc.getCreateDate(),
//								format.format(new Date()),
//								"FishProTask",
//								"Fish制片",
//								(User) ServletActionContext
//										.getRequest()
//										.getSession()
//										.getAttribute(
//												SystemConstants.USER_SESSION_KEY),
//								sc.getId(), scp.getNextFlow(), scp.getResult(),
//								null, null, null, null, null, null, null, null);
//					}
//				}
//			}
			fishProTaskDao.saveOrUpdateAll(saveItems);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delFishProTaskResult(String[] ids) throws Exception {
		for (String id : ids) {
			FishProTaskResult scp = fishProTaskDao
					.get(FishProTaskResult.class, id);
			fishProTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveFishProTaskTemplate(FishProTask sc, String itemDataJson)
			throws Exception {
		List<FishProTaskTemplate> saveItems = new ArrayList<FishProTaskTemplate>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			FishProTaskTemplate scp = new FishProTaskTemplate();
			// 将map信息读入实体类
			scp = (FishProTaskTemplate) fishProTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setFishProTask(sc);
			saveItems.add(scp);
		}
		fishProTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delFishProTaskTemplate(String[] ids) throws Exception {
		for (String id : ids) {
			FishProTaskTemplate scp = fishProTaskDao.get(
					FishProTaskTemplate.class, id);
			fishProTaskDao.delete(scp);
		}
	}

	// ============2015-12-02 ly================
	/**
	 * 根据ID删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delFishProTaskTemplateOne(String ids) throws Exception {
		FishProTaskTemplate scp = fishProTaskDao.get(FishProTaskTemplate.class,
				ids);
		fishProTaskDao.delete(scp);
	}

	// ============================
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveFishProTaskReagent(FishProTask sc, String itemDataJson)
			throws Exception {
		List<FishProTaskReagent> saveItems = new ArrayList<FishProTaskReagent>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			FishProTaskReagent scp = new FishProTaskReagent();
			// 将map信息读入实体类
			scp = (FishProTaskReagent) fishProTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			// 用量 = 单个用量*数量
			if (scp != null && scp.getOneNum() != null
					&& scp.getCount() != null) {
				scp.setNum(scp.getOneNum() * scp.getCount());
			}
			scp.setFishProTask(sc);
			saveItems.add(scp);
		}
		fishProTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delFishProTaskReagent(String[] ids) throws Exception {
		for (String id : ids) {
			FishProTaskReagent scp = fishProTaskDao.get(FishProTaskReagent.class,
					id);
			fishProTaskDao.delete(scp);
		}
	}

	/**
	 * 根据ID删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delFishProTaskReagentOne(String ids) throws Exception {
		FishProTaskReagent scp = fishProTaskDao.get(FishProTaskReagent.class, ids);
		fishProTaskDao.delete(scp);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveFishProTaskCos(FishProTask sc, String itemDataJson)
			throws Exception {
		List<FishProTaskCos> saveItems = new ArrayList<FishProTaskCos>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			FishProTaskCos scp = new FishProTaskCos();
			// 将map信息读入实体类
			scp = (FishProTaskCos) fishProTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setFishProTask(sc);
			saveItems.add(scp);
		}
		fishProTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delFishProTaskCos(String[] ids) throws Exception {
		for (String id : ids) {
			FishProTaskCos scp = fishProTaskDao.get(FishProTaskCos.class, id);
			fishProTaskDao.delete(scp);
		}
	}

	/**
	 * 根据ID删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delFishProTaskCosOne(String ids) throws Exception {
		FishProTaskCos scp = fishProTaskDao.get(FishProTaskCos.class, ids);
		fishProTaskDao.delete(scp);
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FishProTask sc, Map jsonMap) throws Exception {
		if (sc != null) {
			fishProTaskDao.saveOrUpdate(sc);
			if (sc.getTemplate() != null) {
				String id = sc.getTemplate().getId();
				templateService.setCosIsUsedSave(id);
			}
			String jsonStr = "";
			jsonStr = (String) jsonMap.get("fishProTaskItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveFishProTaskItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("fishProTaskResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveFishProTaskResult(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("fishProTaskTemplate");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveFishProTaskTemplate(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("fishProTaskReagent");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveFishProTaskReagent(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("fishProTaskCos");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveFishProTaskCos(sc, jsonStr);
			}
			
			List<FishProTaskItem> snlist=fishProTaskDao.getItem(sc.getId());
			List<FishProTaskReagent> rglist=fishProTaskDao.getReagent(sc.getId());
			DecimalFormat df = new DecimalFormat("#.00");
			Double nums = 0.00;
			if(snlist.size()>0){
				if(rglist.size()>0){
					for(FishProTaskReagent sr:rglist){
						sr.setSampleNum(Double.valueOf(snlist.size()));
						if(sr.getOneNum()!=null && 
								sr.getSampleNum()!=null){
							nums =Double.valueOf(df.format(sr.getOneNum()*snlist.size()));
							sr.setNum(nums);
						}
					}
				}
			}
		}
	}

	// 审批完成
	public void chengeState(String applicationTypeActionId, String id) throws Exception {
		FishProTask sct = fishProTaskDao.get(FishProTask.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);

		fishProTaskDao.update(sct);
		if (sct.getTemplate() != null) {
			String iid = sct.getTemplate().getId();
			if (iid != null) {
				templateService.setCosIsUsedOver(iid);
			}
		}
		// 将批次信息反馈到模板中
		List<FishProTaskReagent> list1 = fishProTaskDao.setReagentList(sct
				.getId());
		for (FishProTaskReagent dt : list1) {
			String bat1 = dt.getBatch(); // 血浆分离实验中试剂的批次
			String drid = dt.gettReagent(); // 血浆分离实验中保存的试剂ID
			List<ReagentItem> list2 = templateDao.setReagentListById(drid);
			for (ReagentItem ri : list2) {
				if (bat1 != null) {
					ri.setBatch(bat1);
				}
			}
		}

		// 修改样本接收子表的样本状态
		List<FishProTaskResult> bsr1 = fishProTaskDao.findFishProTaskResult(sct
				.getId());
		for (FishProTaskResult bsr : bsr1) {
			changeStateForTemp(bsr.getTempId());
		}
		submitSample(id,null);
	}

	/**
	 * 结果表被保存时 修改临时表的状态
	 * @param code
	 */
	public void changeStateForTemp(String code) {
		FishProTaskTemp FishProTaskTemp = this.commonDAO.get(
				FishProTaskTemp.class, code);
		if (FishProTaskTemp != null) {
			FishProTaskTemp.setState("2");
		}
	}

	/**
	 * 根据选中的步骤查询相关的试剂明细
	 * 
	 * @param id
	 * @param code
	 * @return
	 */
	public List<Map<String, String>> setReagent(String id, String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = fishProTaskDao.setReagent(id, code);
		List<FishProTaskReagent> list = (List<FishProTaskReagent>) result
				.get("list");
		if (list != null && list.size() > 0) {
			for (FishProTaskReagent ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("code", ti.getCode());
				map.put("name", ti.getName());
				map.put("batch", ti.getBatch());
				map.put("isGood", ti.getIsGood());
				map.put("note", ti.getNote());
				map.put("tReagent", ti.gettReagent());

				if (ti.getOneNum() != null) {
					map.put("oneNum", ti.getOneNum().toString());
				} else {
					map.put("oneNum", "");
				}

				if (ti.getSampleNum() != null) {
					map.put("sampleNum", ti.getSampleNum().toString());
				} else {
					map.put("sampleNum", "");
				}

				if (ti.getNum() != null) {
					map.put("num", ti.getNum().toString());
				} else {
					map.put("num", "");
				}
				map.put("itemId", ti.getItemId());
				map.put("tId", ti.getFishProTask().getId());
				map.put("tName", ti.getFishProTask().getName());
				mapList.add(map);
			}

		}
		return mapList;
	}

	/**
	 * 根据选中的步骤查询相关的设备明细
	 * 
	 * @param id
	 * @param code
	 * @return
	 */
	public List<Map<String, String>> setCos(String id, String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = fishProTaskDao.setCos(id, code);
		List<FishProTaskCos> list = (List<FishProTaskCos>) result.get("list");
		if (list != null && list.size() > 0) {
			for (FishProTaskCos ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("code", ti.getCode());
				map.put("name", ti.getName());
				map.put("isGood", ti.getIsGood());
				if (ti.getTemperature() != null) {
					map.put("temperature", ti.getTemperature().toString());
				} else {
					map.put("temperature", "");
				}

				if (ti.getSpeed() != null) {
					map.put("speed", ti.getSpeed().toString());
				} else {
					map.put("speed", "");
				}

				if (ti.getTime() != null) {
					map.put("time", ti.getTime().toString());
				} else {
					map.put("time", "");
				}

				map.put("itemId", ti.getItemId());
				map.put("note", ti.getNote());
				map.put("tCos", ti.gettCos());
				map.put("tId", ti.getFishProTask().getId());
				map.put("tName", ti.getFishProTask().getName());
				mapList.add(map);
			}

		}
		return mapList;
	}


	//提交样本
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitSample(String id, String[] ids) throws Exception {
		// 获取主表信息
		FishProTask sc = this.fishProTaskDao.get(FishProTask.class, id);
		// 获取结果表样本信息

		List<FishProTaskResult> list;
		if (ids == null)
			list = this.fishProTaskDao.setResultById(id);
		else
			list = this.fishProTaskDao.setResultByIds(ids);
		for (FishProTaskResult scp : list) {
			if (scp != null) {
				if (scp.getResult() != null && scp.getSubmit() == null) {
					String isOk = scp.getResult();
					if (isOk.equals("1")) {
						FishCrossTaskTemp k = new FishCrossTaskTemp();
						k.setCode(scp.getCode());
						k.setSampleCode(scp.getSampleCode());
						k.setProductId(scp.getProductId());
						k.setProductName(scp.getProductName());
						k.setSampleType(scp.getSampleType());
//						k.setChipNum(scp.getChipNum());
						k.setState("1");
						fishProTaskDao.saveOrUpdate(k);
						DateFormat format = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");
						sampleStateService
								.saveSampleState(
										scp.getCode(),
										scp.getSampleCode(),
										scp.getProductId(),
										scp.getProductName(),
										"",
//										DateUtil.format(sc.getCreateDate()),
										sc.getCreateDate(),
										format.format(new Date()),
										"SamplePreTask",
										"样本预处理",
										(User) ServletActionContext
												.getRequest()
												.getSession()
												.getAttribute(
														SystemConstants.USER_SESSION_KEY),
										sc.getId(), "Fish制片",
										scp.getResult(), null, null, null,
										null, null, null, null, null);
						scp.setSubmit("1");
						fishProTaskDao.saveOrUpdate(scp);
					} else {
						if(scp.getNextFlowId().equals("0038")){
							//提交不和合格的到报告终止
					    	EndReport cr = new EndReport();
							cr.setCode(scp.getCode());
							cr.setSampleCode(scp.getSampleCode());
							cr.setSampleType(scp.getSampleType());
							cr.setProductId(scp.getProductId());
							cr.setProductName(scp.getProductName());
							cr.setAdvice(scp.getNote());
							cr.setTaskType("Fish制片");
							cr.setTaskId(scp.getId());
							cr.setTaskResultId(scp.getFishProTask().getId());
							cr.setWaitDate(new Date());
					    	commonDAO.saveOrUpdate(cr);
						}else{
							// 不合格的到异常
							FishAbnormal ka = new FishAbnormal();
							ka.setCode(scp.getCode());
							ka.setSampleCode(scp.getSampleCode());
							ka.setProductId(scp.getProductId());
							ka.setProductName(scp.getProductName());
							ka.setSampleType(scp.getSampleType());
							ka.setTaskId(scp.getFishProTask().getId());
							ka.setNote(scp.getNote());
							ka.setTaskName("Fish制片");
							ka.setState("1");
							commonDAO.saveOrUpdate(ka);
						} 
						scp.setSubmit("1");
					}
				}
			}
		}
	}
}




