package com.biolims.experiment.fish.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.fish.dao.FishCrossTaskDao;
import com.biolims.experiment.fish.fishabnormal.model.FishAbnormal;
import com.biolims.experiment.fish.model.FishCrossTask;
import com.biolims.experiment.fish.model.FishCrossTaskCos;
import com.biolims.experiment.fish.model.FishCrossTaskItem;
import com.biolims.experiment.fish.model.FishCrossTaskReagent;
import com.biolims.experiment.fish.model.FishCrossTaskResult;
import com.biolims.experiment.fish.model.FishCrossTaskTemp;
import com.biolims.experiment.fish.model.FishCrossTaskTemplate;
import com.biolims.experiment.fish.model.FishProTask;
import com.biolims.experiment.fish.model.FishProTaskItem;
import com.biolims.experiment.fish.model.FishProTaskReagent;
import com.biolims.experiment.fish.model.FishProTaskResult;
import com.biolims.experiment.fish.model.SamplePreTaskTemp;
import com.biolims.experiment.karyoget.model.KaryoGetTaskTemp;
import com.biolims.experiment.snpjc.abnormal.model.SnpAbnormal;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.report.model.EndReport;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.storage.container.model.StorageContainer;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.service.TemplateService;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class FishCrossTaskService {
	@Resource
	private FishCrossTaskDao fishCrossTaskDao;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private TemplateService templateService;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findFishCrossTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return fishCrossTaskDao.selectFishCrossTaskList(mapForQuery, startNum,
				limitNum, dir, sort);
	}
	public Map<String, Object> findFishCrossTaskListByState(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return fishCrossTaskDao.selectFishCrossTaskListByState(mapForQuery, startNum,
				limitNum, dir, sort);
	}
	/**
	 * 获取样本接收的样本
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectFishCrossTaskTempList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = fishCrossTaskDao
				.selectFishCrossTaskTempList(mapForQuery, startNum,
						limitNum, dir, sort);
		List<FishCrossTaskTemp> list = (List<FishCrossTaskTemp>) result.get("list");
		Long count = (Long) result.get("total");
		Map<String, Object> result2 = new HashMap<String, Object>();
		List<FishCrossTaskTemp> list2=new ArrayList<FishCrossTaskTemp>();
		for(FishCrossTaskTemp t:list){
			SampleInfo s=this.sampleInfoMainDao.findSampleInfo(t.getSampleCode());
			if(s!=null && s.getSampleOrder()!=null){
				t.setChargeNote(s.getSampleOrder().getChargeNote());
				t.setPatientName(s.getSampleOrder().getName());
				if(s.getSampleOrder().getCrmDoctor()!=null){
					t.setDoctor(s.getSampleOrder().getCrmDoctor().getName());
				}else{
					t.setDoctor("");
				}
				if(s.getSampleOrder().getCrmCustomer()!=null){
					t.setHospital(s.getSampleOrder().getCrmCustomer().getName());
				}else{
					t.setHospital("");
				}
				t.setGender(s.getSampleOrder().getGender());
				t.setReason(s.getSampleOrder().getSubmitReasonName());
			}
			list2.add(t);
		}
		result2.put("total", count);
		result2.put("list", list2);
		return result2;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FishCrossTask i) throws Exception {

		fishCrossTaskDao.saveOrUpdate(i);

	}

	public FishCrossTask get(String id) {
		FishCrossTask FishCrossTask = commonDAO.get(FishCrossTask.class, id);
		return FishCrossTask;
	}

	public Map<String, Object> findFishCrossTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = fishCrossTaskDao.selectFishCrossTaskItemList(
				scId, startNum, limitNum, dir, sort);
		List<FishCrossTaskItem> list = (List<FishCrossTaskItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findFishCrossTaskResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = fishCrossTaskDao.selectFishCrossTaskResultList(
				scId, startNum, limitNum, dir, sort);
		List<FishCrossTaskResult> list = (List<FishCrossTaskResult>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findFishCrossTaskTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = fishCrossTaskDao
				.selectFishCrossTaskTemplateList(scId, startNum, limitNum, dir,
						sort);
		List<FishCrossTaskTemplate> list = (List<FishCrossTaskTemplate>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findFishCrossTaskReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = fishCrossTaskDao.selectFishCrossTaskReagentList(
				scId, startNum, limitNum, dir, sort);
		List<FishCrossTaskReagent> list = (List<FishCrossTaskReagent>) result
				.get("list");
		return result;
	}

	// 根据步骤加载试剂明细
	public Map<String, Object> findFishCrossTaskReagentListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		Map<String, Object> result = fishCrossTaskDao
				.selectFishCrossTaskReagentListByItemId(scId, startNum, limitNum,
						dir, sort, itemId);
		List<FishCrossTaskReagent> list = (List<FishCrossTaskReagent>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findFishCrossTaskCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = fishCrossTaskDao.selectFishCrossTaskCosList(
				scId, startNum, limitNum, dir, sort);
		List<FishCrossTaskCos> list = (List<FishCrossTaskCos>) result.get("list");
		return result;
	}

	public Map<String, Object> findCosItemListByItemId(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String itemId) throws Exception {
		Map<String, Object> result = fishCrossTaskDao
				.selectFishCrossTaskCosListByItemId(scId, startNum, limitNum, dir,
						sort, itemId);
		List<FishCrossTaskCos> list = (List<FishCrossTaskCos>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveFishCrossTaskItem(FishCrossTask sc, String itemDataJson)
			throws Exception {
		List<FishCrossTaskItem> saveItems = new ArrayList<FishCrossTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		Template temp = null;
		StorageContainer storageContainer = null;
		Integer rowCode = null;
		Integer colCode = null;
		Integer maxNum = null;
		if (sc.getTemplate() != null && !sc.getTemplate().equals("")) {
			temp = templateDao.get(Template.class, sc.getTemplate().getId());
			if (temp.getStorageContainer() != null
					&& !temp.getStorageContainer().equals("")) {
				storageContainer = fishCrossTaskDao.get(StorageContainer.class,
						temp.getStorageContainer().getId());
				rowCode = storageContainer.getRowNum();
				colCode = storageContainer.getColNum();

			}
		}

		for (Map<String, Object> map : list) {
			FishCrossTaskItem scp = new FishCrossTaskItem();
			// 将map信息读入实体类
			scp = (FishCrossTaskItem) fishCrossTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			if (scp.getOrderNumber() != null
					&& !scp.getOrderNumber().equals("")) {
				if (rowCode != null && colCode != null) {
					Double num;
					if ((scp.getOrderNumber() - 1) / (rowCode * colCode) > 0) {
						int count = scp.getOrderNumber() / (rowCode * colCode);
						num = Math.floor((scp.getOrderNumber() - count
								* rowCode * colCode - 1)
								/ rowCode) + 1;
					} else {
						num = Math.floor((scp.getOrderNumber() - 1) / rowCode) + 1;
					}
					scp.setColCode(Integer.toString(num.intValue()));
					if (scp.getOrderNumber() % rowCode == 0) {
						scp.setRowCode(String.valueOf((char) (64 + rowCode)));
					} else {
						scp.setRowCode(String.valueOf((char) (64 + scp
								.getOrderNumber() % rowCode)));
					}
					scp.setCounts(String.valueOf((scp.getOrderNumber() - 1)
							/ (rowCode * colCode) + 1));
				}
			}
			// 改变左侧样本状态
			FishCrossTaskTemp dt = commonDAO.get(FishCrossTaskTemp.class, scp.getTempId());
			if (dt != null) {
				dt.setState("2");
			}
			commonDAO.update(dt);
			scp.setFishCrossTask(sc);
			// if (temp != null && !temp.equals(""))
			// scp.setSampleNum(temp.getSampleNum());
			saveItems.add(scp);
		}
		fishCrossTaskDao.saveOrUpdateAll(saveItems);
		if (sc.getTemplate() != null && !sc.getTemplate().equals("")) {
			temp = templateDao.get(Template.class, sc.getTemplate().getId());
			if (temp.getStorageContainer() != null
					&& !temp.getStorageContainer().equals("")) {
				maxNum = Integer.valueOf(fishCrossTaskDao.selectCountMax(
						sc.getId()).toString());
				sc.setMaxNum((maxNum - 1) / (rowCode * colCode) + 1);
			}
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delFishCrossTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			FishCrossTaskItem scp = fishCrossTaskDao.get(FishCrossTaskItem.class, id);
			if (scp.getId() != null) {
				fishCrossTaskDao.delete(scp);
			}
			// 改变左侧样本状态
			FishCrossTaskTemp dt = commonDAO.get(FishCrossTaskTemp.class, scp.getTempId());
			if (dt != null) {
				dt.setState("1");
			}
			commonDAO.update(dt);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveFishCrossTaskResult(FishCrossTask sc, String itemDataJson)
			throws Exception {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		List<FishCrossTaskResult> saveItems = new ArrayList<FishCrossTaskResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			FishCrossTaskResult scp = new FishCrossTaskResult();
			// 将map信息读入实体类
			scp = (FishCrossTaskResult) fishCrossTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setFishCrossTask(sc);
			saveItems.add(scp);
//			if (scp != null){
//				if (scp.getResult() != null && scp.getSubmit() != null &&
//						!scp.getResult().equals("") && !scp.getSubmit().equals("") ){
//					if(scp.getSubmit().equals("1")){
//						if(scp.getResult().equals("0")){
//							FishAbnormal ka=new FishAbnormal();
//							ka.setCode(scp.getCode());
//							ka.setSampleCode(scp.getSampleCode());
//							ka.setProductId(scp.getProductId());
//							ka.setProductName(scp.getProductName());
//							ka.setOrderId(scp.getOrderId());
//							ka.setAcceptDate(scp.getAcceptDate());
//							ka.setReportDate(scp.getReportDate());
//							ka.setSampleType(scp.getSampleType());
//							ka.setTaskId(scp.getFishCrossTask().getId());
//							ka.setNote(scp.getNote());
//							ka.setTaskName("Fish杂交洗脱");
//							ka.setState("1");
//							
//							commonDAO.saveOrUpdate(ka);
//						}
//					}
//				}
//			}
		}
		fishCrossTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delFishCrossTaskResult(String[] ids) throws Exception {
		for (String id : ids) {
			FishCrossTaskResult scp = fishCrossTaskDao
					.get(FishCrossTaskResult.class, id);
			fishCrossTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveFishCrossTaskTemplate(FishCrossTask sc, String itemDataJson)
			throws Exception {
		List<FishCrossTaskTemplate> saveItems = new ArrayList<FishCrossTaskTemplate>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			FishCrossTaskTemplate scp = new FishCrossTaskTemplate();
			// 将map信息读入实体类
			scp = (FishCrossTaskTemplate) fishCrossTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setFishCrossTask(sc);
			saveItems.add(scp);
		}
		fishCrossTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delFishCrossTaskTemplate(String[] ids) throws Exception {
		for (String id : ids) {
			FishCrossTaskTemplate scp = fishCrossTaskDao.get(
					FishCrossTaskTemplate.class, id);
			fishCrossTaskDao.delete(scp);
		}
	}

	// ============2015-12-02 ly================
	/**
	 * 根据ID删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delFishCrossTaskTemplateOne(String ids) throws Exception {
		FishCrossTaskTemplate scp = fishCrossTaskDao.get(FishCrossTaskTemplate.class,
				ids);
		fishCrossTaskDao.delete(scp);
	}

	// ============================
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveFishCrossTaskReagent(FishCrossTask sc, String itemDataJson)
			throws Exception {
		List<FishCrossTaskReagent> saveItems = new ArrayList<FishCrossTaskReagent>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			FishCrossTaskReagent scp = new FishCrossTaskReagent();
			// 将map信息读入实体类
			scp = (FishCrossTaskReagent) fishCrossTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			// 用量 = 单个用量*数量
			if (scp != null && scp.getOneNum() != null
					&& scp.getCount() != null) {
				scp.setNum(scp.getOneNum() * scp.getCount());
			}
			scp.setFishCrossTask(sc);
			saveItems.add(scp);
		}
		fishCrossTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delFishCrossTaskReagent(String[] ids) throws Exception {
		for (String id : ids) {
			FishCrossTaskReagent scp = fishCrossTaskDao.get(FishCrossTaskReagent.class,
					id);
			fishCrossTaskDao.delete(scp);
		}
	}

	// ================2015-12-02 ly=====================
	/**
	 * 根据ID删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delFishCrossTaskReagentOne(String ids) throws Exception {
		FishCrossTaskReagent scp = fishCrossTaskDao.get(FishCrossTaskReagent.class, ids);
		fishCrossTaskDao.delete(scp);
	}

	// =====================================
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveFishCrossTaskCos(FishCrossTask sc, String itemDataJson)
			throws Exception {
		List<FishCrossTaskCos> saveItems = new ArrayList<FishCrossTaskCos>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			FishCrossTaskCos scp = new FishCrossTaskCos();
			// 将map信息读入实体类
			scp = (FishCrossTaskCos) fishCrossTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setFishCrossTask(sc);
			saveItems.add(scp);
		}
		fishCrossTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delFishCrossTaskCos(String[] ids) throws Exception {
		for (String id : ids) {
			FishCrossTaskCos scp = fishCrossTaskDao.get(FishCrossTaskCos.class, id);
			fishCrossTaskDao.delete(scp);
		}
	}

	// ============2015-12-02 ly====================
	/**
	 * 根据ID删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delFishCrossTaskCosOne(String ids) throws Exception {
		FishCrossTaskCos scp = fishCrossTaskDao.get(FishCrossTaskCos.class, ids);
		fishCrossTaskDao.delete(scp);
	}

	// ================================
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FishCrossTask sc, Map jsonMap) throws Exception {
		if (sc != null) {
			fishCrossTaskDao.saveOrUpdate(sc);
			if (sc.getTemplate() != null) {
				String id = sc.getTemplate().getId();
				templateService.setCosIsUsedSave(id);
			}
			String jsonStr = "";
			jsonStr = (String) jsonMap.get("fishCrossTaskItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveFishCrossTaskItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("fishCrossTaskResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveFishCrossTaskResult(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("fishCrossTaskTemplate");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveFishCrossTaskTemplate(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("fishCrossTaskReagent");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveFishCrossTaskReagent(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("fishCrossTaskCos");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveFishCrossTaskCos(sc, jsonStr);
			}
			
			List<FishCrossTaskItem> snlist=fishCrossTaskDao.getItem(sc.getId());
			List<FishCrossTaskReagent> rglist=fishCrossTaskDao.getReagent(sc.getId());
			if(snlist.size()>0){
				if(rglist.size()>0){
					for(FishCrossTaskReagent sr:rglist){
						sr.setSampleNum(Double.valueOf(snlist.size()));
						if(sr.getOneNum()!=null && 
								sr.getSampleNum()!=null){
							sr.setNum(sr.getOneNum()*snlist.size());
						}
					}
				}
			}
		}
	}

	// 审批完成
	public void chengeState(String applicationTypeActionId, String id) throws Exception{
		FishCrossTask sct = fishCrossTaskDao.get(FishCrossTask.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);

		fishCrossTaskDao.update(sct);

		submitSample(id,null);

		// 修改样本接收子表的样本状态
		List<FishCrossTaskResult> bsr1 = fishCrossTaskDao.findFishCrossTaskResult(sct
				.getId());

		// List<FishCrossTaskTemp> sri = FishCrossTaskDao.findBeforeResultList();
		for (FishCrossTaskResult scp : bsr1) {
			FishCrossTaskTemp ft=commonDAO.get(FishCrossTaskTemp.class, scp.getTempId());
			if(ft!=null){
				ft.setState("2");
			}
		}
		/*if (scp != null) {
				if (scp.getResult() != null) {
					String isOk = scp.getResult();
					if (isOk.equals("1")) {
						DateFormat format = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss");
						sampleStateService
								.saveSampleState(
										scp.getCode(),
										scp.getSampleCode(),
										scp.getProductId(),
										scp.getProductName(),
										"",
//										DateUtil.format(sct.getCreateDate()),
										sct.getCreateDate(),
										format.format(new Date()),
										"FishCrossTask",
										"制片",
										(User) ServletActionContext
												.getRequest()
												.getSession()
												.getAttribute(
														SystemConstants.USER_SESSION_KEY),
														sct.getId(), "杂交洗脱",
										scp.getResult(), null, null, null,
										null, null, null, null, null);
					} else {
						FishAbnormal ka=new FishAbnormal();
						ka.setCode(scp.getCode());
						ka.setSampleCode(scp.getSampleCode());
						ka.setProductId(scp.getProductId());
						ka.setProductName(scp.getProductName());
						ka.setSampleType(scp.getSampleType());
						ka.setTaskId(scp.getFishCrossTask().getId());
						ka.setNote(scp.getNote());
						ka.setTaskName("制片");
						ka.setState("1");
						fishCrossTaskDao.saveOrUpdate(ka);
					}
				}
			}*/
		}
	
/**
 * 提交样本
 * @param id
 * @param ids
 * @throws Exception
 */
@WriteOperLog
@WriteExOperLog
@Transactional(rollbackFor = Exception.class)
public void submitSample(String id, String[] ids) throws Exception {
	// 获取主表信息
	FishCrossTask sc = this.fishCrossTaskDao.get(FishCrossTask.class, id);
	// 获取结果表样本信息

	List<FishCrossTaskResult> list=new ArrayList<FishCrossTaskResult>();
	if (ids == null)
		list = this.fishCrossTaskDao.setResultById(id);
	else
		list = this.fishCrossTaskDao.setResultByIds(ids);
	for (FishCrossTaskResult scp : list) {
		if (scp != null) {
			if (scp.getResult() != null && scp.getSubmit() == null) {
				String isOk = scp.getResult();
				if (isOk.equals("1")) {
					DateFormat format = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss");
					sampleStateService
							.saveSampleState(
									scp.getCode(),
									scp.getSampleCode(),
									scp.getProductId(),
									scp.getProductName(),
									"",
									sc.getCreateDate(),
									format.format(new Date()),
									"FishCrossTask",
									"Fish杂交洗脱",
									(User) ServletActionContext
											.getRequest()
											.getSession()
											.getAttribute(
													SystemConstants.USER_SESSION_KEY),
									sc.getId(), "Fish上机阅片",
									scp.getResult(), null, null, null,
									null, null, null, null, null);
					scp.setSubmit("1");
					fishCrossTaskDao.saveOrUpdate(scp);
				} else {
					if(scp.getNextFlowId().equals("0038")){
						//提交不和合格的到报告终止
				    	EndReport cr = new EndReport();
						cr.setCode(scp.getCode());
						cr.setSampleCode(scp.getSampleCode());
						cr.setSampleType(scp.getSampleType());
						cr.setProductId(scp.getProductId());
						cr.setProductName(scp.getProductName());
						cr.setAdvice(scp.getNote());
						cr.setTaskType("Fish杂交洗脱");
						cr.setTaskId(scp.getId());
						cr.setTaskResultId(scp.getFishCrossTask().getId());
						cr.setWaitDate(new Date());
				    	commonDAO.saveOrUpdate(cr);
					}else{
						// 不合格的到异常
						FishAbnormal ka = new FishAbnormal();
						ka.setCode(scp.getCode());
						ka.setSampleCode(scp.getSampleCode());
						ka.setProductId(scp.getProductId());
						ka.setProductName(scp.getProductName());
						ka.setSampleType(scp.getSampleType());
						ka.setTaskId(scp.getFishCrossTask().getId());
						ka.setNote(scp.getNote());
						ka.setTaskName("Fish杂交洗脱");
						ka.setState("1");
						commonDAO.saveOrUpdate(ka);
					} 
					scp.setSubmit("1");
				}
			}
		}
	}
}

		
	

	/**
	 * 结果表被保存时 修改临时表的状态
	 * @param code
	 */
	public void changeStateForTemp(String code) {
		FishCrossTaskTemp FishCrossTaskTemp = this.commonDAO.get(
				FishCrossTaskTemp.class, code);
		if (FishCrossTaskTemp != null) {
			FishCrossTaskTemp.setState("2");
		}
	}

	/**
	 * 根据选中的步骤查询相关的试剂明细
	 * 
	 * @param id
	 * @param code
	 * @return
	 */
	public List<Map<String, String>> setReagent(String id, String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = fishCrossTaskDao.setReagent(id, code);
		List<FishCrossTaskReagent> list = (List<FishCrossTaskReagent>) result
				.get("list");
		if (list != null && list.size() > 0) {
			for (FishCrossTaskReagent ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("code", ti.getCode());
				map.put("name", ti.getName());
				map.put("batch", ti.getBatch());
				map.put("isGood", ti.getIsGood());
				map.put("note", ti.getNote());
				map.put("tReagent", ti.gettReagent());

				if (ti.getOneNum() != null) {
					map.put("oneNum", ti.getOneNum().toString());
				} else {
					map.put("oneNum", "");
				}

				if (ti.getSampleNum() != null) {
					map.put("sampleNum", ti.getSampleNum().toString());
				} else {
					map.put("sampleNum", "");
				}

				if (ti.getNum() != null) {
					map.put("num", ti.getNum().toString());
				} else {
					map.put("num", "");
				}
				map.put("itemId", ti.getItemId());
				map.put("tId", ti.getFishCrossTask().getId());
				map.put("tName", ti.getFishCrossTask().getName());
				mapList.add(map);
			}
		}
		return mapList;
	}

	/**
	 * 根据选中的步骤查询相关的设备明细
	 * 
	 * @param id
	 * @param code
	 * @return
	 */
	public List<Map<String, String>> setCos(String id, String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = fishCrossTaskDao.setCos(id, code);
		List<FishCrossTaskCos> list = (List<FishCrossTaskCos>) result.get("list");
		if (list != null && list.size() > 0) {
			for (FishCrossTaskCos ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("code", ti.getCode());
				map.put("name", ti.getName());
				map.put("isGood", ti.getIsGood());
				if (ti.getTemperature() != null) {
					map.put("temperature", ti.getTemperature().toString());
				} else {
					map.put("temperature", "");
				}

				if (ti.getSpeed() != null) {
					map.put("speed", ti.getSpeed().toString());
				} else {
					map.put("speed", "");
				}

				if (ti.getTime() != null) {
					map.put("time", ti.getTime().toString());
				} else {
					map.put("time", "");
				}

				map.put("itemId", ti.getItemId());
				map.put("note", ti.getNote());
				map.put("tCos", ti.gettCos());
				map.put("tId", ti.getFishCrossTask().getId());
				map.put("tName", ti.getFishCrossTask().getName());
				mapList.add(map);
			}

		}
		return mapList;
	}
	
	/**
	 * 根据FishCrossTask加载info子表
	 * @param code
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, String>> showFishCrossTaskResultList(String code) throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = fishCrossTaskDao.setFishCrossTaskResultList(code);
		List<FishCrossTaskResult> list = (List<FishCrossTaskResult>) result.get("list");
		if (list != null && list.size() > 0) {
			for (FishCrossTaskResult pt : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", pt.getId());
				map.put("code", pt.getCode());
				map.put("sampleCode", pt.getSampleCode());
				map.put("slideCode", pt.getSlideCode());
				map.put("productId", pt.getProductId());
				map.put("productName", pt.getProductName());
				map.put("sampleType", pt.getSampleType());
				map.put("result", pt.getResult());
				map.put("submit", pt.getSubmit());
				map.put("nextId", pt.getNextFlowId());
				map.put("next", pt.getNextFlow());
				map.put("note",pt.getNote());
				mapList.add(map);
			}
		}
		return mapList;
	}
}
