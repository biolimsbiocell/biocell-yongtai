package com.biolims.experiment.dnap.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.experiment.dnap.service.DnapTaskService;

public class DnapTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		DnapTaskService mbService = (DnapTaskService) ctx
				.getBean("dnapTaskService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
