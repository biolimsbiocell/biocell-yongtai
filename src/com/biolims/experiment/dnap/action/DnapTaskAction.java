package com.biolims.experiment.dnap.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.dnap.model.DnapTask;
import com.biolims.experiment.dnap.model.DnapTaskCos;
import com.biolims.experiment.dnap.model.DnapTaskItem;
import com.biolims.experiment.dnap.model.DnapTaskReagent;
import com.biolims.experiment.dnap.model.DnapTaskResult;
import com.biolims.experiment.dnap.model.DnapTaskTemp;
import com.biolims.experiment.dnap.model.DnapTaskTemplate;
import com.biolims.experiment.dnap.service.DnapTaskService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/dnap/dnapTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class DnapTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "2410";
	@Autowired
	private DnapTaskService dnapTaskService;
	private DnapTask dnapTask = new DnapTask();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;

	@Action(value = "showDnapTaskList")
	public String showDnapTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/dnap/dnapTask.jsp");
	}

	@Action(value = "showDnapTaskListJson")
	public void showDnapTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = dnapTaskService.findDnapTaskList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<DnapTask> list = (List<DnapTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("reciveUser-id", "");
		map.put("reciveUser-name", "");
		map.put("reciveDate", "yyyy-MM-dd");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("indexa", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("maxNum", "");
		map.put("qcNum", "");
		map.put("confirmDate", "yyyy-MM-dd HH:mm:ss");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "dnapTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogDnapTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/dnap/dnapTaskDialog.jsp");
	}

	@Action(value = "showDialogDnapTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogDnapTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = dnapTaskService.findDnapTaskList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<DnapTask> list = (List<DnapTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("reciveUser-id", "");
		map.put("reciveUser-name", "");
		map.put("reciveDate", "yyyy-MM-dd");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("indexa", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("maxNum", "");
		map.put("qcNum", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editDnapTask")
	public String editDnapTask() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			dnapTask = dnapTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "dnapTask");
		} else {
			dnapTask.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			dnapTask.setCreateUser(user);
			dnapTask.setCreateDate(new Date());
			dnapTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			dnapTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/dnap/dnapTaskEdit.jsp");
	}

	@Action(value = "copyDnapTask")
	public String copyDnapTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		dnapTask = dnapTaskService.get(id);
		dnapTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/dnap/dnapTaskEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = dnapTask.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "DnapTask";
			String markCode = "DNAP";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			dnapTask.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("dnapTaskItem", getParameterFromRequest("dnapTaskItemJson"));

		aMap.put("dnapTaskTemplate",
				getParameterFromRequest("dnapTaskTemplateJson"));

		aMap.put("dnapTaskReagent",
				getParameterFromRequest("dnapTaskReagentJson"));

		aMap.put("dnapTaskCos", getParameterFromRequest("dnapTaskCosJson"));

		aMap.put("dnapTaskResult",
				getParameterFromRequest("dnapTaskResultJson"));

		aMap.put("dnapTaskTemp", getParameterFromRequest("dnapTaskTempJson"));

		dnapTaskService.save(dnapTask, aMap);
		return redirect("/experiment/dnap/dnapTask/editDnapTask.action?id="
				+ dnapTask.getId());

	}

	@Action(value = "viewDnapTask")
	public String toViewDnapTask() throws Exception {
		String id = getParameterFromRequest("id");
		dnapTask = dnapTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/dnap/dnapTaskEdit.jsp");
	}

	@Action(value = "showDnapTaskItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDnapTaskItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/dnap/dnapTaskItem.jsp");
	}

	@Action(value = "showDnapTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDnapTaskItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = dnapTaskService.findDnapTaskItemList(
					scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<DnapTaskItem> list = (List<DnapTaskItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("orderNumber", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("indexa", "");
			map.put("concentration", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("stepNum", "");
			map.put("note", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "yyyy-MM-dd");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("dnapTask-name", "");
			map.put("dnapTask-id", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("volume", "");
			map.put("sampleNum", "");
			map.put("addVolume", "");
			map.put("sumVolume", "");
			map.put("counts", "");
			map.put("unit", "");
			map.put("projectId", "");
			map.put("contractId", "");
			map.put("orderType", "");
			map.put("jkTaskId", "");
			map.put("classify", "");
			map.put("dataType", "");
			map.put("dataNum", "");
			map.put("productNum", "");
			map.put("sampleType", "");
			map.put("tempId", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delDnapTaskItem")
	public void delDnapTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			dnapTaskService.delDnapTaskItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showDnapTaskTemplateList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDnapTaskTemplateList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/dnap/dnapTaskTemplate.jsp");
	}

	@Action(value = "showDnapTaskTemplateListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDnapTaskTemplateListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = dnapTaskService
					.findDnapTaskTemplateList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<DnapTaskTemplate> list = (List<DnapTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("reciveUser-name", "");
			map.put("reciveUser-id", "");
			map.put("tItem", "");
			map.put("code", "");
			map.put("stepName", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("state", "");
			map.put("note", "");
			map.put("sampleCodes", "");
			map.put("dnapTask-name", "");
			map.put("dnapTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delDnapTaskTemplate")
	public void delDnapTaskTemplate() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			dnapTaskService.delDnapTaskTemplate(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据id删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delDnapTaskTemplateOne")
	public void delDnapTaskTemplateOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			dnapTaskService.delDnapTaskTemplateOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showDnapTaskReagentList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDnapTaskReagentList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/dnap/dnapTaskReagent.jsp");
	}

	@Action(value = "showDnapTaskReagentListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDnapTaskReagentListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = dnapTaskService
					.findDnapTaskReagentList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<DnapTaskReagent> list = (List<DnapTaskReagent>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("batch", "");
			map.put("count", "");
			map.put("oneNum", "");
			map.put("sampleNum", "");
			map.put("num", "");
			map.put("isGood", "");
			map.put("note", "");
			map.put("dnapTask-name", "");
			map.put("dnapTask-id", "");
			map.put("itemId", "");
			map.put("tReagent", "");
			map.put("sn", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delDnapTaskReagent")
	public void delDnapTaskReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			dnapTaskService.delDnapTaskReagent(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据id删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delDnapTaskReagentOne")
	public void delDnapTaskReagentOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			dnapTaskService.delDnapTaskReagentOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showDnapTaskCosList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDnapTaskCosList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/dnap/dnapTaskCos.jsp");
	}

	@Action(value = "showDnapTaskCosListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDnapTaskCosListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = dnapTaskService.findDnapTaskCosList(
					scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<DnapTaskCos> list = (List<DnapTaskCos>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("temperature", "");
			map.put("speed", "");
			map.put("time", "");
			map.put("note", "");
			map.put("isGood", "");
			map.put("dnapTask-name", "");
			map.put("dnapTask-id", "");
			map.put("itemId", "");
			map.put("tCos", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delDnapTaskCos")
	public void delDnapTaskCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			dnapTaskService.delDnapTaskCos(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据id删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delDnapTaskCosOne")
	public void delDnapTaskCosOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			dnapTaskService.delDnapTaskCosOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showDnapTaskResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDnapTaskResultList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/dnap/dnapTaskResult.jsp");
	}

	@Action(value = "showDnapTaskResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDnapTaskResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = dnapTaskService
					.findDnapTaskResultList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<DnapTaskResult> list = (List<DnapTaskResult>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("jkTaskId", "");
			map.put("indexa", "");
			map.put("volume", "");
			map.put("unit", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("submit", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "yyyy-MM-dd");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("method", "");
			map.put("isExecute", "");
			map.put("note", "");
			map.put("dnapTask-name", "");
			map.put("dnapTask-id", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("counts", "");
			map.put("contractId", "");
			map.put("projectId", "");
			map.put("orderType", "");
			map.put("classify", "");
			map.put("dataType", "");
			map.put("dataNum", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("sampleType", "");
			map.put("tempId", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delDnapTaskResult")
	public void delDnapTaskResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			dnapTaskService.delDnapTaskResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showDnapTaskTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDnapTaskTempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/dnap/dnapTaskTemp.jsp");
	}

	@Action(value = "showDnapTaskTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDnapTaskTempListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = dnapTaskService.findDnapTaskTempList(
					scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<DnapTaskTemp> list = (List<DnapTaskTemp>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "yyyy-MM-dd");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("volume", "");
			map.put("unit", "");
			map.put("idCard", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("state", "");
			map.put("note", "");
			map.put("classify", "");
			map.put("sampleType", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delDnapTaskTemp")
	public void delDnapTaskTemp() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			dnapTaskService.delDnapTaskTemp(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public DnapTaskService getDnapTaskService() {
		return dnapTaskService;
	}

	public void setDnapTaskService(DnapTaskService dnapTaskService) {
		this.dnapTaskService = dnapTaskService;
	}

	public DnapTask getDnapTask() {
		return dnapTask;
	}

	public void setDnapTask(DnapTask dnapTask) {
		this.dnapTask = dnapTask;
	}

//	// 提交样本
//		@Action(value = "submitSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//		public void submitSample() throws Exception {
//			String id = getParameterFromRequest("id");
//			String[] ids = getRequest().getParameterValues("ids[]");
//			Map<String, Object> result = new HashMap<String, Object>();
//			try {
//				this.dnapTaskService.submitSample(id, ids);
//				result.put("success", true);
//
//			} catch (Exception e) {
//				result.put("success", false);
//			}
//			HttpUtils.write(JsonUtils.toJsonString(result));
//		}
//
//	}
//
//	
}
