package com.biolims.experiment.dnap.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.experiment.dna.model.DnaTaskAbnormal;
import com.biolims.experiment.dnap.dao.DnapTaskDao;
import com.biolims.experiment.dnap.model.DnapTask;
import com.biolims.experiment.dnap.model.DnapTaskCos;
import com.biolims.experiment.dnap.model.DnapTaskItem;
import com.biolims.experiment.dnap.model.DnapTaskReagent;
import com.biolims.experiment.dnap.model.DnapTaskResult;
import com.biolims.experiment.dnap.model.DnapTaskTemp;
import com.biolims.experiment.dnap.model.DnapTaskTemplate;
import com.biolims.experiment.plasma.model.PlasmaAbnormal;
import com.biolims.experiment.qc.model.Qc2100TaskTemp;
import com.biolims.experiment.qc.model.QcQpcrTaskTemp;
import com.biolims.experiment.wk.model.WkTaskAbnormal;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class DnapTaskService {
	@Resource
	private DnapTaskDao dnapTaskDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private CommonService commonService;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findDnapTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return dnapTaskDao.selectDnapTaskList(mapForQuery, startNum, limitNum,
				dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(DnapTask i) throws Exception {

		dnapTaskDao.saveOrUpdate(i);

	}

	public DnapTask get(String id) {
		DnapTask dnapTask = commonDAO.get(DnapTask.class, id);
		return dnapTask;
	}

	public Map<String, Object> findDnapTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = dnapTaskDao.selectDnapTaskItemList(scId,
				startNum, limitNum, dir, sort);
		List<DnapTaskItem> list = (List<DnapTaskItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findDnapTaskTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = dnapTaskDao.selectDnapTaskTemplateList(
				scId, startNum, limitNum, dir, sort);
		List<DnapTaskTemplate> list = (List<DnapTaskTemplate>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findDnapTaskReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = dnapTaskDao.selectDnapTaskReagentList(
				scId, startNum, limitNum, dir, sort);
		List<DnapTaskReagent> list = (List<DnapTaskReagent>) result.get("list");
		return result;
	}

	public Map<String, Object> findDnapTaskCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = dnapTaskDao.selectDnapTaskCosList(scId,
				startNum, limitNum, dir, sort);
		List<DnapTaskCos> list = (List<DnapTaskCos>) result.get("list");
		return result;
	}

	public Map<String, Object> findDnapTaskResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = dnapTaskDao.selectDnapTaskResultList(scId,
				startNum, limitNum, dir, sort);
		List<DnapTaskResult> list = (List<DnapTaskResult>) result.get("list");
		return result;
	}

	public Map<String, Object> findDnapTaskTempList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = dnapTaskDao.selectDnapTaskTempList(scId,
				startNum, limitNum, dir, sort);
		List<DnapTaskTemp> list = (List<DnapTaskTemp>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveDnapTaskItem(DnapTask sc, String itemDataJson)
			throws Exception {
		List<DnapTaskItem> saveItems = new ArrayList<DnapTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			DnapTaskItem scp = new DnapTaskItem();
			// 将map信息读入实体类
			scp = (DnapTaskItem) dnapTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setDnapTask(sc);

			saveItems.add(scp);
		}
		dnapTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDnapTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			DnapTaskItem scp = dnapTaskDao.get(DnapTaskItem.class, id);
			dnapTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveDnapTaskTemplate(DnapTask sc, String itemDataJson)
			throws Exception {
		List<DnapTaskTemplate> saveItems = new ArrayList<DnapTaskTemplate>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			DnapTaskTemplate scp = new DnapTaskTemplate();
			// 将map信息读入实体类
			scp = (DnapTaskTemplate) dnapTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setDnapTask(sc);

			saveItems.add(scp);
		}
		dnapTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDnapTaskTemplate(String[] ids) throws Exception {
		for (String id : ids) {
			DnapTaskTemplate scp = dnapTaskDao.get(DnapTaskTemplate.class, id);
			dnapTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDnapTaskTemplateOne(String ids) throws Exception {
		DnapTaskTemplate scp = dnapTaskDao.get(DnapTaskTemplate.class, ids);
		dnapTaskDao.delete(scp);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveDnapTaskReagent(DnapTask sc, String itemDataJson)
			throws Exception {
		List<DnapTaskReagent> saveItems = new ArrayList<DnapTaskReagent>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			DnapTaskReagent scp = new DnapTaskReagent();
			// 将map信息读入实体类
			scp = (DnapTaskReagent) dnapTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setDnapTask(sc);
			// 用量 = 单个用量*样本数量
			if (scp != null && scp.getSampleNum() != null
					&& scp.getOneNum() != null) {
				scp.setNum(scp.getOneNum() * scp.getSampleNum());
			}

			saveItems.add(scp);
		}
		dnapTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDnapTaskReagent(String[] ids) throws Exception {
		for (String id : ids) {
			DnapTaskReagent scp = dnapTaskDao.get(DnapTaskReagent.class, id);
			dnapTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDnapTaskReagentOne(String ids) throws Exception {
		DnapTaskReagent scp = dnapTaskDao.get(DnapTaskReagent.class, ids);
		dnapTaskDao.delete(scp);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveDnapTaskCos(DnapTask sc, String itemDataJson)
			throws Exception {
		List<DnapTaskCos> saveItems = new ArrayList<DnapTaskCos>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			DnapTaskCos scp = new DnapTaskCos();
			// 将map信息读入实体类
			scp = (DnapTaskCos) dnapTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setDnapTask(sc);

			saveItems.add(scp);
		}
		dnapTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDnapTaskCos(String[] ids) throws Exception {
		for (String id : ids) {
			DnapTaskCos scp = dnapTaskDao.get(DnapTaskCos.class, id);
			dnapTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDnapTaskCosOne(String ids) throws Exception {
		DnapTaskCos scp = dnapTaskDao.get(DnapTaskCos.class, ids);
		dnapTaskDao.delete(scp);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveDnapTaskResult(DnapTask sc, String itemDataJson)
			throws Exception {
		List<DnapTaskResult> saveItems = new ArrayList<DnapTaskResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			DnapTaskResult scp = new DnapTaskResult();
			// 将map信息读入实体类
			scp = (DnapTaskResult) dnapTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setDnapTask(sc);
			if (scp.getCode() == null || scp.getCode().equals("")) {
				String markCode = scp.getSampleCode() + "CH";
				String code = codingRuleService.getCode("DnapTaskResult",
						markCode, 00, 2, null);
				scp.setCode(code);
			}
			dnapTaskDao.saveOrUpdate(scp);

			if (scp.getResult() != null && scp.getSubmit() != null) {
				if (scp.getSubmit().equals("1")) {
					if (scp.getResult().equals("1")) {
						String nextFlowId = scp.getNextFlowId();NextFlow nf = commonService.get(NextFlow.class, nextFlowId);nextFlowId = nf.getSysCode();
						if (nextFlowId.equals("0009")) {// 样本入库
							SampleInItemTemp st = new SampleInItemTemp();
							st.setCode(scp.getCode());
							st.setSampleCode(scp.getSampleCode());
							st.setNum(scp.getVolume());
							st.setSampleType(scp.getSampleType());
							st.setState("1");
							dnapTaskDao.saveOrUpdate(st);
						} else if (scp.getNextFlowId().equals("0010")) {// 重提取
							DnaTaskAbnormal eda = new DnaTaskAbnormal();
							scp.setState("4");// 状态为4 在重提取中显示
							sampleInputService.copy(eda, scp);

						} else if (scp.getNextFlowId().equals("0011")) {// 重建库
							WkTaskAbnormal wka = new WkTaskAbnormal();
							scp.setState("4");
							sampleInputService.copy(wka, scp);
						} else if (scp.getNextFlowId().equals("0024")) {// 重抽血
							PlasmaAbnormal wka = new PlasmaAbnormal();
							scp.setState("4");
							sampleInputService.copy(wka, scp);
						} else if (nextFlowId.equals("0012")) {// 暂停

						} else if (nextFlowId.equals("0013")) {// 终止

						} else if (nextFlowId.equals("0020")) {// 2100质控
							Qc2100TaskTemp qc2100 = new Qc2100TaskTemp();
							scp.setState("1");
							sampleInputService.copy(qc2100, scp);
							qc2100.setWkType("2");
							dnapTaskDao.saveOrUpdate(qc2100);
						} else if (nextFlowId.equals("0021")) {// QPCR质控
							QcQpcrTaskTemp qpcr = new QcQpcrTaskTemp();
							scp.setState("1");
							sampleInputService.copy(qpcr, scp);
							qpcr.setWkType("1");
							dnapTaskDao.saveOrUpdate(qpcr);
						} else {
							// 得到下一步流向的相关表单
							List<NextFlow> list_nextFlow = nextFlowDao
									.seletNextFlowById(nextFlowId);
							for (NextFlow n : list_nextFlow) {
								Object o = Class.forName(
										n.getApplicationTypeTable()
												.getClassPath()).newInstance();
								scp.setState("1");
								sampleInputService.copy(o, scp);
							}
						}
					}
					DateFormat format = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");
					sampleStateService.saveSampleState(
							scp.getCode(),
							scp.getSampleCode(),
							scp.getProductId(),
							scp.getProductName(),
							"",
							format.format(sc.getCreateDate()),
							format.format(new Date()),
							"CfdnaTask",
							"cfDNA质量评估",
							(User) ServletActionContext
									.getRequest()
									.getSession()
									.getAttribute(
											SystemConstants.USER_SESSION_KEY),
							sc.getId(), scp.getNextFlow(), scp.getResult(),
							null, null, null, null, null, null, null, null);
				}
			}
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDnapTaskResult(String[] ids) throws Exception {
		for (String id : ids) {
			DnapTaskResult scp = dnapTaskDao.get(DnapTaskResult.class, id);
			dnapTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDnapTaskTemp(String[] ids) throws Exception {
		for (String id : ids) {
			DnapTaskTemp scp = dnapTaskDao.get(DnapTaskTemp.class, id);
			dnapTaskDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(DnapTask sc, Map jsonMap) throws Exception {
		if (sc != null) {
			dnapTaskDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("dnapTaskItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveDnapTaskItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("dnapTaskTemplate");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveDnapTaskTemplate(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("dnapTaskReagent");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveDnapTaskReagent(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("dnapTaskCos");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveDnapTaskCos(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("dnapTaskResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveDnapTaskResult(sc, jsonStr);
			}

		}
	}

	// 审核完成
	public void changeState(String applicationTypeActionId, String id) {
		DnapTask sct = dnapTaskDao.get(DnapTask.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		List<DnapTaskItem> item = dnapTaskDao.setItemList(sct.getId());
		for (DnapTaskItem c : item) {
			DnapTaskTemp temp = commonDAO
					.get(DnapTaskTemp.class, c.getTempId());
			if (temp != null) {
				temp.setState("2");
			}
		}
	}
}
