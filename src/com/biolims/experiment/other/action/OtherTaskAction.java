package com.biolims.experiment.other.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.experiment.other.model.OtherTask;
import com.biolims.experiment.other.model.OtherTaskCos;
import com.biolims.experiment.other.model.OtherTaskItem;
import com.biolims.experiment.other.model.OtherTaskReagent;
import com.biolims.experiment.other.model.OtherTaskResult;
import com.biolims.experiment.other.model.OtherTaskTemp;
import com.biolims.experiment.other.model.OtherTaskTemplate;
import com.biolims.experiment.other.service.OtherTaskService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/experiment/other/otherTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class OtherTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "2411";
	@Autowired
	private OtherTaskService otherTaskService;
	private OtherTask otherTask = new OtherTask();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;

	@Action(value = "showOtherTaskList")
	public String showOtherTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/other/otherTask.jsp");
	}

	@Action(value = "showOtherTaskListJson")
	public void showOtherTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = otherTaskService.findOtherTaskList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<OtherTask> list = (List<OtherTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("reciveUser-id", "");
		map.put("reciveUser-name", "");
		map.put("reciveDate", "yyyy-MM-dd");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("indexa", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("maxNum", "");
		map.put("qcNum", "");
		map.put("confirmDate", "yyyy-MM-dd HH:mm:ss");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "otherTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogOtherTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/experiment/other/otherTaskDialog.jsp");
	}

	@Action(value = "showDialogOtherTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogOtherTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = otherTaskService.findOtherTaskList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<OtherTask> list = (List<OtherTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("reciveUser-id", "");
		map.put("reciveUser-name", "");
		map.put("reciveDate", "yyyy-MM-dd");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("indexa", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("maxNum", "");
		map.put("qcNum", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editOtherTask")
	public String editOtherTask() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			otherTask = otherTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "otherTask");
		} else {
			otherTask.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			otherTask.setCreateUser(user);
			otherTask.setCreateDate(new Date());
			otherTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			otherTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/experiment/other/otherTaskEdit.jsp");
	}

	@Action(value = "copyOtherTask")
	public String copyOtherTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		otherTask = otherTaskService.get(id);
		otherTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/experiment/other/otherTaskEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = otherTask.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "OtherTask";
			String markCode = "OTHER";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			otherTask.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("otherTaskItem", getParameterFromRequest("otherTaskItemJson"));

		aMap.put("otherTaskTemplate",
				getParameterFromRequest("otherTaskTemplateJson"));

		aMap.put("otherTaskReagent",
				getParameterFromRequest("otherTaskReagentJson"));

		aMap.put("otherTaskCos", getParameterFromRequest("otherTaskCosJson"));

		aMap.put("otherTaskResult",
				getParameterFromRequest("otherTaskResultJson"));

		aMap.put("otherTaskTemp", getParameterFromRequest("otherTaskTempJson"));

		otherTaskService.save(otherTask, aMap);
		return redirect("/experiment/other/otherTask/editOtherTask.action?id="
				+ otherTask.getId());

	}

	@Action(value = "viewOtherTask")
	public String toViewOtherTask() throws Exception {
		String id = getParameterFromRequest("id");
		otherTask = otherTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/experiment/other/otherTaskEdit.jsp");
	}

	@Action(value = "showOtherTaskItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showOtherTaskItemList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/other/otherTaskItem.jsp");
	}

	@Action(value = "showOtherTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showOtherTaskItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = otherTaskService
					.findOtherTaskItemList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<OtherTaskItem> list = (List<OtherTaskItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("orderNumber", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("indexa", "");
			map.put("concentration", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("stepNum", "");
			map.put("note", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "yyyy-MM-dd");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("otherTask-name", "");
			map.put("otherTask-id", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("volume", "");
			map.put("sampleNum", "");
			map.put("addVolume", "");
			map.put("sumVolume", "");
			map.put("counts", "");
			map.put("unit", "");
			map.put("projectId", "");
			map.put("contractId", "");
			map.put("orderType", "");
			map.put("jkTaskId", "");
			map.put("classify", "");
			map.put("dataType", "");
			map.put("dataNum", "");
			map.put("productNum", "");
			map.put("sampleType", "");
			map.put("tempId", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delOtherTaskItem")
	public void delOtherTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			otherTaskService.delOtherTaskItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showOtherTaskTemplateList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showOtherTaskTemplateList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/other/otherTaskTemplate.jsp");
	}

	@Action(value = "showOtherTaskTemplateListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showOtherTaskTemplateListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = otherTaskService
					.findOtherTaskTemplateList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<OtherTaskTemplate> list = (List<OtherTaskTemplate>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("reciveUser-name", "");
			map.put("reciveUser-id", "");
			map.put("tItem", "");
			map.put("code", "");
			map.put("stepName", "");
			map.put("startTime", "");
			map.put("endTime", "");
			map.put("state", "");
			map.put("note", "");
			map.put("sampleCodes", "");
			map.put("otherTask-name", "");
			map.put("otherTask-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delOtherTaskTemplate")
	public void delOtherTaskTemplate() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			otherTaskService.delOtherTaskTemplate(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据id删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delOtherTaskTemplateOne")
	public void delOtherTaskTemplateOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			otherTaskService.delOtherTaskTemplateOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showOtherTaskReagentList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showOtherTaskReagentList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/other/otherTaskReagent.jsp");
	}

	@Action(value = "showOtherTaskReagentListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showOtherTaskReagentListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = otherTaskService
					.findOtherTaskReagentList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<OtherTaskReagent> list = (List<OtherTaskReagent>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("batch", "");
			map.put("count", "");
			map.put("oneNum", "");
			map.put("sampleNum", "");
			map.put("num", "");
			map.put("isGood", "");
			map.put("note", "");
			map.put("otherTask-name", "");
			map.put("otherTask-id", "");
			map.put("itemId", "");
			map.put("tReagent", "");
			map.put("sn", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delOtherTaskReagent")
	public void delOtherTaskReagent() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			otherTaskService.delOtherTaskReagent(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据id删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delOtherTaskReagentOne")
	public void delOtherTaskReagentOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			otherTaskService.delOtherTaskReagentOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showOtherTaskCosList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showOtherTaskCosList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/other/otherTaskCos.jsp");
	}

	@Action(value = "showOtherTaskCosListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showOtherTaskCosListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = otherTaskService.findOtherTaskCosList(
					scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<OtherTaskCos> list = (List<OtherTaskCos>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("temperature", "");
			map.put("speed", "");
			map.put("time", "");
			map.put("note", "");
			map.put("isGood", "");
			map.put("otherTask-name", "");
			map.put("otherTask-id", "");
			map.put("itemId", "");
			map.put("tCos", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delOtherTaskCos")
	public void delOtherTaskCos() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			otherTaskService.delOtherTaskCos(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据id删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delOtherTaskCosOne")
	public void delOtherTaskCosOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids = getParameterFromRequest("ids");
			otherTaskService.delOtherTaskCosOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showOtherTaskResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showOtherTaskResultList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/other/otherTaskResult.jsp");
	}

	@Action(value = "showOtherTaskResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showOtherTaskResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = otherTaskService
					.findOtherTaskResultList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<OtherTaskResult> list = (List<OtherTaskResult>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("jkTaskId", "");
			map.put("indexa", "");
			map.put("volume", "");
			map.put("unit", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("submit", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "yyyy-MM-dd");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("idCard", "");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("method", "");
			map.put("isExecute", "");
			map.put("note", "");
			map.put("otherTask-name", "");
			map.put("otherTask-id", "");
			map.put("rowCode", "");
			map.put("colCode", "");
			map.put("counts", "");
			map.put("contractId", "");
			map.put("projectId", "");
			map.put("orderType", "");
			map.put("classify", "");
			map.put("dataType", "");
			map.put("dataNum", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("sampleType", "");
			map.put("tempId", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delOtherTaskResult")
	public void delOtherTaskResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			otherTaskService.delOtherTaskResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showOtherTaskTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showOtherTaskTempList() throws Exception {
		return dispatcher("/WEB-INF/page/experiment/other/otherTaskTemp.jsp");
	}

	@Action(value = "showOtherTaskTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showOtherTaskTempListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = otherTaskService
					.findOtherTaskTempList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<OtherTaskTemp> list = (List<OtherTaskTemp>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "yyyy-MM-dd");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("volume", "");
			map.put("unit", "");
			map.put("idCard", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("state", "");
			map.put("note", "");
			map.put("classify", "");
			map.put("sampleType", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delOtherTaskTemp")
	public void delOtherTaskTemp() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			otherTaskService.delOtherTaskTemp(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public OtherTaskService getOtherTaskService() {
		return otherTaskService;
	}

	public void setOtherTaskService(OtherTaskService otherTaskService) {
		this.otherTaskService = otherTaskService;
	}

	public OtherTask getOtherTask() {
		return otherTask;
	}

	public void setOtherTask(OtherTask otherTask) {
		this.otherTask = otherTask;
	}

}
