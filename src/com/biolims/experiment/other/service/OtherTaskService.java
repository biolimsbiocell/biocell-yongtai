package com.biolims.experiment.other.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.experiment.cfdna.model.CfdnaTaskItem;
import com.biolims.experiment.cfdna.model.CfdnaTaskTemp;
import com.biolims.experiment.dna.model.DnaTaskAbnormal;
import com.biolims.experiment.dnap.model.DnapTask;
import com.biolims.experiment.dnap.model.DnapTaskTemp;
import com.biolims.experiment.other.dao.OtherTaskDao;
import com.biolims.experiment.other.model.OtherTask;
import com.biolims.experiment.other.model.OtherTaskCos;
import com.biolims.experiment.other.model.OtherTaskItem;
import com.biolims.experiment.other.model.OtherTaskReagent;
import com.biolims.experiment.other.model.OtherTaskResult;
import com.biolims.experiment.other.model.OtherTaskTemp;
import com.biolims.experiment.other.model.OtherTaskTemplate;
import com.biolims.experiment.plasma.model.PlasmaAbnormal;
import com.biolims.experiment.qc.model.Qc2100TaskTemp;
import com.biolims.experiment.qc.model.QcQpcrTaskTemp;
import com.biolims.experiment.wk.model.WkTaskAbnormal;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class OtherTaskService {
	@Resource
	private OtherTaskDao otherTaskDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private CommonService commonService;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findOtherTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return otherTaskDao.selectOtherTaskList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(OtherTask i) throws Exception {

		otherTaskDao.saveOrUpdate(i);

	}

	public OtherTask get(String id) {
		OtherTask otherTask = commonDAO.get(OtherTask.class, id);
		return otherTask;
	}

	public Map<String, Object> findOtherTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = otherTaskDao.selectOtherTaskItemList(scId,
				startNum, limitNum, dir, sort);
		List<OtherTaskItem> list = (List<OtherTaskItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findOtherTaskTemplateList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = otherTaskDao.selectOtherTaskTemplateList(
				scId, startNum, limitNum, dir, sort);
		List<OtherTaskTemplate> list = (List<OtherTaskTemplate>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findOtherTaskReagentList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = otherTaskDao.selectOtherTaskReagentList(
				scId, startNum, limitNum, dir, sort);
		List<OtherTaskReagent> list = (List<OtherTaskReagent>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findOtherTaskCosList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = otherTaskDao.selectOtherTaskCosList(scId,
				startNum, limitNum, dir, sort);
		List<OtherTaskCos> list = (List<OtherTaskCos>) result.get("list");
		return result;
	}

	public Map<String, Object> findOtherTaskResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = otherTaskDao.selectOtherTaskResultList(
				scId, startNum, limitNum, dir, sort);
		List<OtherTaskResult> list = (List<OtherTaskResult>) result.get("list");
		return result;
	}

	public Map<String, Object> findOtherTaskTempList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = otherTaskDao.selectOtherTaskTempList(scId,
				startNum, limitNum, dir, sort);
		List<OtherTaskTemp> list = (List<OtherTaskTemp>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveOtherTaskItem(OtherTask sc, String itemDataJson)
			throws Exception {
		List<OtherTaskItem> saveItems = new ArrayList<OtherTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			OtherTaskItem scp = new OtherTaskItem();
			// 将map信息读入实体类
			scp = (OtherTaskItem) otherTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setOtherTask(sc);

			saveItems.add(scp);
		}
		otherTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delOtherTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			OtherTaskItem scp = otherTaskDao.get(OtherTaskItem.class, id);
			otherTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveOtherTaskTemplate(OtherTask sc, String itemDataJson)
			throws Exception {
		List<OtherTaskTemplate> saveItems = new ArrayList<OtherTaskTemplate>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			OtherTaskTemplate scp = new OtherTaskTemplate();
			// 将map信息读入实体类
			scp = (OtherTaskTemplate) otherTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setOtherTask(sc);

			saveItems.add(scp);
		}
		otherTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delOtherTaskTemplate(String[] ids) throws Exception {
		for (String id : ids) {
			OtherTaskTemplate scp = otherTaskDao.get(OtherTaskTemplate.class,
					id);
			otherTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delOtherTaskTemplateOne(String ids) throws Exception {
		OtherTaskTemplate scp = otherTaskDao.get(OtherTaskTemplate.class, ids);
		otherTaskDao.delete(scp);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveOtherTaskReagent(OtherTask sc, String itemDataJson)
			throws Exception {
		List<OtherTaskReagent> saveItems = new ArrayList<OtherTaskReagent>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			OtherTaskReagent scp = new OtherTaskReagent();
			// 将map信息读入实体类
			scp = (OtherTaskReagent) otherTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setOtherTask(sc);
			// 用量 = 单个用量*样本数量
			if (scp != null && scp.getSampleNum() != null
					&& scp.getOneNum() != null) {
				scp.setNum(scp.getOneNum() * scp.getSampleNum());
			}

			saveItems.add(scp);
		}
		otherTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delOtherTaskReagent(String[] ids) throws Exception {
		for (String id : ids) {
			OtherTaskReagent scp = otherTaskDao.get(OtherTaskReagent.class, id);
			otherTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delOtherTaskReagentOne(String ids) throws Exception {
		OtherTaskReagent scp = otherTaskDao.get(OtherTaskReagent.class, ids);
		otherTaskDao.delete(scp);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveOtherTaskCos(OtherTask sc, String itemDataJson)
			throws Exception {
		List<OtherTaskCos> saveItems = new ArrayList<OtherTaskCos>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			OtherTaskCos scp = new OtherTaskCos();
			// 将map信息读入实体类
			scp = (OtherTaskCos) otherTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setOtherTask(sc);

			saveItems.add(scp);
		}
		otherTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delOtherTaskCos(String[] ids) throws Exception {
		for (String id : ids) {
			OtherTaskCos scp = otherTaskDao.get(OtherTaskCos.class, id);
			otherTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delOtherTaskCosOne(String ids) throws Exception {
		OtherTaskCos scp = otherTaskDao.get(OtherTaskCos.class, ids);
		otherTaskDao.delete(scp);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveOtherTaskResult(OtherTask sc, String itemDataJson)
			throws Exception {
		List<OtherTaskResult> saveItems = new ArrayList<OtherTaskResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			OtherTaskResult scp = new OtherTaskResult();
			// 将map信息读入实体类
			scp = (OtherTaskResult) otherTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setOtherTask(sc);
			if (scp.getCode() == null || scp.getCode().equals("")) {
				String markCode = scp.getSampleCode() + "QT";
				String code = codingRuleService.getCode("OtherTaskResult",
						markCode, 00, 2, null);
				scp.setCode(code);
			}
			otherTaskDao.saveOrUpdate(scp);

			if (scp.getResult() != null && scp.getSubmit() != null) {
				if (scp.getSubmit().equals("1")) {
					if (scp.getResult().equals("1")) {
						String nextFlowId = scp.getNextFlowId();NextFlow nf = commonService.get(NextFlow.class, nextFlowId);nextFlowId = nf.getSysCode();
						if (nextFlowId.equals("0009")) {// 样本入库
							SampleInItemTemp st = new SampleInItemTemp();
							st.setCode(scp.getCode());
							st.setSampleCode(scp.getSampleCode());
							st.setNum(scp.getVolume());
							st.setSampleType(scp.getSampleType());
							st.setState("1");
							otherTaskDao.saveOrUpdate(st);
						} else if (scp.getNextFlowId().equals("0010")) {// 重提取
							DnaTaskAbnormal eda = new DnaTaskAbnormal();
							scp.setState("4");// 状态为4 在重提取中显示
							sampleInputService.copy(eda, scp);

						} else if (scp.getNextFlowId().equals("0011")) {// 重建库
							WkTaskAbnormal wka = new WkTaskAbnormal();
							scp.setState("4");
							sampleInputService.copy(wka, scp);
						} else if (scp.getNextFlowId().equals("0024")) {// 重抽血
							PlasmaAbnormal wka = new PlasmaAbnormal();
							scp.setState("4");
							sampleInputService.copy(wka, scp);
						} else if (nextFlowId.equals("0012")) {// 暂停

						} else if (nextFlowId.equals("0013")) {// 终止

						} else if (nextFlowId.equals("0020")) {// 2100质控
							Qc2100TaskTemp qc2100 = new Qc2100TaskTemp();
							scp.setState("1");
							sampleInputService.copy(qc2100, scp);
							qc2100.setWkType("2");
							otherTaskDao.saveOrUpdate(qc2100);
						} else if (nextFlowId.equals("0021")) {// QPCR质控
							QcQpcrTaskTemp qpcr = new QcQpcrTaskTemp();
							scp.setState("1");
							sampleInputService.copy(qpcr, scp);
							qpcr.setWkType("1");
							otherTaskDao.saveOrUpdate(qpcr);
						} else {
							// 得到下一步流向的相关表单
							List<NextFlow> list_nextFlow = nextFlowDao
									.seletNextFlowById(nextFlowId);
							for (NextFlow n : list_nextFlow) {
								Object o = Class.forName(
										n.getApplicationTypeTable()
												.getClassPath()).newInstance();
								scp.setState("1");
								sampleInputService.copy(o, scp);
							}
						}
					}
					DateFormat format = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");
					sampleStateService.saveSampleState(
							scp.getCode(),
							scp.getSampleCode(),
							scp.getProductId(),
							scp.getProductName(),
							"",
							format.format(sc.getCreateDate()),
							format.format(new Date()),
							"CfdnaTask",
							"cfDNA质量评估",
							(User) ServletActionContext
									.getRequest()
									.getSession()
									.getAttribute(
											SystemConstants.USER_SESSION_KEY),
							sc.getId(), scp.getNextFlow(), scp.getResult(),
							null, null, null, null, null, null, null, null);
				}
			}
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delOtherTaskResult(String[] ids) throws Exception {
		for (String id : ids) {
			OtherTaskResult scp = otherTaskDao.get(OtherTaskResult.class, id);
			otherTaskDao.delete(scp);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delOtherTaskTemp(String[] ids) throws Exception {
		for (String id : ids) {
			OtherTaskTemp scp = otherTaskDao.get(OtherTaskTemp.class, id);
			otherTaskDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(OtherTask sc, Map jsonMap) throws Exception {
		if (sc != null) {
			otherTaskDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("otherTaskItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveOtherTaskItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("otherTaskTemplate");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveOtherTaskTemplate(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("otherTaskReagent");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveOtherTaskReagent(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("otherTaskCos");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveOtherTaskCos(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("otherTaskResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveOtherTaskResult(sc, jsonStr);
			}

		}
	}

	// 审核完成
	public void changeState(String applicationTypeActionId, String id) {
		OtherTask sct = otherTaskDao.get(OtherTask.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		List<OtherTaskItem> item = otherTaskDao.setItemList(sct.getId());
		for (OtherTaskItem c : item) {
			OtherTaskTemp temp = commonDAO.get(OtherTaskTemp.class,
					c.getTempId());
			if (temp != null) {
				temp.setState("2");
			}

		}
	}
}
