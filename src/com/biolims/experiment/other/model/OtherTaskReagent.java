package com.biolims.experiment.other.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 试剂明细
 * @author lims-platform
 * @date 2016-03-25 11:24:07
 * @version V1.0
 * 
 */
@Entity
@Table(name = "OTHER_TASK_REAGENT")
@SuppressWarnings("serial")
public class OtherTaskReagent extends EntityDao<OtherTaskReagent> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 描述 */
	private String name;
	/** 试剂编号 */
	private String code;
	/** 批次 */
	private String batch;
	/** 数量 */
	private Integer count;
	/** 单个用量 */
	private Double oneNum;
	/** 样本数量 */
	private Double sampleNum;
	/** 用量 */
	private Double num;
	/** 是否通过检验 */
	private String isGood;
	/** 备注 */
	private String note;
	/** 相关主表 */
	private OtherTask otherTask;
	/** 关联步骤id */
	private String itemId;
	/** 模板试剂id */
	private String tReagent;
	// sn
	private String sn;

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 100)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 试剂编号
	 */
	@Column(name = "CODE", length = 50)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 试剂编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 批次
	 */
	@Column(name = "BATCH", length = 50)
	public String getBatch() {
		return this.batch;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 批次
	 */
	public void setBatch(String batch) {
		this.batch = batch;
	}

	/**
	 * 方法: 取得Integer
	 * 
	 * @return: Integer 数量
	 */
	@Column(name = "COUNT", length = 50)
	public Integer getCount() {
		return this.count;
	}

	/**
	 * 方法: 设置Integer
	 * 
	 * @param: Integer 数量
	 */
	public void setCount(Integer count) {
		this.count = count;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 单个用量
	 */
	@Column(name = "ONE_NUM", length = 50)
	public Double getOneNum() {
		return this.oneNum;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 单个用量
	 */
	public void setOneNum(Double oneNum) {
		this.oneNum = oneNum;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 样本数量
	 */
	@Column(name = "SAMPLE_NUM", length = 50)
	public Double getSampleNum() {
		return this.sampleNum;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 样本数量
	 */
	public void setSampleNum(Double sampleNum) {
		this.sampleNum = sampleNum;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double 用量
	 */
	@Column(name = "NUM", length = 50)
	public Double getNum() {
		return this.num;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double 用量
	 */
	public void setNum(Double num) {
		this.num = num;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 是否通过检验
	 */
	@Column(name = "IS_GOOD", length = 50)
	public String getIsGood() {
		return this.isGood;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 是否通过检验
	 */
	public void setIsGood(String isGood) {
		this.isGood = isGood;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得OtherTask
	 * 
	 * @return: OtherTask 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "OTHER_TASK")
	public OtherTask getOtherTask() {
		return this.otherTask;
	}

	/**
	 * 方法: 设置OtherTask
	 * 
	 * @param: OtherTask 相关主表
	 */
	public void setOtherTask(OtherTask otherTask) {
		this.otherTask = otherTask;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 关联步骤id
	 */
	@Column(name = "ITEM_ID", length = 50)
	public String getItemId() {
		return this.itemId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 关联步骤id
	 */
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 模板试剂id
	 */
	@Column(name = "T_REAGENT", length = 50)
	public String getTReagent() {
		return this.tReagent;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 模板试剂id
	 */
	public void setTReagent(String tReagent) {
		this.tReagent = tReagent;
	}
}