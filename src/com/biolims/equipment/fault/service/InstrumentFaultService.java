/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：Service
 * 类描述：故障报告管理service
 * 创建人：倪毅
 * 创建时间：2011-9
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.equipment.fault.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.SystemCodeService;
import com.biolims.dic.model.DicState;
import com.biolims.dic.model.DicType;
import com.biolims.equipment.common.constants.SystemCode;
import com.biolims.equipment.fault.dao.InstrumentFaultDao;
import com.biolims.equipment.model.Instrument;
import com.biolims.equipment.model.InstrumentFault;
import com.biolims.equipment.model.InstrumentFaultDetail;
import com.biolims.equipment.model.InstrumentRepair;
import com.biolims.equipment.model.InstrumentRepairTask;
import com.biolims.equipment.model.InstrumentRepairTemp;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;
import com.biolims.workflow.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

import oracle.net.aso.u;

@Service
@SuppressWarnings("unchecked")
public class InstrumentFaultService {

	@Resource
	private CommonDAO commonDAO;

	@Resource
	private InstrumentFaultDao instrumentFaultDao;
	@Resource
	private SystemCodeService systemCodeService;

	/**
	 * 检索实验,采用map方式传递检索参数
	 * 
	 * @return 参数值MAP，如果传递的为null返回空字符串
	 */
	public Map<String, Object> findInstrumentFaultList(int startNum, int limitNum, String dir, String sort,
			String contextPath, String data, String rightsId, String curUserId) throws Exception {
		// 检索条件map,每个字段检索值
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		// 检索方式map,每个字段的检索方式
		Map<String, String> mapForCondition = new HashMap<String, String>();
		// 检索方式map,每个字段的检索方式
		if (data != null && !data.equals("")) {
			BeanUtils.data2Map(data, mapForQuery, mapForCondition);
			mapForCondition.put("id", "like");
			mapForCondition.put("note", "like");
		}

		// 设置组织机构管理范围Map
		Map<String, String> mapForManageScope = new HashMap<String, String>();

		// 生成管理范围map
		mapForManageScope = BeanUtils.configueDepartmentMap("department", "createUser.id", rightsId, curUserId);

		mapForQuery.put("mapForManageScope", mapForManageScope);
		// 将map值传入，获取列表
		Map<String, Object> controlMap = commonDAO.findObjectCondition(startNum, limitNum, dir, sort,
				InstrumentFault.class, mapForQuery, mapForCondition);
		List<InstrumentFault> list = (List<InstrumentFault>) controlMap.get("list");
		controlMap.put("list", list);
		return controlMap;
	}

	/**
	 * 保存
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void save(InstrumentFault an) throws Exception {
		commonDAO.saveOrUpdate(an);
	}

	public InstrumentFault getInstrumentFault(String id) throws Exception {
		InstrumentFault animal = commonDAO.get(InstrumentFault.class, id);
		return animal;
	}

	public Map<String, Object> findInstrumentFaultDetailList(int startNum, int limitNum, String dir, String sort,
			String contextPath, String instrumentFault_id) throws Exception {
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		Map<String, String> mapForCondition = new HashMap<String, String>();
		if (instrumentFault_id != null && !instrumentFault_id.equals("")) {
			mapForQuery.put("instrumentFault.id", instrumentFault_id);
			mapForCondition.put("instrumentFault.id", "=");
		}

		Map<String, Object> controlMap = commonDAO.findObjectCondition(startNum, limitNum, dir, sort,
				InstrumentFaultDetail.class, mapForQuery, mapForCondition);

		List<InstrumentFaultDetail> list = (List<InstrumentFaultDetail>) controlMap.get("list");
		controlMap.put("list", list);
		return controlMap;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void saveInstrumentFaultDetail(String id, String data) throws Exception {
		InstrumentFault epf = commonDAO.get(InstrumentFault.class, id);
		// 将json读入map
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(data, List.class);
		for (Map<String, Object> map : list) {
			InstrumentFaultDetail emf = new InstrumentFaultDetail();
			// Instrument ins=new Instrument();
			// 将map信息读入实体类
			emf = (InstrumentFaultDetail) commonDAO.Map2Bean(map, emf);
			// InstrumentRepair ep = new InstrumentRepair();
			// em.setNote(em.getNote().replace("<br>", "\\n\\r"));
			// ep.setId(id);
			emf.setInstrumentFault(epf);
			commonDAO.saveOrUpdate(emf);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<InstrumentFaultDetail> findInstrumentFaultDetail(String id) throws Exception {
		return instrumentFaultDao.findInstrumentFaultDetail(id);
	}

	/**
	 * 
	 * 删除明细,由ext ajax调用
	 * 
	 * @param id 认证ID
	 * @return void
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delInstrumentFaultDetail(String[] ids) {
		for (String id : ids) {
//			InstrumentFaultDetail ppi = new InstrumentFaultDetail();
			InstrumentFaultDetail ppi = commonDAO.get(InstrumentFaultDetail.class, id);
			ppi.setId(id);
			commonDAO.delete(ppi);

			if (ids.length > 0) {
				LogInfo li = new LogInfo();
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setLogDate(new Date());
				li.setFileId(ppi.getInstrumentFault().getId());
				li.setClassName("InstrumentFault");
				li.setModifyContent("设备维修:" + "编号:" + ppi.getInstrument().getId() + "设备名称:"
						+ ppi.getInstrument().getName() + "的数据已删除");
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void instrumentFaultSetInstrument(String formId) throws Exception {
		List<InstrumentFaultDetail> list = instrumentFaultDao.findInstrumentFaultDetail(formId);
		InstrumentFault in = new InstrumentFault();
		in = commonDAO.get(InstrumentFault.class, formId);
		in.setState(WorkflowConstants.WORKFLOW_COMPLETE);
		in.setStateName(WorkflowConstants.WORKFLOW_COMPLETE_NAME);
		commonDAO.saveOrUpdate(in);
		DicState ds = new DicState();
		for (InstrumentFaultDetail ifd : list) {
			InstrumentRepairTemp irt = new InstrumentRepairTemp();
			Instrument ins = commonDAO.get(Instrument.class, ifd.getInstrument().getId());
			ds.setId("0r");
			ins.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			ins.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			ins.setState(ds);
			irt.setInstrument(ins);
			irt.setState("3");
			irt.setNote(ifd.getNote());
			commonDAO.saveOrUpdate(ins);
			commonDAO.saveOrUpdate(irt);
		}
	}

	public Map<String, Object> findInstrumentFaultTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return instrumentFaultDao.findInstrumentFaultTable(start, length, query, col, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveInstrumentFaultDetail(InstrumentFault sc, String itemDataJson, String logInfo) throws Exception {
		List<InstrumentFaultDetail> saveItems = new ArrayList<InstrumentFaultDetail>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			InstrumentFaultDetail scp = new InstrumentFaultDetail();

			// 将map信息读入实体类
			scp = (InstrumentFaultDetail) instrumentFaultDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setInstrumentFault(sc);

			saveItems.add(scp);
		}
		instrumentFaultDao.saveOrUpdateAll(saveItems);
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("InstrumentFault");
			li.setModifyContent(logInfo);
			li.setState("3");
			li.setStateName("数据修改");
			commonDAO.saveOrUpdate(li);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(InstrumentFault sc, Map jsonMap, String logInfo, Map logMap,String log) throws Exception {
		if (sc != null) {
			sc.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			sc.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			instrumentFaultDao.saveOrUpdate(sc);

			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(sc.getId());
				li.setClassName("InstrumentFault");
				li.setModifyContent(logInfo);
				if("123".equals(log)) {
					li.setState("1");
					li.setStateName("数据新增");
				}else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				commonDAO.saveOrUpdate(li);
			}

			String jsonStr = "";
			String logStr = "";
			logStr = (String) logMap.get("instrumentFaultDetail");
			jsonStr = (String) jsonMap.get("instrumentFaultDetail");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveInstrumentFaultDetail(sc, jsonStr, logStr);
			}
		}
	}

	public Map<String, Object> findInstrumentFaultDetailTable(Integer start, Integer length, String query, String col,
			String sort, String id) throws Exception {
		Map<String, Object> result = instrumentFaultDao.findInstrumentFaultDetailTable(start, length, query, col, sort,
				id);
		List<InstrumentFaultDetail> list = (List<InstrumentFaultDetail>) result.get("list");
		return result;
	}

	public Map<String, Object> findInstrumentTable(Integer start, Integer length, String query, String col, String sort,
			String id) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> result = instrumentFaultDao.findInstrumentTable(start, length, query, col, sort);
		List<Instrument> list = (List<Instrument>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String addInstrument(String[] ids, String note, String instrId, String createUser, String createDate,
			String state, String typeId) throws Exception {

		// 保存主表信息
		InstrumentFault si = new InstrumentFault();
		String log = "";
		if (instrId == null || instrId.length() <= 0 || "NEW".equals(instrId)) {
			log = "123";
			String code = systemCodeService.getSystemCode(SystemCode.INSTRUMENT_FAULT_NAME,
					SystemCode.INSTRUMENT_FAULT_CODE, SystemCode.INSTRUMENT_FAULT_CODE_PREFIX,
					SystemCode.INSTRUMENT_FAULT_CODE_LENGTH);
			si.setId(code);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			DicType type = commonDAO.get(DicType.class, typeId);
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			si.setHandleUser(u);
			si.setCreateUser(u);
			si.setType(type);
			si.setCreateDate(sdf.parse(createDate));
			si.setState("3");
			si.setStateName("NEW");
			si.setNote(note);
			instrId = si.getId();
			si.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			si.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			commonDAO.saveOrUpdate(si);
		} else {
			si = commonDAO.get(InstrumentFault.class, instrId);
		}
		for (int i = 0; i < ids.length; i++) {
			String id = ids[i];
			List<InstrumentFaultDetail> list = instrumentFaultDao.findInstrumentFault(id,instrId);
			if(list.size()>0) {
				return null;
			}else {
				// 通过id查询库存主数据
				Instrument s = commonDAO.get(Instrument.class, id);
				InstrumentFaultDetail sii = new InstrumentFaultDetail();
				if (null != s) {
					sii.setInstrument(s);
				}
				sii.setInstrumentFault(si);
				commonDAO.saveOrUpdate(sii);

				String kucun = "设备维修:" + "设备编号:" + s.getId() + "设备名称:" + s.getName() + "的数据已添加到明细";
				if (kucun != null && !"".equals(kucun)) {
					LogInfo li = new LogInfo();
					User u = (User) ServletActionContext.getRequest().getSession()
							.getAttribute(SystemConstants.USER_SESSION_KEY);
					li.setLogDate(new Date());
					li.setFileId(si.getId());
					li.setClassName("InstrumentFault");
					li.setUserId(u.getId());
					li.setModifyContent(kucun);
					if("123".equals(log)) {
						li.setState("1");
						li.setStateName("数据新增");
					}else {
						li.setState("3");
						li.setStateName("数据修改");
					}
					commonDAO.saveOrUpdate(li);
				}
			}
		}
		return instrId;

	}

}
