package com.biolims.equipment.fault.dao;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.common.dao.CommonDAO;
import com.biolims.equipment.model.Instrument;
import com.biolims.equipment.model.InstrumentFault;
import com.biolims.equipment.model.InstrumentFaultDetail;
import com.biolims.equipment.model.InstrumentRepairTemp;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class InstrumentFaultDao extends CommonDAO {
 public List<InstrumentFaultDetail> findInstrumentFaultDetail(String id) throws Exception{
	 String sql="from InstrumentFaultDetail where 1=1 and instrumentFault.id='"+id+"'";
	 String key="";
	 List<InstrumentFaultDetail> list=new ArrayList<InstrumentFaultDetail>();
	 list=this.getSession().createQuery(sql+key).list();
	return list;
	 
 }


	public Map<String, Object> findInstrumentFaultTable(Integer start,Integer length, String query, String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from  InstrumentFault where 1=1";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from InstrumentFault where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<InstrumentFault> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}


	public Map<String, Object> findInstrumentFaultDetailTable(Integer start, Integer length, String query, String col, String sort, String id) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from InstrumentFaultDetail where 1=1 and instrumentFault.id='"+id+"'";
		String key = "";

		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from InstrumentFaultDetail where 1=1 and instrumentFault.id='"+id+"' ";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<InstrumentFaultDetail> list = new ArrayList<InstrumentFaultDetail>();
			list=this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}


	public Map<String, Object> findInstrumentTable(Integer start, Integer length, String query, String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from Instrument where 1=1 ";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from Instrument where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			
			List<Instrument> list = new ArrayList<Instrument>();
			list=this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}


	public List<InstrumentFaultDetail> findInstrumentFault(String idd, String inId) {
		 String hql="from InstrumentFaultDetail where 1=1 and instrument.id='"+idd+"'and instrumentFault='"+inId+"' ";
		 List<InstrumentFaultDetail> list= this.getSession().createQuery(hql).list();
		return list;
	}

}
