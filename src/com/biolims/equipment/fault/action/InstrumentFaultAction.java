/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：Action
 * 类描述：故障管理
 * 创建人：倪毅
 * 创建时间：2011-12
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.equipment.fault.action;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.common.service.SystemCodeService;
import com.biolims.equipment.common.constants.SystemCode;
import com.biolims.equipment.common.constants.SystemConstants;
import com.biolims.equipment.fault.service.InstrumentFaultService;
import com.biolims.equipment.model.Instrument;
import com.biolims.equipment.model.InstrumentFault;
import com.biolims.equipment.model.InstrumentFaultDetail;
import com.biolims.equipment.model.InstrumentRepairTask;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/equipment/fault")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public class InstrumentFaultAction extends BaseActionSupport {

	private static final long serialVersionUID = -164661537469123056L;

	//用于页面上显示模块名称
	private String title = "设备维修申请";

	//该action权限id
	private String rightsId = "4021";

	private InstrumentFault instrumentFault = new InstrumentFault();

	@Autowired
	private InstrumentFaultService instrumentFaultService;
	@Resource
	private CommonService commonService;

	@Resource
	private SystemCodeService systemCodeService;
	@Resource
	private FieldService  fieldService;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CommonDAO commonDAO;
	private String log="";
/*NEW	*/
	@Action(value = "showInstrumentFaultList")
	public String showInstrumentFaultList() throws Exception {
		rightsId = "4021";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/equipment/fault/instrumentFault.jsp");
	}

	@Action(value = "showInstrumentFaultEditList")
	public String showInstrumentFaultEditList() throws Exception {
		rightsId = "4021";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/equipment/fault/instrumentFaultEditList.jsp");
	}
	
	
	@Action(value = "showInstrumentFaultTableJson")
	public void showInstrumentFaultTableJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {	
			Map<String, Object> result = instrumentFaultService.findInstrumentFaultTable(
					start, length, query, col, sort);
			Long count = (Long) result.get("total");
			List<InstrumentFault> list = (List<InstrumentFault>) result.get("list");
	
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("note", "");
			map.put("type-id", "");
			map.put("type-name", "");
			map.put("stateName", "");
			map.put("handleUser-id", "");
			map.put("handleUser-name", "");
			map.put("handleDate", "yyyy-MM-dd");
			String data=new SendData().getDateJsonForDatatable(map, list);
			//根据模块查询自定义字段数据
			Map<String,Object> mapField = fieldService.findFieldByModuleValue("InstrumentFault");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result,dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "instrumentFaultSelectTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogInstrumentFaultList() throws Exception {
		rightsId = "4021";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/equipment/fault/instrumentFaultSelectTable.jsp");
	}
	@Action(value = "editInstrumentFault")
	public String editInstrumentFault() throws Exception {
		rightsId = "4021";
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			instrumentFault = instrumentFaultService.getInstrumentFault(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "instrumentFault");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			instrumentFault.setCreateUser(user);
			instrumentFault.setConfirmDate(new Date());
			instrumentFault.setHandleUser(user);
			instrumentFault.setHandleDate(new Date());
			instrumentFault.setCreateDate(new Date());
			instrumentFault.setState(SystemConstants.DIC_STATE_NEW);
			instrumentFault.setStateName(SystemConstants.DIC_STATE_NEW_NAME);
	/*		instrumentFault.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			instrumentFault.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));*/
		
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(instrumentFault.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/equipment/fault/instrumentFaultEdit.jsp");
	}
	
	@Action(value = "copyInstrumentFault")
	public String copyInstrumentFault() throws Exception {
		rightsId = "4021";
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		instrumentFault = instrumentFaultService.getInstrumentFault(id);
		instrumentFault.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/equipment/fault/instrumentFaultEdit.jsp");
	}
	@Action(value = "save")
	public void save() throws Exception {
		
		
		String msg = "";
		String zId = "";
		boolean bool = true;	
		
		try {
			String dataValue = getParameterFromRequest("dataValue");
			String str = "["+dataValue+"]";
			String changeLog = getParameterFromRequest("changeLog");
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					str, List.class);
			
			for (Map<String, Object> map : list) {
				instrumentFault = (InstrumentFault)commonDAO.Map2Bean(map, instrumentFault);
			}
			String id = instrumentFault.getId();
			if(id!=null&&id.equals("")){
				instrumentFault.setId(null);
			}
			
			if (id == null || id.length() <= 0 || "NEW".equals(id)) {
				log="123";
				String code = systemCodeService.getSystemCode(SystemCode.INSTRUMENT_FAULT_NAME,
						SystemCode.INSTRUMENT_FAULT_CODE, SystemCode.INSTRUMENT_FAULT_CODE_PREFIX,
						SystemCode.INSTRUMENT_FAULT_CODE_LENGTH);
				instrumentFault.setId(code);
				instrumentFault.setCreateDate(new Date());
				User user = (User) this
						.getObjFromSession(SystemConstants.USER_SESSION_KEY);
				instrumentFault.setHandleUser(user);
			}
			Map aMap = new HashMap();
			Map lMap = new HashMap();
				aMap.put("instrumentFaultDetail",getParameterFromRequest("instrumentFaultDetailJson"));
			
			
			instrumentFaultService.save(instrumentFault,aMap,changeLog,lMap,log);
			
			zId = instrumentFault.getId();
		} catch (Exception e) {
			e.printStackTrace();
			msg = "保存失败！";
			bool = false;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("msg", msg);
		map.put("success", bool);
		map.put("id", zId);
	
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null")) {
			map.put("bpmTaskId", bpmTaskId);
			
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	@Action(value = "viewInstrumentFault")
	public String toViewInstrumentFault() throws Exception {
		rightsId = "4021";
		String id = getParameterFromRequest("id");
		instrumentFault = instrumentFaultService.getInstrumentFault(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/equipment/fault/instrumentFaultEdit.jsp");
	}
	@Action(value = "showInstrumentFaultDetailTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showInstrumentFaultDetailTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = instrumentFaultService
				.findInstrumentFaultDetailTable(start, length, query, col, sort,
						id);
		List<InstrumentFaultDetail> list=(List<InstrumentFaultDetail>) result.get("list");
		Map<String, String> map=new HashMap<String, String>();
		map.put("id", "");
		map.put("instrument-id", "");
		map.put("instrument-name", "");
		map.put("instrument-effectiveDate", "yyyy-MM-dd");
		map.put("instrument-expiryDate", "yyyy-MM-dd");
		map.put("instrument-nextDate", "yyyy-MM-dd");
		map.put("instrument-spec", "");
		map.put("instrument-serialNumber", "");
		map.put("instrument-producer-name", "");
		map.put("instrument-state-name", "");
		map.put("note", "");
		String data=new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw,result,data));
	}
	/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delInstrumentFaultDetail")
	public void delInstrumentFaultDetail() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			instrumentFaultService.delInstrumentFaultDetail(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	@Action(value = "saveInstrumentFaultDetailTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveInstrumentFaultDetailTable() throws Exception {
		Map aMap = new HashMap();
		Map lMap = new HashMap();
		
		String id=getParameterFromRequest("id");
		String logInfo = getParameterFromRequest("logInfo");
		instrumentFault=commonService.get(InstrumentFault.class, id);
		Map<String, Object> map=new HashMap<String, Object>();
		aMap.put("instrumentFaultDetail",
				getParameterFromRequest("dataJson"));
		lMap.put("instrumentFaultDetail",
				getParameterFromRequest("changeLog"));
		try {
			instrumentFaultService.save(instrumentFault, aMap,logInfo,lMap,log);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}



	@Action(value = "saveInstrumentFaultTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveInstrumentFaultTable() throws Exception {
		String dataValue = getParameterFromRequest("dataJson");
		String changeLog = getParameterFromRequest("changeLog");
		
		String str = "["+dataValue+"]";
			
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					str, List.class);
			
		Map<String, Object> map=new HashMap<String, Object>();
		try {
			
		for (Map<String, Object> map1 : list) {
		
			InstrumentFault a = new InstrumentFault();
		
			Map aMap = new HashMap();
			Map lMap = new HashMap();
			a = (InstrumentFault)commonDAO.Map2Bean(map1, a);
		
			instrumentFaultService.save(a,aMap,changeLog,lMap,log);
			map.put("id", a.getId());
		}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	@Action(value = "showInstrumentTempTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showInstrumentRepairTaskTableJson() throws Exception {
		
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = instrumentFaultService.findInstrumentTable(start, length, query, col, sort, id);
		List<Instrument> list=(List<Instrument>) result.get("list");
		Map<String, String> map=new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("position-id", "");
		map.put("dutyUser-name", "");
		map.put("state-name", "");
		map.put("spec", "");
		map.put("rank", "");
		map.put("type-id", "");
		map.put("type-name", "");
		map.put("dutyUserTwo-name", "");
		map.put("serialNumber", "");
		map.put("enquiryOddNumber", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("outCode", "");
		map.put("accuracy", "");
		map.put("purchaseDate", "yyyy-MM-dd");
		map.put("surveyScope", "");
		map.put("checkItem", "");
		map.put("producer-name", "");
		map.put("producer-linkMan", "");
		map.put("producer-linkTel", "");
		map.put("supplier-name", "");
		map.put("supplier-linkMan", "");
		map.put("supplier-linkTel", "");
		map.put("studyType-name", "");
		map.put("effectiveDate", "yyyy-MM-dd");
		map.put("expiryDate", "yyyy-MM-dd");
		map.put("nextDate", "yyyy-MM-dd");
		String data=new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw,result,data));
	}
	/**
	 * 
	 * @Title: addInstrument
	 * @Description: 添加设备到报修表
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "addInstrument")
	public void addInstrumentRepairTask() throws Exception {
		String note = getParameterFromRequest("note");
		String instrId = getParameterFromRequest("id");
		String createDate = getParameterFromRequest("createDate");
		String createUser = getParameterFromRequest("createUser");
		String state = getParameterFromRequest("state");
		String typeId = getParameterFromRequest("typeId");
		
		
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String instrumentRepairId= instrumentFaultService.addInstrument(ids, note, instrId, createUser, createDate, state, typeId);
			if(instrumentRepairId!=null) {
				result.put("success", true);
				result.put("data", instrumentRepairId);
			}else {
				result.put("success", true);
				result.put("zy", "1");
			}
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
/*	Old*/
	/**
	 * 访问列表
	 */

/*	@Action(value = "showInstrumentFaultList")
	public String showInstrumentFaultList() throws Exception {
//		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
//		map.put("id", new String[] { "", "string", "", "申请编码 ", "150", "true", "", "", "", "", "", "" });
//		map.put("note", new String[] { "", "string", "", "描述", "250", "true", "", "", "", "", "", "" });
//		map.put("storage-id", new String[] { "", "string", "", "设备编号", "150", "true", "", "", "", "", "", "" });
//		map.put("type-name", new String[] { "", "string", "", "类型", "150", "true", "", "", "", "", "", "" });
//		map.put("handleUser-name", new String[] { "", "string", "", "报告人", "120", "true", "", "", "", "", "", "" });
//		map.put("storage-note", new String[] { "", "string", "", "设备名称", "150", "true", "", "", "", "", "", "" });
//		map.put("storage-spec", new String[] { "", "string", "", "规格型号", "150", "true", "", "", "", "", "", "" });
//		map.put("stateName", new String[] { "", "string", "", "工作流状态", "120", "true", "", "", "", "", "", "" });
//		map.put("handleDate", new String[] { "", "string", "", "报告日期", "150", "true", "true", "", "", "", "", "" });
//		String type = generalexttype(map);
//		String col = generalextcol(map);
//		putObjToContext("type", type);
//		putObjToContext("col", col);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		putObjToContext("path", ServletActionContext.getRequest().getContextPath()
				+ "/equipment/fault/showInstrumentFaultListJson.action");
		return dispatcher("/WEB-INF/page/equipment/fault/showInstrumentFaultList.jsp");
	}*/

	@Action(value = "showInstrumentFaultListJson")
	public void showInstrumentFaultListJson() throws Exception {
		//开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		//limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		//字段
		String dir = getParameterFromRequest("dir");
		//排序方式
		String sort = getParameterFromRequest("sort");
		//取出检索用的对象
		String data = getParameterFromRequest("data");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		Map<String, Object> controlMap = instrumentFaultService.findInstrumentFaultList(startNum, limitNum, dir, sort,
				getContextPath(), data, rightsId, user.getId());
		long totalCount = Long.parseLong(controlMap.get("totalCount").toString());
		List<InstrumentFault> list = (List<InstrumentFault>) controlMap.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("note", "");
		map.put("type-name", "");
		map.put("stateName", "");
		map.put("handleUser-name", "");
		map.put("handleDate", "yyyy-MM-dd");
		new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}
	
	@Action(value = "showInstrumentFaultDetailListJson")
	public void showInstrumentFaultListDetailJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String instrumentFaultId = getParameterFromRequest("instrumentFaultId");
		List<InstrumentFaultDetail> list = null;
		long totalCount = 0;
		putObjToContext("instrumentFaultId", instrumentFaultId);

		if (instrumentFaultId != null && !instrumentFaultId.equals("")) {
			Map<String, Object> controlMap = instrumentFaultService.findInstrumentFaultDetailList(startNum, limitNum,
					dir, sort, getContextPath(), instrumentFaultId);
			totalCount = Long.parseLong(controlMap.get("totalCount").toString());
			list = (List<InstrumentFaultDetail>) controlMap.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("instrument-id", "");
			map.put("instrument-name", "");
			map.put("instrument-effectiveDate", "yyyy-MM-dd");
			map.put("instrument-expiryDate", "yyyy-MM-dd");
			map.put("instrument-nextDate", "yyyy-MM-dd");
			map.put("instrument-spec", "");
			map.put("instrument-serialNumber", "");
			map.put("instrument-producer-name", "");
			map.put("instrument-state-name", "");
			map.put("note", "");

			new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
		}
	}

	/**
	 * 访问编辑页面
	 */

	@Action(value = "toEditInstrumentFault")
	public String toEditInstrumentFault() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = "";
		if (id != null && !id.equals("")) {
			instrumentFault = instrumentFaultService.getInstrumentFault(id);
			putObjToContext("instrumentFaultId", id);
			if (instrumentFault.getState().equals(SystemConstants.DIC_STATE_YES))
				handlemethod = SystemConstants.PAGE_HANDLE_METHOD_VIEW;
			else
				handlemethod = SystemConstants.PAGE_HANDLE_METHOD_MODIFY;
			toToolBar(rightsId, "", "", handlemethod);
			toState(instrumentFault.getState());
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			instrumentFault.setCreateUser(user);
			instrumentFault.setHandleUser(user);
			instrumentFault.setCreateDate(new Date());
			instrumentFault.setHandleDate(new Date());
			instrumentFault.setId(SystemCode.DEFAULT_SYSTEMCODE);
			instrumentFault.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			instrumentFault.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toState(instrumentFault.getState());
		}
		return dispatcher("/WEB-INF/page/equipment/fault/editInstrumentFault.jsp");
	}

	/**
	 * 复制页面
	 */

	@Action(value = "toCopyInstrumentFault")
	public String toCopyInstrumentFault() throws Exception {
		String id = getParameterFromRequest("id");
		instrumentFault = instrumentFaultService.getInstrumentFault(id);
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		instrumentFault.setId(SystemCode.DEFAULT_SYSTEMCODE);
		instrumentFault.setCreateUser(user);
		instrumentFault.setHandleUser(user);
		instrumentFault.setCreateDate(new Date());
		instrumentFault.setHandleDate(new Date());
		instrumentFault.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
		instrumentFault.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
		toSetStateCopy();
		toState(instrumentFault.getState());
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		return dispatcher("/WEB-INF/page/equipment/fault/editInstrumentFault.jsp");
	}

	/**
	 * 访问查看页面
	 */

	/*@Action(value = "toViewInstrumentFault")
	public String toViewInstrumentFault() throws Exception {
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
			instrumentFault = instrumentFaultService.getInstrumentFault(id);
			putObjToContext("instrumentFaultId", id);
		}
		return dispatcher("/WEB-INF/page/equipment/fault/editInstrumentFault.jsp");
	}*/

	/**
	 * 保存
	 */
	/*@Action(value = "save")
	public String save() throws Exception {
		String data = getParameterFromRequest("jsonDataStr");
//		String data1 = getParameterFromRequest("jsonDataStr1");
//		String data2 = getParameterFromRequest("jsonDataStr2");
		try {
			String id = this.instrumentFault.getId();
			if (id == null || id.length() <= 0 || SystemCode.DEFAULT_SYSTEMCODE.equals(id)) {
				String code = systemCodeService.getSystemCode(SystemCode.INSTRUMENT_FAULT_NAME,
						SystemCode.INSTRUMENT_FAULT_CODE, SystemCode.INSTRUMENT_FAULT_CODE_PREFIX,
						SystemCode.INSTRUMENT_FAULT_CODE_LENGTH);
				this.instrumentFault.setId(code);
			}

			instrumentFaultService.save(instrumentFault);
			if (data != null && !data.equals(""))
				instrumentFaultService.saveInstrumentFaultDetail(instrumentFault.getId(), data);
//			if (data1 != null && !data1.equals(""))
//				saveInstrumentRepairTaskStorage(data1);
//			if (data2 != null && !data2.equals(""))
//				saveInstrumentRepairTaskPerson(data2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return redirect("/equipment/fault/toEditInstrumentFault.action?id=" + instrumentFault.getId());
	}*/
	/**
	 * 保存明细
	 */
	@Action(value = "saveInstrumentFaultDetail")
	public void saveInstrumentFaultDetail() throws Exception {
		String instrumentFaultId = getParameterFromRequest("instrumentFaultId");
		String json = getParameterFromRequest("data");
		instrumentFaultService.saveInstrumentFaultDetail(instrumentFaultId, json);
	
	}
	/**
	 * 删除明细
	 */
	/*@Action(value = "delInstrumentFaultDetail")
	public void delInstrumentRepairTask() throws Exception {
		String[] ids = getRequest().getParameterValues("ids[]");
		instrumentFaultService.delInstrumentFaultDetail(ids);
	}*/
	
	@Action(value = "instrumentFaultSelect")
	public String instrumentFaultSelect() throws Exception {
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		map.put("id", new String[] { "", "string", "", "故障单编码", "150", "true", "", "", "", "", "", "" });
		map.put("note", new String[] { "", "string", "", "描述", "250", "true", "", "", "", "", "", "" });
		map.put("type-name", new String[] { "", "string", "", "故障类型", "150", "true", "", "", "", "", "", "" });
		map.put("handleUser-name", new String[] { "", "string", "", "报告人", "120", "true", "", "", "", "", "", "" });
		map.put("stateName", new String[] { "", "string", "", "工作流状态", "120", "true", "", "", "", "", "", "" });
		map.put("id", new String[] { "", "string", "", "故障单编码", "150", "true", "", "", "", "", "", "" });
		map.put("note", new String[] { "", "string", "", "描述", "250", "true", "", "", "", "", "", "" });
		map.put("stateName", new String[] { "", "string", "", "工作流状态", "120", "true", "", "", "", "", "", "" });
		map.put("handleDate", new String[] { "", "string", "", "报告日期", "150", "true", "true", "", "", "", "", "" });
		String type = generalexttype(map);
		String col = generalextcol(map);
		putObjToContext("type", type);
		putObjToContext("col", col);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		putObjToContext("path", ServletActionContext.getRequest().getContextPath()
				+ "/equipment/fault/instrumentFaultSelectJson.action");
		return dispatcher("/WEB-INF/page/equipment/fault/instrumentRepairFaultSelect.jsp");
	}

	@Action(value = "instrumentFaultSelectJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void instrumentFaultSelectJson() throws Exception {
		//开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		//limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		//字段
		String dir = getParameterFromRequest("dir");
		//排序方式
		String sort = getParameterFromRequest("sort");
		//取出检索用的对象
		String data = getParameterFromRequest("data");

		Map<String, Object> controlMap = instrumentFaultService.findInstrumentFaultList(startNum, limitNum, dir, sort,
				getContextPath(), data, "", "");
		long totalCount = Long.parseLong(controlMap.get("totalCount").toString());
		List<InstrumentFault> list = (List<InstrumentFault>) controlMap.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("note", "");
		map.put("type-name", "");
		map.put("state", "");
		map.put("handleUser-name", "");
		new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public InstrumentFault getInstrumentFault() {
		return instrumentFault;
	}

	public void setInstrumentFault(InstrumentFault instrumentFault) {
		this.instrumentFault = instrumentFault;
	}

}
