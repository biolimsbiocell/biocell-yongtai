package com.biolims.equipment.repair.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.common.dao.CommonDAO;
import com.biolims.equipment.model.InstrumentRepair;
import com.biolims.equipment.model.InstrumentRepairTask;
import com.biolims.equipment.model.InstrumentRepairTemp;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class InstrumentRepairDao extends CommonDAO {

	public Map<String, Object> selectInstrumentRepair(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort,String p_type) throws Exception {
		String hql = " from InstrumentRepair where 1=1 and p_type='"+p_type+"'";
		String key = "";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery("select count(*)" + hql + key).uniqueResult();
		List<InstrumentRepair> list = new ArrayList<InstrumentRepair>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum != null && limitNum != null) {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			} else {
				list = this.getSession().createQuery(hql + key).list();
			}

		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("total", total);
		map.put("list", list);
		return map;
	}

	public Map<String, Object> selectInstrumentRepairLeftList(
			Map<String, String> map2Query, Integer startNum, Integer limitNum,
			String dir, String sort) {
		String key = " ";
		String hql = " from InstrumentRepairTemp where 1=1 and state='3'";
		if (map2Query != null)
			key = map2where(map2Query);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<InstrumentRepairTemp> list = new ArrayList<InstrumentRepairTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	
	public List<InstrumentRepairTask> findInstrumentRepairTask(String id){
		String hql="from InstrumentRepairTask where 1=1 and instrumentRepair.id='"+id+"'";
		String key="";
		List<InstrumentRepairTask> list=new ArrayList<InstrumentRepairTask>();
		list=this.getSession().createQuery(hql+key).list();
		return list;
		
	}

	public Map<String, Object> findInstrumentRepairTable(Integer start,Integer length, String query, String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from  InstrumentRepair where 1=1";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from InstrumentRepair where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<InstrumentRepair> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> findInstrumentRepairTaskTable(Integer start, Integer length, String query, String col, String sort, String id) throws Exception {
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from InstrumentRepairTask where 1=1 and instrumentRepair.id='"+id+"'";
			String key = "";

			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
				String hql = "from InstrumentRepairTask where 1=1 and instrumentRepair.id='"+id+"' ";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					col=col.replace("-", ".");
					key+=" order by "+col+" "+sort;
				}
				List<InstrumentRepairTask> list = new ArrayList<InstrumentRepairTask>();
				list=this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
			}
			return map;
		}

	public Map<String, Object> findInstrumentRepairTempTable(Integer start, Integer length, String query, String col, String sort) throws Exception {
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from InstrumentRepairTemp where 1=1 and state='3'";
			String key = "";
			if(query!=null){
				key=map2Where(query);
			}
			String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
			if(!"all".equals(scopeId)){
				key+=" and instrument.scopeId='"+scopeId+"'";
			}
			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
				String hql = "from InstrumentRepairTemp where 1=1 and state='3' ";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					col=col.replace("-", ".");
					key+=" order by "+col+" "+sort;
				}
				List<InstrumentRepairTemp> list = new ArrayList<InstrumentRepairTemp>();
				list=this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
			}
			return map;
		}
}
