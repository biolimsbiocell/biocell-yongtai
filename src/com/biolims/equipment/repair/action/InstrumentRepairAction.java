/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：Action
 * 类描述：维护管理
 * 创建人：倪毅
 * 创建时间：2012-01
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.equipment.repair.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.common.service.SystemCodeService;
import com.biolims.equipment.common.constants.SystemCode;
import com.biolims.equipment.common.constants.SystemConstants;
import com.biolims.equipment.main.service.InstrumentService;
import com.biolims.equipment.model.Instrument;
import com.biolims.equipment.model.InstrumentRepair;
import com.biolims.equipment.model.InstrumentRepairPlanTask;
import com.biolims.equipment.model.InstrumentRepairTask;
import com.biolims.equipment.model.InstrumentRepairTaskPerson;
import com.biolims.equipment.model.InstrumentRepairTaskStorage;
import com.biolims.equipment.model.InstrumentRepairTemp;
import com.biolims.equipment.plan.service.InstrumentRepairPlanService;
import com.biolims.equipment.repair.service.InstrumentRepairService;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

import sun.util.logging.resources.logging;

@Namespace("/equipment/repair")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public class InstrumentRepairAction extends BaseActionSupport {

	private static final long serialVersionUID = -164661537469123056L;

	// 该action权限id
	private String rightsId = "404";

	private InstrumentRepair instrumentRepair = new InstrumentRepair();

	@Autowired
	private InstrumentRepairService instrumentRepairService;

	@Autowired
	private InstrumentRepairPlanService instrumentRepairPlanService;

	@Resource
	private SystemCodeService systemCodeService;

	@Autowired
	private InstrumentService instrumentService;
	@Resource
	private FieldService fieldService;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CommonService commonService;
	private String log = "";
	/* NEW */
	@Action(value = "showInstrumentRepairListNew")
	public String showInstrumentRepairList() throws Exception {
		rightsId = "404";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/equipment/repair/instrumentRepair.jsp");
	}

	@Action(value = "showInstrumentRepairEditListNew")
	public String showInstrumentRepairEditList() throws Exception {
		rightsId = "404";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/equipment/repair/instrumentRepairEditList.jsp");
	}

	@Action(value = "showInstrumentRepairTableJsonNew")
	public void showInstrumentRepairTableJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = instrumentRepairService.findInstrumentRepairTable(start, length, query, col,
					sort);
			Long count = (Long) result.get("total");
			List<InstrumentRepair> list = (List<InstrumentRepair>) result.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("note", "");
			map.put("startDate", "yyyy-MM-dd");
			map.put("endDate", "yyyy-MM-dd");
			map.put("factEndDate", "yyyy-MM-dd");
			map.put("name", "");
			map.put("createDate", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("verify", "");
			map.put("factStartDate", "yyyy-MM-dd");
			map.put("type-id", "");
			map.put("type-name", "");
			map.put("stateName", "");
			map.put("instrumentRepairPlan-id", "");
			map.put("instrumentRepairPlan-name", "");

			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("InstrumentRepair");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "instrumentRepairSelectTableNew", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogInstrumentRepairList() throws Exception {
		rightsId = "404";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/equipment/repair/instrumentRepairSelectTable.jsp");
	}

	@Action(value = "editInstrumentRepairNew")
	public String editInstrumentRepair() throws Exception {
		rightsId = "404";
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			instrumentRepair = instrumentRepairService.getInstrumentRepair(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "instrumentRepair");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			instrumentRepair.setCreateUser(user);
			instrumentRepair.setCreateDate(new Date());
			instrumentRepair.setState(SystemConstants.DIC_STATE_NEW);
			instrumentRepair.setStateName(SystemConstants.DIC_STATE_NEW_NAME);

			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/equipment/repair/instrumentRepairEdit.jsp");
	}

	@Action(value = "copyInstrumentRepairNew")
	public String copyInstrumentRepair() throws Exception {
		rightsId = "404";
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		instrumentRepair = instrumentRepairService.getInstrumentRepair(id);
		instrumentRepair.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/equipment/repair/instrumentRepairEdit.jsp");
	}

	@Action(value = "saveNew")
	public void save() throws Exception {
		String msg = "";
		String zId = "";
		boolean bool = true;

		try {
			String dataValue = getParameterFromRequest("dataValue");
			String str = "[" + dataValue + "]";
			String changeLog = getParameterFromRequest("changeLog");
			String changeLogItem = getParameterFromRequest("changeLogItem");
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str, List.class);

			for (Map<String, Object> map : list) {
				instrumentRepair = (InstrumentRepair) commonDAO.Map2Bean(map, instrumentRepair);
			}
			String id = instrumentRepair.getId();
			if (id != null && id.equals("")) {
				instrumentRepair.setId(null);
			}
			if (id == null || id.length() <= 0 || SystemCode.DEFAULT_SYSTEMCODE.equals(id)) {
				log = "123";
				String code = systemCodeService.getSystemCode(SystemCode.INSTRUMENT_REPAIR_NAME,
						SystemCode.INSTRUMENT_REPAIR_CODE, SystemCode.INSTRUMENT_REPAIR_CODE_PREFIX,
						SystemCode.INSTRUMENT_REPAIR_CODE_LENGTH);
				instrumentRepair.setId(code);
				instrumentRepair.setCreateDate(new Date());
				User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);

				this.instrumentRepair.setId(SystemCode.DEFAULT_SYSTEMCODE);
				instrumentRepair.setCreateUser(user);
			}
			Map aMap = new HashMap();
			Map lMap = new HashMap();
			aMap.put("instrumentRepairTask", getParameterFromRequest("instrumentRepairTaskJson"));

			aMap.put("instrumentRepairTemp", getParameterFromRequest("instrumentRepairTempJson"));

			instrumentRepairService.save(instrumentRepair, aMap, changeLog, lMap, changeLogItem,log);

			zId = instrumentRepair.getId();
		} catch (Exception e) {
			e.printStackTrace();
			msg = "保存失败！";
			bool = false;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("msg", msg);
		map.put("success", bool);
		map.put("id", zId);

		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		if (bpmTaskId != null && !bpmTaskId.equals("") && !bpmTaskId.equals("null")) {
			map.put("bpmTaskId", bpmTaskId);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "viewInstrumentRepairNew")
	public String toViewInstrumentRepair() throws Exception {
		rightsId = "404";
		String id = getParameterFromRequest("id");
		instrumentRepair = instrumentRepairService.getInstrumentRepair(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/equipment/repair/instrumentRepairEdit.jsp");
	}

	@Action(value = "showInstrumentRepairTaskTableJsonNew", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showInstrumentRepairTaskTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = instrumentRepairService.findInstrumentRepairTaskTable(start, length, query, col,
				sort, id);
		List<InstrumentRepairTask> list = (List<InstrumentRepairTask>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("tempId", "");
		map.put("instrument-id", "");
		map.put("instrument-name", "");
		map.put("instrument-effectiveDate", "yyyy-MM-dd");
		map.put("instrument-expiryDate", "yyyy-MM-dd");
		map.put("instrument-nextDate", "yyyy-MM-dd");
		map.put("instrument-spec", "");
		map.put("instrument-serialNumber", "");
		map.put("instrument-producer-name", "");
		map.put("instrument-state-name", "");
		map.put("reason", "");
		map.put("note", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delInstrumentRepairTaskNew")
	public void delInstrumentRepairTask() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			instrumentRepairService.delInstrumentRepairTask(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "saveInstrumentRepairTaskTableNew", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveInstrumentRepairTaskTable() throws Exception {
		Map aMap = new HashMap();
		Map lMap = new HashMap();

		String id = getParameterFromRequest("id");
		String logInfo = getParameterFromRequest("logInfo");
		instrumentRepair = commonService.get(InstrumentRepair.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		aMap.put("instrumentRepairTask", getParameterFromRequest("dataJson"));
		lMap.put("instrumentRepairTask", getParameterFromRequest("changeLog"));
		try {
			instrumentRepairService.save(instrumentRepair, aMap, "", lMap, logInfo,log);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showInstrumentRepairTempTableJsonNew", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showInstrumentRepairTempTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = instrumentRepairService.findInstrumentRepairTempTable(start, length, query, col,
				sort);
		List<InstrumentRepairTemp> list = (List<InstrumentRepairTemp>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("instrument-id", "");
		map.put("instrument-name", "");
		map.put("instrument-spec", "");
		map.put("instrument-serialNumber", "");
		map.put("instrument-producer-name", "");
		map.put("instrument-state-name", "");
		map.put("note", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delInstrumentRepairTempNew")
	public void delInstrumentRepairTemp() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			instrumentRepairService.delInstrumentRepairTemp(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "saveInstrumentRepairTempTableNew", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveInstrumentRepairTempTable() throws Exception {
		Map aMap = new HashMap();
		Map lMap = new HashMap();
		String id = getParameterFromRequest("id");
		instrumentRepair = commonService.get(InstrumentRepair.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		aMap.put("instrumentRepairTemp", getParameterFromRequest("dataJson"));
		lMap.put("instrumentRepairTemp", getParameterFromRequest("changeLog"));
		try {
			instrumentRepairService.save(instrumentRepair, aMap, "", lMap, "",log);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "saveInstrumentRepairTableNew", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveInstrumentRepairTable() throws Exception {
		String dataValue = getParameterFromRequest("dataJson");
		String changeLog = getParameterFromRequest("changeLog");

		String str = "[" + dataValue + "]";

		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str, List.class);

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			for (Map<String, Object> map1 : list) {

				InstrumentRepair a = new InstrumentRepair();

				Map aMap = new HashMap();
				Map lMap = new HashMap();
				a = (InstrumentRepair) commonDAO.Map2Bean(map1, a);

				instrumentRepairService.save(a, aMap, changeLog, lMap, changeLog,log);
				map.put("id", a.getId());
			}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: addInstrumentRepairTask @Description: 添加数据到维修明细表 @author
	 * : @date @throws Exception void @throws
	 */
	@Action(value = "addInstrumentRepairTask")
	public void addInstrumentRepairTask() throws Exception {
		String name = getParameterFromRequest("name");
		String factEndDate = getParameterFromRequest("factEndDate");
		String verify = getParameterFromRequest("verify");
		String instrId = getParameterFromRequest("id");
		String createDate = getParameterFromRequest("createDate");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		String createUser = getParameterFromRequest("createUser");

		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String instrumentRepairId = instrumentRepairService.addInstrumentRepairTask(ids, name, factEndDate, verify,
					instrId, createUser, createDate);
			result.put("success", true);
			result.put("data", instrumentRepairId);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/* Old */
	/**
	 * 访问列表
	 */

	@Action(value = "showInstrumentRepairList")
	public String showExperimentList() throws Exception {
		// 用于接收通用查询的参数
		String p_type = getParameterFromRequest("p_type");
		putObjToContext("p_type", p_type);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		putObjToContext("path",
				ServletActionContext.getRequest().getContextPath()
						+ "/equipment/repair/showInstrumentRepairListJson.action?queryMethod="
						+ getParameterFromRequest("queryMethod"));
		return dispatcher("/WEB-INF/page/equipment/repair/showInstrumentRepairList.jsp");
	}

	@Action(value = "showInstrumentRepairListJson")
	public void showInstrumentRepairListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		// 区分维护类型
		String p_type = getParameterFromRequest("p_type");
		// 取出检索用的对象
		String data = getParameterFromRequest("data");
		String queryMethod = getParameterFromRequest("queryMethod");
		Map r = this.toMakeSessionQuery(data, "InstrumentRepair", dir, sort, queryMethod);
		dir = (String) r.get("dir");
		sort = (String) r.get("sort");
		data = (String) r.get("queryData");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		Map<String, Object> controlMap = instrumentRepairService.selectInstrumentRepair(startNum, limitNum, dir, sort,
				getContextPath(), data, rightsId, user.getId(), p_type);
		long totalCount = Long.parseLong(controlMap.get("total").toString());
		List<InstrumentRepair> list = (List<InstrumentRepair>) controlMap.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("note", "");
		map.put("type-name", "");
		map.put("stateName", "");
		map.put("verify", "");
		map.put("instrumentRepairPlan-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("startDate", "yyyy-MM-dd");
		map.put("endDate", "yyyy-MM-dd");
		map.put("factStartDate", "yyyy-MM-dd");
		map.put("factEndDate", "yyyy-MM-dd");
		new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}

	/**
	 * 访问编辑页面
	 */

	@Action(value = "toEditInstrumentRepair")
	public String toEditInstrumentRepair() throws Exception {
		String id = getParameterFromRequest("id");
		String p_type = getParameterFromRequest("p_type");
		if (id != null && !id.equals("")) {
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			instrumentRepair = instrumentRepairService.getInstrumentRepair(id);
			putObjToContext("p_type", p_type);
			putObjToContext("instrumentRepairId", id);
			toState(instrumentRepair.getState());
			if (instrumentRepair.getState().equals(SystemConstants.DIC_STATE_YES))
				putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
			else
				putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);

//			showInstrumentRepairTaskList(handlemethod, id);
//			showInstrumentRepairTaskPersonList(handlemethod, id, "");
//			showInstrumentRepairTaskStorageList(handlemethod, id, "");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);

			this.instrumentRepair.setId(SystemCode.DEFAULT_SYSTEMCODE);
			instrumentRepair.setCreateUser(user);

			instrumentRepair.setCreateDate(new Date());
			instrumentRepair.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);

			instrumentRepair.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);

			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toState(instrumentRepair.getState());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			putObjToContext("p_type", p_type);
//			showInstrumentRepairTaskList(
//					SystemConstants.PAGE_HANDLE_METHOD_ADD, id);
//			showInstrumentRepairTaskPersonList(
//					SystemConstants.PAGE_HANDLE_METHOD_ADD, id, "");
//			showInstrumentRepairTaskStorageList(
//					SystemConstants.PAGE_HANDLE_METHOD_ADD, id, "");
		}
		return dispatcher("/WEB-INF/page/equipment/repair/editInstrumentRepair.jsp");
	}

	/**
	 * 复制页面
	 */

	@Action(value = "toCopyInstrumentRepair")
	public String toCopyInstrumentRepair() throws Exception {
		String id = getParameterFromRequest("id");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		instrumentRepair = instrumentRepairService.getInstrumentRepair(id);
		instrumentRepair.setId(SystemCode.DEFAULT_SYSTEMCODE);
		instrumentRepair.setCreateUser(user);
		instrumentRepair.setCreateDate(new Date());
		instrumentRepair.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
		instrumentRepair.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
//		showInstrumentRepairTaskList(SystemConstants.PAGE_HANDLE_METHOD_MODIFY,
//				id);
//		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
//		toState(instrumentRepair.getState());
//		toSetStateCopy();
//		showInstrumentRepairTaskList(SystemConstants.PAGE_HANDLE_METHOD_ADD, id);
		return dispatcher("/WEB-INF/page/equipment/repair/editInstrumentRepair.jsp");
	}

	public void showInstrumentRepairTaskPersonList(String handlemethod, String instrumentRepairId,
			String instrumentRepairTaskId) throws Exception {

		String exttype = "";
		String extcol = "";
		String editflag = "true";
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		map.put("workType-id",
				new String[] { "", "string", "", "工种ID", "180", "true", "false", "", "", editflag, "", "workType" });
		map.put("workType-name", new String[] { "", "string", "", "工种", "180", "true", "false", "", "", "", "", "" });
		map.put("id", new String[] { "", "string", "", "id", "150", "true", "true", "", "", "", "", "" });
		map.put("user-id",
				new String[] { "", "string", "", "人员编号", "80", "true", "false", "", "", editflag, "", "user" });
		map.put("user-name", new String[] { "", "string", "", "姓名", "150", "true", "false", "", "", "", "", "" });
		map.put("workTime",
				new String[] { "", "string", "", "工时", "80", "true", "false", "", "", editflag, "", "workTime" });
		map.put("scale", new String[] { "", "string", "", "费率", "80", "true", "false", "", "", editflag, "", "scale" });
		map.put("instrumentRepairTask-id",
				new String[] { "", "string", "", "任务编号", "150", "true", "false", "", "", "", "", "" });
		String statement = "";
		String statementLisener = "";
		exttype = generalexttype(map);
		extcol = generalextcol(map);
		putObjToContext("type2", exttype);
		putObjToContext("col2", extcol);
		putObjToContext("path2",
				ServletActionContext.getRequest().getContextPath()
						+ "/equipment/repair/showInstrumentRepairTaskPersonListJson.action?instrumentRepairTaskId="
						+ instrumentRepairTaskId + "&instrumentRepairId=" + instrumentRepairId);
		putObjToContext("statement", statement);
		putObjToContext("statementLisener", statementLisener);
		putObjToContext("handlemethod", handlemethod);

	}

	public void showInstrumentRepairTaskStorageList(String handlemethod, String instrumentRepairId,
			String instrumentRepairTaskId) throws Exception {
		String exttype = "";
		String extcol = "";
		String editflag = "true";
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		map.put("id", new String[] { "", "string", "", "id", "150", "true", "true", "", "", "", "", "" });
		map.put("storage-id",
				new String[] { "", "string", "", "组件编号", "120", "true", "false", "", "", editflag, "", "storage" });
		map.put("storage-name",
				new String[] { "", "string", "", "对象名称", "150", "true", "false", "", "", editflag, "", "storage" });
		map.put("price", new String[] { "", "string", "", "价格", "80", "true", "false", "", "", editflag, "", "price" });
		map.put("num", new String[] { "", "string", "", "数量", "80", "true", "false", "", "", editflag, "", "num" });
		map.put("factNum",
				new String[] { "", "string", "", "实际数量", "80", "true", "false", "", "", editflag, "", "factNum" });
		map.put("instrumentRepairTask-id", new String[] { "", "string", "", "任务编号", "160", "true", "false", "", "",
				editflag, "", "instrumentRepairTask" });
		String statement = "";
		String statementLisener = "";
		exttype = generalexttype(map);
		extcol = generalextcol(map);
		putObjToContext("type1", exttype);
		putObjToContext("col1", extcol);
		putObjToContext("path1",
				ServletActionContext.getRequest().getContextPath()
						+ "/equipment/repair/showInstrumentRepairTaskStorageListJson.action?instrumentRepairTaskId="
						+ instrumentRepairTaskId + "&instrumentRepairId=" + instrumentRepairId);

		putObjToContext("statement", statement);
		putObjToContext("statementLisener", statementLisener);

		putObjToContext("handlemethod", handlemethod);

	}

	/**
	 * 访问查看页面
	 */

	/*
	 * @Action(value = "toViewInstrumentRepair") public String
	 * toViewInstrumentRepair() throws Exception { String id =
	 * getParameterFromRequest("id"); String p_type =
	 * getParameterFromRequest("p_type"); if (id != null && !id.equals("")) {
	 * putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
	 * instrumentRepair = instrumentRepairService.getInstrumentRepair(id);
	 * putObjToContext("instrumentRepairId", id); putObjToContext("p_type", p_type);
	 * }
	 * 
	 * return dispatcher("/WEB-INF/page/equipment/repair/editInstrumentRepair.jsp");
	 * }
	 */

	/**
	 * 保存
	 */
	/*
	 * @Action(value = "save") public String save() throws Exception { String data =
	 * getParameterFromRequest("jsonDataStr"); String data1 =
	 * getParameterFromRequest("jsonDataStr1"); String data2 =
	 * getParameterFromRequest("jsonDataStr2"); String p_type =
	 * getParameterFromRequest("p_type"); String id = this.instrumentRepair.getId();
	 * if (id == null || id.length() <= 0 ||
	 * SystemCode.DEFAULT_SYSTEMCODE.equals(id)) { String code =
	 * systemCodeService.getSystemCode( SystemCode.INSTRUMENT_REPAIR_NAME,
	 * SystemCode.INSTRUMENT_REPAIR_CODE, SystemCode.INSTRUMENT_REPAIR_CODE_PREFIX,
	 * SystemCode.INSTRUMENT_REPAIR_CODE_LENGTH); this.instrumentRepair.setId(code);
	 * } instrumentRepair.setP_type(p_type);
	 * instrumentRepairService.save(instrumentRepair); if (data != null &&
	 * !data.equals("")) saveInstrumentRepairTask(instrumentRepair.getId(), data);
	 * if (data1 != null && !data1.equals(""))
	 * saveInstrumentRepairTaskStorage(data1); if (data2 != null &&
	 * !data2.equals("")) saveInstrumentRepairTaskPerson(data2); return
	 * redirect("/equipment/repair/toEditInstrumentRepair.action?id=" +
	 * instrumentRepair.getId() + "&p_type=" + p_type); }
	 */
	@Action(value = "showInstrument", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showInstrument() throws Exception {
		String gid = getParameterFromRequest("gid");
		putObjToContext("gid", gid);
		return dispatcher("/WEB-INF/page/equipment/repair/showInstrument.jsp");
	}

	@Action(value = "showInstrumentJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showInstrumentJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");

		String data = getParameterFromRequest("data");

		String queryMethod = getParameterFromRequest("queryMethod");
		Map r = this.toMakeSessionQuery(data, "Instrument", dir, sort, queryMethod);
		dir = (String) r.get("dir");
		sort = (String) r.get("sort");
		data = (String) r.get("queryData");
		Map<String, Object> result = null;

		try {
			result = this.instrumentService.selectInstrumentList(startNum, limitNum, dir, sort, data, null);
		} catch (Exception e) {
			e.printStackTrace();
		}

		List<Instrument> list = (List<Instrument>) result.get("list");
		Long count = (Long) result.get("total");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("position-id", "");
		map.put("dutyUser-name", "");
		map.put("state-name", "");
		map.put("spec", "");
		map.put("rank", "");
		map.put("type-id", "");
		map.put("type-name", "");
		map.put("dutyUserTwo-name", "");
		map.put("serialNumber", "");
		map.put("enquiryOddNumber", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("outCode", "");
		map.put("accuracy", "");
		map.put("purchaseDate", "yyyy-MM-dd");
		map.put("surveyScope", "");
		map.put("checkItem", "");
		map.put("producer-name", "");
		map.put("producer-linkMan", "");
		map.put("producer-linkTel", "");
		map.put("supplier-name", "");
		map.put("supplier-linkMan", "");
		map.put("supplier-linkTel", "");
		map.put("studyType-name", "");
		map.put("effectiveDate", "yyyy-MM-dd");
		map.put("expiryDate", "yyyy-MM-dd");
		map.put("nextDate", "yyyy-MM-dd");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	public void saveInstrumentRepairTask(String instrumentRepairId, String json) throws Exception {

		instrumentRepairService.saveInstrumentRepairTask(instrumentRepairId, json);

	}

	public void saveInstrumentRepairTaskPerson(String json) throws Exception {

		instrumentRepairService.saveInstrumentRepairTaskPerson(json);

	}

	public void saveInstrumentRepairTaskStorage(String json) throws Exception {

		instrumentRepairService.saveInstrumentRepairTaskStorage(json);

	}

	/**
	 * 访问页面
	 */
	@Action(value = "showInstrumentRepairTaskList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showInstrumentRepairTaskList() throws Exception {
		return dispatcher("/WEB-INF/page/equipment/repair/instrumentRepairItem.jsp");

	}

	@Action(value = "showInstrumentRepairTaskListJson")
	public void showInstrumentRepairTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String instrumentRepairId = getParameterFromRequest("instrumentRepairId");
		List<InstrumentRepairTask> list = null;
		long totalCount = 0;
		putObjToContext("instrumentRepairId", instrumentRepairId);

		if (instrumentRepairId != null && !instrumentRepairId.equals("")) {
			Map<String, Object> controlMap = instrumentRepairService.findInstrumentRepairTaskList(startNum, limitNum,
					dir, sort, getContextPath(), instrumentRepairId);
			totalCount = Long.parseLong(controlMap.get("totalCount").toString());
			list = (List<InstrumentRepairTask>) controlMap.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("tempId", "");
			map.put("instrument-id", "");
			map.put("instrument-name", "");
			map.put("instrument-effectiveDate", "yyyy-MM-dd");
			map.put("instrument-expiryDate", "yyyy-MM-dd");
			map.put("instrument-nextDate", "yyyy-MM-dd");
			map.put("instrument-spec", "");
			map.put("instrument-serialNumber", "");
			map.put("instrument-producer-name", "");
			map.put("instrument-state-name", "");
			map.put("reason", "");
			map.put("note", "");
			new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
		}
	}

	/**
	 * 保存明细
	 */
	@Action(value = "saveInstrumentRepairTask")
	public void saveInstrumentRepairTask() throws Exception {
		String instrumentRepairId = getParameterFromRequest("instrumentRepairId");
		String json = getParameterFromRequest("data");
		instrumentRepairService.saveInstrumentRepairTask(instrumentRepairId, json);

	}

	/**
	 * 删除明细
	 */
	/*
	 * @Action(value = "delInstrumentRepairTask") public void
	 * delInstrumentRepairTask() throws Exception { String[] ids =
	 * getRequest().getParameterValues("ids[]");
	 * instrumentRepairService.delInstrumentRepairTask(ids); }
	 */

	@Action(value = "showInstrumentRepairTaskPersonListJson")
	public void showInstrumentRepairTaskPersonListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String instrumentRepairId = getParameterFromRequest("instrumentRepairId");
		String instrumentRepairTaskId = getParameterFromRequest("instrumentRepairTaskId");
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		List<InstrumentRepairTaskPerson> list = null;
		long totalCount = 0;
		if (instrumentRepairId != null && !instrumentRepairId.equals("")) {
			Map<String, Object> controlMap = instrumentRepairService.findInstrumentRepairTaskPersonList(startNum,
					limitNum, dir, sort, getContextPath(), instrumentRepairId, instrumentRepairTaskId);
			totalCount = Long.parseLong(controlMap.get("totalCount").toString());
			list = (List<InstrumentRepairTaskPerson>) controlMap.get("list");
		}
		Map<String, String> map = new HashMap<String, String>();
		map.put("workType-name", "");
		map.put("id", "");
		map.put("user-id", "");
		map.put("user-name", "");
		map.put("workTime", "");
		map.put("scale", "");
		map.put("instrumentRepairTask-id", "");
		if (list != null)
			new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}

	/**
	 * 保存明细
	 */
	@Action(value = "saveInstrumentRepairTaskPerson")
	public void saveInstrumentRepairTaskPerson() throws Exception {
		String json = getParameterFromRequest("data");
		instrumentRepairService.saveInstrumentRepairTaskPerson(json);

	}

	/**
	 * 删除明细
	 */
	@Action(value = "delInstrumentRepairTaskPerson")
	public void delInstrumentRepairTaskPerson() throws Exception {
		String id = getParameterFromRequest("id");
		instrumentRepairService.delInstrumentRepairTaskPerson(id);
	}

	@Action(value = "showInstrumentRepairTaskStorageListJson")
	public void showProjectFactItemListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String instrumentRepairId = getParameterFromRequest("instrumentRepairId");
		String instrumentRepairTaskId = getParameterFromRequest("instrumentRepairTaskId");
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		List<InstrumentRepairTaskStorage> list = null;
		long totalCount = 0;
		if (instrumentRepairId != null && !instrumentRepairId.equals("")) {
			Map<String, Object> controlMap = instrumentRepairService.findInstrumentRepairTaskStorageList(startNum,
					limitNum, dir, sort, getContextPath(), instrumentRepairId, instrumentRepairTaskId);
			totalCount = Long.parseLong(controlMap.get("totalCount").toString());
			list = (List<InstrumentRepairTaskStorage>) controlMap.get("list");
		}
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("storage-id", "");
		map.put("storage-name", "");
		map.put("price", "");
		map.put("num", "");
		map.put("factNum", "");
		map.put("instrumentRepairTask-id", "");
		if (list != null)
			new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}

	/**
	 * 保存明细
	 */
	@Action(value = "saveInstrumentRepairTaskStorage")
	public void saveProjectPlanItem() throws Exception {
		// String instrumentRepairTaskId =
		// getParameterFromRequest("instrumentRepairTaskId");
		String json = getParameterFromRequest("data");
		instrumentRepairService.saveInstrumentRepairTaskStorage(json);

	}

	/**
	 * 删除明细
	 */
	@Action(value = "delInstrumentRepairTaskStorage")
	public void delInstrumentRepairTaskStorage() throws Exception {
		String id = getParameterFromRequest("id");
		instrumentRepairService.delInstrumentRepairTaskStorage(id);
	}

	@Action(value = "showInstrumentRepairPlanTaskListJson")
	public void showInstrumentRepairPlanTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");

		String instrumentRepairPlanId = getParameterFromRequest("instrumentRepairPlanId");
		List<InstrumentRepairPlanTask> list = null;
		long totalCount = 0;
		putObjToContext("instrumentRepairPlanId", instrumentRepairPlanId);

		if (instrumentRepairPlanId != null && !instrumentRepairPlanId.equals("")) {
			Map<String, Object> controlMap = instrumentRepairPlanService.findInstrumentRepairPlanTaskList(startNum,
					limitNum, dir, sort, getContextPath(), instrumentRepairPlanId);
			totalCount = Long.parseLong(controlMap.get("totalCount").toString());
			list = (List<InstrumentRepairPlanTask>) controlMap.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("type-id", "");
			map.put("type-name", "");
			map.put("note", "");
			new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
		}
	}

	/**
	 * 左侧表
	 */
	@Action(value = "showInstrumentRepairFromFaultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showInstrumentReapriFromFaultList() throws Exception {
		return dispatcher("/WEB-INF/page/equipment/repair/instrumentRepairTemp.jsp");
	}

	@Action(value = "showInstrumentRepairFromFaultListJson")
	public void showInstrumentReapriFromFaultListJson() throws Exception {

		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");

		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = instrumentRepairService.selectInstrumentRepairTemp(map2Query, startNum,
					limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<InstrumentRepairTemp> list = (List<InstrumentRepairTemp>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("instrument-id", "");
			map.put("instrument-name", "");
			map.put("instrument-spec", "");
			map.put("instrument-serialNumber", "");
			map.put("instrument-producer-name", "");
			map.put("instrument-supplier-name", "");
			map.put("instrument-state-name", "");
			map.put("note", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public InstrumentRepair getInstrumentRepair() {
		return instrumentRepair;
	}

	public void setInstrumentRepair(InstrumentRepair instrumentRepair) {
		this.instrumentRepair = instrumentRepair;
	}
}
