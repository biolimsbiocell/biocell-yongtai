/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：InstrumentRepairService
 * 类描述：设备维护管理service
 * 创建人：倪毅
 * 创建时间：2012-01
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.equipment.repair.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.applicationType.service.ApplicationTypeService;
import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.common.service.SystemCodeService;
import com.biolims.dic.model.DicState;
import com.biolims.equipment.common.constants.SystemCode;
import com.biolims.equipment.model.Instrument;
import com.biolims.equipment.model.InstrumentRepair;
import com.biolims.equipment.model.InstrumentRepairPlan;
import com.biolims.equipment.model.InstrumentRepairTask;
import com.biolims.equipment.model.InstrumentRepairTaskPerson;
import com.biolims.equipment.model.InstrumentRepairTaskStorage;
import com.biolims.equipment.model.InstrumentRepairTemp;
import com.biolims.equipment.plan.service.InstrumentRepairPlanService;
import com.biolims.equipment.repair.dao.InstrumentRepairDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.storage.model.Storage;
import com.biolims.storage.model.StorageIn;
import com.biolims.storage.model.StorageInItem;
import com.biolims.util.BeanUtils;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.opensymphony.xwork2.ActionContext;

@Service
@SuppressWarnings("unchecked")
public class InstrumentRepairService extends ApplicationTypeService {

	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CommonService commonService;
	@Resource
	private InstrumentRepairPlanService instrumentRepairPlanService;

	@Resource
	private InstrumentRepairDao instrumentRepairDao;
	@Resource
	private SystemCodeService systemCodeService;

	/**
	 * 
	 * 检索实验,采用map方式传递检索参数
	 * 
	 * 
	 * @return 参数值MAP，如果传递的为null返回空字符串
	 */
	public Map<String, Object> findInstrumentRepairList(int startNum, int limitNum, String dir, String sort,
			String contextPath, String data, String rightsId, String curUserId) throws Exception {
		// 检索条件map,每个字段检索值
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		// 检索方式map,每个字段的检索方式
		Map<String, String> mapForCondition = new HashMap<String, String>();
		if (data != null && !data.equals("")) {
			BeanUtils.data2Map(data, mapForQuery, mapForCondition);
			mapForQuery.remove("queryStartDate");
			mapForQuery.remove("queryEndDate");
			mapForCondition.remove("queryStartDate");
			mapForCondition.remove("queryEndDate");
		}

		// 设置组织机构管理范围Map
		Map<String, String> mapForManageScope = new HashMap<String, String>();

		// 生成管理范围map
		mapForManageScope = BeanUtils.configueDepartmentMap("department", "createUser.id", rightsId, curUserId);

		mapForQuery.put("mapForManageScope", mapForManageScope);
		// 将map值传入，获取列表
		Map<String, Object> controlMap = commonDAO.findObjectCondition(startNum, limitNum, dir, sort,
				InstrumentRepair.class, mapForQuery, mapForCondition);
		List<InstrumentRepair> list = (List<InstrumentRepair>) controlMap.get("list");
		controlMap.put("list", list);
		return controlMap;
	}

	public Map<String, Object> selectInstrumentRepair(int startNum, int limitNum, String dir, String sort,
			String contextPath, String data, String rightsId, String curUserId, String p_type) throws Exception {
		// 检索条件map,每个字段检索值
		Map<String, String> map2Query = new HashMap<String, String>();
		// 检索方式map,每个字段的检索方式
		if (data != null && !data.equals("")) {
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		}
		// 将map值传入，获取列表
		Map<String, Object> controlMap = instrumentRepairDao.selectInstrumentRepair(map2Query, startNum, limitNum, dir,
				sort, p_type);
		List<InstrumentRepair> list = (List<InstrumentRepair>) controlMap.get("list");
		controlMap.put("list", list);
		return controlMap;
	}

	/**
	 * 保存
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void save(InstrumentRepair an) throws Exception {
		commonDAO.saveOrUpdate(an);
	}

	public InstrumentRepair getInstrumentRepair(String id) throws Exception {
		InstrumentRepair animal = commonDAO.get(InstrumentRepair.class, id);
		return animal;
	}

	// 检索明细
	public Map<String, Object> findInstrumentRepairTaskList(int startNum, int limitNum, String dir, String sort,
			String contextPath, String animalId) throws Exception {
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		Map<String, String> mapForCondition = new HashMap<String, String>();
		if (animalId != null && !animalId.equals("")) {
			mapForQuery.put("instrumentRepair.id", animalId);
			mapForCondition.put("instrumentRepair.id", "=");
		}

		Map<String, Object> controlMap = commonDAO.findObjectCondition(startNum, limitNum, dir, sort,
				InstrumentRepairTask.class, mapForQuery, mapForCondition);

		List<InstrumentRepairTask> list = (List<InstrumentRepairTask>) controlMap.get("list");
		controlMap.put("list", list);
		return controlMap;
	}

	/**
	 * 
	 * 维护明细信息,由ext ajax调用
	 * 
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveInstrumentRepairTask(String id, String jsonString) throws Exception {
		InstrumentRepair ep = commonDAO.get(InstrumentRepair.class, id);
		// 将json读入map
		Double fee = 0.0;
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(jsonString, List.class);
		for (Map<String, Object> map : list) {
			InstrumentRepairTask em = new InstrumentRepairTask();
			// Instrument ins=new Instrument();
			// 将map信息读入实体类
			em = (InstrumentRepairTask) commonDAO.Map2Bean(map, em);
			// em.getInstrument();
			// InstrumentRepair ep = new InstrumentRepair();
			// em.setNote(em.getNote().replace("<br>", "\\n\\r"));
			// ep.setId(id);
			em.setInstrumentRepair(ep);
			commonDAO.saveOrUpdate(em);
		}

		// List<Object> a = commonDAO
		// .find("select sum(irt.fee) from InstrumentRepairTask irt where
		// irt.instrumentRepair.id=?",
		// id);
		// Double b = (Double) a.get(0);
		// ep.setFee(b);

		commonDAO.saveOrUpdate(ep);

	}

	/**
	 * 
	 * 删除明细,由ext ajax调用
	 * 
	 * @param id
	 *            认证ID
	 * @return void
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delInstrumentRepairTask(String[] ids) {
		for (String id : ids) {
//			InstrumentRepairTask ppi = new InstrumentRepairTask();
			InstrumentRepairTask ppi = commonDAO.get(InstrumentRepairTask.class, id);
			ppi.setId(id);
			commonDAO.delete(ppi);
			
			if (ids.length>0) {
				LogInfo li = new LogInfo();
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setClassName("InstrumentRepair");
				li.setLogDate(new Date());
				li.setFileId(ppi.getInstrumentRepair().getId());
				li.setModifyContent("设备维修:"+"编号:"+ppi.getInstrument().getId()+"设备名称:"+ppi.getInstrument().getName()+"的数据已删除");
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}
		}
	}

	// 检索用户明细
	public Map<String, Object> findInstrumentRepairTaskPersonList(int startNum, int limitNum, String dir, String sort,
			String contextPath, String instrumentRepairId, String instrumentRepairTaskId) throws Exception {
		// String hql = "from UserCert ";
		// String where = "where user.id='" + userId + "'";
		// Map<String,Object> controlMap = userDAO.findObject(hql+where,
		// startNum,
		// limitNum, dir, sort);
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		Map<String, String> mapForCondition = new HashMap<String, String>();
		if (instrumentRepairId != null && !instrumentRepairId.equals("")) {
			mapForQuery.put("instrumentRepairTask", "alias");
			mapForCondition.put("instrumentRepairTask", "");
			mapForQuery.put("instrumentRepairTask.instrumentRepair.id", instrumentRepairId);
			mapForCondition.put("instrumentRepairTask.instrumentRepair.id", "=");
		}
		if (instrumentRepairTaskId != null && !instrumentRepairTaskId.equals("")) {
			mapForQuery.put("instrumentRepairTask.id", instrumentRepairTaskId);
			mapForCondition.put("instrumentRepairTask.id", "=");
		}
		Map<String, Object> controlMap = commonDAO.findObjectCondition(startNum, limitNum, dir, sort,
				InstrumentRepairTaskPerson.class, mapForQuery, mapForCondition);

		List<InstrumentRepairTaskPerson> list = (List<InstrumentRepairTaskPerson>) controlMap.get("list");
		controlMap.put("list", list);
		return controlMap;
	}

	/**
	 * 维护明细信息,由ext ajax调用
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveInstrumentRepairTaskPerson(String jsonString) throws Exception {

		// 将json读入map
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(jsonString, List.class);
		for (Map<String, Object> map : list) {
			InstrumentRepairTaskPerson em = new InstrumentRepairTaskPerson();
			// 将map信息读入实体类
			em = (InstrumentRepairTaskPerson) commonDAO.Map2Bean(map, em);
			commonDAO.saveOrUpdate(em);
		}
	}

	/**
	 * 删除明细,由ext ajax调用
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delInstrumentRepairTaskPerson(String id) {
		InstrumentRepairTaskPerson ppi = new InstrumentRepairTaskPerson();
		ppi.setId(id);
		commonDAO.delete(ppi);
	}

	// 检索用户明细
	public Map<String, Object> findInstrumentRepairTaskStorageList(int startNum, int limitNum, String dir, String sort,
			String contextPath, String instrumentRepairId, String instrumentRepairTaskId) throws Exception {

		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		Map<String, String> mapForCondition = new HashMap<String, String>();

		if (instrumentRepairId != null && !instrumentRepairId.equals("")) {
			mapForQuery.put("instrumentRepairTask", "alias");
			mapForCondition.put("instrumentRepairTask", "");
			mapForQuery.put("instrumentRepairTask.instrumentRepair.id", instrumentRepairId);
			mapForCondition.put("instrumentRepairTask.instrumentRepair.id", "=");
		}

		if (instrumentRepairTaskId != null && !instrumentRepairTaskId.equals("")) {
			mapForQuery.put("instrumentRepairTask.id", instrumentRepairTaskId);
			mapForCondition.put("instrumentRepairTask.id", "=");
		}
		Map<String, Object> controlMap = commonDAO.findObjectCondition(startNum, limitNum, dir, sort,
				InstrumentRepairTaskStorage.class, mapForQuery, mapForCondition);

		List<InstrumentRepairTaskStorage> list = (List<InstrumentRepairTaskStorage>) controlMap.get("list");
		controlMap.put("list", list);
		return controlMap;
	}

	/**
	 * 维护明细信息,由ext ajax调用
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveInstrumentRepairTaskStorage(String jsonString) throws Exception {

		// 将json读入map
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(jsonString, List.class);
		for (Map<String, Object> map : list) {
			InstrumentRepairTaskStorage em = new InstrumentRepairTaskStorage();

			// 将map信息读入实体类
			em = (InstrumentRepairTaskStorage) commonDAO.Map2Bean(map, em);

			commonDAO.saveOrUpdate(em);
		}
	}

	/**
	 * 删除明细,由ext ajax调用
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delInstrumentRepairTaskStorage(String id) {
		InstrumentRepairTaskStorage ppi = new InstrumentRepairTaskStorage();
		ppi.setId(id);
		commonDAO.delete(ppi);
	}

	/**
	 * 设置维护计划
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void instrumentRepairSetInstrumentRepairPlan(String formId) throws Exception {
		InstrumentRepair ib = commonService.get(InstrumentRepair.class, formId);

		InstrumentRepairPlan irp = null;
		if ((ib.getInstrumentRepairPlan() != null) && (ib.getType().getId().equals("yfxwh"))) {
			irp = ib.getInstrumentRepairPlan();

			if (ib.getFactStartDate() != null)
				irp.setEffectiveDate(ib.getFactStartDate());
			else
				irp.setEffectiveDate(new Date());
			Date aDate = irp.getEffectiveDate();
			Date aDates = irp.getEffectiveDate();
			String typeId = irp.getUnit().getId();
			Integer period = irp.getPeriod();

			if (typeId.equals("DAY")) {// 天数

				aDate = DateUtil.addDay(aDate, period);
			}
			if (typeId.equals("WEEK")) {// 周数

				aDate = DateUtil.addDay(aDate, period / 7);
			}
			if (typeId.equals("MONTH")) {// 月数
				aDate = DateUtil.addMonth(aDate, period);
			}
			if (typeId.equals("DAY")) {// 天数

				aDates = DateUtil.addDay(aDates, period / 2);
			}
			if (typeId.equals("WEEK")) {// 周数

				aDates = DateUtil.addDay(aDates, (period / 7) / 2);
			}
			if (typeId.equals("MONTH")) {// 月数
				aDates = DateUtil.addMonth(aDates, period / 2);
			}

			aDate = DateUtil.addDay(aDate, -1);

			irp.setNextDate(aDate);

			irp.setNextDates(aDates);

			Date aDate1 = DateUtil.addMonth(aDate, -2);

			irp.setTsDate(aDate1);

			commonService.saveOrUpdate(irp);

		}

	}

	public Map<String, Object> selectInstrumentRepairTemp(Map<String, String> map2Query, int startNum, int limitNum,
			String dir, String sort) {
		return instrumentRepairDao.selectInstrumentRepairLeftList(map2Query, startNum, limitNum, dir, sort);
	}

	public void instrumentRepairSetInstrumentRepair(String formId) {
		InstrumentRepair ir = commonDAO.get(InstrumentRepair.class, formId);
		ir.setState("1");
		ir.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_COMPLETE_NAME);
		List<InstrumentRepairTask> list = instrumentRepairDao.findInstrumentRepairTask(formId);
		DicState ds = new DicState();
		ds.setId("1r");
		for (InstrumentRepairTask irt : list) {
			Instrument ins = irt.getInstrument();
			ins.setState(ds);

			InstrumentRepairTemp inst = commonDAO.get(InstrumentRepairTemp.class, irt.getTempId());
			inst.setState("1");
			commonDAO.saveOrUpdate(ins);
			commonDAO.saveOrUpdate(inst);
		}
		commonDAO.saveOrUpdate(ir);
	}

	public Map<String, Object> findInstrumentRepairTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return instrumentRepairDao.findInstrumentRepairTable(start, length, query, col, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveInstrumentRepairTask(InstrumentRepair sc, String itemDataJson, String logInfo,String changeLogItem,String log) throws Exception {
		List<InstrumentRepairTask> saveItems = new ArrayList<InstrumentRepairTask>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			InstrumentRepairTask scp = new InstrumentRepairTask();
			// 将map信息读入实体类
			scp = (InstrumentRepairTask) instrumentRepairDao.Map2Bean(map, scp);
			InstrumentRepairTask irt = commonDAO.get(InstrumentRepairTask.class, scp.getId());
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setInstrumentRepair(sc);
			scp.setTempId(irt.getTempId());
			commonDAO.getSession().clear();
			saveItems.add(scp);
		}
		instrumentRepairDao.saveOrUpdateAll(saveItems);
		if (changeLogItem != null && !"".equals(changeLogItem)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setClassName("InstrumentRepair");
			li.setFileId(sc.getId());
			li.setModifyContent(changeLogItem);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			commonDAO.saveOrUpdate(li);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveInstrumentRepairTemp(InstrumentRepair sc, String itemDataJson, String logInfo, String changeLogItem,String log)
			throws Exception {
		List<InstrumentRepairTemp> saveItems = new ArrayList<InstrumentRepairTemp>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			InstrumentRepairTemp scp = new InstrumentRepairTemp();
			// 将map信息读入实体类
			scp = (InstrumentRepairTemp) instrumentRepairDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);

			saveItems.add(scp);
		}
		instrumentRepairDao.saveOrUpdateAll(saveItems);
		if (changeLogItem != null && !"".equals(changeLogItem)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("InstrumentRepair");
			li.setModifyContent(changeLogItem);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			commonDAO.saveOrUpdate(li);
		}

	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(InstrumentRepair sc, Map jsonMap, String logInfo, Map logMap, String changeLogItem,String log)
			throws Exception {
		if (sc != null) {
			instrumentRepairDao.saveOrUpdate(sc);

			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(sc.getId());
				li.setClassName("InstrumentRepair");
				li.setModifyContent(logInfo);
				if("123".equals(log)) {
					li.setState("1");
					li.setStateName("数据新增");
				}else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				commonDAO.saveOrUpdate(li);
			}

			String jsonStr = "";
			String logStr = "";
			logStr = (String) logMap.get("instrumentRepairTask");
			jsonStr = (String) jsonMap.get("instrumentRepairTask");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveInstrumentRepairTask(sc, jsonStr, logStr,changeLogItem,log);
			}
			logStr = (String) logMap.get("instrumentRepairTemp");
			jsonStr = (String) jsonMap.get("instrumentRepairTemp");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveInstrumentRepairTemp(sc, jsonStr, logStr, changeLogItem,log);
			}
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delInstrumentRepairTemp(String[] ids) throws Exception {
		for (String id : ids) {
			InstrumentRepairTemp scp = instrumentRepairDao.get(InstrumentRepairTemp.class, id);
			instrumentRepairDao.delete(scp);
			if (ids.length > 0) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(scp.getInstrument().getId());
				li.setClassName("InstrumentRepair");
				li.setModifyContent("InstrumentRepairTemp删除信息" + scp.getInstrument().getId());
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}
		}

	}

	public Map<String, Object> findInstrumentRepairTaskTable(Integer start, Integer length, String query, String col,
			String sort, String id) throws Exception {
		Map<String, Object> result = instrumentRepairDao.findInstrumentRepairTaskTable(start, length, query, col, sort,
				id);
		List<InstrumentRepairTask> list = (List<InstrumentRepairTask>) result.get("list");
		return result;
	}

	public Map<String, Object> findInstrumentRepairTempTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> result = instrumentRepairDao.findInstrumentRepairTempTable(start, length, query, col, sort);
		List<InstrumentRepairTemp> list = (List<InstrumentRepairTemp>) result.get("list");
		return result;
	}

	// 主数据内选择数据添加到入库明细表
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String addInstrumentRepairTask(String[] ids, String name, String factEndDate, String verify, String instrId,
			String createUser, String createDate) throws Exception {

		// 保存主表信息
		InstrumentRepair si = new InstrumentRepair();
		String log = "";
		if (instrId == null || instrId.length() <= 0 || "NEW".equals(instrId)) {
			log = "123";
			String code = systemCodeService.getSystemCode(SystemCode.INSTRUMENT_REPAIR_NAME,
					SystemCode.INSTRUMENT_REPAIR_CODE, SystemCode.INSTRUMENT_REPAIR_CODE_PREFIX,
					SystemCode.INSTRUMENT_REPAIR_CODE_LENGTH);
			si.setId(code);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			if ("" != createDate) {
				si.setCreateDate(sdf.parse(createDate));
			}
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			si.setCreateUser(u);
			si.setState("3");
			si.setStateName("NEW");
			si.setName(name);
			if ("" != factEndDate) {
				si.setFactEndDate(sdf.parse(factEndDate));
			}
			si.setVerify(verify);
			instrId = si.getId();
			si.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			si.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			commonDAO.saveOrUpdate(si);
		} else {
			si = commonDAO.get(InstrumentRepair.class, instrId);
		}
		for (int i = 0; i < ids.length; i++) {
			String id = ids[i];
			// 通过id查询库存主数据
			InstrumentRepairTemp s = commonDAO.get(InstrumentRepairTemp.class, id);
			InstrumentRepairTask sii = new InstrumentRepairTask();
			if (null != s) {
				sii.setTempId(s.getId());
				sii.setInstrument(s.getInstrument());
			}
			sii.setInstrumentRepair(si);
			commonDAO.saveOrUpdate(sii);
			
			String kucun = "设备维修:"+"设备编号:"+s.getInstrument().getId()+"设备名称:"+s.getInstrument().getName()+"的数据已添加到明细";
			if (kucun != null && !"".equals(kucun)) {
				LogInfo li = new LogInfo();
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(si.getId());
				li.setClassName("InstrumentRepair");
				li.setLogDate(new Date());
				li.setModifyContent(kucun);
				if("123".equals(log)) {
					li.setState("1");
					li.setStateName("数据新增");
				}else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				commonDAO.saveOrUpdate(li);
			}
		}
		return instrId;
	}
}
