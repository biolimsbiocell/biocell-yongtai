package com.biolims.equipment.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dic.model.DicType;
import com.biolims.dic.model.DicUnit;
import com.biolims.dao.EntityDao;

/**
 * 预防维修任务
 * @author Vera
 */
@Entity
@Table(name = "T_INSTRUMENT_REPAIR_PLAN_TASK")
public class InstrumentRepairPlanTask extends EntityDao<Instrument> implements Serializable {

	private static final long serialVersionUID = 2191801684912559179L;
	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(length = 32)
	private String id;//任务编号

	@Column(length = 4000)
	private String note;//任务描述
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private DicType type;//维护类型
	
	@Column(length = 100)
	private Integer tsTime;//提示时间

	@Column(length = 100)
	private Integer period;//周期
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private DicUnit unit;//单位
	
	private Double fee;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private InstrumentRepairPlan instrumentRepairPlan;//预防维护单号

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private Instrument instrument;//设备编号
	
	private String unitName;
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public DicType getType() {
		return type;
	}

	public void setType(DicType type) {
		this.type = type;
	}

	public InstrumentRepairPlan getInstrumentRepairPlan() {
		return instrumentRepairPlan;
	}

	public void setInstrumentRepairPlan(InstrumentRepairPlan instrumentRepairPlan) {
		this.instrumentRepairPlan = instrumentRepairPlan;
	}

	public Double getFee() {
		return fee;
	}

	public void setFee(Double fee) {
		this.fee = fee;
	}

	public Instrument getInstrument() {
		return instrument;
	}

	public void setInstrument(Instrument instrument) {
		this.instrument = instrument;
	}

	/**
	 * @return the tsTime
	 */
	public Integer getTsTime() {
		return tsTime;
	}

	/**
	 * @param tsTime the tsTime to set
	 */
	public void setTsTime(Integer tsTime) {
		this.tsTime = tsTime;
	}

	/**
	 * @return the period
	 */
	public Integer getPeriod() {
		return period;
	}

	/**
	 * @param period the period to set
	 */
	public void setPeriod(Integer period) {
		this.period = period;
	}

	/**
	 * @return the unit
	 */
	public DicUnit getUnit() {
		return unit;
	}

	/**
	 * @param unit the unit to set
	 */
	public void setUnit(DicUnit unit) {
		this.unit = unit;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public String setUnitName(String unitName,String unit){
		this.unitName =unitName;
		return unitName;
	}
}
