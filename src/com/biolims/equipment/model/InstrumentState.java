package com.biolims.equipment.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;

/**
 * 设备状态（各个实验的设备的状态记录）
 * 
 */
@Entity
@Table(name = "T_INSTRUMENT_STATE")
public class InstrumentState implements Serializable {

	private static final long serialVersionUID = -407759837281114436L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;// ID
	/** 设备编号 */
	@Column(name = "INSTRUMENT_CODE", length = 100)
	private String instrumentCode;
	/** 设备名称 */
	@Column(name = "INSTRUMENT_NAME", length = 100)
	private String instrumentName;
	/** 设备状态 */
	@Column(name = "INSTRUMENT_STATE", length = 100)
	private String instrumentState;
	/** 检测类型 */
	@Column(name = "PRODUCT_ID", length = 100)
	private String productId;
	/** 检测名称 */
	@Column(name = "PRODUCT_NAME", length = 100)
	private String productName;
	/** 阶段时间 */
	@Column(name = "STAGE_TIME", length = 100)
	private String stageTime;
	/** 开始时间 */
	private String startDate;
	/** 结束时间 */
	private String endDate;
	/** 阶段id（占用实验单号） */
	@Column(name = "TABLE_TYPE_ID", length = 32)
	private String tableTypeId;
	/** 阶段名称（实验名称）（占用实验名称） */
	@Column(name = "STAGE_NAME", length = 32)
	private String stageName;
	/** 操作人 */
	@JoinColumn(name = "ACCEPT_USER_ID")
	private String acceptUser;
	/** 执行单ID */
	@Column(name = "TASK_ID", length = 100)
	private String taskId;
	/** 执行处理方式（下一步流向） */
	@Column(name = "TASK_METHOD", length = 100)
	private String taskMethod;
	/** 执行结果 */
	@Column(name = "TASK_RESULT", length = 100)
	private String taskResult;
	/** 科技服务任务单ID */
	@Column(name = "TECH_TASK_ID", length = 100)
	private String techTaskId;
	/** 实验模板 */
	@Column(name = "MODE", length = 100)
	private String mode;
	/** 实验人员组 */
	@Column(name = "USER_GROUP", length = 100)
	private String userGroup;
	/** 用量 */
	@Column(name = "COUNT_DATA", length = 100)
	private String countData;
	/** 占用实验阶段 */
	@Column(name = "Note", length = 200)
	private String note;// 说明
	/** 占用实验阶段名称 */
	@Column(name = "Note2", length = 200)
	private String note2;// 说明

	@Column(name = "Note3", length = 200)
	private String note3;// 说明

	@Column(name = "Note4", length = 200)
	private String note4;// 说明

	@Column(name = "Note5", length = 200)
	private String note5;// 说明

	@Column(name = "Note6", length = 200)
	private String note6;// 说明

	@Column(name = "Note7", length = 200)
	private String note7;// 说明
	
	//患者姓名
	private String name;
	
	//生产批号
	private String batch;
	

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private Instrument instrument;//主表
	
	
	
	
	

	public Instrument getInstrument() {
		return instrument;
	}

	public void setInstrument(Instrument instrument) {
		this.instrument = instrument;
	}

	/**
	 * @return id
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return instrumentCode
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getInstrumentCode() {
		return instrumentCode;
	}

	/**
	 * @param instrumentCode
	 *            the instrumentCode to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setInstrumentCode(String instrumentCode) {
		this.instrumentCode = instrumentCode;
	}

	/**
	 * @return instrumentName
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getInstrumentName() {
		return instrumentName;
	}

	/**
	 * @param instrumentName
	 *            the instrumentName to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setInstrumentName(String instrumentName) {
		this.instrumentName = instrumentName;
	}

	/**
	 * @return instrumentState
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getInstrumentState() {
		return instrumentState;
	}

	/**
	 * @param instrumentState
	 *            the instrumentState to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setInstrumentState(String instrumentState) {
		this.instrumentState = instrumentState;
	}

	/**
	 * @return productId
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @param productId
	 *            the productId to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * @return productName
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * @param productName
	 *            the productName to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * @return stageTime
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getStageTime() {
		return stageTime;
	}

	/**
	 * @param stageTime
	 *            the stageTime to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setStageTime(String stageTime) {
		this.stageTime = stageTime;
	}

	/**
	 * @return startDate
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return endDate
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate
	 *            the endDate to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return tableTypeId
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getTableTypeId() {
		return tableTypeId;
	}

	/**
	 * @param tableTypeId
	 *            the tableTypeId to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setTableTypeId(String tableTypeId) {
		this.tableTypeId = tableTypeId;
	}

	/**
	 * @return stageName
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getStageName() {
		return stageName;
	}

	/**
	 * @param stageName
	 *            the stageName to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setStageName(String stageName) {
		this.stageName = stageName;
	}


	/**
	 * @return taskId
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getTaskId() {
		return taskId;
	}

	/**
	 * @param taskId
	 *            the taskId to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	/**
	 * @return taskMethod
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getTaskMethod() {
		return taskMethod;
	}

	/**
	 * @param taskMethod
	 *            the taskMethod to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setTaskMethod(String taskMethod) {
		this.taskMethod = taskMethod;
	}

	/**
	 * @return taskResult
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getTaskResult() {
		return taskResult;
	}

	/**
	 * @param taskResult
	 *            the taskResult to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setTaskResult(String taskResult) {
		this.taskResult = taskResult;
	}

	/**
	 * @return techTaskId
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getTechTaskId() {
		return techTaskId;
	}

	/**
	 * @param techTaskId
	 *            the techTaskId to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setTechTaskId(String techTaskId) {
		this.techTaskId = techTaskId;
	}

	/**
	 * @return note
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getNote() {
		return note;
	}

	/**
	 * @param note
	 *            the note to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * @return note2
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getNote2() {
		return note2;
	}

	/**
	 * @param note2
	 *            the note2 to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setNote2(String note2) {
		this.note2 = note2;
	}

	/**
	 * @return note3
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getNote3() {
		return note3;
	}

	/**
	 * @param note3
	 *            the note3 to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setNote3(String note3) {
		this.note3 = note3;
	}

	/**
	 * @return note4
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getNote4() {
		return note4;
	}

	/**
	 * @param note4
	 *            the note4 to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setNote4(String note4) {
		this.note4 = note4;
	}

	/**
	 * @return note5
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getNote5() {
		return note5;
	}

	/**
	 * @param note5
	 *            the note5 to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setNote5(String note5) {
		this.note5 = note5;
	}

	/**
	 * @return note6
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getNote6() {
		return note6;
	}

	/**
	 * @param note6
	 *            the note6 to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setNote6(String note6) {
		this.note6 = note6;
	}

	/**
	 * @return note7
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getNote7() {
		return note7;
	}

	/**
	 * @param note7
	 *            the note7 to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setNote7(String note7) {
		this.note7 = note7;
	}

	/** 
	 * @return mode
	 * @author  zhiqiang.yang@biolims.cn  
	 */
	public String getMode() {
		return mode;
	}

	/**
	 * @param mode the mode to set
	 * @author  zhiqiang.yang@biolims.cn 
	 */
	public void setMode(String mode) {
		this.mode = mode;
	}

	/** 
	 * @return userGroup
	 * @author  zhiqiang.yang@biolims.cn  
	 */
	public String getUserGroup() {
		return userGroup;
	}

	/**
	 * @param userGroup the userGroup to set
	 * @author  zhiqiang.yang@biolims.cn 
	 */
	public void setUserGroup(String userGroup) {
		this.userGroup = userGroup;
	}

	/** 
	 * @return countData
	 * @author  zhiqiang.yang@biolims.cn  
	 */
	public String getCountData() {
		return countData;
	}

	/**
	 * @param countData the countData to set
	 * @author  zhiqiang.yang@biolims.cn 
	 */
	public void setCountData(String countData) {
		this.countData = countData;
	}

	/** 
	 * @return acceptUser
	 * @author  zhiqiang.yang@biolims.cn  
	 */
	public String getAcceptUser() {
		return acceptUser;
	}

	/**
	 * @param acceptUser the acceptUser to set
	 * @author  zhiqiang.yang@biolims.cn 
	 */
	public void setAcceptUser(String acceptUser) {
		this.acceptUser = acceptUser;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	



}
