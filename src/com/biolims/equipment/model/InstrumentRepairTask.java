package com.biolims.equipment.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dic.model.DicType;
import com.biolims.dao.EntityDao;
import com.biolims.storage.model.Storage;

/**
 * 维修任务
 * 
 * @author Vera
 */
@Entity
@Table(name = "T_INSTRUMENT_REPAIR_TASK")
public class InstrumentRepairTask extends EntityDao<Instrument> implements
		Serializable {

	private static final long serialVersionUID = 2191801684912559179L;
	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(length = 32)
	private String id;// 任务编号

	@Column(length = 4000)
	private String note;// 维修方式及备注

	@Column(length = 32, name = "TEMP_ID")
	private String tempId;// 临时表ID

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private DicType type;// 类型

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private InstrumentRepair instrumentRepair;// 维护单号

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private Instrument instrument;// 设备编号

	@Column(length = 4000)
	private String reason;// 故障原因

	@Column(length = 4000)
	public Instrument getInstrument() {
		return instrument;
	}

	public void setInstrument(Instrument instrument) {
		this.instrument = instrument;
	}

	private Double fee;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public DicType getType() {
		return type;
	}

	public void setType(DicType type) {
		this.type = type;
	}

	public InstrumentRepair getInstrumentRepair() {
		return instrumentRepair;
	}

	public void setInstrumentRepair(InstrumentRepair instrumentRepair) {
		this.instrumentRepair = instrumentRepair;
	}

	public Double getFee() {
		return fee;
	}

	public void setFee(Double fee) {
		this.fee = fee;
	}

	/**
	 * @return the tempId
	 */
	public String getTempId() {
		return tempId;
	}

	/**
	 * @param tempId
	 *            the tempId to set
	 */
	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

}
