package com.biolims.equipment.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dic.model.DicFinanceItem;
import com.biolims.dic.model.DicState;
import com.biolims.dic.model.DicType;
import com.biolims.core.model.user.Department;
import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.storage.model.Storage;

/**
 * 设备借用调拨
 * @author Vera
 */
@Entity
@Table(name = "T_INSTRUMENT_BORROW")
public class InstrumentBorrow extends EntityDao<Instrument> implements Serializable {

	private static final long serialVersionUID = 2191801684912559179L;
	@Id
	@Column(length = 32)
	private String id;//ID

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private Department oldDepartment;//原所属部门

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private DicFinanceItem oldFinanceCode;//原财务科目

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private User newDutyUser;//新责任人

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private Department newDepartment;//调拨借用部门

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private User dutyUser;//原责任人

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private DicFinanceItem newFinanceCode;//新财务科目

	private Date handleDate;//调拨日期

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private User appleUser;//申请人

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private User createUser;//创建人

	private Date createDate;//创建时间
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private DicType type;//类型

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private User confirmUser;//批准人
	private Date confirmDate;//批准时间

	@Column(length = 200)
	private String stateName;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_NEW_STATE_ID")
	private DicState newState;//状态

	private String note;//调整状态

	private String state;
	/** 范围Id */
	private String scopeId;
	/** 范围 */
	private String scopeName;
	public User getConfirmUser() {
		return confirmUser;
	}

	public void setConfirmUser(User confirmUser) {
		this.confirmUser = confirmUser;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Department getOldDepartment() {
		return oldDepartment;
	}

	public void setOldDepartment(Department oldDepartment) {
		this.oldDepartment = oldDepartment;
	}

	public DicFinanceItem getOldFinanceCode() {
		return oldFinanceCode;
	}

	public void setOldFinanceCode(DicFinanceItem oldFinanceCode) {
		this.oldFinanceCode = oldFinanceCode;
	}

	public User getNewDutyUser() {
		return newDutyUser;
	}

	public void setNewDutyUser(User newDutyUser) {
		this.newDutyUser = newDutyUser;
	}

	public Department getNewDepartment() {
		return newDepartment;
	}

	public void setNewDepartment(Department newDepartment) {
		this.newDepartment = newDepartment;
	}

	public User getDutyUser() {
		return dutyUser;
	}

	public void setDutyUser(User dutyUser) {
		this.dutyUser = dutyUser;
	}

	public DicFinanceItem getNewFinanceCode() {
		return newFinanceCode;
	}

	public void setNewFinanceCode(DicFinanceItem newFinanceCode) {
		this.newFinanceCode = newFinanceCode;
	}

	public Date getHandleDate() {
		return handleDate;
	}

	public void setHandleDate(Date handleDate) {
		this.handleDate = handleDate;
	}

	public User getAppleUser() {
		return appleUser;
	}

	public void setAppleUser(User appleUser) {
		this.appleUser = appleUser;
	}

	public DicType getType() {
		return type;
	}

	public void setType(DicType type) {
		this.type = type;
	}


	public User getCreateUser() {
		return createUser;
	}

	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public DicState getNewState() {
		return newState;
	}

	public void setNewState(DicState newState) {
		this.newState = newState;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getScopeId() {
		return scopeId;
	}

	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	public String getScopeName() {
		return scopeName;
	}

	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

}
