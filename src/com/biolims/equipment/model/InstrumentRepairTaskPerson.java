package com.biolims.equipment.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dic.model.DicType;
import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;

/**
 * 维修人员
 * @author Vera
 */
@Entity
@Table(name = "T_INSTR_R_T_PERSON")
public class InstrumentRepairTaskPerson extends EntityDao<InstrumentRepairTaskPerson> implements Serializable {

	private static final long serialVersionUID = 2191801684912559179L;
	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(length = 32)
	private String id;//ID

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "USER_ID")
	private User user;//人员

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "WORK_TYPE_ID")
	private DicType workType;//工种

	private Double scale;//费率

	private Double workTime;//工时

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "INSTRUMENT_R_T")
	private InstrumentRepairTask instrumentRepairTask;//任务

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public DicType getWorkType() {
		return workType;
	}

	public void setWorkType(DicType workType) {
		this.workType = workType;
	}

	public Double getScale() {
		return scale;
	}

	public void setScale(Double scale) {
		this.scale = scale;
	}

	public Double getWorkTime() {
		return workTime;
	}

	public void setWorkTime(Double workTime) {
		this.workTime = workTime;
	}

	public InstrumentRepairTask getInstrumentRepairTask() {
		return instrumentRepairTask;
	}

	public void setInstrumentRepairTask(InstrumentRepairTask instrumentRepairTask) {
		this.instrumentRepairTask = instrumentRepairTask;
	}

}
