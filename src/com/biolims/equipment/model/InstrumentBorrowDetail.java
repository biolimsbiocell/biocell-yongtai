package com.biolims.equipment.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.experiment.roomManagement.model.RoomManagement;

/**
 * 维修任务
 * 
 * @author Vera
 */
@Entity
@Table(name = "T_INSTRUMENT_BORROW_DETAIL")
public class InstrumentBorrowDetail extends EntityDao<Instrument> implements Serializable {

	private static final long serialVersionUID = -5574374406447896499L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(length = 32)
	private String id;// 设备调整明细ID

	@Column(length = 4000)
	private String note;// 备注

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private InstrumentBorrow instrumentBorrow;// 调整单号

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private Instrument instrument;// 设备编号

	/** 房间 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")
	private RoomManagement roomManagement;

	public RoomManagement getRoomManagement() {
		return roomManagement;
	}

	public void setRoomManagement(RoomManagement roomManagement) {
		this.roomManagement = roomManagement;
	}

	public Instrument getInstrument() {
		return instrument;
	}

	public void setInstrument(Instrument instrument) {
		this.instrument = instrument;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public InstrumentBorrow getInstrumentBorrow() {
		return instrumentBorrow;
	}

	public void setInstrumentBorrow(InstrumentBorrow instrumentBorrow) {
		this.instrumentBorrow = instrumentBorrow;
	}

}
