package com.biolims.equipment.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dic.model.DicFinanceItem;
import com.biolims.dic.model.DicState;
import com.biolims.dic.model.DicType;
import com.biolims.dic.model.DicUnit;
import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.storage.model.Storage;

/**
 * 设备预防维护
 * @author Vera
 */
@Entity
@Table(name = "T_INSTRUMENT_REPAIR_PLAN")
public class InstrumentRepairPlan extends EntityDao<Instrument> implements Serializable {

	private static final long serialVersionUID = 2191801684912559179L;
	@Id
	@Column(length = 32)
	private String id;//预防维护单号

	@Column(length = 110)
	private String note;//描述

	@Column(length = 110)
	private String tsNote;//备注

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private DicType type;//维护类型

	@Column(length = 100)
	private String name;//名称

	private Date startDate;//开始时间

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private DicFinanceItem materialCode;//材料科目

	private Date endDate;//计划完成时间
	private Date effectiveDate;//启用日期
	private Date expiryDate;//报废日期

	private Date nextDate;
	private Date nextDates;

	private Date tsDate;//提示日期
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private DicFinanceItem personCode;//人员科目

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private DicFinanceItem repairCode;//维护科目

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private User confirmUser;//批准人

	private Date createDate;//创建日期

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private User createUser;//创建人

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private User dutyUser;//负责人

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private DicState state;//状态

	@Column(length = 200)
	private String stateName;//状态

	@Column(length = 100)
	private Integer period;//周期
	
	@Column(length = 100)
	private Integer tsTime;//提示时间

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private DicUnit unit;//单位

	//单位
	private String unitName;
	/** 范围Id */
	private String scopeId;
	/** 范围 */
	private String scopeName;
	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public DicType getType() {
		return type;
	}

	public void setType(DicType type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public DicFinanceItem getMaterialCode() {
		return materialCode;
	}

	public void setMaterialCode(DicFinanceItem materialCode) {
		this.materialCode = materialCode;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public DicFinanceItem getPersonCode() {
		return personCode;
	}

	public void setPersonCode(DicFinanceItem personCode) {
		this.personCode = personCode;
	}

	public DicFinanceItem getRepairCode() {
		return repairCode;
	}

	public void setRepairCode(DicFinanceItem repairCode) {
		this.repairCode = repairCode;
	}

	public User getConfirmUser() {
		return confirmUser;
	}

	public void setConfirmUser(User confirmUser) {
		this.confirmUser = confirmUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public User getCreateUser() {
		return createUser;
	}

	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	public DicState getState() {
		return state;
	}

	public void setState(DicState state) {
		this.state = state;
	}

	public Integer getPeriod() {
		return period;
	}

	public void setPeriod(Integer period) {
		this.period = period;
	}

	public DicUnit getUnit() {
		return unit;
	}

	public void setUnit(DicUnit unit) {
		this.unit = unit;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public Date getNextDate() {
		return nextDate;
	}

	public void setNextDate(Date nextDate) {
		this.nextDate = nextDate;
	}

	public User getDutyUser() {
		return dutyUser;
	}

	public void setDutyUser(User dutyUser) {
		this.dutyUser = dutyUser;
	}

	public Date getNextDates() {
		return nextDates;
	}

	public void setNextDates(Date nextDates) {
		this.nextDates = nextDates;
	}

	public String getTsNote() {
		return tsNote;
	}

	public void setTsNote(String tsNote) {
		this.tsNote = tsNote;
	}

	public Date getTsDate() {
		return tsDate;
	}

	public void setTsDate(Date tsDate) {
		this.tsDate = tsDate;
	}

	public Integer getTsTime() {
		return tsTime;
	}

	public void setTsTime(Integer tsTime) {
		this.tsTime = tsTime;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getScopeName() {
		return scopeName;
	}

	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

	public String getScopeId() {
		return scopeId;
	}

	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}
	
}
