package com.biolims.equipment.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.core.model.user.Department;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicFinanceItem;
import com.biolims.dic.model.DicState;
import com.biolims.dic.model.DicType;
import com.biolims.dic.model.DicUnit;
import com.biolims.experiment.roomManagement.model.RoomManagement;
import com.biolims.storage.container.model.StorageContainer;
import com.biolims.storage.position.model.StoragePosition;
import com.biolims.supplier.model.Supplier;

/**
 * 设备
 * 
 * @author 倪毅
 * 
 */
@Entity
@Table(name = "T_Instrument")
public class Instrument extends EntityDao<Instrument> implements Serializable {

	private static final long serialVersionUID = 6024018966809089686L;
	@Id
	@Column(length = 32)
	private String id;

	@Column(name = "NAME", length = 110)
	private String name;// 名称

	@Column(name = "ENG_NAME", length = 200)
	private String engName;// 英文名称

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")
	@JoinColumn(name = "USER_DUTY_USER_ID")
	private User dutyUser;// 保管人

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")
	@JoinColumn(name = "USER_DUTY_USER_TWO_ID")
	private User dutyUserTwo;// 保管人2

	@Column(name = "RANK", length = 100)
	private String rank;// 级别

	@Column(name = "SERIAL_NUMBER", length = 150)
	private String serialNumber;// 序列号

	@Column(name = "ENQUIRYODD_NUMBER", length = 150)
	private String enquiryOddNumber;// 询价单号

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")
	@JoinColumn(name = "DIC_STORAGE_MAIN_TYPE_ID")
	private DicType mainType;// 分类

	@Column(name = "SEARCH_CODE", length = 15)
	private String searchCode;// 检索码

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")
	@JoinColumn(name = "DIC_STUDY_TYPE_ID")
	private DicType studyType;// 子类型

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")
	@JoinColumn(name = "PARENT_ID")
	private Instrument parent;// 所属冰箱

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")
	@JoinColumn(name = "PARENT_TWO_ID")
	private Instrument parent2;// 监控主机

	@Column(name = "CURRENT_INDEX", length = 2)
	private Integer currentIndex;// 当前INDEX索引

	@Column(name = "USE_DESC", length = 110)
	private String useDesc;// 用途

	@Column(name = "BAR_CODE", length = 32)
	private String barCode;// 条形码

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")
	@JoinColumn(name = "DIC_SOURCE_TYPE_ID")
	private DicType source;// 来源

	@Column(name = "SPEC", length = 80)
	private String spec;// 规格

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")
	@JoinColumn(name = "DIC_STATE_ID")
	private DicState state;// 状态

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")
	@JoinColumn(name = "USER_CREATE_USER_ID")
	private User createUser;// 创建人

	@Column(name = "CREATE_DATE", length = 30)
	private Date createDate;// 创建时间

	@Column(name = "IF_CALL", length = 32)
	private String ifCall;// 是否失效提醒

	@Column(name = "PIC_PATH", length = 200)
	private String picPath;// 图片

	@Column(name = "NUM")
	private Double num;// 库存数量

	@Column(name = "OUT_PRICE")
	private Double outPrice;// 出库价格

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")
	@JoinColumn(name = "DIC_UNIT_ID")
	private DicUnit unit;// 单位

	@Column(name = "SAVE_CONDITION", length = 100)
	private String saveCondition;// 存储条件

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")
	@JoinColumn(name = "STORAGE_POSITION_ID")
	private StoragePosition position;// 存储位置

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")
	@JoinColumn(name = "DIC_STORAGE_TYPE_ID")
	private DicType type;// 类型 器具和采购试剂

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")
	@JoinColumn(name = "DEPARTMENT_ID")
	private Department department;// 部门

	@Column(name = "BREED", length = 60)
	private String breed;// 品牌

	@Column(name = "PRODUCT_ADDRESS", length = 120)
	private String productAddress;// 产地

	@Column(name = "PRICE")
	private Double price;// 价格

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")
	@JoinColumn(name = "DIC_CURRENCY_TYPE_ID")
	private DicType currencyType;// 币种

	@Column(name = "NOTE", length = 110)
	private String note;// 备注

	@Column(length = 110)
	private String capitalCode;// 资产号

	@Column
	private Date useDate;// 最近一次维护时间

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")
	private DicFinanceItem financeItem;// 财务科目

	@Column(name = "OUT_CODE", length = 110)
	private String outCode;// 出厂编号

	@Column(name = "ACCURACY", length = 110)
	private String accuracy;// 不确定度/准确度

	@Column(length = 30)
	private Date effectiveDate;// 安装日期

	@Column(length = 30)
	private Date expiryDate;// 结束日期

	@Column(length = 30)
	private Date nextDate;// 下个检测日期

	@Column(name = "PURCHASE_DATE", length = 30)
	private Date purchaseDate;// 购买日期

	@Column(name = "SURVEY_SCOPE", length = 110)
	private String surveyScope;// 测量范围

	@Column(name = "CHECK_ITEM", length = 110)
	private String checkItem;// 检测项目

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")
	private Supplier supplier;// 供应商

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")
	private DicType producer;// 生产商

	@Column(name = "IS_FULL", length = 110)
	private String isFull;// 是否占用

	@Column(name = "P_TYPE", length = 32)
	private String p_type;// 区分设备主数据

	@Column(name = "TS_DATE")
	private Date tsDate;// 提示日期

	@Column(name = "HEAD_PERSON")
	private String headPerson;// 负责人
	@Column(name = "GUARANTEE_PERIOD")
	private String guaranteePeriod;// 质保期
	@Column(name = "METERAGE_VALIDATE")
	private String meterageValidate;// 计量验证
	@Column(name = "METERAGE_VALI_DATE")
	private Date meterageValiDate;// 计量验证有效期
	@Column(name = "PERIOD")
	private Date period;// 保修年限
	@Column(name = "ENGINEER")
	private String engineer;// 工程师
	@Column(name = "INVOICENUMBER")
	private String invoiceNumber;// 发票号
	@Column(name = "CONTRACTNUMBER")
	private String contractNumber;// 合同号
	/** 房间 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")
	private RoomManagement roomManagement;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")
	@JoinColumn(name = "PRODUCTS") // 生产商
	private DicType products;

	// 使用年限
	private String useYear;
	// 注册证号
	private String registration;
	// 工程师联系方式
	private String engineerNum;
	// 领用方式
	private String useWay;
	// 供应商
	private String supplierNote;
	// 维修信息
	private String repairNote;
	// 是否需验证
	private String verify;
	// 负责人
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")
	@JoinColumn(name = "HEAD_USER_ID")
	private User headUser;

	// 状态
	private String state1;
	// 状态名称
	private String stateName;
	// 成本中心
	private String scopeId;
	// 成本中心名称
	private String ScopeName;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "storage_container")
	private StorageContainer storageContainer;// 容器  只有二氧化碳培养箱用
	
	// 总位置数 只有二氧化碳培养箱用
	private Integer totalLocationsNumber;
	// 剩余位置数 只有二氧化碳培养箱用
	private Integer surplusLocationsNumber;
	
	

	public Integer getTotalLocationsNumber() {
		return totalLocationsNumber;
	}

	public void setTotalLocationsNumber(Integer totalLocationsNumber) {
		this.totalLocationsNumber = totalLocationsNumber;
	}

	public Integer getSurplusLocationsNumber() {
		return surplusLocationsNumber;
	}

	public void setSurplusLocationsNumber(Integer surplusLocationsNumber) {
		this.surplusLocationsNumber = surplusLocationsNumber;
	}

	public StorageContainer getStorageContainer() {
		return storageContainer;
	}

	public void setStorageContainer(StorageContainer storageContainer) {
		this.storageContainer = storageContainer;
	}

	public RoomManagement getRoomManagement() {
		return roomManagement;
	}

	public void setRoomManagement(RoomManagement roomManagement) {
		this.roomManagement = roomManagement;
	}

	public String getState1() {
		return state1;
	}

	public void setState1(String state1) {
		this.state1 = state1;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getScopeId() {
		return scopeId;
	}

	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	public String getScopeName() {
		return ScopeName;
	}

	public void setScopeName(String scopeName) {
		ScopeName = scopeName;
	}

	public User getHeadUser() {
		return headUser;
	}

	public void setHeadUser(User headUser) {
		this.headUser = headUser;
	}

	public String getUseYear() {
		return useYear;
	}

	public void setUseYear(String useYear) {
		this.useYear = useYear;
	}

	public String getSupplierNote() {
		return supplierNote;
	}

	public void setSupplierNote(String supplierNote) {
		this.supplierNote = supplierNote;
	}

	public String getRegistration() {
		return registration;
	}

	public void setRegistration(String registration) {
		this.registration = registration;
	}

	public String getEngineerNum() {
		return engineerNum;
	}

	public void setEngineerNum(String engineerNum) {
		this.engineerNum = engineerNum;
	}

	public String getUseWay() {
		return useWay;
	}

	public void setUseWay(String useWay) {
		this.useWay = useWay;
	}

	public String getRepairNote() {
		return repairNote;
	}

	public void setRepairNote(String repairNote) {
		this.repairNote = repairNote;
	}

	public DicType getProducts() {
		return products;
	}

	public void setProducts(DicType products) {
		this.products = products;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEngName() {
		return engName;
	}

	public void setEngName(String engName) {
		this.engName = engName;
	}

	public User getDutyUser() {
		return dutyUser;
	}

	public void setDutyUser(User dutyUser) {
		this.dutyUser = dutyUser;
	}

	public User getDutyUserTwo() {
		return dutyUserTwo;
	}

	public void setDutyUserTwo(User dutyUserTwo) {
		this.dutyUserTwo = dutyUserTwo;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getEnquiryOddNumber() {
		return enquiryOddNumber;
	}

	public void setEnquiryOddNumber(String enquiryOddNumber) {
		this.enquiryOddNumber = enquiryOddNumber;
	}

	public DicType getMainType() {
		return mainType;
	}

	public void setMainType(DicType mainType) {
		this.mainType = mainType;
	}

	public String getSearchCode() {
		return searchCode;
	}

	public void setSearchCode(String searchCode) {
		this.searchCode = searchCode;
	}

	public DicType getStudyType() {
		return studyType;
	}

	public void setStudyType(DicType studyType) {
		this.studyType = studyType;
	}

	public Integer getCurrentIndex() {
		return currentIndex;
	}

	public void setCurrentIndex(Integer currentIndex) {
		this.currentIndex = currentIndex;
	}

	public String getUseDesc() {
		return useDesc;
	}

	public void setUseDesc(String useDesc) {
		this.useDesc = useDesc;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public DicType getSource() {
		return source;
	}

	public void setSource(DicType source) {
		this.source = source;
	}

	public String getSpec() {
		return spec;
	}

	public void setSpec(String spec) {
		this.spec = spec;
	}

	public DicState getState() {
		return state;
	}

	public void setState(DicState state) {
		this.state = state;
	}

	public User getCreateUser() {
		return createUser;
	}

	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getIfCall() {
		return ifCall;
	}

	public void setIfCall(String ifCall) {
		this.ifCall = ifCall;
	}

	public String getPicPath() {
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}

	public Double getNum() {
		return num;
	}

	public void setNum(Double num) {
		this.num = num;
	}

	public Double getOutPrice() {
		return outPrice;
	}

	public void setOutPrice(Double outPrice) {
		this.outPrice = outPrice;
	}

	public DicUnit getUnit() {
		return unit;
	}

	public void setUnit(DicUnit unit) {
		this.unit = unit;
	}

	public String getSaveCondition() {
		return saveCondition;
	}

	public void setSaveCondition(String saveCondition) {
		this.saveCondition = saveCondition;
	}

	public StoragePosition getPosition() {
		return position;
	}

	public void setPosition(StoragePosition position) {
		this.position = position;
	}

	public DicType getType() {
		return type;
	}

	public void setType(DicType type) {
		this.type = type;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public String getBreed() {
		return breed;
	}

	public void setBreed(String breed) {
		this.breed = breed;
	}

	public String getProductAddress() {
		return productAddress;
	}

	public void setProductAddress(String productAddress) {
		this.productAddress = productAddress;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public DicType getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(DicType currencyType) {
		this.currencyType = currencyType;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getCapitalCode() {
		return capitalCode;
	}

	public void setCapitalCode(String capitalCode) {
		this.capitalCode = capitalCode;
	}

	public Date getUseDate() {
		return useDate;
	}

	public void setUseDate(Date useDate) {
		this.useDate = useDate;
	}

	public DicFinanceItem getFinanceItem() {
		return financeItem;
	}

	public void setFinanceItem(DicFinanceItem financeItem) {
		this.financeItem = financeItem;
	}

	public String getOutCode() {
		return outCode;
	}

	public void setOutCode(String outCode) {
		this.outCode = outCode;
	}

	public String getAccuracy() {
		return accuracy;
	}

	public void setAccuracy(String accuracy) {
		this.accuracy = accuracy;
	}

	public Date getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public String getSurveyScope() {
		return surveyScope;
	}

	public void setSurveyScope(String surveyScope) {
		this.surveyScope = surveyScope;
	}

	public String getCheckItem() {
		return checkItem;
	}

	public void setCheckItem(String checkItem) {
		this.checkItem = checkItem;
	}

	public String getIsFull() {
		return isFull;
	}

	public void setIsFull(String isFull) {
		this.isFull = isFull;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Date getNextDate() {
		return nextDate;
	}

	public void setNextDate(Date nextDate) {
		this.nextDate = nextDate;
	}

	public String getP_type() {
		return p_type;
	}

	public void setP_type(String p_type) {
		this.p_type = p_type;
	}

	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public DicType getProducer() {
		return producer;
	}

	public void setProducer(DicType producer) {
		this.producer = producer;
	}

	/**
	 * @return the parent
	 */
	public Instrument getParent() {
		return parent;
	}

	/**
	 * @param parent
	 *            the parent to set
	 */
	public void setParent(Instrument parent) {
		this.parent = parent;
	}

	/**
	 * @return the tsDate
	 */
	public Date getTsDate() {
		return tsDate;
	}

	/**
	 * @param tsDate
	 *            the tsDate to set
	 */
	public void setTsDate(Date tsDate) {
		this.tsDate = tsDate;
	}

	/**
	 * @return headPerson
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getHeadPerson() {
		return headPerson;
	}

	/**
	 * @param headPerson
	 *            the headPerson to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setHeadPerson(String headPerson) {
		this.headPerson = headPerson;
	}

	/**
	 * @return guaranteePeriod
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getGuaranteePeriod() {
		return guaranteePeriod;
	}

	/**
	 * @param guaranteePeriod
	 *            the guaranteePeriod to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setGuaranteePeriod(String guaranteePeriod) {
		this.guaranteePeriod = guaranteePeriod;
	}

	/**
	 * @return meterageValidate
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getMeterageValidate() {
		return meterageValidate;
	}

	/**
	 * @param meterageValidate
	 *            the meterageValidate to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setMeterageValidate(String meterageValidate) {
		this.meterageValidate = meterageValidate;
	}

	/**
	 * @return meterageValiDate
	 * @author zhiqiang.yang@biolims.cn
	 */
	public Date getMeterageValiDate() {
		return meterageValiDate;
	}

	/**
	 * @param meterageValiDate
	 *            the meterageValiDate to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setMeterageValiDate(Date meterageValiDate) {
		this.meterageValiDate = meterageValiDate;
	}

	/**
	 * @return period
	 * @author zhiqiang.yang@biolims.cn
	 */
	public Date getPeriod() {
		return period;
	}

	/**
	 * @param period
	 *            the period to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setPeriod(Date period) {
		this.period = period;
	}

	/**
	 * @return engineer
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getEngineer() {
		return engineer;
	}

	/**
	 * @param engineer
	 *            the engineer to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setEngineer(String engineer) {
		this.engineer = engineer;
	}

	/**
	 * @return invoiceNumber
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	/**
	 * @param invoiceNumber
	 *            the invoiceNumber to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	/**
	 * @return contractNumber
	 * @author zhiqiang.yang@biolims.cn
	 */
	public String getContractNumber() {
		return contractNumber;
	}

	/**
	 * @param contractNumber
	 *            the contractNumber to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	/**
	 * @return parent2
	 * @author zhiqiang.yang@biolims.cn
	 */
	public Instrument getParent2() {
		return parent2;
	}

	/**
	 * @param parent2
	 *            the parent2 to set
	 * @author zhiqiang.yang@biolims.cn
	 */
	public void setParent2(Instrument parent2) {
		this.parent2 = parent2;
	}

	/**
	 * @return the verify
	 */
	public String getVerify() {
		return verify;
	}

	/**
	 * @param verify
	 *            the verify to set
	 */
	public void setVerify(String verify) {
		this.verify = verify;
	}

}
