package com.biolims.equipment.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dic.model.DicFinanceItem;
import com.biolims.dic.model.DicType;
import com.biolims.core.model.user.Department;
import com.biolims.common.model.user.User;
import com.biolims.core.model.user.UserGroup;
import com.biolims.dao.EntityDao;
import com.biolims.storage.model.Storage;

/**
 * 设备维护
 * @author Vera
 */
@Entity
@Table(name = "T_INSTRUMENT_REPAIR")
public class InstrumentRepair extends EntityDao<Instrument> implements Serializable {

	private static final long serialVersionUID = 2191801684912559179L;
	@Id
	@Column(length = 32)
	private String id;//ID

	@Column(length = 110)
	private String note;//维修方式及备注

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private InstrumentFault instrumentFault;//故障通知单号

	@Column(length = 100)
	private String name;//描述

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private DicType type;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private Department department;//所属部门

	private Date startDate;//计划开始时间

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private DicFinanceItem materialCode;//材料科目

	private Date endDate;//计划完成时间

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private DicFinanceItem personCode;//人员科目

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private DicFinanceItem repairCode;//维护科目

	private Date factStartDate;//维修时间

	private Date factEndDate;//实际完成时间

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private User dutyUser;//保管人
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private User confirmUser;//批准人
	private Date createDate;//创建日期

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private User createUser;//保养维护人

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private UserGroup repairTeam;//维护组
	private Date confirmDate;//批准时间
	private Double fee;

	@Column(length = 32)
	private String state;// 状态

	@Column(length = 200)
	private String stateName;
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private InstrumentRepairPlan instrumentRepairPlan;//预防维护单

	@Column(length = 400)
	private String maintain;//保养维护情况

	@Column(length = 400)
	private String verify;//维修单位
	
	@Column(length = 400)
	private String repairPrice;//维修价格
	
	@Column(length = 32)
	private String p_type;// 区分维护类型
	/** 范围Id */
	private String scopeId;
	/** 范围 */
	private String scopeName;
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public UserGroup getRepairTeam() {
		return repairTeam;
	}

	public void setRepairTeam(UserGroup repairTeam) {
		this.repairTeam = repairTeam;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public InstrumentFault getInstrumentFault() {
		return instrumentFault;
	}

	public void setInstrumentFault(InstrumentFault instrumentFault) {
		this.instrumentFault = instrumentFault;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public DicType getType() {
		return type;
	}

	public void setType(DicType type) {
		this.type = type;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public DicFinanceItem getMaterialCode() {
		return materialCode;
	}

	public void setMaterialCode(DicFinanceItem materialCode) {
		this.materialCode = materialCode;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public DicFinanceItem getPersonCode() {
		return personCode;
	}

	public void setPersonCode(DicFinanceItem personCode) {
		this.personCode = personCode;
	}

	public DicFinanceItem getRepairCode() {
		return repairCode;
	}

	public void setRepairCode(DicFinanceItem repairCode) {
		this.repairCode = repairCode;
	}

	public Date getFactStartDate() {
		return factStartDate;
	}

	public void setFactStartDate(Date factStartDate) {
		this.factStartDate = factStartDate;
	}

	public Date getFactEndDate() {
		return factEndDate;
	}

	public void setFactEndDate(Date factEndDate) {
		this.factEndDate = factEndDate;
	}

	public User getDutyUser() {
		return dutyUser;
	}

	public void setDutyUser(User dutyUser) {
		this.dutyUser = dutyUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public User getCreateUser() {
		return createUser;
	}

	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	public InstrumentRepairPlan getInstrumentRepairPlan() {
		return instrumentRepairPlan;
	}

	public void setInstrumentRepairPlan(InstrumentRepairPlan instrumentRepairPlan) {
		this.instrumentRepairPlan = instrumentRepairPlan;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public Double getFee() {
		return fee;
	}

	public void setFee(Double fee) {
		this.fee = fee;
	}

	public User getConfirmUser() {
		return confirmUser;
	}

	public void setConfirmUser(User confirmUser) {
		this.confirmUser = confirmUser;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	public String getMaintain() {
		return maintain;
	}

	public void setMaintain(String maintain) {
		this.maintain = maintain;
	}

	public String getVerify() {
		return verify;
	}

	public void setVerify(String verify) {
		this.verify = verify;
	}

	public String getP_type() {
		return p_type;
	}

	public void setP_type(String p_type) {
		this.p_type = p_type;
	}

	public String getRepairPrice() {
		return repairPrice;
	}

	public void setRepairPrice(String repairPrice) {
		this.repairPrice = repairPrice;
	}

	public String getScopeId() {
		return scopeId;
	}

	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	public String getScopeName() {
		return scopeName;
	}

	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

}
