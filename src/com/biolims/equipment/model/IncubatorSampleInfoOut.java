package com.biolims.equipment.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.dao.EntityDao;
import com.biolims.experiment.dna.model.DnaTaskInfo;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.system.storage.model.StorageBox;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;

/**
 * @Title: Model
 * @Description: 培养箱库存样本
 * @author lims-platform
 * @date 2015-11-03 16:19:13
 * @version V1.0
 * 
 */
@Entity
@Table(name = "INCUBATOR_SAMPLE_INFO_OUT")
@SuppressWarnings("serial")
public class IncubatorSampleInfoOut extends EntityDao<IncubatorSampleInfoOut> implements java.io.Serializable {
	/** 库存编号 */
	private String id;
	/** 样本编号/批次号  */
	private String batch;
	/** 入库任务单编号 */
	private String taskId;
	/** 入库步骤号 */
	private String taskStepNum;
	/** 二氧化碳培养箱编号 */
	private String incubatorId;
	/** 储位 */
	private String location;
	// 出库人
	private User sampleOutUser;
	// 出库时间
	private Date sampleOutDate;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_IN_USER")
	public User getSampleOutUser() {
		return sampleOutUser;
	}

	public void setSampleOutUser(User sampleOutUser) {
		this.sampleOutUser = sampleOutUser;
	}

	public Date getSampleOutDate() {
		return sampleOutDate;
	}

	public void setSampleOutDate(Date sampleOutDate) {
		this.sampleOutDate = sampleOutDate;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getTaskStepNum() {
		return taskStepNum;
	}

	public void setTaskStepNum(String taskStepNum) {
		this.taskStepNum = taskStepNum;
	}

	public String getIncubatorId() {
		return incubatorId;
	}

	public void setIncubatorId(String incubatorId) {
		this.incubatorId = incubatorId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 入库明细id
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             入库明细id
	 */
	public void setId(String id) {
		this.id = id;
	}


	/**
	 * 方法: 取得String
	 * 
	 * @return: String 储位
	 */
	@Column(name = "LOCATION", length = 50)
	public String getLocation() {
		return this.location;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             储位
	 */
	public void setLocation(String location) {
		this.location = location;
	}



}