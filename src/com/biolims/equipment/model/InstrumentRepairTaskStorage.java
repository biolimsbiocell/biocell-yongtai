package com.biolims.equipment.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.storage.model.Storage;

/**
 * 维修材料
 * @author Vera
 */
@Entity
@Table(name = "T_INSTRUMENT_R_T_STORAGE")
public class InstrumentRepairTaskStorage extends EntityDao<InstrumentRepairTaskStorage> implements Serializable {

	private static final long serialVersionUID = 2191801684912559179L;
	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(length = 32)
	private String id;//ID

	@Column(length = 110)
	private String note;//描述

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private InstrumentFault instrumentFault;//故障通知单号

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private Storage storage;//设备编号

	@Column(length = 100)
	private String name;//名称

	private Double price;//价格

	private Double num;//数量

	private Double factNum;//实际数量

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "INSTRUMENT_R_T")
	private InstrumentRepairTask instrumentRepairTask;//任务

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public InstrumentFault getInstrumentFault() {
		return instrumentFault;
	}

	public void setInstrumentFault(InstrumentFault instrumentFault) {
		this.instrumentFault = instrumentFault;
	}

	public Storage getStorage() {
		return storage;
	}

	public void setStorage(Storage storage) {
		this.storage = storage;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getNum() {
		return num;
	}

	public void setNum(Double num) {
		this.num = num;
	}

	public Double getFactNum() {
		return factNum;
	}

	public void setFactNum(Double factNum) {
		this.factNum = factNum;
	}

	public InstrumentRepairTask getInstrumentRepairTask() {
		return instrumentRepairTask;
	}

	public void setInstrumentRepairTask(InstrumentRepairTask instrumentRepairTask) {
		this.instrumentRepairTask = instrumentRepairTask;
	}

}
