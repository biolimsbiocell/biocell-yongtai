package com.biolims.equipment.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.storage.model.Storage;

/**
 * 报修明细
 * @author Vera
 */
@Entity
@Table(name = "T_INSTRUMENT_FAULT_DETAIL")
public class InstrumentFaultDetail extends EntityDao<Instrument> implements Serializable {

	private static final long serialVersionUID = 3070985758044624409L;

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(length = 32)
	private String id;//任务编号

	@Column(length = 4000)
	private String note;//备注

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private InstrumentFault instrumentFault;//报修单号
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private Instrument instrument;//设备编号
	
	public Instrument getInstrument() {
		return instrument;
	}

	public void setInstrument(Instrument instrument) {
		this.instrument = instrument;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}


	public InstrumentFault getInstrumentFault() {
		return instrumentFault;
	}

	public void setInstrumentFault(InstrumentFault instrumentFault) {
		this.instrumentFault = instrumentFault;
	}

}
