package com.biolims.equipment.borrow.dao;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.common.dao.CommonDAO;
import com.biolims.equipment.model.InstrumentBorrow;
import com.biolims.equipment.model.InstrumentBorrowDetail;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class InstrumentBorrowDao extends CommonDAO {
	
	public List<InstrumentBorrowDetail> findInstrumentBorrowDetail(String ib) throws Exception{
		String hql=" from InstrumentBorrowDetail where instrumentBorrow.id ='"+ib+"'";
		String key="";
		List<InstrumentBorrowDetail> list=new ArrayList<InstrumentBorrowDetail>();
		list = this.getSession().createQuery(hql + key).list();
		return list;
	}

	public Map<String, Object> findInstrumentBorrowTable(Integer start,Integer length, String query, String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from  InstrumentBorrow where 1=1";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from InstrumentBorrow where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<InstrumentBorrow> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}


	public Map<String, Object> findInstrumentBorrowDetailTable(Integer start, Integer length, String query, String col, String sort, String id) throws Exception {
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from InstrumentBorrowDetail where 1=1 and instrumentBorrow.id='"+id+"'";
			String key = "";

			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
				String hql = "from InstrumentBorrowDetail where 1=1 and instrumentBorrow.id='"+id+"' ";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					col=col.replace("-", ".");
					key+=" order by "+col+" "+sort;
				}
				List<InstrumentBorrowDetail> list = new ArrayList<InstrumentBorrowDetail>();
				list=this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
			}
			return map;
		}

	public List<InstrumentBorrowDetail> findInstrumentFault(String idd,String inId) {
		 String hql="from InstrumentBorrowDetail where 1=1 and instrument.id='"+idd+"'and instrumentBorrow='"+inId+"' ";
		 List<InstrumentBorrowDetail> list= this.getSession().createQuery(hql).list();
		return list;
	}
}
