/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：EVENT
 * 类描述：领用事件
 * 创建人：倪毅
 * 创建时间：2012-02
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.equipment.borrow.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.equipment.borrow.service.InstrumentBorrowService;

public class InstrumentBorrowSetInstrumentEvent implements ObjectEvent {
	public String operation(String applicationTypeActionId, String formId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		InstrumentBorrowService aService = (InstrumentBorrowService) ctx.getBean("instrumentBorrowService");
		aService.instrumentBorrowSetInstrument(formId);
		return "";
	}
}
