/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：InstrumentBorrowService
 * 类描述：设备借用管理service
 * 创建人：倪毅
 * 创建时间：2011-12
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.equipment.borrow.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.applicationType.service.ApplicationTypeService;
import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.common.service.SystemCodeService;
import com.biolims.dic.model.DicState;
import com.biolims.equipment.borrow.dao.InstrumentBorrowDao;
import com.biolims.equipment.common.constants.SystemCode;
import com.biolims.equipment.model.Instrument;
import com.biolims.equipment.model.InstrumentBorrow;
import com.biolims.equipment.model.InstrumentBorrowDetail;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;
import com.biolims.workflow.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

@Service
public class InstrumentBorrowService extends ApplicationTypeService {

	@Resource
	private CommonDAO commonDAO;

	@Resource
	private InstrumentBorrowDao instrumentBorrowDao;
	@Resource
	private SystemCodeService systemCodeService;

	/**
	 * 检索实验,采用map方式传递检索参数
	 * 
	 * @return 参数值MAP，如果传递的为null返回空字符串
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> findInstrumentBorrowList(int startNum, int limitNum, String dir, String sort,
			String contextPath, String data, String rightsId, String curUserId) throws Exception {
		// 检索条件map,每个字段检索值
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		// 检索方式map,每个字段的检索方式
		Map<String, String> mapForCondition = new HashMap<String, String>();
		InstrumentBorrow experiment = new InstrumentBorrow();
		if (data != null && !data.equals("")) {
			mapForCondition.put("id", "like");
			mapForCondition.put("name", "like");
			Map<String, Object> map = JsonUtils.toObjectByJson(data, Map.class);
			experiment = (InstrumentBorrow) commonDAO.Map2Bean(map, experiment);
			// mapForQuery = BeanUtils.po2MapNotNull(mapForCondition, experiment);

			BeanUtils.data2Map(data, mapForQuery, mapForCondition);

		}

		// 将map值传入，获取列表
		Map<String, Object> controlMap = commonDAO.findObjectCondition(startNum, limitNum, dir, sort,
				InstrumentBorrow.class, mapForQuery, mapForCondition);
		List<InstrumentBorrow> list = (List<InstrumentBorrow>) controlMap.get("list");
		controlMap.put("list", list);
		return controlMap;
	}

	/**
	 * 保存
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void save(InstrumentBorrow an) throws Exception {
		commonDAO.saveOrUpdate(an);

	}

	public InstrumentBorrow getInstrumentBorrow(String id) throws Exception {
		InstrumentBorrow animal = commonDAO.get(InstrumentBorrow.class, id);
		return animal;
	}

	/**
	 * 保存
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void instrumentBorrowSetInstrument(String formId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext.getServletContext());

		CommonService commonService = (CommonService) ctx.getBean("commonService");

		InstrumentBorrow ib = commonService.get(InstrumentBorrow.class, formId);
		ib.setState("1");
		ib.setStateName(WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		List<InstrumentBorrowDetail> list = instrumentBorrowDao.findInstrumentBorrowDetail(ib.getId());
		for (InstrumentBorrowDetail ibd : list) {
			Instrument s = commonService.get(Instrument.class, ibd.getInstrument().getId());
			if (ib.getNewDutyUser() != null)
				s.setDutyUser(ib.getNewDutyUser());
			if (ib.getNewDepartment() != null)
				s.setDepartment(ib.getNewDepartment());
			if (ib.getNewState() != null)
				s.setState(ib.getNewState());
			s.setRoomManagement(ibd.getRoomManagement());
			commonService.saveOrUpdate(s);
		}
	}

	// 保存子表信息
	@WriteOperLog
	@WriteExOperLog
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void saveInstrumentBorrowDetail(String id, String data) throws Exception {
		InstrumentBorrow ibs = commonDAO.get(InstrumentBorrow.class, id);
		// 将json读入map
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(data, List.class);
		for (Map<String, Object> map : list) {
			InstrumentBorrowDetail ibd = new InstrumentBorrowDetail();
			// Instrument ins=new Instrument();
			// 将map信息读入实体类
			ibd = (InstrumentBorrowDetail) commonDAO.Map2Bean(map, ibd);
			// InstrumentRepair ep = new InstrumentRepair();
			// em.setNote(em.getNote().replace("<br>", "\\n\\r"));
			// ep.setId(id);
			ibd.setInstrumentBorrow(ibs);
			commonDAO.save(ibd);
		}
	}

	/*
	 * //删除子表信息 public void delInstrumentBorrowDetail(String[] ids) { for (String id
	 * : ids) { InstrumentBorrowDetail ppi = new InstrumentBorrowDetail();
	 * ppi.setId(id); commonDAO.delete(ppi); } }
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> findInstrumentBorrowDetailList(int startNum, int limitNum, String dir, String sort,
			String contextPath, String instrumentBorrowId) throws Exception {
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		Map<String, String> mapForCondition = new HashMap<String, String>();
		if (instrumentBorrowId != null && !instrumentBorrowId.equals("")) {
			mapForQuery.put("instrumentBorrow.id", instrumentBorrowId);
			mapForCondition.put("instrumentBorrow.id", "=");
		}

		Map<String, Object> controlMap = commonDAO.findObjectCondition(startNum, limitNum, dir, sort,
				InstrumentBorrowDetail.class, mapForQuery, mapForCondition);

		List<InstrumentBorrowDetail> list = (List<InstrumentBorrowDetail>) controlMap.get("list");
		controlMap.put("list", list);
		return controlMap;
	}

	public Map<String, Object> findInstrumentBorrowTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return instrumentBorrowDao.findInstrumentBorrowTable(start, length, query, col, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveInstrumentBorrowDetail(InstrumentBorrow sc, String itemDataJson, String logInfo,String log) throws Exception {
		sc.setHandleDate(new Date());
		commonDAO.saveOrUpdate(sc);
		List<InstrumentBorrowDetail> saveItems = new ArrayList<InstrumentBorrowDetail>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		if (list.size() > 0) {
			for (Map<String, Object> map : list) {
				InstrumentBorrowDetail scp = new InstrumentBorrowDetail();
				// 将map信息读入实体类
				scp = (InstrumentBorrowDetail) instrumentBorrowDao.Map2Bean(map, scp);
				InstrumentBorrowDetail ibd = commonDAO.get(InstrumentBorrowDetail.class, scp.getId());
				Instrument s = ibd.getInstrument();
				// Instrument s = commonDAO.get(Instrument.class, scp.getInstrument().getId());
				if (null != sc.getNewState())
					s.setState(sc.getNewState());
				commonDAO.saveOrUpdate(s);
				if (scp.getId() != null && scp.getId().equals(""))
					scp.setId(null);
				scp.setInstrumentBorrow(sc);
				// saveItems.add(scp);
				scp.setInstrument(s);
				commonDAO.merge(scp);
			}
		} else {

			List<InstrumentBorrowDetail> itemList = instrumentBorrowDao.findInstrumentBorrowDetail(sc.getId());
			for (InstrumentBorrowDetail ibd : itemList) {
				Instrument s = commonDAO.get(Instrument.class, ibd.getInstrument().getId());
				if (sc.getNewState() != null)
					s.setState(sc.getNewState());
				commonDAO.saveOrUpdate(s);
				saveItems.add(ibd);
			}
		}
		instrumentBorrowDao.saveOrUpdateAll(saveItems);
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("InstrumentBorrow");
			li.setModifyContent(logInfo);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delInstrumentBorrowDetail(String[] ids) throws Exception {
		for (String id : ids) {
			InstrumentBorrowDetail scp = instrumentBorrowDao.get(InstrumentBorrowDetail.class, id);
			instrumentBorrowDao.delete(scp);
			if (ids.length > 0) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(scp.getInstrumentBorrow().getId());
				li.setClassName("InstrumentBorrow");
				li.setModifyContent("设备调整:"+"设备编号:"+scp.getInstrument().getId()+"设备名称:"+scp.getInstrument().getName()+"的数据已删除");
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}
		}

	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(InstrumentBorrow sc, Map jsonMap, String logInfo, Map logMap,String log) throws Exception {
		if (sc != null) {
			sc.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			sc.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			instrumentBorrowDao.saveOrUpdate(sc);

			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(sc.getId());
				li.setClassName("InstrumentBorrow");
				li.setModifyContent(logInfo);
				if("123".equals(log)) {
					li.setState("1");
					li.setStateName("数据新增");
				}else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				commonDAO.saveOrUpdate(li);
			}

			String jsonStr = "";
			String logStr = "";
			logStr = (String) logMap.get("instrumentBorrowDetail");
			jsonStr = (String) jsonMap.get("instrumentBorrowDetail");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveInstrumentBorrowDetail(sc, jsonStr, logStr,log);
			} else {
				List<InstrumentBorrowDetail> list = instrumentBorrowDao.findInstrumentBorrowDetail(sc.getId());
				for (InstrumentBorrowDetail ibd : list) {
					Instrument s = commonDAO.get(Instrument.class, ibd.getInstrument().getId());
					if (sc.getNewState() != null)
						s.setState(sc.getNewState());
					commonDAO.saveOrUpdate(s);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> findInstrumentBorrowDetailTable(Integer start, Integer length, String query, String col,
			String sort, String id) throws Exception {
		Map<String, Object> result = instrumentBorrowDao.findInstrumentBorrowDetailTable(start, length, query, col,
				sort, id);
		List<InstrumentBorrowDetail> list = (List<InstrumentBorrowDetail>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String addInstrument(String[] ids, String note, String instrId, String createUser, String state,
			String newStateId) throws Exception {

		// 保存主表信息
		DicState newState = commonDAO.get(DicState.class, newStateId);
		InstrumentBorrow si = new InstrumentBorrow();
		String log = "";
		if (instrId == null || instrId.length() <= 0 || "NEW".equals(instrId)) {
			log = "123";
			String code = systemCodeService.getSystemCode(SystemCode.INSTRUMENT_BORROW_NAME,
					SystemCode.INSTRUMENT_BORROW_CODE, null, null);
			si.setId(code);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			si.setCreateUser(u);
			si.setAppleUser(u);
			si.setState("3");
			si.setStateName("NEW");
			si.setCreateDate(new Date());
			si.setHandleDate(new Date());
			si.setNewState(newState);
			si.setNote(note);
			instrId = si.getId();
			si.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			si.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			commonDAO.saveOrUpdate(si);
		} else {
			si = commonDAO.get(InstrumentBorrow.class, instrId);
		}
		for (int i = 0; i < ids.length; i++) {
			String id = ids[i];
			List<InstrumentBorrowDetail> list = instrumentBorrowDao.findInstrumentFault(id,instrId);
			if(list.size()>0) {
				return null;
			}else {
				// 通过id查询库存主数据
				Instrument s = commonDAO.get(Instrument.class, id);
				if (null != newState) {
					s.setState(newState);
				}

				InstrumentBorrowDetail sii = new InstrumentBorrowDetail();
				if (null != s) {
					sii.setInstrument(s);
				}
				sii.setRoomManagement(s.getRoomManagement());
				sii.setInstrumentBorrow(si);
				commonDAO.saveOrUpdate(s);
				commonDAO.saveOrUpdate(sii);
				
				String kucun = "设备调整:"+"设备编号:"+s.getId()+"设备名称:"+s.getName()+"的数据已添加到明细";
				if (kucun != null && !"".equals(kucun)) {
					LogInfo li = new LogInfo();
					User u = (User) ServletActionContext.getRequest().getSession()
							.getAttribute(SystemConstants.USER_SESSION_KEY);
					li.setUserId(u.getId());
					li.setFileId(si.getId());
					li.setClassName("InstrumentBorrow");
					li.setLogDate(new Date());
					li.setModifyContent(kucun);
					if("123".equals(log)) {
						li.setState("1");
						li.setStateName("数据新增");
					}else {
						li.setState("3");
						li.setStateName("数据修改");
					}
					commonDAO.saveOrUpdate(li);
				}
			}
		}
		return instrId;

	}

}
