/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：Action
 * 类描述：设备调拨管理
 * 创建人：倪毅
 * 创建时间：2011-12
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.equipment.borrow.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.common.service.SystemCodeService;
import com.biolims.equipment.borrow.service.InstrumentBorrowService;
import com.biolims.equipment.common.constants.SystemCode;
import com.biolims.equipment.common.constants.SystemConstants;
import com.biolims.equipment.model.InstrumentBorrow;
import com.biolims.equipment.model.InstrumentBorrowDetail;
import com.biolims.equipment.model.InstrumentRepairTask;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.opensymphony.xwork2.ActionContext;

@Controller
@Scope("protod")
@ParentPackage("default")
@Namespace("/equipment/borrow")
@SuppressWarnings("unchecked")
public class InstrumentBorrowAction extends BaseActionSupport {

	private static final long serialVersionUID = -164661537469123056L;

	// 用于页面上显示模块名称
	private String title = "设备调整";

	// 该action权限id
	private String rightsId = "405";

	private InstrumentBorrow instrumentBorrow = new InstrumentBorrow();

	@Autowired
	private InstrumentBorrowService instrumentBorrowService;

	@Resource
	private SystemCodeService systemCodeService;
	/* NEW */
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CommonService commonService;
	@Resource
	private FieldService fieldService;
	@Resource
	private CommonDAO commonDAO;
	private String log ="";
	
	@Action(value = "showInstrumentBorrowList")
	public String showInstrumentBorrowList() throws Exception {
		rightsId = "405";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/equipment/borrow/instrumentBorrow.jsp");
	}

	@Action(value = "showInstrumentBorrowEditList")
	public String showInstrumentBorrowEditList() throws Exception {
		rightsId = "405";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/equipment/borrow/instrumentBorrowEditList.jsp");
	}

	@Action(value = "showInstrumentBorrowTableJson")
	public void showInstrumentBorrowTableJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = instrumentBorrowService.findInstrumentBorrowTable(start, length, query, col,
					sort);
			Long count = (Long) result.get("total");
			List<InstrumentBorrow> list = (List<InstrumentBorrow>) result.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("appleUser-id", "");
			map.put("appleUser-name", "");
			map.put("stateName", "");
			map.put("newState-id", "");
			map.put("newState-name", "");
			map.put("note", "");
			map.put("handleDate", "yyyy-MM-dd");

			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("InstrumentBorrow");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "instrumentBorrowSelectTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogInstrumentBorrowList() throws Exception {
		rightsId = "405";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/equipment/borrow/instrumentBorrowSelectTable.jsp");
	}

	@Action(value = "editInstrumentBorrow")
	public String editInstrumentBorrow() throws Exception {
		rightsId = "405";
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			instrumentBorrow = instrumentBorrowService.getInstrumentBorrow(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "instrumentBorrow");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			this.instrumentBorrow.setId(SystemCode.DEFAULT_SYSTEMCODE);
			instrumentBorrow.setCreateUser(user);
			instrumentBorrow.setCreateDate(new Date());
			instrumentBorrow.setAppleUser(user);
			instrumentBorrow.setHandleDate(new Date());
			instrumentBorrow.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			instrumentBorrow.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			instrumentBorrow.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			instrumentBorrow.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));

			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(instrumentBorrow.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/equipment/borrow/instrumentBorrowEdit.jsp");
	}

	@Action(value = "copyInstrumentBorrow")
	public String copyInstrumentBorrow() throws Exception {
		rightsId = "405";
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		instrumentBorrow = instrumentBorrowService.getInstrumentBorrow(id);
		instrumentBorrow.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/equipment/borrow/instrumentBorrowEdit.jsp");
	}

	@Action(value = "save")
	public void save() throws Exception {

		String msg = "";
		String zId = "";
		boolean bool = true;

		try {
			String dataValue = getParameterFromRequest("dataValue");
			String str = "[" + dataValue + "]";
			String changeLog = getParameterFromRequest("changeLog");
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str, List.class);

			for (Map<String, Object> map : list) {
				instrumentBorrow = (InstrumentBorrow) commonDAO.Map2Bean(map, instrumentBorrow);
			}
			String id = instrumentBorrow.getId();
			if (id != null && id.equals("")) {
				instrumentBorrow.setId(null);
			}
			if (id == null || id.length() <= 0 || SystemCode.DEFAULT_SYSTEMCODE.equals(id)) {
				log ="123";
				String code = systemCodeService.getSystemCode(SystemCode.INSTRUMENT_BORROW_NAME,
						SystemCode.INSTRUMENT_BORROW_CODE, null, null);
				this.instrumentBorrow.setId(code);
			}
			Map aMap = new HashMap();
			Map lMap = new HashMap();
			aMap.put("instrumentBorrowDetail", getParameterFromRequest("instrumentBorrowDetailJson"));

			instrumentBorrowService.save(instrumentBorrow, aMap, changeLog, lMap,log);

			zId = instrumentBorrow.getId();
		} catch (Exception e) {
			e.printStackTrace();
			msg = "保存失败！";
			bool = false;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("msg", msg);
		map.put("success", bool);
		map.put("id", zId);

		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		if (bpmTaskId != null && !bpmTaskId.equals("") && !bpmTaskId.equals("null")) {
			map.put("bpmTaskId", bpmTaskId);

		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "viewInstrumentBorrow")
	public String toViewInstrumentBorrow() throws Exception {
		rightsId = "405";
		String id = getParameterFromRequest("id");
		instrumentBorrow = instrumentBorrowService.getInstrumentBorrow(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/equipment/borrow/instrumentBorrowEdit.jsp");
	}

	@Action(value = "showInstrumentBorrowDetailTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showInstrumentBorrowDetailTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = instrumentBorrowService.findInstrumentBorrowDetailTable(start, length, query, col,
				sort, id);
		List<InstrumentBorrowDetail> list = (List<InstrumentBorrowDetail>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("instrument-id", "");
		map.put("instrument-name", "");
		map.put("instrument-effectiveDate", "yyyy-MM-dd");
		map.put("instrument-expiryDate", "yyyy-MM-dd");
		map.put("instrument-nextDate", "yyyy-MM-dd");
		map.put("instrument-spec", "");
		map.put("instrument-state-name", "");
		map.put("note", "");
		map.put("roomManagement-id", "");
		map.put("roomManagement-roomName", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delInstrumentBorrowDetail")
	public void delInstrumentBorrowDetail() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			instrumentBorrowService.delInstrumentBorrowDetail(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "saveInstrumentBorrowDetailTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveInstrumentBorrowDetailTable() throws Exception {
		Map aMap = new HashMap();
		Map lMap = new HashMap();

		String id = getParameterFromRequest("id");
		String changeLogItem = getParameterFromRequest("logInfo");
		instrumentBorrow = commonService.get(InstrumentBorrow.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		aMap.put("instrumentBorrowDetail", getParameterFromRequest("dataJson"));
		lMap.put("instrumentBorrowDetail", getParameterFromRequest("changeLog"));
		try {
			instrumentBorrowService.save(instrumentBorrow, aMap, changeLogItem, lMap,log);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "saveInstrumentBorrowTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveInstrumentBorrowTable() throws Exception {
		String dataValue = getParameterFromRequest("dataJson");
		String changeLog = getParameterFromRequest("changeLog");

		String str = "[" + dataValue + "]";

		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str, List.class);

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			for (Map<String, Object> map1 : list) {

				InstrumentBorrow a = new InstrumentBorrow();

				Map aMap = new HashMap();
				Map lMap = new HashMap();
				a = (InstrumentBorrow) commonDAO.Map2Bean(map1, a);
				a.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				a.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));

				instrumentBorrowService.save(a, aMap, changeLog, lMap,log);
				map.put("id", a.getId());
			}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/* 添加设备到维修计划左侧表 */
	@Action(value = "addInstrument")
	public void addInstrumentRepairPlanTask() throws Exception {
		String note = getParameterFromRequest("note");
		String instrId = getParameterFromRequest("id");
		String createUser = getParameterFromRequest("createUser");
		String state = getParameterFromRequest("state");
		String newStateId = getParameterFromRequest("newStateId");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String instrumentBorrowId = instrumentBorrowService.addInstrument(ids, note, instrId, createUser, state,
					newStateId);
			if(instrumentBorrowId!=null) {
				result.put("success", true);
				result.put("data", instrumentBorrowId);
			}else {
				result.put("success", true);
				result.put("zy", "1");
			}
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/* old */
	/**
	 * 访问列表
	 */

	// @Action(value = "showInstrumentBorrowList")
	public String showExperimentList() throws Exception {
		// LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		// map.put("id", new String[] { "", "string", "", "设备调整编号", "120", "true", "",
		// "", "", "", "", "" });
		// map.put("storage-id", new String[] { "", "string", "", "调整设备", "100", "true",
		// "", "", "", "", "", "" });
		// map.put("storage-note", new String[] { "", "string", "", "设备描述", "250",
		// "true", "", "", "", "", "", "" });
		// map.put("appleUser-name", new String[] { "", "string", "", "申请人", "150",
		// "true", "", "", "", "", "", "" });
		// map.put("stateName", new String[] { "", "string", "", "工作流状态", "120", "true",
		// "", "", "", "", "", "" });
		// map.put("dutyUser-name", new String[] { "", "string", "", "负责人", "150",
		// "true", "true", "", "", "", "", "" });
		// map.put("newState-name", new String[] { "", "string", "", "调整类型", "150",
		// "true", "false", "", "", "", "", "" });
		//
		// map.put("note", new String[] { "", "string", "", "调整原因", "200", "true",
		// "false", "", "", "", "", "" });
		// String type = generalexttype(map);
		// String col = generalextcol(map);
		// putObjToContext("type", type);
		// putObjToContext("col", col);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		putObjToContext("path",
				ServletActionContext.getRequest().getContextPath()
						+ "/equipment/borrow/showInstrumentBorrowListJson.action?queryMethod="
						+ getParameterFromRequest("queryMethod"));
		// 导向jsp页面显示
		return dispatcher("/WEB-INF/page/equipment/borrow/showInstrumentBorrowList.jsp");
	}

	@Action(value = "showInstrumentBorrowListJson")
	public void showInstrumentBorrowListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		// 取出检索用的对象
		String data = getParameterFromRequest("data");
		String queryMethod = getParameterFromRequest("queryMethod");
		Map r = this.toMakeSessionQuery(data, "InstrumentBorrow", dir, sort, queryMethod);
		dir = (String) r.get("dir");
		sort = (String) r.get("sort");
		data = (String) r.get("queryData");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		Map<String, Object> controlMap = instrumentBorrowService.findInstrumentBorrowList(startNum, limitNum, dir, sort,
				getContextPath(), data, rightsId, user.getId());
		long totalCount = Long.parseLong(controlMap.get("totalCount").toString());
		List<InstrumentBorrow> list = (List<InstrumentBorrow>) controlMap.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("appleUser-name", "");
		map.put("stateName", "");
		map.put("dutyUser-name", "");
		map.put("oldDepartment-name", "");
		map.put("oldFinanceCode-name", "");
		map.put("type-name", "");
		map.put("newFinanceCode-name", "");
		map.put("newDepartment-name", "");
		map.put("newDutyUser-name", "");
		map.put("newDepartment-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("handleDate", "yyyy-MM-dd");
		map.put("newState-name", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}

	/**
	 * 设备列表数据
	 */
	@Action(value = "showInstrumentBorrowDetailListJson")
	public void showInstrumentFaultListDetailJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String instrumentBorrowId = getParameterFromRequest("instrumentBorrowId");
		List<InstrumentRepairTask> list = null;
		long totalCount = 0;
		putObjToContext("instrumentBorrowId", instrumentBorrowId);

		if (instrumentBorrowId != null && !instrumentBorrowId.equals("")) {
			Map<String, Object> controlMap = instrumentBorrowService.findInstrumentBorrowDetailList(startNum, limitNum,
					dir, sort, getContextPath(), instrumentBorrowId);
			totalCount = Long.parseLong(controlMap.get("totalCount").toString());
			list = (List<InstrumentRepairTask>) controlMap.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("instrument-id", "");
			map.put("instrument-name", "");
			map.put("instrument-effectiveDate", "yyyy-MM-dd");
			map.put("instrument-expiryDate", "yyyy-MM-dd");
			map.put("instrument-nextDate", "yyyy-MM-dd");
			map.put("instrument-spec", "");
			map.put("instrument-state-name", "");
			map.put("note", "");

			new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
		}
	}

	/**
	 * 访问编辑页面
	 */

	@Action(value = "toEditInstrumentBorrow")
	public String toEditInstrumentBorrow() throws Exception {
		String id = getParameterFromRequest("id");

		if (id != null && !id.equals("")) {
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			instrumentBorrow = instrumentBorrowService.getInstrumentBorrow(id);
			putObjToContext("instrumentBorrowId", id);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toState(instrumentBorrow.getState());

		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			this.instrumentBorrow.setId(SystemCode.DEFAULT_SYSTEMCODE);
			instrumentBorrow.setCreateUser(user);
			instrumentBorrow.setCreateDate(new Date());
			instrumentBorrow.setAppleUser(user);
			instrumentBorrow.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			instrumentBorrow.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toState(instrumentBorrow.getState());
		}
		return dispatcher("/WEB-INF/page/equipment/borrow/editInstrumentBorrow.jsp");
	}

	/**
	 * 访问编辑页面
	 */
	/*
	 * @Action(value = "toCopyInstrumentBorrow") public String
	 * toCopyInstrumentBorrow() throws Exception { String id =
	 * getParameterFromRequest("id"); String handlemethod = ""; if (id != null &&
	 * !id.equals("")) { instrumentBorrow =
	 * instrumentBorrowService.getInstrumentBorrow(id); handlemethod =
	 * SystemConstants.PAGE_HANDLE_METHOD_MODIFY; toToolBar(rightsId, "", "",
	 * handlemethod); } else { User user = (User)
	 * this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
	 * instrumentBorrow.setCreateUser(user); instrumentBorrow.setCreateDate(new
	 * Date());
	 * instrumentBorrow.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW
	 * ); instrumentBorrow.setAppleUser(user);
	 * instrumentBorrow.setStateName(com.biolims.workflow.WorkflowConstants.
	 * WORKFLOW_NEW_NAME); putObjToContext("handlemethod",
	 * SystemConstants.PAGE_HANDLE_METHOD_ADD); toToolBar(rightsId, "", "",
	 * SystemConstants.PAGE_HANDLE_METHOD_ADD);
	 * toState(instrumentBorrow.getState()); toSetStateCopy(); } return
	 * dispatcher("/WEB-INF/page/equipment/borrow/editInstrumentBorrow.jsp"); }
	 */

	/**
	 * 访问查看页面
	 */

	/*
	 * @Action(value = "toViewInstrumentBorrow") public String
	 * toViewInstrumentBorrow() throws Exception { String id =
	 * getParameterFromRequest("id");
	 * 
	 * if (id != null && !id.equals("")) { putObjToContext("handlemethod",
	 * SystemConstants.PAGE_HANDLE_METHOD_VIEW); instrumentBorrow =
	 * instrumentBorrowService.getInstrumentBorrow(id);
	 * putObjToContext("instrumentBorrowId", id); } return
	 * dispatcher("/WEB-INF/page/equipment/borrow/editInstrumentBorrow.jsp"); }
	 */

	/**
	 * 保存
	 */
	/*
	 * @Action(value = "save") public String save() throws Exception { String data =
	 * getParameterFromRequest("jsonDataStr"); String id =
	 * this.instrumentBorrow.getId(); if (id == null || id.length() <= 0 ||
	 * SystemCode.DEFAULT_SYSTEMCODE.equals(id)) { String code =
	 * systemCodeService.getSystemCode(SystemCode.INSTRUMENT_BORROW_NAME,
	 * SystemCode.INSTRUMENT_BORROW_CODE, null, null);
	 * this.instrumentBorrow.setId(code); }
	 * instrumentBorrowService.save(instrumentBorrow); if (data != null &&
	 * !data.equals(""))
	 * instrumentBorrowService.saveInstrumentBorrowDetail(instrumentBorrow.getId(),
	 * data); return redirect("/equipment/borrow/toEditInstrumentBorrow.action?id="
	 * + instrumentBorrow.getId()); }
	 */
	@Action(value = "saveInstrumentBorrowDetail")
	public void saveInstrumentBorrowDetail() throws Exception {
		String instrumentBorrowId = getParameterFromRequest("instrumentBorrowId");
		String json = getParameterFromRequest("data");
		instrumentBorrowService.saveInstrumentBorrowDetail(instrumentBorrowId, json);
	}

	/**
	 * 删除明细
	 */
	// @Action(value = "delInstrumentBorrowDetail")
	public void delInstrumentRepairTask() throws Exception {
		String[] ids = getRequest().getParameterValues("ids[]");
		instrumentBorrowService.delInstrumentBorrowDetail(ids);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public InstrumentBorrow getInstrumentBorrow() {
		return instrumentBorrow;
	}

	public void setInstrumentBorrow(InstrumentBorrow instrumentBorrow) {
		this.instrumentBorrow = instrumentBorrow;
	}

	public InstrumentBorrowService getInstrumentBorrowService() {
		return instrumentBorrowService;
	}

	public void setInstrumentBorrowService(InstrumentBorrowService instrumentBorrowService) {
		this.instrumentBorrowService = instrumentBorrowService;
	}

	public SystemCodeService getSystemCodeService() {
		return systemCodeService;
	}

	public void setSystemCodeService(SystemCodeService systemCodeService) {
		this.systemCodeService = systemCodeService;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
