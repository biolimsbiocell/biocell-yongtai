/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：Action
 * 类描述：预维护计划管理
 * 创建人：倪毅
 * 创建时间：2012-01
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.equipment.plan.action;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.common.service.SystemCodeService;
import com.biolims.dic.model.DicState;
import com.biolims.equipment.common.constants.SystemCode;
import com.biolims.equipment.common.constants.SystemConstants;
import com.biolims.equipment.model.Instrument;
import com.biolims.equipment.model.InstrumentRepairPlan;
import com.biolims.equipment.model.InstrumentRepairPlanTask;
import com.biolims.equipment.model.InstrumentRepairPlanTaskPerson;
import com.biolims.equipment.model.InstrumentRepairPlanTaskStorage;
import com.biolims.equipment.plan.service.InstrumentRepairPlanService;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.opensymphony.xwork2.ActionContext;

@Namespace("/equipment/plan")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public class InstrumentRepairPlanAction extends BaseActionSupport {

	private static final long serialVersionUID = -164661537469123056L;

	//该action权限id
	private String rightsId = "4033";

	private InstrumentRepairPlan instrumentRepairPlan = new InstrumentRepairPlan();

	@Autowired
	private InstrumentRepairPlanService instrumentRepairPlanService;

	@Resource
	private SystemCodeService systemCodeService;
/*NEW*/
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CommonService commonService;	
	@Resource
	private FieldService fieldService;
	@Resource
	private CommonDAO commonDAO;
	private String log = "";
	
	
	@Action(value = "showInstrumentRepairPlanList")
	public String showInstrumentRepairPlanList() throws Exception {
		rightsId = "4033";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/equipment/plan/instrumentRepairPlan.jsp");
	}

	@Action(value = "showInstrumentRepairPlanEditList")
	public String showInstrumentRepairPlanEditList() throws Exception {
		rightsId = "4033";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/equipment/plan/instrumentRepairPlanEditList.jsp");
	}
	
	

	@Action(value = "showInstrumentRepairPlanTableJson")
	public void showInstrumentRepairPlanTableJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {	
			Map<String, Object> result = instrumentRepairPlanService.findInstrumentRepairPlanTable(
					start, length, query, col, sort);
			Long count = (Long) result.get("total");
			List<InstrumentRepairPlan> list = (List<InstrumentRepairPlan>) result.get("list");
	
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("unit-id", "");
			map.put("unit-name", "");
			map.put("unitName", "");
			map.put("dutyUser-name", "");
			map.put("nextDate", "yyyy-MM-dd");
			map.put("note", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("type-id", "");
			map.put("type-name", "");
			map.put("stateName", "");
			map.put("effectiveDate", "yyyy-MM-dd");
			map.put("expiryDate", "yyyy-MM-dd");
			map.put("period", "");
				
			String data=new SendData().getDateJsonForDatatable(map, list);
			//根据模块查询自定义字段数据
			Map<String,Object> mapField = fieldService.findFieldByModuleValue("InstrumentRepairPlan");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result,dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Action(value = "instrumentRepairPlanSelectTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogInstrumentRepairPlanList() throws Exception {
		rightsId = "4033";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/equipment/plan/instrumentRepairPlanSelectTable.jsp");
	}
	@Action(value = "editInstrumentRepairPlan")
	public String editInstrumentRepairPlan() throws Exception {
		rightsId = "4033";
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			instrumentRepairPlan = instrumentRepairPlanService.getInstrumentRepairPlan(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "instrumentRepairPlan");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			DicState ds = new DicState();
			ds.setId("1q");
			instrumentRepairPlan.setState(ds);
			instrumentRepairPlan.setCreateUser(user);
			instrumentRepairPlan.setCreateDate(new Date());
			instrumentRepairPlan.setDutyUser(user);
			instrumentRepairPlan.setStateName(SystemConstants.DIC_STATE_NEW_NAME);
			instrumentRepairPlan.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			instrumentRepairPlan.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/equipment/plan/instrumentRepairPlanEdit.jsp");
	}
	@Action(value = "copyInstrumentRepairPlan")
	public String copyInstrumentRepairPlan() throws Exception {
		rightsId = "4033";
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		instrumentRepairPlan = instrumentRepairPlanService.getInstrumentRepairPlan(id);
		instrumentRepairPlan.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/equipment/plan/instrumentRepairPlanEdit.jsp");
	}

	@Action(value = "save")
	public void save() throws Exception {
		String msg = "";
		String zId = "";
		boolean bool = true;	
		
		try {
			String dataValue = getParameterFromRequest("dataValue");
			String str = "["+dataValue+"]";
			String changeLog = getParameterFromRequest("changeLog");
			String changeLogItem = getParameterFromRequest("changeLogItem");
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					str, List.class);
			
			for (Map<String, Object> map : list) {
				instrumentRepairPlan = (InstrumentRepairPlan)commonDAO.Map2Bean(map, instrumentRepairPlan);
			}
			String id = "";
			if(instrumentRepairPlan!=null) {
				id = instrumentRepairPlan.getId();
			}else {
				
			}
			if(id!=null&&id.equals("")){
				instrumentRepairPlan.setId(null);
			}
			if (id == null || id.length() <= 0 || SystemCode.DEFAULT_SYSTEMCODE.equals(id)) {
				log = "123";
				String code = systemCodeService.getSystemCode(SystemCode.INSTRUMENT_REPAIRPLAN_NAME,
						SystemCode.INSTRUMENT_REPAIRPLAN_CODE, SystemCode.INSTRUMENT_REPAIRPLAN_CODE_PREFIX,
						SystemCode.INSTRUMENT_REPAIRPLAN_CODE_LENGTH);
				this.instrumentRepairPlan.setId(code);
			}
			Map aMap = new HashMap();
			Map lMap = new HashMap();
				aMap.put("instrumentRepairPlanTask",getParameterFromRequest("instrumentRepairPlanTaskJson"));
			
			
			instrumentRepairPlanService.save(instrumentRepairPlan,aMap,changeLog,lMap,changeLogItem,log);
			
			zId = instrumentRepairPlan.getId();
		} catch (Exception e) {
			e.printStackTrace();
			msg = "保存失败！";
			bool = false;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("msg", msg);
		map.put("success", bool);
		map.put("id", zId);
	
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null")) {
			map.put("bpmTaskId", bpmTaskId);
			
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	@Action(value = "viewInstrumentRepairPlan")
	public String toViewInstrumentRepairPlan() throws Exception {
		rightsId = "4033";
		String id = getParameterFromRequest("id");
		instrumentRepairPlan = instrumentRepairPlanService.getInstrumentRepairPlan(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/equipment/plan/instrumentRepairPlanEdit.jsp");
	}

	@Action(value = "showInstrumentRepairPlanTaskTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showInstrumentRepairPlanTaskTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = instrumentRepairPlanService
				.findInstrumentRepairPlanTaskTable(start, length, query, col, sort,
						id);
		List<InstrumentRepairPlanTask> list=(List<InstrumentRepairPlanTask>) result.get("list");
		Map<String, String> map=new HashMap<String, String>();
		map.put("id", "");
		map.put("type-id", "");
		map.put("type-name", "");
		map.put("instrument-id", "");
		map.put("instrument-name", "");
		map.put("instrument-effectiveDate", "yyyy-MM-dd");
		map.put("instrument-expiryDate", "yyyy-MM-dd");
		map.put("instrument-useDate", "yyyy-MM-dd");
		map.put("instrument-nextDate", "yyyy-MM-dd");
		map.put("instrument-tsDate", "yyyy-MM-dd");
		map.put("instrument-spec", "");
		map.put("instrument-state-name", "");
		map.put("instrumentRepairPlan-id", "");
		map.put("instrumentRepairPlan-name", "");
		map.put("fee", "");
		map.put("note", "");
		map.put("tsTime", "");
		String data=new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw,result,data));
	}
	/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delInstrumentRepairPlanTask")
	public void delInstrumentRepairPlanTask() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			instrumentRepairPlanService.delInstrumentRepairPlanTask(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	@Action(value = "saveInstrumentRepairPlanTaskTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveInstrumentRepairPlanTaskTable() throws Exception {
		Map aMap = new HashMap();
		Map lMap = new HashMap();
		
		String id=getParameterFromRequest("id");
		String changeLogItem = getParameterFromRequest("changeLog");
		instrumentRepairPlan=commonService.get(InstrumentRepairPlan.class, id);
		Map<String, Object> map=new HashMap<String, Object>();
		aMap.put("instrumentRepairPlanTask",
				getParameterFromRequest("dataJson"));
		lMap.put("instrumentRepairPlanTask",
				getParameterFromRequest("changeLog"));
		try {
			instrumentRepairPlanService.save(instrumentRepairPlan, aMap,changeLogItem,lMap,"",log);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	@Action(value = "saveInstrumentRepairPlanTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveInstrumentRepairPlanTable() throws Exception {
		String dataValue = getParameterFromRequest("dataJson");
		String changeLog = getParameterFromRequest("changeLog");
		String str = "["+dataValue+"]";
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					str, List.class);
		Map<String, Object> map=new HashMap<String, Object>();
		try {
		for (Map<String, Object> map1 : list) {
			InstrumentRepairPlan a = new InstrumentRepairPlan();
			Map aMap = new HashMap();
			Map lMap = new HashMap();
			a = (InstrumentRepairPlan)commonDAO.Map2Bean(map1, a);
			a.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			a.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			instrumentRepairPlanService.save(a,aMap,changeLog,lMap,changeLog,log);
			map.put("id", a.getId());
		}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	/*添加设备到维修计划左侧表*/
	@Action(value = "addInstrument")
	public void addInstrumentRepairPlanTask() throws Exception {
		String note = getParameterFromRequest("note");
		String instrId = getParameterFromRequest("id");
		String createUser = getParameterFromRequest("createUser");
		String state = getParameterFromRequest("state");
		String typeId = getParameterFromRequest("typeId");
		String period = getParameterFromRequest("period");
		String tsTime = getParameterFromRequest("tsTime");
		String unitId = getParameterFromRequest("unitId");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String instrumentRepairPlanId= instrumentRepairPlanService.addInstrument(ids, note, instrId, createUser, state, typeId, period, tsTime,unitId);
			if(instrumentRepairPlanId!=null) {
				result.put("success", true);
				result.put("data", instrumentRepairPlanId);
			}else {
				result.put("success", true);
				result.put("zy", "1");
			}
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
/*old*/
	/**
	 * 访问列表
	 */
/*
	@Action(value = "showInstrumentRepairPlanList")
	public String showInstrumentRepairPlanList() throws Exception {
//		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
//		map.put("id", new String[] { "", "string", "", "编号", "100", "true", "", "", "", "", "", "" });
//		map.put("note", new String[] { "", "string", "", "检测机构", "240", "true", "", "", "", "", "", "" });
//		map.put("storage-id", new String[] { "", "string", "", "设备编号", "100", "true", "", "", "", "", "", "" });
//		map.put("storage-note", new String[] { "", "string", "", "设备名称", "240", "true", "", "", "", "", "", "" });
//		map.put("storage-spec", new String[] { "", "string", "", "规格型号", "140", "true", "", "", "", "", "", "" });
//		map.put("storage-position-id", new String[] { "", "string", "", "位置", "100", "true", "", "", "", "", "", "" });
//		map.put("createUser-name", new String[] { "", "string", "", "创建人", "80", "true", "", "", "", "", "", "" });
//		map.put("state-name", new String[] { "", "string", "", "状态", "80", "true", "", "", "", "",
//				"workflowStateChange", "" });
//		map.put("effectiveDate", new String[] { "", "string", "", "启用日期", "150", "true", "true", "", "", "", "", "" });
//		map.put("expiryDate", new String[] { "", "string", "", "结束日期", "150", "true", "true", "", "", "", "", "" });
//		map.put("period", new String[] { "", "string", "", "周期", "150", "true", "true", "", "", "", "", "" });
//		map.put("unit-name", new String[] { "", "string", "", "单位", "150", "true", "true", "", "", "", "", "" });
//		map.put("nextDate", new String[] { "", "string", "", "下次检查日期", "150", "true", "", "", "", "", "", "" });
//		String type = generalexttype(map);
//		String col = generalextcol(map);
//		// 用于ext显示
//		putObjToContext("type", type);
//		putObjToContext("col", col);
//		// 用于判断当前页面类型,LIST类型
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);

		putObjToContext("path", ServletActionContext.getRequest().getContextPath()
				+ "/equipment/plan/showInstrumentRepairPlanListJson.action?queryMethod="
				+ getParameterFromRequest("queryMethod"));
		return dispatcher("/WEB-INF/page/equipment/plan/showInstrumentRepairPlanList.jsp");
	}
*/
	@Action(value = "showInstrumentRepairPlanListJson")
	public void showInstrumentRepairPlanListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		//limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		//字段
		String dir = getParameterFromRequest("dir");
		//排序方式
		String sort = getParameterFromRequest("sort");

		//取出检索用的对象
		String data = getParameterFromRequest("data");

		String queryMethod = getParameterFromRequest("queryMethod");
		Map r = this.toMakeSessionQuery(data, "InstrumentRepairPlan", dir, sort, queryMethod);
		dir = (String) r.get("dir");
		sort = (String) r.get("sort");
		data = (String) r.get("queryData");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);

		Map<String, String> map2Query = new HashMap<String, String>();

		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);

		Map<String, Object> controlMap = instrumentRepairPlanService.findInstrumentRepairPlanList(map2Query, startNum,
				limitNum, dir, sort);
		long totalCount = Long.parseLong(controlMap.get("total").toString());
		List<InstrumentRepairPlan> list = (List<InstrumentRepairPlan>) controlMap.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("note", "");
		map.put("type-id", "");
		map.put("type-name", "");
		map.put("state-name", "");
		map.put("createUser-name", "");
		map.put("effectiveDate", "yyyy-MM-dd");
		map.put("expiryDate", "yyyy-MM-dd");
		map.put("createDate", "yyyy-MM-dd");
		map.put("period", "####");
		map.put("unit-name", "");
		map.put("nextDate", "yyyy-MM-dd");
		map.put("tsTime", "####");
		new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}

	@Action(value = "showAllPlanList")
	public String showAllPlanList() throws Exception {
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		map.put("id", new String[] { "", "string", "", "编号", "100", "true", "", "", "", "", "", "" });
		map.put("tsNote", new String[] { "", "string", "", "说明", "240", "true", "", "", "", "", "", "" });

		map.put("createUser-name", new String[] { "", "string", "", "创建人", "80", "true", "", "", "", "", "", "" });
		map.put("state-name", new String[] { "", "string", "", "状态", "80", "true", "", "", "", "",
				"workflowStateChange", "" });
		map.put("effectiveDate", new String[] { "", "string", "", "启用日期", "150", "true", "true", "", "", "", "", "" });
		map.put("expiryDate", new String[] { "", "string", "", "结束日期", "150", "true", "true", "", "", "", "", "" });
		map.put("period", new String[] { "", "string", "", "周期", "150", "true", "true", "", "", "", "", "" });
		map.put("unit-name", new String[] { "", "string", "", "单位", "150", "true", "true", "", "", "", "", "" });
		map.put("nextDate", new String[] { "", "string", "", "下次检查日期", "150", "true", "", "", "", "", "", "" });
		String type = generalexttype(map);
		String col = generalextcol(map);
		// 用于ext显示
		putObjToContext("type", type);
		putObjToContext("col", col);
		// 用于判断当前页面类型,LIST类型
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);

		putObjToContext("path", ServletActionContext.getRequest().getContextPath()
				+ "/equipment/plan/showAllPlanListJson.action?queryMethod=" + getParameterFromRequest("queryMethod"));
		return dispatcher("/WEB-INF/page/equipment/plan/showInstrumentRepairPlanList.jsp");
	}

	@Action(value = "showAllPlanListJson")
	public void showAllPlanListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		//limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		//字段
		String dir = getParameterFromRequest("dir");
		//排序方式
		String sort = getParameterFromRequest("sort");

		//取出检索用的对象
		String data = getParameterFromRequest("data");

		String queryMethod = getParameterFromRequest("queryMethod");
		Map r = this.toMakeSessionQuery(data, "InstrumentRepairPlan", dir, sort, queryMethod);
		dir = (String) r.get("dir");
		sort = (String) r.get("sort");
		data = (String) r.get("queryData");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		Map<String, String> map2Query = new HashMap<String, String>();

		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);

		Map<String, Object> controlMap = instrumentRepairPlanService.findPlanList(map2Query, startNum, limitNum, dir,
				sort);
		long totalCount = Long.parseLong(controlMap.get("total").toString());
		List<InstrumentRepairPlan> list = (List<InstrumentRepairPlan>) controlMap.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("note", "");
		map.put("tsNote", "");
		map.put("state-name", "");
		map.put("createUser-name", "");
		map.put("effectiveDate", "yyyy-MM-dd");
		map.put("expiryDate", "yyyy-MM-dd");
		map.put("createDate", "yyyy-MM-dd");
		map.put("period", "####");
		map.put("unit-name", "");
		map.put("nextDate", "yyyy-MM-dd");
		map.put("tsTime", "####");
		map.put("dutyUser-name", "");
		new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}

	/**
	 * 访问编辑页面
	 */

	@Action(value = "toEditInstrumentRepairPlan")
	public String toEditInstrumentRepairPlan() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = "";
		if (id != null && !id.equals("")) {
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			instrumentRepairPlan = instrumentRepairPlanService.getInstrumentRepairPlan(id);
			putObjToContext("instrumentRepairPlanId", id);
			handlemethod = SystemConstants.PAGE_HANDLE_METHOD_MODIFY;
			toToolBar(rightsId, "", "", handlemethod);
			showInstrumentRepairPlanTaskList(handlemethod, id);
			showInstrumentRepairPlanTaskStorageList(handlemethod, id, "");
			showInstrumentRepairPlanTaskPersonList(handlemethod, id, "");

		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);

			DicState ds = new DicState();
			ds.setId("1q");
			instrumentRepairPlan.setState(ds);
			this.instrumentRepairPlan.setId(SystemCode.DEFAULT_SYSTEMCODE);
			instrumentRepairPlan.setCreateUser(user);
			instrumentRepairPlan.setCreateDate(new Date());
			instrumentRepairPlan.setDutyUser(user);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			showInstrumentRepairPlanTaskList(SystemConstants.PAGE_HANDLE_METHOD_ADD, id);
			showInstrumentRepairPlanTaskStorageList(SystemConstants.PAGE_HANDLE_METHOD_ADD, id, "");
			showInstrumentRepairPlanTaskPersonList(SystemConstants.PAGE_HANDLE_METHOD_ADD, id, "");
		}
		return dispatcher("/WEB-INF/page/equipment/plan/editInstrumentRepairPlan.jsp");
	}

	/**
	 * 访问编辑页面
	 */

	@Action(value = "toCopyInstrumentRepairPlan")
	public String toCopyInstrumentRepairPlan() throws Exception {
		String id = getParameterFromRequest("id");
		instrumentRepairPlan = instrumentRepairPlanService.getInstrumentRepairPlan(id);
		showInstrumentRepairPlanTaskList(SystemConstants.PAGE_HANDLE_METHOD_MODIFY, id);
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		this.instrumentRepairPlan.setId(SystemCode.DEFAULT_SYSTEMCODE);
		instrumentRepairPlan.setCreateUser(user);
		instrumentRepairPlan.setCreateDate(new Date());
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		showInstrumentRepairPlanTaskList(SystemConstants.PAGE_HANDLE_METHOD_ADD, id);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/equipment/plan/editInstrumentRepairPlan.jsp");
	}

	public void showInstrumentRepairPlanTaskList(String handlemethod, String instrumentRepairPlanId) throws Exception {

		String editflag = "true";
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		map.put("id", new String[] { "", "string", "", "任务编号", "120", "true", "true", "", "", "", "", "" });
		//map
		//	.put("type-id", new String[] { "", "string", "", "类型", "120", "true", "true", "", "", editflag, "",
		//		"type" });
		//map.put("type-name", new String[] { "", "string", "", "类型名称", "150", "true", "false", "", "", editflag, "",
		//		"type" });
		map.put("note", new String[] { "", "string", "", "描述", "300", "true", "", "", "", editflag, "",
				"new Ext.form.TextArea()" });
		String type = generalexttype(map);
		String col = generalextcol(map);
		String statement = "";
		String statementLisener = "";
		putObjToContext("type", type);
		putObjToContext("col", col);
		putObjToContext("handlemethod", handlemethod);
		putObjToContext("instrumentRepairPlanId", instrumentRepairPlanId);
		putObjToContext("path", ServletActionContext.getRequest().getContextPath()
				+ "/equipment/plan/showInstrumentRepairPlanTaskListJson.action?instrumentRepairPlanId="
				+ instrumentRepairPlanId);
		putObjToContext("statement", statement);
		putObjToContext("statementLisener", statementLisener);

	}

	public void showInstrumentRepairPlanTaskPersonList(String handlemethod, String instrumentRepairPlanId,
			String instrumentRepairPlanTaskId) throws Exception {

		String exttype = "";
		String extcol = "";
		String editflag = "true";
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		map.put("workType-id", new String[] { "", "string", "", "工种ID", "180", "true", "false", "", "", editflag, "",
				"workType" });
		map.put("workType-name", new String[] { "", "string", "", "工种", "180", "true", "false", "", "", "", "", "" });
		map.put("id", new String[] { "", "string", "", "id", "150", "true", "true", "", "", "", "", "" });
		map.put("user-id",
				new String[] { "", "string", "", "人员编号", "80", "true", "false", "", "", editflag, "", "user" });
		map.put("user-name", new String[] { "", "string", "", "姓名", "150", "true", "false", "", "", "", "", "" });
		map.put("workTime", new String[] { "", "string", "", "工时", "80", "true", "false", "", "", editflag, "",
				"workTime" });
		map.put("scale", new String[] { "", "string", "", "费率", "80", "true", "false", "", "", editflag, "", "scale" });
		map.put("instrumentRepairPlanTask-id", new String[] { "", "string", "", "任务编号", "150", "true", "false", "", "",
				editflag, "", "instrumentRepairPlanTask" });
		String statement = "";
		String statementLisener = "";
		exttype = generalexttype(map);
		extcol = generalextcol(map);
		putObjToContext("type2", exttype);
		putObjToContext("col2", extcol);
		putObjToContext("path2", ServletActionContext.getRequest().getContextPath()
				+ "/equipment/plan/showInstrumentRepairPlanTaskPersonListJson.action?instrumentRepairPlanTaskId="
				+ instrumentRepairPlanTaskId + "&instrumentRepairPlanId=" + instrumentRepairPlanId);
		putObjToContext("statement", statement);
		putObjToContext("statementLisener", statementLisener);
		putObjToContext("handlemethod", handlemethod);

	}

	public void showInstrumentRepairPlanTaskStorageList(String handlemethod, String instrumentRepairPlanId,
			String instrumentRepairPlanTaskId) throws Exception {
		String exttype = "";
		String extcol = "";
		String editflag = "true";
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		map.put("id", new String[] { "", "string", "", "id", "150", "true", "true", "", "", "", "", "" });
		map.put("storage-id", new String[] { "", "string", "", "组件编号", "120", "true", "false", "", "", editflag, "",
				"storage" });
		map.put("storage-name", new String[] { "", "string", "", "对象名称", "150", "true", "false", "", "", editflag, "",
				"storage" });
		map.put("price", new String[] { "", "string", "", "价格", "80", "true", "false", "", "", editflag, "", "price" });
		map.put("num", new String[] { "", "string", "", "数量", "80", "true", "false", "", "", editflag, "", "num" });

		map.put("instrumentRepairPlanTask-id", new String[] { "", "string", "", "任务编号", "160", "true", "false", "", "",
				editflag, "", "instrumentRepairPlanTask" });
		String statement = "";
		String statementLisener = "";
		exttype = generalexttype(map);
		extcol = generalextcol(map);
		putObjToContext("type1", exttype);
		putObjToContext("col1", extcol);
		putObjToContext("path1", ServletActionContext.getRequest().getContextPath()
				+ "/equipment/plan/showInstrumentRepairPlanTaskStorageListJson.action?instrumentRepairPlanTaskId="
				+ instrumentRepairPlanTaskId + "&instrumentRepairPlanId=" + instrumentRepairPlanId);

		putObjToContext("statement", statement);
		putObjToContext("statementLisener", statementLisener);

		putObjToContext("handlemethod", handlemethod);

	}

	/**
	 * 访问查看页面
	 */
/*
	@Action(value = "toViewInstrumentRepairPlan")
	public String toViewInstrumentRepairPlan() throws Exception {
		String id = getParameterFromRequest("id");

		if (id != null && !id.equals("")) {
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
			instrumentRepairPlan = instrumentRepairPlanService.getInstrumentRepairPlan(id);
			putObjToContext("instrumentRepairPlanId", id);
		}

		return dispatcher("/WEB-INF/page/equipment/plan/editInstrumentRepairPlan.jsp");
	}*/

	/**
	 * 添加
	 */
/*	@Action(value = "save")
	public String save() throws Exception {

		String copyMode = getParameterFromRequest("copyMode");
		String oldId = getParameterFromRequest("oldId");
		String data = getParameterFromRequest("jsonDataStr");
		String data1 = getParameterFromRequest("jsonDataStr1");
		String data2 = getParameterFromRequest("jsonDataStr2");
		String id = this.instrumentRepairPlan.getId();
		if (id == null || id.length() <= 0 || SystemCode.DEFAULT_SYSTEMCODE.equals(id)) {
			String code = systemCodeService.getSystemCode(SystemCode.INSTRUMENT_REPAIRPLAN_NAME,
					SystemCode.INSTRUMENT_REPAIRPLAN_CODE, SystemCode.INSTRUMENT_REPAIRPLAN_CODE_PREFIX,
					SystemCode.INSTRUMENT_REPAIRPLAN_CODE_LENGTH);
			this.instrumentRepairPlan.setId(code);
		}
		if (copyMode.equals("true") && !oldId.equals("")) {
			instrumentRepairPlanService.copySave(instrumentRepairPlan, oldId);
		} else {
			instrumentRepairPlanService.save(instrumentRepairPlan);
		}
		if (data != null && !data.equals(""))
			saveInstrumentRepairPlanTask(instrumentRepairPlan, data);
		if (data1 != null && !data1.equals(""))
			saveInstrumentRepairPlanTaskStorage(data1);
		if (data2 != null && !data2.equals(""))
			saveInstrumentRepairPlanTaskPerson(data2);

		//具体操作，如删除，填加动作，应用redirect
		return redirect("/equipment/plan/toEditInstrumentRepairPlan.action?id=" + instrumentRepairPlan.getId());
	}*/

	public void saveInstrumentRepairPlanTask(InstrumentRepairPlan instrumentRepairPlan2, String json) throws Exception {

		instrumentRepairPlanService.saveInstrumentRepairPlanTask(instrumentRepairPlan2, json);

	}

	public void saveInstrumentRepairPlanTaskPerson(String json) throws Exception {

		instrumentRepairPlanService.saveInstrumentRepairPlanTaskPerson(json);

	}

	public void saveInstrumentRepairPlanTaskStorage(String json) throws Exception {

		instrumentRepairPlanService.saveInstrumentRepairPlanTaskStorage(json);

	}

	/**
	 * 删除
	 */
	@Action(value = "delInstrumentRepairPlan")
	public void delInstrumentRepairPlan() throws Exception {
		//String id = getParameterFromRequest("id");
		//experimentService.delExperimentMainStorage(id);
		//具体操作，如删除，填加动作，应用redirect
		//return redirect("/sysmanage/user/userVaccineShow.action");
	}

	/**
	 * 访问列表
	 */

	@Action(value = "showInstrumentRepairPlanTaskList")
	public String showInstrumentRepairPlanTaskList() throws Exception {
		String handlemethod = getParameterFromRequest("handlemethod");
		String instrumentRepairPlanId = getParameterFromRequest("instrumentRepairPlanId");
		String editflag = "true";
//		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
//		map.put("id", new String[] { "", "string", "", "任务编号", "120", "true", "false", "", "", editflag, "", "id" });
//		map.put("type-id", new String[] { "", "string", "", "类型", "120", "true", "true", "", "", "", "", "" });
//		map.put("type-name", new String[] { "", "string", "", "类型名称", "150", "true", "false", "", "", editflag, "",
//				"type" });
//		map.put("note", new String[] { "", "string", "", "描述", "300", "true", "", "", "", editflag, "", "note" });
//		String type = generalexttype(map);
//		String col = generalextcol(map);
//		String statement = "";
//		String statementLisener = "";
//
//		putObjToContext("type", type);
//		putObjToContext("col", col);
		putObjToContext("handlemethod", handlemethod);
		putObjToContext("instrumentRepairPlanId", instrumentRepairPlanId);
		putObjToContext("path", ServletActionContext.getRequest().getContextPath()
				+ "/equipment/plan/showInstrumentRepairPlanTaskListJson.action?instrumentRepairPlanId="
				+ instrumentRepairPlanId);
		//putObjToContext("statement", statement);
		//putObjToContext("statementLisener", statementLisener);

		return dispatcher("/WEB-INF/page/equipment/plan/showInstrumentRepairPlanTaskList.jsp");
	}

	@Action(value = "showInstrumentRepairPlanTaskListJson")
	public void showInstrumentRepairPlanTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");

		String instrumentRepairPlanId = getParameterFromRequest("instrumentRepairPlanId");
		List<InstrumentRepairPlanTask> list = null;
		long totalCount = 0;
		putObjToContext("instrumentRepairPlanId", instrumentRepairPlanId);

		if (instrumentRepairPlanId != null && !instrumentRepairPlanId.equals("")) {
			Map<String, Object> controlMap = instrumentRepairPlanService.findInstrumentRepairPlanTaskList(startNum,
					limitNum, dir, sort, getContextPath(), instrumentRepairPlanId);
			totalCount = Long.parseLong(controlMap.get("totalCount").toString());
			list = (List<InstrumentRepairPlanTask>) controlMap.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("type-id", "");
			map.put("type-name", "");
			map.put("instrument-id", "");
			map.put("instrument-name", "");
			map.put("instrument-effectiveDate", "yyyy-MM-dd");
			map.put("instrument-expiryDate", "yyyy-MM-dd");
			map.put("instrument-useDate", "yyyy-MM-dd");
			map.put("instrument-nextDate", "yyyy-MM-dd");
			map.put("instrument-tsDate", "yyyy-MM-dd");
			map.put("instrument-spec", "");
			map.put("instrument-state-name", "");
			map.put("fee", "");
			map.put("note", "");

			new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
		}
	}

//	/**
//	 * 保存明细
//	 */
//	@Action(value = "saveInstrumentRepairPlanTask")
//	public void saveInstrumentRepairPlanTask() throws Exception {
//		String instrumentRepairPlanId = getParameterFromRequest("instrumentRepairPlanId");
//		String json = getParameterFromRequest("data");
//		instrumentRepairPlanService.saveInstrumentRepairPlanTask(instrumentRepairPlanId, json);
//
//	}

	/**
	 * 删除明细
	 */
/*	@Action(value = "delInstrumentRepairPlanTask")
	public void delInstrumentRepairPlanTask() throws Exception {
		String[] ids = getRequest().getParameterValues("ids[]");
		instrumentRepairPlanService.delInstrumentRepairPlanTask(ids);
	}*/

	@Action(value = "showInstrumentRepairPlanTaskPersonListJson")
	public void showInstrumentRepairPlanTaskPersonListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String instrumentRepairPlanId = getParameterFromRequest("instrumentRepairPlanId");
		String instrumentRepairPlanTaskId = getParameterFromRequest("instrumentRepairPlanTaskId");
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		List<InstrumentRepairPlanTaskPerson> list = null;
		long totalCount = 0;
		if (instrumentRepairPlanId != null && !instrumentRepairPlanId.equals("")) {
			Map<String, Object> controlMap = instrumentRepairPlanService
					.findInstrumentRepairPlanTaskPersonList(startNum, limitNum, dir, sort, getContextPath(),
							instrumentRepairPlanId, instrumentRepairPlanTaskId);
			totalCount = Long.parseLong(controlMap.get("totalCount").toString());
			list = (List<InstrumentRepairPlanTaskPerson>) controlMap.get("list");
		}
		Map<String, String> map = new HashMap<String, String>();
		map.put("workType-name", "");
		map.put("id", "");
		map.put("user-id", "");
		map.put("user-name", "");
		map.put("scale", "");
		map.put("instrumentRepairPlanTask-id", "");
		if (list != null)
			new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}

	/**
	 * 保存明细
	 */
	@Action(value = "saveInstrumentRepairPlanTaskPerson")
	public void saveInstrumentRepairPlanTaskPerson() throws Exception {
		String json = getParameterFromRequest("data");
		instrumentRepairPlanService.saveInstrumentRepairPlanTaskPerson(json);

	}

	/**
	 * 删除明细
	 */
	@Action(value = "delInstrumentRepairPlanTaskPerson")
	public void delInstrumentRepairPlanTaskPerson() throws Exception {
		String id = getParameterFromRequest("id");
		instrumentRepairPlanService.delInstrumentRepairPlanTaskPerson(id);
	}

	@Action(value = "showInstrumentRepairPlanTaskStorageListJson")
	public void showProjectFactItemListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String instrumentRepairPlanId = getParameterFromRequest("instrumentRepairPlanId");
		String instrumentRepairPlanTaskId = getParameterFromRequest("instrumentRepairPlanTaskId");
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		List<InstrumentRepairPlanTaskStorage> list = null;
		long totalCount = 0;
		if (instrumentRepairPlanId != null && !instrumentRepairPlanId.equals("")) {
			Map<String, Object> controlMap = instrumentRepairPlanService
					.findInstrumentRepairPlanTaskStorageList(startNum, limitNum, dir, sort, getContextPath(),
							instrumentRepairPlanId, instrumentRepairPlanTaskId);
			totalCount = Long.parseLong(controlMap.get("totalCount").toString());
			list = (List<InstrumentRepairPlanTaskStorage>) controlMap.get("list");
		}
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("storage-id", "");
		map.put("storage-name", "");
		map.put("price", "");
		map.put("num", "");

		map.put("instrumentRepairPlanTask-id", "");
		if (list != null)
			new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}

	/**
	 * 保存明细
	 */
	@Action(value = "saveInstrumentRepairPlanTaskStorage")
	public void saveProjectPlanItem() throws Exception {
		String json = getParameterFromRequest("data");
		instrumentRepairPlanService.saveInstrumentRepairPlanTaskStorage(json);
	}

	/**
	 * 删除明细
	 */
	@Action(value = "delInstrumentRepairPlanTaskStorage")
	public void delInstrumentRepairPlanTaskStorage() throws Exception {
		String id = getParameterFromRequest("id");
		instrumentRepairPlanService.delInstrumentRepairPlanTaskStorage(id);
	}

	@Action(value = "instrumentRepairPlanSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String instrumentRepairPlanSelect() throws Exception {
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		map.put("id", new String[] { "", "string", "", "编号", "100", "true", "", "", "", "", "", "" });
		map.put("note", new String[] { "", "string", "", "描述", "150", "true", "", "", "", "", "", "" });
		map.put("storage-id", new String[] { "", "string", "", "故障设备", "100", "true", "", "", "", "", "", "" });
		map.put("storage-note", new String[] { "", "string", "", "设备描述", "100", "true", "", "", "", "", "", "" });
		map.put("type-name", new String[] { "", "string", "", "故障类型", "80", "true", "", "", "", "", "", "" });
		map.put("state",
				new String[] { "", "string", "", "状态", "80", "true", "", "", "", "", "workflowStateChange", "" });
		map.put("createUser-name", new String[] { "", "string", "", "创建人", "80", "true", "", "", "", "", "", "" });
		String type = generalexttype(map);
		String col = generalextcol(map);
		putObjToContext("type", type);
		putObjToContext("col", col);
		putObjToContext("path", ServletActionContext.getRequest().getContextPath()
				+ "/equipment/plan/instrumentRepairPlanSelectJson.action");
		return dispatcher("/WEB-INF/page/equipment/plan/instrumentRepairPlanSelect.jsp");
	}

	@Action(value = "instrumentRepairPlanSelectJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void instrumentRepairPlanSelectJson() throws Exception {
		//开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		//limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		//字段
		String dir = getParameterFromRequest("dir");
		//排序方式
		String sort = getParameterFromRequest("sort");

		//取出检索用的对象
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();

		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);

		Map<String, Object> controlMap = instrumentRepairPlanService.findInstrumentRepairPlanList(map2Query, startNum,
				limitNum, dir, sort);
		long totalCount = Long.parseLong(controlMap.get("total").toString());
		List<InstrumentRepairPlan> list = (List<InstrumentRepairPlan>) controlMap.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("note", "");
		map.put("storage-id", "");
		map.put("storage-note", "");
		map.put("type-name", "");

		map.put("state", "");
		map.put("createUser-name", "");
		new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}
	@Action(value = "showInstrumentTempTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showInstrumentRepairTaskTableJson() throws Exception {
		
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = instrumentRepairPlanService.findInstrumentTable(start, length, query, col, sort, id);
		List<Instrument> list=(List<Instrument>) result.get("list");
		Map<String, String> map=new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("position-id", "");
		map.put("dutyUser-name", "");
		map.put("state-name", "");
		map.put("spec", "");
		map.put("rank", "");
		map.put("type-id", "");
		map.put("type-name", "");
		map.put("dutyUserTwo-name", "");
		map.put("serialNumber", "");
		map.put("enquiryOddNumber", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("outCode", "");
		map.put("accuracy", "");
		map.put("purchaseDate", "yyyy-MM-dd");
		map.put("surveyScope", "");
		map.put("checkItem", "");
		map.put("producer-name", "");
		map.put("producer-linkMan", "");
		map.put("producer-linkTel", "");
		map.put("supplier-name", "");
		map.put("supplier-linkMan", "");
		map.put("supplier-linkTel", "");
		map.put("studyType-name", "");
		map.put("effectiveDate", "yyyy-MM-dd");
		map.put("expiryDate", "yyyy-MM-dd");
		map.put("nextDate", "yyyy-MM-dd");
		String data=new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw,result,data));
	}
	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}


	public InstrumentRepairPlan getInstrumentRepairPlan() {
		return instrumentRepairPlan;
	}

	public void setInstrumentRepairPlan(InstrumentRepairPlan instrumentRepairPlan) {
		this.instrumentRepairPlan = instrumentRepairPlan;
	}

}
