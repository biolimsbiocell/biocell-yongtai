package com.biolims.equipment.plan.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.equipment.plan.service.InstrumentRepairPlanService;

public class RepairPlanTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		InstrumentRepairPlanService mbService = (InstrumentRepairPlanService) ctx
				.getBean("instrumentRepairPlanService");
		mbService.changeState(contentId);

		return "";
	}
}
