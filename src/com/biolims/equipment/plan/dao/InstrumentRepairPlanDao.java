package com.biolims.equipment.plan.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.common.dao.CommonDAO;
import com.biolims.equipment.model.Instrument;
import com.biolims.equipment.model.InstrumentRepairPlan;
import com.biolims.equipment.model.InstrumentRepairPlanTask;
import com.biolims.equipment.model.InstrumentRepairPlanTaskPerson;
import com.biolims.equipment.model.InstrumentRepairPlanTaskStorage;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class InstrumentRepairPlanDao extends CommonDAO {

	public Map<String, Object> selectInstrumentRepairPlan(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = " from InstrumentRepairPlan where state.id like '1%' ";
		String key = "";
		if (mapForQuery != null)
			key = map2where4Split(mapForQuery);
		Long total = (Long) this.getSession().createQuery("select count(*)" + hql + key).uniqueResult();
		List<InstrumentRepairPlan> list = new ArrayList<InstrumentRepairPlan>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum != null && limitNum != null) {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			} else {
				list = this.getSession().createQuery(hql + key).list();
			}

		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("total", total);
		map.put("list", list);
		return map;
	}

	public Map<String, Object> selectInstrumentRepairPlanAll(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = " from InstrumentRepairPlan where 1=1 ";
		String key = "";
		if (mapForQuery != null)
			key = map2where4Split(mapForQuery);
		Long total = (Long) this.getSession().createQuery("select count(*)" + hql + key).uniqueResult();
		List<InstrumentRepairPlan> list = new ArrayList<InstrumentRepairPlan>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum != null && limitNum != null) {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			} else {
				list = this.getSession().createQuery(hql + key).list();
			}

		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("total", total);
		map.put("list", list);
		return map;
	}

	public Map<String, Object> selectInstrumentRepairPlanNotYq(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = " from InstrumentRepairPlan where 1=1 ";
		String key = "";
		if (mapForQuery != null)
			key = map2where4Split(mapForQuery);
		Long total = (Long) this.getSession().createQuery("select count(*)" + hql + key).uniqueResult();
		List<InstrumentRepairPlan> list = new ArrayList<InstrumentRepairPlan>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum != null && limitNum != null) {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			} else {
				list = this.getSession().createQuery(hql + key).list();
			}

		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("total", total);
		map.put("list", list);
		return map;
	}

	public Map<String, Object> selectInstrumentRepairPlanTask(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = " from InstrumentRepairPlanTask where 1=1";
		String key = "";
		if (mapForQuery != null) {
			key = map2where(mapForQuery);
		}
		Long total = (Long) this.getSession().createQuery("select count(*)" + hql + key).uniqueResult();
		List<InstrumentRepairPlanTask> list = new ArrayList<InstrumentRepairPlanTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum != null && limitNum != null) {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			} else {
				list = this.getSession().createQuery(hql + key).list();
			}

		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("total", total);
		map.put("list", list);
		return map;
	}

	public Map<String, Object> selectInstrumentRepairPlanTaskStorage(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = " from InstrumentRepairPlanTaskStorage where 1=1";
		String key = "";
		if (mapForQuery != null) {
			key = map2where(mapForQuery);
		}
		Long total = (Long) this.getSession().createQuery("select count(*)" + hql + key).uniqueResult();
		List<InstrumentRepairPlanTaskStorage> list = new ArrayList<InstrumentRepairPlanTaskStorage>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum != null && limitNum != null) {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			} else {
				list = this.getSession().createQuery(hql + key).list();
			}

		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("total", total);
		map.put("list", list);
		return map;
	}

	public Map<String, Object> selectInstrumentRepairPlanTaskPerson(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = " from InstrumentRepairPlanTaskPerson where 1=1";
		String key = "";
		if (mapForQuery != null) {
			key = map2where(mapForQuery);
		}
		Long total = (Long) this.getSession().createQuery("select count(*)" + hql + key).uniqueResult();
		List<InstrumentRepairPlanTaskPerson> list = new ArrayList<InstrumentRepairPlanTaskPerson>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum != null && limitNum != null) {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			} else {
				list = this.getSession().createQuery(hql + key).list();
			}

		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("total", total);
		map.put("list", list);
		return map;
	}

	public List selectInstrumentRepairPlan(String id, String typeId) throws Exception {
		String hql = " from InstrumentRepairPlan where storage.id = '" + id + "' and state.id like '1%'";
		String key = "";
		List<InstrumentRepairPlan> list = new ArrayList<InstrumentRepairPlan>();
		list = this.getSession().createQuery(hql + key).list();
		return list;
	}
	public List selectInstrumentRepairTaskPlan(String planId) throws Exception{
		String hql="from InstrumentRepairPlanTask where instrumentRepairPlan.id='"+planId+"'";
		String key="";
		List<InstrumentRepairPlanTask>list=new ArrayList<InstrumentRepairPlanTask>();
		list=this.getSession().createQuery(hql+key).list();
		return list;
		
	}


	public Map<String, Object> findInstrumentRepairPlanTable(Integer start,Integer length, String query, String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from  InstrumentRepairPlan where 1=1";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from InstrumentRepairPlan where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<InstrumentRepairPlan> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> findInstrumentRepairPlanTaskTable(Integer start, Integer length, String query, String col, String sort, String id) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from InstrumentRepairPlanTask where 1=1 and instrumentRepairPlan.id='"+id+"'";
		String key = "";

		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from InstrumentRepairPlanTask where 1=1 and instrumentRepairPlan.id='"+id+"' ";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<InstrumentRepairPlanTask> list = new ArrayList<InstrumentRepairPlanTask>();
			list=this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> findInstrumentTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from Instrument where 1=1 and verify='1'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from Instrument where 1=1 and verify='1'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			
			List<Instrument> list = new ArrayList<Instrument>();
			list=this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<InstrumentRepairPlanTask> findInstrumentFault(String idd,String inId) {
		 String hql="from InstrumentRepairPlanTask where 1=1 and instrument.id='"+idd+"'and instrumentRepairPlan='"+inId+"' ";
		 List<InstrumentRepairPlanTask> list= this.getSession().createQuery(hql).list();
		return list;
	}
}
