/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：Service
 * 类描述：预维护计划service
 * 创建人：倪毅
 * 创建时间：2011-9
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.equipment.plan.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.SystemCodeService;
import com.biolims.dic.model.DicState;
import com.biolims.dic.model.DicType;
import com.biolims.dic.model.DicUnit;
import com.biolims.equipment.common.constants.SystemCode;
import com.biolims.equipment.common.constants.SystemConstants;
import com.biolims.equipment.main.service.InstrumentService;
import com.biolims.equipment.model.Instrument;
import com.biolims.equipment.model.InstrumentFault;
import com.biolims.equipment.model.InstrumentFaultDetail;
import com.biolims.equipment.model.InstrumentRepair;
import com.biolims.equipment.model.InstrumentRepairPlan;
import com.biolims.equipment.model.InstrumentRepairPlanTask;
import com.biolims.equipment.model.InstrumentRepairPlanTaskPerson;
import com.biolims.equipment.model.InstrumentRepairPlanTaskStorage;
import com.biolims.equipment.model.InstrumentRepairTask;
import com.biolims.equipment.model.InstrumentRepairTaskPerson;
import com.biolims.equipment.model.InstrumentRepairTaskStorage;
import com.biolims.equipment.plan.dao.InstrumentRepairPlanDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.remind.model.SysRemind;
import com.biolims.util.ConfigurableContants;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.opensymphony.xwork2.ActionContext;

@Service
@SuppressWarnings("unchecked")
public class InstrumentRepairPlanService extends ConfigurableContants {
	static {
		String lan = (String)ServletActionContext.getRequest().getSession().getAttribute("systemLan");
		
		init("/ResouseInternational/msg_"+lan+".properties");
	}

	@Resource
	private CommonDAO commonDAO;
	
	@Resource
	private InstrumentRepairPlanDao instrumentRepairPlanDao;

	@Resource
	private SystemCodeService systemCodeService;
	
	@Autowired
	private InstrumentService instrumentService;

	/**
	 * 检索实验,采用map方式传递检索参数
	 * @return 参数值MAP，如果传递的为null返回空字符串
	 */
	public Map<String, Object> findInstrumentRepairPlanList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> controlMap = instrumentRepairPlanDao.selectInstrumentRepairPlanAll(mapForQuery, startNum,
				limitNum, dir, sort);
		List<InstrumentRepairPlan> list = (List<InstrumentRepairPlan>) controlMap.get("list");
		controlMap.put("list", list);
		return controlMap;
	}

	public Map<String, Object> findPlanList(Map<String, String> mapForQuery, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		Map<String, Object> controlMap = instrumentRepairPlanDao.selectInstrumentRepairPlanNotYq(mapForQuery, startNum,
				limitNum, dir, sort);
		List<InstrumentRepairPlan> list = (List<InstrumentRepairPlan>) controlMap.get("list");
		controlMap.put("list", list);
		return controlMap;
	}

	/**
	 * 保存
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void save(InstrumentRepairPlan an) throws Exception {
		instrumentRepairPlanDao.saveOrUpdate(an);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void copySave(InstrumentRepairPlan er, String oldId) throws Exception {
		instrumentRepairPlanDao.saveOrUpdate(er);
		//保存模板中信息
		insertInstrumentRepairPlanTask(er.getId(), oldId);
		//insertInstrumentRepairPlanTaskStorage(er.getId(),oldId);
		//insertInstrumentRepairPlanTaskPerson(er.getId(),oldId);

	}

	public InstrumentRepairPlan getInstrumentRepairPlan(String id) throws Exception {
		InstrumentRepairPlan animal = instrumentRepairPlanDao.get(InstrumentRepairPlan.class, id);
		return animal;
	}

	//检索明细
	public Map<String, Object> findInstrumentRepairPlanTaskList(int startNum, int limitNum, String dir, String sort,
			String contextPath, String animalId) throws Exception {
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		Map<String, String> mapForCondition = new HashMap<String, String>();
		if (animalId != null && !animalId.equals("")) {
			mapForQuery.put("instrumentRepairPlan.id", animalId);
			mapForCondition.put("instrumentRepairPlan.id", "=");
		}

		Map<String, Object> controlMap = instrumentRepairPlanDao.findObjectCondition(startNum, limitNum, dir, sort,
				InstrumentRepairPlanTask.class, mapForQuery, mapForCondition);

		List<InstrumentRepairPlanTask> list = (List<InstrumentRepairPlanTask>) controlMap.get("list");
		controlMap.put("list", list);
		return controlMap;
	}

	/**
	 * 
	 * 维护明细信息,由ext ajax调用
	 * 
	 * @param userId
	 *            用户id
	 * @param jsonString
	 *            用户认证jsonString
	 * @return void
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveInstrumentRepairPlanTask(InstrumentRepairPlan an, String jsonString) throws Exception {
		InstrumentRepairPlan ep = instrumentRepairPlanDao.get(InstrumentRepairPlan.class, an.getId());
		//em.setNote(em.getNote().replace("<br>", "\\n\\r"));
//		ep.setId(animalId);
		// 将json读入map
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(jsonString, List.class);
		String typeId = an.getUnitName();
		Integer period = an.getPeriod();
		Integer tsTime=an.getTsTime();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		for (Map<String, Object> map : list) {
			InstrumentRepairPlanTask em = new InstrumentRepairPlanTask();
			em = (InstrumentRepairPlanTask) instrumentRepairPlanDao.Map2Bean(map, em);
			em.setInstrumentRepairPlan(ep);
			em.setType(an.getType());
			em.setUnit(an.getUnit());
			em.setPeriod(period);
			em.setTsTime(tsTime);
			if("1".equals(an.getUnitName())){
				em.setUnitName("天");
			}else if("2".equals(an.getUnitName())){
				em.setUnitName("周");
			}else if("3".equals(an.getUnitName())){
				em.setUnitName("月");
			}
			Instrument ins=null; 
			ins=instrumentService.getInstrument(em.getInstrument().getId());
			String effectiveDate =(String) map.get("instrument-effectiveDate");
			Date effectiveDates=null;
			if(effectiveDate!=null&&!effectiveDate.equals("")){
				effectiveDates=sdf.parse(effectiveDate);
				ins.setEffectiveDate(effectiveDates);
			}
			String expiryDate=(String) map.get("instrument-expiryDate");
			if(expiryDate!=null&&!expiryDate.equals("")){
				ins.setExpiryDate(sdf.parse(expiryDate));
			}
			String useDate=(String) map.get("instrument-useDate");
			Date useDates=null;
			if(useDate!=null&&!useDate.equals("")){
				useDates=sdf.parse(useDate);
				ins.setUseDate(useDates);																
			}else{
				useDates=effectiveDates;
				ins.setUseDate(useDates);
			}
			Date nextDates=null;
			if (useDates != null && period != null) {
				if("1".equals(an.getUnitName())){//天数

					nextDates = DateUtil.addDay(useDates, period);
				}else if("2".equals(an.getUnitName())){//周数

					nextDates = DateUtil.addDay(useDates, period * 7);
				}else if("3".equals(an.getUnitName())){//月数
					nextDates = DateUtil.addMonth(useDates, period);
				}
				nextDates = DateUtil.addDay(nextDates, -1);
					ins.setNextDate(nextDates);
				Date tsDate = DateUtil.addDay(nextDates, -tsTime);
					ins.setTsDate(tsDate);
			}
			String note=(String)map.get("note");
			if(note!=null&&!note.equals("")){
				ins.setNote((String) map.get("note"));
			}
			commonDAO.saveOrUpdate(ins);
			instrumentRepairPlanDao.saveOrUpdate(em);
		}
	}

	/**
	 * 
	 * 删除明细,由ext ajax调用
	 * 
	 * @param id
	 *            认证ID
	 * @return void
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delInstrumentRepairPlanTask(String[] ids) {
		for(String id:ids){
//			InstrumentRepairPlanTask ppi = new InstrumentRepairPlanTask();
			InstrumentRepairPlanTask ppi = commonDAO.get(InstrumentRepairPlanTask.class, id);
			ppi.setId(id);
			instrumentRepairPlanDao.delete(ppi);
			
			if (ids.length>0) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(ppi.getInstrumentRepairPlan().getId());
				li.setClassName("InstrumentRepairPlan");
				li.setModifyContent("设备周期验证计划:"+"编号:"+ppi.getInstrument().getId()+"设备名称:"+ppi.getInstrument().getName()+"的数据已删除");
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}
			
		}
	}
	//检索用户明细
	public Map<String, Object> findInstrumentRepairPlanTaskPersonList(int startNum, int limitNum, String dir,
			String sort, String contextPath, String instrumentRepairPlanId, String instrumentRepairPlanTaskId)
			throws Exception {
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		Map<String, String> mapForCondition = new HashMap<String, String>();
		if (instrumentRepairPlanId != null && !instrumentRepairPlanId.equals("")) {
			mapForQuery.put("instrumentRepairPlanTask", "alias");
			mapForCondition.put("instrumentRepairPlanTask", "");
			mapForQuery.put("instrumentRepairPlanTask.instrumentRepairPlan.id", instrumentRepairPlanId);
			mapForCondition.put("instrumentRepairPlanTask.instrumentRepairPlan.id", "=");
		}
		if (instrumentRepairPlanTaskId != null && !instrumentRepairPlanTaskId.equals("")) {
			mapForQuery.put("instrumentRepairPlanTask.id", instrumentRepairPlanTaskId);
			mapForCondition.put("instrumentRepairPlanTask.id", "=");
		}
		Map<String, Object> controlMap = instrumentRepairPlanDao.findObjectCondition(startNum, limitNum, dir, sort,
				InstrumentRepairPlanTaskPerson.class, mapForQuery, mapForCondition);

		List<InstrumentRepairPlanTaskPerson> list = (List<InstrumentRepairPlanTaskPerson>) controlMap.get("list");
		controlMap.put("list", list);
		return controlMap;
	}

	/**
	 * 
	 * 维护明细信息,由ext ajax调用
	 * 
	 * 
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveInstrumentRepairPlanTaskPerson(String jsonString) throws Exception {

		// 将json读入map
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(jsonString, List.class);
		for (Map<String, Object> map : list) {
			InstrumentRepairPlanTaskPerson em = new InstrumentRepairPlanTaskPerson();

			// 将map信息读入实体类
			em = (InstrumentRepairPlanTaskPerson) instrumentRepairPlanDao.Map2Bean(map, em);

			instrumentRepairPlanDao.saveOrUpdate(em);
		}
	}

	/**
	 * 
	 * 删除明细,由ext ajax调用
	 * 
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delInstrumentRepairPlanTaskPerson(String id) {
		InstrumentRepairPlanTaskPerson ppi = new InstrumentRepairPlanTaskPerson();
		ppi.setId(id);
		instrumentRepairPlanDao.delete(ppi);
	}

	//检索用户明细
	public Map<String, Object> findInstrumentRepairPlanTaskStorageList(int startNum, int limitNum, String dir,
			String sort, String contextPath, String instrumentRepairPlanId, String instrumentRepairPlanTaskId)
			throws Exception {
		// String hql = "from UserCert ";
		// String where = "where user.id='" + userId + "'";
		// Map<String,Object> controlMap = userDAO.findObject(hql+where,
		// startNum,
		// limitNum, dir, sort);
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		Map<String, String> mapForCondition = new HashMap<String, String>();

		if (instrumentRepairPlanId != null && !instrumentRepairPlanId.equals("")) {
			mapForQuery.put("instrumentRepairPlanTask", "alias");
			mapForCondition.put("instrumentRepairPlanTask", "");
			mapForQuery.put("instrumentRepairPlanTask.instrumentRepairPlan.id", instrumentRepairPlanId);
			mapForCondition.put("instrumentRepairPlanTask.instrumentRepairPlan.id", "=");
		}

		if (instrumentRepairPlanTaskId != null && !instrumentRepairPlanTaskId.equals("")) {
			mapForQuery.put("instrumentRepairPlanTask.id", instrumentRepairPlanTaskId);
			mapForCondition.put("instrumentRepairPlanTask.id", "=");
		}
		Map<String, Object> controlMap = instrumentRepairPlanDao.findObjectCondition(startNum, limitNum, dir, sort,
				InstrumentRepairPlanTaskStorage.class, mapForQuery, mapForCondition);

		List<InstrumentRepairPlanTaskStorage> list = (List<InstrumentRepairPlanTaskStorage>) controlMap.get("list");
		controlMap.put("list", list);
		return controlMap;
	}

	/**
	 * 
	 * 维护明细信息,由ext ajax调用
	 * 
	 * 
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveInstrumentRepairPlanTaskStorage(String jsonString) throws Exception {

		// 将json读入map
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(jsonString, List.class);
		for (Map<String, Object> map : list) {
			InstrumentRepairPlanTaskStorage em = new InstrumentRepairPlanTaskStorage();

			// 将map信息读入实体类
			em = (InstrumentRepairPlanTaskStorage) instrumentRepairPlanDao.Map2Bean(map, em);

			instrumentRepairPlanDao.saveOrUpdate(em);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void insertInstrumentRepairPlanTaskStorage(String id, String oldId) throws Exception {

		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		Map<String, String> mapForCondition = new HashMap<String, String>();
		mapForQuery.put("instrumentRepairPlanTask", "alias");
		mapForCondition.put("instrumentRepairPlanTask", "");
		mapForQuery.put("instrumentRepairPlanTask.instrumentRepairPlan.id", oldId);
		mapForCondition.put("instrumentRepairPlanTask.instrumentRepairPlan.id", "=");

		Map<String, Object> controlMap = instrumentRepairPlanDao.findObjectCondition(-1, -1, "", "",
				InstrumentRepairPlanTaskStorage.class, mapForQuery, mapForCondition);

		InstrumentRepairPlanTask e = new InstrumentRepairPlanTask();
		e.setId(id);
		List<InstrumentRepairPlanTaskStorage> list = (List<InstrumentRepairPlanTaskStorage>) controlMap.get("list");
		for (InstrumentRepairPlanTaskStorage ets : list) {
			InstrumentRepairPlanTaskStorage es = new InstrumentRepairPlanTaskStorage();
			es.setStorage(ets.getStorage());
			es.setName(ets.getName());
			es.setNum(ets.getNum());
			es.setPrice(ets.getPrice());
			es.setInstrumentRepairPlanTask(e);
			es.setNote(ets.getNote());
			instrumentRepairPlanDao.save(es);
		}

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void insertInstrumentRepairPlanTaskPerson(String id, String oldId) throws Exception {

		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		Map<String, String> mapForCondition = new HashMap<String, String>();
		mapForQuery.put("instrumentRepairPlanTask", "alias");
		mapForCondition.put("instrumentRepairPlanTask", "");
		mapForQuery.put("instrumentRepairPlanTask.instrumentRepairPlan.id", oldId);
		mapForCondition.put("instrumentRepairPlanTask.instrumentRepairPlan.id", "=");

		Map<String, Object> controlMap = instrumentRepairPlanDao.findObjectCondition(-1, -1, "", "",
				InstrumentRepairPlanTaskPerson.class, mapForQuery, mapForCondition);

		InstrumentRepairPlanTask e = new InstrumentRepairPlanTask();
		e.setId(id);
		List<InstrumentRepairPlanTaskPerson> list = (List<InstrumentRepairPlanTaskPerson>) controlMap.get("list");
		for (InstrumentRepairPlanTaskPerson ets : list) {
			InstrumentRepairPlanTaskPerson es = new InstrumentRepairPlanTaskPerson();
			es.setScale(ets.getScale());
			es.setUser(ets.getUser());
			es.setWorkType(ets.getWorkType());
			es.setInstrumentRepairPlanTask(e);

			instrumentRepairPlanDao.save(es);
		}

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void insertInstrumentRepairPlanTask(String id, String oldId) throws Exception {

		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		Map<String, String> mapForCondition = new HashMap<String, String>();

		mapForQuery.put("instrumentRepairPlan.id", oldId);
		mapForCondition.put("instrumentRepairPlan.id", "=");

		Map<String, Object> controlMap = instrumentRepairPlanDao.findObjectCondition(-1, -1, "", "",
				InstrumentRepairPlanTask.class, mapForQuery, mapForCondition);

		InstrumentRepairPlan e = new InstrumentRepairPlan();
		e.setId(id);
		List<InstrumentRepairPlanTask> list = (List<InstrumentRepairPlanTask>) controlMap.get("list");
		for (InstrumentRepairPlanTask ets : list) {
			InstrumentRepairPlanTask es = new InstrumentRepairPlanTask();
			es.setId(ets.getId() + "FZ");
			es.setNote(ets.getNote());
			es.setType(ets.getType());
			es.setInstrumentRepairPlan(e);
			instrumentRepairPlanDao.save(es);
		}

	}

	/**
	 * 
	 * 删除明细,由ext ajax调用
	 * 
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delInstrumentRepairPlanTaskStorage(String id) {
		InstrumentRepairPlanTaskStorage ppi = new InstrumentRepairPlanTaskStorage();
		ppi.setId(id);
		instrumentRepairPlanDao.delete(ppi);
	}

	public Map<String, Object> findInstrumentRepairPlan(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		return instrumentRepairPlanDao.selectInstrumentRepairPlan(mapForQuery, startNum, limitNum, dir, sort);
	}

	/**
	 * 创建维修任务
	 * @param id
	 * @param irp
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveInstrumentRepairTask(String id, InstrumentRepair ir) throws Exception {
		Map<String, String> mapForQuery = new HashMap<String, String>();
		mapForQuery.put("instrumentRepairPlan.id", id);
		Map<String, Object> map = instrumentRepairPlanDao.selectInstrumentRepairPlanTask(mapForQuery, null, null, null,
				null);
		if ((Long) map.get("total") > 0) {
			List<InstrumentRepairPlanTask> irptList = (List<InstrumentRepairPlanTask>) map.get("list");
			for (InstrumentRepairPlanTask irpt : irptList) {
				InstrumentRepairTask irt = new InstrumentRepairTask();
				String code = systemCodeService.getSystemCode(SystemCode.INSTRUMENT_REPAIR_TASK_NAME,
						SystemCode.INSTRUMENT_REPAIR_TASK_CODE, SystemCode.INSTRUMENT_REPAIR_TASK_CODE_PREFIX,
						SystemCode.INSTRUMENT_REPAIR_TASK_CODE_LENGTH);
				irt.setId(code);
				irt.setNote(irpt.getNote());
				irt.setFee(irpt.getFee());
				irt.setType(irpt.getType());
				irt.setInstrumentRepair(ir);
				this.instrumentRepairPlanDao.saveOrUpdate(irt);
				// 创建维修人员
				this.saveInstrumentRepairTaskPerson(irpt.getId(), irt);
				// 创建维修材料
				this.saveInstrumentRepairTaskStorage(irpt.getId(), irt);
			}
		}
	}

	/**
	 * 创建维修人员
	 * @param irp
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveInstrumentRepairTaskPerson(String id, InstrumentRepairTask irt) throws Exception {
		Map<String, String> mapForQuery = new HashMap<String, String>();
		mapForQuery.put("instrumentRepairPlanTask.id", id);
		Map<String, Object> map = instrumentRepairPlanDao.selectInstrumentRepairPlanTaskPerson(mapForQuery, null, null,
				null, null);
		if ((Long) map.get("total") > 0) {
			List<InstrumentRepairPlanTaskPerson> irptplist = (List<InstrumentRepairPlanTaskPerson>) map.get("list");
			for (InstrumentRepairPlanTaskPerson irptp : irptplist) {
				InstrumentRepairTaskPerson irtp = new InstrumentRepairTaskPerson();
				irtp.setScale(irptp.getScale());
				irtp.setInstrumentRepairTask(irt);
				irtp.setUser(irptp.getUser());
				irtp.setWorkType(irptp.getWorkType());
				this.instrumentRepairPlanDao.saveOrUpdate(irtp);
			}
		}
	}

	/**
	 * 创建维修材料
	 * @param irpId
	 * @param irt
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveInstrumentRepairTaskStorage(String id, InstrumentRepairTask irt) throws Exception {
		Map<String, String> mapForQuery = new HashMap<String, String>();
		mapForQuery.put("instrumentRepairPlanTask.id", id);
		Map<String, Object> map = instrumentRepairPlanDao.selectInstrumentRepairPlanTaskStorage(mapForQuery, null,
				null, null, null);
		if ((Long) map.get("total") > 0) {
			List<InstrumentRepairPlanTaskStorage> irptslist = (List<InstrumentRepairPlanTaskStorage>) map.get("list");
			for (InstrumentRepairPlanTaskStorage irpts : irptslist) {
				InstrumentRepairTaskStorage irts = new InstrumentRepairTaskStorage();
				irts.setName(irpts.getName());
				irts.setNote(irpts.getNote());
				irts.setNum(irpts.getNum());
				irts.setPrice(irpts.getPrice());
				irts.setStorage(irpts.getStorage());
				irts.setInstrumentRepairTask(irt);
				this.instrumentRepairPlanDao.saveOrUpdate(irts);
			}
		}
	}

	/**
	 * 生成设备维修单与设备维修明细
	 * @param scplist 预计盘点单
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void createInstrumentRepair(List<InstrumentRepairPlan> irplist) throws Exception {
		for (InstrumentRepairPlan irp : irplist) {

			// 创建设备维修单
			InstrumentRepair ir = new InstrumentRepair();
			String code = systemCodeService.getSystemCode(SystemCode.INSTRUMENT_REPAIR_NAME,
					SystemCode.INSTRUMENT_REPAIR_CODE, SystemCode.INSTRUMENT_REPAIR_CODE_PREFIX,
					SystemCode.INSTRUMENT_REPAIR_CODE_LENGTH);
			ir.setId(code);
			DicType dt = new DicType();
			dt.setId("yfxwh");
			ir.setType(dt);
			ir.setNote(irp.getNote());
			ir.setPersonCode(irp.getPersonCode());
			ir.setMaterialCode(irp.getMaterialCode());
			ir.setRepairCode(irp.getRepairCode());
			ir.setInstrumentRepairPlan(irp);
			ir.setCreateUser(irp.getCreateUser());
			ir.setConfirmUser(irp.getConfirmUser());
			ir.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			ir.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			this.instrumentRepairPlanDao.saveOrUpdate(ir);
			// 创建维修任务
			this.saveInstrumentRepairTask(irp.getId(), ir);

			SysRemind sr = new SysRemind();

			sr.setHandleUser(ir.getDutyUser());
			
			String lan = (String)ServletActionContext.getRequest().getSession().getAttribute("systemLan");
			
			if(lan.equals("en")){
				
				sr.setTitle(getProperty("biolims.instrumentRepairPlan.planId","检定单号为：")+ ir.getId()+getProperty("biolims.instrumentRepairPlan.planNote","需要准备进行设备周期检定准备"));
			}
			else{
				sr.setTitle(getProperty("biolims.instrumentRepairPlan.planId","检定单号为：")+ ir.getId()+getProperty("biolims.instrumentRepairPlan.planNote","需要准备进行设备周期检定准备"));
				
			}
			
			sr.setState("0");
			sr.setTableId("instrumentRepair");
			sr.setContentId(ir.getId());
			sr.setType("1");
			sr.setRemindUser("SYSTEM");
			sr.setStartDate(new Date());

			instrumentRepairPlanDao.saveOrUpdate(sr);

		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void createInstrumentRepairQjhc(List<InstrumentRepairPlan> irplist) throws Exception {
		for (InstrumentRepairPlan irp : irplist) {

			// 创建设备维修单
			InstrumentRepair ir = new InstrumentRepair();
			String code = systemCodeService.getSystemCode(SystemCode.INSTRUMENT_REPAIR_NAME,
					SystemCode.INSTRUMENT_REPAIR_CODE, SystemCode.INSTRUMENT_REPAIR_CODE_PREFIX,
					SystemCode.INSTRUMENT_REPAIR_CODE_LENGTH);
			ir.setId(code);
			DicType dt = new DicType();
			dt.setId("qjhc");
			ir.setType(dt);
			ir.setNote(irp.getNote());
			ir.setPersonCode(irp.getPersonCode());
			ir.setMaterialCode(irp.getMaterialCode());
			ir.setRepairCode(irp.getRepairCode());
			ir.setInstrumentRepairPlan(irp);
			ir.setCreateUser(irp.getCreateUser());
			ir.setConfirmUser(irp.getConfirmUser());
			ir.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			ir.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			this.instrumentRepairPlanDao.saveOrUpdate(ir);
			// 创建维修任务
			this.saveInstrumentRepairTask(irp.getId(), ir);

			SysRemind sr = new SysRemind();

			sr.setHandleUser(irp.getDutyUser());
			String lan = (String)ServletActionContext.getRequest().getSession().getAttribute("systemLan");
			
			
			if(lan.equals("en")){
				
				sr.setTitle(getProperty("biolims.instrumentRepairPlan.planId","检定单号为：")+ ir.getId()+getProperty("biolims.instrumentRepairPlan.planNote","需要准备进行设备周期检定准备"));
			}
			else{
				sr.setTitle(getProperty("biolims.instrumentRepairPlan.planId","检定单号为：")+ ir.getId()+getProperty("biolims.instrumentRepairPlan.planNote","需要准备进行设备周期检定准备"));
				
			}
			sr.setState("0");
			sr.setTableId("instrumentRepair");
			sr.setContentId(ir.getId());
			sr.setType("1");
			sr.setRemindUser("SYSTEM");
			sr.setStartDate(new Date());

			instrumentRepairPlanDao.saveOrUpdate(sr);

		}
	}

	/**
	 * 设备维修单计划执行规则
	 * @param date 当前时间
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void rulesInstrumentRepair(Date nowDate) throws Exception {
		Date date = nowDate;
		Map<String, String> mapForQuery = new HashMap<String, String>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = formatter.format(date);
		mapForQuery.put("tsDate##@@##=##@@##date", dateString);
		mapForQuery.put("period##@@##is", "not null");
		mapForQuery.put("state.id", "1%");
		Map<String, Object> map = this.findInstrumentRepairPlan(mapForQuery, null, null, null, null);
		List<InstrumentRepairPlan> irplist = (List<InstrumentRepairPlan>) map.get("list");

		this.createInstrumentRepair(irplist);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void rulesInstrumentRepairQjhc(Date nowDate) throws Exception {
		Date date = nowDate;
		Map<String, String> mapForQuery = new HashMap<String, String>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = formatter.format(date);
		mapForQuery.put("nextDates##@@##=##@@##date", dateString);
		mapForQuery.put("period##@@##is", "not null");
		mapForQuery.put("state.id", "1%");
		mapForQuery.put("storage.studyType.id", "yestype");

		Map<String, Object> map = this.findInstrumentRepairPlan(mapForQuery, null, null, null, null);
		List<InstrumentRepairPlan> irplist = (List<InstrumentRepairPlan>) map.get("list");

		this.createInstrumentRepairQjhc(irplist);
	}

	public InstrumentRepairPlan findInstrumentRepairPlan(String instrumentId, String typeId) throws Exception {

		List<InstrumentRepairPlan> list = instrumentRepairPlanDao.selectInstrumentRepairPlan(instrumentId, typeId);
		InstrumentRepairPlan irp = null;

		if (list.size() > 0)
			irp = list.get(0);

		return irp;
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String an) throws Exception {
		List<InstrumentRepairPlanTask> list=instrumentRepairPlanDao.selectInstrumentRepairTaskPlan(an);
		InstrumentRepairPlan irp=getInstrumentRepairPlan(an);
		Date aDate = irp.getNextDate();
		String typeId = irp.getUnitName();
		Integer period = irp.getPeriod();
		Integer tsTime=irp.getTsTime();
		if (aDate != null && period != null) {
			if (typeId.equals("1")) {//天数

				aDate = DateUtil.addDay(aDate, period);
			}
			if (typeId.equals("2")) {//周数

				aDate = DateUtil.addDay(aDate, period * 7);
			}
			if (typeId.equals("3")) {//月数
				aDate = DateUtil.addMonth(aDate, period);
			}

			aDate = DateUtil.addDay(aDate, -1);

			irp.setNextDate(aDate);


			Date aDate1 = DateUtil.addDay(aDate, -tsTime);
			irp.setTsDate(aDate1);
		}
		instrumentRepairPlanDao.saveOrUpdate(irp);
		for(InstrumentRepairPlanTask insp:list){
			Instrument ins=new Instrument();
			ins=insp.getInstrument();
			ins.setNextDate(aDate);
			commonDAO.saveOrUpdate(ins);
		};
	}

	public Map<String, Object> findInstrumentRepairPlanTable(Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
			return instrumentRepairPlanDao.findInstrumentRepairPlanTable(start, length, query, col, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveInstrumentRepairPlanTask(InstrumentRepairPlan sc, String itemDataJson,String logInfo,String changeLogItem,String log) throws Exception {
		List<InstrumentRepairPlanTask> saveItems = new ArrayList<InstrumentRepairPlanTask>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		String typeId = sc.getUnitName();
		Integer period = sc.getPeriod();
		Integer tsTime= sc.getTsTime();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		for (Map<String, Object> map : list) {
			if(!map.isEmpty()) {
				InstrumentRepairPlanTask scp = new InstrumentRepairPlanTask();
				// 将map信息读入实体类
				scp = (InstrumentRepairPlanTask) instrumentRepairPlanDao.Map2Bean(map, scp);
				if (scp.getId() != null && scp.getId().equals(""))
					scp.setId(null);
				scp.setInstrumentRepairPlan(sc);
				scp.setType(sc.getType());
//				em.setUnit(an.getUnit());
				scp.setTsTime(tsTime);
				scp.setPeriod(period);
				if("1".equals(sc.getUnitName())){
					scp.setUnitName("天");
				}else if("2".equals(sc.getUnitName())){
					scp.setUnitName("周");
				}else if("3".equals(sc.getUnitName())){
					scp.setUnitName("月");
				}
				Instrument ins=null; 
				ins=instrumentService.getInstrument(scp.getInstrument().getId());
				String effectiveDate =(String) map.get("instrument-effectiveDate");
				Date effectiveDates=null;
				if(effectiveDate!=null&&!effectiveDate.equals("")){
					effectiveDates=sdf.parse(effectiveDate);
					ins.setEffectiveDate(effectiveDates);
				}
				String expiryDate=(String) map.get("instrument-expiryDate");
				if(expiryDate!=null&&!expiryDate.equals("")){
					ins.setExpiryDate(sdf.parse(expiryDate));
				}
				String useDate=(String) map.get("instrument-nextDate");
				Date useDates=null;
				if(useDate!=null&&!useDate.equals("")){
					useDates=sdf.parse(useDate);
					ins.setUseDate(useDates);																
				}else{
					useDates=effectiveDates;
					ins.setUseDate(useDates);
				}
				Date nextDates=null;
				if (useDates != null && period != null) {
					if("1".equals(sc.getUnitName())){//天数

						nextDates = DateUtil.addDay(useDates, period);
					}else if("2".equals(sc.getUnitName())){//周数

						nextDates = DateUtil.addDay(useDates, period * 7);
					}else if("3".equals(sc.getUnitName())){//月数
						nextDates = DateUtil.addMonth(useDates, period);
					}
					nextDates = DateUtil.addDay(nextDates, -1);
						ins.setNextDate(nextDates);
					Date tsDate = DateUtil.addDay(nextDates, -tsTime);
						ins.setTsDate(tsDate);
				}
				String note=(String)map.get("note");
				if(note!=null&&!note.equals("")){
					ins.setNote((String) map.get("note"));
				}
				commonDAO.saveOrUpdate(ins);
				saveItems.add(scp);
			}
		}
		instrumentRepairPlanDao.saveOrUpdateAll(saveItems);
		if (changeLogItem != null && !"".equals(changeLogItem)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("InstrumentRepairPlan");
			li.setModifyContent(changeLogItem);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			commonDAO.saveOrUpdate(li);
		}
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(InstrumentRepairPlan sc, Map jsonMap,String logInfo, Map logMap,String changeLogItem,String log) throws Exception {
		if (sc != null) {
			sc.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			sc.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			instrumentRepairPlanDao.saveOrUpdate(sc);
			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(sc.getId());
				li.setClassName("InstrumentRepairPlan");
				li.setModifyContent(logInfo);
				if("123".equals(log)) {
					li.setState("1");
					li.setStateName("数据新增");
				}else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				commonDAO.saveOrUpdate(li);
			}
		
			String jsonStr = "";
			String logStr = "";
           	logStr =  (String)logMap.get("instrumentRepairPlanTask");
			jsonStr = (String)jsonMap.get("instrumentRepairPlanTask");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveInstrumentRepairPlanTask(sc, jsonStr,logStr,changeLogItem,log);
			}
	}
   }

	public Map<String, Object> findInstrumentRepairPlanTaskTable(Integer start,
			Integer length, String query, String col,
			String sort, String id) throws Exception {
		Map<String, Object> result = instrumentRepairPlanDao.findInstrumentRepairPlanTaskTable(start, length, query,
				col, sort, id);
		List<InstrumentRepairPlanTask> list = (List<InstrumentRepairPlanTask>) result.get("list");
		return result;
	}

		@WriteOperLog
		@WriteExOperLog
		@Transactional(rollbackFor = Exception.class)
		public String addInstrument(String[] ids, String note,String instrId, String createUser,String state,String typeId,String period,String tsTime,String unitId) throws Exception {
			// 保存主表信息
			InstrumentRepairPlan si = new InstrumentRepairPlan();
			String log = ""; 
			if (instrId == null || instrId.length() <= 0
					|| "NEW".equals(instrId)) {
				log = "123";
				String code = systemCodeService.getSystemCode(SystemCode.INSTRUMENT_REPAIRPLAN_NAME,
						SystemCode.INSTRUMENT_REPAIRPLAN_CODE, SystemCode.INSTRUMENT_REPAIRPLAN_CODE_PREFIX,
						SystemCode.INSTRUMENT_REPAIRPLAN_CODE_LENGTH);
				si.setId(code);
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				DicType type = commonDAO.get(DicType.class, typeId);
				DicUnit unit = commonDAO.get(DicUnit.class,unitId);

				User u = (User) ServletActionContext
						.getRequest()
						.getSession()
						.getAttribute(
								SystemConstants.USER_SESSION_KEY);
				if(null!=tsTime&&!"".equals(tsTime)){
					Integer tsT = Integer.parseInt(tsTime);
					si.setTsTime(tsT);
				}
				if(null!=period&&!"".equals(period)){
					Integer per = Integer.parseInt(period);
					si.setPeriod(per);
				}
				si.setUnit(unit);
				si.setDutyUser(u);
				si.setCreateUser(u);
				si.setType(type);
				DicState ds = new DicState();
				ds.setId("1q");
				si.setState(ds);
				si.setStateName("NEW");
				si.setNote(note);
				instrId = si.getId();
				si.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				si.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
				commonDAO.saveOrUpdate(si);
			}else{
				si = commonDAO.get(InstrumentRepairPlan.class, instrId);
			}
			for (int i = 0; i < ids.length; i++) {
				String id = ids[i];
				List<InstrumentRepairPlanTask> list = instrumentRepairPlanDao.findInstrumentFault(id,instrId);
				if(list.size()>0) {
					return null;
				}else {
					// 通过id查询库存主数据
					Instrument s = commonDAO.get(Instrument.class, id);
					InstrumentRepairPlanTask sii = new InstrumentRepairPlanTask();
					if(null!=s){
							sii.setInstrument(s);
					}
					sii.setInstrumentRepairPlan(si);
					sii.setType(si.getType());
//					em.setUnit(an.getUnit());
					sii.setPeriod(si.getPeriod());
					sii.setTsTime(si.getTsTime());
					commonDAO.saveOrUpdate(sii);
					
					String kucun = "设备周期验证计划:"+"设备编号:"+s.getId()+"设备名称:"+s.getName()+"的数据已添加到明细";
					if (kucun != null && !"".equals(kucun)) {
						LogInfo li = new LogInfo();
						User u = (User) ServletActionContext.getRequest().getSession()
								.getAttribute(SystemConstants.USER_SESSION_KEY);
						li.setUserId(u.getId());
						li.setFileId(si.getId());
						li.setClassName("InstrumentRepairPlan");
						li.setLogDate(new Date());
						li.setModifyContent(kucun);
						if("123".equals(log)) {
							li.setState("1");
							li.setStateName("数据新增");
						}else {
							li.setState("3");
							li.setStateName("数据修改");
						}
						commonDAO.saveOrUpdate(li);
					}
				}
				
			}
			return instrId;
		}

		public Map<String, Object> findInstrumentTable(Integer start, Integer length, String query, String col,
				String sort, String id) throws Exception {Map<String, Object> result = instrumentRepairPlanDao.findInstrumentTable(start, length, query,
						col, sort);
				List<Instrument> list = (List<Instrument>) result.get("list");
				return result;}
}
