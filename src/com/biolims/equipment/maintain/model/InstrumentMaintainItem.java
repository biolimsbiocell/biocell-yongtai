package com.biolims.equipment.maintain.model;

import java.util.Date;
import java.lang.Integer;
import java.lang.String;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.GeneratedValue;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicUnit;
import com.biolims.equipment.model.Instrument;
/**   
 * @Title: Model
 * @Description: 设备维护任务单
 * @author lims-platform
 * @date 2016-12-15 23:33:02
 * @version V1.0   
 *
 */
@Entity
@Table(name = "T_INSTRUMENT_MAINTAIN_ITEM")
@SuppressWarnings("serial")
public class InstrumentMaintainItem extends EntityDao<InstrumentMaintainItem> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
	/**设备*/
	private Instrument instrument;
	/**临时表id*/
	private String tempId;
	@Column(length = 100)
	private Integer period;//周期
	
	
	private DicUnit unit;//单位
	/**设备维护*/
	private InstrumentMaintain instrumentMaintain;
	
	@Column
	private Date useDate;// 最近一次维护时间
	
	@Column(length = 30)
	private Date nextDate;// 下个检测日期
	
	@Column(name="TS_DATE")
	private Date tsDate;//提示日期
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得Instrument
	 *@return: Instrument  设备
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "INSTRUMENT")
	public Instrument getInstrument(){
		return this.instrument;
	}
	/**
	 *方法: 设置Instrument
	 *@param: Instrument  设备
	 */
	public void setInstrument(Instrument instrument){
		this.instrument = instrument;
	}
	/**
	 *方法: 取得String
	 *@return: String  临时表id
	 */
	@Column(name ="TEMP_ID", length = 50)
	public String getTempId(){
		return this.tempId;
	}
	/**
	 *方法: 设置String
	 *@param: String  临时表id
	 */
	public void setTempId(String tempId){
		this.tempId = tempId;
	}
	/**
	 *方法: 取得InstrumentMaintain
	 *@return: InstrumentMaintain  设备维护
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "INSTRUMENT_MAINTAIN")
	public InstrumentMaintain getInstrumentMaintain(){
		return this.instrumentMaintain;
	}
	/**
	 *方法: 设置InstrumentMaintain
	 *@param: InstrumentMaintain  设备维护
	 */
	public void setInstrumentMaintain(InstrumentMaintain instrumentMaintain){
		this.instrumentMaintain = instrumentMaintain;
	}
	/**
	 * @return the period
	 */
	public Integer getPeriod() {
		return period;
	}
	/**
	 * @param period the period to set
	 */
	public void setPeriod(Integer period) {
		this.period = period;
	}
	/**
	 * @return the unit
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "UNIT")
	public DicUnit getUnit() {
		return unit;
	}
	/**
	 * @param unit the unit to set
	 */
	public void setUnit(DicUnit unit) {
		this.unit = unit;
	}
	/**
	 * @return the useDate
	 */
	public Date getUseDate() {
		return useDate;
	}
	/**
	 * @param useDate the useDate to set
	 */
	public void setUseDate(Date useDate) {
		this.useDate = useDate;
	}
	/**
	 * @return the nextDate
	 */
	public Date getNextDate() {
		return nextDate;
	}
	/**
	 * @param nextDate the nextDate to set
	 */
	public void setNextDate(Date nextDate) {
		this.nextDate = nextDate;
	}
	/**
	 * @return the tsDate
	 */
	public Date getTsDate() {
		return tsDate;
	}
	/**
	 * @param tsDate the tsDate to set
	 */
	public void setTsDate(Date tsDate) {
		this.tsDate = tsDate;
	}
	
}