package com.biolims.equipment.maintain.model;

import java.lang.String;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.equipment.model.InstrumentRepairPlanTask;
/**   
 * @Title: Model
 * @Description: 设备维护临时表
 * @author lims-platform
 * @date 2016-12-15 23:33:42
 * @version V1.0   
 *
 */
/**
 * @author Biolims
 *
 */
@Entity
@Table(name = "T_INSTRUMENT_MAINTAIN_TEMP")
@SuppressWarnings("serial")
public class InstrumentMaintainTemp extends EntityDao<InstrumentMaintainTemp> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**检定计划*/
	private InstrumentRepairPlanTask instrumentRepairPlanTask;
	/**临时表状态*/
	private String state;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得
	 *@return:   检定计划
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "INSTRUMENT_REPAIR_PLAN_TASK")
	public InstrumentRepairPlanTask getInstrumentRepairPlanTask(){
		return this.instrumentRepairPlanTask;
	}
	/**
	 *方法: 设置
	 *@param:  检定计划
	 */
	public void setInstrumentRepairPlanTask(InstrumentRepairPlanTask instrumentRepairPlanTask){
		this.instrumentRepairPlanTask = instrumentRepairPlanTask;
	}
	/**
	 * @return the state
	 */
	public String getState(){
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state){
		this.state = state;
	}
	
}