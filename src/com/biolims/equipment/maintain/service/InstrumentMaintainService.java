package com.biolims.equipment.maintain.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.SystemCodeService;
import com.biolims.dic.model.DicState;
import com.biolims.dic.model.DicType;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.equipment.common.constants.SystemCode;
import com.biolims.equipment.maintain.dao.InstrumentMaintainDao;
import com.biolims.equipment.maintain.model.InstrumentMaintain;
import com.biolims.equipment.maintain.model.InstrumentMaintainItem;
import com.biolims.equipment.maintain.model.InstrumentMaintainTemp;
import com.biolims.equipment.model.Instrument;
import com.biolims.equipment.model.InstrumentFault;
import com.biolims.equipment.model.InstrumentFaultDetail;
import com.biolims.equipment.model.InstrumentRepairPlanTask;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.opensymphony.xwork2.ActionContext;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class InstrumentMaintainService {
	@Resource
	private InstrumentMaintainDao instrumentMaintainDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private SystemCodeService systemCodeService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findInstrumentMaintainList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return instrumentMaintainDao.selectInstrumentMaintainList(mapForQuery, startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(InstrumentMaintain i) throws Exception {

		instrumentMaintainDao.saveOrUpdate(i);

	}

	public InstrumentMaintain get(String id) {
		InstrumentMaintain instrumentMaintain = commonDAO.get(InstrumentMaintain.class, id);
		return instrumentMaintain;
	}

	public Map<String, Object> findInstrumentMaintainItemList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		Map<String, Object> result = instrumentMaintainDao.selectInstrumentMaintainItemList(scId, startNum, limitNum,
				dir, sort);
		List<InstrumentMaintainItem> list = (List<InstrumentMaintainItem>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveInstrumentMaintainItem(InstrumentMaintain sc, String itemDataJson) throws Exception {
		List<InstrumentMaintainItem> saveItems = new ArrayList<InstrumentMaintainItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			InstrumentMaintainItem scp = new InstrumentMaintainItem();
			// 将map信息读入实体类
			scp = (InstrumentMaintainItem) instrumentMaintainDao.Map2Bean(map, scp);

			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setInstrumentMaintain(sc);
			scp.setUseDate(sc.getMaintainDate());

			saveItems.add(scp);
		}
		instrumentMaintainDao.saveOrUpdateAll(saveItems);
	}

	/*	*//**
			 * 删除明细
			 * 
			 * @param ids
			 * @throws Exception
			 *//*
				 * @WriteOperLog
				 * 
				 * @WriteExOperLog
				 * 
				 * @Transactional(rollbackFor = Exception.class) public void
				 * delInstrumentMaintainItem(String[] ids) throws Exception { for (String id :
				 * ids) { InstrumentMaintainItem scp = instrumentMaintainDao.get(
				 * InstrumentMaintainItem.class, id); instrumentMaintainDao.delete(scp); } }
				 */

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(InstrumentMaintain sc, Map jsonMap) throws Exception {
		if (sc != null) {
			instrumentMaintainDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("instrumentMaintainItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveInstrumentMaintainItem(sc, jsonStr);
			}
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Map<String, Object> findInstrumentMaintainTempist(Map<String, String> map2Query, int startNum, int limitNum,
			String dir, String sort) throws Exception {
		return instrumentMaintainDao.findInstrumentMaintainTempist(map2Query, startNum, limitNum, dir, sort);
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void createInstrumentMaintainTemp() {
		List<InstrumentRepairPlanTask> list = instrumentMaintainDao.createInstrumentMaintainTemp();
		for (InstrumentRepairPlanTask irp : list) {
			InstrumentMaintainTemp imt = instrumentMaintainDao.getInstrumentMaintainTemp(irp.getId());
			if (imt == null) {
				imt = new InstrumentMaintainTemp();
				imt.setInstrumentRepairPlanTask(irp);
				imt.setState("3");
			}
			instrumentMaintainDao.saveOrUpdate(imt);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void instrumentMaintainSetState(String formId) {
		InstrumentMaintain im = instrumentMaintainDao.get(InstrumentMaintain.class, formId);
		im.setState("1");
		im.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_COMPLETE_NAME);
		List<InstrumentMaintainItem> list = instrumentMaintainDao.findInstrumentMaintainItem(formId);
		DicState ds = new DicState();
		ds.setId("0r3");
		for (InstrumentMaintainItem imi : list) {
			Instrument ins = commonDAO.get(Instrument.class, imi.getInstrument().getId());
			ins.setUseDate(im.getMaintainDate());
			imi.setUseDate(im.getMaintainDate());
			InstrumentRepairPlanTask irpt = instrumentMaintainDao.findInstrumentRepairPlanTask(ins.getId(),
					im.getType().getId());
			String typeId = irpt.getUnitName();
			Integer period = irpt.getPeriod();
			Integer tsTime = irpt.getTsTime();
			Date nextDates = null;
			if (ins.getUseDate() != null && period != null) {
				if (typeId.equals("天")) {// 天数

					nextDates = DateUtil.addDay(ins.getUseDate(), period);
				}
				if (typeId.equals("周")) {// 周数
					nextDates = DateUtil.addDay(ins.getUseDate(), period * 7);
				}
				if (typeId.equals("月")) {// 月数
					nextDates = DateUtil.addMonth(ins.getUseDate(), period);
				}
				nextDates = DateUtil.addDay(nextDates, -1);
				ins.setNextDate(nextDates);
				imi.setNextDate(nextDates);
				Date tsDate = DateUtil.addDay(nextDates, -tsTime);
				ins.setTsDate(tsDate);
				imi.setTsDate(tsDate);
			}
			ins.setState(ds);
			InstrumentMaintainTemp imt = commonDAO.get(InstrumentMaintainTemp.class, imi.getTempId());
			imt.setState("1");
			commonDAO.saveOrUpdate(ins);
			commonDAO.saveOrUpdate(imt);
		}
		commonDAO.saveOrUpdate(im);
	}

	public Map<String, Object> findInstrumentMaintainTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return instrumentMaintainDao.findInstrumentMaintainTable(start, length, query, col, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveInstrumentMaintainItem(InstrumentMaintain sc, String itemDataJson, String logInfo,
			String changeLogItem,String log) throws Exception {
		List<InstrumentMaintainItem> saveItems = new ArrayList<InstrumentMaintainItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			InstrumentMaintainItem scp = new InstrumentMaintainItem();
			// 将map信息读入实体类
			scp = (InstrumentMaintainItem) instrumentMaintainDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setInstrumentMaintain(sc);
			saveItems.add(scp);

		}
		instrumentMaintainDao.saveOrUpdateAll(saveItems);
		if (changeLogItem != null && !"".equals(changeLogItem)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("InstrumentMaintain");
			li.setModifyContent(changeLogItem);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delInstrumentMaintainItem(String[] ids) throws Exception {
		for (String id : ids) {
			InstrumentMaintainItem scp = instrumentMaintainDao.get(InstrumentMaintainItem.class, id);
			instrumentMaintainDao.delete(scp);
			if (ids.length > 0) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(scp.getInstrumentMaintain().getId());
				li.setClassName("InstrumentMaintain");
				li.setModifyContent("设备验证:" + "设备编号:" + scp.getInstrument().getId() + "设备名称:"
						+ scp.getInstrument().getName() + "的数据已删除");
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}
		}

	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(InstrumentMaintain sc, Map jsonMap, String logInfo, Map logMap, String changeLogItem,String log)
			throws Exception {
		if (sc != null) {
			sc.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			sc.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			instrumentMaintainDao.saveOrUpdate(sc);

			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(sc.getId());
				li.setClassName("InstrumentMaintain");
				li.setModifyContent(logInfo);
				if("123".equals(log)) {
					li.setState("1");
					li.setStateName("数据新增");
				}else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				commonDAO.saveOrUpdate(li);
			}

			String jsonStr = "";
			String logStr = "";
			logStr = (String) logMap.get("instrumentMaintainItem");
			jsonStr = (String) jsonMap.get("instrumentMaintainItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveInstrumentMaintainItem(sc, jsonStr, logStr, changeLogItem,log);
			}
		}
	}

	public Map<String, Object> findInstrumentMaintainItemTable(Integer start, Integer length, String query, String col,
			String sort, String id) throws Exception {
		Map<String, Object> result = instrumentMaintainDao.findInstrumentMaintainItemTable(start, length, query, col,
				sort, id);
		List<InstrumentMaintainItem> list = (List<InstrumentMaintainItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findInstrumentMainTempTable(Integer start, Integer length, String query, String col,
			String sort, String id) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> result = instrumentMaintainDao.findInstrumentMianTempTable(start, length, query, col, sort,
				id);
		List<InstrumentMaintainTemp> list = (List<InstrumentMaintainTemp>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String addInstrument(String[] ids, String note, String instrId, String createUser, String createDate,
			String state, String name, String maintainDate, String typeId) throws Exception {

		// 保存主表信息
		InstrumentMaintain si = new InstrumentMaintain();
		String log = "";
		if (instrId == null || instrId.length() <= 0 || "NEW".equals(instrId)) {
			log = "123";
			String modelName = "InstrumentMaintain";
			String markCode = "IM";
			String code = codingRuleService.genTransID(modelName, markCode);

			si.setId(code);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			si.setCreateUser(u);
			si.setName(name);
			DicType type = commonDAO.get(DicType.class, typeId);
			if (null != type) {
				si.setType(type);
			}
			if (null != maintainDate && !"".equals(maintainDate))
				si.setMaintainDate(sdf.parse(maintainDate));
			si.setCreateDate(sdf.parse(createDate));
			si.setState("3");
			si.setStateName("NEW");
			si.setNote(note);
			instrId = si.getId();
			si.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			si.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			commonDAO.saveOrUpdate(si);
		} else {
			si = commonDAO.get(InstrumentMaintain.class, instrId);
		}
		for (int i = 0; i < ids.length; i++) {
			String id = ids[i];
			// 通过id查询库存主数据
			InstrumentMaintainTemp s = commonDAO.get(InstrumentMaintainTemp.class, id);
			InstrumentMaintainItem sii = new InstrumentMaintainItem();
			if (null != s) {
				sii.setInstrument(s.getInstrumentRepairPlanTask().getInstrument());
				sii.setUnit(s.getInstrumentRepairPlanTask().getUnit());
				sii.setTempId(s.getId());
				sii.setPeriod(s.getInstrumentRepairPlanTask().getPeriod());
			}
			sii.setInstrumentMaintain(si);
			commonDAO.saveOrUpdate(sii);

			String kucun = "设备验证:" + "设备编号:" + s.getInstrumentRepairPlanTask().getInstrument().getId() + "设备名称:"
					+ s.getInstrumentRepairPlanTask().getInstrument().getName() + "的数据已添加到明细";
			if (kucun != null && !"".equals(kucun)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(si.getId());
				li.setClassName("InstrumentMaintain");
				li.setModifyContent(kucun);
				if("123".equals(log)) {
					li.setState("1");
					li.setStateName("数据新增");
				}else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				commonDAO.saveOrUpdate(li);
			}
		}
		return instrId;

	}
}
