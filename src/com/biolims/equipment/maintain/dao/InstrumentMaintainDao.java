package com.biolims.equipment.maintain.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.equipment.maintain.model.InstrumentMaintain;
import com.biolims.equipment.maintain.model.InstrumentMaintainItem;
import com.biolims.equipment.maintain.model.InstrumentMaintainTemp;
import com.biolims.equipment.model.Instrument;
import com.biolims.equipment.model.InstrumentRepairPlanTask;
import com.biolims.equipment.model.InstrumentRepairTemp;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class InstrumentMaintainDao extends BaseHibernateDao {
	public Map<String, Object> selectInstrumentMaintainList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from InstrumentMaintain where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<InstrumentMaintain> list = new ArrayList<InstrumentMaintain>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectInstrumentMaintainItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from InstrumentMaintainItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and instrumentMaintain.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<InstrumentMaintainItem> list = new ArrayList<InstrumentMaintainItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> findInstrumentMaintainTempist(
			Map<String, String> map2Query, Integer startNum, Integer limitNum,
			String dir, String sort) {
		String key = " ";
		String hql = " from InstrumentMaintainTemp where 1=1 and state='3'";
		if (map2Query != null)
			key = map2where(map2Query);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<InstrumentRepairTemp> list = new ArrayList<InstrumentRepairTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> findInstrumentMianTempTable(Integer start, Integer length, String query, String col, String sort,String typeId) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from InstrumentMaintainTemp where 1=1 and state='3' and instrumentRepairPlanTask.type='"+typeId+"'";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and instrumentRepairPlanTask.instrumentRepairPlan.scopeId='"+scopeId+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from InstrumentMaintainTemp where 1=1 and state='3' and instrumentRepairPlanTask.type='"+typeId+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<InstrumentMaintainTemp> list = new ArrayList<InstrumentMaintainTemp>();
			list=this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
	public List<InstrumentRepairPlanTask> createInstrumentMaintainTemp() {
		String hql = "from InstrumentRepairPlanTask where 1=1 and datediff(instrument.tsDate,now())<=0";
		String key = "";
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<InstrumentRepairPlanTask> list = new ArrayList<InstrumentRepairPlanTask>();
		if (total > 0) {
			key += " order by (datediff(instrument.nextDate,now())) ASC";
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public InstrumentMaintainTemp getInstrumentMaintainTemp(String id) {
		String hql = "from InstrumentMaintainTemp where 1=1 and instrumentRepairPlanTask.id='"
				+ id + "'";
		String key = "and state = '3'";
		List<InstrumentMaintainTemp> list = this.getSession()
				.createQuery(hql + key).list();
		InstrumentMaintainTemp imt = null;
		if (list.size()>0) {
			imt = list.get(0);
		}
		return imt;
	}

	public List<InstrumentMaintainItem> findInstrumentMaintainItem(String formId) {
		String hql="from InstrumentMaintainItem where 1=1 and instrumentMaintain.id='"+formId+"'";
		String key="";
		List<InstrumentMaintainItem> list =this.getSession()
				.createQuery(hql + key).list();
		return list;
	}

	public InstrumentRepairPlanTask findInstrumentRepairPlanTask(String instrumentId,
			String typeId) {
		String hql="from InstrumentRepairPlanTask where 1=1 and instrument.id='"+instrumentId+"' and type.id='"+typeId+"'";
		List<InstrumentRepairPlanTask> list =this.getSession()
				.createQuery(hql).list();
		return list.get(0);
	}

	public Map<String, Object> findInstrumentMaintainTable(Integer start,Integer length, String query, String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from  InstrumentMaintain where 1=1";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from InstrumentMaintain where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<InstrumentMaintain> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}


	public Map<String, Object> findInstrumentMaintainItemTable(Integer start, Integer length, String query, String col, String sort, String id) throws Exception {
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from InstrumentMaintainItem where 1=1 and instrumentMaintain.id='"+id+"'";
			String key = "";

			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
				String hql = "from InstrumentMaintainItem where 1=1 and instrumentMaintain.id='"+id+"' ";
				if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
					col=col.replace("-", ".");
					key+=" order by "+col+" "+sort;
				}
				List<InstrumentMaintainItem> list = new ArrayList<InstrumentMaintainItem>();
				list=this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
			}
			return map;
		}


}