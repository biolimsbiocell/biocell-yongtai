package com.biolims.equipment.maintain.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.equipment.common.constants.SystemCode;
import com.biolims.equipment.maintain.model.InstrumentMaintain;
import com.biolims.equipment.maintain.model.InstrumentMaintainItem;
import com.biolims.equipment.maintain.model.InstrumentMaintainTemp;
import com.biolims.equipment.maintain.service.InstrumentMaintainService;
import com.biolims.equipment.model.Instrument;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.util.HttpUtils;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.common.service.SystemCodeService;
import com.biolims.dic.model.DicType;
import com.biolims.file.service.FileInfoService;
import com.opensymphony.xwork2.ActionContext;
@Namespace("/equipment/maintain")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class InstrumentMaintainAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "4035";
	@Autowired
	private InstrumentMaintainService instrumentMaintainService;
	private InstrumentMaintain instrumentMaintain = new InstrumentMaintain();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	/*new*/
	@Resource
	private CommonService commonService;	
	@Resource
	private FieldService fieldService;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SystemCodeService systemCodeService;
	private String log = "";
	
	@Action(value = "showInstrumentMaintainList")
	public String showInstrumentMaintainList() throws Exception {
		rightsId = "4035";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/equipment/maintain/instrumentMaintain.jsp");
	}
	@Action(value = "showInstrumentMaintainEditList")
	public String showInstrumentMaintainEditList() throws Exception {
		rightsId = "4035";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/equipment/maintain/instrumentMaintainEditList.jsp");
	}
	@Action(value = "showInstrumentMaintainTableJson")
	public void showInstrumentMaintainTableJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {	
			Map<String, Object> result = instrumentMaintainService.findInstrumentMaintainTable(
					start, length, query, col, sort);
			Long count = (Long) result.get("total");
			List<InstrumentMaintain> list = (List<InstrumentMaintain>) result.get("list");
	
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("stateName", "");
			map.put("maintainDate", "yyyy-MM-dd");
			map.put("note", "");
				
			String data=new SendData().getDateJsonForDatatable(map, list);
			//根据模块查询自定义字段数据
			Map<String,Object> mapField = fieldService.findFieldByModuleValue("InstrumentMaintain");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result,dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Action(value = "instrumentMaintainSelectTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogInstrumentMaintainList() throws Exception {
		rightsId = "4035";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/equipment/maintain/instrumentMaintainSelectTable.jsp");
	}
	@Action(value = "editInstrumentMaintain")
	public String editInstrumentMaintain() throws Exception {
		rightsId = "4035";
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			instrumentMaintain = instrumentMaintainService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "instrumentMaintain");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			instrumentMaintain.setCreateUser(user);
			instrumentMaintain.setCreateDate(new Date());
			DicType type = commonDAO.get(DicType.class, "dailyMaintenance");
			instrumentMaintain.setType(type);
			this.instrumentMaintain.setId(SystemCode.DEFAULT_SYSTEMCODE);
			instrumentMaintain.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			instrumentMaintain.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			instrumentMaintain.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			instrumentMaintain.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
		
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(instrumentMaintain.getState());
		instrumentMaintainService.createInstrumentMaintainTemp();
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/equipment/maintain/instrumentMaintainEdit.jsp");
	}
	@Action(value = "copyInstrumentMaintain")
	public String copyInstrumentMaintain() throws Exception {
		rightsId = "4035";
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		instrumentMaintain = instrumentMaintainService.get(id);
		instrumentMaintain.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/equipment/maintain/instrumentMaintainEdit.jsp");
	}
	@Action(value = "save")
	public void save() throws Exception {
		
		
		
		String msg = "";
		String zId = "";
		boolean bool = true;	
		
		try {
			String dataValue = getParameterFromRequest("dataValue");
			String str = "["+dataValue+"]";
			String changeLog = getParameterFromRequest("changeLog");
			String changeLogItem = getParameterFromRequest("changeLogItem");
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					str, List.class);
			
			for (Map<String, Object> map : list) {
				instrumentMaintain = (InstrumentMaintain)commonDAO.Map2Bean(map, instrumentMaintain);
			}
			String id = instrumentMaintain.getId();
			if(id!=null&&id.equals("")){
				instrumentMaintain.setId(null);
			}
			if (id == null || id.length() <= 0 || "NEW".equals(id)) {
				log = "123";
				String modelName = "InstrumentMaintain";
				String markCode = "IM";
				String code = codingRuleService.genTransID(modelName, markCode);
				 instrumentMaintain.setId(code);
			}
			Map aMap = new HashMap();
			Map lMap = new HashMap();
				aMap.put("instrumentMaintainItem",getParameterFromRequest("instrumentMaintainItemJson"));
			
			
			instrumentMaintainService.save(instrumentMaintain,aMap,changeLog,lMap,changeLogItem,log);
			
			zId = instrumentMaintain.getId();
		} catch (Exception e) {
			e.printStackTrace();
			msg = "保存失败！";
			bool = false;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("msg", msg);
		map.put("success", bool);
		map.put("id", zId);
	
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null")) {
			map.put("bpmTaskId", bpmTaskId);
			
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "viewInstrumentMaintain")
	public String toViewInstrumentMaintain() throws Exception {
		rightsId = "4035";
		String id = getParameterFromRequest("id");
		instrumentMaintain = instrumentMaintainService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/equipment/maintain/instrumentMaintainEdit.jsp");
	}
	
	@Action(value = "showInstrumentMaintainItemTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showInstrumentMaintainItemTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = instrumentMaintainService
				.findInstrumentMaintainItemTable(start, length, query, col, sort,
						id);
		List<InstrumentMaintainItem> list=(List<InstrumentMaintainItem>) result.get("list");
		Map<String, String> map=new HashMap<String, String>();
			map.put("instrumentMaintain-name", "");
			map.put("instrumentMaintain-id", "");
			map.put("instrument-name", "");
			map.put("instrument-id", "");
			map.put("useDate", "yyyy-MM-dd");
			map.put("nextDate", "yyyy-MM-dd");
			map.put("tsDate", "yyyy-MM-dd");
			map.put("period", "");
			map.put("unit-name", "");
			map.put("unit-id", "");
			map.put("tempId", "");
			map.put("id", "");
		String data=new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw,result,data));
	}
	/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delInstrumentMaintainItem")
	public void delInstrumentMaintainItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			instrumentMaintainService.delInstrumentMaintainItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	@Action(value = "saveInstrumentMaintainItemTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveInstrumentMaintainItemTable() throws Exception {
		Map aMap = new HashMap();
		Map lMap = new HashMap();
		
		String id=getParameterFromRequest("id");
		instrumentMaintain=commonService.get(InstrumentMaintain.class, id);
		Map<String, Object> map=new HashMap<String, Object>();
		aMap.put("instrumentMaintainItem",
				getParameterFromRequest("dataJson"));
		lMap.put("instrumentMaintainItem",
				getParameterFromRequest("changeLog"));
		try {
			instrumentMaintainService.save(instrumentMaintain, aMap,"",lMap,"",log);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "saveInstrumentMaintainTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveInstrumentMaintainTable() throws Exception {
		String dataValue = getParameterFromRequest("dataJson");
		String changeLog = getParameterFromRequest("changeLog");
		
		String str = "["+dataValue+"]";
			
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					str, List.class);
			
		Map<String, Object> map=new HashMap<String, Object>();
		try {
			
		for (Map<String, Object> map1 : list) {
		
			InstrumentMaintain a = new InstrumentMaintain();
		
			Map aMap = new HashMap();
			Map lMap = new HashMap();
			a = (InstrumentMaintain)commonDAO.Map2Bean(map1, a);
			a.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			a.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
		
			instrumentMaintainService.save(a,aMap,changeLog,lMap,changeLog,log);
			map.put("id", a.getId());
		}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	@Action(value = "showInstrumentMaintainTempJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showInstrumentMaintainTempJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = instrumentMaintainService.findInstrumentMainTempTable(start, length, query, col, sort,id);
		List<InstrumentMaintainTemp> list=(List<InstrumentMaintainTemp>) result.get("list");
		Map<String, String> map=new HashMap<String, String>();
		map.put("id", "");
		map.put("instrumentRepairPlanTask-id", "");
		map.put("instrumentRepairPlanTask-instrument-id", "");
		map.put("instrumentRepairPlanTask-instrument-name", "");
		map.put("instrumentRepairPlanTask-type-id", "");
		map.put("instrumentRepairPlanTask-type-name", "");
		map.put("instrumentRepairPlanTask-instrument-nextDate", "yyyy-MM-dd");
		map.put("instrumentRepairPlanTask-period", "####");
		map.put("instrumentRepairPlanTask-unit-id", "");
		map.put("instrumentRepairPlanTask-unit-name", "");
		map.put("instrumentRepairPlanTask-note", "");
		String data=new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw,result,data));
	}
	/**
	 * 
	 * @Title: addInstrument
	 * @Description: 添加维护明细信息
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "addInstrumentMaintainItem")
	public void addInstrumentMaintainItem() throws Exception {
		String note = getParameterFromRequest("note");
		String instrId = getParameterFromRequest("id");
		String createDate = getParameterFromRequest("createDate");
		String createUser = getParameterFromRequest("createUser");
		String state = getParameterFromRequest("state");
		String name = getParameterFromRequest("name");
		String typeId = getParameterFromRequest("typeId");
		String maintainDate = getParameterFromRequest("maintainDate");
		
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String instrumentRepairId= instrumentMaintainService.addInstrument(ids, note, instrId, createUser, createDate, state, name,maintainDate,typeId);
			result.put("success", true);
			result.put("data", instrumentRepairId);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	/*old*/
	/*@Action(value = "showInstrumentMaintainList")
	public String showInstrumentMaintainList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/equipment/maintain/instrumentMaintain.jsp");
	}*/

	@Action(value = "showInstrumentMaintainListJson")
	public void showInstrumentMaintainListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = instrumentMaintainService.findInstrumentMaintainList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<InstrumentMaintain> list = (List<InstrumentMaintain>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	/*@Action(value = "instrumentMaintainSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogInstrumentMaintainList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/equipment/maintain/instrumentMaintainDialog.jsp");
	}*/

	@Action(value = "showDialogInstrumentMaintainListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogInstrumentMaintainListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = instrumentMaintainService.findInstrumentMaintainList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<InstrumentMaintain> list = (List<InstrumentMaintain>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	/*@Action(value = "editInstrumentMaintain")
	public String editInstrumentMaintain() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			instrumentMaintain = instrumentMaintainService.get(id);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "instrumentMaintain");
			toState(instrumentMaintain.getState());
			if (instrumentMaintain.getState().equals(
					SystemConstants.DIC_STATE_YES))
				putObjToContext("handlemethod",SystemConstants.PAGE_HANDLE_METHOD_VIEW);
			else
				putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			instrumentMaintain.setCreateUser(user);
			instrumentMaintain.setCreateDate(new Date());
			this.instrumentMaintain.setId(SystemCode.DEFAULT_SYSTEMCODE);
			instrumentMaintain.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			instrumentMaintain.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		instrumentMaintainService.createInstrumentMaintainTemp();
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/equipment/maintain/instrumentMaintainEdit.jsp");
	}
*/
/*	@Action(value = "copyInstrumentMaintain")
	public String copyInstrumentMaintain() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		instrumentMaintain = instrumentMaintainService.get(id);
		instrumentMaintain.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/equipment/maintain/instrumentMaintainEdit.jsp");
	}
*/

/*	@Action(value = "save")
	public String save() throws Exception {
		String id = instrumentMaintain.getId();
		if (id == null || id.length() <= 0 || "NEW".equals(id)) {
			String modelName = "Instrument";
			 String markCode="WH_MAN";
			 String code=null;
			 code = codingRuleService.genTransID(modelName,markCode);
			 instrumentMaintain.setId(code);
		}
		Map aMap = new HashMap();
			aMap.put("instrumentMaintainItem",getParameterFromRequest("instrumentMaintainItemJson"));
		
		instrumentMaintainService.save(instrumentMaintain,aMap);
		return redirect("/equipment/maintain/instrumentMaintain/editInstrumentMaintain.action?id=" + instrumentMaintain.getId());

	}*/

/*	@Action(value = "viewInstrumentMaintain")
	public String toViewInstrumentMaintain() throws Exception {
		String id = getParameterFromRequest("id");
		instrumentMaintain = instrumentMaintainService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/equipment/maintain/instrumentMaintainEdit.jsp");
	}
	*/

	@Action(value = "showInstrumentMaintainItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showInstrumentMaintainItemList() throws Exception {
		return dispatcher("/WEB-INF/page/equipment/maintain/instrumentMaintainItem.jsp");
	}

	@Action(value = "showInstrumentMaintainItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showInstrumentMaintainItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = instrumentMaintainService.findInstrumentMaintainItemList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<InstrumentMaintainItem> list = (List<InstrumentMaintainItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("instrumentMaintain-id", "");
			map.put("instrumentMaintain-name", "");
			map.put("instrumentMaintain-type-id", "");
			map.put("instrumentMaintain-type-name", "");
			map.put("instrument-name", "");
			map.put("useDate", "yyyy-MM-dd");
			map.put("nextDate", "yyyy-MM-dd");
			map.put("tsDate", "yyyy-MM-dd");
			map.put("instrument-id", "");
			map.put("tempId", "");
			map.put("period", "####");
			map.put("unit-id", "");
			map.put("unit-name", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
/*		*//**
	 * 删除明细信息
	 * @throws Exception
	 *//*
	@Action(value = "delInstrumentMaintainItem")
	public void delInstrumentMaintainItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			instrumentMaintainService.delInstrumentMaintainItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	*/
	@Action(value = "showInstrumentMaintainTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showInstrumentMaintainTempList() throws Exception {
		return dispatcher("/WEB-INF/page/equipment/maintain/instrumentMaintainTemp.jsp");
	}

	@Action(value = "showInstrumentMaintainTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showInstrumentMaintainTempListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		try {
			Map<String, Object> result = instrumentMaintainService.findInstrumentMaintainTempist(map2Query,startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<InstrumentMaintainTemp> list = (List<InstrumentMaintainTemp>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("instrumentRepairPlanTask-id", "");
			map.put("instrumentRepairPlanTask-instrument-id", "");
			map.put("instrumentRepairPlanTask-instrument-name", "");
			map.put("instrumentRepairPlanTask-type-id", "");
			map.put("instrumentRepairPlanTask-type-name", "");
			map.put("instrumentRepairPlanTask-instrument-nextDate", "yyyy-MM-dd");
			map.put("instrumentRepairPlanTask-period", "####");
			map.put("instrumentRepairPlanTask-unit-id", "");
			map.put("instrumentRepairPlanTask-unit-name", "");
			map.put("instrumentRepairPlanTask-note", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public InstrumentMaintainService getInstrumentMaintainService() {
		return instrumentMaintainService;
	}

	public void setInstrumentMaintainService(InstrumentMaintainService instrumentMaintainService) {
		this.instrumentMaintainService = instrumentMaintainService;
	}

	public InstrumentMaintain getInstrumentMaintain() {
		return instrumentMaintain;
	}

	public void setInstrumentMaintain(InstrumentMaintain instrumentMaintain) {
		this.instrumentMaintain = instrumentMaintain;
	}


}
