/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：InstrumentService
 * 类描述：设备管理service
 * 创建人：倪毅
 * 创建时间：2011-12
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.equipment.common.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.biolims.common.service.CommonService;
import com.biolims.equipment.common.dao.InstrumentCommonDao;
import com.biolims.equipment.model.Instrument;
import com.biolims.util.BeanUtils;

@Service
public class InstrumentCommonService extends CommonService {

	@Resource
	private InstrumentCommonDao instrumentCommonDao;

	/**
	 * 检索list,采用map方式传递检索参数
	 * 
	 * @return 参数值MAP，如果传递的为null返回空字符串
	 */
	public Map<String, Object> findInstrumentList(int startNum, int limitNum,
			String dir, String sort, String data) throws Exception {
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		Map<String, String> mapForCondition = new HashMap<String, String>();
		if (data != null && !data.equals("")) {

			BeanUtils.data2Map(data, mapForQuery, mapForCondition);
		}
		Map<String, Object> controlMap = findObjectCondition(startNum,
				limitNum, dir, sort, Instrument.class, mapForQuery,
				mapForCondition);
		return controlMap;
	}

	public Instrument getInstrument(String id) throws Exception {
		Instrument ao = get(Instrument.class, id);
		return ao;
	}

	public Map<String, Object> findInstrumentList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		return this.instrumentCommonDao.selectInstrumentList(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	public List<Instrument> findInstrumentListByType(String type)
			throws Exception {

		List<Instrument> list = new ArrayList<Instrument>();
		list = instrumentCommonDao.selectInstrumentListByType(type);
		return list;
	}

	/*
	 * public List<Instrument> findInstrumentListByTypes(String type) throws
	 * Exception { List<Instrument> list = new ArrayList<Instrument>(); list =
	 * instrumentCommonDao.selectInstrumentListByTypes(type); return list; }
	 */
}
