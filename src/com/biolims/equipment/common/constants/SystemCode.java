package com.biolims.equipment.common.constants;

//系统业务数据的编码
public class SystemCode {

	public static final String DEFAULT_SYSTEMCODE = "NEW";
	/**
	 * 设备-设备故障管理FN000-FN999
	 */
	public static final long INSTRUMENT_PAYMENT_CODE = 000;
	/**
	 * 设备-设备故障
	 */
	public static final String INSTRUMENT_PAYMENT_NAME = "";
	/**
	 * 设备-设备维修管理W0000-W9999
	 */
	public static final long INSTRUMENT_FAULT_CODE = 0000;

	public static final String INSTRUMENT_FAULT_NAME = "InstrumentFault";

	public static final Integer INSTRUMENT_FAULT_CODE_LENGTH = 4;
	/**
	 * 设备-设备维护管理W0000-W9999
	 */
	public static final String INSTRUMENT_MAINTAIN_NAME = "InstrumentMainatin";
	public static final long INSTRUMENT_MAINTAIN_CODE = 0000;
	public static final Integer INSTRUMENT_MAINTAIN_CODE_LENGTH = 4;
	public static final String INSTRUMENT_MAINTAIN_CODE_PREFIX = "IM";
	/**
	 * 设备-设备维修管理前缀
	 */
	public static final String INSTRUMENT_FAULT_CODE_PREFIX = "F";

	/**
	 * 设备-设备维修管理W0000-W9999
	 */
	public static final long INSTRUMENT_REPAIR_CODE = 0000;
	/**
	 * 设备-设备维修
	 */
	public static final String INSTRUMENT_REPAIR_NAME = "InstrumentRepair";
	/**
	 * 设备-设备维修管理前缀
	 */
	public static final String INSTRUMENT_REPAIR_CODE_PREFIX = "W";
	/**
	 * 设备-设备维修管理去除前缀后的位数
	 */
	public static final Integer INSTRUMENT_REPAIR_CODE_LENGTH = 4;
	/**
	 * 设备-预防性维修管理PM000-999
	 */
	public static final long INSTRUMENT_REPAIRPLAN_CODE = 000;
	/**
	 * 设备-预防性维修
	 */
	public static final String INSTRUMENT_REPAIRPLAN_NAME = "InstrumentRepairPlan";
	/**
	 * 设备-预防性维修前缀
	 */
	public static final String INSTRUMENT_REPAIRPLAN_CODE_PREFIX = "PM";
	/**
	 * 设备-设备维修管理去除前缀后的位数
	 */
	public static final Integer INSTRUMENT_REPAIRPLAN_CODE_LENGTH = 3;

	/**
	 * 设备-预防性维修-任务列表PM000-999
	 */
	public static final long INSTRUMENT_REPAIR_TASK_CODE = 00000;
	/**
	 * 设备维修-任务列表
	 */
	public static final String INSTRUMENT_REPAIR_TASK_NAME = "InstrumentRepairTask";
	/**
	 * 设备维修前缀-任务列表
	 */
	public static final String INSTRUMENT_REPAIR_TASK_CODE_PREFIX = "W";
	/**
	 * 设备维修管理去除前缀后的位数-任务列表
	 */
	public static final Integer INSTRUMENT_REPAIR_TASK_CODE_LENGTH = 5;

	/**
	 * 设备-借用调拨管理100-999
	 */
	public static final long INSTRUMENT_BORROW_CODE = 100;
	/**
	 * 设备-借用调拨
	 */
	public static final String INSTRUMENT_BORROW_NAME = "InstrumentBorrow";

}
