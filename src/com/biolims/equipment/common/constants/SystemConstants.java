package com.biolims.equipment.common.constants;

public class SystemConstants extends com.biolims.common.constants.SystemConstants {
	public static final String DIC_TYPE_INSTRUMENT_BORROW_JY = "jy";
	public static final String DIC_TYPE_INSTRUMENT_BORROW_DB = "db";
}
