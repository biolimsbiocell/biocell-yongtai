package com.biolims.equipment.common.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.equipment.model.Instrument;

@Repository
public class InstrumentCommonDao extends BaseHibernateDao {

	@SuppressWarnings("unchecked")
	public Map<String, Object> selectInstrumentList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = " from Instrument where 1=1";
		String key = "";
		if (mapForQuery != null)
			key = map2where4Split(mapForQuery);

		Long total = (Long) this.getSession().createQuery("select count(*)" + hql + key).uniqueResult();
		List<Instrument> list = new ArrayList<Instrument>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by id desc";
			}
			if (startNum != null && limitNum != null) {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			} else {
				list = this.getSession().createQuery(hql + key).list();
			}

		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("total", total);
		map.put("list", list);
		return map;
	}

	/**
	 * 单个设备类型
	 * @param type
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<Instrument> selectInstrumentListByType(String type) throws Exception {
		String hql = " from Instrument where 1=1";
		String key = "";
		if (type != null) {
			key = key + " and type.sysCode = '" + type + "' and state like '1%'";
		}
		List<Instrument> list = new ArrayList<Instrument>();
		list = this.getSession().createQuery(hql + key).list();
		return list;
	}

	/**
	 * 多个设备类型
	 * @param type
	 * @return
	 * @throws Exception
	 */
	/*@SuppressWarnings("unchecked")
	public List<Instrument> selectInstrumentListByTypes(String type) throws Exception {
		String hql = " from Instrument where 1=1";
		String key = "";
		if (type != null) {
			key = key + " and type.sysCode in '" + type + "' and state like '1%'";
		}
		List<Instrument> list = new ArrayList<Instrument>();
		list = this.getSession().createQuery(hql + key).list();
		return list;
	}*/
}
