/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：Action
 * 类描述：设备管理ACTION
 * 创建人：vera
 * 创建时间：2011-11
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.equipment.common.action;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.equipment.common.constants.SystemConstants;
import com.biolims.equipment.common.service.InstrumentCommonService;
import com.biolims.equipment.model.Instrument;
import com.biolims.storage.model.Storage;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

/**
 * 库存主数据
 */
@Controller
@Scope("prototype")
@ParentPackage("default")
@Namespace("/equipment/common")
@SuppressWarnings("unchecked")
public class InstrumentCommonAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;

	@Autowired
	private InstrumentCommonService instrumentService;

	@Action(value = "instrumentSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String instrumentSelect() throws Exception {

		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();

		map.put("id", new String[] { "", "string", "", "编码", "150", "true", "", "", "", "", "", "" });
		map.put("note", new String[] { "", "string", "", "名称", "150", "true", "", "", "", "", "", "" });
		map.put("type-name", new String[] { "", "string", "", "类型", "150", "false", "", "", "", "", "", "" });
		map.put("department-id", new String[] { "", "string", "", "部门ID", "150", "false", "true", "", "", "", "", "" });
		map.put("department-name",
				new String[] { "", "string", "", "部门名称", "150", "false", "true", "", "", "", "", "" });
		map.put("dutyUser-id", new String[] { "", "string", "", "人员ID", "150", "false", "true", "", "", "", "", "" });
		map.put("dutyUser-name", new String[] { "", "string", "", "姓名", "150", "false", "true", "", "", "", "", "" });
		map.put("spec", new String[] { "", "string", "", "规格参数", "250", "false", "", "", "", "", "", "" });
		map.put("searchCode", new String[] { "", "string", "", "检索码", "150", "true", "true", "", "", "", "", "" });
		map.put("purchaseDate", new String[] { "", "string", "", "购置日期", "150", "true", "true", "", "", "", "", "" });
		String type = generalexttype(map);
		String col = generalextcol(map);
		super.getRequest().setAttribute("type", type);
		super.getRequest().setAttribute("col", col);
		// 用于判断当前页面类型,LIST类型
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		super.getRequest().setAttribute("path",
				super.getRequest().getContextPath() + "/equipment/common/instrumentSelectJson.action");
		return dispatcher("/WEB-INF/page/equipment/common/instrumentSelect.jsp");
	}

	/**
	 * 库存主数据列表
	 * 
	 * @throws Exception
	 */
	@Action(value = "instrumentSelectJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void instrumentSelectJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, Object> controlMap = instrumentService.findInstrumentList(startNum, limitNum, dir, sort, data);

		List<Storage> list = (List<Storage>) controlMap.get("list");
		Long count = (Long) controlMap.get("totalCount");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("note", "");
		map.put("type-name", "");
		map.put("department-id", "");
		map.put("department-name", "");
		map.put("dutyUser-id", "");
		map.put("dutyUser-name", "");
		map.put("spec", "");
		map.put("purchaseDate", "yyyy-MM-dd");
		map.put("searchCode", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	/**
	 * 选择-查询设备
	 * @return
	 * @throws Exception
	 */
	@Action(value = "selEquipmentList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String selEquipmentList() throws Exception {
		return dispatcher("/WEB-INF/page/equipment/common/selEquipmentList.jsp");
	}

	@Action(value = "selEquipmentListJosn", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selEquipmentListJosn() throws Exception {
		// 开始记录数
		Integer startNum = (getParameterFromRequest("start") == "" ? null : Integer
				.parseInt(getParameterFromRequest("start")));
		// limit
		Integer limitNum = (getParameterFromRequest("limit") == "" ? null : Integer
				.parseInt(getParameterFromRequest("limit")));
		//字段
		String dir = getParameterFromRequest("dir");
		//排序方式
		String sort = getParameterFromRequest("sort");

		String data = getRequest().getParameter("data");

		Map<String, String> mapForQuery = new HashMap<String, String>();
		if (data != null && data.length() > 0) {
			Map<String, String> map = JsonUtils.toObjectByJson(data, Map.class);
			if (map.get("id") != null && map.get("id").length() > 0) {
				mapForQuery.put("id", "like##@@##'%" + map.get("id") + "%' ");
			}
			if (map.get("note") != null && map.get("note").length() > 0) {
				mapForQuery.put("name", "like##@@##'%" + map.get("note") + "%'");
			}
		}

		Map<String, Object> result = this.instrumentService.findInstrumentList(mapForQuery, startNum, limitNum, dir,
				sort);

		long total = Long.parseLong(result.get("total").toString());

		List<Instrument> list = (List<Instrument>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "id");
		map.put("name", "");
		map.put("searchCode", "searchCode");
		map.put("spec", "");
		map.put("department-name", "");
		map.put("unit-name", "");
		map.put("unit-id", "unit-id");
		map.put("note", "");

		new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
	}
}
