package com.biolims.equipment.main.service;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.equipment.model.InstrumentState;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleState;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class InstrumentStateService {

	@Resource
	private CommonDAO commonDAO;

	@Resource
	private CommonService commonService;

	/**
	 * @param note8 
	 * 
	 * @Title: saveInstrumentState
	 * @Description: TODO(保存设备各个实验状态)
	 * @param @param code
	 * @param @param sampleCode
	 * @param @param productId
	 * @param @param productName
	 * @param @param stageTime
	 * @param @param startDate
	 * @param @param endDate
	 * @param @param tableTypeId
	 * @param @param stageName
	 * @param @param acceptUser
	 * @param @param taskId
	 * @param @param taskMethod
	 * @param @param taskResult
	 * @param @param techTaskId
	 * @param @param note
	 * @param @param note2
	 * @param @param note3
	 * @param @param note4
	 * @param @param note5
	 * @param @param note6
	 * @param @param note7
	 * @param @throws Exception    设定文件
	 * @return void    返回类型
	 * @author zhiqiang.yang@biolims.cn
	 * @date 2017-11-10 上午10:09:33
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveInstrumentState(String instrumentCode, String sampleCode,
			String instrumentName, String instrState, String productId,
			String productName, String stageTime, String startDate,
			String endDate, String tableTypeId, String stageName,
			String acceptUser, String taskId, String taskMethod,
			String taskResult, String techTaskId, String mode,
			String userGroup, String note, String note2, String note3,
			String note4, String note5, String note6, String note7, String  note8)
			throws Exception {
		InstrumentState instrumentState = new InstrumentState();
		instrumentState.setInstrumentCode(instrumentCode);
		instrumentState.setInstrumentName(instrumentName);
		instrumentState.setInstrumentState(instrState);
		instrumentState.setProductId(productId);
		instrumentState.setProductName(productName);
		instrumentState.setStageTime(stageTime);
		instrumentState.setStartDate(startDate);
		instrumentState.setEndDate(endDate);
		instrumentState.setTableTypeId(tableTypeId);
		instrumentState.setStageName(stageName);
		instrumentState.setAcceptUser(acceptUser);
		instrumentState.setTaskId(taskId);
		instrumentState.setTaskMethod(taskMethod);
		instrumentState.setTaskResult(taskResult);
		instrumentState.setTechTaskId(techTaskId);
		instrumentState.setMode(mode);
		instrumentState.setUserGroup(userGroup);
		instrumentState.setNote(note);
		instrumentState.setNote2(note2);
		instrumentState.setNote3(note3);
		instrumentState.setNote4(note4);
		instrumentState.setNote5(note5);
		instrumentState.setNote6(note6);
		instrumentState.setNote7(note7);
		
		if (sampleCode != null && !sampleCode.equals("")) {
			String[] scode = sampleCode.split(",");
			for (int i = 0; i < scode.length; i++) {
				List<SampleInfo> sil = commonService.get(SampleInfo.class,
						"code", scode[i]);
				if (sil.size() > 0) {
					SampleInfo si = sil.get(0);
					si.setState(tableTypeId);
					si.setStateName(stageName);
					commonDAO.saveOrUpdate(si);
				}
			}
		}
		if (!startDate.equals(null) && !endDate.equals(null)) {
			
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	        long between = sdf.parse(endDate).getTime() - sdf.parse(startDate).getTime();
	        
			long day = between / (24 * 60 * 60 * 1000);
			long hour = (between / (60 * 60 * 1000) - day * 24);
			long min = ((between / (60 * 1000)) - day * 24 * 60 - hour * 60);
			long s = (between / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
			long ms = (between - day * 24 * 60 * 60 * 1000 - hour * 60 * 60
					* 1000 - min * 60 * 1000 - s * 1000);
			String timeDifference = day + "天" + hour + "小时" + min + "分" + s
					+ "秒" + ms + "毫秒";
			instrumentState.setCountData(timeDifference);
		}
		commonDAO.saveOrUpdate(instrumentState);
	}

}
