/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：InstrumentService
 * 类描述：设备管理service
 * 创建人：倪毅
 * 创建时间：2011-12
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.equipment.main.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.equipment.main.dao.InstrumentDao;
import com.biolims.equipment.model.IncubatorSampleInfo;
import com.biolims.equipment.model.Instrument;
import com.biolims.equipment.model.InstrumentState;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.storage.container.model.StorageContainer;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;
import com.opensymphony.xwork2.ActionContext;

@Service
@SuppressWarnings("unchecked")
public class InstrumentService {
	/*
	 * @Resource private CommonDAO commonDAO;
	 */
	@Resource
	private InstrumentDao instrumentDAO;

	@Resource
	private InstrumentDao instrumentDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();

	/**
	 * 检索list,采用map方式传递检索参数
	 * 
	 * @return 参数值MAP，如果传递的为null返回空字符串
	 */
	public Map<String, Object> findInstrumentList(int startNum, int limitNum, String dir, String sort, String data)
			throws Exception {
		// 检索条件map,每个字段检索值
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		// 检索方式map,每个字段的检索方式
		Map<String, String> mapForCondition = new HashMap<String, String>();
		if (data != null && !data.equals("")) {

			BeanUtils.data2Map(data, mapForQuery, mapForCondition);
		}
		Map<String, Object> controlMap = commonDAO.findObjectCondition(startNum, limitNum, dir, sort, Instrument.class,
				mapForQuery, mapForCondition);
		return controlMap;
	}

	public Map<String, Object> selectInstrumentList(int startNum, int limitNum, String dir, String sort, String data,
			String p_type) throws Exception {
		// 检索条件map,每个字段检索值
		Map<String, String> mapForQuery = new HashMap<String, String>();
		// 检索方式map,每个字段的检索方式
		if (data != null && !data.equals("")) {
			mapForQuery = JsonUtils.toObjectByJson(data, Map.class);
		}
		Map<String, Object> controlMap = instrumentDAO.selectInstrument(mapForQuery, startNum, limitNum, dir, sort,
				p_type);
		return controlMap;
	}

	public Instrument getInstrument(String id) throws Exception {
		Instrument ao = commonDAO.get(Instrument.class, id);
		return ao;
	}

	/*	*//**
			 * 保存
			 *//*
				 * @WriteOperLog
				 * 
				 * @WriteExOperLog
				 * 
				 * @Transactional(rollbackFor = Exception.class) public void save(Instrument i)
				 * throws Exception { commonDAO.saveOrUpdate(i); }
				 */

	/**
	 * 
	 * @Title: findInstrumentTastList @Description: TODO(加载实验仪器记录) @param @param
	 * startNum @param @param limitNum @param @param dir @param @param
	 * sort @param @param contextPath @param @param
	 * instrumentFaultId @param @return    设定文件 @return Map<String,Object>   
	 * 返回类型 @author zhiqiang.yang@biolims.cn @date 2017-11-13 上午11:07:44 @throws
	 */
	public Map<String, Object> findInstrumentTastList(int startNum, int limitNum, String dir, String sort,
			String instrumentId) {
		return instrumentDAO.findInstrumentTastList(startNum, limitNum, dir, sort, instrumentId);
	}

	public Map<String, Object> selectCosJson(String typeId, Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return instrumentDAO.selectCosJson(typeId, start, length, query, col, sort);
	}

	public Map<String, Object> findInstrumentTable(Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return instrumentDao.findInstrumentTable(start, length, query, col, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(Instrument i) throws Exception {

		instrumentDao.saveOrUpdate(i);

	}

	public Instrument get(String id) {
		Instrument instrument = commonDAO.get(Instrument.class, id);
		return instrument;
	}

	public Map<String, Object> findInstrumentStateTable(Integer start, Integer length, String query, String col,
			String sort, String id) throws Exception {
		Map<String, Object> result = instrumentDao.findInstrumentStateTable(start, length, query, col, sort, id);
		List<InstrumentState> list = (List<InstrumentState>) result.get("list");
		return result;
	}
	
	public Map<String, Object> findIncubatorSampleInfoTable(Integer start, Integer length, String query, String col,
			String sort, String id) throws Exception {
		Map<String, Object> result = instrumentDao.findIncubatorSampleInfoTable(start, length, query, col, sort, id);
		List<IncubatorSampleInfo> list = (List<IncubatorSampleInfo>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveInstrumentState(Instrument sc, String itemDataJson, String logInfo, String changeLogItem)
			throws Exception {
		List<InstrumentState> saveItems = new ArrayList<InstrumentState>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			InstrumentState scp = new InstrumentState();
			// 将map信息读入实体类
			scp = (InstrumentState) instrumentDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setInstrument(sc);

			saveItems.add(scp);
		}
		instrumentDao.saveOrUpdateAll(saveItems);
		if (changeLogItem != null && !"".equals(changeLogItem)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("Instrument");
			li.setModifyContent(changeLogItem);
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delInstrumentState(String[] ids) throws Exception {
		for (String id : ids) {
			InstrumentState scp = instrumentDao.get(InstrumentState.class, id);
			instrumentDao.delete(scp);
			if (ids.length > 0) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(scp.getInstrument().getId());
				li.setClassName("Instrument");
				li.setModifyContent("InstrumentState删除信息" + scp.getInstrument().getId());
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}
		}

	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(Instrument sc, Map jsonMap, String logInfo, Map logMap, String changeLogItem) throws Exception {
		if (sc != null) {
			sc.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			sc.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			
			if(sc.getMainType()!=null
					&&"281616366a9b6ff9016a9bf060f70019".equals(sc.getMainType().getId())){
				if(sc.getStorageContainer()!=null){
					StorageContainer scc = commonDAO.get(StorageContainer.class, sc.getStorageContainer().getId());
					if(scc!=null){
						sc.setStorageContainer(scc);
						if(sc.getSurplusLocationsNumber()!=null){
							sc.setSurplusLocationsNumber(scc.getColNum()*scc.getRowNum()-(sc.getTotalLocationsNumber()-sc.getSurplusLocationsNumber()));
							sc.setTotalLocationsNumber(scc.getColNum()*scc.getRowNum());
						}else{
							sc.setTotalLocationsNumber(scc.getColNum()*scc.getRowNum());
							sc.setSurplusLocationsNumber(scc.getColNum()*scc.getRowNum());
						}
					}
				}
			}
			instrumentDao.saveOrUpdate(sc);

			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(sc.getId());
				li.setClassName("Instrument");
				li.setModifyContent(logInfo);
				li.setState("1");
				li.setStateName("数据新增");
				commonDAO.saveOrUpdate(li);
			}

			String jsonStr = "";
			String logStr = "";
			logStr = (String) logMap.get("instrumentState");
			jsonStr = (String) jsonMap.get("instrumentState");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveInstrumentState(sc, jsonStr, logStr, changeLogItem);
			}
		}
	}

	public Map<String, Object> showInstrumentDialogList(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return instrumentDao.showInstrumentDialogList(start, length, query, col, sort);
	}
}
