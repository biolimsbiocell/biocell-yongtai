/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：Action
 * 类描述：设备管理ACTION
 * 创建人：vera
 * 创建时间：2011-11
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.equipment.main.action;

import java.net.URLDecoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.equipment.main.service.InstrumentService;
import com.biolims.equipment.model.IncubatorSampleInfo;
import com.biolims.equipment.model.Instrument;
import com.biolims.equipment.model.InstrumentFaultDetail;
import com.biolims.equipment.model.InstrumentState;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.opensymphony.xwork2.ActionContext;

/**
 * 库存主数据
 */
@Controller
@Scope("prototype")
@ParentPackage("default")
@Namespace("/equipment/main")
@SuppressWarnings("unchecked")
public class InstrumentAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;

	private String rightsId = "4011";
	/*
	 * private Instrument instrument = new Instrument();// 设备数据
	 * 
	 * @Autowired private InstrumentService instrumentService;
	 */

	@Autowired
	private InstrumentService instrumentService;
	private Instrument instrument = new Instrument();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CommonService commonService;
	@Resource
	private FieldService fieldService;
	@Resource
	private CommonDAO commonDAO;

	/**
	 * 列表Dialog @Title: showInstrumentDialogList @Description:
	 * TODO @param @return @param @throws Exception @return String @author 孙灵达 @date
	 * 2018年8月3日 @throws
	 */
	@Action(value = "showInstrumentDialogList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showInstrumentDialogList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/equipment/main/showInstrumentDialog.jsp");

	}

	@Action(value = "showInstrumentDialogListJson")
	public void showInstrumentDialogListJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = instrumentService.showInstrumentDialogList(start, length, query, col, sort);
			List<Instrument> list = (List<Instrument>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("serialNumber", "");
			map.put("spec", "");
			map.put("supplier-id", "");
			map.put("supplier-name", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("Instrument");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: showInstrumentTest @Description:
	 *         TODO(实验设备记录页面加载) @param @return @param @throws Exception   
	 *         设定文件 @return String    返回类型 @author zhiqiang.yang@biolims.cn @date
	 *         2017-11-13 上午10:37:48 @throws
	 */
	@Action(value = "showInstrumentTest", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showInstrumentTest() throws Exception {
		String instrumentId = getParameterFromRequest("id");
		// putObjToContext("instrumentId", instrumentId);
		return dispatcher("/WEB-INF/page/equipment/main/instrumentTest.jsp");
	}

	/***
	 * 
	 * @Title: showInstrumentFaultListDetailJson @Description:
	 *         TODO(实验设备记录数据查询) @param @throws Exception    设定文件 @return void   
	 *         返回类型 @author zhiqiang.yang@biolims.cn @date 2017-11-13
	 *         上午10:51:29 @throws
	 */
	@Action(value = "showInstrumentTestListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showInstrumentTestListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String instrumentId = getParameterFromRequest("instrumentId");
		String sort = getParameterFromRequest("sort");
		List<InstrumentFaultDetail> list = null;
		long totalCount = 0;
		putObjToContext("instrumentId", instrumentId);

		if (instrumentId != null && !instrumentId.equals("")) {
			Map<String, Object> controlMap = instrumentService.findInstrumentTastList(startNum, limitNum, dir, sort,
					instrumentId);
			list = (List<InstrumentFaultDetail>) controlMap.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("instrumentCode", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("stageTime", "yyyy-MM-dd");
			map.put("startDate", "yyyy-MM-dd");
			map.put("endDate", "yyyy-MM-dd");
			map.put("tableTypeId", "");
			map.put("stageName", "");
			map.put("acceptUser", "");
			map.put("taskId", "");
			map.put("taskMethod", "");
			map.put("taskResult", "");
			map.put("techTaskId", "");
			map.put("userGroup", "");
			map.put("countData", "");
			map.put("mode", "");
			map.put("note", "");
			map.put("note2", "");
			map.put("note3", "");
			map.put("note4", "");
			map.put("note5", "");
			map.put("note6", "");
			map.put("note7", "");

			new SendData().sendDateJson(map, list, list.size(), ServletActionContext.getResponse());
		}
	}

	/*	*//**
			 * 库存主数据列表表头
			 * 
			 */
	/*
	 * @Action(value = "showInstrumentList") public String showInstrumentList()
	 * throws Exception { String p_type = getParameterFromRequest("p_type");
	 * putObjToContext("p_type", p_type); // 用于判断当前页面类型,LIST类型
	 * putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	 * toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	 * 
	 * return dispatcher("/WEB-INF/page/equipment/main/showInstrumentList.jsp"); }
	 */

	/**
	 * 库存主数据列表
	 * 
	 * @throws Exception
	 */
	@Action(value = "showInstrumentListJson")
	public void showInstrumentListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");

		String data = getParameterFromRequest("data");

		String p_type = getParameterFromRequest("p_type");
		String queryMethod = getParameterFromRequest("queryMethod");
		Map r = this.toMakeSessionQuery(data, "Instrument", dir, sort, queryMethod);
		dir = (String) r.get("dir");
		sort = (String) r.get("sort");
		data = (String) r.get("queryData");
		Map<String, Object> result = null;

		try {
			result = this.instrumentService.selectInstrumentList(startNum, limitNum, dir, sort, data, p_type);
		} catch (Exception e) {
			e.printStackTrace();
		}

		List<Instrument> list = (List<Instrument>) result.get("list");
		Long count = (Long) result.get("total");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("type-id", "");
		map.put("type-name", "");
		// map.put("position-id", "");
		map.put("position", "");
		map.put("dutyUser-name", "");
		map.put("state-name", "");
		map.put("spec", "");
		map.put("rank", "");
		map.put("dutyUserTwo-name", "");
		map.put("serialNumber", "");
		map.put("enquiryOddNumber", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("outCode", "");
		map.put("accuracy", "");
		map.put("purchaseDate", "yyyy-MM-dd");
		map.put("surveyScope", "");
		map.put("checkItem", "");
		map.put("producer-name", "");
		map.put("producer-linkMan", "");
		map.put("producer-linkTel", "");
		map.put("supplier-name", "");
		map.put("supplier-linkMan", "");
		map.put("supplier-linkTel", "");
		map.put("studyType-name", "");
		map.put("effectiveDate", "yyyy-MM-dd");
		map.put("expiryDate", "yyyy-MM-dd");
		map.put("nextDate", "yyyy-MM-dd");
		map.put("parent-id", "");
		map.put("parent-name", "");
		map.put("parent2-id", "");
		map.put("parent2-name", "");
		map.put("capitalCode", "");
		map.put("note", "");
		map.put("products-id", "");
		map.put("products-name", "");

		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	/**
	 * 添加和修改页面
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "toEditInstrument")
	public String toEditInstrument() throws Exception {
		String id = getParameterFromRequest("id");
		String p_type = getParameterFromRequest("p_type");
		String handlemethod = "";
		if (id != null && !id.equals("")) {
			instrument = instrumentService.getInstrument(id);
			handlemethod = SystemConstants.PAGE_HANDLE_METHOD_MODIFY;
			toToolBar(rightsId, "", "", handlemethod);
			putObjToContext("p_type", p_type);
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			instrument.setCreateUser(user);
			instrument.setCreateDate(new Date());
			putObjToContext("p_type", p_type);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		return dispatcher("/WEB-INF/page/equipment/main/editInstrument.jsp");
	}

	/**
	 * 复制页面
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "toCopyInstrument")
	public String toCopyInstrument() throws Exception {
		String id = getParameterFromRequest("id");
		instrument = instrumentService.getInstrument(id);
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		instrument.setId("");
		instrument.setCreateUser(user);
		instrument.setCreateDate(new Date());
		toSetStateCopy();
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		return dispatcher("/WEB-INF/page/equipment/main/editInstrument.jsp");
	}

	/**
	 * 访问查看页面
	 */
	/*
	 * 
	 * @Action(value = "toViewInstrument") public String toViewInstrument() throws
	 * Exception { String id = getParameterFromRequest("id"); String p_type =
	 * getParameterFromRequest("p_type"); putObjToContext("p_type", p_type); if (id
	 * != null && !id.equals("")) { instrument =
	 * instrumentService.getInstrument(id); toToolBar(rightsId, "", "",
	 * SystemConstants.PAGE_HANDLE_METHOD_VIEW); } return
	 * dispatcher("/WEB-INF/page/equipment/main/editInstrument.jsp"); }
	 */

	/*
	 * @Action(value = "instrumentSelect", interceptorRefs =
	 * 
	 * @InterceptorRef("biolimsDefaultStack")) public String
	 * showDialogInstrumentList() throws Exception { String p_type =
	 * getParameterFromRequest("p_type"); putObjToContext("handlemethod",
	 * SystemConstants.PAGE_HANDLE_METHOD_LIST); putObjToContext("p_type", p_type);
	 * toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST); return
	 * dispatcher("/WEB-INF/page/system/template/instrumentDialog.jsp"); }
	 */

	@Action(value = "showDialogInstrumentListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogInstrumentListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		String p_type = getParameterFromRequest("p_type");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0) {
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		}
		Map<String, Object> result = this.instrumentService.selectInstrumentList(startNum, limitNum, dir, sort, data,
				p_type);
		;
		Long count = (Long) result.get("total");
		List<Instrument> list = (List<Instrument>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		// map.put("position-id", "");
		map.put("position", "");
		map.put("dutyUser-name", "");
		map.put("rank", "");
		map.put("dutyUserTwo-name", "");
		map.put("serialNumber", "");
		map.put("state-name", "");
		map.put("spec", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("outCode", "");
		map.put("accuracy", "");
		map.put("purchaseDate", "yyyy-MM-dd");
		map.put("surveyScope", "");
		map.put("checkItem", "");
		map.put("producer-name", "");
		map.put("producer-linkMan", "");
		map.put("producer-linkTel", "");
		map.put("supplier-name", "");
		map.put("supplier-linkMan", "");
		map.put("supplier-linkTel", "");
		map.put("studyType-name", "");
		map.put("effectiveDate", "yyyy-MM-dd");
		map.put("expiryDate", "yyyy-MM-dd");
		map.put("nextDate", "yyyy-MM-dd");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	/*	*//**
			 * 保存
			 */
	/*
	 * @Action(value = "save") public String save() throws Exception { String p_type
	 * = getParameterFromRequest("p_type"); this.instrument.setP_type(p_type);
	 * instrumentService.save(this.instrument); return
	 * redirect("/equipment/main/toEditInstrument.action?id=" +
	 * this.instrument.getId() + "&p_type=" + p_type); }
	 */

	/**
	 * 
	 * @Title: getCosById @Description: 扫码获取设备 @author : shengwei.wang @date
	 *         2018年1月26日上午10:27:21 @throws Exception void @throws
	 */
	@Action(value = "getCosById")
	public void getCosById() throws Exception {
		String id = getParameterFromRequest("id");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			Instrument ins = instrumentService.getInstrument(id);
			map.put("ins", ins);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: selectCos @Description: 选择设备 @author : shengwei.wang @date
	 *         2018年1月26日上午11:56:18 @return @throws Exception String @throws
	 */
	@Action(value = "selectCos")
	public String selectCos() throws Exception {
		String typeId = getParameterFromRequest("typeId");
		String roomName = URLDecoder.decode(getParameterFromRequest("roomName"), "UTF-8");
		putObjToContext("typeId", typeId);
		putObjToContext("roomName", roomName);
		return dispatcher("/WEB-INF/page/system/template/cosTemplateDialog.jsp");

	}

	@Action(value = "selectCosJson")
	public void selectCosJson() throws Exception {
		String typeId = getParameterFromRequest("id");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = instrumentService.selectCosJson(typeId, start, length, query, col, sort);
		List<Instrument> list = (List<Instrument>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("mainType-id", "");
		map.put("mainType-name", "");
		map.put("state-name", "");
		map.put("isFull", "");
		map.put("roomManagement-roomName", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));

	}

	/*
	 * public String getRightsId() { return rightsId; }
	 * 
	 * public void setRightsId(String rightsId) { this.rightsId = rightsId; }
	 * 
	 * public Instrument getInstrument() { return instrument; }
	 * 
	 * public void setInstrument(Instrument instrument) { this.instrument =
	 * instrument; }
	 */

	@Action(value = "showInstrumentList")
	public String showInstrumentList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/equipment/main/instrument.jsp");
	}

	@Action(value = "showInstrumentEditList")
	public String showInstrumentEditList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/equipment/main/instrumentEditList.jsp");
	}

	@Action(value = "showInstrumentTableJson")
	public void showInstrumentTableJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = instrumentService.findInstrumentTable(start, length, query, col, sort);
			Long count = (Long) result.get("total");
			List<Instrument> list = (List<Instrument>) result.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("rank", "");
			map.put("serialNumber", "");
			map.put("mainType-id", "");
			map.put("mainType-name", "");
			map.put("spec", "");
			map.put("state-id", "");
			map.put("state-name", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("department-id", "");
			map.put("department-name", "");
			map.put("price", "");
			map.put("currencyType-id", "");
			map.put("currencyType-name", "");
			map.put("note", "");
			map.put("capitalCode", "");
			map.put("effectiveDate", "yyyy-MM-dd");
			map.put("supplier-id", "");
			map.put("supplier-name", "");
			map.put("isFull", "");
			map.put("totalLocationsNumber", "");
			map.put("surplusLocationsNumber", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("Instrument");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "instrumentSelectTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogInstrumentList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/equipment/main/instrumentSelectTable.jsp");
	}

	@Action(value = "editInstrument")
	public String editInstrument() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			instrument = instrumentService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "instrument");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			instrument.setCreateUser(user);
			instrument.setCreateDate(new Date());
			instrument.setState1(SystemConstants.DIC_STATE_NEW);
			instrument.setStateName(SystemConstants.DIC_STATE_NEW_NAME);
			instrument.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			instrument.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));

			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/equipment/main/instrumentEdit.jsp");
	}

	@Action(value = "copyInstrument")
	public String copyInstrument() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		instrument = instrumentService.get(id);
		instrument.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/equipment/main/instrumentEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {

		String msg = "";
		String zId = "";
		boolean bool = true;

		try {
			String dataValue = getParameterFromRequest("dataValue");
			String str = "[" + dataValue + "]";
			String changeLog = getParameterFromRequest("changeLog");
//			changeLog = new String(changeLog.getBytes("ISO-8859-1"), "utf-8");
			String changeLogItem = getParameterFromRequest("changeLogItem");
			changeLogItem = new String(changeLogItem.getBytes("ISO-8859-1"), "utf-8");

			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str, List.class);

			for (Map<String, Object> map : list) {
				instrument = (Instrument) commonDAO.Map2Bean(map, instrument);
			}
			String id = instrument.getId();
			if (id != null && id.equals("")) {
				instrument.setId(null);
			}
			Map aMap = new HashMap();
			Map lMap = new HashMap();
			// aMap.put("instrumentState",getParameterFromRequest("instrumentStateJson"));

			instrumentService.save(instrument, aMap, changeLog, lMap, changeLogItem);

			zId = instrument.getId();
		} catch (Exception e) {
			e.printStackTrace();
			msg = "保存失败！";
			bool = false;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("msg", msg);
		map.put("success", bool);
		map.put("id", zId);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		if (bpmTaskId != null && !bpmTaskId.equals("") && !bpmTaskId.equals("null")) {
			map.put("bpmTaskId", bpmTaskId);

		}
		// HttpUtils.write(JsonUtils.toJsonString(map));
		return redirect("/equipment/main/editInstrument.action?id=" + zId);
	}

	@Action(value = "viewInstrument")
	public String toViewInstrument() throws Exception {
		String id = getParameterFromRequest("id");
		instrument = instrumentService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/equipment/main/instrumentEdit.jsp");
	}

	@Action(value = "showInstrumentStateTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showInstrumentStateTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = instrumentService.findInstrumentStateTable(start, length, query, col, sort, id);
		List<InstrumentState> list = (List<InstrumentState>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("instrumentCode", "");
		map.put("stageName", "");
		map.put("acceptUser", "");
		map.put("taskId", "");
		map.put("taskMethod", "");
		map.put("taskResult", "");
		map.put("techTaskId", "");
		map.put("mode", "");
		map.put("userGroup", "");
		map.put("countData", "");
		map.put("note", "");
		map.put("instrumentName", "");
		map.put("instrument-name", "");
		map.put("instrument-id", "");
		map.put("instrumentState", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("stageTime", "");
		map.put("startDate", "");
		map.put("endDate", "");
		map.put("tableTypeId", "");
		map.put("batch","");
		map.put("name","");

		map.put("note2", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}
	
	@Action(value = "showIncubatorSampleInfoTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showIncubatorSampleInfoTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = instrumentService.findIncubatorSampleInfoTable(start, length, query, col, sort, id);
		List<IncubatorSampleInfo> list = (List<IncubatorSampleInfo>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		/** 库存编号  */
		map.put("id", "");
		/** 样本编号/批次号  */
		map.put("batch", "");
		/** 储位 */
		map.put("location", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delInstrumentState")
	public void delInstrumentState() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			instrumentService.delInstrumentState(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "saveInstrumentStateTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveInstrumentStateTable() throws Exception {
		Map aMap = new HashMap();
		Map lMap = new HashMap();

		String id = getParameterFromRequest("id");
		instrument = commonService.get(Instrument.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		aMap.put("instrumentState", getParameterFromRequest("dataJson"));
		lMap.put("instrumentState", getParameterFromRequest("changeLog"));
		try {
			instrumentService.save(instrument, aMap, "", lMap, "");
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "saveInstrumentTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveInstrumentTable() throws Exception {
		String dataValue = getParameterFromRequest("dataJson");
		String changeLog = getParameterFromRequest("changeLog");

		String str = "[" + dataValue + "]";

		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str, List.class);

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			for (Map<String, Object> map1 : list) {

				Instrument a = new Instrument();

				Map aMap = new HashMap();
				Map lMap = new HashMap();
				a = (Instrument) commonDAO.Map2Bean(map1, a);
				a.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				a.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));

				instrumentService.save(a, aMap, changeLog, lMap, changeLog);
				map.put("id", a.getId());
			}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public InstrumentService getInstrumentService() {
		return instrumentService;
	}

	public void setInstrumentService(InstrumentService instrumentService) {
		this.instrumentService = instrumentService;
	}

	public Instrument getInstrument() {
		return instrument;
	}

	public void setInstrument(Instrument instrument) {
		this.instrument = instrument;
	}

}
