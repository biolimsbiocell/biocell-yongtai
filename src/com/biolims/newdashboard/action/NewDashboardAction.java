package com.biolims.newdashboard.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.core.model.user.UserRole;
import com.biolims.dashboard.model.RoleDashboardModel;
import com.biolims.newdashboard.service.NewDashboardService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.web.action.BaseActionSupport;

/**
 * 工作台组件查询
 * 
 */

@Controller
@Scope("prototype")
@ParentPackage("default")
@Namespace("/newDashboard")
public class NewDashboardAction extends BaseActionSupport {

	/**
	 * 
	 */
	@Resource
	private NewDashboardService newDashboardService;

	/**
	 * 查询指定用户工作台
	 * 
	 * @throws Exception
	 */
	@Action(value = "toNewDashboard", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void toNewDashboard() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List<String> list = new ArrayList<String>();
			String userId = getRequest().getParameter("userId");
			// 根据userId查询用户对应角色ID
			List<UserRole> roleId = newDashboardService.selRoleId(userId);
			// 根据角色ID查询工作台组件selDashboardId
			for (int i = 0; i < roleId.size(); i++) {
				List<RoleDashboardModel> dashboardId = newDashboardService
						.selDashboardId(roleId.get(i).getRole().getId());
				System.out.println("角色ID============"+roleId.get(i).getRole().getId());
				for (int j = 0; j < dashboardId.size(); j++) {
					list.add(dashboardId.get(j).getDbm().getId());
				}
			}
			Set<String> set = new HashSet<String>(list);
			list = new ArrayList<String>(set);
			
			System.out.println("去重后+++============="+list);
			List<Map<String, Object>> dashboardList = newDashboardService
					.findDashboard(userId);

			map.put("data", list);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

}
