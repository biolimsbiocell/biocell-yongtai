package com.biolims.newdashboard.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.biolims.core.model.user.UserRole;
import com.biolims.dashboard.model.RoleDashboardModel;
import com.biolims.newdashboard.dao.NewDashboardDao;

@Service
public class NewDashboardService {

	@Resource
	private NewDashboardDao newDashboardDao;

	
	
	
	
	
	//根据用户ID查询用户所在角色ID
	public List<UserRole> selRoleId (String userId)throws	Exception{
		return newDashboardDao.findRoleId(userId);
	}

	
	//根据角色ID查询工作台组件findDashboardId
	public List<RoleDashboardModel> selDashboardId(String roleId)throws Exception{
		return newDashboardDao.findDashboardId(roleId);
	}
	/**
	 * 查询用户工作台
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, Object>> findDashboard(String userId) throws Exception {
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		List<Object> dbList = newDashboardDao.selectDashboard(userId);
		if (dbList != null && dbList.size() > 0) {
			for (Object obj : dbList) {
				Map<String, Object> map = new HashMap<String, Object>();
				Object[] objs = (Object[]) obj;
				map.put("layoutColumn", objs[0]);
				map.put("fkId", objs[1]);
				List<Object> childrenList = newDashboardDao.selectDashboardByLayoutColumn(objs[1].toString(), Integer
						.valueOf(objs[0].toString()));
				map.put("children", listObjectToMap(childrenList));
				result.add(map);
			}
		}
		return result;
	}
	private List<Map<String, Object>> listObjectToMap(List<Object> objList) throws Exception {
		List<Map<String, Object>> listMap = new ArrayList<Map<String, Object>>();
		if (objList != null && objList.size() > 0) {
			for (Object obj : objList) {
				Object[] objs = (Object[]) obj;
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("id", objs[0]);
				map.put("modileId", objs[1]);
				map.put("sequenceNumber", objs[2]);
				map.put("config", objs[3]);
				map.put("moduleName", objs[4]);
				map.put("moduleConfUrl", objs[5]);
				map.put("moduleUrl", objs[6]);
				map.put("isFrame", objs[7]);
				listMap.add(map);
			}
		}
		return listMap;
	}

}
