package com.biolims.newdashboard.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.biolims.core.model.user.UserGroupUser;
import com.biolims.core.model.user.UserRole;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.dashboard.model.RoleDashboardModel;

@Repository
@SuppressWarnings("unchecked")
public class NewDashboardDao extends BaseHibernateDao {
	
	
	
	
	//根据用户ID查询角色ID
	public List<UserRole> findRoleId(String userId)throws Exception{
		String hql = "from UserRole where user='"+ userId  +"'" ;
		List<UserRole> list =  this.getSession().createQuery(hql).list();
		return list;
	}
	
	
	//根据角色ID查询工作台组件findDashboardId
	public List<RoleDashboardModel> findDashboardId(String roleId)throws Exception{
		String hql = "from RoleDashboardModel where role='"+ roleId  +"'" ;
		List<RoleDashboardModel> list =  this.getSession().createQuery(hql).list();
		return list;
	}
	
	
	
	public List<Object> selectDashboard(String userId) throws Exception {
		String hql = "select distinct layoutColumn,userId from Dashboard where userId='" + userId
				+ "' ORDER BY layoutColumn";
		return find(hql);
	}

	public List<Object> selectDashboardByLayoutColumn(String userId, int layoutColumn) throws Exception {

		String hql = "select d.id,d.model.id,d.squenceNum,d.config ,dm.moduleName,dm.moduleConfUrl,dm.moduleUrl,dm.isFrame from Dashboard d,DashboardModel dm where d.model.id=dm.id and  d.userId='"
				+ userId + "' and d.layoutColumn=" + layoutColumn + " order by d.squenceNum asc";
		return find(hql);
	}
}
