﻿package com.biolims.report.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.analysis.data.model.DataTask;
import com.biolims.analysis.data.model.DataTaskItem;
import com.biolims.analysis.data.model.DataTaskSvItem;
import com.biolims.analysis.data.model.DataTaskSvnItem;
import com.biolims.analysis.data.model.DateTaskCnvItem;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.kb.model.KnowledgeBase;
import com.biolims.report.model.SampleReport;
import com.biolims.report.model.SampleReportItem;
import com.biolims.report.model.SampleReportTemp;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class ReportDao extends BaseHibernateDao {
	public List<SampleReportItem> selectReportItemListAll(String id)
			throws Exception {
		String key = "";
		String hql = "from SampleReportItem where report.id = '" + id + "'";

		List<SampleReportItem> list = this.getSession().createQuery(hql + key)
				.list();

		return list;
	}


	/**
	 * 获取电子病历的数据
	 * 
	 * @param bloodCode
	 * @param patientId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> getPatientInfo(String patientId)
			throws Exception {
		String hql = "from CrmPatient where 1=1 ";
		String key = "";
		if (patientId != null) {
			key = key + "and id = '" + patientId + "'";
		}
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<CrmPatient> list = new ArrayList<CrmPatient>();
		list = this.getSession().createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	/**
	 * 获取订单数据
	 * 
	 * @param bloodCode
	 * @param patientId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> getSampleOrderInfo(String sampleCode)
			throws Exception {
		String hql1 = "from SampleInfo where 1=1 and code = '" + sampleCode
				+ "'";
		List<SampleInfo> listsampleInfo = this.getSession().createQuery(hql1)
				.list();
		if (listsampleInfo.size() > 0) {
			SampleInfo sampleInfo = listsampleInfo.get(0);
			String hql = "from SampleOrder where 1=1 ";
			String key = "";
			if (sampleInfo.getOrderNum() != null) {
				key = key + "and id= '" + sampleInfo.getOrderNum() + "'";
			}

			Long total = (Long) this.getSession()
					.createQuery("select count(*) " + hql + key).uniqueResult();
			List<SampleOrder> list = new ArrayList<SampleOrder>();
			list = this.getSession().createQuery(hql + key).list();
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("total", total);
			result.put("list", list);
			return result;
		} else {
			Map<String, Object> result1 = new HashMap<String, Object>();
			return result1;
		}

	}

	/**
	 * 获取基因突变的总数量
	 * 
	 * @param bloodCode
	 * @param patientId
	 * @return
	 * @throws Exception
	 */
	public int getmutantGenesCount(String patientId) throws Exception {
		String hql = "from DataTaskItem where 1=1 and crmPatientId='"
				+ patientId + "'";
		List<DataTaskItem> listDataTaskItem = this.getSession()
				.createQuery(hql).list();
		int total = 0;
		if (listDataTaskItem.size() > 0) {
			DataTaskItem dataTaskItem = listDataTaskItem.get(0);
			String hql1 = "from DataTask where 1=1 and id='"
					+ dataTaskItem.getDataTask().getId() + "'";
			List<DataTask> listDataTask = this.getSession().createQuery(hql1)
					.list();
			if (listDataTask.size() > 0) {
				DataTask dataTask = listDataTask.get(0);

				/** 基因融合突变表 */
				String hql2 = "select count(*) from DataTaskSvItem where 1=1 and dataTask.id='"
						+ dataTask.getId() + "'";
				int svItem = Integer.parseInt(this.getSession()
						.createQuery(hql2).uniqueResult().toString());
				/** 碱基突变表 */
				String hql3 = "select count(*) from DataTaskSvnItem where 1=1 and dataTask.id='"
						+ dataTask.getId() + "'";
				int svnItem = Integer.parseInt(this.getSession()
						.createQuery(hql3).uniqueResult().toString());
				/** 肿瘤突变表 */
				String hql4 = "select count(*) from DateTaskCnvItem where 1=1 and dataTask.id='"
						+ dataTask.getId() + "'";
				int cnvItem = Integer.parseInt(this.getSession()
						.createQuery(hql4).uniqueResult().toString());
				total = svItem + svnItem + cnvItem;
			}
		}
		return total;
	}

	/**
	 * 获取基因突变的列表
	 * 
	 * @param bloodCode
	 * @param patientId
	 * @return
	 * @throws Exception
	 */
	public String getmutantGenesList(String patientId) throws Exception {
		String hql = "from DataTaskItem where 1=1 and crmPatientId='"
				+ patientId + "'";
		List<DataTaskItem> listDataTaskItem = this.getSession()
				.createQuery(hql).list();
		StringBuffer sb = new StringBuffer();
		// sb = sb.append("{");
		if (listDataTaskItem.size() > 0) {
			DataTaskItem dataTaskItem = listDataTaskItem.get(0);
			String hql1 = "from DataTask where 1=1 and id='"
					+ dataTaskItem.getDataTask().getId() + "'";
			List<DataTask> listDataTask = this.getSession().createQuery(hql1)
					.list();
			if (listDataTask.size() > 0) {
				DataTask dataTask = listDataTask.get(0);

				/** 基因融合突变表 */
				String hql2 = "from DataTaskSvItem where 1=1 and dataTask.id='"
						+ dataTask.getId() + "'";
				List<DataTaskSvItem> SvItemlist = new ArrayList<DataTaskSvItem>();
				SvItemlist = this.getSession().createQuery(hql2).list();
				// SvItemlist = selectDataTaskSvItemList(dataTask.getId(), null,
				// null, null, null);
				for (int i = 0; i < SvItemlist.size(); i++) {
					DataTaskSvItem dts = new DataTaskSvItem();
					dts = SvItemlist.get(i);
					sb.append(dts.getMutantGenes() + ",");
				}
				/** 碱基突变表 */
				String hql3 = "from DataTaskSvnItem where 1=1 and dataTask.id='"
						+ dataTask.getId() + "'";
				List<DataTaskSvnItem> dataTaskSvnItem = new ArrayList<DataTaskSvnItem>();
				dataTaskSvnItem = this.getSession().createQuery(hql3).list();
				for (int i = 0; i < dataTaskSvnItem.size(); i++) {
					DataTaskSvnItem dts = new DataTaskSvnItem();
					dts = dataTaskSvnItem.get(i);
					sb.append(dts.getMutantGenes() + ",");
				}
				/** 肿瘤突变表 */
				String hql4 = "from DateTaskCnvItem where 1=1 and dataTask.id='"
						+ dataTask.getId() + "'";
				List<DateTaskCnvItem> dateTaskCnvItem = new ArrayList<DateTaskCnvItem>();
				dateTaskCnvItem = this.getSession().createQuery(hql4).list();
				for (int i = 0; i < dateTaskCnvItem.size(); i++) {
					DateTaskCnvItem dts = new DateTaskCnvItem();
					dts = dateTaskCnvItem.get(i);
					if (i == dateTaskCnvItem.size() - 1) {
						sb.append(dts.getMutantGenes());
					} else {
						sb.append(dts.getMutantGenes() + ",");
					}
				}
			}
			// sb.append("}");
		}
		String[] strs = sb.toString().split(",");
		Arrays.sort(strs);
		StringBuffer mutantGenes = new StringBuffer();
		for (String str : strs) {
			mutantGenes.append(str + "  ");
		}
		return mutantGenes.toString();
	}

	/**
	 * 获取基因突变的知识库信息
	 * 
	 * @param bloodCode
	 * @param patientId
	 * @return
	 * @throws Exception
	 */
	public List<KnowledgeBase> getMutantGenesKnowledgeList(String patientId)
			throws Exception {
		List<KnowledgeBase> knowledges = new ArrayList<KnowledgeBase>();
		String hql = "from DataTaskItem where 1=1 and crmPatientId='"
				+ patientId + "'";
		List<DataTaskItem> listDataTaskItem = this.getSession()
				.createQuery(hql).list();
		if (listDataTaskItem.size() > 0) {
			DataTaskItem dataTaskItem = listDataTaskItem.get(0);
			String hql1 = "from DataTask where 1=1 and id='"
					+ dataTaskItem.getDataTask().getId() + "'";
			List<DataTask> listDataTask = this.getSession().createQuery(hql1)
					.list();
			if (listDataTask.size() > 0) {
				DataTask dataTask = listDataTask.get(0);
				/** 基因融合突变表 */
				String hql2 = "from DataTaskSvItem where 1=1 and dataTask.id='"
						+ dataTask.getId() + "'";
				List<DataTaskSvItem> dataTaskSvItem = this.getSession()
						.createQuery(hql2).list();
				for (int i = 0; i < dataTaskSvItem.size(); i++) {
					KnowledgeBase kb = new KnowledgeBase();
					String hql5 = "from KnowledgeBase where 1=1 and mutationGenes='"
							+ dataTaskSvItem.get(i).getMutantGenes() + "'";
					List<KnowledgeBase> kbs = this.getSession()
							.createQuery(hql5).list();
					if (kbs.size() > 0) {
						kb = kbs.get(0);
						knowledges.add(kb);
					}
				}
				/** 碱基突变表 */
				String hql3 = "from DataTaskSvnItem where 1=1 and dataTask.id='"
						+ dataTask.getId() + "'";
				List<DataTaskSvnItem> dataTaskSvnItem = this.getSession()
						.createQuery(hql3).list();
				for (int i = 0; i < dataTaskSvnItem.size(); i++) {
					KnowledgeBase kb = new KnowledgeBase();
					String hql6 = "from KnowledgeBase where 1=1 and mutationGenes='"
							+ dataTaskSvnItem.get(i).getMutantGenes() + "'";
					List<KnowledgeBase> kbs = this.getSession()
							.createQuery(hql6).list();
					if (kbs.size() > 0) {
						kb = kbs.get(0);
						knowledges.add(kb);
					}
				}
				/** 肿瘤突变表 */
				String hql4 = "from DateTaskCnvItem where 1=1 and dataTask.id='"
						+ dataTask.getId() + "'";
				List<DateTaskCnvItem> dateTaskCnvItem = this.getSession()
						.createQuery(hql4).list();
				for (int i = 0; i < dateTaskCnvItem.size(); i++) {
					KnowledgeBase kb = new KnowledgeBase();
					String hql7 = "from KnowledgeBase where 1=1 and mutationGenes='"
							+ dateTaskCnvItem.get(i).getMutantGenes() + "'";
					List<KnowledgeBase> kbs = this.getSession()
							.createQuery(hql7).list();
					if (kbs.size() > 0) {
						kb = kbs.get(0);
						knowledges.add(kb);
					}
				}
			}
		}
		return knowledges;
	}

	public List<DataTaskSvItem> selectDataTaskSvItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from DataTaskSvItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and dataTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<DataTaskSvItem> list = new ArrayList<DataTaskSvItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return list;
	}

	/**
	 * 
	 * @param 报告生成完成
	 * @throws Exception
	 */
	// public void reportCompleted(String reportId) throws Exception {
	// String hql = "from SampleReportItem where 1=1 and id= '"+reportId+"'";
	// List<SampleReportItem> list = new ArrayList<SampleReportItem>();
	// list = this.getSession().createQuery(hql).list();
	// if(list.size()>0){
	// SampleOrder sampleOrder = new SampleOrder();
	// if(list.get(0).getOrderNum()!=null &&
	// !"".equals(list.get(0).getOrderNum())){
	// sampleOrder = this.get(SampleOrder.class, list.get(0).getOrderNum());
	// sampleOrder.setBgState("2");
	// this.saveOrUpdate(sampleOrder);
	// }
	// }
	// }

	public SampleReportTemp updateByOrderNum(String orderNum, String productId)
			throws Exception {
		if (orderNum == null || ("").equals(orderNum) || productId == null
				|| ("").equals(productId)) {
			return null;
		}
		String hql = "from SampleReportTemp where 1=1 and state='1' and orderNum= '"
				+ orderNum + "' and productId= '" + productId + "'";
		List<SampleReportTemp> list = new ArrayList<SampleReportTemp>();
		list = this.getSession().createQuery(hql).list();
		if (list.size() > 0) {
			SampleReportTemp sampleReportTemp = new SampleReportTemp();
			sampleReportTemp = list.get(0);
			sampleReportTemp.setState("0");
			/*
			 * sampleReportTemp.setState("0"); Transaction
			 * trans=getSession().beginTransaction(); String
			 * hql2="update SampleReportTemp set state='0' where id= '"
			 * +sampleReportTemp.getId()+"'"; Query
			 * queryupdate=getSession().createQuery(hql2);
			 * queryupdate.executeUpdate(); trans.commit();
			 */
			return sampleReportTemp;
		} else {
			return null;
		}
	}

	public SampleReportItem getSampleReportItem(String orderNum) {
		String hql = "from SampleReportItem where 1=1 and orderNum='"
				+ orderNum + "'";
		SampleReportItem s = (SampleReportItem) this.getSession()
				.createQuery(hql).uniqueResult();
		return s;
	}

	

	public Map<String, Object> findReportList(Integer start, Integer length,
			String query, String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleReport where 1=1 ";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession()
				.get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from SampleReport where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SampleReport> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
	public Map<String, Object> findSampleReportTempList(Integer start,
			Integer length, String query, String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleReportTemp where 1=1 and state='1'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession()
				.get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from SampleReportTemp where 1=1 and state='1'";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SampleReportTemp> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> findReportItemList(String id, Integer start,
			Integer length, String query, String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleReportItem where 1=1 and report.id='"+id+"'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession()
				.get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from SampleReportItem where 1=1 and report.id='"+id+"'";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SampleReportItem> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> findReportSendList(Integer start,
			Integer length, String query, String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleReportItem where 1=1 and state='2'";
		String key = "";
		if (query != null&&!"".equals(query)) {
			key = map2Where(query);
		}else{
			key+="  and (arc is null or arc !='1')";
		}
		String scopeId = (String) ActionContext.getContext().getSession()
				.get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		Long sumCount = (Long) getSession().createQuery(countHql+key)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from SampleReportItem where 1=1 and state='2'";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SampleReportItem> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
}
