package com.biolims.report.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.common.service.SystemCodeService;
import com.biolims.file.model.FileInfo;
import com.biolims.file.service.FileInfoService;
import com.biolims.file.service.OperFileService;
import com.biolims.kb.model.KnowledgeBase;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.report.dao.ReportDao;
import com.biolims.report.model.SampleReport;
import com.biolims.report.model.SampleReportItem;
import com.biolims.report.model.SampleReportTemp;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.service.SampleStateService;
import com.biolims.system.sysreport.model.ReportTemplateInfo;
import com.biolims.util.ConfigFileUtil;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.opensymphony.xwork2.ActionContext;

@Service
@SuppressWarnings({ "rawtypes", "unchecked" })
public class ReportService {

	@Resource
	private ReportDao reportDao;
	@Resource
	private CommonService commonService;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private SystemCodeService systemCodeService;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private SampleStateService sampleStateService;

	
	@Resource
	private OperFileService operFileService; 

	/**
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public SampleReport getSampleReportById(String id) throws Exception {
		return reportDao.get(SampleReport.class, id);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleReportItemById(String id, String fileRetId,
			String type) throws Exception {
		SampleReportItem sri = commonService.get(SampleReportItem.class, id);
		FileInfo rfi = commonService.get(FileInfo.class, fileRetId);
		rfi.setFileName(sri.getSampleCode() + ".doc");
		commonService.saveOrUpdate(rfi);
		sri.setReportFile(rfi);

		saveReportItem(sri);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveReportItem(SampleReportItem sc) throws Exception {
		if (sc != null) {

			this.reportDao.saveOrUpdate(sc);

		}
	}

	/**
	 * 报表明细信息检索
	 * 
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */

	// public Map<String, Object> findSampleReportItemList(String taskId,
	// Integer startNum, Integer limitNum, String dir,
	// String sort) throws Exception {
	// return reportDao.selectSampleReportItemList(taskId, startNum, limitNum,
	// dir, sort);
	// }

	/**
	 * 保存报表发送明细
	 * 
	 * @param taskId
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveReportItem(SampleReport sr, String itemDataJson)
			throws Exception {
		List<SampleReportItem> saveItems = new ArrayList<SampleReportItem>();
		List<Map> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map map : list) {
			SampleReportItem sri = new SampleReportItem();
			// 将map信息读入实体类
			sri = (SampleReportItem) reportDao.Map2Bean(map, sri);
			if (sri.getId() == null) {
				// if (sri.getId().equals("NEW")) {
				// CrmProduct a = commonService.get(CrmProduct.class,
				// sri.getProduct().getId());
				String code = systemCodeService.getCodeByPrefix(
						"SampleReportItem",
						"JT"
								+ DateUtil.dateFormatterByPattern(new Date(),
										"yyMMdd"), 00000, 5, null);
				sri.setId(code);
			}
			sri.setReport(sr);
			// SampleReportTemp sampleReportTemp = new SampleReportTemp();
			// sampleReportTemp = reportDao.updateByOrderNum(sri.getOrderNum(),
			// sri.getProductId());
			// if (sampleReportTemp != null) {
			// reportDao.saveOrUpdate(sampleReportTemp);
			// }

			reportDao.saveOrUpdate(sri);
			
			SampleReportTemp srt = commonDAO.get(SampleReportTemp.class, sri.getReportResultId());
			srt.setState("2");
			reportDao.saveOrUpdate(srt);
		}

	}

	/**
	 * 删除报表列表,由ext ajax调用
	 * @param user 
	 * @param delStr 
	 * @param id 
	 * 
	 * @param id
	 * @return void
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSampleReportItemList(String[] ids, String delStr, User user, String report_id) {
		String delId="";
		for (String id : ids) {
			SampleReportItem scp = reportDao.get(SampleReportItem.class, id);
			if (scp.getId() != null) {
				// 改变左侧样本状态
				SampleReportTemp srt= this.commonDAO.get(
						SampleReportTemp.class, scp.getTempId());
				if (srt != null) {
					srt.setState("1");
					reportDao.update(srt);
				}
				reportDao.delete(scp);
			}
		delId+=scp.getSampleCode()+",";
		}
		if (ids.length>0) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setUserId(user.getId());
			li.setFileId(report_id);
			li.setModifyContent(delStr+delId);
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 生成常染色体报告文件
	 * 
	 * @param ids
	 * @throws Exception
	 */

	private void scanDeskImgDir(String path, Map<String, File> map)
			throws Exception {
		File[] files = new File(path).listFiles();
		for (File file : files) {
			if (file.isFile()) {
				String fileName = file.getName().substring(0,
						file.getName().indexOf("."));
				map.put(fileName, file);
			} else {
				scanDeskImgDir(file.getAbsolutePath(), map);
			}
		}
	}

	public ReportTemplateInfo getTemplateById(String id) {
		ReportTemplateInfo reportTemplateInfo = this.reportDao.get(
				ReportTemplateInfo.class, id);
		return reportTemplateInfo;
	}

	public Map<String, Object> findReportSendList(
			Integer start, Integer length,
			String query, String col, String sort) throws Exception {
		return reportDao.findReportSendList(start, length, query, col, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleSendItem(SampleReportItem sr, String itemDataJson)
			throws Exception {
		List<SampleReportItem> saveItems = new ArrayList<SampleReportItem>();
		List<Map> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map map : list) {
			SampleReportItem sri = new SampleReportItem();
			// 将map信息读入实体类
			sri = (SampleReportItem) reportDao.Map2Bean(map, sri);
			if(sri.getArc().equals("1")){
				sri.setState("1");
				sri.setSendDate(new Date());
			}else{
				sri.setState("2");
			}
			reportDao.saveOrUpdate(sri);
		}

	}

	/**
	 * 生成PDF报告文件
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void createReportFilePDF(String[] ids) throws Exception {
		for (String id : ids) {
			SampleReportItem sri = commonDAO.get(SampleReportItem.class, id);
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if (sri.getReti() != null) {
				FileInfo fileInfo = sri.getReti().getAttach();
				if (fileInfo != null) {
					Map<String, File> imgMap = new HashMap<String, File>();
					String tempFile = ConfigFileUtil
							.getValueByKey("file.report.temp.path");
					String formFile = ConfigFileUtil
							.getValueByKey("file.report.form.path");
					FileInputStream in = new FileInputStream(
							fileInfo.getFilePath());
					PdfReader reader = new PdfReader(in);
					String root = ConfigFileUtil.getRootPath() + File.separator
							+ DateUtil.format(new Date(), "yyyyMMdd");
					if (!new File(root).exists()) {
						new File(root).mkdirs();
					}
					// 自动生成的PDF文件名
					// String deskFileName = UUID.randomUUID().toString();
					// 设置文件路径
					// File deskFile = new File("D://getPDF", "PDF"+deskFileName
					// + ".pdf");
					File deskFile = new File(formFile, "PDF" + sri.getId()
							+ ".pdf");
					PdfStamper ps = new PdfStamper(reader,
							new FileOutputStream(deskFile)); // 生成的输出流

					// 获取样本信息
					// SampleInfo sbi = commonDAO.get(SampleInfo.class,
					// sri.getSampleCode());
					SampleOrder so = commonDAO.get(SampleOrder.class,
							sri.getOrderNum());
					// SampleReceive sbr = commonDAO.get(SampleReceive.class,
					// so.getSampleReceive());
					SampleReport sr = commonDAO.get(SampleReport.class, sri
							.getReport().getId());
					// 向ct-DNA中的pdf中插入数据

					AcroFields s = ps.getAcroFields();
					SimpleDateFormat nyr = new SimpleDateFormat("yyyy-MM-dd");
					// 姓名
					if (so.getName() != null) {
						s.setField("name", so.getName());
					} else {
						s.setField("name", "");
					}
					// 检测编号
					if (so.getId() != null) {
						s.setField("testNumber", so.getId());
					} else {
						s.setField("testNumber", "");
					}
					// 样本送检日期
					if (so.getReceivedDate() != null) {
						Date date = so.getReceivedDate();

						s.setField("sampleSubmissionDate", nyr.format(date));
					} else {
						s.setField("sampleSubmissionDate", "");
					}
					// 样本类型
					if (so.getSampleTypeId() != null) {
						s.setField("sampletype", so.getSampleTypeId());
					} else {
						s.setField("sampletype", "");
					}
					// 检测项目
					if (sri.getProductName() != null) {
						s.setField("testItem", sri.getProductName());
					} else {
						s.setField("testItem", "");
					}
					// 年龄
					if (so.getAge() != null) {
						s.setField("age", so.getAge());
					} else {
						s.setField("age", "");
					}
					// 报告日期
					if (new Date() != null) {
						Date dt = new Date();

						s.setField("reportDate", nyr.format(dt));
					} else {
						s.setField("reportDate", "");
					}
					// 性别
					if (so.getGender() != null) {
						if ((so.getGender()).equals(1)) {
							s.setField("gender", "男");
						} else {
							s.setField("gender", "女");
						}
					} else {
						s.setField("gender", "");
					}
					// 民族
					if (so.getNation() != null) {
						s.setField("nation", so.getNation());
					} else {
						s.setField("nation", "");
					}
					// 通讯地址
					if (so.getFamilySite() != null) {
						s.setField("postalAddress", so.getFamilySite());
					} else {
						s.setField("postalAddress", "");
					}
					// 邮编
					if (so.getZipCode() != null) {
						s.setField("zipCode", so.getZipCode());
					} else {
						s.setField("zipCode", "");
					}
					// 移动电话
					if (so.getFamilyPhone() != null) {
						s.setField("mobilePhone", so.getFamilyPhone());
					} else {
						s.setField("mobilePhone", "");
					}
					// 检测人
					s.setField("detectionPeople", "");
					// 报告人
					s.setField("reportPerson", "");
					// 审核人
					s.setField("auditPerson", "");
					// 复核人
					s.setField("reviewPerson", "");
					// 获取报告模板信息
					// ReportTemplateInfo rt = sri.getReportInfo();

					ps.setFormFlattening(true);// 这句不能少
					ps.close();
					reader.close();
					in.close();

					User user = (User) ServletActionContext.getRequest()
							.getSession()
							.getAttribute(SystemConstants.USER_SESSION_KEY);
					FileInfo fi = new FileInfo();
					fi.setUploadUser(user);
					// String a =
					// (String)super.getRequest().getAttribute("fileFileName");
					String fileName = "PDF" + sri.getId() + ".pdf";
					fi.setFileName(fileName);
					fi.setUploadTime(new Date());
					fi.setFilePath(formFile + "PDF" + sri.getId() + ".pdf");
					// if (!sri.getState().equals("1")) {
					// modelType = "";
					// }
					fi.setOwnerModel("sampleReportItem");
					fi.setUseType("pdf");
					fi.setModelContentId(sri.getId());

					fi.setFileType("pdf");
					operFileService.uploadFileStream(in, fi);
					//commonDAO.saveOrUpdate(fi);

					sri.setReportFile(fi);
					commonDAO.update(sri);
				}
			}
			sri.setState("70");
			// sri.setStateName("已生成报告");
		}
	}

	public Map<String, Object> getPatientInfo(String patientId)
			throws Exception {
		return reportDao.getPatientInfo(patientId);
	}

	public Map<String, Object> getSampleOrderInfo(String sampleCode)
			throws Exception {
		return reportDao.getSampleOrderInfo(sampleCode);
	}

	public int getmutantGenesCount(String patientId) throws Exception {
		return reportDao.getmutantGenesCount(patientId);
	}

	public String getmutantGenesList(String patientId) throws Exception {
		return reportDao.getmutantGenesList(patientId);
	}

	public List<KnowledgeBase> getMutantGenesKnowledgeList(String patientId)
			throws Exception {
		return reportDao.getMutantGenesKnowledgeList(patientId);
	}

	public void reportCompleted(String reportId) throws Exception {
		// reportDao.reportCompleted(reportId);
	}

	// 审核完成
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String id)
			throws Exception {
		SampleReport sct = reportDao.get(SampleReport.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		List<SampleReportItem> item = reportDao.selectReportItemListAll(sct
				.getId());
		for (SampleReportItem sri : item) {
			sri.setState("2");
			sri.setArc("0");
			sri.setCompleteDate(new Date());
			SampleReportTemp srt=commonDAO.get(SampleReportTemp.class, sri.getTempId());
			srt.setState("2");
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			// sampleStateService.saveSampleState(item.getCode(),
			// item.getSampleCode(), item.getProductId(),
			// item.getProductName(), "", item.getCreateDate(),
			// format.format(new Date()), "SampleReport", "报告生成",
			// (User) ServletActionContext.getRequest().getSession()
			// .getAttribute(SystemConstants.USER_SESSION_KEY),
			// sc.getId(), scp.getNextFlow(), scp.getResult(), null, null,
			// null, null, null, null, null, null);
			reportDao.saveOrUpdate(sri);
			reportDao.saveOrUpdate(srt);
		}
	}

	/**
	 * 读取PDF文件
	 * 
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public boolean getPDF() throws Exception {
		InputStream is = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("system.properties");
		Properties pro = new Properties();
		pro.load(is);
		String filePath = pro.getProperty("file.report.form.path");
		// String filePath = "D://limsPDF";
		boolean flag = true;
		File root = new File(filePath);
		File[] files = root.listFiles();
		for (File file : files) {
			SampleReportItem item = new SampleReportItem();
			String name = file.getName();
			String orderNum = name.substring(0, name.length() - 4);
			// 判断订单是否存在报告
			SampleReportItem sri = this.reportDao.getSampleReportItem(orderNum);
			if (sri == null) {
				DateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
				item.setId(format.format(new Date()) + orderNum);
				item.setOrderNum(orderNum);
				SampleOrder so = commonDAO.get(SampleOrder.class, orderNum);
				if (so != null) {
					item.setPatientId(so.getMedicalNumber());
					item.setProductId(so.getProductId());
					item.setProductName(so.getProductName());
				}
				item.setCompleteDate(new Date());
				// CrmDoctor cd=commonDAO.get(CrmDoctor.class,
				// so.getCrmDoctor().getId());
				item.setState("2");
				commonDAO.saveOrUpdate(item);
			}
			// 读取完文件后改变文件路径名
			// file.renameTo(new File(filePath1+"\\"+name));
		}
		return flag;
	}

	public Map<String, Object> findReportList(Integer start, Integer length,
			String query, String col, String sort) throws Exception {
		return reportDao.findReportList(start,
				length, query, col, sort);
	}

	public Map<String, Object> findSampleReportTempList(Integer start,
			Integer length, String query, String col, String sort) throws Exception {
		return reportDao.findSampleReportTempList(start, length, query, col, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String save(String main, String[] tempId, String userId,
			String logInfo) throws Exception {
		String id = "";
		if (main != null) {
			String mainJson = "[" + main + "]";
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					mainJson, List.class);
			SampleReport pt = new SampleReport();
			pt = (SampleReport) commonDAO.Map2Bean(list.get(0), pt);
			String name=pt.getNote();
			Date reportDate=pt.getReportDate();
			if ((pt.getId() != null && pt.getId().equals(""))
					|| pt.getId().equals("NEW")) {
				String modelName = "SampleReport";
				String markCode = "SR";
				id = codingRuleService.genTransID(modelName, markCode);
				pt.setId(id);
				pt.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				pt.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			} else {
				id = pt.getId();
				pt=commonDAO.get(SampleReport.class, id);
				pt.setNote(name);
				pt.setReportDate(reportDate);
			}
			pt.setSampleuser(userId);
			commonDAO.saveOrUpdate(pt);
			if (tempId != null) {
				for (String temp : tempId) {
					SampleReportTemp ptt = commonDAO.get(SampleReportTemp.class, temp);
					SampleReportItem sri=new SampleReportItem();
					sri.setSampleCode(ptt.getSampleCode());
					sri.setProductId(ptt.getProductId());
					sri.setOrderNum(ptt.getOrderNum());
					sri.setScopeId(ptt.getScopeId());
					sri.setScopeName(ptt.getScopeName());
					sri.setProductName(ptt.getProductName());
					sri.setReport(pt);
					sri.setTempId(ptt.getId());
					commonDAO.saveOrUpdate(sri);
				}
			}
			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext
						.getRequest()
						.getSession()
						.getAttribute(
								SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(pt.getId());
				li.setModifyContent(logInfo);
				commonDAO.saveOrUpdate(li);
			}

		}
		return id;

	}

	public Map<String, Object> findReportItemList(String id, Integer start,
			Integer length, String query, String col, String sort) throws Exception {
		return reportDao.findReportItemList(id,
				start, length, query, col, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveReportItem(String id, String dataJson, String logInfo) throws Exception {
		List<SampleReportItem> saveItems = new ArrayList<SampleReportItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(dataJson,
				List.class);
		SampleReport pt = commonDAO.get(SampleReport.class, id);
		for (Map<String, Object> map : list) {
			SampleReportItem scp = new SampleReportItem();
			// 将map信息读入实体类
			scp = (SampleReportItem) reportDao.Map2Bean(map, scp);
			SampleReportItem sri=commonDAO.get(SampleReportItem.class, scp.getId());
			if(sri!=null){
				sri.setGenotype(scp.getGenotype());
				sri.setNoteState(scp.getNoteState());
				sri.setTp(scp.getTp());
			}else{
				sri=scp;
			}
			sri.setReport(pt);
			saveItems.add(sri);
		}
		if(logInfo!=null&&!"".equals(logInfo)){
			LogInfo li=new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
					.getRequest()
					.getSession()
					.getAttribute(
							SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		reportDao.saveOrUpdateAll(saveItems);
		
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FileInfo fileInfo) {
		reportDao.saveOrUpdate(fileInfo);
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleReportTemp reportTemp) {
		reportDao.saveOrUpdate(reportTemp);
	}
}
