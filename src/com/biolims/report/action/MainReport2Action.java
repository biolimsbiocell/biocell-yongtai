package com.biolims.report.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.report.model.SampleReportItem;
import com.biolims.report.service.ReportService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/sample/report/main")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public class MainReport2Action extends BaseActionSupport {
	private static final long serialVersionUID = -8354314800608144980L;

	private String rightsId = "22022";

	@Resource
	private ReportService reportService;
	private SampleReportItem sri;
	/**
	 * 
	 * @Title: showSampleMainReportList  
	 * @Description: 列表  
	 * @author : shengwei.wang
	 * @date 2018年4月1日下午12:33:14
	 * @return
	 * @throws Exception
	 * String
	 * @throws
	 */
	@Action(value = "showSampleMainReportList")
	public String showSampleMainReportList() throws Exception {
		return dispatcher("/WEB-INF/page/report/showReportList.jsp");
	}

	@Action(value = "showSampleMainReportListJson")
	public void showSampleMainReportListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = this.reportService.findReportSendList(
					start, length, query, col, sort);
			List<SampleReportItem> list = (List<SampleReportItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("judgMent-id", "");
			map.put("judgMent-name", "");
			map.put("state", "");
			map.put("reportFile-id", "");
			map.put("reportFile-fileName", "");
			map.put("expressCompany", "");
			map.put("emsId", "");
			map.put("arc", "");
			map.put("note", "");
			map.put("remark", "");
			map.put("completeDate", "yyyy-MM-dd");
			map.put("sendDate", "yyyy-MM-dd");
			map.put("reportResult", "");
			map.put("reportResultId", "");
			map.put("crmProduct-id", "");
			map.put("crmProduct-name", "");
			map.put("reti-id", "");
			map.put("reti-name", "");
			map.put("reti-note", "");
			map.put("report-id", "");
			map.put("disease", "");
			map.put("sampleCode", "");
			map.put("fileNum", "");
			map.put("patientId", "");
			map.put("orderNum", "");
			map.put("productId", "");
			map.put("productName", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 
	 * @Title: saveSampleReportItemList  
	 * @Description: 保存  
	 * @author : shengwei.wang
	 * @date 2018年4月1日下午12:34:00
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value = "saveSampleReportItemList")
	public void saveSampleReportItemList() throws Exception {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String dataJson = getRequest().getParameter("dataJson");

			SampleReportItem sr = new SampleReportItem();

			reportService.saveSampleSendItem(sr, dataJson);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 读取PDF文件
	 * 
	 * @throws Exception
	 */
	@Action(value = "getPDF", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void getPDF() throws Exception {
		boolean flag = true;
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			flag = reportService.getPDF();
			map.put("success", flag);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}

		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public ReportService getReportService() {
		return reportService;
	}

	public void setReportService(ReportService reportService) {
		this.reportService = reportService;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public SampleReportItem getSri() {
		return sri;
	}

	public void setSri(SampleReportItem sri) {
		this.sri = sri;
	}

}
