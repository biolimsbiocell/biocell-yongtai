package com.biolims.report.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.common.service.SystemCodeService;
import com.biolims.core.model.user.UserGroupUser;
import com.biolims.core.userGroup.service.UserGroupService;
import com.biolims.crm.customer.patient.dao.CrmPatientDao;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.file.model.FileInfo;
import com.biolims.kb.model.KnowledgeBase;
import com.biolims.report.model.SampleReport;
import com.biolims.report.model.SampleReportItem;
import com.biolims.report.model.SampleReportTemp;
import com.biolims.report.service.ReportService;
import com.biolims.sample.model.SampleOrder;
import com.biolims.system.sysreport.model.ReportTemplateInfo;
import com.biolims.system.user.server.UserGroupUserService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.opensymphony.xwork2.ActionContext;

/**
 * 报告生成
 * 
 * @author niyi
 * 
 */
@Namespace("/reports")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class ReportAction extends BaseActionSupport {

	private static final long serialVersionUID = 3045828985184221805L;
	// 用于页面上显示模块名称
	private String title = "报告管理";

	// 该action权限id
	private String rightsId = "";

	private SampleReport report;

	@Resource
	private ReportService reportService;
	@Resource
	private UserGroupUserService userGroupUserService;

	@Resource
	private CommonService commonService;

	@Resource
	private SystemCodeService systemCodeService;
	@Resource
	private CrmPatientDao crmPatientDao;
	private SampleReportItem sri;

	@Resource
	private UserGroupService userGroupService;

	/**
	 * 
	 * @Title: showSampleReportList
	 * @Description: 报告生成列表
	 * @author : shengwei.wang
	 * @date 2018年3月30日上午10:16:16
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showSampleReportList")
	public String showSampleReportList() throws Exception {
		rightsId = "2022";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/report/showSampleReportList.jsp");
	}

	@Action(value = "showSampleReportListJson")
	public void showReportListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = this.reportService.findReportList(
					start, length, query, col, sort);
			List<SampleReport> list = (List<SampleReport>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("note", "");
			map.put("reportTemplate-id", "");
			map.put("sendDate", "yyyy-MM-dd");
			map.put("sendUser-name", "");
			map.put("acceptUser-name", "");
			map.put("qcUser-name", "");
			map.put("confirmDate", "yyyy-MM-dd");
			map.put("stateName", "");
			map.put("scopeId", "");
			map.put("scopeName", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: toEditSampleReport
	 * @Description:新建、编辑
	 * @author : shengwei.wang
	 * @date 2018年3月30日上午11:20:31
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "toEditSampleReport")
	public String toEditSampleReport() throws Exception {
		rightsId = "2013";
		String id = getRequest().getParameter("id");
		if (id != null && id.length() > 0) {
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			report = reportService.getSampleReportById(id);
		} else {
			report = new SampleReport();
			// sr.setId(SystemCode.DEFAULT_SYSTEMCODE);
			report.setId("NEW");
			report.setSendDate(new Date());
			report.setSendUser((User) getSession().getAttribute(
					SystemConstants.USER_SESSION_KEY));
			report.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			report.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		}
		toState(report.getState());
		List<UserGroupUser> userList = (List<UserGroupUser>) userGroupUserService
				.getUserGroupUserBygroupId("admin").get("list");
		List<UserGroupUser> selUser = new ArrayList<UserGroupUser>();
		for (int i = 0; i < userList.size(); i++) {
			if (report.getSampleuser() != null) {
				if (report.getSampleuser().indexOf(
						userList.get(i).getUser().getId()) > -1) {
					selUser.add(userList.get(i));
					userList.remove(i);
					i--;
				}
			}
		}
		putObjToContext("user", userList);
		putObjToContext("selUser", selUser);
		return dispatcher("/WEB-INF/page/report/editSampleReport.jsp");
	}

	/**
	 * 发送报告明细列表
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showSampleReportItemList")
	public String showSampleReportItemList() throws Exception {
		String bpmTaskId=getParameterFromRequest("");
		putObjToContext("bpmTaskId", bpmTaskId);
		String id=getParameterFromRequest("id");
		report=reportService.getSampleReportById(id);
		return dispatcher("/WEB-INF/page/report/showSampleReportItemList.jsp");
	}

	@Action(value = "showSampleReportItemListJson")
	public void showSampleReportItemListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String id=getParameterFromRequest("id");
		String draw = getParameterFromRequest("draw");
		try {
			List<SampleReportItem> list = new ArrayList<SampleReportItem>();
				Map<String, Object> result = reportService.findReportItemList(id,
						start, length, query, col, sort);
				list = (List<SampleReportItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "id");
			map.put("reportResult", "");
			map.put("reportResultId", "");
			map.put("sampleCode", "");
			map.put("sampleId", "");
			map.put("state", "");
			map.put("patientId", "");
			map.put("crmProduct-id", "");
			map.put("crmProduct-name", "");
			map.put("writingPersonOne-id", "");
			map.put("writingPersonOne-name", "");
			map.put("writingPersonTwo-id", "");
			map.put("writingPersonTwo-name", "");
			map.put("auditPerson-id", "");
			map.put("auditPerson-name", "");
			map.put("reti-id", "");
			map.put("reti-name", "");
			map.put("reti-note", "");
			map.put("disease", "");
			map.put("reportFile-id", "");
			map.put("reportFile-fileName", "");
			map.put("reportXFile-id", "");
			map.put("reportXFile-fileName", "");
			map.put("reportImgFile-id", "");
			map.put("reportImgFile-fileName", "");
			map.put("tp", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("orderNum", "");
			map.put("genotype", "");

			map.put("sellPerson-id", "");
			map.put("sellPerson-name", "");
			map.put("noteState", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: save  
	 * @Description: 保存  
	 * @author : shengwei.wang
	 * @date 2018年3月30日上午11:39:05
	 * @return
	 * @throws Exception
	 * String
	 * @throws
	 */
	@Action(value = "save")
	public void save() throws Exception {
		String main = getRequest().getParameter("main");
		String userId = getParameterFromRequest("user");
		String[] tempId = getRequest().getParameterValues("temp[]");
		String logInfo = getParameterFromRequest("logInfo");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String id = reportService.save(main, tempId, userId,logInfo);
			result.put("success", true);
			result.put("id", id);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 删除报表明细列表
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSampleReportItemList")
	public void delSamplePlasmaInfo() throws Exception {
		String delStr = getParameterFromRequest("del");
		String id=getParameterFromRequest("id");
		User user = (User) this
				.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			reportService.delSampleReportItemList(ids,delStr,user,id);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}

		HttpUtils.write(JsonUtils.toJsonString(map));

	}

	/**
	 * 保存结果
	 * 
	 * @return
	 * @throws Exception
	 */

	@Action(value = "saveSampleReportItemList")
	public void saveSampleReportItemList() throws Exception {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String dataJson = getRequest().getParameter("dataJson");
			String id = getRequest().getParameter("id");
			String logInfo=getRequest().getParameter("logInfo");
			reportService.saveReportItem(id,dataJson,logInfo);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "toReportGen")
	public String toReportGen() throws Exception {

		String id = getParameterFromRequest("id");// id
		String reloadAction = getParameterFromRequest("reloadAction");// reloadAction

		// String inCode = getParameterFromRequest("reportResultId");
		String inCode = "";// id
		putObjToContext("contentId", id);

		try {
			// list集合 报告明细表 sriList 赋予 获取值
			SampleReportItem sri = commonService
					.get(SampleReportItem.class, id);
			List<KnowledgeBase> kb = commonService.get(KnowledgeBase.class,
					"antistop", sri.getDisease());

			ReportTemplateInfo rti = sri.getReti();

			if (rti != null) {
				FileInfo a = rti.getAttach();

				if (sri.getReportFile() != null && reloadAction.equals("")) {
					putObjToContext("fileId", sri.getReportFile().getId());
				} else {

					if (a != null)
						putObjToContext("fileId", a.getId());
				}
				// // }

			}
		} catch (Exception e) {
			e.printStackTrace();
			return dispatcher("");
		}
		return dispatcher("/WEB-INF/page/iweboffice/DocumentEdit.jsp");
	}

	@Action(value = "toReportEdit")
	public String toReportEdit() throws Exception {
		String id = getParameterFromRequest("id");
		try {
			sri = commonService.get(SampleReportItem.class, id);
			if (sri.getReportFile() == null) {
				return dispatcher("");
			} else {
				putObjToContext("fileId", sri.getReportFile().getId());
				return dispatcher("/WEB-INF/page/iweboffice/DocumentEdit.jsp");
			}

		} catch (Exception e) {
			e.printStackTrace();
			return dispatcher("");
		}

	}

	@Action(value = "toReportEdits")
	public String toReportEdits() throws Exception {
		String id = getParameterFromRequest("id");
		try {
			sri = commonService.get(SampleReportItem.class, id);
			if (sri.getReportImgFile() == null) {
				// if (sri.getProduct().getPath() != null) {
				// FileInfo fi = new FileInfo();
				// fi.setId(sri.getProduct().getPath());
				// sri.setReportFile(fi);
				// putObjToContext("fileId", fi.getId());
				// }
				return dispatcher("");
			} else {
				putObjToContext("fileId", sri.getReportImgFile().getId());
				return dispatcher("/WEB-INF/page/iweboffice/DocumentEdit.jsp");
			}

		} catch (Exception e) {
			e.printStackTrace();
			return dispatcher("");
		}

	}

	@Action(value = "getTemplate")
	public String getTemplate() throws Exception {
		ActionContext ac = ActionContext.getContext();
		HttpServletRequest request = (HttpServletRequest) ac
				.get(ServletActionContext.HTTP_REQUEST);
		String templateId = getParameterFromRequest("templateId");
		String id = getParameterFromRequest("id");
		String sampleCode = getParameterFromRequest("sampleCode");
		Map<String, Object> result = reportService
				.getSampleOrderInfo(sampleCode);
		/*
		 * List<SampleOrder> sampleOrderInfo = (List<SampleOrder>)
		 * result.get("list"); request.setAttribute("sampleOrderInfo",
		 * sampleOrderInfo.get(0));
		 */
		String orderNum = getParameterFromRequest("orderNum");
		SampleOrder sampleOrder = crmPatientDao
				.get(SampleOrder.class, orderNum);
		Map<String, Object> result1 = reportService.getPatientInfo(sampleOrder
				.getMedicalNumber());
		List<CrmPatient> patientInfos = (List<CrmPatient>) result1.get("list");
		CrmPatient patientInfo = new CrmPatient();
		if (patientInfos.size() > 0) {
			request.setAttribute("patientInfo", patientInfos.get(0));
		} else {
			request.setAttribute("patientInfo", patientInfo);
		}
		// /**获取突变基因总数*/
		// int total = reportService.getmutantGenesCount(patientId);
		// request.setAttribute("total",total);
		// /**获取突变基因列表*/
		// String mutantGenes = reportService.getmutantGenesList(patientId);
		// request.setAttribute("mutantGenes",mutantGenes);
		// /**获取突变基因的知识库信息*/
		// List<KnowledgeBase> knowledges = new ArrayList<KnowledgeBase>();
		// knowledges = reportService.getMutantGenesKnowledgeList(patientId);
		// KnowledgeBase knowledgeBase = knowledges.get(0);
		// System.out.println(knowledgeBase.getMutationGenes());
		// request.setAttribute("knowledges",knowledges);
		String mFilePath = "";
		try {
			mFilePath = request.getSession().getServletContext()
					.getRealPath("");
			// System.out.println("mFilePath="+mFilePath);
			File f = new File(mFilePath + "/Document/" + id + ".doc");
			if (!f.exists() && !("").equals(templateId) && templateId != null) {
				ReportTemplateInfo reportTemplateInfo = this.reportService
						.getTemplateById(templateId);
				// System.out.println("模板名称="+reportTemplateInfo.getAttach().getFileName());
				FileInputStream input = new FileInputStream(mFilePath
						+ "/uploadFileRoot/"
						+ reportTemplateInfo.getAttach().getFileName());// 可替换为任何路径何和文件名
				FileOutputStream output = new FileOutputStream(mFilePath
						+ "/Document/" + id + ".doc");// 可替换为任何路径何和文件名
				int in = input.read();
				while (in != -1) {
					output.write(in);
					in = input.read();
				}
			}
		} catch (IOException e) {
			System.out.println(e.toString());
		}
		request.setAttribute("mRecordID", id);
		return dispatcher("/WEB-INF/page/iweboffice/DocumentEdit.jsp");
	}

	/**
	 * 
	 * @Title: showSampleReportTempList
	 * @Description: 左侧表
	 * @author : shengwei.wang
	 * @date 2018年3月30日上午11:21:11
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showSampleReportTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleReportTempListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = reportService
					.findSampleReportTempList(start, length, query, col, sort);
			List<SampleReportTemp> list = (List<SampleReportTemp>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("genotype", "");
			map.put("code", "");
			map.put("orderNum", "");
			map.put("sampleCode", "");
			map.put("patientId", "");
			map.put("patientName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("inspectDate", "yyyy-MM-dd");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("volume", "");
			map.put("unit", "");
			map.put("idCard", "");
			map.put("sequenceFun", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("phone", "");
			map.put("orderId", "");
			map.put("state", "");
			map.put("note", "");
			map.put("classify", "");
			map.put("writingPersonOne-id", "");
			map.put("writingPersonOne-name", "");
			map.put("writingPersonTwo-id", "");
			map.put("writingPersonTwo-name", "");

			map.put("testFroms", "");
			map.put("testCode", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "reportCompleted", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void reportCompleted() throws Exception {
		String reportId = getParameterFromRequest("reportId");
		try {
			reportService.reportCompleted(reportId);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @throws Exception
	 */
	@Action(value = "createReportFile", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void createReportFile() throws Exception {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			reportService.createReportFilePDF(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}

		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	/**
	 * @return the report
	 */
	public SampleReport getReport() {
		return report;
	}

	/**
	 * @param report
	 *            the report to set
	 */
	public void setReport(SampleReport report) {
		this.report = report;
	}

	public SampleReportItem getSri() {
		return sri;
	}

	public void setSri(SampleReportItem sri) {
		this.sri = sri;
	}

}
