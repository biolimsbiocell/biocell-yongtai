package com.biolims.report.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.biolims.dao.EntityDao;

/**
 * 报表模板
 * @author Vera
 */
@Entity
@Table(name = "T_SAMPLE_REPORT_TEMPLATE")
public class SampleReportTemplate extends EntityDao<SampleReportTemplate> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2485905971986865700L;

	@Id
	@Column(name = "ID", length = 32)
	private String id;//编号

	@Column(name = "NAME", length = 110)
	private String name;//名称

	@Column(name = "NOTE", length = 200)
	private String note;//描述

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

}
