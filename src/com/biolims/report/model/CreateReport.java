package com.biolims.report.model;

import java.lang.String;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.GeneratedValue;
import com.biolims.dao.EntityDao;
import com.biolims.file.model.FileInfo;
import com.biolims.system.sysreport.model.ReportTemplateInfo;
/**   
 * @Title: Model
 * @Description: 报告生成
 * @author lims-platform
 * @date 2015-11-03 16:21:27
 * @version V1.0   
 *
 */
@Entity
@Table(name = "CREATE_REPORT")
@SuppressWarnings("serial")
public class CreateReport extends EntityDao<CreateReport> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**玻片编号*/
	private String slideCode;
	/**订单编号*/
	private String orderCode;
	/**描述*/
	private String name;
	/**样本编号*/
	private String code;
	/**原始样本编号*/
	private String sampleCode;
	/**样本类型*/
	private String sampleType;
	/**检测项目ID*/
	private String productId;
	/**检测项目*/
	private String productName;
	/**结果*/
	private String result;
	/**结果描述*/
	private String resultDescription;
	/**临床建议*/
	private String advice;
	/**是否提交*/
	private String isSubmit;
	/**备注*/
	private String note;
	/**状态*/
	private String state;
	/**状态*/
	private String stateName;
	/**相关 实验模块*/
	private String taskType;
	/**实验分析结果ID*/
	private String taskResultId;
	/**相关实验模块ID*/
	private String taskId;
	/**报告生成日期*/
	private Date createDate;
	/**审核日期*/
	private Date waitDate;
	/**模板*/
	private ReportTemplateInfo reportInfo;
	/**附件*/
	private FileInfo template;
	/** 是否异常报告 */
	private String ycbg;
	/** 模板Id*/
	private String reportInfoId;
	/** 模板名称*/
	private String reportInfoName;
	/**细胞计数*/
	private String cellNum;
	/** 分裂相细胞计数 */
	private String flxCellNum;
	/** 核型分析细胞计数 */
	private String ktCellNum;
	/**生成报告类型*/
	private String crType;
	
	/**主治医生*/
	private String attendingDoctor;
	/**送检单位id*/
	private String crmCustomerId;
	/**送检单位name*/
	private String crmCustomerName;
	/**收样日期*/
	private Date receivedDate;
	
	public String getCrType() {
		return crType;
	}

	public void setCrType(String crType) {
		this.crType = crType;
	}

	public String getFlxCellNum() {
		return flxCellNum;
	}

	public void setFlxCellNum(String flxCellNum) {
		this.flxCellNum = flxCellNum;
	}

	public String getKtCellNum() {
		return ktCellNum;
	}

	public void setKtCellNum(String ktCellNum) {
		this.ktCellNum = ktCellNum;
	}
	
	public String getCellNum() {
		return cellNum;
	}
	public void setCellNum(String cellNum) {
		this.cellNum = cellNum;
	}
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  玻片编号
	 */
	@Column(name ="SLIDE_CODE", length = 50)
	public String getSlideCode(){
		return this.slideCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  玻片编号
	 */
	public void setSlideCode(String slideCode){
		this.slideCode = slideCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本编号
	 */
	@Column(name ="CODE", length = 50)
	public String getCode(){
		return this.code;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本编号
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 *方法: 取得String
	 *@return: String  原始样本编号
	 */
	@Column(name ="SAMPLE_CODE", length = 50)
	public String getSampleCode(){
		return this.sampleCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  原始样本编号
	 */
	public void setSampleCode(String sampleCode){
		this.sampleCode = sampleCode;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  结果
	 */
	@Column(name ="RESULT", length = 50)
	public String getResult(){
		return this.result;
	}
	/**
	 *方法: 设置String
	 *@param: String  结果
	 */
	public void setResult(String result){
		this.result = result;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 2000)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	
	public String getIsSubmit() {
		return isSubmit;
	}
	public void setIsSubmit(String isSubmit) {
		this.isSubmit = isSubmit;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setState(String state){
		this.state = state;
	}
	
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TEMPLATE")
	public FileInfo getTemplate() {
		return template;
	}
	public void setTemplate(FileInfo template) {
		this.template = template;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "REPORT_INFO")
	public ReportTemplateInfo getReportInfo() {
		return reportInfo;
	}
	public void setReportInfo(ReportTemplateInfo reportInfo) {
		this.reportInfo = reportInfo;
	}
	
	@Column(name ="RESULT_DESCRIPTION", length = 4000)
	public String getResultDescription() {
		return resultDescription;
	}
	public void setResultDescription(String resultDescription) {
		this.resultDescription = resultDescription;
	}
	
	@Column(name ="ADVICE", length = 4000)
	public String getAdvice() {
		return advice;
	}
	public void setAdvice(String advice) {
		this.advice = advice;
	}
	public String getTaskType() {
		return taskType;
	}
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
	public String getTaskResultId() {
		return taskResultId;
	}
	public void setTaskResultId(String taskResultId) {
		this.taskResultId = taskResultId;
	}
	public String getYcbg() {
		return ycbg;
	}
	public void setYcbg(String ycbg) {
		this.ycbg = ycbg;
	}
	public String getSampleType() {
		return sampleType;
	}
	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}
	public String getReportInfoId() {
		return reportInfoId;
	}
	public void setReportInfoId(String reportInfoId) {
		this.reportInfoId = reportInfoId;
	}
	public String getReportInfoName() {
		return reportInfoName;
	}
	public void setReportInfoName(String reportInfoName) {
		this.reportInfoName = reportInfoName;
	}
	public Date getWaitDate() {
		return waitDate;
	}
	public void setWaitDate(Date waitDate) {
		this.waitDate = waitDate;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}
	public String getAttendingDoctor() {
		return attendingDoctor;
	}

	public void setAttendingDoctor(String attendingDoctor) {
		this.attendingDoctor = attendingDoctor;
	}

	public String getCrmCustomerId() {
		return crmCustomerId;
	}

	public void setCrmCustomerId(String crmCustomerId) {
		this.crmCustomerId = crmCustomerId;
	}

	public String getCrmCustomerName() {
		return crmCustomerName;
	}

	public void setCrmCustomerName(String crmCustomerName) {
		this.crmCustomerName = crmCustomerName;
	}

	public Date getReceivedDate() {
		return receivedDate;
	}

	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}
}