package com.biolims.report.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import org.hibernate.annotations.ForeignKey;

import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;

/**
 * 报告发送
 * @author Vera
 */
@Entity
@Table(name = "T_SAMPLE_REPORT")
public class SampleReport extends EntityDao<SampleReport> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1697998033484315525L;
	@Id
	@Column(name = "ID", length = 32)
	private String id;//id

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "USER_SEND_USER_ID")
	private User sendUser;//发送人

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "CHECK_USER")
	private User checkUser;//审核人

	@Column(name = "SAMPLE_USER", length = 200)
	private String sampleuser;//实验操作人

	@Column(name = "NOTE", length = 200)
	private String note;//描述

	@Column(name = "SEND_DATE")
	private Date sendDate;//发送日期reportDate

	@Column(name = "REPORT_DATE")
	private Date reportDate;//报告日期

	@Column(name = "STATE", length = 32)
	private String state;//状态

	@Column(name = "STATE_NAME", length = 110)
	private String stateName;//状态名称

	

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "SAMPLE_REPORT_TEMPLATE_ID")
	private SampleReportTemplate reportTemplate;//报告模板

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "USER_ACCEPT_USER_ID")
	private User acceptUser;//接收人

	private Date acceptDate;//接收日期
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "USER_CONFIRM_USER_ID")
	private User confirmUser;//接收人

	private Date confirmDate;//接收日期
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "USER_QC_USER_ID")
	private User qcUser;//接收人

	private Date qcDate;//接收日期
	
	/** 范围Id */
	private String scopeId;
	/** 范围 */
	private String scopeName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public User getSendUser() {
		return sendUser;
	}

	public void setSendUser(User sendUser) {
		this.sendUser = sendUser;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Date getSendDate() {
		return sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

	public SampleReportTemplate getReportTemplate() {
		return reportTemplate;
	}

	public void setReportTemplate(SampleReportTemplate reportTemplate) {
		this.reportTemplate = reportTemplate;
	}


	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public User getCheckUser() {
		return checkUser;
	}

	public void setCheckUser(User checkUser) {
		this.checkUser = checkUser;
	}

	public String getSampleuser() {
		return sampleuser;
	}

	public void setSampleuser(String sampleuser) {
		this.sampleuser = sampleuser;
	}

	public User getAcceptUser() {
		return acceptUser;
	}

	public void setAcceptUser(User acceptUser) {
		if(acceptUser!=null&&acceptUser.getId()!=null&&acceptUser.getId().equals("")) acceptUser= null; this.acceptUser = acceptUser;
	}

	public Date getAcceptDate() {
		return acceptDate;
	}

	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	public User getQcUser() {
		return qcUser;
	}

	public void setQcUser(User qcUser) {
		this.qcUser = qcUser;
	}

	public Date getQcDate() {
		return qcDate;
	}

	public void setQcDate(Date qcDate) {
		this.qcDate = qcDate;
	}

	public User getConfirmUser() {
		return confirmUser;
	}

	public void setConfirmUser(User confirmUser) {
		if(confirmUser!=null&&confirmUser.getId()!=null&&confirmUser.getId().equals(""))  confirmUser= null; this.confirmUser = confirmUser;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	/**
	 * @return the scopeId
	 */
	public String getScopeId() {
		return scopeId;
	}

	/**
	 * @param scopeId the scopeId to set
	 */
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	/**
	 * @return the scopeName
	 */
	public String getScopeName() {
		return scopeName;
	}

	/**
	 * @param scopeName the scopeName to set
	 */
	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

}
