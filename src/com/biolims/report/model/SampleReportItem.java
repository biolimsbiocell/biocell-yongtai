package com.biolims.report.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

import org.hibernate.annotations.ForeignKey;

import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.file.model.FileInfo;
import com.biolims.system.product.model.Product;
import com.biolims.system.sysreport.model.ReportTemplateInfo;

/**
 * 报告明细
 * 
 * @author Vera
 */
@Entity
@Table(name = "T_SAMPLE_REPORT_ITEM")
public class SampleReportItem extends EntityDao<SampleReportItem> implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3483551441456784825L;
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 32)
	private String id;// 编号

	// @ManyToOne(fetch = FetchType.LAZY)
	// @NotFound(action = NotFoundAction.IGNORE)
	// @ForeignKey(name = "none") @JoinColumn(name = "SAMPLE_BLOOD_INFO_ID")
	// private SampleInfo blood;//全血样本

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "JUDG_MENT_ID")
	private DicType judgMent;// 常染色体结果判定

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "X_JUDG_MENT_ID")
	private DicType xJudgMent;// 性染色体结果判定

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "SAMPLE_REPORT_ID")
	private SampleReport report;// 报告

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "REPORT_TEMPLATE_INFO_ID")
	private ReportTemplateInfo reti; // 常染色体报告模版

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "X_REPORT_TEMPLATE_INFO_ID")
	private ReportTemplateInfo xReti; // 性染色体报告模版

	@Column(name = "NOTE", length = 200)
	private String note;// 备注

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "FILE_INFO_ID")
	private FileInfo reportFile;// 生成报告

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "X_FILE_INFO_ID")
	private FileInfo reportXFile;// 生成报告

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "FILE_INFO_IMG_ID")
	private FileInfo reportImgFile;// 报告图片
	@Column(name = "REPORT_RESULE", length = 2000)
	private String reportResult;//
	@Column(name = "REPORT_RESULE_ID", length = 2000)
	private String reportResultId;//

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "CRMPRODUCT_ID")
	private Product crmProduct;//
	@Column(name = "DISEASE", length = 2000)
	private String disease;// 疾病类型
	@Column(name = "TP", length = 20)
	private String tp;// 图片是否生成
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "WRITING_PERSON_ONE_ID")
	private User writingPersonOne;// 编写人1
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "WRITING_PERSON_TWO_ID")
	private User writingPersonTwo;// 编写人2
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "AUDIT_PERSON_ID")
	private User auditPerson;// 审核人
	@Column(name = "STATE", length = 20)
	private String state;// 状态 1 明细显示 2 发送报告显示
	@Column(name = "PATIENTID", length = 20)
	private String patientId;// 病历号
	@Column(name = "SAMPLECODE", length = 20)
	private String sampleCode;// 原始样本编号
	/** 检测项目编号 */
	private String productId;
	/** 检测项目名称 */
	private String productName;
	private String sampleId;// 样本编号
	/** 基因型 */
	private String genotype;
	/** 左侧表 */
	private String tempId;
	/** 订单编号 */
	private String orderNum;
	@Transient
	private String fileNum;// 附件数量

	/**
	 * 发送报告信息
	 */
	// 报告完成日期
	private Date completeDate;
	// 发送报告日期
	private Date sendDate;
	// 快递公司
	private String expressCompany;
	// 快递单号
	private String emsId;
	// 是否发送
	private String arc;
	// 报告说明
	private String remark;
	/** 范围Id */
	private String scopeId;
	/** 范围 */
	private String scopeName;
	// 结题状态
	private String noteState;

	public String getNoteState() {
		return noteState;
	}

	public void setNoteState(String noteState) {
		this.noteState = noteState;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none") @JoinColumn(name = "T_USER")
	private User sellPerson;// 实验员

	public User getSellPerson() {
		return sellPerson;
	}

	public void setSellPerson(User sellPerson) {
		this.sellPerson = sellPerson;
	}

	public Date getCompleteDate() {
		return completeDate;
	}

	public void setCompleteDate(Date completeDate) {
		this.completeDate = completeDate;
	}

	public Date getSendDate() {
		return sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

	public String getExpressCompany() {
		return expressCompany;
	}

	public void setExpressCompany(String expressCompany) {
		this.expressCompany = expressCompany;
	}

	public String getEmsId() {
		return emsId;
	}

	public void setEmsId(String emsId) {
		this.emsId = emsId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getArc() {
		return arc;
	}

	public void setArc(String arc) {
		this.arc = arc;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getSampleId() {
		return sampleId;
	}

	public void setSampleId(String sampleId) {
		this.sampleId = sampleId;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public SampleReport getReport() {
		return report;
	}

	public void setReport(SampleReport report) {
		this.report = report;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public ReportTemplateInfo getReti() {
		return reti;
	}

	public void setReti(ReportTemplateInfo reti) {
		this.reti = reti;
	}

	public FileInfo getReportFile() {
		return reportFile;
	}

	public void setReportFile(FileInfo reportFile) {
		this.reportFile = reportFile;
	}

	public DicType getJudgMent() {
		return judgMent;
	}

	public void setJudgMent(DicType judgMent) {
		this.judgMent = judgMent;
	}

	public FileInfo getReportImgFile() {
		return reportImgFile;
	}

	public void setReportImgFile(FileInfo reportImgFile) {
		this.reportImgFile = reportImgFile;
	}

	public DicType getxJudgMent() {
		return xJudgMent;
	}

	public void setxJudgMent(DicType xJudgMent) {
		this.xJudgMent = xJudgMent;
	}

	public ReportTemplateInfo getxReti() {
		return xReti;
	}

	public void setxReti(ReportTemplateInfo xReti) {
		this.xReti = xReti;
	}

	public FileInfo getReportXFile() {
		return reportXFile;
	}

	public void setReportXFile(FileInfo reportXFile) {
		this.reportXFile = reportXFile;
	}

	public String getReportResult() {
		return reportResult;
	}

	public void setReportResult(String reportResult) {
		this.reportResult = reportResult;
	}

	public String getReportResultId() {
		return reportResultId;
	}

	public void setReportResultId(String reportResultId) {
		this.reportResultId = reportResultId;
	}

	// public SampleInfo getBlood() {
	// return blood;
	// }
	//
	// public void setBlood(SampleInfo blood) {
	// this.blood = blood;
	// }

	public Product getCrmProduct() {
		return crmProduct;
	}

	public void setCrmProduct(Product crmProduct) {
		this.crmProduct = crmProduct;
	}

	public String getDisease() {
		return disease;
	}

	public void setDisease(String disease) {
		this.disease = disease;
	}

	public String getTp() {
		return tp;
	}

	public void setTp(String tp) {
		this.tp = tp;
	}

	public User getWritingPersonOne() {
		return writingPersonOne;
	}

	public void setWritingPersonOne(User writingPersonOne) {
		this.writingPersonOne = writingPersonOne;
	}

	public User getWritingPersonTwo() {
		return writingPersonTwo;
	}

	public void setWritingPersonTwo(User writingPersonTwo) {
		this.writingPersonTwo = writingPersonTwo;
	}

	public User getAuditPerson() {
		return auditPerson;
	}

	public void setAuditPerson(User auditPerson) {
		this.auditPerson = auditPerson;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getFileNum() {
		return fileNum;
	}

	public void setFileNum(String fileNum) {
		this.fileNum = fileNum;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	public String getGenotype() {
		return genotype;
	}

	public void setGenotype(String genotype) {
		this.genotype = genotype;
	}

	/**
	 * @return the tempId
	 */
	public String getTempId() {
		return tempId;
	}

	/**
	 * @param tempId the tempId to set
	 */
	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	/**
	 * @return the scopeId
	 */
	public String getScopeId() {
		return scopeId;
	}

	/**
	 * @param scopeId the scopeId to set
	 */
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	/**
	 * @return the scopeName
	 */
	public String getScopeName() {
		return scopeName;
	}

	/**
	 * @param scopeName the scopeName to set
	 */
	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}
	
}
