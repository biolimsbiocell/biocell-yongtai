package com.biolims.applicationType.action;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.applicationType.model.ApplicationTypeTable;
import com.biolims.applicationType.service.ApplicationTypeTableService;
import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.model.user.User;
import com.biolims.core.constants.SystemConstants;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/applicationTypeTable")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public class ApplicationTypeTableAction extends BaseActionSupport {
	private static final long serialVersionUID = 1362534331526869328L;

	private String rightsId = "1107";
	private ApplicationTypeTable att;
	@Resource
	private ApplicationTypeTableService applicationTypeTableService;

	/**
	 * 表单应用管理的遍历
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showApplicationTypeTableList")
	public String showApplicationTypeTableList() throws Exception {
		return dispatcher("/WEB-INF/page/applicationTypeTable/showApplicationTypeTableList.jsp");
	}

	/**
	 * 表单应用管理的遍历的遍历
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showApplicationTypeTableListJson")
	public void showApplicationTypeTableListJson() throws Exception {
		// 开始记录数
		int startNum = Integer
				.parseInt(getParameterFromRequest("start").trim());
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		limitNum = 10000;
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String queryData = getRequest().getParameter("data");

		Map<String, String> mapForQuery = null;
		if (queryData != null && queryData.length() > 0) {
			mapForQuery = JsonUtils.toObjectByJson(queryData, Map.class);
		}
		try {
			Map<String, Object> result = this.applicationTypeTableService
					.findSampleApplicationTypeTableList(mapForQuery, startNum,
							limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<ApplicationTypeTableService> list = (List<ApplicationTypeTableService>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "id");
			map.put("name", "name");
			map.put("code", "code");
			map.put("tableName", "tableName");
			map.put("entitypackage", "entitypackage");
			map.put("entityname", "entityname");
			map.put("type", "type");
			map.put("orderNumber", "orderNumber");
			map.put("pathName", "pathName");
			map.put("applicationType-id", "applicationType-id");
			map.put("classPath", "");
			map.put("workflowUserColumn", "workflowUserColumn");
			map.put("state", "state");
			map.put("upTable", "upTable");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 保存详细信息列表
	 * 
	 * @throws Exception
	 */
	@Action(value = "saveApplicationTypeTableList")
	public void saveApplicationTypeTableList() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String itemDataJson = getRequest().getParameter("itemDataJson");
			applicationTypeTableService
					.saveApplicationTypeTableList(itemDataJson);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除表单应用管理的信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delApplicationTypeTable")
	public void delApplicationTypeTable() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			applicationTypeTableService.delApplicationTypeTable(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: showDialogApplicationTypeTableTable
	 * @Description: 选择实验模块
	 * @author : shengwei.wang
	 * @date 2018年3月12日上午10:49:57
	 * @return String
	 * @throws
	 */
	@Action(value = "showDialogApplicationTypeTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack") )
	public String showDialogApplicationTypeTable() {
		String flag=getParameterFromRequest("flag");
		putObjToContext("flag", flag);
		return dispatcher("/WEB-INF/page/applicationTypeTable/showDialogApplicationTypeTable.jsp");

	}

	@Action(value = "showDialogApplicationTypeTableJson")
	public void showDialogApplicationTypeTableJson() {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String flag=getParameterFromRequest("flag");
		try {
			Map<String, Object> result = applicationTypeTableService
					.showDialogApplicationTypeTableJson(flag,start, length, query,
							col, sort);
			List<ApplicationTypeTable> list = (List<ApplicationTypeTable>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("enName", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showDialogApplicationTypeTableList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogApplicationTypeTableList() throws Exception {
		return dispatcher("/WEB-INF/page/applicationTypeTable/showDialogApplicationTypeTableList.jsp");
	}

	@Action(value = "applicationTypeTableSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String userSelect() throws Exception {
		String planType = getParameterFromRequest("planType");
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		map.put("id", new String[] { "", "string", "", "ID", "180", "true", "",
				"", "", "", "", "" });
		map.put("name", new String[] { "", "string", "", "名称", "250", "true",
				"", "", "", "", "", "" });

		String type = generalexttype(map);
		String col = generalextcol(map);
		// 用于ext显示
		putObjToContext("planType", planType);
		putObjToContext("type", type);
		putObjToContext("col", col);
		// 用于判断当前页面类型,LIST类型
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		// 用于调用json时的action链接
		putObjToContext("path", ServletActionContext.getRequest()
				.getContextPath()
				+ "/applicationTypeTable/applicationTypeTableSelectJson.action");
		// 导向jsp页面显示
		return dispatcher("/WEB-INF/page/applicationTypeTable/applicationTypeTableSelect.jsp");

	}

	@Action(value = "applicationTypeTableSelectJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void applicationTypeTableSelectJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式

		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");

		Map<String, Object> controlMap = applicationTypeTableService
				.findSampleApplicationTypeTableList(null, startNum, limitNum,
						dir, sort);

		long totalCount = Long.parseLong(controlMap.get("total").toString());
		List<User> list = (List<User>) controlMap.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "name");

		new SendData().sendDateJson(map, list, totalCount,
				ServletActionContext.getResponse());
	}

	public ApplicationTypeTable getAtt() {
		return att;
	}

	public void setAtt(ApplicationTypeTable att) {
		this.att = att;
	}

	public ApplicationTypeTableService getApplicationTypeTableService() {
		return applicationTypeTableService;
	}

	public void setApplicationTypeTableService(
			ApplicationTypeTableService applicationTypeTableService) {
		this.applicationTypeTableService = applicationTypeTableService;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}
}
