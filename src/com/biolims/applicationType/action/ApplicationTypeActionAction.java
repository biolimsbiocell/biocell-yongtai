package com.biolims.applicationType.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.applicationType.model.ApplicationTypeAction;
import com.biolims.applicationType.service.ApplicationTypeActionService;
import com.biolims.applicationType.service.ApplicationTypeTableService;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.main.service.MainService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.MD5Util;
import com.biolims.util.SendData;

@Namespace("/applicationTypeAction")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public class ApplicationTypeActionAction extends BaseActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8496447109231185390L;
	private String rightsId = "1108";

	private ApplicationTypeAction ata;

	@Resource
	private ApplicationTypeActionService applicationTypeActionService;

	@Autowired
	private MainService mainService;

	/**
	 *动作管理的列表
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showApplicationTypeActionList")
	public String showApplicationTypeActionList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/applicationTypeAction/showApplicationTypeActionList.jsp");
	}

	/**
	 * 动作管理的列表
	 * @return
	 * @throws Exception 
	 */
	@Action(value = "showApplicationTypeActionListJson")
	public void showApplicationTypeActionListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");

		String queryData = getRequest().getParameter("data");

		Map<String, String> mapForQuery = null;
		if (queryData != null && queryData.length() > 0) {
			mapForQuery = JsonUtils.toObjectByJson(queryData, Map.class);
		}
		try {
			Map<String, Object> result = this.applicationTypeActionService.findApplicationTypeActionList(mapForQuery,
					startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<ApplicationTypeTableService> list = (List<ApplicationTypeTableService>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "id");
			map.put("actionName", "actionName");
			map.put("applicationTypeTable-id", "applicationTypeTable-id");
			map.put("classPath", "classPath");
			map.put("modifyAttribute", "modifyAttribute");
			map.put("modifyValue", "modifyValue");
			map.put("exeFuncPath", "exeFuncPath");
			map.put("stateValue", "stateValue");
			map.put("actionMethod", "actionMethod");
			map.put("type", "type");
			map.put("state", "state");
			map.put("orderNumber", "orderNumber");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 保存表单类型
	 * @throws Exception
	 */
	@Action(value = "saveApplicationTypeActionList")
	public void saveApplicationTypeActionList() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String itemDataJson = getRequest().getParameter("itemDataJson");
			applicationTypeActionService.saveApplicationTypeActionList(itemDataJson);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 *删除表单类型
	 * @throws Exception
	 */
	@Action(value = "delApplicationTypeAction")
	public void delApplicationTypeAction() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			applicationTypeActionService.delApplicationTypeAction(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	//动作查看
	@Action(value = "applicationTypeActionLook", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String applicationTypeActionLook() throws Exception {
		String tableId = ServletActionContext.getRequest().getParameter("tableId");
		String formId = ServletActionContext.getRequest().getParameter("formId");
		List<ApplicationTypeAction> app=applicationTypeActionService
				.findApplicationTypeActionList(tableId);
		putObjToContext("applicationTypeActionList", applicationTypeActionService
				.findApplicationTypeActionList(tableId));
		putObjToContext("formId", formId);
		return dispatcher("/WEB-INF/page/applicationTypeAction/applicationTypeActionLook.jsp");

	}

	/**
	 * 执行改变状态的方法
	 */
	@Action(value = "exeFun", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String exeFun() throws Exception {
		Map<String, String> messageMap = new HashMap<String, String>();
		try {

			String applicationTypeActionId = ServletActionContext.getRequest().getParameter("applicationTypeActionId");
			String contentId = ServletActionContext.getRequest().getParameter("formId");
//			String userId = ServletActionContext.getRequest().getParameter("userId");
//			String userPass = ServletActionContext.getRequest().getParameter("userPass");
			String r = "";

//			User user = mainService.loginUser(userId, userPass);
//
//			User sessionUser = (User) getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
//
//			if (user != null && user.getState() != null && user.getState().getId().startsWith("1")) {
//				if (!user.equals(sessionUser)) {
//					if (!MD5Util.getMD5Lower(userPass).equals(user.getUserPassword())) {
//						messageMap.put("message", "用户名、密码错!");
//						return this.renderText(JsonUtils.toJsonString(messageMap));
//					}
//				} else {
//
//					messageMap.put("message", "用户名无效!");
//					return this.renderText(JsonUtils.toJsonString(messageMap));
//
//				}
//			}
//			getRequest().getSession().setAttribute("spUser", user);
			r = applicationTypeActionService.exeEvent(applicationTypeActionId, contentId);
			messageMap.put("message", r);
			//messageMap.put("message", "保存成功!");
		} catch (Exception e) {
			e.printStackTrace();
			messageMap.put("message", "请稍后再试，或者联系管理员!");

		}
		return this.renderText(JsonUtils.toJsonString(messageMap));

	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public ApplicationTypeAction getAta() {
		return ata;
	}

	public void setAta(ApplicationTypeAction ata) {
		this.ata = ata;
	}

	public ApplicationTypeActionService getApplicationTypeActionService() {
		return applicationTypeActionService;
	}

	public void setApplicationTypeActionService(ApplicationTypeActionService applicationTypeActionService) {
		this.applicationTypeActionService = applicationTypeActionService;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
