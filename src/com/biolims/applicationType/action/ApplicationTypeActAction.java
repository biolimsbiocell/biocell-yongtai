package com.biolims.applicationType.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.applicationType.model.ApplicationType;
import com.biolims.applicationType.service.ApplicationTypeService;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/applicationType")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class ApplicationTypeActAction extends BaseActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = -592833248848236109L;

	private String rightsId = "1120";

	private ApplicationType at;

	@Resource
	private ApplicationTypeService applicationTypeService;

	/**
	 * 表单类型列表
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showApplicationTypeList")
	public String showApplicationTypeList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/applicationType/showApplicationTypeList.jsp");
	}

	/**
	 * 表单类型列表
	 * @return
	 * @throws Exception 
	 */
	@Action(value = "showApplicationTypeListJson")
	public void showApplicationTypeListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");

		String queryData = getRequest().getParameter("data");

		Map<String, String> mapForQuery = null;
		if (queryData != null && queryData.length() > 0) {
			mapForQuery = JsonUtils.toObjectByJson(queryData, Map.class);
		}
		try {
			Map<String, Object> result = this.applicationTypeService.findApplicationTypeList(mapForQuery, startNum,
					limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<ApplicationTypeService> list = (List<ApplicationTypeService>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "id");
			map.put("name", "name");
			map.put("state", "state");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 保存表单类型
	 * @throws Exception
	 */
	@Action(value = "saveApplicationTypeList")
	public void saveApplicationTypeList() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String itemDataJson = getRequest().getParameter("itemDataJson");
			applicationTypeService.saveApplicationTypeList(itemDataJson);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 *删除表单类型
	 * @throws Exception
	 */
	@Action(value = "delApplicationType")
	public void delApplicationType() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			applicationTypeService.delApplicationType(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showDialogApplicationTypeList")
	public String showDialogApplicationTypeList() throws Exception {
		return dispatcher("/WEB-INF/page/applicationType/showDialogApplicationTypeList.jsp");
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public ApplicationType getAt() {
		return at;
	}

	public void setAt(ApplicationType at) {
		this.at = at;
	}

	public ApplicationTypeService getApplicationTypeService() {
		return applicationTypeService;
	}

	public void setApplicationTypeService(ApplicationTypeService applicationTypeService) {
		this.applicationTypeService = applicationTypeService;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
