package com.biolims.applicationType.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.applicationType.dao.ApplicationTypeActionDao;
import com.biolims.applicationType.model.ApplicationTypeAction;
import com.biolims.applicationType.model.ApplicationTypeActionLog;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.common.model.user.User;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.main.service.MainService;
import com.biolims.util.JsonUtils;

@Service
@Transactional
public class ApplicationTypeActionService {
	@Resource
	private ApplicationTypeActionDao applicationTypeActionDao;

	/**
	 * 动作管理列表
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findApplicationTypeActionList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		return applicationTypeActionDao.selectApplicationTypeActionList(mapForQuery, startNum, limitNum, dir, sort);
	}

	/**
	 * 动作管理保存和修改
	 * @param sbr
	 * @param itemDataJson
	 * @throws Exception
	 */
	/*	@WriteOperLog
		@WriteExOperLog
		@Transactional(rollbackFor = Exception.class)
		public void saveApplicationTypeActionList(String itemDataJson) throws Exception {
			List<ApplicationTypeAction> saveItems = new ArrayList<ApplicationTypeAction>();
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
			for (Map<String, Object> map : list) {
				ApplicationTypeAction sba = new ApplicationTypeAction();
				// 将map信息读入实体类
				sba = (ApplicationTypeAction) applicationTypeActionDao.Map2Bean(map, sba);
				saveItems.add(sba);
			}
			applicationTypeActionDao.saveOrUpdateAll(saveItems);
		}*/
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveApplicationTypeActionList(String itemDataJson) throws Exception {
		//		List<ApplicationTypeAction> saveItems = new ArrayList<ApplicationTypeAction>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			ApplicationTypeAction sba = new ApplicationTypeAction();
			// 将map信息读入实体类
			sba = (ApplicationTypeAction) applicationTypeActionDao.Map2Bean(map, sba);
			applicationTypeActionDao.saveOrUpdate(sba);
		}
		//		applicationTypeActionDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveApplicationTypeActionLog(String actionName, String tableId, String oldActionName, String contentId,
			String note, User operUser, Date operDate) throws Exception {
		ApplicationTypeActionLog atal = new ApplicationTypeActionLog();
		atal.setActionName(actionName);
		atal.setApplicationTypeTableId(tableId);
		atal.setContentId(contentId);
		atal.setNote(note);
		atal.setOperUser(operUser);
		atal.setOperDate(operDate);
		atal.setOldActionName(oldActionName);

		applicationTypeActionDao.saveOrUpdate(atal);
	}

	/**
	 * 动作管理的删除
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delApplicationTypeAction(String[] ids) throws Exception {
		for (String id : ids) {
			ApplicationTypeAction at = applicationTypeActionDao.get(ApplicationTypeAction.class, id);
			at.setState("0");
			applicationTypeActionDao.saveOrUpdate(at);
		}
	}

	public List<ApplicationTypeAction> findApplicationTypeActionList(String tableId) throws Exception {

		return applicationTypeActionDao.findApplicationTypeActionListByTable(tableId);

	}

	public String exeEvent(String applicationTypeActionId, String formId) throws Exception {
		ObjectEvent objectEvent = null;
		ApplicationTypeAction ata = applicationTypeActionDao.get(ApplicationTypeAction.class, applicationTypeActionId);
		String r = "";
		if (ata != null) {
			if (ata.getClassPath() != null && !ata.getClassPath().equals(""))
				objectEvent = (ObjectEvent) Class.forName(ata.getClassPath()).newInstance();
			if (objectEvent != null) {

				r = objectEvent.operation(ata.getId(), formId);
			}
		}
		return r;
	}
}
