package com.biolims.applicationType.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.applicationType.dao.ApplicationTypeTableDao;
import com.biolims.applicationType.model.ApplicationTypeTable;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.util.JsonUtils;

@Service
public class ApplicationTypeTableService {

	@Resource
	private ApplicationTypeTableDao applicationTypeTableDao;

	/**
	 * 应用管理列表
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findSampleApplicationTypeTableList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		return applicationTypeTableDao.selectApplicationTypeTableList(mapForQuery, startNum, limitNum, dir, sort);
	}

	/**
	 * 保存应用管理
	 * @param sbr
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveApplicationTypeTableList(String itemDataJson) throws Exception {
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			ApplicationTypeTable sbi = new ApplicationTypeTable();
			// 将map信息读入实体类
			sbi = (ApplicationTypeTable) applicationTypeTableDao.Map2Bean(map, sbi);
			applicationTypeTableDao.saveOrUpdate(sbi);
		}

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delApplicationTypeTable(String[] ids) throws Exception {
		for (String id : ids) {
			ApplicationTypeTable att = applicationTypeTableDao.get(ApplicationTypeTable.class, id);
			att.setState("0");
			applicationTypeTableDao.saveOrUpdate(att);
		}
	}

	public Map<String, Object> showDialogApplicationTypeTableJson(
			String flag, Integer start, Integer length, String query, String col, String sort) throws Exception {
		return applicationTypeTableDao.showDialogApplicationTypeTableJson(flag,start, length, query,
				col, sort);
	}
}
