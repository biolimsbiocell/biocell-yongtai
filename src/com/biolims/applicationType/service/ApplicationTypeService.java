package com.biolims.applicationType.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.applicationType.dao.ApplicationTypeDAO;
import com.biolims.applicationType.model.ApplicationType;
import com.biolims.applicationType.model.ApplicationTypeAction;
import com.biolims.applicationType.model.ApplicationTypeTable;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.main.service.MainService;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;

@Service
@Transactional
public class ApplicationTypeService extends CommonService {
	@Resource
	private ApplicationTypeDAO applicationTypeDAO;

	/**
	 * 表单类型的列表
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findApplicationTypeList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		return applicationTypeDAO.selectApplicationTypeList(mapForQuery, startNum, limitNum, dir, sort);
	}

	/**
	 * 表单类型的保存和修改
	 * @param sbr
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveApplicationTypeList(String itemDataJson) throws Exception {
		List<ApplicationType> saveItems = new ArrayList<ApplicationType>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			ApplicationType sbi = new ApplicationType();
			// 将map信息读入实体类
			sbi = (ApplicationType) applicationTypeDAO.Map2Bean(map, sbi);
			applicationTypeDAO.saveOrUpdate(sbi);
		}

	}

	/**
	 * 表单类型的删除
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delApplicationType(String[] ids) throws Exception {
		for (String id : ids) {
			ApplicationType at = applicationTypeDAO.get(ApplicationType.class, id);
			at.setState("0");
			applicationTypeDAO.saveOrUpdate(at);
		}
	}

	public void setObjectState(Object o, String id, String value) throws Exception {
		o = applicationTypeDAO.get(o.getClass(), id);

		try {
			BeanUtils.getDeclaredField(o, "state");
			BeanUtils.setFieldValue(o, "state", value);
			applicationTypeDAO.update(o);

		} catch (NoSuchFieldException n) {

		}

	}

	public void setObjectCurrencyWork(Object o, String id, User confirmUser, String workflowNodeName) throws Exception {
		o = applicationTypeDAO.get(o.getClass(), id);

		try {
			BeanUtils.getDeclaredField(o, "stateName");
			BeanUtils.getDeclaredField(o, "confirmUser");
			BeanUtils.getDeclaredField(o, "confirmDate");
			BeanUtils.setFieldValue(o, "stateName", workflowNodeName);
			BeanUtils.setFieldValue(o, "confirmUser", confirmUser);
			BeanUtils.setFieldValue(o, "confirmDate", new Date());
			applicationTypeDAO.update(o);

		} catch (NoSuchFieldException n) {

		}

	}

	public void setObjectValue(Object o, String id, String field, String value) throws Exception {
		o = applicationTypeDAO.get(o.getClass(), id);

		try {
			BeanUtils.getDeclaredField(o, field);
			BeanUtils.setFieldValue(o, field, value);
			applicationTypeDAO.update(o);

		} catch (NoSuchFieldException n) {

		}

	}

	public List<ApplicationTypeAction> findApplicationTypeActionList(String tableId) throws Exception {

		return applicationTypeDAO.selectApplicationTypeActionListByTable(tableId);

	}

	public ApplicationTypeTable getApplicationTypeTable(String tableId) throws Exception {

		return applicationTypeDAO.getApplicationTypeTable(tableId);

	}

	public User getApplicationTypeTableWorkflowUserColumn(ApplicationTypeTable att, String formId) throws Exception {

		return applicationTypeDAO.getApplicationTypeTableWorkflowUserColumn(att, formId);

	}

	public String exeEvent(String applicationTypeActionId, String formId) throws Exception {
		ObjectEvent objectEvent = null;
		ApplicationTypeAction ata = applicationTypeDAO.get(ApplicationTypeAction.class, applicationTypeActionId);
		String r = "";
		if (ata != null) {
			if (ata.getClassPath() != null && !ata.getClassPath().equals(""))
				objectEvent = (ObjectEvent) Class.forName(ata.getClassPath()).newInstance();
			if (objectEvent != null) {

				r = objectEvent.operation(applicationTypeActionId, formId);
			}
		}
		return r;
	}

	public void setObjectStateNameByObject(Object o, String formId, String value) throws Exception {

		try {
			Object b = get(o.getClass(), formId);
			BeanUtils.getDeclaredField(b, "stateName");
			BeanUtils.setFieldValue(b, "stateName", value);
			saveOrUpdate(b);

		} catch (NoSuchFieldException n) {

		}

	}

	public void setObjectStateName(String applicationTypeActionId, String formId, String value) throws Exception {

		try {
			ApplicationTypeAction ata = applicationTypeDAO.get(ApplicationTypeAction.class, applicationTypeActionId);

			if (ata.getApplicationTypeTable().getClassPath() != null
					&& !ata.getApplicationTypeTable().getClassPath().equals("")) {

				Object o = Class.forName(ata.getApplicationTypeTable().getClassPath()).newInstance();
				setObjectStateNameByObject(o, formId, ata.getActionName());
				saveOrUpdate(o);
			}

		} catch (NoSuchFieldException n) {

		}

	}

	public void setObjectConfirmUser(Object o, String id, User user) throws Exception {

		Object b = get(o.getClass(), id);

		try {
			BeanUtils.getDeclaredField(b, "confirmUser");
			BeanUtils.setFieldValue(b, "confirmUser", user);
			saveOrUpdate(b);

		} catch (NoSuchFieldException n) {

		}

	}

	public void commonSetState(String applicationTypeActionId, String formId, String value) throws Exception {

		ApplicationTypeAction ata = applicationTypeDAO.get(ApplicationTypeAction.class, applicationTypeActionId);

		if (ata.getApplicationTypeTable().getClassPath() != null
				&& !ata.getApplicationTypeTable().getClassPath().equals("")) {

			Object o = Class.forName(ata.getApplicationTypeTable().getClassPath()).newInstance();
			setObjectState(o, formId, value);
			setObjectStateNameByObject(o, formId, ata.getActionName());

			User user;
			if (ServletActionContext.getRequest().getSession().getAttribute("spUser") != null)
				user = (User) ServletActionContext.getRequest().getSession().getAttribute("spUser");
			else
				user = (User) ServletActionContext.getRequest().getSession().getAttribute(
						SystemConstants.USER_SESSION_KEY);
			BeanUtils.setFieldValue(o, "confirmUser", user);
			BeanUtils.setFieldValue(o, "confirmDate", new Date());

		}
	}
}
