package com.biolims.applicationType.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.biolims.dao.EntityDao;

/**
 * 应用类型表
 * @author WR 
 *
 */
@Entity
@Table(name = "T_APPLICATION_TYPE")
@SuppressWarnings("serial")
public class ApplicationType extends EntityDao<ApplicationType> {

	//主键
	@Id
	@Column(name = "ID", length = 32)
	private String id;

	//应用类型名称
	@Column(name = "NAME", length = 50)
	private String name;
	//状态（0 无效 1生效）
	@Column(name = "STATE", length = 32)
	private String state;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
