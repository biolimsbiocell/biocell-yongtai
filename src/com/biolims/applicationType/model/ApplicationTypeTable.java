package com.biolims.applicationType.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;

/**
 * 应用表单表
 * @author WR
 *
 */
@Entity
@Table(name = "T_APPLICATION_TYPE_TABLE")
@SuppressWarnings("serial")
public class ApplicationTypeTable extends EntityDao<ApplicationTypeTable> {

	//主键
	@Id
	@Column(name = "ID", length = 32)
	private String id;

	//表单名称
	@Column(name = "NAME", length = 50)
	private String name;

	//路径
	@Column(name = "PATH_NAME", length = 200)
	private String pathName;

	//所属应用类型
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "APPLICATION_TYPE_ID")
	private ApplicationType applicationType;

	//表单对应实体的类路径
	@Column(name = "CLASS_PATH", length = 200)
	private String classPath;
	//workflow用户字段
	@Column(name = "WORKFLOW_USER_COLUMN", length = 50)
	private String workflowUserColumn;

	//状态(0 无效 1生效)
	@Column(name = "STATE", length = 1)
	private String state;
	//表名称
	@Column(name = "TABLE_NAME", length = 200)
	private String tableName;
	//包名称
	@Column(name = "ENTITYPACKAGE", length = 200)
	private String entitypackage;
	//实体名称
	@Column(name = "ENTITY_NAME", length = 200)
	private String entityname;
	//类型  主页 标签
	@Column(name = "TYPE", length = 200)
	private String type;
	//排序号
	@Column(name = "ORDER_NUMBER", length = 200)
	private Integer orderNumber;

	@Column(name = "TREE_TYPE", length = 200)
	private String treeType;

	@Column(name = "UP_TABLE", length = 200)
	private String upTable;

	@Column(name = "CODE", length = 200)
	private String code;
	
	@Column(name = "NEW_PATH_NAME", length = 200)
	private String newPathName;
	
	@Column(name = "EN_NAME", length = 200)
	private String enName;
	
	/**模块类型 用于区分 一个模块下不同类型的子模块*/
	@Column(name = "MODULE_TYPE",length = 200)
	private String moduleType;
	

	public String getModuleType() {
		return moduleType;
	}

	public void setModuleType(String moduleType) {
		this.moduleType = moduleType;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPathName() {
		return pathName;
	}

	public void setPathName(String pathName) {
		this.pathName = pathName;
	}

	public ApplicationType getApplicationType() {
		return applicationType;
	}

	public void setApplicationType(ApplicationType applicationType) {
		this.applicationType = applicationType;
	}

	public String getClassPath() {
		return classPath;
	}

	public void setClassPath(String classPath) {
		this.classPath = classPath;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getWorkflowUserColumn() {
		return workflowUserColumn;
	}

	public void setWorkflowUserColumn(String workflowUserColumn) {
		this.workflowUserColumn = workflowUserColumn;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getEntitypackage() {
		return entitypackage;
	}

	public void setEntitypackage(String entitypackage) {
		this.entitypackage = entitypackage;
	}

	public String getEntityname() {
		return entityname;
	}

	public void setEntityname(String entityname) {
		this.entityname = entityname;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getTreeType() {
		return treeType;
	}

	public void setTreeType(String treeType) {
		this.treeType = treeType;
	}

	public String getUpTable() {
		return upTable;
	}

	public void setUpTable(String upTable) {
		this.upTable = upTable;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getNewPathName() {
		return newPathName;
	}

	public void setNewPathName(String newPathName) {
		this.newPathName = newPathName;
	}

	public String getEnName() {
		return enName;
	}

	public void setEnName(String enName) {
		this.enName = enName;
	}

}
