package com.biolims.applicationType.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;

/**
 * 应用表单表
 * @author WR
 *
 */
@Entity
@Table(name = "T_APPL_TYPE_ACTION_LOG")
@SuppressWarnings("serial")
public class ApplicationTypeActionLog extends EntityDao<ApplicationTypeActionLog> {

	//主键
	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	//事件名称
	@Column(name = "ACTION_NAME", length = 200)
	private String actionName;

	//所属应用表单类型

	@Column(name = "APPL_TYPE_TABLE_ID", length = 36)
	private String applicationTypeTableId;

	//内容id
	@Column(name = "CONTENT_ID", length = 200)
	private String contentId;

	//需要修改的状态值
	@Column(name = "OLD_ACTION_NAME", length = 32)
	private String oldActionName;

	//0.修改状态 1.执行程序 2.修改属性
	@Column(name = "NOTE", length = 200)
	private String note;

	@Column(name = "NEXT_NOTE", length = 200)
	private String nextNote;

	//类型  0，null 单一实体型 1 通用型
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")
	@JoinColumn(name = "OPER_USER")
	private User operUser;

	private Date operDate;

	private Date startDate;

	private Date endDate;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getActionName() {
		return actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

	public String getApplicationTypeTableId() {
		return applicationTypeTableId;
	}

	public void setApplicationTypeTableId(String applicationTypeTableId) {
		this.applicationTypeTableId = applicationTypeTableId;
	}

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public String getOldActionName() {
		return oldActionName;
	}

	public void setOldActionName(String oldActionName) {
		this.oldActionName = oldActionName;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public User getOperUser() {
		return operUser;
	}

	public void setOperUser(User operUser) {
		this.operUser = operUser;
	}

	public Date getOperDate() {
		return operDate;
	}

	public void setOperDate(Date operDate) {
		this.operDate = operDate;
	}

	public String getNextNote() {
		return nextNote;
	}

	public void setNextNote(String nextNote) {
		this.nextNote = nextNote;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

}
