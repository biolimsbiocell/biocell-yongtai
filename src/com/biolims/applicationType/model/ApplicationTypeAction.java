package com.biolims.applicationType.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;

/**
 * 应用表单表
 * @author WR
 *
 */
@Entity
@Table(name = "T_APPL_TYPE_ACTION")
@SuppressWarnings("serial")
public class ApplicationTypeAction extends EntityDao<ApplicationTypeAction> {

	//主键
	@Id
	@Column(name = "ID", length = 32)
	private String id;

	//事件名称
	@Column(name = "ACTION_NAME", length = 200)
	private String actionName;
	//事件名称
	@Column(name = "EN_NAME", length = 200)
	private String enName;

	//所属应用表单类型
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")
	@JoinColumn(name = "APPL_TYPE_TABLE_ID")
	private ApplicationTypeTable applicationTypeTable;

	//表单对应实体的类路径
	@Column(name = "CLASS_PATH", length = 200)
	private String classPath;

	//需要修改的属性
	@Column(name = "MODIFY_ATTRIBUTE", length = 200)
	private String modifyAttribute;

	//需要修改的值
	@Column(name = "MODIFY_VALUE", length = 200)
	private String modifyValue;

	//需要修改的值的路径
	@Column(name = "EXE_FUNC_PATH", length = 200)
	private String exeFuncPath;

	//需要修改的状态值
	@Column(name = "STATE_VALUE", length = 32)
	private String stateValue;

	//0.修改状态 1.执行程序 2.修改属性
	@Column(name = "ACTION_METHOD", length = 200)
	private String actionMethod;

	//类型  0，null 单一实体型 1 通用型
	@Column(name = "TYPE", length = 32)
	private String type;

	//状态
	@Column(name = "STATE", length = 32)
	private String state;
	//排序号
	@Column(name = "ORDER_NUMBER")
	private Integer orderNumber;

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public ApplicationTypeAction() {

	}

	public ApplicationTypeAction(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ApplicationTypeTable getApplicationTypeTable() {
		return applicationTypeTable;
	}

	public void setApplicationTypeTable(ApplicationTypeTable applicationTypeTable) {
		this.applicationTypeTable = applicationTypeTable;
	}

	public String getClassPath() {
		return classPath;
	}

	public void setClassPath(String classPath) {
		this.classPath = classPath;
	}

	public String getModifyAttribute() {
		return modifyAttribute;
	}

	public void setModifyAttribute(String modifyAttribute) {
		this.modifyAttribute = modifyAttribute;
	}

	public String getModifyValue() {
		return modifyValue;
	}

	public void setModifyValue(String modifyValue) {
		this.modifyValue = modifyValue;
	}

	public String getExeFuncPath() {
		return exeFuncPath;
	}

	public void setExeFuncPath(String exeFuncPath) {
		this.exeFuncPath = exeFuncPath;
	}

	public String getStateValue() {
		return stateValue;
	}

	public void setStateValue(String stateValue) {
		this.stateValue = stateValue;
	}

	public String getActionMethod() {
		return actionMethod;
	}

	public void setActionMethod(String actionMethod) {
		this.actionMethod = actionMethod;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getActionName() {
		return actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

	public Integer getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}

	/**
	 * @return the enName
	 */
	public String getEnName() {
		return enName;
	}

	/**
	 * @param enName the enName to set
	 */
	public void setEnName(String enName) {
		this.enName = enName;
	}

}
