package com.biolims.applicationType.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.applicationType.model.ApplicationTypeAction;
import com.biolims.applicationType.model.ApplicationTypeTable;
import com.biolims.common.model.user.User;
import com.biolims.dao.BaseHibernateDao;

@Repository
public class ApplicationTypeDAO extends BaseHibernateDao {

	public Map<String, Object> selectApplicationTypeList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String key = "";
		String hql = "from ApplicationType where 1=1";
		if (mapForQuery != null) {
			key = map2where(mapForQuery);
		}
		Long total = (Long) queryUniqueResult("select count(*) " + hql + key);
		List<ApplicationTypeTable> list = new ArrayList<ApplicationTypeTable>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			list = queryList(hql + key, startNum, limitNum);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public List<ApplicationTypeAction> selectApplicationTypeActionListByTable(String applicationTypeTablId)
			throws Exception {
		String hql = "select ata from ApplicationTypeAction ata where ata.applicationTypeTable.id=? and ata.state='1' order by orderNumber ASC";
		return find(hql, new Object[] { applicationTypeTablId });
	}

	public ApplicationTypeTable getApplicationTypeTable(String id) {
		return this.get(ApplicationTypeTable.class, id);
	}

	public User getApplicationTypeTableWorkflowUserColumn(ApplicationTypeTable att, String formId) {

		List<User> userList = find("select a." + att.getWorkflowUserColumn() + " from " + att.getClassPath()
				+ " a where a.id ='" + formId + "'");

		User aUser = new User();
		if (userList.size() > 0)
			aUser = userList.get(0);
		else
			aUser = null;
		return aUser;
	}
}
