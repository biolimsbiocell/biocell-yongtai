package com.biolims.applicationType.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.applicationType.model.ApplicationTypeTable;
import com.biolims.dao.BaseHibernateDao;

@Repository
@SuppressWarnings("unchecked")
public class ApplicationTypeTableDao extends BaseHibernateDao {

	public Map<String, Object> selectApplicationTypeTableList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String key = "";
		String hql = "from ApplicationTypeTable where 1=1";
		if (mapForQuery != null) {
			key = map2where(mapForQuery);
		}

		Long total = (Long) queryUniqueResult("select count(*) " + hql + key);
		List<ApplicationTypeTable> list = new ArrayList<ApplicationTypeTable>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			list = queryList(hql + key, startNum, limitNum);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> showDialogApplicationTypeTableJson(String flag, Integer start, Integer length,
			String query, String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from ApplicationTypeTable where 1=1 and code='2'";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		if (flag != null && !"".equals(flag)) {
			key += " and code='" + flag + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from ApplicationTypeTable where 1=1 and code='2' ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				key += " order by " + col + " " + sort;
			}
			List<ApplicationTypeTable> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

}
