package com.biolims.applicationType.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.applicationType.model.ApplicationTypeAction;
import com.biolims.dao.BaseHibernateDao;

@Repository
public class ApplicationTypeActionDao extends BaseHibernateDao {

	public Map<String, Object> selectApplicationTypeActionList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String key = "";
		String hql = "from ApplicationTypeAction where state='1'";
		if (mapForQuery != null) {
			key = map2where(mapForQuery);
		}

		Long total = (Long) queryUniqueResult("select count(*) " + hql + key);
		List<ApplicationTypeAction> list = new ArrayList<ApplicationTypeAction>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			list = queryList(hql + key, startNum, limitNum);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public List<ApplicationTypeAction> findApplicationTypeActionListByTable(String applicationTypeTablId)
			throws Exception {
		String hql = "select ata from ApplicationTypeAction ata where ata.applicationTypeTable.id=? and ata.state='1' order by orderNumber ASC";

		return find(hql, new Object[] { applicationTypeTablId });

	}

}
