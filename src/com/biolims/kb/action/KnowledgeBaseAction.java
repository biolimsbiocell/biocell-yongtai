package com.biolims.kb.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemCode;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.kb.model.KnowledgeBase;
import com.biolims.kb.service.KnowledgeBaseService;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/kb")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class KnowledgeBaseAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "2306";
	@Autowired
	private KnowledgeBaseService knowledgeBaseService;
	private KnowledgeBase knowledgeBase = new KnowledgeBase();

	@Action(value = "showKnowledgeBaseList")
	public String showKnowledgeBaseList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/kb/knowledgeBase.jsp");
	}

	@Action(value = "showKnowledgeBaseListJson")
	public void showKnowledgeBaseListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = knowledgeBaseService.findKnowledgeBaseList(map2Query, startNum, limitNum, dir,
				sort);
		Long count = (Long) result.get("total");
		List<KnowledgeBase> list = (List<KnowledgeBase>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("mutationGenes", "");
		map.put("type-id", "");
		map.put("type-name", "");
		map.put("original", "");
		map.put("otherName", "");
		map.put("daodu", "");
		map.put("biologySpecific", "");
		map.put("antistop", "");
		map.put("commonMutaionCancer", "");
		map.put("noopen", "");
		map.put("state-id", "");
		map.put("state-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "knowledgeBaseSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogKnowledgeBaseList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/kb/knowledgeBaseDialog.jsp");
	}

	@Action(value = "showDialogKnowledgeBaseListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogKnowledgeBaseListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = knowledgeBaseService.findKnowledgeBaseList(map2Query, startNum, limitNum, dir,
				sort);
		Long count = (Long) result.get("total");
		List<KnowledgeBase> list = (List<KnowledgeBase>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("mutationGenes", "");
		map.put("type-id", "");
		map.put("type-name", "");
		map.put("original", "");
		map.put("otherName", "");
		map.put("daodu", "");
		map.put("biologySpecific", "");
		map.put("antistop", "");
		map.put("commonMutaionCancer", "");
		map.put("noopen", "");
		map.put("state-id", "");
		map.put("state-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "editKnowledgeBase")
	public String editKnowledgeBase() throws Exception {
		String id = getParameterFromRequest("id");

		if (id != null && !id.equals("")) {
			knowledgeBase = knowledgeBaseService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		} else {
			knowledgeBase.setId(SystemCode.DEFAULT_SYSTEMCODE);
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			knowledgeBase.setCreateUser(user);
			knowledgeBase.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		return dispatcher("/WEB-INF/page/kb/knowledgeBaseEdit.jsp");
	}

	@Action(value = "copyKnowledgeBase")
	public String copyKnowledgeBase() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		knowledgeBase = knowledgeBaseService.get(id);
		knowledgeBase.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/kb/knowledgeBaseEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = knowledgeBase.getId();
		if (id != null && id.equals("")) {
			knowledgeBase.setId(null);
		}
		Map aMap = new HashMap();
		knowledgeBaseService.save(knowledgeBase, aMap);
		return redirect("/kb/editKnowledgeBase.action?id=" + knowledgeBase.getId());

	}

	@Action(value = "viewKnowledgeBase")
	public String toViewKnowledgeBase() throws Exception {
		String id = getParameterFromRequest("id");
		knowledgeBase = knowledgeBaseService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/kb/knowledgeBaseEdit.jsp");
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public KnowledgeBaseService getKnowledgeBaseService() {
		return knowledgeBaseService;
	}

	public void setKnowledgeBaseService(KnowledgeBaseService knowledgeBaseService) {
		this.knowledgeBaseService = knowledgeBaseService;
	}

	public KnowledgeBase getKnowledgeBase() {
		return knowledgeBase;
	}

	public void setKnowledgeBase(KnowledgeBase knowledgeBase) {
		this.knowledgeBase = knowledgeBase;
	}

}
