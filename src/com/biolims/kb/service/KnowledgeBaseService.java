package com.biolims.kb.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.constants.SystemCode;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.service.SystemCodeService;
import com.biolims.kb.dao.KnowledgeBaseDao;
import com.biolims.kb.model.KnowledgeBase;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.util.DateUtil;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class KnowledgeBaseService {
	@Resource
	private KnowledgeBaseDao knowledgeBaseDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SystemCodeService systemCodeService;

	public Map<String, Object> findKnowledgeBaseList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return knowledgeBaseDao.selectKnowledgeBaseList(mapForQuery, startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(KnowledgeBase i) throws Exception {

		knowledgeBaseDao.saveOrUpdate(i);

	}

	public List<KnowledgeBase> findKnowledgeBaseList() {
		return knowledgeBaseDao.selectKnowledgeBaseList();
	}

	public KnowledgeBase get(String id) {
		KnowledgeBase knowledgeBase = commonDAO.get(KnowledgeBase.class, id);
		return knowledgeBase;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(KnowledgeBase sc, Map jsonMap) throws Exception {
		if (sc != null) {
			if (sc.getId().equals(SystemCode.DEFAULT_SYSTEMCODE)) {
				String code = systemCodeService.getCodeByPrefix("KnowledgeBase", "KB"
						+ DateUtil.dateFormatterByPattern(new Date(), "yyyyMMdd"), 000, 3,
						null);
				sc.setId(code);
			}

			knowledgeBaseDao.saveOrUpdate(sc);

			String jsonStr = "";
		}
	}
}
