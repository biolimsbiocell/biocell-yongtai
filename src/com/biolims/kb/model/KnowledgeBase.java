package com.biolims.kb.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;

import com.biolims.dic.model.DicType;
import com.biolims.dic.model.DicCurrencyType;

import com.biolims.dic.model.DicState;

import com.biolims.common.model.user.User;

/**   
 * @Title: Model
 * @Description: 知识库
 * @author lims-platform
 * @date 2014-07-16 09:17:44
 * @version V1.0   
 *
 */
@Entity
@Table(name = "KNOWLEDGE_BASE")
@SuppressWarnings("serial")
public class KnowledgeBase extends EntityDao<KnowledgeBase> implements java.io.Serializable {
	/**编码*/
	private java.lang.String id;
	/**选择类别*/
	private DicType type;
	/**突变基因*/
	private java.lang.String mutationGenes;//新增突变基因
	/**其他名称*/
	private java.lang.String otherName;//变更字段title
	/**生物学特定功能	*/
	private java.lang.String biologySpecific;//变更字段note
	/**常见突变与癌症*/
	private java.lang.String commonMutaionCancer;//变更字段correlation
	
	
	/**是否原创*/
	private java.lang.String original;	
	/**导读*/
	private java.lang.String daodu;	
	/**关键词*/
	private java.lang.String antistop;
	
	/**不公开*/
	private java.lang.String noopen;
	/**状态*/
	private DicState state;
	/**创建人*/
	private User createUser;
	/**创建日期*/
	private java.util.Date createDate;
	/**附件路径*/
	private java.lang.String path;
	
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  编码
	 */

	@Id
	@Column(name = "ID", length = 36)
	public java.lang.String getId() {
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  编码
	 */
	public void setId(java.lang.String id) {
		this.id = id;
	}

	/**
	 *方法: 取得DicType
	 *@return: DicType  选择类别
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TYPE")
	public DicType getType() {
		return this.type;
	}

	/**
	 *方法: 设置DicType
	 *@param: DicType  选择类别
	 */
	public void setType(DicType type) {
		this.type = type;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  是否原创
	 */
	@Column(name = "ORIGINAL", length = 5)
	public java.lang.String getOriginal() {
		return this.original;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  是否原创
	 */
	public void setOriginal(java.lang.String original) {
		this.original = original;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  标题
	 */
	@Column(name = "OTHERNAME", length = 110)
	public java.lang.String getOtherName() {
		return otherName;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  标题
	 */
	public void setOtherName(java.lang.String otherName) {
		this.otherName = otherName;
	}
	

	

	

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  导读
	 */
	@Column(name = "DAODU", length = 200)
	public java.lang.String getDaodu() {
		return this.daodu;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  导读
	 */
	public void setDaodu(java.lang.String daodu) {
		this.daodu = daodu;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  内容
	 */
	@Column(name = "BIOLOGYSPECIFIC", length = 4000)
	public java.lang.String getBiologySpecific() {
		return biologySpecific;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  内容
	 */
	public void setBiologySpecific(java.lang.String biologySpecific) {
		this.biologySpecific = biologySpecific;
	}
	

	

	

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  关键词
	 */
	@Column(name = "ANTISTOP", length = 50)
	public java.lang.String getAntistop() {
		return this.antistop;
	}//对

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  关键词
	 */
	public void setAntistop(java.lang.String antistop) {
		this.antistop = antistop;
	}

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  相关知识
	 */
	@Column(name = "COMMON_MUTATION_CANCER", length = 2000)
	public java.lang.String getCommonMutaionCancer() {
		return commonMutaionCancer;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  相关知识
	 */
	public void setCommonMutaionCancer(java.lang.String commonMutaionCancer) {
		this.commonMutaionCancer = commonMutaionCancer;
	}

	

	

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  不公开
	 */
	@Column(name = "NOOPEN", length = 5)
	public java.lang.String getNoopen() {
		return this.noopen;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  不公开
	 */
	public void setNoopen(java.lang.String noopen) {
		this.noopen = noopen;
	}

	/**
	 *方法: 取得DicState
	 *@return: DicState  状态
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STATE")
	public DicState getState() {
		return this.state;
	}

	/**
	 *方法: 设置DicState
	 *@param: DicState  状态
	 */
	public void setState(DicState state) {
		this.state = state;
	}

	public java.lang.String getPath() {
		return path;
	}

	public void setPath(java.lang.String path) {
		this.path = path;
	}

	/**
	 *方法: 取得User
	 *@return: User  创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 *方法: 设置User
	 *@param: User  创建人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  创建日期
	 */
	@Column(name = "CREATE_DATE", length = 50)
	public java.util.Date getCreateDate() {
		return this.createDate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  创建日期
	 */
	public void setCreateDate(java.util.Date createDate) {
		this.createDate = createDate;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  新增突变基因
	 */
	@Column(name = "MUTATIONGENES", length = 50)
	public java.lang.String getMutationGenes() {
		return mutationGenes;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String 新增突变基因
	 */
	public void setMutationGenes(java.lang.String mutationGenes) {
		this.mutationGenes = mutationGenes;
	}

	

	
	
	
	
	
}