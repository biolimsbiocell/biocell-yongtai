package com.biolims.kb.zsk.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.kb.zsk.model.KnowledgeBXBXZLZSK;
import com.biolims.kb.zsk.model.KnowledgeBXJDZSKResult;
import com.biolims.kb.zsk.model.KnowledgeBXZLZSK;
import com.biolims.kb.zsk.model.KnowledgeBXZSK;

@Repository
@SuppressWarnings("unchecked")
public class KnowledgeBXZSKDao extends BaseHibernateDao {
	public Map<String, Object> selectKnowledgeBXZSKList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from KnowledgeBXZSK where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<KnowledgeBXZSK> list = new ArrayList<KnowledgeBXZSK>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
		public Map<String, Object> selectKnowledgeBXJDZSKResultList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from KnowledgeBXJDZSKResult where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and knowledge.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<KnowledgeBXJDZSKResult> list = new ArrayList<KnowledgeBXJDZSKResult>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectKnowledgeBXBXZLZSKList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from KnowledgeBXBXZLZSK where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and knowledge.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<KnowledgeBXBXZLZSK> list = new ArrayList<KnowledgeBXBXZLZSK>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public List<KnowledgeBXBXZLZSK> isExist(String name){
			List<KnowledgeBXBXZLZSK> list = new ArrayList<KnowledgeBXBXZLZSK>();
			String hql = "from KnowledgeBXBXZLZSK where 1=1 ";
			String key = "";
			Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
			if(total > 0){
				key = key + " and ywName = '" + name +"'";
				Long total_name = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
				if(total_name>0){
					list = this.getSession().createQuery(hql + key).list();
					return list;
				}
			}
			return null;			
		}
		public Integer isCount(String name){
			List<KnowledgeBXBXZLZSK> list = new ArrayList<KnowledgeBXBXZLZSK>();
			String hql = "from KnowledgeBXBXZLZSK where 1=1 ";
			String key = "";
			Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
			if(total > 0){
				key = key + " and ywName = '" + name +"'";
				Long total_name = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
				if(total_name>0){
					list = this.getSession().createQuery(hql + key).list();
					return list.size()+1;
				}
			}
			return 1;			
		}
}