package com.biolims.kb.zsk.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.kb.zsk.model.KnowledgeBXBXZLZSK;
import com.biolims.kb.zsk.model.KnowledgeBXZLZSK;
import com.biolims.kb.zsk.model.KnowledgeJDZSKResult;
import com.biolims.kb.zsk.model.KnowledgeZSK;

@Repository
@SuppressWarnings("unchecked")
public class KnowledgeZSKDao extends BaseHibernateDao {
	public Map<String, Object> selectKnowledgeZSKList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from KnowledgeZSK where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<KnowledgeZSK> list = new ArrayList<KnowledgeZSK>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
		public Map<String, Object> selectKnowledgeJDZSKResultList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from KnowledgeJDZSKResult where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and knowledge.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<KnowledgeJDZSKResult> list = new ArrayList<KnowledgeJDZSKResult>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectKnowledgeBXZLZSKList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from KnowledgeBXZLZSK where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and knowledge.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<KnowledgeBXZLZSK> list = new ArrayList<KnowledgeBXZLZSK>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public List<KnowledgeJDZSKResult> isExist(String name){
			List<KnowledgeJDZSKResult> list = new ArrayList<KnowledgeJDZSKResult>();
			String hql = "from KnowledgeJDZSKResult where 1=1 ";
			String key = "";
			Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
			if(total > 0){
				key = key + " and gene = '" + name +"'";
				Long total_name = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
				if(total_name>0){
					list = this.getSession().createQuery(hql + key).list();
					return list;
				}
			}
			return null;			
		}
		public Integer isCount(String name){
			List<KnowledgeJDZSKResult> list = new ArrayList<KnowledgeJDZSKResult>();
			String hql = "from KnowledgeJDZSKResult where 1=1 ";
			String key = "";
			Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
			if(total > 0){
				key = key + " and gene = '" + name +"'";
				Long total_name = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
				if(total_name>0){
					list = this.getSession().createQuery(hql + key).list();
					return list.size()+1;
				}
			}
			return 1;			
		}
}