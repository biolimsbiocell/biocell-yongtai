package com.biolims.kb.zsk.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.common.model.user.User;
/**   
 * @Title: Model
 * @Description: 解读结果
 * @author lims-platform
 * @date 2016-07-11 16:36:14
 * @version V1.0   
 *
 */
@Entity
@Table(name = "KNOWLEDGE_JD_ZSK_RESULT")
@SuppressWarnings("serial")
public class KnowledgeJDZSKResult extends EntityDao<KnowledgeJDZSKResult> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**基因*/
	private String gene;
	/**突变类型*/
	private String tbType;
	/**解读*/
	private String jdResult;
	/**上传人*/
	private User createUser;
	/**上传时间*/
	private Date createDate;
	/**相关主表*/
	private KnowledgeZSK knowledge;
	/**是否有效*/
	private String isValid;
	/**版本号*/
	private String version;
	@Column(name ="VERSION", length = 20)
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	@Column(name ="IS_VALID", length = 20)
	public String getIsValid() {
		return isValid;
	}
	public void setIsValid(String isValid) {
		this.isValid = isValid;
	}
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  基因
	 */
	@Column(name ="GENE", length = 255)
	public String getGene(){
		return this.gene;
	}
	/**
	 *方法: 设置String
	 *@param: String  基因
	 */
	public void setGene(String gene){
		this.gene = gene;
	}
	/**
	 *方法: 取得String
	 *@return: String  突变类型
	 */
	@Column(name ="TB_TYPE", length = 255)
	public String getTbType(){
		return this.tbType;
	}
	/**
	 *方法: 设置String
	 *@param: String  突变类型
	 */
	public void setTbType(String tbType){
		this.tbType = tbType;
	}
	/**
	 *方法: 取得String
	 *@return: String  解读
	 */
	@Column(name ="JD_RESULT", length = 255)
	public String getJdResult(){
		return this.jdResult;
	}
	/**
	 *方法: 设置String
	 *@param: String  解读
	 */
	public void setJdResult(String jdResult){
		this.jdResult = jdResult;
	}
	/**
	 *方法: 取得User
	 *@return: User  上传人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser(){
		return this.createUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  上传人
	 */
	public void setCreateUser(User createUser){
		this.createUser = createUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  上传时间
	 */
	@Column(name ="CREATE_DATE", length = 50)
	public Date getCreateDate(){
		return this.createDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  上传时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 *方法: 取得KnowledgeZSK
	 *@return: KnowledgeZSK  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "KNOWLEDGE")
	public KnowledgeZSK getKnowledge(){
		return this.knowledge;
	}
	/**
	 *方法: 设置KnowledgeZSK
	 *@param: KnowledgeZSK  相关主表
	 */
	public void setKnowledge(KnowledgeZSK knowledge){
		this.knowledge = knowledge;
	}
	
}