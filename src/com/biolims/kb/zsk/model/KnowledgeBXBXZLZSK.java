package com.biolims.kb.zsk.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.common.model.user.User;
/**   
 * @Title: Model
 * @Description: 靶向治疗药物
 * @author lims-platform
 * @date 2016-07-11 16:36:16
 * @version V1.0   
 *
 */
@Entity
@Table(name = "KNOWLEDGE_BX_BXZL_ZSK")
@SuppressWarnings("serial")
public class KnowledgeBXBXZLZSK extends EntityDao<KnowledgeBXBXZLZSK> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**药物名称*/
	private String ywName;
	/**商品名*/
	private String spName;
	/**靶点/原理*/
	private String spot;
	/**审批状态/临床试验状态*/
	private String state;
	/**上传人*/
	private User createUser;
	/**上传时间*/
	private Date createDate;
	/**相关主表*/
	private KnowledgeBXZSK knowledge;
	/**是否有效*/
	private String isValid;
	/**版本号*/
	private String version;
	@Column(name ="VERSION", length = 20)
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	@Column(name ="IS_VALID", length = 20)
	public String getIsValid() {
		return isValid;
	}
	public void setIsValid(String isValid) {
		this.isValid = isValid;
	}
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  药物名称
	 */
	@Column(name ="YW_NAME", length = 255)
	public String getYwName(){
		return this.ywName;
	}
	/**
	 *方法: 设置String
	 *@param: String  药物名称
	 */
	public void setYwName(String ywName){
		this.ywName = ywName;
	}
	/**
	 *方法: 取得String
	 *@return: String  商品名
	 */
	@Column(name ="SP_NAME", length = 255)
	public String getSpName(){
		return this.spName;
	}
	/**
	 *方法: 设置String
	 *@param: String  商品名
	 */
	public void setSpName(String spName){
		this.spName = spName;
	}
	/**
	 *方法: 取得String
	 *@return: String  靶点/原理
	 */
	@Column(name ="SPOT", length = 255)
	public String getSpot(){
		return this.spot;
	}
	/**
	 *方法: 设置String
	 *@param: String  靶点/原理
	 */
	public void setSpot(String spot){
		this.spot = spot;
	}
	/**
	 *方法: 取得String
	 *@return: String  审批状态/临床试验状态
	 */
	@Column(name ="STATE", length = 255)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  审批状态/临床试验状态
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得User
	 *@return: User  上传人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser(){
		return this.createUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  上传人
	 */
	public void setCreateUser(User createUser){
		this.createUser = createUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  上传时间
	 */
	@Column(name ="CREATE_DATE", length = 50)
	public Date getCreateDate(){
		return this.createDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  上传时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 *方法: 取得KnowledgeZSK
	 *@return: KnowledgeZSK  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "KNOWLEDGE")
	public KnowledgeBXZSK getKnowledge(){
		return this.knowledge;
	}
	/**
	 *方法: 设置KnowledgeZSK
	 *@param: KnowledgeZSK  相关主表
	 */
	public void setKnowledge(KnowledgeBXZSK knowledge){
		this.knowledge = knowledge;
	}
}