package com.biolims.kb.zsk.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.kb.zsk.model.KnowledgeBXBXZLZSK;
import com.biolims.kb.zsk.model.KnowledgeBXJDZSKResult;
import com.biolims.kb.zsk.model.KnowledgeBXZLZSK;
import com.biolims.kb.zsk.model.KnowledgeBXZSK;
import com.biolims.kb.zsk.model.KnowledgeJDZSKResult;
import com.biolims.kb.zsk.model.KnowledgeZSK;
import com.biolims.kb.zsk.service.KnowledgeBXZSKService;
import com.biolims.kb.zsk.service.KnowledgeZSKService;
import com.biolims.util.DateHelper;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.util.HttpUtils;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;

@Namespace("/kb/zsk/knowledgeBXZSK")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class KnowledgeBXZSKAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "260110";
	@Autowired
	private KnowledgeBXZSKService knowledgeBXZSKService;
	private KnowledgeBXZSK knowledgeBXZSK = new KnowledgeBXZSK();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;

	@Action(value = "showKnowledgeBXZSKList")
	public String showKnowledgeBXZSKList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/kb/zsk/knowledgeBXZSK.jsp");
	}

	@Action(value = "showKnowledgeBXZSKListJson")
	public void showKnowledgeBXZSKListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = knowledgeBXZSKService
				.findKnowledgeBXZSKList(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<KnowledgeBXZSK> list = (List<KnowledgeBXZSK>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("patientId", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("sampleType-id", "");
		map.put("sampleType-name", "");
		map.put("product-id", "");
		map.put("product-name", "");
		map.put("joinlab", "");
		// map.put("geneName", "");
		// map.put("mutationType", "");
		map.put("drugName", "");
		map.put("goodName", "");
		map.put("spot", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "knowledgeBXZSKSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogKnowledgeBXZSKList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/kb/zsk/knowledgeBXZSKDialog.jsp");
	}

	@Action(value = "showDialogKnowledgeBXZSKListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogKnowledgeBXZSKListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = knowledgeBXZSKService
				.findKnowledgeBXZSKList(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<KnowledgeBXZSK> list = (List<KnowledgeBXZSK>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("patientId", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("sampleType-id", "");
		map.put("sampleType-name", "");
		map.put("product-id", "");
		map.put("product-name", "");
		map.put("joinlab", "");
		map.put("geneName", "");
		map.put("mutationType", "");
		map.put("drugName", "");
		map.put("goodName", "");
		map.put("spot", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editKnowledgeBXZSK")
	public String editKnowledgeBXZSK() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			knowledgeBXZSK = knowledgeBXZSKService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "knowledgeBXZSK");
		} else {
			knowledgeBXZSK.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			knowledgeBXZSK.setCreateUser(user);
			knowledgeBXZSK.setCreateDate(new Date());
			/*
			 * knowledgeZSK.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			 * knowledgeZSK.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			 */
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/kb/zsk/knowledgeBXZSKEdit.jsp");
	}

	@Action(value = "copyKnowledgeBXZSK")
	public String copyKnowledgeBXZSK() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		knowledgeBXZSK = knowledgeBXZSKService.get(id);
		knowledgeBXZSK.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/kb/zsk/knowledgeBXZSKEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = knowledgeBXZSK.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "KnowledgeBXZSK";
			String markCode = "KNBX";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			knowledgeBXZSK.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("knowledgeBXJDZSKResult",
				getParameterFromRequest("knowledgeBXJDZSKResultJson"));

		aMap.put("knowledgeBXBXZLZSK",
				getParameterFromRequest("knowledgeBXBXZLZSKJson"));

		knowledgeBXZSKService.save(knowledgeBXZSK, aMap);
		return redirect("/kb/zsk/knowledgeBXZSK/editKnowledgeBXZSK.action?id="
				+ knowledgeBXZSK.getId());

	}

	@Action(value = "viewKnowledgeBXZSK")
	public String toViewKnowledgeBXZSK() throws Exception {
		String id = getParameterFromRequest("id");
		knowledgeBXZSK = knowledgeBXZSKService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/kb/zsk/knowledgeBXZSKEdit.jsp");
	}

	@Action(value = "showKnowledgeBXJDZSKResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showKnowledgeBXJDZSKResultList() throws Exception {
		return dispatcher("/WEB-INF/page/kb/zsk/knowledgeBXJDZSKResult.jsp");
	}

	@Action(value = "showKnowledgeBXJDZSKResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showKnowledgeBXJDZSKResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = knowledgeBXZSKService
					.findKnowledgeBXJDZSKResultList(scId, startNum, limitNum,
							dir, sort);
			Long total = (Long) result.get("total");
			List<KnowledgeBXJDZSKResult> list = (List<KnowledgeBXJDZSKResult>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("gene", "");
			map.put("tbType", "");
			map.put("jdResult", "");
			map.put("createUser-name", "");
			map.put("createUser-id", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("knowledge-name", "");
			map.put("knowledge-id", "");
			map.put("isValid", "");
			map.put("version", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKnowledgeBXJDZSKResult")
	public void delKnowledgeBXJDZSKResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			knowledgeBXZSKService.delKnowledgeBXJDZSKResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showKnowledgeBXBXZLZSKList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showKnowledgBXeBXZLZSKList() throws Exception {
		return dispatcher("/WEB-INF/page/kb/zsk/knowledgeBXBXZLZSK.jsp");
	}

	@Action(value = "showKnowledgeBXBXZLZSKListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showKnowledgeBXBXZLZSKListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = knowledgeBXZSKService
					.findKnowledgeBXBXZLZSKList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<KnowledgeBXBXZLZSK> list = (List<KnowledgeBXBXZLZSK>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("ywName", "");
			map.put("spName", "");
			map.put("spot", "");
			map.put("state", "");
			map.put("createUser-name", "");
			map.put("createUser-id", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("knowledge-name", "");
			map.put("knowledge-id", "");
			map.put("isValid", "");
			map.put("version", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKnowledgeBXBXZLZSK")
	public void delKnowledgeBXBXZLZSK() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			knowledgeBXZSKService.delKnowledgeBXBXZLZSK(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public KnowledgeBXZSKService getKnowledgeBXZSKService() {
		return knowledgeBXZSKService;
	}

	public void setKnowledgeBXZSKService(
			KnowledgeBXZSKService knowledgeBXZSKService) {
		this.knowledgeBXZSKService = knowledgeBXZSKService;
	}

	public KnowledgeBXZSK getKnowledgeBXZSK() {
		return knowledgeBXZSK;
	}

	public void setKnowledgeBXZSK(KnowledgeBXZSK knowledgeBXZSK) {
		this.knowledgeBXZSK = knowledgeBXZSK;
	}

}
