package com.biolims.kb.zsk.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.kb.zsk.model.KnowledgeBXZLZSK;
import com.biolims.kb.zsk.model.KnowledgeJDZSKResult;
import com.biolims.kb.zsk.model.KnowledgeZSK;
import com.biolims.kb.zsk.service.KnowledgeZSKService;
import com.biolims.util.DateHelper;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.util.HttpUtils;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;

@Namespace("/kb/zsk/knowledgeZSK")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class KnowledgeZSKAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "260112";
	@Autowired
	private KnowledgeZSKService knowledgeZSKService;
	private KnowledgeZSK knowledgeZSK = new KnowledgeZSK();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;

	@Action(value = "showKnowledgeZSKList")
	public String showKnowledgeZSKList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/kb/zsk/knowledgeZSK.jsp");
	}

	@Action(value = "showKnowledgeZSKListJson")
	public void showKnowledgeZSKListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = knowledgeZSKService.findKnowledgeZSKList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<KnowledgeZSK> list = (List<KnowledgeZSK>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("patientId", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("sampleType-id", "");
		map.put("sampleType-name", "");
		map.put("product-id", "");
		map.put("product-name", "");
		// map.put("joinlab", "");
		map.put("geneName", "");
		map.put("mutationType", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "knowledgeZSKSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogKnowledgeZSKList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/kb/zsk/knowledgeZSKDialog.jsp");
	}

	@Action(value = "showDialogKnowledgeZSKListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogKnowledgeZSKListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = knowledgeZSKService.findKnowledgeZSKList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<KnowledgeZSK> list = (List<KnowledgeZSK>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("patientId", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("sampleType-id", "");
		map.put("sampleType-name", "");
		map.put("product-id", "");
		map.put("product-name", "");
		// map.put("joinlab", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editKnowledgeZSK")
	public String editKnowledgeZSK() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			knowledgeZSK = knowledgeZSKService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "knowledgeZSK");
		} else {
			knowledgeZSK.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			knowledgeZSK.setCreateUser(user);
			knowledgeZSK.setCreateDate(new Date());
			/*
			 * knowledgeZSK.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			 * knowledgeZSK.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			 */
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/kb/zsk/knowledgeZSKEdit.jsp");
	}

	@Action(value = "copyKnowledgeZSK")
	public String copyKnowledgeZSK() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		knowledgeZSK = knowledgeZSKService.get(id);
		knowledgeZSK.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/kb/zsk/knowledgeZSKEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = knowledgeZSK.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "KnowledgeZSK";
			String markCode = "KNZSK";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			knowledgeZSK.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("knowledgeJDZSKResult",
				getParameterFromRequest("knowledgeJDZSKResultJson"));

		aMap.put("knowledgeBXZLZSK",
				getParameterFromRequest("knowledgeBXZLZSKJson"));

		knowledgeZSKService.save(knowledgeZSK, aMap);
		return redirect("/kb/zsk/knowledgeZSK/editKnowledgeZSK.action?id="
				+ knowledgeZSK.getId());

	}

	@Action(value = "viewKnowledgeZSK")
	public String toViewKnowledgeZSK() throws Exception {
		String id = getParameterFromRequest("id");
		knowledgeZSK = knowledgeZSKService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/kb/zsk/knowledgeZSKEdit.jsp");
	}

	@Action(value = "showKnowledgeJDZSKResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showKnowledgeJDZSKResultList() throws Exception {
		return dispatcher("/WEB-INF/page/kb/zsk/knowledgeJDZSKResult.jsp");
	}

	@Action(value = "showKnowledgeJDZSKResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showKnowledgeJDZSKResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = knowledgeZSKService
					.findKnowledgeJDZSKResultList(scId, startNum, limitNum,
							dir, sort);
			Long total = (Long) result.get("total");
			List<KnowledgeJDZSKResult> list = (List<KnowledgeJDZSKResult>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("gene", "");
			map.put("tbType", "");
			map.put("jdResult", "");
			map.put("createUser-name", "");
			map.put("createUser-id", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("knowledge-name", "");
			map.put("knowledge-id", "");
			map.put("isValid", "");
			map.put("version", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKnowledgeJDZSKResult")
	public void delKnowledgeJDZSKResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			knowledgeZSKService.delKnowledgeJDZSKResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showKnowledgeBXZLZSKList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showKnowledgeBXZLZSKList() throws Exception {
		return dispatcher("/WEB-INF/page/kb/zsk/knowledgeBXZLZSK.jsp");
	}

	@Action(value = "showKnowledgeBXZLZSKListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showKnowledgeBXZLZSKListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = knowledgeZSKService
					.findKnowledgeBXZLZSKList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<KnowledgeBXZLZSK> list = (List<KnowledgeBXZLZSK>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("ywName", "");
			map.put("spName", "");
			map.put("spot", "");
			map.put("state", "");
			map.put("createUser-name", "");
			map.put("createUser-id", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("knowledge-name", "");
			map.put("knowledge-id", "");
			map.put("isValid", "");
			map.put("version", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKnowledgeBXZLZSK")
	public void delKnowledgeBXZLZSK() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			knowledgeZSKService.delKnowledgeBXZLZSK(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public KnowledgeZSKService getKnowledgeZSKService() {
		return knowledgeZSKService;
	}

	public void setKnowledgeZSKService(KnowledgeZSKService knowledgeZSKService) {
		this.knowledgeZSKService = knowledgeZSKService;
	}

	public KnowledgeZSK getKnowledgeZSK() {
		return knowledgeZSK;
	}

	public void setKnowledgeZSK(KnowledgeZSK knowledgeZSK) {
		this.knowledgeZSK = knowledgeZSK;
	}

}
