package com.biolims.kb.zsk.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.kb.zsk.dao.KnowledgeZSKDao;
import com.biolims.kb.zsk.model.KnowledgeBXZLZSK;
import com.biolims.kb.zsk.model.KnowledgeJDZSKResult;
import com.biolims.kb.zsk.model.KnowledgeZSK;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class KnowledgeZSKService {
	@Resource
	private KnowledgeZSKDao knowledgeZSKDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findKnowledgeZSKList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return knowledgeZSKDao.selectKnowledgeZSKList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(KnowledgeZSK i) throws Exception {

		knowledgeZSKDao.saveOrUpdate(i);

	}
	public KnowledgeZSK get(String id) {
		KnowledgeZSK knowledgeZSK = commonDAO.get(KnowledgeZSK.class, id);
		return knowledgeZSK;
	}
	public KnowledgeJDZSKResult get(KnowledgeJDZSKResult knowledgeJDZSKResult){
		KnowledgeJDZSKResult jd=commonDAO.get(KnowledgeJDZSKResult.class, knowledgeJDZSKResult.getKnowledge().getId());
		return jd;		
	}
	public Map<String, Object> findKnowledgeJDZSKResultList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = knowledgeZSKDao.selectKnowledgeJDZSKResultList(scId, startNum, limitNum, dir, sort);
		List<KnowledgeJDZSKResult> list = (List<KnowledgeJDZSKResult>) result.get("list");
		return result;
	}
	public Map<String, Object> findKnowledgeBXZLZSKList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = knowledgeZSKDao.selectKnowledgeBXZLZSKList(scId, startNum, limitNum, dir, sort);
		List<KnowledgeBXZLZSK> list = (List<KnowledgeBXZLZSK>) result.get("list");
		return result;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveKnowledgeJDZSKResult(KnowledgeZSK sc, String itemDataJson) throws Exception {
		List<KnowledgeJDZSKResult> saveItems = new ArrayList<KnowledgeJDZSKResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			KnowledgeJDZSKResult scp = new KnowledgeJDZSKResult();
			// 将map信息读入实体类
			scp = (KnowledgeJDZSKResult) knowledgeZSKDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setKnowledge(sc);

			saveItems.add(scp);
		}
		knowledgeZSKDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKnowledgeJDZSKResult(String[] ids) throws Exception {
		for (String id : ids) {
			KnowledgeJDZSKResult scp =  knowledgeZSKDao.get(KnowledgeJDZSKResult.class, id);
			 knowledgeZSKDao.delete(scp);
		}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveKnowledgeBXZLZSK(KnowledgeZSK sc, String itemDataJson) throws Exception {
		List<KnowledgeBXZLZSK> saveItems = new ArrayList<KnowledgeBXZLZSK>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			KnowledgeBXZLZSK scp = new KnowledgeBXZLZSK();
			// 将map信息读入实体类
			scp = (KnowledgeBXZLZSK) knowledgeZSKDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setKnowledge(sc);

			saveItems.add(scp);
		}
		knowledgeZSKDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKnowledgeBXZLZSK(String[] ids) throws Exception {
		for (String id : ids) {
			KnowledgeBXZLZSK scp =  knowledgeZSKDao.get(KnowledgeBXZLZSK.class, id);
			 knowledgeZSKDao.delete(scp);
		}
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(KnowledgeZSK sc, Map jsonMap) throws Exception {
		if (sc != null) {
			knowledgeZSKDao.saveOrUpdate(sc);
		
			String jsonStr = "";
			jsonStr = (String)jsonMap.get("knowledgeJDZSKResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveKnowledgeJDZSKResult(sc, jsonStr);
			}
			jsonStr = (String)jsonMap.get("knowledgeBXZLZSK");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveKnowledgeBXZLZSK(sc, jsonStr);
			}
	}
   }
}
