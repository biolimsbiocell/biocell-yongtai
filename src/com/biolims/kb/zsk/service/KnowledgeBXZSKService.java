package com.biolims.kb.zsk.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.kb.zsk.dao.KnowledgeBXZSKDao;
import com.biolims.kb.zsk.model.KnowledgeBXBXZLZSK;
import com.biolims.kb.zsk.model.KnowledgeBXJDZSKResult;
import com.biolims.kb.zsk.model.KnowledgeBXZSK;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class KnowledgeBXZSKService {
	@Resource
	private KnowledgeBXZSKDao knowledgeBXZSKDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findKnowledgeBXZSKList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return knowledgeBXZSKDao.selectKnowledgeBXZSKList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(KnowledgeBXZSK i) throws Exception {

		knowledgeBXZSKDao.saveOrUpdate(i);

	}
	public KnowledgeBXZSK get(String id) {
		KnowledgeBXZSK knowledgeBXZSK = commonDAO.get(KnowledgeBXZSK.class, id);
		return knowledgeBXZSK;
	}
	public KnowledgeBXJDZSKResult get(KnowledgeBXJDZSKResult knowledgeBXJDZSKResult){
		KnowledgeBXJDZSKResult jd=commonDAO.get(KnowledgeBXJDZSKResult.class, knowledgeBXJDZSKResult.getKnowledge().getId());
		return jd;		
	}
	public Map<String, Object> findKnowledgeBXJDZSKResultList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = knowledgeBXZSKDao.selectKnowledgeBXJDZSKResultList(scId, startNum, limitNum, dir, sort);
		List<KnowledgeBXJDZSKResult> list = (List<KnowledgeBXJDZSKResult>) result.get("list");
		return result;
	}
	public Map<String, Object> findKnowledgeBXBXZLZSKList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = knowledgeBXZSKDao.selectKnowledgeBXBXZLZSKList(scId, startNum, limitNum, dir, sort);
		List<KnowledgeBXBXZLZSK> list = (List<KnowledgeBXBXZLZSK>) result.get("list");
		return result;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveKnowledgeBXJDZSKResult(KnowledgeBXZSK sc, String itemDataJson) throws Exception {
		List<KnowledgeBXJDZSKResult> saveItems = new ArrayList<KnowledgeBXJDZSKResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			KnowledgeBXJDZSKResult scp = new KnowledgeBXJDZSKResult();
			// 将map信息读入实体类
			scp = (KnowledgeBXJDZSKResult) knowledgeBXZSKDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setKnowledge(sc);

			saveItems.add(scp);
		}
		knowledgeBXZSKDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKnowledgeBXJDZSKResult(String[] ids) throws Exception {
		for (String id : ids) {
			KnowledgeBXJDZSKResult scp =  knowledgeBXZSKDao.get(KnowledgeBXJDZSKResult.class, id);
			 knowledgeBXZSKDao.delete(scp);
		}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveKnowledgeBXBXZLZSK(KnowledgeBXZSK sc, String itemDataJson) throws Exception {
		List<KnowledgeBXBXZLZSK> saveItems = new ArrayList<KnowledgeBXBXZLZSK>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			KnowledgeBXBXZLZSK scp = new KnowledgeBXBXZLZSK();
			// 将map信息读入实体类
			scp = (KnowledgeBXBXZLZSK) knowledgeBXZSKDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setKnowledge(sc);

			saveItems.add(scp);
		}
		knowledgeBXZSKDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKnowledgeBXBXZLZSK(String[] ids) throws Exception {
		for (String id : ids) {
			KnowledgeBXBXZLZSK scp =  knowledgeBXZSKDao.get(KnowledgeBXBXZLZSK.class, id);
			 knowledgeBXZSKDao.delete(scp);
		}
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(KnowledgeBXZSK sc, Map jsonMap) throws Exception {
		if (sc != null) {
			knowledgeBXZSKDao.saveOrUpdate(sc);
		
			String jsonStr = "";
			jsonStr = (String)jsonMap.get("knowledgeBXJDZSKResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveKnowledgeBXJDZSKResult(sc, jsonStr);
			}
			jsonStr = (String)jsonMap.get("knowledgeBXBXZLZSK");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveKnowledgeBXBXZLZSK(sc, jsonStr);
			}
	}
   }
}
