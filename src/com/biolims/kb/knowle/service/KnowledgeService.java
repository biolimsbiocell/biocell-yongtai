package com.biolims.kb.knowle.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.kb.knowle.dao.KnowledgeDao;
import com.biolims.kb.knowle.model.Knowledge;
import com.biolims.kb.knowle.model.KnowledgeBXZL;
import com.biolims.kb.knowle.model.KnowledgeJDResult;
import com.biolims.kb.knowle.model.KnowledgeJYCPResult;
import com.biolims.kb.knowle.model.KnowledgeJYTBResult;
import com.biolims.kb.knowle.model.KnowledgeKBSJResult;
import com.biolims.kb.zsk.dao.KnowledgeBXZSKDao;
import com.biolims.kb.zsk.dao.KnowledgeZSKDao;
import com.biolims.kb.zsk.model.KnowledgeBXBXZLZSK;
import com.biolims.kb.zsk.model.KnowledgeBXJDZSKResult;
import com.biolims.kb.zsk.model.KnowledgeBXZSK;
import com.biolims.kb.zsk.model.KnowledgeJDZSKResult;
import com.biolims.kb.zsk.model.KnowledgeZSK;
import com.biolims.kb.zsk.service.KnowledgeZSKService;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.dao.DicSampleTypeDao;
import com.biolims.sample.dao.SampleOrderDao;
import com.biolims.sample.dao.SampleReceiveDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleReceiveItem;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class KnowledgeService {
	@Resource
	private KnowledgeDao knowledgeDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleReceiveDao sampleReceiveDao;
	@Resource
	private DicSampleTypeDao dicSampleTypeDao;
	@Resource
	private SampleOrderDao sampleOrderDao;
	@Resource
	private KnowledgeZSKDao knowledgeZSKDao;
	@Resource
	private KnowledgeBXZSKDao knowledgeBXZSKDao;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findKnowledgeList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return knowledgeDao.selectKnowledgeList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(Knowledge i) throws Exception {

		knowledgeDao.saveOrUpdate(i);

	}

	public Knowledge get(String id) {
		Knowledge knowledge = commonDAO.get(Knowledge.class, id);
		return knowledge;
	}

	public Map<String, Object> findKnowledgeJYTBResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = knowledgeDao
				.selectKnowledgeJYTBResultList(scId, startNum, limitNum, dir,
						sort);
		List<KnowledgeJYTBResult> list = (List<KnowledgeJYTBResult>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findKnowledgeJYCPResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = knowledgeDao
				.selectKnowledgeJYCPResultList(scId, startNum, limitNum, dir,
						sort);
		List<KnowledgeJYCPResult> list = (List<KnowledgeJYCPResult>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findKnowledgeKBSJResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = knowledgeDao
				.selectKnowledgeKBSJResultList(scId, startNum, limitNum, dir,
						sort);
		List<KnowledgeKBSJResult> list = (List<KnowledgeKBSJResult>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findKnowledgeJDResultList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = knowledgeDao.selectKnowledgeJDResultList(
				scId, startNum, limitNum, dir, sort);
		List<KnowledgeJDResult> list = (List<KnowledgeJDResult>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findKnowledgeBXZLList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = knowledgeDao.selectKnowledgeBXZLList(scId,
				startNum, limitNum, dir, sort);
		List<KnowledgeBXZL> list = (List<KnowledgeBXZL>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveKnowledgeJYTBResult(Knowledge sc, String itemDataJson)
			throws Exception {
		List<KnowledgeJYTBResult> saveItems = new ArrayList<KnowledgeJYTBResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			KnowledgeJYTBResult scp = new KnowledgeJYTBResult();
			// 将map信息读入实体类
			scp = (KnowledgeJYTBResult) knowledgeDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setKnowledge(sc);

			saveItems.add(scp);
		}
		knowledgeDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKnowledgeJYTBResult(String[] ids) throws Exception {
		for (String id : ids) {
			KnowledgeJYTBResult scp = knowledgeDao.get(
					KnowledgeJYTBResult.class, id);
			knowledgeDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveKnowledgeJYCPResult(Knowledge sc, String itemDataJson)
			throws Exception {
		List<KnowledgeJYCPResult> saveItems = new ArrayList<KnowledgeJYCPResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			KnowledgeJYCPResult scp = new KnowledgeJYCPResult();
			// 将map信息读入实体类
			scp = (KnowledgeJYCPResult) knowledgeDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setKnowledge(sc);

			saveItems.add(scp);
		}
		knowledgeDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKnowledgeJYCPResult(String[] ids) throws Exception {
		for (String id : ids) {
			KnowledgeJYCPResult scp = knowledgeDao.get(
					KnowledgeJYCPResult.class, id);
			knowledgeDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveKnowledgeKBSJResult(Knowledge sc, String itemDataJson)
			throws Exception {
		List<KnowledgeKBSJResult> saveItems = new ArrayList<KnowledgeKBSJResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			KnowledgeKBSJResult scp = new KnowledgeKBSJResult();
			// 将map信息读入实体类
			scp = (KnowledgeKBSJResult) knowledgeDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setKnowledge(sc);

			saveItems.add(scp);
		}
		knowledgeDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKnowledgeKBSJResult(String[] ids) throws Exception {
		for (String id : ids) {
			KnowledgeKBSJResult scp = knowledgeDao.get(
					KnowledgeKBSJResult.class, id);
			knowledgeDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveKnowledgeJDResult(Knowledge sc, String itemDataJson)
			throws Exception {
		List<KnowledgeJDResult> saveItems = new ArrayList<KnowledgeJDResult>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			KnowledgeJDResult scp = new KnowledgeJDResult();
			// 将map信息读入实体类
			scp = (KnowledgeJDResult) knowledgeDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setKnowledge(sc);

			saveItems.add(scp);
		}
		for (int i = 0; i < list.size(); i++) {
			/*
			 * KnowledgeJDZSKResult jdzskResult=new KnowledgeJDZSKResult();
			 * jdzskResult.setGene(list.get(i).get("gene").toString());
			 */
			User user = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			KnowledgeZSK zsk = new KnowledgeZSK();
			if (knowledgeZSKDao
					.isExist(String.valueOf(list.get(i).get("gene"))) == null) {

				zsk.setGeneName(list.get(i).get("gene") == null ? "" : list
						.get(i).get("gene").toString());
				zsk.setMutationType(list.get(i).get("tbType") == null ? ""
						: list.get(i).get("tbType").toString());
				zsk.setCreateDate(new Date());
				zsk.setCreateUser(user);
				knowledgeZSKDao.saveOrUpdate(zsk);

				KnowledgeJDZSKResult jdzskResult = new KnowledgeJDZSKResult();
				jdzskResult.setGene(list.get(i).get("gene") == null ? "" : list
						.get(i).get("gene").toString());
				jdzskResult.setTbType(list.get(i).get("tbType") == null ? ""
						: list.get(i).get("tbType").toString());
				jdzskResult
						.setJdResult(list.get(i).get("jdResult") == null ? ""
								: list.get(i).get("jdResult").toString());
				jdzskResult.setCreateDate(new Date());
				jdzskResult.setCreateUser(user);
				jdzskResult.setIsValid("1");
				jdzskResult.setVersion("1");
				KnowledgeZSK knowledge = new KnowledgeZSK();
				knowledge.setId(zsk.getId());
				jdzskResult.setKnowledge(knowledge);
				knowledgeZSKDao.saveOrUpdate(jdzskResult);
			} else {
				KnowledgeJDZSKResult jdzskResult = new KnowledgeJDZSKResult();
				jdzskResult.setGene(list.get(i).get("gene") == null ? "" : list
						.get(i).get("gene").toString());
				jdzskResult.setTbType(list.get(i).get("tbType") == null ? ""
						: list.get(i).get("tbType").toString());
				jdzskResult
						.setJdResult(list.get(i).get("jdResult") == null ? ""
								: list.get(i).get("jdResult").toString());
				jdzskResult.setCreateDate(new Date());
				jdzskResult.setCreateUser(user);
				jdzskResult.setIsValid("2");
				KnowledgeZSK knowledge = new KnowledgeZSK();
				knowledge.setId(knowledgeZSKDao
						.isExist(String.valueOf(list.get(i).get("gene")))
						.get(i).getKnowledge().getId());
				jdzskResult.setKnowledge(knowledge);
				jdzskResult.setVersion(knowledgeZSKDao.isCount(
						String.valueOf(list.get(i).get("gene"))).toString());
				knowledgeZSKDao.saveOrUpdate(jdzskResult);
			}
			System.out.println("zzzzzzzzzzzzzzzzzzzz" + zsk.getId());
		}
		knowledgeDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKnowledgeJDResult(String[] ids) throws Exception {
		for (String id : ids) {
			KnowledgeJDResult scp = knowledgeDao.get(KnowledgeJDResult.class,
					id);
			knowledgeDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveKnowledgeBXZL(Knowledge sc, String itemDataJson)
			throws Exception {
		List<KnowledgeBXZL> saveItems = new ArrayList<KnowledgeBXZL>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			KnowledgeBXZL scp = new KnowledgeBXZL();
			// 将map信息读入实体类
			scp = (KnowledgeBXZL) knowledgeDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setKnowledge(sc);

			saveItems.add(scp);
		}
		for (int i = 0; i < list.size(); i++) {
			User user = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			KnowledgeBXZSK zsk = new KnowledgeBXZSK();
			if (knowledgeBXZSKDao.isExist(String.valueOf(list.get(i).get(
					"ywName"))) == null) {
				// bxbxzlzsk.setState(list.get(i).get("state")==null?"":list.get(i).get("state").toString());
				zsk.setDrugName(list.get(i).get("ywName") == null ? "" : list
						.get(i).get("ywName").toString());
				zsk.setGoodName(list.get(i).get("spName") == null ? "" : list
						.get(i).get("spName").toString());
				zsk.setSpot(list.get(i).get("spot") == null ? "" : list.get(i)
						.get("spot").toString());
				zsk.setCreateDate(new Date());
				zsk.setCreateUser(user);
				knowledgeBXZSKDao.saveOrUpdate(zsk);
				KnowledgeBXBXZLZSK bxbxzlzsk = new KnowledgeBXBXZLZSK();
				bxbxzlzsk.setSpName(list.get(i).get("spName") == null ? ""
						: list.get(i).get("spName").toString());
				bxbxzlzsk.setYwName(list.get(i).get("ywName") == null ? ""
						: list.get(i).get("ywName").toString());
				bxbxzlzsk.setSpot(list.get(i).get("spot") == null ? "" : list
						.get(i).get("spot").toString());
				bxbxzlzsk.setState(list.get(i).get("state") == null ? "" : list
						.get(i).get("state").toString());
				bxbxzlzsk.setCreateDate(new Date());
				bxbxzlzsk.setCreateUser(user);
				bxbxzlzsk.setIsValid("1");
				KnowledgeBXZSK knowledge = new KnowledgeBXZSK();
				knowledge.setId(zsk.getId());
				bxbxzlzsk.setKnowledge(knowledge);
				bxbxzlzsk.setVersion("1");
				knowledgeBXZSKDao.saveOrUpdate(bxbxzlzsk);
			} else {
				KnowledgeBXBXZLZSK bxbxzlzsk = new KnowledgeBXBXZLZSK();
				bxbxzlzsk.setSpName(list.get(i).get("spName") == null ? ""
						: list.get(i).get("spName").toString());
				bxbxzlzsk.setYwName(list.get(i).get("ywName") == null ? ""
						: list.get(i).get("ywName").toString());
				bxbxzlzsk.setSpot(list.get(i).get("spot") == null ? "" : list
						.get(i).get("spot").toString());
				bxbxzlzsk.setState(list.get(i).get("state") == null ? "" : list
						.get(i).get("state").toString());
				bxbxzlzsk.setCreateDate(new Date());
				bxbxzlzsk.setCreateUser(user);
				bxbxzlzsk.setIsValid("2");
				KnowledgeBXZSK knowledge = new KnowledgeBXZSK();
				knowledge.setId(knowledgeBXZSKDao
						.isExist(String.valueOf(list.get(i).get("ywName")))
						.get(i).getKnowledge().getId());
				bxbxzlzsk.setKnowledge(knowledge);
				bxbxzlzsk.setVersion(knowledgeBXZSKDao.isCount(
						String.valueOf(list.get(i).get("ywName"))).toString());
				knowledgeBXZSKDao.saveOrUpdate(bxbxzlzsk);
			}
			System.out.println("zzzzzzzzzzzzzzzzzzzz" + zsk.getId());
		}
		knowledgeDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delKnowledgeBXZL(String[] ids) throws Exception {
		for (String id : ids) {
			KnowledgeBXZL scp = knowledgeDao.get(KnowledgeBXZL.class, id);
			knowledgeDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(Knowledge sc, Map jsonMap) throws Exception {
		if (sc != null) {
			// SampleOrder so=sampleOrderDao.get(SampleOrder.class, sc.getId());
			// SampleReceiveItem
			// sri=sampleReceiveDao.selectItemByOrderCode(sc.getId());
			// if(so!=null){
			// if(sri!=null){
			// /*sc.setProductId(sri.getProductId());
			// sc.setProductName(sri.getProductName());*/
			// sc.setProductId(sri.getProductId());
			// sc.setProductName(sri.getProductName());
			// sc.setPatientId(so.getVisitId());
			// sc.setSampleCode(sri.getSampleCode());
			// DicSampleType
			// sampleType=dicSampleTypeDao.selectDicSampleTypeByName(so.getSampleTypeName().replace(",",
			// ""));
			// sc.setSampleType(sampleType);
			// }
			// }
			knowledgeDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("knowledgeJYTBResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveKnowledgeJYTBResult(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("knowledgeJYCPResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveKnowledgeJYCPResult(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("knowledgeKBSJResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveKnowledgeKBSJResult(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("knowledgeJDResult");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveKnowledgeJDResult(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("knowledgeBXZL");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveKnowledgeBXZL(sc, jsonStr);
			}
		}
	}
}
