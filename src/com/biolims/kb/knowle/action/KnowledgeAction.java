package com.biolims.kb.knowle.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.kb.knowle.model.Knowledge;
import com.biolims.kb.knowle.model.KnowledgeBXZL;
import com.biolims.kb.knowle.model.KnowledgeJDResult;
import com.biolims.kb.knowle.model.KnowledgeJYCPResult;
import com.biolims.kb.knowle.model.KnowledgeJYTBResult;
import com.biolims.kb.knowle.model.KnowledgeKBSJResult;
import com.biolims.kb.knowle.service.KnowledgeService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/kb/knowle/knowledge")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class KnowledgeAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "260110";
	@Autowired
	private KnowledgeService knowledgeService;
	private Knowledge knowledge = new Knowledge();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;

	@Action(value = "showKnowledgeList")
	public String showKnowledgeList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/kb/knowle/knowledge.jsp");
	}

	@Action(value = "showKnowledgeListJson")
	public void showKnowledgeListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = knowledgeService.findKnowledgeList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<Knowledge> list = (List<Knowledge>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("patientId", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("sampleType-id", "");
		map.put("sampleType-name", "");
		map.put("sampleCode", "");
		map.put("product-id", "");
		map.put("product-name", "");
		// map.put("joinlab", "");
		map.put("path", "");
		map.put("productId", "");
		map.put("productName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "knowledgeSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogKnowledgeList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/kb/knowle/knowledgeDialog.jsp");
	}

	@Action(value = "showDialogKnowledgeListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogKnowledgeListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = knowledgeService.findKnowledgeList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<Knowledge> list = (List<Knowledge>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("patientId", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("sampleType-id", "");
		map.put("sampleType-name", "");
		map.put("product-id", "");
		map.put("product-name", "");
		// map.put("joinlab", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editKnowledge")
	public String editKnowledge() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			knowledge = knowledgeService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "knowledge");
		} else {
			knowledge.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			knowledge.setCreateUser(user);
			knowledge.setCreateDate(new Date());
			/*
			 * knowledge.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			 * knowledge.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			 */
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/kb/knowle/knowledgeEdit.jsp");
	}

	@Action(value = "copyKnowledge")
	public String copyKnowledge() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		knowledge = knowledgeService.get(id);
		knowledge.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/kb/knowle/knowledgeEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = knowledge.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "Knowledge";
			String markCode = "KN";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			knowledge.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("knowledgeJYTBResult",
				getParameterFromRequest("knowledgeJYTBResultJson"));

		aMap.put("knowledgeJYCPResult",
				getParameterFromRequest("knowledgeJYCPResultJson"));

		aMap.put("knowledgeKBSJResult",
				getParameterFromRequest("knowledgeKBSJResultJson"));

		aMap.put("knowledgeJDResult",
				getParameterFromRequest("knowledgeJDResultJson"));

		aMap.put("knowledgeBXZL", getParameterFromRequest("knowledgeBXZLJson"));

		knowledgeService.save(knowledge, aMap);
		return redirect("/kb/knowle/knowledge/editKnowledge.action?id="
				+ knowledge.getId());

	}

	@Action(value = "viewKnowledge")
	public String toViewKnowledge() throws Exception {
		String id = getParameterFromRequest("id");
		knowledge = knowledgeService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/kb/knowle/knowledgeEdit.jsp");
	}

	@Action(value = "showKnowledgeJYTBResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showKnowledgeJYTBResultList() throws Exception {
		return dispatcher("/WEB-INF/page/kb/knowle/knowledgeJYTBResult.jsp");
	}

	@Action(value = "showKnowledgeJYTBResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showKnowledgeJYTBResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = knowledgeService
					.findKnowledgeJYTBResultList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<KnowledgeJYTBResult> list = (List<KnowledgeJYTBResult>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("type", "");
			map.put("scale", "");
			map.put("changeHgs", "");
			map.put("changeAjs", "");
			map.put("chromosome", "");
			map.put("zlCode", "");
			map.put("knowledge-name", "");
			map.put("knowledge-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKnowledgeJYTBResult")
	public void delKnowledgeJYTBResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			knowledgeService.delKnowledgeJYTBResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showKnowledgeJYCPResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showKnowledgeJYCPResultList() throws Exception {
		return dispatcher("/WEB-INF/page/kb/knowle/knowledgeJYCPResult.jsp");
	}

	@Action(value = "showKnowledgeJYCPResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showKnowledgeJYCPResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = knowledgeService
					.findKnowledgeJYCPResultList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<KnowledgeJYCPResult> list = (List<KnowledgeJYCPResult>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("gene1", "");
			map.put("gene1Chromosome", "");
			map.put("gene1Position", "");
			map.put("gene2", "");
			map.put("gene2Chromosome", "");
			map.put("gene2Position", "");
			map.put("structType", "");
			map.put("knowledge-name", "");
			map.put("knowledge-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKnowledgeJYCPResult")
	public void delKnowledgeJYCPResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			knowledgeService.delKnowledgeJYCPResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showKnowledgeKBSJResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showKnowledgeKBSJResultList() throws Exception {
		return dispatcher("/WEB-INF/page/kb/knowle/knowledgeKBSJResult.jsp");
	}

	@Action(value = "showKnowledgeKBSJResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showKnowledgeKBSJResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = knowledgeService
					.findKnowledgeKBSJResultList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<KnowledgeKBSJResult> list = (List<KnowledgeKBSJResult>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("gene", "");
			map.put("chromosome", "");
			map.put("cpStartP", "");
			map.put("cpEndP", "");
			map.put("changeMultiple", "");
			map.put("changeType", "");
			map.put("knowledge-name", "");
			map.put("knowledge-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKnowledgeKBSJResult")
	public void delKnowledgeKBSJResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			knowledgeService.delKnowledgeKBSJResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showKnowledgeJDResultList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showKnowledgeJDResultList() throws Exception {
		return dispatcher("/WEB-INF/page/kb/knowle/knowledgeJDResult.jsp");
	}

	@Action(value = "showKnowledgeJDResultListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showKnowledgeJDResultListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = knowledgeService
					.findKnowledgeJDResultList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<KnowledgeJDResult> list = (List<KnowledgeJDResult>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("gene", "");
			map.put("tbType", "");
			map.put("jdResult", "");
			map.put("knowledge-name", "");
			map.put("knowledge-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKnowledgeJDResult")
	public void delKnowledgeJDResult() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			knowledgeService.delKnowledgeJDResult(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showKnowledgeBXZLList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showKnowledgeBXZLList() throws Exception {
		return dispatcher("/WEB-INF/page/kb/knowle/knowledgeBXZL.jsp");
	}

	@Action(value = "showKnowledgeBXZLListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showKnowledgeBXZLListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = knowledgeService
					.findKnowledgeBXZLList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<KnowledgeBXZL> list = (List<KnowledgeBXZL>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("ywName", "");
			map.put("spName", "");
			map.put("spot", "");
			map.put("state", "");
			map.put("knowledge-name", "");
			map.put("knowledge-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delKnowledgeBXZL")
	public void delKnowledgeBXZL() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			knowledgeService.delKnowledgeBXZL(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public KnowledgeService getKnowledgeService() {
		return knowledgeService;
	}

	public void setKnowledgeService(KnowledgeService knowledgeService) {
		this.knowledgeService = knowledgeService;
	}

	public Knowledge getKnowledge() {
		return knowledge;
	}

	public void setKnowledge(Knowledge knowledge) {
		this.knowledge = knowledge;
	}

}
