package com.biolims.kb.knowle.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 基因突变分析结果
 * @author lims-platform
 * @date 2016-07-07 18:43:36
 * @version V1.0   
 *
 */
@Entity
@Table(name = "KNOWLEDGE_JYTB_RESULT")
@SuppressWarnings("serial")
public class KnowledgeJYTBResult extends EntityDao<KnowledgeJYTBResult> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**基因名称*/
	private String name;
	/**突变类型*/
	private String type;
	/**突变比例*/
	private String scale;
	/**核苷酸变化*/
	private String changeHgs;
	/**氨基酸变化*/
	private String changeAjs;
	/**染色体*/
	private String chromosome;
	/**转录本编号*/
	private String zlCode;
	/**相关主表*/
	private Knowledge knowledge;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  基因名称
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  基因名称
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  突变类型
	 */
	@Column(name ="TYPE", length = 50)
	public String getType(){
		return this.type;
	}
	/**
	 *方法: 设置String
	 *@param: String  突变类型
	 */
	public void setType(String type){
		this.type = type;
	}
	/**
	 *方法: 取得String
	 *@return: String  突变比例
	 */
	@Column(name ="SCALE", length = 50)
	public String getScale(){
		return this.scale;
	}
	/**
	 *方法: 设置String
	 *@param: String  突变比例
	 */
	public void setScale(String scale){
		this.scale = scale;
	}
	/**
	 *方法: 取得String
	 *@return: String  核苷酸变化
	 */
	@Column(name ="CHANGE_HGS", length = 50)
	public String getChangeHgs(){
		return this.changeHgs;
	}
	/**
	 *方法: 设置String
	 *@param: String  核苷酸变化
	 */
	public void setChangeHgs(String changeHgs){
		this.changeHgs = changeHgs;
	}
	/**
	 *方法: 取得String
	 *@return: String  氨基酸变化
	 */
	@Column(name ="CHANGE_AJS", length = 50)
	public String getChangeAjs(){
		return this.changeAjs;
	}
	/**
	 *方法: 设置String
	 *@param: String  氨基酸变化
	 */
	public void setChangeAjs(String changeAjs){
		this.changeAjs = changeAjs;
	}
	/**
	 *方法: 取得String
	 *@return: String  染色体
	 */
	@Column(name ="CHROMOSOME", length = 50)
	public String getChromosome(){
		return this.chromosome;
	}
	/**
	 *方法: 设置String
	 *@param: String  染色体
	 */
	public void setChromosome(String chromosome){
		this.chromosome = chromosome;
	}
	/**
	 *方法: 取得String
	 *@return: String  转录本编号
	 */
	@Column(name ="ZL_CODE", length = 50)
	public String getZlCode(){
		return this.zlCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  转录本编号
	 */
	public void setZlCode(String zlCode){
		this.zlCode = zlCode;
	}
	/**
	 *方法: 取得Knowledge
	 *@return: Knowledge  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "KNOWLEDGE")
	public Knowledge getKnowledge(){
		return this.knowledge;
	}
	/**
	 *方法: 设置Knowledge
	 *@param: Knowledge  相关主表
	 */
	public void setKnowledge(Knowledge knowledge){
		this.knowledge = knowledge;
	}
}