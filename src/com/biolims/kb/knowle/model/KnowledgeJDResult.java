package com.biolims.kb.knowle.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 解读结果
 * @author lims-platform
 * @date 2016-07-07 18:43:41
 * @version V1.0   
 *
 */
@Entity
@Table(name = "KNOWLEDGE_JD_RESULT")
@SuppressWarnings("serial")
public class KnowledgeJDResult extends EntityDao<KnowledgeJDResult> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**基因*/
	private String gene;
	/**突变类型*/
	private String tbType;
	/**解读*/
	private String jdResult;
	/**相关主表*/
	private Knowledge knowledge;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  基因
	 */
	@Column(name ="GENE", length = 255)
	public String getGene(){
		return this.gene;
	}
	/**
	 *方法: 设置String
	 *@param: String  基因
	 */
	public void setGene(String gene){
		this.gene = gene;
	}
	/**
	 *方法: 取得String
	 *@return: String  突变类型
	 */
	@Column(name ="TB_TYPE", length = 255)
	public String getTbType(){
		return this.tbType;
	}
	/**
	 *方法: 设置String
	 *@param: String  突变类型
	 */
	public void setTbType(String tbType){
		this.tbType = tbType;
	}
	/**
	 *方法: 取得String
	 *@return: String  解读
	 */
	@Column(name ="JD_RESULT", length = 255)
	public String getJdResult(){
		return this.jdResult;
	}
	/**
	 *方法: 设置String
	 *@param: String  解读
	 */
	public void setJdResult(String jdResult){
		this.jdResult = jdResult;
	}
	/**
	 *方法: 取得Knowledge
	 *@return: Knowledge  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "KNOWLEDGE")
	public Knowledge getKnowledge(){
		return this.knowledge;
	}
	/**
	 *方法: 设置Knowledge
	 *@param: Knowledge  相关主表
	 */
	public void setKnowledge(Knowledge knowledge){
		this.knowledge = knowledge;
	}
}