package com.biolims.kb.knowle.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 基因重排分析结果
 * @author lims-platform
 * @date 2016-07-07 18:43:37
 * @version V1.0   
 *
 */
@Entity
@Table(name = "KNOWLEDGE_JYCP_RESULT")
@SuppressWarnings("serial")
public class KnowledgeJYCPResult extends EntityDao<KnowledgeJYCPResult> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**基因1*/
	private String gene1;
	/**基因1位于的染色体*/
	private String gene1Chromosome;
	/**基因1的断点位置*/
	private String gene1Position;
	/**基因2*/
	private String gene2;
	/**基因2位于的染色体*/
	private String gene2Chromosome;
	/**基因2的断点位置*/
	private String gene2Position;
	/**结构变化类型*/
	private String structType;
	/**相关主表*/
	private Knowledge knowledge;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  基因1
	 */
	@Column(name ="GENE_1", length = 255)
	public String getGene1(){
		return this.gene1;
	}
	/**
	 *方法: 设置String
	 *@param: String  基因1
	 */
	public void setGene1(String gene1){
		this.gene1 = gene1;
	}
	/**
	 *方法: 取得String
	 *@return: String  基因1位于的染色体
	 */
	@Column(name ="GENE_1_CHROMOSOME", length = 255)
	public String getGene1Chromosome(){
		return this.gene1Chromosome;
	}
	/**
	 *方法: 设置String
	 *@param: String  基因1位于的染色体
	 */
	public void setGene1Chromosome(String gene1Chromosome){
		this.gene1Chromosome = gene1Chromosome;
	}
	/**
	 *方法: 取得String
	 *@return: String  基因1的断点位置
	 */
	@Column(name ="GENE_1_POSITION", length = 255)
	public String getGene1Position(){
		return this.gene1Position;
	}
	/**
	 *方法: 设置String
	 *@param: String  基因1的断点位置
	 */
	public void setGene1Position(String gene1Position){
		this.gene1Position = gene1Position;
	}
	/**
	 *方法: 取得String
	 *@return: String  基因2
	 */
	@Column(name ="GENE_2", length = 255)
	public String getGene2(){
		return this.gene2;
	}
	/**
	 *方法: 设置String
	 *@param: String  基因2
	 */
	public void setGene2(String gene2){
		this.gene2 = gene2;
	}
	/**
	 *方法: 取得String
	 *@return: String  基因2位于的染色体
	 */
	@Column(name ="GENE_2_CHROMOSOME", length = 255)
	public String getGene2Chromosome(){
		return this.gene2Chromosome;
	}
	/**
	 *方法: 设置String
	 *@param: String  基因2位于的染色体
	 */
	public void setGene2Chromosome(String gene2Chromosome){
		this.gene2Chromosome = gene2Chromosome;
	}
	/**
	 *方法: 取得String
	 *@return: String  基因2的断点位置
	 */
	@Column(name ="GENE_2_POSITION", length = 255)
	public String getGene2Position(){
		return this.gene2Position;
	}
	/**
	 *方法: 设置String
	 *@param: String  基因2的断点位置
	 */
	public void setGene2Position(String gene2Position){
		this.gene2Position = gene2Position;
	}
	/**
	 *方法: 取得String
	 *@return: String  结构变化类型
	 */
	@Column(name ="STRUCT_TYPE", length = 255)
	public String getStructType(){
		return this.structType;
	}
	/**
	 *方法: 设置String
	 *@param: String  结构变化类型
	 */
	public void setStructType(String structType){
		this.structType = structType;
	}
	/**
	 *方法: 取得Knowledge
	 *@return: Knowledge  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "KNOWLEDGE")
	public Knowledge getKnowledge(){
		return this.knowledge;
	}
	/**
	 *方法: 设置Knowledge
	 *@param: Knowledge  相关主表
	 */
	public void setKnowledge(Knowledge knowledge){
		this.knowledge = knowledge;
	}
}