package com.biolims.kb.knowle.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 拷贝数据分析结果
 * @author lims-platform
 * @date 2016-07-07 18:43:39
 * @version V1.0   
 *
 */
@Entity
@Table(name = "KNOWLEDGE_KBSJ_RESULT")
@SuppressWarnings("serial")
public class KnowledgeKBSJResult extends EntityDao<KnowledgeKBSJResult> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**基因*/
	private String gene;
	/**染色体*/
	private String chromosome;
	/**拷贝改变起始位置*/
	private String cpStartP;
	/**拷贝改变终止位置*/
	private String cpEndP;
	/**改变倍数*/
	private String changeMultiple;
	/**改变类型*/
	private String changeType;
	/**相关主表*/
	private Knowledge knowledge;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  基因
	 */
	@Column(name ="GENE", length = 255)
	public String getGene(){
		return this.gene;
	}
	/**
	 *方法: 设置String
	 *@param: String  基因
	 */
	public void setGene(String gene){
		this.gene = gene;
	}
	/**
	 *方法: 取得String
	 *@return: String  染色体
	 */
	@Column(name ="CHROMOSOME", length = 255)
	public String getChromosome(){
		return this.chromosome;
	}
	/**
	 *方法: 设置String
	 *@param: String  染色体
	 */
	public void setChromosome(String chromosome){
		this.chromosome = chromosome;
	}
	/**
	 *方法: 取得String
	 *@return: String  拷贝改变起始位置
	 */
	@Column(name ="CP_START_P", length = 255)
	public String getCpStartP(){
		return this.cpStartP;
	}
	/**
	 *方法: 设置String
	 *@param: String  拷贝改变起始位置
	 */
	public void setCpStartP(String cpStartP){
		this.cpStartP = cpStartP;
	}
	/**
	 *方法: 取得String
	 *@return: String  拷贝改变终止位置
	 */
	@Column(name ="CP_END_P", length = 255)
	public String getCpEndP(){
		return this.cpEndP;
	}
	/**
	 *方法: 设置String
	 *@param: String  拷贝改变终止位置
	 */
	public void setCpEndP(String cpEndP){
		this.cpEndP = cpEndP;
	}
	/**
	 *方法: 取得String
	 *@return: String  改变倍数
	 */
	@Column(name ="CHANGE_MULTIPLE", length = 255)
	public String getChangeMultiple(){
		return this.changeMultiple;
	}
	/**
	 *方法: 设置String
	 *@param: String  改变倍数
	 */
	public void setChangeMultiple(String changeMultiple){
		this.changeMultiple = changeMultiple;
	}
	/**
	 *方法: 取得String
	 *@return: String  改变类型
	 */
	@Column(name ="CHANGE_TYPE", length = 255)
	public String getChangeType(){
		return this.changeType;
	}
	/**
	 *方法: 设置String
	 *@param: String  改变类型
	 */
	public void setChangeType(String changeType){
		this.changeType = changeType;
	}
	/**
	 *方法: 取得Knowledge
	 *@return: Knowledge  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "KNOWLEDGE")
	public Knowledge getKnowledge(){
		return this.knowledge;
	}
	/**
	 *方法: 设置Knowledge
	 *@param: Knowledge  相关主表
	 */
	public void setKnowledge(Knowledge knowledge){
		this.knowledge = knowledge;
	}
}