package com.biolims.kb.knowle.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.system.product.model.Product;

/**
 * @Title: Model
 * @Description: 知识库
 * @author lims-platform
 * @date 2016-07-07 18:43:51
 * @version V1.0
 * 
 */
@Entity
@Table(name = "KNOWLEDGE")
@SuppressWarnings("serial")
public class Knowledge extends EntityDao<Knowledge> implements
		java.io.Serializable {
	/** 检测申请号 */
	private String id;
	private String name;
	/** 病案号 */
	private String patientId;
	/** 上传人 */
	private User createUser;
	/** 上传时间 */
	private Date createDate;
	/** 样本类型 */
	private DicSampleType sampleType;
	/** 检测项目 */
	private Product product;
	// /**合作实验室*/
	// private String joinlab;
	// 样本编号
	private String sampleCode;
	/** 附件路径 */
	private String path;
	/** 检测项目 */
	private String productId;
	/** 检测项目名称 */
	private String productName;

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name = "PATH", length = 225)
	public String getPath() {
		return this.path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	@Column(name = "NAME", length = 50)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "SAMPLE_CODE", length = 100)
	public String getSampleCode() {
		return this.sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 检测申请号
	 */

	@Id
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 检测申请号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 病案号
	 */
	@Column(name = "PATIENT_ID", length = 50)
	public String getPatientId() {
		return this.patientId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 病案号
	 */
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 上传人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 上传人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 上传时间
	 */
	@Column(name = "CREATE_DATE", length = 50)
	public Date getCreateDate() {
		return this.createDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 上传时间
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 方法: 取得DicSampleType
	 * 
	 * @return: DicSampleType 样本类型
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_TYPE")
	public DicSampleType getSampleType() {
		return this.sampleType;
	}

	/**
	 * 方法: 设置DicSampleType
	 * 
	 * @param: DicSampleType 样本类型
	 */
	public void setSampleType(DicSampleType sampleType) {
		this.sampleType = sampleType;
	}

	/**
	 * 方法: 取得Product
	 * 
	 * @return: Product 检测项目
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PRODUCE")
	public Product getProduct() {
		return this.product;
	}

	/**
	 * 方法: 设置Product
	 * 
	 * @param: Product 检测项目
	 */
	public void setProduct(Product product) {
		this.product = product;
	}

	// /**
	// * 方法: 取得String
	// *
	// * @return: String 合作实验室
	// */
	// @Column(name = "JOINLAB", length = 50)
	// public String getJoinlab() {
	// return this.joinlab;
	// }
	//
	// /**
	// * 方法: 设置String
	// *
	// * @param: String 合作实验室
	// */
	// public void setJoinlab(String joinlab) {
	// this.joinlab = joinlab;
	// }

}