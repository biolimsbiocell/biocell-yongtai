/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：Action
 * 类描述：供应商管理
 * 创建人：倪毅
 * 创建时间：2011-11
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.supplier.common.action;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.storage.model.DicStorageType;
import com.biolims.supplier.common.constants.SystemConstants;
import com.biolims.supplier.common.service.SupplierCommonService;
import com.biolims.supplier.model.Supplier;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/supplier/common")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public class SupplierCommonAction extends BaseActionSupport {

	private static final long serialVersionUID = -164661537469123056L;

	@Autowired
	private SupplierCommonService supplierCommonService;

	/**
	 * 访问客户列表
	 */
	@Action(value = "supplierSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSupplierList() throws Exception {
		// 用于接收通用查询的参数
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		map.put("id", new String[] { "", "string", "", "供应商编码", "120", "true", "", "", "", "", "", "" });
		map.put("name", new String[] { "", "string", "", "供应商名称", "200", "true", "", "", "", "", "", "" });
		map.put("way", new String[] { "", "string", "", "行业", "120", "true", "", "", "", "", "", "" });
		map.put("supplierType-name", new String[] { "", "string", "", "类型", "150", "true", "", "", "", "", "", "" });
		map.put("entKind-name", new String[] { "", "string", "", "企业性质", "150", "true", "true", "", "", "", "", "" });
		map.put("selA-name", new String[] { "", "string", "", "国家", "100", "true", "", "", "", "", "", "" });
		map.put("linkMan", new String[] { "", "string", "", "联系人", "100", "true", "", "", "", "", "", "" });
		map.put("linkTel", new String[] { "", "string", "", "联系电话", "100", "true", "", "", "", "", "", "" });
		map.put("fax", new String[] { "", "string", "", "传真", "100", "true", "", "", "", "", "", "" });
		map.put("email", new String[] { "", "string", "", "Email", "100", "true", "", "", "", "", "", "" });
		String type = generalexttype(map);
		String col = generalextcol(map);
		putObjToContext("type", type);
		putObjToContext("col", col);
		putObjToContext("supplierTypeId", getParameterFromRequest("id"));
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		putObjToContext("path", ServletActionContext.getRequest().getContextPath()
				+ "/supplier/common/supplierSelectJson.action?id=" + getParameterFromRequest("id"));
		return dispatcher("/WEB-INF/page/supplier/common/supplierSelect.jsp");
	}

	@Action(value = "supplierSelectJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSupplierListJson() throws Exception {
		//开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		//limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		//字段
		String dir = getParameterFromRequest("dir");
		//排序方式
		String sort = getParameterFromRequest("sort");
		//取出session中检索用的对象
		String data = getParameterFromRequest("data");
		String supplierTypeId = getParameterFromRequest("id");
		/*Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		if (supplierTypeId != null) {
			map2Query.put("supplierType.id", supplierTypeId);

		}*/
		Map<String, Object> controlMap = supplierCommonService.findSupplierCustomerList(startNum, limitNum, dir, sort,
				getContextPath(), data, SystemConstants.DIC_SUPPLIER_TYPE, supplierTypeId);
		long totalCount = Long.parseLong(controlMap.get("totalCount").toString());
		List<Supplier> list = (List<Supplier>) controlMap.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("way", "");
		map.put("entKind-name", "");
		map.put("supplierType-name", "");
		map.put("selA-name", "");
		map.put("address1", "");
		map.put("linkMan", "");
		map.put("linkTel", "");
		map.put("fax", "");
		map.put("email", "");
		new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}
	
	@Action(value = "supplierSelectTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String supplierSelectTable() throws Exception {
		String flag = getParameterFromRequest("flag");
		putObjToContext("flag", flag);
		
		return dispatcher("/WEB-INF/page/supplier/common/supplierSelectTable.jsp");
	}
	
	@Action(value="supplierSelectTableJson")
	public void supplierSelectTableJson()throws Exception{
		String flag=getParameterFromRequest("flag");
		String query=getParameterFromRequest("query");
		String colNum=getParameterFromRequest("order[0][column]");
		String col=getParameterFromRequest("columns["+colNum+"][data]");
		String sort=getParameterFromRequest("order[0][dir]");
		Integer start=Integer.valueOf(getParameterFromRequest("start"));
		Integer length=Integer.valueOf(getParameterFromRequest("length"));
		String draw=getParameterFromRequest("draw");
		Map<String,Object> result=supplierCommonService.supplierSelectTableJson(start,length,query,col,sort,flag);
		List<Supplier> list = (List<Supplier>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}
	

	/**
	 * 访问客户列表
	 */

	@Action(value = "producerSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCustomerList() throws Exception {
		// 用于接收通用查询的参数
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		map.put("id", new String[] { "", "string", "", "生产商编码", "120", "true", "", "", "", "", "", "" });
		map.put("name", new String[] { "", "string", "", "名称", "200", "true", "", "", "", "", "", "" });
		map.put("way", new String[] { "", "string", "", "行业", "120", "true", "", "", "", "", "", "" });
		map.put("entKind-name", new String[] { "", "string", "", "企业性质", "150", "true", "", "", "", "", "", "" });
		map.put("address1", new String[] { "", "string", "", "国家", "100", "true", "", "", "", "", "", "" });
		map.put("linkMan", new String[] { "", "string", "", "联系人", "100", "true", "", "", "", "", "", "" });
		map.put("linkTel", new String[] { "", "string", "", "联系电话", "100", "true", "", "", "", "", "", "" });
		map.put("fax", new String[] { "", "string", "", "传真", "100", "true", "", "", "", "", "", "" });
		map.put("email", new String[] { "", "string", "", "Email", "100", "true", "", "", "", "", "", "" });
		//	map.put("state", new String[]{"","string","","状态","100","true","","","","","",""});

		String type = generalexttype(map);
		String col = generalextcol(map);
		// 用于ext显示
		putObjToContext("type", type);
		putObjToContext("col", col);
		// 用于判断当前页面类型,LIST类型
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		putObjToContext("path", ServletActionContext.getRequest().getContextPath()
				+ "/supplier/common/producerSelectJson.action");
		//导向jsp页面显示
		return dispatcher("/WEB-INF/page/supplier/common/supplierSelect.jsp");
	}

	@Action(value = "producerSelectJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showCustomerListJson() throws Exception {
		//开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		//limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		//字段
		String dir = getParameterFromRequest("dir");
		//排序方式
		String sort = getParameterFromRequest("sort");
		//取出检索用的对象
		String data = getParameterFromRequest("data");
		Map<String, Object> controlMap = supplierCommonService.findProducerList(startNum, limitNum, dir, sort,
				getContextPath(), data, SystemConstants.DIC_CUSTOMER_TYPE);
		// Map<String, Object> controlMap = userService.findUser(startNum,
		// limitNum, dir, sort, getContextPath(), "", "", "");
		long totalCount = Long.parseLong(controlMap.get("totalCount").toString());
		List<Supplier> list = (List<Supplier>) controlMap.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("way", "");
		map.put("entKind-name", "");
		map.put("address1", "");
		map.put("linkMan", "");
		map.put("linkTel", "");
		map.put("fax", "");
		map.put("email", "");
		//	map.put("state", "");
		new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}
}