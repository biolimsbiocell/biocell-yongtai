package com.biolims.supplier.common.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.common.dao.CommonDAO;
import com.biolims.dic.model.DicState;
import com.biolims.purchase.model.PurchaseApply;
import com.biolims.sample.model.SampleOrder;
import com.biolims.supplier.model.Supplier;
import com.opensymphony.xwork2.ActionContext;
@Repository
@SuppressWarnings({ "unchecked" })
public class SupplierDAO extends CommonDAO {
     public Supplier getSupplierByName(String name){
    	 String hql="from Supplier where name='"+name+"'";
    	 String key="";
    	List<Supplier> list =new ArrayList<Supplier>();
    	Supplier sup=null;
    	list=this.getSession().createQuery(hql+key)
				.list();
    	if(list.size()>0){
    		sup=list.get(0);
    	}
		return sup;
     }

	public Map<String, Object> findSupplierCustomerList(Integer start,
			Integer length, String query, String col,
			String sort) throws Exception {
		
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from Supplier where 1=1";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession()
				.get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from Supplier where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				key+=" order by "+col+" "+sort;
			}
			List<SampleOrder> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
		
		/*Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from DicStorageType where 1=1 and type='"
				+ type + "'";
		String countHql = "select count(*) from Supplier where 1=1 ";
		String key = "";
		// if(queryMap!=null){
		// key=map2where(queryMap);
		// }
		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			//String hql = "from Supplier where 1=1 and type='" + type + "' ";
			String hql = "from Supplier where 1=1 ";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				key += " order by " + col + " " + sort;
			}
			List<DicState> list = new ArrayList<DicState>();
			list = this.getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;*/
	}

	public Map<String, Object> supplierSelectTableJson(Integer start,
			Integer length, String query, String col, String sort, String flag) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from Supplier where 1=1";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from Supplier where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				key+=" order by "+col+" "+sort;
			}
			List<SampleOrder> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> selectSupplierByStorageNameList(Map<String, String> mapForQuery, Integer startNum,
 			Integer limitNum, String dir, String sort) {
		String key = " "; 
  		String hql = " from Supplier where 1=1 ";
  		if (mapForQuery != null)
  			key = map2where(mapForQuery);
  		Long total = (Long) this.getSession()
  				.createQuery(" select count(*) " + hql + key).uniqueResult();
  		List<Supplier> list = new ArrayList<Supplier>();
  		if (total > 0) {
  			if (dir != null && dir.length() > 0 && sort != null
  					&& sort.length() > 0) {
  				if (sort.indexOf("-") != -1) {
  					sort = sort.substring(0, sort.indexOf("-"));
  				}
  				key = key + " order by " + sort + " " + dir;
  			}
  			if (startNum == null || limitNum == null) {
  				list = this.getSession().createQuery(hql + key).list();
  			} else {
  				list = this.getSession().createQuery(hql + key)
  						.setFirstResult(startNum).setMaxResults(limitNum)
  						.list();
  			}
  		}
  		Map<String, Object> result = new HashMap<String, Object>();
  		result.put("total", total);
  		result.put("list", list);
  		return result;
	}

	public Map<String, Object> showSupplierDialogListJson(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from Supplier where 1=1 ";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
				
		Long sumCount = (Long) getSession().createQuery(countHql+key)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from Supplier where 1=1 ";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<Supplier> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
}
