/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：
 * 类描述：供应商客户service
 * 创建人：倪毅
 * 创建时间：2011-11
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.supplier.common.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.supplier.common.dao.SupplierDAO;
import com.biolims.supplier.model.Supplier;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;

@Service
public class SupplierCommonService {
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SupplierDAO supplierDAO; 

	/**
	 * 
	 * 检索,采用map方式传递检索参数
	 * 
	 * 
	 * @return 参数值MAP，如果传递的为null返回空字符串
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> findSupplierCustomerList(int startNum, int limitNum, String dir, String sort,
			String contextPath, String data, String type, String id) throws Exception {
		//检索条件map,每个字段检索值
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		//检索方式map,每个字段的检索方式
		Map<String, String> mapForCondition = new HashMap<String, String>();
		Supplier sc = new Supplier();
		mapForCondition.put("state.id", "plike");
		if (id != null && id != "") {
			mapForQuery.put("supplierType.id", id);
			mapForCondition.put("supplierType.id", "=");
		}

		mapForQuery.put("state.id", SystemConstants.DIC_STATE_YES);

		//mapForCondition.put("supplierType.id", "<>");

		//mapForQuery.put("supplierType.id", "4028fe8147c8795b0147c886c8b30000");

		if (data != null && !data.equals("")) {
			mapForCondition.put("id", "like");
			mapForCondition.put("name", "like");

			Map<String, Object> map = JsonUtils.toObjectByJson(data, Map.class);
			sc = (Supplier) commonDAO.Map2Bean(map, sc);
			mapForQuery = BeanUtils.po2MapNotNull(mapForCondition, sc);

		}
		//将map值传入，获取列表
		Map<String, Object> controlMap = commonDAO.findObjectCondition(startNum, limitNum, dir, sort, Supplier.class,
				mapForQuery, mapForCondition);
		List<Supplier> list = (List<Supplier>) controlMap.get("list");
		controlMap.put("list", list);
		return controlMap;
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> findProducerList(int startNum, int limitNum, String dir, String sort,
			String contextPath, String data, String type) throws Exception {
		//检索条件map,每个字段检索值
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		//检索方式map,每个字段的检索方式
		Map<String, String> mapForCondition = new HashMap<String, String>();
		Supplier sc = new Supplier();
		mapForCondition.put("state.id", "plike");

		mapForQuery.put("state.id", SystemConstants.DIC_STATE_YES);

		//mapForCondition.put("supplierType.id", "=");

		//mapForQuery.put("supplierType.id", "4028fe8147c8795b0147c886c8b30000");

		if (data != null && !data.equals("")) {
			mapForCondition.put("id", "like");
			mapForCondition.put("name", "like");

			Map<String, Object> map = JsonUtils.toObjectByJson(data, Map.class);
			sc = (Supplier) commonDAO.Map2Bean(map, sc);
			mapForQuery = BeanUtils.po2MapNotNull(mapForCondition, sc);

		}
		//将map值传入，获取列表
		Map<String, Object> controlMap = commonDAO.findObjectCondition(startNum, limitNum, dir, sort, Supplier.class,
				mapForQuery, mapForCondition);
		List<Supplier> list = (List<Supplier>) controlMap.get("list");
		controlMap.put("list", list);
		return controlMap;
	}

	public Map<String, Object> supplierSelectTableJson(Integer start,
			Integer length, String query, String col, String sort, String flag) throws Exception {
		return supplierDAO.supplierSelectTableJson(start,length,query,col,sort,flag);
	}
}
