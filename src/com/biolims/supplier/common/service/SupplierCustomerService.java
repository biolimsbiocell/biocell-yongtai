/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：
 * 类描述：供应商客户service
 * 创建人：倪毅
 * 创建时间：2011-11
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.supplier.common.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.applicationType.service.ApplicationTypeService;
import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.SystemCodeService;
import com.biolims.contract.model.Contract;
import com.biolims.experiment.dna.model.DnaTask;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.model.LogInfo;
import com.biolims.supplier.common.dao.SupplierDAO;
import com.biolims.supplier.model.Supplier;
import com.biolims.system.quality.model.QualityProduct;
import com.biolims.util.JsonUtils;
import com.biolims.supplier.common.constants.SystemCode;
import com.opensymphony.xwork2.ActionContext;

@Service
public class SupplierCustomerService extends ApplicationTypeService {
	@Resource
	private CommonDAO commonDAO;

	@Resource
	private SupplierDAO supplierDAO;
	
	@Resource
	private CodingRuleService codingRuleService;
	
	@Resource
	private SystemCodeService systemCodeService;

	/**
	 * 
	 * 检索,采用map方式传递检索参数
	 * 
	 * 
	 * @return 参数值MAP，如果传递的为null返回空字符串
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> findSupplierCustomerList(Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {

		return supplierDAO.findSupplierCustomerList(start, length, query, col,
				sort);

		// 检索条件map,每个字段检索值
		/*
		 * Map<String, Object> mapForQuery = new HashMap<String, Object>();
		 * //检索方式map,每个字段的检索方式 Map<String, String> mapForCondition = new
		 * HashMap<String, String>(); Supplier sc = new Supplier(); if (data !=
		 * null && !data.equals("")) { // mapForCondition.put("id", "like"); //
		 * mapForCondition.put("name", "like"); BeanUtils.data2Map(data,
		 * mapForQuery, mapForCondition); // Map<String, Object> map =
		 * JsonUtils.toObjectByJson(data, Map.class); // sc = (Supplier)
		 * commonDAO.Map2Bean(map, sc); // mapForQuery =
		 * BeanUtils.po2MapNotNull(mapForCondition, sc); } //将map值传入，获取列表
		 * Map<String, Object> controlMap =
		 * commonDAO.findObjectCondition(startNum, limitNum, dir, sort,
		 * Supplier.class, mapForQuery, mapForCondition); List<Supplier> list =
		 * (List<Supplier>) controlMap.get("list"); controlMap.put("list",
		 * list); return controlMap;
		 */
	}

	public Supplier get(String id) throws Exception {
		Supplier supplier = commonDAO.get(Supplier.class, id);
		return supplier;
	}

	public Supplier getSupplierByName(String name) throws Exception {
		Supplier supplier = supplierDAO.getSupplierByName(name);
		return supplier;
	}

	/**
	 * 保存
	 * @return 
	 */
	/*
	 * public void save(Supplier an) throws Exception {
	 * commonDAO.saveOrUpdate(an);
	 * 
	 * }
	 */
	/*
	 * public void save(Supplier an,String logInfo) throws Exception {
	 * 
	 * commonDAO.saveOrUpdate(an);
	 * 
	 * if (logInfo != null && !"".equals(logInfo)) { LogInfo li = new LogInfo();
	 * li.setLogDate(new Date()); User u = (User) ServletActionContext
	 * .getRequest() .getSession() .getAttribute(
	 * SystemConstants.USER_SESSION_KEY); li.setUserId(u.getId());
	 * li.setFileId(an.getId()); li.setModifyContent(logInfo);
	 * commonDAO.saveOrUpdate(li); }
	 * 
	 * }
	 */
	/*@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String save(String main, String[] tempId, String userId,
			String templateId, String logInfo) throws Exception {
		String id = "";
		if (main != null) {
			String mainJson = "[" + main + "]";
//			String mainJson =main;
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					mainJson, List.class);
			Supplier supplier = new Supplier();
			supplier= (Supplier) commonDAO.Map2Bean(list.get(0),supplier);
			if ((supplier.getId() != null && supplier.getId().equals(""))
					|| supplier.getId().equals("NEW")) {
				String modelName = "Supplier";
				String markCode = "S";
				id = codingRuleService.genTransID(modelName, markCode);
				supplier.setId(id);
				supplier.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				supplier.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			} else {
				id = supplier.getId();
//				supplier=commonDAO.get(Supplier.class, id);
				supplier.setName(name);
				supplier.setSampleNum(sampleNum);
				supplier.setMaxNum(maxNum);
			}
			commonDAO.saveOrUpdate(supplier);
			id = supplier.getId();
			if (id == null || id.length() <= 0
					|| SystemCode.SUPPLIER_SYSTEMCODE.equals(id)) {

				String modelName = "Supplier";
				String markCode = "S";
				String autoID = codingRuleService.genTransID(modelName, markCode);
				supplier.setId(autoID);
			}
		}
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
					.getRequest()
					.getSession()
					.getAttribute(
							SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
		return id;
	}*/

	@WriteOperLog
	@WriteExOperLog
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void save(Supplier pr,String logInfo,String log)
			throws Exception {
		commonDAO.saveOrUpdate(pr);
		
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
			.getRequest()
			.getSession()
			.getAttribute(
					SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(pr.getId());
			li.setClassName("Supplier");
			li.setModifyContent(logInfo);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			commonDAO.saveOrUpdate(li);
		}

	}
	/**
	 * 
	 * 检索list,采用map方式传递检索参数
	 * 
	 * 
	 * @return 参数值MAP，如果传递的为null返回空字符串
	 */
	public Map<String, Object> findCustomerContractList(int startNum,
			int limitNum, String dir, String sort, String customerId,
			String type) throws Exception {
		// 检索条件map,每个字段检索值
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		// 检索方式map,每个字段的检索方式
		Map<String, String> mapForCondition = new HashMap<String, String>();

		mapForCondition.put("supplier.id", "=");
		mapForQuery.put("supplier.id", customerId);

		mapForCondition.put("type", "=");
		mapForQuery.put("type", type);

		// 将map值传入，获取列表
		Map<String, Object> controlMap = commonDAO.findObjectCondition(
				startNum, limitNum, dir, sort, Contract.class, mapForQuery,
				mapForCondition);
		return controlMap;
	}

	public Map<String, Object> findSupplierCustomerByStorageNameList(Map<String, String> mapForQuery, int startNum,
			int limitNum, String dir, String sort) {
		return supplierDAO.selectSupplierByStorageNameList(
				mapForQuery, startNum,
				 limitNum, dir,  sort);
	}

	public Map<String, Object> showSupplierDialogListJson(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return supplierDAO.showSupplierDialogListJson(start, length, query, col, sort);
	}
}
