/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：Action
 * 类描述：供应商管理
 * 创建人：倪毅
 * 创建时间：2011-11
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.supplier.main.action;

import java.net.URLDecoder;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.common.service.SystemCodeService;
import com.biolims.crm.customer.customer.model.CrmCity;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.dic.model.DicCurrencyType;
import com.biolims.experiment.dna.model.DnaTask;
import com.biolims.file.service.FileInfoService;
import com.biolims.purchase.model.PurchaseApply;
import com.biolims.supplier.common.constants.SystemCode;
import com.biolims.supplier.common.constants.SystemConstants;
import com.biolims.supplier.common.service.SupplierCustomerService;
import com.biolims.supplier.model.Supplier;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/supplier")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public class SupplierAction extends BaseActionSupport {

	private static final long serialVersionUID = -164661537469123056L;

	@Resource
	private CodingRuleService codingRuleService;
	// 该action权限id
	private String rightsId = "50102";

	// 用于页面上显示模块名称
	private String title = "供应商管理";

	private Supplier supplier = new Supplier();

	@Autowired
	private SupplierCustomerService supplierCustomerService;

	@Autowired
	private CommonService commonService;
	@Resource
	private SystemCodeService systemCodeService;
	@Resource
	private FileInfoService fileInfoService;
	
	@Autowired
	private CommonDAO commonDAO;
	@Resource
	private FieldService fieldService;
	
	/**
		 *	供应商Dialog
	     * @Title: showSupplierDialogList  
	     * @Description: TODO  
	     * @param @return
	     * @param @throws Exception    
	     * @return String  
		 * @author 孙灵达  
	     * @date 2018年8月8日
	     * @throws
	 */
	@Action(value = "showSupplierDialogList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSupplierDialogList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/supplier/main/showSupplierDialogList.jsp");
	}
	
	@Action(value = "showSupplierDialogListJson")
	public void showSupplierDialogListJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = supplierCustomerService.showSupplierDialogListJson(start, length, query, col, sort);
			List<Supplier> list = (List<Supplier>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("Supplier");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "supplierSelectByStorageName", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogSupplierListByStorageName() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		String commodityName = getParameterFromRequest("storageId");
//		putObjToContext("commodityName", commodityName);
		return dispatcher("/WEB-INF/page/supplier/main/supplierDialogByStorageName.jsp");
	}

	@Action(value = "showDialogSupplierListByStoragNameJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogSupplierListJsonByStorageName() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
//		String commodityName = getParameterFromRequest("name");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
//		Map<String, Object> result = supplierCustomerService
//				.findSupplierCustomerList(map2Query,startNum, limitNum, dir, sort,
//						getContextPath(), data,
//						SystemConstants.DIC_SUPPLIER_TYPE);
		Map<String, Object> result = supplierCustomerService.findSupplierCustomerByStorageNameList(map2Query,
				startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<Supplier> list = (List<Supplier>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}
	// 查询成本中心
	/*@Action(value = "showScopName", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String crmPatientSelectTable() throws Exception {
		String code = getRequest().getParameter("code");
		String code1 = URLDecoder.decode(code, "UTF-8");
		putObjToContext("code", code1);
		return dispatcher("/WEB-INF/page/supplier/show/showScopeName.jsp");
	}*/
	/*@Action(value = "crmPatientSelectTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void crmPatientSelectTableJson() throws Exception {
		String code=getParameterFromRequest("code");
		String name = URLDecoder.decode(code, "UTF-8");
		String query=getParameterFromRequest("query");
		String colNum=getParameterFromRequest("order[0][column]");
		String col=getParameterFromRequest("columns["+colNum+"][data]");
		String sort=getParameterFromRequest("order[0][dir]");
		Integer start=Integer.valueOf(getParameterFromRequest("start"));
		Integer length=Integer.valueOf(getParameterFromRequest("length"));
		String draw=getParameterFromRequest("draw");
		Map<String,Object> result=crmPatientService.crmPatientSelectTableJson(start,length,query,col,sort,name);
		List<CrmPatient> list = (List<CrmPatient>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("gender", "");
		map.put("dateOfBirth", "yyyy-MM-dd");
		map.put("race", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}*/

	/**
	 * 访问供应商列表
	 */

	@Action(value = "showSupplierList")
	public String showSupplierList() throws Exception {
		rightsId = "50102";
		// 用于接收通用查询的参数
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		// 属性map<字段名称, ext-list属性数组说明,值为空""则没有该属性>

		// String type = generalexttype(map);
		// String col = generalextcol(map);
		// // 用于ext显示
		// putObjToContext("type", type);
		// putObjToContext("col", col);
		// 用于判断当前页面类型,LIST类型
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		// toolbar设置
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		// 用于调用json时的action链接
		/*
		 * putObjToContext("path", ServletActionContext.getRequest()
		 * .getContextPath() + "/supplier/showSupplierListJson.action");
		 */
		// 导向jsp页面显示
		return dispatcher("/WEB-INF/page/supplier/main/showSupplierList.jsp");
	}

	@Action(value = "showSupplierListJson")
	public void showAnimalListJson() throws Exception {
		// 开始记录数
		/*
		 * int startNum = Integer.parseInt(getParameterFromRequest("start")); //
		 * limit int limitNum =
		 * Integer.parseInt(getParameterFromRequest("limit")); // 字段 String dir
		 * = getParameterFromRequest("dir"); // 排序方式 String sort =
		 * getParameterFromRequest("sort"); String data =
		 * getParameterFromRequest("data");
		 */

		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");

		try {
			Map<String, Object> result = supplierCustomerService
					.findSupplierCustomerList(start, length, query, col, sort);
			// Map<String, Object> controlMap = userService.findUser(startNum,
			// limitNum, dir, sort, getContextPath(), "", "", "");
			/*
			 * long totalCount = Long.parseLong(controlMap.get("totalCount")
			 * .toString());
			 */
			List<Supplier> list = ((List<Supplier>) result.get("list"));
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("way", "");
			map.put("entKind-name", "");
			map.put("address1", "");
			map.put("state-name", "");
			map.put("supplierType-id", "");
			map.put("supplierType-name", "");
			map.put("website", "");
			map.put("linkMan", "");
			map.put("linkTel", "");
			map.put("selA-name", "");
			map.put("fax", "");
			map.put("postCode", "");
			map.put("address2", "");
			map.put("address3", "");
			map.put("address4", "");
			map.put("address5", "");
			map.put("capitalNum", "");
			map.put("currencyType-name", "");
			map.put("orgCode", "");
			map.put("aptitude1", "");
			map.put("aptitude2", "");
			map.put("aptitude3", "");
			map.put("accountBank", "");
			map.put("account", "");
			map.put("bankCode", "");
			map.put("scopeId", "");
			map.put("scopeName", "");
			map.put("fieldContent", "");
			/*
			 * new SendData().sendDateJson(map, list, totalCount,
			 * ServletActionContext.getResponse());
			 */
			//根据模块查询自定义字段数据
			String data=new SendData().getDateJsonForDatatable(map, list);
		/*	Map<String,Object> mapField = fieldService.findFieldByModuleValue("Supplier");
			String dataStr = PushData.pushFieldData(data, mapField);*/
			HttpUtils.write(PushData.pushData(draw, result,data));
			
			/*String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));*/
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 访问编辑页面
	 */

	@Action(value = "toEditSupplier")
	public String toEditSupplier() throws Exception {
		rightsId = "50101";
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			supplier = supplierCustomerService.get(id);
			putObjToContext("supplierId", id);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);

			num = fileInfoService.findFileInfoCount(id, "supplier");

		} else {
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			supplier.setId(SystemCode.SUPPLIER_SYSTEMCODE);
			supplier.setCreateUser(user);

			supplier.setType(SystemConstants.DIC_SUPPLIER_TYPE);
			supplier.setCreateDate(new Date());
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);

		return dispatcher("/WEB-INF/page/supplier/main/editSupplier.jsp");
	}

	/**
	 * 复制页面
	 */

	@Action(value = "toCopySupplier")
	public String toCopySupplier() throws Exception {
		String id = getParameterFromRequest("id");
		supplier = supplierCustomerService.get(id);
		User user = (User) this
				.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		supplier.setId("");
		supplier.setCreateUser(user);
		supplier.setType(SystemConstants.DIC_SUPPLIER_TYPE);
		supplier.setCreateDate(new Date());
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/supplier/main/editSupplier.jsp");
	}

	/**
	 * 访问编辑页面
	 */

	@Action(value = "toViewSupplier")
	public String toViewSupplier() throws Exception {
		String id = getParameterFromRequest("id");

		if (id != null && !id.equals("")) {

			supplier = supplierCustomerService.get(id);
			putObjToContext("supplierId", id);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);

		}
		return dispatcher("/WEB-INF/page/supplier/main/editSupplier.jsp");
	}

	@Action(value = "toEditCustomer")
	public String toEditCustomer() throws Exception {
		String id = getParameterFromRequest("id");

		if (id != null && !id.equals("")) {
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			supplier = supplierCustomerService.get(id);
			putObjToContext("supplierId", id);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);

		} else {
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			supplier.setCreateUser(user);
			supplier.setType(SystemConstants.DIC_CUSTOMER_TYPE);
			supplier.setCreateDate(new Date());
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			// toToolBar(rightsId,SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		return dispatcher("/WEB-INF/page/supplier/main/editSupplier.jsp");
	}

	/**
	 * 添加
	 */
	@Action(value = "saveSupplier")
	public String saveSupplier() throws Exception {
		String id = this.supplier.getId();
		String log="";
		if (id != null && "NEW".equals(id)) {
			log = "123";
			String modelName = "Supplier";
			String markCode = "S";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			supplier.setId(autoID);
		} 
		String changeLog = getParameterFromRequest("changeLog");
//		changeLog=new String(changeLog.getBytes("ISO-8859-1"),"utf-8");
		supplierCustomerService.save(supplier,changeLog,log);
		return redirect("/supplier/toEditSupplier.action?id="+supplier.getId());
		
	}

	// 选择供应商窗口
	@Action(value = "supplierSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogSupplierList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/supplier/main/supplierDialog.jsp");
	}

	@Action(value = "showDialogSupplierListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogSupplierListJson() throws Exception {
		/*
		 * int startNum = Integer.parseInt(getParameterFromRequest("start"));
		 * int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		 * String dir = getParameterFromRequest("dir"); String sort =
		 * getParameterFromRequest("sort"); String data =
		 * getParameterFromRequest("data");
		 */
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = supplierCustomerService
				.findSupplierCustomerList(start, length, query, col, sort);
		Long count = (Long) result.get("total");
		List<Supplier> list = (List<Supplier>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("linkMan", "");
		map.put("linkTel", "");
		map.put("email", "");
		map.put("fax", "");
		map.put("address5", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public CommonService getCommonService() {
		return commonService;
	}

	public void setCommonService(CommonService commonService) {
		this.commonService = commonService;
	}

	public SupplierCustomerService getSupplierCustomerService() {
		return supplierCustomerService;
	}

	public void setSupplierCustomerService(
			SupplierCustomerService supplierCustomerService) {
		this.supplierCustomerService = supplierCustomerService;
	}

	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

}
