package com.biolims.supplier.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

import org.hibernate.annotations.ForeignKey;

import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dic.model.DicCurrencyType;
import com.biolims.dic.model.DicState;
import com.biolims.dic.model.DicType;
import com.biolims.common.model.user.User;
import com.biolims.crm.customer.customer.model.CrmCity;
import com.biolims.dao.EntityDao;

/**
 * 生产商/供应商
 */
@Entity
@Table(name = "T_SUPPLIER_CUSTOMER")
public class Supplier extends EntityDao<Supplier> implements Serializable {

	private static final long serialVersionUID = 2191801684912559179L;
	@Id
	@Column(name = "ID", length = 30)
	private String id;

	@Column(name = "NAME", length = 60)
	private String name;//名称

	@Column(name = "WEBSITE", length = 100)
	private String website;//网址

	@Column(name = "APTITUDE1", length = 60)
	private String aptitude1;//资质1

	@Column(name = "LINK_MAN", length = 60)
	private String linkMan;//联系人

	@Column(name = "LINK_TEL", length = 60)
	private String linkTel;//联系电话

	@Column(name = "EMAIL", length = 60)
	private String email;//email

	@Column(name = "LINK_MAN1", length = 60)
	private String linkMan1;//联系人

	@Column(name = "LINK_TEL1", length = 60)
	private String linkTel1;//联系电话

	@Column(name = "EMAIL1", length = 60)
	private String email1;//email

	@Column(name = "LINK_MAN2", length = 60)
	private String linkMan2;//联系人

	@Column(name = "LINK_TEL2", length = 60)
	private String linkTel2;//联系电话

	@Column(name = "EMAIL2", length = 60)
	private String email2;//email

	@Column(name = "FAX", length = 20)
	private String fax;//传真

	@Column(name = "ACCOUNT_BANK", length = 200)
	private String accountBank;//开户行

	@Column(name = "ACCOUNT_BANK_ADDRESS", length = 200)
	private String accountBankAddress;//开户行地址

	@Column(name = "ACCOUNT_NAME", length = 200)
	private String accountName;//账号名称

	@Column(name = "BIC", length = 200)
	private String bic;//账号名称

	@Column(name = "ARN", length = 200)
	private String arn;//账号名称

	@Column(name = "IBAN", length = 200)
	private String iban;//账号名称

	@Column(name = "ACCOUNT", length = 200)
	private String account;//账号

	@Column(name = "POST_CODE", length = 30)
	private String postCode;//邮编

	@Column(name = "COUTRY", length = 20)
	private String coutry;//国家

	@Column(name = "BANK_CODE", length = 200)
	private String bankCode;//银行代码

	@Column(name = "CREDIT", length = 20)
	private String credit;//授信度

	@Column(name = "CANCEL_NUM")
	private Integer cancelNum;//退货数量

	@Column(name = "ADDRESS1", length = 60)
	private String address1;//国家

	@Column(name = "ADDRESS2", length = 60)
	private String address2;//省或地区

	@Column(name = "ADDRESS3", length = 60)
	private String address3;//城市

	@Column(name = "ADDRESS4", length = 200)
	private String address4;//街道门牌

	@Column(name = "ADDRESS5", length = 60)
	private String address5;//建筑物

	@Column(name = "ORG_CODE", length = 200)
	private String orgCode;//机构代码

	@Column(name = "TYPE", length = 32)
	private String type;//类型

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_SUPPLIER_TYPE_ID")
	private DicType supplierType;//类型

	@Column(name = "CAPITALNUM", length = 20)
	private String capitalNum;//注册资本

	@Column(name = "APTITUDE3", length = 60)
	private String aptitude3;//资质3

	@Column(name = "APTITUDE2", length = 60)
	private String aptitude2;//资质2

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_STATE_ID")
	private DicState state;//状态

	@Column(name = "WAY", length = 60)
	private String way;//行业

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_ENT_KIND_TYPE_ID")
	private DicType entKind;//企业性质

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_CURRENCY_TYPE_ID")
	private DicCurrencyType currencyType;//币种
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "USER_CREATE_USER_ID")
	private User createUser;//创建人
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SELA")
	private CrmCity selA;//国家
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SELB")
	private CrmCity selB;//国家
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SELC")
	private CrmCity selC;//国家
	/** 范围Id */
	@Column(name = "SCOPE_ID", length = 30)
	private String scopeId;
	/** 范围 */
	@Column(name = "SCOPE_NAME", length = 60)
	private String scopeName;
	
	/** 自定义字段*/
	@Column(name = "FIELD_CONTENT", length = 255)
	private String fieldContent;
	
	public String getFieldContent() {
		return fieldContent;
	}

	public void setFieldContent(String fieldContent) {
		this.fieldContent = fieldContent;
	}
	

	public String getScopeId() {
		return scopeId;
	}

	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	public String getScopeName() {
		return scopeName;
	}

	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

	public DicCurrencyType getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(DicCurrencyType currencyType) {
		this.currencyType = currencyType;
	}

	private Date createDate;//创建日期

	public User getCreateUser() {
		return createUser;
	}

	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getAptitude1() {
		return aptitude1;
	}

	public void setAptitude1(String aptitude1) {
		this.aptitude1 = aptitude1;
	}

	public String getLinkMan() {
		return linkMan;
	}

	public void setLinkMan(String linkMan) {
		this.linkMan = linkMan;
	}

	public String getLinkTel() {
		return linkTel;
	}

	public void setLinkTel(String linkTel) {
		this.linkTel = linkTel;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getAccountBank() {
		return accountBank;
	}

	public void setAccountBank(String accountBank) {
		this.accountBank = accountBank;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public String getCoutry() {
		return coutry;
	}

	public void setCoutry(String coutry) {
		this.coutry = coutry;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getCredit() {
		return credit;
	}

	public void setCredit(String credit) {
		this.credit = credit;
	}

	public Integer getCancelNum() {
		return cancelNum;
	}

	public void setCancelNum(Integer cancelNum) {
		this.cancelNum = cancelNum;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getAddress4() {
		return address4;
	}

	public void setAddress4(String address4) {
		this.address4 = address4;
	}

	public String getAddress5() {
		return address5;
	}

	public void setAddress5(String address5) {
		this.address5 = address5;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCapitalNum() {
		return capitalNum;
	}

	public void setCapitalNum(String capitalNum) {
		this.capitalNum = capitalNum;
	}

	public String getAptitude3() {
		return aptitude3;
	}

	public void setAptitude3(String aptitude3) {
		this.aptitude3 = aptitude3;
	}

	public String getAptitude2() {
		return aptitude2;
	}

	public void setAptitude2(String aptitude2) {
		this.aptitude2 = aptitude2;
	}

	public DicState getState() {
		return state;
	}

	public void setState(DicState state) {
		this.state = state;
	}

	public String getWay() {
		return way;
	}

	public void setWay(String way) {
		this.way = way;
	}

	public DicType getEntKind() {
		return entKind;
	}

	public void setEntKind(DicType entKind) {
		this.entKind = entKind;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public CrmCity getSelA() {
		return selA;
	}

	public void setSelA(CrmCity selA) {
		this.selA = selA;
	}

	public CrmCity getSelB() {
		return selB;
	}

	public void setSelB(CrmCity selB) {
		this.selB = selB;
	}

	public CrmCity getSelC() {
		return selC;
	}

	public void setSelC(CrmCity selC) {
		this.selC = selC;
	}

	public DicType getSupplierType() {
		return supplierType;
	}

	public void setSupplierType(DicType supplierType) {
		this.supplierType = supplierType;
	}

	public String getLinkMan1() {
		return linkMan1;
	}

	public void setLinkMan1(String linkMan1) {
		this.linkMan1 = linkMan1;
	}

	public String getLinkTel1() {
		return linkTel1;
	}

	public void setLinkTel1(String linkTel1) {
		this.linkTel1 = linkTel1;
	}

	public String getEmail1() {
		return email1;
	}

	public void setEmail1(String email1) {
		this.email1 = email1;
	}

	public String getLinkMan2() {
		return linkMan2;
	}

	public void setLinkMan2(String linkMan2) {
		this.linkMan2 = linkMan2;
	}

	public String getLinkTel2() {
		return linkTel2;
	}

	public void setLinkTel2(String linkTel2) {
		this.linkTel2 = linkTel2;
	}

	public String getEmail2() {
		return email2;
	}

	public void setEmail2(String email2) {
		this.email2 = email2;
	}

	public String getAccountBankAddress() {
		return accountBankAddress;
	}

	public void setAccountBankAddress(String accountBankAddress) {
		this.accountBankAddress = accountBankAddress;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getBic() {
		return bic;
	}

	public void setBic(String bic) {
		this.bic = bic;
	}

	public String getArn() {
		return arn;
	}

	public void setArn(String arn) {
		this.arn = arn;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

}
