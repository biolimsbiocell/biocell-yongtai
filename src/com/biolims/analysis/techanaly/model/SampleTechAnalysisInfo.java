package com.biolims.analysis.techanaly.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 科技服务信息分析结果
 * @author lims-platform
 * @date 2016-02-26 13:14:22
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SAMPLE_TECH_ANALYSIS_INFO")
@SuppressWarnings("serial")
public class SampleTechAnalysisInfo extends EntityDao<SampleTechAnalysisInfo> implements java.io.Serializable {
	/**分析明细id*/
	private String id;
	/**描述*/
	private String name;
	//图片1
	private String pricture1;
	//图片2
	private String pricture2;
	//文件1
	private String file1;
	//文件2
	private String file2;
	/**文库号*/
	private String code;
	/**原始样本号*/
	private String sampleCode;
	/**比对率*/
	private String compareRates;
	/**multi map率*/
	private String multiMapRates;
	/**差异基因数目*/
	private String geneDifferNum;
	/**覆盖度*/
	private String coverage;
	/**snp数目*/
	private String snpNum;
	/**mirna碱基偏好性*/
	private String miRnaPrefer;
	/**contig数量*/
	private String contigNum;
	/**均一性分布*/
	private String disHomo;
	/**样品聚类效果*/
	private String sampleCluster;
	/**转化率*/
	private String convertRates;
	/**插入片段长度分值*/
	private String fragmentSize;
	/**peaks数目*/
	private String peaksNum;
	/**motif预测结果不为空*/
	private String motifResult;
	/**富集区域相关基因数目*/
	private String geneRegionNum;
	/**unique mapped ratio*/
	private String uniqueMappedRatio;
	/**valid paired-end reads*/
	private String vaildPairdEndReads;
	/**dangling end paired-end reads*/
	private String danglingEndPairdReads;
	/**self circle paired-end reads*/
	private String selfCirclePairdReads;
	/**理论酶切片段数量是否正确*/
	private String eriNum;
	/**理论酶切片段分布描述是否正确*/
	private String eriDis;
	/**同一物种不同样本顺反比例是否一致*/
	private String transCisRate;
	/**捕获特异性*/
	private String catchSpecific;
	/**snp数量级=~基因组大小*0.001*/
	private String snpNumLevel;
	/**contig n50*/
	private String contigN50;
	/**宿主比对率*/
	private String hostCompareRates;
	/**kmer depth*/
	private String kimerDepth;
	/**signalip类型*/
	private String singalIp;
	/**gc-depth图聚集分布深度*/
	private String gcDepth;
	/**基因数目*/
	private String geneNum;
	/**各样品拼接比例*/
	private String sampleSpilice;
	/**扩增16s v3-v4区域的箱线图范围*/
	private String aug16sV3;
	/**扩增16s v3区域的箱线图范围*/
	private String aug16sV4;
	/**标准值*/
	private String standardValue;
	/**实际值*/
	private String realValue;
	/**结果*/
	private String result;
	/**处理方式*/
	private String method;
	/**相关主表*/
	private TechAnalysisTask techAnalysisTask;
	/**
	 *方法: 取得String
	 *@return: String  分析明细id
	 */
	
	@Id
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  分析明细id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  文库号
	 */
	@Column(name ="CODE", length = 50)
	public String getCode(){
		return this.code;
	}
	/**
	 *方法: 设置String
	 *@param: String  文库号
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 *方法: 取得String
	 *@return: String  原始样本号
	 */
	@Column(name ="SAMPLE_CODE", length = 50)
	public String getSampleCode(){
		return this.sampleCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  原始样本号
	 */
	public void setSampleCode(String sampleCode){
		this.sampleCode = sampleCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  比对率
	 */
	@Column(name ="COMPARE_RATES", length = 50)
	public String getCompareRates(){
		return this.compareRates;
	}
	/**
	 *方法: 设置String
	 *@param: String  比对率
	 */
	public void setCompareRates(String compareRates){
		this.compareRates = compareRates;
	}
	/**
	 *方法: 取得String
	 *@return: String  multi map率
	 */
	@Column(name ="MULTI_MAP_RATES", length = 50)
	public String getMultiMapRates(){
		return this.multiMapRates;
	}
	/**
	 *方法: 设置String
	 *@param: String  multi map率
	 */
	public void setMultiMapRates(String multiMapRates){
		this.multiMapRates = multiMapRates;
	}
	/**
	 *方法: 取得String
	 *@return: String  差异基因数目
	 */
	@Column(name ="GENE_DIFFER_NUM", length = 50)
	public String getGeneDifferNum(){
		return this.geneDifferNum;
	}
	/**
	 *方法: 设置String
	 *@param: String  差异基因数目
	 */
	public void setGeneDifferNum(String geneDifferNum){
		this.geneDifferNum = geneDifferNum;
	}
	/**
	 *方法: 取得String
	 *@return: String  覆盖度
	 */
	@Column(name ="COVERAGE", length = 50)
	public String getCoverage(){
		return this.coverage;
	}
	/**
	 *方法: 设置String
	 *@param: String  覆盖度
	 */
	public void setCoverage(String coverage){
		this.coverage = coverage;
	}
	/**
	 *方法: 取得String
	 *@return: String  snp数目
	 */
	@Column(name ="SNP_NUM", length = 50)
	public String getSnpNum(){
		return this.snpNum;
	}
	/**
	 *方法: 设置String
	 *@param: String  snp数目
	 */
	public void setSnpNum(String snpNum){
		this.snpNum = snpNum;
	}
	/**
	 *方法: 取得String
	 *@return: String  mirna碱基偏好性
	 */
	@Column(name ="MI_RNA_PREFER", length = 50)
	public String getMiRnaPrefer(){
		return this.miRnaPrefer;
	}
	/**
	 *方法: 设置String
	 *@param: String  mirna碱基偏好性
	 */
	public void setMiRnaPrefer(String miRnaPrefer){
		this.miRnaPrefer = miRnaPrefer;
	}
	/**
	 *方法: 取得String
	 *@return: String  contig数量
	 */
	@Column(name ="CONTIG_NUM", length = 50)
	public String getContigNum(){
		return this.contigNum;
	}
	/**
	 *方法: 设置String
	 *@param: String  contig数量
	 */
	public void setContigNum(String contigNum){
		this.contigNum = contigNum;
	}
	/**
	 *方法: 取得String
	 *@return: String  均一性分布
	 */
	@Column(name ="DIS_HOMO", length = 50)
	public String getDisHomo(){
		return this.disHomo;
	}
	/**
	 *方法: 设置String
	 *@param: String  均一性分布
	 */
	public void setDisHomo(String disHomo){
		this.disHomo = disHomo;
	}
	/**
	 *方法: 取得String
	 *@return: String  样品聚类效果
	 */
	@Column(name ="SAMPLE_CLUSTER", length = 50)
	public String getSampleCluster(){
		return this.sampleCluster;
	}
	/**
	 *方法: 设置String
	 *@param: String  样品聚类效果
	 */
	public void setSampleCluster(String sampleCluster){
		this.sampleCluster = sampleCluster;
	}
	/**
	 *方法: 取得String
	 *@return: String  转化率
	 */
	@Column(name ="CONVERT_RATES", length = 50)
	public String getConvertRates(){
		return this.convertRates;
	}
	/**
	 *方法: 设置String
	 *@param: String  转化率
	 */
	public void setConvertRates(String convertRates){
		this.convertRates = convertRates;
	}
	/**
	 *方法: 取得String
	 *@return: String  插入片段长度分值
	 */
	@Column(name ="FRAGMENT_SIZE", length = 50)
	public String getFragmentSize(){
		return this.fragmentSize;
	}
	/**
	 *方法: 设置String
	 *@param: String  插入片段长度分值
	 */
	public void setFragmentSize(String fragmentSize){
		this.fragmentSize = fragmentSize;
	}
	/**
	 *方法: 取得String
	 *@return: String  peaks数目
	 */
	@Column(name ="PEAKS_NUM", length = 50)
	public String getPeaksNum(){
		return this.peaksNum;
	}
	/**
	 *方法: 设置String
	 *@param: String  peaks数目
	 */
	public void setPeaksNum(String peaksNum){
		this.peaksNum = peaksNum;
	}
	/**
	 *方法: 取得String
	 *@return: String  motif预测结果不为空
	 */
	@Column(name ="MOTIF_RESULT", length = 50)
	public String getMotifResult(){
		return this.motifResult;
	}
	/**
	 *方法: 设置String
	 *@param: String  motif预测结果不为空
	 */
	public void setMotifResult(String motifResult){
		this.motifResult = motifResult;
	}
	/**
	 *方法: 取得String
	 *@return: String  富集区域相关基因数目
	 */
	@Column(name ="GENE_REGION_NUM", length = 50)
	public String getGeneRegionNum(){
		return this.geneRegionNum;
	}
	/**
	 *方法: 设置String
	 *@param: String  富集区域相关基因数目
	 */
	public void setGeneRegionNum(String geneRegionNum){
		this.geneRegionNum = geneRegionNum;
	}
	/**
	 *方法: 取得String
	 *@return: String  unique mapped ratio
	 */
	@Column(name ="UNIQUE_MAPPED_RATIO", length = 50)
	public String getUniqueMappedRatio(){
		return this.uniqueMappedRatio;
	}
	/**
	 *方法: 设置String
	 *@param: String  unique mapped ratio
	 */
	public void setUniqueMappedRatio(String uniqueMappedRatio){
		this.uniqueMappedRatio = uniqueMappedRatio;
	}
	/**
	 *方法: 取得String
	 *@return: String  valid paired-end reads
	 */
	@Column(name ="VAILD_PAIRD_END_READS", length = 50)
	public String getVaildPairdEndReads(){
		return this.vaildPairdEndReads;
	}
	/**
	 *方法: 设置String
	 *@param: String  valid paired-end reads
	 */
	public void setVaildPairdEndReads(String vaildPairdEndReads){
		this.vaildPairdEndReads = vaildPairdEndReads;
	}
	/**
	 *方法: 取得String
	 *@return: String  dangling end paired-end reads
	 */
	@Column(name ="DANGLING_END_PAIRD_READS", length = 50)
	public String getDanglingEndPairdReads(){
		return this.danglingEndPairdReads;
	}
	/**
	 *方法: 设置String
	 *@param: String  dangling end paired-end reads
	 */
	public void setDanglingEndPairdReads(String danglingEndPairdReads){
		this.danglingEndPairdReads = danglingEndPairdReads;
	}
	/**
	 *方法: 取得String
	 *@return: String  self circle paired-end reads
	 */
	@Column(name ="SELF_CIRCLE_PAIRD_READS", length = 50)
	public String getSelfCirclePairdReads(){
		return this.selfCirclePairdReads;
	}
	/**
	 *方法: 设置String
	 *@param: String  self circle paired-end reads
	 */
	public void setSelfCirclePairdReads(String selfCirclePairdReads){
		this.selfCirclePairdReads = selfCirclePairdReads;
	}
	/**
	 *方法: 取得String
	 *@return: String  理论酶切片段数量是否正确
	 */
	@Column(name ="ERI_NUM", length = 50)
	public String getEriNum(){
		return this.eriNum;
	}
	/**
	 *方法: 设置String
	 *@param: String  理论酶切片段数量是否正确
	 */
	public void setEriNum(String eriNum){
		this.eriNum = eriNum;
	}
	/**
	 *方法: 取得String
	 *@return: String  理论酶切片段分布描述是否正确
	 */
	@Column(name ="ERI_DIS", length = 50)
	public String getEriDis(){
		return this.eriDis;
	}
	/**
	 *方法: 设置String
	 *@param: String  理论酶切片段分布描述是否正确
	 */
	public void setEriDis(String eriDis){
		this.eriDis = eriDis;
	}
	/**
	 *方法: 取得String
	 *@return: String  同一物种不同样本顺反比例是否一致
	 */
	@Column(name ="TRANS_CIS_RATE", length = 50)
	public String getTransCisRate(){
		return this.transCisRate;
	}
	/**
	 *方法: 设置String
	 *@param: String  同一物种不同样本顺反比例是否一致
	 */
	public void setTransCisRate(String transCisRate){
		this.transCisRate = transCisRate;
	}
	/**
	 *方法: 取得String
	 *@return: String  捕获特异性
	 */
	@Column(name ="CATCH_SPECIFIC", length = 50)
	public String getCatchSpecific(){
		return this.catchSpecific;
	}
	/**
	 *方法: 设置String
	 *@param: String  捕获特异性
	 */
	public void setCatchSpecific(String catchSpecific){
		this.catchSpecific = catchSpecific;
	}
	/**
	 *方法: 取得String
	 *@return: String  snp数量级=~基因组大小*0.001
	 */
	@Column(name ="SNP_NUM_LEVEL", length = 50)
	public String getSnpNumLevel(){
		return this.snpNumLevel;
	}
	/**
	 *方法: 设置String
	 *@param: String  snp数量级=~基因组大小*0.001
	 */
	public void setSnpNumLevel(String snpNumLevel){
		this.snpNumLevel = snpNumLevel;
	}
	/**
	 *方法: 取得String
	 *@return: String  contig n50
	 */
	@Column(name ="CONTIG_N50", length = 50)
	public String getContigN50(){
		return this.contigN50;
	}
	/**
	 *方法: 设置String
	 *@param: String  contig n50
	 */
	public void setContigN50(String contigN50){
		this.contigN50 = contigN50;
	}
	/**
	 *方法: 取得String
	 *@return: String  宿主比对率
	 */
	@Column(name ="HOST_COMPARE_RATES", length = 50)
	public String getHostCompareRates(){
		return this.hostCompareRates;
	}
	/**
	 *方法: 设置String
	 *@param: String  宿主比对率
	 */
	public void setHostCompareRates(String hostCompareRates){
		this.hostCompareRates = hostCompareRates;
	}
	/**
	 *方法: 取得String
	 *@return: String  kmer depth
	 */
	@Column(name ="KIMER_DEPTH", length = 50)
	public String getKimerDepth(){
		return this.kimerDepth;
	}
	/**
	 *方法: 设置String
	 *@param: String  kmer depth
	 */
	public void setKimerDepth(String kimerDepth){
		this.kimerDepth = kimerDepth;
	}
	/**
	 *方法: 取得String
	 *@return: String  signalip类型
	 */
	@Column(name ="SINGAL_IP", length = 50)
	public String getSingalIp(){
		return this.singalIp;
	}
	/**
	 *方法: 设置String
	 *@param: String  signalip类型
	 */
	public void setSingalIp(String singalIp){
		this.singalIp = singalIp;
	}
	/**
	 *方法: 取得String
	 *@return: String  gc-depth图聚集分布深度
	 */
	@Column(name ="GC_DEPTH", length = 50)
	public String getGcDepth(){
		return this.gcDepth;
	}
	/**
	 *方法: 设置String
	 *@param: String  gc-depth图聚集分布深度
	 */
	public void setGcDepth(String gcDepth){
		this.gcDepth = gcDepth;
	}
	/**
	 *方法: 取得String
	 *@return: String  基因数目
	 */
	@Column(name ="GENE_NUM", length = 50)
	public String getGeneNum(){
		return this.geneNum;
	}
	/**
	 *方法: 设置String
	 *@param: String  基因数目
	 */
	public void setGeneNum(String geneNum){
		this.geneNum = geneNum;
	}
	/**
	 *方法: 取得String
	 *@return: String  各样品拼接比例
	 */
	@Column(name ="SAMPLE_SPILICE", length = 50)
	public String getSampleSpilice() {
		return sampleSpilice;
	}
	public void setSampleSpilice(String sampleSpilice) {
		this.sampleSpilice = sampleSpilice;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  扩增16s v3-v4区域的箱线图范围
	 */
	@Column(name ="AUG_16S_V3", length = 50)
	public String getAug16sV3(){
		return this.aug16sV3;
	}
	/**
	 *方法: 设置String
	 *@param: String  扩增16s v3-v4区域的箱线图范围
	 */
	public void setAug16sV3(String aug16sV3){
		this.aug16sV3 = aug16sV3;
	}
	/**
	 *方法: 取得String
	 *@return: String  扩增16s v3区域的箱线图范围
	 */
	@Column(name ="AUG_16S_V4", length = 50)
	public String getAug16sV4(){
		return this.aug16sV4;
	}
	/**
	 *方法: 设置String
	 *@param: String  扩增16s v3区域的箱线图范围
	 */
	public void setAug16sV4(String aug16sV4){
		this.aug16sV4 = aug16sV4;
	}
	/**
	 *方法: 取得String
	 *@return: String  标准值
	 */
	@Column(name ="STANDARD_VALUE", length = 50)
	public String getStandardValue(){
		return this.standardValue;
	}
	/**
	 *方法: 设置String
	 *@param: String  标准值
	 */
	public void setStandardValue(String standardValue){
		this.standardValue = standardValue;
	}
	/**
	 *方法: 取得String
	 *@return: String  实际值
	 */
	@Column(name ="REAL_VALUE", length = 50)
	public String getRealValue(){
		return this.realValue;
	}
	/**
	 *方法: 设置String
	 *@param: String  实际值
	 */
	public void setRealValue(String realValue){
		this.realValue = realValue;
	}
	/**
	 *方法: 取得String
	 *@return: String  结果
	 */
	@Column(name ="RESULT", length = 50)
	public String getResult(){
		return this.result;
	}
	/**
	 *方法: 设置String
	 *@param: String  结果
	 */
	public void setResult(String result){
		this.result = result;
	}
	/**
	 *方法: 取得String
	 *@return: String  处理方式
	 */
	@Column(name ="METHOD", length = 50)
	public String getMethod(){
		return this.method;
	}
	/**
	 *方法: 设置String
	 *@param: String  处理方式
	 */
	public void setMethod(String method){
		this.method = method;
	}
	public String getPricture1() {
		return pricture1;
	}
	public void setPricture1(String pricture1) {
		this.pricture1 = pricture1;
	}
	public String getPricture2() {
		return pricture2;
	}
	public void setPricture2(String pricture2) {
		this.pricture2 = pricture2;
	}
	public String getFile1() {
		return file1;
	}
	public void setFile1(String file1) {
		this.file1 = file1;
	}
	public String getFile2() {
		return file2;
	}
	public void setFile2(String file2) {
		this.file2 = file2;
	}
	/**
	 *方法: 取得String
	 *@return: String  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TECH_ANALYSIS_TASK")
	public TechAnalysisTask getTechAnalysisTask(){
		return this.techAnalysisTask;
	}
	/**
	 *方法: 设置String
	 *@param: String  相关主表
	 */
	public void setTechAnalysisTask(TechAnalysisTask techAnalysisTask){
		this.techAnalysisTask = techAnalysisTask;
	}
}