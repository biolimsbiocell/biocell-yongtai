package com.biolims.analysis.techanaly.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 科技服务信息分析
 * @author lims-platform
 * @date 2016-02-26 13:14:55
 * @version V1.0   
 *
 */
@Entity
@Table(name = "TECH_ANALYSIS_TASK")
@SuppressWarnings("serial")
public class TechAnalysisTask extends EntityDao<TechAnalysisTask> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
	/**创建人*/
	private String createUser;
	/**创建日期*/
	private String createDate;
	/**负责人*/
	private String headUser;
	/**项目id*/
	private String projectId;
	/**合同号*/
	private String contractId;
	/**任务单id*/
	private String techTaskId;
	/**状态id*/
	private String state;
	/**工作流状态*/
	private String stateName;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  创建人
	 */
	@Column(name ="CREATE_USER", length = 50)
	public String getCreateUser(){
		return this.createUser;
	}
	/**
	 *方法: 设置String
	 *@param: String  创建人
	 */
	public void setCreateUser(String createUser){
		this.createUser = createUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  创建日期
	 */
	@Column(name ="CREATE_DATE", length = 50)
	public String getCreateDate(){
		return this.createDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  创建日期
	 */
	public void setCreateDate(String createDate){
		this.createDate = createDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  负责人
	 */
	@Column(name ="HEAD_USER", length = 50)
	public String getHeadUser(){
		return this.headUser;
	}
	/**
	 *方法: 设置String
	 *@param: String  负责人
	 */
	public void setHeadUser(String headUser){
		this.headUser = headUser;
	}
	/**
	 *方法: 取得String
	 *@return: String  项目id
	 */
	@Column(name ="PROJECT_ID", length = 50)
	public String getProjectId(){
		return this.projectId;
	}
	/**
	 *方法: 设置String
	 *@param: String  项目id
	 */
	public void setProjectId(String projectId){
		this.projectId = projectId;
	}
	/**
	 *方法: 取得String
	 *@return: String  合同号
	 */
	@Column(name ="CONTRACT_ID", length = 50)
	public String getContractId(){
		return this.contractId;
	}
	/**
	 *方法: 设置String
	 *@param: String  合同号
	 */
	public void setContractId(String contractId){
		this.contractId = contractId;
	}
	/**
	 *方法: 取得String
	 *@return: String  任务单id
	 */
	@Column(name ="TECH_TASK_ID", length = 50)
	public String getTechTaskId(){
		return this.techTaskId;
	}
	/**
	 *方法: 设置String
	 *@param: String  任务单id
	 */
	public void setTechTaskId(String techTaskId){
		this.techTaskId = techTaskId;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态id
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态id
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  工作流状态
	 */
	@Column(name ="STATE_NAME", length = 50)
	public String getStateName(){
		return this.stateName;
	}
	/**
	 *方法: 设置String
	 *@param: String  工作流状态
	 */
	public void setStateName(String stateName){
		this.stateName = stateName;
	}
}