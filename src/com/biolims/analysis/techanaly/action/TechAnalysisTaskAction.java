﻿
package com.biolims.analysis.techanaly.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.analysis.techanaly.model.SampleTechAnalysisInfo;
import com.biolims.analysis.techanaly.model.TechAnalysisTask;
import com.biolims.analysis.techanaly.service.TechAnalysisTaskService;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/analysis/techanaly/techAnalysisTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class TechAnalysisTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "260104";
	@Autowired
	private TechAnalysisTaskService techAnalysisTaskService;
	private TechAnalysisTask techAnalysisTask = new TechAnalysisTask();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showTechAnalysisTaskList")
	public String showTechAnalysisTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/analysis/techanaly/techAnalysisTask.jsp");
	}

	@Action(value = "showTechAnalysisTaskListJson")
	public void showTechAnalysisTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = techAnalysisTaskService.findTechAnalysisTaskList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<TechAnalysisTask> list = (List<TechAnalysisTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("headUser", "");
		map.put("projectId", "");
		map.put("contractId", "");
		map.put("techTaskId", "");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "techAnalysisTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogTechAnalysisTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/analysis/techanaly/techAnalysisTaskDialog.jsp");
	}

	@Action(value = "showDialogTechAnalysisTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogTechAnalysisTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = techAnalysisTaskService.findTechAnalysisTaskList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<TechAnalysisTask> list = (List<TechAnalysisTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("headUser", "");
		map.put("projectId", "");
		map.put("contractId", "");
		map.put("techTaskId", "");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editTechAnalysisTask")
	public String editTechAnalysisTask() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			techAnalysisTask = techAnalysisTaskService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "techAnalysisTask");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//			techAnalysisTask.setCreateUser(user);
//			techAnalysisTask.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/analysis/techanaly/techAnalysisTaskEdit.jsp");
	}

	@Action(value = "copyTechAnalysisTask")
	public String copyTechAnalysisTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		techAnalysisTask = techAnalysisTaskService.get(id);
		techAnalysisTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/analysis/techanaly/techAnalysisTaskEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = techAnalysisTask.getId();
		if(id!=null&&id.equals("")){
			techAnalysisTask.setId(null);
		}
		Map aMap = new HashMap();
			aMap.put("sampleTechAnalysisInfo",getParameterFromRequest("sampleTechAnalysisInfoJson"));
		
		techAnalysisTaskService.save(techAnalysisTask,aMap);
		return redirect("/analysis/techanaly/techAnalysisTask/editTechAnalysisTask.action?id=" + techAnalysisTask.getId());

	}

	@Action(value = "viewTechAnalysisTask")
	public String toViewTechAnalysisTask() throws Exception {
		String id = getParameterFromRequest("id");
		techAnalysisTask = techAnalysisTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/analysis/techanaly/techAnalysisTaskEdit.jsp");
	}
	

	@Action(value = "showSampleTechAnalysisInfoList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleTechAnalysisInfoList() throws Exception {
		return dispatcher("/WEB-INF/page/analysis/techanaly/sampleTechAnalysisInfo.jsp");
	}

	@Action(value = "showSampleTechAnalysisInfoListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleTechAnalysisInfoListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = techAnalysisTaskService.findSampleTechAnalysisInfoList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<SampleTechAnalysisInfo> list = (List<SampleTechAnalysisInfo>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("code", "");
			map.put("sampleCode", "");
//			map.put("compareRates", "");
//			map.put("multiMapRates", "");
//			map.put("geneDifferNum", "");
//			map.put("coverage", "");
//			map.put("snpNum", "");
//			map.put("miRnaPrefer", "");
//			map.put("contigNum", "");
//			map.put("disHomo", "");
//			map.put("sampleCluster", "");
//			map.put("convertRates", "");
//			map.put("fragmentSize", "");
//			map.put("peaksNum", "");
//			map.put("motifResult", "");
//			map.put("geneRegionNum", "");
//			map.put("uniqueMappedRatio", "");
//			map.put("vaildPairdEndReads", "");
//			map.put("danglingEndPairdReads", "");
//			map.put("selfCirclePairdReads", "");
//			map.put("eriNum", "");
//			map.put("eriDis", "");
//			map.put("transCisRate", "");
//			map.put("catchSpecific", "");
//			map.put("snpNumLevel", "");
//			map.put("contigN50", "");
//			map.put("hostCompareRates", "");
//			map.put("kimerDepth", "");
//			map.put("singalIp", "");
//			map.put("gcDepth", "");
//			map.put("geneNum", "");
//			map.put("sample+spilice", "");
//			map.put("aug16sV3", "");
//			map.put("aug16sV4", "");
//			map.put("standardValue", "");
//			map.put("realValue", "");
			map.put("result", "");
			map.put("method", "");
			map.put("techAnalysisTask-name", "");
			map.put("techAnalysisTask-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delSampleTechAnalysisInfo")
	public void delSampleTechAnalysisInfo() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			techAnalysisTaskService.delSampleTechAnalysisInfo(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	@Action(value="getCsv")
	public void getCsv() throws Exception{
//		String taskId = getRequest().getParameter("taskId");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			techAnalysisTaskService.getCsvContent();
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
//加载图片
	@Action(value = "loadPicture")
	public String loadPicture() throws Exception {
		String scId = getRequest().getParameter("picId");
		putObjToContext("picId", "ftp://192.168.60.40/"+scId+".png");
		return dispatcher("/WEB-INF/page/analysis/techanaly/showPicList.jsp");
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public TechAnalysisTaskService getTechAnalysisTaskService() {
		return techAnalysisTaskService;
	}

	public void setTechAnalysisTaskService(TechAnalysisTaskService techAnalysisTaskService) {
		this.techAnalysisTaskService = techAnalysisTaskService;
	}

	public TechAnalysisTask getTechAnalysisTask() {
		return techAnalysisTask;
	}

	public void setTechAnalysisTask(TechAnalysisTask techAnalysisTask) {
		this.techAnalysisTask = techAnalysisTask;
	}


}
