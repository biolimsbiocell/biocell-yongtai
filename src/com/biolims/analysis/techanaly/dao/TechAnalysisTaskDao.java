package com.biolims.analysis.techanaly.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.analysis.filt.model.FiltrateTask;
import com.biolims.analysis.techanaly.model.SampleTechAnalysisInfo;
import com.biolims.analysis.techanaly.model.TechAnalysisTask;
import com.biolims.dao.BaseHibernateDao;

@Repository
@SuppressWarnings("unchecked")
public class TechAnalysisTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectTechAnalysisTaskList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from TechAnalysisTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<TechAnalysisTask> list = new ArrayList<TechAnalysisTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
		public Map<String, Object> selectSampleTechAnalysisInfoList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from SampleTechAnalysisInfo where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and techAnalysisTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleTechAnalysisInfo> list = new ArrayList<SampleTechAnalysisInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		
//		根据状态获取下机质控集合
		public List<TechAnalysisTask> findTaskByState(){
			String hql = "from TechAnalysisTask t where t.stateName=  '新建'";
			List<TechAnalysisTask> list = this.getSession().createQuery(hql).list();
			return list;
		}
}