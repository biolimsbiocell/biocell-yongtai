package com.biolims.analysis.techanaly.service;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.analysis.techanaly.dao.TechAnalysisTaskDao;
import com.biolims.analysis.techanaly.model.SampleTechAnalysisInfo;
import com.biolims.analysis.techanaly.model.TechAnalysisTask;
import com.biolims.common.FtpUtil;
import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.util.JsonUtils;
import com.csvreader.CsvReader;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class TechAnalysisTaskService {
	@Resource
	private TechAnalysisTaskDao techAnalysisTaskDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findTechAnalysisTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return techAnalysisTaskDao.selectTechAnalysisTaskList(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(TechAnalysisTask i) throws Exception {

		techAnalysisTaskDao.saveOrUpdate(i);

	}

	public TechAnalysisTask get(String id) {
		TechAnalysisTask techAnalysisTask = commonDAO.get(
				TechAnalysisTask.class, id);
		return techAnalysisTask;
	}

	public Map<String, Object> findSampleTechAnalysisInfoList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = techAnalysisTaskDao
				.selectSampleTechAnalysisInfoList(scId, startNum, limitNum,
						dir, sort);
		List<SampleTechAnalysisInfo> list = (List<SampleTechAnalysisInfo>) result
				.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleTechAnalysisInfo(TechAnalysisTask sc,
			String itemDataJson) throws Exception {
		List<SampleTechAnalysisInfo> saveItems = new ArrayList<SampleTechAnalysisInfo>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SampleTechAnalysisInfo scp = new SampleTechAnalysisInfo();
			// 将map信息读入实体类
			scp = (SampleTechAnalysisInfo) techAnalysisTaskDao.Map2Bean(map,
					scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setTechAnalysisTask(sc);

			saveItems.add(scp);
		}
		techAnalysisTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSampleTechAnalysisInfo(String[] ids) throws Exception {
		for (String id : ids) {
			SampleTechAnalysisInfo scp = techAnalysisTaskDao.get(
					SampleTechAnalysisInfo.class, id);
			techAnalysisTaskDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(TechAnalysisTask sc, Map jsonMap) throws Exception {
		if (sc != null) {
			techAnalysisTaskDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("sampleTechAnalysisInfo");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSampleTechAnalysisInfo(sc, jsonStr);
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void getCsvContent() throws Exception {
		// Map<String, String> mapForQuery = new HashMap<String, String>();
		// mapForQuery.put("task.id", taskId);
		// DeSequencingTask dst = commonDAO.get(DeSequencingTask.class, taskId);

		List<TechAnalysisTask> listDes = this.techAnalysisTaskDao
				.findTaskByState();
		for (TechAnalysisTask dst1 : listDes) {
			// String filePath =
			// com.biolims.system.code.SystemConstants.SEQ_QC_PATH+"\\"+dst1.getFlowCode();
			String path = "";
			// File dir = new File(filePath);
			// if (dir.exists() && dir.isDirectory() && dir.listFiles().length >
			// 0) {
			// File[] fm = dir.listFiles();
			// for (File file : fm) {
			// if(dir.isDirectory()){

			// 如果目录存在 进行读取操作
			// path = filePath ;
			// File a = new File(path+ "\\fulltable.csv");
			// if(a.isFile()){
			// InputStream is = new FileInputStream(path+ "\\fulltable.csv");
			InputStream is = FtpUtil.downFile(
					dst1.getContractId() + "/" + dst1.getProjectId() + "/"
							+ dst1.getHeadUser(), "fulltable.csv");
			if (is != null) {
				CsvReader reader = new CsvReader(is, Charset.forName("GBK"));
				reader.readHeaders();// 去除表头
				while (reader.readRecord()) {// 下机质控明细
					SampleTechAnalysisInfo item = new SampleTechAnalysisInfo();
					item.setName(reader.get(reader.getHeader(0)));

					item.setTechAnalysisTask(dst1);
					this.techAnalysisTaskDao.saveOrUpdate(item);

				}
				reader.close();
			}
			// //读取完成 改变表的状态为 “完成下机质控”
			// dst.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_SEQC_COMPLETE);
			dst1.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_TQ_DATA_NAME);
			this.techAnalysisTaskDao.saveOrUpdate(dst1);
			// }
			// }
		}

	}
}
