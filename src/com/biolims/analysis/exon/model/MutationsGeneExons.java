package com.biolims.analysis.exon.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.common.model.user.User;
/**   
 * @Title: Model
 * @Description: 突变基因转录本及总外显子表
 * @author lims-platform
 * @date 2016-04-08 13:56:19
 * @version V1.0   
 *
 */
@Entity
@Table(name = "MUTATION_GENE_EXONS")
@SuppressWarnings("serial")
public class MutationsGeneExons extends EntityDao<MutationsGeneExons> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**突变基因*/
	private String mutationGenes;
	/**转录本*/
	private String transcript;
	/**总外显子数*/
	private String totleNumberExon;
	/**时间*/
	private Date createDate;
	/**创建人*/
	private User createUser;
	/**工作流状态*/
	private String state;
	/**状态描述*/
	private String stateName;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  突变基因
	 */
	@Column(name ="MUTATION_GENES", length = 50)
	public String getMutationGenes(){
		return this.mutationGenes;
	}
	/**
	 *方法: 设置String
	 *@param: String  突变基因
	 */
	public void setMutationGenes(String mutationGenes){
		this.mutationGenes = mutationGenes;
	}
	/**
	 *方法: 取得String
	 *@return: String  转录本
	 */
	@Column(name ="TRANSCRIPT", length = 50)
	public String getTranscript(){
		return this.transcript;
	}
	/**
	 *方法: 设置String
	 *@param: String  转录本
	 */
	public void setTranscript(String transcript){
		this.transcript = transcript;
	}
	/**
	 *方法: 取得String
	 *@return: String  总外显子数
	 */
	@Column(name ="TOTLE_NUMBER_EXON", length = 50)
	public String getTotleNumberExon(){
		return this.totleNumberExon;
	}
	/**
	 *方法: 设置String
	 *@param: String  总外显子数
	 */
	public void setTotleNumberExon(String totleNumberExon){
		this.totleNumberExon = totleNumberExon;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  时间
	 */
	@Column(name ="CREATE_DATE", length = 100)
	public Date getCreateDate(){
		return this.createDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 *方法: 取得User
	 *@return: User  创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser(){
		return this.createUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  创建人
	 */
	public void setCreateUser(User createUser){
		this.createUser = createUser;
	}
	/**
	 *方法: 取得String
	 *@return: String  工作流状态
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  工作流状态
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态描述
	 */
	@Column(name ="STATE_NAME", length = 50)
	public String getStateName(){
		return this.stateName;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态描述
	 */
	public void setStateName(String stateName){
		this.stateName = stateName;
	}
}