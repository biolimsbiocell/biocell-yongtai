package com.biolims.analysis.exon.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.analysis.exon.dao.MutationsGeneExonsDao;
import com.biolims.analysis.exon.model.MutationsGeneExons;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class MutationsGeneExonsService {
	@Resource
	private MutationsGeneExonsDao mutationsGeneExonsDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findMutationsGeneExonsList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return mutationsGeneExonsDao.selectMutationsGeneExonsList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(MutationsGeneExons i) throws Exception {

		mutationsGeneExonsDao.saveOrUpdate(i);

	}
	public MutationsGeneExons get(String id) {
		MutationsGeneExons mutationsGeneExons = commonDAO.get(MutationsGeneExons.class, id);
		return mutationsGeneExons;
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(MutationsGeneExons sc, Map jsonMap) throws Exception {
		if (sc != null) {
			mutationsGeneExonsDao.saveOrUpdate(sc);
		
			String jsonStr = "";
	}
   }
}
