package com.biolims.analysis.exon.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.analysis.exon.model.MutationsGeneExons;
import com.biolims.analysis.exon.service.MutationsGeneExonsService;
import com.biolims.util.DateHelper;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.util.HttpUtils;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
@Namespace("/analysis/exon/mutationsGeneExons")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class MutationsGeneExonsAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "260106";
	@Autowired
	private MutationsGeneExonsService mutationsGeneExonsService;
	private MutationsGeneExons mutationsGeneExons = new MutationsGeneExons();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showMutationsGeneExonsList")
	public String showMutationsGeneExonsList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/analysis/exon/mutationsGeneExons.jsp");
	}

	@Action(value = "showMutationsGeneExonsListJson")
	public void showMutationsGeneExonsListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = mutationsGeneExonsService.findMutationsGeneExonsList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<MutationsGeneExons> list = (List<MutationsGeneExons>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("mutationGenes", "");
		map.put("transcript", "");
		map.put("totleNumberExon", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "mutationsGeneExonsSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogMutationsGeneExonsList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/analysis/exon/mutationsGeneExonsDialog.jsp");
	}

	@Action(value = "showDialogMutationsGeneExonsListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogMutationsGeneExonsListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = mutationsGeneExonsService.findMutationsGeneExonsList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<MutationsGeneExons> list = (List<MutationsGeneExons>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("mutationGenes", "");
		map.put("transcript", "");
		map.put("totleNumberExon", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editMutationsGeneExons")
	public String editMutationsGeneExons() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			mutationsGeneExons = mutationsGeneExonsService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "mutationsGeneExons");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			mutationsGeneExons.setCreateUser(user);
			mutationsGeneExons.setCreateDate(new Date());
			mutationsGeneExons.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			mutationsGeneExons.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/analysis/exon/mutationsGeneExonsEdit.jsp");
	}

	@Action(value = "copyMutationsGeneExons")
	public String copyMutationsGeneExons() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		mutationsGeneExons = mutationsGeneExonsService.get(id);
		mutationsGeneExons.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/analysis/exon/mutationsGeneExonsEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = mutationsGeneExons.getId();
		if(id!=null&&id.equals("")){
			mutationsGeneExons.setId(null);
		}
		Map aMap = new HashMap();
		mutationsGeneExonsService.save(mutationsGeneExons,aMap);
		return redirect("/analysis/exon/mutationsGeneExons/editMutationsGeneExons.action?id=" + mutationsGeneExons.getId());

	}

	@Action(value = "viewMutationsGeneExons")
	public String toViewMutationsGeneExons() throws Exception {
		String id = getParameterFromRequest("id");
		mutationsGeneExons = mutationsGeneExonsService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/analysis/exon/mutationsGeneExonsEdit.jsp");
	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public MutationsGeneExonsService getMutationsGeneExonsService() {
		return mutationsGeneExonsService;
	}

	public void setMutationsGeneExonsService(MutationsGeneExonsService mutationsGeneExonsService) {
		this.mutationsGeneExonsService = mutationsGeneExonsService;
	}

	public MutationsGeneExons getMutationsGeneExons() {
		return mutationsGeneExons;
	}

	public void setMutationsGeneExons(MutationsGeneExons mutationsGeneExons) {
		this.mutationsGeneExons = mutationsGeneExons;
	}


}
