package com.biolims.analysis.data.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.analysis.data.service.DataTaskGetService;
import com.biolims.common.interfaces.ObjectEvent;

public class analysisDataGetEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		DataTaskGetService mbService = (DataTaskGetService) ctx
				.getBean("dataTaskGetService");
		String text = mbService.changeState(applicationTypeActionId, contentId);

		return text;
	}
}
