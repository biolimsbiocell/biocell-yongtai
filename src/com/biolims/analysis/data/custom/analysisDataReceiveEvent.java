package com.biolims.analysis.data.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.analysis.data.service.DataTaskReceiveService;
import com.biolims.common.interfaces.ObjectEvent;

public class analysisDataReceiveEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId)
			throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		DataTaskReceiveService mbService = (DataTaskReceiveService) ctx
				.getBean("dataTaskReceiveService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "成功";
	}
}
