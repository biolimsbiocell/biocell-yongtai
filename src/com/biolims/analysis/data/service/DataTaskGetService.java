package com.biolims.analysis.data.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.analysis.data.dao.DataTaskDao;
import com.biolims.analysis.data.model.CnvResult;
import com.biolims.analysis.data.model.DataTask;
import com.biolims.analysis.data.model.DataTaskItem;
import com.biolims.analysis.data.model.DataTaskSvItem;
import com.biolims.analysis.data.model.DataTaskSvnItem;
import com.biolims.analysis.data.model.DateTaskCnvItem;
import com.biolims.analysis.data.model.SnvResult;
import com.biolims.analysis.data.model.SvResult;
import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.core.userGroup.service.UserGroupService;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.report.model.SampleReportTemp;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.util.DateUtil;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class DataTaskGetService {
	@Resource
	private DataTaskDao dataTaskDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private UserGroupService userGroupService;
	@Resource
	private CommonService commonService;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findDataTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return dataTaskDao.selectDataTaskList(mapForQuery, startNum, limitNum,
				dir, sort);

	}

	// SVNItem子表信息审核
	public void checkedSvn(List<DataTaskSvnItem> svn1,
			List<DataTaskSvnItem> svn2, DataTaskItem di) {
		for (DataTaskSvnItem s1 : svn1) {
			for (DataTaskSvnItem s2 : svn2) {
				if (s1.getMutantGenes().equals(s2.getMutantGenes())) {
					// 审核通过
					di.setStates("2");
					di.setMethod("1");
					s1.setValidState("1");
					s1.setAccordance("1");
					s2.setValidState("1");
					s2.setAccordance("1");
					this.commonDAO.saveOrUpdate(s1);
					this.commonDAO.saveOrUpdate(s2);
				} else {
					// 审核不通过
					di.setStates("1");
					di.setMethod("0");
					s1.setAccordance("0");
					s2.setAccordance("0");
					this.commonDAO.saveOrUpdate(s1);
					this.commonDAO.saveOrUpdate(s2);
				}
				this.commonDAO.saveOrUpdate(di);
			}
		}
	}

	// SVItem子表信息审核
	public void checkedSv(List<DataTaskSvItem> sv1, List<DataTaskSvItem> sv2,
			DataTaskItem di) {
		for (DataTaskSvItem s1 : sv1) {
			for (DataTaskSvItem s2 : sv2) {
				if (s1.getMutantGenes().equals(s2.getMutantGenes())) {
					// 审核通过
					di.setStates("2");
					s1.setValidState("1");
					s1.setAccordance("1");
					s2.setAccordance("1");
					this.commonDAO.saveOrUpdate(s1);
					this.commonDAO.saveOrUpdate(s2);
				} else {
					// 审核不通过
					di.setStates("1");
					s1.setAccordance("0");
					s2.setAccordance("0");
					this.commonDAO.saveOrUpdate(s1);
					this.commonDAO.saveOrUpdate(s2);
				}
				this.commonDAO.saveOrUpdate(di);
			}
		}
	}

	// CnvItem子表信息审核
	public void checkedCnv(List<DateTaskCnvItem> cnv1,
			List<DateTaskCnvItem> cnv2, DataTaskItem di) {
		for (DateTaskCnvItem s1 : cnv1) {
			for (DateTaskCnvItem s2 : cnv2) {
				if (s1.getMutantGenes().equals(s2.getMutantGenes())) {
					// 审核通过
					di.setStates("2");
					s1.setValidState("1");
					s1.setAccordance("1");
					s2.setAccordance("1");
					this.commonDAO.saveOrUpdate(s1);
					this.commonDAO.saveOrUpdate(s2);
				} else {
					// 审核不通过
					di.setStates("1");
					s1.setAccordance("0");
					s2.setAccordance("0");
					this.commonDAO.saveOrUpdate(s1);
					this.commonDAO.saveOrUpdate(s2);
				}
				this.commonDAO.saveOrUpdate(di);
			}
		}
	}

	// 审批完成
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String changeState(String applicationTypeActionId, String id)
			throws Exception {
		// 得到DataTask对象
		try {
			DataTask sct = this.dataTaskDao.get(DataTask.class, id);
			sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
			sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
			/**
			 * 对比后有效数据添加到结果表
			 */

			List<DataTaskItem> l = queryDateTaskItemList(id);
			List<String> orderList = new ArrayList<String>();
			for (DataTaskItem qis : l) {
				SampleReportTemp st = new SampleReportTemp();

				SampleInfo si = new SampleInfo();
				st.setSampleCode(qis.getSampleCode());
				List<SampleInfo> sil = commonService.get(SampleInfo.class,
						"code", qis.getSampleCode());
				if (sil.size() > 0)
					si = sil.get(0);

				if ((si.getOrderNum() != null)
						&& !orderList.contains(si.getOrderNum())) {

					st.setPatientName(si.getPatientName());
					st.setProductId(si.getProductId());
					st.setProductName(si.getProductName());
					st.setReportDate(DateUtil.parse(si.getReportDate()));
					st.setOrderNum(si.getOrderNum());
					st.setNote(qis.getNote());
					st.setState("1");
					commonDAO.saveOrUpdate(st);
					orderList.add(si.getOrderNum());
				}
			}
			// 碱基突变表
			List<DataTaskSvnItem> list_snv = this.dataTaskDao
					.selDataTaskSvnItemList(id);
			for (DataTaskSvnItem d : list_snv) {
				// 根据样本号查询SampleInfo
				SampleInfo s = this.sampleInfoMainDao.findSampleInfo(d
						.getSampleCode());
				SnvResult snv = new SnvResult();
				sampleInputService.copy(snv, d);
				snv.setCode(d.getSampleCode());
				snv.setOrderCode(s.getSampleOrder().getId());
				snv.setMedicalNumber(s.getSampleOrder().getMedicalNumber());
				snv.setPatientName(s.getPatientName());
				// snv.setMutantGenes(d.getMutantGenes());
				// snv.setOtherName(d.getOtherName());
				// snv.setTranscript(d.getTranscript());
				// snv.setMutantGeneLc(d.getMutantGeneLc());
				// snv.setAminoAcidMutation(d.getAminoAcidMutation());
				// snv.setBaseMutation(d.getBaseMutation());
				// snv.setReferenceBase(d.getReferenceBase());
				// snv.setMutantBase(d.getMutantBase());
				// snv.setMutationStartPosition(d.getMutationStartPosition());
				// snv.setMutationStopPosition(d.getMutationStopPosition());
				// snv.setMutationClass(d.getMutationClass());
				// snv.setMutationType(d.getMutationType());
				// snv.setMutationStatus(d.getMutationStatus());
				// snv.setMutatinAbundance(d.getMutatinAbundance());
				// snv.setRelatedMutation(d.getRelatedMutation());
				// snv.setRelatedChemotherapy(d.getRelatedChemotherapy());
				snv.setDataTask(sct);
				this.dataTaskDao.saveOrUpdate(snv);
			}
			// 基因融合突变表
			List<DataTaskSvItem> list_sv = this.dataTaskDao
					.selDataTaskSvItemList(id);
			for (DataTaskSvItem d : list_sv) {
				// 根据样本号查询SampleInfo
				SampleInfo s = this.sampleInfoMainDao.findSampleInfo(d
						.getSampleCode());
				SvResult sv = new SvResult();
				sampleInputService.copy(sv, d);
				sv.setCode(d.getSampleCode());
				sv.setOrderCode(s.getSampleOrder().getId());
				sv.setMedicalNumber(s.getSampleOrder().getMedicalNumber());
				sv.setPatientName(s.getPatientName());
				sv.setDataTask(sct);
				this.dataTaskDao.saveOrUpdate(sv);
			}
			// 拷贝数变异
			List<DateTaskCnvItem> list_cnv = this.dataTaskDao
					.selDateTaskCnvItemList(id);
			for (DateTaskCnvItem d : list_cnv) {
				// 根据样本号查询SampleInfo
				SampleInfo s = this.sampleInfoMainDao.findSampleInfo(d
						.getSampleCode());
				CnvResult cnv = new CnvResult();
				sampleInputService.copy(cnv, d);
				cnv.setCode(d.getSampleCode());
				cnv.setOrderCode(s.getSampleOrder().getId());
				cnv.setMedicalNumber(s.getSampleOrder().getMedicalNumber());
				cnv.setPatientName(s.getPatientName());
				cnv.setDataTask(sct);
				this.dataTaskDao.saveOrUpdate(cnv);
			}
			// 根据主表查询分析明细
			List<DataTaskItem> listItem = this.dataTaskDao
					.selectDataTaskItemList(id);
			for (DataTaskItem d : listItem) {
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				sampleStateService
						.saveSampleState(
								d.getSampleId(),
								d.getSampleCode(),
								d.getProductId(),
								d.getProductName(),
								"",
								format.format(sct.getCreateDate()),
								format.format(new Date()),
								"DataTask",
								"突变筛选",
								(User) ServletActionContext
										.getRequest()
										.getSession()
										.getAttribute(
												SystemConstants.USER_SESSION_KEY),
								sct.getId(), "出报告", null, null, null, null,
								null, null, null, null, null);
			}
			// /**
			// * 开始审核数据
			// */
			// String userOne = "";
			// String userTwo = "";
			// String sampleCode = "";
			// List<DataTaskItem> dataTaskItem = queryDateTaskItemList(id);
			// for (DataTaskItem di : dataTaskItem) {
			// this.dataTaskDao.saveReportTemp(di);
			// // 得到两个实验员
			// userOne = di.getAcceptUser().getId();
			// userTwo = di.getAcceptUser2().getId();
			// sampleCode = di.getSampleId();
			// // 调用子表信息svn
			// List<DataTaskSvnItem> svn1 = qDataTaskSvnItemList(id, userOne,
			// sampleCode);
			// List<DataTaskSvnItem> svn2 = qDataTaskSvnItemList(id, userTwo,
			// sampleCode);
			// checkedSvn(svn1, svn2, di);
			// // 调用子表信息sv
			// List<DataTaskSvItem> sv1 = qDataTaskSvItemList(id, userOne,
			// sampleCode);
			// List<DataTaskSvItem> sv2 = qDataTaskSvItemList(id, userTwo,
			// sampleCode);
			// checkedSv(sv1, sv2, di);
			// // 调用子表信息cnv
			// List<DateTaskCnvItem> cnv1 = qDateTaskCnvItemList(id, userOne,
			// sampleCode);
			// List<DateTaskCnvItem> cnv2 = qDateTaskCnvItemList(id, userTwo,
			// sampleCode);
			// checkedCnv(cnv1, cnv2, di);
			//
			// }
			// /**
			// * 循环赋值到已审核突变筛选
			// *
			// */
			// fuzhi(id);
			//
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "失败";
		}
		return "成功";
	}

	// // 循环赋值
	// public void fuzhi(String id) throws Exception{
	// //查询字表记录1
	// List<DataTaskSvnItem> Svnlist = selectDataTaskSvnItemListEvent(id);
	// /**
	// * 循环判断添加字段
	// */
	// for (DataTaskSvnItem svn : Svnlist) {
	// if("1".equals(svn.getValidState())){
	// DataTaskSvnItemChecked data = new DataTaskSvnItemChecked();
	// copySvn(data,svn);
	// }
	// }
	// //查询字表记录2
	// List<DataTaskSvItem> Svlist = selectDataTaskSvItemListEvent(id);
	// /**
	// * 循环判断添加字段
	// */
	// for (DataTaskSvItem sv : Svlist) {
	// if("1".equals(sv.getValidState())){
	// DataTaskSvItemChecked data = new DataTaskSvItemChecked();
	// copySv(data,sv);
	// }
	// }
	// //查询字表记录3
	// List<DateTaskCnvItem> Cnvlist = selectDateTaskCnvItemListEvent(id);
	// /**
	// * 循环判断添加字段
	// */
	// for (DateTaskCnvItem cnv : Cnvlist) {
	// if("1".equals(cnv.getValidState())){
	// DateTaskCnvItemChecked data = new DateTaskCnvItemChecked();
	// copyCnv(data,cnv);
	// }
	// }
	// }
	//
	// public void copySvn(Object modelOne, Object modelTwo) throws Exception {
	// Map<String, Object> inputMap = new HashMap<String, Object>();
	// Field[] inputFields = modelOne.getClass().getDeclaredFields();
	// for (Field fd : inputFields) {
	// if(fd.getName().equals("id")
	// ||fd.getName().equals("acceptUser")
	// ||fd.getName().equals("validState")
	// ){
	// continue;
	// }
	// try {
	// inputMap.put(fd.getName(),
	// BeanUtils.getFieldValue(modelTwo, fd.getName()));
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	// Field[] inputTempFields = modelOne.getClass().getDeclaredFields();
	// for (Field fd : inputTempFields) {
	// if (inputMap.get(fd.getName()) == null) {
	// continue;
	// }
	// try {
	// // 给属性赋值
	// BeanUtils.setFieldValue(modelOne, fd.getName(),
	// inputMap.get(fd.getName()));
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	// //两条记录基本一致，直接修改订单纪录
	// DataTaskSvnItemChecked svndate = (DataTaskSvnItemChecked) modelOne;
	// this.dataTaskDao.merge(svndate);
	//
	// }
	// public void copySv(Object modelOne, Object modelTwo) throws Exception {
	// Map<String, Object> inputMap = new HashMap<String, Object>();
	// Field[] inputFields = modelOne.getClass().getDeclaredFields();
	// for (Field fd : inputFields) {
	// if(fd.getName().equals("id")
	// ||fd.getName().equals("acceptUser")
	// ||fd.getName().equals("validState")
	// ){
	// continue;
	// }
	// try {
	// inputMap.put(fd.getName(),
	// BeanUtils.getFieldValue(modelTwo, fd.getName()));
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	// Field[] inputTempFields = modelOne.getClass().getDeclaredFields();
	// for (Field fd : inputTempFields) {
	// if (inputMap.get(fd.getName()) == null) {
	// continue;
	// }
	// try {
	// // 给属性赋值
	// BeanUtils.setFieldValue(modelOne, fd.getName(),
	// inputMap.get(fd.getName()));
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	// //两条记录基本一致，直接修改订单纪录
	// DataTaskSvItemChecked svdate = (DataTaskSvItemChecked) modelOne;
	// this.dataTaskDao.merge(svdate);
	//
	// }
	//
	// public void copyCnv(Object modelOne, Object modelTwo) throws Exception {
	// Map<String, Object> inputMap = new HashMap<String, Object>();
	// Field[] inputFields = modelOne.getClass().getDeclaredFields();
	// for (Field fd : inputFields) {
	// if(fd.getName().equals("id")
	// ||fd.getName().equals("acceptUser")
	// ||fd.getName().equals("validState")
	// ){
	// continue;
	// }
	// try {
	// inputMap.put(fd.getName(),
	// BeanUtils.getFieldValue(modelTwo, fd.getName()));
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	// Field[] inputTempFields = modelOne.getClass().getDeclaredFields();
	// for (Field fd : inputTempFields) {
	// if (inputMap.get(fd.getName()) == null) {
	// continue;
	// }
	// try {
	// // 给属性赋值
	// BeanUtils.setFieldValue(modelOne, fd.getName(),
	// inputMap.get(fd.getName()));
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	// //两条记录基本一致，直接修改订单纪录
	// DateTaskCnvItemChecked cnvdate = (DateTaskCnvItemChecked) modelOne;
	// this.dataTaskDao.merge(cnvdate);
	//
	// }

	/**
	 * ===========================================================添加字表查询方法
	 */
	public List<DataTaskSvnItem> selectDataTaskSvnItemListEvent(String scId)
			throws Exception {
		String hql = "from DataTaskSvnItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and dataTask.id='" + scId + "'";
		List<DataTaskSvnItem> list = new ArrayList<DataTaskSvnItem>();
		list = this.dataTaskDao.getSession().createQuery(hql + key).list();
		return list;
	}

	public List<DataTaskSvItem> selectDataTaskSvItemListEvent(String scId)
			throws Exception {
		String hql = "from DataTaskSvItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and dataTask.id='" + scId + "'";
		List<DataTaskSvItem> list = new ArrayList<DataTaskSvItem>();
		list = this.dataTaskDao.getSession().createQuery(hql + key).list();
		return list;
	}

	public List<DateTaskCnvItem> selectDateTaskCnvItemListEvent(String scId)
			throws Exception {
		String hql = "from DateTaskCnvItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and dataTask.id='" + scId + "'";
		List<DateTaskCnvItem> list = new ArrayList<DateTaskCnvItem>();
		list = this.dataTaskDao.getSession().createQuery(hql + key).list();
		return list;
	}

	/**
	 * ===============================================查询数据分析明细
	 * 
	 * @return
	 */
	public List<DataTaskItem> queryDateTaskItemList(String scId)
			throws Exception {
		String hql = "from DataTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and dataTask.id='" + scId + "'";
		List<DataTaskItem> list = new ArrayList<DataTaskItem>();
		list = this.dataTaskDao.getSession().createQuery(hql + key).list();
		return list;
	}

	/**
	 * ===========================================================添加字表查询方法
	 */
	public List<DataTaskSvnItem> qDataTaskSvnItemList(String scId,
			String userId, String sampleCode) throws Exception {
		String hql = "from DataTaskSvnItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and dataTask.id='" + scId + "' and acceptUser.id = '"
					+ userId + "' and sampleCode = '" + sampleCode + "'";
		List<DataTaskSvnItem> list = new ArrayList<DataTaskSvnItem>();
		list = this.dataTaskDao.getSession().createQuery(hql + key).list();
		return list;
	}

	public List<DataTaskSvItem> qDataTaskSvItemList(String scId, String userId,
			String sampleCode) throws Exception {
		String hql = "from DataTaskSvItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and dataTask.id='" + scId + "' and acceptUser.id = '"
					+ userId + "' and sampleCode = '" + sampleCode + "'";
		List<DataTaskSvItem> list = new ArrayList<DataTaskSvItem>();
		list = this.dataTaskDao.getSession().createQuery(hql + key).list();
		return list;
	}

	public List<DateTaskCnvItem> qDateTaskCnvItemList(String scId,
			String userId, String sampleCode) throws Exception {
		String hql = "from DateTaskCnvItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and dataTask.id='" + scId + "' and acceptUser.id = '"
					+ userId + "' and sampleCode = '" + sampleCode + "'";
		List<DateTaskCnvItem> list = new ArrayList<DateTaskCnvItem>();
		list = this.dataTaskDao.getSession().createQuery(hql + key).list();
		return list;
	}

	public DataTaskDao getDataTaskDao() {
		return dataTaskDao;
	}

	public void setDataTaskDao(DataTaskDao dataTaskDao) {
		this.dataTaskDao = dataTaskDao;
	}

	public CommonDAO getCommonDAO() {
		return commonDAO;
	}

	public void setCommonDAO(CommonDAO commonDAO) {
		this.commonDAO = commonDAO;
	}

	public UserGroupService getUserGroupService() {
		return userGroupService;
	}

	public void setUserGroupService(UserGroupService userGroupService) {
		this.userGroupService = userGroupService;
	}

	public CommonService getCommonService() {
		return commonService;
	}

	public void setCommonService(CommonService commonService) {
		this.commonService = commonService;
	}

}
