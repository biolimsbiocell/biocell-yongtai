package com.biolims.analysis.data.service;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.analysis.data.dao.DataTaskDao;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.service.CommonService;
import com.biolims.core.userGroup.service.UserGroupService;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class DataTaskReceiveService {
	@Resource
	private DataTaskDao dataTaskDao;
	@Resource
	private CommonDAO commonDAO;
	
	@Resource
	private UserGroupService userGroupService;
	@Resource
	private CommonService commonService;
	
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findDataTaskList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return dataTaskDao.selectDataTaskList(mapForQuery, startNum, limitNum, dir, sort);
	}
	
	// 审批完成
	public void changeState(String applicationTypeActionId, String id) {
		System.out.println("接收");
	}
}
