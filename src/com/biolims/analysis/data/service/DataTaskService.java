package com.biolims.analysis.data.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.activiti.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.analysis.data.dao.DataTaskDao;
import com.biolims.analysis.data.model.DataTask;
import com.biolims.analysis.data.model.DataTaskItem;
import com.biolims.analysis.data.model.DataTaskSvItem;
import com.biolims.analysis.data.model.DataTaskSvItemChecked;
import com.biolims.analysis.data.model.DataTaskSvnItem;
import com.biolims.analysis.data.model.DataTaskSvnItemChecked;
import com.biolims.analysis.data.model.DataTaskTemp;
import com.biolims.analysis.data.model.DateTaskCnvItem;
import com.biolims.analysis.data.model.DateTaskCnvItemChecked;
import com.biolims.analysis.exon.model.MutationsGeneExons;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.service.CommonService;
import com.biolims.core.userGroup.service.UserGroupService;
import com.biolims.experiment.dna.model.DnaTaskInfo;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.report.model.SampleReportTemp;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class DataTaskService {
	@Resource
	private DataTaskDao dataTaskDao;
	@Resource
	private CommonDAO commonDAO;

	@Resource
	private UserGroupService userGroupService;
	@Resource
	private CommonService commonService;

	StringBuffer json = new StringBuffer();

	public Map<String, Object> findDataTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return dataTaskDao.selectDataTaskList(mapForQuery, startNum, limitNum,
				dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(DataTask i) throws Exception {

		dataTaskDao.saveOrUpdate(i);

	}

	public DataTask get(String id) {
		DataTask dataTask = commonDAO.get(DataTask.class, id);
		return dataTask;
	}

	public Map<String, Object> findDataTaskSvnItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = dataTaskDao.selectDataTaskSvnItemList(
				scId, startNum, limitNum, dir, sort);
		List<DataTaskSvnItem> list = (List<DataTaskSvnItem>) result.get("list");
		return result;
	}

	/**
	 * 查询突变筛选的字表svnItem2
	 * 
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */

	public Map<String, Object> findDataTaskSvnItemList2(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = dataTaskDao.selectDataTaskSvnItemList2(
				scId, startNum, limitNum, dir, sort);
		List<DataTaskSvnItem> list = (List<DataTaskSvnItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findDataTaskSvItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = dataTaskDao.selectDataTaskSvItemList(scId,
				startNum, limitNum, dir, sort);
		List<DataTaskSvItem> list = (List<DataTaskSvItem>) result.get("list");
		return result;
	}

	/**
	 * 查询突变筛选的字表svItem2
	 * 
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findDataTaskSvItemList2(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = dataTaskDao.selectDataTaskSvItemList2(
				scId, startNum, limitNum, dir, sort);
		List<DataTaskSvItem> list = (List<DataTaskSvItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findDateTaskCnvItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = dataTaskDao.selectDateTaskCnvItemList(
				scId, startNum, limitNum, dir, sort);
		List<DateTaskCnvItem> list = (List<DateTaskCnvItem>) result.get("list");
		return result;
	}

	/**
	 * 查询突变筛选的字表cnvItem2
	 * 
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findDateTaskCnvItemList2(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = dataTaskDao.selectDateTaskCnvItemList2(
				scId, startNum, limitNum, dir, sort);
		List<DateTaskCnvItem> list = (List<DateTaskCnvItem>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveDataTaskSvnItem(DataTask sc, String itemDataJson)
			throws Exception {
		List<DataTaskSvnItem> saveItems = new ArrayList<DataTaskSvnItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			DataTaskSvnItem scp = new DataTaskSvnItem();
			// 将map信息读入实体类
			scp = (DataTaskSvnItem) dataTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			// if
			// (scp.getAcceptUser().getId()!=null&&scp.getAcceptUser().getId().equals(""))
			// scp.setAcceptUser(null);
			scp.setDataTask(sc);

			saveItems.add(scp);
		}
		dataTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 保存审核明细SVNitem
	 * 
	 * @param sc
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSvnItem(String itemDataJson) throws Exception {
		List<DataTaskSvnItem> saveItems = new ArrayList<DataTaskSvnItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			DataTaskSvnItem scp = new DataTaskSvnItem();
			// 将map信息读入实体类
			scp = (DataTaskSvnItem) dataTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			// scp.setDataTask(sc);
			if (scp.getAcceptUser().getId() != null
					&& scp.getAcceptUser().getId().equals(""))
				scp.setAcceptUser(null);

			saveItems.add(scp);
		}
		dataTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDataTaskSvnItem(String[] ids) throws Exception {
		for (String id : ids) {
			DataTaskSvnItem scp = dataTaskDao.get(DataTaskSvnItem.class, id);
			dataTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveDataTaskSvItem(DataTask sc, String itemDataJson)
			throws Exception {
		List<DataTaskSvItem> saveItems = new ArrayList<DataTaskSvItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			DataTaskSvItem scp = new DataTaskSvItem();
			// 将map信息读入实体类
			scp = (DataTaskSvItem) dataTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			// if
			// (scp.getAcceptUser().getId()!=null&&scp.getAcceptUser().getId().equals(""))
			// scp.setAcceptUser(null);
			scp.setDataTask(sc);

			saveItems.add(scp);
		}
		dataTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 保存svitem明细
	 * 
	 * @param sc
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSvItem(String itemDataJson) throws Exception {
		List<DataTaskSvItem> saveItems = new ArrayList<DataTaskSvItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			DataTaskSvItem scp = new DataTaskSvItem();
			// 将map信息读入实体类
			scp = (DataTaskSvItem) dataTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			if (scp.getAcceptUser().getId() != null
					&& scp.getAcceptUser().getId().equals(""))
				scp.setAcceptUser(null);

			// scp.setDataTask(sc);

			saveItems.add(scp);
		}
		dataTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDataTaskSvItem(String[] ids) throws Exception {
		for (String id : ids) {
			DataTaskSvItem scp = dataTaskDao.get(DataTaskSvItem.class, id);
			dataTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveDateTaskCnvItem(DataTask sc, String itemDataJson)
			throws Exception {
		List<DateTaskCnvItem> saveItems = new ArrayList<DateTaskCnvItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			DateTaskCnvItem scp = new DateTaskCnvItem();
			// 将map信息读入实体类
			scp = (DateTaskCnvItem) dataTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setDataTask(sc);

			saveItems.add(scp);
		}
		dataTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 保存明细cnvitem
	 * 
	 * @param sc
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCnvItem(String itemDataJson) throws Exception {
		List<DateTaskCnvItem> saveItems = new ArrayList<DateTaskCnvItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			DateTaskCnvItem scp = new DateTaskCnvItem();
			// 将map信息读入实体类
			scp = (DateTaskCnvItem) dataTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			if (scp.getAcceptUser().getId() != null
					&& scp.getAcceptUser().getId().equals(""))
				scp.setAcceptUser(null);
			// scp.setDataTask(sc);

			saveItems.add(scp);
		}
		dataTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 病人信息
	 * 
	 * @param id
	 * @return
	 */
//	public Map<String, Object> findSampleInfoList(String fcId,
//			Integer startNum, Integer limitNum, String dir, String sort)
//			throws Exception {
//		Map<String, Object> result = this.dataTaskDao.selectSampleInfoList(
//				fcId, startNum, limitNum, dir, sort);
//		List<DnaTaskInfo> list = (List<DnaTaskInfo>) result.get("list");
//		return result;
//	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDateTaskCnvItem(String[] ids) throws Exception {
		for (String id : ids) {
			DateTaskCnvItem scp = dataTaskDao.get(DateTaskCnvItem.class, id);
			dataTaskDao.delete(scp);
		}
	}

	/**
	 * ajax查询外显子数表
	 * 
	 * @param sc
	 * @param jsonMap
	 * @throws Exception
	 */
	// 根据模板ID加载子表明细
	public List<Map<String, String>> setSvnItem(String mutationGene) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = this.dataTaskDao.setSvnItem(mutationGene);
		List<MutationsGeneExons> list = (List<MutationsGeneExons>) result
				.get("list");
		if (list != null && list.size() > 0) {
			for (MutationsGeneExons ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("mutationGenes", ti.getMutationGenes());
				map.put("transcript", ti.getTranscript());
				map.put("totleNumberExon", ti.getTotleNumberExon());
				map.put("createDate", ti.getCreateDate().toString());
				if (ti.getCreateUser() != null)
					map.put("createUser", ti.getCreateUser().getName());
				else
					map.put("createUser", "");
				map.put("state", ti.getState());
				map.put("stateName", ti.getStateName());
				mapList.add(map);
			}

		}
		return mapList;
	}

	public List<MutationsGeneExons> setSvnItem2(String mutationGene) {

		Map<String, Object> result = this.dataTaskDao.setSvnItem(mutationGene);
		List<MutationsGeneExons> list = (List<MutationsGeneExons>) result
				.get("list");
		for (MutationsGeneExons m : list) {
			System.out
					.println(m.getMutationGenes() + "---" + m.getTranscript());
		}
		return list;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(DataTask sc, Map jsonMap) throws Exception {
		if (sc != null) {
			dataTaskDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("dataTaskSvnItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveDataTaskSvnItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("dataTaskSvItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveDataTaskSvItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("dateTaskCnvItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveDateTaskCnvItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("dateTaskItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveDataTaskItem(sc, jsonStr);
			}
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveMutation(Map jsonMap) throws Exception {
		String jsonStr = "";
		jsonStr = (String) jsonMap.get("mutationSvnItem");
		if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
			saveSvnItem(jsonStr);
		}
		jsonStr = (String) jsonMap.get("mutationSvItem");
		if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
			saveSvItem(jsonStr);
		}
		jsonStr = (String) jsonMap.get("mutationCnvItem");
		if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
			saveCnvItem(jsonStr);
		}
		jsonStr = (String) jsonMap.get("mutationdateTaskItem");
		if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
			savemutationItem(jsonStr);
		}

	}

	/**
	 * 数据分析明细
	 */
	public Map<String, Object> findDataTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = this.dataTaskDao.selectDataTaskItemList(
				scId, startNum, limitNum, dir, sort);
		// List<DataTaskItem> list = (List<DataTaskItem>) result.get("list");
		return result;
	}

	/**
	 * 左侧临时表
	 */
	public Map<String, Object> findDataTaskTempList(String fcId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = this.dataTaskDao.selectDataTaskTempList(
				fcId, startNum, limitNum, dir, sort);
		// List<DataTaskItem> list = (List<DataTaskItem>) result.get("list");
		return result;
	}

	/**
	 * 根据FC号查询添加
	 */
//	public Map<String, Object> selectOrAddByFC(String fcId) throws Exception {
//		Map<String, Object> result = this.dataTaskDao.selectOrAddByFC(fcId);
//		return result;
//	}

	/**
	 * 数据分析明细2
	 */
	public Map<String, Object> findDataTaskItemList2(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = this.dataTaskDao.selectDataTaskItemList2(
				scId, startNum, limitNum, dir, sort);
		List<DataTaskItem> list = (List<DataTaskItem>) result.get("list");
		return result;
	}

	/**
	 * 数据分析明细check
	 */
	public Map<String, Object> findDataTaskCheckItemList(String scId,
			String fcId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = this.dataTaskDao
				.selectDataTaskCheckItemList(scId, fcId, startNum, limitNum,
						dir, sort);
		List<DataTaskItem> list = (List<DataTaskItem>) result.get("list");
		return result;
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDataTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			DataTaskItem scp = this.dataTaskDao.get(DataTaskItem.class, id);
			this.dataTaskDao.delete(scp);
		}
	}

	/**
	 * 保存
	 * 
	 * @param sc
	 * @param itemDataJson
	 * @throws Exception
	 */

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveDataTaskItem(DataTask sc, String itemDataJson)
			throws Exception {
		List<DataTaskItem> saveItems = new ArrayList<DataTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			DataTaskItem scp = new DataTaskItem();
			// 将map信息读入实体类
			scp = (DataTaskItem) dataTaskDao.Map2Bean(map, scp);
			DataTaskTemp dataTaskTemp = new DataTaskTemp();
			dataTaskTemp = dataTaskDao.get(DataTaskTemp.class, scp.getTempId());
			if (dataTaskTemp != null) {
				dataTaskTemp.setState("1");
				dataTaskDao.saveOrUpdate(dataTaskTemp);
			}
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setDataTask(sc);

			saveItems.add(scp);
			// 把子表数据插入到临时表SampleReportTemp,处理方式为通过的就添加数据到临时表
			/* &&"2".equals(scp.getStates()) */
			/*
			 * if("1".equals(scp.getMethod())){ //List<SampleReportTemp>
			 * listReportTemp = new ArrayList<SampleReportTemp>();
			 * SampleReportTemp temp = new SampleReportTemp();
			 * temp.setSampleCode(scp.getSampleId());
			 * temp.setPatientId(scp.getCrmPatientId());
			 * temp.setWritingPersonOne(scp.getAcceptUser());
			 * temp.setWritingPersonTwo(scp.getAcceptUser2());
			 * temp.setState("1"); this.dataTaskDao.saveOrUpdate(temp); }
			 */

		}
		dataTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 保存2
	 * 
	 * @param sc
	 * @param itemDataJson
	 * @throws Exception
	 */

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savemutationItem(String itemDataJson) throws Exception {
		List<DataTaskItem> saveItems = new ArrayList<DataTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			DataTaskItem scp = new DataTaskItem();
			// 将map信息读入实体类
			scp = (DataTaskItem) dataTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			// scp.setDataTask(sc);

			saveItems.add(scp);
			// 把子表数据插入到临时表SampleReportTemp,处理方式为通过的就添加数据到临时表
			if ("1".equals(scp.getMethod()) && "2".equals(scp.getStates())) {
				// List<SampleReportTemp> listReportTemp = new
				// ArrayList<SampleReportTemp>();
				SampleReportTemp temp = new SampleReportTemp();
				temp.setSampleCode(scp.getSampleId());
				temp.setPatientId(scp.getCrmPatientId());
				temp.setState("1");

				this.dataTaskDao.saveOrUpdate(temp);
			}

		}
		dataTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * ========================================================================
	 * =============== 查询突变筛选字表
	 */
	public Map<String, Object> findDataTaskSvnItemCheckedList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = dataTaskDao
				.selectDataTaskSvnItemCheckedList(scId, startNum, limitNum,
						dir, sort);
		List<DataTaskSvnItemChecked> list = (List<DataTaskSvnItemChecked>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findDataTaskSvItemCheckedList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = dataTaskDao
				.selectDataTaskSvItemCheckedList(scId, startNum, limitNum, dir,
						sort);
		List<DataTaskSvItemChecked> list = (List<DataTaskSvItemChecked>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findDateTaskCnvItemCheckedList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = dataTaskDao
				.selectDateTaskCnvItemCheckedList(scId, startNum, limitNum,
						dir, sort);
		List<DateTaskCnvItemChecked> list = (List<DateTaskCnvItemChecked>) result
				.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveDataTaskSvnItemChecked(DataTask sc, String itemDataJson)
			throws Exception {
		List<DataTaskSvnItemChecked> saveItems = new ArrayList<DataTaskSvnItemChecked>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			DataTaskSvnItemChecked scp = new DataTaskSvnItemChecked();
			// 将map信息读入实体类
			scp = (DataTaskSvnItemChecked) dataTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setDataTask(sc);

			saveItems.add(scp);
		}
		dataTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDataTaskSvnItemChecked(String[] ids) throws Exception {
		for (String id : ids) {
			DataTaskSvnItemChecked scp = dataTaskDao.get(
					DataTaskSvnItemChecked.class, id);
			dataTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveDataTaskSvItemChecked(DataTask sc, String itemDataJson)
			throws Exception {
		List<DataTaskSvItemChecked> saveItems = new ArrayList<DataTaskSvItemChecked>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			DataTaskSvItemChecked scp = new DataTaskSvItemChecked();
			// 将map信息读入实体类
			scp = (DataTaskSvItemChecked) dataTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setDataTask(sc);

			saveItems.add(scp);
		}
		dataTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDataTaskSvItemChecked(String[] ids) throws Exception {
		for (String id : ids) {
			DataTaskSvItemChecked scp = dataTaskDao.get(
					DataTaskSvItemChecked.class, id);
			dataTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveDateTaskCnvItemChecked(DataTask sc, String itemDataJson)
			throws Exception {
		List<DateTaskCnvItemChecked> saveItems = new ArrayList<DateTaskCnvItemChecked>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			DateTaskCnvItemChecked scp = new DateTaskCnvItemChecked();
			// 将map信息读入实体类
			scp = (DateTaskCnvItemChecked) dataTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setDataTask(sc);

			saveItems.add(scp);
		}
		dataTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDateTaskCnvItemChecked(String[] ids) throws Exception {
		for (String id : ids) {
			DateTaskCnvItemChecked scp = dataTaskDao.get(
					DateTaskCnvItemChecked.class, id);
			dataTaskDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCheckedItem(DataTask sc, Map jsonMap) throws Exception {
		if (sc != null) {
			dataTaskDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("dataTaskSvnItemChecked");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveDataTaskSvnItemChecked(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("dataTaskSvItemChecked");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveDataTaskSvItemChecked(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("dateTaskCnvItemChecked");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveDateTaskCnvItemChecked(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("dateTaskItemChecked");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveDataTaskItem(sc, jsonStr);
			}
		}
	}

	/**
	 * 数据分析明细
	 */
	public List<DataTaskItem> findDataTaskItemList(String scId)
			throws Exception {
		List<DataTaskItem> list = this.dataTaskDao.selectDataTaskItemList(scId);
		return list;
	}

	public List<String> getSampleInformationUsers(DelegateExecution execution)
			throws Exception {
		List<String> userList = new ArrayList<String>();
		try {
			String businessKey = (String) execution.getProcessBusinessKey();

			List<DataTaskItem> list = dataTaskDao
					.selectDataTaskItemList(businessKey);

			if (list.size() > 0) {
				DataTaskItem sc = list.get(0);
				userList.add(sc.getAcceptUser().getId());
				userList.add(sc.getAcceptUser2().getId());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return userList;
	}
}
