package com.biolims.analysis.data.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.analysis.data.model.DataTask;
import com.biolims.analysis.data.model.DataTaskItem;
import com.biolims.analysis.data.model.DataTaskSvItem;
import com.biolims.analysis.data.model.DataTaskSvItemChecked;
import com.biolims.analysis.data.model.DataTaskSvnItem;
import com.biolims.analysis.data.model.DataTaskSvnItemChecked;
import com.biolims.analysis.data.model.DataTaskTemp;
import com.biolims.analysis.data.model.DateTaskCnvItem;
import com.biolims.analysis.data.model.DateTaskCnvItemChecked;
import com.biolims.analysis.exon.model.MutationsGeneExons;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.dic.model.DicType;
import com.biolims.experiment.pooling.model.PoolingItem;
import com.biolims.experiment.sequencing.model.SequencingTask;
import com.biolims.experiment.sequencing.model.SequencingTaskItem;
import com.biolims.report.model.SampleReportTemp;
import com.biolims.sample.encod.model.SampleOrderInfoItem;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;

@Repository
@SuppressWarnings("unchecked")
public class DataTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectDataTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from DataTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<DataTask> list = new ArrayList<DataTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectDataTaskSvnItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from DataTaskSvnItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and dataTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<DataTaskSvnItem> list = new ArrayList<DataTaskSvnItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 查询突变审核字表svnItem
	 * 
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectDataTaskSvnItemList2(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {

		// String hql1 = "from DataTaskItem where 1=1 and states = '1'";

		String hql = "from DataTaskSvnItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and dataTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<DataTaskSvnItem> list = new ArrayList<DataTaskSvnItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectDataTaskSvItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from DataTaskSvItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and dataTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<DataTaskSvItem> list = new ArrayList<DataTaskSvItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * svItem
	 * 
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectDataTaskSvItemList2(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from DataTaskSvItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and dataTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<DataTaskSvItem> list = new ArrayList<DataTaskSvItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectDateTaskCnvItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from DateTaskCnvItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and dataTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<DateTaskCnvItem> list = new ArrayList<DateTaskCnvItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * cnvItem
	 * 
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectDateTaskCnvItemList2(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from DateTaskCnvItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and dataTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<DateTaskCnvItem> list = new ArrayList<DateTaskCnvItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 病人信息
	 * 
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
//	public Map<String, Object> selectSampleInfoList(String fcId,
//			Integer startNum, Integer limitNum, String dir, String sort)
//			throws Exception {
//
//		// 根据fc号查询上机表
//		String hqlsequence = "from SequencingTask where 1=1 and fcCode ='"
//				+ fcId + "' ";
//		List<SequencingTask> listSequencing = this.getSession()
//				.createQuery(hqlsequence).list();
//		SequencingTask sequenc = listSequencing.get(0);
//		// 测序明系
//		String hqlsequenceItem = "from SequencingTaskItem where 1=1 and sequencing.id ='"
//				+ sequenc.getId() + "' ";
//		List<SequencingTaskItem> listsequenceItem = this.getSession()
//				.createQuery(hqlsequenceItem).list();
//		SequencingTaskItem sequencItem = listsequenceItem.get(0);
//		System.out.println(sequencItem.getPoolingCode());// Pooling2016013100001
//		// 查询poolingtaskItem//poolingTask.id
//		String hqlpoolingItem = "from PoolingTaskItem where 1=1 and poolingCode ='"
//				+ sequencItem.getPoolingCode() + "' ";
//		List<PoolingItem> listPoolingTaskItem = this.getSession()
//				.createQuery(hqlpoolingItem).list();
//		PoolingItem poolingTaskItem = listPoolingTaskItem.get(0);
//		// 查询样本
//
//		String hql = "from SampleInfo where 1=1 ";
//		String key = "";
//		if (poolingTaskItem.getSampleCode() != null)
//			key = key + " and code ='" + poolingTaskItem.getSampleCode() + "'";
//		Long total = (Long) this.getSession()
//				.createQuery("select count(*) " + hql + key).uniqueResult();
//		List<SampleInfo> list = new ArrayList<SampleInfo>();
//		if (total > 0) {
//			if (dir != null && dir.length() > 0 && sort != null
//					&& sort.length() > 0) {
//				if (sort.indexOf("-") != -1) {
//					sort = sort.substring(0, sort.indexOf("-"));
//				}
//				key = key + " order by " + sort + " " + dir;
//			}
//			if (startNum == null || limitNum == null) {
//				list = this.getSession().createQuery(hql + key).list();
//			} else {
//				list = this.getSession().createQuery(hql + key)
//						.setFirstResult(startNum).setMaxResults(limitNum)
//						.list();
//			}
//		}
//		Map<String, Object> result = new HashMap<String, Object>();
//		result.put("total", total);
//		result.put("list", list);
//		return result;
//
//	}

	/***
	 * ajax加载数据 外显子数
	 * 
	 */
	// 根据主表ID加载子表明细数据
	public Map<String, Object> setSvnItem(String mutationGene) {
		String hql = "from MutationsGeneExons t where 1=1 and t.mutationGenes='"
				+ mutationGene + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql).uniqueResult();
		List<MutationsGeneExons> list = this.getSession().createQuery(hql)
				.list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);

		return result;
	}

	/**
	 * 数据分析明细
	 */
	public Map<String, Object> selectDataTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {

		/*
		 * if(!"".equals(scId)&&!"".equals(fcId)){ addDataTaskItem(scId,fcId); }
		 */
		/**
		 * 查询数据分析结果明细
		 */
		String hql = "from DataTaskItem where 1=1 ";
		String key = "";
		if (scId != null) {
			key = key + " and dataTask.id ='" + scId + "'";
		}
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<DataTaskItem> list = new ArrayList<DataTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		if (list.size() <= 0) {
			DataTask d = get(DataTask.class, scId);
			if (d != null) {
				d.setKnumber("0");
				save(d);
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 左侧临时表
	 */
	public Map<String, Object> selectDataTaskTempList(String fcId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from DataTaskTemp where 1=1 and state= '1' and FC= '"
				+ fcId + "'";
		String key = "";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql).uniqueResult();
		List<DataTaskTemp> list = new ArrayList<DataTaskTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据FC号查询添加
//	public Map<String, Object> selectOrAddByFC(String fcId) throws Exception {
//		String hql1 = "from DataTaskTemp where 1=1 and FC='" + fcId + "'";
//		List<DataTaskTemp> list1 = new ArrayList<DataTaskTemp>();
//		list1 = this.getSession().createQuery(hql1).list();
//		if (list1.size() == 0) {
//			addDataTaskTemp(fcId);
//		}
//
//		/**
//		 * 查询数据分析结果明细
//		 */
//		String hql = "from DataTaskTemp where 1=1 and state= '" + "1"
//				+ "' and FC= '" + fcId + "'";
//		Long total = (Long) this.getSession()
//				.createQuery("select count(*) " + hql).uniqueResult();
//		List<DataTaskTemp> list = new ArrayList<DataTaskTemp>();
//		if (total > 0) {
//			list = this.getSession().createQuery(hql).list();
//		}
//		Map<String, Object> result = new HashMap<String, Object>();
//		result.put("total", total);
//		result.put("list", list);
//		return result;
//	}

	/**
	 * ======================================== ===============================
	 * 数据分析明细check
	 */
	public Map<String, Object> selectDataTaskCheckItemList(String scId,
			String fcId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {

		String hql = "from DataTaskItem where 1=1 and states='2' ";
		String key = "";
		if (scId != null) {
			key = key + " and dataTask.id ='" + scId + "'";
		}
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<DataTaskItem> list = new ArrayList<DataTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		if (list.size() <= 0) {
			DataTask d = get(DataTask.class, scId);
			d.setKnumber("0");
			save(d);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 左侧临时表插数据
//	public void addDataTaskTemp(String fcId) {
//		// 根据fc号查询上机表
//		String hqlsequence = "from SequencingTask where 1=1 and fcCode ='"
//				+ fcId + "' ";
//		SequencingTask sequencingTask = (SequencingTask) this.getSession()
//				.createQuery(hqlsequence).uniqueResult();
//
//		// 根据上机测序主表id查询子表
//		String hql2 = "from SequencingTaskItem where 1=1 and sequencing.id = '"
//				+ sequencingTask.getId() + "'";
//		List<SequencingTaskItem> list2 = new ArrayList<SequencingTaskItem>();
//		list2 = this.getSession().createQuery(hql2).list();
//
//		for (SequencingTaskItem sqt : list2) {
//			// 根据上机测序子表上机分组号查询富集结果表
//			String hql3 = "from PoolingBlendInfo where 1=1 and groupNum='"
//					+ sqt.getPoolingCode() + "'";
//			List<PoolingBlendInfo> list3 = new ArrayList<PoolingBlendInfo>();
//			list3 = this.getSession().createQuery(hql3).list();
//
//			for (PoolingBlendInfo ptr : list3) {
//				// 根据富集结果富集文库号查询到富集分配
//				String hql4 = "from PoolingTaskItem where 1=1 and fjWkCode='"
//						+ ptr.getFjWkCode() + "'";
//				List<PoolingTaskItem> list4 = new ArrayList<PoolingTaskItem>();
//				list4 = this.getSession().createQuery(hql4).list();
//				for (PoolingTaskItem pti : list4) {
//					String hql5 = "from SampleInfo where 1=1 and code ='"
//							+ pti.getSampleCode() + "'";
//					SampleInfo sample = (SampleInfo) this.getSession()
//							.createQuery(hql5).uniqueResult();
//					DataTaskTemp d = new DataTaskTemp();
//					d.setState("1");
//					d.setSampleCode(pti.getSampleCode());// 原始样本号
//					d.setSampleId(pti.getCode());// 样本号
//					d.setFC(fcId);
//					if (sample.getSampleOrder().getMedicalNumber() != null) {
//						d.setCrmPatientId(sample.getSampleOrder()
//								.getMedicalNumber());// 电子病历号
//					}
//
//					d.setSampleTypeId(sample.getSampleType().getId());// 样本类型id
//					d.setSampleTypeName(sample.getSampleType().getName());// 样本类型
//					d.setProductId(pti.getProductId());// 检测项目id
//					d.setProductName(pti.getProductName());// 检测项目名称
//					if (sample.getSampleOrder().getCancerType() != null) {
//						d.setCancerType(sample.getSampleOrder().getCancerType()
//								.getCancerTypeName());// 癌症种类
//					}
//					if (sample.getSampleOrder().getCancerTypeSeedOne() != null) {
//						d.setCancerTypeSeedOne(sample.getSampleOrder()
//								.getCancerTypeSeedOne().getCancerTypeName());// 癌症子类一
//					}
//					if (sample.getSampleOrder().getCancerTypeSeedTwo() != null) {
//						d.setCancerTypeSeedTwo(sample.getSampleOrder()
//								.getCancerTypeSeedTwo().getCancerTypeName());// 癌症子类二
//					}
//
//					saveOrUpdate(d);
//				}
//			}
//		}
//		// SequencingTask sequencingTask = (SequencingTask) this.getSession()
//		// .createQuery(hqlsequence).uniqueResult();
//		//
//		// // 根据上机测序主表id查询子表
//		// String hql2 =
//		// "from SequencingTaskItem where 1=1 and sequencing.id = '"
//		// + sequencingTask.getId() + "'";
//		// List<SequencingTaskItem> list2 = new ArrayList<SequencingTaskItem>();
//		// list2 = this.getSession().createQuery(hql2).list();
//
//		// for (SequencingTaskItem sqt : list2) {
//		// // 根据上机测序子表上机分组号查询富集分配结果表
//		// String hql3 = "from PoolingfpTaskResult where 1=1 and groupnum='"
//		// + sqt.getCode() + "'";
//		// List<PoolingfpTaskResult> list3 = new
//		// ArrayList<PoolingfpTaskResult>();
//		// list3 = this.getSession().createQuery(hql3).list();
//		//
//		// for (PoolingfpTaskResult ptr : list3) {
//		// // 根据富集分配结果富集文库号查询到富集分配
//		// String hql4 = "from PoolingTaskItem where 1=1 and fjWkCode='"
//		// + ptr.getFjwkcode() + "'";
//		// List<PoolingTaskItem> list4 = new ArrayList<PoolingTaskItem>();
//		// list4 = this.getSession().createQuery(hql4).list();
//		// for (PoolingTaskItem pti : list4) {
//		// String hql5 = "from SampleInfo where 1=1 and code ='"
//		// + pti.getSampleCode() + "'";
//		// SampleInfo sample = (SampleInfo) this.getSession()
//		// .createQuery(hql5).uniqueResult();
//		// DataTaskTemp d = new DataTaskTemp();
//		// d.setState("1");
//		// d.setSampleCode(pti.getSampleCode());// 原始样本号
//		// d.setSampleId(pti.getCode());// 样本号
//		// d.setFC(fcId);
//		// if (sample.getSampleOrder().getMedicalNumber() != null) {
//		// d.setCrmPatientId(sample.getSampleOrder()
//		// .getMedicalNumber());// 电子病历号
//		// }
//		//
//		// d.setSampleTypeId(sample.getSampleType().getId());// 样本类型id
//		// d.setSampleTypeName(sample.getSampleType().getName());// 样本类型
//		// d.setProductId(pti.getProductId());// 检测项目id
//		// d.setProductName(pti.getProductName());// 检测项目名称
//		// if (sample.getSampleOrder().getCancerType() != null) {
//		// d.setCancerType(sample.getSampleOrder().getCancerType()
//		// .getCancerTypeName());// 癌症种类
//		// }
//		// if (sample.getSampleOrder().getCancerTypeSeedOne() != null) {
//		// d.setCancerTypeSeedOne(sample.getSampleOrder()
//		// .getCancerTypeSeedOne().getCancerTypeName());// 癌症子类一
//		// }
//		// if (sample.getSampleOrder().getCancerTypeSeedTwo() != null) {
//		// d.setCancerTypeSeedTwo(sample.getSampleOrder()
//		// .getCancerTypeSeedTwo().getCancerTypeName());// 癌症子类二
//		// }
//		//
//		// saveOrUpdate(d);
//		// }
//		// }
//		// }
//
//	}

	public Long selectDataTaskItemByTaskId(String taskId) throws Exception {
		String hql = "from DataTaskItem where dataTask.id ='" + taskId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql).uniqueResult();
		return total;
	}

	public List<DataTaskItem> selectDataTaskItemList(String id)
			throws Exception {
		String key = "";
		String hql = "from DataTaskItem where dataTask.id = '" + id + "'";

		List<DataTaskItem> list = new ArrayList<DataTaskItem>();
		list = this.getSession().createQuery(hql + key).list();

		return list;

	}

	/**
	 * 数据分析明细2
	 */
	public Map<String, Object> selectDataTaskItemList2(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		/**
		 * 查询数据分析结果明细
		 */
		String hql = "from DataTaskItem where 1=1 and states = '1'";
		String key = "";
		if (scId != null) {
			key = key + " and dataTask.id ='" + scId + "'";
		}
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<DataTaskItem> list = new ArrayList<DataTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * ========================================================================
	 * =============== 查询突变筛选字表
	 */
	public Map<String, Object> selectDataTaskSvnItemCheckedList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from DataTaskSvnItemChecked where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and dataTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<DataTaskSvnItemChecked> list = new ArrayList<DataTaskSvnItemChecked>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectDataTaskSvItemCheckedList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from DataTaskSvItemChecked where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and dataTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<DataTaskSvItemChecked> list = new ArrayList<DataTaskSvItemChecked>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectDateTaskCnvItemCheckedList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from DateTaskCnvItemChecked where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and dataTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<DateTaskCnvItemChecked> list = new ArrayList<DateTaskCnvItemChecked>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public void saveReportTemp(DataTaskItem dataTaskItem) {
		/* SampleOrderInfoItem */
		// List<SampleOrderInfoItem> sampleOrderInfoItems = new
		// ArrayList<SampleOrderInfoItem>();
		SampleReportTemp temp = new SampleReportTemp();
		String hql = "from SampleOrderInfoItem where 1=1 and sampleCode= '"
				+ dataTaskItem.getSampleCode() + "'";
		List<SampleOrderInfoItem> list = new ArrayList<SampleOrderInfoItem>();
		list = this.getSession().createQuery(hql).list();
		if (list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				List<SampleOrder> sampleOrders = new ArrayList<SampleOrder>();
				String hql1 = "from SampleOrder where 1=1 and id= '"
						+ list.get(i).getCode() + "'";
				sampleOrders = this.getSession().createQuery(hql1).list();
				for (int j = 0; j < sampleOrders.size(); j++) {
					SampleOrder sampleOrder = new SampleOrder();
					sampleOrder = sampleOrders.get(j);
					// if ("1".equals(sampleOrder.getBgState())) {
					temp.setOrderNum(sampleOrder.getId());
					temp.setSampleOrder(sampleOrder);
					temp.setSampleCode(dataTaskItem.getSampleCode());
					temp.setCode(dataTaskItem.getSampleId());
					temp.setProductId(dataTaskItem.getProductId());
					temp.setProductName(dataTaskItem.getProductName());
					temp.setPatientId(dataTaskItem.getCrmPatientId());
					temp.setWritingPersonOne(dataTaskItem.getAcceptUser());
					temp.setWritingPersonTwo(dataTaskItem.getAcceptUser2());
					temp.setState("1");
					// }
				}
			}
		}
		saveOrUpdate(temp);
	}

	// 根据突变基因查询基因别称
	public DicType selDicTypejybc(String name) {
		String hql = "from DicType where type.id='jybc' and name='" + name
				+ "'";
		DicType d = (DicType) this.getSession().createQuery(hql).uniqueResult();
		return d;
	}

	// 根据突变基因查询转录本
	public DicType selDicTypecyzlb(String name) {
		String hql = "from DicType where type.id='cyzlb' and name='" + name
				+ "'";
		DicType d = (DicType) this.getSession().createQuery(hql).uniqueResult();
		return d;
	}

	// 根据突变基因查询CNV专用染色体位置
	public DicType selDicTypejyrstw(String name) {
		String hql = "from DicType where type.id='jyrstw' and name='" + name
				+ "'";
		DicType d = (DicType) this.getSession().createQuery(hql).uniqueResult();
		return d;
	}

	// 根据突变主表查询有效snv数据
	public List<DataTaskSvnItem> selDataTaskSvnItemList(String id) {
		String hql = "from DataTaskSvnItem where dataTask.id='" + id
				+ "' and validState='1'";
		List<DataTaskSvnItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据突变主表查询有效sv数据
	public List<DataTaskSvItem> selDataTaskSvItemList(String id) {
		String hql = "from DataTaskSvItem where dataTask.id='" + id
				+ "' and validState='1'";
		List<DataTaskSvItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据突变主表查询有效cnv数据
	public List<DateTaskCnvItem> selDateTaskCnvItemList(String id) {
		String hql = "from DateTaskCnvItem where dataTask.id='" + id
				+ "' and validState='1'";
		List<DateTaskCnvItem> list = this.getSession().createQuery(hql).list();
		return list;
	}
}