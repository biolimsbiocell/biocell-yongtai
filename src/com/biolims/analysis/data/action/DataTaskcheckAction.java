package com.biolims.analysis.data.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.analysis.data.model.DataTask;
import com.biolims.analysis.data.model.DataTaskItem;
import com.biolims.analysis.data.model.DataTaskSvItemChecked;
import com.biolims.analysis.data.model.DataTaskSvnItemChecked;
import com.biolims.analysis.data.model.DateTaskCnvItemChecked;
import com.biolims.analysis.data.service.DataTaskService;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/analysis/check/dataTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class DataTaskcheckAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "260108";
	@Autowired
	private DataTaskService dataTaskService;
	private DataTask dataTask = new DataTask();
	@Resource
	private CommonService commonService;
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showDataTaskList")
	public String showDataTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/analysis/check/dataTask.jsp");
	}

	@Action(value = "showDataTaskListJson")
	public void showDataTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = dataTaskService.findDataTaskList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<DataTask> list = (List<DataTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("fcnumber", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "dataTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogDataTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/analysis/check/dataTaskDialog.jsp");
	}

	@Action(value = "showDialogDataTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogDataTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = dataTaskService.findDataTaskList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<DataTask> list = (List<DataTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("fcnumber", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editDataTask")
	public String editDataTask() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			dataTask = dataTaskService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "dataTask");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			dataTask.setCreateUser(user);
			dataTask.setCreateDate(new Date());
			dataTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			dataTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/analysis/check/dataTaskEdit.jsp");
	}

	@Action(value = "copyDataTask")
	public String copyDataTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		dataTask = dataTaskService.get(id);
		dataTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/analysis/check/dataTaskEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = dataTask.getId();
		if(id!=null&&id.equals("")){
			dataTask.setId(null);
		}
		Map aMap = new HashMap();
			aMap.put("dataTaskSvnItemChecked",getParameterFromRequest("dataTaskSvnItemCheckedJson"));
		
			aMap.put("dataTaskSvItemChecked",getParameterFromRequest("dataTaskSvItemCheckedJson"));
		
			aMap.put("dateTaskCnvItemChecked",getParameterFromRequest("dateTaskCnvItemCheckedJson"));
			
			aMap.put("dateTaskItemChecked",getParameterFromRequest("dateTaskItemCheckedJson"));
		
		dataTaskService.saveCheckedItem(dataTask,aMap);
		return redirect("/analysis/check/dataTask/editDataTask.action?id=" + dataTask.getId());

	}

	@Action(value = "viewDataTask")
	public String toViewDataTask() throws Exception {
		String id = getParameterFromRequest("id");
		dataTask = dataTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/analysis/check/dataTaskEdit.jsp");
	}
	

	@Action(value = "showDataTaskSvnItemCheckedList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDataTaskSvnItemCheckedList() throws Exception {
		return dispatcher("/WEB-INF/page/analysis/check/dataTaskSvnItemChecked.jsp");
	}

	@Action(value = "showDataTaskSvnItemCheckedListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDataTaskSvnItemCheckedListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = dataTaskService.findDataTaskSvnItemCheckedList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<DataTaskSvnItemChecked> list = (List<DataTaskSvnItemChecked>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("mutantGenes", "");
			map.put("otherName", "");
			map.put("transcript", "");
			map.put("exon", "");
			map.put("aminoAcidMutation", "");
			map.put("baseMutation", "");
			map.put("mutationStartPosition", "");
			map.put("mutationStopPosition", "");
			map.put("mutationSource", "");
			map.put("mutationClass", "");
			map.put("mutationType", "");
			map.put("mutationStatus", "");
			map.put("mutatinAbundance", "");
			map.put("mutationFiction", "");
			map.put("dataTask-name", "");
			map.put("dataTask-id", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("createUser-name", "");
			map.put("createUser-id", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("sampleCode", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delDataTaskSvnItemChecked")
	public void delDataTaskSvnItemChecked() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			dataTaskService.delDataTaskSvnItemChecked(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	

	@Action(value = "showDataTaskSvItemCheckedList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDataTaskSvItemCheckedList() throws Exception {
		return dispatcher("/WEB-INF/page/analysis/check/dataTaskSvItemChecked.jsp");
	}

	@Action(value = "showDataTaskSvItemCheckedListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDataTaskSvItemCheckedListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = dataTaskService.findDataTaskSvItemCheckedList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<DataTaskSvItemChecked> list = (List<DataTaskSvItemChecked>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("mutantGenes", "");
			map.put("otherName", "");
			map.put("partnerGenes", "");
			map.put("fusionProduct", "");
			map.put("transcript", "");
			map.put("exon1", "");
			map.put("exon2", "");
			map.put("mutationStartPosition", "");
			map.put("mutationStopPosition", "");
			map.put("mutationSource", "");
			map.put("mutationClass", "");
			map.put("mutationType", "");
			map.put("mutationStatus", "");
			map.put("mutatinAbundance", "");
			map.put("mutationFiction", "");
			map.put("dataTask-name", "");
			map.put("dataTask-id", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("createUser-name", "");
			map.put("createUser-id", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("sampleCode", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delDataTaskSvItemChecked")
	public void delDataTaskSvItemChecked() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			dataTaskService.delDataTaskSvItemChecked(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	

	@Action(value = "showDateTaskCnvItemCheckedList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDateTaskCnvItemCheckedList() throws Exception {
		return dispatcher("/WEB-INF/page/analysis/check/dateTaskCnvItemChecked.jsp");
	}

	@Action(value = "showDateTaskCnvItemCheckedListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDateTaskCnvItemCheckedListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = dataTaskService.findDateTaskCnvItemCheckedList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<DateTaskCnvItemChecked> list = (List<DateTaskCnvItemChecked>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("mutantGenes", "");
			map.put("otherName", "");
			map.put("copyNumberVaration", "");
			map.put("copyVarationRatio", "");
			map.put("mutationSource", "");
			map.put("mutationClass", "");
			map.put("mutationType", "");
			map.put("mutationFiction", "");
			map.put("dataTask-name", "");
			map.put("dataTask-id", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("createUser-name", "");
			map.put("createUser-id", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("sampleCode", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delDateTaskCnvItemChecked")
	public void delDateTaskCnvItemChecked() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			dataTaskService.delDateTaskCnvItemChecked(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	/**
	 * 查询数据信息明细
	 * ======================================================
	 * @return
	 */
	/**
	 * 查询数据分析明细
	 * @throws Exception
	 */
	
	@Action(value = "showDataTaskCheckItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDataTaskCheckItemList() throws Exception {
		//putObjToContext("sgt2", getParameterFromRequest("sgt2"));
		return dispatcher("/WEB-INF/page/analysis/check/dataTaskcheckItem.jsp");
	}

	@Action(value = "showDataTaskCheckItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDataTaskCheckItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String scId = getRequest().getParameter("id");
		String fcId = getRequest().getParameter("fcId");
		Map<String, String> mapForQuery = new HashMap<String, String>();
		try {

			Map<String, Object> result = dataTaskService.findDataTaskCheckItemList(scId,fcId, startNum, limitNum,
					dir, sort);
			Long total = (Long) result.get("total");
			List<DataTaskItem> list = (List<DataTaskItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");//编号
			map.put("sampleId", "");//样本编号
			map.put("crmPatientId", "");
			map.put("name", "");//名称
			map.put("location", "");//位置
			map.put("xjPot", "");//碱基改变
			map.put("tbFrequence", "");//突变频率
			map.put("genePot", "");//基因改变
			map.put("hgmdPubmedid", "");//参考文献ID
			map.put("note", "");//注释
			map.put("form", "");//类型
			
			map.put("dataTask-id", "");
			map.put("dataTask-name", "");
			map.put("MicroRNA", "");//ran
			map.put("type", "");//type
			map.put("acceptUser-id", "");//实验员id
			map.put("acceptUser-name", "");//实验员名称
			map.put("acceptUser2-id", "");//实验员id2
			map.put("acceptUser2-name", "");//实验员名称2
			
			map.put("sampleInfo-id", "");//样本id
			map.put("sampleInfo-name", "");//样本编号
			
			map.put("method", "");//处理方法
			map.put("reason", "");//失败原因
			map.put("states", "");//通过状态

			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 保存明细
	 */
	@Action(value = "saveDataTaskItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveDataTaskItem() throws Exception {

		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String dataJson = getRequest().getParameter("itemDataJson");
			String taskId = getRequest().getParameter("taskId");
			DataTask a = commonService.get(DataTask.class, taskId);
			dataTaskService.saveDataTaskItem(a, dataJson);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	/**
	 * 删除明细
	 * @throws Exception
	 */
	@Action(value = "delDataTaskItem")
	public void delDataTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			dataTaskService.delDataTaskItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}


	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public DataTaskService getDataTaskService() {
		return dataTaskService;
	}

	public void setDataTaskService(DataTaskService dataTaskService) {
		this.dataTaskService = dataTaskService;
	}

	public DataTask getDataTask() {
		return dataTask;
	}

	public void setDataTask(DataTask dataTask) {
		this.dataTask = dataTask;
	}

	public CommonService getCommonService() {
		return commonService;
	}

	public void setCommonService(CommonService commonService) {
		this.commonService = commonService;
	}

	public FileInfoService getFileInfoService() {
		return fileInfoService;
	}

	public void setFileInfoService(FileInfoService fileInfoService) {
		this.fileInfoService = fileInfoService;
	}


}
