package com.biolims.analysis.data.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.analysis.data.dao.DataTaskDao;
import com.biolims.analysis.data.model.DataTask;
import com.biolims.analysis.data.model.DataTaskItem;
import com.biolims.analysis.data.model.DataTaskSvItem;
import com.biolims.analysis.data.model.DataTaskSvnItem;
import com.biolims.analysis.data.model.DataTaskTemp;
import com.biolims.analysis.data.model.DateTaskCnvItem;
import com.biolims.analysis.data.service.DataTaskService;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.dic.model.DicType;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/analysis/data/dataTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class DataTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";
	@Autowired
	private DataTaskService dataTaskService;
	private DataTask dataTask = new DataTask();
	@Resource
	private CommonService commonService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private DataTaskDao dataTaskDao;

	@Action(value = "showDataTaskList")
	public String showDataTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/analysis/data/dataTask.jsp");
	}

	@Action(value = "showDataTaskListJson")
	public void showDataTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = dataTaskService.findDataTaskList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<DataTask> list = (List<DataTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("fcnumber", "");
		map.put("knumber", "");

		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "dataTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogDataTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/analysis/data/dataTaskDialog.jsp");
	}

	@Action(value = "showDialogDataTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogDataTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = dataTaskService.findDataTaskList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<DataTask> list = (List<DataTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("fcnumber", "");
		map.put("knumber", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editDataTask")
	public String editDataTask() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			dataTask = dataTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "dataTask");
		} else {
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			dataTask.setCreateUser(user);
			dataTask.setCreateDate(new Date());
			dataTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			dataTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			dataTask.setId("NEW");
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(dataTask.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/analysis/data/dataTaskEdit.jsp");
	}

	@Action(value = "copyDataTask")
	public String copyDataTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		dataTask = dataTaskService.get(id);
		dataTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/analysis/data/dataTaskEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = dataTask.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "DataTask";
			String markCode = "MU";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			dataTask.setId(autoID);
		}

		Map aMap = new HashMap();
		aMap.put("dataTaskSvnItem",
				getParameterFromRequest("dataTaskSvnItemJson"));

		aMap.put("dataTaskSvItem",
				getParameterFromRequest("dataTaskSvItemJson"));

		aMap.put("dateTaskCnvItem",
				getParameterFromRequest("dateTaskCnvItemJson"));

		aMap.put("dateTaskItem", getParameterFromRequest("dateTaskItemJson"));

		dataTaskService.save(dataTask, aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/analysis/data/dataTask/editDataTask.action?id="
				+ dataTask.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;

		return redirect(url);

	}

	@Action(value = "viewDataTask")
	public String toViewDataTask() throws Exception {
		String id = getParameterFromRequest("id");
		dataTask = dataTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/analysis/data/dataTaskEdit.jsp");
	}

	@Action(value = "showDataTaskSvnItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDataTaskSvnItemList() throws Exception {
		return dispatcher("/WEB-INF/page/analysis/data/dataTaskSvnItem.jsp");
	}

	@Action(value = "showDataTaskSvnItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDataTaskSvnItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = dataTaskService
					.findDataTaskSvnItemList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<DataTaskSvnItem> list = (List<DataTaskSvnItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("mutantGenes", "");
			map.put("otherName", "");
			map.put("transcript", "");
			map.put("exon", "");
			map.put("aminoAcidMutation", "");
			map.put("baseMutation", "");
			map.put("mutationStartPosition", "");
			map.put("mutationStopPosition", "");
			map.put("mutationSource", "");
			map.put("mutationClass", "");
			map.put("mutationType", "");
			map.put("mutationStatus", "");
			map.put("mutatinAbundance", "");
			map.put("mutationFiction", "");
			map.put("dataTask-name", "");
			map.put("dataTask-id", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("createUser-name", "");
			map.put("createUser-id", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("sampleCode", "");
			map.put("acceptUser-id", "");
			map.put("acceptUser-name", "");
			map.put("validState", "");
			map.put("accordance", "");
			map.put("mutantGeneLc", "");
			map.put("referenceBase", "");
			map.put("mutantBase", "");
			map.put("relatedMutation", "");
			map.put("relatedChemotherapy", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delDataTaskSvnItem")
	public void delDataTaskSvnItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			dataTaskService.delDataTaskSvnItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showDataTaskSvItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDataTaskSvItemList() throws Exception {
		return dispatcher("/WEB-INF/page/analysis/data/dataTaskSvItem.jsp");
	}

	@Action(value = "showDataTaskSvItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDataTaskSvItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = dataTaskService
					.findDataTaskSvItemList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<DataTaskSvItem> list = (List<DataTaskSvItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("mutantGenes", "");
			map.put("otherName", "");
			map.put("partnerGenes", "");
			map.put("fusionProduct", "");
			map.put("transcript", "");
			map.put("exon1", "");
			map.put("exon2", "");
			map.put("mutationStartPosition", "");
			map.put("mutationStopPosition", "");
			map.put("mutationSource", "");
			map.put("mutationClass", "");
			map.put("mutationType", "");
			map.put("mutationStatus", "");
			map.put("mutatinAbundance", "");
			map.put("mutationFiction", "");
			map.put("dataTask-name", "");
			map.put("dataTask-id", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("createUser-name", "");
			map.put("createUser-id", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("sampleCode", "");
			map.put("acceptUser-id", "");
			map.put("acceptUser-name", "");
			map.put("validState", "");
			map.put("accordance", "");
			map.put("mutantGeneScript", "");
			map.put("partnerScript", "");
			map.put("mutantGenePoint", "");
			map.put("partnerPoint", "");
			map.put("MutantGeneLc", "");
			map.put("partnerLc", "");
			map.put("relatedMutation", "");
			map.put("relatedChemotherapy", "");
			map.put("partnerGenesName", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delDataTaskSvItem")
	public void delDataTaskSvItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			dataTaskService.delDataTaskSvItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showDateTaskCnvItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDateTaskCnvItemList() throws Exception {
		return dispatcher("/WEB-INF/page/analysis/data/dateTaskCnvItem.jsp");
	}

	@Action(value = "showDateTaskCnvItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDateTaskCnvItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = dataTaskService
					.findDateTaskCnvItemList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<DateTaskCnvItem> list = (List<DateTaskCnvItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("mutantGenes", "");
			map.put("otherName", "");
			map.put("copyNumberVaration", "");
			map.put("copyVarationRatio", "");
			map.put("mutationSource", "");
			map.put("mutationClass", "");
			map.put("mutationType", "");
			map.put("mutationFiction", "");
			map.put("dataTask-name", "");
			map.put("dataTask-id", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("createUser-name", "");
			map.put("createUser-id", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("sampleCode", "");
			map.put("acceptUser-id", "");
			map.put("acceptUser-name", "");
			map.put("validState", "");
			map.put("accordance", "");
			map.put("copyNumberVarationExon", "");
			map.put("relatedMutation", "");
			map.put("relatedChemotherapy", "");
			map.put("mutationPosition", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delDateTaskCnvItem")
	public void delDateTaskCnvItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			dataTaskService.delDateTaskCnvItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 查询样本信息
	 * 
	 * @return
	 */

	@Action(value = "showSampleinfoList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleinfoList() throws Exception {
		return dispatcher("/WEB-INF/page/analysis/data/dataSampleInfo.jsp");
	}

//	@Action(value = "showSampleinfoListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showSampleinfoListJson() throws Exception {
//		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//		try {
//			String fcId = getRequest().getParameter("id");
//			Map<String, Object> result = this.dataTaskService
//					.findSampleInfoList(fcId, startNum, limitNum, dir, sort);
//			Long total = (Long) result.get("total");
//			List<SampleInfo> list = (List<SampleInfo>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("code", "");
//			map.put("name", "");
//			map.put("productId", "");
//			map.put("productName", "");
//			map.put("state", "");
//			map.put("stateName", "");
//			map.put("patientName", "");
//			map.put("note", "");
//			map.put("orderNum", "");
//			map.put("patientId", "");
//			map.put("sampleOrder", "");
//
//			map.put("idCard", "");
//			map.put("businessType", "");
//			map.put("price", "");
//			map.put("type-id", "");
//			map.put("type-name", "");
//			map.put("upLoadAccessory-id", "");
//			map.put("upLoadAccessory-fileName", "");
//			new SendData().sendDateJson(map, list, total,
//					ServletActionContext.getResponse());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	/**
	 * ajax加载 svnItem数据
	 * 
	 * @throws Exception
	 */
	@Action(value = "setSvnItem")
	public void setSvnItem() throws Exception {
		String mutationGene = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.dataTaskService
					.setSvnItem(mutationGene);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 查询数据分析明细
	 * 
	 * @throws Exception
	 */

	@Action(value = "showDataTaskItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDataTaskItemList() throws Exception {
		// putObjToContext("sgt2", getParameterFromRequest("sgt2"));
		return dispatcher("/WEB-INF/page/analysis/data/dataTaskItem.jsp");
	}

	@Action(value = "showDataTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDataTaskItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String scId = getRequest().getParameter("id");
		// String fcId = getRequest().getParameter("fcId");
		Map<String, String> mapForQuery = new HashMap<String, String>();
		try {
			Map<String, Object> result = dataTaskService.findDataTaskItemList(
					scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<DataTaskItem> list = (List<DataTaskItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");// 编号
			map.put("sampleId", "");// 样本编号
			map.put("crmPatientId", "");
			map.put("name", "");// 名称
			map.put("location", "");// 位置
			map.put("xjPot", "");// 碱基改变
			map.put("tbFrequence", "");// 突变频率
			map.put("genePot", "");// 基因改变
			map.put("hgmdPubmedid", "");// 参考文献ID
			map.put("note", "");// 注释
			map.put("form", "");// 类型

			map.put("dataTask-id", "");
			map.put("dataTask-name", "");
			map.put("MicroRNA", "");// ran
			map.put("type", "");// type
			map.put("acceptUser-id", "");// 实验员id
			map.put("acceptUser-name", "");// 实验员名称
			map.put("acceptUser2-id", "");// 实验员id2
			map.put("acceptUser2-name", "");// 实验员名称2

			map.put("sampleInfo-id", "");// 样本id
			map.put("sampleInfo-name", "");// 样本编号

			map.put("method", "");// 处理方法
			map.put("reason", "");// 失败原因
			map.put("states", "");// 通过状态

			map.put("poolingId", "");// pooling编号
			map.put("wkTaskId", "");// 文库编号
			map.put("sampleTypeId", "");// 样本类型ID
			map.put("sampleTypeName", "");// 样本类型名称
			map.put("sampleStage", "");// 分期
			map.put("cancerType", "");// 癌症类型
			map.put("cancerTypeSeedOne", "");// 癌症类型名称1
			map.put("cancerTypeSeedTwo", "");// 癌症类型名称2
			map.put("tempId", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("sampleCode", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 突变筛选左侧临时表
	@Action(value = "showDataTaskTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDataTaskTempList() throws Exception {
		return dispatcher("/WEB-INF/page/analysis/data/dataTaskTemp.jsp");
	}

	// 突变筛选左侧临时表
	@Action(value = "showDataTaskTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDataTaskTempListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		// String scId = getRequest().getParameter("id");
		String fcId = getRequest().getParameter("fcId");
		Map<String, String> mapForQuery = new HashMap<String, String>();
		try {
			Map<String, Object> result = dataTaskService.findDataTaskTempList(
					fcId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<DataTaskTemp> list = (List<DataTaskTemp>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");// 编号
			map.put("sampleId", "");// 样本编号
			map.put("crmPatientId", "");
			map.put("form", "");// 类型

			map.put("dataTask-id", "");
			map.put("dataTask-name", "");
			map.put("MicroRNA", "");// ran
			map.put("type", "");// type
			map.put("sampleInfo-id", "");// 样本id
			map.put("sampleInfo-name", "");// 样本编号

			map.put("state", "");// 读取状态

			map.put("poolingId", "");// pooling编号
			map.put("sampleTypeId", "");// 样本类型ID
			map.put("sampleTypeName", "");// 样本类型名称
			map.put("cancerType", "");// 癌症类型
			map.put("cancerTypeSeedOne", "");// 癌症类型名称1
			map.put("cancerTypeSeedTwo", "");// 癌症类型名称2
			map.put("productId", "");
			map.put("productName", "");
			map.put("sampleCode", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 根据fc号查询、添加
//	@Action(value = "selectOrAddByFC", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void selectOrAddByFC() throws Exception {
//		String fcId = getRequest().getParameter("fcId");
//		// Map<String, String> mapForQuery = new HashMap<String, String>();
//		Map<String, Object> map = new HashMap<String, Object>();
//		try {
//			Map<String, Object> result = dataTaskService.selectOrAddByFC(fcId);
//			Long total = (Long) result.get("total");
//			List<DataTaskTemp> list = (List<DataTaskTemp>) result.get("list");
//			/*
//			 * map.put("id", "");//编号 map.put("sampleId", "");//样本编号
//			 * map.put("crmPatientId", ""); map.put("form", "");//类型
//			 * 
//			 * map.put("dataTask-id", ""); map.put("dataTask-name", "");
//			 * map.put("MicroRNA", "");//ran map.put("type", "");//type
//			 * map.put("sampleInfo-id", "");//样本id map.put("sampleInfo-name",
//			 * "");//样本编号
//			 * 
//			 * map.put("state", "");//读取状态
//			 * 
//			 * map.put("poolingId", "");//pooling编号 map.put("sampleTypeId",
//			 * "");//样本类型ID map.put("sampleTypeName", "");//样本类型名称
//			 * map.put("cancerType", "");//癌症类型 map.put("cancerTypeSeedOne",
//			 * "");//癌症类型名称1 map.put("cancerTypeSeedTwo", "");//癌症类型名称2
//			 * map.put("productId", ""); 检测项目 map.put("productName", ""); 检测项目
//			 */
//			// new SendData().sendDateJson(map, list, total,
//			// ServletActionContext.getResponse());
//			map.put("data", list);
//			map.put("success", "true");
//		} catch (Exception e) {
//			e.printStackTrace();
//			map.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}

	/**
	 * 保存明细
	 */
	@Action(value = "saveDataTaskItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveDataTaskItem() throws Exception {

		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String dataJson = getRequest().getParameter("itemDataJson");
			String taskId = getRequest().getParameter("taskId");
			DataTask a = commonService.get(DataTask.class, taskId);
			dataTaskService.saveDataTaskItem(a, dataJson);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 删除明细
	 * 
	 * @throws Exception
	 */
	@Action(value = "delDataTaskItem")
	public void delDataTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			dataTaskService.delDataTaskItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public DataTaskService getDataTaskService() {
		return dataTaskService;
	}

	public void setDataTaskService(DataTaskService dataTaskService) {
		this.dataTaskService = dataTaskService;
	}

	public DataTask getDataTask() {
		return dataTask;
	}

	public void setDataTask(DataTask dataTask) {
		this.dataTask = dataTask;
	}

	public CommonService getCommonService() {
		return commonService;
	}

	public void setCommonService(CommonService commonService) {
		this.commonService = commonService;
	}

	public FileInfoService getFileInfoService() {
		return fileInfoService;
	}

	public void setFileInfoService(FileInfoService fileInfoService) {
		this.fileInfoService = fileInfoService;
	}

	/**
	 * 查询突变基因对应的信息
	 */
	@Action(value = "selDicType", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selDicType() throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String name = getRequest().getParameter("name");
			// 查询突变基因对应的基因别称
			DicType d = this.dataTaskDao.selDicTypejybc(name);
			// 根据突变基因查询转录本
			DicType d2 = this.dataTaskDao.selDicTypecyzlb(name);
			// 根据突变基因查询CNV专用染色体位置
			DicType d3 = this.dataTaskDao.selDicTypejyrstw(name);
			map.put("success", true);
			map.put("d", d);
			map.put("d2", d2);
			map.put("d3", d3);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据突变基因查询转录本
	 */
	@Action(value = "selDicTypecyzlb", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selDicTypecyzlb() throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String name = getRequest().getParameter("name");
			DicType d = this.dataTaskDao.selDicTypecyzlb(name);
			map.put("success", true);
			map.put("d", d);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 根据突变基因查询CNV专用染色体位置
	 */
	@Action(value = "selDicTypejyrstw", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selDicTypejyrstw() throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String name = getRequest().getParameter("name");
			DicType d = this.dataTaskDao.selDicTypejyrstw(name);
			map.put("success", true);
			map.put("d", d);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	/**
	 * @throws Exception 
	 * 
	 * @Title: projectAnalysis  
	 * @Description: 项目分析 
	 * @author : shengwei.wang
	 * @date 2018年6月1日上午10:33:56
	 * @return
	 * String
	 * @throws
	 */
	@Action(value="projectAnalysis")
	public String projectAnalysis() throws Exception{
		rightsId="7001";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/dataCenter/projectAnalysis.jsp");
	}
	/**
	 * @throws Exception 
	 * 
	 * @Title: dataMining  
	 * @Description:数据挖掘 
	 * @author : shengwei.wang
	 * @date 2018年6月1日上午10:34:06
	 * @return
	 * String
	 * @throws
	 */
	@Action(value="dataMining")
	public String dataMining() throws Exception{
		rightsId="7002";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/dataCenter/dataMining.jsp");
	}
	/**
	 * @throws Exception 
	 * 
	 * @Title: dossier  
	 * @Description: 病历夹
	 * @author : shengwei.wang
	 * @date 2018年6月1日上午10:32:04
	 * @return
	 * String
	 * @throws
	 */
	@Action(value="dossier")
	public String dossier() throws Exception{
		rightsId="7003";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/dataCenter/dossier.jsp");
	}
	/**
	 * @throws Exception 
	 * 
	 * @Title: geneBank  
	 * @Description: 基因库
	 * @author : shengwei.wang
	 * @date 2018年6月1日上午10:32:27
	 * @return
	 * String
	 * @throws
	 */
	@Action(value="geneBank")
	public String geneBank() throws Exception{
		rightsId="7004";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/dataCenter/geneBank.jsp");
	}
	/**
	 * @throws Exception 
	 *  
	 * @Title: documentBank  
	 * @Description: 文献库 
	 * @author : shengwei.wang
	 * @date 2018年6月1日上午10:32:44
	 * @return
	 * String
	 * @throws
	 */
	@Action(value="documentBank")
	public String documentBank() throws Exception{
		rightsId="7005";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/dataCenter/documentBank.jsp");
	}
	/**
	 * @throws Exception 
	 * 
	 * @Title: targetedDrugBank  
	 * @Description: 靶向药物库
	 * @author : shengwei.wang
	 * @date 2018年6月1日上午10:33:05
	 * @return
	 * String
	 * @throws
	 */
	@Action(value="targetedDrugBank")
	public String targetedDrugBank() throws Exception{
		rightsId="7006";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/dataCenter/targetedDrugBank.jsp");
	}
	/**
	 * @throws Exception 
	 * 
	 * @Title: mutableGeneBank  
	 * @Description: 突变基因库 
	 * @author : shengwei.wang
	 * @date 2018年6月1日上午10:35:46
	 * @return
	 * String
	 * @throws
	 */
	@Action(value="mutableGeneBank")
	public String mutableGeneBank() throws Exception{
		rightsId="7007";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/dataCenter/mutableGeneBank.jsp");
	}
	/**
	 * 
	 * @Title: snpCnvTables  
	 * @Description: TODO  
	 * @author : shengwei.wang
	 * @date 2018年6月5日下午1:44:31
	 * @return
	 * @throws Exception
	 * String
	 * @throws
	 */
	@Action(value="snpCnvTables")
	public String snpCnvTables() throws Exception{
		return dispatcher("/WEB-INF/page/dataCenter/snpCnvTables.jsp");
	}
}
