package com.biolims.analysis.data.action;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.omg.CORBA.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.analysis.data.model.DataTask;
import com.biolims.analysis.data.model.DataTaskItem;
import com.biolims.analysis.data.model.DataTaskSvItem;
import com.biolims.analysis.data.model.DataTaskSvnItem;
import com.biolims.analysis.data.model.DateTaskCnvItem;
import com.biolims.analysis.data.service.DataTaskService;
import com.biolims.common.action.BaseActionSupport;

import com.biolims.common.service.CommonService;
import com.biolims.file.service.FileInfoService;

import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/analysis/data/mutation")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class MutationAuditAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393496L;
	private String rightsId = "260107";
	@Autowired
	private DataTaskService dataTaskService;
	private DataTask dataTask = new DataTask();
	@Resource
	private CommonService commonService;
	@Resource
	private FileInfoService fileInfoService;

	/**
	 * 查询数据分析明细
	 * @throws Exception
	 */
	
	@Action(value = "showmutationAuditItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showmutationAuditItemList() throws Exception {	
		return dispatcher("/WEB-INF/page/analysis/data/mutation_dataTask.jsp");
	}

	@Action(value = "showmutationAuditItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showmutationAuditItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String scId = getRequest().getParameter("id");
		try {
			Map<String, Object> result = dataTaskService.findDataTaskItemList2(scId, startNum, limitNum,
					dir, sort);
			Long total = (Long) result.get("total");
			List<DataTaskItem> list = (List<DataTaskItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");//编号
			map.put("sampleId", "");//样本编号
			map.put("crmPatientId", "");
			map.put("name", "");//名称
			map.put("location", "");//位置
			map.put("xjPot", "");//碱基改变
			map.put("tbFrequence", "");//突变频率
			map.put("genePot", "");//基因改变
			map.put("hgmdPubmedid", "");//参考文献ID
			map.put("note", "");//注释
			map.put("form", "");//类型
			
			map.put("dataTask-id", "");
			map.put("dataTask-name", "");
			map.put("MicroRNA", "");//ran
			map.put("type", "");//type
			map.put("acceptUser-id", "");//实验员id
			map.put("acceptUser-name", "");//实验员名称
			map.put("acceptUser2-id", "");//实验员id2
			map.put("acceptUser2-name", "");//实验员名称2
			
			map.put("sampleInfo-id", "");//样本id
			map.put("sampleInfo-name", "");//样本编号
			
			map.put("method", "");//处理方法
			map.put("reason", "");//失败原因
			map.put("states", "");//通过状态

			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 保存明细
	 */
	@Action(value = "saveDataTaskItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveDataTaskItem() throws Exception {

		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String dataJson = getRequest().getParameter("itemDataJson");
			String taskId = getRequest().getParameter("taskId");
			DataTask a = commonService.get(DataTask.class, taskId);
			dataTaskService.saveDataTaskItem(a, dataJson);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	/**
	 * 删除明细
	 * @throws Exception
	 */
	@Action(value = "delDataTaskItem")
	public void delDataTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			dataTaskService.delDataTaskItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	/**
	 * SVNitem字表
	 * @return
	 * @throws Exception
	 */
	@Action(value = "mutationSvnItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String mutationSvnItemList() throws Exception {
		String scId = getRequest().getParameter("id");
		putObjToContext("sgtid", scId);
		return dispatcher("/WEB-INF/page/analysis/data/mutation_dataTaskSvnItem.jsp");
	}

	@Action(value = "mutationSvnItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void mutationSvnItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = dataTaskService.findDataTaskSvnItemList2(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<DataTaskSvnItem> list = (List<DataTaskSvnItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("mutantGenes", "");
			map.put("otherName", "");
			map.put("transcript", "");
			map.put("exon", "");
			map.put("aminoAcidMutation", "");
			map.put("baseMutation", "");
			map.put("mutationStartPosition", "");
			map.put("mutationStopPosition", "");
			map.put("mutationSource", "");
			map.put("mutationClass", "");
			map.put("mutationType", "");
			map.put("mutationStatus", "");
			map.put("mutatinAbundance", "");
			map.put("mutationFiction", "");
			map.put("dataTask-name", "");
			map.put("dataTask-id", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("createUser-name", "");
			map.put("createUser-id", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("sampleCode", "");
			map.put("acceptUser-id", "");
			map.put("acceptUser-name", "");
			map.put("validState", "");
			
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delDataTaskSvnItem")
	public void delDataTaskSvnItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			dataTaskService.delDataTaskSvnItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	/**
	 * svItem字表信息
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showmutationSvItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showmutationSvItemList() throws Exception {
		String scId = getRequest().getParameter("id");
		putObjToContext("sgtid", scId);
		return dispatcher("/WEB-INF/page/analysis/data/motation_dataTaskSvItem.jsp");
	}

	@Action(value = "showmutationSvItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showmutationSvItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = dataTaskService.findDataTaskSvItemList2(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<DataTaskSvItem> list = (List<DataTaskSvItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("mutantGenes", "");
			map.put("otherName", "");
			map.put("partnerGenes", "");
			map.put("fusionProduct", "");
			map.put("transcript", "");
			map.put("exon1", "");
			map.put("exon2", "");
			map.put("mutationStartPosition", "");
			map.put("mutationStopPosition", "");
			map.put("mutationSource", "");
			map.put("mutationClass", "");
			map.put("mutationType", "");
			map.put("mutationStatus", "");
			map.put("mutatinAbundance", "");
			map.put("mutationFiction", "");
			map.put("dataTask-name", "");
			map.put("dataTask-id", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("createUser-name", "");
			map.put("createUser-id", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("sampleCode", "");
			map.put("acceptUser-id", "");
			map.put("acceptUser-name", "");
			map.put("validState", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delDataTaskSvItem")
	public void delDataTaskSvItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			dataTaskService.delDataTaskSvItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	/**
	 * cnvItem字表信息
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showMutationCnvItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showMutationCnvItemList() throws Exception {
		String scId = getRequest().getParameter("id");
		putObjToContext("sgtid", scId);
		return dispatcher("/WEB-INF/page/analysis/data/mutation_dateTaskCnvItem.jsp");
	}

	@Action(value = "showMutationCnvItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showMutationCnvItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = dataTaskService.findDateTaskCnvItemList2(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<DateTaskCnvItem> list = (List<DateTaskCnvItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("mutantGenes", "");
			map.put("otherName", "");
			map.put("copyNumberVaration", "");
			map.put("copyVarationRatio", "");
			map.put("mutationSource", "");
			map.put("mutationClass", "");
			map.put("mutationType", "");
			map.put("mutationFiction", "");
			map.put("dataTask-name", "");
			map.put("dataTask-id", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("createUser-name", "");
			map.put("createUser-id", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("sampleCode", "");
			map.put("acceptUser-id", "");
			map.put("acceptUser-name", "");
			map.put("validState", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delDateTaskCnvItem")
	public void delDateTaskCnvItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			dataTaskService.delDateTaskCnvItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	/**
	 * 保存
	 * @return
	 * @throws Exception
	 */
	@Action(value = "saveMutationItem")
	public String saveMutationItem() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		
		try {
			Map aMap = new HashMap();
			
			aMap.put("mutationdateTaskItem",getParameterFromRequest("mutationItemJson"));
			aMap.put("mutationSvnItem",getParameterFromRequest("mutationSvnItemJson"));
			aMap.put("mutationSvItem",getParameterFromRequest("mutationSvItemJson"));
			aMap.put("mutationCnvItem",getParameterFromRequest("mutationCnvItemJson"));
			
			dataTaskService.saveMutation(aMap);
			result.put("success", true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result.put("success", false);
		}
		
		
		HttpUtils.write(JsonUtils.toJsonString(result));
		return redirect("/analysis/data/mutation/showmutationAuditItemList.action");

	}
	
	/**
	 * 
	 * 选中 查询
	 * @return
	 */
	@Action(value = "showItemListById", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showItemListById() throws Exception {	
		String scId = getRequest().getParameter("id");
		putObjToContext("selectId", scId);
		return dispatcher("/WEB-INF/page/analysis/data/mutation_dataTask.jsp");
	}
	

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public DataTaskService getDataTaskService() {
		return dataTaskService;
	}

	public void setDataTaskService(DataTaskService dataTaskService) {
		this.dataTaskService = dataTaskService;
	}

	public DataTask getDataTask() {
		return dataTask;
	}

	public void setDataTask(DataTask dataTask) {
		this.dataTask = dataTask;
	}

	public CommonService getCommonService() {
		return commonService;
	}

	public void setCommonService(CommonService commonService) {
		this.commonService = commonService;
	}

	public FileInfoService getFileInfoService() {
		return fileInfoService;
	}

	public void setFileInfoService(FileInfoService fileInfoService) {
		this.fileInfoService = fileInfoService;
	}
	

}
