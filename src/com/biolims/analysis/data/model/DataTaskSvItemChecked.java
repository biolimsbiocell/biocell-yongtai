package com.biolims.analysis.data.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.common.model.user.User;
/**   
 * @Title: Model
 * @Description: 基因融合突变表
 * @author lims-platform
 * @date 2016-04-18 13:08:19
 * @version V1.0   
 *
 */
@Entity
@Table(name = "DATA_TASK_SVITEM_CHECKED")
@SuppressWarnings("serial")
public class DataTaskSvItemChecked extends EntityDao<DataTaskSvItemChecked> implements java.io.Serializable {
	/**id*/
	private String id;
	/**突变基因*/
	private String mutantGenes;
	/**其他名称*/
	private String otherName;
	/**伙伴基因*/
	private String partnerGenes;
	/**融合产物*/
	private String fusionProduct;
	/**转录本*/
	private String transcript;
	/**外显子1*/
	private String exon1;
	/**外显子2*/
	private String exon2;
	/**突变起始位置*/
	private String mutationStartPosition;
	/**突变终止位置*/
	private String mutationStopPosition;
	/**突变来源*/
	private String mutationSource;
	/**突变分类*/
	private String mutationClass;
	/**突变类型*/
	private String mutationType;
	/**突变状态*/
	private String mutationStatus;
	/**突变丰度*/
	private String mutatinAbundance;
	/**突变分级*/
	private String mutationFiction;
	/**相关主表*/
	private DataTask dataTask;
	/**下达时间*/
	private Date createDate;
	/**下达人*/
	private User createUser;
	/**工作流状态*/
	private String state;
	/**状态描述*/
	private String stateName;
	/**样本编号*/
	private String sampleCode;
	
	/**
	 *方法: 取得String
	 *@return: String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  突变基因
	 */
	@Column(name ="MUTANT_GENES", length = 50)
	public String getMutantGenes(){
		return this.mutantGenes;
	}
	/**
	 *方法: 设置String
	 *@param: String  突变基因
	 */
	public void setMutantGenes(String mutantGenes){
		this.mutantGenes = mutantGenes;
	}
	/**
	 *方法: 取得String
	 *@return: String  其他名称
	 */
	@Column(name ="OTHER_NAME", length = 50)
	public String getOtherName(){
		return this.otherName;
	}
	/**
	 *方法: 设置String
	 *@param: String  其他名称
	 */
	public void setOtherName(String otherName){
		this.otherName = otherName;
	}
	/**
	 *方法: 取得String
	 *@return: String  伙伴基因
	 */
	@Column(name ="PARTNER_GENES", length = 50)
	public String getPartnerGenes(){
		return this.partnerGenes;
	}
	/**
	 *方法: 设置String
	 *@param: String  伙伴基因
	 */
	public void setPartnerGenes(String partnerGenes){
		this.partnerGenes = partnerGenes;
	}
	/**
	 *方法: 取得String
	 *@return: String  融合产物
	 */
	@Column(name ="FUSION_PRODUCT", length = 50)
	public String getFusionProduct(){
		return this.fusionProduct;
	}
	/**
	 *方法: 设置String
	 *@param: String  融合产物
	 */
	public void setFusionProduct(String fusionProduct){
		this.fusionProduct = fusionProduct;
	}
	/**
	 *方法: 取得String
	 *@return: String  转录本
	 */
	@Column(name ="TRANSCRIPT", length = 50)
	public String getTranscript(){
		return this.transcript;
	}
	/**
	 *方法: 设置String
	 *@param: String  转录本
	 */
	public void setTranscript(String transcript){
		this.transcript = transcript;
	}
	/**
	 *方法: 取得String
	 *@return: String  外显子1
	 */
	@Column(name ="EXON1", length = 50)
	public String getExon1(){
		return this.exon1;
	}
	/**
	 *方法: 设置String
	 *@param: String  外显子1
	 */
	public void setExon1(String exon1){
		this.exon1 = exon1;
	}
	/**
	 *方法: 取得String
	 *@return: String  外显子2
	 */
	@Column(name ="EXON2", length = 50)
	public String getExon2(){
		return this.exon2;
	}
	/**
	 *方法: 设置String
	 *@param: String  外显子2
	 */
	public void setExon2(String exon2){
		this.exon2 = exon2;
	}
	/**
	 *方法: 取得String
	 *@return: String  突变起始位置
	 */
	@Column(name ="MUTATION_START_POSITION", length = 50)
	public String getMutationStartPosition(){
		return this.mutationStartPosition;
	}
	/**
	 *方法: 设置String
	 *@param: String  突变起始位置
	 */
	public void setMutationStartPosition(String mutationStartPosition){
		this.mutationStartPosition = mutationStartPosition;
	}
	/**
	 *方法: 取得String
	 *@return: String  突变终止位置
	 */
	@Column(name ="MUTATION_STOP_POSITION", length = 50)
	public String getMutationStopPosition(){
		return this.mutationStopPosition;
	}
	/**
	 *方法: 设置String
	 *@param: String  突变终止位置
	 */
	public void setMutationStopPosition(String mutationStopPosition){
		this.mutationStopPosition = mutationStopPosition;
	}
	/**
	 *方法: 取得String
	 *@return: String  突变来源
	 */
	@Column(name ="MUTATION_SOURCE", length = 50)
	public String getMutationSource(){
		return this.mutationSource;
	}
	/**
	 *方法: 设置String
	 *@param: String  突变来源
	 */
	public void setMutationSource(String mutationSource){
		this.mutationSource = mutationSource;
	}
	/**
	 *方法: 取得String
	 *@return: String  突变分类
	 */
	@Column(name ="MUTATION_CLASS", length = 50)
	public String getMutationClass(){
		return this.mutationClass;
	}
	/**
	 *方法: 设置String
	 *@param: String  突变分类
	 */
	public void setMutationClass(String mutationClass){
		this.mutationClass = mutationClass;
	}
	/**
	 *方法: 取得String
	 *@return: String  突变类型
	 */
	@Column(name ="MUTATION_TYPE", length = 50)
	public String getMutationType(){
		return this.mutationType;
	}
	/**
	 *方法: 设置String
	 *@param: String  突变类型
	 */
	public void setMutationType(String mutationType){
		this.mutationType = mutationType;
	}
	/**
	 *方法: 取得String
	 *@return: String  突变状态
	 */
	@Column(name ="MUTATION_STATUS", length = 50)
	public String getMutationStatus(){
		return this.mutationStatus;
	}
	/**
	 *方法: 设置String
	 *@param: String  突变状态
	 */
	public void setMutationStatus(String mutationStatus){
		this.mutationStatus = mutationStatus;
	}
	/**
	 *方法: 取得String
	 *@return: String  突变丰度
	 */
	@Column(name ="MUTATIN_ABUNDANCE", length = 50)
	public String getMutatinAbundance(){
		return this.mutatinAbundance;
	}
	/**
	 *方法: 设置String
	 *@param: String  突变丰度
	 */
	public void setMutatinAbundance(String mutatinAbundance){
		this.mutatinAbundance = mutatinAbundance;
	}
	/**
	 *方法: 取得String
	 *@return: String  突变分级
	 */
	@Column(name ="MUTATION_FICTION", length = 50)
	public String getMutationFiction(){
		return this.mutationFiction;
	}
	/**
	 *方法: 设置String
	 *@param: String  突变分级
	 */
	public void setMutationFiction(String mutationFiction){
		this.mutationFiction = mutationFiction;
	}
	/**
	 *方法: 取得DataTask
	 *@return: DataTask  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DATA_TASK")
	public DataTask getDataTask(){
		return this.dataTask;
	}
	/**
	 *方法: 设置DataTask
	 *@param: DataTask  相关主表
	 */
	public void setDataTask(DataTask dataTask){
		this.dataTask = dataTask;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  下达时间
	 */
	@Column(name ="CREATE_DATE", length = 100)
	public Date getCreateDate(){
		return this.createDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  下达时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 *方法: 取得User
	 *@return: User  下达人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser(){
		return this.createUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  下达人
	 */
	public void setCreateUser(User createUser){
		this.createUser = createUser;
	}
	/**
	 *方法: 取得String
	 *@return: String  工作流状态
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  工作流状态
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态描述
	 */
	@Column(name ="STATE_NAME", length = 50)
	public String getStateName(){
		return this.stateName;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态描述
	 */
	public void setStateName(String stateName){
		this.stateName = stateName;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本编号
	 */
	@Column(name ="SAMPLE_CODE", length = 50)
	public String getSampleCode(){
		return this.sampleCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本编号
	 */
	public void setSampleCode(String sampleCode){
		this.sampleCode = sampleCode;
	}
	
}