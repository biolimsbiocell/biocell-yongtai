package com.biolims.analysis.data.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 肿瘤突变表
 * @author lims-platform
 * @date 2016-04-09 16:37:21
 * @version V1.0
 * 
 */
@Entity
@Table(name = "CNV_RESULT")
@SuppressWarnings("serial")
public class CnvResult extends EntityDao<CnvResult> implements Serializable {
	/** id */
	private String id;
	/** 样本号 */
	private String code;
	/** 订单号 */
	private String orderCode;
	/** 电子病历号 */
	private String medicalNumber;
	/** 突变基因 */
	private String mutantGenes;
	/** 其他名称 */
	private String otherName;
	/** 染色体位置 */
	private String mutationPosition;
	/** 拷贝数变异 */
	private String copyNumberVaration;
	/** 拷贝数变异总外显子 */
	private String copyNumberVarationExon;
	/** 拷贝数变异倍数 */
	private String copyVarationRatio;
	/** 突变分类 */
	private String mutationClass;
	/** 靶向相关突变分级 */
	private String relatedMutation;
	/** 化疗相关突变分级 */
	private String relatedChemotherapy;
	/** 相关主表 */
	private DataTask dataTask;
	// 病人姓名
	private String patientName;

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getMutationPosition() {
		return mutationPosition;
	}

	public void setMutationPosition(String mutationPosition) {
		this.mutationPosition = mutationPosition;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String id
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 突变基因
	 */
	@Column(name = "MUTANT_GENES", length = 200)
	public String getMutantGenes() {
		return this.mutantGenes;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 突变基因
	 */
	public void setMutantGenes(String mutantGenes) {
		this.mutantGenes = mutantGenes;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 其他名称
	 */
	@Column(name = "OTHER_NAME", length = 200)
	public String getOtherName() {
		return this.otherName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 其他名称
	 */
	public void setOtherName(String otherName) {
		this.otherName = otherName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 拷贝数变异
	 */
	@Column(name = "COPY_NUMBER_VARATION", length = 200)
	public String getCopyNumberVaration() {
		return this.copyNumberVaration;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 拷贝数变异
	 */
	public void setCopyNumberVaration(String copyNumberVaration) {
		this.copyNumberVaration = copyNumberVaration;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 拷贝数变异倍数
	 */
	@Column(name = "COPY_VARATION_RATIO", length = 200)
	public String getCopyVarationRatio() {
		return this.copyVarationRatio;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 拷贝数变异倍数
	 */
	public void setCopyVarationRatio(String copyVarationRatio) {
		this.copyVarationRatio = copyVarationRatio;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 突变分类
	 */
	@Column(name = "MUTATION_CLASS", length = 200)
	public String getMutationClass() {
		return this.mutationClass;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 突变分类
	 */
	public void setMutationClass(String mutationClass) {
		this.mutationClass = mutationClass;
	}

	/**
	 * 方法: 取得DataTask
	 * 
	 * @return: DataTask 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DATA_TASK")
	public DataTask getDataTask() {
		return this.dataTask;
	}

	/**
	 * 方法: 设置DataTask
	 * 
	 * @param: DataTask 相关主表
	 */
	public void setDataTask(DataTask dataTask) {
		this.dataTask = dataTask;
	}

	@Column(name = "COPY_NUMBER_VARATION_EXON", length = 200)
	public String getCopyNumberVarationExon() {
		return copyNumberVarationExon;
	}

	public void setCopyNumberVarationExon(String copyNumberVarationExon) {
		this.copyNumberVarationExon = copyNumberVarationExon;
	}

	public String getRelatedMutation() {
		return relatedMutation;
	}

	public void setRelatedMutation(String relatedMutation) {
		this.relatedMutation = relatedMutation;
	}

	public String getRelatedChemotherapy() {
		return relatedChemotherapy;
	}

	public void setRelatedChemotherapy(String relatedChemotherapy) {
		this.relatedChemotherapy = relatedChemotherapy;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getMedicalNumber() {
		return medicalNumber;
	}

	public void setMedicalNumber(String medicalNumber) {
		this.medicalNumber = medicalNumber;
	}

}