package com.biolims.analysis.data.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 单碱基突变表
 * @author lims-platform
 * @date 2016-04-09 16:37:19
 * @version V1.0
 * 
 */
@Entity
@Table(name = "SNV_RESULT")
@SuppressWarnings("serial")
public class SnvResult extends EntityDao<SnvResult> implements Serializable {
	/** id */
	private String id;
	/** 样本号 */
	private String code;
	/** 订单号 */
	private String orderCode;
	/** 电子病历号 */
	private String medicalNumber;
	/** 突变基因 */
	private String mutantGenes;
	/** 其他名称 */
	private String otherName;
	/** 转录本 */
	private String transcript;
	/** 突变基因位置 */
	private String mutantGeneLc;
	/** 氨基酸突变 */
	private String aminoAcidMutation;
	/** 碱基突变 */
	private String baseMutation;
	/** 参考碱基 */
	private String referenceBase;
	/** 突变碱基 */
	private String mutantBase;
	/** 染色体起始位置 */
	private String mutationStartPosition;
	/** 染色体终止位置 */
	private String mutationStopPosition;
	/** 突变分类 */
	private String mutationClass;
	/** 突变类型 */
	private String mutationType;
	/** 纯杂合状态 */
	private String mutationStatus;
	/** 突变丰度 */
	private String mutatinAbundance;
	/** 靶向相关突变分级 */
	private String relatedMutation;
	/** 化疗相关突变分级 */
	private String relatedChemotherapy;
	/** 相关主表 */
	private DataTask dataTask;
	// 病人姓名
	private String patientName;

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String id
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 突变基因
	 */
	@Column(name = "MUTANT_GENES", length = 200)
	public String getMutantGenes() {
		return this.mutantGenes;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 突变基因
	 */
	public void setMutantGenes(String mutantGenes) {
		this.mutantGenes = mutantGenes;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 其他名称
	 */
	@Column(name = "OTHER_NAME", length = 200)
	public String getOtherName() {
		return this.otherName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 其他名称
	 */
	public void setOtherName(String otherName) {
		this.otherName = otherName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 转录本
	 */
	@Column(name = "TRANSCRIPT", length = 200)
	public String getTranscript() {
		return this.transcript;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 转录本
	 */
	public void setTranscript(String transcript) {
		this.transcript = transcript;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 氨基酸突变
	 */
	@Column(name = "AMINO_ACID_MUTATION", length = 200)
	public String getAminoAcidMutation() {
		return this.aminoAcidMutation;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 氨基酸突变
	 */
	public void setAminoAcidMutation(String aminoAcidMutation) {
		this.aminoAcidMutation = aminoAcidMutation;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 碱基突变
	 */
	@Column(name = "BASE_MUTATION", length = 200)
	public String getBaseMutation() {
		return this.baseMutation;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 碱基突变
	 */
	public void setBaseMutation(String baseMutation) {
		this.baseMutation = baseMutation;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 突变起始位置
	 */
	@Column(name = "MUTATION_START_POSITION", length = 200)
	public String getMutationStartPosition() {
		return this.mutationStartPosition;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 突变起始位置
	 */
	public void setMutationStartPosition(String mutationStartPosition) {
		this.mutationStartPosition = mutationStartPosition;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 突变终止位置
	 */
	@Column(name = "MUTATION_STOP_POSITION", length = 200)
	public String getMutationStopPosition() {
		return this.mutationStopPosition;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 突变终止位置
	 */
	public void setMutationStopPosition(String mutationStopPosition) {
		this.mutationStopPosition = mutationStopPosition;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 突变分类
	 */
	@Column(name = "MUTATION_CLASS", length = 200)
	public String getMutationClass() {
		return this.mutationClass;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 突变分类
	 */
	public void setMutationClass(String mutationClass) {
		this.mutationClass = mutationClass;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 突变类型
	 */
	@Column(name = "MUTATION_TYPE", length = 200)
	public String getMutationType() {
		return this.mutationType;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 突变类型
	 */
	public void setMutationType(String mutationType) {
		this.mutationType = mutationType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 突变状态
	 */
	@Column(name = "MUTATION_STATUS", length = 200)
	public String getMutationStatus() {
		return this.mutationStatus;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 突变状态
	 */
	public void setMutationStatus(String mutationStatus) {
		this.mutationStatus = mutationStatus;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 突变丰度
	 */
	@Column(name = "MUTATIN_ABUNDANCE", length = 200)
	public String getMutatinAbundance() {
		return this.mutatinAbundance;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 突变丰度
	 */
	public void setMutatinAbundance(String mutatinAbundance) {
		this.mutatinAbundance = mutatinAbundance;
	}

	/**
	 * 方法: 取得DataTask
	 * 
	 * @return: DataTask 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DATA_TASK")
	public DataTask getDataTask() {
		return this.dataTask;
	}

	/**
	 * 方法: 设置DataTask
	 * 
	 * @param: DataTask 相关主表
	 */
	public void setDataTask(DataTask dataTask) {
		this.dataTask = dataTask;
	}

	@Column(name = "MUTANT_GENE", length = 200)
	public String getMutantGeneLc() {
		return mutantGeneLc;
	}

	public void setMutantGeneLc(String mutantGeneLc) {
		this.mutantGeneLc = mutantGeneLc;
	}

	public String getReferenceBase() {
		return referenceBase;
	}

	public void setReferenceBase(String referenceBase) {
		this.referenceBase = referenceBase;
	}

	public String getMutantBase() {
		return mutantBase;
	}

	public void setMutantBase(String mutantBase) {
		this.mutantBase = mutantBase;
	}

	public String getRelatedMutation() {
		return relatedMutation;
	}

	public void setRelatedMutation(String relatedMutation) {
		this.relatedMutation = relatedMutation;
	}

	public String getRelatedChemotherapy() {
		return relatedChemotherapy;
	}

	public void setRelatedChemotherapy(String relatedChemotherapy) {
		this.relatedChemotherapy = relatedChemotherapy;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getMedicalNumber() {
		return medicalNumber;
	}

	public void setMedicalNumber(String medicalNumber) {
		this.medicalNumber = medicalNumber;
	}

}