package com.biolims.analysis.data.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 基因融合突变表
 * @author lims-platform
 * @date 2016-04-09 16:37:20
 * @version V1.0
 * 
 */
@Entity
@Table(name = "SV_RESULT")
@SuppressWarnings("serial")
public class SvResult extends EntityDao<SvResult> implements
		java.io.Serializable {
	/** id */
	private String id;
	/** 样本号 */
	private String code;
	/** 订单号 */
	private String orderCode;
	/** 电子病历号 */
	private String medicalNumber;
	/** 突变基因 */
	private String mutantGenes;
	/** 突变基因其他名称 */
	private String otherName;
	/** 伙伴基因 */
	private String partnerGenes;
	/** 伙伴基因其他名称 */
	private String partnerGenesName;
	/** 融合产物 */
	private String fusionProduct;
	/** 突变基因转录本 */
	private String mutantGeneScript;
	/** 伙伴基因转录本 */
	private String partnerScript;
	/** 突变基因位置 */
	private String MutantGeneLc;
	/** 伙伴基因位置 */
	private String partnerLc;
	/** 突变基因染色体断裂位点 */
	private String mutantGenePoint;
	/** 伙伴基因染色体断裂位点 */
	private String partnerPoint;
	/** 突变分类 */
	private String mutationClass;
	/** 突变丰度 */
	private String mutatinAbundance;
	/** 靶向相关突变分级 */
	private String relatedMutation;
	/** 化疗相关突变分级 */
	private String relatedChemotherapy;
	/** 相关主表 */
	private DataTask dataTask;
	// 病人姓名
	private String patientName;

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 突变基因
	 */
	@Column(name = "MUTANT_GENES", length = 200)
	public String getMutantGenes() {
		return this.mutantGenes;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 突变基因
	 */
	public void setMutantGenes(String mutantGenes) {
		this.mutantGenes = mutantGenes;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 其他名称
	 */
	@Column(name = "OTHER_NAME", length = 200)
	public String getOtherName() {
		return this.otherName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 其他名称
	 */
	public void setOtherName(String otherName) {
		this.otherName = otherName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 伙伴基因
	 */
	@Column(name = "PARTNER_GENES", length = 200)
	public String getPartnerGenes() {
		return this.partnerGenes;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 伙伴基因
	 */
	public void setPartnerGenes(String partnerGenes) {
		this.partnerGenes = partnerGenes;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 融合产物
	 */
	@Column(name = "FUSION_PRODUCT", length = 200)
	public String getFusionProduct() {
		return this.fusionProduct;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 融合产物
	 */
	public void setFusionProduct(String fusionProduct) {
		this.fusionProduct = fusionProduct;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 突变分类
	 */
	@Column(name = "MUTATION_CLASS", length = 200)
	public String getMutationClass() {
		return this.mutationClass;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 突变分类
	 */
	public void setMutationClass(String mutationClass) {
		this.mutationClass = mutationClass;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 突变丰度
	 */
	@Column(name = "MUTATIN_ABUNDANCE", length = 200)
	public String getMutatinAbundance() {
		return this.mutatinAbundance;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 突变丰度
	 */
	public void setMutatinAbundance(String mutatinAbundance) {
		this.mutatinAbundance = mutatinAbundance;
	}

	/**
	 * 方法: 取得DataTask
	 * 
	 * @return: DataTask 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DATA_TASK")
	public DataTask getDataTask() {
		return this.dataTask;
	}

	/**
	 * 方法: 设置DataTask
	 * 
	 * @param: DataTask 相关主表
	 */
	public void setDataTask(DataTask dataTask) {
		this.dataTask = dataTask;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本编号
	 */
	@Column(name = "CODE", length = 50)
	public String getCode() {
		return code;
	}

	public void setCode(String Code) {
		this.code = Code;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getMedicalNumber() {
		return medicalNumber;
	}

	public void setMedicalNumber(String medicalNumber) {
		this.medicalNumber = medicalNumber;
	}

	@Column(name = "MUTANT_GENE_SCRIPT", length = 200)
	public String getMutantGeneScript() {
		return mutantGeneScript;
	}

	@Column(name = "PARTNER_SCRIPT", length = 200)
	public String getPartnerScript() {
		return partnerScript;
	}

	@Column(name = "MUTANT_GENE_POINT", length = 200)
	public String getMutantGenePoint() {
		return mutantGenePoint;
	}

	@Column(name = "PARTNER_POINT", length = 200)
	public String getPartnerPoint() {
		return partnerPoint;
	}

	public void setMutantGeneScript(String mutantGeneScript) {
		this.mutantGeneScript = mutantGeneScript;
	}

	public void setPartnerScript(String partnerScript) {
		this.partnerScript = partnerScript;
	}

	public void setMutantGenePoint(String mutantGenePoint) {
		this.mutantGenePoint = mutantGenePoint;
	}

	public void setPartnerPoint(String partnerPoint) {
		this.partnerPoint = partnerPoint;
	}

	public String getRelatedMutation() {
		return relatedMutation;
	}

	public void setRelatedMutation(String relatedMutation) {
		this.relatedMutation = relatedMutation;
	}

	public String getRelatedChemotherapy() {
		return relatedChemotherapy;
	}

	public void setRelatedChemotherapy(String relatedChemotherapy) {
		this.relatedChemotherapy = relatedChemotherapy;
	}

	public String getPartnerGenesName() {
		return partnerGenesName;
	}

	public void setPartnerGenesName(String partnerGenesName) {
		this.partnerGenesName = partnerGenesName;
	}

	public String getMutantGeneLc() {
		return MutantGeneLc;
	}

	public void setMutantGeneLc(String mutantGeneLc) {
		MutantGeneLc = mutantGeneLc;
	}

	public String getPartnerLc() {
		return partnerLc;
	}

	public void setPartnerLc(String partnerLc) {
		this.partnerLc = partnerLc;
	}

}