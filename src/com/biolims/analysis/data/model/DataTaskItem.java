package com.biolims.analysis.data.model;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.sample.model.SampleInfo;
import com.biolims.weichat.model.WeiChatCancerType;
import com.biolims.weichat.model.WeiChatCancerTypeSeedOne;
import com.biolims.weichat.model.WeiChatCancerTypeSeedTwo;
import com.biolims.common.model.user.User;
/**   
 * @Title: Model
 * @Description: 数据分析结果明细表
 * @author lims-platform
 * @date 2016-04-09 16:37:19
 * @version V1.0   
 *
 */
@Entity
@Table(name = "DATA_TASK_ITEM")
@SuppressWarnings("serial")
public class DataTaskItem extends EntityDao<DataTaskItem> implements java.io.Serializable {
	/**id*/
	private String id;
	/**样本编号*/
	private String sampleId;
	/**原始样本编号*/
	private String sampleCode;
	/**电子病历编号*/
	private String crmPatientId;
	
	/**名称*/
	private String name;
	/**位置*/
	private String location;
	/**碱基改变*/
	private String xjPot;
	/**突变频率*/
	private String tbFrequence;
	/**基因改变*/
	private String genePot;
	/**参考文献id*/
	private String hgmdPubmedid;
	/**注释*/
	private String note;
	/**类型*/
	private String form;
	/**外键*/
	private DataTask dataTask;
	/**MicroRNA;*/
	private String MicroRNA;
	/**图片路径*/
	private String path;
	/**type;*/
	private String type;
	/**实验员*/
	private User acceptUser;
	/**实验员*/
	private User acceptUser2;
	/**样本*/
	private SampleInfo sampleInfo;	
	/**失败原因*/
	private String reason;
	/**处理方式*/
	private String method;
	/**工作流状态*/
	private String states;
	/**pooling编号*/
	private String poolingId;
	/**文库编号*/
	private String wkTaskId;
	/**样本类型id*/
	private String sampleTypeId;
	/**样本类型*/
	private String sampleTypeName;
	/**分期*/
	private String sampleStage;
	/**癌症种类*/
	private String  cancerType;
	/**子类一*/
	private String  cancerTypeSeedOne;
	/**子类二*/
	private String  cancerTypeSeedTwo;
	/**临时表ID*/
	private String  tempId;
	/* 检测项目 */
	private String productId;
	private String productName;
	/**
	 *方法: 取得String
	 *@return: String  id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  id
	 */
	public void setId(String id){
		this.id = id;
	}
	

	@Column(name = "SAMPLE_ID", length = 200)
	public String getSampleId() {
		return sampleId;
	}
	public void setSampleId(String sampleId) {
		this.sampleId = sampleId;
	}
	@Column(name = "SAMPLE_CODE", length = 200)
	public String getSampleCode() {
		return sampleCode;
	}
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}
	@Column(name = "CRMPATIENT_ID", length = 200)
	public String getCrmPatientId() {
		return crmPatientId;
	}
	public void setCrmPatientId(String crmPatientId) {
		this.crmPatientId = crmPatientId;
	}
	@Column(name = "NAME", length = 200)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Column(name = "LOCATION", length = 200)
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	@Column(name = "XJ_POT", length = 200)
	public String getXjPot() {
		return xjPot;
	}
	public void setXjPot(String xjPot) {
		this.xjPot = xjPot;
	}
	@Column(name = "TB_FREQUENCE", length = 200)
	public String getTbFrequence() {
		return tbFrequence;
	}
	public void setTbFrequence(String tbFrequence) {
		this.tbFrequence = tbFrequence;
	}
	@Column(name = "GENE_POT", length = 200)
	public String getGenePot() {
		return genePot;
	}
	public void setGenePot(String genePot) {
		this.genePot = genePot;
	}
	@Column(name = "HGMD_PUBMEDID", length = 200)
	public String getHgmdPubmedid() {
		return hgmdPubmedid;
	}
	public void setHgmdPubmedid(String hgmdPubmedid) {
		this.hgmdPubmedid = hgmdPubmedid;
	}
	@Column(name = "NOTE", length = 200)
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	@Column(name = "FORM", length = 200)
	public String getForm() {
		return form;
	}
	public void setForm(String form) {
		this.form = form;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DATA_TASK")
	public DataTask getDataTask() {
		return dataTask;
	}
	public void setDataTask(DataTask dataTask) {
		this.dataTask = dataTask;
	}
	@Column(name = "MICRO_RNA", length = 200)
	public String getMicroRNA() {
		return MicroRNA;
	}
	public void setMicroRNA(String microRNA) {
		MicroRNA = microRNA;
	}
	@Column(name = "PATH", length = 200)
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	@Column(name = "TYPE", length = 200)
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "USER_ACCEPT_USER_ID")
	public User getAcceptUser() {
		return acceptUser;
	}
	public void setAcceptUser(User acceptUser) {
		this.acceptUser = acceptUser;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "USER_ACCEPT_USER_ID2")
	public User getAcceptUser2() {
		return acceptUser2;
	}
	public void setAcceptUser2(User acceptUser2) {
		this.acceptUser2 = acceptUser2;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO_ID")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}
	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}
	@Column(name = "REASON", length = 200)
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	@Column(name = "METHOD", length = 200)
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	@Column(name = "STATES", length = 200)
	public String getStates() {
		return states;
	}
	public void setStates(String states) {
		this.states = states;
	}
	
	
	
	//新增字段
	public String getPoolingId() {
		return poolingId;
	}
	public void setPoolingId(String poolingId) {
		this.poolingId = poolingId;
	}
	public String getWkTaskId() {
		return wkTaskId;
	}
	public void setWkTaskId(String wkTaskId) {
		this.wkTaskId = wkTaskId;
	}
	public String getSampleTypeId() {
		return sampleTypeId;
	}
	public void setSampleTypeId(String sampleTypeId) {
		this.sampleTypeId = sampleTypeId;
	}
	public String getSampleTypeName() {
		return sampleTypeName;
	}
	public void setSampleTypeName(String sampleTypeName) {
		this.sampleTypeName = sampleTypeName;
	}
	public String getSampleStage() {
		return sampleStage;
	}
	public void setSampleStage(String sampleStage) {
		this.sampleStage = sampleStage;
	}
	public String getCancerType() {
		return cancerType;
	}
	public void setCancerType(String cancerType) {
		this.cancerType = cancerType;
	}
	public String getCancerTypeSeedOne() {
		return cancerTypeSeedOne;
	}
	public void setCancerTypeSeedOne(String cancerTypeSeedOne) {
		this.cancerTypeSeedOne = cancerTypeSeedOne;
	}
	public String getCancerTypeSeedTwo() {
		return cancerTypeSeedTwo;
	}
	public void setCancerTypeSeedTwo(String cancerTypeSeedTwo) {
		this.cancerTypeSeedTwo = cancerTypeSeedTwo;
	}
	
	@Column(name = "TEMPID", length = 200)
	public String getTempId() {
		return tempId;
	}
	public void setTempId(String tempId) {
		this.tempId = tempId;
	}
	@Column(name = "PRODUCT_ID", length = 200)
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	@Column(name = "PRODUCT_NAME", length = 200)
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	
}