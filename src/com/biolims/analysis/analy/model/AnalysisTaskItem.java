package com.biolims.analysis.analy.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.system.template.model.Template;
/**   
 * @Title: Model
 * @Description: 样本明细
 * @author lims-platform
 * @date 2015-12-07 09:37:59
 * @version V1.0   
 *
 */
@Entity
@Table(name = "ANALYSIS_TASK_ITEM")
@SuppressWarnings("serial")
public class AnalysisTaskItem extends EntityDao<AnalysisTaskItem> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
	/**pooling号*/
	private String poolingCode;
	/**文库号*/
	private String wkCode;
	/**样本号*/
	private String sampleCode;
	/**分析员*/
	private User analyzer;
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "ANALYZER")
	public User getAnalyzer() {
		return analyzer;
	}
	public void setAnalyzer(User analyzer) {
		this.analyzer = analyzer;
	}
	/**状态*/
	private String status;
	/**备注*/
	private String note;
	/**关联主表*/
	private AnalysisTask analysisTask;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  pooling号
	 */
	@Column(name ="POOLING_CODE", length = 50)
	public String getPoolingCode(){
		return this.poolingCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  pooling号
	 */
	public void setPoolingCode(String poolingCode){
		this.poolingCode = poolingCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  文库号
	 */
	@Column(name ="WK_CODE", length = 50)
	public String getWkCode(){
		return this.wkCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  文库号
	 */
	public void setWkCode(String wkCode){
		this.wkCode = wkCode;
	}
	/**
	 *方法: 取得Template
	 *@return: Template  样本号
	 */
	@Column(name ="SAMPLE_CODE", length = 50)
	public String getSampleCode(){
		return this.sampleCode;
	}
	/**
	 *方法: 设置Template
	 *@param: Template  样本号
	 */
	public void setSampleCode(String sampleCode){
		this.sampleCode = sampleCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATUS", length = 50)
	public String getStatus(){
		return this.status;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setStatus(String status){
		this.status = status;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得AnalysisTask
	 *@return: AnalysisTask  关联主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "ANALYSIS_TASK")
	public AnalysisTask getAnalysisTask(){
		return this.analysisTask;
	}
	/**
	 *方法: 设置AnalysisTask
	 *@param: AnalysisTask  关联主表
	 */
	public void setAnalysisTask(AnalysisTask analysisTask){
		this.analysisTask = analysisTask;
	}
}