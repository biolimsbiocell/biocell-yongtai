package com.biolims.analysis.analy.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.analysis.desequencing.model.DeSequencingTask;
import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 信息分析
 * @author lims-platform
 * @date 2015-12-07 09:38:18
 * @version V1.0   
 *
 */
@Entity
@Table(name = "ANALYSIS_TASK")
@SuppressWarnings("serial")
public class AnalysisTask extends EntityDao<AnalysisTask> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
	/**预下机时间*/
	private Date downComputerTime;
	
	/**flow cell号*/
	private String flowCell;
	/**产量*/
	private String outPut;
	/**cluster*/
	private String cluster;
	/**PF*/
	private String pf;
	/**原因分类*/
	private String reasonType;
	/**详细情况*/
	private String detailed;
	
	/**下达人*/
	private User createUser;
	/**下达时间*/
	private String createDate;
//	/**下机质控号*/
//	private DeSequencingTask desequencingTask;
	/**任务单*/
	private String jobOrder;
	/**分析员*/
	private User acceptUser;
	/**分析时间*/
	private Date acceptDate;
	/**工作流id*/
	private String state;
	/**工作流状态*/
	private String stateName;
	
	public String getFlowCell() {
		return flowCell;
	}
	public void setFlowCell(String flowCell) {
		this.flowCell = flowCell;
	}
	public String getOutPut() {
		return outPut;
	}
	public void setOutPut(String outPut) {
		this.outPut = outPut;
	}
	
	@Column(name ="CLUSTER_STR", length = 50)
	public String getCluster() {
		return cluster;
	}
	public void setCluster(String cluster) {
		this.cluster = cluster;
	}
	public String getPf() {
		return pf;
	}
	public void setPf(String pf) {
		this.pf = pf;
	}
	public String getReasonType() {
		return reasonType;
	}
	public void setReasonType(String reasonType) {
		this.reasonType = reasonType;
	}
	public String getDetailed() {
		return detailed;
	}
	public void setDetailed(String detailed) {
		this.detailed = detailed;
	}

	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得User
	 *@return: User  下达人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser(){
		return this.createUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  下达人
	 */
	public void setCreateUser(User createUser){
		this.createUser = createUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  下达时间
	 */
	@Column(name ="CREATE_DATE", length = 50)
	public String getCreateDate(){
		return this.createDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  下达时间
	 */
	public void setCreateDate(String createDate){
		this.createDate = createDate;
	}
	/**
	 *方法: 取得DeSequencingTask
	 *@return: DeSequencingTask  下机质控号
	 */
//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "DESEQUENCING_TASK")
//	public DeSequencingTask getDesequencingTask(){
//		return this.desequencingTask;
//	}
//	/**
//	 *方法: 设置DeSequencingTask
//	 *@param: DeSequencingTask  下机质控号
//	 */
//	public void setDesequencingTask(DeSequencingTask desequencingTask){
//		this.desequencingTask = desequencingTask;
//	}
	/**
	 *方法: 取得String
	 *@return: String  任务单
	 */
	@Column(name ="JOB_ORDER", length = 50)
	public String getJobOrder(){
		return this.jobOrder;
	}
	/**
	 *方法: 设置String
	 *@param: String  任务单
	 */
	public void setJobOrder(String jobOrder){
		this.jobOrder = jobOrder;
	}
	/**
	 *方法: 取得User
	 *@return: User  分析员
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "ACCEPT_USER")
	public User getAcceptUser(){
		return this.acceptUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  分析员
	 */
	public void setAcceptUser(User acceptUser){
		this.acceptUser = acceptUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  分析时间
	 */
	@Column(name ="ACCEPT_DATE", length = 50)
	public Date getAcceptDate(){
		return this.acceptDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  分析时间
	 */
	public void setAcceptDate(Date acceptDate){
		this.acceptDate = acceptDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  工作流id
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  工作流id
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  工作流状态
	 */
	@Column(name ="STATE_NAME", length = 50)
	public String getStateName(){
		return this.stateName;
	}
	/**
	 *方法: 设置String
	 *@param: String  工作流状态
	 */
	public void setStateName(String stateName){
		this.stateName = stateName;
	}
	
	public Date getDownComputerTime() {
		return downComputerTime;
	}
	public void setDownComputerTime(Date downComputerTime) {
		this.downComputerTime = downComputerTime;
	}
}