package com.biolims.analysis.analy.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.file.model.FileInfo;
import com.biolims.interpret.interpretation.model.InterpretationTask;
/**   
 * @Title: Model
 * @Description: 信息分析审核
 * @author lims-platform
 * @date 2015-12-07 09:44:37
 * @version V1.0   
 *
 */
@Entity
@Table(name = "ANALYSIS_MANAGER")
@SuppressWarnings("serial")
public class AnalysisManager extends EntityDao<AnalysisManager> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
	/**样本号*/
	private String sampleCode;
	/**cnv*/
	private String cnv;
	/**result1*/
	private String result1;
	/**pooling编号*/
	private String code;
	/**reads_mb*/
	private String reads_mb;
	/**gc_antent*/
	private String gc_antent;
	/**q30_ratio*/
	private String q30_ratio;
	/**align_ratio*/
	private String align_ratio;
	/**ur_ratio*/
	private String ur_ratio;
	/**result2*/
	private String result2;
	/**解读结果*/
	private String readingResult;
	/**是否上传图片*/
	private String upload;
	/**结果*/
	private String result;
	/**是否出报告*/
	private String isReport;
	/**处理方式*/
	private String method;
	//相关联解读主数据
	private InterpretationTask interpretationTask;
	//上传图片
	private FileInfo upLoadAccessory;
	/**下一步流向*/
	private String nextFlow;
	
	public String getNextFlow() {
		return nextFlow;
	}
	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getReads_mb() {
		return reads_mb;
	}
	public void setReads_mb(String reads_mb) {
		this.reads_mb = reads_mb;
	}
	public String getGc_antent() {
		return gc_antent;
	}
	public void setGc_antent(String gc_antent) {
		this.gc_antent = gc_antent;
	}
	public String getQ30_ratio() {
		return q30_ratio;
	}
	public void setQ30_ratio(String q30_ratio) {
		this.q30_ratio = q30_ratio;
	}
	public String getAlign_ratio() {
		return align_ratio;
	}
	public void setAlign_ratio(String align_ratio) {
		this.align_ratio = align_ratio;
	}
	public String getUr_ratio() {
		return ur_ratio;
	}
	public void setUr_ratio(String ur_ratio) {
		this.ur_ratio = ur_ratio;
	}
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本号
	 */
	@Column(name ="SAMPLE_CODE", length = 60)
	public String getSampleCode(){
		return this.sampleCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本号
	 */
	public void setSampleCode(String sampleCode){
		this.sampleCode = sampleCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  cnv
	 */
	@Column(name ="CNV", length = 50)
	public String getCnv(){
		return this.cnv;
	}
	/**
	 *方法: 设置String
	 *@param: String  cnv
	 */
	public void setCnv(String cnv){
		this.cnv = cnv;
	}
	/**
	 *方法: 取得String
	 *@return: String  result1
	 */
	@Column(name ="RESULT1", length = 50)
	public String getResult1(){
		return this.result1;
	}
	/**
	 *方法: 设置String
	 *@param: String  result1
	 */
	public void setResult1(String result1){
		this.result1 = result1;
	}
	/**
	 *方法: 取得String
	 *@return: String  result2
	 */
	@Column(name ="RESULT2", length = 50)
	public String getResult2(){
		return this.result2;
	}
	/**
	 *方法: 设置String
	 *@param: String  result2
	 */
	public void setResult2(String result2){
		this.result2 = result2;
	}
	/**
	 *方法: 取得String
	 *@return: String  解读结果
	 */
	@Column(name ="READING_RESULT", length = 50)
	public String getReadingResult(){
		return this.readingResult;
	}
	/**
	 *方法: 设置String
	 *@param: String  解读结果
	 */
	public void setReadingResult(String readingResult){
		this.readingResult = readingResult;
	}
	/**
	 *方法: 取得String
	 *@return: String  是否上传图片
	 */
	@Column(name ="UPLOAD", length = 50)
	public String getUpload(){
		return this.upload;
	}
	/**
	 *方法: 设置String
	 *@param: String  是否上传图片
	 */
	public void setUpload(String upload){
		this.upload = upload;
	}
	
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	/**
	 *方法: 取得String
	 *@return: String  处理方式
	 */
	@Column(name ="METHOD", length = 50)
	public String getMethod(){
		return this.method;
	}
	/**
	 *方法: 设置String
	 *@param: String  处理方式
	 */
	public void setMethod(String method){
		this.method = method;
	}
	/**
	 *方法: 取得InterpretationTask
	 *@return: InterpretationTask  关联主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "INTERPRETATION_TASK")
	public InterpretationTask getInterpretationTask() {
		return interpretationTask;
	}
	public void setInterpretationTask(InterpretationTask interpretationTask) {
		this.interpretationTask = interpretationTask;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "UPLOAD_ACCESSORY")
	public FileInfo getUpLoadAccessory() {
		return upLoadAccessory;
	}
	public void setUpLoadAccessory(FileInfo upLoadAccessory) {
		this.upLoadAccessory = upLoadAccessory;
	}
	/**
	 *方法: 取得String
	 *@return: String  是否出报告
	 */
	@Column(name ="IS_REPORT", length = 50)
	public String getIsReport() {
		return isReport;
	}
	
	/**
	 *方法: 设置String
	 *@param: String  是否出报告
	 */
	public void setIsReport(String isReport) {
		this.isReport = isReport;
	}
}