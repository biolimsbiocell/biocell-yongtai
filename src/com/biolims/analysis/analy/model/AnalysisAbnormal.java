package com.biolims.analysis.analy.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 异常样本
 * @author lims-platform
 * @date 2015-11-30 16:04:51
 * @version V1.0
 * 
 */
@Entity
@Table(name = "ANALYSIS_ABNORMAL")
@SuppressWarnings("serial")
public class AnalysisAbnormal extends EntityDao<AnalysisAbnormal> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 描述 */
	private String name;
	/** pooling编号 */
	private String code;
	// //样本编号
	// private String sampleCode;
	/** result1 */
	private String result1;
	/** result2 */
	private String result2;
	// /**cnv*/
	// private String cnv;
	// /**reads_mb*/
	// private String reads_mb;
	// /**gc_antent*/
	// private String gc_antent;
	// /**q30_ratio*/
	// private String q30_ratio;
	// /**align_ratio*/
	// private String align_ratio;
	// /**ur_ratio*/
	// private String ur_ratio;

	/** pooling号 */
	private String poolingCode;
	/** 13号染色体T1值 */
	private String tOne13;
	/** 13号染色体T2值 */
	private String tTwo13;
	/** 13号染色体A值 */
	private String a13;
	/** 13号染色体L值 */
	private String l13;
	/** 13号风险系数 */
	private String risk13;
	/** 13号胎儿浓度 */
	private String fetal13;
	/** 13号染色体T4 */
	private String tFour13;
	/** 13号参考样本个数 */
	private String sampleNum13;
	/** 13号染色体T5 */
	private String tFive13;

	/** 18号染色体T1值 */
	private String tOne18;
	/** 18号染色体T2值 */
	private String tTwo18;
	/** 18号染色体A值 */
	private String a18;
	/** 18号染色体L值 */
	private String l18;
	/** 18号风险系数 */
	private String risk18;
	/** 18号胎儿浓度 */
	private String fetal18;
	/** 18号染色体T4 */
	private String tFour18;
	/** 18号参考样本个数 */
	private String sampleNum18;
	/** 18号染色体T5 */
	private String tFive18;

	/** 21号染色体T1值 */
	private String tOne21;
	/** 21号染色体T2值 */
	private String tTwo21;
	/** 21号染色体A值 */
	private String a21;
	/** 21号染色体L值 */
	private String l21;
	/** 21号风险系数 */
	private String risk21;
	/** 21号胎儿浓度 */
	private String fetal21;
	/** 21号染色体T4 */
	private String tFour21;
	/** 21号参考样本个数 */
	private String sampleNum21;
	/** 21号染色体T5 */
	private String tFive21;

	/** X号染色体T1值 */
	private String tOneX;
	/** X号染色体T2值 */
	private String tTwoX;
	/** X号染色体A值 */
	private String aX;
	/** X号染色体L值 */
	private String lX;
	/** X号风险系数 */
	private String riskX;
	/** X号胎儿浓度 */
	private String fetalX;
	/** X号染色体T4 */
	private String tFourX;
	/** X号参考样本个数 */
	private String sampleNumX;
	/** X号染色体T5 */
	private String tFiveX;

	/** Y号染色体T1值 */
	private String tOneY;
	/** Y号染色体T2值 */
	private String tTwoY;
	/** Y号染色体A值 */
	private String aY;
	/** Y号染色体L值 */
	private String lY;
	/** Y号风险系数 */
	private String riskY;
	/** Y号胎儿浓度 */
	private String fetalY;
	/** Y号染色体T4 */
	private String tFourY;
	/** Y号参考样本个数 */
	private String sampleNumY;
	/** Y号染色体T5 */
	private String tFiveY;

	/** 加权胎儿浓度 */
	private String fetalAdd;
	/** 13号染色体 */
	private String r13;
	/** 18号染色体 */
	private String r18;
	/** 21号染色体 */
	private String r21;
	/** X号染色体 */
	private String rx;
	/** Y号染色体 */
	private String ry;

	/** 质控情况 */
	private String seqInfo;
	/** 胎儿浓度异常样本 */
	private String fetalExceptionSample;
	/** 其他染色体异常结果 */
	private String otherRstExceptionResult;
	/** 年龄 */
	private String age;
	/** 孕周 */
	private String gesWeeks;
	/** 胎儿数量 */
	private String babyNum;
	/** 唐筛结果 */
	private String tsResult;
	/** 体重 */
	private String weight;
	/** 21风险值 */
	private String riskValue21;
	/** 18风险值 */
	private String riskValue18;
	/** 补充协议 */
	private String protest;

	/** Raw Read Num */
	private String rawReadNum;
	/** Duplication Rate */
	private String duplicationRate;
	/** Real UR Num */
	private String realURNum;
	/** Chr13 */
	private String chr13;
	/** Chr18 */
	private String chr18;
	/** Chr21 */
	private String chr21;

	/** 结果建议 */
	private String suggestResult;
	/** 常染色体判断结果 */
	private String crstResult;
	/** 性染色体判断结果 */
	private String xrstResult;
	/** result1 */
	private String resultOne;
	/** result2 */
	private String resultTwo;
	/** cnv */
	private String cnv;
	/** reads_mb */
	private String readsMb;
	/** gc_content */
	private String gcContent;
	/** q30_ratio */
	private String q30Ratio;
	/** align_ratio */
	private String alignRate;
	/** ur_ratio */
	private String urRatio;
	// /**结果*/
	// private String result;
	// /**处理方式*/
	// private String method;
	/** 样本编号 */
	private String sampleCode;
	/** 样本类型 */
	private String sampleType;
	/** 性别 */
	private String gender;
	/** 样本接收日期 */
	private String sampleRcvTime;
	/** 报告截止日期 */
	private String reportExpDate;
	/** 上机时间 */
	private String computerTime;
	/** 预下机时间 */
	private String machineTime;
	/** 收到CNV日期 */
	private String rcvCNVTime;
	/** 解读完成日期 */
	private String interpretationTime;
	/** 收到图片日期 */
	private String rcvPotoTime;
	/** 报告发客服日期 */
	private String reportToCustomServiceTime;
	/** 异常样本 */
	private String ExceptionSample;
	/** 报告状态 */
	private String reportState;
	/** 报告发出日期 */
	private String reportSendTime;
	/** 临床信息(补充) */
	private String clinicalInfo;
	// /**是否提交*/
	// private String submit;
	//
	// /**备注*/
	// private String note;
	/** 是否出报告 */
	private String isReport;
	/** 状态ID */
	private String state;
	/** 状态名称 */
	private String stateName;

	@Column(length = 50)
	public String getResult1() {
		return result1;
	}

	public void setResult1(String result1) {
		this.result1 = result1;
	}
	@Column(length = 50)
	public String getResult2() {
		return result2;
	}

	public void setResult2(String result2) {
		this.result2 = result2;
	}
	@Column(length = 50)
	public String getCnv() {
		return cnv;
	}

	public void setCnv(String cnv) {
		this.cnv = cnv;
	}

	// public String getReads_mb() {
	// return reads_mb;
	// }
	// public void setReads_mb(String reads_mb) {
	// this.reads_mb = reads_mb;
	// }
	// public String getGc_antent() {
	// return gc_antent;
	// }
	// public void setGc_antent(String gc_antent) {
	// this.gc_antent = gc_antent;
	// }
	// public String getQ30_ratio() {
	// return q30_ratio;
	// }
	// public void setQ30_ratio(String q30_ratio) {
	// this.q30_ratio = q30_ratio;
	// }
	// public String getAlign_ratio() {
	// return align_ratio;
	// }
	// public void setAlign_ratio(String align_ratio) {
	// this.align_ratio = align_ratio;
	// }
	// public String getUr_ratio() {
	// return ur_ratio;
	// }
	// public void setUr_ratio(String ur_ratio) {
	// this.ur_ratio = ur_ratio;
	// }
	/** 结果 */
	private String result;
	/** 下一步流向 */
	private String nextFlow;
	/** 处理意见 */
	private String reason;
	@Column(length = 50)
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	/** 是否提交 */
	private String submit;
	@Column(length = 50)
	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

	/** 备注 */
	private String note;
	/** 处理结果 */
	private String method;
	@Column(length = 50)
	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 50)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String pooling编号
	 */
	@Column(name = "CODE", length = 50)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String pooling编号
	 */
	public void setCode(String code) {
		this.code = code;
	}
	@Column(length = 50)
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	// 下一步流向
	@Column(length = 50)
	public String getNextFlow() {
		return nextFlow;
	}

	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 300)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}
	@Column(length = 50)
	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}
	@Column(length = 50)
	public String getPoolingCode() {
		return poolingCode;
	}

	public void setPoolingCode(String poolingCode) {
		this.poolingCode = poolingCode;
	}
	@Column(length = 50)
	public String gettOne13() {
		return tOne13;
	}

	public void settOne13(String tOne13) {
		this.tOne13 = tOne13;
	}
	@Column(length = 50)
	public String gettTwo13() {
		return tTwo13;
	}

	public void settTwo13(String tTwo13) {
		this.tTwo13 = tTwo13;
	}
	@Column(length = 50)
	public String getA13() {
		return a13;
	}

	public void setA13(String a13) {
		this.a13 = a13;
	}
	@Column(length = 50)
	public String getL13() {
		return l13;
	}

	public void setL13(String l13) {
		this.l13 = l13;
	}
	@Column(length = 50)
	public String getRisk13() {
		return risk13;
	}

	public void setRisk13(String risk13) {
		this.risk13 = risk13;
	}
	@Column(length = 50)
	public String getFetal13() {
		return fetal13;
	}

	public void setFetal13(String fetal13) {
		this.fetal13 = fetal13;
	}
	@Column(length = 50)
	public String gettFour13() {
		return tFour13;
	}

	public void settFour13(String tFour13) {
		this.tFour13 = tFour13;
	}
	@Column(length = 50)
	public String getSampleNum13() {
		return sampleNum13;
	}

	public void setSampleNum13(String sampleNum13) {
		this.sampleNum13 = sampleNum13;
	}
	@Column(length = 50)
	public String gettFive13() {
		return tFive13;
	}

	public void settFive13(String tFive13) {
		this.tFive13 = tFive13;
	}
	@Column(length = 50)
	public String gettOne18() {
		return tOne18;
	}

	public void settOne18(String tOne18) {
		this.tOne18 = tOne18;
	}
	@Column(length = 50)
	public String gettTwo18() {
		return tTwo18;
	}

	public void settTwo18(String tTwo18) {
		this.tTwo18 = tTwo18;
	}
	@Column(length = 50)
	public String getA18() {
		return a18;
	}

	public void setA18(String a18) {
		this.a18 = a18;
	}
	@Column(length = 50)
	public String getL18() {
		return l18;
	}

	public void setL18(String l18) {
		this.l18 = l18;
	}
	@Column(length = 50)
	public String getRisk18() {
		return risk18;
	}

	public void setRisk18(String risk18) {
		this.risk18 = risk18;
	}
	@Column(length = 50)
	public String getFetal18() {
		return fetal18;
	}

	public void setFetal18(String fetal18) {
		this.fetal18 = fetal18;
	}
	@Column(length = 50)
	public String gettFour18() {
		return tFour18;
	}

	public void settFour18(String tFour18) {
		this.tFour18 = tFour18;
	}
	@Column(length = 50)
	public String getSampleNum18() {
		return sampleNum18;
	}

	public void setSampleNum18(String sampleNum18) {
		this.sampleNum18 = sampleNum18;
	}
	@Column(length = 50)
	public String gettFive18() {
		return tFive18;
	}

	public void settFive18(String tFive18) {
		this.tFive18 = tFive18;
	}
	@Column(length = 50)
	public String gettOne21() {
		return tOne21;
	}

	public void settOne21(String tOne21) {
		this.tOne21 = tOne21;
	}
	@Column(length = 50)
	public String gettTwo21() {
		return tTwo21;
	}

	public void settTwo21(String tTwo21) {
		this.tTwo21 = tTwo21;
	}
	@Column(length = 50)
	public String getA21() {
		return a21;
	}

	public void setA21(String a21) {
		this.a21 = a21;
	}
	@Column(length = 50)
	public String getL21() {
		return l21;
	}

	public void setL21(String l21) {
		this.l21 = l21;
	}
	@Column(length = 50)
	public String getRisk21() {
		return risk21;
	}

	public void setRisk21(String risk21) {
		this.risk21 = risk21;
	}
	@Column(length = 50)
	public String getFetal21() {
		return fetal21;
	}

	public void setFetal21(String fetal21) {
		this.fetal21 = fetal21;
	}
	@Column(length = 50)
	public String gettFour21() {
		return tFour21;
	}

	public void settFour21(String tFour21) {
		this.tFour21 = tFour21;
	}
	@Column(length = 50)
	public String getSampleNum21() {
		return sampleNum21;
	}

	public void setSampleNum21(String sampleNum21) {
		this.sampleNum21 = sampleNum21;
	}
	@Column(length = 50)
	public String gettFive21() {
		return tFive21;
	}

	public void settFive21(String tFive21) {
		this.tFive21 = tFive21;
	}
	@Column(length = 50)
	public String gettOneX() {
		return tOneX;
	}

	public void settOneX(String tOneX) {
		this.tOneX = tOneX;
	}
	@Column(length = 50)
	public String gettTwoX() {
		return tTwoX;
	}

	public void settTwoX(String tTwoX) {
		this.tTwoX = tTwoX;
	}
	@Column(length = 50)
	public String getaX() {
		return aX;
	}

	public void setaX(String aX) {
		this.aX = aX;
	}
	@Column(length = 50)
	public String getlX() {
		return lX;
	}

	public void setlX(String lX) {
		this.lX = lX;
	}
	@Column(length = 50)
	public String getRiskX() {
		return riskX;
	}

	public void setRiskX(String riskX) {
		this.riskX = riskX;
	}
	@Column(length = 50)
	public String getFetalX() {
		return fetalX;
	}

	public void setFetalX(String fetalX) {
		this.fetalX = fetalX;
	}
	@Column(length = 50)
	public String gettFourX() {
		return tFourX;
	}

	public void settFourX(String tFourX) {
		this.tFourX = tFourX;
	}
	@Column(length = 50)
	public String getSampleNumX() {
		return sampleNumX;
	}

	public void setSampleNumX(String sampleNumX) {
		this.sampleNumX = sampleNumX;
	}
	@Column(length = 50)
	public String gettFiveX() {
		return tFiveX;
	}

	public void settFiveX(String tFiveX) {
		this.tFiveX = tFiveX;
	}
	@Column(length = 50)
	public String gettOneY() {
		return tOneY;
	}

	public void settOneY(String tOneY) {
		this.tOneY = tOneY;
	}
	@Column(length = 50)
	public String gettTwoY() {
		return tTwoY;
	}

	public void settTwoY(String tTwoY) {
		this.tTwoY = tTwoY;
	}
	@Column(length = 50)
	public String getaY() {
		return aY;
	}

	public void setaY(String aY) {
		this.aY = aY;
	}
	@Column(length = 50)
	public String getlY() {
		return lY;
	}

	public void setlY(String lY) {
		this.lY = lY;
	}
	@Column(length = 50)
	public String getRiskY() {
		return riskY;
	}

	public void setRiskY(String riskY) {
		this.riskY = riskY;
	}
	@Column(length = 50)
	public String getFetalY() {
		return fetalY;
	}

	public void setFetalY(String fetalY) {
		this.fetalY = fetalY;
	}
	@Column(length = 50)
	public String gettFourY() {
		return tFourY;
	}

	public void settFourY(String tFourY) {
		this.tFourY = tFourY;
	}
	@Column(length = 50)
	public String getSampleNumY() {
		return sampleNumY;
	}

	public void setSampleNumY(String sampleNumY) {
		this.sampleNumY = sampleNumY;
	}
	@Column(length = 50)
	public String gettFiveY() {
		return tFiveY;
	}

	public void settFiveY(String tFiveY) {
		this.tFiveY = tFiveY;
	}
	@Column(length = 50)
	public String getFetalAdd() {
		return fetalAdd;
	}

	public void setFetalAdd(String fetalAdd) {
		this.fetalAdd = fetalAdd;
	}
	@Column(length = 50)
	public String getR13() {
		return r13;
	}

	public void setR13(String r13) {
		this.r13 = r13;
	}
	@Column(length = 50)
	public String getR18() {
		return r18;
	}

	public void setR18(String r18) {
		this.r18 = r18;
	}
	@Column(length = 50)
	public String getR21() {
		return r21;
	}

	public void setR21(String r21) {
		this.r21 = r21;
	}
	@Column(length = 50)
	public String getRx() {
		return rx;
	}

	public void setRx(String rx) {
		this.rx = rx;
	}
	@Column(length = 50)
	public String getRy() {
		return ry;
	}

	public void setRy(String ry) {
		this.ry = ry;
	}
	@Column(length = 50)
	public String getSeqInfo() {
		return seqInfo;
	}

	public void setSeqInfo(String seqInfo) {
		this.seqInfo = seqInfo;
	}
	@Column(length = 50)
	public String getFetalExceptionSample() {
		return fetalExceptionSample;
	}

	public void setFetalExceptionSample(String fetalExceptionSample) {
		this.fetalExceptionSample = fetalExceptionSample;
	}
	@Column(length = 50)
	public String getOtherRstExceptionResult() {
		return otherRstExceptionResult;
	}

	public void setOtherRstExceptionResult(String otherRstExceptionResult) {
		this.otherRstExceptionResult = otherRstExceptionResult;
	}
	@Column(length = 50)
	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}
	@Column(length = 50)
	public String getGesWeeks() {
		return gesWeeks;
	}

	public void setGesWeeks(String gesWeeks) {
		this.gesWeeks = gesWeeks;
	}
	@Column(length = 50)
	public String getBabyNum() {
		return babyNum;
	}

	public void setBabyNum(String babyNum) {
		this.babyNum = babyNum;
	}
	@Column(length = 50)
	public String getTsResult() {
		return tsResult;
	}

	public void setTsResult(String tsResult) {
		this.tsResult = tsResult;
	}
	@Column(length = 50)
	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}
	@Column(length = 50)
	public String getRiskValue21() {
		return riskValue21;
	}

	public void setRiskValue21(String riskValue21) {
		this.riskValue21 = riskValue21;
	}
	@Column(length = 50)
	public String getRiskValue18() {
		return riskValue18;
	}

	public void setRiskValue18(String riskValue18) {
		this.riskValue18 = riskValue18;
	}
	@Column(length = 50)
	public String getProtest() {
		return protest;
	}

	public void setProtest(String protest) {
		this.protest = protest;
	}
	@Column(length = 50)
	public String getRawReadNum() {
		return rawReadNum;
	}

	public void setRawReadNum(String rawReadNum) {
		this.rawReadNum = rawReadNum;
	}
	@Column(length = 50)
	public String getDuplicationRate() {
		return duplicationRate;
	}

	public void setDuplicationRate(String duplicationRate) {
		this.duplicationRate = duplicationRate;
	}
	@Column(length = 50)
	public String getRealURNum() {
		return realURNum;
	}

	public void setRealURNum(String realURNum) {
		this.realURNum = realURNum;
	}
	@Column(length = 50)
	public String getChr13() {
		return chr13;
	}

	public void setChr13(String chr13) {
		this.chr13 = chr13;
	}
	@Column(length = 50)
	public String getChr18() {
		return chr18;
	}

	public void setChr18(String chr18) {
		this.chr18 = chr18;
	}
	@Column(length = 50)
	public String getChr21() {
		return chr21;
	}

	public void setChr21(String chr21) {
		this.chr21 = chr21;
	}
	@Column(length = 50)
	public String getSuggestResult() {
		return suggestResult;
	}

	public void setSuggestResult(String suggestResult) {
		this.suggestResult = suggestResult;
	}
	@Column(length = 50)
	public String getCrstResult() {
		return crstResult;
	}

	public void setCrstResult(String crstResult) {
		this.crstResult = crstResult;
	}
	@Column(length = 50)
	public String getXrstResult() {
		return xrstResult;
	}

	public void setXrstResult(String xrstResult) {
		this.xrstResult = xrstResult;
	}
	@Column(length = 50)
	public String getResultOne() {
		return resultOne;
	}

	public void setResultOne(String resultOne) {
		this.resultOne = resultOne;
	}
	@Column(length = 50)
	public String getResultTwo() {
		return resultTwo;
	}

	public void setResultTwo(String resultTwo) {
		this.resultTwo = resultTwo;
	}
	@Column(length = 50)
	public String getReadsMb() {
		return readsMb;
	}

	public void setReadsMb(String readsMb) {
		this.readsMb = readsMb;
	}
	@Column(length = 50)
	public String getGcContent() {
		return gcContent;
	}

	public void setGcContent(String gcContent) {
		this.gcContent = gcContent;
	}
	@Column(length = 50)
	public String getQ30Ratio() {
		return q30Ratio;
	}

	public void setQ30Ratio(String q30Ratio) {
		this.q30Ratio = q30Ratio;
	}
	@Column(length = 50)
	public String getAlignRate() {
		return alignRate;
	}

	public void setAlignRate(String alignRate) {
		this.alignRate = alignRate;
	}
	@Column(length = 50)
	public String getUrRatio() {
		return urRatio;
	}

	public void setUrRatio(String urRatio) {
		this.urRatio = urRatio;
	}
	@Column(length = 50)
	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}
	@Column(length = 50)
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	@Column(length = 50)
	public String getSampleRcvTime() {
		return sampleRcvTime;
	}

	public void setSampleRcvTime(String sampleRcvTime) {
		this.sampleRcvTime = sampleRcvTime;
	}
	@Column(length = 50)
	public String getReportExpDate() {
		return reportExpDate;
	}

	public void setReportExpDate(String reportExpDate) {
		this.reportExpDate = reportExpDate;
	}
	@Column(length = 50)
	public String getComputerTime() {
		return computerTime;
	}

	public void setComputerTime(String computerTime) {
		this.computerTime = computerTime;
	}
	@Column(length = 50)
	public String getMachineTime() {
		return machineTime;
	}

	public void setMachineTime(String machineTime) {
		this.machineTime = machineTime;
	}
	@Column(length = 50)
	public String getRcvCNVTime() {
		return rcvCNVTime;
	}

	public void setRcvCNVTime(String rcvCNVTime) {
		this.rcvCNVTime = rcvCNVTime;
	}
	@Column(length = 50)
	public String getInterpretationTime() {
		return interpretationTime;
	}

	public void setInterpretationTime(String interpretationTime) {
		this.interpretationTime = interpretationTime;
	}
	@Column(length = 50)
	public String getRcvPotoTime() {
		return rcvPotoTime;
	}

	public void setRcvPotoTime(String rcvPotoTime) {
		this.rcvPotoTime = rcvPotoTime;
	}
	@Column(length = 50)
	public String getReportToCustomServiceTime() {
		return reportToCustomServiceTime;
	}

	public void setReportToCustomServiceTime(String reportToCustomServiceTime) {
		this.reportToCustomServiceTime = reportToCustomServiceTime;
	}
	@Column(length = 50)
	public String getExceptionSample() {
		return ExceptionSample;
	}

	public void setExceptionSample(String exceptionSample) {
		ExceptionSample = exceptionSample;
	}
	@Column(length = 50)
	public String getReportState() {
		return reportState;
	}

	public void setReportState(String reportState) {
		this.reportState = reportState;
	}
	@Column(length = 50)
	public String getReportSendTime() {
		return reportSendTime;
	}

	public void setReportSendTime(String reportSendTime) {
		this.reportSendTime = reportSendTime;
	}
	@Column(length = 50)
	public String getClinicalInfo() {
		return clinicalInfo;
	}

	public void setClinicalInfo(String clinicalInfo) {
		this.clinicalInfo = clinicalInfo;
	}
	@Column(length = 50)
	public String getIsReport() {
		return isReport;
	}

	public void setIsReport(String isReport) {
		this.isReport = isReport;
	}
	@Column(length = 50)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	@Column(length = 50)
	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
}