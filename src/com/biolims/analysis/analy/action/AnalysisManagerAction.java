﻿package com.biolims.analysis.analy.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.analysis.analy.model.AnalysisAbnormal;
import com.biolims.analysis.analy.model.AnalysisManager;
import com.biolims.analysis.analy.service.AnalysisManagerService;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/analysis/analy/analysisManager")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class AnalysisManagerAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "260102";
	@Autowired
	private AnalysisManagerService analysisManagerService;
	private AnalysisManager analysisManager = new AnalysisManager();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showAnalysisManagerList")
	public String showAnalysisManagerList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/analysis/analy/analysisManager.jsp");
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public AnalysisManagerService getAnalysisManagerService() {
		return analysisManagerService;
	}

	public void setAnalysisManagerService(
			AnalysisManagerService analysisManagerService) {
		this.analysisManagerService = analysisManagerService;
	}

	public AnalysisManager getAnalysisManager() {
		return analysisManager;
	}

	public void setAnalysisManager(AnalysisManager analysisManager) {
		this.analysisManager = analysisManager;
	}

	@Action(value = "showAnalysisManagerListJson")
	public void showAnalysisManagerListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = analysisManagerService.findAnalysisManagerList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<AnalysisAbnormal> list = (List<AnalysisAbnormal>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("poolingCode", "");
		map.put("sampleCode", "");
		map.put("id", "");
		map.put("resultOne", "");
		map.put("resultTwo", "");
		map.put("cnv", "");
		//===13
		map.put("tOne13", "");
		map.put("tTwo13", "");
		map.put("a13", "");
		map.put("l13", "");
		map.put("risk13", "");
		map.put("fetal13", "");
		map.put("tFour13", "");
		map.put("tFive13", "");
		map.put("sampleNum13", "");
		//====
		//===18
		map.put("tOne18", "");
		map.put("tTwo18", "");
		map.put("a18", "");
		map.put("l18", "");
		map.put("risk18", "");
		map.put("fetal18", "");
		map.put("tFour18", "");
		map.put("tFive18", "");
		map.put("sampleNum18", "");
		//====
		//===21
		map.put("tOne21", "");
		map.put("tTwo21", "");
		map.put("a21", "");
		map.put("l21", "");
		map.put("risk21", "");
		map.put("fetal21", "");
		map.put("tFour21", "");
		map.put("tFive21", "");
		map.put("sampleNum21", "");
		//====
		//===X
		map.put("tOneX", "");
		map.put("tTwoX", "");
		map.put("lX", "");
		map.put("riskX", "");
		map.put("fetalX", "");
		map.put("tFourX", "");
		map.put("tFiveX", "");
		map.put("sampleNumX", "");
		//====
		//===Y
		map.put("tOneY", "");
		map.put("tTwoY", "");
		map.put("lY", "");
		map.put("riskY", "");
		map.put("fetalY", "");
		map.put("tFourY", "");
		map.put("tFiveY", "");
		map.put("sampleNumY", "");
		//====
		map.put("fetalAdd", "");
		map.put("r13", "");
		map.put("r18", "");
		map.put("r21", "");
		map.put("rx", "");
		map.put("ry", "");
		map.put("fetalExceptionSample", "");
		map.put("otherRstExceptionResult", "");
		map.put("age", "");
		map.put("gesWeeks", "");
		map.put("babyNum", "");
		map.put("tsResult", "");
		map.put("weight", "");
		map.put("riskValue21", "");
		map.put("riskValue18", "");
		map.put("protest", "");
		map.put("rawReadNum", "");
		map.put("alignRate", "");
		map.put("duplicationRate", "");
		map.put("realURNum", "");
		map.put("seqInfo", "");
		map.put("chr13", "");
		map.put("chr18", "");
		map.put("chr21", "");
		map.put("suggestResult", "");
		map.put("crstResult", "");
		map.put("xrstResult", "");
		map.put("readsMb", "");
		map.put("gcContent", "");
		map.put("q30Ratio", "");
		map.put("urRatio", "");
		map.put("result", "");
		map.put("method", "");
		map.put("note", "");
		map.put("submit", "");
//		map.put("analysisTask-name", "");
//		map.put("analysisTask-id", "");
		map.put("isReport", "");
		map.put("state","");
		map.put("stateName","");
//		map.put("id", "");
//		map.put("cnv", "");
//		map.put("readingResult", "");
//		map.put("name", "");
//		map.put("upload", "");
//		map.put("method", "");
//		map.put("result", "");
//		map.put("sampleCode", "");
//		map.put("result1", "");
//		map.put("result2", "");
//		map.put("interpretationTask-id", "");
//		map.put("interpretationTask-name", "");
//		map.put("upLoadAccessory-fileName", "");
//		map.put("upLoadAccessory-id", "");
//		map.put("isReport", "");
//		map.put("code","");
//		map.put("reads_mb","");
//		map.put("gc_antent","");
//		map.put("q30_ratio","");
//		map.put("align_ratio","");
//		map.put("ur_ratio","");
//		map.put("nextFlow", "");
		
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "saveAnalysisManager")
	public void saveAnalysisManager() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getParameterFromRequest("itemDataJson");
			analysisManagerService.saveAnalysisManager(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
//	@Action(value = "setAnalysisInfoList")
//	public void setAnalysisInfoList() throws Exception {
//		String code = getParameterFromRequest("code");
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			List<Map<String, String>> dataListMap = this.analysisTaskService.showAnalysisInfoList(code);
//			result.put("success", true);
//			result.put("data", dataListMap);
//		} catch (Exception e) {
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}
}
