﻿
package com.biolims.analysis.analy.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.analysis.analy.model.AnalysisAbnormal;
import com.biolims.analysis.analy.service.AnalysisTaskAbnormalService;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.project.feedback.model.FeedbackAnalysis;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/analysis/analy/analysisTaskAbnormal")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class AnalysisTaskAbnormalAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "260103";
	@Autowired
	private AnalysisTaskAbnormalService analysisTaskAbnormalService;
	private AnalysisAbnormal analysisTaskAbnormal = new AnalysisAbnormal();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showAnalysisTaskAbnormalList")
	public String showAnalysisTaskAbnormalList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/analysis/analy/analysisTaskAbnormal.jsp");
	}

	@Action(value = "showAnalysisTaskAbnormalListJson")
	public void showAnalysisTaskAbnormalListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
//		else 
//			map2Query.put("state", "2");
		Map<String, Object> result = analysisTaskAbnormalService.findAnalysisTaskAbnormalList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<FeedbackAnalysis> list = (List<FeedbackAnalysis>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("poolingCode", "");
		map.put("sampleCode", "");
		map.put("id", "");
		map.put("resultOne", "");
		map.put("resultTwo", "");
		map.put("cnv", "");
		//===13
		map.put("tOne13", "");
		map.put("tTwo13", "");
		map.put("a13", "");
		map.put("l13", "");
		map.put("risk13", "");
		map.put("fetal13", "");
		map.put("tFour13", "");
		map.put("tFive13", "");
		map.put("sampleNum13", "");
		//===18
		map.put("tOne18", "");
		map.put("tTwo18", "");
		map.put("a18", "");
		map.put("l18", "");
		map.put("risk18", "");
		map.put("fetal18", "");
		map.put("tFour18", "");
		map.put("tFive18", "");
		map.put("sampleNum18", "");
		//===21
		map.put("tOne21", "");
		map.put("tTwo21", "");
		map.put("a21", "");
		map.put("l21", "");
		map.put("risk21", "");
		map.put("fetal21", "");
		map.put("tFour21", "");
		map.put("tFive21", "");
		map.put("sampleNum21", "");
		//===X
		map.put("tOneX", "");
		map.put("tTwoX", "");
		map.put("lX", "");
		map.put("riskX", "");
		map.put("fetalX", "");
		map.put("tFourX", "");
		map.put("tFiveX", "");
		map.put("sampleNumX", "");
		//===Y
		map.put("tOneY", "");
		map.put("tTwoY", "");
		map.put("lY", "");
		map.put("riskY", "");
		map.put("fetalY", "");
		map.put("tFourY", "");
		map.put("tFiveY", "");
		map.put("sampleNumY", "");
		//====
		map.put("fetalAdd", "");
		map.put("r13", "");
		map.put("r18", "");
		map.put("r21", "");
		map.put("rx", "");
		map.put("ry", "");
		map.put("fetalExceptionSample", "");
		map.put("otherRstExceptionResult", "");
		map.put("age", "");
		map.put("gesWeeks", "");
		map.put("babyNum", "");
		map.put("tsResult", "");
		map.put("weight", "");
		map.put("riskValue21", "");
		map.put("riskValue18", "");
		map.put("protest", "");
		map.put("rawReadNum", "");
		map.put("alignRate", "");
		map.put("duplicationRate", "");
		map.put("realURNum", "");
		map.put("seqInfo", "");
		map.put("chr13", "");
		map.put("chr18", "");
		map.put("chr21", "");
		map.put("suggestResult", "");
		map.put("crstResult", "");
		map.put("xrstResult", "");
		map.put("readsMb", "");
		map.put("gcContent", "");
		map.put("q30Ratio", "");
		map.put("urRatio", "");
		map.put("result", "");
		map.put("method", "");
		map.put("note", "");
		map.put("isExecute", "");
		map.put("nextflow", "");
//		map.put("analysisTask-name", "");
//		map.put("analysisTask-id", "");
		map.put("isReport", "");
		map.put("state","");
		map.put("stateName","");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "analysisTaskAbnormalSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogAnalysisTaskAbnormalList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/analysis/analy/analysisTaskAbnormalDialog.jsp");
	}

	@Action(value = "showDialogAnalysisTaskAbnormalListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogAnalysisTaskAbnormalListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = analysisTaskAbnormalService.findAnalysisTaskAbnormalList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<AnalysisAbnormal> list = (List<AnalysisAbnormal>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("code", "");
		map.put("result1", "");
		map.put("result2", "");
		map.put("cnv", "");
		map.put("reads_mb", "");
		map.put("gc_antent", "");
		map.put("q30_ratio", "");
		map.put("align_ratio", "");
		map.put("ur_ratio", "");
		map.put("result", "");
		map.put("nextFlow", "");
		map.put("reason", "");
		map.put("submit", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}


	@Action(value = "editAnalysisTaskAbnormal")
	public String editAnalysisTaskAbnormal() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			analysisTaskAbnormal = analysisTaskAbnormalService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "analysisTaskAbnormal");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//			analysisTaskAbnormal.setCreateUser(user);
//			analysisTaskAbnormal.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/analysis/analy/analysisTaskAbnormalEdit.jsp");
	}

	@Action(value = "copyAnalysisTaskAbnormal")
	public String copyAnalysisTaskAbnormal() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		analysisTaskAbnormal = analysisTaskAbnormalService.get(id);
		analysisTaskAbnormal.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/analysis/analy/analysisTaskAbnormalEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = analysisTaskAbnormal.getId();
		if(id!=null&&id.equals("")){
			analysisTaskAbnormal.setId(null);
		}
		Map aMap = new HashMap();
		analysisTaskAbnormalService.save(analysisTaskAbnormal,aMap);
		return redirect("/analysis/analy/analysisTaskAbnormal/editAnalysisTaskAbnormal.action?id=" + analysisTaskAbnormal.getId());

	}

	@Action(value = "viewAnalysisTaskAbnormal")
	public String toViewAnalysisTaskAbnormal() throws Exception {
		String id = getParameterFromRequest("id");
		analysisTaskAbnormal = analysisTaskAbnormalService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/analysis/analy/analysisTaskAbnormalEdit.jsp");
	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public AnalysisTaskAbnormalService getAnalysisTaskAbnormalService() {
		return analysisTaskAbnormalService;
	}

	public void setAnalysisTaskAbnormalService(AnalysisTaskAbnormalService analysisTaskAbnormalService) {
		this.analysisTaskAbnormalService = analysisTaskAbnormalService;
	}

	public AnalysisAbnormal getAnalysisTaskAbnormal() {
		return analysisTaskAbnormal;
	}

	public void setAnalysisTaskAbnormal(AnalysisAbnormal analysisTaskAbnormal) {
		this.analysisTaskAbnormal = analysisTaskAbnormal;
	}
	
	//保存pooling异常
	@Action(value = "saveAnalysisTaskAbnormal")
	public void saveAnalysisTaskAbnormal() throws Exception {
		//String id=getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
//			String itemDataJson = getRequest().getParameter("itemDataJson");
			String itemDataJson = getParameterFromRequest("itemDataJson");
			analysisTaskAbnormalService.saveAnalysisTaskAbnormalList(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
