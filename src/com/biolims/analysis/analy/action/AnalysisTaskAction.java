﻿
package com.biolims.analysis.analy.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.analysis.analy.model.AnalysisTask;
import com.biolims.analysis.analy.model.AnalysisTaskItem;
import com.biolims.analysis.analy.model.SampleAnalysisInfo;
import com.biolims.analysis.analy.service.AnalysisTaskService;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/analysis/analy/analysisTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class AnalysisTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "260101";
	@Autowired
	private AnalysisTaskService analysisTaskService;
	private AnalysisTask analysisTask = new AnalysisTask();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showAnalysisTaskList")
	public String showAnalysisTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/analysis/analy/analysisTask.jsp");
	}
	//================
	@Action(value = "getdeSequencingTaskList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void getdeSequencingTaskList() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.analysisTaskService.getDeSequencingTaskList(code);
			result.put("success", true);
			result.put("data", dataListMap);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	//===============
	
	//========生成结果=======getAnalysisTaskList
	@Action(value = "getAnalysisTaskList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void getAnalysisTaskList() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.analysisTaskService.getAnalysisTaskList(code);
			result.put("success", true);
			result.put("data", dataListMap);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	//=====================
	@Action(value = "showAnalysisTaskListJson")
	public void showAnalysisTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = analysisTaskService.findAnalysisTaskList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<AnalysisTask> list = (List<AnalysisTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("downComputerTime", "yyyy-MM-dd");
		map.put("flowCell", "");
		map.put("outPut", "");
		map.put("cluster", "");
		map.put("pf", "");
		map.put("reasonType", "");
		map.put("detailed", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "");
		map.put("jobOrder", "");
//		map.put("reasonAnalysis", "");
//		map.put("detailed", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("acceptDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	//查询左侧中间表
	/*@Action(value = "showAnalysisInfoTaskList")
		public String showAnalysisInfoTaskList() throws Exception {
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
			return dispatcher("/WEB-INF/page/analysis/analy/analysisInfoTask.jsp");
		}*/
//		@Action(value = "showAnalysisInfoTaskListJson")
//		public void showAnalysisInfoTaskListJson() throws Exception {
//			int startNum = Integer.parseInt(getParameterFromRequest("start"));
//			int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//			String dir = getParameterFromRequest("dir");
//			String sort = getParameterFromRequest("sort");
//			String data = getParameterFromRequest("data");
//			Map<String, String> map2Query = new HashMap<String, String>();
//			if (data != null && data.length() > 0)
//				map2Query = JsonUtils.toObjectByJson(data, Map.class);
//			Map<String, Object> result = analysisTaskService.findAnalysisInfoTaskList(map2Query, startNum, limitNum, dir, sort);
//			Long count = (Long) result.get("total");
//			List<AnalysisInfoTask> list = (List<AnalysisInfoTask>) result.get("list");
//
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("endTime", "");
//			map.put("sampleCode", "");
//			map.put("checkProject", "");
//			map.put("wkCode", "");
//			map.put("state", "");
//			new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
//		}
	@Action(value = "analysisTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogAnalysisTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/analysis/analy/analysisTaskDialog.jsp");
	}

	@Action(value = "showDialogAnalysisTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogAnalysisTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = analysisTaskService.findAnalysisTaskList1(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<AnalysisTask> list = (List<AnalysisTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("jobOrder", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("acceptDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "editAnalysisTask")
	public String editAnalysisTask() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			analysisTask = analysisTaskService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "analysisTask");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			analysisTask.setCreateUser(user);
			Date date=new Date();
			DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String stime=format.format(date);
			analysisTask.setCreateDate(stime);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(analysisTask.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/analysis/analy/analysisTaskEdit.jsp");
	}

	@Action(value = "copyAnalysisTask")
	public String copyAnalysisTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		analysisTask = analysisTaskService.get(id);
		analysisTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/analysis/analy/analysisTaskEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = analysisTask.getId();
		if(id!=null&&id.equals("")){
			analysisTask.setId(null);
		}
//		analysisTaskService.getAnalysisItemSampleCode(id);
		analysisTaskService.getAnalysisInfoSampleCode(id);
		Map aMap = new HashMap();
//			aMap.put("analysisItem",getParameterFromRequest("analysisItemJson"));
		
			aMap.put("analysisInfo",getParameterFromRequest("analysisInfoJson"));
		
		analysisTaskService.save(analysisTask,aMap);
		return redirect("/analysis/analy/analysisTask/editAnalysisTask.action?id=" + analysisTask.getId());

	}

	@Action(value = "viewAnalysisTask")
	public String toViewAnalysisTask() throws Exception {
		String id = getParameterFromRequest("id");
		analysisTask = analysisTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/analysis/analy/analysisTaskEdit.jsp");
	}
	

	@Action(value = "showAnalysisItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showAnalysisItemList() throws Exception {
		return dispatcher("/WEB-INF/page/analysis/analy/analysisItem.jsp");
	}

	@Action(value = "showAnalysisItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showAnalysisItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = analysisTaskService.findAnalysisItemList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<AnalysisTaskItem> list = (List<AnalysisTaskItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("poolingCode", "");
			map.put("wkCode", "");
			map.put("sampleCode", "");
			map.put("status", "");
			map.put("note", "");
			map.put("analyzer-id", "");
			map.put("analyzer-name", "");
			map.put("analysisTask-name", "");
			map.put("analysisTask-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delAnalysisItem")
	public void delAnalysisItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			analysisTaskService.delAnalysisItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	

	@Action(value = "showAnalysisInfoList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showAnalysisInfoList() throws Exception {
		return dispatcher("/WEB-INF/page/analysis/analy/analysisInfo.jsp");
	}

	@Action(value = "showAnalysisInfoListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showAnalysisInfoListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = analysisTaskService.findAnalysisInfoList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<SampleAnalysisInfo> list = (List<SampleAnalysisInfo>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("poolingCode", "");
			map.put("sampleCode", "");
			map.put("id", "");
			map.put("resultOne", "");
			map.put("resultTwo", "");
			map.put("cnv", "");
			//===13
			map.put("tOne13", "");
			map.put("tTwo13", "");
			map.put("a13", "");
			map.put("l13", "");
			map.put("risk13", "");
			map.put("fetal13", "");
			map.put("tFour13", "");
			map.put("tFive13", "");
			map.put("sampleNum13", "");
			//====
			//===18
			map.put("tOne18", "");
			map.put("tTwo18", "");
			map.put("a18", "");
			map.put("l18", "");
			map.put("risk18", "");
			map.put("fetal18", "");
			map.put("tFour18", "");
			map.put("tFive18", "");
			map.put("sampleNum18", "");
			//====
			//===21
			map.put("tOne21", "");
			map.put("tTwo21", "");
			map.put("a21", "");
			map.put("l21", "");
			map.put("risk21", "");
			map.put("fetal21", "");
			map.put("tFour21", "");
			map.put("tFive21", "");
			map.put("sampleNum21", "");
			//====
			//===X
			map.put("tOneX", "");
			map.put("tTwoX", "");
			map.put("lX", "");
			map.put("riskX", "");
			map.put("fetalX", "");
			map.put("tFourX", "");
			map.put("tFiveX", "");
			map.put("sampleNumX", "");
			//====
			//===Y
			map.put("tOneY", "");
			map.put("tTwoY", "");
			map.put("lY", "");
			map.put("riskY", "");
			map.put("fetalY", "");
			map.put("tFourY", "");
			map.put("tFiveY", "");
			map.put("sampleNumY", "");
			//====
			map.put("fetalAdd", "");
			map.put("r13", "");
			map.put("r18", "");
			map.put("r21", "");
			map.put("rx", "");
			map.put("ry", "");
			map.put("fetalExceptionSample", "");
			map.put("otherRstExceptionResult", "");
			map.put("age", "");
			map.put("gesWeeks", "");
			map.put("babyNum", "");
			map.put("tsResult", "");
			map.put("weight", "");
			map.put("riskValue21", "");
			map.put("riskValue18", "");
			map.put("protest", "");
			map.put("rawReadNum", "");
			map.put("alignRate", "");
			map.put("duplicationRate", "");
			map.put("realURNum", "");
			map.put("seqInfo", "");
			map.put("chr13", "");
			map.put("chr18", "");
			map.put("chr21", "");
			map.put("suggestResult", "");
			map.put("crstResult", "");
			map.put("xrstResult", "");
			map.put("readsMb", "");
			map.put("gcContent", "");
			map.put("q30Ratio", "");
			map.put("urRatio", "");
			map.put("result", "");
			map.put("method", "");
			map.put("note", "");
			map.put("submit", "");
			map.put("analysisTask-name", "");
			map.put("analysisTask-id", "");
			map.put("isReport", "");
			map.put("state","");
			map.put("stateName","");
			
			map.put("contractId", "");
			map.put("projectId", "");
			map.put("orderType", "");
			map.put("techTaskId", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delAnalysisInfo")
	public void delAnalysisInfo() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			analysisTaskService.delAnalysisInfo(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public AnalysisTaskService getAnalysisTaskService() {
		return analysisTaskService;
	}

	public void setAnalysisTaskService(AnalysisTaskService analysisTaskService) {
		this.analysisTaskService = analysisTaskService;
	}

	public AnalysisTask getAnalysisTask() {
		return analysisTask;
	}

	public void setAnalysisTask(AnalysisTask analysisTask) {
		this.analysisTask = analysisTask;
	} 
	
	@Action(value = "setAnalysisInfoList")
	public void setAnalysisInfoList() throws Exception {
		String code = getParameterFromRequest("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.analysisTaskService.showAnalysisInfoList(code);
			result.put("success", true);
			result.put("data", dataListMap);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	@Action(value = "setAnalysisItemList")
	public void setAnalysisItemList() throws Exception {
		String code = getParameterFromRequest("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.analysisTaskService.showAnalysisItemList(code);
			result.put("success", true);
			result.put("data", dataListMap);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	/**
	 * 生成信息分析，分析明细，分析结果的CSV
	 * @throws Exception
	 */
	/*@Action(value = "createCsv")
	public void createExcel() throws Exception {
		String taskId = getRequest().getParameter("taskId");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids =this.analysisTaskService.createCSV(taskId);
			map.put("success", true);
			if (ids != null) {
				map.put("fileId", ids);
			} else {
				map.put("fileId", "");
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("error", false);
		}

		HttpUtils.write(JsonUtils.toJsonString(map));
	}*/
	
	@Action(value = "getCsv")
	public void getCsv() throws Exception{
//		String taskId = getRequest().getParameter("taskId");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
//			 deSequencingTaskService.getCsvContent(taskId);
			analysisTaskService.getCsvContent();
			map.put("success", true);
//			if (ids != null) {
//				map.put("fileId", ids);
//			} else {
//				map.put("fileId", "");
//			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}

		HttpUtils.write(JsonUtils.toJsonString(map));
	}
}
