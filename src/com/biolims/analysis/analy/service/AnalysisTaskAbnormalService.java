package com.biolims.analysis.analy.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.analysis.analy.dao.AnalysisTaskAbnormalDao;
import com.biolims.analysis.analy.model.AnalysisAbnormal;
import com.biolims.analysis.analy.model.SampleAnalysisInfo;
import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.plasma.model.PlasmaAbnormal;
import com.biolims.experiment.sequencing.model.SequencingTaskInfo;
import com.biolims.experiment.wk.model.WkTaskAbnormal;
import com.biolims.experiment.wk.model.WkTaskInfo;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.project.feedback.model.FeedbackAnalysis;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class AnalysisTaskAbnormalService {
	@Resource
	private AnalysisTaskAbnormalDao analysisTaskAbnormalDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findAnalysisTaskAbnormalList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return analysisTaskAbnormalDao.selectAnalysisTaskAbnormalList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(AnalysisAbnormal i) throws Exception {

		analysisTaskAbnormalDao.saveOrUpdate(i);

	}
	public AnalysisAbnormal get(String id) {
		AnalysisAbnormal analysisTaskAbnormal = commonDAO.get(AnalysisAbnormal.class, id);
		return analysisTaskAbnormal;
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(AnalysisAbnormal sc, Map jsonMap) throws Exception {
		if (sc != null) {
			analysisTaskAbnormalDao.saveOrUpdate(sc);
		
			String jsonStr = "";
	}
   }
	//保存异常pooling样本
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveAnalysisTaskAbnormalList(String itemDataJson) throws Exception {
		List<FeedbackAnalysis> saveItems = new ArrayList<FeedbackAnalysis>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			FeedbackAnalysis sbi = new FeedbackAnalysis();
			sbi = (FeedbackAnalysis) analysisTaskAbnormalDao.Map2Bean(map, sbi);
			if (sbi.getId() != null && sbi.getId().equals(""))
				sbi.setId(null);
			saveItems.add(sbi);
			if(sbi!=null){
				String[] wk_code = sbi.getSampleCode().split("_");
				//去除文库号
				String wk=wk_code[0];
				//查出文库结果把值给到重抽血 和退费
				WkTaskInfo sampleWkInfo = analysisTaskAbnormalDao.findByWkcode(wk);
				if(sampleWkInfo!=null){
					if(sbi.getAdvice()!=null && !sbi.getAdvice().equals("")){
						if(sbi.getAdvice().equals("0") || sbi.getAdvice().equals("1") ||sbi.getAdvice().equals("2") ||sbi.getAdvice().equals("3")){
							if(!sbi.getIsExecute().equals("") && sbi.getIsExecute().equals("1") && sbi.getAdvice()!=null){
								String sampleCode = sbi.getSampleCode();
								SampleAnalysisInfo sampleAnalysisInfo = analysisTaskAbnormalDao.findBySampleCode(sampleCode);
								sampleAnalysisInfo.setMethod(sbi.getAdvice());
								sampleAnalysisInfo.setState(com.biolims.workflow.WorkflowConstants.RESULT_DETAIL_CHECK);
								sampleAnalysisInfo.setStateName(com.biolims.workflow.WorkflowConstants.RESULT_DETAIL_CHECK_NAME);
							}
						}else if(sbi.getAdvice().equals("4")){//重抽血
							if(sampleWkInfo!=null){
								if(!sbi.getIsExecute().equals("") && sbi.getIsExecute().equals("1")){
									PlasmaAbnormal plasmaAbnormal = new PlasmaAbnormal();				//new 出重抽血的实体
									plasmaAbnormal.setCode(sampleWkInfo.getCode());						//血浆编号
									plasmaAbnormal.setSampleCode(sampleWkInfo.getSampleCode());			//原始样本编号
									plasmaAbnormal.setProductId(sampleWkInfo.getProductId());			//检测项目ID
									plasmaAbnormal.setProductName(sampleWkInfo.getProductName());		//检测项目名称
									plasmaAbnormal.setReportDate(sampleWkInfo.getReportDate());			//应出报告日期
									plasmaAbnormal.setOrderId(sampleWkInfo.getOrderId());				//任务单ID
									plasmaAbnormal.setResult(sampleWkInfo.getResult());					//结果
									plasmaAbnormal.setNextFlow(sampleWkInfo.getNextFlow());				//下一步流向
									plasmaAbnormal.setMethod(sampleWkInfo.getMethod());					//处理意见
									plasmaAbnormal.setState("2");										//state设置成2
									analysisTaskAbnormalDao.saveOrUpdate(plasmaAbnormal);
								}
							}
						}else if(sbi.getAdvice().equals("5")){//退费
							if(!sbi.getIsExecute().equals("") && sbi.getIsExecute().equals("1")){
								PlasmaAbnormal plasmaAbnormal = new PlasmaAbnormal();				//new 出重抽血的实体  退费把数据也给到重抽血
								plasmaAbnormal.setCode(sampleWkInfo.getCode());						//血浆编号
								plasmaAbnormal.setSampleCode(sampleWkInfo.getSampleCode());			//原始样本编号
								plasmaAbnormal.setProductId(sampleWkInfo.getProductId());			//检测项目ID
								plasmaAbnormal.setProductName(sampleWkInfo.getProductName());		//检测项目名称
								plasmaAbnormal.setReportDate(sampleWkInfo.getReportDate());			//应出报告日期
								plasmaAbnormal.setOrderId(sampleWkInfo.getOrderId());				//任务单ID
								plasmaAbnormal.setResult(sampleWkInfo.getResult());					//结果
								plasmaAbnormal.setNextFlow(sampleWkInfo.getNextFlow());				//下一步流向
								plasmaAbnormal.setMethod(sampleWkInfo.getMethod());					//处理意见
								plasmaAbnormal.setState("2");										//state设置成2
								analysisTaskAbnormalDao.saveOrUpdate(plasmaAbnormal);
							}
						}else if(sbi.getAdvice().equals("6")){//重建库
							if(!sbi.getIsExecute().equals("") && sbi.getIsExecute().equals("1")){
								WkTaskAbnormal wkAbnormalBack = new WkTaskAbnormal();					//new 出文库构建的重建库实体
								wkAbnormalBack.setCode(sampleWkInfo.getCode());							//样本编号
								wkAbnormalBack.setSampleCode(sampleWkInfo.getSampleCode());				//原始样本号
								wkAbnormalBack.setProductId(sampleWkInfo.getProductId());				//检测项目ID
								wkAbnormalBack.setProductName(sampleWkInfo.getProductName());			//检测项目名称
								wkAbnormalBack.setReportDate(sampleWkInfo.getReportDate());				//应出报告日期
								wkAbnormalBack.setResult(sampleWkInfo.getResult());						//结果
								wkAbnormalBack.setNextFlow(sampleWkInfo.getNextFlow());					//下一步流向
								wkAbnormalBack.setMethod(sampleWkInfo.getMethod());						//处理意见
								wkAbnormalBack.setNote(sampleWkInfo.getNote());							//备注
								analysisTaskAbnormalDao.saveOrUpdate(wkAbnormalBack);
							}
						}else if(sbi.getAdvice().equals("7")){//重上机
							if(!sbi.getIsExecute().equals("") && sbi.getIsExecute().equals("1")){
								String fcCode = sbi.getFcCode();
								SequencingTaskInfo sampleSequencingInfo = analysisTaskAbnormalDao.findByPooling(fcCode);
								if(sampleSequencingInfo!=null){
//									sampleSequencingInfo.setState("4");
								}
							}
						}
	//					else if(sbi.getAdvice().equals("8")){//待反馈不做操作
	//					}
					}
				}
			}	
			//下一步流向
//			if(sbi!=null && sbi.getSubmit()!=null && sbi.getNextFlow()!=null){
//				if(sbi.getSubmit().equals("1")){
					/*if(sbi.getNextFlow().equals("0")){ //下一步流向：终止
						PoolingTemp pt = new PoolingTemp();
						pt.setSampleCode(sbi.getSampleCode());
						pt.setName(sbi.getName());
						pt.setNote(sbi.getNote());
						pt.setState("1");
						this.analysisTaskAbnormalDao.saveOrUpdate(pt);
					}else*/ 
//					if(sbi.getNextFlow().equals("1")){//下一步流向：继续检测
						/*SequencingTemp st = new SequencingTemp();*/
						/*it.setCode(sbi.getSampleCode());
						st.setName(sbi.getName());
						st.setNote(sbi.getNote());*/
//						InterpretationTemp it=new InterpretationTemp();
//						it.setAlignRatio(sbi.getAlign_ratio());
//						it.setCnv(sbi.getCnv());
//						it.setGcContent(sbi.getGc_antent());
//						it.setQ30Ratio(sbi.getQ30_ratio());
//						it.setReadsMb(sbi.getReads_mb());
//						it.setResultOne(sbi.getResult1());
//						it.setResultTwo(sbi.getResult2());
//						it.setSampleCode(sbi.getSampleCode());
//						it.setUrRatio(sbi.getUr_ratio());
//						it.setMethod(sbi.getMethod());
//						it.setResult(sbi.getResult());
//						String id=sbi.getId();
//						delAnalysisTaskAbnormalSubmitIsOne(id);
//						this.analysisTaskAbnormalDao.saveOrUpdate(it);
//					}else if(sbi.getNextFlow().equals("2")){//下一步流向 ：异常反馈至项目组
//						FeedbackPooling pf = new FeedbackPooling();
//						pf.setId(sbi.getId());
//						pf.setSampleCode(sbi.getSampleCode());
//						pf.setName(sbi.getName());
//						pf.setResult("1");
//						this.analysisTaskAbnormalDao.saveOrUpdate(pf);
//					}
//				}
//			}
		}
		analysisTaskAbnormalDao.saveOrUpdateAll(saveItems);
	}
//	private void delAnalysisTaskAbnormalSubmitIsOne(String result) {
//	AnalysisTaskAbnormal scp =  analysisTaskAbnormalDao.delAnalyInfoResultIsZore(result);
//	analysisTaskAbnormalDao.delete(scp);
//}
}
