package com.biolims.analysis.analy.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.analysis.analy.dao.AnalysisManagerDao;
import com.biolims.analysis.analy.dao.AnalysisTaskDao;
import com.biolims.analysis.analy.model.AnalysisAbnormal;
import com.biolims.analysis.analy.model.AnalysisManager;
import com.biolims.analysis.analy.model.SampleAnalysisInfo;
import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class AnalysisManagerService {
	@Resource
	private AnalysisManagerDao analysisManagerDao;
	@Resource
	private AnalysisTaskDao analysisTaskDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findAnalysisManagerList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return analysisManagerDao.selectAnalysisManagerList(mapForQuery, startNum, limitNum, dir, sort);
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(AnalysisManager i) throws Exception {

		analysisManagerDao.saveOrUpdate(i);

	}
	public AnalysisManager get(String id) {
		AnalysisManager analysisManager = commonDAO.get(AnalysisManager.class, id);
		return analysisManager;
	}
	
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(AnalysisManager sc, Map jsonMap) throws Exception {
		if (sc != null) {
			analysisManagerDao.saveOrUpdate(sc);
		
			String jsonStr = "";
			
		}
   }
	//保存信息分析审核
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveAnalysisManager(String itemDataJson) throws Exception {
		List<AnalysisAbnormal> saveItems = new ArrayList<AnalysisAbnormal>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			AnalysisAbnormal sbi = new AnalysisAbnormal();
			sbi = (AnalysisAbnormal) analysisManagerDao.Map2Bean(map, sbi);
			if (sbi.getId() != null && sbi.getId().equals(""))
				sbi.setId(null);
			if( sbi.getIsReport()!=null && !sbi.equals("1")){
				SampleAnalysisInfo sai = analysisTaskDao.setAnalysisInfoListByCode(sbi.getSampleCode());
				sai.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_REPORT_NEW);
				sai.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_REPORT_NEW_NAME);
			}
			saveItems.add(sbi);
//			if(sbi!=null && sbi.getUpLoadAccessory()!=null){
//					if(sbi.getUpload().equals("1")){
//						InterpretationBack ib=new InterpretationBack();
//						
//						ib.setCnv(sbi.getCnv());
//						ib.setResult1(sbi.getResult1());
//						ib.setResult2(sbi.getResult2());
//						ib.setSampleCode(sbi.getSampleCode());
//						ib.setReadingResult(sbi.getReadingResult());
//						ib.setInterpretationTask(sbi.getInterpretationTask());
//						
//						analysisManagerDao.saveOrUpdate(ib);
//				
//					InterpretationInfo tf=new InterpretationInfo();
//					tf.setCode(sbi.getId());
//					tf.setSampleCode(sbi.getSampleCode());
//					tf.setInterpretationTask(sbi.getInterpretationTask());
//					tf.setCnv(sbi.getCnv());
//					tf.setResult1(sbi.getResult1());
//					tf.setResult2(sbi.getResult2());
//					tf.setReadingResult(sbi.getReadingResult());
//					tf.setUpLoadAccessory(sbi.getUpLoadAccessory());
//					
//					commonDAO.saveOrUpdate(tf);
//					}
//			}
		}
		analysisManagerDao.saveOrUpdateAll(saveItems);
	}	
}
