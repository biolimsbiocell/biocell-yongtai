package com.biolims.analysis.analy.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.analysis.analy.dao.AnalysisTaskDao;
import com.biolims.analysis.analy.model.AnalysisAbnormal;
import com.biolims.analysis.analy.model.AnalysisTask;
import com.biolims.analysis.analy.model.AnalysisTaskItem;
import com.biolims.analysis.analy.model.SampleAnalysisInfo;
import com.biolims.analysis.desequencing.model.SampleDeSequencingInfo;
import com.biolims.common.FtpUtil;
import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.project.feedback.dao.AnalysisFeedbackDao;
import com.biolims.project.feedback.model.FeedbackAnalysis;
import com.biolims.util.JsonUtils;
import com.csvreader.CsvReader;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class AnalysisTaskService {
	@Resource
	private AnalysisTaskDao analysisTaskDao;
	@Resource
	private AnalysisFeedbackDao analysisFeedbackDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findAnalysisTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return analysisTaskDao.selectAnalysisTaskList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	// 状态完成的数据
	public Map<String, Object> findAnalysisTaskList1(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return analysisTaskDao.selectAnalysisTaskList1(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(AnalysisTask i) throws Exception {

		analysisTaskDao.saveOrUpdate(i);

	}

	public AnalysisTask get(String id) {
		AnalysisTask analysisTask = commonDAO.get(AnalysisTask.class, id);
		return analysisTask;
	}

	public Map<String, Object> findAnalysisItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = analysisTaskDao.selectAnalysisItemList(
				scId, startNum, limitNum, dir, sort);
		List<AnalysisTaskItem> list = (List<AnalysisTaskItem>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findAnalysisInfoList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = analysisTaskDao.selectAnalysisInfoList(
				scId, startNum, limitNum, dir, sort);
		List<SampleAnalysisInfo> list = (List<SampleAnalysisInfo>) result
				.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveAnalysisItem(AnalysisTask sc, String itemDataJson)
			throws Exception {
		List<AnalysisTaskItem> saveItems = new ArrayList<AnalysisTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			AnalysisTaskItem scp = new AnalysisTaskItem();
			// 将map信息读入实体类
			scp = (AnalysisTaskItem) analysisTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setAnalysisTask(sc);

			saveItems.add(scp);
		}
		analysisTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delAnalysisItem(String[] ids) throws Exception {
		for (String id : ids) {
			AnalysisTaskItem scp = analysisTaskDao.get(AnalysisTaskItem.class,
					id);
			analysisTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveAnalysisInfo(AnalysisTask sc, String itemDataJson)
			throws Exception {
		List<SampleAnalysisInfo> saveItems = new ArrayList<SampleAnalysisInfo>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SampleAnalysisInfo scp = new SampleAnalysisInfo();
			// 将map信息读入实体类
			scp = (SampleAnalysisInfo) analysisTaskDao.Map2Bean(map, scp);
			 if (scp.getId() != null && scp.getId().equals(""))
			 scp.setId(null);
			scp.setAnalysisTask(sc);

			saveItems.add(scp);
			if(scp != null && scp.getResult()!=null && scp.getMethod()!=null){
				if(scp.getMethod().equals("0") || scp.getMethod().equals("1") ||scp.getMethod().equals("2") ||scp.getMethod().equals("3")){
					//如果选的是前4个就进入信息分析审核
					AnalysisAbnormal aa = new AnalysisAbnormal();
					aa.setA13(scp.getA13());
					aa.setA18(scp.getA18());
					aa.setA21(scp.getA21());
					aa.setAge(scp.getAge());
					aa.setAlignRate(scp.getAlignRate());
					aa.setaX(scp.getaX());
					aa.setaY(scp.getaY());
					aa.setBabyNum(scp.getBabyNum());
					aa.setChr13(scp.getChr13());
					aa.setChr18(scp.getChr18());
					aa.setChr21(scp.getChr21());
					aa.setClinicalInfo(scp.getClinicalInfo());
					aa.setCnv(scp.getCnv());
					aa.setCode(scp.getPoolingCode());
					aa.setComputerTime(scp.getComputerTime());
					aa.setCrstResult(scp.getCrstResult());
					aa.setDuplicationRate(scp.getDuplicationRate());
					aa.setExceptionSample(scp.getExceptionSample());
					aa.setFetal13(scp.getFetal13());
					aa.setFetal18(scp.getFetal18());
					aa.setFetal21(scp.getFetal21());
					aa.setFetalAdd(scp.getFetalAdd());
					aa.setFetalX(scp.getFetalX());
					aa.setFetalY(scp.getFetalY());
					aa.setGcContent(scp.getGcContent());
					aa.setGender(scp.getGender());
					aa.setGesWeeks(scp.getGesWeeks());
					aa.setInterpretationTime(scp.getInterpretationTime());
					aa.setIsReport(scp.getIsReport());
					aa.setL13(scp.getL13());
					aa.setL18(scp.getL18());
					aa.setL21(scp.getL21());
					aa.setlX(scp.getlX());
					aa.setlY(scp.getlY());
					aa.setMachineTime(scp.getMachineTime());
					aa.setMethod(scp.getMethod());
					aa.setNote(scp.getNote());
					aa.setOtherRstExceptionResult(scp.getOtherRstExceptionResult());
					aa.setPoolingCode(scp.getPoolingCode());
					aa.setProtest(scp.getProtest());
					aa.setQ30Ratio(scp.getQ30Ratio());
					aa.setR13(scp.getR13());
					aa.setR18(scp.getR18());
					aa.setR21(scp.getR21());
					aa.setRawReadNum(scp.getRawReadNum());
					aa.setRcvCNVTime(scp.getRcvCNVTime());
					aa.setRcvPotoTime(scp.getRcvPotoTime());
					aa.setReadsMb(scp.getReadsMb());
					aa.setRealURNum(scp.getRealURNum());
					aa.setReportExpDate(scp.getReportExpDate());
					aa.setReportSendTime(scp.getReportSendTime());
					aa.setReportState(scp.getReportState());
					aa.setReportToCustomServiceTime(scp.getReportToCustomServiceTime());
					aa.setResult(scp.getResult());
					aa.setResultOne(scp.getResultOne());
					aa.setResultTwo(scp.getResultTwo());
					aa.setRisk13(scp.getRisk13());
					aa.setRisk18(scp.getRisk18());
					aa.setRisk21(scp.getRisk21());
					aa.setRiskValue18(scp.getRiskValue18());
					aa.setRiskValue21(scp.getRiskValue21());
					aa.setRiskX(scp.getRiskX());
					aa.setRiskY(scp.getRiskY());
					aa.setSampleCode(scp.getSampleCode());
					aa.setSampleNum13(scp.getSampleNum13());
					aa.setSampleNum18(scp.getSampleNum18());
					aa.setSampleNum21(scp.getSampleNum21());
					aa.setSampleNumX(scp.getSampleNumX());
					aa.setSampleNumY(scp.getSampleNumY());
					aa.setSampleRcvTime(scp.getSampleRcvTime());
					aa.setSeqInfo(scp.getSeqInfo());
					aa.setSubmit(scp.getSubmit());
					aa.setSuggestResult(scp.getSuggestResult());
					aa.settFive13(scp.gettFive13());
					aa.settFive18(scp.gettFive18());
					aa.settFive21(scp.gettFive21());
					aa.settFiveX(scp.gettFiveX());
					aa.settFiveY(scp.gettFiveY());
					aa.settFour13(scp.gettFour13());
					aa.settFour18(scp.gettFour18());
					aa.settFour21(scp.gettFour21());
					aa.settFourX(scp.gettFourX());
					aa.settFourY(scp.gettFourY());
					aa.settOne13(scp.gettOne13());
					aa.settOne18(scp.gettOne18());
					aa.settOne21(scp.gettOne21());
					aa.settOneX(scp.gettOneX());
					aa.settOneY(scp.gettOneY());
					aa.settTwo13(scp.gettTwo13());
					aa.settTwo21(scp.gettTwo21());
					aa.settTwo18(scp.gettTwo18());
					aa.settTwoX(scp.gettTwoX());
					aa.settTwoY(scp.gettTwoY());
					aa.setTsResult(scp.getTsResult());
					aa.setUrRatio(scp.getUrRatio());
					aa.setWeight(scp.getWeight());
					aa.setXrstResult(scp.getXrstResult());
					aa.setState(com.biolims.workflow.WorkflowConstants.RESULT_DETAIL_CHECK);
					aa.setStateName(com.biolims.workflow.WorkflowConstants.RESULT_DETAIL_CHECK_NAME);
					analysisFeedbackDao.saveOrUpdate(aa);
				}else if(scp.getMethod().equals("8")){
					//如果处理方式是带反馈是时候状态改为  结果明细待反馈
					scp.setState(com.biolims.workflow.WorkflowConstants.RESULT_DETAIL_WAIT);
					scp.setStateName(com.biolims.workflow.WorkflowConstants.RESULT_DETAIL_WAIT_NAME);
				}else{
					//如果选的是后4个就进入信息分析异常和项目管理-->异常反馈-->分析异常
					FeedbackAnalysis feedbackAnalysis = new FeedbackAnalysis();
					feedbackAnalysis.setA13(scp.getA13());
					feedbackAnalysis.setA18(scp.getA18());
					feedbackAnalysis.setA21(scp.getA21());
					feedbackAnalysis.setAge(scp.getAge());
					feedbackAnalysis.setAlignRate(scp.getAlignRate());
					feedbackAnalysis.setaX(scp.getaX());
					feedbackAnalysis.setaY(scp.getaY());
					feedbackAnalysis.setBabyNum(scp.getBabyNum());
					feedbackAnalysis.setChr13(scp.getChr13());
					feedbackAnalysis.setChr18(scp.getChr18());
					feedbackAnalysis.setChr21(scp.getChr21());
					feedbackAnalysis.setClinicalInfo(scp.getClinicalInfo());
					feedbackAnalysis.setCnv(scp.getCnv());
					feedbackAnalysis.setCode(scp.getPoolingCode());
					feedbackAnalysis.setComputerTime(scp.getComputerTime());
					feedbackAnalysis.setCrstResult(scp.getCrstResult());
					feedbackAnalysis.setDuplicationRate(scp.getDuplicationRate());
					feedbackAnalysis.setExceptionSample(scp.getExceptionSample());
					feedbackAnalysis.setFetal13(scp.getFetal13());
					feedbackAnalysis.setFetal18(scp.getFetal18());
					feedbackAnalysis.setFetal21(scp.getFetal21());
					feedbackAnalysis.setFetalAdd(scp.getFetalAdd());
					feedbackAnalysis.setFetalX(scp.getFetalX());
					feedbackAnalysis.setFetalY(scp.getFetalY());
					feedbackAnalysis.setGcContent(scp.getGcContent());
					feedbackAnalysis.setGender(scp.getGender());
					feedbackAnalysis.setGesWeeks(scp.getGesWeeks());
					feedbackAnalysis.setInterpretationTime(scp.getInterpretationTime());
					feedbackAnalysis.setIsReport(scp.getIsReport());
					feedbackAnalysis.setL13(scp.getL13());
					feedbackAnalysis.setL18(scp.getL18());
					feedbackAnalysis.setL21(scp.getL21());
					feedbackAnalysis.setlX(scp.getlX());
					feedbackAnalysis.setlY(scp.getlY());
					feedbackAnalysis.setMachineTime(scp.getMachineTime());
					feedbackAnalysis.setMethod(scp.getMethod());
					feedbackAnalysis.setNote(scp.getNote());
					feedbackAnalysis.setOtherRstExceptionResult(scp.getOtherRstExceptionResult());
					feedbackAnalysis.setPoolingCode(scp.getPoolingCode());
					feedbackAnalysis.setProtest(scp.getProtest());
					feedbackAnalysis.setQ30Ratio(scp.getQ30Ratio());
					feedbackAnalysis.setR13(scp.getR13());
					feedbackAnalysis.setR18(scp.getR18());
					feedbackAnalysis.setR21(scp.getR21());
					feedbackAnalysis.setRawReadNum(scp.getRawReadNum());
					feedbackAnalysis.setRcvCNVTime(scp.getRcvCNVTime());
					feedbackAnalysis.setRcvPotoTime(scp.getRcvPotoTime());
					feedbackAnalysis.setReadsMb(scp.getReadsMb());
					feedbackAnalysis.setRealURNum(scp.getRealURNum());
					feedbackAnalysis.setReportExpDate(scp.getReportExpDate());
					feedbackAnalysis.setReportSendTime(scp.getReportSendTime());
					feedbackAnalysis.setReportState(scp.getReportState());
					feedbackAnalysis.setReportToCustomServiceTime(scp.getReportToCustomServiceTime());
					feedbackAnalysis.setResult(scp.getResult());
					feedbackAnalysis.setResultOne(scp.getResultOne());
					feedbackAnalysis.setResultTwo(scp.getResultTwo());
					feedbackAnalysis.setRisk13(scp.getRisk13());
					feedbackAnalysis.setRisk18(scp.getRisk18());
					feedbackAnalysis.setRisk21(scp.getRisk21());
					feedbackAnalysis.setRiskValue18(scp.getRiskValue18());
					feedbackAnalysis.setRiskValue21(scp.getRiskValue21());
					feedbackAnalysis.setRiskX(scp.getRiskX());
					feedbackAnalysis.setRiskY(scp.getRiskY());
					feedbackAnalysis.setSampleCode(scp.getSampleCode());
					feedbackAnalysis.setSampleNum13(scp.getSampleNum13());
					feedbackAnalysis.setSampleNum18(scp.getSampleNum18());
					feedbackAnalysis.setSampleNum21(scp.getSampleNum21());
					feedbackAnalysis.setSampleNumX(scp.getSampleNumX());
					feedbackAnalysis.setSampleNumY(scp.getSampleNumY());
					feedbackAnalysis.setSampleRcvTime(scp.getSampleRcvTime());
					feedbackAnalysis.setSeqInfo(scp.getSeqInfo());
					feedbackAnalysis.setSubmit(scp.getSubmit());
					feedbackAnalysis.setSuggestResult(scp.getSuggestResult());
					feedbackAnalysis.settFive13(scp.gettFive13());
					feedbackAnalysis.settFive18(scp.gettFive18());
					feedbackAnalysis.settFive21(scp.gettFive21());
					feedbackAnalysis.settFiveX(scp.gettFiveX());
					feedbackAnalysis.settFiveY(scp.gettFiveY());
					feedbackAnalysis.settFour13(scp.gettFour13());
					feedbackAnalysis.settFour18(scp.gettFour18());
					feedbackAnalysis.settFour21(scp.gettFour21());
					feedbackAnalysis.settFourX(scp.gettFourX());
					feedbackAnalysis.settFourY(scp.gettFourY());
					feedbackAnalysis.settOne13(scp.gettOne13());
					feedbackAnalysis.settOne18(scp.gettOne18());
					feedbackAnalysis.settOne21(scp.gettOne21());
					feedbackAnalysis.settOneX(scp.gettOneX());
					feedbackAnalysis.settOneY(scp.gettOneY());
					feedbackAnalysis.settTwo13(scp.gettTwo13());
					feedbackAnalysis.settTwo21(scp.gettTwo21());
					feedbackAnalysis.settTwo18(scp.gettTwo18());
					feedbackAnalysis.settTwoX(scp.gettTwoX());
					feedbackAnalysis.settTwoY(scp.gettTwoY());
					feedbackAnalysis.setTsResult(scp.getTsResult());
					feedbackAnalysis.setUrRatio(scp.getUrRatio());
					feedbackAnalysis.setWeight(scp.getWeight());
					feedbackAnalysis.setXrstResult(scp.getXrstResult());
					//fc号给到
					feedbackAnalysis.setFcCode(scp.getAnalysisTask().getFlowCell());
					if(scp.getMethod().equals("8"))
						feedbackAnalysis.setState(com.biolims.workflow.WorkflowConstants.RESULT_DETAIL_WAIT);
					else
						feedbackAnalysis.setState("2");
					analysisFeedbackDao.saveOrUpdate(feedbackAnalysis);
				}
			}
		}
		analysisTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delAnalysisInfo(String[] ids) throws Exception {
		for (String id : ids) {
			SampleAnalysisInfo scp = analysisTaskDao.get(
					SampleAnalysisInfo.class, id);
			analysisTaskDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(AnalysisTask sc, Map jsonMap) throws Exception {
		if (sc != null) {
//			analysisTaskDao.a();
			analysisTaskDao.saveOrUpdate(sc);

			String jsonStr = "";
//			jsonStr = (String) jsonMap.get("analysisItem");
//			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
//				saveAnalysisItem(sc, jsonStr);
//			}
			jsonStr = (String) jsonMap.get("analysisInfo");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveAnalysisInfo(sc, jsonStr);
			}
		}
	}

	// 审批完成
	public void changeState(String applicationTypeActionId, String id) {
		AnalysisTask sct = analysisTaskDao.get(AnalysisTask.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_DATA_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_DATA_COMPLETE_NAME);
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		List<SampleAnalysisInfo> listAnaInfo = analysisTaskDao
				.getAnalysisInfo(sct.getId());
		// 下机质控工作流状态改变
//		DeSequencingTask dst = new DeSequencingTask();
//		dst.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_DATA_COMPLETE);
//		dst.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_DATA_COMPLETE_NAME);
		// 信息分析完成后，合格的数据到“数据解读中”
		/*
		 * for (SampleAnalysisInfo ai : listAnaInfo) {
		 * if(ai.getResult().equals("1")){ // interpretation_temp
		 * InterpretationTemp it=new InterpretationTemp();
		 * it.setAlignRatio(ai.getAlignRatio()); it.setCnv(ai.getCnv());
		 * it.setQ30Ratio(ai.getQ30Ratio()); it.setGcContent(ai.getGcContent());
		 * it.setReadsMb(ai.getReadsMb()); it.setResultOne(ai.getResultOne());
		 * it.setResultTwo(ai.getResultTwo());
		 * it.setSampleCode(ai.getSampleCode());
		 * it.setAnalysisInfoId(ai.getId()); it.setUrRatio(ai.getUrRatio());
		 * it.setResult(ai.getResult()); analysisTaskDao.saveOrUpdate(it); }
		 * if(ai.getResult().equals("0")){ //不合格的数据放到异常处理 AnalysisAbnormal
		 * ata=new AnalysisAbnormal(); ata.setCnv(ai.getCnv());
		 * ata.setSampleCode(ai.getSampleCode());
		 * ata.setGc_antent(ai.getGcContent());
		 * ata.setQ30_ratio(ai.getQ30Ratio()); ata.setReads_mb(ai.getReadsMb());
		 * ata.setResult1(ai.getResultOne()); ata.setResult2(ai.getResultTwo());
		 * ata.setUr_ratio(ai.getUrRatio());
		 * ata.setAlign_ratio(ai.getAlignRatio());
		 * analysisTaskDao.saveOrUpdate(ata); //删除方法 //
		 * delAnalysisInfoIsZore(ai.getResult()); } }
		 */
		analysisTaskDao.update(sct);
	}

	// 根据是否合格删除不合格数据。
	/*
	 * private void delAnalysisInfoIsZore(String result) { AnalysisInfo scp =
	 * analysisTaskDao.delAnalyInfoResultIsZore(result);
	 * System.out.println(scp.getResultOne()); analysisTaskDao.delete(scp); }
	 */
	public List<Map<String, String>> showAnalysisInfoList(String code)
			throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = analysisTaskDao.setAnalysisInfoList(code);
		List<SampleAnalysisInfo> list = (List<SampleAnalysisInfo>) result
				.get("list");
		if (list != null && list.size() > 0) {
			for (SampleAnalysisInfo ai : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ai.getId());
				map.put("resultOne", ai.getResultOne());
				map.put("resultTwo", ai.getResultTwo());
				map.put("alignRatio", ai.getAlignRate());
				map.put("cnv", ai.getCnv());
				map.put("gcContent", ai.getGcContent());
				map.put("method", ai.getMethod());
				map.put("q30Ratio", ai.getQ30Ratio());
				map.put("readsMb", ai.getReadsMb());
				map.put("result", ai.getResult());
				map.put("urRatio", ai.getUrRatio());
				map.put("aId", ai.getAnalysisTask().getId());
				map.put("aName", ai.getAnalysisTask().getName());
				mapList.add(map);
			}
		}
		return mapList;
	}

	public List<Map<String, String>> showAnalysisItemList(String code)
			throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = analysisTaskDao.setAnalysisItemList(code);
		List<AnalysisTaskItem> list = (List<AnalysisTaskItem>) result
				.get("list");

		if (list != null && list.size() > 0) {
			for (AnalysisTaskItem ai : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ai.getId());
				map.put("name", ai.getName());
				map.put("poolingCode", ai.getPoolingCode());
				map.put("sampleCode", ai.getSampleCode());
				map.put("wkCode", ai.getWkCode());
				map.put("state", ai.getStatus());
				map.put("aId", ai.getAnalysisTask().getId());
				map.put("aName", ai.getAnalysisTask().getName());
				mapList.add(map);
			}
		}
		return mapList;
	}

	public List<Map<String, String>> getDeSequencingTaskList(String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = analysisTaskDao
				.setDeSequencingTaskList(code);
		List<SampleDeSequencingInfo> list = (List<SampleDeSequencingInfo>) result
				.get("list");
		if (list != null && list.size() > 0) {
			for (SampleDeSequencingInfo sr : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("sName", sr.getName());
				map.put("sampleCode", sr.getSampleCode());
				mapList.add(map);
			}
		}
		return mapList;
	}

	public List<Map<String, String>> getAnalysisTaskList(String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = analysisTaskDao.getAnalysisTaskList(code);
		List<AnalysisTaskItem> list = (List<AnalysisTaskItem>) result
				.get("list");
		if (list != null && list.size() > 0) {
			for (AnalysisTaskItem sr : list) {
				Map<String, String> map = new HashMap<String, String>();
				/*
				 * String sampleCode=sr.getSampleCode();
				 * System.out.println("sampleCode="+sampleCode); Map<String,
				 * Object> resultSampleInfo =
				 * analysisTaskDao.getSampleInfoList(sampleCode);
				 * List<SampleInfo> listSample=(List<SampleInfo>)
				 * resultSampleInfo.get("list"); if (listSample != null &&
				 * listSample.size() > 0) { for (SampleInfo si : listSample) {
				 * // Map<String, String> map = new HashMap<String, String>();
				 * map.put("SIGender", si.getAge()); String
				 * sampleType=String.valueOf(si.getSampleType());
				 * map.put("SIsampleType",sampleType); map.put("SINote",
				 * si.getNote()); map.put("SIReportDate", si.getReportDate());
				 * map.put("SISampleCode", si.getCode()); } }
				 */
				map.put("SISampleCode", sr.getSampleCode());
				map.put("sName", sr.getName());
				mapList.add(map);
			}
		}
		/*
		 * Map<String, Object> resultSampleInfo =
		 * analysisTaskDao.getSampleInfoList(sampleCode); List<SampleInfo>
		 * listSample=(List<SampleInfo>) resultSampleInfo.get("list"); if
		 * (listSample != null && listSample.size() > 0) { for (SampleInfo si :
		 * listSample) { Map<String, String> map = new HashMap<String,
		 * String>(); map.put("SIGender", si.getAge()); String
		 * sampleType=String.valueOf(si.getSampleType());
		 * map.put("SIsampleType",sampleType); map.put("SINote", si.getNote());
		 * map.put("SIReportDate", si.getReportDate()); map.put("SISampleCode",
		 * si.getCode()); mapList.add(map); } }
		 */
		return mapList;
	}

	public void getAnalysisItemSampleCode(String id) {
		analysisTaskDao.getAnalysisItem(id);
	}

	public void getAnalysisInfoSampleCode(String id) {
		analysisTaskDao.getAnalysisInfo(id);
	}

	/**
	 * 生成信息分析CSV
	 * 
	 * @return
	 * @throws Exception
	 */
	/*@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String createCSV(String taskId) throws Exception {
		Map<String, String> mapForQuery = new HashMap<String, String>();
		mapForQuery.put("task.id", taskId);

		Map<String, Object> resultInfo = analysisTaskDao.selectAnalysisInfoList(taskId, null, null, null, null);
		List<SampleAnalysisInfo> listInfo = (List<SampleAnalysisInfo>) resultInfo.get("list");// 信息分析结果表
		if (listInfo == null || listInfo.size() <= 0)
			return null;

		String filePath = SystemConstants.INFO_QC_PATH;// 文件路径
		String fileName = filePath + listInfo.get(0).getAnalysisTask().getId()+ DateUtil.format(new Date(), "yyyyMMdd") + ".csv";
		File csvFile = null;
		BufferedWriter csvWtriter = null;
		csvFile = new File(fileName);
		File parent = csvFile.getParentFile();
		if (parent != null && !parent.exists()) {
			parent.mkdirs();
		}
		csvFile.createNewFile();

		// GB2312使正确读取分隔符","
		csvWtriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(csvFile), "GB2312"), 1024);
		// 写入文件头部
		Object[] head = { "文库号", "年龄", "体重", "孕周", "单双胎", "筛查模式", "21-三体比值","18-三体比值", "补充协议", "需要发报告", "提取方法" };
		List<Object> headList = Arrays.asList(head);
		for (Object data : headList) {
			StringBuffer sb = new StringBuffer();
			String rowStr = sb.append("\"").append(data).append("\",").toString();
			csvWtriter.write(rowStr);
		}
		csvWtriter.newLine();
		for(SampleAnalysisInfo info : listInfo ){
			StringBuffer sb = new StringBuffer();
			String productName=this.analysisTaskDao.selectProduct(info.getPoolingCode());
			 sb.append("\"").append(info.getSampleCode()).append("\",");
			 sb.append("\"").append(info.getAge()).append("\",");
			 sb.append("\"").append(info.getWeight()).append("\",");
			 sb.append("\"").append(info.getGesWeeks()).append("\",");
			 sb.append("\"").append(info.getBabyNum()).append("\",");
			 sb.append("\"").append(info.getTsResult()).append("\",");
			 sb.append("\"").append(info.getA21()).append("\",");
			 sb.append("\"").append(info.getA18()).append("\",");
			 sb.append("\"").append(info.getProtest()).append("\",");
			 sb.append("\"").append(info.getReportState()).append("\",");
			 sb.append("\"").append(productName).append("\",");
			 String rowStr =	sb.toString();
			 csvWtriter.write(rowStr);
			 csvWtriter.newLine();
		}
		csvWtriter.newLine();
		csvWtriter.flush();
		csvWtriter.close();
		return csvFile.getName();
	}*/
	
	/**
	 * 读取CSV文件
	 * 
	 * @param taskId
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void getCsvContent() throws Exception {
//		Map<String, String> mapForQuery = new HashMap<String, String>();
//		mapForQuery.put("task.id", taskId);

//		AnalysisTask at = this.commonDAO.get(AnalysisTask.class, taskId);
		
		List<AnalysisTask> listAnaly = this.analysisTaskDao.findTaskByState();
		for (AnalysisTask at1 : listAnaly) {
			//String filePath = com.biolims.system.code.SystemConstants.INFO_WRITE_PATH+"/"+at1.getFlowCell();// 文件夹路径
			//String fileName = "";// 文件名称
			//File dir = new File(filePath);
		//	if(dir.isDirectory()){
		//	File[] fm = dir.listFiles();

		//	for (File file : fm) {
				//if (file.getName().indexOf("Example.") >= 0&&(file.getName().indexOf(at1.getFlowCell()) >= 0)) {
					//if (file.isFile()) {
					//	fileName = file.getName();
						
						//InputStream is = new FileInputStream(filePath + "/" + fileName);
						InputStream is =  FtpUtil.downFileByStr(at1.getFlowCell(), "Example.",at1.getFlowCell());
						if(is!=null){
						CsvReader reader = new CsvReader(is, Charset.forName("GBK"));
						reader.readHeaders();			//去除表头
						while (reader.readRecord()) {
							SampleAnalysisInfo info = new SampleAnalysisInfo();
							//13号染色体
							info.setSampleCode(reader.get(reader.getHeader(0)));
							info.settOne13(reader.get(reader.getHeader(1)));
							info.settTwo13(reader.get(reader.getHeader(2)));
							info.setA13(reader.get(reader.getHeader(3)));
							info.setL13(reader.get(reader.getHeader(4)));
							info.setRisk13(reader.get(reader.getHeader(5)));
							info.setFetal13(reader.get(reader.getHeader(6)));
							info.settFour13(reader.get(reader.getHeader(7)));
							info.setSampleNum13(reader.get(reader.getHeader(8)));
							info.settFive13(reader.get(reader.getHeader(9)));

							//18号染色体
							info.settOne18(reader.get(reader.getHeader(10)));
							info.settTwo18(reader.get(reader.getHeader(11)));
							info.setA18(reader.get(reader.getHeader(12)));
							info.setL18(reader.get(reader.getHeader(13)));
							info.setRisk18(reader.get(reader.getHeader(14)));
							info.setFetal18(reader.get(reader.getHeader(15)));
							info.settFour18(reader.get(reader.getHeader(16)));
							info.setSampleNum18(reader.get(reader.getHeader(17)));
							info.settFive18(reader.get(reader.getHeader(18)));

							//21号染色体
							info.settOne21(reader.get(reader.getHeader(19)));
							info.settTwo21(reader.get(reader.getHeader(20)));
							info.setA21(reader.get(reader.getHeader(21)));
							info.setL21(reader.get(reader.getHeader(22)));
							info.setRisk21(reader.get(reader.getHeader(23)));
							info.setFetal21(reader.get(reader.getHeader(24)));
							info.settFour21(reader.get(reader.getHeader(25)));
							info.setSampleNum21(reader.get(reader.getHeader(26)));
							info.settFive21(reader.get(reader.getHeader(27)));

							//x号染色体
							info.settOneX(reader.get(reader.getHeader(28)));
							info.settTwoX(reader.get(reader.getHeader(29)));
							info.setlX(reader.get(reader.getHeader(30)));
							info.setRiskX(reader.get(reader.getHeader(31)));
							info.setFetalX(reader.get(reader.getHeader(32)));
							info.settFourX(reader.get(reader.getHeader(33)));
							info.setSampleNumX(reader.get(reader.getHeader(34)));
							info.settFiveX(reader.get(reader.getHeader(35)));

//							y号染色体
							info.settOneY(reader.get(reader.getHeader(36)));
							info.settTwoY(reader.get(reader.getHeader(37)));
							info.setlY(reader.get(reader.getHeader(38)));
							info.setRiskY(reader.get(reader.getHeader(39)));
							info.setFetalY(reader.get(reader.getHeader(40)));
							info.settFourY(reader.get(reader.getHeader(41)));
							info.setSampleNumY(reader.get(reader.getHeader(42)));
							info.settFiveY(reader.get(reader.getHeader(43)));

							
							info.setFetalAdd(reader.get(reader.getHeader(44)));
							info.setGender(reader.get(reader.getHeader(45)));
							info.setR13(reader.get(reader.getHeader(46)));
							info.setR18(reader.get(reader.getHeader(47)));
							info.setR21(reader.get(reader.getHeader(48)));
							info.setRx(reader.get(reader.getHeader(49)));
							info.setRy(reader.get(reader.getHeader(50)));

							info.setSeqInfo(reader.get(reader.getHeader(51)));
							info.setFetalExceptionSample(reader.get(reader.getHeader(52)));
							info.setOtherRstExceptionResult(reader.get(reader.getHeader(53)));
							info.setAge(reader.get(reader.getHeader(54)));
							info.setGesWeeks(reader.get(reader.getHeader(55)));
							info.setBabyNum(reader.get(reader.getHeader(56)));
							info.setTsResult(reader.get(reader.getHeader(57)));
							info.setWeight(reader.get(reader.getHeader(58)));
							info.setRiskValue21(reader.get(reader.getHeader(59)));
							info.setRiskValue18(reader.get(reader.getHeader(60)));
							info.setProtest(reader.get(reader.getHeader(61)));

							info.setRawReadNum(reader.get(reader.getHeader(62)));
							info.setAlignRate(reader.get(reader.getHeader(63)));
							info.setDuplicationRate(reader.get(reader.getHeader(64)));
							info.setRealURNum(reader.get(reader.getHeader(65)));
							info.setGcContent(reader.get(reader.getHeader(66)));
							info.setChr13(reader.get(reader.getHeader(67)));
							info.setChr18(reader.get(reader.getHeader(68)));
							info.setChr21(reader.get(reader.getHeader(69)));
							info.setSuggestResult(reader.get(reader.getHeader(70)));
							info.setCrstResult(reader.get(reader.getHeader(71)));
							info.setXrstResult(reader.get(reader.getHeader(72)));
							info.setMethod(reader.get(reader.getHeader(73)));
							info.setAnalysisTask(at1);
							this.analysisTaskDao.saveOrUpdate(info);
						}
						reader.close();
//						读取完成 修改状态为 “完成信息分析”
//						at.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_DATA_COMPLETE);
						at1.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_TQ_DATA_NAME);
						this.analysisTaskDao.saveOrUpdate(at1);
		}
			//		}
			//	}
			//}
		//	}
		}
	}
}
