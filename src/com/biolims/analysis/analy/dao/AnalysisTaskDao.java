package com.biolims.analysis.analy.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.analysis.analy.model.SampleAnalysisInfo;
import com.biolims.analysis.analy.model.AnalysisTaskItem;
import com.biolims.analysis.analy.model.AnalysisTask;
import com.biolims.analysis.desequencing.model.DeSequencingTask;
import com.biolims.analysis.desequencing.model.SampleDeSequencingInfo;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.goods.mate.model.GoodsMaterialsApplyItem;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleInput;

@Repository
@SuppressWarnings("unchecked")
public class AnalysisTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectAnalysisTaskList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from AnalysisTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<AnalysisTask> list = new ArrayList<AnalysisTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	//状态完成的数据
	public Map<String, Object> selectAnalysisTaskList1(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from AnalysisTask where 1=1 and state='1'";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<AnalysisTask> list = new ArrayList<AnalysisTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
		public Map<String, Object> selectAnalysisItemList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from AnalysisTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and analysisTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<AnalysisTaskItem> list = new ArrayList<AnalysisTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectAnalysisInfoList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from SampleAnalysisInfo t where 1=1";
		String key = "";
		if (scId != null)
			key = key + " and analysisTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleAnalysisInfo> list = new ArrayList<SampleAnalysisInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	public Map<String, Object> setAnalysisItemList(String code) throws Exception {
		String hql = "from AnalysisTaskItem t where 1=1 ";
		String key = "";
		if (code != null)
			key = key + " and  t.analysisTask='" + code + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<AnalysisTaskItem> list = this.getSession().createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	public Map<String, Object> setAnalysisInfoList(String code) throws Exception {
		String hql = "from SampleAnalysisInfo t where 1=1 ";
		String key = "";
		if (code != null)
			key = key + " and  t.analysisTask='" + code + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleAnalysisInfo> list = this.getSession().createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	public Map<String, Object> setDeSequencingTaskList(String code) {
		String hql = "from SampleDeSequencingInfo t where 1=1 and isGood='1'";
		String key = "";
		if (code != null)
			key = key + " and  t.desequencingTask='" + code + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleDeSequencingInfo> list = this.getSession().createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	//根据文库号查找分析结果
	public SampleAnalysisInfo setAnalysisInfoListByCode(String code) throws Exception {
		String hql = "from SampleAnalysisInfo t where 1=1 ";
		String key = "";
		if (code != null)
			key = key + " and  t.sampleCode='" + code + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleAnalysisInfo> list = this.getSession().createQuery(hql + key).list();
		if(list.size()>0)
		return list.get(0);
		return null;
	}
	public Map<String, Object> getAnalysisTaskList(String code) {
		String hql = "from AnalysisTaskItem t where 1=1 ";
		String key = "";
		if (code != null)
			key = key + " and  t.analysisTask='" + code + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<AnalysisTaskItem> list = this.getSession().createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	public Map<String, Object> getSampleInfoList(String code) {
		String hql = "from SampleInfo t where 1=1 ";
		String key = "";
		if (code != null)
			key = key + " and  t.analysisTask='" + code + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleInfo> list = this.getSession().createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
//	//查询左侧中间表
//	public Map<String, Object> selectAnalysisInfoTaskList(Map<String, String> mapForQuery, Integer startNum,
//			Integer limitNum, String dir, String sort) {
//		String key = " ";
//		String hql = " from AnalysisInfoTask where 1=1 and state is null";
//		if (mapForQuery != null)
//			key = map2where(mapForQuery);
//		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
//		List<AnalysisInfoTask> list = new ArrayList<AnalysisInfoTask>();
//		if (total > 0) {
//			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
//				if (sort.indexOf("-") != -1) {
//					sort = sort.substring(0, sort.indexOf("-"));
//				}
//				key = key + " order by " + sort + " " + dir;
//			}
//			if (startNum == null || limitNum == null) {
//				list = this.getSession().createQuery(hql + key).list();
//			} else {
//				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
//			}
//		}
//		Map<String, Object> result = new HashMap<String, Object>();
//		result.put("total", total);
//		result.put("list", list);
//		return result;
//	}
	public void getAnalysisItem(String id) {
		
	}
	public List<SampleAnalysisInfo> getAnalysisInfo(String id) {
		String hql = "from SampleAnalysisInfo t where 1=1 and t.analysisTask='"+id+"'";
		List<SampleAnalysisInfo> analIfo=this.getSession().createQuery(hql).list();
		return analIfo;
	}
	public SampleAnalysisInfo delAnalyInfoResultIsZore(String result) {
		String hql = "from SampleAnalysisInfo t where 1=1 and t.result='"+result+"'";
		SampleAnalysisInfo analIfo=(SampleAnalysisInfo) this.getSession().createQuery(hql).uniqueResult();
		return analIfo;
	}
	
//	//根据样本编号查询信息录入
//	public SampleInput selectInputByCode(String code){
//		String hql = "from SampleInput t where t.code = '"+code+"'";
//		SampleInput input=(SampleInput) this.getSession().createQuery(hql).uniqueResult();
//		return input;
//	}
	
	//根据pooling编号查询业务类型
	public String selectProduct(String poolingCode){
		String sql  = "select t.name from SYS_PRODUCT t where t.id in (select t1.product from pooling_task t1 where t1.id = '"+poolingCode+"')";
		String productName= (String) this.getSession().createSQLQuery(sql).uniqueResult();
		return productName;
	}
	
	//根据状态查询信息分析
	public List<AnalysisTask> findTaskByState(){
		String hql = "from AnalysisTask t where t.stateName= '新建'";
		List<AnalysisTask> list = this.getSession().createQuery(hql).list();
		return list;
	}
	public void a(){
		this.getSession().clear();
	}
}