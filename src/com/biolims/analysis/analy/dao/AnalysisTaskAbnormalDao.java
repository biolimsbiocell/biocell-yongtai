package com.biolims.analysis.analy.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.analysis.analy.model.SampleAnalysisInfo;
import com.biolims.analysis.analy.model.AnalysisAbnormal;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.sequencing.model.SequencingTaskInfo;
import com.biolims.experiment.wk.model.WkTaskInfo;

@Repository
@SuppressWarnings("unchecked")
public class AnalysisTaskAbnormalDao extends BaseHibernateDao {
	public Map<String, Object> selectAnalysisTaskAbnormalList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from FeedbackAnalysis where 1=1  and state is not null  and (isExecute ='0' or isExecute is null)";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<AnalysisAbnormal> list = new ArrayList<AnalysisAbnormal>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

//	public AnalysisTaskAbnormal delAnalyInfoResultIsZore(String result) {
//		String hql = "from AnalysisTaskAbnormal t where 1=1 and t.id='"+result+"'";
//		AnalysisTaskAbnormal ataInfo=(AnalysisTaskAbnormal) this.getSession().createQuery(hql).uniqueResult();
//		return ataInfo;
//	}
	
	/**
	 * 根据文库号查文库结果  
	 * @param wk_code
	 * @return
	 * @throws Exception
	 */
	public WkTaskInfo findByWkcode(String wk_code) throws Exception{
		String hql ="from WkTaskInfo t where t.wkCode = '"+wk_code+"'";
		WkTaskInfo wkTaskInfo  = (WkTaskInfo) this.getSession().createQuery(hql).uniqueResult();
		return wkTaskInfo;
	}
	
	/**
	 * 根据FcCode号查询上机的测序结果表
	 * @param fcCode
	 * @return
	 * @throws Exception
	 */
	public SequencingTaskInfo findByPooling(String fcCode) throws Exception{
		String hql ="from SequencingTaskInfo t where t.fcCode ='"+fcCode+"'";
		SequencingTaskInfo sequencingInfo = (SequencingTaskInfo) this.getSession().createQuery(hql).uniqueResult();
		return sequencingInfo;
	}
	
	/**
	 * 根据样本编号查 信息分析的结果明细表
	 * @param sampleCode
	 * @return
	 * @throws Exception
	 */
	public SampleAnalysisInfo findBySampleCode(String sampleCode) throws Exception{
		String hql ="from SampleAnalysisInfo t where t.sampleCode ='"+sampleCode+"'";
		SampleAnalysisInfo sampleAnalysisInfo =(SampleAnalysisInfo) this.getSession().createQuery(hql).uniqueResult();
		return sampleAnalysisInfo;
	}
	
}