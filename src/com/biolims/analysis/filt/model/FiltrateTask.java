package com.biolims.analysis.filt.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 过滤
 * @author lims-platform
 * @date 2016-02-23 11:05:40
 * @version V1.0   
 *
 */
@Entity
@Table(name = "FILTRATE_TASK")
@SuppressWarnings("serial")
public class FiltrateTask extends EntityDao<FiltrateTask> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
	/**项目整体状态*/
	private String projectIsGood;
	/**数据量是否足够*/
	private String dataIsGood;
	/**合同号*/
	private String contractId;
	/**项目号*/
	private String projectId;
	/**任务单号*/
	private String techTaskId;
	public String getTechTaskId() {
		return techTaskId;
	}
	public void setTechTaskId(String techTaskId) {
		this.techTaskId = techTaskId;
	}
	/**负责人*/
	private String headUser;
	/**状态id*/
	private String state;
	/**工作流状态*/
	private String stateName;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态id
	 */
	@Column(name ="STATE", length = 20)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态id
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  工作流状态
	 */
	@Column(name ="STATE_NAME", length = 20)
	public String getStateName() {
		return stateName;
	}
	/**
	 *方法: 设置String
	 *@param: String  工作流状态
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getProjectIsGood() {
		return projectIsGood;
	}
	public void setProjectIsGood(String projectIsGood) {
		this.projectIsGood = projectIsGood;
	}
	public String getDataIsGood() {
		return dataIsGood;
	}
	public void setDataIsGood(String dataIsGood) {
		this.dataIsGood = dataIsGood;
	}
	public String getContractId() {
		return contractId;
	}
	public void setContractId(String contractId) {
		this.contractId = contractId;
	}
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public String getHeadUser() {
		return headUser;
	}
	public void setHeadUser(String headUser) {
		this.headUser = headUser;
	}
	
	
}