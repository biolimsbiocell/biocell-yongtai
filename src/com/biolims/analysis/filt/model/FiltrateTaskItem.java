package com.biolims.analysis.filt.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 过滤明细
 * @author lims-platform
 * @date 2016-02-23 11:05:07
 * @version V1.0   
 *
 */
@Entity
@Table(name = "FILTRATE_TASK_ITEM")
@SuppressWarnings("serial")
public class FiltrateTaskItem extends EntityDao<FiltrateTaskItem> implements java.io.Serializable {
	/**过滤明细id*/
	private String id;
	/**样本编号*/
	private String smapleCode;
	/**文库编号*/
	private String wkCode;
	/**描述*/
	private String name;
	
	private String rawReadsNumber;
	private String rawBasesNumber;
	private String cleanReadsNumber;
	private String cleanBasesNumber;
	private String cleanReadsRate;
	private String rawReadsLength;
	private String cleanReadsLength;
	private String lowqualityReadsNumber;
	private String lowqualityReadsRate;
	private String nsReadsNumber;
	private String nsReadsRate;
	private String adapterPollutedReadsNumber;
	private String adapterPollutedReadsRate;
	private String polyAReadsNumber;
	private String polyAReadsRate;
	private String polyTReadsNumber;
	private String polyTReadsRate;
	private String highATReadsNumber;
	private String highATReadsRate;
	private String dpprn;//discardedPrimerPollutedReadsNumber
	private String dpprr;//discardedPrimerPollutedReadsRate
	private String tpprn;//trimedPrimerPollutedReadsNumber
	private String tpprr;//trimedPrimerPollutedReadsRate
//	private String discardedPrimerPollutedReadsNumber;
//	private String discardedPrimerPollutedReadsRate;
//	private String trimedPrimerPollutedReadsNumber;
//	private String trimedPrimerPollutedReadsRate;
	private String cleanQ30BasesRate;
	private String rawQ30BasesRate;
	private String cleanQ20BasesRate;
	private String rawQ20BasesRate;
	private String rRNAMappingRate;
	private String without3pAdaptorReadsRate;
	private String withoutInsertReadsRate;
	private String polyATReadsRate;
	private String exLengthrReadsRate;
	private String rRNAMappingNumber;
	private String without3pAdaptorReadsNumber;
	private String withoutInsertReadsNumber;
	private String polyATReadsNumber;
	private String exLengthrReadsNumber;
	private String totalReadsNumber;
	private String totalCleanBasesNumber;
	private String q30;
	
	/**状态*/
	private String state;
	/**备注*/
	private String note;
	/**关联主表*/
	private FiltrateTask filtrateTask;
	/**
	 *方法: 取得String
	 *@return: String  过滤明细id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  过滤明细id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本编号
	 */
	@Column(name ="SMAPLE_CODE", length = 100)
	public String getSmapleCode(){
		return this.smapleCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本编号
	 */
	public void setSmapleCode(String smapleCode){
		this.smapleCode = smapleCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  文库编号
	 */
	@Column(name ="WK_CODE", length = 100)
	public String getWkCode(){
		return this.wkCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  文库编号
	 */
	public void setWkCode(String wkCode){
		this.wkCode = wkCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得FiltrateTask
	 *@return: FiltrateTask  关联主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "FILTRATE_TASK")
	public FiltrateTask getFiltrateTask(){
		return this.filtrateTask;
	}
	/**
	 *方法: 设置FiltrateTask
	 *@param: FiltrateTask  关联主表
	 */
	public void setFiltrateTask(FiltrateTask filtrateTask){
		this.filtrateTask = filtrateTask;
	}
	public String getRawReadsNumber() {
		return rawReadsNumber;
	}
	public void setRawReadsNumber(String rawReadsNumber) {
		this.rawReadsNumber = rawReadsNumber;
	}
	public String getRawBasesNumber() {
		return rawBasesNumber;
	}
	public void setRawBasesNumber(String rawBasesNumber) {
		this.rawBasesNumber = rawBasesNumber;
	}
	public String getCleanReadsNumber() {
		return cleanReadsNumber;
	}
	public void setCleanReadsNumber(String cleanReadsNumber) {
		this.cleanReadsNumber = cleanReadsNumber;
	}
	public String getCleanBasesNumber() {
		return cleanBasesNumber;
	}
	public void setCleanBasesNumber(String cleanBasesNumber) {
		this.cleanBasesNumber = cleanBasesNumber;
	}
	public String getCleanReadsRate() {
		return cleanReadsRate;
	}
	public void setCleanReadsRate(String cleanReadsRate) {
		this.cleanReadsRate = cleanReadsRate;
	}
	public String getRawReadsLength() {
		return rawReadsLength;
	}
	public void setRawReadsLength(String rawReadsLength) {
		this.rawReadsLength = rawReadsLength;
	}
	public String getLowqualityReadsNumber() {
		return lowqualityReadsNumber;
	}
	public void setLowqualityReadsNumber(String lowqualityReadsNumber) {
		this.lowqualityReadsNumber = lowqualityReadsNumber;
	}
	public String getLowqualityReadsRate() {
		return lowqualityReadsRate;
	}
	public void setLowqualityReadsRate(String lowqualityReadsRate) {
		this.lowqualityReadsRate = lowqualityReadsRate;
	}
	public String getNsReadsNumber() {
		return nsReadsNumber;
	}
	public void setNsReadsNumber(String nsReadsNumber) {
		this.nsReadsNumber = nsReadsNumber;
	}
	public String getNsReadsRate() {
		return nsReadsRate;
	}
	public void setNsReadsRate(String nsReadsRate) {
		this.nsReadsRate = nsReadsRate;
	}
	public String getAdapterPollutedReadsNumber() {
		return adapterPollutedReadsNumber;
	}
	public void setAdapterPollutedReadsNumber(String adapterPollutedReadsNumber) {
		this.adapterPollutedReadsNumber = adapterPollutedReadsNumber;
	}
	public String getAdapterPollutedReadsRate() {
		return adapterPollutedReadsRate;
	}
	public void setAdapterPollutedReadsRate(String adapterPollutedReadsRate) {
		this.adapterPollutedReadsRate = adapterPollutedReadsRate;
	}
	public String getPolyAReadsNumber() {
		return polyAReadsNumber;
	}
	public void setPolyAReadsNumber(String polyAReadsNumber) {
		this.polyAReadsNumber = polyAReadsNumber;
	}
	public String getPolyAReadsRate() {
		return polyAReadsRate;
	}
	public void setPolyAReadsRate(String polyAReadsRate) {
		this.polyAReadsRate = polyAReadsRate;
	}
	public String getPolyTReadsNumber() {
		return polyTReadsNumber;
	}
	public void setPolyTReadsNumber(String polyTReadsNumber) {
		this.polyTReadsNumber = polyTReadsNumber;
	}
	public String getPolyTReadsRate() {
		return polyTReadsRate;
	}
	public void setPolyTReadsRate(String polyTReadsRate) {
		this.polyTReadsRate = polyTReadsRate;
	}
	public String getHighATReadsNumber() {
		return highATReadsNumber;
	}
	public void setHighATReadsNumber(String highATReadsNumber) {
		this.highATReadsNumber = highATReadsNumber;
	}
	public String getHighATReadsRate() {
		return highATReadsRate;
	}
	public void setHighATReadsRate(String highATReadsRate) {
		this.highATReadsRate = highATReadsRate;
	}
	
	public String getDpprn() {
		return dpprn;
	}
	public void setDpprn(String dpprn) {
		this.dpprn = dpprn;
	}
	public String getDpprr() {
		return dpprr;
	}
	public void setDpprr(String dpprr) {
		this.dpprr = dpprr;
	}
	public String getTpprn() {
		return tpprn;
	}
	public void setTpprn(String tpprn) {
		this.tpprn = tpprn;
	}
	public String getTpprr() {
		return tpprr;
	}
	public void setTpprr(String tpprr) {
		this.tpprr = tpprr;
	}
	public String getCleanQ30BasesRate() {
		return cleanQ30BasesRate;
	}
	public String getCleanQ20BasesRate() {
		return cleanQ20BasesRate;
	}
	public void setCleanQ20BasesRate(String cleanQ20BasesRate) {
		this.cleanQ20BasesRate = cleanQ20BasesRate;
	}
	public String getRawQ20BasesRate() {
		return rawQ20BasesRate;
	}
	public void setRawQ20BasesRate(String rawQ20BasesRate) {
		this.rawQ20BasesRate = rawQ20BasesRate;
	}
	public void setCleanQ30BasesRate(String cleanQ30BasesRate) {
		this.cleanQ30BasesRate = cleanQ30BasesRate;
	}
	public String getrRNAMappingRate() {
		return rRNAMappingRate;
	}
	public void setrRNAMappingRate(String rRNAMappingRate) {
		this.rRNAMappingRate = rRNAMappingRate;
	}
	public String getWithout3pAdaptorReadsRate() {
		return without3pAdaptorReadsRate;
	}
	public void setWithout3pAdaptorReadsRate(String without3pAdaptorReadsRate) {
		this.without3pAdaptorReadsRate = without3pAdaptorReadsRate;
	}
	public String getWithoutInsertReadsRate() {
		return withoutInsertReadsRate;
	}
	public void setWithoutInsertReadsRate(String withoutInsertReadsRate) {
		this.withoutInsertReadsRate = withoutInsertReadsRate;
	}
	public String getPolyATReadsRate() {
		return polyATReadsRate;
	}
	public void setPolyATReadsRate(String polyATReadsRate) {
		this.polyATReadsRate = polyATReadsRate;
	}
	public String getExLengthrReadsRate() {
		return exLengthrReadsRate;
	}
	public void setExLengthrReadsRate(String exLengthrReadsRate) {
		this.exLengthrReadsRate = exLengthrReadsRate;
	}
	public String getrRNAMappingNumber() {
		return rRNAMappingNumber;
	}
	public void setrRNAMappingNumber(String rRNAMappingNumber) {
		this.rRNAMappingNumber = rRNAMappingNumber;
	}
	public String getWithout3pAdaptorReadsNumber() {
		return without3pAdaptorReadsNumber;
	}
	public void setWithout3pAdaptorReadsNumber(String without3pAdaptorReadsNumber) {
		this.without3pAdaptorReadsNumber = without3pAdaptorReadsNumber;
	}
	public String getWithoutInsertReadsNumber() {
		return withoutInsertReadsNumber;
	}
	public void setWithoutInsertReadsNumber(String withoutInsertReadsNumber) {
		this.withoutInsertReadsNumber = withoutInsertReadsNumber;
	}
	public String getPolyATReadsNumber() {
		return polyATReadsNumber;
	}
	public void setPolyATReadsNumber(String polyATReadsNumber) {
		this.polyATReadsNumber = polyATReadsNumber;
	}
	public String getExLengthrReadsNumber() {
		return exLengthrReadsNumber;
	}
	public void setExLengthrReadsNumber(String exLengthrReadsNumber) {
		this.exLengthrReadsNumber = exLengthrReadsNumber;
	}
	public String getCleanReadsLength() {
		return cleanReadsLength;
	}
	public void setCleanReadsLength(String cleanReadsLength) {
		this.cleanReadsLength = cleanReadsLength;
	}
	public String getRawQ30BasesRate() {
		return rawQ30BasesRate;
	}
	public void setRawQ30BasesRate(String rawQ30BasesRate) {
		this.rawQ30BasesRate = rawQ30BasesRate;
	}
	public String getTotalReadsNumber() {
		return totalReadsNumber;
	}
	public void setTotalReadsNumber(String totalReadsNumber) {
		this.totalReadsNumber = totalReadsNumber;
	}
	public String getTotalCleanBasesNumber() {
		return totalCleanBasesNumber;
	}
	public void setTotalCleanBasesNumber(String totalCleanBasesNumber) {
		this.totalCleanBasesNumber = totalCleanBasesNumber;
	}
	public String getQ30() {
		return q30;
	}
	public void setQ30(String q30) {
		this.q30 = q30;
	}
	
}