package com.biolims.analysis.filt.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 过滤结果
 * @author lims-platform
 * @date 2016-02-23 11:05:17
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SAMPLE_FILTRATE_INFO")
@SuppressWarnings("serial")
public class SampleFiltrateInfo extends EntityDao<SampleFiltrateInfo> implements java.io.Serializable {
	/**过滤结果id*/
	private String id;
	/**文库编号*/
	private String wkCode;
	/**描述*/
	private String name;
	/**样本编号*/
	private String sampleCode;
	/**下机数据量*/
	private String desDataNum;
	/**过滤数据量*/
	private String filtDataNum;
	/**要求数据量*/
	private String normDataNum;
	/**要求测序类型*/
	private String sequenceType;
	/**实际测序类型*/
	private String realSequenceType;
	/**测序类型是否一致*/
	private String isSequenceType;
	/**是否满足数据量要求*/
	private String isDataNum;
	/**需要加测数据量*/
	private String addDataNum;
	/**异常值*/
	private String abnomal;
	/**是否合格*/
	private String idGood;
	/**下一步流向*/
	private String nextflow;
	/**是否提交*/
	private String submit;
	/**备注*/
	private String note;
	/**关联主表*/
	private FiltrateTask filtrateTask;
	/**
	 *方法: 取得String
	 *@return: String  过滤结果id
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  过滤结果id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  文库编号
	 */
	@Column(name ="WK_CODE", length = 50)
	public String getWkCode(){
		return this.wkCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  文库编号
	 */
	public void setWkCode(String wkCode){
		this.wkCode = wkCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本编号
	 */
	@Column(name ="SAMPLE_CODE", length = 50)
	public String getSampleCode(){
		return this.sampleCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本编号
	 */
	public void setSampleCode(String sampleCode){
		this.sampleCode = sampleCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  下机数据量
	 */
	@Column(name ="DES_DATA_NUM", length = 50)
	public String getDesDataNum(){
		return this.desDataNum;
	}
	/**
	 *方法: 设置String
	 *@param: String  下机数据量
	 */
	public void setDesDataNum(String desDataNum){
		this.desDataNum = desDataNum;
	}
	/**
	 *方法: 取得String
	 *@return: String  过滤数据量
	 */
	@Column(name ="FILT_DATA_NUM", length = 50)
	public String getFiltDataNum(){
		return this.filtDataNum;
	}
	/**
	 *方法: 设置String
	 *@param: String  过滤数据量
	 */
	public void setFiltDataNum(String filtDataNum){
		this.filtDataNum = filtDataNum;
	}
	/**
	 *方法: 取得String
	 *@return: String  要求数据量
	 */
	@Column(name ="NORM_DATA_NUM", length = 50)
	public String getNormDataNum(){
		return this.normDataNum;
	}
	/**
	 *方法: 设置String
	 *@param: String  要求数据量
	 */
	public void setNormDataNum(String normDataNum){
		this.normDataNum = normDataNum;
	}
	/**
	 *方法: 取得String
	 *@return: String  要求测序类型
	 */
	@Column(name ="SEQUENCE_TYPE", length = 50)
	public String getSequenceType(){
		return this.sequenceType;
	}
	/**
	 *方法: 设置String
	 *@param: String  要求测序类型
	 */
	public void setSequenceType(String sequenceType){
		this.sequenceType = sequenceType;
	}
	/**
	 *方法: 取得String
	 *@return: String  实际测序类型
	 */
	@Column(name ="REAL_SEQUENCE_TYPE", length = 50)
	public String getRealSequenceType(){
		return this.realSequenceType;
	}
	/**
	 *方法: 设置String
	 *@param: String  实际测序类型
	 */
	public void setRealSequenceType(String realSequenceType){
		this.realSequenceType = realSequenceType;
	}
	/**
	 *方法: 取得String
	 *@return: String  测序类型是否一致
	 */
	@Column(name ="IS_SEQUENCE_TYPE", length = 50)
	public String getIsSequenceType(){
		return this.isSequenceType;
	}
	/**
	 *方法: 设置String
	 *@param: String  测序类型是否一致
	 */
	public void setIsSequenceType(String isSequenceType){
		this.isSequenceType = isSequenceType;
	}
	/**
	 *方法: 取得String
	 *@return: String  是否满足数据量要求
	 */
	@Column(name ="IS_DATA_NUM", length = 50)
	public String getIsDataNum(){
		return this.isDataNum;
	}
	/**
	 *方法: 设置String
	 *@param: String  是否满足数据量要求
	 */
	public void setIsDataNum(String isDataNum){
		this.isDataNum = isDataNum;
	}
	/**
	 *方法: 取得String
	 *@return: String  需要加测数据量
	 */
	@Column(name ="ADD_DATA_NUM", length = 50)
	public String getAddDataNum(){
		return this.addDataNum;
	}
	/**
	 *方法: 设置String
	 *@param: String  需要加测数据量
	 */
	public void setAddDataNum(String addDataNum){
		this.addDataNum = addDataNum;
	}
	/**
	 *方法: 取得String
	 *@return: String  异常值
	 */
	@Column(name ="ABNOMAL", length = 200)
	public String getAbnomal(){
		return this.abnomal;
	}
	/**
	 *方法: 设置String
	 *@param: String  异常值
	 */
	public void setAbnomal(String abnomal){
		this.abnomal = abnomal;
	}
	/**
	 *方法: 取得String
	 *@return: String  是否合格
	 */
	@Column(name ="ID_GOOD", length = 20)
	public String getIdGood(){
		return this.idGood;
	}
	/**
	 *方法: 设置String
	 *@param: String  是否合格
	 */
	public void setIdGood(String idGood){
		this.idGood = idGood;
	}
	/**
	 *方法: 取得String
	 *@return: String  下一步流向
	 */
	@Column(name ="NEXTFLOW", length = 50)
	public String getNextflow(){
		return this.nextflow;
	}
	/**
	 *方法: 设置String
	 *@param: String  下一步流向
	 */
	public void setNextflow(String nextflow){
		this.nextflow = nextflow;
	}
	/**
	 *方法: 取得String
	 *@return: String  是否提交
	 */
	@Column(name ="SUBMIT", length = 50)
	public String getSubmit(){
		return this.submit;
	}
	/**
	 *方法: 设置String
	 *@param: String  是否提交
	 */
	public void setSubmit(String submit){
		this.submit = submit;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得FiltrateTask
	 *@return: FiltrateTask  关联主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "FILTRATE_TASK")
	public FiltrateTask getFiltrateTask(){
		return this.filtrateTask;
	}
	/**
	 *方法: 设置FiltrateTask
	 *@param: FiltrateTask  关联主表
	 */
	public void setFiltrateTask(FiltrateTask filtrateTask){
		this.filtrateTask = filtrateTask;
	}
}