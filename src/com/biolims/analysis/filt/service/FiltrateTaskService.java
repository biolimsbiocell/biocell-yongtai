package com.biolims.analysis.filt.service;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.analysis.filt.dao.FiltrateTaskDao;
import com.biolims.analysis.filt.model.FiltrateTask;
import com.biolims.analysis.filt.model.FiltrateTaskItem;
import com.biolims.analysis.filt.model.SampleFiltrateInfo;
import com.biolims.common.FtpUtil;
import com.biolims.common.dao.CommonDAO;
import com.biolims.experiment.check.dao.TechCheckServiceTaskDao;
import com.biolims.experiment.wk.dao.WkTaskDao;
import com.biolims.experiment.wk.model.WkTaskInfo;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.technology.wk.model.TechJkServiceTask;
import com.biolims.util.JsonUtils;
import com.csvreader.CsvReader;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class FiltrateTaskService {
	@Resource
	private FiltrateTaskDao filtrateTaskDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private TechCheckServiceTaskDao techCheckServiceTaskDao;
	@Resource
	private WkTaskDao wkTaskDao;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findFiltrateTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return filtrateTaskDao.selectFiltrateTaskList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FiltrateTask i) throws Exception {

		filtrateTaskDao.saveOrUpdate(i);

	}

	public FiltrateTask get(String id) {
		FiltrateTask filtrateTask = commonDAO.get(FiltrateTask.class, id);
		return filtrateTask;
	}

	public Map<String, Object> findFiltrateTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = filtrateTaskDao
				.selectFiltrateTaskItemList(scId, startNum, limitNum, dir, sort);
		List<FiltrateTaskItem> list = (List<FiltrateTaskItem>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findSampleFiltrateInfoList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = filtrateTaskDao
				.selectSampleFiltrateInfoList(scId, startNum, limitNum, dir,
						sort);
		List<SampleFiltrateInfo> list = (List<SampleFiltrateInfo>) result
				.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveFiltrateTaskItem(FiltrateTask sc, String itemDataJson)
			throws Exception {
		List<FiltrateTaskItem> saveItems = new ArrayList<FiltrateTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			FiltrateTaskItem scp = new FiltrateTaskItem();
			// 将map信息读入实体类
			scp = (FiltrateTaskItem) filtrateTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setFiltrateTask(sc);

			saveItems.add(scp);
		}
		filtrateTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delFiltrateTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			FiltrateTaskItem scp = filtrateTaskDao.get(FiltrateTaskItem.class,
					id);
			filtrateTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleFiltrateInfo(FiltrateTask sc, String itemDataJson)
			throws Exception {
		List<SampleFiltrateInfo> saveItems = new ArrayList<SampleFiltrateInfo>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SampleFiltrateInfo scp = new SampleFiltrateInfo();
			// 将map信息读入实体类
			scp = (SampleFiltrateInfo) filtrateTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setFiltrateTask(sc);

			saveItems.add(scp);
		}
		filtrateTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSampleFiltrateInfo(String[] ids) throws Exception {
		for (String id : ids) {
			SampleFiltrateInfo scp = filtrateTaskDao.get(
					SampleFiltrateInfo.class, id);
			filtrateTaskDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(FiltrateTask sc, Map jsonMap) throws Exception {
		if (sc != null) {
			filtrateTaskDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("filtrateTaskItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveFiltrateTaskItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("sampleFiltrateInfo");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSampleFiltrateInfo(sc, jsonStr);
			}
		}
	}

//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void getCsvContent() throws Exception {
//		// Map<String, String> mapForQuery = new HashMap<String, String>();
//		// mapForQuery.put("task.id", taskId);
//		// DeSequencingTask dst = commonDAO.get(DeSequencingTask.class, taskId);
//
//		List<FiltrateTask> listDes = this.filtrateTaskDao.findTaskByState();
//		for (FiltrateTask dst1 : listDes) {
//			// String filePath =
//			// com.biolims.system.code.SystemConstants.SEQ_QC_PATH+"\\"+dst1.getFlowCode();
//			String path = "";
//			// File dir = new File(filePath);
//			// if (dir.exists() && dir.isDirectory() && dir.listFiles().length >
//			// 0) {
//			// File[] fm = dir.listFiles();
//			// for (File file : fm) {
//			// if(dir.isDirectory()){
//
//			// 如果目录存在 进行读取操作
//			// path = filePath ;
//			// File a = new File(path+ "\\fulltable.csv");
//			// if(a.isFile()){
//			// InputStream is = new FileInputStream(path+ "\\fulltable.csv");
//			InputStream is = FtpUtil.downFile(
//					dst1.getContractId() + "/" + dst1.getProjectId() + "/"
//							+ dst1.getHeadUser(), "fulltable.csv");
//			if (is != null) {
//				CsvReader reader = new CsvReader(is, Charset.forName("GBK"));
//				reader.readHeaders();// 去除表头
//				while (reader.readRecord()) {// 下机质控明细
//					FiltrateTaskItem item = new FiltrateTaskItem();
//					item.setWkCode(reader.get(reader.getHeader(0)));
//					item.setRawReadsNumber(reader.get(reader.getHeader(1)));
//					item.setRawBasesNumber(reader.get(reader.getHeader(2)));
//					item.setRawReadsLength(reader.get(reader.getHeader(3)));
//					item.setCleanReadsNumber(reader.get(reader.getHeader(4)));
//					item.setCleanBasesNumber(reader.get(reader.getHeader(5)));
//					if (dst1.getProjectId() != null
//							&& (!dst1.getProjectId().equals("3")// 原核有参转录组
//									|| !dst1.getProjectId().equals("4")
//									|| !dst1.getProjectId().equals("5")// 原核有参转录组
//									|| !dst1.getProjectId().equals("6") || !dst1
//									.getProjectId().equals("7"))) {// incRNA转录组
//						item.setCleanReadsLength(reader.get(reader.getHeader(6)));
//						item.setCleanReadsRate(reader.get(reader.getHeader(7)));
//						item.setAdapterPollutedReadsNumber(reader.get(reader
//								.getHeader(8)));
//						item.setAdapterPollutedReadsRate(reader.get(reader
//								.getHeader(9)));
//						item.setNsReadsNumber(reader.get(reader.getHeader(10)));
//						item.setNsReadsRate(reader.get(reader.getHeader(11)));
//						item.setLowqualityReadsNumber(reader.get(reader
//								.getHeader(12)));
//						item.setLowqualityReadsRate(reader.get(reader
//								.getHeader(13)));
//						item.setRawQ30BasesRate(reader.get(reader.getHeader(14)));
//						item.setCleanQ30BasesRate(reader.get(reader
//								.getHeader(15)));
//					} else {
//						item.setCleanReadsRate(reader.get(reader.getHeader(6)));
//						item.setLowqualityReadsNumber(reader.get(reader
//								.getHeader(7)));
//						item.setLowqualityReadsRate(reader.get(reader
//								.getHeader(8)));
//					}
//					if (dst1.getProjectId() != null
//							&& (dst1.getProjectId().equals("3")// 原核有参转录组
//									|| dst1.getProjectId().equals("4")
//									|| dst1.getProjectId().equals("5") || dst1
//									.getProjectId().equals("6"))) {
//						item.setNsReadsNumber(reader.get(reader.getHeader(9)));
//						item.setNsReadsRate(reader.get(reader.getHeader(10)));
//					} else {
//						item.setWithout3pAdaptorReadsNumber(reader.get(reader
//								.getHeader(9)));
//						item.setWithout3pAdaptorReadsRate(reader.get(reader
//								.getHeader(10)));
//					}
//					if (dst1.getProjectId() != null
//							&& (dst1.getProjectId().equals("3")// 原核有参转录组
//							|| dst1.getProjectId().equals("4"))) {
//						item.setAdapterPollutedReadsNumber(reader.get(reader
//								.getHeader(11)));
//						item.setAdapterPollutedReadsRate(reader.get(reader
//								.getHeader(12)));
//						item.setRawQ30BasesRate(reader.get(reader.getHeader(13)));
//						item.setCleanQ30BasesRate(reader.get(reader
//								.getHeader(14)));
//						item.setrRNAMappingNumber(reader.get(reader
//								.getHeader(15)));
//						item.setrRNAMappingRate(reader.get(reader.getHeader(16)));
//						item.setTotalReadsNumber(reader.get(reader
//								.getHeader(17)));
//						item.setTotalCleanBasesNumber(reader.get(reader
//								.getHeader(18)));
//						item.setQ30(reader.get(reader.getHeader(19)));
//					}
//					if (dst1.getProjectId() != null
//							&& !dst1.getProjectId().equals("5")) {// 单细胞转录组
//						item.setPolyAReadsNumber(reader.get(reader
//								.getHeader(11)));
//						item.setPolyAReadsRate(reader.get(reader.getHeader(12)));
//						item.setPolyTReadsNumber(reader.get(reader
//								.getHeader(13)));
//						item.setPolyTReadsRate(reader.get(reader.getHeader(14)));
//						item.setHighATReadsNumber(reader.get(reader
//								.getHeader(15)));
//						item.setHighATReadsRate(reader.get(reader.getHeader(16)));
//						item.setAdapterPollutedReadsNumber(reader.get(reader
//								.getHeader(17)));
//						item.setAdapterPollutedReadsRate(reader.get(reader
//								.getHeader(18)));
//						item.setDpprn(reader.get(reader.getHeader(19)));
//						item.setDpprr(reader.get(reader.getHeader(20)));
//						item.setTpprn(reader.get(reader.getHeader(21)));
//						item.setTpprr(reader.get(reader.getHeader(22)));
//						item.setCleanQ30BasesRate(reader.get(reader
//								.getHeader(23)));
//					}
//					if (dst1.getProjectId() != null
//							&& !dst1.getProjectId().equals("6")) {// 单细胞基因组
//						item.setAdapterPollutedReadsNumber(reader.get(reader
//								.getHeader(11)));
//						item.setAdapterPollutedReadsRate(reader.get(reader
//								.getHeader(12)));
//						item.setTpprn(reader.get(reader.getHeader(13)));
//						item.setTpprr(reader.get(reader.getHeader(14)));
//						item.setCleanQ30BasesRate(reader.get(reader
//								.getHeader(15)));
//					}
//					if (dst1.getProjectId() != null
//							&& dst1.getProjectId().equals("7")) {// 小RNA转录组
//						item.setWithoutInsertReadsNumber(reader.get(reader
//								.getHeader(11)));
//						item.setWithoutInsertReadsRate(reader.get(reader
//								.getHeader(12)));
//						item.setPolyATReadsNumber(reader.get(reader
//								.getHeader(13)));
//						item.setPolyATReadsRate(reader.get(reader.getHeader(14)));
//						item.setExLengthrReadsNumber(reader.get(reader
//								.getHeader(15)));
//						item.setExLengthrReadsRate(reader.get(reader
//								.getHeader(16)));
//						item.setRawQ20BasesRate(reader.get(reader.getHeader(17)));
//						item.setRawQ30BasesRate(reader.get(reader.getHeader(18)));
//						item.setCleanQ20BasesRate(reader.get(reader
//								.getHeader(19)));
//						item.setCleanQ30BasesRate(reader.get(reader
//								.getHeader(20)));
//					}
//					// item.setLowqualityReadsNumber(reader.get(reader.getHeader(7)));
//					// item.setNsReadsNumber(reader.get(reader.getHeader(9)));
//					// item.setAdapterPollutedReadsNumber(reader.get(reader.getHeader(11)));
//					// item.setPolyAReadsNumber(reader.get(reader.getHeader(13)));
//					// item.setPolyTReadsNumber(reader.get(reader.getHeader(15)));
//					// item.setHighATReadsNumber(reader.get(reader.getHeader(17)));
//					// item.setDiscardedPrimerPollutedReadsNumber(reader.get(reader.getHeader(19)));
//					// item.setTrimedPrimerPollutedReadsNumber(reader.get(reader.getHeader(21)));
//					// DeSequencingTask dt = new DeSequencingTask();
//					// dt.setId(taskId);
//					item.setFiltrateTask(dst1);
//					this.filtrateTaskDao.saveOrUpdate(item);
//
//				}
//				reader.close();
//			}
//			List<FiltrateTaskItem> sfiList = filtrateTaskDao
//					.findFiltrateaItemByTaskId(dst1.getId());
//			if (sfiList.size() > 0) {
//				for (FiltrateTaskItem sti : sfiList) {
//					SampleFiltrateInfo sfi = new SampleFiltrateInfo();
//					// sfi.setSampleCode(sti.getSmapleCode());
//					sfi.setWkCode(sti.getWkCode());
//					sfi.setDesDataNum(sti.getRawBasesNumber());
//					sfi.setFiltDataNum(sti.getCleanBasesNumber());
//					List<WkTaskInfo> swi = wkTaskDao
//							.getWKSampleInfoBySampleCode(sti.getWkCode());
//					TechJkServiceTask tjst = new TechJkServiceTask();
//					if (swi != null) {
//						tjst = wkTaskDao.get(TechJkServiceTask.class, swi
//								.get(0).getJkTaskId());
//						// SampleInfo si =
//						// sampleInfoMainDao.findSampleInfo(swi.get(0).getSampleCode());
//						String dataType = swi.get(0).getDataType();
//						if (swi.get(0).getDataNum() != null
//								&& !swi.get(0).getDataNum().equals("")) {
//							if (dataType != null && !dataType.equals("")
//									&& dataType.equals("0")) {// data
//								sfi.setNormDataNum(swi.get(0).getDataNum());
//							} else {// read
//								if (tjst.getSequenceType() != null
//										&& tjst.getSequenceType().equals("0")) {// PE
//									sfi.setNormDataNum(String.valueOf(Double
//											.parseDouble(swi.get(0)
//													.getDataNum())
//											* (Double.parseDouble(tjst
//													.getSequenceLength())) * 2));
//								} else {
//									sfi.setNormDataNum(String.valueOf(Double
//											.parseDouble(swi.get(0)
//													.getDataNum())
//											* (Double.parseDouble(tjst
//													.getSequenceLength()))));
//								}
//							}
//						}
//						sfi.setSequenceType(tjst.getSequenceType());
//						sfi.setRealSequenceType(sti.getRawReadsLength());
//						String result1 = "";
//						String result2 = "";
//						// 根据项目方向将不合格的指标传递
//						if (dataType != null
//								&& !dataType.equals("")
//								&& (dataType.equals("0")
//										|| dataType.equals("1")
//										|| dataType.equals("2")
//										|| dataType.equals("8")
//										|| dataType.equals("9")
//										|| dataType.equals("10")
//										|| dataType.equals("11")
//										|| dataType.equals("13")
//										|| dataType.equals("14")
//										|| dataType.equals("15") || dataType
//											.equals("16"))) {
//							if (Integer.parseInt(sti.getCleanReadsRate()) > 75
//									&& Integer
//											.parseInt(sti.getCleanReadsRate()) < 80) {
//								result1 += "Clean Reads Rate (%):"
//										+ sti.getCleanReadsRate() + ";";
//							}
//							if (Integer.parseInt(sti.getCleanReadsRate()) < 75) {
//								result2 += "Clean Reads Rate (%):"
//										+ sti.getCleanReadsRate() + ";";
//							}
//							if (Integer.parseInt(sti.getLowqualityReadsRate()) < 15
//									&& Integer.parseInt(sti
//											.getLowqualityReadsRate()) > 10) {
//								result1 += "Low-quality Reads Rate (%):"
//										+ sti.getLowqualityReadsRate() + ";";
//							}
//							if (Integer.parseInt(sti.getLowqualityReadsRate()) > 15) {
//								result2 += "Low-quality Reads Rate (%):"
//										+ sti.getLowqualityReadsRate() + ";";
//							}
//							if (Integer.parseInt(sti
//									.getAdapterPollutedReadsRate()) < 15
//									&& Integer.parseInt(sti
//											.getAdapterPollutedReadsRate()) > 10) {
//								result1 += "Adapter Polluted Reads Rate (%):"
//										+ sti.getAdapterPollutedReadsRate()
//										+ ";";
//							}
//							if (Integer.parseInt(sti.getLowqualityReadsRate()) > 15) {
//								result2 += "Adapter Polluted Reads Rate (%):"
//										+ sti.getAdapterPollutedReadsRate()
//										+ ";";
//							}
//							if (Integer.parseInt(sti.getNsReadsRate()) < 5
//									&& Integer.parseInt(sti.getNsReadsRate()) > 1) {
//								result1 += "Ns Reads Rate (%):"
//										+ sti.getAdapterPollutedReadsRate()
//										+ ";";
//							}
//							if (Integer.parseInt(sti.getLowqualityReadsRate()) > 5) {
//								result2 += "Ns Reads Rate (%):"
//										+ sti.getAdapterPollutedReadsRate()
//										+ ";";
//							}
//							if (Integer.parseInt(sti.getCleanQ30BasesRate()) < 85
//									&& Integer.parseInt(sti
//											.getCleanQ30BasesRate()) > 83) {
//								result1 += "Clean Q30 Bases Rate (%):"
//										+ sti.getCleanQ30BasesRate() + ";";
//							}
//							if (Integer.parseInt(sti.getLowqualityReadsRate()) < 83) {
//								result2 += "Clean Q30 Bases Rate (%):"
//										+ sti.getCleanQ30BasesRate() + ";";
//							}
//						}
//						if (dataType != null
//								&& !dataType.equals("")
//								&& (dataType.equals("3") || dataType
//										.equals("4"))) {
//							if (Integer.parseInt(sti.getCleanReadsRate()) > 70
//									&& Integer
//											.parseInt(sti.getCleanReadsRate()) < 75) {
//								result1 += "Clean Reads Rate (%):"
//										+ sti.getCleanReadsRate() + ";";
//							}
//							if (Integer.parseInt(sti.getCleanReadsRate()) < 70) {
//								result2 += "Clean Reads Rate (%):"
//										+ sti.getCleanReadsRate() + ";";
//							}
//							if (Integer.parseInt(sti.getLowqualityReadsRate()) < 15
//									&& Integer.parseInt(sti
//											.getLowqualityReadsRate()) > 10) {
//								result1 += "Low-quality Reads Rate (%):"
//										+ sti.getLowqualityReadsRate() + ";";
//							}
//							if (Integer.parseInt(sti.getLowqualityReadsRate()) > 15) {
//								result2 += "Low-quality Reads Rate (%):"
//										+ sti.getLowqualityReadsRate() + ";";
//							}
//							if (Integer.parseInt(sti
//									.getAdapterPollutedReadsRate()) < 15
//									&& Integer.parseInt(sti
//											.getAdapterPollutedReadsRate()) > 10) {
//								result1 += "Adapter Polluted Reads Rate (%):"
//										+ sti.getAdapterPollutedReadsRate()
//										+ ";";
//							}
//							if (Integer.parseInt(sti.getLowqualityReadsRate()) > 15) {
//								result2 += "Adapter Polluted Reads Rate (%):"
//										+ sti.getAdapterPollutedReadsRate()
//										+ ";";
//							}
//							if (Integer.parseInt(sti.getNsReadsRate()) < 5
//									&& Integer.parseInt(sti.getNsReadsRate()) > 1) {
//								result1 += "Ns Reads Rate (%):"
//										+ sti.getAdapterPollutedReadsRate()
//										+ ";";
//							}
//							if (Integer.parseInt(sti.getLowqualityReadsRate()) > 5) {
//								result2 += "Ns Reads Rate (%):"
//										+ sti.getAdapterPollutedReadsRate()
//										+ ";";
//							}
//							if (Integer.parseInt(sti.getCleanQ30BasesRate()) < 85
//									&& Integer.parseInt(sti
//											.getCleanQ30BasesRate()) > 83) {
//								result1 += "Clean Q30 Bases Rate (%):"
//										+ sti.getCleanQ30BasesRate() + ";";
//							}
//							if (Integer.parseInt(sti.getLowqualityReadsRate()) < 83) {
//								result2 += "Clean Q30 Bases Rate (%):"
//										+ sti.getCleanQ30BasesRate() + ";";
//							}
//							if (Integer.parseInt(sti.getrRNAMappingRate()) < 12
//									&& Integer.parseInt(sti
//											.getrRNAMappingRate()) > 10) {
//								result1 += "rRNA Mapping Rate (%):"
//										+ sti.getrRNAMappingRate() + ";";
//							}
//							if (Integer.parseInt(sti.getrRNAMappingRate()) > 12) {
//								result2 += "rRNA Mapping Rate (%):"
//										+ sti.getrRNAMappingRate() + ";";
//							}
//						}
//						if (dataType != null
//								&& !dataType.equals("")
//								&& (dataType.equals("5") || dataType
//										.equals("6"))) {
//							if (dataType != null && !dataType.equals("")
//									&& dataType.equals("5")) {
//								if (Integer.parseInt(sti.getCleanReadsRate()) > 70
//										&& Integer.parseInt(sti
//												.getCleanReadsRate()) < 75) {
//									result1 += "Clean Reads Rate (%):"
//											+ sti.getCleanReadsRate() + ";";
//								}
//								if (Integer.parseInt(sti.getPolyAReadsRate()) < 10
//										&& Integer.parseInt(sti
//												.getPolyAReadsRate()) > 5) {
//									result1 += "PolyA Reads Rate (%):"
//											+ sti.getPolyAReadsRate() + ";";
//								}
//								if (Integer.parseInt(sti.getPolyAReadsRate()) > 10) {
//									result2 += "PolyA Reads Rate (%):"
//											+ sti.getPolyAReadsRate() + ";";
//								}
//								if (Integer.parseInt(sti.getPolyTReadsRate()) < 10
//										&& Integer.parseInt(sti
//												.getPolyTReadsRate()) > 5) {
//									result1 += "PolyT Reads Rate (%):"
//											+ sti.getPolyTReadsRate() + ";";
//								}
//								if (Integer.parseInt(sti.getPolyTReadsRate()) > 10) {
//									result2 += "PolyT Reads Rate (%):"
//											+ sti.getPolyTReadsRate() + ";";
//								}
//								if (Integer.parseInt(sti.getHighATReadsRate()) < 10
//										&& Integer.parseInt(sti
//												.getHighATReadsRate()) > 5) {
//									result1 += "HighAT Reads Rate (%):"
//											+ sti.getHighATReadsRate() + ";";
//								}
//								if (Integer.parseInt(sti.getHighATReadsRate()) > 10) {
//									result2 += "HighAT Reads Rate (%):"
//											+ sti.getHighATReadsRate() + ";";
//								}
//								if (Integer.parseInt(sti.getDpprr()) < 15
//										&& Integer.parseInt(sti.getDpprr()) > 10) {
//									result1 += "Discarded Primer Polluted Reads Rate (%):"
//											+ sti.getDpprr() + ";";
//								}
//								if (Integer.parseInt(sti.getDpprr()) > 15) {
//									result2 += "Discarded Primer Polluted Reads Rate (%):"
//											+ sti.getDpprr() + ";";
//								}
//								if (Integer.parseInt(sti.getTpprr()) < 20
//										&& Integer.parseInt(sti.getTpprr()) > 15) {
//									result1 += "Trimed Primer Polluted Reads Rate (%):"
//											+ sti.getTpprr() + ";";
//								}
//								if (Integer.parseInt(sti.getTpprr()) > 20) {
//									result2 += "Trimed Primer Polluted Reads Rate (%):"
//											+ sti.getTpprr() + ";";
//								}
//							} else {
//								if (Integer.parseInt(sti.getCleanReadsRate()) > 70
//										&& Integer.parseInt(sti
//												.getCleanReadsRate()) < 80) {
//									result1 += "Clean Reads Rate (%):"
//											+ sti.getCleanReadsRate() + ";";
//								}
//								if (Integer.parseInt(sti.getTpprr()) < 15
//										&& Integer.parseInt(sti.getTpprr()) > 10) {
//									result1 += "Trimed Primer Polluted Reads Rate (%):"
//											+ sti.getTpprr() + ";";
//								}
//								if (Integer.parseInt(sti.getTpprr()) > 15) {
//									result2 += "Trimed Primer Polluted Reads Rate (%):"
//											+ sti.getTpprr() + ";";
//								}
//							}
//							if (Integer.parseInt(sti.getCleanReadsRate()) < 70) {
//								result2 += "Clean Reads Rate (%):"
//										+ sti.getCleanReadsRate() + ";";
//							}
//							if (Integer.parseInt(sti.getLowqualityReadsRate()) < 15
//									&& Integer.parseInt(sti
//											.getLowqualityReadsRate()) > 10) {
//								result1 += "Low-quality Reads Rate (%):"
//										+ sti.getLowqualityReadsRate() + ";";
//							}
//							if (Integer.parseInt(sti.getLowqualityReadsRate()) > 15) {
//								result2 += "Low-quality Reads Rate (%):"
//										+ sti.getLowqualityReadsRate() + ";";
//							}
//							if (Integer.parseInt(sti
//									.getAdapterPollutedReadsRate()) < 15
//									&& Integer.parseInt(sti
//											.getAdapterPollutedReadsRate()) > 10) {
//								result1 += "Adapter Polluted Reads Rate (%):"
//										+ sti.getAdapterPollutedReadsRate()
//										+ ";";
//							}
//							if (Integer.parseInt(sti.getLowqualityReadsRate()) > 15) {
//								result2 += "Adapter Polluted Reads Rate (%):"
//										+ sti.getAdapterPollutedReadsRate()
//										+ ";";
//							}
//							if (Integer.parseInt(sti.getNsReadsRate()) < 5
//									&& Integer.parseInt(sti.getNsReadsRate()) > 1) {
//								result1 += "Ns Reads Rate (%):"
//										+ sti.getAdapterPollutedReadsRate()
//										+ ";";
//							}
//							if (Integer.parseInt(sti.getLowqualityReadsRate()) > 5) {
//								result2 += "Ns Reads Rate (%):"
//										+ sti.getAdapterPollutedReadsRate()
//										+ ";";
//							}
//							if (Integer.parseInt(sti.getCleanQ30BasesRate()) < 85
//									&& Integer.parseInt(sti
//											.getCleanQ30BasesRate()) > 83) {
//								result1 += "Clean Q30 Bases Rate (%):"
//										+ sti.getCleanQ30BasesRate() + ";";
//							}
//							if (Integer.parseInt(sti.getLowqualityReadsRate()) < 83) {
//								result2 += "Clean Q30 Bases Rate (%):"
//										+ sti.getCleanQ30BasesRate() + ";";
//							}
//						}
//						if (dataType != null && !dataType.equals("")
//								&& dataType.equals("7")) {
//							if (Integer.parseInt(sti.getCleanReadsRate()) > 70
//									&& Integer
//											.parseInt(sti.getCleanReadsRate()) < 73) {
//								result1 += "Clean Reads Rate (%):"
//										+ sti.getCleanReadsRate() + ";";
//							}
//							if (Integer.parseInt(sti.getCleanReadsRate()) < 70) {
//								result2 += "Clean Reads Rate (%):"
//										+ sti.getCleanReadsRate() + ";";
//							}
//							if (Integer.parseInt(sti.getLowqualityReadsRate()) < 15
//									&& Integer.parseInt(sti
//											.getLowqualityReadsRate()) > 10) {
//								result1 += "Low-quality Reads Rate (%):"
//										+ sti.getLowqualityReadsRate() + ";";
//							}
//							if (Integer.parseInt(sti.getLowqualityReadsRate()) > 15) {
//								result2 += "Low-quality Reads Rate (%):"
//										+ sti.getLowqualityReadsRate() + ";";
//							}
//							if (Integer.parseInt(sti.getCleanQ30BasesRate()) < 85
//									&& Integer.parseInt(sti
//											.getCleanQ30BasesRate()) > 80) {
//								result1 += "Clean Q30 Bases Rate (%):"
//										+ sti.getCleanQ30BasesRate() + ";";
//							}
//							if (Integer.parseInt(sti.getLowqualityReadsRate()) < 80) {
//								result2 += "Clean Q30 Bases Rate (%):"
//										+ sti.getCleanQ30BasesRate() + ";";
//							}
//							if (Integer.parseInt(sti
//									.getWithout3pAdaptorReadsRate()) < 15
//									&& Integer.parseInt(sti
//											.getWithout3pAdaptorReadsRate()) > 10) {
//								result1 += "Without 3p Adaptor Reads Rate (%):"
//										+ sti.getWithout3pAdaptorReadsRate()
//										+ ";";
//							}
//							if (Integer.parseInt(sti
//									.getWithout3pAdaptorReadsRate()) > 15) {
//								result2 += "Without 3p Adaptor Reads Rate (%):"
//										+ sti.getWithout3pAdaptorReadsRate()
//										+ ";";
//							}
//							if (Integer.parseInt(sti
//									.getWithoutInsertReadsRate()) < 15
//									&& Integer.parseInt(sti
//											.getWithoutInsertReadsRate()) > 10) {
//								result1 += "Without Insert Reads Rate (%):"
//										+ sti.getWithoutInsertReadsRate() + ";";
//							}
//							if (Integer.parseInt(sti
//									.getWithoutInsertReadsRate()) > 15) {
//								result2 += "Without Insert Reads Rate (%):"
//										+ sti.getWithoutInsertReadsRate() + ";";
//							}
//							if (Integer.parseInt(sti.getPolyATReadsRate()) < 15
//									&& Integer.parseInt(sti
//											.getPolyATReadsRate()) > 10) {
//								result1 += "Poly A/T Reads Rate (%):"
//										+ sti.getPolyATReadsRate() + ";";
//							}
//							if (Integer.parseInt(sti.getPolyATReadsRate()) > 15) {
//								result2 += "Poly A/T Reads Rate (%):"
//										+ sti.getPolyATReadsRate() + ";";
//							}
//							if (Integer.parseInt(sti.getExLengthrReadsRate()) < 15
//									&& Integer.parseInt(sti
//											.getExLengthrReadsRate()) > 10) {
//								result1 += "Ex-length Reads Rate (%):"
//										+ sti.getExLengthrReadsRate() + ";";
//							}
//							if (Integer.parseInt(sti.getExLengthrReadsRate()) > 15) {
//								result2 += "Ex-length Reads Rate (%):"
//										+ sti.getExLengthrReadsRate() + ";";
//							}
//						}
//						if (dataType != null && !dataType.equals("")
//								&& dataType.equals("12")) {
//							if (Integer.parseInt(sti.getCleanReadsRate()) > 80
//									&& Integer
//											.parseInt(sti.getCleanReadsRate()) < 85) {
//								result1 += "Clean Reads Rate (%):"
//										+ sti.getCleanReadsRate() + ";";
//							}
//							if (Integer.parseInt(sti.getCleanReadsRate()) < 80) {
//								result2 += "Clean Reads Rate (%):"
//										+ sti.getCleanReadsRate() + ";";
//							}
//							if (Integer.parseInt(sti.getLowqualityReadsRate()) < 5
//									&& Integer.parseInt(sti
//											.getLowqualityReadsRate()) > 3) {
//								result1 += "Low-quality Reads Rate (%):"
//										+ sti.getLowqualityReadsRate() + ";";
//							}
//							if (Integer.parseInt(sti.getLowqualityReadsRate()) > 5) {
//								result2 += "Low-quality Reads Rate (%):"
//										+ sti.getLowqualityReadsRate() + ";";
//							}
//							if (Integer.parseInt(sti
//									.getAdapterPollutedReadsRate()) < 10
//									&& Integer.parseInt(sti
//											.getAdapterPollutedReadsRate()) > 5) {
//								result1 += "Adapter Polluted Reads Rate (%):"
//										+ sti.getAdapterPollutedReadsRate()
//										+ ";";
//							}
//							if (Integer.parseInt(sti.getLowqualityReadsRate()) > 10) {
//								result2 += "Adapter Polluted Reads Rate (%):"
//										+ sti.getAdapterPollutedReadsRate()
//										+ ";";
//							}
//							if (Integer.parseInt(sti.getNsReadsRate()) < 5
//									&& Integer.parseInt(sti.getNsReadsRate()) > 1) {
//								result1 += "Ns Reads Rate (%):"
//										+ sti.getAdapterPollutedReadsRate()
//										+ ";";
//							}
//							if (Integer.parseInt(sti.getLowqualityReadsRate()) > 5) {
//								result2 += "Ns Reads Rate (%):"
//										+ sti.getAdapterPollutedReadsRate()
//										+ ";";
//							}
//							if (Integer.parseInt(sti.getCleanQ30BasesRate()) < 87
//									&& Integer.parseInt(sti
//											.getCleanQ30BasesRate()) > 85) {
//								result1 += "Clean Q30 Bases Rate (%):"
//										+ sti.getCleanQ30BasesRate() + ";";
//							}
//							if (Integer.parseInt(sti.getLowqualityReadsRate()) < 85) {
//								result2 += "Clean Q30 Bases Rate (%):"
//										+ sti.getCleanQ30BasesRate() + ";";
//							}
//
//						}
//						sfi.setAbnomal(result2);
//
//						sfi.setFiltrateTask(dst1);
//						filtrateTaskDao.saveOrUpdate(sfi);
//					}
//				}
//			}
//			// //读取完成 改变表的状态为 “完成下机质控”
//			// dst.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_SEQC_COMPLETE);
//			dst1.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_TQ_DATA_NAME);
//			this.filtrateTaskDao.saveOrUpdate(dst1);
//			// }
//			// }
//		}
//
//	}
}
