﻿
package com.biolims.analysis.filt.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.analysis.filt.model.FiltrateTask;
import com.biolims.analysis.filt.model.FiltrateTaskItem;
import com.biolims.analysis.filt.model.SampleFiltrateInfo;
import com.biolims.analysis.filt.service.FiltrateTaskService;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/analysis/filt/filtrateTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class FiltrateTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "260301";
	@Autowired
	private FiltrateTaskService filtrateTaskService;
	private FiltrateTask filtrateTask = new FiltrateTask();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showFiltrateTaskList")
	public String showFiltrateTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/analysis/filt/filtrateTask.jsp");
	}

	@Action(value = "showFiltrateTaskListJson")
	public void showFiltrateTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = filtrateTaskService.findFiltrateTaskList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<FiltrateTask> list = (List<FiltrateTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("projectIsGood", "");
		map.put("dataIsGood", "");
		map.put("contractId", "");
		map.put("projectId", "");
		map.put("headUser", "");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "filtrateTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogFiltrateTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/analysis/filt/filtrateTaskDialog.jsp");
	}

	@Action(value = "showDialogFiltrateTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogFiltrateTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = filtrateTaskService.findFiltrateTaskList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<FiltrateTask> list = (List<FiltrateTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("projectIsGood", "");
		map.put("dataIsGood", "");
		map.put("contractId", "");
		map.put("projectId", "");
		map.put("headUser", "");
		map.put("state", "");
		map.put("statename", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editFiltrateTask")
	public String editFiltrateTask() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			filtrateTask = filtrateTaskService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "filtrateTask");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//			filtrateTask.setCreateUser(user);
//			filtrateTask.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(filtrateTask.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/analysis/filt/filtrateTaskEdit.jsp");
	}

	@Action(value = "copyFiltrateTask")
	public String copyFiltrateTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		filtrateTask = filtrateTaskService.get(id);
		filtrateTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/analysis/filt/filtrateTaskEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = filtrateTask.getId();
		if(id!=null&&id.equals("")){
			filtrateTask.setId(null);
		}
		Map aMap = new HashMap();
			aMap.put("filtrateTaskItem",getParameterFromRequest("filtrateTaskItemJson"));
		
			aMap.put("sampleFiltrateInfo",getParameterFromRequest("sampleFiltrateInfoJson"));
		
		filtrateTaskService.save(filtrateTask,aMap);
		return redirect("/analysis/filt/filtrateTask/editFiltrateTask.action?id=" + filtrateTask.getId());

	}

	@Action(value = "viewFiltrateTask")
	public String toViewFiltrateTask() throws Exception {
		String id = getParameterFromRequest("id");
		filtrateTask = filtrateTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/analysis/filt/filtrateTaskEdit.jsp");
	}
	

	@Action(value = "showFiltrateTaskItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showFiltrateTaskItemList() throws Exception {
		return dispatcher("/WEB-INF/page/analysis/filt/filtrateTaskItem.jsp");
	}

	@Action(value = "showFiltrateTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showFiltrateTaskItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = filtrateTaskService.findFiltrateTaskItemList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<FiltrateTaskItem> list = (List<FiltrateTaskItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("smapleCode", "");
			map.put("wkCode", "");
			map.put("name", "");
			map.put("rRNAMappingRate", "");
			map.put("rRNAMappingNumber", "");
			map.put("without3pAdaptorReadsRate", "");
			map.put("without3pAdaptorReadsNumber", "");
			map.put("withoutInsertReadsRate", "");
			map.put("withoutInsertReadsNumber", "");
			map.put("polyATReadsRate", "");
			map.put("polyATReadsNumber", "");
			map.put("exLengthrReadsRate", "");
			map.put("exLengthrReadsNumber", "");
			map.put("rawReadsNumber", "");
			map.put("rawBasesNumber", "");
			map.put("cleanReadsNumber", "");
			map.put("cleanBasesNumber", "");
			map.put("cleanReadsRate", "");
			map.put("cleanReadsLength", "");
			map.put("rawReadsLength", "");
			map.put("lowqualityReadsNumber", "");
			map.put("lowqualityReadsRate", "");
			map.put("nsReadsNumber", "");
			map.put("nsReadsRate", "");
			map.put("adapterPollutedReadsNumber", "");
			map.put("adapterPollutedReadsRate", "");
			map.put("polyAReadsNumber", "");
			map.put("polyAReadsRate", "");
			map.put("polyTReadsNumber", "");
			map.put("polyTReadsRate", "");
			map.put("highATReadsNumber", "");
			map.put("highATReadsRate", "");
			map.put("dpprn", "");
			map.put("dpprr", "");
			map.put("tpprn", "");
			map.put("tpprr", "");
			map.put("cleanQ30BasesRate", "");
			map.put("cleanQ20BasesRate", "");
			map.put("rawQ20BasesRate", "");
			map.put("rawQ30BasesRate", "");
			map.put("totalReadsNumber", "");
			map.put("totalCleanBasesNumber", "");
			map.put("q30", "");
			map.put("state", "");
			map.put("note", "");
			map.put("filtrateTask-name", "");
			map.put("filtrateTask-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delFiltrateTaskItem")
	public void delFiltrateTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			filtrateTaskService.delFiltrateTaskItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	

	@Action(value = "showSampleFiltrateInfoList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleFiltrateInfoList() throws Exception {
		return dispatcher("/WEB-INF/page/analysis/filt/sampleFiltrateInfo.jsp");
	}

	@Action(value = "showSampleFiltrateInfoListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleFiltrateInfoListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = filtrateTaskService.findSampleFiltrateInfoList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<SampleFiltrateInfo> list = (List<SampleFiltrateInfo>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("wkCode", "");
			map.put("name", "");
			map.put("sampleCode", "");
			map.put("desDataNum", "");
			map.put("filtDataNum", "");
			map.put("normDataNum", "");
			map.put("sequenceType", "");
			map.put("realSequenceType", "");
			map.put("isSequenceType", "");
			map.put("isDataNum", "");
			map.put("addDataNum", "");
			map.put("abnomal", "");
			map.put("idGood", "");
			map.put("nextflow", "");
			map.put("submit", "");
			map.put("note", "");
			map.put("filtrateTask-name", "");
			map.put("filtrateTask-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delSampleFiltrateInfo")
	public void delSampleFiltrateInfo() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			filtrateTaskService.delSampleFiltrateInfo(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
//	@Action(value="getCsv")
//	public void getCsv() throws Exception{
////		String taskId = getRequest().getParameter("taskId");
//		Map<String, Object> map = new HashMap<String, Object>();
//		try {
//			filtrateTaskService.getCsvContent();
//		} catch (Exception e) {
//			e.printStackTrace();
//			map.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public FiltrateTaskService getFiltrateTaskService() {
		return filtrateTaskService;
	}

	public void setFiltrateTaskService(FiltrateTaskService filtrateTaskService) {
		this.filtrateTaskService = filtrateTaskService;
	}

	public FiltrateTask getFiltrateTask() {
		return filtrateTask;
	}

	public void setFiltrateTask(FiltrateTask filtrateTask) {
		this.filtrateTask = filtrateTask;
	}


}
