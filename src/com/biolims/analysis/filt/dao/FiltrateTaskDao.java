package com.biolims.analysis.filt.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.analysis.desequencing.model.DeSequencingTask;
import com.biolims.analysis.filt.model.FiltrateTask;
import com.biolims.analysis.filt.model.FiltrateTaskItem;
import com.biolims.analysis.filt.model.SampleFiltrateInfo;
import com.biolims.dao.BaseHibernateDao;

@Repository
@SuppressWarnings("unchecked")
public class FiltrateTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectFiltrateTaskList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from FiltrateTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<FiltrateTask> list = new ArrayList<FiltrateTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
		public Map<String, Object> selectFiltrateTaskItemList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from FiltrateTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and filtrateTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<FiltrateTaskItem> list = new ArrayList<FiltrateTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectSampleFiltrateInfoList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from SampleFiltrateInfo where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and filtrateTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleFiltrateInfo> list = new ArrayList<SampleFiltrateInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		
//		根据主表id查找结果
		public List<FiltrateTaskItem> findFiltrateaItemByTaskId(String id){
			String hql = "from FiltrateTaskItem t where t.filtrateTask= '"+id+"'";
			List<FiltrateTaskItem> list = this.getSession().createQuery(hql).list();
			return list;
		}
		
//		根据状态获取下机质控集合
		public List<FiltrateTask> findTaskByState(){
			String hql = "from FiltrateTask t where t.stateName=  '新建'";
			List<FiltrateTask> list = this.getSession().createQuery(hql).list();
			return list;
		}
}