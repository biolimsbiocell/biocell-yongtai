package com.biolims.analysis.desequencing.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.analysis.desequencing.dao.DeSequencingTaskAbnormalDao;
import com.biolims.analysis.desequencing.model.DeSequencingAbnormal;
import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.experiment.plasma.model.PlasmaAbnormal;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.model.LogInfo;
import com.biolims.project.feedback.model.FeedbackDeSequencing;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.util.JsonUtils;

@Service
@Transactional
public class DeSequencingTaskAbnormalService {
	@Resource
	private DeSequencingTaskAbnormalDao deSequencingTaskAbnormalDao;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public DeSequencingAbnormal get(String id) {
		DeSequencingAbnormal deSequencingTaskAbnormal = commonDAO.get(DeSequencingAbnormal.class, id);
		return deSequencingTaskAbnormal;
	}
	//保存异常核酸样本
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveDeSequencingTaskAbnormalList(String itemDataJson) throws Exception {
//		List<DeSequencingAbnormal> saveItems = new ArrayList<DeSequencingAbnormal>();
		List<FeedbackDeSequencing> saveItems = new ArrayList<FeedbackDeSequencing>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			FeedbackDeSequencing sbi = new FeedbackDeSequencing();
			
			sbi = (FeedbackDeSequencing) deSequencingTaskAbnormalDao.Map2Bean(map, sbi);
			if(sbi.getId()!=null && sbi.getId().equals("")){
				sbi.setId(null);
			}
			saveItems.add(sbi);
			if(sbi.getNextflow()!=null && sbi.getIsRun() !=null && sbi.getIsRun().equals("1")){
				if(sbi.getMethod()!=null && !sbi.getMethod().equals("")){//如果处理意见不为空 则说明去过项目组 根据处理意见执行
					if(sbi.getMethod().equals("2")){//处理意见为 合格
						sbi.setResult("1");
					}
				}else{									//如果处理意见为空 按照下一步流向执行
					if(sbi.getNextflow().equals("0")){//下一步流向：反馈至建库组
						
					}else if(sbi.getNextflow().equals("1")){//下一步流向：合格
						sbi.setResult("1");
					}else{//下一步流向：反馈至项目组
						sbi.setState("3");
					}
				}
			}else{
				sbi.setState("2");
			}
			this.deSequencingTaskAbnormalDao.saveOrUpdate(sbi);
//			DeSequencingAbnormal sbi = new DeSequencingAbnormal();
//			sbi = (DeSequencingAbnormal) deSequencingTaskAbnormalDao.Map2Bean(map, sbi);
//			if (sbi.getId() != null && sbi.getId().equals(""))
//				sbi.setId(null);
//			saveItems.add(sbi);
			//下机质控异常，如果结果为“合格-1”那么就将该条数据放到“信息分析”
//			if(sbi.getResult().equals("1")){
//				AnalysisTask at=new AnalysisTask();
////				at.setDesequencingTask();
//			}
		}
		deSequencingTaskAbnormalDao.saveOrUpdateAll(saveItems);
	}
	public Map<String, Object> findDeSequencingTaskAbnormalList(Integer start,
			Integer length, String query, String col, String sort) throws Exception {
		return deSequencingTaskAbnormalDao.findDeSequencingTaskAbnormalList(start, length, query, col, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void executeAbnormal(String[] ids, String dataJson, String changeLog) throws Exception {
		List<DeSequencingAbnormal> saveItems1 = new ArrayList<DeSequencingAbnormal>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(dataJson,
				List.class);
		DeSequencingAbnormal scp = new DeSequencingAbnormal();
		for (Map<String, Object> map : list) {
			scp = (DeSequencingAbnormal) deSequencingTaskAbnormalDao.Map2Bean(map, scp);
			DeSequencingAbnormal sbi = commonDAO.get(DeSequencingAbnormal.class,
					scp.getId());
			sbi.setNextFlowId(scp.getNextFlowId());
			sbi.setNextFlow(scp.getNextFlow());
			sbi.setIsExecute(scp.getIsExecute());
			sbi.setNote(scp.getNote());
			saveItems1.add(sbi);
		}
		if (changeLog != null && !"".equals(changeLog)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setModifyContent(changeLog);
			commonDAO.saveOrUpdate(li);
		}
		deSequencingTaskAbnormalDao.saveOrUpdateAll(saveItems1);
		List<PlasmaAbnormal> saveItems = new ArrayList<PlasmaAbnormal>();
		for (String id : ids) {
			PlasmaAbnormal sbi = commonDAO.get(PlasmaAbnormal.class, id);
			sbi.setIsExecute("1");
			if (sbi != null) {
				if (sbi.getIsExecute().equals("1")) {// 确认执行
					String next = sbi.getNextFlowId();
					if (next.equals("0009")) {// 样本入库
						SampleInItemTemp st = new SampleInItemTemp();
						st.setCode(sbi.getCode());
						st.setSampleCode(sbi.getSampleCode());
						st.setNum(sbi.getSampleNum());
						st.setState("1");
						deSequencingTaskAbnormalDao.saveOrUpdate(st);
					} else if (next.equals("0012")) {// 暂停
					} else if (next.equals("0013")) {// 终止
					} else if (next.equals("0014")) {// 反馈项目组
					} else {
						// 得到下一步流向的相关表单
						List<NextFlow> list_nextFlow = nextFlowDao
								.seletNextFlowById(next);
						for (NextFlow n : list_nextFlow) {
							Object o = Class.forName(
									n.getApplicationTypeTable().getClassPath())
									.newInstance();
							sbi.setState("1");
							sampleInputService.copy(o, sbi);
						}
					}
					// 改变状态不显示在异常样本中
					sbi.setState("2");
				}
			}
			saveItems.add(sbi);
		}
		deSequencingTaskAbnormalDao.saveOrUpdateAll(saveItems);
	}
}
