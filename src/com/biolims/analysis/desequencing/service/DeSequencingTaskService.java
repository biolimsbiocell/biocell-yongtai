package com.biolims.analysis.desequencing.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.analysis.desequencing.dao.DeSequencingTaskDao;
import com.biolims.analysis.desequencing.model.DeSequencingAbnormal;
import com.biolims.analysis.desequencing.model.DeSequencingTask;
import com.biolims.analysis.desequencing.model.DeSequencingTaskItem;
import com.biolims.analysis.desequencing.model.DeSequencingUpLoadItem;
import com.biolims.analysis.desequencing.model.SampleDeSequencingInfo;
import com.biolims.common.FtpUtil;
import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.experiment.sequencing.dao.SequencingTaskDao;
import com.biolims.experiment.sequencing.model.SequencingTaskInfo;
import com.biolims.experiment.wk.model.WkTaskInfo;
import com.biolims.file.model.FileInfo;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.project.feedback.model.FeedbackDeSequencing;
import com.biolims.report.model.SampleReportTemp;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleInput;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.csvreader.CsvReader;
import com.opensymphony.xwork2.ActionContext;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class DeSequencingTaskService {
	@Resource
	private SequencingTaskDao sequencingTaskDao;
	@Resource
	private DeSequencingTaskDao deSequencingTaskDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private CommonService commonService;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleStateService sampleStateService;
	StringBuffer json = new StringBuffer();
	Map<String, String> locationMap;

	public Map<String, Object> findDeSequencingTaskList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String sampleStyle) {
		return deSequencingTaskDao.selectDeSequencingTaskList(mapForQuery, startNum, limitNum, dir, sort, sampleStyle);
	}

	/**
	 * 查询F号
	 * 
	 * @param model
	 * 
	 * @param i
	 * @throws Exception
	 */
	public Map<String, Object> findDeSequencingTaskByStateList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return deSequencingTaskDao.selectDeSequencingTaskByStateList(mapForQuery, startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(DeSequencingTask i) throws Exception {

		deSequencingTaskDao.saveOrUpdate(i);

	}

	public DeSequencingTask get(String id) {
		DeSequencingTask deSequencingTask = commonDAO.get(DeSequencingTask.class, id);
		return deSequencingTask;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveDeSequencingItem(DeSequencingTask sc, String itemDataJson) throws Exception {
		List<DeSequencingTaskItem> saveItems = new ArrayList<DeSequencingTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			DeSequencingTaskItem scp = new DeSequencingTaskItem();
			// 将map信息读入实体类
			scp = (DeSequencingTaskItem) deSequencingTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setDesequencingTask(sc);

			saveItems.add(scp);

			// if (scp != null && scp.getSubmit() != null
			// && scp.getResult() != null && scp.getSubmit().equals("1")) {// 提交
			// if (scp.getResult().equals("0")) {// 不合格
			// PlasmaAbnormal pa = new PlasmaAbnormal();// 样本异常
			// pa.setCode(scp.getSampleID());
			// pa.setSampleCode(scp.getSampleID());
			// pa.setNote("下机信息分析不合格样本");
			// pa.setState("2");
			// deSequencingTaskDao.saveOrUpdate(pa);
			// }
			// }

		}
		deSequencingTaskDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDeSequencingItem(String[] ids) throws Exception {
		for (String id : ids) {
			DeSequencingTaskItem scp = deSequencingTaskDao.get(DeSequencingTaskItem.class, id);
			deSequencingTaskDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveDeSequencingInfo(DeSequencingTask sc, String itemDataJson) throws Exception {
		List<SampleDeSequencingInfo> saveItems = new ArrayList<SampleDeSequencingInfo>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SampleDeSequencingInfo scp = new SampleDeSequencingInfo();
			// 将map信息读入实体类
			scp = (SampleDeSequencingInfo) deSequencingTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setDesequencingTask(sc);

			saveItems.add(scp);
			if (scp != null && scp.getResult() != null) {
				// if (scp.getOrderType() != null) {// 科技服务任务单：测序
				// if (scp.getOrderType().equals("3")) {
				// scp.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_SEQC_COMPLETE);
				// } else {
				// if (scp.getResult().equals("1")) {
				// // 合格的样本，原始状态改变为“完成下机质控”
				// SampleInfo sf = sampleInfoMainDao
				// .findSampleInfo(scp.getSampleCode());
				// if (sf != null) {
				// sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_SEQC_COMPLETE);
				// sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_SEQC_COMPLETE_NAME);
				// }
				// } else if (scp.getResult().equals("0")) {
				// // 不合格的样本到异常管理
				// FeedbackDeSequencing fds = new FeedbackDeSequencing();
				// fds.setBackDate(new Date());
				// fds.setCode(scp.getPoolingCode());
				// fds.setErrorRate(scp.getErrorRate());
				// fds.setEstimate(scp.getAssess());
				// fds.setIndexs(scp.getIndex());
				// fds.setCode(scp.getPoolingCode());
				// fds.setResult(scp.getResult());
				// fds.setName(scp.getName());
				// fds.setState("3");
				// fds.setId(scp.getIndexLibrary());
				// fds.setWkNum(scp.getIndexLibrary());
				//
				// this.deSequencingTaskDao.saveOrUpdate(fds);
				// }
				// }
				// } else {
				if (scp.getResult().equals("1")) {
					// 合格的样本，原始状态改变为“完成下机质控”
					SampleInfo sf = sampleInfoMainDao.findSampleInfo(scp.getSampleCode());
					if (sf != null) {
						sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_SEQC_COMPLETE);
						sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_SEQC_COMPLETE_NAME);
					}
				} else if (scp.getResult().equals("0")) {
					// 不合格的样本到异常管理
					FeedbackDeSequencing fds = new FeedbackDeSequencing();
					fds.setBackDate(new Date());
					fds.setCode(scp.getPoolingCode());
					fds.setErrorRate(scp.getErrorRate());
					fds.setEstimate(scp.getAssess());
					fds.setIndexs(scp.getIndex());
					fds.setCode(scp.getPoolingCode());
					fds.setResult(scp.getResult());
					fds.setName(scp.getName());
					fds.setState("3");
					fds.setId(scp.getIndexLibrary());
					fds.setWkNum(scp.getIndexLibrary());

					this.deSequencingTaskDao.saveOrUpdate(fds);
					// DeSequencingAbnormal dta = new
					// DeSequencingAbnormal();
					// dta.setCode(scp.getPoolingCode());
					// dta.setBackDate(scp.getBackDate());
					// dta.setErrorRate(scp.getErrorRate());
					// dta.setName(scp.getName());
					// dta.setWkNum(scp.getWkCode());
					// dta.setEstimate(scp.getAssess());
					// dta.setResult(scp.getResult());
					// dta.setBackDate(new Date());
					// deSequencingTaskDao.saveOrUpdate(dta);
				}
			}

			// }
		}
		deSequencingTaskDao.saveOrUpdateAll(saveItems);
		// 计算产量
		Long out = deSequencingTaskDao.getOutPut(sc.getId());
		int output = 0;
		if (out != null && out > 0) {
			output = Integer.valueOf(out.toString());
		}
		Long clus = deSequencingTaskDao.getCluster(sc.getId());
		int cluster = 0;
		if (clus != null && clus > 0) {
			cluster = Integer.valueOf(clus.toString());
		}
		Long pfl = deSequencingTaskDao.getPf(sc.getId());
		int pf = 0;
		if (pfl != null && pfl > 0) {
			pf = Integer.valueOf(pfl.toString());
		}
		sc.setOutPut(output);
		sc.setClusters(cluster);
		sc.setPf(pf);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDeSequencingInfo(String[] ids) throws Exception {
		for (String id : ids) {
			SampleDeSequencingInfo scp = deSequencingTaskDao.get(SampleDeSequencingInfo.class, id);
			deSequencingTaskDao.delete(scp);
		}
	}

	// 审批完成
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String id) throws Exception {
		DeSequencingTask sct = deSequencingTaskDao.get(DeSequencingTask.class, id);
		// 下机质控状态 “完成下机质控”
		sct.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_SEQC_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_SEQC_COMPLETE_NAME);
		deSequencingTaskDao.update(sct);
		// 提交样本
		submit(id, null);
		// // 下机质控状态完成后，信息分析状态为“完成下机质控”
		// AnalysisTask at = new AnalysisTask();
		// at.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_SEQC_COMPLETE);
		// at.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_SEQC_COMPLETE_NAME);

		// // 状态完成后，生成CSV文件
		// List<SampleDeSequencingInfo> list = deSequencingTaskDao.findInfo(sct
		// .getId());
		// // List<SampleDeSequencingInfo> list = (List<SampleDeSequencingInfo>)
		// // this.deSequencingTaskDao.get(SampleDeSequencingInfo.class,
		// // sct.getId());
		// if (list != null && list.size() > 0) {
		// for (SampleDeSequencingInfo sampleDeSequencingInfo : list) {
		// String wkCode = sampleDeSequencingInfo.getIndexLibrary();
		// SampleWkInfo swi = wkSampleTaskDao
		// .getWKSampleInfoByWkCode(wkCode);
		// if (swi != null) {
		// // SampleInfo si =
		// // sampleInfoMainDao.findSampleInfo(swi.getSampleCode());
		// if (swi.getClassify() != null
		// && swi.getClassify().equals("0")) {
		// if (sampleDeSequencingInfo.getResult() != null
		// && sampleDeSequencingInfo.getResult().equals(
		// "1")) {// 查询下机质控为合格的 生成CSV文件
		// createCsv(sct.getId());// 生成CSV文件
		// }
		// }
		// // else{
		// // String orderId = swi.getOrderId();
		// // if(orderId!=null){
		// // TechJkServiceTask tjst =
		// // sampleInfoMainDao.get(TechJkServiceTask.class, orderId);
		// // FiltrateTask filtra = new FiltrateTask();
		// //
		// // }
		// // }
		// }
		// }
		// }

	}

	/**
	 * 提交样本
	 * 
	 * @param id
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submit(String id, String[] ids) throws Exception {
		// 获取主表信息
		DeSequencingTask sc = this.deSequencingTaskDao.get(DeSequencingTask.class, id);
		// 获取结果表样本信息
		List<DeSequencingTaskItem> list;
		if (ids == null)
			list = this.deSequencingTaskDao.setQtTaskResultById(id);
		else
			list = this.deSequencingTaskDao.setQtTaskResultByIds(ids);

		for (DeSequencingTaskItem scp : list) {
			if (scp != null) {
				if (scp.getNextFlow() != null && scp.getResult() != null) {
					String isOk = scp.getResult();
//					String nextFlowId = scp.getNextFlow();
					if (isOk.equals("1")) {
						if (scp.getNextFlowId()!=""&&scp.getNextFlowId().equals("0031")) { // 报告模块
							SampleReportTemp st = new SampleReportTemp();
							List<WkTaskInfo> sil = commonService.get(WkTaskInfo.class, "sampleCode", scp.getSampleCode());
							if (sil.size() > 0) {
								WkTaskInfo scpp = sil.get(0);
								st.setGenotype("");
								st.setCode(scp.getSampleID());
								if(scpp.getSampleInfo()!=null){
									st.setPatientName(scpp.getSampleInfo().getPatientName());
									st.setOrderNum(scpp.getSampleInfo().getOrderNum());
									/*
									 * 赋值订单实体
									 */
									if (null != scpp.getSampleInfo().getOrderNum()
											&& scpp.getSampleInfo().getOrderNum() != "") {
										SampleOrder od = commonDAO.get(SampleOrder.class,
												scpp.getSampleInfo().getOrderNum());
										if (od != null || "".equals(od)) {
											st.setSampleOrder(od);
										}
									}
								}
								st.setSampleCode(scpp.getSampleCode());
								st.setProductId(scpp.getProductId());
								st.setProductName(scpp.getProductName());
								// if (scpp.getInspectDate() != null
								// && !scpp.getInspectDate().equals("")) {
								// st.setInspectDate(DateUtil.parse(scpp
								// .getSampleInfo().getInspectDate()));
								// }
								if (scpp.getReportDate() != null && !scpp.getReportDate().equals("")) {
									st.setReportDate(DateUtil.parse(scpp.getSampleInfo().getReportDate()));
								}
								st.setVolume(scpp.getVolume());
								st.setUnit(scpp.getUnit());
								// st.setIdCard(scpp.getIdCard());
								// st.setSequenceFun(scpp.getSequenceFun());
								// st.setPhone(scpp.getPhone());
								st.setState("1");
								st.setOrderNum(scp.getOrderCode());
								st.setNote(scpp.getNote());
								st.setClassify(scpp.getClassify());
								st.setTestCode("XJZK");
								st.setTestFroms("下机质控");
								deSequencingTaskDao.saveOrUpdate(st);

								DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
								if (scpp.getSampleInfo() != null) {
									sampleStateService.saveSampleState(scpp.getCode(), scpp.getSampleInfo().getCode(),
											scpp.getProductId(), scpp.getProductName(), "",
											format.format(sc.getCreateDate()), format.format(new Date()),
											"DeSequencingTask", "下机质控",
											(User) ServletActionContext.getRequest().getSession()
													.getAttribute(SystemConstants.USER_SESSION_KEY),
											id, "待发送报告订单", scp.getResult(), null, null, null, null, null, null, null,
											null);
								}

							}else{
								st.setCode(scp.getSampleID());
								st.setTestCode("XJZK");
								st.setTestFroms("下机质控");
								deSequencingTaskDao.saveOrUpdate(st);
							}

							//
						} else {
							
						}

					} else {// 不合格
							// 提交的不合格样本到异常管理
						DeSequencingAbnormal eda = new DeSequencingAbnormal();
						scp.setState("2");// 状态为2 在QPCR实验异常中显示
						sampleInputService.copy(eda, scp);
						deSequencingTaskDao.saveOrUpdate(eda);

						List<WkTaskInfo> sil = commonService.get(WkTaskInfo.class, "code", scp.getSampleID());
						if (sil.size() > 0) {
							WkTaskInfo scpp = sil.get(0);
							DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							if (scpp.getSampleInfo() != null) {
								sampleStateService.saveSampleState(scpp.getCode(), scpp.getSampleInfo().getCode(),
										scpp.getProductId(), scpp.getProductName(), "",
										format.format(sc.getCreateDate()), format.format(new Date()),
										"DeSequencingTask", "下机质控",
										(User) ServletActionContext.getRequest().getSession()
												.getAttribute(SystemConstants.USER_SESSION_KEY),
										id, "下机质控异常", scp.getResult(), null, null, null, null, null, null, null, null);
							}
						}
					}

				}
			}
		}
	}

	public List<Map<String, String>> getDeSequencingList(String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = deSequencingTaskDao.setSequencingResultItemList(code);
		List<DeSequencingTaskItem> list = (List<DeSequencingTaskItem>) result.get("list");
		if (list != null && list.size() > 0) {
			for (DeSequencingTaskItem sr : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("sName", sr.getName());
				map.put("code", code);
				map.put("sampleCode", sr.getSampleCode());
				mapList.add(map);
			}
		}
		return mapList;
	}

	/**
	 * 生成下机质控CSV
	 * 
	 * @return
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String createCsv(String taskId) throws Exception {
		Map<String, String> mapForQuery = new HashMap<String, String>();
		mapForQuery.put("task.id", taskId);

		// SequencingTask at =
		// deSequencingTaskDao.get(SequencingTask.class,taskId);
		// Map<String, Object> result =
		// deSequencingTaskDao.selectDeSequencingItemList(taskId, null, null,
		// null, null);
		// List<DeSequencingTaskItem> list = (List<DeSequencingTaskItem>)
		// result.get("list");
		Map<String, Object> resultInfo = deSequencingTaskDao.selectDeSequencingInfoList(taskId, null, null, null, null);
		// 获取下机质控的结果明细
		List<SampleDeSequencingInfo> listInfo = (List<SampleDeSequencingInfo>) resultInfo.get("list");

		if (listInfo == null || listInfo.size() <= 0)
			return null;
		// 获取下机质控信息
		DeSequencingTask dt = deSequencingTaskDao.get(DeSequencingTask.class,
				listInfo.get(0).getDesequencingTask().getId());
		String filePath = SystemConstants.INFO_QC_PATH + "/" + dt.getFlowCode();// 文件路径
		String fileName = filePath + "/info.csv";

		File csvFile = null;
		BufferedWriter csvWtriter = null;
		csvFile = new File(fileName);
		File parent = csvFile.getParentFile();
		if (parent != null && !parent.exists()) {
			parent.mkdirs();
		}
		csvFile.createNewFile();

		// GB2312使正确读取分隔符","
		csvWtriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(csvFile), "GB2312"), 1024);
		// 写入文件头部
		Object[] head = { "文库号", "年龄", "体重", "孕周", "单双胎", "筛查模式", "21-三体比值", "18-三体比值", "补充协议", "需要发报告", "提取方法" };
		List<Object> headList = Arrays.asList(head);
		for (Object data : headList) {
			StringBuffer sb = new StringBuffer();
			String rowStr = sb.append("\"").append(data).append("\",").toString();
			csvWtriter.write(rowStr);
		}
		csvWtriter.newLine();

		for (SampleDeSequencingInfo info : listInfo) {
			StringBuffer sb = new StringBuffer();
			List<SampleInput> list = this.deSequencingTaskDao.findSampleInputBySampleCode(info.getSampleCode());
			for (SampleInput sampleInput : list) {
				sb.append("\"").append(info.getIndexLibrary()).append("\",");
				sb.append("\"").append(sampleInput.getAge()).append("\",");
				sb.append("\"").append(sampleInput.getWeight()).append("\",");
				sb.append("\"").append(sampleInput.getGestationalAge()).append("\",");
				sb.append("\"").append(sampleInput.getEmbryoType()).append("\",");
				sb.append("\"").append(sampleInput.getTestPattern()).append("\",");
				sb.append("\"").append(sampleInput.getTrisome21Value()).append("\",");
				sb.append("\"").append(sampleInput.getTrisome18Value()).append("\",");
				sb.append("\"").append(sampleInput.getSuppleAgreement()).append("\",");
				sb.append("\"").append("").append("\",");// 需要发报告
				sb.append("\"").append(sampleInput.getProductName()).append("\",");
				String rowStr = sb.toString();
				csvWtriter.write(rowStr);
				csvWtriter.newLine();
			}
		}
		csvWtriter.newLine();
		csvWtriter.flush();
		csvWtriter.close();

		InputStream is = new FileInputStream(csvFile.getPath());
		FtpUtil.uploadFile(dt.getFlowCode(), csvFile.getName(), is);

		return null;
	}

	public Long getDeSequencingTaskOutPut(String id) {
		return deSequencingTaskDao.getOutPut(id);
	}

	public Long getDeSequencingTaskCluster(String id) {
		return deSequencingTaskDao.getCluster(id);
	}

	public Long getDeSequencingTaskPf(String id) {
		return deSequencingTaskDao.getPf(id);
	}

	// 根据下机质控id读取数据
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void getCsvContentByid(String id) throws Exception {
		// Map<String, String> mapForQuery = new HashMap<String, String>();
		// mapForQuery.put("task.id", taskId);
		// DeSequencingTask dst = commonDAO.get(DeSequencingTask.class, taskId);

		// List<DeSequencingTask> listDes = this.deSequencingTaskDao
		// .findTaskByState();
		DeSequencingTask dst1 = this.deSequencingTaskDao.findItemInfo(id);
		// for (DeSequencingTask dst1 : listDes) {
		// 此处 FC号已经写死，以便测试。使用时改为dst1.getFlowCode()即可
		// String filePath =
		// com.biolims.system.code.SystemConstants.SEQ_QC_PATH+"\\"+"AQ1122";
		// 读取csv路径 D://qcdate/fc号/in
		String c = "\\in";
		String filePath = com.biolims.system.code.SystemConstants.SEQ_QC_PATH + "\\" + dst1.getFlowCode() + c;
		String path = "";
		int flag = 0;

		// File dir = new File(filePath);
		// if (dir.exists() && dir.isDirectory() && dir.listFiles().length >
		// 0) {
		// File[] fm = dir.listFiles();
		// for (File file : fm) {
		// if(dir.isDirectory()){

		// 如果目录存在 进行读取操作
		path = filePath;
		File a = new File(path + "\\");
		String test[];
		test = a.list();
		// 读取 a 目录下的所有文件名
		for (int i = 0; i < test.length; i++) {
			System.out.println("=======");
			System.out.println(test[i]);

			// if (a.isFile()) {
			InputStream is = new FileInputStream(path + "\\" + test[i]);
			// InputStream is =
			// FtpUtil.downFile(dst1.getFlowCode(),"fulltable.csv");
			if (is != null) {
				CsvReader reader = new CsvReader(is, Charset.forName("GBK"));
				reader.readHeaders();// 去除表头
				while (reader.readRecord()) {// 下机质控明细
					DeSequencingTaskItem item = new DeSequencingTaskItem();
					item.setSampleID(reader.get(reader.getHeader(0)));// 样本号
					item.setSampleType(reader.get(reader.getHeader(1)));// 样本类型
					item.setStat(reader.get(reader.getHeader(2)));// 数据评价
					item.setKit(reader.get(reader.getHeader(3)));// 试剂盒
					item.setLane(reader.get(reader.getHeader(4)));// 泳道
					item.setLength(reader.get(reader.getHeader(5)));// 长度
					item.setGcContent(reader.get(reader.getHeader(6)));// GC含量
					item.setnContent(reader.get(reader.getHeader(7)));// 百万碱基N含量
					item.setqTwentyPCT(reader.get(reader.getHeader(8)));// Q20比例
					item.setqThirtyPCT(reader.get(reader.getHeader(9)));// Q30比例
					item.setRatioOfLane(reader.get(reader.getHeader(10)));// 通量比
					item.setRaw_reads(reader.get(reader.getHeader(11)));// 原始reads数量
					item.setCleanReads(reader.get(reader.getHeader(12)));// QC通过reads数量
					item.setRatioOfReads(reader.get(reader.getHeader(13)));// QC通过reads率
					item.setRaw_size(reader.get(reader.getHeader(14)));// 原始碱基数量
					item.setCleanBases(reader.get(reader.getHeader(15)));// QC通过碱基量
					item.setRatioOfBases(reader.get(reader.getHeader(16)));// QC通过碱率
					item.setMedianInsert(reader.get(reader.getHeader(17)));// 插入长度
					item.setDuplicate(reader.get(reader.getHeader(18)));// 重复率
					item.setOnTarget_raw(reader.get(reader.getHeader(19)));// 核心中靶率
					item.setOnTarget_add(reader.get(reader.getHeader(20)));// 中靶率
					item.setMappingPct(reader.get(reader.getHeader(21)));// 比对率
					item.setOriDepth(reader.get(reader.getHeader(22)));// 测序深度
					item.setOneXcoverage(reader.get(reader.getHeader(23)));// 1倍覆盖率
					item.setTenXcoverage(reader.get(reader.getHeader(24)));// 10倍覆盖率
					item.setTwentyXcoverage(reader.get(reader.getHeader(25)));// 20倍覆盖率
					item.setFiftyXcoverage(reader.get(reader.getHeader(26)));// 50倍覆盖率
					item.setTwentyPctMeancoverage(reader.get(reader.getHeader(27)));// 20%深度覆盖率
					item.setMeanDepthDedup(reader.get(reader.getHeader(28)));// 去重测序深度
					item.setOneCoverageDedup(reader.get(reader.getHeader(29)));// 1倍去重覆盖率
					item.setTenCoverageDedup(reader.get(reader.getHeader(30)));// 10倍去重覆盖率(%)
					item.setTwentyCoverageDedup(reader.get(reader.getHeader(31)));// 20倍去重覆盖率(%)
					item.setFiftyCoverageDedup(reader.get(reader.getHeader(32)));// 50倍去重覆盖率(%)
					item.setTwentyMeanCoverageDedup(reader.get(reader.getHeader(33)));// 20%去重深度覆盖率(%)

					// DeSequencingTask dt = new DeSequencingTask();
					// dt.setId(taskId);
					item.setDesequencingTask(dst1);
					this.deSequencingTaskDao.saveOrUpdate(item);

				}
				reader.close();

				flag++;

			}
			// }

		}

		//
		// File a1 = new File(path + "\\lane.csv");
		// if (a1.isFile()) {
		// InputStream is1 = new FileInputStream(path + "\\lane.csv");
		// // InputStream is1 = FtpUtil.downFile(dst1.getFlowCode(),
		// // dst1.getFlowCode() + ".report_new.csv");
		//
		// if (is1 != null) {
		// CsvReader readerInfo = new CsvReader(is1,
		// Charset.forName("GBK"));
		// readerInfo.readHeaders(); // 去除表头
		// while (readerInfo.readRecord()) { // 下机质控结果
		// SampleDeSequencingInfo item = new SampleDeSequencingInfo();
		//
		// item.setKit(readerInfo.get(readerInfo.getHeader(0)));// 试剂盒
		// item.setLane(readerInfo.get(readerInfo.getHeader(1)));// 泳道
		// item.setPf(readerInfo.get(readerInfo.getHeader(2)));// PF率(%)
		// item.setLength(readerInfo.get(readerInfo.getHeader(3)));// 长度
		// item.setGcContent(readerInfo.get(readerInfo.getHeader(4)));// GC含量
		// item.setnContent(readerInfo.get(readerInfo.getHeader(5)));// 百万碱基N含量
		// item.setqTwentyPCT(readerInfo.get(readerInfo.getHeader(6)));//
		// Q20比例(%)
		// item.setqThirtyPCT(readerInfo.get(readerInfo.getHeader(7)));//
		// Q30比例(%)
		// item.setRaw_reads(readerInfo.get(readerInfo.getHeader(8)));//
		// 原始reads数量
		// item.setCleanReads(readerInfo.get(readerInfo.getHeader(9)));//
		// QC通过reads数量
		// item.setRatioOfReads(readerInfo.get(readerInfo
		// .getHeader(10)));// QC通过reads率
		// item.setRaw_size(readerInfo.get(readerInfo.getHeader(11)));// 原始碱基数量
		// item.setCleanBases(readerInfo.get(readerInfo.getHeader(12)));//
		// QC通过碱基量
		// item.setRatioOfBases(readerInfo.get(readerInfo
		// .getHeader(13)));// QC通过碱率
		// item.setDexReads(readerInfo.get(readerInfo.getHeader(14)));//
		// 解码reads数量
		// item.setRatioOfDex(readerInfo.get(readerInfo.getHeader(15)));//
		// 解码率(%)
		//
		// item.setDesequencingTask(dst1);
		// this.deSequencingTaskDao.saveOrUpdate(item);
		// }
		// readerInfo.close();
		// flag++;
		//
		// }
		// }
		// // //读取完成 改变表的状态为 “完成下机质控”
		// //
		// dst.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_SEQC_COMPLETE);
		// if (flag == 2) {
		// dst1.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_TQ_DATA_NAME);
		// this.deSequencingTaskDao.saveOrUpdate(dst1);
		// }
	}

	// 读取数据
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void getCsvContent() throws Exception {
		// Map<String, String> mapForQuery = new HashMap<String, String>();
		// mapForQuery.put("task.id", taskId);
		// DeSequencingTask dst = commonDAO.get(DeSequencingTask.class, taskId);

		List<DeSequencingTask> listDes = this.deSequencingTaskDao.findTaskByState();
		for (DeSequencingTask dst1 : listDes) {
			// 此处 FC号已经写死，以便测试。使用时改为dst1.getFlowCode()即可
			// String filePath =
			// com.biolims.system.code.SystemConstants.SEQ_QC_PATH+"\\"+"AQ1122";
			String filePath = com.biolims.system.code.SystemConstants.SEQ_QC_PATH + "\\" + dst1.getFlowCode();
			String path = "";
			int flag = 0;

			// File dir = new File(filePath);
			// if (dir.exists() && dir.isDirectory() && dir.listFiles().length >
			// 0) {
			// File[] fm = dir.listFiles();
			// for (File file : fm) {
			// if(dir.isDirectory()){

			// 如果目录存在 进行读取操作
			path = filePath;
			File a = new File(path + "\\sample.csv");
			if (a.isFile()) {
				InputStream is = new FileInputStream(path + "\\sample.csv");
				// InputStream is =
				// FtpUtil.downFile(dst1.getFlowCode(),"fulltable.csv");
				if (is != null) {
					CsvReader reader = new CsvReader(is, Charset.forName("GBK"));
					reader.readHeaders();// 去除表头
					while (reader.readRecord()) {// 下机质控明细
						DeSequencingTaskItem item = new DeSequencingTaskItem();
						item.setSampleID(reader.get(reader.getHeader(0)));// 样本号
						item.setSampleType(reader.get(reader.getHeader(1)));// 样本类型
						item.setStat(reader.get(reader.getHeader(2)));// 数据评价
						item.setKit(reader.get(reader.getHeader(3)));// 试剂盒
						item.setLane(reader.get(reader.getHeader(4)));// 泳道
						item.setLength(reader.get(reader.getHeader(5)));// 长度
						item.setGcContent(reader.get(reader.getHeader(6)));// GC含量
						item.setnContent(reader.get(reader.getHeader(7)));// 百万碱基N含量
						item.setqTwentyPCT(reader.get(reader.getHeader(8)));// Q20比例
						item.setqThirtyPCT(reader.get(reader.getHeader(9)));// Q30比例
						item.setRatioOfLane(reader.get(reader.getHeader(10)));// 通量比
						item.setRaw_reads(reader.get(reader.getHeader(11)));// 原始reads数量
						item.setCleanReads(reader.get(reader.getHeader(12)));// QC通过reads数量
						item.setRatioOfReads(reader.get(reader.getHeader(13)));// QC通过reads率
						item.setRaw_size(reader.get(reader.getHeader(14)));// 原始碱基数量
						item.setCleanBases(reader.get(reader.getHeader(15)));// QC通过碱基量
						item.setRatioOfBases(reader.get(reader.getHeader(16)));// QC通过碱率
						item.setMedianInsert(reader.get(reader.getHeader(17)));// 插入长度
						item.setDuplicate(reader.get(reader.getHeader(18)));// 重复率
						item.setOnTarget_raw(reader.get(reader.getHeader(19)));// 核心中靶率
						item.setOnTarget_add(reader.get(reader.getHeader(20)));// 中靶率
						item.setMappingPct(reader.get(reader.getHeader(21)));// 比对率
						item.setOriDepth(reader.get(reader.getHeader(22)));// 测序深度
						item.setOneXcoverage(reader.get(reader.getHeader(23)));// 1倍覆盖率
						item.setTenXcoverage(reader.get(reader.getHeader(24)));// 10倍覆盖率
						item.setTwentyXcoverage(reader.get(reader.getHeader(25)));// 20倍覆盖率
						item.setFiftyXcoverage(reader.get(reader.getHeader(26)));// 50倍覆盖率
						item.setTwentyPctMeancoverage(reader.get(reader.getHeader(27)));// 20%深度覆盖率
						item.setMeanDepthDedup(reader.get(reader.getHeader(28)));// 去重测序深度
						item.setOneCoverageDedup(reader.get(reader.getHeader(29)));// 1倍去重覆盖率
						item.setTenCoverageDedup(reader.get(reader.getHeader(30)));// 10倍去重覆盖率(%)
						item.setTwentyCoverageDedup(reader.get(reader.getHeader(31)));// 20倍去重覆盖率(%)
						item.setFiftyCoverageDedup(reader.get(reader.getHeader(32)));// 50倍去重覆盖率(%)
						item.setTwentyMeanCoverageDedup(reader.get(reader.getHeader(33)));// 20%去重深度覆盖率(%)

						// DeSequencingTask dt = new DeSequencingTask();
						// dt.setId(taskId);
						item.setDesequencingTask(dst1);
						this.deSequencingTaskDao.saveOrUpdate(item);

					}
					reader.close();

					flag++;

				}
			}

			File a1 = new File(path + "\\lane.csv");
			if (a1.isFile()) {
				InputStream is1 = new FileInputStream(path + "\\lane.csv");
				// InputStream is1 = FtpUtil.downFile(dst1.getFlowCode(),
				// dst1.getFlowCode() + ".report_new.csv");

				if (is1 != null) {
					CsvReader readerInfo = new CsvReader(is1, Charset.forName("GBK"));
					readerInfo.readHeaders(); // 去除表头
					while (readerInfo.readRecord()) { // 下机质控结果
						SampleDeSequencingInfo item = new SampleDeSequencingInfo();

						item.setKit(readerInfo.get(readerInfo.getHeader(0)));// 试剂盒
						item.setLane(readerInfo.get(readerInfo.getHeader(1)));// 泳道
						item.setPf(readerInfo.get(readerInfo.getHeader(2)));// PF率(%)
						item.setLength(readerInfo.get(readerInfo.getHeader(3)));// 长度
						item.setGcContent(readerInfo.get(readerInfo.getHeader(4)));// GC含量
						item.setnContent(readerInfo.get(readerInfo.getHeader(5)));// 百万碱基N含量
						item.setqTwentyPCT(readerInfo.get(readerInfo.getHeader(6)));// Q20比例(%)
						item.setqThirtyPCT(readerInfo.get(readerInfo.getHeader(7)));// Q30比例(%)
						item.setRaw_reads(readerInfo.get(readerInfo.getHeader(8)));// 原始reads数量
						item.setCleanReads(readerInfo.get(readerInfo.getHeader(9)));// QC通过reads数量
						item.setRatioOfReads(readerInfo.get(readerInfo.getHeader(10)));// QC通过reads率
						item.setRaw_size(readerInfo.get(readerInfo.getHeader(11)));// 原始碱基数量
						item.setCleanBases(readerInfo.get(readerInfo.getHeader(12)));// QC通过碱基量
						item.setRatioOfBases(readerInfo.get(readerInfo.getHeader(13)));// QC通过碱率
						item.setDexReads(readerInfo.get(readerInfo.getHeader(14)));// 解码reads数量
						item.setRatioOfDex(readerInfo.get(readerInfo.getHeader(15)));// 解码率(%)

						item.setDesequencingTask(dst1);
						this.deSequencingTaskDao.saveOrUpdate(item);
					}
					readerInfo.close();
					flag++;

				}
			}
			// //读取完成 改变表的状态为 “完成下机质控”
			// dst.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_SEQC_COMPLETE);
			if (flag == 2) {
				dst1.setState("20");
				dst1.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_TQ_DATA_NAME);
				this.deSequencingTaskDao.saveOrUpdate(dst1);
			}
		}
	}

	public void saveUpLoadList(String itemDataJson) throws Exception {
		List<DeSequencingUpLoadItem> saveItems = new ArrayList<DeSequencingUpLoadItem>();
		List<Map> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map map : list) {
			DeSequencingUpLoadItem rti = new DeSequencingUpLoadItem();
			rti = (DeSequencingUpLoadItem) deSequencingTaskDao.Map2Bean(map, rti);
			saveItems.add(rti);
		}
		deSequencingTaskDao.saveOrUpdateAll(saveItems);
	}

	public Map<String, Object> findDeSequencingUpLoadList() {
		return deSequencingTaskDao.selectDeSequencingUpLoadItemList();
	}

	public Map<String, Object> findDeSequencingTaskList(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return deSequencingTaskDao.findDeSequencingTaskList(start, length, query, col, sort);
	}

	public Map<String, Object> findDeSequencingItemList(String scId, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return deSequencingTaskDao.findDeSequencingItemList(scId, start, length, query, col, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String save(String main, String itemJson, User user, String ids, String changeLog, String changeLogItem)
			throws Exception {
		String id = null;
		if (main != null) {
			if (changeLog != null && !"".equals(changeLog)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				li.setUserId(user.getId());
				li.setFileId(ids);
				li.setModifyContent(changeLog);
				commonDAO.saveOrUpdate(li);
			}
			if (changeLogItem != null && !"".equals(changeLogItem)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				li.setUserId(user.getId());
				li.setFileId(ids);
				li.setModifyContent(changeLogItem);
				commonDAO.saveOrUpdate(li);
			}

			String mainJson = "[" + main + "]";
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(mainJson, List.class);
			DeSequencingTask sr = new DeSequencingTask();
			sr = (DeSequencingTask) commonDAO.Map2Bean(list.get(0), sr);
			String name=sr.getName();
			String fcCode=sr.getFlowCode();
			String machine=sr.getMachineCode();
			String ref=sr.getREF();
			if ((sr.getId() != null && "".equals(sr.getId())) || sr.getId().equals("NEW")) {
				String modelName = "DeSequencingTask";
				String markCode = "XJZK";
				String autoID = codingRuleService.genTransID(modelName, markCode);
				id = autoID;
				sr.setId(autoID);
				sr.setAcceptUser(user);
				sr.setCreateDate(new Date());
				User u = (User) ServletActionContext
						.getRequest()
						.getSession()
						.getAttribute(
								SystemConstants.USER_SESSION_KEY);
				sr.setCreateUser(u);
				sr.setState("3");
				sr.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
				sr.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				sr.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
				deSequencingTaskDao.saveOrUpdate(sr);
			} else {
				sr=sequencingTaskDao.get(DeSequencingTask.class, sr.getId());
				sr.setName(name);
				sr.setFlowCode(fcCode);
				sr.setMachineCode(machine);
				sr.setREF(ref);
				deSequencingTaskDao.saveOrUpdate(sr);
				id = sr.getId();
			}
			List<Map<String, Object>> soilist = JsonUtils.toListByJsonArray(itemJson, List.class);
			for (Map<String, Object> map : soilist) {
				DeSequencingTaskItem scp = new DeSequencingTaskItem();
				scp = (DeSequencingTaskItem) commonDAO.Map2Bean(map, scp);
				if (scp.getId() != null && scp.getId().equals(""))
					scp.setId(null);
				scp.setDesequencingTask(sr);
				deSequencingTaskDao.saveOrUpdate(scp);
			}

		}
		return id;
	}

	/**
	 * 
	 * @Title: uploadCsvFile @Description: 上传CSV @author : shengwei.wang @date
	 *         2018年3月29日下午3:36:58 @param id @param fileId @throws Exception
	 *         void @throws
	 */
	/**
	 * @param id
	 * @param fileId
	 * @return
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String uploadCsvFile(String id, String fileId) throws Exception {

		FileInfo fileInfo = deSequencingTaskDao.get(FileInfo.class, fileId);
		String fileName = fileInfo.getFileName();
		fileName = fileName.substring(0, fileName.length() - 4);

		String filepath = fileInfo.getFilePath();
		File a = new File(filepath);
		DeSequencingTask sr = null;
		String mainId = null;
		String message = "noData";
		if (a.isFile()) {
			if (id != null && id.equals("") || id.equals("NEW")) {
				sr = new DeSequencingTask();
				String modelName = "DeSequencingTask";
				String markCode = "XJZK";
				String autoID = codingRuleService.genTransID(modelName, markCode);
				sr.setId(autoID);
				mainId = autoID;
				sr.setCreateDate(new Date());
				User u = (User) ServletActionContext
						.getRequest()
						.getSession()
						.getAttribute(
								SystemConstants.USER_SESSION_KEY);
				sr.setCreateUser(u);
				sr.setState("3");
				sr.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
				sr.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				sr.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			} else {
				sr = sequencingTaskDao.get(DeSequencingTask.class, id);
				mainId = id;
			}
			InputStream is = new FileInputStream(filepath);
			if (is != null) {
				CsvReader reader = new CsvReader(is, Charset.forName("GBK"));
				String sampleCode = null;
				reader.readHeaders();// 去除表头
				while (reader.readRecord()) {
					String code = reader.get(0);
						// 首先判断FC编号
						List<SequencingTaskInfo> list = sequencingTaskDao.findSequencingTaskInfoByFcCode(fileName);
						if (list.size() > 0) {
							sr.setFlowCode(fileName);
							sequencingTaskDao.saveOrUpdate(sr);
							for (SequencingTaskInfo sequencingTaskInfo : list) {
								sampleCode = sequencingTaskInfo.getSampleCode();
								if (code != null && !"".equals(code)) {
									if (sampleCode.indexOf(code) > -1) {
										DeSequencingTaskItem srii = new DeSequencingTaskItem();
										srii.setDesequencingTask(sr);
										srii.setSampleCode(code);
										srii.setName(reader.get(1));
										srii.setOrderCode(reader.get(2));
										srii.setGcContent(reader.get(3));
										srii.setnContent(reader.get(4));
										srii.setqTwentyPCT(reader.get(5));
										srii.setqThirtyPCT(reader.get(6));
										srii.setRatioOfLane(reader.get(7));
										srii.setRaw_reads(reader.get(8));
										srii.setCleanReads(reader.get(9));
										srii.setRatioOfReads(reader.get(10));
										srii.setRaw_size(reader.get(11));
										srii.setCleanBases(reader.get(12));
										srii.setRatioOfBases(reader.get(13));
										srii.setHq_reads(reader.get(14));
										srii.setMedianInsert(reader.get(15));
										deSequencingTaskDao.saveOrUpdate(srii);
										message = "success";
									} else {
										// 不符合要求
										message = "noRequire";
									}
								}
							}
						}else {
							message = "addFcCode";
						}
				}
			}
		}
		return mainId + "," + message;
	}
	/**
	 * @throws Exception 
	 * 
	 * @Title:  DeSequencingTaskService   
	 * @Description:    TODO(读取下机质控文件)
	 * @author: 吴迪
	 * @date:   2018年5月16日 下午2:26:12   
	 * @param:  
	 * @return: void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public JSONObject readCsvFile() throws Exception {
		JSONObject result = new JSONObject();
		String flag ="";
		//添加该路径下的所有文件的文件路径到Map
		List<Map<String, String>> list = getAllFilePath(com.biolims.system.code.SystemConstants.SEQ_QC_PATH);
		outerloop:
		if(list!=null && list.size()>0){
			int i=0;
			for(Map<String,String> map: list){
				String filePath = map.get("path");
				String fileName = map.get("name").substring(0,map.get("name").lastIndexOf("."));
				result.put("fileName", fileName);
				//总数
				result.put("sumNum", list.size());
				//当前操作数
				result.put("nowNum",i);
				if(!"".equals(filePath)){
					File a = new File(filePath);
					DeSequencingTask sr = null;
					if (a.isFile()) {
						sr = new DeSequencingTask();
						String modelName = "DeSequencingTask";
						String markCode = "XJZK";
						String autoID = codingRuleService.genTransID(modelName, markCode);
						sr.setId(autoID);
						sr.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
						sr.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
						InputStream is = new FileInputStream(filePath);
						if (is != null) {
							CsvReader reader = new CsvReader(is, Charset.forName("GBK"));
							reader.readHeaders();// 去除表头
							List<SequencingTaskInfo> seqlist = sequencingTaskDao.findSequencingTaskInfoByFcCode(fileName);
							if (seqlist.size() > 0) {
								//判断表中除去表头是否有数据
								boolean readflag = true;
								//判断是否有编号与上机明细不对应的数据
								boolean isValue = false;
								//统一持久化
								List<DeSequencingTaskItem> desList = new ArrayList<DeSequencingTaskItem>();
								while (reader.readRecord()) {
									readflag = false;
									isValue = false;
									//循环遍历上机明细表 判断明细是否与读取表编号匹配
									for (SequencingTaskInfo sequencingTaskInfo : seqlist) {
										if(sequencingTaskInfo.getCode().equals(reader.get(0))){
											isValue = true;
										}
									}
									//如果没有值直接跳出outerloop到标签位置
									if(!isValue){
										flag = "dataMismatch";
										break outerloop;
									}
									sr.setFlowCode(fileName);
									for (SequencingTaskInfo sequencingTaskInfo : seqlist) {
										DeSequencingTaskItem sri = new DeSequencingTaskItem();
										String sCode = sequencingTaskInfo.getCode();
										sri.setSampleCode(sCode);
										sri.setDesequencingTask(sr);
										sri.setName(reader.get(1));
										sri.setOrderCode(reader.get(2));
										sri.setGcContent(reader.get(3));
										sri.setnContent(reader.get(4));
										sri.setqTwentyPCT(reader.get(5));
										sri.setqThirtyPCT(reader.get(6));
										sri.setRatioOfLane(reader.get(7));
										sri.setRaw_reads(reader.get(8));
										sri.setCleanReads(reader.get(9));
										sri.setRatioOfReads(reader.get(10));
										sri.setRaw_size(reader.get(11));
										sri.setCleanBases(reader.get(12));
										sri.setRatioOfBases(reader.get(13));
										sri.setHq_reads(reader.get(14));
										sri.setMedianInsert(reader.get(15));
										desList.add(sri);
									}
								}
								//如果数据没有问题就统一持久化
								if(isValue){
									sequencingTaskDao.saveOrUpdate(sr);
									deSequencingTaskDao.saveOrUpdateAll(desList);
									flag = "success";
								}
								//表中除去表头没有数据
								if(readflag){
									flag = "noData";
									break;
								}
							}else {
								flag = "addFcCode";
								break;
							}
							//关闭reader
							if(reader!=null){
								reader.close();
							}
						}
						if("success".equals(flag)){
							//移动文件
							Boolean moveflag = moveFile(filePath);
							if(!moveflag){
								flag = "moveError";
								break;
							}
						}
						
					}
				}
				i++;
			}
		}else{
			flag = "noFile";
		}
		result.put("flag", flag);
		return result;
	}
	/**
	 * 
	 * @Title:  DeSequencingTaskService   
	 * @Description:    TODO(获取指定文件夹下的所有文件的文件名)
	 * @author: 吴迪
	 * @date:   2018年5月16日 下午2:33:44   
	 * @param:  @param path 文件夹路径
	 * @return: void
	 * @throws
	 */
	private List<Map<String, String>> getAllFilePath(String path){   
		List<Map<String, String>> list = new ArrayList<Map<String,String>>();
	    // 获得指定文件对象  
	    File file = new File(path);   
	    // 获得该文件夹内的所有文件   
	    File[] array = file.listFiles(); 
	    
	    for(int i=0;i<array.length;i++){   
	    	Map<String, String> map = new HashMap<String, String>();
	    	//如果是文件
	    	if(array[i].isFile()){   
	            // 只输出文件名字  
//	            System.out.println( array[i].getName());   
	            // 输出当前文件的完整路径   
//	            System.out.println("#####" + array[i]);   
//	               	同样输出当前文件的完整路径   大家可以去掉注释 测试一下   
	            System.out.println(array[i].getPath()); 
	            map.put("path", array[i].getPath());
	            map.put("name", array[i].getName());
	            list.add(map);
	        }
	    }  
	    return list;
	}   
	/**
	 * 
	 * @Title:  DeSequencingTaskService   
	 * @Description:    TODO(下机质控移动文件)
	 * @author: 吴迪
	 * @date:   2018年5月16日 下午4:56:44   
	 * @param:  @param filePath
	 * @param:  @return
	 * @return: String
	 * @throws
	 */
	public Boolean moveFile(String filePath){
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Boolean flag=true;
		 try {  
            File afile = new File(filePath);  
            if(afile.isFile()){
            	String out = com.biolims.system.code.SystemConstants.SEQ_QC_OUT_PATH;
            	if(out.length() > 0){
            		String outPath = out + File.separator +format.format(new Date());
            		if (!new File(outPath).exists()) {
            			new File(outPath).mkdirs();
            		}
            		//移动之前一定要检查之前是否使用
            		if (afile.renameTo(new File(outPath +File.separator+ afile.getName()))) {  
            			flag = true;
                    } else {  
                        flag = false;  
                    }  
            		
            	}
            }
        } catch (Exception e) {  
            e.printStackTrace();  
        }
		return flag;  
	}
	public DeSequencingTaskItem getItemById(String id) {
		// TODO Auto-generated method stub
		return deSequencingTaskDao.get(DeSequencingTaskItem.class, id);
	}
}
