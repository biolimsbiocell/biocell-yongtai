package com.biolims.analysis.desequencing.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.analysis.desequencing.dao.DeSequencingItemDao;
import com.biolims.analysis.desequencing.model.DeSequencingTaskItem;
import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class DeSequencingItemService {
	@Resource
	private DeSequencingItemDao deSequencingItemDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findDeSequencingItemList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return deSequencingItemDao.selectDeSequencingItemList(mapForQuery, startNum, limitNum, dir, sort);
	}
	
	public Map<String, Object> findDeSequencingItemNewList(String cid,
			Integer start, Integer length,
			String query, String col, String sort) throws Exception {
		return deSequencingItemDao.selectDeSequencingItemNewList(cid,start, length, query, col, sort);
	}
	
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(DeSequencingTaskItem i) throws Exception {

		deSequencingItemDao.saveOrUpdate(i);

	}
	public DeSequencingTaskItem get(String id) {
		DeSequencingTaskItem deSequencingItem = commonDAO.get(DeSequencingTaskItem.class, id);
		return deSequencingItem;
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(DeSequencingTaskItem sc, Map jsonMap) throws Exception {
		if (sc != null) {
			deSequencingItemDao.saveOrUpdate(sc);
		
			String jsonStr = "";
	}
   }
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<Map<String, String>> getDeSequencingList(String code) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = deSequencingItemDao.setSequencingResultItemList(code);
		List<DeSequencingTaskItem> list = (List<DeSequencingTaskItem>) result.get("list");
		if (list != null && list.size() > 0) {
			for (DeSequencingTaskItem sr : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("sName", sr.getName());
				map.put("code", code);
				map.put("sampleCode", sr.getSampleCode());
				mapList.add(map);
			}
		}
		return mapList;
	}
}
