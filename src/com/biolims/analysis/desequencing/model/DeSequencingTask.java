package com.biolims.analysis.desequencing.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.file.model.FileInfo;
/**   
 * @Title: Model
 * @Description: 下机质控
 * @author lims-platform
 * @date 2015-12-07 09:28:01
 * @version V1.0   
 *
 */
@Entity
@Table(name = "DESEQUENCING_TASK")
@SuppressWarnings("serial")
public class DeSequencingTask extends EntityDao<DeSequencingTask> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**产量*/
	private Integer outPut;
	/**cluster*/
	private Integer clusters;
	/**下达时间*/
	private Date createDate;
	/**操作员*/
	private User acceptUser;
	/**操作时间*/
	private Date acceptDate;
	/**flow cell号*/
	private String flowCode;
	/**工作流id*/
	private String state;
	/**工作流状态*/
	private String stateName;
	/**关联样本号*/
	private String codes;
	//文件
	private FileInfo fileInfo;
	/**PF*/
	private Integer pf;
	/**原因分类*/
	private String reasonType;
	/**详细情况*/
	private String detailed;
	/**描述*/
	private String name;
	/**下达人*/
	private User createUser;
	/**预下机时间*/
	private Date machineTime;
	/**REF*/
	private String REF;
	/**成本中心ID*/
	private String scopeId;
	/**成本中心*/
	private String scopeName;
	/**机器号*/
	private String machineCode;
	/**测试类型*/
	private String testType;
	/**样本类型  1科研2临床*/
	private String sampleStyle;
	
	public String getSampleStyle() {
		return sampleStyle;
	}
	public void setSampleStyle(String sampleStyle) {
		this.sampleStyle = sampleStyle;
	}
	public Integer getClusters() {
		return clusters;
	}
	public void setClusters(Integer clusters) {
		this.clusters = clusters;
	}
	
	public String getReasonType() {
		return reasonType;
	}
	public void setReasonType(String reasonType) {
		this.reasonType = reasonType;
	}

	public Integer getPf() {
		return pf;
	}
	public void setPf(Integer pf) {
		this.pf = pf;
	}
	
	public String getDetailed() {
		return detailed;
	}
	public void setDetailed(String detailed) {
		this.detailed = detailed;
	}
	

	public Date getMachineTime() {
		return machineTime;
	}
	public void setMachineTime(Date machineTime) {
		this.machineTime = machineTime;
	}
	public String getREF() {
		return REF;
	}
	public void setREF(String rEF) {
		REF = rEF;
	}
	public String getMachineCode() {
		return machineCode;
	}
	public void setMachineCode(String machineCode) {
		this.machineCode = machineCode;
	}
	public String getTestType() {
		return testType;
	}
	public void setTestType(String testType) {
		this.testType = testType;
	}
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "FILE_INFO")
	public FileInfo getFileInfo() {
		return fileInfo;
	}
	public void setFileInfo(FileInfo fileInfo) {
		this.fileInfo = fileInfo;
	}
	/**
	 * 
	 *方法: 取得String
	 *@return: String  编号
	 */
	@Id
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得User
	 *@return: User  下达人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser(){
		return this.createUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  下达人
	 */
	public void setCreateUser(User createUser){
		this.createUser = createUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  下达时间
	 */
	@Column(name ="CREATE_DATE", length = 50)
	public Date getCreateDate() {
		return createDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  下达时间
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	
	/**
	 *方法: 取得User
	 *@return: User  操作员
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "ACCEPT_USER")
	public User getAcceptUser(){
		return this.acceptUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  操作员
	 */
	public void setAcceptUser(User acceptUser){
		this.acceptUser = acceptUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  操作时间
	 */
	@Column(name ="ACCEPT_DATE", length = 50)
	public Date getAcceptDate(){
		return this.acceptDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  操作时间
	 */
	public void setAcceptDate(Date acceptDate){
		this.acceptDate = acceptDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  flow cell号
	 */
	@Column(name ="FLOW_CODE", length = 50)
	public String getFlowCode(){
		return this.flowCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  flow cell号
	 */
	public void setFlowCode(String flowCode){
		this.flowCode = flowCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  工作流id
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  工作流id
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  工作流状态
	 */
	@Column(name ="STATE_NAME", length = 50)
	public String getStateName(){
		return this.stateName;
	}
	/**
	 *方法: 设置String
	 *@param: String  工作流状态
	 */
	public void setStateName(String stateName){
		this.stateName = stateName;
	}
	public String getCodes() {
		return codes;
	}
	public void setCodes(String codes) {
		this.codes = codes;
	}
	public Integer getOutPut() {
		return outPut;
	}
	public void setOutPut(Integer outPut) {
		this.outPut = outPut;
	}
	/**
	 * @return the scopeId
	 */
	public String getScopeId() {
		return scopeId;
	}
	/**
	 * @param scopeId the scopeId to set
	 */
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}
	/**
	 * @return the scopeName
	 */
	public String getScopeName() {
		return scopeName;
	}
	/**
	 * @param scopeName the scopeName to set
	 */
	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}
	
}