package com.biolims.analysis.desequencing.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 异常样本
 * @author lims-platform
 * @date 2015-11-30 16:04:51
 * @version V1.0   
 *
 */
@Entity
@Table(name = "DESEQUENCING_ABNORMAL")
@SuppressWarnings("serial")
public class DeSequencingAbnormal extends EntityDao<DeSequencingAbnormal> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
	/**pooling编号*/
	private String code;
	/**文库号*/
	private String wkNum;
	/**测试评估*/
	private String estimate;
	/**反馈时间*/
	private Date backDate;
	/**  */
	private String sampleCode;
	/**错误率*/
	private String errorRate;
	/**结果*/
	private String result;
	/** 下一步流向ID */
	private String nextFlowId;
	/** 下一步流向 */
	private String nextFlow;
	/** 确认执行 */
	private String isExecute;
	/**处理意见*/
	private String reason;
	/**是否提交*/
	private String submit;
	/**状态*/
	private String state;
	/**  */
	private String isRun;
	/**备注*/
	private String note;
	/**处理结果*/
	private String method;
	/**  */
	private String isgood;
	/**  */
	private String advice;
	/**  */
	private String stateName;
	/**  */
	private String indexs;
	
	// RUN_ID
	private String runId;
	/* 检测项目 */
	private String productName;
	/**项目编号*/
	private String projectId;
	/**订单编号*/
	private String orderCode;
	/** 文库编号 */
	private String sampleID;
	/** 云康编号 */
	private String yunKangCode;
	/** 样本类型  1科研2临床 */
	private String sampleStyle;
	
	
	
	public String getSampleStyle() {
		return sampleStyle;
	}
	public void setSampleStyle(String sampleStyle) {
		this.sampleStyle = sampleStyle;
	}
	public String getYunKangCode() {
		return yunKangCode;
	}
	public void setYunKangCode(String yunKangCode) {
		this.yunKangCode = yunKangCode;
	}
	public String getSampleCode() {
		return sampleCode;
	}
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}
	public String getIsRun() {
		return isRun;
	}
	public void setIsRun(String isRun) {
		this.isRun = isRun;
	}
	public String getIsgood() {
		return isgood;
	}
	public void setIsgood(String isgood) {
		this.isgood = isgood;
	}
	public String getAdvice() {
		return advice;
	}
	public void setAdvice(String advice) {
		this.advice = advice;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getIndexs() {
		return indexs;
	}
	public void setIndexs(String indexs) {
		this.indexs = indexs;
	}
	public String getRunId() {
		return runId;
	}
	public void setRunId(String runId) {
		this.runId = runId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public String getOrderCode() {
		return orderCode;
	}
	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}
	public String getSampleID() {
		return sampleID;
	}
	public void setSampleID(String sampleID) {
		this.sampleID = sampleID;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  pooling编号
	 */
	@Column(name ="CODE", length = 50)
	public String getCode(){
		return this.code;
	}
	/**
	 *方法: 设置String
	 *@param: String  pooling编号
	 */
	public void setCode(String code){
		this.code = code;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getSubmit() {
		return submit;
	}
	public void setSubmit(String submit) {
		this.submit = submit;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 300)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	
	public String getWkNum() {
		return wkNum;
	}
	public void setWkNum(String wkNum) {
		this.wkNum = wkNum;
	}
	public String getEstimate() {
		return estimate;
	}
	public void setEstimate(String estimate) {
		this.estimate = estimate;
	}
	@Column(name ="BACK_DATE", length = 50)
	public Date getBackDate() {
		return backDate;
	}
	public void setBackDate(Date backDate) {
		this.backDate = backDate;
	}
	public String getErrorRate() {
		return errorRate;
	}
	public void setErrorRate(String errorRate) {
		this.errorRate = errorRate;
	}
	/**
	 * @return the nextFlowId
	 */
	public String getNextFlowId() {
		return nextFlowId;
	}
	/**
	 * @param nextFlowId the nextFlowId to set
	 */
	public void setNextFlowId(String nextFlowId) {
		this.nextFlowId = nextFlowId;
	}
	/**
	 * @return the nextFlow
	 */
	public String getNextFlow() {
		return nextFlow;
	}
	/**
	 * @param nextFlow the nextFlow to set
	 */
	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}
	/**
	 * @return the isExecute
	 */
	public String getIsExecute() {
		return isExecute;
	}
	/**
	 * @param isExecute the isExecute to set
	 */
	public void setIsExecute(String isExecute) {
		this.isExecute = isExecute;
	}
	
}