package com.biolims.analysis.desequencing.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;

/**   
 * @Title: Model
 * @Description: 结果明细
 * @author lims-platform
 * @date 2015-12-07 09:27:46
 * @version V1.0   
 *
 */
/**
 * @author Administrator结果明细
 * 
 */
@Entity
@Table(name = "SAMPLE_DESEQUENCING_INFO")
@SuppressWarnings("serial")
public class SampleDeSequencingInfo extends EntityDao<SampleDeSequencingInfo>
		implements java.io.Serializable {
	/** 编号 */
	private String id;
	/** 描述 */
	private String name;
	/** 下机质控编号 */
	private String code;
	/** 产量 */
	private Integer outPut;
	/** cluster */
	private Integer clusters;
	// /**PF*/
	// private Integer pf;
	/** 样品量 */
	private String sampleCount;

	/** 原因分类 */
	private String reasonType;
	/** 详细情况 */
	private String detailed;

	/** 样本号 */
	private String sampleCode;

	// //lane
	// private String lane;
	// project
	private String project;
	// INDEX_LIBRARY
	private String indexLibrary;
	// Need(G/M)
	private String need;
	// Base(G)
	private String base;
	// Reads(M)
//	private String reads;
	// PCT
	private String pct;
	// Adapter(%)
	private String adapter;
	// GC(%)
	private String gc;
	// Q20(%)
	private String q20;
	// Q30(%)
	private String q30;
	// index
	private String index;
	// ind
	private String ind;
	// Dup(%)
	private String dup;
	// ployAT
	private String ployAT;
	/** pooling号 */
	private String poolingCode;

	/** 文库号 */
	private String wkCode;
	/** 测试评估 */
	private String assess;
	/** 反馈时间 */
	private Date backDate;
	/** 错误率 */
	private String errorRate;
	/** 结果 */
	private String result;

	/** 处理意见 */
	private String advice;
	/** 处理方式 */
	private String method;
	/** 备注 */
	private String note;
	/** 关联主表 */
	private DeSequencingTask desequencingTask;

	// 合同ID
	private String contractId;
	// 项目ID
	private String projectId;
	/** 任务单类型 */
	private String orderType;
	/** 科技服务任务单 */
	private String techTaskId;
	/** 状态 */
	private String state;
	/**
	 * 下机质控结果明细
	 * 
	 * @return
	 */

	/** 泳道 */
	private String lane2;
	/** 试剂盒 */
	private String kit;
	/** 原始序列数目 */
	private String raw_reads;
	/** 原始序列长度(bp) */
	private String raw_len;
	/** 原始数据GC含量 */
	private String raw_GC;
	/** 原始数据量(MB) */
	private String raw_size;
	/** 高质量序列数据 */
	private String hq_reads;
	/** 高质量序列长度 */
	private String hq_len;
	/** 高质量数据GC含量 */
	private String hq_GC;
	/** 高质量数据量HQ_size(MB) */
	private String hq_size;
	/** 高质量序列百分比HQ_reads_pct */
	private String hq_reads_pct;
	/** 高质量数据百分比HQ_data_pct */
	private String hq_data_pct;
	/** 解码序列数目Demultiplexing_Reads */
	private String demultiplexing_Reads;
	/** 解码比例Demultiplexing_PCT */
	private String demultiplexing_PCT;
	/** Q20比例 */
	private String qTwentyPCT;
	/** Q30比例 */
	private String qThirtyPCT;
	// 泳道
	private String lane;
	// 长度
	private String length;
	// GC含量(%)
	private String gcContent;
	// 百万碱基N含量
	private String nContent;
	// 得率%
	private String pf;
	// QC通过reads数量
	private String cleanReads;
	// QC通过reads率(%)
	private String ratioOfReads;
	// QC通过碱基量
	private String cleanBases;
	// QC通过碱率(%)
	private String ratioOfBases;
	// 解码reads数量
	private String dexReads;
	// 解码率(%)
	private String ratioOfDex;

	@Column(length = 50)
	public String getLength() {
		return length;
	}

	public void setLength(String length) {
		this.length = length;
	}

	@Column(length = 50)
	public String getGcContent() {
		return gcContent;
	}

	public void setGcContent(String gcContent) {
		this.gcContent = gcContent;
	}

	@Column(length = 50)
	public String getnContent() {
		return nContent;
	}

	public void setnContent(String nContent) {
		this.nContent = nContent;
	}

	@Column(length = 50)
	public String getPf() {
		return pf;
	}

	public void setPf(String pf) {
		this.pf = pf;
	}

	@Column(length = 50)
	public String getCleanReads() {
		return cleanReads;
	}

	public void setCleanReads(String cleanReads) {
		this.cleanReads = cleanReads;
	}

	@Column(length = 50)
	public String getRatioOfReads() {
		return ratioOfReads;
	}

	public void setRatioOfReads(String ratioOfReads) {
		this.ratioOfReads = ratioOfReads;
	}

	@Column(length = 50)
	public String getCleanBases() {
		return cleanBases;
	}

	public void setCleanBases(String cleanBases) {
		this.cleanBases = cleanBases;
	}

	@Column(length = 50)
	public String getRatioOfBases() {
		return ratioOfBases;
	}

	public void setRatioOfBases(String ratioOfBases) {
		this.ratioOfBases = ratioOfBases;
	}

	@Column(length = 50)
	public String getDexReads() {
		return dexReads;
	}

	public void setDexReads(String dexReads) {
		this.dexReads = dexReads;
	}

	@Column(length = 50)
	public String getRatioOfDex() {
		return ratioOfDex;
	}

	public void setRatioOfDex(String ratioOfDex) {
		this.ratioOfDex = ratioOfDex;
	}

	@Column(length = 50)
	public Integer getOutPut() {
		return outPut;
	}

	public void setOutPut(Integer outPut) {
		this.outPut = outPut;
	}

	@Column(length = 50)
	public Integer getClusters() {
		return clusters;
	}

	public void setClusters(Integer clusters) {
		this.clusters = clusters;
	}

	// public Integer getPf() {
	// return pf;
	// }
	// public void setPf(Integer pf) {
	// this.pf = pf;
	// }
	@Column(length = 250)
	public String getReasonType() {
		return reasonType;
	}

	public void setReasonType(String reasonType) {
		this.reasonType = reasonType;
	}

	@Column(length = 250)
	public String getDetailed() {
		return detailed;
	}

	public void setDetailed(String detailed) {
		this.detailed = detailed;
	}

	@Column(length = 50)
	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	@Column(length = 50)
	public String getLane() {
		return lane;
	}

	public void setLane(String lane) {
		this.lane = lane;
	}

	@Column(length = 50)
	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	@Column(length = 50)
	public String getIndexLibrary() {
		return indexLibrary;
	}

	public void setIndexLibrary(String indexLibrary) {
		this.indexLibrary = indexLibrary;
	}

	@Column(length = 50)
	public String getNeed() {
		return need;
	}

	public void setNeed(String need) {
		this.need = need;
	}

	@Column(length = 50)
	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

//	@Column(length = 50)
//	public String getReads() {
//		return reads;
//	}
//
//	public void setReads(String reads) {
//		this.reads = reads;
//	}

	@Column(length = 50)
	public String getPct() {
		return pct;
	}

	@Column(length = 50)
	public String getSampleCount() {
		return sampleCount;
	}

	public void setSampleCount(String sampleCount) {
		this.sampleCount = sampleCount;
	}

	public void setPct(String pct) {
		this.pct = pct;
	}

	@Column(length = 50)
	public String getAdapter() {
		return adapter;
	}

	public void setAdapter(String adapter) {
		this.adapter = adapter;
	}

	@Column(length = 50)
	public String getGc() {
		return gc;
	}

	public void setGc(String gc) {
		this.gc = gc;
	}

	@Column(length = 50)
	public String getQ20() {
		return q20;
	}

	public void setQ20(String q20) {
		this.q20 = q20;
	}

	@Column(length = 50)
	public String getQ30() {
		return q30;
	}

	public void setQ30(String q30) {
		this.q30 = q30;
	}

	@Column(name = "INDEXS", length = 50)
	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	@Column(length = 50)
	public String getInd() {
		return ind;
	}

	public void setInd(String ind) {
		this.ind = ind;
	}

	@Column(length = 50)
	public String getDup() {
		return dup;
	}

	public void setDup(String dup) {
		this.dup = dup;
	}

	@Column(length = 50)
	public String getPloyAT() {
		return ployAT;
	}

	public void setPloyAT(String ployAT) {
		this.ployAT = ployAT;
	}

	@Column(length = 50)
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 50)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 下机质控编号
	 */
	@Column(name = "CODE", length = 50)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 下机质控编号
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String pooling号
	 */
	@Column(name = "POOLING_CODE", length = 50)
	public String getPoolingCode() {
		return this.poolingCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String pooling号
	 */
	public void setPoolingCode(String poolingCode) {
		this.poolingCode = poolingCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 文库号
	 */
	@Column(name = "WK_CODE", length = 50)
	public String getWkCode() {
		return this.wkCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 文库号
	 */
	public void setWkCode(String wkCode) {
		this.wkCode = wkCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 测试评估
	 */
	@Column(name = "ASSESS", length = 50)
	public String getAssess() {
		return this.assess;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 测试评估
	 */
	public void setAssess(String assess) {
		this.assess = assess;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: date 反馈时间
	 */
	@Column(name = "BACK_DATE", length = 50)
	public Date getBackDate() {
		return this.backDate;
	}

	/**
	 * 方法: 设置date
	 * 
	 * @param: date 反馈时间
	 */
	public void setBackDate(Date backDate) {
		this.backDate = backDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 错误率
	 */
	@Column(name = "ERROR_RATE", length = 50)
	public String getErrorRate() {
		return this.errorRate;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 错误率
	 */
	public void setErrorRate(String errorRate) {
		this.errorRate = errorRate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 处理意见
	 */
	@Column(name = "ADVICE", length = 50)
	public String getAdvice() {
		return this.advice;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 处理意见
	 */
	public void setAdvice(String advice) {
		this.advice = advice;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 处理方式
	 */
	@Column(name = "METHOD", length = 50)
	public String getMethod() {
		return this.method;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 处理方式
	 */
	public void setMethod(String method) {
		this.method = method;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得DeSequencingTask
	 * 
	 * @return: DeSequencingTask 关联主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "DESEQUENCING_TASK")
	public DeSequencingTask getDesequencingTask() {
		return this.desequencingTask;
	}

	/**
	 * 方法: 设置DeSequencingTask
	 * 
	 * @param: DeSequencingTask 关联主表
	 */
	public void setDesequencingTask(DeSequencingTask desequencingTask) {
		this.desequencingTask = desequencingTask;
	}

	public String getContractId() {
		return contractId;
	}

	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	@Column(length = 50)
	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	@Column(length = 50)
	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	@Column(length = 50)
	public String getTechTaskId() {
		return techTaskId;
	}

	public void setTechTaskId(String techTaskId) {
		this.techTaskId = techTaskId;
	}

	@Column(length = 50)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 
	 * 
	 * 下机质控结果明细
	 */
	@Column(length = 50)
	public String getLane2() {
		return lane2;
	}

	public void setLane2(String lane2) {
		this.lane2 = lane2;
	}

	@Column(length = 50)
	public String getKit() {
		return kit;
	}

	public void setKit(String kit) {
		this.kit = kit;
	}

	@Column(length = 50)
	public String getRaw_reads() {
		return raw_reads;
	}

	public void setRaw_reads(String raw_reads) {
		this.raw_reads = raw_reads;
	}

	@Column(length = 50)
	public String getRaw_len() {
		return raw_len;
	}

	public void setRaw_len(String raw_len) {
		this.raw_len = raw_len;
	}

	@Column(length = 50)
	public String getRaw_GC() {
		return raw_GC;
	}

	public void setRaw_GC(String raw_GC) {
		this.raw_GC = raw_GC;
	}

	@Column(length = 50)
	public String getRaw_size() {
		return raw_size;
	}

	public void setRaw_size(String raw_size) {
		this.raw_size = raw_size;
	}

	@Column(length = 50)
	public String getHq_reads() {
		return hq_reads;
	}

	public void setHq_reads(String hq_reads) {
		this.hq_reads = hq_reads;
	}

	@Column(length = 50)
	public String getHq_len() {
		return hq_len;
	}

	public void setHq_len(String hq_len) {
		this.hq_len = hq_len;
	}

	@Column(length = 50)
	public String Hq_GC() {
		return hq_GC;
	}

	public void setHq_GC(String hq_GC) {
		this.hq_GC = hq_GC;
	}

	@Column(length = 50)
	public String getHq_size() {
		return hq_size;
	}

	public void setHq_size(String hq_size) {
		this.hq_size = hq_size;
	}

	@Column(length = 50)
	public String getHq_reads_pct() {
		return hq_reads_pct;
	}

	public void setHq_reads_pct(String hq_reads_pct) {
		this.hq_reads_pct = hq_reads_pct;
	}

	@Column(length = 50)
	public String getHq_data_pct() {
		return hq_data_pct;
	}

	public void setHq_data_pct(String hq_data_pct) {
		this.hq_data_pct = hq_data_pct;
	}

	@Column(length = 50)
	public String getDemultiplexing_Reads() {
		return demultiplexing_Reads;
	}

	public void setDemultiplexing_Reads(String demultiplexing_Reads) {
		this.demultiplexing_Reads = demultiplexing_Reads;
	}

	@Column(length = 50)
	public String getDemultiplexing_PCT() {
		return demultiplexing_PCT;
	}

	public void setDemultiplexing_PCT(String demultiplexing_PCT) {
		this.demultiplexing_PCT = demultiplexing_PCT;
	}

	@Column(length = 50)
	public String getqTwentyPCT() {
		return qTwentyPCT;
	}

	public void setqTwentyPCT(String qTwentyPCT) {
		this.qTwentyPCT = qTwentyPCT;
	}

	@Column(length = 50)
	public String getqThirtyPCT() {
		return qThirtyPCT;
	}

	public void setqThirtyPCT(String qThirtyPCT) {
		this.qThirtyPCT = qThirtyPCT;
	}

}