package com.biolims.analysis.desequencing.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.file.model.FileInfo;
import com.biolims.storage.position.model.StoragePosition;

/**
 * 文件信息
 *
 */
@Entity
@Table(name = "DESEQUENCING_UPLOAD_ITEM")
public class DeSequencingUpLoadItem extends EntityDao<DeSequencingUpLoadItem> implements Serializable {

	private static final long serialVersionUID = -5234430496355539041L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@Column(name = "FILE_NAME", length = 100)
	private String fileName;//文件名称

	@Column(name = "FILE_TYPE", length = 50)
	private String fileType;//文件类型 word，excel

	@Column(name = "VERSION_NO", length = 32)
	private String versionNo;//版本号
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "ATTACH_FILE_INFO_ID")
	private FileInfo attach;//附件
	
	public FileInfo getAttach() {
		return attach;
	}

	public void setAttach(FileInfo attach) {
		this.attach = attach;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getVersionNo() {
		return versionNo;
	}

	public void setVersionNo(String versionNo) {
		this.versionNo = versionNo;
	}

}
