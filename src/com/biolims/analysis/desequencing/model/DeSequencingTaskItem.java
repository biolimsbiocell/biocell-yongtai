package com.biolims.analysis.desequencing.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 数据明细
 * @author lims-platform
 * @date 2015-12-07 09:27:42
 * @version V1.0
 * 
 */
@Entity
@Table(name = "DESEQUENCING_TASK_ITEM")
@SuppressWarnings("serial")
public class DeSequencingTaskItem extends EntityDao<DeSequencingTaskItem>
		implements java.io.Serializable {
	/** 编号 */
	private String id;
	/** 描述 */
	private String name;
	/** flow cell号 */
	private String flowCode;
	/** laneCode号 */
	private String laneCode;

	/** pooling号 */
	private String poolingCode;
	/** 样本量 */
	private String sampleAmount;
	/** 产量 */
	private String outPut;
	/** cluster */
	private String cluster;
	/** pf */
	private String pf;
	/** Q30 */
	private String q30;

	/** 失败原因 */
	private String reason;
	/** 文库号 */
	private String libraryCode;
	/** 样本号 */
	private String sampleCode;
	/** 状态 */
	private String state;

	/** 备注 */
	private String note;
	/** 关联主表 */
	private DeSequencingTask desequencingTask;

	private String machine;

	private String tiles;

	private String alignRate;

	private String errorRate;

	private String pfCluster;

	private String createDate;

	private String gc;

	private String q20;

	private String phasing;

	private String prePhasing;

	private String desResult;

	private String production;

//	private String length;

	/**
	 * =========================================> 添加下机质控明细字段
	 * 
	 */

	/** 样本编号 */
	private String sampleID;
	/** 试剂盒 */
	private String kit;
	/** 原始序列数目 */
	private String raw_reads;
	/** 原始序列长度(bp) */
	private String raw_len;
	/** 原始数据量 */
	private String raw_size;
	/** 高质量序列数据 */
	private String hq_reads;
	/** 高质量序列长度 */
	private String hq_len;
	/** 高质量数据量HQ_size(MB) */
	private String hq_size;
	/** 高质量序列百分比HQ_reads_pct */
	private String hq_reads_pct;

	/** 原始序列通量比 */
	private String per_lane_percent_raw_reads;
	/** 高质量序列通量比 */
	private String per_lane_percent_HQ_reads;
	/** 片段大小 */
	private String medianInsert;
	/** 重复率 */
	private String duplicate;
	/** 中央中靶率 */
	private String onTarget_raw;

	/** 中靶率 */
	private String onTarget_add;
	/** 原始测序深度 */
	private String oriDepth;
	/** 去重复测序深度 */
	private String dedupDepth;
	/** 高质量数据百分比 */
	private String hqDataPct;
	/** 比对率 */
	private String mappingPct;

	/** Q20比例 */
	private String qTwentyPCT;
	/** Q30比例 */
	private String qThirtyPCT;

	/** 1倍覆盖率 */
	private String oneXcoverage;
	/** 10倍覆盖率 */
	private String tenXcoverage;
	/** 20倍覆盖率 */
	private String twentyXcoverage;
	/** 50倍覆盖率 */
	private String fiftyXcoverage;
	/** 平均深度百分之二十覆盖率 */
	private String twentyPctMeancoverage;
	/** 是否合格 */
	private String result;
	// 是否提交
	private String submit;
	// 下一步流向Id
	private String nextFlowId;
	// 下一步流向
	private String nextFlow;
	// 样本类型
	private String sampleType;
	// 数据评价
	private String stat;	
	// 泳道
	private String lane;
	//长度
	private String length;
	//GC含量(%)
	private String gcContent;
	//百万碱基N含量
	private String nContent;
	//通量比(%)
	private String ratioOfLane;
	//QC通过reads数量
	private String cleanReads;
	//QC通过reads率(%)
	private String ratioOfReads;
	//QC通过碱基量
	private String cleanBases;
	//QC通过碱率(%)
	private String ratioOfBases;
	//去重测序深度
		private String meanDepthDedup;	

		//1倍去重覆盖率(%)
		private String oneCoverageDedup;
		//10倍去重覆盖率(%)
		private String tenCoverageDedup;
		//20倍去重覆盖率(%)
		private String twentyCoverageDedup;
		//50倍去重覆盖率(%)
		private String fiftyCoverageDedup;
		//20%去重深度覆盖率(%)
		private String twentyMeanCoverageDedup;
		
		// RUN_ID
		private String runId;
		/* 检测项目 */
		private String productName;
		/**项目编号*/
		private String projectId;
		/**订单编号*/
		private String orderCode;
		/**Fastq 文件名*/
		private String fastq;
		/**MD5*/
		private String md5;
		/** Read Count*/
		private String  readCount;
		
		public String getRunId() {
			return runId;
		}

		public void setRunId(String runId) {
			this.runId = runId;
		}

		public String getProductName() {
			return productName;
		}

		public void setProductName(String productName) {
			this.productName = productName;
		}

		public String getProjectId() {
			return projectId;
		}

		public void setProjectId(String projectId) {
			this.projectId = projectId;
		}

		public String getOrderCode() {
			return orderCode;
		}

		public void setOrderCode(String orderCode) {
			this.orderCode = orderCode;
		}

		public String getSampleType() {
			return sampleType;
		}

		public void setSampleType(String sampleType) {
			this.sampleType = sampleType;
		}

		public String getStat() {
			return stat;
		}

		public void setStat(String stat) {
			this.stat = stat;
		}

		public String getLane() {
			return lane;
		}

		public void setLane(String lane) {
			this.lane = lane;
		}

		public String getGcContent() {
			return gcContent;
		}

		public void setGcContent(String gcContent) {
			this.gcContent = gcContent;
		}

		public String getnContent() {
			return nContent;
		}

		public void setnContent(String nContent) {
			this.nContent = nContent;
		}

		public String getRatioOfLane() {
			return ratioOfLane;
		}

		public void setRatioOfLane(String ratioOfLane) {
			this.ratioOfLane = ratioOfLane;
		}

		public String getCleanReads() {
			return cleanReads;
		}

		public void setCleanReads(String cleanReads) {
			this.cleanReads = cleanReads;
		}

		public String getRatioOfReads() {
			return ratioOfReads;
		}

		public void setRatioOfReads(String ratioOfReads) {
			this.ratioOfReads = ratioOfReads;
		}

		public String getCleanBases() {
			return cleanBases;
		}

		public void setCleanBases(String cleanBases) {
			this.cleanBases = cleanBases;
		}

		public String getRatioOfBases() {
			return ratioOfBases;
		}

		public void setRatioOfBases(String ratioOfBases) {
			this.ratioOfBases = ratioOfBases;
		}

		public String getMeanDepthDedup() {
			return meanDepthDedup;
		}

		public void setMeanDepthDedup(String meanDepthDedup) {
			this.meanDepthDedup = meanDepthDedup;
		}

		public String getOneCoverageDedup() {
			return oneCoverageDedup;
		}

		public void setOneCoverageDedup(String oneCoverageDedup) {
			this.oneCoverageDedup = oneCoverageDedup;
		}

		public String getTenCoverageDedup() {
			return tenCoverageDedup;
		}

		public void setTenCoverageDedup(String tenCoverageDedup) {
			this.tenCoverageDedup = tenCoverageDedup;
		}

		public String getTwentyCoverageDedup() {
			return twentyCoverageDedup;
		}

		public void setTwentyCoverageDedup(String twentyCoverageDedup) {
			this.twentyCoverageDedup = twentyCoverageDedup;
		}

		public String getFiftyCoverageDedup() {
			return fiftyCoverageDedup;
		}

		public void setFiftyCoverageDedup(String fiftyCoverageDedup) {
			this.fiftyCoverageDedup = fiftyCoverageDedup;
		}

		public String getTwentyMeanCoverageDedup() {
			return twentyMeanCoverageDedup;
		}

		public void setTwentyMeanCoverageDedup(String twentyMeanCoverageDedup) {
			this.twentyMeanCoverageDedup = twentyMeanCoverageDedup;
		}

	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

	public String getNextFlow() {
		return nextFlow;
	}

	public void setNextFlow(String nextFlow) {
		this.nextFlow = nextFlow;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	public String getLaneCode() {
		return laneCode;
	}

	public void setLaneCode(String laneCode) {
		this.laneCode = laneCode;
	}

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 50)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String flow cell号
	 */
	@Column(name = "FLOW_CODE", length = 50)
	public String getFlowCode() {
		return this.flowCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String flow cell号
	 */
	public void setFlowCode(String flowCode) {
		this.flowCode = flowCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String pooling号
	 */
	@Column(name = "POOLING_CODE", length = 50)
	public String getPoolingCode() {
		return this.poolingCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String pooling号
	 */
	public void setPoolingCode(String poolingCode) {
		this.poolingCode = poolingCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 文库号
	 */
	@Column(name = "LIBRARY_CODE", length = 50)
	public String getLibraryCode() {
		return this.libraryCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 文库号
	 */
	public void setLibraryCode(String libraryCode) {
		this.libraryCode = libraryCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 样本号
	 */
	@Column(name = "SAMPLE_CODE", length = 100)
	public String getSampleCode() {
		return this.sampleCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 样本号
	 */
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 50)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得DeSequencingTask
	 * 
	 * @return: DeSequencingTask 关联主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DESEQUENCING_TASK")
	public DeSequencingTask getDesequencingTask() {
		return this.desequencingTask;
	}

	/**
	 * 方法: 设置DeSequencingTask
	 * 
	 * @param: DeSequencingTask 关联主表
	 */
	public void setDesequencingTask(DeSequencingTask desequencingTask) {
		this.desequencingTask = desequencingTask;
	}

	public String getSampleAmount() {
		return sampleAmount;
	}

	public void setSampleAmount(String sampleAmount) {
		this.sampleAmount = sampleAmount;
	}

	public String getOutPut() {
		return outPut;
	}

	public void setOutPut(String outPut) {
		this.outPut = outPut;
	}

	@Column(name = "CLUSTER_STR", length = 50)
	public String getCluster() {
		return cluster;
	}

	public void setCluster(String cluster) {
		this.cluster = cluster;
	}

	public String getPf() {
		return pf;
	}

	public void setPf(String pf) {
		this.pf = pf;
	}

	public String getQ30() {
		return q30;
	}

	public void setQ30(String q30) {
		this.q30 = q30;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getPfCluster() {
		return pfCluster;
	}

	public void setPfCluster(String pfCluster) {
		this.pfCluster = pfCluster;
	}

	public String getAlignRate() {
		return alignRate;
	}

	public void setAlignRate(String alignRate) {
		this.alignRate = alignRate;
	}

	public String getErrorRate() {
		return errorRate;
	}

	public void setErrorRate(String errorRate) {
		this.errorRate = errorRate;
	}

	public String getGc() {
		return gc;
	}

	public void setGc(String gc) {
		this.gc = gc;
	}

	public String getQ20() {
		return q20;
	}

	public void setQ20(String q20) {
		this.q20 = q20;
	}

	public String getPhasing() {
		return phasing;
	}

	public void setPhasing(String phasing) {
		this.phasing = phasing;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getMachine() {
		return machine;
	}

	public void setMachine(String machine) {
		this.machine = machine;
	}

	public String getTiles() {
		return tiles;
	}

	public void setTiles(String tiles) {
		this.tiles = tiles;
	}

	public String getDesResult() {
		return desResult;
	}

	public void setDesResult(String desResult) {
		this.desResult = desResult;
	}

	public String getProduction() {
		return production;
	}

	public void setProduction(String production) {
		this.production = production;
	}

	public String getLength() {
		return length;
	}

	public void setLength(String length) {
		this.length = length;
	}

	public String getPrePhasing() {
		return prePhasing;
	}

	public void setPrePhasing(String prePhasing) {
		this.prePhasing = prePhasing;
	}

	/**
	 * ==========================================> 添加下机质控明细字段
	 * 
	 */
	public String getSampleID() {
		return sampleID;
	}

	public void setSampleID(String sampleID) {
		this.sampleID = sampleID;
	}

	public String getKit() {
		return kit;
	}

	public void setKit(String kit) {
		this.kit = kit;
	}

	public String getRaw_reads() {
		return raw_reads;
	}

	public void setRaw_reads(String raw_reads) {
		this.raw_reads = raw_reads;
	}

	public String getRaw_len() {
		return raw_len;
	}

	public void setRaw_len(String raw_len) {
		this.raw_len = raw_len;
	}

	public String getRaw_size() {
		return raw_size;
	}

	public void setRaw_size(String raw_size) {
		this.raw_size = raw_size;
	}

	public String getHq_reads() {
		return hq_reads;
	}

	public void setHq_reads(String hq_reads) {
		this.hq_reads = hq_reads;
	}

	public String getHq_len() {
		return hq_len;
	}

	public void setHq_len(String hq_len) {
		this.hq_len = hq_len;
	}

	public String getHq_size() {
		return hq_size;
	}

	public void setHq_size(String hq_size) {
		this.hq_size = hq_size;
	}

	public String getHq_reads_pct() {
		return hq_reads_pct;
	}

	public void setHq_reads_pct(String hq_reads_pct) {
		this.hq_reads_pct = hq_reads_pct;
	}

	public String getPer_lane_percent_raw_reads() {
		return per_lane_percent_raw_reads;
	}

	public void setPer_lane_percent_raw_reads(String per_lane_percent_raw_reads) {
		this.per_lane_percent_raw_reads = per_lane_percent_raw_reads;
	}

	public String getPer_lane_percent_HQ_reads() {
		return per_lane_percent_HQ_reads;
	}

	public void setPer_lane_percent_HQ_reads(String per_lane_percent_HQ_reads) {
		this.per_lane_percent_HQ_reads = per_lane_percent_HQ_reads;
	}

	public String getMedianInsert() {
		return medianInsert;
	}

	public void setMedianInsert(String medianInsert) {
		this.medianInsert = medianInsert;
	}

	public String getDuplicate() {
		return duplicate;
	}

	public void setDuplicate(String duplicate) {
		this.duplicate = duplicate;
	}

	public String getOnTarget_raw() {
		return onTarget_raw;
	}

	public void setOnTarget_raw(String onTarget_raw) {
		this.onTarget_raw = onTarget_raw;
	}

	public String getOnTarget_add() {
		return onTarget_add;
	}

	public void setOnTarget_add(String onTarget_add) {
		this.onTarget_add = onTarget_add;
	}

	public String getOriDepth() {
		return oriDepth;
	}

	public void setOriDepth(String oriDepth) {
		this.oriDepth = oriDepth;
	}

	public String getDedupDepth() {
		return dedupDepth;
	}

	public void setDedupDepth(String dedupDepth) {
		this.dedupDepth = dedupDepth;
	}

	public String getHqDataPct() {
		return hqDataPct;
	}

	public void setHqDataPct(String hqDataPct) {
		this.hqDataPct = hqDataPct;
	}

	public String getMappingPct() {
		return mappingPct;
	}

	public void setMappingPct(String mappingPct) {
		this.mappingPct = mappingPct;
	}

	public String getqTwentyPCT() {
		return qTwentyPCT;
	}

	public void setqTwentyPCT(String qTwentyPCT) {
		this.qTwentyPCT = qTwentyPCT;
	}

	public String getqThirtyPCT() {
		return qThirtyPCT;
	}

	public void setqThirtyPCT(String qThirtyPCT) {
		this.qThirtyPCT = qThirtyPCT;
	}

	public String getOneXcoverage() {
		return oneXcoverage;
	}

	public void setOneXcoverage(String oneXcoverage) {
		this.oneXcoverage = oneXcoverage;
	}

	public String getTenXcoverage() {
		return tenXcoverage;
	}

	public void setTenXcoverage(String tenXcoverage) {
		this.tenXcoverage = tenXcoverage;
	}

	public String getTwentyXcoverage() {
		return twentyXcoverage;
	}

	public void setTwentyXcoverage(String twentyXcoverage) {
		this.twentyXcoverage = twentyXcoverage;
	}

	public String getFiftyXcoverage() {
		return fiftyXcoverage;
	}

	public void setFiftyXcoverage(String fiftyXcoverage) {
		this.fiftyXcoverage = fiftyXcoverage;
	}

	public String getTwentyPctMeancoverage() {
		return twentyPctMeancoverage;
	}

	public void setTwentyPctMeancoverage(String twentyPctMeancoverage) {
		this.twentyPctMeancoverage = twentyPctMeancoverage;
	}

	/**
	 * @return the nextFlowId
	 */
	public String getNextFlowId() {
		return nextFlowId;
	}

	/**
	 * @param nextFlowId the nextFlowId to set
	 */
	public void setNextFlowId(String nextFlowId) {
		this.nextFlowId = nextFlowId;
	}

	/**
	 * @return the fastq
	 */
	public String getFastq() {
		return fastq;
	}

	/**
	 * @param fastq the fastq to set
	 */
	public void setFastq(String fastq) {
		this.fastq = fastq;
	}

	/**
	 * @return the md5
	 */
	public String getMd5() {
		return md5;
	}

	/**
	 * @param md5 the md5 to set
	 */
	public void setMd5(String md5) {
		this.md5 = md5;
	}

	/**
	 * @return the readCount
	 */
	public String getReadCount() {
		return readCount;
	}

	/**
	 * @param readCount the readCount to set
	 */
	public void setReadCount(String readCount) {
		this.readCount = readCount;
	}

}