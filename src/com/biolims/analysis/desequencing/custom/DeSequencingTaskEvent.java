package com.biolims.analysis.desequencing.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.analysis.desequencing.service.DeSequencingTaskService;
import com.biolims.common.interfaces.ObjectEvent;

public class DeSequencingTaskEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		DeSequencingTaskService mbService = (DeSequencingTaskService) ctx.getBean("deSequencingTaskService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
