package com.biolims.analysis.desequencing.dao;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.analysis.data.model.DataTaskTemp;
import com.biolims.analysis.desequencing.model.DeSequencingTask;
import com.biolims.analysis.desequencing.model.DeSequencingTaskItem;
import com.biolims.analysis.desequencing.model.DeSequencingUpLoadItem;
import com.biolims.analysis.desequencing.model.SampleDeSequencingInfo;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.dna.model.DnaTaskInfo;
import com.biolims.experiment.wk.model.WkTaskInfo;
import com.biolims.sample.model.SampleInput;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class DeSequencingTaskDao extends BaseHibernateDao {
	//根据样本类型查询  下机
	public Map<String, Object> selectDeSequencingTaskList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String sampleStyle) {
		String key = " ";
		String hql = " from DeSequencingTask where 1=1 ";
		if (!"".equals(sampleStyle) && sampleStyle != null) {
			key += " and sampleStyle='" + sampleStyle + "'";
		}
		if (mapForQuery != null)
			key = key +map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<DeSequencingTask> list = new ArrayList<DeSequencingTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	/**
	 * 查询F号
	 * @param scId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectDeSequencingTaskByStateList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir ,String sort) {
		String key = " ";
		String hql = " from DeSequencingTask where 1=1 and stateName ='完成下机质控' ";
		
		
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<DeSequencingTask> list = new ArrayList<DeSequencingTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		for(int i=0;i<list.size();i++){
			String hql1 = "from DataTaskTemp where 1=1 and FC= '"+list.get(i).getFlowCode()+"'";
			List<DataTaskTemp> dataTaskTemps = new ArrayList<DataTaskTemp>();
			dataTaskTemps = this.getSession().createQuery(hql1).list();
			if(dataTaskTemps.size()>0){
				int state = 0;
				for(int j=0;j<dataTaskTemps.size();j++){
					if("0".equals(dataTaskTemps.get(j).getState())){
						state = 1;
					}
				}
				if(state == 0){
					list.remove(i);
				}
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	
		public Map<String, Object> selectDeSequencingInfoList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from SampleDeSequencingInfo where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and desequencingTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleDeSequencingInfo> list = new ArrayList<SampleDeSequencingInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> setSequencingResultItemList(String code) {
		String hql = "from DeSequencingTaskItem t where 1=1 ";
		String key = "";
		if (code != null)
			key = key + " and  t.desequencingTask='" + code + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<DeSequencingTaskItem> list = this.getSession().createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public List<SampleDeSequencingInfo> getdesInfoNotGood(String id) {
			String hql = "from SampleDeSequencingInfo t where 1=1 and t.desequencingTask='"+id+"'";
			
			List<SampleDeSequencingInfo> list = this.getSession().createQuery(hql).list();

			return list;
		}
		public DeSequencingTask getdsk(String id) {
			String hql = "from DeSequencingTask t where 1=1 and t.id='"+id+"'";
			DeSequencingTask dsk=(DeSequencingTask) this.getSession().createQuery(hql).uniqueResult();
			return dsk;
		}
		public Long getOutPut(String id) {
			String hql = "from SampleDeSequencingInfo t where 1=1 and t.desequencingTask.id='"+id+"'";
			Long total = (Long) this.getSession().createQuery("select sum(t.outPut) " + hql).uniqueResult();
			return total;
		}
		public Long getCluster(String id) {
			String hql = "from SampleDeSequencingInfo t where 1=1 and t.desequencingTask.id='"+id+"'";
			Long total = (Long) this.getSession().createQuery("select sum(t.clusters) " + hql).uniqueResult();
			return total;
		}
		public Long getPf(String id) {
			String hql = "from SampleDeSequencingInfo t where 1=1 and t.desequencingTask.id='"+id+"'";
			Long total = (Long) this.getSession().createQuery("select sum(t.pf) " + hql).uniqueResult();
			return total;
		}
		
//		根据状态获取下机质控集合
		public List<DeSequencingTask> findTaskByState(){
			String hql = "from DeSequencingTask t where t.stateName=  '新建'";
			List<DeSequencingTask> list = this.getSession().createQuery(hql).list();
			return list;
		}
		
		//根据样本编号查询sampleInput表
		public List<SampleInput> findSampleInputBySampleCode(String sampleCode){
			String hql = "from SampleInput t where t.code = '"+sampleCode+"'";
			List<SampleInput> list = this.getSession().createQuery(hql).list();
			return list;
		}
		
		public List<SampleDeSequencingInfo> findInfo(String sampleCode){
			String hql = "from SampleDeSequencingInfo t where t.desequencingTask.id = '"+sampleCode+"'";
			List<SampleDeSequencingInfo> list = this.getSession().createQuery(hql).list();
			return list;
		}
		
		public List<WkTaskInfo> findWkInfo(String sampleCode){
			String hql = "from WkTaskInfo t where t.wkCode = '"+sampleCode+"'";
			List<WkTaskInfo> list = this.getSession().createQuery(hql).list();
			return list;
		}
		
		// 根据主表id和状态获取下机质控数据
		public DeSequencingTask findItemInfo(String id) {
			String hql = "from DeSequencingTask t where t.stateName=  '新建' and t.id='"
					+ id + "'";
			DeSequencingTask list = (DeSequencingTask) this.getSession()
					.createQuery(hql).uniqueResult();
			return list;
		}
		
		public Map<String, Object> selectDeSequencingUpLoadItemList() {
			String hql = "from DeSequencingUpLoadItem where 1=1 order by id desc";
			
			Long total = (Long) this.getSession().createQuery(" select count(*) from DocumentInfoItem where 1=1 order by id desc").uniqueResult();
			List<DeSequencingUpLoadItem> list = new ArrayList<DeSequencingUpLoadItem>();

			list = this.getSession().createQuery(hql).list();
			
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("total", total);
			result.put("list", list);
			return result;
		}
		/**
		 * 根据样本编号查询
		 * 
		 * @param code
		 * @return
		 */
		public List<DeSequencingTaskItem> setQtTaskResultById(String code) {
			String hql = "from DeSequencingTaskItem t where 1=1 and desequencingTask.id='"
					+ code + "'";
			List<DeSequencingTaskItem> list = this.getSession().createQuery(hql).list();
			return list;
		}

		/**
		 * 根据样本编号查询
		 * 
		 * @param codes
		 * @return
		 */
		public List<DeSequencingTaskItem> setQtTaskResultByIds(String[] codes) {

			String insql = "''";
			for (String code : codes) {

				insql += ",'" + code + "'";

			}

			String hql = "from DeSequencingTaskItem t where 1=1 and id in ("
					+ insql + ")";
			List<DeSequencingTaskItem> list = this.getSession().createQuery(hql).list();
			return list;
		}
		public Map<String, Object> findDeSequencingTaskList(Integer start,
				Integer length, String query, String col, String sort) throws Exception {

			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from DeSequencingTask where 1=1";
			String key = "";
			if (query != null) {
				key = map2Where(query);
			}
			String scopeId = (String) ActionContext.getContext().getSession()
					.get("scopeId");
			if (!"all".equals(scopeId)) {
				key += " and scopeId='" + scopeId + "'";
			}

			Long sumCount = (Long) getSession().createQuery(countHql+key)
					.uniqueResult();
			if (0l != sumCount) {
				Long filterCount = (Long) getSession().createQuery(countHql + key)
						.uniqueResult();
				String hql = "from DeSequencingTask where 1=1";
				if (col != null && !"".equals(col) && !"".equals(sort)
						&& sort != null) {
					col = col.replace("-", ".");
					key += " order by " + col + " " + sort;
				}
				List<DeSequencingTask> list = getSession().createQuery(hql + key)
						.setFirstResult(start).setMaxResults(length).list();
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
			}
			return map;
		}
		public Map<String, Object> findDeSequencingItemList(String scId,
				Integer start, Integer length, String query, String col,
				String sort) throws Exception {
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from DeSequencingTaskItem where 1=1 and desequencingTask.id='"
					+ scId + "'";
			String key = "";
			if (query != null) {
				key = map2Where(query);
			}
			Long sumCount = (Long) getSession().createQuery(countHql)
					.uniqueResult();
			if (0l != sumCount) {
				Long filterCount = (Long) getSession().createQuery(countHql + key)
						.uniqueResult();
				String hql = "from DeSequencingTaskItem  where 1=1 and desequencingTask.id='"
						+ scId + "'";
				if (col != null && !"".equals(col) && !"".equals(sort)
						&& sort != null) {
					col = col.replace("-", ".");
					key += " order by " + col + " " + sort;
				}
				List<DeSequencingTaskItem> list = getSession().createQuery(hql + key)
						.setFirstResult(start).setMaxResults(length).list();
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
			}
			return map;
		}
		public List<DeSequencingTaskItem> findDeSequencingTaskItemByCode(String code) {
			String hql = "from DeSequencingTaskItem where 1=1 and sample_code='" + code + "'";
			List<DeSequencingTaskItem> list = new ArrayList<DeSequencingTaskItem>();
			list = getSession().createQuery(hql).list();
			return list;
		}
}