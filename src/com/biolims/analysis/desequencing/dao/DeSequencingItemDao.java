package com.biolims.analysis.desequencing.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.analysis.desequencing.model.DeSequencingTaskItem;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.sample.storage.model.SampleInfoIn;

@Repository
@SuppressWarnings("unchecked")
public class DeSequencingItemDao extends BaseHibernateDao {
	public Map<String, Object> selectDeSequencingItemList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from DeSequencingTaskItem where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<DeSequencingTaskItem> list = new ArrayList<DeSequencingTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	
	public Map<String, Object> selectDeSequencingItemNewList(String cid, Integer start,Integer length, 
			String query, String col, String sort) throws Exception {
		String countHql = "";
		
		if ("".equals(cid) || cid==null) {
			countHql = "from DeSequencingTaskItem where 1=1 ";
		} 
		if(!"".equals(cid) && cid!=null){
			countHql = "from DeSequencingTaskItem where 1=1 and projectId='"+cid+"' ";
		}
		
		
		Map<String, Object> map = new HashMap<String, Object>();
		String key = "";
		if(query!=null&&!"".equals(query)){
			key = key +map2Where(query);
		}
		
//		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
//		if(!"all".equals(scopeId)){
//			key+=" and scopeId='"+scopeId+"'";
//		}
		Long sumCount = (Long) getSession().createQuery("select count(*) "+countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery("select count(*) "+countHql+key).uniqueResult();
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<DeSequencingTaskItem> list = getSession().createQuery(countHql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	public Map<String, Object> setSequencingResultItemList(String code) {
		String hql = "from DeSequencingTaskItem t where 1=1 ";
		String key = "";
		if (code != null)
			key = key + " and  t.desequencingTask='" + code + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<DeSequencingTaskItem> list = this.getSession().createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
}