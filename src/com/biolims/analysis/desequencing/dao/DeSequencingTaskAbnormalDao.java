package com.biolims.analysis.desequencing.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.analysis.desequencing.model.DeSequencingAbnormal;
import com.biolims.analysis.desequencing.model.DeSequencingTask;
import com.biolims.dao.BaseHibernateDao;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class DeSequencingTaskAbnormalDao extends BaseHibernateDao {
	public Map<String, Object> selectDeSequencingTaskAbnormalList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from DeSequencingAbnormal where 1=1 and (state = '2' or state = '3') ";
		if (mapForQuery.size() > 0){
			key = map2where(mapForQuery);
		} else {
			key = " and (isRun ='0' or isRun is null)";
		}
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<DeSequencingAbnormal> list = new ArrayList<DeSequencingAbnormal>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	
	public Map<String, Object> selectDeSequencingTaskAbnormalList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String sampleStyle) {
		String key = " ";
		String hql = " from DeSequencingAbnormal where 1=1 and (state = '2' or state = '3') ";
//		if(!"".equals(sampleStyle)){
//			key += " and sampleStyle='" + sampleStyle + "'";
//		}else{
//			key += " and (sampleStyle='2' or sampleStyle is null)";
//		}
		if (!"".equals(sampleStyle) && sampleStyle!=null) {
			key += " and sampleStyle='" + sampleStyle + "'";
		} else {
			key += " and (sampleStyle='2' or sampleStyle is null)";
		}
		if (mapForQuery.size() > 0){
			key =key+ map2where(mapForQuery);
		} else {
			key += " and (isRun ='0' or isRun is null)";
		}
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<DeSequencingAbnormal> list = new ArrayList<DeSequencingAbnormal>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> findDeSequencingTaskAbnormalList(Integer start,
			Integer length, String query, String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from DeSequencingAbnormal where 1=1";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession()
				.get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		Long sumCount = (Long) getSession().createQuery(countHql+key)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from DeSequencingAbnormal where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<DeSequencingAbnormal> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
}