﻿package com.biolims.analysis.desequencing.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.analysis.desequencing.model.DeSequencingTask;
import com.biolims.analysis.desequencing.model.DeSequencingTaskItem;
import com.biolims.analysis.desequencing.model.DeSequencingUpLoadItem;
import com.biolims.analysis.desequencing.service.DeSequencingTaskService;
import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/analysis/desequencing/deSequencingTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class DeSequencingTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";// 260201
	@Autowired
	private DeSequencingTaskService deSequencingTaskService;
	private DeSequencingTask deSequencingTask = new DeSequencingTask();
	@Resource
	private FileInfoService fileInfoService;

	/**
	 * 
	 * @Title: showDeSequencingTaskList
	 * @Description: 展示列表
	 * @author : shengwei.wang
	 * @date 2018年3月29日下午2:09:32
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showDeSequencingTaskList")
	public String showDeSequencingTaskList() throws Exception {
		rightsId = "260203";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/analysis/desequencing/deSequencingTask.jsp");
	}

	@Action(value = "showDeSequencingTaskListJson")
	public void showDeSequencingTaskListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = deSequencingTaskService
					.findDeSequencingTaskList(start, length, query, col, sort);
			List<DeSequencingTask> list = (List<DeSequencingTask>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("createDate", "");
			map.put("machineTime", "yyyy-MM-dd");
			map.put("REF", "");
			map.put("outPut", "");
			map.put("clusters", "");
			map.put("pf", "");
			map.put("reasonType", "");
			map.put("detailed", "");
			map.put("machineCode", "");
			map.put("testType", "");
			map.put("acceptUser-id", "");
			map.put("acceptUser-name", "");
			map.put("fileInfo-id", "");
			map.put("fileInfo-fileName", "");
			map.put("acceptDate", "yyyy-MM-dd");
			map.put("flowCode", "");
			map.put("state", "");
			map.put("codes", "");
			map.put("scopeName", "");
			map.put("scopeId", "");
			map.put("stateName", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	/**
	 * 
	 * @Title: editDeSequencingTask
	 * @Description: 新建、编辑
	 * @author : shengwei.wang
	 * @date 2018年3月29日下午2:10:18
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "editDeSequencingTask")
	public String editDeSequencingTask() throws Exception {
		rightsId = "260201";
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			deSequencingTask = deSequencingTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "deSequencingTask");
		} else {
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			deSequencingTask.setCreateUser(user);
			Date date = new Date();
			deSequencingTask.setId("NEW");
			deSequencingTask.setCreateDate(date);
			deSequencingTask
					.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			deSequencingTask
					.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(deSequencingTask.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/analysis/desequencing/deSequencingTaskEdit.jsp");
	}

	/**
	 * 
	 * @Title: save
	 * @Description: 保存
	 * @author : shengwei.wang
	 * @date 2018年3月29日下午3:24:24
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "save")
	public void save() throws Exception {
		String ids = getParameterFromRequest("ids");
		String changeLog = getParameterFromRequest("changeLog");
		String changeLogs=new String(changeLog.toString().getBytes("ISO-8859-1"), "UTF-8");
		String changeLogItem = getParameterFromRequest("changeLogIteml");
		String changeLogItems=new String(changeLogItem.toString().getBytes("ISO-8859-1"), "UTF-8");
		User user = (User) this
				.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		String main = getParameterFromRequest("main");
		String itemJson = getParameterFromRequest("itemJson");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String id = deSequencingTaskService.save(main, itemJson, user,ids,changeLogs,changeLogItems);
			map.put("id", id);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "viewDeSequencingTask")
	public String toViewDeSequencingTask() throws Exception {
		String id = getParameterFromRequest("id");
		deSequencingTask = deSequencingTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/analysis/desequencing/deSequencingTaskEdit.jsp");
	}

	/**
	 * 
	 * @Title: showDeSequencingItemListJson
	 * @Description: 下机质控明细
	 * @author : shengwei.wang
	 * @date 2018年3月29日下午2:25:54
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "showDeSequencingItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDeSequencingItemListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = deSequencingTaskService
					.findDeSequencingItemList(scId, start, length, query, col,
							sort);
			List<DeSequencingTaskItem> list = (List<DeSequencingTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("flowCode", "");
			map.put("laneCode", "");
			map.put("poolingCode", "");
			map.put("sampleAmount", "");
			map.put("outPut", "");
			map.put("cluster", "");
			map.put("pf", "");
			map.put("q30", "");
			map.put("result", "");
			map.put("reason", "");
			map.put("libraryCode", "");
			map.put("sampleCode", "");
			map.put("state", "");
			map.put("note", "");
			map.put("desequencingTask-name", "");
			map.put("desequencingTask-id", "");

			map.put("machine", "");
			map.put("tiles", "");
			map.put("alignRate", "");
			map.put("errorRate", "");
			map.put("pfCluster", "");

			map.put("createDate", "");
			map.put("gc", "");
			map.put("q20", "");
			map.put("phasing", "");
			map.put("prePhasing", "");

			map.put("desResult", "");
			map.put("production", "");
			map.put("length", "");
			// 添加 下机质控 明细
			map.put("sampleID", "");
			map.put("kit", "");
			map.put("raw_reads", "");
			map.put("raw_len", "");
			map.put("raw_size", "");
			map.put("hq_reads", "");
			map.put("hq_len", "");
			map.put("hq_size", "");
			map.put("hq_reads_pct", "");
			map.put("per_lane_percent_raw_reads", "");
			map.put("per_lane_percent_HQ_reads", "");
			map.put("medianInsert", "");
			map.put("duplicate", "");
			map.put("onTarget_raw", "");
			map.put("onTarget_add", "");
			map.put("oriDepth", "");
			map.put("dedupDepth", "");
			map.put("hqDataPct", "");
			map.put("mappingPct", "");
			map.put("qTwentyPCT", "");
			map.put("qThirtyPCT", "");
			map.put("fastq", "");
			map.put("md5", "");
			map.put("readCount", "");
			map.put("oneXcoverage", "");
			map.put("tenXcoverage", "");
			map.put("twentyXcoverage", "");
			map.put("fiftyXcoverage", "");
			map.put("twentyPctMeancoverage", "");
			map.put("submit", "");
			map.put("nextFlow", "");
			map.put("nextFlowId", "");
			map.put("sampleType", "");
			map.put("stat", "");
			map.put("lane", "");
			map.put("length", "");
			map.put("gcContent", "");
			map.put("nContent", "");
			map.put("ratioOfLane", "");
			map.put("cleanReads", "");
			map.put("ratioOfReads", "");
			map.put("cleanBases", "");
			map.put("ratioOfBases", "");
			map.put("meanDepthDedup", "");
			map.put("oneCoverageDedup", "");
			map.put("tenCoverageDedup", "");
			map.put("twentyCoverageDedup", "");
			map.put("fiftyCoverageDedup", "");
			map.put("twentyMeanCoverageDedup", "");

			map.put("runId", "");
			map.put("productName", "");
			map.put("projectId", "");
			map.put("orderCode", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: delDeSequencingItem
	 * @Description: 删除明细
	 * @author : shengwei.wang
	 * @date 2018年3月29日下午3:24:48
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "delDeSequencingItem")
	public void delDeSequencingItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			deSequencingTaskService.delDeSequencingItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 下机质控文件上传
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showDeSequencingUpLoadList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDeSequencingUpLoadList() throws Exception {
		return dispatcher("/WEB-INF/page/analysis/desequencing/deSequencingUpLoad.jsp");
	}

	@Action(value = "showdeSequencingUpLoadListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showdeSequencingUpLoadListJson() {

		Map<String, Object> result = deSequencingTaskService
				.findDeSequencingUpLoadList();
		Long count = (Long) result.get("total");
		List<DeSequencingUpLoadItem> list = (List<DeSequencingUpLoadItem>) result
				.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("fileName", "");
		map.put("fileType", "");
		map.put("versionNo", "");
		map.put("attach-id", "");
		map.put("attach-fileName", "");

		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	/**
	 * 
	 * @Title: uploadCsvFile
	 * @Description: 上传附件
	 * @author : shengwei.wang
	 * @date 2018年3月29日下午3:34:55
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "uploadCsvFile", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void uploadCsvFile() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String id = getParameterFromRequest("id");
			String fileId = getParameterFromRequest("fileId");
			String str = deSequencingTaskService.uploadCsvFile(id, fileId);
			String[] split = str.split(",");
			String mainId=split[0];
			String message=split[1];
			map.put("success", true);
			map.put("message", message);
			map.put("mainId", mainId);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	/**
	 * 
	 * @Title: readCsvFile  
	 * @Description: 读取csv  
	 * @author : shengwei.wang
	 * @date 2018年4月2日上午10:44:37
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value = "readCsvFile", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void readCsvFile() throws Exception {
		//使用区块锁将读取移动文件的操作锁住，防止有同时操作
		synchronized (DeSequencingTaskAction.class) {
			Map<String, Object> map = new HashMap<String, Object>();
			try {
				map.put("data", deSequencingTaskService.readCsvFile());
				map.put("success", true);
			} catch (Exception e) {
				e.printStackTrace();
				map.put("success", false);
			}
			HttpUtils.write(JsonUtils.toJsonString(map));
		}
	}
		
	/**
	 * 查询所有的下机质控完成的task
	 */
	@Action(value = "showDeSequencingTaskByState")
	public String showDeSequencingTaskByState() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/analysis/desequencing/deSequencingTaskByState.jsp");
	}

	@Action(value = "showDeSequencingTaskByStateJson")
	public void showDeSequencingTaskByStateJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = deSequencingTaskService
				.findDeSequencingTaskByStateList(map2Query, startNum, limitNum,
						dir, sort);
		Long count = (Long) result.get("total");
		List<DeSequencingTask> list = (List<DeSequencingTask>) result
				.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "");
		map.put("machineTime", "yyyy-MM-dd");
		map.put("REF", "");
		map.put("outPut", "");
		map.put("clusters", "");
		map.put("pf", "");
		map.put("reasonType", "");
		map.put("detailed", "");
		map.put("machineCode", "");
		map.put("testType", "");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("fileInfo-id", "");
		map.put("fileInfo-fileName", "");
		map.put("acceptDate", "yyyy-MM-dd");
		map.put("flowCode", "");
		map.put("state", "");
		map.put("codes", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public DeSequencingTaskService getDeSequencingTaskService() {
		return deSequencingTaskService;
	}

	public void setDeSequencingTaskService(
			DeSequencingTaskService deSequencingTaskService) {
		this.deSequencingTaskService = deSequencingTaskService;
	}

	public DeSequencingTask getDeSequencingTask() {
		return deSequencingTask;
	}

	public void setDeSequencingTask(DeSequencingTask deSequencingTask) {
		this.deSequencingTask = deSequencingTask;
	}
}
