﻿
package com.biolims.analysis.desequencing.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.analysis.desequencing.model.DeSequencingAbnormal;
import com.biolims.analysis.desequencing.service.DeSequencingTaskAbnormalService;
import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/analysis/desequencing/deSequencingTaskAbnormal")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class DeSequencingTaskAbnormalAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";//260202
	@Autowired
	private DeSequencingTaskAbnormalService deSequencingTaskAbnormalService;
	private DeSequencingAbnormal deSequencingTaskAbnormal = new DeSequencingAbnormal();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showDeSequencingTaskAbnormalList")
	public String showDeSequencingTaskAbnormalList() throws Exception {
		rightsId="260202";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/analysis/desequencing/deSequencingTaskAbnormal.jsp");
	}

	@Action(value = "showDeSequencingTaskAbnormalListJson")
	public void showDeSequencingTaskAbnormalListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = deSequencingTaskAbnormalService.findDeSequencingTaskAbnormalList(start, length, query, col, sort);
			List<DeSequencingAbnormal> list = (List<DeSequencingAbnormal>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("sampleCode", "");
			map.put("isgood", "");
			map.put("nextFlowId", "");
			map.put("nextFlow", "");
			map.put("advice", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("method", "");
			
			map.put("submit", "");
			map.put("result", "");
			map.put("indexs", "");
			map.put("code", "");
			map.put("wkNum", "");
			
			map.put("estimate", "");
			map.put("backDate", "");
			map.put("errorRate", "");
			map.put("isRun", "");
			
			map.put("runId", "");
			map.put("productName", "");
			map.put("projectId", "");
			map.put("orderCode", "");
			map.put("sampleID", "");
			
			map.put("sampleStyle", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Action(value = "executeAbnormal")
	public void executeAbnormal() throws Exception {
		String[] ids = getRequest().getParameterValues("ids[]");
		String dataJson = getParameterFromRequest("dataJson");
		String changeLog = getParameterFromRequest("changeLog");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			deSequencingTaskAbnormalService.executeAbnormal(ids, dataJson,changeLog);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public DeSequencingTaskAbnormalService getDeSequencingTaskAbnormalService() {
		return deSequencingTaskAbnormalService;
	}

	public void setDeSequencingTaskAbnormalService(DeSequencingTaskAbnormalService deSequencingTaskAbnormalService) {
		this.deSequencingTaskAbnormalService = deSequencingTaskAbnormalService;
	}

	public DeSequencingAbnormal getDeSequencingTaskAbnormal() {
		return deSequencingTaskAbnormal;
	}

	public void setDeSequencingTaskAbnormal(DeSequencingAbnormal deSequencingTaskAbnormal) {
		this.deSequencingTaskAbnormal = deSequencingTaskAbnormal;
	}
	
	//保存pooling异常
	@Action(value = "saveDeSequencingTaskAbnormal")
	public void saveDeSequencingTaskAbnormal() throws Exception {
		//String id=getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
//		String itemDataJson = getRequest().getParameter("itemDataJson");
			String itemDataJson = getParameterFromRequest("itemDataJson");
			deSequencingTaskAbnormalService.saveDeSequencingTaskAbnormalList(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
