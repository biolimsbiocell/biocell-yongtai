﻿
package com.biolims.analysis.desequencing.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.analysis.desequencing.model.DeSequencingTaskItem;
import com.biolims.analysis.desequencing.service.DeSequencingItemService;
import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.storage.model.SampleInfoIn;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/analysis/desequencing/deSequencingItem")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class DeSequencingItemAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";
	@Autowired
	private DeSequencingItemService deSequencingItemService;
	private DeSequencingTaskItem deSequencingItem = new DeSequencingTaskItem();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showDeSequencingItemList")
	public String showDeSequencingItemList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/analysis/desequencing/deSequencingItem.jsp");
	}
	//=========
	@Action(value = "getDeSequencingList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void getDeSequencingList() throws Exception{
			String code = getRequest().getParameter("code");
			Map<String, Object> result = new HashMap<String, Object>();
			try {
				List<Map<String, String>> dataListMap = this.deSequencingItemService.getDeSequencingList(code);
				result.put("success", true);
				result.put("data", dataListMap);
			} catch (Exception e) {
				result.put("success", false);
			}
			HttpUtils.write(JsonUtils.toJsonString(result));
	}
	//=========
	@Action(value = "showDeSequencingItemListJson")
	public void showDeSequencingItemListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = deSequencingItemService.findDeSequencingItemList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<DeSequencingTaskItem> list = (List<DeSequencingTaskItem>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("flowCode", "");
		map.put("poolingCode", "");
		map.put("libraryCode", "");
		map.put("sampleCode", "");
		map.put("state", "");
		map.put("note", "");
		map.put("desequencingTask-id", "");
		map.put("desequencingTask-name", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "deSequencingItemSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogDeSequencingItemList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		String projectId = getParameterFromRequest("projectId");
		putObjToContext("projectId", projectId);
		return dispatcher("/WEB-INF/page/analysis/desequencing/deSequencingItemDialog.jsp");
	}

	@Action(value = "showDialogDeSequencingItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogDeSequencingItemListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		String projectId = getParameterFromRequest("projectId");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		
		if(projectId!=null	&&!"".equals(projectId)){
			map2Query.put("projectId", projectId);
		}
		Map<String, Object> result = deSequencingItemService.findDeSequencingItemList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<DeSequencingTaskItem> list = (List<DeSequencingTaskItem>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("flowCode", "");
		map.put("laneCode", "");
		map.put("poolingCode", "");
		map.put("sampleAmount", "");
		map.put("outPut", "");
		map.put("cluster", "");
		map.put("desequencingTask-id", "");
		map.put("desequencingTask-name", "");
		map.put("pf", "");
		map.put("q30", "");
		map.put("reason", "");
		map.put("libraryCode", "");
		map.put("sampleCode", "");
		map.put("state", "");
		map.put("note", "");
		map.put("machine", "");
		map.put("tiles", "");
		map.put("alignRate", "");
		map.put("errorRate", "");
		map.put("pfCluster", "");
		map.put("createDate", "");
		map.put("gc", "");
		map.put("q20", "");
		map.put("phasing", "");
		map.put("prePhasing", "");
		map.put("desResult", "");
		map.put("production", "");
		map.put("sampleID", "");
		map.put("kit", "");
		map.put("raw_reads", "");
		map.put("raw_len", "");
		map.put("raw_size", "");
		map.put("hq_reads", "");
		map.put("hq_len", "");
		map.put("hq_size", "");
		map.put("hq_reads_pct", "");
		map.put("per_lane_percent_raw_reads", "");
		map.put("per_lane_percent_HQ_reads", "");
		map.put("medianInsert", "");
		map.put("duplicate", "");
		map.put("onTarget_raw", "");
		map.put("onTarget_add", "");
		map.put("oriDepth", "");
		map.put("dedupDepth", "");
		map.put("hqDataPct", "");
		map.put("mappingPct", "");
		map.put("qTwentyPCT", "");
		map.put("qThirtyPCT", "");
		map.put("oneXcoverage", "");
		map.put("tenXcoverage", "");
		map.put("twentyXcoverage", "");
		map.put("fiftyXcoverage", "");
		map.put("twentyPctMeancoverage", "");
		
		map.put("result", "");
		map.put("submit", "");
		map.put("nextFlow", "");
		map.put("sampleType", "");
		map.put("stat", "");
		map.put("lane", "");
		map.put("length", "");
		map.put("gcContent", "");
		map.put("nContent", "");
		map.put("ratioOfLane", "");
		map.put("cleanReads", "");
		map.put("ratioOfReads", "");
		map.put("cleanBases", "");
		map.put("ratioOfBases", "");
		map.put("meanDepthDedup", "");
		map.put("oneCoverageDedup", "");
		map.put("tenCoverageDedup", "");
		map.put("twentyCoverageDedup", "");
		map.put("fiftyCoverageDedup", "");
		map.put("twentyMeanCoverageDedup", "");
		map.put("runId", "");
		map.put("productName", "");
		
		map.put("projectId", "");
		map.put("orderCode", "");
		
		
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}
	
	@Action(value = "showDialogDeSequencingItemNewListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogDeSequencingItemNewListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			String cid = getRequest().getParameter("id");

			Map<String, Object> result = deSequencingItemService.findDeSequencingItemNewList(cid,start, length, query,
							col, sort);
			List<DeSequencingTaskItem> list = (List<DeSequencingTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("flowCode", "");
			map.put("laneCode", "");
			map.put("poolingCode", "");
			map.put("sampleAmount", "");
			map.put("outPut", "");
			map.put("cluster", "");
			map.put("desequencingTask-id", "");
			map.put("desequencingTask-name", "");
			map.put("pf", "");
			map.put("q30", "");
			map.put("reason", "");
			map.put("libraryCode", "");
			map.put("sampleCode", "");
			map.put("state", "");
			map.put("note", "");
			map.put("machine", "");
			map.put("tiles", "");
			map.put("alignRate", "");
			map.put("errorRate", "");
			map.put("pfCluster", "");
			map.put("createDate", "");
			map.put("gc", "");
			map.put("q20", "");
			map.put("phasing", "");
			map.put("prePhasing", "");
			map.put("desResult", "");
			map.put("production", "");
			map.put("sampleID", "");
			map.put("kit", "");
			map.put("raw_reads", "");
			map.put("raw_len", "");
			map.put("raw_size", "");
			map.put("hq_reads", "");
			map.put("hq_len", "");
			map.put("hq_size", "");
			map.put("hq_reads_pct", "");
			map.put("per_lane_percent_raw_reads", "");
			map.put("per_lane_percent_HQ_reads", "");
			map.put("medianInsert", "");
			map.put("duplicate", "");
			map.put("onTarget_raw", "");
			map.put("onTarget_add", "");
			map.put("oriDepth", "");
			map.put("dedupDepth", "");
			map.put("hqDataPct", "");
			map.put("mappingPct", "");
			map.put("qTwentyPCT", "");
			map.put("qThirtyPCT", "");
			map.put("oneXcoverage", "");
			map.put("tenXcoverage", "");
			map.put("twentyXcoverage", "");
			map.put("fiftyXcoverage", "");
			map.put("twentyPctMeancoverage", "");
			
			map.put("result", "");
			map.put("submit", "");
			map.put("nextFlow", "");
			map.put("sampleType", "");
			map.put("stat", "");
			map.put("lane", "");
			map.put("length", "");
			map.put("gcContent", "");
			map.put("nContent", "");
			map.put("ratioOfLane", "");
			map.put("cleanReads", "");
			map.put("ratioOfReads", "");
			map.put("cleanBases", "");
			map.put("ratioOfBases", "");
			map.put("meanDepthDedup", "");
			map.put("oneCoverageDedup", "");
			map.put("tenCoverageDedup", "");
			map.put("twentyCoverageDedup", "");
			map.put("fiftyCoverageDedup", "");
			map.put("twentyMeanCoverageDedup", "");
			map.put("runId", "");
			map.put("productName", "");
			
			map.put("projectId", "");
			map.put("orderCode", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}



	@Action(value = "editDeSequencingItem")
	public String editDeSequencingItem() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			deSequencingItem = deSequencingItemService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "deSequencingItem");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
//			deSequencingItem.setCreateUser(user);
//			deSequencingItem.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/analysis/desequencing/deSequencingItemEdit.jsp");
	}

	@Action(value = "copyDeSequencingItem")
	public String copyDeSequencingItem() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		deSequencingItem = deSequencingItemService.get(id);
		deSequencingItem.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/analysis/desequencing/deSequencingItemEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = deSequencingItem.getId();
		if(id!=null&&id.equals("")){
			deSequencingItem.setId(null);
		}
		Map aMap = new HashMap();
		deSequencingItemService.save(deSequencingItem,aMap);
		return redirect("/analysis/desequencing/deSequencingItem/editDeSequencingItem.action?id=" + deSequencingItem.getId());

	}

	@Action(value = "viewDeSequencingItem")
	public String toViewDeSequencingItem() throws Exception {
		String id = getParameterFromRequest("id");
		deSequencingItem = deSequencingItemService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/analysis/desequencing/deSequencingItemEdit.jsp");
	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public DeSequencingItemService getDeSequencingItemService() {
		return deSequencingItemService;
	}

	public void setDeSequencingItemService(DeSequencingItemService deSequencingItemService) {
		this.deSequencingItemService = deSequencingItemService;
	}

	public DeSequencingTaskItem getDeSequencingItem() {
		return deSequencingItem;
	}

	public void setDeSequencingItem(DeSequencingTaskItem deSequencingItem) {
		this.deSequencingItem = deSequencingItem;
	}


}
