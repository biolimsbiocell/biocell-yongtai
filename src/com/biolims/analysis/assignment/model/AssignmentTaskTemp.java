package com.biolims.analysis.assignment.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.analysis.data.model.DataTask;
import com.biolims.dao.EntityDao;
import com.biolims.sample.model.SampleInfo;

@Entity
@Table(name = "ASSIGNMENT_TASK_TEMP")
@SuppressWarnings("serial")
public class AssignmentTaskTemp extends EntityDao<AssignmentTaskTemp> implements java.io.Serializable  {
	/** id */
	private String id;
	/** 样本编号 */
	private String sampleId;
	/** 原始样本编号 */
	private String sampleCode;
	/** 电子病历编号 */
	private String crmPatientId;

	/** 类型 */
	private String form;
	/** 外键 */
	private DataTask dataTask;
	/** MicroRNA; */
	private String MicroRNA;
	/** type; */
	private String type;
	/** 样本 */
	private SampleInfo sampleInfo;
	/** 状态 1显示 0不显示 */
	private String state;
	/** pooling编号 */
	private String poolingId;
	/** 样本类型id */
	private String sampleTypeId;
	/** 样本类型 */
	private String sampleTypeName;

	/* 检测项目 */
	private String productId;
	private String productName;
	/** FC号 */
	private String FC;

	/**
	 * 方法: 取得String
	 * 
	 * @return: String id
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String id
	 */
	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "SAMPLE_ID", length = 200)
	public String getSampleId() {
		return sampleId;
	}

	public void setSampleId(String sampleId) {
		this.sampleId = sampleId;
	}

	@Column(name = "SAMPLE_CODE", length = 200)
	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	@Column(name = "CRMPATIENT_ID", length = 200)
	public String getCrmPatientId() {
		return crmPatientId;
	}

	public void setCrmPatientId(String crmPatientId) {
		this.crmPatientId = crmPatientId;
	}

	@Column(name = "FORM", length = 200)
	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DATA_TASK")
	public DataTask getDataTask() {
		return dataTask;
	}

	public void setDataTask(DataTask dataTask) {
		this.dataTask = dataTask;
	}

	@Column(name = "MICRO_RNA", length = 200)
	public String getMicroRNA() {
		return MicroRNA;
	}

	public void setMicroRNA(String microRNA) {
		MicroRNA = microRNA;
	}

	@Column(name = "TYPE", length = 200)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO_ID")
	public SampleInfo getSampleInfo() {
		return sampleInfo;
	}

	public void setSampleInfo(SampleInfo sampleInfo) {
		this.sampleInfo = sampleInfo;
	}

	@Column(name = "STATE", length = 200)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	// 新增字段
	public String getPoolingId() {
		return poolingId;
	}

	public void setPoolingId(String poolingId) {
		this.poolingId = poolingId;
	}

	public String getSampleTypeId() {
		return sampleTypeId;
	}

	public void setSampleTypeId(String sampleTypeId) {
		this.sampleTypeId = sampleTypeId;
	}

	public String getSampleTypeName() {
		return sampleTypeName;
	}

	public void setSampleTypeName(String sampleTypeName) {
		this.sampleTypeName = sampleTypeName;
	}

	@Column(name = "PRODUCT_ID", length = 200)
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Column(name = "PRODUCT_NAME", length = 200)
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name = "FC", length = 200)
	public String getFC() {
		return FC;
	}

	public void setFC(String fC) {
		FC = fC;
	}

}
