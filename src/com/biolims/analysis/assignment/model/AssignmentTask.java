package com.biolims.analysis.assignment.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.analysis.data.model.DataTask;
import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;

@Entity
@Table(name = "ASSIGNMENT_TASK")
@SuppressWarnings("serial")
public class AssignmentTask extends EntityDao<AssignmentTask> implements java.io.Serializable  {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
	/**下达时间*/
	private Date createDate;
	/**下达人*/
	private User createUser;
	/**工作流状态*/
	private String state;
	/**状态描述*/
	private String stateName;
	/**fc号*/
	private String fcnumber;
	/**控制号*/
	private String knumber;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  下达时间
	 */
	@Column(name ="CREATE_DATE", length = 100)
	public Date getCreateDate(){
		return this.createDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  下达时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 *方法: 取得User
	 *@return: User  下达人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser(){
		return this.createUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  下达人
	 */
	public void setCreateUser(User createUser){
		this.createUser = createUser;
	}
	/**
	 *方法: 取得String
	 *@return: String  工作流状态
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  工作流状态
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态描述
	 */
	@Column(name ="STATE_NAME", length = 50)
	public String getStateName(){
		return this.stateName;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态描述
	 */
	public void setStateName(String stateName){
		this.stateName = stateName;
	}
	/**
	 *方法: 取得String
	 *@return: String  fc号
	 */
	@Column(name ="FCNUMBER", length = 50)
	public String getFcnumber(){
		return this.fcnumber;
	}
	/**
	 *方法: 设置String
	 *@param: String  fc号
	 */
	public void setFcnumber(String fcnumber){
		this.fcnumber = fcnumber;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  控制号
	 */
	@Column(name ="KNUMBER", length = 50)
	public String getKnumber() {
		return knumber;
	}
	/**
	 *方法: 设置String
	 *@param: String  控制号
	 */
	
	public void setKnumber(String knumber) {
		this.knumber = knumber;
	}
}
