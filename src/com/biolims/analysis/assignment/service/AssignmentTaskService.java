package com.biolims.analysis.assignment.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.analysis.assignment.dao.AssignmentTaskDao;
import com.biolims.analysis.assignment.model.AssignmentTask;
import com.biolims.analysis.assignment.model.AssignmentTaskItem;
import com.biolims.analysis.assignment.model.AssignmentTaskTemp;
import com.biolims.analysis.data.dao.DataTaskDao;
import com.biolims.analysis.data.model.CnvResult;
import com.biolims.analysis.data.model.DataTask;
import com.biolims.analysis.data.model.DataTaskItem;
import com.biolims.analysis.data.model.DataTaskSvItem;
import com.biolims.analysis.data.model.DataTaskSvnItem;
import com.biolims.analysis.data.model.DataTaskTemp;
import com.biolims.analysis.data.model.DateTaskCnvItem;
import com.biolims.analysis.data.model.SnvResult;
import com.biolims.analysis.data.model.SvResult;
import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.core.userGroup.service.UserGroupService;
import com.biolims.experiment.qa.model.QaTask;
import com.biolims.experiment.qa.model.QaTaskItem;
import com.biolims.experiment.qa.model.QaTaskReagent;
import com.biolims.experiment.sequencing.model.SequencingTaskInfo;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.report.model.SampleReportTemp;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleReceiveItem;
import com.biolims.sample.service.SampleStateService;
import com.biolims.system.template.model.ReagentItem;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class AssignmentTaskService {
	
	@Resource
	private AssignmentTaskDao assignmentTaskDao;
	@Resource
	private CommonDAO commonDAO;

	@Resource
	private UserGroupService userGroupService;
	@Resource
	private CommonService commonService;
	@Resource
	private SampleStateService sampleStateService;
	
	StringBuffer json = new StringBuffer();
	
	public Map<String, Object> findAssignmentTaskList(Map<String, String> map2Query,
			int startNum, int limitNum, String dir, String sort) {
		return assignmentTaskDao.selectAssignmentTaskList(map2Query, startNum, limitNum,
				dir, sort);
	}

	public AssignmentTask get(String id) {
		AssignmentTask assignmentTask = commonDAO.get(AssignmentTask.class, id);
		return assignmentTask;
	}
	
	public AssignmentTaskItem getItem(String id){
		AssignmentTaskItem assignmentTaskItem = assignmentTaskDao.get(id);
		return assignmentTaskItem;
	}

	public Map<String, Object> findAssignmentTaskTempList(String fcId,
			int startNum, int limitNum, String dir, String sort) {
		Map<String, Object> result = this.assignmentTaskDao.selectAssignmentTaskTempList(
				fcId, startNum, limitNum, dir, sort);
		return result;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(AssignmentTask assignmentTask, Map aMap) throws Exception {
		if (assignmentTask != null) {
			assignmentTaskDao.saveOrUpdate(assignmentTask);

			String jsonStr = "";
			
			jsonStr = (String) aMap.get("assignmentTaskItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveAssignmentTaskItem(assignmentTask, jsonStr);
			}
		}
	}
	/**
	 * 保存
	 * 
	 * @param sc
	 * @param itemDataJson
	 * @throws Exception
	 */

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveAssignmentTaskItem(AssignmentTask assignmentTask,
			String jsonStr) throws Exception {
		List<AssignmentTaskItem> saveItems = new ArrayList<AssignmentTaskItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				jsonStr, List.class);
		for (Map<String, Object> map : list) {
			AssignmentTaskItem scp = new AssignmentTaskItem();
			// 将map信息读入实体类
			scp = (AssignmentTaskItem) assignmentTaskDao.Map2Bean(map, scp);
			AssignmentTaskTemp assignmentTaskTemp = new AssignmentTaskTemp();
			assignmentTaskTemp = assignmentTaskDao.get(AssignmentTaskTemp.class, scp.getTempId());
			if (assignmentTaskTemp != null) {
				assignmentTaskTemp.setState("1");
				assignmentTaskDao.saveOrUpdate(assignmentTaskTemp);
			}
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setAssignmentTask(assignmentTask);

			saveItems.add(scp);
			// 把子表数据插入到临时表SampleReportTemp,处理方式为通过的就添加数据到临时表
			/* &&"2".equals(scp.getStates()) */
			/*
			 * if("1".equals(scp.getMethod())){ //List<SampleReportTemp>
			 * listReportTemp = new ArrayList<SampleReportTemp>();
			 * SampleReportTemp temp = new SampleReportTemp();
			 * temp.setSampleCode(scp.getSampleId());
			 * temp.setPatientId(scp.getCrmPatientId());
			 * temp.setWritingPersonOne(scp.getAcceptUser());
			 * temp.setWritingPersonTwo(scp.getAcceptUser2());
			 * temp.setState("1"); this.dataTaskDao.saveOrUpdate(temp); }
			 */

		}
		assignmentTaskDao.saveOrUpdateAll(saveItems);
	}

	public Map<String, Object> findAssignmentTaskItemList(String scId,
			int startNum, int limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = this.assignmentTaskDao.selectAssignmentTaskItemList(
				scId, startNum, limitNum, dir, sort);
		// List<DataTaskItem> list = (List<DataTaskItem>) result.get("list");
		return result;
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delAssignmentTaskItem(String[] ids) throws Exception {
		for (String id : ids) {
			AssignmentTaskItem scp = this.assignmentTaskDao.get(AssignmentTaskItem.class, id);
			this.assignmentTaskDao.delete(scp);
		}
	}


	public Map<String, Object> findSequencingList(
			Map<String, String> mapForQuery, Integer startNum, Integer limitNum,
			String dir, String sort, String flag) {
		return assignmentTaskDao.selectSequencingList(mapForQuery, startNum,
				limitNum, dir, sort, flag);
	}

	public List<SampleInfo> findSequencingResultList(String fcId,
			Integer startNum, Integer limitNum, String dir, String sort) {
		Map<String, Object> result = assignmentTaskDao.selectSequencingResultList(
				fcId, startNum, limitNum, dir, sort);
		List<SequencingTaskInfo> list = (List<SequencingTaskInfo>) result.get("list");
		//保存上机测序的pooling号
		HashSet<String> sequencingPoolingNum = new HashSet<String>();
		
		for (SequencingTaskInfo sampleSequencingInfo : list) {
//			sequencingPoolingNum.add(sampleSequencingInfo.getPoolingCode());
		}
		//查询文库编号
		HashSet<String> wkBlendIds = selectWKBlendId(sequencingPoolingNum);
		//查询富集原始样本信息
		List<SampleInfo> sampleInfoList = selectSampleInfo(wkBlendIds);
		return sampleInfoList;
	}

	//根据文库编号查询样本
	public List<SampleInfo> selectSampleInfo(HashSet<String> wkBlendIds){
		return assignmentTaskDao.selectSampleInfo(wkBlendIds);
	}
	
	//查询文库混合的编号
	public HashSet<String> selectWKBlendId(HashSet poolingIds){
		return assignmentTaskDao.selectWKBlendId(poolingIds);
	}
	
	public List<AssignmentTaskItem> findAssignmentTaskItemList(
			String businessKey) {
		List<AssignmentTaskItem> list = assignmentTaskDao.selectAssignmentTaskItemList(businessKey);
		return list;
	}

	// 审批完成
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String changeState(String applicationTypeActionId, String id)
			throws Exception {
		// 得到DataTask对象
		try {
			AssignmentTask sct = this.assignmentTaskDao.get(AssignmentTask.class, id);
			sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
			sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
			/**
			 * 对比后有效数据添加到结果表
			 */

			List<AssignmentTaskItem> l = queryAssignmentTaskItemList(id);
			List<String> orderList = new ArrayList<String>();
			for (AssignmentTaskItem qis : l) {
				SampleReportTemp st = new SampleReportTemp();

				SampleInfo si = new SampleInfo();
				st.setSampleCode(qis.getSampleCode());
				List<SampleInfo> sil = commonService.get(SampleInfo.class,
						"code", qis.getSampleCode());
				if (sil.size() > 0)
					si = sil.get(0);

				if ((si.getOrderNum() != null)
						&& !orderList.contains(si.getOrderNum())) {

					st.setPatientName(si.getPatientName());
					st.setProductId(si.getProductId());
					st.setProductName(si.getProductName());
					st.setReportDate(DateUtil.parse(si.getReportDate()));
					st.setOrderNum(si.getOrderNum());
					st.setNote(qis.getNote());
					st.setState("1");
					commonDAO.saveOrUpdate(st);
					orderList.add(si.getOrderNum());
				}
			}
			
			// 根据主表查询分析明细
			List<AssignmentTaskItem> listItem = this.assignmentTaskDao
					.selectAssignmentTaskItemList(id);
			for (AssignmentTaskItem d : listItem) {
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				sampleStateService
						.saveSampleState(
								d.getSampleId(),
								d.getSampleCode(),
								d.getProductId(),
								d.getProductName(),
								"",
								format.format(sct.getCreateDate()),
								format.format(new Date()),
								"AssignmentTask",
								"生信任务分配",
								(User) ServletActionContext
										.getRequest()
										.getSession()
										.getAttribute(
												SystemConstants.USER_SESSION_KEY),
								sct.getId(), "出报告", null, null, null, null,
								null, null, null, null, null);
			}
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "失败";
		}
		return "成功";
	}
	/**
	 * 生成上机测序CSV文件
	 * 
	 * @return
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String createCSV(String taskId) throws Exception {

		// 时间转换
		SimpleDateFormat sp = new SimpleDateFormat("yyyy-MM-dd");
		Map<String, String> mapForQuery = new HashMap<String, String>();
		mapForQuery.put("task.id", taskId);
		// 根据ID获取生信任务分配信息
		AssignmentTask sat = assignmentTaskDao.get(AssignmentTask.class, taskId);
		// 查询生信任务分配明细
		Map<String, Object> result = assignmentTaskDao.selectAssignmentTaskItemList(
				taskId, null, null, null, null);
		List<AssignmentTaskItem> list = (List<AssignmentTaskItem>) result
				.get("list");
		if (list == null || list.size() <= 0)
			return null;
		String a = "/out";
		String b = "/in/\\";
		String filePath = SystemConstants.WRITE_QC_PATH + "/" + sat.getId();// 写入csv路径,以fc号为准
		// 往qcdata中写入两个csv文件
		String fileName = filePath + a + "/sampleSheets.csv";// 文件名称

		String fileNames = filePath + "/in";
		File file = new File(fileNames);
		file.mkdirs();

		// String fName = filePath + b + "";// 生成in 文件夹
		File csvFile = null;
		File csvFile1 = null;
		BufferedWriter csvWtriter = null;
		BufferedWriter csvWtriter1 = null;
		csvFile = new File(fileName);
		
		File parent = csvFile.getParentFile();
		if (parent != null && !parent.exists()) {
			parent.mkdirs();
		}
	
		csvFile.createNewFile();
		
		// GB2312使正确读取分隔符","
		csvWtriter = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(csvFile), "GBK"), 1024);
		// 写入文件头部
		Object[] head = { "生信任务分配编号","原始样本号","flowCell号" };

		List<Object> headList = Arrays.asList(head);
		for (Object data : headList) {
			StringBuffer sb = new StringBuffer();
			String rowStr = sb.append("\"").append(data).append("\",")
					.toString();
			csvWtriter.write(rowStr);
		}
		csvWtriter.newLine();
		for (int i = 0; i < list.size(); i++) {
			AssignmentTaskItem info = list.get(i);
			if (info != null) {
				
			StringBuffer sb = new StringBuffer();
			sb.append("\"").append(sat.getId()).append("\",");// 生信任务分配编号
			sb.append("\"").append(info.getSampleCode()).append("\",");// 原始样本号
			sb.append("\"").append(sat.getFcnumber()).append("\",");// flowcell号
			
			String rowStr = sb.toString();
			csvWtriter.write(rowStr);
			csvWtriter.newLine();
		
		}

		}

		csvWtriter.flush();
		csvWtriter.close();

		// InputStream is = new FileInputStream(csvFile.getPath());
		// FtpUtil.uploadFile(listInfo.get(0).getSequencing().getFcCode(),
		// csvFile.getName(), is);
		// InputStream is1 = new FileInputStream(csvFile1.getPath());
		// FtpUtil.uploadFile(listInfo.get(0).getSequencing().getFcCode(),
		// csvFile1.getName(), is1);

		return csvFile.getName();

	}
	/**
	 * ===============================================查询数据分析明细
	 * 
	 * @return
	 */
	public List<AssignmentTaskItem> queryAssignmentTaskItemList(String scId)
			throws Exception {
		String hql = "from AssignmentTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and assignmentTask.id='" + scId + "'";
		List<AssignmentTaskItem> list = new ArrayList<AssignmentTaskItem>();
		list = this.assignmentTaskDao.getSession().createQuery(hql + key).list();
		return list;
	}
}
