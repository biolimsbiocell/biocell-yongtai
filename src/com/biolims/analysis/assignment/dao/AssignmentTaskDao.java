package com.biolims.analysis.assignment.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.analysis.assignment.model.AssignmentTask;
import com.biolims.analysis.assignment.model.AssignmentTaskItem;
import com.biolims.analysis.assignment.model.AssignmentTaskTemp;
import com.biolims.analysis.data.model.DataTask;
import com.biolims.analysis.data.model.DataTaskItem;
import com.biolims.analysis.data.model.DataTaskTemp;
import com.biolims.analysis.desequencing.model.DeSequencingTask;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.pooling.model.PoolingItem;
import com.biolims.experiment.qa.model.QaTaskItem;
import com.biolims.experiment.sequencing.model.SequencingTask;
import com.biolims.experiment.sequencing.model.SequencingTaskInfo;
import com.biolims.experiment.sj.wkblend.model.WkBlendTaskInfo;
import com.biolims.experiment.sj.wkblend.model.WkBlendTaskItem;
import com.biolims.sample.model.SampleInfo;

@Repository
@SuppressWarnings("unchecked")
public class AssignmentTaskDao extends BaseHibernateDao {

	public Map<String, Object> selectAssignmentTaskList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from AssignmentTask where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<AssignmentTask> list = new ArrayList<AssignmentTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	/**
	 * 左侧临时表
	 */
	public Map<String, Object> selectAssignmentTaskTempList(String fcId,
			Integer startNum, Integer limitNum, String dir, String sort) {
		String hql = "from AssignmentTaskTemp where 1=1 and FC= '" + fcId + "'";
		String key = "";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql).uniqueResult();
		List<AssignmentTaskTemp> list = new ArrayList<AssignmentTaskTemp>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 数据分析明细
	 */
	public Map<String, Object> selectAssignmentTaskItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {

		String hql = "from AssignmentTaskItem where 1=1 ";
		String key = "";
		if (scId != null) {
			key = key + " and assignmentTask.id ='" + scId + "'";
		}
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<AssignmentTaskItem> list = new ArrayList<AssignmentTaskItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		if (list.size() <= 0) {
			DataTask d = get(DataTask.class, scId);
			if (d != null) {
				d.setKnumber("0");
				save(d);
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectSequencingList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort, String flag) {
		String key = " ";
		String hql = " ";
		if (flag.equals("100")) {
			hql = " from SequencingTask where 1=1 and stateName = '完成' ";
		} else {
			hql = " from SequencingTask where 1=1 ";
		}
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SequencingTask> list = new ArrayList<SequencingTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id desc";

			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectSequencingResultList(String fcId,
			Integer startNum, Integer limitNum, String dir, String sort) {
		String hql = "from SampleSequencingInfo where 1=1";
		String key = "";
		if (fcId != null)
			key = key + " and sequencing.id='" + fcId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SequencingTaskInfo> list = new ArrayList<SequencingTaskInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public List<AssignmentTaskItem> selectAssignmentTaskItemList(String scId) {
		String hql = "from AssignmentTaskItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and assignmentTask.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<AssignmentTaskItem> list = new ArrayList<AssignmentTaskItem>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		return list;
	}

	public AssignmentTaskItem get(String id) {
		String hql = "from AssignmentTaskItem where assignmentTask.id ='" + id
				+ "'";
		AssignmentTaskItem uniqueResult = (AssignmentTaskItem) this
				.getSession().createQuery(hql).uniqueResult();
		return uniqueResult;
	}

	public HashSet<String> selectWKBlendId(HashSet poolingIds) {
		Iterator<String> sequencingPoolingIterator = poolingIds.iterator();
		String ids = "";
		while (sequencingPoolingIterator.hasNext()) {
			ids = "'" + sequencingPoolingIterator.next() + "',";
		}
		ids = ids.substring(0, ids.length() - 1);
		String hql = "from WkBlendTaskResult where hhzh in (" + ids + ")";
		// 得到文库混合结果的集合
		List<WkBlendTaskInfo> list = this.getSession().createQuery(hql)
				.list();

		hql = "";
		ids = "";
		// 根据文库结果中wkTaskId查询文库明细中的文库编号
		for (WkBlendTaskInfo wkBlendTaskResult : list) {
			ids = "'" + wkBlendTaskResult.getWkBlendTask().getId() + "',";
		}
		ids = ids.substring(0, ids.length() - 1);
		hql = "from WkBlendTaskItem where wkblendTask.id in (" + ids + ")";
		// 得到文库混合明细的集合
		List<WkBlendTaskItem> list2 = this.getSession().createQuery(hql).list();

		// 得到文库编号
		HashSet<String> WkBlendItemId = new HashSet<String>();
		for (WkBlendTaskItem wkBlendTaskItem : list2) {
//			WkBlendItemId.add(wkBlendTaskItem.getFjwk());
		}
		return WkBlendItemId;
	}

	public List<SampleInfo> selectSampleInfo(HashSet<String> wkBlendIds) {
		Iterator<String> wkBlendIdsIterator = wkBlendIds.iterator();
		String ids = "";
		while (wkBlendIdsIterator.hasNext()) {
			ids = "'" + wkBlendIdsIterator.next() + "',";
		}
		ids = ids.substring(0, ids.length() - 1);
		String hql = "from PoolingTaskItem where fjWkCode in (" + ids + ")";
		// 得到富集明细的集合
		List<PoolingItem> list = this.getSession().createQuery(hql).list();

		hql = "";
		ids = "";
		// 根据富集明细中sampleCode查询样本中心中的信息
		for (PoolingItem poolingTaskItem : list) {
			ids = "'" + poolingTaskItem.getSampleCode() + "',";
		}
		ids = ids.substring(0, ids.length() - 1);
		hql = "from SampleInfo t where 1=1 and t.code in (" + ids + ")";
		// 得到样本集合
		List<SampleInfo> list2 = this.getSession().createQuery(hql).list();
		return list2;
	}

}
