package com.biolims.analysis.assignment.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.analysis.assignment.dao.AssignmentTaskDao;
import com.biolims.analysis.assignment.model.AssignmentTask;
import com.biolims.analysis.assignment.model.AssignmentTaskItem;
import com.biolims.analysis.assignment.model.AssignmentTaskTemp;
import com.biolims.analysis.assignment.service.AssignmentTaskService;
import com.biolims.analysis.desequencing.model.DeSequencingTask;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.experiment.sequencing.model.SequencingTask;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.model.SampleInfo;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/analysis/assignment/assignmentTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public class AssignmentTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = -1570364443126947276L;
	private String rightsId = "260111";
	@Autowired
	private AssignmentTaskService assignmentTaskService;
	private AssignmentTask assignmentTask = new AssignmentTask();
	@Resource
	private CommonService commonService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private AssignmentTaskDao assignmentTaskDao;

	@Action(value = "showAssignmentTaskList")
	public String showAssignmentTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/analysis/assignment/assignmentTask.jsp");
	}

	/**
	 * 查询所有fc号
	 */
	@Action(value = "showDeSequencingTaskByState")
	public String showDeSequencingTaskByState() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/analysis/assignment/deSequencingTaskByState.jsp");
	}

	@Action(value = "showDeSequencingTaskByStateJson")
	public void showDeSequencingTaskByStateJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		// String flag = getParameterFromRequest("flag");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = assignmentTaskService.findSequencingList(
				map2Query, startNum, limitNum, dir, sort, "100");
		Long count = (Long) result.get("total");
		List<SequencingTask> list = (List<SequencingTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("reciveUser-id", "");
		map.put("reciveUser-name", "");
		map.put("fcCode", "");
		map.put("refCode", "");
		map.put("spec", "");
		map.put("refName", "");
		map.put("reagent", "");
		map.put("expectDate", "yyyy-MM-dd");
		map.put("reciveDate", "yyyy-MM-dd");
		map.put("template-id", "");
		map.put("template-name", "");
		map.put("deviceCode-id", "");
		map.put("deviceCode-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		// 实验组
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd HH:mm:ss");
		map.put("type", "");
		map.put("testUserOneId", "");
		map.put("testUserOneName", "");
		map.put("testUserTwoId", "");
		map.put("testUserTwoName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	// 根据fc号，查询上机测序结果
	@Action(value = "showSequencingResultListJson")
	public void showSequencingResultListJson() throws Exception {
		// 开始记录数
		int startNum = "".equals(getParameterFromRequest("start")) ? 0
				: Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = "".equals(getParameterFromRequest("limit")) ? 0
				: Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		// fc号
		String fcId = getParameterFromRequest("fcId");

		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<SampleInfo> resultList = assignmentTaskService
					.findSequencingResultList(fcId, startNum, limitNum, dir,
							sort);
			result.put("success", true);
			result.put("data", resultList);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	@Action(value = "showAssignmentTaskListJson")
	public void showAssignmentTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = assignmentTaskService
				.findAssignmentTaskList(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<AssignmentTask> list = (List<AssignmentTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("fcnumber", "");
		map.put("knumber", "");

		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editAssignmentTask")
	public String editAssignmentTask() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			assignmentTask = assignmentTaskService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "assignmentTask");
		} else {
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			assignmentTask.setCreateUser(user);
			assignmentTask.setCreateDate(new Date());
			assignmentTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			assignmentTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			assignmentTask.setId("NEW");
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(assignmentTask.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/analysis/assignment/assignmentTaskEdit.jsp");
	}

	@Action(value = "viewAssignmentTask")
	public String viewAssignmentTask() throws Exception {
		String id = getParameterFromRequest("id");
		assignmentTask = assignmentTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/analysis/assignment/assignmentTaskEdit.jsp");
	}

	// 突变筛选左侧临时表
	@Action(value = "showAssignmentTaskTempList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showAssignmentTaskTempList() throws Exception {
		return dispatcher("/WEB-INF/page/analysis/assignment/assignmentTaskTemp.jsp");
	}

	// 突变筛选左侧临时表
	@Action(value = "showAssignmentTaskTempListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showAssignmentTaskTempListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		// String scId = getRequest().getParameter("id");
		String fcId = getRequest().getParameter("fcId");
		Map<String, String> mapForQuery = new HashMap<String, String>();
		try {
			Map<String, Object> result = assignmentTaskService
					.findAssignmentTaskTempList(fcId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<AssignmentTaskTemp> list = (List<AssignmentTaskTemp>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");// 编号
			map.put("sampleId", "");// 样本编号
			map.put("crmPatientId", "");
			map.put("form", "");// 类型

			map.put("assignmentTask-id", "");
			map.put("assignmentTask-name", "");
			map.put("MicroRNA", "");// ran
			map.put("type", "");// type
			map.put("sampleInfo-id", "");// 样本id
			map.put("sampleInfo-name", "");// 样本编号

			map.put("state", "");// 读取状态

			map.put("poolingId", "");// pooling编号
			map.put("sampleTypeId", "");// 样本类型ID
			map.put("sampleTypeName", "");// 样本类型名称
			map.put("productId", "");
			map.put("productName", "");
			map.put("sampleCode", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = assignmentTask.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "AssignmentTask";
			String markCode = "ASSIGNMENT";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			assignmentTask.setId(autoID);
		}

		Map aMap = new HashMap();

		aMap.put("assignmentTaskItem",
				getParameterFromRequest("assignmentTaskItemJson"));

		assignmentTaskService.save(assignmentTask, aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/analysis/assignment/assignmentTask/editAssignmentTask.action?id="
				+ assignmentTask.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;

		return redirect(url);

	}

	@Action(value = "copyAssignmentTask")
	public String copyAssignmentTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		assignmentTask = assignmentTaskService.get(id);
		assignmentTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/analysis/assignment/assignmentTaskEdit.jsp");
	}

	/**
	 * 查询数据分析明细
	 * 
	 * @throws Exception
	 */

	@Action(value = "showAssignmentTaskItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showAssignmentTaskItemList() throws Exception {
		// putObjToContext("sgt2", getParameterFromRequest("sgt2"));
		return dispatcher("/WEB-INF/page/analysis/assignment/assignmentTaskItem.jsp");
	}

	@Action(value = "showAssignmentTaskItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showAssignmentTaskItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String scId = getRequest().getParameter("id");
		// String fcId = getRequest().getParameter("fcId");
		Map<String, String> mapForQuery = new HashMap<String, String>();
		try {
			Map<String, Object> result = assignmentTaskService
					.findAssignmentTaskItemList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<AssignmentTaskItem> list = (List<AssignmentTaskItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");// 编号
			map.put("sampleId", "");// 样本编号
			map.put("crmPatientId", "");
			map.put("name", "");// 名称
			map.put("location", "");// 位置
			map.put("xjPot", "");// 碱基改变
			map.put("tbFrequence", "");// 突变频率
			map.put("genePot", "");// 基因改变
			map.put("hgmdPubmedid", "");// 参考文献ID
			map.put("note", "");// 注释
			map.put("form", "");// 类型

			map.put("assignmentTask-id", "");
			map.put("assignmentTask-name", "");
			map.put("MicroRNA", "");// ran
			map.put("type", "");// type
			map.put("acceptUser-id", "");// 实验员id
			map.put("acceptUser-name", "");// 实验员名称

			map.put("sampleInfo-id", "");// 样本id
			map.put("sampleInfo-name", "");// 样本编号

			map.put("method", "");// 处理方法
			map.put("reason", "");// 失败原因
			map.put("states", "");// 通过状态

			map.put("poolingId", "");// pooling编号
			map.put("wkTaskId", "");// 文库编号
			map.put("sampleTypeId", "");// 样本类型ID
			map.put("sampleTypeName", "");// 样本类型名称
			map.put("sampleStage", "");// 分期
			map.put("tempId", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("sampleCode", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细
	 * 
	 * @throws Exception
	 */
	@Action(value = "delAssignmentTaskItem")
	public void delAssignmentTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			assignmentTaskService.delAssignmentTaskItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public AssignmentTask getAssignmentTask() {
		return assignmentTask;
	}

	public void setAssignmentTask(AssignmentTask assignmentTask) {
		this.assignmentTask = assignmentTask;
	}

}
