package com.biolims.analysis.datatransfer.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.analysis.datatransfer.dao.DataTransferDao;
import com.biolims.analysis.datatransfer.model.DataTransfer;
import com.biolims.analysis.datatransfer.model.DataTransferItem;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.service.SystemCodeService;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class DataTransferService {

	@Resource
	private CommonDAO commonDAO;
	@Resource
	private DataTransferDao dataTransferDao;
	@Resource
	private SystemCodeService systemCodeService;

	public Map<String, Object> findDataTransferList(
			Map<String, String> map2Query, Integer startNum, Integer limitNum,
			String dir, String sort) {

		return dataTransferDao.selectDataTransferList(map2Query, startNum,
				limitNum, dir, sort);
	}

	public DataTransfer get(String id) {
		return dataTransferDao.get(DataTransfer.class, id);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(DataTransfer i) throws Exception {

		dataTransferDao.saveOrUpdate(i);

	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(DataTransfer sc, Map jsonMap) throws Exception {
		if (sc != null) {
			dataTransferDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("dataTransferItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveDataTransferItem(sc, jsonStr);
			}
		}
	}

	private void saveDataTransferItem(DataTransfer sc, String jsonStr)
			throws Exception {
		List<DataTransferItem> saveItems = new ArrayList<DataTransferItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(jsonStr,
				List.class);
		for (Map<String, Object> map : list) {
			DataTransferItem scp = new DataTransferItem();
			// 将map信息读入实体类
			scp = (DataTransferItem) dataTransferDao.Map2Bean(map, scp);
			scp.setDataTransfer(sc);
			dataTransferDao.saveOrUpdate(scp);
		}
	}

	public Map<String, Object> findDataTransferItemList(String scId,
			int startNum, int limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = dataTransferDao.selectDataTransferItemList(scId,
				startNum, limitNum, dir, sort);
		List<DataTransferItem> list = (List<DataTransferItem>) result.get("list");
		return result;
	}

	public void delDataTransferItem(String[] ids) {
		for (String id : ids) {
			DataTransferItem sri = new DataTransferItem();
			sri.setId(id);
			dataTransferDao.delete(sri);

		}
	}
	
}
