package com.biolims.analysis.datatransfer.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.analysis.datatransfer.model.DataTransfer;
import com.biolims.analysis.datatransfer.model.DataTransferItem;
import com.biolims.dao.BaseHibernateDao;

@Repository
@SuppressWarnings("unchecked")
public class DataTransferDao extends BaseHibernateDao{

	public Map<String, Object> selectDataTransferList(
			Map<String, String> map2Query, Integer startNum, Integer limitNum,
			String dir, String sort) {
		String key = " ";
		String hql = " from DataTransfer where 1=1 ";
		if (map2Query != null)
			key = map2where(map2Query);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<DataTransfer> list = new ArrayList<DataTransfer>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	public Map<String, Object> selectDataTransferItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from DataTransferItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and dataTransfer='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<DataTransferItem> list = new ArrayList<DataTransferItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
}
