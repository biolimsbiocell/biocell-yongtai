package com.biolims.analysis.datatransfer.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.analysis.datatransfer.model.DataTransfer;
import com.biolims.analysis.datatransfer.model.DataTransferItem;
import com.biolims.analysis.datatransfer.service.DataTransferService;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.file.service.FileInfoService;
import com.biolims.technology.wk.model.TechJkServiceTaskItem;
import com.biolims.technology.wk.service.TechJkServiceTaskService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

@Namespace("/analysis/dataTransfer")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public class DataTransferAction extends BaseActionSupport {

	private static final long serialVersionUID = -3885163928389355905L;
	private String rightsId = "2607";
	@Autowired
	private DataTransferService dataTransferService;
	@Resource
	private CommonService commonService;
	private DataTransfer dataTransfer = new DataTransfer();

	@Autowired
	private TechJkServiceTaskService techJkServiceTaskService;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;

	@Action(value = "showDataTransferList")
	public String showDataTransferList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/analysis/dataTransfer/dataTransfer.jsp");
	}

	@Action(value = "showDataTransferListJson")
	public void showDataTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = dataTransferService.findDataTransferList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<DataTransfer> list = (List<DataTransfer>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("project-name", "");
		map.put("template-name", "");
		map.put("userGroup-name", "");
		map.put("tester-name", "");
		map.put("orderId", "");
		map.put("stateName", "");
		map.put("note", "");

		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editDataTransfer")
	public String editDataTransfer() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			dataTransfer = dataTransferService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "dataTransfer");
			String tName = workflowProcessInstanceService
					.findToDoTaskNameByFormId(id);
			putObjToContext("taskName", tName);
		} else {
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			dataTransfer.setCreateUser(user);
			dataTransfer.setCreateDate(new Date());
			dataTransfer.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			dataTransfer.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(dataTransfer.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/analysis/dataTransfer/dataTransferEdit.jsp");
	}

	@Action(value = "showDialogDataTransferListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogDataTransferListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = dataTransferService.findDataTransferList(
				map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<DataTransfer> list = (List<DataTransfer>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("project-name", "");
		map.put("template-name", "");
		map.put("userGroup-name", "");
		map.put("tester-name", "");
		map.put("orderId", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "copyDataTransfer")
	public String copyDataTransfer() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		dataTransfer = dataTransferService.get(id);
		dataTransfer.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/dataTransfer/dataTransferEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = dataTransfer.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "dataTransfer";
			String markCode = "DT";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			dataTransfer.setId(autoID);
		}

		Map aMap = new HashMap();
		aMap.put("dataTransferItem",
				getParameterFromRequest("dataTransferItemJson"));

		dataTransferService.save(dataTransfer, aMap);
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		String url = "/analysis/dataTransfer/editDataTransfer.action?id="
				+ dataTransfer.getId();
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null"))
			url += "&bpmTaskId=" + bpmTaskId;
		return redirect(url);

	}

	@Action(value = "viewDataTransfer")
	public String viewDataTransfer() throws Exception {
		String id = getParameterFromRequest("id");
		dataTransfer = dataTransferService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/analysis/dataTransfer/dataTransferEdit.jsp");
	}

	@Action(value = "showDataTransferItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDataTransferItemList() throws Exception {
		return dispatcher("/WEB-INF/page/analysis/dataTransfer/dataTransferItem.jsp");
	}

	@Action(value = "showDataTransferItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDataTransferItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = dataTransferService
					.findDataTransferItemList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<DataTransferItem> list = (List<DataTransferItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("sampleCode", "");
			map.put("sampleName", "");
			map.put("inwardCode", "");
			map.put("flowCell", "");
			map.put("laneCode", "");
			map.put("transferWay", "");
			map.put("ftp", "");
			map.put("dataTransfer-id", "");
			map.put("note", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delDataTransferItem")
	public void delDataTransferItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			dataTransferService.delDataTransferItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "setDataTransferItem")
	public void setDataTransferItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String id = getParameterFromRequest("id");
			List<TechJkServiceTaskItem> list = techJkServiceTaskService
					.selTechJkserverTaskItem(id);
			map.put("success", true);
			map.put("data", list);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public DataTransferService getDataTransferService() {
		return dataTransferService;
	}

	public void setDataTransferService(DataTransferService dataTransferService) {
		this.dataTransferService = dataTransferService;
	}

	public CommonService getCommonService() {
		return commonService;
	}

	public void setCommonService(CommonService commonService) {
		this.commonService = commonService;
	}

	/**
	 * @return the dataTransfer
	 */
	public DataTransfer getDataTransfer() {
		return dataTransfer;
	}

	/**
	 * @param dataTransfer
	 *            the dataTransfer to set
	 */
	public void setDataTransfer(DataTransfer dataTransfer) {
		this.dataTransfer = dataTransfer;
	}
}
