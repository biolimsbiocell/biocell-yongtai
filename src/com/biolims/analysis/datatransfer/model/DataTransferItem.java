package com.biolims.analysis.datatransfer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 报告明细
 * @author lims-platform
 * @date 2016-11-03 15:08:51
 * @version V1.0
 * 
 */
@Entity
@Table(name = "DATA_TRANSFER_ITEM")
@SuppressWarnings("serial")
public class DataTransferItem extends EntityDao<DataTransferItem> implements
		java.io.Serializable {
	/** id */
	private String id;
	/** 样本编号*/
	private String sampleCode;
	/** 原始样本名称 */
	private String sampleName;
	/** 内部项目号 */
	private String inwardCode;
	/** fc编号 */
	private String flowCell;
	/** lane编号 */
	private String laneCode;
	/** 相关主表 */
	private DataTransfer dataTransfer;
	/**数据传输方式*/
	private String transferWay;
	/**FTP账户*/
	private String ftp;
	/** 备注 */
	private String note;

	/**
	 * 方法: 取得String
	 * 
	 * @return: String id
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 255)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the sampleCode
	 */
	@Column(name = "SAMPLE_CODE", length = 32)
	public String getSampleCode() {
		return sampleCode;
	}

	/**
	 * @param sampleCode the sampleCode to set
	 */
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	/**
	 * @return the sampleName
	 */
	@Column(name = "SAMPLE_NAME", length = 32)
	public String getSampleName() {
		return sampleName;
	}

	/**
	 * @param sampleName the sampleName to set
	 */
	public void setSampleName(String sampleName) {
		this.sampleName = sampleName;
	}

	/**
	 * @return the inwardCode
	 */
	@Column(name = "INWARD_CODE", length = 32)
	public String getInwardCode() {
		return inwardCode;
	}

	/**
	 * @param inwardCode the inwardCode to set
	 */
	public void setInwardCode(String inwardCode) {
		this.inwardCode = inwardCode;
	}

	/**
	 * @return the flowCell
	 */
	@Column(name = "FLOW_CELL", length = 32)
	public String getFlowCell() {
		return flowCell;
	}

	/**
	 * @param flowCell the flowCell to set
	 */
	public void setFlowCell(String flowCell) {
		this.flowCell = flowCell;
	}

	/**
	 * @return the laneCode
	 */
	@Column(name = "LANE_CODE", length = 32)
	public String getLaneCode() {
		return laneCode;
	}

	/**
	 * @param laneCode the laneCode to set
	 */
	public void setLaneCode(String laneCode) {
		this.laneCode = laneCode;
	}

	/**
	 * @return the dataTransfer
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DATA_TRANSFER")
	public DataTransfer getDataTransfer() {
		return dataTransfer;
	}

	/**
	 * @param dataTransfer the dataTransfer to set
	 */
	public void setDataTransfer(DataTransfer dataTransfer) {
		this.dataTransfer = dataTransfer;
	}

	/**
	 * @return the transferWay
	 */
	@Column(name = "TRANSFER_WAY", length = 32)
	public String getTransferWay() {
		return transferWay;
	}

	/**
	 * @param transferWay the transferWay to set
	 */
	public void setTransferWay(String transferWay) {
		this.transferWay = transferWay;
	}

	/**
	 * @return the ftp
	 */
	@Column(name = "FTP", length = 32)
	public String getFtp() {
		return ftp;
	}

	/**
	 * @param ftp the ftp to set
	 */
	public void setFtp(String ftp) {
		this.ftp = ftp;
	}

	/**
	 * @return the note
	 */
	@Column(name = "NOTE", length = 32)
	public String getNote() {
		return note;
	}

	/**
	 * @param note the note to set
	 */
	public void setNote(String note) {
		this.note = note;
	}
	
}