package com.biolims.analysis.datatransfer.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.analysis.data.model.DataTask;
import com.biolims.common.model.user.User;
import com.biolims.core.model.user.UserGroup;
import com.biolims.crm.project.model.Project;
import com.biolims.dao.EntityDao;
import com.biolims.file.model.FileInfo;
import com.biolims.system.template.model.Template;

@Entity
@Table(name = "DATA_TRANSFER")
@SuppressWarnings("serial")
public class DataTransfer extends EntityDao<DataTask> implements java.io.Serializable{
	/**编号*/
	private String id;
	/** 描述 */
	private String name;
	/**任务单号*/
	private String orderId;
	/**项目名称*/
	private Project project;
	/**实验模板*/
	private Template template;
	/**实验组*/
	private UserGroup userGroup;
	/**实验员*/
	private User tester;
	/**创建人*/
	private User createUser;
	/**创建时间*/
	private Date createDate;
	/** 状态 */
	private String state;
	/** 状态名称 */
	private String stateName;
	/**附件*/
	private FileInfo fileInfo;
	/**备注*/
	private String note;
	@Id
	@Column(name = "ID", length = 60)
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PROJECT")
	public Project getProject() {
		return project;
	}
	public void setProject(Project project) {
		this.project = project;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TEMPLATE")
	public Template getTemplate() {
		return template;
	}
	public void setTemplate(Template template) {
		this.template = template;
	}
	
	/**
	 * @return the createUser
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return createUser;
	}
	/**
	 * @param createUser the createUser to set
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}
	/**
	 * @return the createDate
	 */
	@Column(name = "CREATE_DATE", length = 30)
	public Date getCreateDate() {
		return createDate;
	}
	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	/**
	 * @return the orderId
	 */
	@Column(name="ORDER_ID",length=32)
	public String getOrderId() {
		return orderId;
	}
	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "USER_GROUP")
	public UserGroup getUserGroup() {
		return userGroup;
	}
	public void setUserGroup(UserGroup userGroup) {
		this.userGroup = userGroup;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TESTER")
	public User getTester() {
		return tester;
	}
	public void setTester(User tester) {
		this.tester = tester;
	}

	@Column(name = "NAME", length = 60)
	public String getName() {
		return name;
	}
	/**
	 * @return the fileInfo
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "FILE_INFO")
	public FileInfo getFileInfo() {
		return fileInfo;
	}
	/**
	 * @param fileInfo the fileInfo to set
	 */
	public void setFileInfo(FileInfo fileInfo) {
		this.fileInfo = fileInfo;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Column(name = "STATE", length = 60)
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@Column(name = "STATE_NAME", length = 60)
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	@Column(name = "NOTE", length = 400)
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
	
}
