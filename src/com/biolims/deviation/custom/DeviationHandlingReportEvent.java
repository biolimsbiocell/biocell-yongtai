package com.biolims.deviation.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.deviation.service.DeviationHandlingReportService;

public class DeviationHandlingReportEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext.getServletContext());
		DeviationHandlingReportService mbService = (DeviationHandlingReportService) ctx
				.getBean("deviationHandlingReportService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}