﻿
package com.biolims.deviation.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.deviation.model.DeviationHandlingReport;
import com.biolims.deviation.model.InfluenceEquipment;
import com.biolims.deviation.model.InfluenceMateriel;
import com.biolims.deviation.model.InfluenceProduct;
import com.biolims.deviation.service.DeviationHandlingReportService;
import com.biolims.dic.model.DicType;
import com.biolims.dic.service.DicTypeService;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.service.SampleSearchService;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Namespace("/deviation/deviationHandlingReport")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class DeviationHandlingReportAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "dhr001";
	@Autowired
	private DeviationHandlingReportService deviationHandlingReportService;
	@Autowired
	private SampleSearchService sampleSearchService;
	private DeviationHandlingReport deviationHandlingReport = new DeviationHandlingReport();
	@Autowired
	private DicTypeService dicService;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CommonService commonService;
	@Resource
	private FieldService fieldService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private CommonDAO commonDAO;
	private String log="";

	@Action(value = "showDeviationHandlingReportList")
	public String showDeviationHandlingReportList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/deviation/deviationHandlingReport.jsp");
	}

	@Action(value = "showDeviationHandlingReportEditList")
	public String showDeviationHandlingReportEditList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/deviation/deviationHandlingReportEditList.jsp");
	}

	@Action(value = "showDeviationHandlingReportTableJson")
	public void showDeviationHandlingReportTableJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = deviationHandlingReportService.findDeviationHandlingReportTable(start, length,
					query, col, sort);
			Long count = (Long) result.get("total");
			List<DeviationHandlingReport> list = (List<DeviationHandlingReport>) result.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("stateName", "");
			map.put("happenDepartment", "");
			map.put("happenDate", "yyyy-MM-dd");
			map.put("happenAddress", "");
			map.put("discoverer-id", "");
			map.put("discoverer-name", "");
			map.put("correctiveActionId", "");
			map.put("correctiveActionName", "");
			map.put("kindId", "");
			map.put("kindName", "");
			map.put("reportUser-id", "");
			map.put("reportUser-name", "");
			map.put("reportDate", "yyyy-MM-dd");
			map.put("departmentUser-id", "");
			map.put("departmentUser-name", "");
			map.put("departmentDate", "yyyy-MM-dd");
			map.put("monitor-id", "");
			map.put("monitor-name", "");
			map.put("monitoringDate", "yyyy-MM-dd");
			map.put("influence", "");
			map.put("type", "");
			map.put("groupLeader-id", "");
			map.put("groupLeader-name", "");
			map.put("groupMembers", "");
			map.put("no", "");

			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("DeviationHandlingReport");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "deviationHandlingReportSelectTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogDeviationHandlingReportList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/deviation/deviationHandlingReportSelectTable.jsp");
	}

	@Action(value = "editDeviationHandlingReport")
	public String editDeviationHandlingReport() throws Exception {
		String id = getParameterFromRequest("id");
		String barCod = getParameterFromRequest("barCod");

		long num = 0;
		if (id != null && !id.equals("")) {
			sampleSearchService.insertLog(barCod, "偏差处理明细");
			deviationHandlingReport = deviationHandlingReportService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "deviationHandlingReport");
		} else {
			deviationHandlingReport.setId("NEW");
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			deviationHandlingReport.setCreateUser(user);
			deviationHandlingReport.setCreateDate(new Date());
			deviationHandlingReport.setState(SystemConstants.DIC_STATE_NEW);
			deviationHandlingReport.setStateName(SystemConstants.DIC_STATE_NEW_NAME);

			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		List<DicType> kList = dicService.findDicTypeSelectList("kind_checkbox");
		List<DicType> aList = dicService.findDicTypeSelectList("action_checkbox");
		putObjToContext("kList", kList);
		putObjToContext("aList", aList);
		putObjToContext("kind", deviationHandlingReport.getKindId());
		putObjToContext("action", deviationHandlingReport.getCorrectiveActionId());
		putObjToContext("fileNum", num);
		// toState(deviationHandlingReport.getState());
		return dispatcher("/WEB-INF/page/deviation/deviationHandlingReportEdit.jsp");
	}

	@Action(value = "copyDeviationHandlingReport")
	public String copyDeviationHandlingReport() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		deviationHandlingReport = deviationHandlingReportService.get(id);
		deviationHandlingReport.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/deviation/deviationHandlingReportEdit.jsp");
	}

	@Action(value = "save")
	public void save() throws Exception {
		String msg = "";
		String zId = "";
		String kind = "";
		String action = "";
		boolean bool = true;

		try {
			String dataValue = getParameterFromRequest("dataValue");
			String checkBoxJsonStr = getParameterFromRequest("checkBoxJsonStr");
			JSONObject fromObject = JSONObject.fromObject(checkBoxJsonStr);
			JSONArray kindArray = (JSONArray) fromObject.get("kind");
			JSONArray actonArray = (JSONArray) fromObject.get("action");
			if (kindArray != null && kindArray.size() > 0) {
				for (int i = 0; i < kindArray.size(); i++) {
					kind += kindArray.get(i);
					if ((i + 1) != kindArray.size()) {
						kind += ",";
					}
				}
			}
			if (actonArray != null && actonArray.size() > 0) {
				for (int i = 0; i < actonArray.size(); i++) {
					action += actonArray.get(i);
					if ((i + 1) != actonArray.size()) {
						action += ",";
					}
				}
			}

			String str = "[" + dataValue + "]";
			String changeLog = getParameterFromRequest("changeLog");
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str, List.class);

			for (Map<String, Object> map : list) {
				deviationHandlingReport = (DeviationHandlingReport) commonDAO.Map2Bean(map, deviationHandlingReport);
			}
			String id = deviationHandlingReport.getId();
			if (id != null && id.equals("") || id.equals("NEW")) {
				log="123";
				String modelName = "DeviationHandlingReport";
				String markCode = "DHR";
				Date date = new Date();
				DateFormat format = new SimpleDateFormat("yy");
				String stime = format.format(date);
				String autoID = codingRuleService.getCodeByPrefix(modelName, markCode, stime, 000000, 6, null);
				deviationHandlingReport.setId(autoID);
			}
			deviationHandlingReport.setKindId(kind);
			deviationHandlingReport.setCorrectiveActionId(action);
			Map aMap = new HashMap();
			Map lMap = new HashMap();
			aMap.put("influenceProduct", getParameterFromRequest("influenceProductJson"));

			aMap.put("influenceMateriel", getParameterFromRequest("influenceMaterielJson"));

			aMap.put("influenceEquipment", getParameterFromRequest("influenceEquipmentJson"));

			deviationHandlingReportService.save(deviationHandlingReport, aMap, changeLog, lMap,log);

			zId = deviationHandlingReport.getId();
		} catch (Exception e) {
			e.printStackTrace();
			msg = "保存失败！";
			bool = false;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("msg", msg);
		map.put("success", bool);
		map.put("id", zId);

		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		if (bpmTaskId != null && !bpmTaskId.equals("") && !bpmTaskId.equals("null")) {
			map.put("bpmTaskId", bpmTaskId);

		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "viewDeviationHandlingReport")
	public String toViewDeviationHandlingReport() throws Exception {
		String id = getParameterFromRequest("id");
		deviationHandlingReport = deviationHandlingReportService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/deviation/deviationHandlingReportEdit.jsp");
	}

	@Action(value = "showInfluenceProductTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showInfluenceProductTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = deviationHandlingReportService.findInfluenceProductTable(start, length, query, col,
				sort, id);
		List<InfluenceProduct> list = (List<InfluenceProduct>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("orderCode", "");
		map.put("note", "");
		map.put("deviationHandlingReport-name", "");
		map.put("deviationHandlingReport-id", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delInfluenceProduct")
	public void delInfluenceProduct() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			deviationHandlingReportService.delInfluenceProduct(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "saveInfluenceProductTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveInfluenceProductTable() throws Exception {
		Map aMap = new HashMap();
		Map lMap = new HashMap();

		String id = getParameterFromRequest("id");
		deviationHandlingReport = commonService.get(DeviationHandlingReport.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		aMap.put("influenceProduct", getParameterFromRequest("dataJson"));
		lMap.put("influenceProduct", getParameterFromRequest("changeLog"));
		try {
			deviationHandlingReportService.save(deviationHandlingReport, aMap, "", lMap,log);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showInfluenceMaterielTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showInfluenceMaterielTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = deviationHandlingReportService.findInfluenceMaterielTable(start, length, query,
				col, sort, id);
		List<InfluenceMateriel> list = (List<InfluenceMateriel>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("materielId", "");
		map.put("materielName", "");
		map.put("batchNumber", "");
		map.put("note", "");
		map.put("deviationHandlingReport-name", "");
		map.put("deviationHandlingReport-id", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delInfluenceMateriel")
	public void delInfluenceMateriel() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			deviationHandlingReportService.delInfluenceMateriel(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "saveInfluenceMaterielTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveInfluenceMaterielTable() throws Exception {
		Map aMap = new HashMap();
		Map lMap = new HashMap();

		String id = getParameterFromRequest("id");
		deviationHandlingReport = commonService.get(DeviationHandlingReport.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		aMap.put("influenceMateriel", getParameterFromRequest("dataJson"));
		lMap.put("influenceMateriel", getParameterFromRequest("changeLog"));
		try {
			deviationHandlingReportService.save(deviationHandlingReport, aMap, "", lMap,log);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showInfluenceEquipmentTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showInfluenceEquipmentTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = deviationHandlingReportService.findInfluenceEquipmentTable(start, length, query,
				col, sort, id);
		List<InfluenceEquipment> list = (List<InfluenceEquipment>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("equipmentId", "");
		map.put("equipmentName", "");
		map.put("note", "");
		map.put("deviationHandlingReport-name", "");
		map.put("deviationHandlingReport-id", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delInfluenceEquipment")
	public void delInfluenceEquipment() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			deviationHandlingReportService.delInfluenceEquipment(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "saveInfluenceEquipmentTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveInfluenceEquipmentTable() throws Exception {
		Map aMap = new HashMap();
		Map lMap = new HashMap();

		String id = getParameterFromRequest("id");
		deviationHandlingReport = commonService.get(DeviationHandlingReport.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		aMap.put("influenceEquipment", getParameterFromRequest("dataJson"));
		lMap.put("influenceEquipment", getParameterFromRequest("changeLog"));
		try {
			deviationHandlingReportService.save(deviationHandlingReport, aMap, "", lMap,log);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "saveDeviationHandlingReportTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveDeviationHandlingReportTable() throws Exception {
		String dataValue = getParameterFromRequest("dataJson");
		String changeLog = getParameterFromRequest("changeLog");

		String str = "[" + dataValue + "]";

		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str, List.class);

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			for (Map<String, Object> map1 : list) {

				DeviationHandlingReport a = new DeviationHandlingReport();

				Map aMap = new HashMap();
				Map lMap = new HashMap();
				a = (DeviationHandlingReport) commonDAO.Map2Bean(map1, a);

				deviationHandlingReportService.save(a, aMap, changeLog, lMap,log);
				map.put("id", a.getId());
			}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public DeviationHandlingReportService getDeviationHandlingReportService() {
		return deviationHandlingReportService;
	}

	public void setDeviationHandlingReportService(DeviationHandlingReportService deviationHandlingReportService) {
		this.deviationHandlingReportService = deviationHandlingReportService;
	}

	public DeviationHandlingReport getDeviationHandlingReport() {
		return deviationHandlingReport;
	}

	public void setDeviationHandlingReport(DeviationHandlingReport deviationHandlingReport) {
		this.deviationHandlingReport = deviationHandlingReport;
	}

}
