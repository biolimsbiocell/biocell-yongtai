package com.biolims.deviation.plan.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;

/**   
 * @Title: Model
 * @Description: 计划影响的产品
 * @author lims-platform
 * @date 2019-04-22 17:00:45
 * @version V1.0   
 *
 */
@Entity
@Table(name = "CORRECTIVE_PREVENTIVE")
@SuppressWarnings("serial")
public class CorrectivePreventive extends EntityDao<CorrectivePreventive> implements java.io.Serializable {
	/**id*/
	private String id;
	/**描述*/
	private String name;
	/**备注*/
	private String note;
	/**偏差处理报告主表*/
	private DeviationSurveyPlan deviationSurveyPlan;
	/**其他*/
	private String other;
	/**序号*/
	private String number;
	/**执行工作*/
	private String excuteWork;
	/**负责人*/
	private User chargeUser;
	/**完成时限*/
	private Date completeDate;
	
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getExcuteWork() {
		return excuteWork;
	}
	public void setExcuteWork(String excuteWork) {
		this.excuteWork = excuteWork;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CHARGE_USER")
	public User getChargeUser() {
		return chargeUser;
	}
	public void setChargeUser(User chargeUser) {
		this.chargeUser = chargeUser;
	}
	public Date getCompleteDate() {
		return completeDate;
	}
	public void setCompleteDate(Date completeDate) {
		this.completeDate = completeDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  id
	 */
	
	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name ="ID", length = 200)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 200)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 200)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得DeviationSurveyPlan
	 *@return: DeviationSurveyPlan  偏差处理报告主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DEVIATION_SURVEY_PLAN")
	public DeviationSurveyPlan getDeviationSurveyPlan(){
		return this.deviationSurveyPlan;
	}
	/**
	 *方法: 设置DeviationSurveyPlan
	 *@param: DeviationSurveyPlan  偏差处理报告主表
	 */
	public void setDeviationSurveyPlan(DeviationSurveyPlan deviationSurveyPlan){
		this.deviationSurveyPlan = deviationSurveyPlan;
	}
	/**
	 *方法: 取得String
	 *@return: String  其他
	 */
	@Column(name ="OTHER", length = 200)
	public String getOther(){
		return this.other;
	}
	/**
	 *方法: 设置String
	 *@param: String  其他
	 */
	public void setOther(String other){
		this.other = other;
	}
}


/*


//中文JS配置文件
biolims.planInfluenceProduct={};	
biolims.planInfluenceProduct.id="id";
biolims.planInfluenceProduct.name="描述";
biolims.planInfluenceProduct.note="备注";
biolims.planInfluenceProduct.deviationSurveyPlan="偏差处理报告主表";
biolims.planInfluenceProduct.other="其他";




//英文JS配置文件
biolims.planInfluenceProduct.id="id";
biolims.planInfluenceProduct.name="name";
biolims.planInfluenceProduct.note="note";
biolims.planInfluenceProduct.deviationSurveyPlan="deviationSurveyPlan";
biolims.planInfluenceProduct.other="other";


//中文配置文件
biolims.planInfluenceProduct.id=id
biolims.planInfluenceProduct.name=描述
biolims.planInfluenceProduct.note=备注
biolims.planInfluenceProduct.deviationSurveyPlan=偏差处理报告主表
biolims.planInfluenceProduct.other=其他


//英文配置文件
biolims.planInfluenceProduct.id=id
biolims.planInfluenceProduct.name=name
biolims.planInfluenceProduct.note=note
biolims.planInfluenceProduct.deviationSurveyPlan=deviationSurveyPlan
biolims.planInfluenceProduct.other=other

*/