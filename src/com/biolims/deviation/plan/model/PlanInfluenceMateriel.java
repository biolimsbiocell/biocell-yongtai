package com.biolims.deviation.plan.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.deviation.plan.model.*;

/**   
 * @Title: Model
 * @Description: 计划影响的物料
 * @author lims-platform
 * @date 2019-04-22 17:00:46
 * @version V1.0   
 *
 */
@Entity
@Table(name = "PLAN_INFLUENCE_MATERIEL")
@SuppressWarnings("serial")
public class PlanInfluenceMateriel extends EntityDao<PlanInfluenceMateriel> implements java.io.Serializable {
	/**id*/
	private String id;
	/**描述*/
	private String name;
	/**备注*/
	private String note;
	/**偏差处理报告主表*/
	private DeviationSurveyPlan deviationSurveyPlan;
	/**其他*/
	private String other;
	/**物料编号*/
	private String materielId;
	/**物料名称*/
	private String materielName;
	/**批号*/
	private String batchNumber;
	
	
	public String getMaterielId() {
		return materielId;
	}
	public void setMaterielId(String materielId) {
		this.materielId = materielId;
	}
	public String getMaterielName() {
		return materielName;
	}
	public void setMaterielName(String materielName) {
		this.materielName = materielName;
	}
	public String getBatchNumber() {
		return batchNumber;
	}
	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}
	/**
	 *方法: 取得String
	 *@return: String  id
	 */
	
	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name ="ID", length = 200)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 200)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 200)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得DeviationSurveyPlan
	 *@return: DeviationSurveyPlan  偏差处理报告主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DEVIATION_SURVEY_PLAN")
	public DeviationSurveyPlan getDeviationSurveyPlan(){
		return this.deviationSurveyPlan;
	}
	/**
	 *方法: 设置DeviationSurveyPlan
	 *@param: DeviationSurveyPlan  偏差处理报告主表
	 */
	public void setDeviationSurveyPlan(DeviationSurveyPlan deviationSurveyPlan){
		this.deviationSurveyPlan = deviationSurveyPlan;
	}
	/**
	 *方法: 取得String
	 *@return: String  其他
	 */
	@Column(name ="OTHER", length = 200)
	public String getOther(){
		return this.other;
	}
	/**
	 *方法: 设置String
	 *@param: String  其他
	 */
	public void setOther(String other){
		this.other = other;
	}
}


/*


//中文JS配置文件
biolims.planInfluenceMateriel={};	
biolims.planInfluenceMateriel.id="id";
biolims.planInfluenceMateriel.name="描述";
biolims.planInfluenceMateriel.note="备注";
biolims.planInfluenceMateriel.deviationSurveyPlan="偏差处理报告主表";
biolims.planInfluenceMateriel.other="其他";




//英文JS配置文件
biolims.planInfluenceMateriel.id="id";
biolims.planInfluenceMateriel.name="name";
biolims.planInfluenceMateriel.note="note";
biolims.planInfluenceMateriel.deviationSurveyPlan="deviationSurveyPlan";
biolims.planInfluenceMateriel.other="other";


//中文配置文件
biolims.planInfluenceMateriel.id=id
biolims.planInfluenceMateriel.name=描述
biolims.planInfluenceMateriel.note=备注
biolims.planInfluenceMateriel.deviationSurveyPlan=偏差处理报告主表
biolims.planInfluenceMateriel.other=其他


//英文配置文件
biolims.planInfluenceMateriel.id=id
biolims.planInfluenceMateriel.name=name
biolims.planInfluenceMateriel.note=note
biolims.planInfluenceMateriel.deviationSurveyPlan=deviationSurveyPlan
biolims.planInfluenceMateriel.other=other

*/