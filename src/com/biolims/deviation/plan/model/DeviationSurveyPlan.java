package com.biolims.deviation.plan.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.core.model.user.UserGroup;
import com.biolims.dao.EntityDao;
import com.biolims.deviation.model.DeviationHandlingReport;

/**
 * @Title: Model
 * @Description: 偏差调查及措施计划
 * @author lims-platform
 * @date 2019-04-22 17:00:50
 * @version V1.0
 *
 */
@Entity
@Table(name = "DEVIATION_SURVEY_PLAN")
@SuppressWarnings("serial")
public class DeviationSurveyPlan extends EntityDao<DeviationSurveyPlan> implements java.io.Serializable {
	/** id */
	private String id;
	/** 描述 */
	private String name;
	/** 创建人 */
	private User createUser;
	/** 创建时间 */
	private Date createDate;
	/** 状态 */
	private String state;
	/** 状态名称 */
	private String stateName;
	/** 偏差编号 */
	private String no;
	/** 序列号 */
	private String serialNumber;
	/** 原因调查 */
	private String reason;
	/** 组员 */
	private String groupMembers;
	/** 组长 */
	private User groupLeader;
	/** 部门负责人 */
	private User departmentUser;
	/** Qa负责人审核意见 */
	private String auditOpinion;
	/** Qa负责人审核日期 */
	private Date auditDate;
	/** Qa负责人审核人 */
	private User auditUser;
	/** 批准意见 */
	private String approvalOpinion;
	/** 批准人 */
	private User approvalUser;
	/** 批准日期 */
	private Date approvalDate;
	/** 小组 */
	private UserGroup userGroup;
	
	/** 偏差处理 */
	private DeviationHandlingReport deviationHr;
	
	/** Qa审核意见 */
	private String auditQaOpinion;
	/** Qa审核日期 */
	private Date auditQaDate;
	/** Qa审核人 */
	private User auditQaUser;
	
	
	/** 批准人 */
	private User approverUser;
	/** 批准人意见 */
	private String approverUserOpinion;
	/** 批准人批准日期 */
	private Date approverUserDate;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "approver_user")
	public User getApproverUser() {
		return approverUser;
	}

	public void setApproverUser(User approverUser) {
		this.approverUser = approverUser;
	}

	public String getApproverUserOpinion() {
		return approverUserOpinion;
	}

	public void setApproverUserOpinion(String approverUserOpinion) {
		this.approverUserOpinion = approverUserOpinion;
	}

	public Date getApproverUserDate() {
		return approverUserDate;
	}

	public void setApproverUserDate(Date approverUserDate) {
		this.approverUserDate = approverUserDate;
	}

	public String getAuditQaOpinion() {
		return auditQaOpinion;
	}

	public void setAuditQaOpinion(String auditQaOpinion) {
		this.auditQaOpinion = auditQaOpinion;
	}

	public Date getAuditQaDate() {
		return auditQaDate;
	}

	public void setAuditQaDate(Date auditQaDate) {
		this.auditQaDate = auditQaDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "AUDIT_QA_USER")
	public User getAuditQaUser() {
		return auditQaUser;
	}

	public void setAuditQaUser(User auditQaUser) {
		this.auditQaUser = auditQaUser;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DEVIATION_HR")
	public DeviationHandlingReport getDeviationHr() {
		return deviationHr;
	}

	public void setDeviationHr(DeviationHandlingReport deviationHr) {
		this.deviationHr = deviationHr;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String id
	 */

	@Id
	@Column(name = "ID", length = 200)
	public String getId() {
		return this.id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	public UserGroup getUserGroup() {
		return userGroup;
	}

	public void setUserGroup(UserGroup userGroup) {
		this.userGroup = userGroup;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 200)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User
	 *             创建人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 创建时间
	 */
	@Column(name = "CREATE_DATE", length = 200)
	public Date getCreateDate() {
		return this.createDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date
	 *             创建时间
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 200)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态名称
	 */
	@Column(name = "STATE_NAME", length = 200)
	public String getStateName() {
		return this.stateName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             状态名称
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 偏差编号
	 */
	@Column(name = "NO", length = 200)
	public String getNo() {
		return this.no;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             偏差编号
	 */
	public void setNo(String no) {
		this.no = no;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 序列号
	 */
	@Column(name = "SERIAL_NUMBER", length = 200)
	public String getSerialNumber() {
		return this.serialNumber;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             序列号
	 */
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 原因调查
	 */
	@Column(name = "REASON", length = 200)
	public String getReason() {
		return this.reason;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             原因调查
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 组员
	 */
	@Column(name = "GROUP_MEMBERS", length = 200)
	public String getGroupMembers() {
		return this.groupMembers;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             组员
	 */
	public void setGroupMembers(String groupMembers) {
		this.groupMembers = groupMembers;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 组长
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GROUP_LEADER")
	public User getGroupLeader() {
		return this.groupLeader;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User
	 *             组长
	 */
	public void setGroupLeader(User groupLeader) {
		this.groupLeader = groupLeader;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 部门负责人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DEPARTMENT_USER")
	public User getDepartmentUser() {
		return this.departmentUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User
	 *             部门负责人
	 */
	public void setDepartmentUser(User departmentUser) {
		this.departmentUser = departmentUser;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 审核意见
	 */
	@Column(name = "AUDIT_OPINION", length = 200)
	public String getAuditOpinion() {
		return this.auditOpinion;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             审核意见
	 */
	public void setAuditOpinion(String auditOpinion) {
		this.auditOpinion = auditOpinion;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 审核日期
	 */
	@Column(name = "AUDIT_DATE", length = 200)
	public Date getAuditDate() {
		return this.auditDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date
	 *             审核日期
	 */
	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 审核人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "AUDIT_USER")
	public User getAuditUser() {
		return this.auditUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User
	 *             审核人
	 */
	public void setAuditUser(User auditUser) {
		this.auditUser = auditUser;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 批准意见
	 */
	@Column(name = "APPROVAL_OPINION", length = 200)
	public String getApprovalOpinion() {
		return this.approvalOpinion;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             批准意见
	 */
	public void setApprovalOpinion(String approvalOpinion) {
		this.approvalOpinion = approvalOpinion;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 批准人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "APPROVAL_USER")
	public User getApprovalUser() {
		return this.approvalUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User
	 *             批准人
	 */
	public void setApprovalUser(User approvalUser) {
		this.approvalUser = approvalUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 批准日期
	 */
	@Column(name = "APPROVAL_DATE", length = 200)
	public Date getApprovalDate() {
		return this.approvalDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date
	 *             批准日期
	 */
	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}
}

/*
 * 
 * 
 * //中文JS配置文件 biolims.deviationSurveyPlan={};
 * biolims.deviationSurveyPlan.id="id"; biolims.deviationSurveyPlan.name="描述";
 * biolims.deviationSurveyPlan.createUser="创建人";
 * biolims.deviationSurveyPlan.createDate="创建时间";
 * biolims.deviationSurveyPlan.state="状态";
 * biolims.deviationSurveyPlan.stateName="状态名称";
 * biolims.deviationSurveyPlan.no="偏差编号";
 * biolims.deviationSurveyPlan.serialNumber="序列号";
 * biolims.deviationSurveyPlan.reason="原因调查";
 * biolims.deviationSurveyPlan.groupMembers="组员";
 * biolims.deviationSurveyPlan.groupLeader="组长";
 * biolims.deviationSurveyPlan.departmentUser="部门负责人";
 * biolims.deviationSurveyPlan.auditOpinion="审核意见";
 * biolims.deviationSurveyPlan.auditDate="审核日期";
 * biolims.deviationSurveyPlan.auditUser="审核人";
 * biolims.deviationSurveyPlan.approvalOpinion="批准意见";
 * biolims.deviationSurveyPlan.approvalUser="批准人";
 * biolims.deviationSurveyPlan.approvalDate="批准日期";
 * 
 * 
 * 
 * 
 * //英文JS配置文件 biolims.deviationSurveyPlan.id="id";
 * biolims.deviationSurveyPlan.name="name";
 * biolims.deviationSurveyPlan.createUser="createUser";
 * biolims.deviationSurveyPlan.createDate="createDate";
 * biolims.deviationSurveyPlan.state="state";
 * biolims.deviationSurveyPlan.stateName="stateName";
 * biolims.deviationSurveyPlan.no="no";
 * biolims.deviationSurveyPlan.serialNumber="serialNumber";
 * biolims.deviationSurveyPlan.reason="reason";
 * biolims.deviationSurveyPlan.groupMembers="groupMembers";
 * biolims.deviationSurveyPlan.groupLeader="groupLeader";
 * biolims.deviationSurveyPlan.departmentUser="departmentUser";
 * biolims.deviationSurveyPlan.auditOpinion="auditOpinion";
 * biolims.deviationSurveyPlan.auditDate="auditDate";
 * biolims.deviationSurveyPlan.auditUser="auditUser";
 * biolims.deviationSurveyPlan.approvalOpinion="approvalOpinion";
 * biolims.deviationSurveyPlan.approvalUser="approvalUser";
 * biolims.deviationSurveyPlan.approvalDate="approvalDate";
 * 
 * 
 * //中文配置文件 biolims.deviationSurveyPlan.id=id
 * biolims.deviationSurveyPlan.name=描述
 * biolims.deviationSurveyPlan.createUser=创建人
 * biolims.deviationSurveyPlan.createDate=创建时间
 * biolims.deviationSurveyPlan.state=状态
 * biolims.deviationSurveyPlan.stateName=状态名称
 * biolims.deviationSurveyPlan.no=偏差编号
 * biolims.deviationSurveyPlan.serialNumber=序列号
 * biolims.deviationSurveyPlan.reason=原因调查
 * biolims.deviationSurveyPlan.groupMembers=组员
 * biolims.deviationSurveyPlan.groupLeader=组长
 * biolims.deviationSurveyPlan.departmentUser=部门负责人
 * biolims.deviationSurveyPlan.auditOpinion=审核意见
 * biolims.deviationSurveyPlan.auditDate=审核日期
 * biolims.deviationSurveyPlan.auditUser=审核人
 * biolims.deviationSurveyPlan.approvalOpinion=批准意见
 * biolims.deviationSurveyPlan.approvalUser=批准人
 * biolims.deviationSurveyPlan.approvalDate=批准日期
 * 
 * 
 * //英文配置文件 biolims.deviationSurveyPlan.id=id
 * biolims.deviationSurveyPlan.name=name
 * biolims.deviationSurveyPlan.createUser=createUser
 * biolims.deviationSurveyPlan.createDate=createDate
 * biolims.deviationSurveyPlan.state=state
 * biolims.deviationSurveyPlan.stateName=stateName
 * biolims.deviationSurveyPlan.no=no
 * biolims.deviationSurveyPlan.serialNumber=serialNumber
 * biolims.deviationSurveyPlan.reason=reason
 * biolims.deviationSurveyPlan.groupMembers=groupMembers
 * biolims.deviationSurveyPlan.groupLeader=groupLeader
 * biolims.deviationSurveyPlan.departmentUser=departmentUser
 * biolims.deviationSurveyPlan.auditOpinion=auditOpinion
 * biolims.deviationSurveyPlan.auditDate=auditDate
 * biolims.deviationSurveyPlan.auditUser=auditUser
 * biolims.deviationSurveyPlan.approvalOpinion=approvalOpinion
 * biolims.deviationSurveyPlan.approvalUser=approvalUser
 * biolims.deviationSurveyPlan.approvalDate=approvalDate
 * 
 */