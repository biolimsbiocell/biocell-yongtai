﻿
package com.biolims.deviation.plan.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.core.model.user.UserGroup;
import com.biolims.core.model.user.UserGroupUser;
import com.biolims.deviation.model.DeviationHandlingReport;
import com.biolims.deviation.model.InfluenceEquipment;
import com.biolims.deviation.model.InfluenceMateriel;
import com.biolims.deviation.model.InfluenceProduct;
import com.biolims.deviation.plan.model.CorrectivePreventive;
import com.biolims.deviation.plan.model.DeviationSurveyPlan;
import com.biolims.deviation.plan.model.PlanInfluenceEquipment;
import com.biolims.deviation.plan.model.PlanInfluenceMateriel;
import com.biolims.deviation.plan.model.PlanInfluenceProduct;
import com.biolims.deviation.plan.service.DeviationSurveyPlanService;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

import sun.util.logging.resources.logging;

@Namespace("/deviation/plan/deviationSurveyPlan")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class DeviationSurveyPlanAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "dhr002";
	@Autowired
	private DeviationSurveyPlanService deviationSurveyPlanService;
	private DeviationSurveyPlan deviationSurveyPlan = new DeviationSurveyPlan();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CommonService commonService;
	@Resource
	private FieldService fieldService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private CommonDAO commonDAO;
	private String log="";

	@Action(value = "showDeviationSurveyPlanList")
	public String showDeviationSurveyPlanList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/deviation/plan/deviationSurveyPlan.jsp");
	}

	@Action(value = "showDeviationSurveyPlanEditList")
	public String showDeviationSurveyPlanEditList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/deviation/plan/deviationSurveyPlanEditList.jsp");
	}

	@Action(value = "showDeviationSurveyPlanTableJson")
	public void showDeviationSurveyPlanTableJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = deviationSurveyPlanService.findDeviationSurveyPlanTable(start, length, query,
					col, sort);
			Long count = (Long) result.get("total");
			List<DeviationSurveyPlan> list = (List<DeviationSurveyPlan>) result.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("stateName", "");
			map.put("no", "");
			map.put("serialNumber", "");
			map.put("reason", "");
			map.put("groupMembers", "");
			map.put("groupLeader-id", "");
			map.put("groupLeader-name", "");
			map.put("departmentUser-id", "");
			map.put("departmentUser-name", "");
			map.put("auditOpinion", "");
			map.put("auditDate", "yyyy-MM-dd");
			map.put("auditUser-id", "");
			map.put("auditUser-name", "");
			map.put("approvalOpinion", "");
			map.put("approvalUser-id", "");
			map.put("approvalUser-name", "");
			map.put("approvalDate", "yyyy-MM-dd");

			map.put("deviationHr-no", "");
			
			map.put("deviationHr-name", "");
			map.put("deviationHr-type", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("DeviationSurveyPlan");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "deviationSurveyPlanSelectTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogDeviationSurveyPlanList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/deviation/plan/deviationSurveyPlanSelectTable.jsp");
	}

	@Action(value = "editDeviationSurveyPlan")
	public String editDeviationSurveyPlan() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			deviationSurveyPlan = deviationSurveyPlanService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "deviationSurveyPlan");
		} else {
			deviationSurveyPlan.setId("NEW");
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			deviationSurveyPlan.setCreateUser(user);
			deviationSurveyPlan.setCreateDate(new Date());
			deviationSurveyPlan.setState(SystemConstants.DIC_STATE_NEW);
			deviationSurveyPlan.setStateName(SystemConstants.DIC_STATE_NEW_NAME);

			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/deviation/plan/deviationSurveyPlanEdit.jsp");
	}

	@Action(value = "copyDeviationSurveyPlan")
	public String copyDeviationSurveyPlan() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		deviationSurveyPlan = deviationSurveyPlanService.get(id);
		deviationSurveyPlan.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/deviation/plan/deviationSurveyPlanEdit.jsp");
	}

	@Action(value = "save")
	public void save() throws Exception {

		String msg = "";
		String zId = "";
		boolean bool = true;

		try {
			String dataValue = getParameterFromRequest("dataValue");
			String str = "[" + dataValue + "]";
			String changeLog = getParameterFromRequest("changeLog");
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str, List.class);

			for (Map<String, Object> map : list) {
				deviationSurveyPlan = (DeviationSurveyPlan) commonDAO.Map2Bean(map, deviationSurveyPlan);
			}
			String id = deviationSurveyPlan.getId();
			if ((id != null && id.equals("")) || id.equals("NEW")) {
				log="123";
				String modelName = "DeviationSurveyPlan";
				String markCode = "DSP";
				Date date = new Date();
				DateFormat format = new SimpleDateFormat("yy");
				String stime = format.format(date);
				String autoID = codingRuleService.getCodeByPrefix(modelName, markCode, stime, 000000, 6, null);

				deviationSurveyPlan.setId(autoID);
			}
			Map aMap = new HashMap();
			Map lMap = new HashMap();
			aMap.put("correctivePreventive", getParameterFromRequest("correctivePreventiveJson"));

			aMap.put("planInfluenceProduct", getParameterFromRequest("planInfluenceProductJson"));

			aMap.put("planInfluenceMateriel", getParameterFromRequest("planInfluenceMaterielJson"));

			aMap.put("planInfluenceEquipment", getParameterFromRequest("planInfluenceEquipmentJson"));

			deviationSurveyPlanService.save(deviationSurveyPlan, aMap, changeLog, lMap,log);

			zId = deviationSurveyPlan.getId();
		} catch (Exception e) {
			e.printStackTrace();
			msg = "保存失败！";
			bool = false;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("msg", msg);
		map.put("success", bool);
		map.put("id", zId);

		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		if (bpmTaskId != null && !bpmTaskId.equals("") && !bpmTaskId.equals("null")) {
			map.put("bpmTaskId", bpmTaskId);

		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "viewDeviationSurveyPlan")
	public String toViewDeviationSurveyPlan() throws Exception {
		String id = getParameterFromRequest("id");
		deviationSurveyPlan = deviationSurveyPlanService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/deviation/plan/deviationSurveyPlanEdit.jsp");
	}

	@Action(value = "showCorrectivePreventiveTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showCorrectivePreventiveTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = deviationSurveyPlanService.findCorrectivePreventiveTable(start, length, query, col,
				sort, id);
		List<CorrectivePreventive> list = (List<CorrectivePreventive>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("number", "");
		map.put("excuteWork", "");
		map.put("chargeUser-id", "");
		map.put("chargeUser-name", "");
		map.put("completeDate", "yyyy-MM-dd");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	@Action(value = "showPlanInfluenceProductTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showPlanInfluenceProductTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
//		Map<String, Object> result = deviationSurveyPlanService.findPlanInfluenceProductTable(start, length, query, col,
//				sort, id);
//		List<PlanInfluenceProduct> list = (List<PlanInfluenceProduct>) result.get("list");
		
		Map<String, Object> result = deviationSurveyPlanService.findInfluenceProductTable(start, length, query, col,
		sort, id);
		List<InfluenceProduct> list = (List<InfluenceProduct>) result.get("list");
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("note", "");
//		map.put("deviationSurveyPlan-name", "");
//		map.put("deviationSurveyPlan-id", "");
		map.put("orderCode", "");
//		map.put("other", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delPlanInfluenceProduct")
	public void delPlanInfluenceProduct() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			deviationSurveyPlanService.delPlanInfluenceProduct(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "saveCorrectivePreventiveTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveCorrectivePreventiveTable() throws Exception {
		Map aMap = new HashMap();
		Map lMap = new HashMap();

		String id = getParameterFromRequest("id");
		deviationSurveyPlan = commonService.get(DeviationSurveyPlan.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		aMap.put("correctivePreventive", getParameterFromRequest("dataJson"));
		lMap.put("correctivePreventive", getParameterFromRequest("changeLog"));
		try {
			deviationSurveyPlanService.save(deviationSurveyPlan, aMap, "", lMap,log);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "savePlanInfluenceProductTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void savePlanInfluenceProductTable() throws Exception {
		Map aMap = new HashMap();
		Map lMap = new HashMap();

		String id = getParameterFromRequest("id");
		deviationSurveyPlan = commonService.get(DeviationSurveyPlan.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		aMap.put("planInfluenceProduct", getParameterFromRequest("dataJson"));
		lMap.put("planInfluenceProduct", getParameterFromRequest("changeLog"));
		try {
			deviationSurveyPlanService.save(deviationSurveyPlan, aMap, "", lMap,log);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showPlanInfluenceMaterielTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showPlanInfluenceMaterielTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
//		Map<String, Object> result = deviationSurveyPlanService.findPlanInfluenceMaterielTable(start, length, query,
//				col, sort, id);
//		List<PlanInfluenceMateriel> list = (List<PlanInfluenceMateriel>) result.get("list");
		Map<String, Object> result = deviationSurveyPlanService.findInfluenceMaterielTable(start, length, query,
				col, sort, id);
		List<InfluenceMateriel> list = (List<InfluenceMateriel>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("note", "");
//		map.put("deviationSurveyPlan-name", "");
//		map.put("deviationSurveyPlan-id", "");
		map.put("materielId", "");
		map.put("materielName", "");
		map.put("batchNumber", "");
//		map.put("other", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delPlanInfluenceMateriel")
	public void delPlanInfluenceMateriel() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			deviationSurveyPlanService.delPlanInfluenceMateriel(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "savePlanInfluenceMaterielTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void savePlanInfluenceMaterielTable() throws Exception {
		Map aMap = new HashMap();
		Map lMap = new HashMap();

		String id = getParameterFromRequest("id");
		deviationSurveyPlan = commonService.get(DeviationSurveyPlan.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		aMap.put("planInfluenceMateriel", getParameterFromRequest("dataJson"));
		lMap.put("planInfluenceMateriel", getParameterFromRequest("changeLog"));
		try {
			deviationSurveyPlanService.save(deviationSurveyPlan, aMap, "", lMap,log);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showPlanInfluenceEquipmentTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showPlanInfluenceEquipmentTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
//		Map<String, Object> result = deviationSurveyPlanService.findPlanInfluenceEquipmentTable(start, length, query,
//				col, sort, id);
//		List<PlanInfluenceEquipment> list = (List<PlanInfluenceEquipment>) result.get("list");
		Map<String, Object> result = deviationSurveyPlanService.findInfluenceEquipmentTable(start, length, query,
				col, sort, id);
		List<InfluenceEquipment> list = (List<InfluenceEquipment>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("note", "");
//		map.put("deviationSurveyPlan-name", "");
//		map.put("deviationSurveyPlan-id", "");
		map.put("equipmentId", "");
		map.put("equipmentName", "");
//		map.put("other", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delPlanInfluenceEquipment")
	public void delPlanInfluenceEquipment() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			deviationSurveyPlanService.delPlanInfluenceEquipment(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "savePlanInfluenceEquipmentTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void savePlanInfluenceEquipmentTable() throws Exception {
		Map aMap = new HashMap();
		Map lMap = new HashMap();

		String id = getParameterFromRequest("id");
		deviationSurveyPlan = commonService.get(DeviationSurveyPlan.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		aMap.put("planInfluenceEquipment", getParameterFromRequest("dataJson"));
		lMap.put("planInfluenceEquipment", getParameterFromRequest("changeLog"));
		try {
			deviationSurveyPlanService.save(deviationSurveyPlan, aMap, "", lMap,log);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "saveDeviationSurveyPlanTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveDeviationSurveyPlanTable() throws Exception {
		String dataValue = getParameterFromRequest("dataJson");
		String changeLog = getParameterFromRequest("changeLog");

		String str = "[" + dataValue + "]";

		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str, List.class);

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			for (Map<String, Object> map1 : list) {

				DeviationSurveyPlan a = new DeviationSurveyPlan();

				Map aMap = new HashMap();
				Map lMap = new HashMap();
				a = (DeviationSurveyPlan) commonDAO.Map2Bean(map1, a);

				deviationSurveyPlanService.save(a, aMap, changeLog, lMap,log);
				map.put("id", a.getId());
			}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	
	/**
	 * 
	 * @Title: 
	 * @Description:单选偏差处理报告
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "selectDeviationHandlingReportTable")
	public String selectDeviationHandlingReportTable() throws Exception {
		return dispatcher("/WEB-INF/page/deviation/plan/selDeviationHandlingReportList.jsp");
	}

	@Action(value = "selectDeviationHandlingReportTableJson")
	public void selectDeviationHandlingReportTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = deviationSurveyPlanService.selectDeviationHandlingReportTableJson(start,
				length, query, col, sort);
		List<DeviationHandlingReport> list = (List<DeviationHandlingReport>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("no", "");
		map.put("type", "");
		
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}
	
	

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public DeviationSurveyPlanService getDeviationSurveyPlanService() {
		return deviationSurveyPlanService;
	}

	public void setDeviationSurveyPlanService(DeviationSurveyPlanService deviationSurveyPlanService) {
		this.deviationSurveyPlanService = deviationSurveyPlanService;
	}

	public DeviationSurveyPlan getDeviationSurveyPlan() {
		return deviationSurveyPlan;
	}

	public void setDeviationSurveyPlan(DeviationSurveyPlan deviationSurveyPlan) {
		this.deviationSurveyPlan = deviationSurveyPlan;
	}

	@Action(value = "selUserGroupList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String selUserGroupList() throws Exception {

		return dispatcher("/WEB-INF/page/deviation/plan/selUserGroupList.jsp");
	}

	@Action(value = "selUserGroupTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selUserGroupTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = deviationSurveyPlanService.selUserGroupTableJson(start, length, query, col, sort);
		List<UserGroup> list = (List<UserGroup>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}
}
