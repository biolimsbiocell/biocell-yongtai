package com.biolims.deviation.plan.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.deviation.model.DeviationHandlingReport;
import com.biolims.deviation.model.InfluenceEquipment;
import com.biolims.deviation.model.InfluenceMateriel;
import com.biolims.deviation.model.InfluenceProduct;
import com.biolims.deviation.plan.dao.DeviationSurveyPlanDao;
import com.biolims.deviation.plan.model.CorrectivePreventive;
import com.biolims.deviation.plan.model.DeviationSurveyPlan;
import com.biolims.deviation.plan.model.PlanInfluenceEquipment;
import com.biolims.deviation.plan.model.PlanInfluenceMateriel;
import com.biolims.deviation.plan.model.PlanInfluenceProduct;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class DeviationSurveyPlanService {
	@Resource
	private DeviationSurveyPlanDao deviationSurveyPlanDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findDeviationSurveyPlanTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return deviationSurveyPlanDao.findDeviationSurveyPlanTable(start, length, query, col, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(DeviationSurveyPlan i) throws Exception {

		deviationSurveyPlanDao.saveOrUpdate(i);

	}

	public DeviationSurveyPlan get(String id) {
		DeviationSurveyPlan deviationSurveyPlan = commonDAO.get(DeviationSurveyPlan.class, id);
		return deviationSurveyPlan;
	}

	public Map<String, Object> findPlanInfluenceProductTable(Integer start, Integer length, String query, String col,
			String sort, String id) throws Exception {
		Map<String, Object> result = deviationSurveyPlanDao.findPlanInfluenceProductTable(start, length, query, col,
				sort, id);
		List<PlanInfluenceProduct> list = (List<PlanInfluenceProduct>) result.get("list");
		return result;
	}
	
	public Map<String, Object> findInfluenceProductTable(Integer start, Integer length, String query, String col,
			String sort, String id) throws Exception {
		Map<String, Object> result = deviationSurveyPlanDao.findInfluenceProductTable(start, length, query, col,
				sort, id);
		List<InfluenceProduct> list = (List<InfluenceProduct>) result.get("list");
		return result;
	}


	public Map<String, Object> findPlanInfluenceMaterielTable(Integer start, Integer length, String query, String col,
			String sort, String id) throws Exception {
		Map<String, Object> result = deviationSurveyPlanDao.findPlanInfluenceMaterielTable(start, length, query, col,
				sort, id);
		List<PlanInfluenceMateriel> list = (List<PlanInfluenceMateriel>) result.get("list");
		return result;
	}
	
	public Map<String, Object> findInfluenceMaterielTable(Integer start, Integer length, String query, String col,
			String sort, String id) throws Exception {
		Map<String, Object> result = deviationSurveyPlanDao.findInfluenceMaterielTable(start, length, query, col,
				sort, id);
		List<InfluenceMateriel> list = (List<InfluenceMateriel>) result.get("list");
		return result;
	}

	public Map<String, Object> findPlanInfluenceEquipmentTable(Integer start, Integer length, String query, String col,
			String sort, String id) throws Exception {
		Map<String, Object> result = deviationSurveyPlanDao.findPlanInfluenceEquipmentTable(start, length, query, col,
				sort, id);
		List<PlanInfluenceEquipment> list = (List<PlanInfluenceEquipment>) result.get("list");
		return result;
	}
	
	public Map<String, Object> findInfluenceEquipmentTable(Integer start, Integer length, String query, String col,
			String sort, String id) throws Exception {
		Map<String, Object> result = deviationSurveyPlanDao.findInfluenceEquipmentTable(start, length, query, col,
				sort, id);
		List<InfluenceEquipment> list = (List<InfluenceEquipment>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePlanInfluenceProduct(DeviationSurveyPlan sc, String itemDataJson, String logInfo,String log) throws Exception {
		List<PlanInfluenceProduct> saveItems = new ArrayList<PlanInfluenceProduct>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			PlanInfluenceProduct scp = new PlanInfluenceProduct();
			// 将map信息读入实体类
			scp = (PlanInfluenceProduct) deviationSurveyPlanDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setDeviationSurveyPlan(sc);

			saveItems.add(scp);
		}
		deviationSurveyPlanDao.saveOrUpdateAll(saveItems);
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("DeviationSurveyPlan");
			li.setModifyContent(logInfo);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPlanInfluenceProduct(String[] ids) throws Exception {
		for (String id : ids) {
			PlanInfluenceProduct scp = deviationSurveyPlanDao.get(PlanInfluenceProduct.class, id);
			deviationSurveyPlanDao.delete(scp);
			if (ids.length > 0) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setClassName("DeviationSurveyPlan");
				li.setFileId(scp.getId());
				li.setModifyContent("PlanInfluenceProduct删除信息" + ids.toString());
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}
		}

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePlanInfluenceMateriel(DeviationSurveyPlan sc, String itemDataJson, String logInfo,String log)
			throws Exception {
		List<PlanInfluenceMateriel> saveItems = new ArrayList<PlanInfluenceMateriel>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			PlanInfluenceMateriel scp = new PlanInfluenceMateriel();
			// 将map信息读入实体类
			scp = (PlanInfluenceMateriel) deviationSurveyPlanDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setDeviationSurveyPlan(sc);

			saveItems.add(scp);
		}
		deviationSurveyPlanDao.saveOrUpdateAll(saveItems);
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("DeviationSurveyPlan");
			li.setModifyContent(logInfo);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPlanInfluenceMateriel(String[] ids) throws Exception {
		for (String id : ids) {
			PlanInfluenceMateriel scp = deviationSurveyPlanDao.get(PlanInfluenceMateriel.class, id);
			deviationSurveyPlanDao.delete(scp);
			if (ids.length > 0) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(scp.getId());
				li.setClassName("DeviationSurveyPlan");
				li.setModifyContent("PlanInfluenceMateriel删除信息" + ids.toString());
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}
		}

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePlanInfluenceEquipment(DeviationSurveyPlan sc, String itemDataJson, String logInfo,String log)
			throws Exception {
		List<PlanInfluenceEquipment> saveItems = new ArrayList<PlanInfluenceEquipment>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			PlanInfluenceEquipment scp = new PlanInfluenceEquipment();
			// 将map信息读入实体类
			scp = (PlanInfluenceEquipment) deviationSurveyPlanDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setDeviationSurveyPlan(sc);

			saveItems.add(scp);
		}
		deviationSurveyPlanDao.saveOrUpdateAll(saveItems);
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("DeviationSurveyPlan");
			li.setModifyContent(logInfo);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPlanInfluenceEquipment(String[] ids) throws Exception {
		for (String id : ids) {
			PlanInfluenceEquipment scp = deviationSurveyPlanDao.get(PlanInfluenceEquipment.class, id);
			deviationSurveyPlanDao.delete(scp);
			if (ids.length > 0) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(scp.getId());
				li.setClassName("DeviationSurveyPlan");
				li.setModifyContent("PlanInfluenceEquipment删除信息" + ids.toString());
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}
		}

	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(DeviationSurveyPlan sc, Map jsonMap, String logInfo, Map logMap,String log) throws Exception {
		if (sc != null) {
			deviationSurveyPlanDao.saveOrUpdate(sc);

			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(sc.getId());
				li.setClassName("DeviationSurveyPlan");
				li.setModifyContent(logInfo);
				if("123".equals(log)) {
					li.setState("1");
					li.setStateName("数据新增");
				}else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				commonDAO.saveOrUpdate(li);
			}

			String jsonStr = "";
			String logStr = "";
			logStr = (String) logMap.get("correctivePreventive");
			jsonStr = (String) jsonMap.get("correctivePreventive");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")&& !jsonStr.equals("[]")) {
				saveCorrectivePreventive(sc, jsonStr, logStr,log);
			}
			logStr = (String) logMap.get("planInfluenceProduct");
			jsonStr = (String) jsonMap.get("planInfluenceProduct");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")&& !jsonStr.equals("[]")) {
				savePlanInfluenceProduct(sc, jsonStr, logStr,log);
			}
			logStr = (String) logMap.get("planInfluenceMateriel");
			jsonStr = (String) jsonMap.get("planInfluenceMateriel");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")&& !jsonStr.equals("[]")) {
				savePlanInfluenceMateriel(sc, jsonStr, logStr,log);
			}
			logStr = (String) logMap.get("planInfluenceEquipment");
			jsonStr = (String) jsonMap.get("planInfluenceEquipment");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")&& !jsonStr.equals("[]")) {
				savePlanInfluenceEquipment(sc, jsonStr, logStr,log);
			}
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveCorrectivePreventive(DeviationSurveyPlan sc, String itemDataJson, String logInfo,String log)
			throws Exception {
		List<CorrectivePreventive> saveItems = new ArrayList<CorrectivePreventive>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			CorrectivePreventive scp = new CorrectivePreventive();
			// 将map信息读入实体类
			scp = (CorrectivePreventive) deviationSurveyPlanDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setDeviationSurveyPlan(sc);

			saveItems.add(scp);
		}
		deviationSurveyPlanDao.saveOrUpdateAll(saveItems);
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("DeviationSurveyPlan");
			li.setModifyContent(logInfo);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			commonDAO.saveOrUpdate(li);
		}
	}

	public Map<String, Object> findCorrectivePreventiveTable(Integer start, Integer length, String query, String col,
			String sort, String id) {
		return deviationSurveyPlanDao.findCorrectivePreventiveTable(start, length, query, col, sort, id);
	}

	public Map<String, Object> selUserGroupTableJson(Integer start, Integer length, String query, String col,
			String sort) {
		return deviationSurveyPlanDao.selUserGroupTableJson(start, length, query, col, sort);
	}

	public Map<String, Object> selectDeviationHandlingReportTableJson(
			Integer start, Integer length, String query, String col, String sort) {
		return deviationSurveyPlanDao.selectDeviationHandlingReportTableJson(start, length, query, col, sort);
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String contentId) {
		DeviationSurveyPlan sct = commonDAO.get(DeviationSurveyPlan.class, contentId);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		
		if(sct.getDeviationHr()!=null){
			DeviationHandlingReport dhr = commonDAO.get(DeviationHandlingReport.class, sct.getDeviationHr().getId());
			//偏差完成
			dhr.setApprovalStatus("1");
			commonDAO.saveOrUpdate(dhr);
		}
		
		commonDAO.saveOrUpdate(sct);
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveD(DeviationSurveyPlan deviationSurveyPlan) {
		commonDAO.saveOrUpdate(deviationSurveyPlan);
	}
	
	
}
