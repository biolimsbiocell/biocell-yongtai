package com.biolims.deviation.plan.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.deviation.plan.service.DeviationSurveyPlanService;
import com.biolims.deviation.service.DeviationHandlingReportService;

public class DeviationSurveyPlanEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext.getServletContext());
		DeviationSurveyPlanService mbService = (DeviationSurveyPlanService) ctx
				.getBean("deviationSurveyPlanService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}