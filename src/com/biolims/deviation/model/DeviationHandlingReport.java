package com.biolims.deviation.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 偏差处理报告
 * @author lims-platform
 * @date 2019-04-22 13:24:10
 * @version V1.0
 *
 */
@Entity
@Table(name = "DEVIATION_HANDLING_REPORT")
@SuppressWarnings("serial")
public class DeviationHandlingReport extends EntityDao<DeviationHandlingReport> implements java.io.Serializable {
	/** id */
	private String id;
	/** 描述 */
	private String name;
	/** 创建人 */
	private User createUser;
	/** 创建时间 */
	private Date createDate;
	/** 状态 */
	private String state;
	/** 状态名称 */
	private String stateName;
	/** 发生部门 */
	private String happenDepartment;
	/** 发生时间 */
	private Date happenDate;
	/** 发生地点 */
	private String happenAddress;
	/** 发现人 */
	private User discoverer;
	/** 紧急纠正措施id */
	private String correctiveActionId;
	/** 紧急纠正措施 */
	private String correctiveActionName;
	/** 偏差种类id */
	private String kindId;
	/** 偏差种类 */
	private String kindName;
	/** 偏差报告人 */
	private User reportUser;
	/** 偏差报告日期 */
	private Date reportDate;
	/** 部门负责人 */
	private User departmentUser;
	/** 部门负责日期 */
	private Date departmentDate;
	/** 监控人 */
	private User monitor;
	/** 监控日期 */
	private Date monitoringDate;
	/** 影响 */
	private String influence;
	/** 偏差分类 */
	private String type;
	/** 调查组长 */
	private User groupLeader;
	/** 调查组组员ID */
	private String groupMemberIds;
	/** 调查组组员 */
	private String groupMembers;
	/** 偏差编号 */
	private String no;
	
	/** 审批状态 1审批完成 */
	private String approvalStatus;

	
	
	public String getApprovalStatus() {
		return approvalStatus;
	}

	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String id
	 */

	@Id
	@Column(name = "ID", length = 200)
	public String getId() {
		return this.id;
	}

	public String getGroupMemberIds() {
		return groupMemberIds;
	}

	public void setGroupMemberIds(String groupMemberIds) {
		this.groupMemberIds = groupMemberIds;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 200)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User
	 *             创建人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 创建时间
	 */
	@Column(name = "CREATE_DATE", length = 200)
	public Date getCreateDate() {
		return this.createDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date
	 *             创建时间
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE", length = 200)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态名称
	 */
	@Column(name = "STATE_NAME", length = 200)
	public String getStateName() {
		return this.stateName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             状态名称
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 发生部门
	 */
	@Column(name = "HAPPEN_DEPARTMENT", length = 200)
	public String getHappenDepartment() {
		return this.happenDepartment;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             发生部门
	 */
	public void setHappenDepartment(String happenDepartment) {
		this.happenDepartment = happenDepartment;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 发生时间
	 */
	@Column(name = "HAPPEN_DATE", length = 200)
	public Date getHappenDate() {
		return this.happenDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date
	 *             发生时间
	 */
	public void setHappenDate(Date happenDate) {
		this.happenDate = happenDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 发生地点
	 */
	@Column(name = "HAPPEN_ADDRESS", length = 200)
	public String getHappenAddress() {
		return this.happenAddress;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             发生地点
	 */
	public void setHappenAddress(String happenAddress) {
		this.happenAddress = happenAddress;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 发现人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DISCOVERER")
	public User getDiscoverer() {
		return this.discoverer;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User
	 *             发现人
	 */
	public void setDiscoverer(User discoverer) {
		this.discoverer = discoverer;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 紧急纠正措施id
	 */
	@Column(name = "CORRECTIVE_ACTION_ID", length = 200)
	public String getCorrectiveActionId() {
		return this.correctiveActionId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             紧急纠正措施id
	 */
	public void setCorrectiveActionId(String correctiveActionId) {
		this.correctiveActionId = correctiveActionId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 紧急纠正措施
	 */
	@Column(name = "CORRECTIVE_ACTION_NAME", length = 200)
	public String getCorrectiveActionName() {
		return this.correctiveActionName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             紧急纠正措施
	 */
	public void setCorrectiveActionName(String correctiveActionName) {
		this.correctiveActionName = correctiveActionName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 偏差种类id
	 */
	@Column(name = "KIND_ID", length = 200)
	public String getKindId() {
		return this.kindId;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             偏差种类id
	 */
	public void setKindId(String kindId) {
		this.kindId = kindId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 偏差种类
	 */
	@Column(name = "KIND_NAME", length = 200)
	public String getKindName() {
		return this.kindName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             偏差种类
	 */
	public void setKindName(String kindName) {
		this.kindName = kindName;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 偏差报告人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "REPORT_USER")
	public User getReportUser() {
		return this.reportUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User
	 *             偏差报告人
	 */
	public void setReportUser(User reportUser) {
		this.reportUser = reportUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 偏差报告日期
	 */
	@Column(name = "REPORT_DATE", length = 200)
	public Date getReportDate() {
		return this.reportDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date
	 *             偏差报告日期
	 */
	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 部门负责人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DEPARTMENT_USER")
	public User getDepartmentUser() {
		return this.departmentUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User
	 *             部门负责人
	 */
	public void setDepartmentUser(User departmentUser) {
		this.departmentUser = departmentUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 部门负责日期
	 */
	@Column(name = "DEPARTMENT_DATE", length = 200)
	public Date getDepartmentDate() {
		return this.departmentDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date
	 *             部门负责日期
	 */
	public void setDepartmentDate(Date departmentDate) {
		this.departmentDate = departmentDate;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 监控人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "MONITOR")
	public User getMonitor() {
		return this.monitor;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User
	 *             监控人
	 */
	public void setMonitor(User monitor) {
		this.monitor = monitor;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 监控日期
	 */
	@Column(name = "MONITORING_DATE", length = 200)
	public Date getMonitoringDate() {
		return this.monitoringDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date
	 *             监控日期
	 */
	public void setMonitoringDate(Date monitoringDate) {
		this.monitoringDate = monitoringDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 影响
	 */
	@Column(name = "INFLUENCE", length = 200)
	public String getInfluence() {
		return this.influence;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             影响
	 */
	public void setInfluence(String influence) {
		this.influence = influence;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 偏差分类
	 */
	@Column(name = "TYPE", length = 200)
	public String getType() {
		return this.type;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             偏差分类
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 调查组长
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GROUP_LEADER")
	public User getGroupLeader() {
		return this.groupLeader;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User
	 *             调查组长
	 */
	public void setGroupLeader(User groupLeader) {
		this.groupLeader = groupLeader;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 调查组组员
	 */
	@Column(name = "GROUP_MEMBERS", length = 200)
	public String getGroupMembers() {
		return this.groupMembers;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             调查组组员
	 */
	public void setGroupMembers(String groupMembers) {
		this.groupMembers = groupMembers;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 偏差编号
	 */
	@Column(name = "NO", length = 200)
	public String getNo() {
		return this.no;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             偏差编号
	 */
	public void setNo(String no) {
		this.no = no;
	}

	@Override
	public String toString() {
		return super.toString();
	}

}