package com.biolims.deviation.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.deviation.model.*;

/**   
 * @Title: Model
 * @Description: 影响的产品
 * @author lims-platform
 * @date 2019-04-22 13:24:05
 * @version V1.0   
 *
 */
@Entity
@Table(name = "INFLUENCE_PRODUCT")
@SuppressWarnings("serial")
public class InfluenceProduct extends EntityDao<InfluenceProduct> implements java.io.Serializable {
	/**id*/
	private String id;
	/**描述*/
	private String name;
	/**备注*/
	private String note;
	/**偏差处理报告主表*/
	private DeviationHandlingReport deviationHandlingReport;
	/**订单编号*/
	private String orderCode;
	
	
	public String getOrderCode() {
		return orderCode;
	}
	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  id
	 */
	
	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name ="ID", length = 200)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  id
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 200)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 200)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得DeviationHandlingReport
	 *@return: DeviationHandlingReport  偏差处理报告主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DEVIATION_HANDLING_REPORT")
	public DeviationHandlingReport getDeviationHandlingReport(){
		return this.deviationHandlingReport;
	}
	/**
	 *方法: 设置DeviationHandlingReport
	 *@param: DeviationHandlingReport  偏差处理报告主表
	 */
	public void setDeviationHandlingReport(DeviationHandlingReport deviationHandlingReport){
		this.deviationHandlingReport = deviationHandlingReport;
	}
}