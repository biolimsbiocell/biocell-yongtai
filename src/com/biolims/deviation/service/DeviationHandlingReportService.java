package com.biolims.deviation.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.deviation.dao.DeviationHandlingReportDao;
import com.biolims.deviation.model.DeviationHandlingReport;
import com.biolims.deviation.model.InfluenceEquipment;
import com.biolims.deviation.model.InfluenceMateriel;
import com.biolims.deviation.model.InfluenceProduct;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class DeviationHandlingReportService {
	@Resource
	private DeviationHandlingReportDao deviationHandlingReportDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findDeviationHandlingReportTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return deviationHandlingReportDao.findDeviationHandlingReportTable(start, length, query, col, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(DeviationHandlingReport i) throws Exception {

		deviationHandlingReportDao.saveOrUpdate(i);

	}

	public DeviationHandlingReport get(String id) {
		DeviationHandlingReport deviationHandlingReport = commonDAO.get(DeviationHandlingReport.class, id);
		return deviationHandlingReport;
	}

	public Map<String, Object> findInfluenceProductTable(Integer start, Integer length, String query, String col,
			String sort, String id) throws Exception {
		Map<String, Object> result = deviationHandlingReportDao.findInfluenceProductTable(start, length, query, col,
				sort, id);
		List<InfluenceProduct> list = (List<InfluenceProduct>) result.get("list");
		return result;
	}

	public Map<String, Object> findInfluenceMaterielTable(Integer start, Integer length, String query, String col,
			String sort, String id) throws Exception {
		Map<String, Object> result = deviationHandlingReportDao.findInfluenceMaterielTable(start, length, query, col,
				sort, id);
		List<InfluenceMateriel> list = (List<InfluenceMateriel>) result.get("list");
		return result;
	}

	public Map<String, Object> findInfluenceEquipmentTable(Integer start, Integer length, String query, String col,
			String sort, String id) throws Exception {
		Map<String, Object> result = deviationHandlingReportDao.findInfluenceEquipmentTable(start, length, query, col,
				sort, id);
		List<InfluenceEquipment> list = (List<InfluenceEquipment>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveInfluenceProduct(DeviationHandlingReport sc, String itemDataJson, String logInfo) throws Exception {
		List<InfluenceProduct> saveItems = new ArrayList<InfluenceProduct>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			InfluenceProduct scp = new InfluenceProduct();
			// 将map信息读入实体类
			scp = (InfluenceProduct) deviationHandlingReportDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setDeviationHandlingReport(sc);
			commonDAO.saveOrUpdate(scp);
		}
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("DeviationHandlingReport");
			li.setModifyContent(logInfo);
			li.setState("3");
			li.setStateName("数据修改");
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delInfluenceProduct(String[] ids) throws Exception {
		for (String id : ids) {
			InfluenceProduct scp = deviationHandlingReportDao.get(InfluenceProduct.class, id);
			deviationHandlingReportDao.delete(scp);
			if (ids.length > 0) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(scp.getId());
				li.setClassName("DeviationHandlingReport");
				li.setModifyContent("InfluenceProduct删除信息" + ids.toString());
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}
		}

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveInfluenceMateriel(DeviationHandlingReport sc, String itemDataJson, String logInfo)
			throws Exception {
		List<InfluenceMateriel> saveItems = new ArrayList<InfluenceMateriel>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			InfluenceMateriel scp = new InfluenceMateriel();
			// 将map信息读入实体类
			scp = (InfluenceMateriel) deviationHandlingReportDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setDeviationHandlingReport(sc);

			saveItems.add(scp);
		}
		deviationHandlingReportDao.saveOrUpdateAll(saveItems);
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("DeviationHandlingReport");
			li.setModifyContent(logInfo);
			li.setState("3");
			li.setStateName("数据修改");
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delInfluenceMateriel(String[] ids) throws Exception {
		for (String id : ids) {
			InfluenceMateriel scp = deviationHandlingReportDao.get(InfluenceMateriel.class, id);
			deviationHandlingReportDao.delete(scp);
			if (ids.length > 0) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(scp.getId());
				li.setClassName("DeviationHandlingReport");
				li.setModifyContent("InfluenceMateriel删除信息" + ids.toString());
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}
		}

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveInfluenceEquipment(DeviationHandlingReport sc, String itemDataJson, String logInfo)
			throws Exception {
		List<InfluenceEquipment> saveItems = new ArrayList<InfluenceEquipment>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			InfluenceEquipment scp = new InfluenceEquipment();
			// 将map信息读入实体类
			scp = (InfluenceEquipment) deviationHandlingReportDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setDeviationHandlingReport(sc);

			saveItems.add(scp);
		}
		deviationHandlingReportDao.saveOrUpdateAll(saveItems);
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("DeviationHandlingReport");
			li.setModifyContent(logInfo);
			li.setState("3");
			li.setStateName("数据修改");
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delInfluenceEquipment(String[] ids) throws Exception {
		for (String id : ids) {
			InfluenceEquipment scp = deviationHandlingReportDao.get(InfluenceEquipment.class, id);
			deviationHandlingReportDao.delete(scp);
			if (ids.length > 0) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(scp.getId());
				li.setClassName("DeviationHandlingReport");
				li.setModifyContent("InfluenceEquipment删除信息" + ids.toString());
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}
		}

	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(DeviationHandlingReport sc, Map jsonMap, String logInfo, Map logMap,String log) throws Exception {
		if (sc != null) {
			deviationHandlingReportDao.saveOrUpdate(sc);
			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(sc.getId());
				li.setClassName("DeviationHandlingReport");
				li.setModifyContent(logInfo);
				if("123".equals(log)) {
					li.setState("1");
					li.setStateName("数据新增");
				}else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				commonDAO.saveOrUpdate(li);
			}

			String jsonStr = "";
			String logStr = "";
			logStr = (String) logMap.get("influenceProduct");
			jsonStr = (String) jsonMap.get("influenceProduct");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveInfluenceProduct(sc, jsonStr, logStr);
			}
			logStr = (String) logMap.get("influenceMateriel");
			jsonStr = (String) jsonMap.get("influenceMateriel");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveInfluenceMateriel(sc, jsonStr, logStr);
			}
			logStr = (String) logMap.get("influenceEquipment");
			jsonStr = (String) jsonMap.get("influenceEquipment");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveInfluenceEquipment(sc, jsonStr, logStr);
			}
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String contentId) {
		DeviationHandlingReport sct = commonDAO.get(DeviationHandlingReport.class, contentId);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		commonDAO.saveOrUpdate(sct);
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveD(DeviationHandlingReport deviationHandlingReport) {
		commonDAO.saveOrUpdate(deviationHandlingReport);
	}
	

}
