package com.biolims.document.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.document.model.DocumentInfo;

@Repository
@SuppressWarnings("unchecked")
public class DocumentInfoDao extends BaseHibernateDao {
	public Map<String, Object> selectDocumentInfoList(Map<String, String> mapForQuery, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String key = "";
		String hql = "from DocumentInfo where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);

		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<DocumentInfo> list = new ArrayList<DocumentInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}//


}
