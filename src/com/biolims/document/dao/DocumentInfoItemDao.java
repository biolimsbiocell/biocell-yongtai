package com.biolims.document.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.document.model.DocumentInfo;
import com.biolims.document.model.DocumentInfoItem;
import com.biolims.experiment.sanger.model.SangerTaskTemp;
import com.biolims.sample.model.SampleOrder;

@Repository
@SuppressWarnings("unchecked")
public class DocumentInfoItemDao extends BaseHibernateDao {

	/**
	 * 根据父级id查询DocumentInfoItem集合
	 * @param id 父级id
	 * @return DocumentInfoItem集合
	 */
	public List<DocumentInfo> selectDocumentInfoItemToParent(String parentId,String key) {
		List<DocumentInfo> list;
		if(key==null || "".equals(key)){
			list = this.getSession().createQuery("from DocumentInfo where parent='"+parentId+"'").list();
		}else{
			list = this.getSession().createQuery("from DocumentInfo where parent is null").list();
		}
		return list;
	}

	public Map<String, Object> selectDocumentInfoItemList(String id) {
		
		String hql = "from DocumentInfoItem where 1=1 and documentInfo.id='"+id+"' order by id desc";
		
		Long total = (Long) this.getSession().createQuery(" select count(*) from DocumentInfoItem where 1=1 and documentInfo.id='"+id+"' order by id desc").uniqueResult();
		List<DocumentInfoItem> list = new ArrayList<DocumentInfoItem>();

		list = this.getSession().createQuery(hql).list();
		
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	
	public Map<String, Object> selectDocumentInfoItemNewList(Integer start,
			Integer length, String query, String col, String sort, String id) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from DocumentInfoItem where 1=1 ";
		String key = "";

		if("".equals(id)){
			key = key+" and 1=2 ";
		}else{
			key = key+" and documentInfo.id='"+id+"' ";
		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from DocumentInfoItem where 1=1 ";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<SampleOrder> list = new ArrayList<SampleOrder>();
			list=this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
	
}
