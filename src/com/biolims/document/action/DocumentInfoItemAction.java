package com.biolims.document.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.document.model.DocumentInfo;
import com.biolims.document.model.DocumentInfoItem;
import com.biolims.document.service.DocumentInfoItemService;
import com.biolims.experiment.sanger.model.SangerTask;
import com.biolims.sample.model.SampleOrder;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

/**
 * 文档管理
 * 
 * @author wangting
 * 
 */
@Namespace("/document/documentInfoItem")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public class DocumentInfoItemAction extends BaseActionSupport {
	
	private static final long serialVersionUID = 7234363909423733821L;
	// 该action权限id
	private String rightsId = "wdgl001";
	private DocumentInfoItem difi;
	
	private DocumentInfo dif;

	@Resource
	private DocumentInfoItemService documentInfoItemService;

	/**
	 * 批量保存文档基本信息
	 * @throws Exception
	 */
	@Action(value = "save")
	public void save() throws Exception {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String itemDataJson = getRequest().getParameter("itemDataJson");
			documentInfoItemService.save(itemDataJson);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	@Action(value = "showDocumentInfoItemTree")
	public String showDocumentInfoItemTree() throws Exception {

		putObjToContext(
				"path",
				ServletActionContext.getRequest().getContextPath()
						+ "/document/documentInfoItem/showDocumentItemTreeJson.action");
		
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		
		return dispatcher("/WEB-INF/page/document/documentInfo/showDocumentInfoItemTree.jsp");
	}
	
//	@Action(value = "showDocumentInfoItemListJson")
//	public void showDocumentInfoItemListJson(){
//		String id = getParameterFromRequest("id");
//	
//		Map<String, Object> result = documentInfoItemService.findDocumentInfoItemList(id);
//		Long count = (Long) result.get("total");
//		List<DocumentInfoItem> list = (List<DocumentInfoItem>) result.get("list");
//		
//		
//		Map<String, String> map = new HashMap<String, String>();
//		map.put("id", "");
//		map.put("fileName", "");
//		map.put("fileType", "");
//		map.put("uploadTime", "yyyy-MM-dd");
//		map.put("uploadUser-id", "");
//		map.put("uploadUser-name", "");
//		map.put("documentInfo-id", "");
//		map.put("versionNo", "");
//		map.put("attach-id", "");
//		map.put("attach-fileName", "");
//		
//		new SendData().sendDateJson(map, list, count,
//				ServletActionContext.getResponse());
//	}
	
	
	@Action(value = "showDocumentInfoItemListJson")
	public void showDocumentInfoItemListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = documentInfoItemService
				.findDocumentInfoItemNewList(start, length, query, col,
						sort, id);
		List<DocumentInfoItem> list = (List<DocumentInfoItem>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("fileName", "");
		map.put("fileType", "");
		map.put("uploadTime", "yyyy-MM-dd");
		map.put("uploadUser-id", "");
		map.put("uploadUser-name", "");
		map.put("documentInfo-id", "");
		map.put("versionNo", "");
		map.put("attach-id", "");
		map.put("attach-fileName", "");
		
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}
	
	
	
	
	@Action(value = "selectUser")
	public String selectUser(){
		return dispatcher("/WEB-INF/page/document/documentInfo/uploadUser.jsp");
	}
	
	@Action(value = "showDocumentItemTreeJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDocumentItemTreeJson() throws Exception {
		
		String parentId = getParameterFromRequest("id");
		
		List<DocumentInfo> list = null;
		String a = "";
		if ("document".equals(parentId)) {
			parentId = "";
			list = documentInfoItemService.findDocumentInfoItemToParent(parentId,"1");
			a = documentInfoItemService.getDocumentInfoItemJson(list, parentId,"1");
		}else{
			list = documentInfoItemService.findDocumentInfoItemToParent(parentId,"");
			a = documentInfoItemService.getDocumentInfoItemJson(list, parentId,"");
		}
		
		new SendData().sendDataJson(a, ServletActionContext.getResponse());

	}
	
	
	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public DocumentInfoItem getDifi() {
		return difi;
	}

	public void setDifi(DocumentInfoItem difi) {
		this.difi = difi;
	}

	public DocumentInfo getDif() {
		return dif;
	}

	public void setDif(DocumentInfo dif) {
		this.dif = dif;
	}

	public DocumentInfoItemService getDocumentInfoItemService() {
		return documentInfoItemService;
	}

	public void setDocumentInfoItemService(
			DocumentInfoItemService documentInfoItemService) {
		this.documentInfoItemService = documentInfoItemService;
	}
	
}
