package com.biolims.document.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.service.CommonService;
import com.biolims.core.model.user.Department;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.document.model.DocumentInfo;
import com.biolims.document.model.DocumentInfoItem;
import com.biolims.document.service.DocumentInfoItemService;
import com.biolims.document.service.DocumentInfoService;
import com.biolims.file.model.FileInfo;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.opensymphony.xwork2.ActionContext;

/**
 * 文档管理
 * 
 * @author wangting
 * 
 */
@Namespace("/document/documentInfo")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public class DocumentInfoAction extends BaseActionSupport {
	
	private static final long serialVersionUID = 7234363909423733821L;
	// 该action权限id
	private String rightsId = "";
	private DocumentInfo dif;
	

	@Resource
	private DocumentInfoService documentInfoService;
	
	@Resource
	private DocumentInfoItemService documentInfoItemService;
	@Resource
	private CommonService commonService;
	/**
	 * 文档管理的遍历
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showDocumentInfoList")
	public String showDocumentInfoList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/document/documentInfo/showDocumentInfoList.jsp");
	}
	
	/**
	 * 访问 任务树
	 * @throws Exception 
	 */
	@Action(value = "showDocumentInfoTree")
	public String showDocumentInfoTree() throws Exception{
		rightsId = "wdgl001";
//		putObjToContext("path", ServletActionContext.getRequest()
//				.getContextPath()
//				+ "/document/documentInfo/showDocumentInfoTreeJson.action");

		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		
		return dispatcher("/WEB-INF/page/document/documentInfo/showDocumentInfoTree.jsp");
	}
	
	
	@SuppressWarnings("null")
	@Action(value = "showDocumentInfoTreeJson")
	public void showDocumentInfoTreeJson() throws Exception {
		String upId = getParameterFromRequest("upId");
		try {
			List<DocumentInfo> list = documentInfoService.findDocumentInfoTreeList(upId);
			List<DocumentInfo> list2 = new ArrayList<DocumentInfo>();
			if(list.size()>0){
				for(DocumentInfo di : list){
					List<DocumentInfo> list3 = documentInfoService.findDocumentInfoTreeList(di.getId());
					if(list3.size()>0){
						di.setSubNode("yes");
					}else{
						di.setSubNode("no");
					}
					list2.add(di);
				}
			}
			HttpUtils.write(JsonUtils.toJsonString(list2));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 访问 富集选择树
	 * @throws Exception 
	 */
	@Action(value = "showDocumentInfoItemTree")
	public String showDocumentInfoItemTree() throws Exception{
		return dispatcher("/WEB-INF/page/document/documentInfo/showDocumentInfoItemTree.jsp");
	}
	
//	@Action(value = "showDocumentInfoTreeJson")
//	public void showDocumentInfoTreeJson() throws Exception {
//		
//		String upId = getParameterFromRequest("treegrid_id");
//		
//		List<DocumentInfo> aList = null;
//		
//		aList = documentInfoService.findDocumentInfoTreeList(upId);
//
//		String a = documentInfoService.getJson(aList);
//		
//		new SendData().sendDataJson(a, ServletActionContext.getResponse());
//	}
	
	/**
	 * 上传文件
	 * @return
	 */
	@Action(value = "showDocumentInfoItemList")
	public String showDocumentInfoItemList(){

		return dispatcher("/WEB-INF/page/document/documentInfo/editDocumentInfoItem.jsp");
	}

	@Action(value = "showDocumentInfoListJson")
	public void showDocumentInfoListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String queryData = getRequest().getParameter("data");
		try {
			Map<String, String> map2Query = new HashMap<String, String>();

			if (queryData != null && queryData.length() > 0)
				map2Query = JsonUtils.toObjectByJson(queryData, Map.class);

			Map<String, Object> result = documentInfoService.findDocumentInfoList(
					map2Query, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<DocumentInfo> list = (List<DocumentInfo>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("fileName", "");
			map.put("studyDirection-id", "");
			map.put("studyDirection-name", "");
			map.put("contentNote", "");
			map.put("filePath", "");
			map.put("fileType", "");
			map.put("uploadTime", "");
			map.put("uploadUser-id", "");
			map.put("uploadUser-name", "");
			map.put("fileNote", "");
			map.put("parent-id", "");
			map.put("parent-fileName", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	

	/**
	 * 主页面
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "toDocumentMainframe")
	public String toMainframe() throws Exception {
		rightsId = "wdgl002";
		String reqMethodType = getRequest().getParameter("reqMethodType");
		String id = getRequest().getParameter("id");
		if (id != null) {
			DocumentInfo documentInfo = documentInfoService.getDocumentInfoById(id);
			//reqMethodType = SystemConstants.PAGE_HANDLE_METHOD_VIEW;
		}
		getRequest().setAttribute("id", id);
		putObjToContext("handlemethod", reqMethodType);
		toToolBar(rightsId, "", "", reqMethodType);
		return dispatcher("/WEB-INF/page/document/documentInfo/documentMainFrame.jsp");
	}

	/**
	 * 文档管理中心编辑页面
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "editDocumentInfo")
	public String editDocumentInfo() throws Exception {
		rightsId = "wdgl002";
		String id = getRequest().getParameter("id");
		if (id != null && id.length() > 0) {
			dif = documentInfoService.getDocumentInfoById(id);
		} else {
			dif = new DocumentInfo();
			dif.setUploadTime(new Date());
		}
		String reqMethodType = getRequest().getParameter("reqMethodType");
		getRequest().setAttribute("id", id);
		putObjToContext("handlemethod", reqMethodType);
		toToolBar(rightsId, "", "", reqMethodType);
		return dispatcher("/WEB-INF/page/document/documentInfo/editDocumentInfo.jsp");
	}

	/**
	 * 保存
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "save")
	public String save() throws Exception {
		String changeLog = getParameterFromRequest("changeLog");
		String dataJson = getParameterFromRequest("documentInfoItemJson");
		
		dif.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
		dif.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
		
		if("".equals(dif.getId())){
			documentInfoService.save(dif,changeLog,dataJson);
		}else{
			documentInfoService.saveOrUpdate(dif,changeLog,dataJson);
		}
		
		return redirect("/document/documentInfo/editDocumentInfo.action?id=" + dif.getId()
				+ "&reqMethodType=" + SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
	}
	
	/**
	 * 批量保存文档基本信息
	 * @throws Exception
	 */
	@Action(value = "saveItem")
	public void saveItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = getParameterFromRequest("id");
		String changeLog = getParameterFromRequest("changeLog");
		String dataJson = getParameterFromRequest("dataJson");
		if(id!=null && !"".equals(id)){
			try {
				dif = commonService.get(DocumentInfo.class, id);
				documentInfoItemService.saveDocumentInfoItem(dif, dataJson,
							changeLog);
				map.put("success", true);
			}catch (Exception e) {
				e.printStackTrace();
				map.put("success", false);
			}
		}else{
			map.put("success", "error");
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public DocumentInfo getDif() {
		return dif;
	}

	public void setDif(DocumentInfo dif) {
		this.dif = dif;
	}

	public DocumentInfoService getDocumentInfoService() {
		return documentInfoService;
	}

	public void setDocumentInfoService(DocumentInfoService documentInfoService) {
		this.documentInfoService = documentInfoService;
	}
	
	
	@Action(value = "showDocumentInfoItemListJson")
	public void showDocumentInfoItemListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = documentInfoItemService
				.findDocumentInfoItemNewList(start, length, query, col,
						sort, id);
		List<DocumentInfoItem> list = (List<DocumentInfoItem>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("fileName", "");
		map.put("fileType", "");
		map.put("uploadTime", "yyyy-MM-dd");
		map.put("uploadUser-id", "");
		map.put("uploadUser-name", "");
		map.put("documentInfo-id", "");
		map.put("versionNo", "");
		map.put("attach-id", "");
		map.put("attach-fileName", "");
		
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}
	
	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delDocumentInfoItem")
	public void delDocumentInfoItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			documentInfoItemService.delDocumentInfoItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
}
