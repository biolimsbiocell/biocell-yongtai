package com.biolims.document.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.storage.position.model.StoragePosition;

/**
 * 文件信息
 *
 */
@Entity
@Table(name = "T_DOCUMENT_INFO")
public class DocumentInfo extends EntityDao<DocumentInfo> implements Serializable {

	private static final long serialVersionUID = -5234430496355539041L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@Column(name = "FILE_NAME", length = 100)
	private String fileName;//文件名称

	@Column(name = "CONTENT_NOTE", length = 200)
	private String contentNote;//文件说明

	@Column(name = "FILE_TYPE", length = 50)
	private String fileType;//文件类型 word，excel

	@Column(name = "FILE_PATH", length = 500)
	private String filePath;//文件路径

	@Column(name = "UPLOAD_TIME")
	private Date uploadTime;//上传时间

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "USER_UPLOAD_USER_ID")
	private User uploadUser;//上传人

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STUDY_DIRECTION")
	private DicType studyDirection;//研究方向

	@Column(name = "FILE_NOTE", length = 2000)
	private String fileNote;//文件用途说明

	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DOCUMENT_INFO_ID")
	private DocumentInfo parent;// 父级

	private String upId;
	
	private String subNode;
	
	
	/** 范围Id */
	private String scopeId;
	/** 范围 */
	private String scopeName;
	
	
	
	
	public String getScopeId() {
		return scopeId;
	}

	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	public String getScopeName() {
		return scopeName;
	}

	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

	public String getSubNode() {
		return subNode;
	}

	public void setSubNode(String subNode) {
		this.subNode = subNode;
	}

	public String getUpId() {
		return upId;
	}

	public void setUpId(String upId) {
		this.upId = upId;
	}

	public DocumentInfo getParent() {
		return parent;
	}

	public void setParent(DocumentInfo parent) {
		this.parent = parent;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Date getUploadTime() {
		return uploadTime;
	}

	public void setUploadTime(Date uploadTime) {
		this.uploadTime = uploadTime;
	}

	public User getUploadUser() {
		return uploadUser;
	}

	public void setUploadUser(User uploadUser) {
		this.uploadUser = uploadUser;
	}

	public String getContentNote() {
		return contentNote;
	}

	public void setContentNote(String contentNote) {
		this.contentNote = contentNote;
	}

	public String getFileNote() {
		return fileNote;
	}

	public void setFileNote(String fileNote) {
		this.fileNote = fileNote;
	}

	public DicType getStudyDirection() {
		return studyDirection;
	}

	public void setStudyDirection(DicType studyDirection) {
		this.studyDirection = studyDirection;
	}

}
