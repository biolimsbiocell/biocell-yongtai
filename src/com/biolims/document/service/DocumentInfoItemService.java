package com.biolims.document.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.crm.customer.linkman.model.CrmLinkManItem;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.document.dao.DocumentInfoItemDao;
import com.biolims.document.model.DocumentInfo;
import com.biolims.document.model.DocumentInfoItem;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.model.SampleReceiveItem;
import com.biolims.util.JsonUtils;

@Service
public class DocumentInfoItemService {
	@Resource
	private DocumentInfoItemDao documentInfoItemDao;

	StringBuffer json = new StringBuffer();

	private Object crmPatientDao;
	
	
	/**
	 * 批量保存或更新报告模板基本信息
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(String itemDataJson) throws Exception {
		List<DocumentInfoItem> saveItems = new ArrayList<DocumentInfoItem>();
		List<Map> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map map : list) {
			DocumentInfoItem rti = new DocumentInfoItem();
			rti = (DocumentInfoItem) documentInfoItemDao.Map2Bean(map, rti);
			saveItems.add(rti);
		}
		documentInfoItemDao.saveOrUpdateAll(saveItems);
	}


	public List<DocumentInfo> findDocumentInfoItemToParent(
			String parentId,String key) {
		return documentInfoItemDao.selectDocumentInfoItemToParent(parentId,key);
	}


	public String getDocumentInfoItemJson(List<DocumentInfo> list,
			String parentId,String key) {
		if (list != null && list.size() > 0) {
		}
		json = new StringBuffer();
		List<DocumentInfo> nodeList0 = new ArrayList<DocumentInfo>();
		Iterator<DocumentInfo> it1 = list.iterator();
		while (it1.hasNext()) {
			DocumentInfo node = (DocumentInfo) it1.next();
			// if (node.getLevel() == 0) {
			nodeList0.add(node);
			// }
		}
		Iterator<DocumentInfo> it = nodeList0.iterator();
		while (it.hasNext()) {
			DocumentInfo node = (DocumentInfo) it.next();
			constructorPhenoDatabaseJson(list, node,parentId,key);
		}
		String jsonDate = json.toString();
		return ("[" + jsonDate + "]").replaceAll(",]", "]");
	}


	public void constructorPhenoDatabaseJson(List<DocumentInfo> list,
			DocumentInfo treeNode, String parentId ,String key) {
		json.append("{id:\"");
		json.append(treeNode.getId() + "\"");
		json.append(",text:\"");
		json.append(treeNode.getFileName() + "\"");
		List<DocumentInfo> databaseListToParent = findDocumentInfoItemToParent(treeNode.getId(),key);
		if(databaseListToParent.size()>0 && databaseListToParent!=null){
			json.append(",leaf:false");
		}else{
			json.append(",leaf:true");
		}
		json.append(",checked:false");
		json.append("},");
		
	}


	public Map<String, Object> findDocumentInfoItemList(String id) {
		
		return documentInfoItemDao.selectDocumentInfoItemList(id);
	}
	
	public Map<String, Object> findDocumentInfoItemNewList(Integer start,
			Integer length, String query, String col, String sort, String id) {
		return documentInfoItemDao.selectDocumentInfoItemNewList(start, length,
				query, col, sort, id);
	}


	public void delDocumentInfoItem(String[] ids) {
		for (String id : ids) {
			DocumentInfoItem scp = documentInfoItemDao.get(
					DocumentInfoItem.class, id);
			documentInfoItemDao.delete(scp);
		}
	}


	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveDocumentInfoItem(DocumentInfo sc, String itemDataJson,
			String logInfo) throws Exception {
		List<DocumentInfoItem> saveItems = new ArrayList<DocumentInfoItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			DocumentInfoItem scp = new DocumentInfoItem();
			// 将map信息读入实体类
			scp = (DocumentInfoItem) documentInfoItemDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setDocumentInfo(sc);

			saveItems.add(scp);
		}
		documentInfoItemDao.saveOrUpdateAll(saveItems);
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setModifyContent(logInfo);
			documentInfoItemDao.saveOrUpdate(li);
		}
	}
}
