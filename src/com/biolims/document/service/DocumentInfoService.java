package com.biolims.document.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.document.dao.DocumentInfoDao;
import com.biolims.document.model.DocumentInfo;
import com.biolims.document.model.DocumentInfoItem;
import com.biolims.file.model.FileInfo;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.model.LogInfo;
import com.biolims.util.JsonUtils;
import com.opensymphony.xwork2.ActionContext;

@Service
public class DocumentInfoService {
	@Resource
	private DocumentInfoDao documentInfoDao;

	StringBuffer json = new StringBuffer();
	/**
	 * 文档管理的遍历
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findDocumentInfoList(Map<String, String> mapForQuery, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		return documentInfoDao.selectDocumentInfoList(mapForQuery, startNum, limitNum, dir, sort);
	}//

	public DocumentInfo getDocumentInfoById(String id) throws Exception {
		return documentInfoDao.get(DocumentInfo.class, id);
	}//
	
	public List<DocumentInfo> findDocumentInfoTreeList(String upId) {
		String hql = " from DocumentInfo where 1=1 ";
		String key = "";
		
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		
		List<DocumentInfo> list = null;
		if ("".equals(upId)||upId==null) {
			key = key + " and parent.id is null or parent.id='' order by id asc";
			list = documentInfoDao.find(hql + key);
		} else {
			key = key + " and parent.id='" + upId
					+ "' order by id asc";
			list = documentInfoDao.find(hql + key);
		}
		return list;
	}
	
	public String getJson(List<DocumentInfo> list) throws Exception {
		if (list != null && list.size() > 0) {
		}
		json = new StringBuffer();
		List<DocumentInfo> nodeList0 = new ArrayList<DocumentInfo>();
		Iterator<DocumentInfo> it1 = list.iterator();
		while (it1.hasNext()) {
			DocumentInfo node = (DocumentInfo) it1.next();
			
			nodeList0.add(node);
		
		}
		Iterator<DocumentInfo> it = nodeList0.iterator();
		while (it.hasNext()) {
			DocumentInfo node = (DocumentInfo) it.next();
			constructorJson(list, node);
		}
		String jsonDate = json.toString();
		return ("[" + jsonDate + "]").replaceAll(",]", "]");
	}
	
	/**
	 * 构建Json文件
	 * 
	 * @param list
	 * @param node
	 */

	public void constructorJson(List<DocumentInfo> list,
			DocumentInfo treeNode) throws Exception {
		
		
		json.append("{\"id\":'");
		json.append(treeNode.getId() + "'");
		json.append(",");
		
		json.append("\"fileName\":'");
		json.append(JsonUtils.formatStr(treeNode.getFileName() == null ? ""
				: treeNode.getFileName()) + "");
		
		json.append("',\"uploadUser-name\":'");
		json.append(JsonUtils.formatStr(treeNode.getUploadUser() == null ? ""
				: treeNode.getUploadUser().getName()) + "");
		
		json.append("',\"contentNote\":'");
		json.append(JsonUtils.formatStr(treeNode.getContentNote() == null ? ""
				: treeNode.getContentNote()) + "");
		
		json.append("',\"fileType\":'");
		json.append(JsonUtils.formatStr(treeNode.getFileType() == null ? ""
				: treeNode.getFileType()) + "");
		
		json.append("',\"uploadTime\":'");
		json.append(treeNode.getUploadTime() == null ? ""
				: treeNode.getUploadTime() + "");

		if (hasChildCount(treeNode)) {

			json.append("',\"leaf\":false");
		} else {

			json.append("',\"leaf\":true");


		}

		json.append(",\"upId\":'");
		json.append((treeNode.getParent() == null ? "" : treeNode
				.getParent().getId()) + "'");

		json.append("},");
		
	}
	
	public boolean hasChildCount(DocumentInfo treeNode) throws Exception {

		Long c = documentInfoDao
				.getCount("from DocumentInfo where parent.id='"
						+ treeNode.getId() + "'");
		return c > 0 ? true : false;
	}

	/**
	 * 增加文档中心模块
	 * @param et
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(DocumentInfo dif,String logInfo, String itemDataJson) throws Exception {
		this.documentInfoDao.save(dif);

		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
			.getRequest()
			.getSession()
			.getAttribute(
					SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(dif.getId());
			li.setModifyContent(logInfo);
			documentInfoDao.saveOrUpdate(li);
		}
		
		if(itemDataJson!=null&&!"".equals(itemDataJson)){
			List<DocumentInfoItem> saveItems = new ArrayList<DocumentInfoItem>();
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					itemDataJson, List.class);
			for (Map<String, Object> map : list) {
				DocumentInfoItem scp = new DocumentInfoItem();
				// 将map信息读入实体类
				scp = (DocumentInfoItem) documentInfoDao.Map2Bean(map, scp);
				if (scp.getId() != null && scp.getId().equals(""))
					scp.setId(null);
				scp.setDocumentInfo(dif);

				saveItems.add(scp);
			}
			documentInfoDao.saveOrUpdateAll(saveItems);
		}
	}//
	/**
	 * 修改文档中心模块
	 * @param et
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveOrUpdate(DocumentInfo dif,String logInfo, String itemDataJson) throws Exception {
		this.documentInfoDao.saveOrUpdate(dif);
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
			.getRequest()
			.getSession()
			.getAttribute(
					SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(dif.getId());
			li.setModifyContent(logInfo);
			documentInfoDao.saveOrUpdate(li);
		}
		if(itemDataJson!=null&&!"".equals(itemDataJson)){
			List<DocumentInfoItem> saveItems = new ArrayList<DocumentInfoItem>();
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					itemDataJson, List.class);
			for (Map<String, Object> map : list) {
				DocumentInfoItem scp = new DocumentInfoItem();
				// 将map信息读入实体类
				scp = (DocumentInfoItem) documentInfoDao.Map2Bean(map, scp);
				if (scp.getId() != null && scp.getId().equals(""))
					scp.setId(null);
				scp.setDocumentInfo(dif);

				saveItems.add(scp);
			}
			documentInfoDao.saveOrUpdateAll(saveItems);
		}
		
		
	}
	
	
}
