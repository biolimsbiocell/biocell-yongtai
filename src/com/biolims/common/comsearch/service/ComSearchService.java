package com.biolims.common.comsearch.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.code.dao.CodingRuleDao;
import com.biolims.common.comsearch.dao.ComSearchDao;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.SystemCodeService;
import com.biolims.dic.model.DicUnit; 
import com.biolims.file.model.FileInfo;
import com.biolims.remind.model.SysRemind;
import com.biolims.sample.model.DicSampleType;
import com.biolims.technology.dna.model.TechDnaServiceTaskItem;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class ComSearchService {
	@Resource
	private ComSearchDao comSearchDao;
	@Resource
	private CommonDAO commonDAO;

	/**
	 * 查询字典表单位信息
	 * @return
	 * @throws Exception
	 */
	public String  getDicUnit() throws Exception{
		List<DicUnit> list = comSearchDao.getDicUnit();
		return JsonUtils.toJsonString(list);
	}
	/**
	 * 根据类型查询字典表单位信息
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public String  getDicUnitByType(String type) throws Exception{
		List<DicUnit> list = comSearchDao.getDicUnitByType(type);
		return JsonUtils.toJsonString(list);
	}
	
	/**
	 * 根据实体名和ID查询图片信息
	 * @param id
	 * @param model
	 * @return
	 */
	public List<Map<String, String>> compareCode(String id,String model) {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = comSearchDao.selectFileInfo(id, model);
		List<FileInfo> list = (List<FileInfo>) result
				.get("list");
		if (list != null && list.size() > 0) {
			for (FileInfo ti : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ti.getId());
				map.put("fileName", ti.getFileName());
				map.put("filePath", ti.getFilePath());
				map.put("fileType", ti.getFileType());
				map.put("modelContentId", ti.getModelContentId());
				map.put("model", ti.getOwnerModel());
				map.put("classify", ti.getUploadTime().toString());
				mapList.add(map);
			}
		}
		return mapList;
	}
	
	public boolean vd(String str){  
	    char[] chars=str.toCharArray();   
	    boolean isGB2312=false;   
	    for(int i=0;i<chars.length;i++){  
            byte[] bytes=(""+chars[i]).getBytes();   
            if(bytes.length==2){   
	            int[] ints=new int[2];   
	            ints[0]=bytes[0]& 0xff;   
	            ints[1]=bytes[1]& 0xff;   
	            if(ints[0]>=0x81 && ints[0]<=0xFE && ints[1]>=0x40 && ints[1]<=0xFE){   
                    isGB2312=true;   
                    break;   
	            }   
            }   
	    }   
	    return isGB2312;   
	}
	
	
	public boolean isNumeric(String str){  
	       Pattern pattern = Pattern.compile("[0-9]*");  
	       Matcher isNum = pattern.matcher(str);  
	       if( !isNum.matches() ) {  
	          return false;  
	       }  
	       return true;  
	}  
	
	
	public String bSubstring(String s, int length) throws Exception
    {

        byte[] bytes = s.getBytes("Unicode");
        int n = 0; // 表示当前的字节数
        int i = 2; // 要截取的字节数，从第3个字节开始
        for (; i < bytes.length && n < length; i++)
        {
            // 奇数位置，如3、5、7等，为UCS2编码中两个字节的第二个字节
            if (i % 2 == 1)
            {
                n++; // 在UCS2第二个字节时n加1
            }
            else
            {
                // 当UCS2编码的第一个字节不等于0时，该UCS2字符为汉字，一个汉字算两个字节
                if (bytes[i] != 0)
                {
                    n++;
                }
            }
        }
        // 如果i为奇数时，处理成偶数
        if (i % 2 == 1)

        {
            // 该UCS2字符是汉字时，去掉这个截一半的汉字
            if (bytes[i - 1] != 0)
                i = i - 1;
            // 该UCS2字符是字母或数字，则保留该字符
            else
                i = i + 1;
        }

        return new String(bytes, 0, i, "Unicode");
    }
}
