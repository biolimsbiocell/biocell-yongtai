﻿package com.biolims.common.comsearch.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.comsearch.dao.ComSearchDao;
import com.biolims.common.comsearch.service.ComSearchService;
import com.biolims.file.model.FileInfo;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/common/comsearch/com")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class ComSearchAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";
	@Autowired
	private ComSearchService comSearchService;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private ComSearchDao comSearchDao;
	
	/**
	 * 加载单位下拉框
	 * @throws Exception
	 */
	@Action(value = "getDicUnit",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String getDicUnit() throws Exception {
		String str="{results:" + comSearchService.getDicUnit() + "}";
		return renderText(str);
	}
	/**
	 * 根据类型加载单位下拉框
	 * @throws Exception
	 */
	@Action(value = "getDicUnitByType",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String getDicUnitByType() throws Exception {
		String type = getParameterFromRequest("type");
		String str="{results:" + comSearchService.getDicUnitByType(type) + "}";
		return renderText(str);
	}
	
	
	public ComSearchService getComSearchService() {
		return comSearchService;
	}
	public void setComSearchService(ComSearchService comSearchService) {
		this.comSearchService = comSearchService;
	}
	public ComSearchDao getComSearchDao() {
		return comSearchDao;
	}
	public void setComSearchDao(ComSearchDao comSearchDao) {
		this.comSearchDao = comSearchDao;
	}
	/**
	 * 查询图片数量，下载PDF报告文件
	 * 
	 * @throws Exception
	 */
	@Action(value = "downloadReportFile", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void downloadReportFile() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String id = getParameterFromRequest("id");
			String model = getParameterFromRequest("model");
			List<FileInfo> picList=comSearchDao.findPicture(id, model);
			int num=picList.size();
			map.put("success", true);
			map.put("num", num);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}

		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	/**
	 * 查询图片
	 * @throws Exception
	 */
	@Action(value = "compareCode", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void compareCode() throws Exception {
		String id = getParameterFromRequest("id");
		String model = getParameterFromRequest("model");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> listMap = 
					this.comSearchService.compareCode(id, model);
			result.put("success", true);
			result.put("data", listMap);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
