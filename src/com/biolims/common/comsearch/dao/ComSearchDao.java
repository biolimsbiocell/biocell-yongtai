package com.biolims.common.comsearch.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.dao.CommonDAO;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.dic.model.DicType;
import com.biolims.dic.model.DicUnit;
import com.biolims.experiment.bobs.second.model.BobsSecondInstance;
import com.biolims.experiment.fish.second.model.FishSecondInstance;
import com.biolims.experiment.karyotyping.model.KaryotypingSecond;
import com.biolims.experiment.snpjc.second.model.SnpSecondInstance;
import com.biolims.file.model.FileInfo;
import com.biolims.system.sysreport.model.ReportTemplateInfo;
import com.biolims.system.template.model.Template;
import com.biolims.technology.dna.model.TechDnaServiceTaskItem;

@Repository
@SuppressWarnings("unchecked")
public class ComSearchDao extends BaseHibernateDao {
	/**
	 * 查询DicUnit表单位信息
	 * @return
	 */
	public List<DicUnit>  getDicUnit(){
		String hql="from DicUnit t where 1=1 and t.type='weight'";
		List<DicUnit> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据类型查询DicUnit表单位信息
	 * @param type
	 * @return
	 */
	public List<DicUnit>  getDicUnitByType(String type){
		String hql="from DicUnit t where 1=1 and t.type='"+type+"'";
		List<DicUnit> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据ID查询FileInfo对象
	 * @param type
	 * @return
	 */
	public List<FileInfo>  getFileInfoById(String type){
		String hql="from FileInfo t where 1=1 and t.type='"+type+"'";
		List<FileInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据类型查询template实验模板
	 * @param type
	 * @return
	 */
	public List<Template>  getTemplateByType(String type){
		String hql="from Template t where 1=1 and t.testType='"+type+"'";
		List<Template> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据检测项目和模板类型查询报告模板
	 * @param name,type
	 * @return
	 */
	public List<ReportTemplateInfo>  getReportTemplateInfo(String pid,String type){
		String hql="from ReportTemplateInfo t where  t.productId='"+pid+"' and state='1' and t.type='"+type+"'";
		List<ReportTemplateInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据检测项目查询报告模板
	 * @param type
	 * @return
	 */
	public List<ReportTemplateInfo>  getReportTemplateInfo1(String type){
		String hql="from ReportTemplateInfo t where t.productId='"+type+"' and state='1' and t.type='1' order by t.name";
		List<ReportTemplateInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据实体名和id查询图片
	 * 
	 * @param id
	 * @return
	 */
	public List<FileInfo> findPicture(String id, String model) {
		String hql = "from FileInfo t where t.modelContentId ='" + id
				+ "' and t.ownerModel='" + model + "' order by t.fileName";
		List<FileInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据实体名和id查询图片
	 * 
	 * @param id
	 * @return
	 */
	public List<FileInfo> findFileName(String id, String model) {
		String hql = "from FileInfo t where t.modelContentId in (" + id
				+ ") and t.ownerModel='" + model + "'";
		List<FileInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	
	/**
	 * 根据实体名和id查询图片
	 * 
	 * @param id
	 * @return
	 */
	public List<FileInfo> findFileNameByFileName(String fname) {
		String hql = "from FileInfo t where t.fileName= '"+fname+".png' and t.ownerModel='report'";
		List<FileInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	/**
	 * 根据实体名和id查询图片
	 * 
	 * @param id
	 * @return
	 */
	public Map<String, Object> selectFileInfo(String id, String model) {
		String hql = "from FileInfo t where t.modelContentId ='" + id
				+ "' and t.ownerModel='" + model + "' order by t.fileName";
		Long total = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();
		List<FileInfo> list = this.getSession().createQuery(hql)
				.list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	/**
	 * 根据实体名和id查询图片
	 * 并按照"-"后面的序号排序
	 * @param id
	 * @return
	 */
	public List<FileInfo> findPictureOrderByName(String id, String model) {
		String hql = "from FileInfo t where t.modelContentId ='" + id
				+ "' and t.ownerModel='" + model + "' order by cast((substr(substr(t.fileName,instr(t.fileName,'-',1,1)+1),0,instr(substr(t.fileName,instr(t.fileName,'-',1,1)+1),'.',1,1)-1)) as int)";
		System.out.println("hql: " + hql);
		//hql = "from FileInfo";
		System.out.println("this.getSession(): " + this.getSession());
		System.out.println("this.getSession().createQuery(hql): " + this.getSession().createQuery(hql));
		List<FileInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据实体名、id、图片名查询模糊图片
	 * 按照图片名排序
	 * @param id
	 * @return
	 */
	public List<FileInfo> findPicture1(String model,String id,String code) {
		String hql = "from FileInfo t where t.modelContentId ='" + id
				+ "' and t.ownerModel='" + model
				+ "' and substr(t.fileName,0,instr(t.fileName,'-',1,1)-1) like '%"+code+"%' order by t.fileName";
		List<FileInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据实体名、id、图片名查询图片
	 * 
	 * @param id
	 * @return
	 */
	public List<FileInfo> findPicture2(String model,String id,String code) {
		String hql = "from FileInfo t where t.modelContentId ='" + id
				+ "' and t.ownerModel='" + model
				+ "' and t.fileName = '"+code+"' order by t.fileName";
		List<FileInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据第二个-后面的序号、实体名和id查询图片
	 * 
	 * @param id
	 * @return
	 */
	//"select (substr(substr(t.fileName,instr(t.fileName,'-',2,2)+1),0,instr(substr(t.fileName,instr(t.fileName,'-',2,2)+1),'.',2,1)-1))" +
	public List<FileInfo> findPictureOrderByNum(String id, String model) {
		String hql = " from FileInfo t where t.modelContentId='"+id+"' and t.ownerModel='"+model+ 
				"'order by cast((substr(substr(t.fileName,instr(t.fileName,'-',2,2)+1),0,instr(substr(t.fileName,instr(t.fileName,'-',2,2)+1),'.',1,1)-1)) as int)";
		List<FileInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据第二个-后面的序号、实体名和id查询图片
	 * 
	 * @param id
	 * @return
	 */
	//"select (substr(substr(t.fileName,instr(t.fileName,'-',2,2)+1),0,instr(substr(t.fileName,instr(t.fileName,'-',2,2)+1),'.',2,1)-1))" +
	public List<String> findPictureNameOrderByNum(String id, String model) {
		String hql = "select t.fileName from FileInfo t where t.modelContentId='"+id+"' and t.ownerModel='"+model+ 
				"'order by cast((substr(substr(t.fileName,instr(t.fileName,'-',2,2)+1),0,instr(substr(t.fileName,instr(t.fileName,'-',2,2)+1),'.',1,1)-1)) as int)";
		List<String> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据样本号查询核型报告审核数据
	 * @param code
	 * @return
	 */
	public List<KaryotypingSecond>  getKaryotypingSecond(String code){
		String hql="from KaryotypingSecond t where t.isCommit='1' and t.code='"+code+"'";
		List<KaryotypingSecond> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据样本号查询Fish报告审核数据
	 * @param code
	 * @return
	 */
	public List<FishSecondInstance>  getFishSecondInstance(String code){
		String hql="from FishSecondInstance t where t.isCommit='1' and t.code='"+code+"'";
		List<FishSecondInstance> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据样本号查询Snp报告审核数据
	 * @param code
	 * @return
	 */
	public List<SnpSecondInstance>  getSnpSecondInstance(String code){
		String hql="from SnpSecondInstance t where t.submit='1' and t.code='"+code+"'";
		List<SnpSecondInstance> list = this.getSession().createQuery(hql).list();
		return list;
	}
	/**
	 * 根据样本号查询Bobs报告审核数据
	 * @param code
	 * @return
	 */
	public List<BobsSecondInstance>  getBobsSecondInstance(String code){
		String hql="from BobsSecondInstance t where t.submit='1' and t.code='"+code+"'";
		List<BobsSecondInstance> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	/**
	 * 根据检测项目、模板类型查询报告模板
	 * @param type
	 * @return
	 */
	public List<ReportTemplateInfo>  getReportTemplate(String productId,String ct){
		String hql="from ReportTemplateInfo t where t.productId='"+productId+"' and state='1' and t.crType='"+ct+"'";
		List<ReportTemplateInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}
}