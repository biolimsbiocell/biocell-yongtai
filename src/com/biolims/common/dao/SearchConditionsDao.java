package com.biolims.common.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.biolims.common.model.search.SearchConditions;
import com.biolims.dao.BaseHibernateDao;

@Repository
@SuppressWarnings("unchecked")
public class SearchConditionsDao extends BaseHibernateDao {

	public List<SearchConditions> selectSearchConditions(String userId, String fkModel) throws Exception {
		String hql = "from SearchConditions where userId='" + userId + "' and fkModel='" + fkModel + "'";
		return find(hql);
	}
}
