package com.biolims.common.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.sample.model.SampleInfo;

@Repository
public class CommonDAO extends BaseHibernateDao {
	public <T> Map<String, Object> findObjectCondition(int startNum, int limitNum, String dir, String sort,
			Class<T> persistentClass, Map mapForQuery, Map mapForCondition) throws Exception {
		String defaultDir = "";
		String defaultSort = "";
		if (sort.indexOf("-") != -1)
			sort = sort.substring(0, sort.indexOf("-"));
		Criteria criteria = this.getSession().createCriteria(persistentClass);
		Criteria criteriaCount = this.getSession().createCriteria(persistentClass)
				.setProjection(Projections.rowCount());

		Set<String> key = mapForQuery.keySet();
		for (Iterator it = key.iterator(); it.hasNext();) {

			String s = (String) it.next();
			if (mapForQuery.get(s).equals("alias")) {

				criteria.createAlias(s, s);
				criteriaCount.createAlias(s, s);

			}

			if (!s.equals("mapForManageScope") && !s.equals("orderBy")) {
				if (mapForCondition.get(s).equals("=")) {
					criteria.add(Restrictions.eq(s, mapForQuery.get(s)));
					criteriaCount.add(Restrictions.eq(s, mapForQuery.get(s)));
				}

				if (mapForCondition.get(s).equals("like")) {
					criteria.add(Restrictions.like(s, (String) mapForQuery.get(s), MatchMode.ANYWHERE));
					criteriaCount.add(Restrictions.like(s, (String) mapForQuery.get(s), MatchMode.ANYWHERE));
				}
				if (mapForCondition.get(s).equals("plike")) {
					criteria.add(Restrictions.like(s, (String) mapForQuery.get(s), MatchMode.START));
					criteriaCount.add(Restrictions.like(s, (String) mapForQuery.get(s), MatchMode.START));
				}
				if (mapForCondition.get(s).equals("elike")) {
					criteria.add(Restrictions.like(s, (String) mapForQuery.get(s), MatchMode.END));
					criteriaCount.add(Restrictions.like(s, (String) mapForQuery.get(s), MatchMode.END));
				}
				if (mapForCondition.get(s).equals(">=")) {
					criteria.add(Restrictions.ge(s, mapForQuery.get(s)));
					criteriaCount.add(Restrictions.ge(s, mapForQuery.get(s)));
				}
				if (mapForCondition.get(s).equals(">")) {
					criteria.add(Restrictions.gt(s, mapForQuery.get(s)));
					criteriaCount.add(Restrictions.gt(s, mapForQuery.get(s)));
				}
				if (mapForCondition.get(s).equals("<=")) {
					criteria.add(Restrictions.le(s, mapForQuery.get(s)));
					criteriaCount.add(Restrictions.le(s, mapForQuery.get(s)));
				}
				if (mapForCondition.get(s).equals("<")) {
					criteria.add(Restrictions.lt(s, mapForQuery.get(s)));
					criteriaCount.add(Restrictions.lt(s, mapForQuery.get(s)));
				}

				if (mapForCondition.get(s).equals("<>")) {
					criteria.add(Restrictions.ne(s, mapForQuery.get(s)));
					criteriaCount.add(Restrictions.ne(s, mapForQuery.get(s)));
				}
				if (mapForCondition.get(s).equals("in")) {
					criteria.add(Restrictions.in(s, (Object[]) mapForQuery.get(s)));
					criteriaCount.add(Restrictions.in(s, (Object[]) mapForQuery.get(s)));
				}
				if (mapForCondition.get(s).equals("isNull")) {
					criteria.add(Restrictions.isNull(s));
					criteriaCount.add(Restrictions.isNull(s));
				}
				if (mapForCondition.get(s).equals("isNotNull")) {
					criteria.add(Restrictions.isNotNull(s));
					criteriaCount.add(Restrictions.isNotNull(s));
				}

				if (mapForCondition.get(s).equals("isEmpty")) {
					criteria.add(Restrictions.isEmpty(s));
					criteriaCount.add(Restrictions.isEmpty(s));
				}

				if (mapForCondition.get(s).equals("isNotEmpty")) {
					criteria.add(Restrictions.isNotEmpty(s));
					criteriaCount.add(Restrictions.isNotEmpty(s));
				}
			}
			if (s.equals("orderBy")) {
				Map aMap = (Map) mapForQuery.get("orderBy");
				defaultSort = (String) aMap.get("name");
				defaultDir = (String) aMap.get("order");

			}
			if (s.equals("mapForManageScope")) {
				// 通用部门管理范围检索条件

			}
		}
		/*
		 * 
		 * criteria.createAlias("storage", "storage");
		 * criteriaCount.createAlias("storage", "storage");
		 * criteria.add(Restrictions.like("storage.name", "层", MatchMode.ANYWHERE));
		 * criteriaCount.add(Restrictions.like("storage.name", "层",
		 * MatchMode.ANYWHERE));
		 */
		// criteriaCount.add(Restrictions.like("note1.name", "层",
		// MatchMode.ANYWHERE));
		// criteria.add(Restrictions.like("department1.name", "12",
		// MatchMode.START));

		// criteriaCount.add(Restrictions.like("department1.name", "12",
		// MatchMode.START));
		List results = criteriaCount.setProjection(Projections.rowCount()).list();
		if (dir != null && sort != null && !"".equals(dir) && !"".equals(sort)) {
			if (sort.indexOf("-") != -1) {
				sort = sort.substring(0, sort.indexOf("-"));
			}

			if (dir.equals("DESC"))
				criteria.addOrder(Order.desc(sort));
			if (dir.equals("ASC"))
				criteria.addOrder(Order.asc(sort));
		} else {
			if (!defaultSort.equals("") && !defaultDir.equals("")) {
				if (defaultDir.equals("DESC"))
					criteria.addOrder(Order.desc(defaultSort));
				if (defaultDir.equals("ASC"))
					criteria.addOrder(Order.asc(defaultSort));
			}
		}

		/*
		 * Criterion cr2; Set<String> key1 = mapForQuery.keySet(); for (Iterator it =
		 * key1.iterator(); it.hasNext();) { String s = (String) it.next(); Criterion cs
		 * = Restrictions.ilike("", "", MatchMode.ANYWHERE); cr2 =
		 * Restrictions.or(cr2,cs);
		 * 
		 * } Criterion cr = Restrictions.ilike("", "", MatchMode.ANYWHERE); Criterion
		 * cr1 = Restrictions.ilike("", "", MatchMode.ANYWHERE); Criterion cr2 =
		 * Restrictions.or(cr,cr1); Criterion cr3 = Restrictions.ilike("", "",
		 * MatchMode.ANYWHERE); Criterion cr4 = Restrictions.or(cr2,cr3);
		 * criteria.add(Restrictions.or(Restrictions.ilike("", "", MatchMode.ANYWHERE),
		 * Restrictions.or(Restrictions.ilike("", "", MatchMode.ANYWHERE),
		 * Restrictions.ilike("", "", MatchMode.ANYWHERE))));
		 */
		if (startNum != -1 || limitNum != -1) {
			criteria.setFirstResult(startNum);
			criteria.setMaxResults(limitNum);
		}
		Map<String, Object> mapResult = new HashMap<String, Object>();

		List<T> list = criteria.list();
		mapResult.put("totalCount", Long.valueOf(String.valueOf(results.get(0))));
		mapResult.put("list", list);
		// ////getSession().clear();
		return mapResult;
	}

	public <T> Map<String, Object> findObjectCriteria(int startNum, int limitNum, String dir, String sort,
			Class<T> persistentClass, Map mapForQuery, Map mapForCondition) throws Exception {
		String defaultDir = "";
		String defaultSort = "";
		if (sort.indexOf("-") != -1)
			sort = sort.substring(0, sort.indexOf("-"));
		Criteria criteria = this.getSession().createCriteria(persistentClass);
		Criteria criteriaCount = this.getSession().createCriteria(persistentClass)
				.setProjection(Projections.rowCount());

		Set<String> key = mapForQuery.keySet();
		for (Iterator it = key.iterator(); it.hasNext();) {

			String s = (String) it.next();
			if (mapForQuery.get(s).equals("alias")) {

				criteria.createAlias(s, s);
				criteriaCount.createAlias(s, s);

			}

			if (!s.equals("mapForManageScope") && !s.equals("orderBy")) {
				if (mapForCondition.get(s).equals("=")) {
					criteria.add(Restrictions.eq(s, mapForQuery.get(s)));
					criteriaCount.add(Restrictions.eq(s, mapForQuery.get(s)));
				}

				if (mapForCondition.get(s).equals("like")) {
					criteria.add(Restrictions.like(s, (String) mapForQuery.get(s), MatchMode.ANYWHERE));
					criteriaCount.add(Restrictions.like(s, (String) mapForQuery.get(s), MatchMode.ANYWHERE));
				}
				if (mapForCondition.get(s).equals("plike")) {
					criteria.add(Restrictions.like(s, (String) mapForQuery.get(s), MatchMode.START));
					criteriaCount.add(Restrictions.like(s, (String) mapForQuery.get(s), MatchMode.START));
				}
				if (mapForCondition.get(s).equals("elike")) {
					criteria.add(Restrictions.like(s, (String) mapForQuery.get(s), MatchMode.END));
					criteriaCount.add(Restrictions.like(s, (String) mapForQuery.get(s), MatchMode.END));
				}
				if (mapForCondition.get(s).equals(">=")) {
					criteria.add(Restrictions.ge(s, mapForQuery.get(s)));
					criteriaCount.add(Restrictions.ge(s, mapForQuery.get(s)));
				}
				if (mapForCondition.get(s).equals(">")) {
					criteria.add(Restrictions.gt(s, mapForQuery.get(s)));
					criteriaCount.add(Restrictions.gt(s, mapForQuery.get(s)));
				}
				if (mapForCondition.get(s).equals("<=")) {
					criteria.add(Restrictions.le(s, mapForQuery.get(s)));
					criteriaCount.add(Restrictions.le(s, mapForQuery.get(s)));
				}
				if (mapForCondition.get(s).equals("<")) {
					criteria.add(Restrictions.lt(s, mapForQuery.get(s)));
					criteriaCount.add(Restrictions.lt(s, mapForQuery.get(s)));
				}

				if (mapForCondition.get(s).equals("<>")) {
					criteria.add(Restrictions.ne(s, mapForQuery.get(s)));
					criteriaCount.add(Restrictions.ne(s, mapForQuery.get(s)));
				}
				if (mapForCondition.get(s).equals("in")) {
					criteria.add(Restrictions.in(s, (Object[]) mapForQuery.get(s)));
					criteriaCount.add(Restrictions.in(s, (Object[]) mapForQuery.get(s)));
				}
				if (mapForCondition.get(s).equals("isNull")) {
					criteria.add(Restrictions.isNull(s));
					criteriaCount.add(Restrictions.isNull(s));
				}
				if (mapForCondition.get(s).equals("isNotNull")) {
					criteria.add(Restrictions.isNotNull(s));
					criteriaCount.add(Restrictions.isNotNull(s));
				}

				if (mapForCondition.get(s).equals("isEmpty")) {
					criteria.add(Restrictions.isEmpty(s));
					criteriaCount.add(Restrictions.isEmpty(s));
				}

				if (mapForCondition.get(s).equals("isNotEmpty")) {
					criteria.add(Restrictions.isNotEmpty(s));
					criteriaCount.add(Restrictions.isNotEmpty(s));
				}
			}
			if (s.equals("orderBy")) {
				Map aMap = (Map) mapForQuery.get("orderBy");
				defaultSort = (String) aMap.get("name");
				defaultDir = (String) aMap.get("order");

			}
			if (s.equals("mapForManageScope")) {
				// 通用部门管理范围检索条件

			}
		}
		Map<String, Object> mapResult = new HashMap<String, Object>();
		mapResult.put("criteria", criteria);
		mapResult.put("criteriaCount", criteriaCount);
		return mapResult;
	}

	public <T> Map<String, Object> findObjectList(int startNum, int limitNum, String dir, String sort,
			Class<T> persistentClass, Map mapForQuery, Map mapForCondition, Criteria criteriaCount, Criteria criteria)
			throws Exception {
		String defaultDir = "DESC";
		String defaultSort = "id";
		List results = criteriaCount.setProjection(Projections.rowCount()).list();
		if (dir != null && sort != null && !"".equals(dir) && !"".equals(sort)) {
			if (sort.indexOf("-") != -1) {
				sort = sort.substring(0, sort.indexOf("-"));
			}
			if (dir.equals("DESC"))
				criteria.addOrder(Order.desc(sort));
			if (dir.equals("ASC"))
				criteria.addOrder(Order.asc(sort));
		} else {
			if (!defaultSort.equals("") && !defaultDir.equals("")) {
				if (defaultDir.equals("DESC"))
					criteria.addOrder(Order.desc(defaultSort));
				if (defaultDir.equals("ASC"))
					criteria.addOrder(Order.asc(defaultSort));
			}
		}

		/*
		 * Criterion cr2; Set<String> key1 = mapForQuery.keySet(); for (Iterator it =
		 * key1.iterator(); it.hasNext();) { String s = (String) it.next(); Criterion cs
		 * = Restrictions.ilike("", "", MatchMode.ANYWHERE); cr2 =
		 * Restrictions.or(cr2,cs);
		 * 
		 * } Criterion cr = Restrictions.ilike("", "", MatchMode.ANYWHERE); Criterion
		 * cr1 = Restrictions.ilike("", "", MatchMode.ANYWHERE); Criterion cr2 =
		 * Restrictions.or(cr,cr1); Criterion cr3 = Restrictions.ilike("", "",
		 * MatchMode.ANYWHERE); Criterion cr4 = Restrictions.or(cr2,cr3);
		 * criteria.add(Restrictions.or(Restrictions.ilike("", "", MatchMode.ANYWHERE),
		 * Restrictions.or(Restrictions.ilike("", "", MatchMode.ANYWHERE),
		 * Restrictions.ilike("", "", MatchMode.ANYWHERE))));
		 */
		if (startNum != -1 || limitNum != -1) {
			criteria.setFirstResult(startNum);
			criteria.setMaxResults(limitNum);
		}
		Map<String, Object> mapResult = new HashMap<String, Object>();

		List<T> list = criteria.list();
		mapResult.put("totalCount", Long.valueOf(String.valueOf(results.get(0))));
		mapResult.put("list", list);
		// ////getSession().clear();
		return mapResult;
	}

	public List<SampleInfo> getSampleInfoFromOrderId(String orderCode) {
		List<SampleInfo> list = new ArrayList<SampleInfo>();
		String hql = "from SampleInfo where 1=1 and sampleOrder='" + orderCode + "'";
		list = getSession().createQuery(hql).list();
		return list;
	}
}
