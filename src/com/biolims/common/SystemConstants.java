package com.biolims.common;

import org.apache.struts2.ServletActionContext;

public class SystemConstants extends
		com.biolims.common.constants.SystemConstants {


	/**
	 * 上机测序csv
	 */
	public static final String SEQINFO_CERATE_INFO_CSV_FILE_NAME = "seqInfo.csv";
	public static final String CERATE_TASK_EXECL_FILE_NAME = "wkTaskExcel.xls";
	public static final String SEQ_CERATE_TASK_EXECL_FILE_NAME = "seqtask.xls";
	public static final String SEQITEM_CERATE_TASK_EXECL_FILE_NAME = "seqitem.xls";
	public static final String SEQINFO_CERATE_TASK_EXECL_FILE_NAME = "seqinfo.xls";
	/**
	 * 信息分析Excel
	 */
	// 信息分析主表(EXCEL)
	public static final String ANALY_CERATE_TASK_EXECL_FILE_NAME = "analytask.xls";
	// 信息分析明细表
	public static final String ANALY_CERATE_ITEM_EXECL_FILE_NAME = "analyitem.xls";
	// 信息分析结果表
	public static final String ANALY_CERATE_INFO_EXECL_FILE_NAME = "anainfo.xls";

	// 信息分析主表(csv)
	public static final String ANALY_CERATE_INFO_CSV_FILE_NAME = "analyInfo.csv";

	/**
	 * 上机测序写入文件路径
	 */
	public static final String WRITE_QC_PATH = getProperty("write_qc_path",
			"d:/qcData/");

	/**
	 * 下机质控生成文件路径
	 */
	public static final String SEQ_QC_PATH = getProperty("seq_qc_path",
			"d:/qcData/");

	/**
	 * 信息分析文件路径
	 */
	public static final String INFO_QC_PATH = getProperty("info_qc_path",
			"d:/qcData/");

	/**
	 * 信息分析写入文件路径
	 */
	public static final String INFO_WRITE_PATH = getProperty("info_write_path",
			"d:/qcData/");

}
