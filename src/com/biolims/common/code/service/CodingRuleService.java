package com.biolims.common.code.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.code.dao.CodingRuleDao;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.SystemCodeService;
import com.biolims.remind.model.SysRemind;
import com.biolims.sample.model.SampleOrder;
import com.biolims.util.DateUtil;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class CodingRuleService {
	@Resource
	private CodingRuleDao codingRuleDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SystemCodeService systemCodeService;

	// 自动生成编码
	public String findAutoID(String modelName, int nn) throws Exception {
		String autoID = "";
		switch (nn) {
		case 3:
			autoID = systemCodeService.getCodeByPrefix(modelName, DateUtil.dateFormatterByPattern(new Date(), "yy"),
					000, 3, null);
			break;
		case 4:
			autoID = systemCodeService.getCodeByPrefix(modelName, DateUtil.dateFormatterByPattern(new Date(), "yy"),
					0000, 4, null);
			break;
		case 5:
			autoID = systemCodeService.getCodeByPrefix(modelName, DateUtil.dateFormatterByPattern(new Date(), "yy"),
					00000, 5, null);
			break;
		}
		return autoID;
	}

	// 生成编码日期+4位
	public String genTransID(String modelName, String markCode) throws Exception {
		Date date = new Date();
		DateFormat format = new SimpleDateFormat("yyMMdd");
		String stime = format.format(date);
		String autoID = getCodeByPrefix(modelName, markCode, stime, 0000, 4, null);
		return autoID;
	}

	public String genTransID3(String modelName, String markCode) throws Exception {
		Date date = new Date();
		DateFormat format = new SimpleDateFormat("yyyyMMdd");
		String stime = format.format(date);
		String autoID = getCodeByPrefix(modelName, markCode, stime, 000, 3, null);
		return autoID;
	}

	public String getCodeByPrefix(String modelName, String markCode, String prefix, long code, Integer flowLength,
			Map<String, String> mapForQuery) throws Exception {
		Integer flowNum = 0;

		String val = markCode + prefix;
		if (mapForQuery == null)
			mapForQuery = new HashMap<String, String>();
		mapForQuery.put("id", "like##@@##'" + val + "%'");

		String maxId = codingRuleDao.selectModelTotalByType(modelName, mapForQuery);
		String _maxId = flowNum.toString();
		if (maxId != null)
			_maxId = maxId.substring(maxId.length() - flowLength);
		else {
			_maxId = String.valueOf(code);
		}

		String parseId = "";

		parseId = codingRuleDao.codeParse(Integer.valueOf(_maxId) + 1 + "", flowLength);

		return val + parseId;
	}

	public String getSampleOrderBarcode(String modelName, SampleOrder so, long code, Integer flowLength,
			Map<String, String> mapForQuery) throws Exception {
		Integer flowNum = 0;
		String val = so.getProductId();
		if (mapForQuery == null)
			mapForQuery = new HashMap<String, String>();
		mapForQuery.put("barcode", "like##@@##'" + val + "%'");

		String maxId = codingRuleDao.selectSampleOrderBarcode(so, mapForQuery);
		String _maxId = flowNum.toString();
		if (maxId != null)
			_maxId = maxId.substring(maxId.length() - flowLength);
		else {
			_maxId = String.valueOf(code);
		}

		String parseId = "";

		parseId = codingRuleDao.codeParse(Integer.valueOf(_maxId) + 1 + "", flowLength);

		return val + parseId;
	}
	
	public String getSampleOrderBarcode(String modelName, String productid, long code, Integer flowLength,
			Map<String, String> mapForQuery) throws Exception {
		Integer flowNum = 0;
		String val = productid;
		if (mapForQuery == null)
			mapForQuery = new HashMap<String, String>();
		mapForQuery.put("barcode", "like##@@##'" + val + "%'");

		String maxId = codingRuleDao.selectSampleOrderBarcode(productid, mapForQuery);
		String _maxId = flowNum.toString();
		if (maxId != null)
			_maxId = maxId.substring(maxId.length() - flowLength);
		else {
			_maxId = String.valueOf(code);
		}

		String parseId = "";

		parseId = codingRuleDao.codeParse(Integer.valueOf(_maxId) + 1 + "", flowLength);

		return val + parseId;
	}

	// 查询编码的前缀
	public String findIdPrefix(String modelName) {
		return codingRuleDao.findIdPrefix(modelName);
	}

	/**
	 * 生成6位日期时间+4位数字
	 * 
	 * @param modelName
	 * @param markCode
	 * @return
	 * @throws Exception
	 */
	public String sampleCode(String modelName, String markCode) throws Exception {
		Date date = new Date();
		DateFormat format = new SimpleDateFormat("yyMMdd");
		String stime = format.format(date);
		String autoID = getSampleCode(modelName, stime, 0000, 4, null);
		return autoID;
	}

	public String getSampleCode(String modelName, String markCode, long code, Integer flowLength,
			Map<String, String> mapForQuery) throws Exception {
		Integer flowNum = 0;

		String val = markCode;
		if (mapForQuery == null)
			mapForQuery = new HashMap<String, String>();
		mapForQuery.put("sampleCode", "like##@@##'" + val + "%'");

		String maxId = codingRuleDao.selectMaxSampleCode(modelName, mapForQuery);
		String _maxId = flowNum.toString();
		if (maxId != null)
			_maxId = maxId.substring(maxId.length() - flowLength);
		else {
			_maxId = String.valueOf(code);
		}

		String parseId = "";

		parseId = codingRuleDao.codeParse(Integer.valueOf(_maxId) + 1 + "", flowLength);

		return val + parseId;
	}

	/**
	 * 订单编码 8位数字
	 * 
	 * @param modelName
	 * @param markCode
	 * @return
	 * @throws Exception
	 */
	public String getCode(String modelName) throws Exception {
		String autoID = getMaxCode(modelName, 00000000, 8, null);
		return autoID;
	}

	public String getMaxCode(String modelName, long code, Integer flowLength, Map<String, String> mapForQuery)
			throws Exception {
		Integer flowNum = 0;

		String maxCode = codingRuleDao.selectMaxCode(modelName);
		String _maxId = flowNum.toString();
		if (maxCode != null)
			_maxId = maxCode.substring(maxCode.length() - flowLength);
		else {
			_maxId = String.valueOf(code);
		}

		String parseId = "";
		parseId = codingRuleDao.codeParse(Integer.valueOf(_maxId) + 1 + "", flowLength);

		return parseId;
	}

	public String getCode(String modelName, String markCode, long code, Integer flowLength,
			Map<String, String> mapForQuery) throws Exception {
		Integer flowNum = 0;

		String val = markCode;
		if (mapForQuery == null)
			mapForQuery = new HashMap<String, String>();
		mapForQuery.put("code", "like##@@##'" + val + "%'");

		String maxId = codingRuleDao.selectMaxCode(modelName, mapForQuery);
		String _maxId = flowNum.toString();
		if (maxId != null)
			_maxId = maxId.substring(maxId.length() - flowLength);
		else {
			_maxId = String.valueOf(code);
		}

		String parseId = "";

		parseId = codingRuleDao.codeParse(Integer.valueOf(_maxId) + 1 + "", flowLength);

		return val + parseId;
	}

	public String getCode1(String modelName, String markCode, long code, Integer flowLength,
			Map<String, String> mapForQuery) throws Exception {
		Integer flowNum = 0;

		String val = markCode;
		if (mapForQuery == null)
			mapForQuery = new HashMap<String, String>();
		mapForQuery.put("code", "like##@@##'" + val + "%'");

		String maxId = codingRuleDao.selectMaxCode1(modelName, mapForQuery);
		String _maxId = flowNum.toString();
		if (maxId != null)
			_maxId = maxId.substring(maxId.length() - flowLength);
		else {
			_maxId = String.valueOf(code);
		}

		String parseId = "";

		parseId = codingRuleDao.codeParse(Integer.valueOf(_maxId) + 1 + "", flowLength);

		return val + parseId;
	}

	public String getSerialCode(String modelName, String prefix, Integer suffixLength, String minCodeSuffix,
			String field) {
		String maxId = codingRuleDao.selectModelTotalByType(modelName, prefix, field);
		if (null != maxId) {

			Integer maxIdInt = Integer.parseInt(maxId.replace(prefix, "").toString());
			String _prefix = "";
			if (maxIdInt < Integer.parseInt(minCodeSuffix)) {
				return prefix + minCodeSuffix;
			} else {
				if (maxIdInt.SIZE < suffixLength) {
					for (int i = 0; i < suffixLength - maxIdInt.SIZE; i++) {
						_prefix += "0";
					}
					return prefix + _prefix + (maxIdInt + 1);
				} else {

					return prefix + (maxIdInt + 1);
				}
			}
		} else {
			return prefix + minCodeSuffix;
		}
	}

	public String getpoolingId(String modelName, long code, Integer flowLength, Map<String, String> mapForQuery)
			throws Exception {
		Date date = new Date();
		DateFormat format = new SimpleDateFormat("yyyyMMdd");
		String prefix = format.format(date);

		Integer flowNum = 0;

		String val = prefix;
		if (mapForQuery == null)
			mapForQuery = new HashMap<String, String>();
		mapForQuery.put("id", "like##@@##'" + val + "%'");

		String maxId = codingRuleDao.selectModelTotalByType(modelName, mapForQuery);
		String _maxId = flowNum.toString();
		// if (maxId != null)
		// _maxId = maxId.substring(maxId.length() - flowLength);
		// else {
		//
		// }
		String[] poolingId = null;
		String parseId = "";
		if (maxId != null) {
			poolingId = maxId.split("-");
			parseId = codingRuleDao.codeParse(Integer.valueOf(poolingId[0]) + 1 + "", flowLength);
			return parseId;
		} else {
			_maxId = String.valueOf(code);
			parseId = codingRuleDao.codeParse(Integer.valueOf(_maxId) + 1 + "", flowLength);
			return val + parseId;
		}

	}

	public String getorderId(String modelName, long code, Integer flowLength, Map<String, String> mapForQuery,
			String orderType) throws Exception {
		Integer flowNum = 0;
		mapForQuery = new HashMap<String, String>();
		mapForQuery.put("orderType", orderType);
		String maxCode = codingRuleDao.selectModelTotalByType(modelName, mapForQuery);
		String _maxId = flowNum.toString();
		if (maxCode != null)
			_maxId = maxCode.substring(maxCode.length() - flowLength);
		else {
			_maxId = String.valueOf(code);
		}

		String parseId = "";
		parseId = codingRuleDao.codeParse(Integer.valueOf(_maxId) + 1 + "", flowLength);

		return parseId;
	}

	public String get(String sampleCode, String modelName, String markCode, long code, Integer flowLength,
			Map<String, String> mapForQuery) throws Exception {
		Integer flowNum = 0;
		Date date = new Date();
		DateFormat format = new SimpleDateFormat("yyMMdd");
		String stime = format.format(date);
		String val = markCode + stime;
		if (mapForQuery == null)
			mapForQuery = new HashMap<String, String>();
		mapForQuery.put(sampleCode, "like##@@##'" + val + "%'");

		String maxId = codingRuleDao.selectMax(sampleCode, modelName, mapForQuery);
		String _maxId = flowNum.toString();
		if (maxId != null)
			_maxId = maxId.substring(maxId.length() - flowLength);
		else {
			_maxId = String.valueOf(code);
		}

		String parseId = "";

		parseId = codingRuleDao.codeParse(Integer.valueOf(_maxId) + 1 + "", flowLength);

		return val + parseId;
	}

	public String getpoolingCode(String modelName, String markCode, long code, Integer flowLength,
			Map<String, String> mapForQuery) throws Exception {
		Integer flowNum = 0;
		Date date = new Date();
		DateFormat format = new SimpleDateFormat("yyyyMMdd");
		String stime = format.format(date);
		String val = markCode + stime;
		if (mapForQuery == null)
			mapForQuery = new HashMap<String, String>();
		mapForQuery.put("poolingCode", "like##@@##'" + val + "%'");

		String maxId = codingRuleDao.selectpoolingCode(modelName, mapForQuery);
		String _maxId = flowNum.toString();
		if (maxId != null)
			_maxId = maxId.substring(maxId.length() - flowLength);
		else {
			_maxId = String.valueOf(code);
		}

		String parseId = "";

		parseId = codingRuleDao.codeParse(Integer.valueOf(_maxId) + 1 + "", flowLength);

		return val + parseId;
	}

	/*
	 * sampleTaskService.createSampleSysRemind("单号:" + contentId + "接收单于" +
	 * DateUtil.dateFormatterByPattern(new Date(), "yyyy-MM-dd") + "已经完成接收,请注收款情况!",
	 * zg, "sampleReceive", contentId);
	 */

	public void createSampleSysRemind(String title1, User user, String tableId, String contentId) {
		SysRemind sr = new SysRemind();
		String title11 = "";
		title11 = title1;
		title1 = title1.substring(0, title1.length() - 1);
		sr.setHandleUser(user);
		sr.setTitle(title11);
		sr.setState("0");
		sr.setTableId(tableId);
		sr.setContent(title1);
		sr.setType("1");
		sr.setRemindUser("SYSTEM");
		sr.setStartDate(new Date());
		sr.setContentId(contentId);
		codingRuleDao.saveOrUpdate(sr);
	}

	/**
	 * 
	 * @param modelName
	 *            实体名称
	 * @param markCode
	 *            查询的编号前缀
	 * @param code
	 *            几位数
	 * @param flowLength
	 * @param mapForQuery
	 * @param field
	 *            查询的字段
	 * @return
	 * @throws Exception
	 */
	public String get(String modelName, String markCode, long code, Integer flowLength, Map<String, String> mapForQuery,
			String field) throws Exception {
		Integer flowNum = 0;

		String val = markCode;
		if (mapForQuery == null)
			mapForQuery = new HashMap<String, String>();
		mapForQuery.put(field, "like##@@##'" + val + "%'");

		String maxId = codingRuleDao.selectMax(modelName, mapForQuery, field);
		String _maxId = flowNum.toString();
		if (maxId != null)
			_maxId = maxId.substring(maxId.length() - flowLength);
		else {
			_maxId = String.valueOf(code);
		}

		String parseId = "";

		parseId = codingRuleDao.codeParse(Integer.valueOf(_maxId) + 1 + "", flowLength);

		return val + parseId;
	}
}
