package com.biolims.common.code.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.sample.model.SampleOrder;

@Repository
@SuppressWarnings("unchecked")
public class CodingRuleDao extends BaseHibernateDao {
	// 查询编码前缀
	public String findIdPrefix(String modelName) {
		String sql = "select t.id from t_application_type_table t where 1=1 and t.id = '" + modelName + "'";
		List<Map<String, Object>> list = this.findForJdbc(sql);
		String result = modelName;
		if (list != null && list.size() > 0 && list.get(0) != null && list.get(0).get("id") != null
				&& !list.get(0).get("id").equals("")) {
			result = list.get(0).get("id").toString();
		}
		return result;
	}

	public String selectModelTotalByType(String modelName, String prefix, String field) {
		String key = "";
		String hql = "select max(" + field + ") from " + modelName + " where 1=1 and " + field + " like '" + prefix
				+ "%'";
		return (String) this.getSession().createQuery(hql + key).uniqueResult();
	}

	public String codeParse(String val, Integer codeLength) {
		String result = null;
		if (codeLength != null && val.length() < codeLength) {
			int endVal = codeLength - val.length();
			String tem = "";
			for (int i = 0; i < endVal; i++) {
				tem = tem + 0;
			}
			result = tem + val;
		} else {
			result = val;
		}
		return result;
	}

	// 查询编码的最大值
	public String selectModelTotalByType(String modelName, Map<String, String> mapForQuery) {
		String key = "";
		String hql = "select max(id) from " + modelName + " where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		return (String) this.getSession().createQuery(hql + key).uniqueResult();
	}

	// SampleOrder
	public String selectSampleOrderBarcode(SampleOrder so, Map<String, String> mapForQuery) {
		String key = "";
		int pn = so.getProductId().length();
		// t.barcode,1,INSTR(t.barcode,'-')-1
//		String hql = "select max(substring(barcode," + (pn+1) + "," + 4
		String hql = "select max(substring(barcode," + (pn+1) + "," + 4
//				+ ")) from SampleOrder  where 1=1 and substring(barcode,1," + pn + ")='" + so.getProductId()
				+ ")) from SampleOrder  where 1=1 and productId='" + so.getProductId()
				+ "' and stateName!='作废' ";//and stateName!='新建'  and stateName!='预订单'
		return (String) this.getSession().createQuery(hql + key).uniqueResult();
	}
	
	// SampleOrder
	public String selectSampleOrderBarcode(String productid, Map<String, String> mapForQuery) {
		String key = "";
		int pn = productid.length();
		// t.barcode,1,INSTR(t.barcode,'-')-1
//		String hql = "select max(substring(barcode," + (pn+1) + "," + 4
		String hql = "select max(substring(barcode," + (pn+1) + "," + 4
//				+ ")) from SampleOrder  where 1=1 and substring(barcode,1," + pn + ")='" + productid
				+ ")) from SampleOrder  where 1=1 and productId='" + productid
				+ "' and stateName!='作废'";// and stateName!='新建' and stateName!='预订单'
		return (String) this.getSession().createQuery(hql + key).uniqueResult();
	}

	// 查询样本编号的最大值
	public String selectMaxSampleCode(String modelName, Map<String, String> mapForQuery) {
		String key = "";
		String hql = "select max(sampleCode) from " + modelName + " where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		return (String) this.getSession().createQuery(hql + key).uniqueResult();
	}

	// 查询样本编号的最大值
	public String selectMaxCode(String modelName, Map<String, String> mapForQuery) {
		String key = "";
		String hql = "select max(code) from " + modelName + " where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		return (String) this.getSession().createQuery(hql + key).uniqueResult();
	}

	// 查询样本编号的最大值
	public String selectMaxCode1(String modelName, Map<String, String> mapForQuery) {
		String key = "";
		String hql = "select max(splitCode) from " + modelName + " where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		return (String) this.getSession().createQuery(hql + key).uniqueResult();
	}

	// 查询订单编码的最大值
	public String selectMaxCode(String modelName) {

		String hql = "select max(code) from " + modelName + " where 1=1 ";
		String max = (String) this.getSession().createQuery(hql).uniqueResult();
		return max;
	}

	// 查询富集的最大值
	public String selectMax(String sampleCode, String modelName, Map<String, String> mapForQuery) {
		String key = "";
		String hql = "select max(" + sampleCode + ") from " + modelName + " where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		return (String) this.getSession().createQuery(hql + key).uniqueResult();
	}

	// 查询混合pooling编号最大值
	public String selectpoolingCode(String modelName, Map<String, String> mapForQuery) {
		String key = "";
		String hql = "select max(poolingCode) from " + modelName + " where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		return (String) this.getSession().createQuery(hql + key).uniqueResult();
	}

	public String selectMax(String modelName, Map<String, String> mapForQuery, String field) {
		String key = "";
		String hql = "select max(" + field + ") from " + modelName + " where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		return (String) this.getSession().createQuery(hql + key).uniqueResult();
	}

}