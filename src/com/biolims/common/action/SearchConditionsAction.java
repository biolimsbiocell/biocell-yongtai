package com.biolims.common.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.model.search.SearchConditions;
import com.biolims.common.model.user.User;
import com.biolims.common.service.SearchConditionsService;
import com.biolims.core.constants.SystemConstants;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;

/**
 * 检索条件
 * @author cong
 */
@Namespace("/search")
@Controller
@Scope("prototype")
@ParentPackage("default")
public final class SearchConditionsAction extends BaseActionSupport {

	private static final long serialVersionUID = 777715358982940270L;

	@Resource
	private SearchConditionsService searchConditionsService;

	/**
	 * 条件页面
	 * @return
	 * @throws Exception
	 */
	@Action(value = "toSearch", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String toSearch() throws Exception {
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);

		String fkModel = getRequest().getParameter("fkMdoel");
		List<SearchConditions> list = searchConditionsService.findSearchConditions(user.getId(), fkModel);
		getRequest().setAttribute("searchList", list);
		return dispatcher("/WEB-INF/page/search/toSearch.jsp");
	}

	/**
	 * 保存
	 * @throws Exception
	 */
	@Action(value = "save", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void save() throws Exception {
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);

		String fkModel = getRequest().getParameter("fkModel");
		String name = getRequest().getParameter("name");
		String searchInfo = getRequest().getParameter("searchInfo");
		String allInfo = getRequest().getParameter("allInfo");

		Map<String, Object> map = new HashMap<String, Object>();
		try {
			SearchConditions sc = new SearchConditions();
			sc.setUserId(user.getId());
			sc.setName(name);
			sc.setSearchInfo(searchInfo);
			sc.setAllInfo(allInfo);
			sc.setFkModel(fkModel);
			searchConditionsService.save(sc);
			map.put("success", true);
			map.put("data", sc);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "delete", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void delete() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String searchId = getRequest().getParameter("searchId");
			searchConditionsService.deleteSearchConditions(searchId);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

}
