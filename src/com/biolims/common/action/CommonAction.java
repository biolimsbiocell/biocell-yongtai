/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：
 * 类描述：通用Action类
 * 创建人：倪毅
 * 创建时间：2013-1
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.common.action;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.service.CommonService;

@Namespace("/common")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings( { "unchecked", "rawtypes" })
public class CommonAction extends BaseActionSupport {

	private static final long serialVersionUID = -164661537469123056L;

	@Autowired
	private CommonService commonService;

	/**
	 * 查询是否编号重复
	 */
	@Action(value = "hasId", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String hasId() throws Exception {
		try {
			String id = getParameterFromRequest("id");
			String obj = getParameterFromRequest("obj");
			return this.renderText(commonService.hasId(id, obj));
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}

	@Action(value = "hasCode", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String hasCode() throws Exception {
		try {
			String code = getParameterFromRequest("code");
			String obj = getParameterFromRequest("obj");
			return this.renderText(commonService.hasCode(code, obj));
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}

	/**
	 * 导出Excel
	 */
	@Action(value = "exportExcel", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String exportExcel() throws Exception {

		return dispatcher("/WEB-INF/page/export/exportExcel.jsp");
	}
}
