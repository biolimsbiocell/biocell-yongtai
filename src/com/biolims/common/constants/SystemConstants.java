package com.biolims.common.constants;

import com.biolims.util.ConfigurableContants;

public class SystemConstants extends ConfigurableContants {

	static {
		init("/system.properties");
	}
	public static final String COOKIE_SESSION_KEY = "session_key";

	public static final boolean SESSION_CLUSTERID = false;
	/**
	 * session中user的key
	 */
	public static final String USER_SESSION_KEY = "userSession";
	/**
	 * session中user的权限列表
	 */
	public static final String USER_SESSION_RIGHTS = "userRights";
	/**
	 * session中user的权限列表
	 */
	public static final String USER_SESSION_PORTLET_CONTENT = "userPortlets";
	/**
	 * session中user的权限列表
	 */
	public static final String USER_SESSION_ACTION_RIGHTS = "userActionRights";
	/**
	 * session中user的权限列表
	 */
	public static final String USER_SESSION_RIGHTS_DEPARTMENT = "userRightsDepartment";
	/**
	 * session中user的权限列表
	 */
	public static final String USER_SESSION_RIGHTS_APPLICATION_TYPE_ACTION = "userRightsApplicationTypeAction";
	/**
	 * session中user的权限列表
	 */
	public static final String USER_SESSION_RIGHTS_ACTION = "userRightsAction";
	/**
	 * session中user的部门列表
	 */
	public static final String USER_SESSION_DEPARTMENT = "userDepartment";

	/**
	 * session中user的权限列表
	 */
	public static final String USER_SESSION_IS_SALE_MANAGE = "isSaleManage";

	/**
	 * session中user的组字符串
	 */
	public static final String USER_SESSION_GROUPS = "groupIds";
	/**
	 * session中user的部门范围是否全部
	 */
	public static final String USER_SESSION_DEPARTMENT_ALL = "userDepartmentAll";

	/**
	 * 页面方式-填加
	 */
	public static final String PAGE_HANDLE_METHOD_ADD = "add";
	/**
	 * 填加权限号
	 */
	public static final String PAGE_HANDLE_METHOD_ADD_NUM = "3";
	/**
	 * 页面方式-修改
	 */
	public static final String PAGE_HANDLE_METHOD_MODIFY = "modify";
	/**
	 * 修改权限号
	 */
	public static final String PAGE_HANDLE_METHOD_MODIFY_NUM = "4";
	/**
	 * 页面方式-列表显示
	 */
	public static final String PAGE_HANDLE_METHOD_LIST = "list";

	/**
	 * 列表权限号
	 */
	public static final String PAGE_HANDLE_METHOD_LIST_NUM = "1";

	/**
	 * 页面方式-页面显示
	 */
	public static final String PAGE_HANDLE_METHOD_VIEW = "view";

	public static final String USER_SESSION_SCOPE_KEY = "scopeId";

	/**
	 * 1增加权限1新建 2修改3提交审批4删除5检索6其他7改变状态8复制9打印
	 */
	public static final String RIGHTS_ADD = "1";

	/**
	 *  2修改
	 */
	public static final String RIGHTS_MODIFY = "2";
	/**
	 *  3提交审批
	 */
	public static final String RIGHTS_SUBMIT = "3";
	/**
	 *  4删除
	 */
	public static final String RIGHTS_DEL = "4";
	/**
	 *  5检索
	 */
	public static final String RIGHTS_SEARCH = "5";
	/**
	 *  6其他
	 */
	public static final String RIGHTS_ORTHER = "6";
	/**
	 *  7改变状态
	 */
	public static final String RIGHTS_CHANGE = "7";
	/**
	 *  8复制
	 */
	public static final String RIGHTS_COPY = "8";

	/**
	 *  9打印
	 */
	public static final String RIGHTS_PRINT = "9";

	/**
	 * 0状态失效
	 */
	public static final String DIC_STATE_NO = "0";
	/**
	 * 1状态生效(审批通过)
	 */
	public static final String DIC_STATE_YES = "1";

	/**
	 * 2状态 审批中
	 */
	public static final String DIC_STATE_WORKFLOW_IN_PROCESS = "2";
	/**
	 * 1状态 审批通过
	 */
	public static final String DIC_STATE_WORKFLOW_YES_PROCESS = "1";
	/**
	 * 3状态 新建
	 */
	public static final String DIC_STATE_NEW = "3";
	public static final String DIC_STATE_NEW_NAME = com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME;
	/**
	 * 20状态 审批中，需编辑
	 */
	public static final String DIC_STATE_WORKFLOW_IN_PROCESS_EDIT = "20";
	/**
	 * 2状态 审批中，需编辑前缀
	 */
	public static final String DIC_STATE_WORKFLOW_IN_PROCESS_EDIT_PRE = "2";

	/**
	 * 0状态 
	 */
	public static final String DIC_STATE_WORKFLOW_NO_PROCESS = "0";
	/**
	 * 显示权限号
	 */
	public static final String PAGE_HANDLE_METHOD_VIEW_NUM = "2";
	//用于归类普通操作,在列表外,如填加,编辑,提交审批
	public static final String PAGE_HANDLE_COMMON_NUM = "0";
	//用于归类动作操作,在选择动作列表内,如生效，失效等
	public static final String PAGE_HANDLE_ACTION_NUM = "1";
	//用于归类检索操作,在选择检索列表内
	public static final String PAGE_HANDLE_SEARCH_NUM = "2";

	/**
	 * 页面方式-页面处理变量名
	 */
	public static final String PAGE_HANDLE_METHOD_NAME = "handlemethod";

	/**
	 * 状表字典表id为1表示生效
	 */
	public static final String DIC_STATE_YES_ID = "1";
	/**
	 * 状态字典表ID为0的表示报废
	 */
	public static final String DIC_STATE_NO_ID = "0";

}
