package com.biolims.common.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.SearchConditionsDao;
import com.biolims.common.model.search.SearchConditions;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;

@Service
public class SearchConditionsService {

	@Resource
	private SearchConditionsDao searchConditionsDao;

	public List<SearchConditions> findSearchConditions(String userId, String fkModel) throws Exception {
		return searchConditionsDao.selectSearchConditions(userId, fkModel);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SearchConditions sc) throws Exception {
		searchConditionsDao.save(sc);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void deleteSearchConditions(String id) throws Exception {
		SearchConditions sc = searchConditionsDao.get(SearchConditions.class, id);
		searchConditionsDao.delete(sc);
	}

}
