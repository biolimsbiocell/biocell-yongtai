/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：
 * 类描述：通用字典表访问service类
 * 创建人：倪毅
 * 创建时间：2011-8
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.common.service;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import oracle.net.aso.o;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.StringUtils;

@Service
public class CommonService {

	@Resource
	private CommonDAO commonDAO;

	/**
	 * 
		 * 检索通用字典OBJECT
		 * 
		 *@return list
	 */
	public <T> List<T> findObject(Class<T> persistentClass) {
		List<T> list = commonDAO
				.find("from " + persistentClass.getName() + " where state='1' order by orderNumber asc");
		return list;

	}

	public CommonDAO getCommonDAO() {
		return commonDAO;
	}

	public void setCommonDAO(CommonDAO commonDAO) {
		this.commonDAO = commonDAO;
	}

	/**
	 * 
	 * 增加或更改对象
	 * 
	 * 
	 */

	public <T> void save(T o) {
		commonDAO.saveOrUpdate(o);

	}

	public <T> T get(Class<T> persistentClass, Serializable id) {
		T t = (T) commonDAO.get(persistentClass, id);
		//////getSession().clear();
		return t;
	}

	public <T> List<T> get(Class<T> persistentClass, String propertyName, String value) {
		List<T> t = (List<T>) commonDAO.findByProperty(persistentClass, propertyName, value);
		//////getSession().clear();
		return t;
	}

	public <T> List<T> find(String hql) {
		List<T> t = (List<T>) commonDAO.find(hql);
		//////getSession().clear();
		return t;
	}

	@Transactional(rollbackFor = Exception.class)
	public <T> void saveOrUpdate(T bean) {
		commonDAO.saveOrUpdate(bean);
	}

	@Transactional(rollbackFor = Exception.class)
	public <T> void saveOrUpdate(Collection<T> collenction) {
		commonDAO.saveOrUpdateAll(collenction);
	}

	@Transactional(rollbackFor = Exception.class)
	public void setObjectState(Object o, String id, String value) throws Exception {
		o = commonDAO.get(o.getClass(), id);

		try {
			BeanUtils.getDeclaredField(o, "state");
			BeanUtils.setFieldValue(o, "state", value);
			commonDAO.update(o);

		} catch (NoSuchFieldException n) {

		}

	}

	public <T> String hasId(String id, Class<T> persistentClass) throws Exception {
		Map<String, String> messageMap = new HashMap<String, String>();
		Object[] a = { id };
		long count = commonDAO.getCount("from " + persistentClass.getName() + " where id =?", a);
		if (count > 0)
			messageMap.put("message", "编码已经存在，请重新设置编码！");
		else
			messageMap.put("message", "");
		return JsonUtils.toJsonString(messageMap);
	}

	public <T> String hasId(String id, String obj) throws Exception {
		Map<String, String> messageMap = new HashMap<String, String>();
		Object[] a = { id };

		if (!id.trim().equals(id)) {
			messageMap.put("message", "编码前后不能有空格！");
		} else {

			if (StringUtils.isOnlyDitalLetter(id)) {
				long count = commonDAO.getCount("from " + obj + " where id =?", a);
				if (count > 0)
					messageMap.put("message", "编码已经存在，请重新设置编码！");
				else
					messageMap.put("message", "");
			} else
				messageMap.put("message", "编码只能是字母，#-@.字符或者数字！");
		}

		return JsonUtils.toJsonString(messageMap);
	}

	public <T> String hasCode(String code, Class<T> persistentClass) throws Exception {
		Map<String, String> messageMap = new HashMap<String, String>();
		Object[] a = { code };
		long count = commonDAO.getCount("from " + persistentClass.getName() + " where code =?", a);
		if (count > 0)
			messageMap.put("message", "编码已经存在，请重新设置编码！");
		else
			messageMap.put("message", "");
		return JsonUtils.toJsonString(messageMap);
	}

	public <T> String hasCode(String code, String obj) throws Exception {
		Map<String, String> messageMap = new HashMap<String, String>();
		Object[] a = { code };

		if (!code.trim().equals(code)) {
			messageMap.put("message", "编码前后不能有空格！");
		} else {

			if (StringUtils.isOnlyDitalLetter(code)) {
				long count = commonDAO.getCount("from " + obj + " where code =?", a);
				if (count > 0)
					messageMap.put("message", "编码已经存在，请重新设置编码！");
				else
					messageMap.put("message", "");
			} else
				messageMap.put("message", "编码只能是字母，#-@.字符或者数字！");
		}

		return JsonUtils.toJsonString(messageMap);
	}

	public <T> Map<String, Object> findCommonObject(String className, String condition, int startNum, int limitNum,
			String dir, String sort, String contextPath) throws Exception {
		String where = "where 1=1 ";
		if (condition != null && !condition.equals("")) {
			where = where + " and " + condition;
		}
		String hql = "from " + className + " ";
		Map<String, Object> controlMap = findObject(hql + where, startNum, limitNum, dir, sort);
		List<T> list = (List<T>) controlMap.get("list");
		controlMap.put("list", list);
		return controlMap;
	}

	public Map<String, Object> findObject(String shql, int startNum, int limitNum, String dir, String sort)
			throws Exception {
		return commonDAO.findObject(shql, startNum, limitNum, dir, sort);

	}

	public <T> Map<String, Object> findObjectCondition(int startNum, int limitNum, String dir, String sort,
			Class<T> persistentClass, Map mapForQuery, Map mapForCondition) throws Exception {
		return commonDAO.findObjectCondition(startNum, limitNum, dir, sort, persistentClass, mapForQuery,
				mapForCondition);
	}

	@SuppressWarnings("unchecked")
	public <T> Map<String, Object> findObjectList(int startNum, int limitNum, String dir, String sort,
			String contextPath, T t) throws Exception {
		//检索条件map,每个字段检索值
		Map<String, Object> mapForQuery = new HashMap<String, Object>();

		//检索方式map,每个字段的检索方式
		Map<String, String> mapForCondition = new HashMap<String, String>();
		mapForCondition.put("id", "like");
		mapForQuery = BeanUtils.po2MapNotNull(mapForCondition, t);
		//将map值传入，获取列表
		Map<String, Object> controlMap = commonDAO.findObjectCondition(startNum, limitNum, dir, sort, t.getClass(),
				mapForQuery, mapForCondition);
		List<T> list = (List<T>) controlMap.get("list");

		controlMap.put("list", list);
		return controlMap;
	}
}
