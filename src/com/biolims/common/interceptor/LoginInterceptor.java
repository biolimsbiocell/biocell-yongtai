package com.biolims.common.interceptor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.Rights;
import com.biolims.common.model.user.User;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

public class LoginInterceptor implements Interceptor {

	private String path;

	private static final long serialVersionUID = -4911122707518868115L;

	public void destroy() {
	}

	public void init() {
	}

	public String intercept(ActionInvocation actionInvocation) throws Exception {
		HttpServletRequest req = ServletActionContext.getRequest();
		HttpServletResponse res = ServletActionContext.getResponse();
		HttpSession session = req.getSession();
		User user = (User) session.getAttribute(SystemConstants.USER_SESSION_KEY);
		if (user == null) {
			String url = req.getContextPath() + path;
			res.setHeader("Refresh", "0; URL=" + url);
			return null;
		} else {
			ActionContext ctx = ActionContext.getContext();
			String rightsId = (String) req.getAttribute("rightsId");
			if (rightsId != null && !rightsId.equals("null") && !rightsId.equals("")) {
				String contentId = "";
				String allowRights = "";
				String actionName = actionInvocation.getInvocationContext().getName();
				if (session.getAttribute("contentId") != null) {
					contentId = (String) session.getAttribute("contentId");

					Map parameters = actionInvocation.getInvocationContext().getParameters();
					if (actionName.contains("Edit") && parameters.get("id") != null) {
						String[] t = (String[]) parameters.get("id");
						if (t[0].equals(contentId)) {
							allowRights = "true";

						}

					}

				}
				if ((actionName.contains("Json") || actionName.contains("get") || actionName.contains("View"))
						|| rightsId.equals("noRights")) {

					allowRights = "true";

				}
				List userRightsMap = (List) session.getAttribute(SystemConstants.USER_SESSION_RIGHTS);
				if (hasRights(userRightsMap, rightsId) == true || allowRights.equals("true")) {

				} else {
					HashMap<String, String> map = new HashMap();
					map.put("message", "您没有权限,请联系管理员!");
					ctx.put("messageMap", map);
					req.setAttribute("message", "您没有权限,请联系管理员!");
					return "global.checkrights.exception";
				}
			}
		}
		return actionInvocation.invoke();
	}

	public void setPath(String path) {
		this.path = path;
	}

	public boolean hasRights(List list, String rightsId) {
		for (int i = 0; i < list.size(); i++) {
			Rights r = (Rights) list.get(i);

			if (r.getId().equals(rightsId) || r.getLevelId().equals(rightsId)) {
				return true;
			}
		}
		return false;
	}
}