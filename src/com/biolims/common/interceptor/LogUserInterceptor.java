package com.biolims.common.interceptor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.Rights;
import com.biolims.common.model.user.User;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

public class LogUserInterceptor implements Interceptor {

	private String path;

	private static final long serialVersionUID = -4911122707518868115L;

	public void destroy() {
	}

	public void init() {
	}

	public String intercept(ActionInvocation actionInvocation) throws Exception {
		HttpServletRequest req = ServletActionContext.getRequest();
		HttpServletResponse res = ServletActionContext.getResponse();
		HttpSession session = req.getSession();
		User user = (User) session.getAttribute(SystemConstants.USER_SESSION_KEY);
		if (user == null) {
			String url = req.getContextPath() + path;
			res.setHeader("Refresh", "0; URL=" + url);
			return null;
		}
		return actionInvocation.invoke();
	}

	public void setPath(String path) {
		this.path = path;
	}

}