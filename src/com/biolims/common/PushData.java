package com.biolims.common;

import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.util.JSONTokener;

import com.biolims.system.customfields.model.FieldTemplateItem;

public class PushData {

	public static String pushData(String draw, Map<String, Object> map,
			String data) {
		String dataJson = "";
		if (map.size() > 0) {
			dataJson = "{\"draw\":" + Integer.parseInt(draw)
					+ ",\"recordsTotal\":" + map.get("recordsTotal")
					+ ",\"recordsFiltered\":" + map.get("recordsFiltered")
					+ ",\"data\":" + data + "}";
		} else {
			dataJson = "{\"draw\":" + Integer.parseInt(draw)
					+ ",\"recordsTotal\":" + 0 + ",\"recordsFiltered\":" + 0
					+ ",\"data\":" + data + "}";
		}
		return dataJson;

	}

	/**
	 * 
	 * @Title: pushFieldData
	 * @Description: TODO(将自定义字段的数据加入返回的数据中)
	 * @author 尹标舟
	 * @date 2018-3-14下午1:03:57
	 * @param data
	 * @return String
	 * @throws
	 */
	public static String pushFieldData(String data, Map<String, Object> map) {
		JSONArray jsonArray = JSONArray.fromObject(data);
		JSONArray jsonArrayStr = JSONArray.fromObject(data);
		List<FieldTemplateItem> list = (List<FieldTemplateItem>) map.get("result");
		
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			JSONObject fromObject=new JSONObject();
			for (int j = 0; j < list.size(); j++) {
				FieldTemplateItem fieldTemplateItem = list.get(j);
				String fieldContent = (String) jsonObject.get("fieldContent");
				if(fieldContent!=null && !fieldContent.equals("")&&!fieldContent.equals("{}"))
					fromObject = JSONObject.fromObject(fieldContent);
					
				if(fromObject.keySet().contains(fieldTemplateItem.getFieldName())&&fromObject.getString(fieldTemplateItem.getFieldName())!=null&&!"".equals(fromObject.getString(fieldTemplateItem.getFieldName()))){
					Object listArray = new JSONTokener(fromObject.getString(fieldTemplateItem.getFieldName())).nextValue();
					String strValue = "";
					if (listArray instanceof JSONArray) {
						JSONArray jsonArray1 = (JSONArray) listArray;
						for (int k = 0; k < jsonArray1.size(); k++) {
							String index =  (String) jsonArray1.get(k);
							String[] OptionNameArray = fieldTemplateItem.getSingleOptionName().split("/");
							strValue += OptionNameArray[Integer.parseInt(index)] + ",";
						}
					} else if (listArray instanceof JSONObject) {
						JSONObject jsonObject3 = (JSONObject) listArray;
						strValue = jsonObject3.getString(fieldTemplateItem.getFieldName());
					}else{
						strValue = listArray.toString();
					}
					jsonArrayStr.getJSONObject(i).put(fieldTemplateItem.getFieldName(), strValue);
				}else{
					jsonArrayStr.getJSONObject(i).put(fieldTemplateItem.getFieldName(), "");
				}
			}
		}
		return jsonArrayStr.toString();
	}
}
