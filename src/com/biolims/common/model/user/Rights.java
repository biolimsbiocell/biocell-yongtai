package com.biolims.common.model.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.biolims.dao.EntityDao;

@Entity
@Table(name = "T_RIGHTS")
public class Rights extends EntityDao<Rights> {

	private static final long serialVersionUID = -7915457065164332028L;
	@Id
	@Column(name = "ID", length = 32)
	private String id;

	@Column(name = "NAME", length = 80)
	private String name; //名称

	@Column(name = "EN_NAME", length = 80)
	private String enName; //名称
	
	@Column(name = "NOTE", length = 100)
	private String note; //说明

	@Column(name = "RIGHTS_FUNCTION", length = 32)
	private String rightsFunction; //操作类型字符串  1新建 2修改3提交审批4删除5检索6其他

	@Column(name = "LEAF", length = 2)
	private String leaf; //是否叶子 0 父级:菜单目录  1 子级:菜单项  

	@Column(name = "LEVEL_NUMBER", length = 3)
	private int level; //级别 1,2,3

	@Column(name = "LEVEL_ID")
	private String levelId; //级别 1.1

	@Column(name = "STATE", length = 2)
	private String state; //状态 0失效 1生效

	@Column(name = "PARENT_ID", length = 32)
	private String parentId; //上级id

	public String getLevelId() {
		return levelId;
	}

	public void setLevelId(String levelId) {
		this.levelId = levelId;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getContenthref() {
		return contenthref;
	}

	public void setContenthref(String contenthref) {
		this.contenthref = contenthref;
	}

	//功能连接
	@Column(name = "CONTENTHREF")
	private String contenthref;

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getLeaf() {
		return leaf;
	}

	public void setLeaf(String leaf) {
		this.leaf = leaf;
	}

	@Column(name = "ORDER_NUMBER")
	private int orderNumber;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getRightsFunction() {
		return rightsFunction;
	}

	public void setRightsFunction(String rightsFunction) {
		this.rightsFunction = rightsFunction;
	}

	public String getEnName() {
		return enName;
	}

	public void setEnName(String enName) {
		this.enName = enName;
	}

}
