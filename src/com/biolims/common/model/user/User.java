package com.biolims.common.model.user;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

import org.hibernate.annotations.ForeignKey;

import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.Empty2Null;
import com.biolims.core.model.user.Department;
import com.biolims.core.model.user.DicJob;
import com.biolims.dic.model.DicState;
import com.biolims.dic.model.DicType;

@Entity
@Table(name = "T_USER")
public class User extends BaseUser {

	private static final long serialVersionUID = -759731055446276228L;

	// 用户账号
	@Id
	@Column(name = "ID", length = 80)
	private String id;

	// 姓名
	@Column(name = "NAME", length = 80)
	private String name;

	// 用户密码
	@Column(name = "USER_PASSWORD", length = 80)
	private String userPassword;

	@Column(name = "EMAIL_PASSWORD", length = 80)
	private String emailPassword;
	// 状态
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "DIC_STATE_ID")
	private DicState state;

	
	//生产状态
	private String productState;
	// 排序号
	@Column(name = "ORDER_NUMBER")
	private int orderNumber = 0;

	// e-mail
	@Column(name = "EMAIL", length = 80)
	private String email;

	@Column(name = "FILE_ID", length = 80)
	private String fileId;

	// 办公电话
	@Column(name = "PHONE", length = 60)
	private String phone;

	// 性别
	@Column(name = "SEX", length = 2)
	private String sex;

	// 生日
	@Column(name = "BIRTHDAY")
	private Date birthday;

	// 入职日期
	@Column(name = "IN_DATE")
	private Date inDate;

	// 地址
	@Column(name = "ADDRESS", length = 60)
	private String address;

	// 片区
	@Column(name = "AREA", length = 60)
	private String area;

	// 城市
	@Column(name = "CITY", length = 60)
	private String city;

	// 岗位
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "DIC_JOB_ID")
	private DicJob dicJob;

	// 手机
	@Column(name = "MOBILE", length = 60)
	private String mobile;

	// 身份证号
	@Column(name = "CARD_NO", length = 60)
	private String cardNo;

	// 教育程度
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "DIC_EDU_TYPE_ID")
	private DicType edu;

	// 部门
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "DEPARTMENT_ID")
	private Department department;

	// 毕业学校
	@Column(name = "GRADUATE", length = 50)
	private String graduate;

	// 微信号
	@Column(name = "WEI_CHAT_ID", length = 50)
	private String weichatId;

	// 联系方式
	@Column(name = "CONTACT_METHOD", length = 60)
	private String contactMethod;

	// 紧急联系人
	@Column(name = "EMERGENCY_CONTACT_PERSON", length = 60)
	private String emergencyContactPerson;

	// 紧急联系方式
	@Column(name = "EMERGENCY_CONTACT_METHOD", length = 100)
	private String emergencyContactMethod;

	@Transient
	private String roles;
	@Transient
	private String userGroups;
	@Transient
	private String userJobs;
	@Transient
	private String password;

	@Transient
	private String oldPassword;

	@Column(name = "SCOPE_ID", length = 200)
	private String scopeId;

	@Column(name = "SCOPE_NAME", length = 200)
	private String scopeName;

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public DicType getEdu() {
		return edu;
	}

	public void setEdu(DicType edu) {
		this.edu = edu;
	}

	public String getUserGroups() {
		return userGroups;
	}

	public void setUserGroups(String userGroups) {
		this.userGroups = userGroups;
	}

	public Date getInDate() {
		return inDate;
	}

	public void setInDate(Date inDate) {
		this.inDate = inDate;
	}

	public String getEmergencyContactPerson() {
		return emergencyContactPerson;
	}

	public void setEmergencyContactPerson(String emergencyContactPerson) {
		this.emergencyContactPerson = emergencyContactPerson;
	}

	public String getEmergencyContactMethod() {
		return emergencyContactMethod;
	}

	public void setEmergencyContactMethod(String emergencyContactMethod) {
		this.emergencyContactMethod = emergencyContactMethod;
	}

	public String getContactMethod() {
		return contactMethod;
	}

	public void setContactMethod(String contactMethod) {
		this.contactMethod = contactMethod;
	}

	public String getGraduate() {
		return graduate;
	}

	public void setGraduate(String graduate) {
		this.graduate = graduate;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public DicJob getDicJob() {
		return dicJob;
	}

	public void setDicJob(DicJob dicJob) {
		this.dicJob = dicJob;
	}

	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public DicState getState() {
		return state;
	}

	public void setState(DicState state) {
		this.state = state;
	}

	public int getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getUserJobs() {
		return userJobs;
	}

	public void setUserJobs(String userJobs) {
		this.userJobs = userJobs;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public User() {
	}

	public String getEmailPassword() {
		return emailPassword;
	}

	public void setEmailPassword(String emailPassword) {
		this.emailPassword = emailPassword;
	}

	

	public String getScopeId() {
		return scopeId;
	}

	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	public String getScopeName() {
		return scopeName;
	}

	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getWeichatId() {
		return weichatId;
	}

	public void setWeichatId(String weichatId) {
		this.weichatId = weichatId;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getProductState() {
		return productState;
	}

	public void setProductState(String productState) {
		this.productState = productState;
	}

}
