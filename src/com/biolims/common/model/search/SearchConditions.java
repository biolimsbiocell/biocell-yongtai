package com.biolims.common.model.search;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.biolims.dao.EntityDao;

/**
 * 检索条件
 * @author cong
 *
 */
@Entity
@Table(name = "T_SEARCH_CONDITIONS")
public class SearchConditions extends EntityDao<SearchConditions> implements Serializable {

	private static final long serialVersionUID = 7368110761621967038L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;//ID

	@Column(name = "NAME", length = 20)
	private String name;

	@Column(name = "USER_ID", length = 32)
	private String userId;

	@Column(name = "F_K_MODEL", length = 20)
	private String fkModel;//模块

	@Column(name = "SEARCH_INFO", length = 500)
	private String searchInfo;//条件

	@Column(name = "ALL_INFO", length = 1000)
	private String allInfo;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFkModel() {
		return fkModel;
	}

	public void setFkModel(String fkModel) {
		this.fkModel = fkModel;
	}

	public String getSearchInfo() {
		return searchInfo;
	}

	public void setSearchInfo(String searchInfo) {
		this.searchInfo = searchInfo;
	}

	public String getAllInfo() {
		return allInfo;
	}

	public void setAllInfo(String allInfo) {
		this.allInfo = allInfo;
	}

}
