package com.biolims.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.csvreader.CsvReader;

/**
 * 百奥利盟数据初始化导入
 * @author rixu.cong
 * 2012-06-04
 */
public class CsvDataToDB {

	public static List<File> files = null;

	public static Date toDate(String str, String format) {

		Date date = null;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
		try {
			date = simpleDateFormat.parse(str);

		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

//	public static void main(String[] args) {
//
//		String input = "";
//		String logOutput = "";
//		if (args.length == 2) {
//			input = args[0].toUpperCase();
//			logOutput = args[1];
//		} else {
//			System.out.println("您指定的参数有误！");
//			return;
//		}
//		if (!new File(logOutput).exists()) {
//			new File(logOutput).mkdirs();
//		}
//
//		if (!new File(input).exists()) {
//			System.out.println("您指定的输入目录不存在！");
//		}
//
//		PrintWriter pw = null;
//		Connection con = null;
//		try {
//			con = JdbcUtil.getConnection();
//			pw = new PrintWriter(logOutput + File.separator + "log.text");
//			files = new ArrayList<File>();
//			getFiles(input, "CSV");
//			System.out.println("总共有【" + files.size() + "】个文件要处理...............");
//			int i = 0;
//			for (File file : files) {
//				System.out.println("开始处理第 " + (++i) + " 个文件：" + file.getAbsolutePath());
//				try {
//					parseFile(con, file, pw);
//					System.out.println("完成处理第 " + (i) + " 个文件：" + file.getAbsolutePath());
//				} catch (Exception e) {
//					e.printStackTrace();
//					System.out.println("处理文件：" + file.getAbsolutePath() + " 发生错误 ");
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			JdbcUtil.release(con);
//			if (pw != null)
//				pw.close();
//		}
//	}

	public static void parseFile(Connection con, File file, PrintWriter pw) throws Exception {
		InputStream is = new FileInputStream(file);
		CsvReader reader = new CsvReader(is, Charset.forName("UTF-8"));
		reader.readHeaders();
		String[] heads = reader.getHeaders();

		String tableName = file.getName().substring(0, file.getName().indexOf("."));
		String sql = getSql(tableName, heads);
		PreparedStatement ps = con.prepareStatement(sql);
		while (reader.readRecord()) {
			try {
				for (int i = 0; i < reader.getHeaderCount(); i++) {

					if (!reader.getHeader(i).contains("DATE"))
						ps.setString(i + 1, reader.get(reader.getHeader(i)));
					else

					{
						if (reader.get(reader.getHeader(i)) != null && !reader.get(reader.getHeader(i)).equals(""))
							ps.setDate(i + 1, new java.sql.Date(toDate(reader.get(reader.getHeader(i)), "yyyy/M/d")
									.getTime()));
						else
							ps.setDate(i + 1, null);

					}
				}
				//ps.setString(1, reader.get(reader.getHeader(0)));
				//ps.setString(2, reader.get(reader.getHeader(1)));
				ps.executeUpdate();
			} catch (Exception e) {
				pw.println(file.getAbsolutePath() + ":" + reader.getRawRecord());
				e.printStackTrace();
			}
		}
//		JdbcUtil.release(ps);
		is.close();
		reader.close();
	}

	public static String getSql(String tableName, String[] columns) {
		String sql = "insert into " + tableName;
		String colSql = "";
		String parm = "";
		for (String column : columns) {
			colSql = colSql + column + ",";
			parm = parm + "?,";
		}
		if (colSql.endsWith(",")) {
			colSql = colSql.substring(0, colSql.length() - 1);
		}
		if (parm.endsWith(",")) {
			parm = parm.substring(0, parm.length() - 1);
		}
		sql = sql + "(" + colSql + ") values (" + parm + ")";
		System.out.println(" sql is" + sql);
		return sql;
	}

	public static void getFiles(String path, String ext) {
		File[] fileArray = new File(path).listFiles();
		for (File file : fileArray) {
			if (file.isFile() && file.getName().toUpperCase().endsWith(ext)) {
				files.add(file);
			} else if (file.isDirectory()) {
				getFiles(file.getAbsolutePath(), ext);
			}
		}
	}

}
