package com.biolims.stepstoquery.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.crm.customer.customer.model.CrmCustomerIteam;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.dic.model.DicType;
import com.biolims.experiment.cell.passage.model.CellPassageTemplate;
import com.biolims.experiment.enmonitor.dust.model.CleanAreaDustItem;
import com.biolims.experiment.roomManagement.model.RoomManagement;
import com.biolims.sample.model.SampleFutureFind;
import com.biolims.stamp.birtVersion.model.BirtVersion;
import com.biolims.stamp.birtVersion.model.BirtVersionItem;
import com.biolims.stepstoquery.model.StepsQuery;

@Repository
@SuppressWarnings("unchecked")
public class StepsQueryDao extends BaseHibernateDao {

	/**
	 * 查询时间
	 * 
	 * @param startDate
	 * @param endDate
	 * @param stepsName
	 * @return
	 */
	public List<CellPassageTemplate> chaxun(String startDate, String endDate, String stepsName) {
		String hql = "from CellPassageTemplate  where 1=1 and productStartTime >= '" + startDate
				+ "' and productEndTime <= '" + endDate
				+ "'and productStartTime !='' and productStartTime is not null group by date_format(product_start_time,'%Y-%m-%d')";
		List<CellPassageTemplate> list = getSession().createQuery(hql).list();
		return list;

	}

	/**
	 * 根据时间查询步骤
	 * 
	 * @param sjbb
	 * @param bzname
	 * @return
	 */
	public List<Object[]> chaxunsjAndStepsName(String[] sjbb, String bzname) {
		String sj = "";
		for (String sjb : sjbb) {
			if ("".equals(sj)) {
				sj = sj + "'" + sjb + "'";
			} else {
				sj = sj + ",'" + sjb + "'";
			}
		}
		String sql = "select name,product_start_time,count(*) from cell_passage_template where 1=1 and product_start_time in ("
				+ sj + ") and name='" + bzname
				+ "'and product_start_time !='' and product_start_time is not null group by date_format(product_start_time,'%Y-%m-%d')  ORDER BY product_start_time";
		List<Object[]> list = getSession().createSQLQuery(sql).list();
		return list;
	}

}
