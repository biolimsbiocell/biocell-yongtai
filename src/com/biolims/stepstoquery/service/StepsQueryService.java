package com.biolims.stepstoquery.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.SystemCodeService;
import com.biolims.crm.customer.customer.model.CrmCustomerIteam;
import com.biolims.dic.model.DicType;
import com.biolims.equipment.common.constants.SystemConstants;
import com.biolims.experiment.cell.passage.model.CellPassageTemplate;
import com.biolims.experiment.roomManagement.dao.RoomManagementDao;
import com.biolims.experiment.roomManagement.model.RoomManagement;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.stamp.birtVersion.dao.BirtVersionRepository;
import com.biolims.stamp.birtVersion.model.BirtVersion;
import com.biolims.stamp.birtVersion.model.BirtVersionItem;
import com.biolims.stepstoquery.dao.StepsQueryDao;
import com.biolims.stepstoquery.model.StepsQuery;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class StepsQueryService {

	@Resource
	private CommonDAO commonDAO;
	@Resource
	private StepsQueryDao stepsQueryDao;
	@Resource
	private SystemCodeService systemCodeService;

	/**
	 * 查询时间
	 * 
	 * @param startDate
	 * @param endDate
	 * @param stepsName
	 * @return
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<CellPassageTemplate> chaxun(String startDate, String endDate, String stepsName) {
		List<CellPassageTemplate> list = stepsQueryDao.chaxun(startDate, endDate, stepsName);
		for (CellPassageTemplate cpt : list) {
			String startTimeStr = cpt.getProductStartTime();
			cpt.setProductStartTime(startTimeStr.substring(0, 10));
		}
		return list;
	}

	/**
	 * 根据日期查询步骤
	 * 
	 * @param sjbb
	 * @param bzname
	 * @return
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public List<Object[]> chaxunsjAndStepsName(String[] sjbb, String bzname) {
		return stepsQueryDao.chaxunsjAndStepsName(sjbb, bzname);
	}

	/**
	 * 根据步骤ID获取步骤名
	 * 
	 * @param stepsName
	 * @return
	 */
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String[] findstepsNameIds(String stepsName) {
		String[] arr = stepsName.split(",");
		String[] brr = new String[arr.length];
		for (int i = 0; i < arr.length; i++) {
			DicType dt = new DicType();
			dt = commonDAO.get(DicType.class, arr[i]);
			if (dt != null) {
				brr[i] = dt.getName();
			} else {
				brr[i] = "";
			}

		}
		return brr;
	}

}
