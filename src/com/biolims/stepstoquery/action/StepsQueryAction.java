package com.biolims.stepstoquery.action;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;

import com.biolims.common.service.CommonService;

import com.biolims.dic.model.DicType;
import com.biolims.experiment.cell.passage.model.CellPassageTemplate;

import com.biolims.file.service.FileInfoService;

import com.biolims.stepstoquery.service.StepsQueryService;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;

@Namespace("/stepstoquery/stepsQuery")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class StepsQueryAction extends BaseActionSupport {
	private static final long serialVersionUID = 3488450258677393696L;

	private String rightsId = "";

	@Autowired
	private StepsQueryService stepsQueryService;
	@Resource
	private FieldService fieldService;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CommonService commonService;

	/**
	 * 跳转页面
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showStepQuery")
	public String showStepQuery() throws Exception {
		rightsId = "2019112";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/stepsQuery/stepsQuery/stepsQueryEdit.jsp");
	}

	/**
	 * 根据条件查询
	 * 
	 * @throws Exception
	 */
	@Action(value = "chaxun")
	public void chaxun() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String startDate = getParameterFromRequest("kssj");
		String endDate = getParameterFromRequest("jssj");
		String stepsName = getParameterFromRequest("stepsName");
		try {
			List<CellPassageTemplate> list = stepsQueryService.chaxun(startDate, endDate, stepsName);
			map.put("sj", list);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 生产步骤查询列表
	 * 
	 * @throws Exception
	 */
	@Action(value = "showStepsNameListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showStepsNameListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String sjbb1 = getParameterFromRequest("sjbb");
		String stepsName = getParameterFromRequest("stepsName");
		String[] sjbb = sjbb1.split(",");
		// 根据步骤ID获取步骤名,返回数组格式
		String[] stepsName1 = stepsQueryService.findstepsNameIds(stepsName);

		try {
			Map<String, Object> result = new HashMap<String, Object>();
			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			if (!"".equals(stepsName) && stepsName != null) {
				for (String bz : stepsName1) {
					Map<String, Object> a = new HashMap<String, Object>();
					a.put("name", bz);
					a.put("warningLine", "");
					List<Object[]> sffs = stepsQueryService.chaxunsjAndStepsName(sjbb, bz);
					List<DicType> ms1 = commonService.get(DicType.class, "name", bz);
					if (sffs.size() > 0) {
						if (ms1.size() > 0) {
							System.out.println(ms1.get(0).getCode());
							a.put("warningLine", ms1.get(0).getCode());
						}
					}
					for (String zzz : sjbb) {
						if (sffs.size() > 0) {
							String ms = "";
							for (Object[] obj : sffs) {
								if (zzz.equals(obj[1])) {
									// 计数
									ms = ms + obj[2];

								}
							}
							if (!"".equals(ms) && ms != null) {
								a.put(zzz, ms);
							} else {
								a.put(zzz, 0);
							}

						} else {
							a.put(zzz, 0);
						}
					}

					list.add(a);
				}
			}

			HttpUtils.write(PushData.pushData(draw, result, JsonUtils.toJsonString(list)));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public StepsQueryService getStepsQueryService() {
		return stepsQueryService;
	}

	public void setStepsQueryService(StepsQueryService stepsQueryService) {
		this.stepsQueryService = stepsQueryService;
	}

	public FieldService getFieldService() {
		return fieldService;
	}

	public void setFieldService(FieldService fieldService) {
		this.fieldService = fieldService;
	}

	public FileInfoService getFileInfoService() {
		return fileInfoService;
	}

	public void setFileInfoService(FileInfoService fileInfoService) {
		this.fileInfoService = fileInfoService;
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

}
