package com.biolims.stepstoquery.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.experiment.roomManagement.model.RoomManagement;

/**
 * @Title: Model
 * @Description: 报表版本管理主数据实体
 */

@Entity
@Table(name = "STEPS_QUERY")
@SuppressWarnings("serial")
public class StepsQuery extends EntityDao<StepsQuery> implements java.io.Serializable, Cloneable {

	// id
	private String id;

	// 名称
	private String name;

	// 日期
	private Date riqi;

	// 警戒线
	private String warningLine;
	
	//次数
	private String countNumber;


	@Id
	@Column(name = "ID", length = 50)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWarningLine() {
		return warningLine;
	}

	public void setWarningLine(String warningLine) {
		this.warningLine = warningLine;
	}

	public Date getRiqi() {
		return riqi;
	}

	public void setRiqi(Date riqi) {
		this.riqi = riqi;
	}

	public String getCountNumber() {
		return countNumber;
	}

	public void setCountNumber(String countNumber) {
		this.countNumber = countNumber;
	}
	
	

}
