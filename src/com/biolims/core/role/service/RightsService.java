/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：Service
 * 类描述：权限管理service
 * 创建人：倪毅
 * 创建时间：2011-9
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.core.role.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.Rights;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.core.model.user.ActionRights;
import com.biolims.core.model.user.Department;
import com.biolims.core.model.user.Role;
import com.biolims.core.model.user.RoleRights;
import com.biolims.util.JsonUtils;

@Service
public class RightsService {
	@Resource
	private CommonDAO commonDAO;
	@Autowired
	CommonService commonService;

	/**
	 * 
	 * 验证登录
	 * 
	 * 
	 */
	public User loginUser(String id, String password) {
		User user = commonDAO.get(User.class, id);

		return user;

	}

	/**
	 * 
	 * 检索权限树,返回map
	 * 
	 * 
	 */
	public List<Rights> findRights() throws Exception {
		List<Rights> rightList = commonDAO.find("from Rights where state ='1'");
		return rightList;

	}

	/**
	 * 
	 * 检索权限树,返回map
	 * 
	 * 
	 */
	public List<RoleRights> findRoleRights(String roleId) throws Exception {
		List<RoleRights> rightList = commonDAO.find("from RoleRights  where role.id='" + roleId + "'");
		return rightList;

	}

	/**
	 * 
	 * 检索权限树,返回map
	 * 
	 * 
	 */
	public String findRoleRightsStr(String rightId, String roleId) throws Exception {
		List<RoleRights> rightList = commonDAO.find("from RoleRights  where rights.id ='" + rightId + "' and role.id='"
				+ roleId + "'");
		String rs = "";
		for (int i = 0; i < rightList.size(); i++) {
			rs += rightList.get(i).getRightsAction() + "|";

		}
		return rs;

	}

	public RoleRights findRoleRights(String rightId, String roleId) throws Exception {
		List<RoleRights> rightList = commonDAO.find("from RoleRights  where rights.id ='" + rightId + "' and role.id='"
				+ roleId + "'");

		if (rightList.size() > 0)
			return rightList.get(0);

		return null;

	}

	public Rights findRights(String rightId) throws Exception {
		List<Rights> rightList = commonDAO.find("from Rights  where id ='" + rightId + "'");

		if (rightList.size() > 0)
			return rightList.get(0);

		return null;

	}

	/**
	 * 
	 * 检索权限树,返回map
	 * 
	 * 
	 */
	public String findRoleRightsStr(String roleId) throws Exception {
		List<RoleRights> rightList = commonDAO.find("from RoleRights  where  role.id='" + roleId + "'");
		String rs = "";
		for (int i = 0; i < rightList.size(); i++) {
			rs += rightList.get(i).getRightsAction() + "|";

		}
		return rs;

	}

	/**
	 * 
	 * 检索权限树,返回map
	 * 
	 * 
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void saveRoleRights(String jsonString, String roleId) throws Exception {

		/*String strArray[]=rightsStr.split(",");
		
		Role r = new Role();
		r.setId(roleId);
		
		for(int i=0;i<strArray.length;i++){
			Rights rt = new Rights();
			rt.setId(strArray[i]);
			RoleRights rr = new RoleRights();
			rr.setRole(r);
			rr.setRights(rt);
			commonDAO.saveOrUpdate(rr);			
			
		}*/

		// 将json读入map
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(jsonString, List.class);
		List<RoleRights> hasRightsList = new ArrayList<RoleRights>();
		List<RoleRights> oldRightsList = new ArrayList<RoleRights>();
		for (Map<String, Object> map : list) {
			RoleRights em = new RoleRights();

			// 将map信息读入实体类
			em = (RoleRights) commonDAO.Map2Bean(map, em);
			hasRightsList.add(em);

			//commonDAO.saveOrUpdate(em);

		}
		commonDAO.saveOrUpdateAll(hasRightsList);
		oldRightsList = findRoleRights(roleId);

		for (RoleRights rr : oldRightsList) {
			if (!hasRightsList.contains(rr)) {
				commonDAO.delete(rr);

			}

		}

	}

	public boolean isHasRights(Rights r, List<Rights> rightList) {
		if (rightList.contains(r)) {

			return true;
		}
		return false;

	}

	/**
	 * 
	 * 检索权限树,返回List
	 * 
	 * 
	 */
	public List<Rights> findUserRights(String userId) throws Exception {
		List<Rights> rightList = commonDAO
				.find("select distinct b.rights from UserRole a,RoleRights b where a.role = b.role and a.user.id='"
						+ userId + "'");
		return rightList;

	}

	/**
	 * 
	 * 生成用户权限树,返回List
	 * 
	 * 
	 */
	public List<Rights> findUserRightsTree(String userId) throws Exception {
		List<Rights> allRights = findRights();
		List<Rights> userRights = findUserRights(userId);
		List<Rights> buildRights = new ArrayList<Rights>();
		for (int i = 0; i < allRights.size(); i++) {
			Rights rt = allRights.get(i);
			for (int j = 0; j < userRights.size(); j++) {
				Rights rt1 = userRights.get(j);
				if (rt.getLevelId().startsWith(rt1.getLevelId() + ".")
						|| rt1.getLevelId().startsWith(rt.getLevelId() + ".")
						|| rt1.getLevelId().equals(rt.getLevelId())) {
					buildRights.add(rt);
					break;
				}
			}
		}
		return buildRights;
	}

	/**
	 * 
	 * 检索权限动作,返回List
	 * 
	 * 
	 */
	public List<RoleRights> findUserRightsActionRights(String userId) throws Exception {
		List<RoleRights> rightList = commonDAO
				.find("select b from UserRole a,RoleRights b where a.role = b.role  and a.user.id='" + userId + "'");
		return rightList;

	}

	/**
	 * 
	 * 通过userId,权限检索权限动作,返回List
	 * 
	 * 
	 */
	public List<ActionRights> findUserRightsActionRights(String userId, String rightsId) throws Exception {
		List<ActionRights> rightList = commonDAO
				.find("select distinct c.actionRights from UserRole a,RoleRights b,RoleRightsActionRights c where a.role = b.role and b.role = '"
						+ rightsId + "' and b.id = c.roleRights and a.user.id='" + userId + "'");
		return rightList;

	}

	/**
	 * 
	 * 检索权限部门范围,返回List
	 * 
	 * 
	 */
	public List<Department> findUserRightsDepartment(User user) throws Exception {
		String userId = user.getId();
		Object[] a = { userId };
		List<Department> departmentList = commonDAO
				.find(
						"select distinct c.department from UserRole a,RoleRights b,RoleRightsDepartment c where a.role = b.role and b.id = c.roleRights and a.user.id=?",
						a);
		Department userDeparment = user.getDepartment();
		//加入自己所属部门
		if (!departmentList.contains(userDeparment))
			departmentList.add(userDeparment);
		return departmentList;

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void saveRoleActionRights(String roleRightsId, String rightsStr) throws Exception {

		RoleRights rr = commonDAO.get(RoleRights.class, roleRightsId);
		rr.setRightsAction(rightsStr);
		commonDAO.saveOrUpdate(rr);
	}

	/**
	 * 
	 * 检索map
	 * 
	 * 
	 */
	public Map<String, Object> findMap(Map<String, Object> aMap) throws Exception {
		for (String propertie : aMap.keySet()) {
			Object o = aMap.get(propertie);
		}
		return aMap;
	}

	StringBuffer json = new StringBuffer();
	List<Rights> rightList = new ArrayList<Rights>();
	Role allrole = new Role();

	/**
	 * 构建Json文件
	 * @param list
	 * @param node
	 */

	public void constructorJson(List<Rights> list, Rights treeNode) throws Exception {
		String lan = (String)ServletActionContext.getRequest().getSession().getAttribute("lan");
		if (hasChild(list, treeNode)) {
			json.append("{\"val\":\"");
			json.append(treeNode.getId() + "\"");
			json.append(",\"title\":\"");
				if(lan.equals("en"))
					json.append(treeNode.getEnName() + "\"");
				else
					json.append(treeNode.getName() + "\"");
			json.append(",\"data\":[");
			List<Rights> childList = getChildList(list, treeNode);
			Iterator<Rights> iterator = childList.iterator();
			while (iterator.hasNext()) {
				Rights node = (Rights) iterator.next();
				constructorJson(list, node);
			}
			json.append("]},");
		} else {
			json.append("{\"val\":\"");
			json.append(treeNode.getId() + "\"");
			json.append(",\"title\":\"");
			
			if(lan.equals("en"))
				json.append(treeNode.getEnName() + "\"");
			else
				json.append(treeNode.getName() + "\"");
			if (isHasRights(treeNode, rightList) == false) {

				json.append(",\"checked\":false");
			} else {
				json.append(",\"checked\":true");
			}

			RoleRights rr = findRoleRights(treeNode.getId(), allrole.getId());
			String rstr;
			if (rr != null && rr.getRightsAction() != null) {
				rstr = rr.getRightsAction();
			} else {
				rstr = findRights(treeNode.getId()).getRightsFunction();
			}

			json.append(",\"rightsAction\":\"" + rstr + "\"");
			json.append(",\"rightsFunction\":\"" + (rr != null ? rr.getRights().getRightsFunction() : rstr) + "\"");
			json.append(",\"data\":[]");
			json.append("},");
			

		}

	}

	/**
	* 获得子节点列表信息
	* @param list
	* @param node
	* @return
	*/
	public List<Rights> getChildList(List<Rights> list, Rights node) { //得到子节点列表   
		List<Rights> li = new ArrayList<Rights>();
		Iterator<Rights> it = list.iterator();
		while (it.hasNext()) {
			Rights n = (Rights) it.next();
			if (n.getParentId() != null && n.getParentId().equals(node.getId())) {
				li.add(n);
			}
		}
		return li;
	}

	/**
	 * 判断是否有子节点
	 * @param list
	 * @param node
	 * @return
	 */
	public boolean hasChild(List<Rights> list, Rights node) {
		return getChildList(list, node).size() > 0 ? true : false;
	}

	public String getJson(List<Rights> list, String roleId) throws Exception {
		json = new StringBuffer();
		allrole.setId(roleId);
		rightList = commonDAO.find("select a.rights from RoleRights a where a.role.id='" + roleId + "'");
		List<Rights> nodeList0 = new ArrayList<Rights>();
		// commonDAO.find("from Department where level='0'"); 
		Iterator<Rights> it1 = list.iterator();
		while (it1.hasNext()) {
			Rights node = (Rights) it1.next();
			if (node.getLevel() == 0) {
				nodeList0.add(node);
			}

		}
		Iterator<Rights> it = nodeList0.iterator();
		while (it.hasNext()) {
			Rights node = (Rights) it.next();
			constructorJson(list, node);

		}

		String jsonDate = json.toString();
		return ("[" + jsonDate + "]").replaceAll(",]", "]");

	}
}
