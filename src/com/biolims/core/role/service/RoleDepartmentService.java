/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：Service
 * 类描述：角色部门管理service
 * 创建人：倪毅
 * 创建时间：2011-9
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.core.role.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.core.model.user.Department;
import com.biolims.core.model.user.Role;
import com.biolims.core.model.user.RoleDepartment;
import com.biolims.util.JsonUtils;

@Service
public class RoleDepartmentService {
	@Resource
	private CommonDAO commonDAO;
	@Autowired
	CommonService commonService;

	/**
	 * 
	 * 验证登录
	 * 
	 * 
	 */
	public User loginUser(String id, String password) {
		User user = commonDAO.get(User.class, id);

		return user;

	}

	/**
	 * 
	 * 检索权限树,返回map
	 * 
	 * 
	 */
	public List<Department> findDepartment() throws Exception {
		List<Department> rightList = commonDAO.find("from Department where state ='1'");
		return rightList;

	}

	/**
	 * 
	 * 检索权限树,返回map
	 * 
	 * 
	 */
	public List<RoleDepartment> findRoleDepartment(String roleId) throws Exception {
		List<RoleDepartment> rightList = commonDAO.find("from RoleDepartment  where role.id='" + roleId + "'");
		return rightList;

	}

	public RoleDepartment findRoleDepartment(String rightId, String roleId) throws Exception {
		List<RoleDepartment> rightList = commonDAO.find("from RoleDepartment  where department.id ='" + rightId
				+ "' and role.id='" + roleId + "'");

		if (rightList.size() > 0)
			return rightList.get(0);

		return null;

	}

	/**
	 * 
	 * 检索权限树,返回map
	 * 
	 * 
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void saveRoleDepartment(String jsonString, String roleId) throws Exception {

		/*String strArray[]=departmentStr.split(",");
		
		Role r = new Role();
		r.setId(roleId);
		
		for(int i=0;i<strArray.length;i++){
			Department rt = new Department();
			rt.setId(strArray[i]);
			RoleDepartment rr = new RoleDepartment();
			rr.setRole(r);
			rr.setDepartment(rt);
			commonDAO.saveOrUpdate(rr);			
			
		}*/

		// 将json读入map
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(jsonString, List.class);
		List<RoleDepartment> hasDepartmentList = new ArrayList<RoleDepartment>();
		List<RoleDepartment> oldDepartmentList = new ArrayList<RoleDepartment>();
		for (Map<String, Object> map : list) {
			RoleDepartment em = new RoleDepartment();

			// 将map信息读入实体类
			em = (RoleDepartment) commonDAO.Map2Bean(map, em);
			hasDepartmentList.add(em);

			commonDAO.saveOrUpdate(em);
		}
		oldDepartmentList = findRoleDepartment(roleId);

		for (RoleDepartment rr : oldDepartmentList) {
			if (!hasDepartmentList.contains(rr)) {
				commonDAO.delete(rr);
				//break;
			}

		}

	}

	public boolean isHasDepartment(Department r, List<Department> rightList) {
		if (rightList.contains(r)) {

			return true;
		}
		return false;

	}

	/**
	 * 
	 * 检索权限树,返回List
	 * 
	 * 
	 */
	public List<Department> findUserDepartment(String userId) throws Exception {
		List<Department> rightList = commonDAO
				.find("select distinct b.department from UserRole a,RoleDepartment b where a.role = b.role and a.user.id='"
						+ userId + "'");
		return rightList;

	}

	/**
	 * 
	 * 生成用户权限树,返回List
	 * 
	 * 
	 */
	public List<Department> findUserDepartmentTree(String userId) throws Exception {
		List<Department> allDepartment = findDepartment();
		List<Department> userDepartment = findUserDepartment(userId);
		List<Department> buildDepartment = new ArrayList<Department>();
		for (int i = 0; i < allDepartment.size(); i++) {
			Department rt = allDepartment.get(i);
			for (int j = 0; j < userDepartment.size(); j++) {
				Department rt1 = userDepartment.get(j);
				if (rt.getLevelId().startsWith(rt1.getLevelId() + ".")
						|| rt1.getLevelId().startsWith(rt.getLevelId() + ".")
						|| rt1.getLevelId().equals(rt.getLevelId())) {
					buildDepartment.add(rt);
					break;
				}
			}
		}
		return buildDepartment;
	}

	/**
	 * 
	 * 检索权限动作,返回List
	 * 
	 * 
	 */
	public List<RoleDepartment> findUserDepartmentActionDepartment(String userId) throws Exception {
		List<RoleDepartment> rightList = commonDAO
				.find("select b from UserRole a,RoleDepartment b where a.role = b.role  and a.user.id='" + userId + "'");
		return rightList;

	}

	/**
	 * 
	 * 检索权限部门范围,返回List
	 * 
	 * 
	 */
	public List<Department> findUserDepartmentDepartment(User user) throws Exception {
		String userId = user.getId();
		Object[] a = { userId };
		List<Department> departmentList = commonDAO
				.find(
						"select distinct c.department from UserRole a,RoleDepartment b,RoleDepartmentDepartment c where a.role = b.role and b.id = c.roleDepartment and a.user.id=?",
						a);
		Department userDeparment = user.getDepartment();
		//加入自己所属部门
		if (!departmentList.contains(userDeparment))
			departmentList.add(userDeparment);
		return departmentList;

	}

	/**
	 * 
	 * 检索map
	 * 
	 * 
	 */
	public Map<String, Object> findMap(Map<String, Object> aMap) throws Exception {
		for (String propertie : aMap.keySet()) {
			Object o = aMap.get(propertie);
		}
		return aMap;
	}

	StringBuffer json = new StringBuffer();
	List<Department> rightList = new ArrayList<Department>();
	Role allrole = new Role();

	/**
	 * 构建Json文件
	 * @param list
	 * @param node
	 */

	public void constructorJson(List<Department> list, Department treeNode) throws Exception {

		if (hasChild(list, treeNode)) {
			json.append("{\"id\":'");
			json.append(treeNode.getId() + "'");
			json.append(",\"text\":'");
			json.append(treeNode.getName() + "'");

			json.append(",\"leaf\":false");
			if (isHasDepartment(treeNode, rightList) == false) {

				json.append(",\"checked\":false");
			} else {
				json.append(",\"checked\":true");
			}

			RoleDepartment rr = findRoleDepartment(treeNode.getId(), allrole.getId());

			json.append(",\"roleDepartmentId\":'" + (rr != null ? rr.getId() : "") + "'");

			json.append(",\"children\":[");
			List<Department> childList = getChildList(list, treeNode);
			Iterator<Department> iterator = childList.iterator();
			while (iterator.hasNext()) {
				Department node = (Department) iterator.next();
				constructorJson(list, node);
			}
			json.append("]},");
		} else {
			json.append("{\"id\":'");
			json.append(treeNode.getId() + "'");
			json.append(",\"text\":'");
			json.append(treeNode.getName() + "'");
			json.append(",\"leaf\":true");
			if (isHasDepartment(treeNode, rightList) == false) {

				json.append(",\"checked\":false");
			} else {
				json.append(",\"checked\":true");
			}
			//String rStr = findRoleDepartmentStr(treeNode.getId(),allrole.getId());

			RoleDepartment rr = findRoleDepartment(treeNode.getId(), allrole.getId());

			json.append(",\"roleDepartmentId\":'" + (rr != null ? rr.getId() : "") + "'");

			//json.append(treeNode.getLeaf()+"");   

			json.append("},");

		}

	}

	/**
	* 获得子节点列表信息
	* @param list
	* @param node
	* @return
	*/
	public List<Department> getChildList(List<Department> list, Department node) { //得到子节点列表   
		List<Department> li = new ArrayList<Department>();
		Iterator<Department> it = list.iterator();
		while (it.hasNext()) {
			Department n = (Department) it.next();
			if (n.getUpDepartment() != null && n.getUpDepartment().equals(node)) {
				li.add(n);
			}
		}
		return li;
	}

	/**
	 * 判断是否有子节点
	 * @param list
	 * @param node
	 * @return
	 */
	public boolean hasChild(List<Department> list, Department node) {
		return getChildList(list, node).size() > 0 ? true : false;
	}

	public String getJson(List<Department> list, String roleId) throws Exception {
		json = new StringBuffer();
		allrole.setId(roleId);
		rightList = commonDAO.find("select a.department from RoleDepartment a where a.role.id='" + roleId + "'");
		List<Department> nodeList0 = new ArrayList<Department>();
		// commonDAO.find("from Department where level='0'"); 
		Iterator<Department> it1 = list.iterator();
		while (it1.hasNext()) {
			Department node = (Department) it1.next();
			if (node.getLevel() == 0) {
				nodeList0.add(node);
			}

		}
		Iterator<Department> it = nodeList0.iterator();
		while (it.hasNext()) {
			Department node = (Department) it.next();
			constructorJson(list, node);

		}

		String jsonDate = json.toString();
		return ("[" + jsonDate + "]").replaceAll(",]", "]");

	}
}
