package com.biolims.core.role.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.Rights;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.core.constants.SystemConstants;
import com.biolims.core.model.user.Role;
import com.biolims.core.model.user.RoleRights;
import com.biolims.core.model.user.UserRole;
import com.biolims.core.role.dao.RoleDAO;
import com.biolims.core.user.dao.UserDAO;
import com.biolims.dashboard.model.DashboardModel;
import com.biolims.dashboard.model.RoleDashboardModel;
import com.biolims.dashboard.service.DashboardService;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.model.LogInfo;
import com.biolims.util.JsonUtils;
import com.microsoft.schemas.office.x2006.encryption.CTKeyEncryptor.Uri;

@Service
public class RoleService {
	@Resource
	private CommonDAO commonDAO;
	@Autowired
	CommonService commonService;
	@Autowired
	RightsService rightsService;
	@Autowired
	DashboardService dashboardService;
	@Resource
	private UserDAO userDAO;
	@Resource
	private RoleDAO roleDAO;

	/**
	 * 
	 * 检索实验,采用map方式传递检索参数
	 * 
	 * 
	 * @return 参数值MAP，如果传递的为null返回空字符串
	 */

	public Map<String, Object> findRoleList(int start, int length,
			String query, String col, String sort) throws Exception {
		return roleDAO.findRoleList(start, length, query, col, sort);

	}

	/**
	 * 保存
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(Role an) throws Exception {
		commonDAO.saveOrUpdate(an);

	}

	public Role getRole(String id) throws Exception {
		Role animal = commonDAO.get(Role.class, id);
		return animal;
	}

	public String hasId(String id) throws Exception {
		return commonService.hasId(id, Role.class);
	}

	public Map<String, Object> findRoleUserList(String roleId, Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		List<UserRole> urList = roleDAO.getUserRole(roleId);
		List<User> uList = new ArrayList<User>();
		for (UserRole ur : urList) {
			User u = ur.getUser();
			uList.add(u);
		}
		map.put("list", uList);
		map.put("recordsTotal", uList.size());
		map.put("recordsFiltered", uList.size());
		return map;
	}

	/**
	 * 
	 * @Title: saveRoleUser
	 * @Description: 保存人员
	 * @author : shengwei.wang
	 * @date 2018年4月9日下午6:34:55
	 * @param roleId
	 * @param userId
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveRoleUser(String roleId, String[] userId) throws Exception {
		if (roleId != null && !"".equals(roleId)) {
			UserRole ur = null;
			for (String user : userId) {
				ur = roleDAO.getUserRole(roleId, user);
				if (ur != null) {

				} else {
					ur = new UserRole();
					Role r = new Role();
					User u = new User();
					r.setId(roleId);
					u.setId(user);
					ur.setUser(u);
					ur.setRole(r);
					commonDAO.saveOrUpdate(ur);
				}
			}
		}
	}

	/**
	 * 
	 * 删除明细,由ext ajax调用
	 * 
	 * @param roleId
	 * 
	 * 
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delRoleUser(String[] ids, String roleId) {
		List<UserRole> list = roleDAO.getUserRole(roleId);
		for (String id : ids) {
			for (UserRole ur : list) {
				if (ur.getUser().getId().equals(id)) {
					commonDAO.delete(ur);
				}
				
			}

			if (ids.length>0) {
				LogInfo li = new LogInfo();
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setLogDate(new Date());
				li.setUserId(u.getId());
				li.setClassName("Role");
				li.setFileId(id);
				li.setModifyContent("角色:"+"用户名:"+id+"的数据已删除");
				commonDAO.saveOrUpdate(li);
			}
		}
	}

	// 查询dicType表内模块名称
	public Map<String, Object> finDicTypeByType(String type) {
		Map<String, Object> result = this.userDAO.finDicTypeByType(type);
		return result;
	}

	public String getRights(String roleId) throws Exception {
		List<Rights> rList = rightsService.findRights();
		String tree = rightsService.getJson(rList, roleId);
		return tree;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(String main, String right, String dashBoard,
			String userRole, String id,String changeLog) throws Exception {
		if (main != null && !"".equals(main)) {
			String mainJson = "[" + main + "]";
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					mainJson, List.class);
			Role r = new Role();
			r = (Role) commonDAO.Map2Bean(list.get(0), r);
			roleDAO.saveOrUpdate(r);
			if (right != null && !"".equals(right)) {
				saveRight(right, r);
			}
			if (dashBoard != null && !"".equals(dashBoard)) {
				saveDashBoard(dashBoard, r);
			}
		}
		
		if (changeLog != null && !"".equals(changeLog)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setFileId(id);
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setLogDate(new Date());
			li.setUserId(u.getId());
			li.setClassName("Role");
			li.setModifyContent(changeLog);
			if(li.getId()!=null) {
				li.setState("3");
				li.setStateName("数据修改");
			}else {
				li.setState("1");
				li.setStateName("数据新增");
			}
			
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * @throws Exception
	 * 
	 * @Title: saveRight
	 * @Description: 保存权限
	 * @author : shengwei.wang
	 * @date 2018年4月9日下午2:31:04
	 * @param right
	 * @param r
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveRight(String right, Role r) throws Exception {
		List<RoleRights> rrList = rightsService.findRoleRights(r.getId());
		commonDAO.deleteAll(rrList);
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(right,
				List.class);
		for (Map<String, Object> map : list) {
			RoleRights rr = new RoleRights();
			Rights ri = new Rights();
			String id = (String) map.get("val");
			ri.setId(id);
			String rightsAction = (String) map.get("rightsAction");
			rr.setRights(ri);
			rr.setRightsAction(rightsAction);
			rr.setRole(r);
			commonDAO.saveOrUpdate(rr);
		}

	}

	/**
	 * @throws Exception
	 * 
	 * @Title: saveDashBoard
	 * @Description: 保存工作台
	 * @author : shengwei.wang
	 * @date 2018年4月9日下午2:31:18
	 * @param dashBoard
	 * @param r
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveDashBoard(String dashboard, Role r) throws Exception {
		List<RoleDashboardModel> rrList = dashboardService
				.getRoleDashboardModel(r.getId());
		commonDAO.deleteAll(rrList);
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(dashboard,
				List.class);
		for (Map<String, Object> map : list) {
			RoleDashboardModel rr = new RoleDashboardModel();
			DashboardModel ri = new DashboardModel();
			String id = (String) map.get("val");
			ri.setId(id);
			rr.setDbm(ri);
			rr.setRole(r);
			commonDAO.saveOrUpdate(rr);
		}
	}

	/**
	 * @throws Exception
	 * 
	 * @Title: saveUserRole
	 * @Description: 保存人员
	 * @author : shengwei.wang
	 * @date 2018年4月9日下午2:31:39
	 * @param userRole
	 * @param r
	 *            void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveUserRole(String userRole, Role r) throws Exception {
		List<UserRole> rrList = roleDAO.getUserRole(r.getId());
		commonDAO.deleteAll(rrList);
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(userRole,
				List.class);
		for (Map<String, Object> map : list) {
			UserRole rr = new UserRole();
			User ri = new User();
			String id = (String) map.get("id");
			ri.setId(id);
			rr.setUser(ri);
			rr.setRole(r);
			commonDAO.saveOrUpdate(rr);
		}
	}
}
