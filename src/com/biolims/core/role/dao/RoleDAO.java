package com.biolims.core.role.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.common.dao.CommonDAO;
import com.biolims.core.model.user.Role;
import com.biolims.core.model.user.UserRole;

@Repository
@SuppressWarnings("unchecked")
public class RoleDAO extends CommonDAO {

	public Map<String, Object> findRoleList(int start, int length,
			String query, String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from Role where 1=1 ";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from Role where 1=1 ";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<Role> list = new ArrayList<Role>();
			list = this.getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	public List<UserRole> getUserRole(String roleId) {
		String hql = " from UserRole where 1=1 and role.id='" + roleId + "'";
		String key = "";
		List<UserRole> list = this.getSession().createQuery(hql + key).list();
		return list;
	}

	public UserRole getUserRole(String roleId, String user) {
		String hql = " from UserRole where 1=1 and role.id='" + roleId
				+ "' and user.id='" + user + "'";
		String key = "";
		UserRole ur = (UserRole) this.getSession().createQuery(hql + key)
				.uniqueResult();
		return ur;
	}

}
