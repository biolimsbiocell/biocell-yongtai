/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：Action
 * 类描述：角色管理
 * 创建人：倪毅
 * 创建时间：2011-11
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.core.role.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.core.constants.SystemConstants;
import com.biolims.core.model.user.Role;
import com.biolims.core.role.service.RoleService;
import com.biolims.dic.model.DicType;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/core/role")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public class RoleAction extends BaseActionSupport {

	private static final long serialVersionUID = -164661537469123056L;

	@Autowired
	private RoleService roleService;

	@Autowired
	private CommonService commonService;
	// 用于页面上显示模块名称
	private String title = "角色";

	// 该action权限id
	private String rightsId = "110302";

	private Role role = new Role();

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public CommonService getCommonService() {
		return commonService;
	}

	public void setCommonService(CommonService commonService) {
		this.commonService = commonService;
	}

	public RoleService getRoleService() {
		return roleService;
	}

	public void setRoleService(RoleService roleService) {
		this.roleService = roleService;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	/**
	 * 
	 * @Title: showRoleList
	 * @Description: 角色列表
	 * @author : shengwei.wang
	 * @date 2018年4月8日上午11:18:59
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showRoleList")
	public String showRoleList() throws Exception {
		rightsId = "110302";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/core/role/showRoleList.jsp");
	}

	@Action(value = "showRoleListJson")
	public void showRoleListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = roleService.findRoleList(start,
					length, query, col, sort);
			List<Role> list = (List<Role>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("note", "");
			map.put("state", "");
			map.put("modelId", "");
			map.put("modelName", "");
			map.put("abnormalId", "");
			map.put("abnormalName", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @Title: toEditRole
	 * @Description: 新建、编辑
	 * @author : shengwei.wang
	 * @date 2018年4月9日下午6:36:32
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "toEditRole")
	public String toEditRole() throws Exception {
		String id = getParameterFromRequest("id");
		rightsId = "110301";
		if (id != null && !id.equals("")) {
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			role = roleService.getRole(id);
			putObjToContext("roleId", id);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);

		} else {
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		return dispatcher("/WEB-INF/page/core/role/editRole.jsp");
	}

	@Action(value = "getRights")
	public void getRights() {
		String roleId = getParameterFromRequest("roleId");
		try {
			String json = roleService.getRights(roleId);
			HttpUtils.write(json);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 访问编辑页面
	 */

	@Action(value = "toView")
	public String toView() throws Exception {
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_VIEW);
			role = roleService.getRole(id);

		}
		return dispatcher("/WEB-INF/page/core/role/editRole.jsp");
	}

	/**
	 * 
	 * @Title: save
	 * @Description: 保存
	 * @author : shengwei.wang
	 * @date 2018年4月9日下午6:36:53
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "save")
	public void save() throws Exception {
		String main = getParameterFromRequest("main");
		String right = getParameterFromRequest("rights");
		String dashBoard = getParameterFromRequest("dashboard");
		String userRole = getParameterFromRequest("userRole");
		String id = getParameterFromRequest("id");
		String changeLog = getParameterFromRequest("changeLog");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			roleService.save(main, right, dashBoard, userRole, id,changeLog);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}

		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 查询是否编号重复
	 */
	@Action(value = "hasId")
	public String hasId() throws Exception {
		String id = getParameterFromRequest("id");
		return this.renderText(roleService.hasId(id));

	}

	// @Action(value = "showRoleUserList")
	// public String showRoleUserList() throws Exception {
	//
	// String roleId = getParameterFromRequest("roleId");
	//
	// LinkedHashMap<String, String[]> map = new LinkedHashMap<String,
	// String[]>();
	// map.put("id", new String[] { "", "string", "", "ID", "150", "true",
	// "true", "", "", "", "", "" });
	// map.put("user-id", new String[] { "", "string", "", "用户账号", "150",
	// "true", "", "", "", "", "", "" });
	// map.put("user-name", new String[] { "", "string", "", "姓名", "150",
	// "true", "", "", "", "", "", "" });
	// map.put("user-department-name", new String[] { "", "string", "", "部门",
	// "150", "true", "", "", "", "", "", "" });
	// map.put("user-dicJob-name",
	// new String[] { "common", "string", "", "岗位", "150", "true", "", "", "",
	// "", "", "" });
	// String type = generalexttype(map);
	// String col = generalextcol(map);
	// putObjToContext("roleId", roleId);
	// putObjToContext("type", type);
	// putObjToContext("col", col);
	// putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// putObjToContext("path",
	// ServletActionContext.getRequest().getContextPath()
	// + "/core/role/showRoleUserListJson.action?roleId=" + roleId);
	// return dispatcher("/WEB-INF/page/core/role/showRoleUserList.jsp");
	// }

	@Action(value = "showRoleUserListJson")
	public void showRoleUserListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String roleId = getParameterFromRequest("roleId");
		Map<String, Object> result = roleService.findRoleUserList(roleId,
				start, length, query, col, sort);
		List<User> list = (List<User>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("department-name", "");
		map.put("dicJob-name", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * 删除
	 */
	@Action(value = "delRoleUser")
	public void delRoleUser() throws Exception {
		String[] ids = getRequest().getParameterValues("ids[]");
		String roleId = getParameterFromRequest("id");
		Map<String,Object> map=new HashMap<String, Object>();
		try {
			
			roleService.delRoleUser(ids, roleId);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: saveRoleUser
	 * @Description: 保存人员
	 * @author : shengwei.wang
	 * @date 2018年4月9日下午6:37:10
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "saveRoleUser")
	public void saveRoleUser() throws Exception {
		String roleId = getParameterFromRequest("roleId");
		String[] userId = getRequest().getParameterValues("userId[]");
		Map<String,Object> map=new HashMap<String, Object>();
		try {
			roleService.saveRoleUser(roleId, userId);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 选择待办数量模块
	@Action(value = "showBacklogList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showBacklogList() throws Exception {
		String type = getParameterFromRequest("type");
		putObjToContext("type", type);
		return dispatcher("/WEB-INF/page/core/role/showBacklogList.jsp");
	}

	@Action(value = "showBacklogListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showBacklogListJson() throws Exception {
		String type = getParameterFromRequest("type");
		Map<String, Object> result = roleService.finDicTypeByType(type);
		Long count = (Long) result.get("total");
		List<DicType> list = (List<DicType>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	// 异常信息模块
	@Action(value = "showAbnormalList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showAbnormalList() throws Exception {
		String type = getParameterFromRequest("type");
		putObjToContext("type", type);
		return dispatcher("/WEB-INF/page/core/role/showAbnormalList.jsp");
	}

	@Action(value = "showAbnormalListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showAbnormalListJson() throws Exception {
		String type = getParameterFromRequest("type");
		Map<String, Object> result = roleService.finDicTypeByType(type);
		Long count = (Long) result.get("total");
		List<DicType> list = (List<DicType>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}
}
