/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：ACTION
 * 类描述：权限管理
 * 创建人：倪毅
 * 创建时间：2011-10
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.core.role.action;

import java.util.List;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.model.user.Rights;
import com.biolims.core.role.service.RightsService;
import com.biolims.util.SendData;

/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：
 * 类描述：权限管理
 * 创建人：倪毅
 * 创建时间：2012-1-10
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
@Namespace("/rights")
@Controller
@Scope("prototype")
@ParentPackage("default")
public class RightsAction extends BaseActionSupport {
	private static final long serialVersionUID = -164661537469123056L;

	@Autowired
	private RightsService rightsService;

	private String rightsId = "1103";

	public RightsService getRightsService() {
		return rightsService;
	}

	public void setRightsService(RightsService rightsService) {
		this.rightsService = rightsService;
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	/**
	 *  访问tree导航区
	 */
	@Action(value = "showRights")
	public String toTree() throws Exception {

		return dispatcher("/WEB-INF/page/core/role/showRights.jsp");
	}

	@Action(value = "showRightsJson")
	public void rightsShowJsonByQuery() throws Exception {
		String roleId = getParameterFromRequest("roleId");
		List<Rights> aList = rightsService.findRights();

		String a = rightsService.getJson(aList, roleId);
		new SendData().sendDataJson(a, ServletActionContext.getResponse());
	}

	@Action(value = "saveRoleRights")
	public void saveRoleRights() throws Exception {

		String data = getParameterFromRequest("data");
		String roleId = getParameterFromRequest("roleId");
		rightsService.saveRoleRights(data, roleId);
	}

	@Action(value = "saveRoleActionRights")
	public void saveRoleActionRights() throws Exception {
		String roleRightsId = getParameterFromRequest("roleRightsId");
		String rightsStr = getParameterFromRequest("rightsStr");
		rightsService.saveRoleActionRights(roleRightsId, rightsStr);

	}
}
