package com.biolims.core.model.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Entity
@Table(name = "T_DIC_JOB")
public class DicJob implements Serializable {

	private static final long serialVersionUID = -7905670753586542029L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@Column(name = "NAME", length = 100)
	private String name;//名称

	@Column(name = "ORDER_NUMBER")
	private Integer orderNumber; //排序号

	@Column(name = "STATE", length = 32)
	private String state; //状态 

	@Column(name = "NOTE", length = 200)
	private String note; //备注

	@Column(name = "IS_LEADER", length = 32)
	private String isLeader; //是否该部门领导 0 否 1是

	@Column(name = "IS_PURCHASE", length = 32)
	private String isPurchase; //是否采购0 否 1是

	@Column(name = "IS_SALE_MANAGE", length = 32)
	private String isSaleManage; //是否采购0 否 1是

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "DIC_DEPARTMENT_ID")
	private Department department; //部门

	@Column(name = "FEE_SCALE")
	private float feeScale; //费率 

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public float getFeeScale() {
		return feeScale;
	}

	public void setFeeScale(float feeScale) {
		this.feeScale = feeScale;
	}

	public Integer getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getIsLeader() {
		return isLeader;
	}

	public void setIsLeader(String isLeader) {
		this.isLeader = isLeader;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public String getIsPurchase() {
		return isPurchase;
	}

	public void setIsPurchase(String isPurchase) {
		this.isPurchase = isPurchase;
	}

	public String getIsSaleManage() {
		return isSaleManage;
	}

	public void setIsSaleManage(String isSaleManage) {
		this.isSaleManage = isSaleManage;
	}

}
