package com.biolims.core.model.user;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicUnit;

@Entity
@Table(name = "T_USER_CERT")
public class UserCert extends EntityDao<UserCert> {

	private static final long serialVersionUID = -7915457065164332028L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;//id
	@Column(name = "NAME", length = 100)
	private String name;//名称
	@Column(name = "NO", length = 50)
	private String no;//编号
	@Column(name = "SEND_ORG", length = 100)
	private String sendOrg;//发证机关
	private Date sendDate;//发证日期
	private Double expireTime;//期限
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "DIC_UNIT_ID")
	private DicUnit dicUnit;//时间单位
	private Date overDate;//到期日期
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "USER_ID")
	private User user;//关联用户
	@Column(name = "REMARK", length = 300)
	private String remark;

	public User getUser() {
		return user;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public String getSendOrg() {
		return sendOrg;
	}

	public void setSendOrg(String sendOrg) {
		this.sendOrg = sendOrg;
	}

	public Date getSendDate() {
		return sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

	public Double getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(Double expireTime) {
		this.expireTime = expireTime;
	}

	public DicUnit getDicUnit() {
		return dicUnit;
	}

	public void setDicUnit(DicUnit dicUnit) {
		this.dicUnit = dicUnit;
	}

	public Date getOverDate() {
		return overDate;
	}

	public void setOverDate(Date overDate) {
		this.overDate = overDate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
