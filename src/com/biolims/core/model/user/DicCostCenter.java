package com.biolims.core.model.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dic.model.DicType;

@Entity
@Table(name = "T_DIC_COST_CENTER")
public class DicCostCenter implements Serializable {

	private static final long serialVersionUID = -7915457065164332028L;

	@Id
	@Column(name = "ID", length = 32)
	private String id;
	//名称
	@Column(name = "NAME", length = 110)
	private String name;
	//排序号
	private Integer orderNumber;
	//字典类型
	@Column(name = "LEVEL_ID", length = 32)
	private String levelId;
	@Column(name = "UP_ID", length = 32)
	private String upId;//上级编码
	//状态
	@Column(name = "STATE", length = 32)
	private String state;
	//树级别
	@Column(name = "LEVEL_NUMBER", length = 3)
	private Integer level;
	//是否叶子节点
	@Column(name = "LEAF", length = 32)
	private String leaf;
	//说明
	@Column(name = "NOTE", length = 200)
	private String note;
	//字典类型
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	private DicType type;
	//上级财务科目
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private DicCostCenter upCostCenter;
	//成本中心所属部门
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	private Department department;

	public DicType getType() {
		return type;
	}

	public void setType(DicType type) {
		this.type = type;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public int getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getUpId() {
		return upId;
	}

	public void setUpId(String upId) {
		this.upId = upId;
	}

	public String getLeaf() {
		return leaf;
	}

	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}

	public void setLeaf(String leaf) {
		this.leaf = leaf;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public DicCostCenter getUpCostCenter() {
		return upCostCenter;
	}

	public void setUpCostCenter(DicCostCenter upCostCenter) {
		this.upCostCenter = upCostCenter;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getLevelId() {
		return levelId;
	}

	public void setLevelId(String levelId) {
		this.levelId = levelId;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

}
