package com.biolims.core.model.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;

@Entity
@Table(name = "T_DEPARTMENT")
public class Department extends EntityDao<Department> {

	private static final long serialVersionUID = -7915457065164332028L;
	@Id
	@Column(name = "ID", length = 32)
	private String id;//部门ID

	@Column(name = "NAME", length = 200)
	private String name;//部门名称

	@Column(name = "BRIEF_NAME", length = 40)
	private String briefName;//部门名称简称

	@Column(name = "LEVEL_ID", length = 100)
	private String levelId;//层级ID 唯一

	@Column(name = "LEVEL_NUMBER", length = 3)
	private int level;//级别

	@Column(name = "ORDER_NUMBER", length = 3)
	private int orderNumber;//同一级别下的顺序

	@Column(name = "NOTE", length = 200)
	private String note;//备注

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "DEPARTMENT_UP_ID")
	private Department upDepartment;//上级部门ID

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "DIC_DEP_TYPE_ID")
	private DicType type;//部门类型

	@Column(name = "STATE", length = 2)
	private String state;//部门状态

	@Column(name = "ADDRESS", length = 200)
	private String address;//单位地址

	@Transient
	private String leaf;
	@Column(name = "PARENT", length = 255)
	private String parent;
	
	/** 范围Id */
	private String scopeId;
	/** 范围 */
	private String scopeName;
	
	

	public String getScopeId() {
		return scopeId;
	}

	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	public String getScopeName() {
		return scopeName;
	}

	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public Department getUpDepartment() {
		return upDepartment;
	}

	public void setUpDepartment(Department upDepartment) {
		this.upDepartment = upDepartment;
	}

	public String getLeaf() {
		return leaf;
	}

	public void setLeaf(String leaf) {
		this.leaf = leaf;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBriefName() {
		return this.briefName;
	}

	public void setBriefName(String briefName) {
		this.briefName = briefName;
	}

	public String getLevelId() {
		return levelId;
	}

	public void setLevelId(String levelId) {
		this.levelId = levelId;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}

	public DicType getType() {
		return type;
	}

	public void setType(DicType type) {
		this.type = type;
	}

}