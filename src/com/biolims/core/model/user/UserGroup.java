package com.biolims.core.model.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.biolims.dao.EntityDao;

/**
 * 用户组
 * @author WR
 *
 */
@Entity
@Table(name = "T_USER_GROUP")
@SuppressWarnings("serial")
public class UserGroup extends EntityDao<UserGroup> {

	//主键
	@Id
	@Column(name = "ID", length = 32)
	private String id;

	//用户组名称
	@Column(name = "NAME", length = 200)
	private String name;

	//排序号
	@Column(name = "ORDER_NUMBER", length = 200)
	private Integer orderNumber = 0;

	//状态（0 无效 1生效）
	@Column(name = "STATE", length = 1)
	private String state;
	@Column(name = "FUNCTION_CLASS", length = 100)
	private String functionClass;//函数名称

	//说明
	@Column(name = "NOTE", length = 600)
	private String note;
	//类型 0 普通 1函数

	@Column(name = "TYPE", length = 32)
	private String type;

	//说明
	@Column(name = "PRODUCT_ID", length = 600)
	private String productId;

	@Column(name = "PRODUCT_NAME", length = 600)
	private String productName;
	private String isCheck;
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFunctionClass() {
		return functionClass;
	}

	public void setFunctionClass(String functionClass) {
		this.functionClass = functionClass;
	}

	public UserGroup() {
	}

	public UserGroup(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * @return the isCheck
	 */
	public String getIsCheck() {
		return isCheck;
	}

	/**
	 * @param isCheck the isCheck to set
	 */
	public void setIsCheck(String isCheck) {
		this.isCheck = isCheck;
	}
	
}
