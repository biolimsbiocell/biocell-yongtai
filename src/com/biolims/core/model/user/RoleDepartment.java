package com.biolims.core.model.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import com.biolims.dao.EntityDao;

//角色对应权限下管理范围
@Entity
@Table(name = "T_ROLE_DEPARTMENT")
public class RoleDepartment extends EntityDao<RoleDepartment> {

	private static final long serialVersionUID = -7915457065164332028L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@ManyToOne()
	@ForeignKey(name = "none")
	@JoinColumn(name = "ROLE_ID")
	private Role role;//关联角色

	@ManyToOne()
	@ForeignKey(name = "none")
	@JoinColumn(name = "DEPARTMENT_ID")
	private Department department;//关联部门

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
