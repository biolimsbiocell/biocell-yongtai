package com.biolims.core.model.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.biolims.common.model.user.Rights;
import com.biolims.dao.EntityDao;

@Entity
@Table(name = "T_ACTION_RIGHTS")
public class ActionRights extends EntityDao<ActionRights> {

	private static final long serialVersionUID = -7915457065164332028L;

	@Id
	@Column(name = "ID", length = 32)
	private String id;

	@Column(name = "NAME", length = 80)
	private String name; //名称

	@Column(name = "NOTE", length = 100)
	private String note; //说明

	@Column(name = "ACTION_METHOD", length = 100)
	private String actionMethod; //操作类型字符串  1新建 2修改3提交审批4删除5检索6高级检索

	@Column(name = "STATE", length = 2)
	private String state;//状态 0失效 1生效

	@Column(name = "TYPE", length = 2)
	private String type;//1 功能列表下 2 检索列表下 0 公用普通功能

	@Column(name = "PAGE_TYPE", length = 10)
	private String pageType;//1 List 2 View 3 Modify 4 Add

	@Column(name = "FUNCTION_NAME", length = 80)
	private String functionName;//通用函数或连接名称

	@ManyToOne()
	@ForeignKey(name = "none")
	@JoinColumn(name = "RIGHTS_ID")
	private Rights rights;//所属功能

	public String getPageType() {
		return pageType;
	}

	public void setPageType(String pageType) {
		this.pageType = pageType;
	}

	public String getFunctionName() {
		return functionName;
	}

	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}

	//排序号
	private int orderNumber;

	public String getType() {
		return type;
	}

	public Rights getRights() {
		return rights;
	}

	public void setRights(Rights rights) {
		this.rights = rights;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getActionMethod() {
		return actionMethod;
	}

	public void setActionMethod(String actionMethod) {
		this.actionMethod = actionMethod;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}

}
