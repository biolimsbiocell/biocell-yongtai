package com.biolims.core.model.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.Rights;
import com.biolims.dao.EntityDao;

//角色对应权限
@Entity
@Table(name = "T_ROLE_RIGHTS")
public class RoleRights extends EntityDao<RoleRights> {

	private static final long serialVersionUID = -7915457065164332028L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id; //ID

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "ROLE_ID")
	private Role role; //关联角色

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "RIGHTS_ID")
	private Rights rights;//关联功能

	//操作类型字符串  1新建 2修改3提交审批4删除5检索6其他
	@Column(name = "RIGHTS_ACTION")
	private String rightsAction;//动作权限字符串

	public Rights getRights() {
		return rights;
	}

	public void setRights(Rights rights) {
		this.rights = rights;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getRightsAction() {
		return rightsAction;
	}

	public void setRightsAction(String rightsAction) {
		this.rightsAction = rightsAction;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
