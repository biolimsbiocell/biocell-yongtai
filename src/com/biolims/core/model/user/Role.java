package com.biolims.core.model.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.biolims.dao.EntityDao;

@Entity
@Table(name = "T_ROLE")
public class Role extends EntityDao<Role> {

	private static final long serialVersionUID = -7915457065164332028L;

	@Id
	@Column(name = "ID", length = 32)
	private String id;

	@Column(name = "NAME", length = 80)
	private String name; //名称

	@Column(name = "NOTE", length = 600)
	private String note; //说明

	@Column(name = "TYPE")
	private String type;//类型

	@Column(name = "STATE", length = 32)
	private String state; //状态 0失效 1生效

	@Column(name = "PORTLET_CONTENT", length = 100)
	private String portletContent;//所含门户子项

	@Column(name = "ROLE_CLASS", length = 100)
	private String roleClass;//函数名称

	@Column(name = "DEPARTMENT_TYPE", length = 32)
	private String departmentType; //0 无部门  1 全部部门 2 管理部门  3本部门

	@Column(name = "ORDER_NUMBER")
	private int orderNumber; //排序号
	private String isCheck;
	
	//选择待办数量权限  模块ID
	private String modelId;
	//选择待办数量权限  模块名称
	private String modelName;
	
	
	// 选择异常信息模块ID
	private String abnormalId;
	// 选择异常信息权限 模块名称
	private String abnormalName;
	
	

	public String getAbnormalId() {
		return abnormalId;
	}

	public void setAbnormalId(String abnormalId) {
		this.abnormalId = abnormalId;
	}

	public String getAbnormalName() {
		return abnormalName;
	}

	public void setAbnormalName(String abnormalName) {
		this.abnormalName = abnormalName;
	}

	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getRoleClass() {
		return roleClass;
	}

	public void setRoleClass(String roleClass) {
		this.roleClass = roleClass;
	}

	public String getDepartmentType() {
		return departmentType;
	}

	public void setDepartmentType(String departmentType) {
		this.departmentType = departmentType;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getPortletContent() {
		return portletContent;
	}

	public void setPortletContent(String portletContent) {
		this.portletContent = portletContent;
	}

	/**
	 * @return the isCheck
	 */
	public String getIsCheck() {
		return isCheck;
	}

	/**
	 * @param isCheck the isCheck to set
	 */
	public void setIsCheck(String isCheck) {
		this.isCheck = isCheck;
	}

}
