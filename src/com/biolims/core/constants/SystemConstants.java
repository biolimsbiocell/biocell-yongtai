package com.biolims.core.constants;

public class SystemConstants extends com.biolims.common.constants.SystemConstants {

	/**
	 * 男性
	 */
	public static final String DIC_USER_MALE = "1";
	/**
	 * 女性
	 */
	public static final String DIC_USER_FEMALE = "0";
	/**
	 * 用户状态:生效
	 */
	public static final String DIC_USER_IN_USE = "1";
	/**
	 * 用户状态:失效
	 */
	public static final String DIC_USER_NOT_USE = "0";
	/**
	 * 状态:生效
	 */
	public static final String DIC_TYPE_IN_USE = "1";
	/**
	 * 状态:失效
	 */
	public static final String DIC_TYPE_NOT_USE = "0";
	/**
	 * 字典表-教育程度
	 */
	public static final String DIC_TYPE_EDU_LEVEL = "edulevel";
}
