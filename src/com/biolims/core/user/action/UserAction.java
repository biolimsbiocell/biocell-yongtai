/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：UserAction
 * 类描述：用户管理
 * 创建人：倪毅
 * 创建时间：2011-8
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.core.user.action;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.model.user.User;
import com.biolims.core.constants.SystemConstants;
import com.biolims.core.model.user.DicCostCenter;
import com.biolims.core.model.user.UserCert;
import com.biolims.core.model.user.UserGroupUser;
import com.biolims.core.model.user.UserJobUser;
import com.biolims.core.model.user.UserVaccine;
import com.biolims.core.user.service.UserService;
import com.biolims.dic.model.DicUnit;
import com.biolims.dic.service.DicUnitService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/core/user")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public class UserAction extends BaseActionSupport {

	private static final long serialVersionUID = -164661537469123056L;

	@Autowired
	private UserService userService;
	@Autowired
	private DicUnitService dicUnitService;

	// 用于页面上显示模块名称
	private String title = "用户管理";

	// 该action权限id
	private String rightsId = "110102";

	private User user = new User();
	private List<DicUnit> listDicUnit;// 单位类型
	
	/**
		 *	展示用户的健康状况
	     * @Title: showUserHealthTableListJson  
	     * @Description: TODO  
	     * @param     
	     * @return void  
		 * @author 孙灵达  
	     * @date 2018年8月1日
	     * @throws
	 */
	@Action(value="showUserHealthTableListJson")
	public void showUserHealthTableListJson() {
		String uid = getParameterFromRequest("id");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = userService.showUserHealthTableListJson(uid, start, length,
					query, col, sort);
			List<UserJobUser> list = (List<UserJobUser>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("testedDate", "yyyy-MM-dd");
			map.put("expireDate", "yyyy-MM-dd");
			map.put("checkResult", "");
			map.put("note", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @throws Exception 
		 *	保存用户健康状况的小保存
	     * @Title: saveUserHealthItemTable  
	     * @Description: TODO  
	     * @param     
	     * @return void  
		 * @author 孙灵达  
	     * @date 2018年8月1日
	     * @throws
	 */
	@Action(value="saveUserHealthItemTable")
	public void saveUserHealthItemTable() throws Exception {
		String id = getParameterFromRequest("id");
		String changeLog = getParameterFromRequest("changeLog");
		String itemJson=getParameterFromRequest("dataJson");
		Map<String, Object> map=new HashMap<String, Object>();
		try {
			userService.saveUserHealthItemTable(id,changeLog,itemJson);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	/**
		 *	删除选中的用户健康状况明细
	     * @Title: delUserHealthItem  
	     * @Description: TODO  
	     * @param @throws Exception    
	     * @return void  
		 * @author 孙灵达  
	     * @date 2018年8月1日
	     * @throws
	 */
	@Action(value="delUserHealthItem")
	public void delUserHealthItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			userService.delUserHealthItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	/**
		 *	展示获取证书情况
	     * @Title: showUserCertTableListJson  
	     * @Description: TODO  
	     * @param     
	     * @return void  
		 * @author 孙灵达  
	     * @date 2018年8月1日
	     * @throws
	 */
	@Action(value="showUserCertTableListJson")
	public void showUserCertTableListJson() {
		String uid = getParameterFromRequest("id");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = userService.showUserCertTableListJson(uid, start, length,
					query, col, sort);
			List<UserCert> list = (List<UserCert>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("no", "");
			map.put("sendOrg", "");
			map.put("sendDate", "yyyy-MM-dd");
			map.put("overDate", "yyyy-MM-dd");
			map.put("remark", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
		 *	小保存 获取证书情况
	     * @Title: saveUserCertItemTable  
	     * @Description: TODO  
	     * @param @throws Exception    
	     * @return void  
		 * @author 孙灵达  
	     * @date 2018年8月1日
	     * @throws
	 */
	@Action(value="saveUserCertItemTable")
	public void saveUserCertItemTable() throws Exception {
		String id = getParameterFromRequest("id");
		String changeLog = getParameterFromRequest("changeLog");
		String itemJson=getParameterFromRequest("dataJson");
		Map<String, Object> map=new HashMap<String, Object>();
		try {
			userService.saveUserCertItemTable(id,changeLog,itemJson);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	/**
		 *	选中删除(用户证书情况)
	     * @Title: delUserCertItem  
	     * @Description: TODO  
	     * @param @throws Exception    
	     * @return void  
		 * @author 孙灵达  
	     * @date 2018年8月1日
	     * @throws
	 */
	@Action(value="delUserCertItem")
	public void delUserCertItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			userService.delUserCertItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: showUserList
	 * @Description:用户列表
	 * @author : shengwei.wang
	 * @date 2018年4月4日上午9:45:21
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showUserList")
	public String showUserList() throws Exception {
		rightsId = "110102";
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/core/user/showUserList.jsp");
	}

	@Action(value = "getUserList")
	public void getUserList() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = userService.findUser(start, length,
					query, col, sort);
			List<User> list = (List<User>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "name");
			map.put("sex", "");
			map.put("birthday", "yyyy-MM-dd");
			map.put("mobile", "");
			map.put("inDate", "yyyy-MM-dd");
			map.put("department-id", "");
			map.put("department-name", "");
			map.put("area", "");
			map.put("city", "");
			map.put("state-name", "");
			map.put("cardNo", "");
			map.put("phone", "");
			map.put("email", "");
			map.put("address", "");
			map.put("scopeName", "");
			map.put("emergencyContactPerson", "");
			map.put("emergencyContactMethod", "");
			map.put("edu-name", "");
			map.put("contactMethod", "");
			map.put("graduate", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	

	/**
	 * 
	 * @Title: userRoleShow
	 * @Description: 用户权限
	 * @author : shengwei.wang
	 * @date 2018年4月4日上午11:53:19
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */

	@Action(value = "userRoleShowJson")
	public void userRoleShowJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String userId = getParameterFromRequest("id");
		try {
			Map<String, Object> result = new HashMap<String, Object>();
			result = userService.userRoleShowJson(userId, start, length, query,
					col, sort);
			HttpUtils.write(PushData.pushData(draw, result,
					(String) result.get("data")));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: setRole
	 * @Description: 设置权限
	 * @author : shengwei.wang
	 * @date 2018年4月8日下午2:58:59
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "setRole")
	public void setRole() throws Exception {
		String data = getParameterFromRequest("dataJson");
		String id = getParameterFromRequest("id");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			userService.setRole(data, id);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: showUserGroupUserList
	 * @Description: 人员组
	 * @author : shengwei.wang
	 * @date 2018年4月4日上午11:55:13
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */

	@Action(value = "showUserGroupUserListJson")
	public void showUserGroupUserListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String userId = getParameterFromRequest("id");
		try {
			Map<String, Object> result = new HashMap<String, Object>();
			result = userService.showUserGroupUserListJson(userId, start,
					length, query, col, sort);
			HttpUtils.write(PushData.pushData(draw, result,
					(String) result.get("data")));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: showUserJobUserList
	 * @Description: 岗位
	 * @author : shengwei.wang
	 * @date 2018年4月4日上午11:56:32
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showUserJobUserListJson")
	public void showUserJobUserListJson() throws Exception {
		String userId = getParameterFromRequest("userId");
		User user = (User) this
				.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		new SendData().sendDataJson(
				userService.findUserJobUser(user.getId(), userId),
				ServletActionContext.getResponse());
	}

	/**
	 * 
	 * @Title: setUserGroup
	 * @Description: 用户人员组配置
	 * @author : shengwei.wang
	 * @date 2018年4月8日下午6:15:45
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "setUserGroup")
	public void setUserGroup() throws Exception {
		String data = getParameterFromRequest("dataJson");
		String id = getParameterFromRequest("id");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			userService.setUserGroup(data, id);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: setUserJob
	 * @Description: 设置岗位
	 * @author : shengwei.wang
	 * @date 2018年4月9日下午5:17:37
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "setUserJob")
	public String setUserJob() throws Exception {

		userService.setUserJob(user);
		return redirect("/core/user/showUserJobUserList.action?userId="
				+ user.getId());
	}

	/**
	 * 访问用户认证信息列表
	 */

	@Action(value = "userCertShowJsonByQuery")
	public void userCertShowJsonByQuery() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String userId = getParameterFromRequest("userId");

		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		List<UserCert> list = null;
		long totalCount = 0;
		if (userId != null && !userId.equals("")) {
			Map<String, Object> controlMap = userService.findUserCert(startNum,
					limitNum, dir, sort, getContextPath(), userId);
			totalCount = Long
					.parseLong(controlMap.get("totalCount").toString());
			list = (List<UserCert>) controlMap.get("list");
		}
		Map<String, String> map = new HashMap<String, String>();
		map.put("name", "");
		map.put("no", "");
		map.put("sendOrg", "");
		map.put("sendDate", "yyyy-MM-dd");
		map.put("expireTime", "");
		map.put("dicUnit-id", "");
		map.put("remark", "");
		map.put("id", "");
		// map.put("user-id", "");
		if (list != null)
			new SendData().sendDateJson(map, list, totalCount,
					ServletActionContext.getResponse());
	}

	/**
	 * 访问用户疫苗信息列表
	 */

	@Action(value = "userVaccineShowJson")
	public void userVaccineShowJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String userId = getParameterFromRequest("userId");

		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		List<UserVaccine> list = null;
		long totalCount = 0;
		if (userId != null && !userId.equals("")) {
			Map<String, Object> controlMap = userService.findUserVaccine(
					startNum, limitNum, dir, sort, getContextPath(), userId);
			totalCount = Long
					.parseLong(controlMap.get("totalCount").toString());
			list = (List<UserVaccine>) controlMap.get("list");
		}
		Map<String, String> map = new HashMap<String, String>();
		map.put("name", "");
		map.put("vaccinationDate", "yyyy-MM-dd");
		map.put("expireTime", "");
		map.put("dicUnit-id", "");
		map.put("note", "");
		map.put("id", "");
		// map.put("user-id", "");
		if (list != null)
			new SendData().sendDateJson(map, list, totalCount,
					ServletActionContext.getResponse());
	}

	/**
	 * 添加用户
	 */
	@Action(value = "save")
	public String save() throws Exception {
		Map aMap = new HashMap();
		aMap.put("userRoleJson",
				getParameterFromRequest("userRoleJson"));
		aMap.put("userGroupJson",
				getParameterFromRequest("userGroupJson"));
		String changeLog = URLDecoder.decode(getParameterFromRequest("changeLog"), "UTF-8");
		userService.addUser(user,aMap,changeLog);
		return redirect("/core/user/toEditUser.action?id=" + user.getId());
	}

	/**
	 * 查询是否用户帐号重复
	 */
	@Action(value = "hasUserId")
	public String hasUserId() throws Exception {
		String userId = getParameterFromRequest("userId");
		return this.renderText(userService.hasUserId(userId));

	}

	/**
	 * 添加用户证书
	 */
	@Action(value = "addUserCert")
	public String addUserCert() throws Exception {
		String userId = getParameterFromRequest("userId");
		String jsonString = getParameterFromRequest("data");
		userService.addUserCert(userId, jsonString);
		return redirect("/core/user/userCertShow.action");
	}

	/**
	 * 删除用户证书
	 */
	@Action(value = "delUserCert")
	public String delUserCert() throws Exception {
		String id = getParameterFromRequest("id");
		userService.delUserCert(id);
		// 具体操作，如删除，填加动作，应用redirect
		return redirect("/core/user/userCertShow.action");
	}

	/**
	 * 保存用户疫苗信息
	 */
	@Action(value = "saveUserVaccine")
	public void saveUserVaccine() throws Exception {
		String userId = getParameterFromRequest("userId");
		String jsonString = getParameterFromRequest("data");
		userService.saveUserVaccine(userId, jsonString);
		// return redirect("/core/user/userVaccineShow.action");
	}

	/**
	 * 删除用户疫苗信息
	 */
	@Action(value = "delUserVaccine")
	public void delUserVaccine() throws Exception {
		String id = getParameterFromRequest("id");
		userService.delUserVaccine(id);
		// 具体操作，如删除，填加动作，应用redirect
		// return redirect("/core/user/userVaccineShow.action");
	}

	/**
	 * 
	 * @Title: toEditUser
	 * @Description:新建、编辑
	 * @author : shengwei.wang
	 * @date 2018年4月4日下午3:11:56
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "toEditUser")
	public String toEditUser() throws Exception {
		rightsId = "110102";
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			user = userService.findUser(id);
			String userDepartmentId = "";
			if (user.getDepartment() != null) {
				userDepartmentId = user.getDepartment().getId();
			}
			toToolBar(rightsId, userDepartmentId, user.getId(),
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			putObjToContext("userId", user.getId());
		} else {
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);

		}
		List<DicCostCenter> list = userService.findDicCostCenterList();
		putObjToContext("DicCostCenter", list);
		return dispatcher("/WEB-INF/page/core/user/editUser.jsp");
	}

	/**
	 * 访问 查看用户页面
	 */
	@Action(value = "toViewUser")
	public String toViewUser() throws Exception {
		String id = getParameterFromRequest("id");

		user = userService.findUser(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		putObjToContext("userId", user.getId());
		return dispatcher("/WEB-INF/page/core/user/editUser.jsp");
	}

	/**
	 * 删除用户,设置失效
	 */
	@Action(value = "delUser")
	public String delUser() throws Exception {
		String id = getParameterFromRequest("id");
		user = userService.findUser(id);
		userService.delUser(user);
		// 具体操作，如删除，填加动作，应用redirect
		return redirect("/core/user/userShow.action");
	}

	/**
	 * 激活用户,设置生效
	 */
	@Action(value = "useUser")
	public String useUser() throws Exception {
		String id = getParameterFromRequest("id");
		String changeLog = getParameterFromRequest("changeLog");
		user = userService.findUser(id);
		userService.addUser(user,null,changeLog);
		// 具体操作，如删除，填加动作，应用redirect
		return redirect("/core/user/showUserList.action");
	}

	@Action(value = "userSetView")
	public String userSetView() throws Exception {
		user = (User) getRequest().getSession().getAttribute(
				SystemConstants.USER_SESSION_KEY);
		// 具体操作，如删除，填加动作，应用redirect
		return dispatcher("/WEB-INF/page/core/user/userSet.jsp");
	}

	@Action(value = "userSet", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String userSet() throws Exception {
		String id = getRequest().getParameter("id");
		String pwd = getRequest().getParameter("pwd");
		String mobile = getRequest().getParameter("mobile");
		String address = getRequest().getParameter("address");
		User modifyUser = new User();
		modifyUser.setAddress(address);
		modifyUser.setId(id);
		modifyUser.setMobile(mobile);
		modifyUser.setUserPassword(pwd);

		user = (User) getRequest().getSession().getAttribute(
				SystemConstants.USER_SESSION_KEY);
		User _modifyUser = this.userService.modifyUser(modifyUser, user);
		getRequest().getSession().setAttribute(
				SystemConstants.USER_SESSION_KEY, _modifyUser);
		return this.renderText("true");
	}

	// /**
	// * 访问通用用户检索列表
	// */
	//
	// @Action(value = "showUserListAllSelect", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public String showUserListAllSelect() throws Exception {
	// String planType = getParameterFromRequest("planType");
	// LinkedHashMap<String, String[]> map = new LinkedHashMap<String,
	// String[]>();
	// map.put("id", new String[] { "", "string", "", "用户账号", "80", "true",
	// "", "", "", "", "", "" });
	// map.put("name", new String[] { "", "string", "", "姓名", "150", "true",
	// "", "", "", "", "", "" });
	// map.put("sex", new String[] { "", "string", "", "性别", "75", "true", "",
	// "", "", "", "", "" });
	// map.put("birthday", new String[] { "", "date", "dateFormat:'Y-m-d'",
	// "生日", "100", "true", "", "", "", "",
	// "Ext.util.Format.dateRenderer('Y-m-d')", "" });
	// map.put("mobile", new String[] { "", "string", "", "手机", "150", "true",
	// "", "", "", "", "", "" });
	// map.put("inDate", new String[] { "", "date", "dateFormat:'Y-m-d'",
	// "入职日期", "100", "true", "", "", "", "",
	// "Ext.util.Format.dateRenderer('Y-m-d')", "" });
	// map.put("department-id", new String[] { "", "string", "", "部门ID",
	// "100", "true", "", "", "", "", "", "" });
	// map.put("department-name", new String[] { "", "string", "", "部门",
	// "150", "true", "", "", "", "", "", "" });
	//
	// String type = generalexttype(map);
	// String col = generalextcol(map);
	// putObjToContext("planType", planType);
	// putObjToContext("type", type);
	// putObjToContext("col", col);
	// putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// putObjToContext("path", ServletActionContext.getRequest()
	// .getContextPath()
	// + "/core/user/showUserListAllSelectJson.action");
	// return dispatcher("/WEB-INF/page/core/user/showUserListAllSelect.jsp");
	// }
	//
	// @Action(value = "showUserListAllSelectJson", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void showUserListAllSelectJson() throws Exception {
	// // 开始记录数
	// int startNum = Integer.parseInt(getParameterFromRequest("start"));
	// // limit
	// int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
	// // 字段
	// String dir = getParameterFromRequest("dir");
	// // 排序方式
	//
	// String sort = getParameterFromRequest("sort");
	// String data = getParameterFromRequest("data");
	//
	// Map<String, Object> controlMap = userService.findUserList(startNum,
	// limitNum, dir, sort, getContextPath(), data);
	// long totalCount = Long.parseLong(controlMap.get("totalCount")
	// .toString());
	// List<User> list = (List<User>) controlMap.get("list");
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("name", "name");
	//
	// map.put("birthday", "yyyy-MM-dd");
	// map.put("mobile", "");
	// map.put("inDate", "yyyy-MM-dd");
	// map.put("department-id", "");
	// map.put("department-name", "");
	// map.put("state", "");
	// new SendData().sendDateJson(map, list, totalCount,
	// ServletActionContext.getResponse());
	// }

	// @Action(value = "userSelect", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public String userSelect() throws Exception {
	// String planType = getParameterFromRequest("planType");
	// String flag = getParameterFromRequest("flag");
	// LinkedHashMap<String, String[]> map = new LinkedHashMap<String,
	// String[]>();
	// map.put("id", new String[] { "", "string", "", "用户账号", "80", "true",
	// "", "", "", "", "", "" });
	// map.put("name", new String[] { "", "string", "", "姓名", "100", "true",
	// "", "", "", "", "", "" });
	// map.put("birthday", new String[] { "", "date", "dateFormat:'Y-m-d'",
	// "生日", "100", "true", "", "", "", "",
	// "Ext.util.Format.dateRenderer('Y-m-d')", "" });
	// map.put("mobile", new String[] { "", "string", "", "手机", "100", "true",
	// "", "", "", "", "", "" });
	// map.put("inDate", new String[] { "", "date", "dateFormat:'Y-m-d'",
	// "入职日期", "100", "true", "", "", "", "",
	// "Ext.util.Format.dateRenderer('Y-m-d')", "" });
	// map.put("department-id", new String[] { "", "string", "", "部门ID",
	// "100", "true", "true", "", "", "", "", "" });
	// map.put("department-name", new String[] { "", "string", "", "部门",
	// "150", "true", "", "", "", "", "", "" });
	// map.put("dicJob-name", new String[] { "common", "string", "", "岗位",
	// "100", "true", "", "", "", "", "", "" });
	//
	// String type = generalexttype(map);
	// String col = generalextcol(map);
	// // 用于ext显示
	// putObjToContext("planType", planType);
	// putObjToContext("type", type);
	// putObjToContext("col", col);
	// putObjToContext("flag", col);
	// // 用于判断当前页面类型,LIST类型
	// putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
	// // 用于调用json时的action链接
	// putObjToContext("path", ServletActionContext.getRequest()
	// .getContextPath() + "/core/user/userSelectJson.action");
	// // 导向jsp页面显示
	// return dispatcher("/WEB-INF/page/core/user/userSelect.jsp");
	// }
	//
	// @Action(value = "userSelectJson", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void showUserListJson() throws Exception {
	// // 开始记录数
	// int startNum = Integer.parseInt(getParameterFromRequest("start"));
	// // limit
	// int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
	// // 字段
	// String dir = getParameterFromRequest("dir");
	// // 排序方式
	//
	// String sort = getParameterFromRequest("sort");
	// String data = getParameterFromRequest("data");
	//
	// Map<String, Object> controlMap = userService.findUserList(startNum,
	// limitNum, dir, sort, getContextPath(), data);
	// long totalCount = Long.parseLong(controlMap.get("totalCount")
	// .toString());
	// List<User> list = (List<User>) controlMap.get("list");
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("name", "name");
	//
	// map.put("birthday", "yyyy-MM-dd");
	// map.put("mobile", "");
	// map.put("inDate", "yyyy-MM-dd");
	// map.put("department-id", "");
	// map.put("department-name", "");
	// map.put("state", "");
	// new SendData().sendDateJson(map, list, totalCount,
	// ServletActionContext.getResponse());
	// }
	/**
	 * 
	 * @Title: 
	 * @Description:单选用户
	 * @author : shengwei.wang
	 * @date 2018年4月9日下午5:24:09
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "selectUserTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String selectUserTable() throws Exception {
		String groupId = getParameterFromRequest("groupId");
		putObjToContext("groupId", groupId);
		return dispatcher("/WEB-INF/page/core/user/selectUserTable.jsp");
	}
	//生产计划选择操作人
	@Action(value = "selectProductUserTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String selectProductUserTable() throws Exception {
		String groupId = getParameterFromRequest("groupId");
		String planWorkDate = getParameterFromRequest("planWorkDate");
		putObjToContext("groupId", groupId);
		putObjToContext("planWorkDate", planWorkDate);
		return dispatcher("/WEB-INF/page/core/user/selectProductUserTable.jsp");
	}

	@Action(value = "selectUserTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selectUserTableJson() throws Exception {
		String groupId = getParameterFromRequest("groupId");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = userService.selectUserTableJson(start,
				length, query, col, sort, groupId);
		List<UserGroupUser> list = (List<UserGroupUser>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("user-id", "");
		map.put("user-name", "");
		map.put("userGroup-name", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}
	//生产计划
	@Action(value = "selectProductUserTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selectProductUserTableJson() throws Exception {
		String groupId = getParameterFromRequest("groupId");
		String query = getParameterFromRequest("query");
		String planWorkDate = getParameterFromRequest("planWorkDate");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");

		Map<String, Object> result = userService.selectProductUserTableJson(start,
				length, query, col, sort, groupId,planWorkDate);
		List<Object[]> list = (List<Object[]>) result.get("list");
	    
		List<Map<String,String>> data =new ArrayList<Map<String,String>>();	
		for (Object[] objects : list) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("user-id", String.valueOf(objects[0]));	
			map.put("user-name", String.valueOf(objects[1]));	
			map.put("user-productState", String.valueOf(objects[2]));	
			data.add(map);
		}
	
//		map.put("user-id", "");
//		map.put("user-name", "");
//		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, JsonUtils.toJsonString(data)));
	}
	
	@Action(value = "selectUserTableNewJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selectUserTableNewJson() throws Exception {
		String groupId = getParameterFromRequest("groupId");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = userService.selectUserTableNewJson(start,
				length, query, col, sort, groupId);
		List<User> list = (List<User>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
//		map.put("userGroup-name", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}
	@Action(value = "selectProductUserTableNewJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selectProductUserTableNewJson() throws Exception {
		String groupId = getParameterFromRequest("groupId");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = userService.selectProductUserTableNewJson(start,
				length, query, col, sort, groupId);
		List<User> list = (List<User>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
//		map.put("userGroup-name", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}
	/**
	 * 
	 * @Title: selectUsersTable  
	 * @Description: 多选  
	 * @author : shengwei.wang
	 * @date 2018年4月9日下午5:24:33
	 * @return
	 * @throws Exception
	 * String
	 * @throws
	 */
	@Action(value = "selectUsersTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String selectUsersTable() throws Exception {
		String groupId = getParameterFromRequest("groupId");
		putObjToContext("groupId", groupId);
		return dispatcher("/WEB-INF/page/core/user/checkboxUsersTable.jsp");
	}

	@Action(value = "selectUsersTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selectUsersTableJson() throws Exception {
		String groupId = getParameterFromRequest("groupId");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = userService.selectUsersTableJson(start,
				length, query, col, sort, groupId);
		List<User> list = (List<User>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("email", "");
		map.put("inDate", "");
		map.put("address", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}
	@Action(value="showCostCenter", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCostCenter(){
		return dispatcher("/WEB-INF/page/costCenter/selectCostCenter.jsp");
	}
	@Action(value="showCostCenterJson")
	public void showCostCenterJson(){
		try {
			List<DicCostCenter> list=userService.showCostCenterJson();
			HttpUtils.write(JsonUtils.toJsonString(list));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public List<DicUnit> getListDicUnit() {
		return listDicUnit;
	}

	public void setListDicUnit(List<DicUnit> listDicUnit) {
		this.listDicUnit = listDicUnit;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
