package com.biolims.core.user.action;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.core.model.user.DicJob;
import com.biolims.core.user.service.DicJobService;
import com.biolims.util.SendData;

@Namespace("/dic/job")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings( { "unchecked" })
public class DicJobAction extends BaseActionSupport {

	private static final long serialVersionUID = -8841346883208605554L;

	@Autowired
	private DicJobService dicJobService;

	//用于页面上显示模块名称
	private String title = "岗位管理";

	//该action权限id
	private String rightsId = "1112";

	/**
	 * 访问列表
	 */
	@Action(value = "showDicjobList")
	public String showDicjobListView() throws Exception {
		String handlemethod = getParameterFromRequest("handlemethod");
		String exttype = "";
		String extcol = "";
		String editflag = "true";
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		//  属性map<字段名称, ext-list属性数组说明,值为空""则没有该属性>
		//  id type format header width  sort hide align xtype  editable renderer editor
		//  列的id 当"common" 时该列为扩展列  type类型 format格式 header列头 width列宽 sort是否排序  hide是否隐藏 align左右格式 xtype ediable是否可编辑 renderer渲染 editor编辑器 
		map.put("id", new String[] { "", "string", "", "ID", "32", "true", "true", "", "", "", "", "" });
		map.put("name",
				new String[] { "", "string", "", "名称", "110", "true", "false", "", "", editflag, "", "typeName" });
		map.put("isLeader", new String[] { "", "string", "", "是否为部门领导", "120", "true", "false", "", "", editflag,
				"Ext.util.Format.comboRenderer(cob)", "cob" });
		map.put("isPurchase", new String[] { "", "string", "", "是否为采购岗位", "120", "true", "false", "", "", editflag,
				"Ext.util.Format.comboRenderer(cob1)", "cob1" });
		map.put("isSaleManage", new String[] { "", "string", "", "是否为销售管理", "120", "true", "false", "", "", editflag,
				"Ext.util.Format.comboRenderer(cob2)", "cob2" });
		map.put("note", new String[] { "", "string", "", "备注", "80", "true", "false", "", "", editflag, "", "note" });
		map.put("department-id", new String[] { "", "string", "", "部门ID", "110", "true", "false", "", "", editflag, "",
				"department" });
		map.put("department-name", new String[] { "", "string", "", "部门名称", "110", "true", "false", "", "", editflag,
				"", "department" });
		//生成ext用type字符串
		exttype = generalexttype(map);
		//生成ext用col字符串
		extcol = generalextcol(map);
		putObjToContext("type", exttype);
		putObjToContext("col", extcol);
		putObjToContext("path", ServletActionContext.getRequest().getContextPath() + "/dic/job/showDicjobJson.action");
		putObjToContext("statement", "");
		putObjToContext("statementLisener", "");

		putObjToContext("handlemethod", handlemethod);
		return dispatcher("/WEB-INF/page/dic/showDicjobList.jsp");
	}

	@Action(value = "showDicjobJson")
	public void showDicjobJson() throws Exception {
		int start = Integer.parseInt(getParameterFromRequest("start"));
		int limit = Integer.parseInt(getParameterFromRequest("limit"));

		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		long total = 0;

		Map<String, Object> result = this.dicJobService.queryDicJob(start, limit, dir, sort);
		total = (Long) result.get("total");
		List<DicJob> list = (List<DicJob>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("isLeader", "");
		map.put("isPurchase", "");
		map.put("isSaleManage", "");
		map.put("note", "");
		map.put("department-id", "");
		map.put("department-name", "");
		if (list != null)
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
	}

	/**
	 * 添加
	 */
	@Action(value = "editDicJob")
	public void editDicJob() throws Exception {
		String jsonString = getParameterFromRequest("data");
		dicJobService.addOrUpdate(jsonString);
	}

	/**
	 * 删除
	 */
	@Action(value = "delDicJob")
	public void delDicJob() throws Exception {
		String id = getParameterFromRequest("id");
		dicJobService.delDicJob(id);

	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

}
