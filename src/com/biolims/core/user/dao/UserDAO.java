package com.biolims.core.user.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.cfg.Configuration;
import org.hibernate.mapping.Column;
import org.hibernate.mapping.PersistentClass;
import org.hibernate.mapping.Property;
import org.springframework.stereotype.Repository;

import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.core.model.user.DicCostCenter;
import com.biolims.core.model.user.DicJob;
import com.biolims.core.model.user.Role;
import com.biolims.core.model.user.UserCert;
import com.biolims.core.model.user.UserGroup;
import com.biolims.core.model.user.UserGroupUser;
import com.biolims.core.model.user.UserJobUser;
import com.biolims.core.model.user.UserRole;
import com.biolims.dic.model.DicType;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class UserDAO extends CommonDAO {

	public long getUserGroupCount() throws Exception {
		return this.getCount("from UserGroup");

	}

	public long getUserJobCount() throws Exception {
		return this.getCount("from DicJob");
	}

	public List<UserGroup> getUserGroupList() throws Exception {
		return this.find("from UserGroup");

	}

	public List<UserGroup> getUserCommonGroupList() throws Exception {
		return this.find("from UserGroup where type ='0'");

	}

	public List<DicCostCenter> getDicCostCenterList() throws Exception {
		return this.find("from DicCostCenter where state like '1%' order by orderNumber ASC");

	}

	public List<DicJob> getDicJobList() throws Exception {
		return this.find("from DicJob");

	}

	public List<UserGroup> getUserGroupUserListByUser(String userId) throws Exception {
		return this.find("select a.userGroup from UserGroupUser a where a.user.id=?", new Object[] { userId });

	}

	public long getUserGroupUserCountByUser(String userId) throws Exception {
		return this.getCount("from UserGroupUser where user.id=?", new Object[] { userId });

	}

	public void deleteUserGroupUserByUser(String userId, String userGroupId) throws Exception {
		executeUpdate("delete UserGroupUser where user.id='" + userId + "' and userGroup.id='" + userGroupId + "'");

	}

	public void deleteUserJobUserByUser(String userId, String userJobId) throws Exception {
		executeUpdate("delete UserJobUser where user.id='" + userId + "' and job.id='" + userJobId + "'");

	}

	public void deleteUserRole(String userId, String roleId) throws Exception {
		executeUpdate("delete UserRole where user.id='" + userId + "' and role.id='" + roleId + "'");

	}

	public List<UserGroup> getUserGroupByUser(String userId) throws Exception {
		return this.find("select a.userGroup from UserGroupUser a where a.user.id=?", new Object[] { userId });

	}

	public List<DicJob> getUserJobByUser(String userId) throws Exception {
		return this.find("select a.job from UserJobUser a where a.user.id=?", new Object[] { userId });

	}

	public List<User> getUserByUserPurchaseJob() throws Exception {
		return this.find("select a.user from UserJobUser a where a.job.isPurchase=?",
				new Object[] { SystemConstants.DIC_STATE_YES_ID });

	}

	// 查询dicType表内模块名称
	public Map<String, Object> finDicTypeByType(String type) {
		String hql = "from DicType where 1=1 and type='" + type + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql).uniqueResult();
		List<DicType> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

//	public Map<String, Object> showUserTable(Integer start, Integer length,
//			String query, String col, String sort,
//			String groupId) throws Exception {
//		Map<String, Object> map = new HashMap<String, Object>();
//		String userHql=" select ";
//		String key = "";
////		if(queryMap!=null){
////			key=map2where(queryMap);
////		}
//		if(groupId!=null){
//			key+=" and groupId";
//		}
//		String countHql = "select count(*) from User where 1=1 and state='1z'";
//		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
//		if(0l!= sumCount){
//			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
//			String hql = "from User  where 1=1 and state='1z'";
//			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
//				key+=" order by "+col+" "+sort;
//			}
//			@SuppressWarnings("unchecked")
//			List<User> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
//			map.put("recordsTotal", sumCount);
//			map.put("recordsFiltered", filterCount);
//			map.put("list", list);
//			}
//		return map;
//	}

	public Map<String, Object> selectUserTableJson(Integer start, Integer length, String query, String col, String sort,
			String groupId) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from UserGroupUser where 1=1 and userGroup.id='" + groupId + "'";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from UserGroupUser where 1=1 and userGroup.id='" + groupId + "' ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<UserGroupUser> list = new ArrayList<UserGroupUser>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	// 生产计划
	public Map<String, Object> selectProductUserTableJson(Integer start,
			Integer length, String query, String col,
			String sort, String groupId,String planWorkDate ) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from UserGroupUser where 1=1 and userGroup.id='"+groupId+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
//		  String name="";
//		Configuration configuration = new Configuration().configure();
//		PersistentClass classMapping = configuration.getClassMapping(User.class.getName());
//		Property property = classMapping.getProperty("orderNumber");
//		Iterator columnIterator = property.getColumnIterator();
//		if(columnIterator.hasNext()) {
//			  Column column = (Column) columnIterator.next();
//			 name= column.getName();
//		}
//		System.out.println(name);
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String sql="SELECT" + 
					"  tu.id," + 
					"	tu.name," + 
					"	count( cpr.temple_operator ) AS sum " + 
					"FROM" + 
					"	T_USER_GROUP_USER tgu" + 
					"	INNER JOIN t_user tu ON tgu.user_id = tu.id" + 
					"	LEFT JOIN ( SELECT * FROM cell_production_record WHERE plan_work_date = '"+planWorkDate+"' ) AS cpr ON tgu.user_id = cpr.temple_operator " + 
					"WHERE" + 
					"	tgu.user_group_id = '"+groupId+"'" + 
					"GROUP BY" + 
					"	tgu.user_id " + 
					"ORDER BY" + 
					"	sum asc";
//			String hql = "from UserGroupUser where 1=1 and userGroup.id='"+groupId+"' ";
//			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
//				col=col.replace("-", ".");
//				key+=" ,"+col+" "+sort;
//			}
			List<Object[]> objects =new ArrayList<Object[]>();
//			List<UserGroupUser> list = new ArrayList<UserGroupUser>();
//			objects=this.getSession().createSqlQuery(hql + key)
//					.setFirstResult(start).setMaxResults(length)
//					.list();
			objects=this.getSession().createSQLQuery(sql).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", objects);
		}
		return map;
		
	}

	public Map<String, Object> selectUserTableNewJson(Integer start, Integer length, String query, String col,
			String sort, String groupId) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from User where 1=1 and state.id='1z'";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from User where 1=1 and state.id='1z'";
//			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
//				col=col.replace("-", ".");
//				key+=" order by "+col+" "+sort;
//			}
			List<User> list = new ArrayList<User>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	// 生产计划选择操作人
	public Map<String, Object> selectProductUserTableNewJson(Integer start, Integer length, String query, String col,
			String sort, String groupId) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from User where 1=1 and state.id='1z'";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from User where 1=1 and state.id='1z'";
//			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
//				col=col.replace("-", ".");
//				key+=" order by "+col+" "+sort;
//			}
			List<User> list = new ArrayList<User>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	public Map<String, Object> findUser(Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from User where 1=1 ";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from User where 1=1 ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<User> list = new ArrayList<User>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	public List<Role> userRoleShowJson() {
		String hql = " from Role where 1=1 and state='1'";
		String key = "";
		List<Role> list = this.getSession().createQuery(hql + key).list();
		return list;
	}
	public Map<String, Object> userRoleShowJsonNew(Integer start,Integer length,String col,String sort) {
		Map<String, Object> map = new HashMap<String, Object>();

		String countHql ="select count(*) from Role where 1=1 and state='1'";
		String key = "";

		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();

		
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();

			String hql = " from Role where 1=1 and state='1'";
			
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<Role> list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		
		return map;
	}

	public List<UserRole> getUserRole(String userId) {
		String hql = " from UserRole where 1=1 and user.id='" + userId + "'";
		String key = "";
		List<UserRole> list = this.getSession().createQuery(hql + key).list();
		return list;
	}

	public List<UserGroup> showUserGroupUserListJson() {
		String hql = " from UserGroup where 1=1 and state='1'";
		String key = "";
		List<UserGroup> list = this.getSession().createQuery(hql + key).list();
		return list;
	}
	
	public Map<String, Object> showUserGroupUserListJsonNew(Integer start,Integer length,String col,String sort) {
		Map<String, Object> map = new HashMap<String, Object>();

		String countHql ="select count(*) from UserGroup where 1=1 and state='1'";
		String key = "";

		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();

		
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();

			String hql = " from UserGroup where 1=1 and state='1'";
			
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<UserGroup> list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		
		return map;
	}

	public List<UserGroupUser> getUserGroup(String userId) {
		String hql = " from UserGroupUser where 1=1 and user.id='" + userId + "'";
		String key = "";
		List<UserGroupUser> list = this.getSession().createQuery(hql + key).list();
		return list;
	}

	public List<User> getUser() {
		return this.find("from User");
	}

	public Map<String, Object> selectUsersTableJson(Integer start, Integer length, String query, String col,
			String sort, String groupId) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from User where 1=1 and state.id='1z'";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from User where 1=1 and state.id='1z'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<User> list = new ArrayList<User>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	public List<DicCostCenter> showCostCenterJson() {
		String hql = " from DicCostCenter where state='1'";
		List<DicCostCenter> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> showUserHealthTableListJson(String uid, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from UserJobUser where 1=1 and user.id='" + uid + "'";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from UserJobUser where 1=1 and user.id='" + uid + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<UserJobUser> list = new ArrayList<UserJobUser>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> showUserCertTableListJson(String uid, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from UserCert where 1=1 and user.id='" + uid + "'";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from UserCert where 1=1 and user.id='" + uid + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<UserCert> list = new ArrayList<UserCert>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
}
