package com.biolims.core.user.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.common.dao.CommonDAO;
import com.biolims.core.model.user.DicJob;

@Repository
@SuppressWarnings("unchecked")
public class DicJobDao extends CommonDAO {

	public Map<String, Object> selectDicJob(int startNum, int limitNum, String dir, String sort) {
		String hql = "from DicJob";
		Map<String, Object> map = new HashMap<String, Object>();
		Long total = (Long) this.getSession().createQuery("select count(id) " + hql).uniqueResult();
		List<DicJob> list = null;
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0)
				hql = hql + " order by " + sort + " " + dir;
			list = this.getSession().createQuery(hql).setFirstResult(startNum).setMaxResults(limitNum).list();

		}
		map.put("total", total);
		map.put("list", list);
		return map;
	}
}
