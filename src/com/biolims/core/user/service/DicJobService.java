package com.biolims.core.user.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.core.model.user.DicJob;
import com.biolims.core.user.dao.DicJobDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.util.JsonUtils;

@Service
public class DicJobService {
	@Resource
	private DicJobDao dicJobDao;

	public Map<String, Object> queryDicJob(int startNum, int limitNum, String dir, String sort) {
		return dicJobDao.selectDicJob(startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void addOrUpdate(String jsonString) throws Exception {
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(jsonString, List.class);
		for (Map<String, Object> map : list) {
			DicJob dj = new DicJob();
			dj = (DicJob) dicJobDao.Map2Bean(map, dj);
			dicJobDao.saveOrUpdate(dj);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDicJob(String id) {
		DicJob uct = dicJobDao.get(DicJob.class, id);
		dicJobDao.delete(uct);
	}

}
