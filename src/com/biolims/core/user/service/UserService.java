/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：UserService
 * 类描述：用户管理service
 * 创建人：倪毅
 * 创建时间：2011-9
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.core.user.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.core.constants.SystemConstants;
import com.biolims.core.model.user.DicCostCenter;
import com.biolims.core.model.user.DicJob;
import com.biolims.core.model.user.Role;
import com.biolims.core.model.user.UserCert;
import com.biolims.core.model.user.UserGroup;
import com.biolims.core.model.user.UserGroupUser;
import com.biolims.core.model.user.UserJobUser;
import com.biolims.core.model.user.UserRole;
import com.biolims.core.model.user.UserVaccine;
import com.biolims.core.user.dao.UserDAO;
import com.biolims.experiment.dna.model.DnaTaskInfo;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.model.LogInfo;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.MD5Util;
import com.biolims.util.MsgLocale;

@Service
public class UserService {
	@Resource
	private UserDAO userDAO;

	@Resource
	private CommonDAO commonDAO;

	public Map<String, Object> findUser(Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return userDAO.findUser(start, length, query, col, sort);
	}

	// /**
	// *
	// * 检索用户,采用map方式传递检索参数
	// *
	// *
	// * @return 参数值MAP，如果传递的为null返回空字符串
	// */
	// public String findUserRoleStr(String myUserId, String userId)
	// throws Exception {
	// long count = userDAO.getCount("from Role where state = '1'");
	// List<Role> roleList = userDAO.find("from Role where state = '1'");
	// String[] a = { userId };
	// List<Role> userRoleList = userDAO.find(
	// "select a.role from UserRole a where a.user.id =?", a);
	// StringBuffer str = new StringBuffer("");
	// for (Role aRole : roleList) {
	//
	// str.append("{'select':'")
	// .append("<input type=\"checkbox\" name=\"select\"
	// style=\"height:15;width:15\" ")
	// .append(userRoleList.contains(aRole) ? "checked" : "")
	// .append(" id=\"select\" value=\"" + aRole.getId() + "\" />");
	// str.append("','id':'").append(aRole.getId()).append("'");
	// str.append(",'name':'").append(aRole.getName()).append("'");
	// str.append(",'note':'")
	// .append(aRole.getNote() == null ? "" : aRole.getNote())
	// .append("'}");
	// str.append(",");
	// }
	// String jsonDate = str.toString();
	// return ("{'total':" + count + ",'results':[" + jsonDate + "]}")
	// .replaceAll(",]", "]");
	// }

	// /**
	// * 用户组
	// *
	// * @param myUserId
	// * @param userId
	// * @return
	// * @throws Exception
	// */
	// public String findUserGroupUser(String myUserId, String userId)
	// throws Exception {
	// long count = userDAO.getUserGroupCount();
	// List<UserGroup> roleList = userDAO.getUserCommonGroupList();
	//
	// List<UserGroup> userRoleList = userDAO.getUserGroupByUser(userId);
	// StringBuffer str = new StringBuffer("");
	// for (UserGroup aRole : roleList) {
	//
	// str.append("{'select':'")
	// .append("<input type=\"checkbox\" name=\"select\"
	// style=\"height:15;width:15\" ")
	// .append(userRoleList.contains(aRole) ? "checked" : "")
	// .append(" id=\"select\" value=\"" + aRole.getId() + "\" />");
	// str.append("','id':'").append(aRole.getId()).append("'");
	// str.append(",'name':'").append(aRole.getName()).append("'");
	// str.append(",'note':'")
	// .append(aRole.getNote() == null ? "" : aRole.getNote())
	// .append("'}");
	// str.append(",");
	// }
	// String jsonDate = str.toString();
	// return ("{'total':" + count + ",'results':[" + jsonDate + "]}")
	// .replaceAll(",]", "]");
	// }

	/**
	 * 用户岗位
	 * 
	 * @param myUserId
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public String findUserJobUser(String myUserId, String userId) throws Exception {
		long count = userDAO.getUserJobCount();
		List<DicJob> jobList = userDAO.getDicJobList();

		List<DicJob> userJobList = userDAO.getUserJobByUser(userId);
		StringBuffer str = new StringBuffer("");
		for (DicJob aJob : jobList) {

			str.append("{'select':'").append("<input type=\"checkbox\" name=\"select\" style=\"height:15;width:15\" ")
					.append(userJobList.contains(aJob) ? "checked" : "")
					.append(" id=\"select\" value=\"" + aJob.getId() + "\" />");
			str.append("','id':'").append(aJob.getId()).append("'");
			str.append(",'name':'").append(aJob.getName()).append("'");
			str.append(",'note':'").append(aJob.getNote() == null ? "" : aJob.getNote()).append("'}");
			str.append(",");
		}
		String jsonDate = str.toString();
		return ("{'total':" + count + ",'results':[" + jsonDate + "]}").replaceAll(",]", "]");
	}

	public List<User> getDepartmentLeader(String id) throws Exception {
		String[] a = { id, SystemConstants.DIC_STATE_YES };
		List<User> leadUser = userDAO.find("select a from User a where a.department.id = ? and a.dicJob.isLeader =?",
				a);
		return leadUser;

	}

	public List<User> getDepartmentLeaderByDepartment(String id) throws Exception {
		String[] a = { id, SystemConstants.DIC_STATE_YES };
		List<User> leadUser = userDAO
				.find("select a.user from UserJobUser a where a.job.department.id = ?  and a.job.isLeader =?", a);

		List<User> leadUserByOwnDepartment = userDAO.find(
				"select a.user from UserJobUser a where a.user.department.id = ? and (a.job.department is null or a.job.department='') and a.job.isLeader =?",
				a);

		for (User aUser : leadUserByOwnDepartment) {

			if (!leadUser.contains(aUser)) {
				leadUser.add(aUser);
			}

		}

		return leadUser;

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void setRole(String data, String id) throws Exception {
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(data, List.class);
		List<UserRole> urList = userDAO.getUserRole(id);
		User u = userDAO.get(User.class, id);
		userDAO.deleteAll(urList);
		for (Map<String, Object> map : list) {
			Role r = new Role();
			r = (Role) userDAO.Map2Bean(map, r);
			if (r.getIsCheck().equals("1")) {
				UserRole ur = new UserRole();
				ur.setRole(r);
				ur.setUser(u);
				userDAO.save(ur);
			}

		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void setUserGroup(User ui) throws Exception {

		List<UserGroup> source = userDAO.getUserGroupList();

		List<UserGroup> aid = userDAO.getUserGroupUserListByUser(ui.getId());

		// 相同的
		List<UserGroup> thesames = new ArrayList<UserGroup>();

		// 计算相同的和增加的
		for (Iterator<UserGroup> ss = source.iterator(); ss.hasNext();) {
			UserGroup sri = ss.next();
			boolean flag = false;
			for (Iterator<UserGroup> ts = aid.iterator(); ts.hasNext();) {
				UserGroup tri = ts.next();
				if (sri.getId().equals(tri.getId())) {
					thesames.add(tri);
					flag = true;
					break;
				}
			}
			if (!flag) {
				for (String id : ui.getUserGroups().split(",")) {
					if (id.equals(sri.getId())) {
						UserGroupUser ur = new UserGroupUser();
						UserGroup role = new UserGroup();
						role.setId(id);
						ur.setUserGroup(role);
						ur.setUser(ui);
						userDAO.save(ur);

					}
				}
			}
		}
		// 计算减少的
		for (Iterator<UserGroup> sames = aid.iterator(); sames.hasNext();) {
			UserGroup sri = sames.next();
			boolean flag = false;
			for (String id : ui.getUserGroups().split(",")) {
				if (id.equals(sri.getId())) {
					flag = true;
					break;
				}
			}
			if (!flag) {
				userDAO.deleteUserGroupUserByUser(ui.getId(), sri.getId());
			}
		}

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void setUserJob(User ui) throws Exception {

		List<DicJob> source = userDAO.getDicJobList();

		List<DicJob> aid = userDAO.getUserJobByUser(ui.getId());

		// 相同的
		List<DicJob> thesames = new ArrayList<DicJob>();

		// 计算相同的和增加的
		for (Iterator<DicJob> ss = source.iterator(); ss.hasNext();) {
			DicJob sri = ss.next();
			boolean flag = false;
			for (Iterator<DicJob> ts = aid.iterator(); ts.hasNext();) {
				DicJob tri = ts.next();
				if (sri.getId().equals(tri.getId())) {
					thesames.add(tri);
					flag = true;
					break;
				}
			}
			if (!flag) {
				for (String id : ui.getUserJobs().split(",")) {
					if (id.equals(sri.getId())) {
						UserJobUser ur = new UserJobUser();
						DicJob job = new DicJob();
						job.setId(id);
						ur.setUser(ui);
						userDAO.save(ur);

					}
				}
			}
		}
		// 计算减少的
		for (Iterator<DicJob> sames = aid.iterator(); sames.hasNext();) {
			DicJob sri = sames.next();
			boolean flag = false;
			for (String id : ui.getUserJobs().split(",")) {
				if (id.equals(sri.getId())) {
					flag = true;
					break;
				}
			}
			if (!flag) {
				userDAO.deleteUserJobUserByUser(ui.getId(), sri.getId());
			}
		}

	}

	/**
	 * 
	 * 检索用户认证信息
	 * 
	 * 
	 * @return Map
	 */
	public Map<String, Object> findUserCert(int startNum, int limitNum, String dir, String sort, String contextPath,
			String userId) throws Exception {
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		Map<String, String> mapForCondition = new HashMap<String, String>();
		if (userId != null && !userId.equals("")) {
			mapForQuery.put("user.id", userId);
			mapForCondition.put("user.id", "=");
		}
		Map<String, Object> controlMap = userDAO.findObjectCondition(startNum, limitNum, dir, sort, UserCert.class,
				mapForQuery, mapForCondition);

		List<UserCert> list = (List<UserCert>) controlMap.get("list");
		controlMap.put("list", list);
		return controlMap;
	}

	/**
	 * 
	 * 检索用户认证信息
	 * 
	 * 
	 * @return Map
	 */
	public Map<String, Object> findUserVaccine(int startNum, int limitNum, String dir, String sort, String contextPath,
			String userId) throws Exception {

		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		Map<String, String> mapForCondition = new HashMap<String, String>();
		if (userId != null && !userId.equals("")) {
			mapForQuery.put("user.id", userId);
			mapForCondition.put("user.id", "=");
		}
		Map<String, Object> controlMap = userDAO.findObjectCondition(startNum, limitNum, dir, sort, UserVaccine.class,
				mapForQuery, mapForCondition);

		List<UserCert> list = (List<UserCert>) controlMap.get("list");
		controlMap.put("list", list);
		return controlMap;
	}

	/**
	 * 
	 * 用主键查询一个用户
	 * 
	 * 
	 */
	public User findUser(String id) {
		User user = userDAO.get(User.class, id);
		return user;

	}

	public List<DicCostCenter> findDicCostCenter() throws Exception {
		List<DicCostCenter> user = userDAO.getDicCostCenterList();
		return user;

	}

	/**
	 * 
	 * 增加或更改用户
	 * 
	 * @param aMap
	 * 
	 * 
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void addUser(User user, Map aMap, String changeLog) throws Exception {

		if (user.getPassword() != null && !user.getPassword().equals("")) {
			user.setUserPassword(MD5Util.getMD5Lower(user.getPassword()));
		}
		userDAO.saveOrUpdate(user);

		String jsonStr = "";
		jsonStr = (String) aMap.get("userGroupJson");
		if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
			setUserGroup(jsonStr, user.getId());
		}
		jsonStr = (String) aMap.get("userRoleJson");
		if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
			setRole(jsonStr, user.getId());
		}

		if (changeLog != null && !"".equals(changeLog)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			if (u != null) {
				li.setUserId(u.getId());
			}
			li.setFileId(user.getId());
			li.setClassName("User");
			li.setModifyContent(changeLog);
			li.setState("3");
			li.setStateName("数据修改");
			commonDAO.saveOrUpdate(li);
		}

	}

	/**
	 * 
	 * 增加用户认证,由ext ajax调用
	 * 
	 * @param userId     用户id
	 * @param jsonString 用户认证jsonString
	 * @return void
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void addUserCert(String userId, String jsonString) throws Exception {

		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(jsonString, List.class);
		for (Map<String, Object> map : list) {
			UserCert uct = new UserCert();
			uct = (UserCert) userDAO.Map2Bean(map, uct);
			User user = new User();
			user.setId(userId);
			uct.setUser(user);
			userDAO.saveOrUpdate(uct);

		}

	}

	/**
	 * 
	 * 删除用户认证,由ext ajax调用
	 * 
	 * @param id 认证ID
	 * @return void
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delUserCert(String id) {
		UserCert uct = userDAO.get(UserCert.class, id);
		userDAO.delete(uct);
	}

	/**
	 * 
	 * 维护用户接种信息,由ext ajax调用
	 * 
	 * @param userId     用户id
	 * @param jsonString 用户认证jsonString
	 * @return void
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveUserVaccine(String userId, String jsonString) throws Exception {

		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(jsonString, List.class);
		for (Map<String, Object> map : list) {
			UserVaccine uct = new UserVaccine();
			uct = (UserVaccine) userDAO.Map2Bean(map, uct);
			User user = new User();
			user.setId(userId);
			uct.setUser(user);
			userDAO.saveOrUpdate(uct);
		}

	}

	/**
	 * 
	 * 删除用户接种信息,由ext ajax调用
	 * 
	 * @param id 认证ID
	 * @return void
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delUserVaccine(String id) throws Exception {
		UserVaccine uct = userDAO.get(UserVaccine.class, id);
		userDAO.delete(uct);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delUser(User user) throws Exception {

		userDAO.saveOrUpdate(user);

	}

	public String hasUserId(String userId) throws Exception {
		Map<String, String> messageMap = new HashMap<String, String>();
		Object[] a = { userId };
		long count = userDAO.getCount("from User where id =?", a);
		if (count > 0)
			messageMap.put("message", MsgLocale.getLocale("user.check.id.no"));
		else
			messageMap.put("message", MsgLocale.getLocale("user.check.id.ok"));
		return JsonUtils.toJsonString(messageMap);
	}

	@Transactional(rollbackFor = Exception.class)
	public User modifyUser(User modifyUser, User user) {
		String pwd = modifyUser.getUserPassword();
		if (pwd != null && pwd.length() > 0) {
			user.setUserPassword(pwd);
		}
		user.setMobile(modifyUser.getMobile());
		user.setAddress(modifyUser.getAddress());
		this.userDAO.update(user);
		return user;
	}

	/**
	 * 
	 * 检索,采用map方式传递检索参数
	 * 
	 * 
	 * @return 参数值MAP，如果传递的为null返回空字符串
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> findUserList(int startNum, int limitNum, String dir, String sort, String contextPath,
			String data) throws Exception {
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		Map<String, String> mapForCondition = new HashMap<String, String>();

		if (data != null && !data.equals(""))
			BeanUtils.data2Map(data, mapForQuery, mapForCondition);
		mapForCondition.put("id", "like");
		mapForCondition.put("name", "like");
		mapForCondition.put("state.id", "plike");
		mapForQuery.put("state.id", SystemConstants.DIC_STATE_YES);
		// 将map值传入，获取列表
		Map<String, Object> controlMap = userDAO.findObjectCondition(startNum, limitNum, dir, sort, User.class,
				mapForQuery, mapForCondition);
		List<User> list = (List<User>) controlMap.get("list");

		controlMap.put("list", list);
		return controlMap;
	}

	public List<DicCostCenter> findDicCostCenterList() throws Exception {
		return userDAO.getDicCostCenterList();
	}

	// public Map<String, Object> showUserTable(Integer start, Integer length,
	// String query, String col, String sort,
	// String groupId) throws Exception {
	// return userDAO.showUserTable(start, length, queryMap, col, sort,groupId);
	// }

	public Map<String, Object> selectUserTableJson(Integer start, Integer length, String query, String col, String sort,
			String groupId) throws Exception {
		return userDAO.selectUserTableJson(start, length, query, col, sort, groupId);
	}
	//生产计划查
	public Map<String, Object> selectProductUserTableJson(Integer start, Integer length, String query, String col, String sort,
			String groupId,String planWorkDate) throws Exception {
		return userDAO.selectProductUserTableJson(start, length, query, col, sort, groupId,planWorkDate);
	}

	public Map<String, Object> selectUserTableNewJson(Integer start, Integer length, String query, String col,
			String sort, String groupId) throws Exception {
		return userDAO.selectUserTableNewJson(start, length, query, col, sort, groupId);
	}
	//生产计划选择操作人
	public Map<String, Object> selectProductUserTableNewJson(Integer start, Integer length, String query, String col,
			String sort, String groupId) throws Exception {
		return userDAO.selectProductUserTableNewJson(start, length, query, col, sort, groupId);
	}

	public Map<String, Object> userRoleShowJson(String userId, Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
//		List<Role> list = userDAO.userRoleShowJson();
		Map<String, Object> userRoleShowJsonNew = userDAO.userRoleShowJsonNew(start,length,col,sort);
		List<Role> list =(List<Role>)userRoleShowJsonNew.get("list");
		List<UserRole> urList = userDAO.getUserRole(userId);
		for (Role r : list) {
			String isCheck = "0";
			for (UserRole ur : urList) {
				if (ur.getRole().getId().equals(r.getId())) {
					isCheck = "1";
				}
			}
			r.setIsCheck(isCheck);
		}
		map.put("data", JsonUtils.toJsonString(list));
		map.put("recordsTotal", userRoleShowJsonNew.get("recordsTotal"));
		map.put("recordsFiltered", userRoleShowJsonNew.get("recordsFiltered"));
		return map;
	}

	public Map<String, Object> showUserGroupUserListJson(String userId, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
//		List<UserGroup> list = userDAO.showUserGroupUserListJson();
		Map<String, Object> showUserGroupUserListJsonNew = userDAO.showUserGroupUserListJsonNew(start,length,col,sort);
		List<UserGroup> list =(List<UserGroup>)showUserGroupUserListJsonNew.get("list");
		List<UserGroupUser> urList = userDAO.getUserGroup(userId);
		for (UserGroup r : list) {
			String isCheck = "0";
			for (UserGroupUser ur : urList) {
				if (ur.getUserGroup().getId().equals(r.getId())) {
					isCheck = "1";
				}
			}
			r.setIsCheck(isCheck);
		}
		map.put("data", JsonUtils.toJsonString(list));
		map.put("recordsTotal", showUserGroupUserListJsonNew.get("recordsTotal"));
		map.put("recordsFiltered", showUserGroupUserListJsonNew.get("recordsFiltered"));
		return map;
	}

	/**
	 * 
	 * @Title: setUserGroup @Description: 配置人员组 @author : shengwei.wang @date
	 * 2018年4月8日下午6:17:09 @param data @param id @throws Exception void @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void setUserGroup(String data, String id) throws Exception {

		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(data, List.class);
		List<UserGroupUser> urList = userDAO.getUserGroup(id);
		User u = userDAO.get(User.class, id);
		userDAO.deleteAll(urList);
		for (Map<String, Object> map : list) {
			UserGroup r = new UserGroup();
			r = (UserGroup) userDAO.Map2Bean(map, r);
			if (r.getIsCheck().equals("1")) {
				UserGroupUser ur = new UserGroupUser();
				ur.setUserGroup(r);
				ur.setUser(u);
				userDAO.save(ur);
			}

		}

	}

	public Map<String, Object> selectUsersTableJson(Integer start, Integer length, String query, String col,
			String sort, String groupId) throws Exception {
		return userDAO.selectUsersTableJson(start, length, query, col, sort, groupId);
	}

	public List<DicCostCenter> showCostCenterJson() {
		return userDAO.showCostCenterJson();
	}

	public Map<String, Object> showUserHealthTableListJson(String uid, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return userDAO.showUserHealthTableListJson(uid, start, length, query, col, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveUserHealthItemTable(String id, String changeLog, String itemJson) throws Exception {
		User user = commonDAO.get(User.class, id);
		List<UserJobUser> saveItems = new ArrayList<UserJobUser>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemJson, List.class);
		for (Map<String, Object> map : list) {
			UserJobUser uju = new UserJobUser();
			uju = (UserJobUser) commonDAO.Map2Bean(map, uju);
			if (uju.getId() != null && uju.getId().equals("")) {
				uju.setId(null);
			}
			uju.setUser(user);
			saveItems.add(uju);
		}
		commonDAO.saveOrUpdateAll(saveItems);
		if (changeLog != null && !"".equals(changeLog)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			if (u != null) {
				li.setUserId(u.getId());
			}
			li.setFileId(id);
			li.setClassName("User");
			li.setState("3");
			li.setStateName("数据修改");
			li.setModifyContent(changeLog);
			commonDAO.saveOrUpdate(li);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delUserHealthItem(String[] ids) {
		for (String id : ids) {
			UserJobUser uju = commonDAO.get(UserJobUser.class, id);
			commonDAO.delete(uju);
		}
	}

	public Map<String, Object> showUserCertTableListJson(String uid, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return userDAO.showUserCertTableListJson(uid, start, length, query, col, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveUserCertItemTable(String id, String changeLog, String itemJson) throws Exception {
		User user = commonDAO.get(User.class, id);
		List<UserCert> saveItems = new ArrayList<UserCert>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemJson, List.class);
		for (Map<String, Object> map : list) {
			UserCert uju = new UserCert();
			uju = (UserCert) commonDAO.Map2Bean(map, uju);
			if (uju.getId() != null && uju.getId().equals("")) {
				uju.setId(null);
			}
			uju.setUser(user);
			saveItems.add(uju);
		}
		commonDAO.saveOrUpdateAll(saveItems);
		if (changeLog != null && !"".equals(changeLog)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			if (u != null) {
				li.setUserId(u.getId());
			}
			li.setFileId(id);
			li.setClassName("User");
			li.setModifyContent(changeLog);
			li.setState("3");
			li.setStateName("数据修改");
			commonDAO.saveOrUpdate(li);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delUserCertItem(String[] ids) {
		for (String id : ids) {
			UserCert uju = commonDAO.get(UserCert.class, id);
			commonDAO.delete(uju);
		}
	}

}
