/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：DepartmentAction
 * 类描述：用户管理
 * 创建人：倪毅
 * 创建时间：2011-10-20
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.core.department.action;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.model.user.User;
import com.biolims.core.constants.SystemConstants;
import com.biolims.core.department.service.DepartmentService;
import com.biolims.core.model.user.Department;
import com.biolims.core.user.service.UserService;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.dic.model.DicType;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.system.product.model.Product;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.opensymphony.xwork2.ActionContext;

@Namespace("/core/department")
@Controller
@Scope("prototype")
@ParentPackage("default")
public class DepartmentAction extends BaseActionSupport {
	private static final long serialVersionUID = -164661537469123056L;
	@Autowired
	private UserService userService;
	@Resource
	private FieldService fieldService;

	//该action权限id
	private String rightsId = "110202";//"1102";

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	@Autowired
	private DepartmentService departmentService;
	// 用于页面上显示模块名称
	private String title = "组织机构管理";
	//状态map
	private HashMap stateMap = new LinkedHashMap();
	//组织机构
	private Department department = new Department();
	//部门类型
	private List<DicType> listDepartmentType;//部门类型

	public HashMap getStateMap() {
		return stateMap;
	}

	public void setStateMap(HashMap stateMap) {
		this.stateMap = stateMap;
	}

	public List<DicType> getListDepartmentType() {
		return listDepartmentType;
	}

	public void setListDepartmentType(List<DicType> listDepartmentType) {
		this.listDepartmentType = listDepartmentType;
	}

	public UserService getUserService() {
		return userService;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public DepartmentService getDepartmentService() {
		return departmentService;
	}

	public void setDepartmentService(DepartmentService departmentService) {
		this.departmentService = departmentService;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 访问 组织机构列表
	 */
	@Action(value = "showDepartmentList")
	public String showDepartmentList() throws Exception {
		rightsId="110202";
		ServletActionContext.getRequest().getSession().setAttribute("department", department);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/core/department/showDepartmentList.jsp");
	}
	
	@Action(value = "showProductNewListJson")
	public void showProductNewListJson() throws Exception {
		String query = getParameterFromRequest("query");
		try {
			List<Department> list = departmentService.showDepartmentSelTreeNewJson(query);
			HttpUtils.write(JsonUtils.toJsonString(list));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Action(value = "showProductNewNewListJson")
	public void showProductNewNewListJson() throws Exception {
		String query = getParameterFromRequest("query");
		try {
			List<Department> list = departmentService.showDepartmentSelTreeNewNewJson(query);
			HttpUtils.write(JsonUtils.toJsonString(list));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "departmentShowJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void departmentShowJson() throws Exception {

		List<Department> aList = departmentService.findDepartment();
		String a = departmentService.getJson(aList);

		new SendData().sendDataJson(a, ServletActionContext.getResponse());
	}

	/**
	 * 访问添加组织机构页面
	 */

	@Action(value = "toAddDepartment")
	public String toAddDepartment() throws Exception {
		rightsId="110201";
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			Department upDepartment = departmentService.findDepartment(id);
			department.setUpDepartment(upDepartment);
		}
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);

		return dispatcher("/WEB-INF/page/core/department/editDepartment.jsp");
	}

	/**
	 * 访问修改页面
	 */
	@Action(value = "toModifyDepartment")
	public String toModifyDepartment() throws Exception {
		rightsId="110201";
		String id = getParameterFromRequest("id");
		putObjToContext("stateMap", stateMap);
		long childCount = departmentService.findDownDepartmentCount(id);
		department = departmentService.findDepartment(id);
		if (childCount == 0)
			department.setLeaf("true");
		else
			department.setLeaf("false");
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		putObjToContext("departmentId", id);
		return dispatcher("/WEB-INF/page/core/department/editDepartment.jsp");
	}

	/**
	 * 访问查看页面
	 */
	@Action(value = "toViewDepartments")
	public String toViewDepartments() throws Exception {
		rightsId="110201";
		String id = getParameterFromRequest("id");
		putObjToContext("stateMap", stateMap);
		long childCount = departmentService.findDownDepartmentCount(id);
		department = departmentService.findDepartment(id);
		if (childCount == 0)
			department.setLeaf("true");
		else
			department.setLeaf("false");
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		putObjToContext("departmentId", id);
		return dispatcher("/WEB-INF/page/core/department/editDepartment.jsp");
	}
	
	/**
	 * 保存组织机构
	 */
	@Action(value = "save")
	public String save() throws Exception {
		String id = this.department.getId();
		String changeLog = getParameterFromRequest("changeLog");
		if(!"".equals(id)&&id != null){
			department.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			department.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
		}
		departmentService.save(department,changeLog);
		//具体操作，如删除，填加动作，应用redirect
		return redirect("/core/department/toModifyDepartment.action?id=" + department.getId());
	}

	/**
	 * 访问 查看页面
	 */
	@Action(value = "toViewDepartment")
	public String toViewDepartment() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/core/department/departmentAdd.jsp");
	}

	@Action(value = "showDepartmentUserList")
	public String showRoleUserList() throws Exception {

		String departmentId = getParameterFromRequest("departmentId");

		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		//  属性map<字段名称, ext-list属性数组说明,值为空""则没有该属性>
		//  id type format header width  sort hide align xtype  editable renderer editor
		//  列的id 当"common" 时该列为扩展列  type类型 format格式 header列头 width列宽 sort是否排序  hide是否隐藏 align左右格式 xtype ediable是否可编辑 renderer渲染 editor编辑器 
		map.put("id", new String[] { "", "string", "", "用户账号", "150", "true", "", "", "", "", "", "" });
		map.put("name", new String[] { "", "string", "", "姓名", "150", "true", "", "", "", "", "", "" });
		map.put("department-name", new String[] { "", "string", "", "部门", "150", "true", "", "", "", "", "", "" });
		map.put("dicJob-name", new String[] { "common", "string", "", "岗位", "150", "true", "", "", "", "", "", "" });
		String type = generalexttype(map);
		String col = generalextcol(map);
		// 用于ext显示
		putObjToContext("departmentId", departmentId);
		putObjToContext("type", type);
		putObjToContext("col", col);
		// 用于判断当前页面类型,LIST类型
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		// 用于调用json时的action链接
		putObjToContext("path", ServletActionContext.getRequest().getContextPath()
				+ "/core/department/showDepartmentUserListJson.action?departmentId=" + departmentId);
		//导向jsp页面显示
		return dispatcher("/WEB-INF/page/core/department/showDepartmentUserList.jsp");
	}

//	@SuppressWarnings("unchecked")
//	@Action(value = "showDepartmentUserListJson")
//	public void showRoleUserListJson() throws Exception {
//		//开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		//limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		//字段
//		String dir = getParameterFromRequest("dir");
//		//排序方式
//		String sort = getParameterFromRequest("sort");
//		String departmentId = getParameterFromRequest("departmentId");
//
//		//取出session中检索用的对象
//		Map<String, Object> controlMap = departmentService.findDepartmentUserList(startNum, limitNum, dir, sort,
//				getContextPath(), departmentId);
//		long totalCount = Long.parseLong(controlMap.get("totalCount").toString());
//		List<User> list = (List<User>) controlMap.get("list");
//		Map<String, String> map = new HashMap<String, String>();
//		map.put("id", "");
//		map.put("name", "name");
//		map.put("department-name", "");
//		map.put("dicJob-name", "");
//		new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
//	}
	
	
	@SuppressWarnings("unchecked")
	@Action(value = "showDepartmentUserListJson")
	public void showRoleUserListJson() throws Exception {
		String id= getParameterFromRequest("id");
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {	
			Map<String, Object> result = departmentService.findDepartmentUserList(
					start, length, query, col, sort, id);
			Long count = (Long) result.get("total");
			List<User> list = (List<User>) result.get("list");
	
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "name");
			map.put("department-name", "");
			map.put("dicJob-name", "");			
			
			String data=new SendData().getDateJsonForDatatable(map, list);
			//根据模块查询自定义字段数据
			Map<String,Object> mapField = fieldService.findFieldByModuleValue("User");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result,dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	

	@Action(value = "departmentSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String departmentShow() throws Exception {

		return dispatcher("/WEB-INF/page/core/department/departmentSelect.jsp");
	}

}
