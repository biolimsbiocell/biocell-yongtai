package com.biolims.core.department.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.core.model.user.Department;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.dao.BaseHibernateDao;
import com.opensymphony.xwork2.ActionContext;
@Repository
@SuppressWarnings("unchecked")
public class DepartmentDao extends BaseHibernateDao{

	public List<Department> showDepartmentSelTreeJson(String query) throws Exception {
		String key = " ";
		String hql = " from Department where 1=1";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
//		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
//		if(!"all".equals(scopeId)){
//			key+=" and scopeId='"+scopeId+"'";
//		}
		List<Department> list = this.getSession().createQuery(hql + key).list();
		return list;
	}
	
	public List<Department> showDepartmentSelTreeNewJson(String query) throws Exception {
		String key = " ";
		String hql = " from Department where 1=1";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		List<Department> list = this.getSession().createQuery(hql + key).list();
		return list;
	}
	public List<Department> showDepartmentSelTreeNewNewJson(String query) throws Exception {
		String key = " ";
		String hql = " from Department where 1=1 and state='1' and upDepartment is null ";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		List<Department> list = this.getSession().createQuery(hql + key).list();
		return list;
	}

	public Map<String, Object> selectDepartmentUserList(Integer start,
			Integer length, String query, String col, String sort, String id) {  
		Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from User where 1=1 and department.id='"+id+"'";
			String key = "";
//			if(queryMap!=null){
//				key=map2Where(queryMap);
//			}
			Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
			if(0l!= sumCount){
				Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
				String hql = "from User where 1=1 and department.id='"+id+"'";
				List<CrmPatient> list = new ArrayList<CrmPatient>();
				list=this.getSession().createQuery(hql + key)
						.setFirstResult(start).setMaxResults(length)
						.list();
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
			}
			return map;
	}

}
