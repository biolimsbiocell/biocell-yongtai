/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：Service
 * 类描述：组织机构管理service
 * 创建人：倪毅
 * 创建时间：2011-9
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.core.department.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.core.constants.SystemConstants;
import com.biolims.core.department.dao.DepartmentDao;
import com.biolims.core.model.user.Department;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.model.LogInfo;
import com.biolims.system.product.dao.ProductDao;
import com.biolims.system.product.model.Product;
import com.biolims.util.JsonUtils;
import com.biolims.util.MsgLocale;

@Service
@SuppressWarnings("unchecked")
public class DepartmentService {
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private DepartmentDao departmentDao;

	/**
	 * 
	 *  查询组织机构列表，带检索条件
	 * 
	 * 
	 */
	public List<Department> findDepartment() throws Exception {

		String whereStr = "";
		Object[] a = {};

		List<Department> list = commonDAO.find("from Department  where state='" + SystemConstants.DIC_TYPE_IN_USE
				+ "' " + whereStr + " order by orderNumber asc", a);
		return list;

	}

	public Map<String, Object> findDepartmentUserList(int startNum, int limitNum, String dir, String sort,
			String contextPath, String departmentId) throws Exception {
		//检索条件map,每个字段检索值
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		//检索方式map,每个字段的检索方式
		Map<String, String> mapForCondition = new HashMap<String, String>();

		//将map值传入，获取列表

		if (departmentId != null && !departmentId.equals("")) {
			mapForQuery.put("department.id", departmentId);
			mapForCondition.put("department.id", "=");
		}

		Map<String, Object> controlMap = commonDAO.findObjectCondition(startNum, limitNum, dir, sort, User.class,
				mapForQuery, mapForCondition);
		List<User> list = (List<User>) controlMap.get("list");

		controlMap.put("list", list);
		return controlMap;
	}

	/**
	 * 
	 *  保存组织机构
	 * 
	 * 
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(Department department,String logInfo) throws Exception {
		int level = 0;
		String levelId = "";
		if (department.getUpDepartment() != null && department.getUpDepartment().getId() != null
				&& !department.getUpDepartment().getId().equals("")) {
			Department up = commonDAO.get(Department.class, department.getUpDepartment().getId());
			level = Integer.valueOf(up.getLevel());
			level += 1;
			levelId = up.getLevelId() + department.getId() + ".";
		} else {
			level = 0;
			levelId = department.getId() + ".";

		}
		department.setLevel(level);
		department.setLevelId(levelId);
		if (department.getType() != null && department.getType().getId().equals(""))
			department.setType(null);
		commonDAO.saveOrUpdate(department);
		
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
			.getRequest()
			.getSession()
			.getAttribute(
					SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(department.getId());
			li.setClassName("Department");
			li.setModifyContent(logInfo);
			li.setState("1");
			li.setStateName("数据新增");
			commonDAO.saveOrUpdate(li);
		}

	}

	/**
	 * 
	 *  查询是否有下级组织机构
	 * 
	 * 
	 */
	public long findDownDepartmentCount(String id) throws Exception {
		Object[] a = { id, SystemConstants.DIC_TYPE_IN_USE };
		long num = commonDAO.getCount("from Department where upDepartment.id=? and state=?", a);
		return num;

	}

	StringBuffer json = new StringBuffer();

	/**
	 * 
	 * 用主键查询一个组织机构
	 * 
	 * 
	 */
	public Department findDepartment(String id) {
		Department department = commonDAO.get(Department.class, id);
		return department;

	}

	/**
	 * 构建Json文件
	 * @param list
	 * @param node
	 */

	public void constructorJson(List<Department> list, Department treeNode) {
		if (hasChild(list, treeNode)) {
			json.append("{\"id\":'");
			json.append(treeNode.getId() + "'");
			json.append(",\"name\":'");
			json.append(JsonUtils.formatStr(treeNode.getName()) + "'");
			json.append(",\"leaf\":false");
			json.append(",\"briefName\":'");
			json.append(treeNode.getBriefName() + "'");
			json.append(",\"type\":''");

			json.append(",\"state\":'");
			if (treeNode.getState() != null) {
				if (treeNode.getState().equals(SystemConstants.DIC_TYPE_IN_USE))
					json.append(MsgLocale.getLocale("DIC_TYPE_IN_USE") + "'");
				if (treeNode.getState().equals(SystemConstants.DIC_TYPE_NOT_USE))
					json.append(MsgLocale.getLocale("DIC_TYPE_NOT_USE") + "'");
			}
			json.append(",\"address\":'");
			json.append(JsonUtils.formatStr(treeNode.getAddress() == null ? "" : treeNode.getAddress()) + "'");
			json.append(",\"note\":'");
			json.append(JsonUtils.formatStr(treeNode.getNote() == null ? "" : treeNode.getNote()) + "'");

			json.append(",\"children\":[");
			List<Department> childList = getChildList(list, treeNode);
			Iterator<Department> iterator = childList.iterator();
			while (iterator.hasNext()) {
				Department node = (Department) iterator.next();
				constructorJson(list, node);
			}
			json.append("]},");
		} else {
			json.append("{\"id\":'");
			json.append(treeNode.getId() + "'");
			json.append(",\"name\":'");
			json.append(JsonUtils.formatStr(treeNode.getName()) + "'");
			json.append(",\"leaf\":true");
			json.append(",\"briefName\":'");
			json.append(treeNode.getBriefName() + "'");
			json.append(",\"type\":''");

			json.append(",\"state\":'");
			if (treeNode.getState() != null) {
				if (treeNode.getState().equals(SystemConstants.DIC_TYPE_IN_USE))
					json.append(MsgLocale.getLocale("DIC_TYPE_IN_USE") + "'");
				if (treeNode.getState().equals(SystemConstants.DIC_TYPE_NOT_USE))
					json.append(MsgLocale.getLocale("DIC_TYPE_NOT_USE") + "'");
			}
			json.append(",\"address\":'");
			json.append(JsonUtils.formatStr(treeNode.getAddress() == null ? "" : treeNode.getAddress()) + "'");
			json.append(",\"note\":'");
			json.append(JsonUtils.formatStr(treeNode.getNote() == null ? "" : treeNode.getNote()) + " '");

			json.append("},");

		}

	}

	/**
	* 获得子节点列表信息
	* @param list
	* @param node
	* @return
	*/
	public List<Department> getChildList(List<Department> list, Department node) { //得到子节点列表   
		List<Department> li = new ArrayList<Department>();
		Iterator<Department> it = list.iterator();
		while (it.hasNext()) {
			Department n = (Department) it.next();
			if (n.getUpDepartment() != null && n.getUpDepartment().getId().equals(node.getId())) {
				li.add(n);
			}
		}
		return li;
	}

	/**
	 * 判断是否有子节点
	 * @param list
	 * @param node
	 * @return
	 */
	public boolean hasChild(List<Department> list, Department node) {
		return getChildList(list, node).size() > 0 ? true : false;
	}

	public String getJson(List<Department> list) {
		json = new StringBuffer();
		List<Department> nodeList0 = new ArrayList<Department>();
		// commonDAO.find("from Department where level='0'"); 
		Iterator<Department> it1 = list.iterator();
		while (it1.hasNext()) {
			Department node = (Department) it1.next();
			if (node.getLevel() == 0) {
				nodeList0.add(node);
			}

		}
		Iterator<Department> it = nodeList0.iterator();
		while (it.hasNext()) {
			Department node = (Department) it.next();
			constructorJson(list, node);

		}

		String jsonDate = json.toString();
		return ("[" + jsonDate + "]").replaceAll(",]", "]");

	}
	
	/**
	 * 
	 * @Title: showDepartmentSelTreeJson
	 * @Description: 展示组织机构
	 * @param query
	 * @return
	 * @throws Exception
	 *             List<Department>
	 * @throws
	 */
	public List<Department> showDepartmentSelTreeJson(String query) throws Exception {
		List<Department> result=new ArrayList<Department>();
		List<Department> pList = departmentDao.showDepartmentSelTreeJson(query);
		List<Department> alist=new ArrayList<Department>();
		result.addAll(pList);
		List<String> strList = new ArrayList<String>();
		for (Department pd : pList) {
			strList.add(pd.getId());
		}
		for (Department p : pList) {
			if (p.getParent() != null&&!"".equals(p.getParent())) {
				List<Department> list = findDepartmentParent(alist, p.getParent());
				for (Department pds : list) {
					if (!strList.contains(pds.getId())) {
						result.add(pds);
						strList.add(pds.getId());
					}
				}
			}
		}
		return result;
	}
	
	
	/**
	 * 
	 * @Title: showDepartmentSelTreeNewJson
	 * @Description: 展示组织机构
	 * @param query
	 * @return
	 * @throws Exception
	 *             List<Department>
	 * @throws
	 */
	public List<Department> showDepartmentSelTreeNewJson(String query) throws Exception {
		List<Department> result=new ArrayList<Department>();
		List<Department> pList = departmentDao.showDepartmentSelTreeNewJson(query);
		List<Department> alist=new ArrayList<Department>();
		result.addAll(pList);
		List<String> strList = new ArrayList<String>();
		for (Department pd : pList) {
			strList.add(pd.getId());
		}
		for (Department p : pList) {
			if (p.getParent() != null&&!"".equals(p.getParent())) {
				List<Department> list = findDepartmentParent(alist, p.getParent());
				for (Department pds : list) {
					if (!strList.contains(pds.getId())) {
						result.add(pds);
						strList.add(pds.getId());
					}
				}
			}
		}
		return result;
	}
	public List<Department> showDepartmentSelTreeNewNewJson(String query) throws Exception {
		List<Department> result=new ArrayList<Department>();
		List<Department> pList = departmentDao.showDepartmentSelTreeNewNewJson(query);
		List<Department> alist=new ArrayList<Department>();
		result.addAll(pList);
		List<String> strList = new ArrayList<String>();
		for (Department pd : pList) {
			strList.add(pd.getId());
		}
		for (Department p : pList) {
			if (p.getParent() != null&&!"".equals(p.getParent())) {
				List<Department> list = findDepartmentParent(alist, p.getParent());
				for (Department pds : list) {
					if (!strList.contains(pds.getId())) {
						result.add(pds);
						strList.add(pds.getId());
					}
				}
			}
		}
		return result;
	}
	
	public List<Department> findDepartmentParent(List<Department> list, String id) {
		Department p = departmentDao.get(Department.class, id);
		if (p.getParent() != null&&!"".equals(p.getParent())) {
			list.add(p);
			findDepartmentParent(list, p.getParent());
		} else {
			list.add(p);
		}
		return list;
	}

	public Map<String, Object> findDepartmentUserList(Integer start,
			Integer length, String query, String col, String sort, String id) {
		return departmentDao.selectDepartmentUserList(
				start, length, query, col, sort,
				id);
	}


}
