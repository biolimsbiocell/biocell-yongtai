package com.biolims.core.userGroup.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.core.model.user.UserGroup;
import com.biolims.core.model.user.UserGroupUser;
import com.biolims.core.model.user.UserRole;
import com.biolims.dao.BaseHibernateDao;

@Repository
@SuppressWarnings("unchecked")
public class UserGroupDao extends BaseHibernateDao {

	public List<UserGroupUser> selectUserGroupUserByGroupId(String groupId) throws Exception {
		String hql = " from UserGroupUser a where a.userGroup.id = '" + groupId
				+ "' and a.user.state.id ='1z' order by a.orderNumber ASC";
		return this.getSession().createQuery(hql).list();
	}

	public List<UserGroupUser> selectUserGroupUserByGroupIdList(List<String> groupIdList) throws Exception {

		String in = "";
		for (String a : groupIdList) {
			if (!in.equals("")) {
				in += ",";
			}
			in += "'" + a + "'";
		}

		String hql = " from UserGroupUser a where a.userGroup.id in (" + in
				+ ") and a.user.state.id ='1z' order by a.orderNumber ASC";
		return this.getSession().createQuery(hql).list();
	}

	public Map<String, Object> userGroupSelTableJson(Integer start,
			Integer length, String query, String col, String sort) {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from UserGroup where 1=1 and state ='1'";
		String key = "";
//		if(queryMap!=null){
//			key=map2Where(queryMap);
//		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from UserGroup where 1=1 and state ='1'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<UserGroup> list = new ArrayList<UserGroup>();
			list=this.getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	
	
	
	}

	public Map<String, Object> findUserGroupList(int start, int length,
			String query, String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from UserGroup where 1=1 ";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from UserGroup where 1=1 ";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<UserGroup> list = new ArrayList<UserGroup>();
			list=this.getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<UserGroupUser> getUserGroupUser(String groupId) {
		String hql = " from UserGroupUser where 1=1 and userGroup.id='" + groupId + "'";
		String key = "";
		List<UserGroupUser> list = this.getSession().createQuery(hql + key).list();
		return list;
	}

	public UserGroupUser getUserGroupUser(String groupId, String user) {
		String hql = " from UserGroupUser where 1=1 and userGroup.id='" + groupId + "' and user.id='"+user+"'";
		String key = "";
		UserGroupUser ugu= (UserGroupUser) this.getSession().createQuery(hql + key).uniqueResult();
		return ugu;
	}
}
