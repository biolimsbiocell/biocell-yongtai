/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：Service
 * 类描述：人员组管理service
 * 创建人：倪毅
 * 创建时间：2011-9
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.core.userGroup.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.core.constants.SystemConstants;
import com.biolims.core.model.user.Role;
import com.biolims.core.model.user.UserGroup;
import com.biolims.core.model.user.UserGroupUser;
import com.biolims.core.model.user.UserRole;
import com.biolims.core.userGroup.dao.UserGroupDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.model.LogInfo;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;

@Service
public class UserGroupService {
	@Resource
	private CommonDAO commonDAO;
	@Autowired
	CommonService commonService;
	@Resource
	private UserGroupDao userGroupDao;

	/**
	 * 
	 * 检索
	 * 
	 * 
	 * @return 参数值MAP，如果传递的为null返回空字符串
	 */
	public Map<String, Object> findUserGroupList(int start, int length, String query, String col, String sort)
			throws Exception {
		return userGroupDao.findUserGroupList(start, length, query, col, sort);

	}

	/**
	 * 保存
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(UserGroup an, String changeLog) throws Exception {
		commonDAO.saveOrUpdate(an);

		if (changeLog != null && !"".equals(changeLog)) {
			LogInfo li = new LogInfo();
			User user2 = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setLogDate(new Date());
			li.setClassName("UserGroupUser");
			li.setFileId(an.getId());
			li.setUserId(user2.getId());
			li.setModifyContent(changeLog);
			li.setState("1");
			li.setStateName("数据新增");

			commonDAO.saveOrUpdate(li);
		}

	}

	public UserGroup getUserGroup(String id) throws Exception {
		UserGroup animal = commonDAO.get(UserGroup.class, id);
		return animal;
	}

	public String hasId(String id) throws Exception {
		return commonService.hasId(id, UserGroup.class);
	}

	public Map<String, Object> findUserGroupUserList(int startNum, int limitNum, String dir, String sort,
			String contextPath, String userGroupId) throws Exception {
		// 检索条件map,每个字段检索值
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		// 检索方式map,每个字段的检索方式
		Map<String, String> mapForCondition = new HashMap<String, String>();
		mapForQuery.put("userGroup.id", userGroupId);
		mapForCondition.put("userGroup.id", "=");

		Map<String, Object> controlMap = commonDAO.findObjectCondition(startNum, limitNum, dir, sort,
				UserGroupUser.class, mapForQuery, mapForCondition);
		return controlMap;
	}

	public List<UserGroupUser> findUserGroupUserList(String userGroupId) throws Exception {

		return userGroupDao.selectUserGroupUserByGroupId(userGroupId);

	}

	public List<UserGroupUser> findUserGroupUserListByList(List<String> groupIdList) throws Exception {

		return userGroupDao.selectUserGroupUserByGroupIdList(groupIdList);

	}

	/**
	 * 保存工作组用户
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveUserGroupUser(String groupId, String[] userId) throws Exception {
		if (groupId != null && !"".equals(groupId)) {
			UserGroupUser ugu = null;
			for (String user : userId) {
				ugu = userGroupDao.getUserGroupUser(groupId, user);
				if (ugu != null) {

				} else {
					ugu = new UserGroupUser();
					UserGroup ug = new UserGroup();
					User u = new User();
					ug.setId(groupId);
					u.setId(user);
					ugu.setUser(u);
					ugu.setUserGroup(ug);
					commonDAO.saveOrUpdate(ugu);

					String kucun = "人员组:" + "用户名:" + ugu.getUser().getId() + "的数据已添加";
					if (kucun != null && !"".equals(kucun)) {
						LogInfo li = new LogInfo();
						User user2 = (User) ServletActionContext.getRequest().getSession()
								.getAttribute(SystemConstants.USER_SESSION_KEY);
						li.setLogDate(new Date());
						li.setClassName("UserGroupUser");
						li.setFileId(ugu.getId());
						li.setUserId(user2.getId());
						li.setModifyContent(kucun);
						li.setState("3");
						li.setStateName("数据修改");
						commonDAO.saveOrUpdate(li);
					}
				}
			}
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delUserGroupUser(String[] ids, String groupId) {
		List<UserGroupUser> list = userGroupDao.getUserGroupUser(groupId);
		for (String id : ids) {
			for (UserGroupUser ur : list) {
				if (ur.getUser().getId().equals(id)) {
					commonDAO.delete(ur);
				}

			}
			if (ids.length > 0) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User user2 = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(user2.getId());
				li.setFileId(groupId);
				li.setClassName("UserGroupUser");
				li.setModifyContent("人员组:" + "用户名:" + id + "的数据已删除");
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}
		}

	}

	public Map<String, Object> userGroupSelTableJson(Integer start, Integer length, String query, String col,
			String sort) {
		return userGroupDao.userGroupSelTableJson(start, length, query, col, sort);
	}

	public Map<String, Object> findUserGroupUserList(String groupId, Integer start, Integer length, String query,
			String col, String sort) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<UserGroupUser> urList = userGroupDao.getUserGroupUser(groupId);
		List<User> uList = new ArrayList<User>();
		for (UserGroupUser ur : urList) {
			User u = ur.getUser();
			uList.add(u);
		}
		map.put("list", uList);
		map.put("recordsTotal", uList.size());
		map.put("recordsFiltered", uList.size());
		return map;
	}
}
