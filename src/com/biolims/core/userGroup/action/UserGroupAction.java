/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：Action
 * 类描述：人员组管理
 * 创建人：倪毅
 * 创建时间：2011-11
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.core.userGroup.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.model.user.User;
import com.biolims.core.constants.SystemConstants;
import com.biolims.core.model.user.UserGroup;
import com.biolims.core.model.user.UserGroupUser;
import com.biolims.core.userGroup.service.UserGroupService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/core/userGroup")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public class UserGroupAction extends BaseActionSupport {

	private static final long serialVersionUID = -164661537469123056L;

	@Autowired
	private UserGroupService userGroupService;

	// 用于页面上显示模块名称
	private String title = "用户工作组";

	// 该action权限id
	private String rightsId = "11102";

	private UserGroup userGroup = new UserGroup();

	/**
	 * 
	 * @Title: showExperimentList
	 * @Description: 人员组列表
	 * @author : shengwei.wang
	 * @date 2018年4月9日下午4:29:55
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showUserGroupList")
	public String showExperimentList() throws Exception {
		rightsId = "11102";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/core/userGroup/showUserGroupList.jsp");
	}

	@Action(value = "showUserGroupListJson")
	public void showUserGroupListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = userGroupService.findUserGroupList(
					start, length, query, col, sort);
			List<UserGroup> list = (List<UserGroup>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("note", "");
			map.put("state", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: toEditUserGroup
	 * @Description: 新建、编辑
	 * @author : shengwei.wang
	 * @date 2018年4月9日下午5:54:14
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "toEditUserGroup")
	public String toEditUserGroup() throws Exception {
		rightsId = "11101";
		String id = getParameterFromRequest("id");

		if (id != null && !id.equals("")) {
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			userGroup = userGroupService.getUserGroup(id);
			putObjToContext("userGroupId", id);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		} else {
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		return dispatcher("/WEB-INF/page/core/userGroup/editUserGroup.jsp");
	}

	/**
	 * 访问编辑页面
	 */

	@Action(value = "toView")
	public String toView() throws Exception {
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_VIEW);
			userGroup = userGroupService.getUserGroup(id);

		}
		return dispatcher("/WEB-INF/page/core/userGroup/editUserGroup.jsp");
	}

	/**
	 * 添加
	 */
	@Action(value = "save")
	public String save() throws Exception {
		String changeLog = getParameterFromRequest("changeLog");
		userGroupService.save(userGroup,changeLog);
		return redirect("/core/userGroup/toEditUserGroup.action?id="
				+ userGroup.getId());
	}

	/**
	 * 查询是否编号重复
	 */
	@Action(value = "hasId")
	public String hasId() throws Exception {
		String id = getParameterFromRequest("id");
		return this.renderText(userGroupService.hasId(id));

	}

	@Action(value = "showUserGroupUserListJson")
	public void showUserGroupUserListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String groupId = getParameterFromRequest("id");
		Map<String, Object> result = userGroupService.findUserGroupUserList(
				groupId, start, length, query, col, sort);
		List<User> list = (List<User>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("department-name", "");
		map.put("dicJob-name", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * 删除设备明细
	 */
	@Action(value = "delUserGroupUser")
	public void delUserGroupUser() throws Exception {
		String[] ids = getRequest().getParameterValues("ids[]");
		String groupId = getParameterFromRequest("id");
		Map<String, Object> map = new HashMap<String, Object>();
		try {

			userGroupService.delUserGroupUser(ids, groupId);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 添加用户
	 */
	@Action(value = "saveUserGroupUser")
	public void saveUserGroupUser() throws Exception {
		String groupId = getParameterFromRequest("groupId");
		String[] userId = getRequest().getParameterValues("userId[]");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			userGroupService.saveUserGroupUser(groupId, userId);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// public void saveUserGroupUser(String json, String userGroupId)
	// throws Exception {
	// userGroupService.saveUserGroupUser(userGroupId, json);
	// }

	// @Action(value = "userGroupSelect", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public String showUserGroupList() throws Exception {
	// LinkedHashMap<String, String[]> map = new LinkedHashMap<String,
	// String[]>();
	// map.put("id", new String[] { "", "string", "", "编码", "150", "true", "",
	// "", "", "", "", "" });
	// map.put("name", new String[] { "", "string", "", "名称", "150", "true",
	// "", "", "", "", "", "" });
	// map.put("note", new String[] { "", "string", "", "描述", "250", "true",
	// "", "", "", "", "", "" });
	// map.put("state", new String[] { "", "string", "", "状态", "120", "true",
	// "", "", "", "", "", "" });
	//
	// String type = generalexttype(map);
	// String col = generalextcol(map);
	// putObjToContext("type", type);
	// putObjToContext("col", col);
	//
	// putObjToContext("path", ServletActionContext.getRequest()
	// .getContextPath()
	// + "/core/userGroup/userGroupSelectJson.action");
	// return dispatcher("/WEB-INF/page/core/userGroup/userGroupSelect.jsp");
	// }
	//
	// @Action(value = "userGroupSelectJson", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void userGroupSelectJson() throws Exception {
	// int startNum = Integer.parseInt(getParameterFromRequest("start"));
	// int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
	// String dir = getParameterFromRequest("dir");
	// String sort = getParameterFromRequest("sort");
	// String data = getParameterFromRequest("data");
	// Map<String, Object> controlMap = userGroupService.findUserGroupList(
	// startNum, limitNum, dir, sort, getContextPath(), data);
	// long totalCount = Long.parseLong(controlMap.get("totalCount")
	// .toString());
	// List<UserGroup> list = (List<UserGroup>) controlMap.get("list");
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "");
	// map.put("name", "");
	// map.put("note", "");
	// map.put("state", "");
	// new SendData().sendDateJson(map, list, totalCount,
	// ServletActionContext.getResponse());
	// }

	/**
	 * 
	 * @Title: userGroupSelTable
	 * @Description: 选择负责组
	 * @author : shengwei.wang
	 * @date 2018年2月6日下午6:15:03
	 * @return String
	 * @throws
	 */
	@Action(value = "userGroupSelTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String userGroupSelTable() {
		return dispatcher("/WEB-INF/page/core/userGroup/showUserGroupSelTable.jsp");
	}

	@Action(value = "userGroupSelTableJson")
	public void userGroupSelTableJson() {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = userGroupService
					.userGroupSelTableJson(start, length, query, col, sort);
			List<UserGroupUser> list = (List<UserGroupUser>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("note", "");
			map.put("state", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public UserGroupService getUserGroupService() {
		return userGroupService;
	}

	public void setUserGroupService(UserGroupService userGroupService) {
		this.userGroupService = userGroupService;
	}

	public UserGroup getUserGroup() {
		return userGroup;
	}

	public void setUserGroup(UserGroup userGroup) {
		this.userGroup = userGroup;
	}

}
