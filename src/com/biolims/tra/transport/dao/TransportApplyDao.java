package com.biolims.tra.transport.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.tra.transport.model.TransportApply;
import com.biolims.tra.transport.model.TransportApplyAnimal;
import com.biolims.tra.transport.model.TransportApplyBacteria;
import com.biolims.tra.transport.model.TransportApplyCell;
import com.biolims.tra.transport.model.TransportApplyTemp;
import com.biolims.tra.transport.model.TransportApplyTissue;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class TransportApplyDao extends BaseHibernateDao {
	public Map<String, Object> selectTransportApplyList(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from TransportApply where 1=1 ";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from TransportApply where 1=1 ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<TransportApply> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

	public Map<String, Object> selectDialogTransportApplyList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from TransportApply t where 1=1 and t.state='1' ";// and (isDialog=null or isDialog <> '0') ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<TransportApply> list = new ArrayList<TransportApply>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectTransportApplyAnimalList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from TransportApplyAnimal where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and transportApply.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<TransportApplyAnimal> list = new ArrayList<TransportApplyAnimal>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectTransportApplyBacteriaList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from TransportApplyBacteria where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and transportApply.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<TransportApplyBacteria> list = new ArrayList<TransportApplyBacteria>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectTransportApplyCellList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from TransportApplyCell where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and transportApply.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<TransportApplyCell> list = new ArrayList<TransportApplyCell>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectTransportApplyTissueList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from TransportApplyTissue where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and transportApply.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<TransportApplyTissue> list = new ArrayList<TransportApplyTissue>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// ajax要选择的动物数据
	public Map<String, Object> setAnimalList(String code) throws Exception {
		String hql = "from TransportApplyAnimal where 1=1 and (isDialog=null or isDialog <> '0') ";
		String key = "";
		if (code != null)
			key = key + " and transportApply.id='" + code + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<TransportApplyAnimal> list = this.getSession().createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// ajax要选择的质粒数据
	public Map<String, Object> setBacteriaList(String code) throws Exception {
		String hql = "from TransportApplyBacteria where 1=1 and (isDialog=null or isDialog <> '0') ";
		String key = "";
		if (code != null)
			key = key + " and transportApply.id='" + code + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<TransportApplyBacteria> list = this.getSession().createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// ajax要选择的CELL数据
	public Map<String, Object> setCellList(String code) throws Exception {
		String hql = "from TransportApplyCell where 1=1 and (isDialog=null or isDialog <> '0') ";
		String key = "";
		if (code != null)
			key = key + " and transportApply.id='" + code + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<TransportApplyCell> list = this.getSession().createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// ajax要选择的Tissue数据
	public Map<String, Object> setTissueList(String code) throws Exception {
		String hql = "from TransportApplyTissue where 1=1 and (isDialog=null or isDialog <> '0') ";
		String key = "";
		if (code != null)
			key = key + " and transportApply.id='" + code + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<TransportApplyTissue> list = this.getSession().createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/**
	 * 更改isDialog数据
	 * 
	 * @param type
	 * @param idStr
	 */
	public Map<String, Object> findIsDialog(String type, String id) {
		String hql = "";
		Map<String, Object> result = new HashMap<String, Object>();
		switch (Integer.parseInt(type)) {
		case 0:
			hql = " from TransportApplyAnimal  where id = '" + id + "'";
			List<TransportApplyAnimal> list = this.getSession().createQuery(hql).list();
			if (list != null && list.size() > 0)
				result.put("list", list.get(0));
			break;
		case 1:
			hql = " from TransportApplyBacteria where id = '" + id + "'";
			List<TransportApplyBacteria> list1 = this.getSession().createQuery(hql).list();
			if (list1 != null && list1.size() > 0)
				result.put("list", list1.get(0));
			break;
		case 2:
			hql = " from TransportApplyCell where id = '" + id + "'";
			List<TransportApplyCell> list2 = this.getSession().createQuery(hql).list();
			if (list2 != null && list2.size() > 0)
				result.put("list", list2.get(0));
			break;
		case 3:
			hql = " from TransportApplyTissue where id = '" + id + "'";
			List<TransportApplyTissue> list3 = this.getSession().createQuery(hql).list();
			if (list3 != null && list3.size() > 0)
				result.put("list", list3.get(0));
			break;
		}
		return result;
	}

	// saveToOrderBacteria
	public List<TransportApplyAnimal> saveToOrderAnimal(String id) {
		String hql = "from TransportApplyAnimal where transportApply.id='" + id + "'";
		List<TransportApplyAnimal> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<TransportApplyBacteria> saveToOrderBacteria(String id) {
		String hql = "from TransportApplyBacteria where transportApply.id='" + id + "'";
		List<TransportApplyBacteria> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<TransportApplyCell> saveToOrderCell(String id) {
		String hql = "from TransportApplyCell where transportApply.id='" + id + "'";
		List<TransportApplyCell> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<TransportApplyTissue> saveToOrderTissue(String id) {
		String hql = "from TransportApplyTissue where transportApply.id='" + id + "'";
		List<TransportApplyTissue> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> showTransportApplyItemListJson(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from TransportApplyCell where 1=1 and transportApply.id ='" + id + "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			List<TransportApplyCell> list = null;
			Long filterCount = (Long) getSession().createQuery(countHql).uniqueResult();
			String hql = "from TransportApplyCell  where 1=1 and transportApply.id ='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> showTransportOrderTempList(Integer start, Integer length, String query, String col,
			String sort, String flag) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from TransportApplyTemp where 1=1 and state='1' ";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		if (flag != null && flag.equals("plan")) {
			key += " and planState = '1'";
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			List<TransportApplyTemp> list = null;
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from TransportApplyTemp  where 1=1 and state='1'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
}