package com.biolims.tra.transport.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.cell.revive.model.CellReviveInfo;
import com.biolims.experiment.plasma.model.PlasmaReceiveItem;
import com.biolims.experiment.quality.model.QualityTestInfo;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.sample.model.SampleReceive;
import com.biolims.tra.sampletransport.model.SampleTransportItem;
import com.biolims.tra.sampletransport.model.SampleTransportState;
import com.biolims.tra.transport.model.TransportApply;
import com.biolims.tra.transport.model.TransportOrder;
import com.biolims.tra.transport.model.TransportOrderAnimal;
import com.biolims.tra.transport.model.TransportOrderBacteria;
import com.biolims.tra.transport.model.TransportOrderCell;
import com.biolims.tra.transport.model.TransportOrderPlasmid;
import com.biolims.tra.transport.model.TransportOrderTemp;
import com.biolims.tra.transport.model.TransportOrderTissue;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class TransportOrderDao extends BaseHibernateDao {
	public Map<String, Object> selectTransportOrderList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from TransportOrder transportOrder where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<TransportOrder> list = new ArrayList<TransportOrder>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by id desc";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectTransportOrderTissueList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from TransportOrderTissue where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and transportOrder.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<TransportOrderTissue> list = new ArrayList<TransportOrderTissue>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectTransportOrderBacteriaList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from TransportOrderBacteria where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and transportOrder.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<TransportOrderBacteria> list = new ArrayList<TransportOrderBacteria>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectTransportOrderAnimalList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from TransportOrderAnimal where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and transportOrder.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<TransportOrderAnimal> list = new ArrayList<TransportOrderAnimal>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectTransportOrderPlasmidList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from TransportOrderPlasmid where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and transportOrder.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<TransportOrderPlasmid> list = new ArrayList<TransportOrderPlasmid>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public List<TransportApply> selectApply(String id) {
		String hql = "from TransportApply where transportApply.id='" + id + "'";
		List<TransportApply> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> selectAnimalOutItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from CellReviveInfo where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and cellRevive.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<CellReviveInfo> list = new ArrayList<CellReviveInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> showTransportOrderListJson(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from TransportOrder where 1=1 and ( zuoState is null or zuoState='' or zuoState ='1')   ";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from TransportOrder where 1=1  and ( zuoState is null or zuoState='' or zuoState ='1') ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<TransportOrder> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> showTransportOrderItemListJson(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from TransportOrderCell where 1=1 and transportOrder.id = '" + id + "'";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from TransportOrderCell where 1=1 and transportOrder.id = '" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<TransportOrderCell> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> showTransportOrderTempList(Integer start, Integer length, String query, String col,
			String sort, String flag) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from TransportOrderTemp where 1=1 and state='1' ";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
//		if (flag != null && flag.equals("plan")) {
//			key += " and planState = '1'";
//		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			List<TransportOrderTemp> list = null;
			Long filterCount = (Long) getSession().createQuery(countHql).uniqueResult();
			String hql = "from TransportOrderTemp where 1=1 and state='1' ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
//				key += " order by " + col + " " + sort+",sampleOrder.crmCustomer.name desc,sampleOrder.inspectionDepartment.name";
				if ("id".equals(col)) {
					key += " order by sampleOrder.crmCustomer.name desc,sampleOrder.inspectionDepartment.name ";
				} else {
					key += " order by " + col + " " + sort;
				}

			}
			list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> showTransportItemSampleTableJson(Integer start, Integer length, String query, String col,
			String sort, String id) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleTransportItem where 1=1 and transportOrder.id='" + id + "'";
		String key = "";
		// if(query!=null){
		// key=map2Where(query);
		// }
		// String scopeId=(String)
		// ActionContext.getContext().getSession().get("scopeId");
		// if(!"all".equals(scopeId)){
		// key+=" and scopeId='"+scopeId+"'";
		// }
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql).uniqueResult();
			String hql = "from SampleTransportItem  where 1=1 and transportOrder.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SampleTransportItem> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> showTransportStateSampleTableJson(Integer start, Integer length, String query,
			String col, String sort, String id) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleTransportState where 1=1 and transportOrder.id='" + id + "'";
		String key = "";
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql).uniqueResult();
			String hql = "from SampleTransportState  where 1=1 and transportOrder.id='" + id + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SampleTransportState> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<SampleTransportItem> showSampleTransportItemList(String id) {
		List<SampleTransportItem> list = new ArrayList<SampleTransportItem>();
		String hql = "from SampleTransportItem where 1=1 and transportOrder='" + id + "'";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<SampleOrderItem> showSampleOrderItem(String orderNo) {
		List<SampleOrderItem> list = new ArrayList<SampleOrderItem>();
		String hql = "from SampleOrderItem where 1=1 and sampleOrder='" + orderNo + "'";
		list = getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> showQualityTestTableJson(Integer start, Integer length, String query, String col,
			String sort, String batch) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from QualityTestInfo where 1=1 and batch='" + batch + "'";
		String key = "";
		// if (query != null && !"".equals(query)) {
		// key = map2Where(query);
		// }
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from QualityTestInfo  where 1=1 and batch='" + batch + "'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
			}
			key += " order by " + col + " " + sort;
			List<QualityTestInfo> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<TransportOrderCell> getTransportOrderCellsByIdAndCode(String id, String sampleCode) {
		List<TransportOrderCell> list = new ArrayList<TransportOrderCell>();
		String hql = "from TransportOrderCell where 1=1 and transportOrder.id='" + id + "' and code='" + sampleCode
				+ "' ";
		list = getSession().createQuery(hql).list();
		return list;
	}

	
	//选择订单后带入日期和QA审核人
	public List<SampleReceive> showDateAndUser(String orderId) {
		String hql = "from SampleReceive where 1=1 and sampleOrder ='"+orderId+"'";
		List<SampleReceive> sr =  getSession().createQuery(hql).list();
		return sr;
	}

	public List<PlasmaReceiveItem> getPlasmaReceiveItemByCode(String sampleCode) {
		String hql = "from PlasmaReceiveItem where 1=1 and batchNumber ='"+sampleCode+"' and auditPass='1' ";
		List<PlasmaReceiveItem> sr =  getSession().createQuery(hql).list();
		return sr;
	}
	
	public List<PlasmaReceiveItem> getPlasmaReceiveItemByCode1(String sampleCode) {
		String hql = "from PlasmaReceiveItem where 1=1 and sampleCode ='"+sampleCode+"' and auditPassTwo='1' ";
		List<PlasmaReceiveItem> sr =  getSession().createQuery(hql).list();
		return sr;
	}

	public List<PlasmaReceiveItem> showPlasmaReceiveItems(String sampleCode) {
		String hql = "from PlasmaReceiveItem where 1=1 and sampleCode ='"+sampleCode+"' and auditPassTwo='1' ";
		List<PlasmaReceiveItem> sr =  getSession().createQuery(hql).list();
		return sr;
	}

	public List<TransportOrderCell> getTransportOrderCellsByids(String isd) {
		String hql = "from TransportOrderCell where 1=1 and id in ("+isd+") and  submitData='是' ";
		List<TransportOrderCell> sr =  getSession().createQuery(hql).list();
		return sr;
	}

	
	

	
	

}