package com.biolims.tra.transport.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.tra.transport.service.TransportOrderService;

public class TransportOrderEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		TransportOrderService mbService = (TransportOrderService) ctx.getBean("transportOrderService");
		mbService.chengeState(applicationTypeActionId, contentId);

		return "";
	}
}
