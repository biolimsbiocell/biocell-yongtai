package com.biolims.tra.transport.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.tra.transport.service.TransportApplyService;

public class TransportApplyEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		TransportApplyService mbService = (TransportApplyService) ctx.getBean("transportApplyService");
		mbService.chengeState(applicationTypeActionId, contentId);

		return "";
	}
}
