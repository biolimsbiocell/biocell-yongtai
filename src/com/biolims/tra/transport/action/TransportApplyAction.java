package com.biolims.tra.transport.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.tra.transport.model.TransportApply;
import com.biolims.tra.transport.model.TransportApplyAnimal;
import com.biolims.tra.transport.model.TransportApplyBacteria;
import com.biolims.tra.transport.model.TransportApplyCell;
import com.biolims.tra.transport.model.TransportApplyTemp;
import com.biolims.tra.transport.model.TransportApplyTissue;
import com.biolims.tra.transport.service.TransportApplyService;
import com.biolims.tra.transport.service.TransportOrderService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/tra/transport/transportApply")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class TransportApplyAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "tra05";
	@Autowired
	private TransportApplyService transportApplyService;
	@Autowired
	private TransportOrderService transportOrderService;
	@Resource
	private CodingRuleService codingRuleService;
	private TransportApply transportApply = new TransportApply();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private FieldService fieldService;
	@Resource
	private CommonDAO commonDAO;

	@Action(value = "showTransportApplyList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTransportApplyList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/tra/transport/transportApply.jsp");
	}

	@Action(value = "showTransportApplyListJson")
	public void showTransportApplyListJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = transportApplyService.findTransportApplyList(start, length, query, col, sort);
			List<TransportApply> list = (List<TransportApply>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			// map.put("project-id", "");
			// map.put("project-name", "");
			// map.put("project-projectId", "");
			// map.put("type-id", "");
			// map.put("type-name", "");
			// // map.put("objType-id", "");
			// map.put("crmContractName", "");
			// map.put("crmContractNum", "");
			// map.put("crmCustomerName", "");
			// map.put("crmCustomerDept", "");
			// map.put("crmLinkMan", "");
			// map.put("crmLinkMoblie", "");
			// map.put("projectCity", "");
			// map.put("objType", "");
			// map.put("createUser-id", "");
			// map.put("createUser-name", "");
			// map.put("createDate", "yyyy-MM-dd");
			// map.put("applyDepartment", "");
			// map.put("confirmUser-id", "");
			// map.put("confirmUser-name", "");
			// map.put("confirmDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("stateName", "");
			// map.put("content1", "");
			// map.put("content2", "");
			// map.put("content3", "");
			// map.put("content4", "");
			// map.put("content5", "");
			// map.put("content6", "");
			// map.put("content7", "");
			// map.put("content8", "");
			// map.put("content9", "yyyy-MM-dd");
			// map.put("content10", "yyyy-MM-dd");
			// map.put("content11", "yyyy-MM-dd");
			// map.put("content12", "yyyy-MM-dd");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("TransportApply");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 编辑的方法 @Title: editTransportOrder @Description:
	 * TODO @param @return @param @throws Exception @return String @author 孙灵达 @date
	 * 2019年4月18日 @throws
	 */
	@Action(value = "editTransportApply")
	public String editTransportApply() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			transportApply = transportApplyService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "transportApply");
		} else {
			transportApply.setId("NEW");
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			transportApply.setCreateUser(user);
			transportApply.setCreateDate(new Date());
			transportApply.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_NEW);
			transportApply.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_NEW_NAME);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(transportApply.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/tra/transport/transportApplyEdit.jsp");
	}

	/**
	 * 产品运输计划左侧表展示 @Title: showTransportApplyTempTableJson @Description:
	 * TODO @param @throws Exception @return void @author 孙灵达 @date
	 * 2019年4月19日 @throws
	 */
	@Action(value = "showTransportApplyTempTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTransportApplyTempTableJson() throws Exception {
		String flag = "plan";
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = transportApplyService.showTransportOrderTempList(start, length, query, col,
					sort, flag);
			List<TransportApplyTemp> list = (List<TransportApplyTemp>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("productName", "");
			map.put("pronoun", "");
			map.put("sampleOrder-crmCustomer-name", "");
			map.put("sampleOrder-medicalInstitutionsPhone", "");
			map.put("orderCode", "");
			map.put("sampleType", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-project-id", "");
			map.put("orderId", "");
			map.put("classify", "");
			map.put("scopeId", "");
			map.put("scopeName", "");
			map.put("sampleInfo-changeType", "");
			map.put("productId", "");
			map.put("sampleOrder-id", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Action(value = "showTransportApplyItemListJson")
	public void showTransportApplyItemListJson() {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String id = getParameterFromRequest("id");
		try {
			Map<String, Object> result = transportApplyService.showTransportApplyItemListJson(id, start, length, query,
					col, sort);
			List<TransportApplyCell> list = (List<TransportApplyCell>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("num", "");
			map.put("way-id", "");
			map.put("way-name", "");
			map.put("sampleOrder-id", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("TransportOrderCell");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "transportApplySelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogTransportApplyList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/tra/transport/transportApplyDialog.jsp");
	}

	@Action(value = "showDialogTransportApplyListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogTransportApplyListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = transportApplyService.findDialogTransportApplyList(map2Query, startNum, limitNum,
				dir, sort);
		Long count = (Long) result.get("total");
		List<TransportApply> list = (List<TransportApply>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("project-id", "");
		map.put("project-name", "");
		map.put("project-projectId", "");
		map.put("type-id", "");
		map.put("type-name", "");
		// map.put("objType-id", "");
		map.put("crmContractName", "");
		map.put("crmContractNum", "");
		map.put("crmCustomerName", "");
		map.put("crmCustomerDept", "");
		map.put("crmLinkMan", "");
		map.put("crmLinkMoblie", "");
		map.put("projectCity", "");
		map.put("objType", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("applyDepartment", "");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("content1", "");
		map.put("content2", "");
		map.put("content3", "");
		map.put("content4", "");
		map.put("content5", "");
		map.put("content6", "");
		map.put("content7", "");
		map.put("content8", "");
		map.put("content9", "yyyy-MM-dd");
		map.put("content10", "yyyy-MM-dd");
		map.put("content11", "yyyy-MM-dd");
		map.put("content12", "yyyy-MM-dd");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	/*
	 * @Action(value = "editTransportApply") public String editTransportApply()
	 * throws Exception { String id = getParameterFromRequest("id"); long num = 0;
	 * if (id != null && !id.equals("")) { transportApply =
	 * transportApplyService.get(id); putObjToContext("handlemethod",
	 * SystemConstants.PAGE_HANDLE_METHOD_MODIFY); toToolBar(rightsId, "", "",
	 * SystemConstants.PAGE_HANDLE_METHOD_MODIFY); num =
	 * fileInfoService.findFileInfoCount(id, "transportApply"); } else {
	 * transportApply.setId("NEW"); User user = (User) this
	 * .getObjFromSession(SystemConstants.USER_SESSION_KEY);
	 * transportApply.setCreateUser(user); transportApply.setCreateDate(new Date());
	 * transportApply
	 * .setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
	 * transportApply
	 * .setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
	 * putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
	 * toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD); }
	 * toState(transportApply.getState()); putObjToContext("fileNum", num); return
	 * dispatcher("/WEB-INF/page/tra/transport/transportApplyEdit.jsp"); }
	 */

	@Action(value = "copyTransportApply")
	public String copyTransportApply() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		transportApply = transportApplyService.get(id);
		transportApply.setId("NEW");
		transportApply.setState("3");
		transportApply.setStateName("新建");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/tra/transport/transportApplyEdit.jsp");
	}

	@Action(value = "save1")
	public String save1() throws Exception {
		String id = transportApply.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "TransportApply";
			String markCode = "YSSQ";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			transportApply.setId(autoID);
		}
		Map aMap = new HashMap();
		// aMap.put("transportApplyAnimal",
		// getParameterFromRequest("transportApplyAnimalJson"));
		//
		// aMap.put("transportApplyBacteria",
		// getParameterFromRequest("transportApplyBacteriaJson"));
		//
		// aMap.put("transportApplyCell",
		// getParameterFromRequest("transportApplyCellJson"));
		//
		// aMap.put("transportApplyTissue",
		// getParameterFromRequest("transportApplyTissueJson"));

		aMap.put("itemData", getParameterFromRequest("itemData"));
		transportApplyService.save(transportApply, aMap);
		return redirect("/tra/transport/transportApply/editTransportApply.action?id=" + transportApply.getId());
	}

	@Action(value = "saveNew")
	public void saveNew() throws Exception {

		Map<String, Object> mm = new HashMap<String, Object>();
		try {
			String dataValue = getParameterFromRequest("dataValue");
			String str = "[" + dataValue + "]";
			String changeLog = getParameterFromRequest("changeLog");
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str, List.class);

			for (Map<String, Object> map : list) {
				transportApply = (TransportApply) commonDAO.Map2Bean(map, transportApply);
			}
			String id = transportApply.getId();
			if ((id != null && id.equals("")) || id.equals("NEW")) {
				String modelName = "TransportApply";
				String markCode = "YSSQ";
				String autoID = codingRuleService.genTransID(modelName, markCode);
				transportApply.setId(autoID);
			}
			Map aMap = new HashMap();
			Map lMap = new HashMap();
			// aMap.put("sampleTransportItem",
			// getParameterFromRequest("sampleTransportItemJson"));
			// aMap.put("sampleTrackingCondition",
			// getParameterFromRequest("sampleTrackingConditionJson"));
			transportApplyService.save(transportApply, aMap, changeLog, lMap);
			mm.put("id", transportApply.getId());
			mm.put("success", true);

		} catch (Exception e) {
			e.printStackTrace();
			mm.put("success", false);
		}
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		if (bpmTaskId != null && !bpmTaskId.equals("") && !bpmTaskId.equals("null")) {
			mm.put("bpmTaskId", bpmTaskId);
		}
		HttpUtils.write(JsonUtils.toJsonString(mm));
	}

	/**
	 * @throws Exception
	 *             产品运输左侧表添加到运输明细 @Title: addTransportOrderMakeUp @Description:
	 *             TODO @param @return void @author 孙灵达 @date 2018年8月30日 @throws
	 */
	@Action(value = "addTransportOrderMakeUp")
	public void addTransportOrderMakeUp() throws Exception {
		String mainJson = getParameterFromRequest("main");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String id = transportApplyService.addTransportOrderMakeUp(mainJson, ids);
			map.put("data", id);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "viewTransportApply")
	public String toViewTransportApply() throws Exception {
		String id = getParameterFromRequest("id");
		transportApply = transportApplyService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/tra/transport/transportApplyEdit.jsp");
	}

	@Action(value = "showTransportApplyAnimalList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTransportApplyAnimalList() throws Exception {
		return dispatcher("/WEB-INF/page/tra/transport/transportApplyAnimal.jsp");
	}

	@Action(value = "showTransportApplyAnimalListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTransportApplyAnimalListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = transportApplyService.findTransportApplyAnimalList(scId, startNum, limitNum,
					dir, sort);
			Long total = (Long) result.get("total");
			List<TransportApplyAnimal> list = (List<TransportApplyAnimal>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("animalId", "");
			map.put("project-name", "");
			map.put("project-id", "");
			map.put("mark", "");
			map.put("genetype", "");
			map.put("gender", "");
			map.put("birthDate", "yyyy-MM-dd");
			map.put("clean", "");
			map.put("num", "");
			map.put("transportApply-name", "");
			map.put("transportApply-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delTransportApplyAnimal")
	public void delTransportApplyAnimal() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			transportApplyService.delTransportApplyAnimal(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 质粒、菌液
	@Action(value = "showTransportApplyBacteriaList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTransportApplyBacteriaList() throws Exception {
		return dispatcher("/WEB-INF/page/tra/transport/transportApplyBacteria.jsp");
	}

	@Action(value = "showTransportApplyBacteriaListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTransportApplyBacteriaListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = transportApplyService.findTransportApplyBacteriaList(scId, startNum, limitNum,
					dir, sort);
			Long total = (Long) result.get("total");
			List<TransportApplyBacteria> list = (List<TransportApplyBacteria>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			// map.put("transportApply-objName", "");
			// map.put("materailsMain-id", "");
			map.put("projectName", "");
			map.put("objName", "");
			// map.put("project-id", "");
			map.put("concentration", "");
			map.put("volume", "");
			map.put("antibody", "");
			map.put("od260", "");
			map.put("od230", "");
			map.put("note", "");
			map.put("transportApply-name", "");
			map.put("transportApply-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delTransportApplyBacteria")
	public void delTransportApplyBacteria() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			transportApplyService.delTransportApplyBacteria(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showTransportApplyCellList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTransportApplyCellList() throws Exception {
		return dispatcher("/WEB-INF/page/tra/transport/transportApplyCell.jsp");
	}

	@Action(value = "showTransportApplyCellListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTransportApplyCellListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = transportApplyService.findTransportApplyCellList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<TransportApplyCell> list = (List<TransportApplyCell>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("materailsMain-name", "");
			map.put("materailsMain-id", "");
			map.put("materailsMainCell-name", "");
			map.put("materailsMainCell-id", "");
			map.put("materailsMainCell-num", "");
			// map.put("materailsMainCell-cloneCode", "");
			map.put("materailsMainCell-generation", "");
			// map.put("materailsMainCell-background", "");
			map.put("num", "");
			map.put("way-id", "");
			map.put("way-name", "");
			map.put("transportApply-name", "");
			map.put("transportApply-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delTransportApplyCell")
	public void delTransportApplyCell() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			transportApplyService.delTransportApplyCell(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showTransportApplyTissueList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTransportApplyTissueList() throws Exception {
		return dispatcher("/WEB-INF/page/tra/transport/transportApplyTissue.jsp");
	}

	@Action(value = "showTransportApplyTissueListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTransportApplyTissueListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = transportApplyService.findTransportApplyTissueList(scId, startNum, limitNum,
					dir, sort);
			Long total = (Long) result.get("total");
			List<TransportApplyTissue> list = (List<TransportApplyTissue>) result.get("list");
			// AnimalItem animalItem1=new AnimalItem();
			// String a=animalItem1.getBirthDate().toString();
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("animalMain-name", "");
			map.put("animalMain-id", "");
			map.put("animalItem-id", "");
			map.put("animalItem-birthDate", "yyyy-MM-dd");
			map.put("animalItem-inNum", "");
			map.put("gender", "");
			map.put("num", "");
			map.put("sampleNum", "");
			map.put("animalType", "");
			map.put("genetype", "");
			map.put("way", "");
			map.put("note", "");
			map.put("transportApply-name", "");
			map.put("transportApply-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delTransportApplyTissue")
	public void delTransportApplyTissue() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			transportApplyService.delTransportApplyTissue(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public TransportApplyService getTransportApplyService() {
		return transportApplyService;
	}

	public void setTransportApplyService(TransportApplyService transportApplyService) {
		this.transportApplyService = transportApplyService;
	}

	public TransportApply getTransportApply() {
		return transportApply;
	}

	public void setTransportApply(TransportApply transportApply) {
		this.transportApply = transportApply;
	}

	// @Action(value = "showAnimalItemList")
	// public void showAnimalItemList() throws Exception {
	// String animalId = getRequest().getParameter("animalId");
	// Map<String, Object> result = new HashMap<String, Object>();
	// try {
	// List<Map<String, String>> dataListMap = this.animalMainService
	// .showAnimalItemList(animalId);
	// result.put("success", true);
	// result.put("data", dataListMap);
	// } catch (Exception e) {
	// result.put("success", false);
	// }
	// HttpUtils.write(JsonUtils.toJsonString(result));
	// }

	// 创建运输单
	// @Action(value = "setAnimalList")
	// public void setAnimalList() throws Exception {
	// String animalId = getRequest().getParameter("animalId");
	// String typeId = getRequest().getParameter("typeId");
	// TransportApply sct = transportApplyDao.get(TransportApply.class,
	// animalId);
	// TransportOrder mm = new TransportOrder();
	// String modelName = "TransportOrder";
	// String code = projectService.findAutoID(modelName, 5);
	// mm.setId(code);
	// mm.setName("运输申请" + sct.getId() + "生成运输单");
	// mm.setTransportApply(sct);
	// mm.setType(sct.getType());
	// mm.setCreateDate(new Date());
	// mm.setCreateUser(sct.getCreateUser());
	// mm.setObjType(sct.getObjType());
	// mm.setTransportApply(sct);
	// transportOrderDao.saveOrUpdate(mm);
	//
	// Map<String, Object> result = new HashMap<String, Object>();
	// try {
	// if (typeId.equals("0")) {
	// transportApplyService.saveToOrderAnimal(mm, animalId);
	// List<Map<String, String>> dataListMap = this.transportApplyService
	// .setAnimalList(animalId);
	// result.put("success", true);
	// result.put("data", dataListMap);
	// } else if (typeId.equals("1")) {
	// transportApplyService.saveToOrderBacteria(mm, animalId);
	// List<Map<String, String>> dataListMap = this.transportApplyService
	// .setBacteriaList(animalId);
	// result.put("success", true);
	// result.put("data", dataListMap);
	// } else if (typeId.equals("2")) {
	// transportApplyService.saveToOrderCell(mm, animalId);
	// List<Map<String, String>> dataListMap = this.transportApplyService
	// .setCellList(animalId);
	// result.put("success", true);
	// result.put("data", dataListMap);
	// } else {
	// transportApplyService.saveToOrderTissue(mm, animalId);
	// List<Map<String, String>> dataListMap = this.transportApplyService
	// .setTissueList(animalId);
	// result.put("success", true);
	// result.put("data", dataListMap);
	// }
	//
	// } catch (Exception e) {
	// result.put("success", false);
	// }
	// HttpUtils.write(JsonUtils.toJsonString(result));
	// }

	// @Action(value = "showMainCellList")
	// public void showMainCellList() throws Exception {
	// String mainId = getRequest().getParameter("mainId");
	// Map<String, Object> result = new HashMap<String, Object>();
	// try {
	// List<Map<String, String>> dataListMap = this.materailsMainService
	// .showMainCellList(mainId);
	// result.put("success", true);
	// result.put("data", dataListMap);
	// } catch (Exception e) {
	// result.put("success", false);
	// }
	// HttpUtils.write(JsonUtils.toJsonString(result));
	// }

}
