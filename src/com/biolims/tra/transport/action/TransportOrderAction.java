package com.biolims.tra.transport.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.SystemCodeService;
import com.biolims.experiment.plasma.model.PlasmaReceiveItem;
import com.biolims.experiment.quality.model.QualityTestInfo;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.tra.sampletransport.model.SampleTransportItem;
import com.biolims.tra.sampletransport.model.SampleTransportState;
import com.biolims.tra.transport.model.TransportOrder;
import com.biolims.tra.transport.model.TransportOrderAnimal;
import com.biolims.tra.transport.model.TransportOrderBacteria;
import com.biolims.tra.transport.model.TransportOrderCell;
import com.biolims.tra.transport.model.TransportOrderPlasmid;
import com.biolims.tra.transport.model.TransportOrderTemp;
import com.biolims.tra.transport.model.TransportOrderTissue;
import com.biolims.tra.transport.service.TransportApplyService;
import com.biolims.tra.transport.service.TransportOrderService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/tra/transport/transportOrder")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class TransportOrderAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "tra04";
	@Autowired
	private TransportOrderService transportOrderService;
	private TransportOrder transportOrder = new TransportOrder();
	@Resource
	private FileInfoService fileInfoService;
	@Autowired
	private TransportApplyService transportApplyService;
	@Resource
	private SystemCodeService systemCodeService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private FieldService fieldService;
	// @Autowired
	// private AnimalOutService animalOutService;
	// @Resource
	// private ProjectService projectService;

	/**
	 * 展示产品运输的左侧表数据 @Title: showTransportOrderTempTableJson @Description:
	 * TODO @param @throws Exception @return void @author 孙灵达 @date
	 * 2018年8月30日 @throws
	 */
	@Action(value = "showTransportOrderTempTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTransportOrderTempTableJson() throws Exception {
		String flag = "";
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = transportOrderService.showTransportOrderTempList(start, length, query, col,
					sort, flag);
			List<TransportOrderTemp> list = (List<TransportOrderTemp>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("productName", "");
			map.put("pronoun", "");
			map.put("sampleOrder-crmCustomer-name", "");
			map.put("sampleOrder-medicalInstitutionsPhone", "");
			map.put("orderCode", "");
			map.put("sampleType", "");
			map.put("sampleInfo-id", "");
			map.put("sampleInfo-project-id", "");
			map.put("orderId", "");
			map.put("classify", "");
			map.put("scopeId", "");
			map.put("scopeName", "");
			map.put("sampleInfo-changeType", "");
			map.put("productId", "");

			map.put("planState", "");
			map.put("sampleOrder-inspectionDepartment-name", "");
			map.put("sampleOrder-attendingDoctor", "");
			map.put("sampleOrder-attendingDoctorPhone", "");
			map.put("note", "");
			map.put("sampleOrder-barcode", "");
			map.put("sampleOrder-name", "");
			map.put("reinfusionPlanDate", "");

			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * @throws Exception 产品运输左侧表添加到运输明细 @Title:
	 *                   addTransportOrderMakeUp @Description: TODO @param @return
	 *                   void @author 孙灵达 @date 2018年8月30日 @throws
	 */
	@Action(value = "addTransportOrderMakeUp")
	public void addTransportOrderMakeUp() throws Exception {
		String mainJson = getParameterFromRequest("main");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String id = transportOrderService.addTransportOrderMakeUp(mainJson, ids);
			map.put("data", id);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: scanCodeInsertpanduan @Description: 扫码判断样本是否存在，存在更新状态 2 @throws
	 *         Exception void @throws
	 */
	@Action(value = "scanCodeInsertpanduan")
	public void scanCodeInsertpanduan() throws Exception {
		String id = getParameterFromRequest("id");
		String sampleCode = getParameterFromRequest("sampleCode");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			// 查询样本接收子表是否有该条码号
			List<TransportOrderCell> sris = transportOrderService.getTransportOrderCellsByIdAndCode(id, sampleCode);
			if (sris.size() > 0) {
				map.put("sfcz", true);
				TransportOrderCell sri = sris.get(0);
				if (sri.getTransportState() != null && "1".equals(sri.getTransportState())) {
					map.put("sfjj", true);
				} else {
					transportOrderService.saveTransportOrderCells(sri);
					map.put("sfjj", false);
				}
			} else {
				map.put("sfcz", false);
			}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	/**
	 * 
	 * @Title: scanCodeInsertpanduan @Description: 扫码判断样本是否通过放行审核 @throws
	 *         Exception void @throws
	 */
	@Action(value = "reinfusionTongguo")
	public void reinfusionTongguo() throws Exception {
		String sampleCode = getParameterFromRequest("sampleCode");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			// 查询放行审核已通过的数据
			List<PlasmaReceiveItem> sris = transportOrderService.getPlasmaReceiveItemByCode1(sampleCode);
			if (sris.size() > 0) {
				map.put("sfcz", true);
			} else {
				map.put("sfcz", false);
			}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showTransportOrderList")
	public String showTransportOrderList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/tra/transport/transportOrder.jsp");
	}

	@Action(value = "showTransportOrderListJson")
	public void showTransportOrderListJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = transportOrderService.showTransportOrderListJson(start, length, query, col,
					sort);
			List<TransportOrder> list = (List<TransportOrder>) result.get("list");
			
			Map<String, String> map = new HashMap<String, String>();
//			for (TransportOrder transportOrder : list) {
//				if("1".equals(transportOrder.getZuoState())|| null== transportOrder.getZuoState() ) {
					map.put("id", "");
					map.put("name", "");
					map.put("transportApply-id", "");
					map.put("transportApply-name", "");
					map.put("type-id", "");
					map.put("type-name", "");
					map.put("objType", "");
					map.put("createUser-id", "");
					map.put("createUser-name", "");
					map.put("createDate", "yyyy-MM-dd");
					map.put("confirmUser-id", "");
					map.put("confirmUser-name", "");
					map.put("confirmDate", "yyyy-MM-dd");
					map.put("transUser", "");
					map.put("transCompany-name", "");
					map.put("transCode", "");
					map.put("linkman", "");
					map.put("phone", "");
					map.put("receiveCompany", "");
					map.put("address", "");
					map.put("emall", "");
					map.put("flightCode", "");
					map.put("willDate", "yyyy-MM-dd");
					map.put("arrived", "");
					map.put("fromDate", "yyyy-MM-dd");
					map.put("receiveDate", "yyyy-MM-dd");
					map.put("transFee", "");
					map.put("state", "");
					map.put("stateName", "");
					map.put("boxes", "");
					map.put("ysfs-name", "");
					map.put("deliveryDate", "yyyy-MM-dd");
					map.put("expectedDeliveryDate", "yyyy-MM-dd");
					map.put("zuoState", "");
					map.put("transportDate","yyyy-MM-dd");
//				}
//				
//			}
			
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("TransportOrder");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		 * int startNum = Integer.parseInt(getParameterFromRequest("start")); int
		 * limitNum = Integer.parseInt(getParameterFromRequest("limit")); String dir =
		 * getParameterFromRequest("dir"); String sort =
		 * getParameterFromRequest("sort"); String data =
		 * getParameterFromRequest("data"); Map<String, String> map2Query = new
		 * HashMap<String, String>(); if (data != null && data.length() > 0) map2Query =
		 * JsonUtils.toObjectByJson(data, Map.class); Map<String, Object> result =
		 * transportOrderService .findTransportOrderList(map2Query, startNum, limitNum,
		 * dir, sort); Long count = (Long) result.get("total"); List<TransportOrder>
		 * list = (List<TransportOrder>) result.get("list");
		 * 
		 * Map<String, String> map = new HashMap<String, String>(); map.put("id", "");
		 * map.put("name", ""); map.put("transportApply-id", "");
		 * map.put("transportApply-name", ""); map.put("type-id", "");
		 * map.put("type-name", ""); map.put("objType", ""); map.put("createUser-id",
		 * ""); map.put("createUser-name", ""); map.put("createDate", "yyyy-MM-dd");
		 * map.put("confirmUser-id", ""); map.put("confirmUser-name", "");
		 * map.put("confirmDate", "yyyy-MM-dd"); map.put("transUser", "");
		 * map.put("transCompany", ""); map.put("transCode", ""); map.put("linkman",
		 * ""); map.put("phone", ""); map.put("receiveCompany", ""); map.put("address",
		 * ""); map.put("emall", ""); map.put("flightCode", ""); map.put("willDate",
		 * "yyyy-MM-dd"); map.put("arrived", ""); map.put("fromDate", "yyyy-MM-dd");
		 * map.put("receiveDate", "yyyy-MM-dd"); map.put("transFee", "");
		 * map.put("state", ""); map.put("stateName", ""); map.put("boxes", "");
		 * map.put("transportType", ""); map.put("deliveryDate", "yyyy-MM-dd");
		 * map.put("expectedDeliveryDate", "yyyy-MM-dd"); new
		 * SendData().sendDateJson(map, list, count,
		 * ServletActionContext.getResponse());
		 */
	}

	@Action(value = "transportOrderSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogTransportOrderList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/tra/transport/transportOrderDialog.jsp");
	}

	@Action(value = "showDialogTransportOrderListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogTransportOrderListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = transportOrderService.findTransportOrderList(map2Query, startNum, limitNum, dir,
				sort);
		Long count = (Long) result.get("total");
		List<TransportOrder> list = (List<TransportOrder>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("transportApply-id", "");
		map.put("transportApply-name", "");
		map.put("type-id", "");
		map.put("type-name", "");
		map.put("objType", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd HH:mm:ss");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd HH:mm:ss");
		// map.put("transUser-id", "");
		map.put("transUser", "");
		map.put("transCompany", "");
		map.put("transCode", "");
		map.put("linkman", "");
		map.put("phone", "");
		map.put("receiveCompany", "");
		map.put("address", "");
		map.put("emall", "");
		map.put("flightCode", "");
		map.put("willDate", "yyyy-MM-dd");
		map.put("arrived", "");
		map.put("fromDate", "yyyy-MM-dd");
		map.put("receiveDate", "yyyy-MM-dd");
		map.put("transFee", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("content1", "");
		map.put("content2", "");
		map.put("content3", "");
		map.put("content4", "");
		map.put("content5", "");
		map.put("content6", "");
		map.put("content7", "");
		map.put("content8", "");
		map.put("content9", "yyyy-MM-dd");
		map.put("content10", "yyyy-MM-dd");
		map.put("content11", "yyyy-MM-dd");
		map.put("content12", "yyyy-MM-dd");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "editTransportOrder")
	public String editTransportOrder() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			transportOrder = transportOrderService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "transportOrder");
		} else {
			transportOrder.setId("NEW");
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			transportOrder.setCreateUser(user);
			transportOrder.setCreateDate(new Date());
			transportOrder.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_NEW);
			transportOrder.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_NEW_NAME);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(transportOrder.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/tra/transport/transportOrderEdit.jsp");
	}

	@Action(value = "copyTransportOrder")
	public String copyTransportOrder() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		transportOrder = transportOrderService.get(id);
		transportOrder.setId("NEW");
		transportOrder.setState("3");
		transportOrder.setStateName("新建");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/tra/transport/transportOrderEdit.jsp");
	}

	@Action(value = "save")
	public void save() throws Exception {
		String mainData = getParameterFromRequest("mainData");
		String changeLog = getParameterFromRequest("changeLog");
		String itemData = getParameterFromRequest("itemData");
		String changeLogItem = getParameterFromRequest("changeLogItem");
		String transportSampleItem = getParameterFromRequest("transportSampleItem");
		String transportSampleChangeLog = getParameterFromRequest("transportSampleChangeLog");
		String sampleTransportState = getParameterFromRequest("sampleTransportState");
		String sampleTransportStateChangeLog = getParameterFromRequest("sampleTransportStateChangeLog");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String id = transportOrderService.save(mainData, changeLog, itemData, changeLogItem, transportSampleItem,
					transportSampleChangeLog, sampleTransportState, sampleTransportStateChangeLog);
			map.put("success", true);
			map.put("id", id);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "toViewTransportOrder")
	public String toViewTransportOrder() throws Exception {
		String id = getParameterFromRequest("id");
		transportOrder = transportOrderService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/tra/transport/transportOrderEdit.jsp");
	}

	@Action(value = "showTransportOrderCellList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTransportOrderCellList() throws Exception {
		String wid = getParameterFromRequest("wid");
		putObjToContext("wid", wid);
		return dispatcher("/WEB-INF/page/tra/transport/transportOrderCell.jsp");
	}

	/**
	 * 产品运输明细 @Title: showTransportOrderItemListJson @Description:
	 * TODO @param @return void @author 孙灵达 @date 2018年8月2日 @throws
	 */
	@Action(value = "showTransportOrderItemListJson")
	public void showTransportOrderItemListJson() {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String id = getParameterFromRequest("id");
		try {
			Map<String, Object> result = transportOrderService.showTransportOrderItemListJson(id, start, length, query,
					col, sort);
			List<TransportOrderCell> list = (List<TransportOrderCell>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("pronoun", "");
			map.put("sampleType", "");
			map.put("sampleOrder-crmCustomer-name", "");
			map.put("transportDate", "HH:mm");
			map.put("transportDate2", "HH:mm");
//			map.put("transportDate", "yyyy-MM-dd HH:mm");
//			map.put("transportDate2", "yyyy-MM-dd HH:mm");
			map.put("sampleOrder-name", "");
			map.put("doctorNotice", "");
			map.put("doctorReply", "");
			map.put("patientNotice", "");
			map.put("patientReply", "");
			map.put("address", "");
			map.put("shippingNo", "");
			map.put("signatory", "");
			map.put("signatureDate", "yyyy-MM-dd HH:mm");
			map.put("confirmationUser", "");
			map.put("confirmationDate", "yyyy-MM-dd HH:mm");
			map.put("freight", "");
			map.put("note", "");
			map.put("cellula", "");
			map.put("notification","");
			map.put("affirm","");
			map.put("sampleOrder-filtrateCode","");
			map.put("sampleOrder-round","");
			map.put("sampleOrder-barcode","");
			map.put("numTubes","");
			map.put("bloodSampling","");
			map.put("oddNumber", "");
			/*
			 * map.put("dicSampleType-id", ""); map.put("dicSampleType-name", "");
			 */
			map.put("numTubes", "");
			map.put("transportTemperature", "");

			map.put("state", "");

			map.put("tempId", "");
			map.put("sampleOrder-id", "");

			map.put("transportState", "");
			map.put("hospital", "");
			map.put("hospitalDepartment", "");
			map.put("doctor", "");
			map.put("hospitalTel", "");
			map.put("doctorTel", "");

			map.put("harvestDate", "yyyy-MM-dd HH:mm");
			map.put("cellDate", "yyyy-MM-dd HH:mm");
			map.put("endHarvestDate", "yyyy-MM-dd HH:mm");
			map.put("efficacious", "");
			map.put("phoneNumber","");
			map.put("transportCompany","");
			map.put("transfusion","");
			map.put("saline","");
			map.put("submitData", "");
			map.put("linkmanPhone","");
			map.put("linkman","");
			
			map.put("planState", "");
			
			map.put("reinfusionPlanDate", "");
			
			map.put("lowTemperature", "");//最低温度
			map.put("highTemperature", "");//最高温度
			map.put("avgTemperature", "");//平均温度
			map.put("equimentNumber", "");//保温箱型号
			map.put("recorder", "");//温度计型号
			map.put("personFollowCar", "");//跟车人员
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("TransportOrderCell");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delTransportOrderCell")
	public void delTransportOrderCell() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			transportOrderService.delTransportOrderCell(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showTransportOrderTissueList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTransportOrderTissueList() throws Exception {
		String wid = getParameterFromRequest("wid");
		putObjToContext("wid", wid);
		return dispatcher("/WEB-INF/page/tra/transport/transportOrderTissue.jsp");
	}

	@Action(value = "showTransportOrderTissueListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTransportOrderTissueListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			// String scId = getRequest().getParameter("wid");
			Map<String, Object> result = transportOrderService.findTransportOrderTissueList(scId, startNum, limitNum,
					dir, sort);
			Long total = (Long) result.get("total");
			List<TransportOrderTissue> list = (List<TransportOrderTissue>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			// map.put("transportOrder-name", "");
			// map.put("transportOrder-id", "");
			// map.put("birthDate", "");
			// map.put("id", "");
			// map.put("name", "");
			// map.put("animalMain-name", "");
			// map.put("animalMain-id", "");
			// map.put("name", "");
			// map.put("animalMain-name", "");
			// map.put("animalMain-id", "");
			// map.put("animalItem-id", "");
			// map.put("gender", "");
			map.put("id", "");
			map.put("tissueName", "");// 组织名称
			map.put("animalId", "");// 动物ID
			map.put("animalName", "");
			map.put("strain", "");// 品系
			map.put("gender", "");// 性别
			map.put("birthDate", "yyyy-MM-dd");// 出生日期
			map.put("sign", "");// 标记
			map.put("genetype", "");// 基因型
			map.put("transportTemperature", "");// 运输温度
			map.put("note", "");// 备注
			map.put("num", "");
			map.put("way", "");
			map.put("transportOrder-id", "");
			map.put("transportOrder-name", "");
			// map.put("transportOrder-project-id", "");
			// map.put("transportOrder-project-projectId", "");
			// map.put("num", "");
			// map.put("genetype", "");
			// map.put("animalItem-birthDate", "");
			// map.put("way", "");
			// map.put("transportApply-id", "");
			// map.put("transportApply-name", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delTransportOrderTissue")
	public void delTransportOrderTissue() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			transportOrderService.delTransportOrderTissue(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showTransportOrderBacteriaList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTransportOrderBacteriaList() throws Exception {
		String wid = getParameterFromRequest("wid");
		putObjToContext("wid", wid);
		return dispatcher("/WEB-INF/page/tra/transport/transportOrderBacteria.jsp");
	}

	@Action(value = "showTransportOrderBacteriaListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTransportOrderBacteriaListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			// String oederId = getRequest().getParameter("wid");
			String scId = getRequest().getParameter("id");
			// if (scId != null && !scId.equals("")) {
			// TransportApply sct = transportApplyDao.get(TransportApply.class,
			// scId);
			// TransportOrder mm = new TransportOrder();
			// mm.setObjType(sct.getObjType());
			// String type=sct.getObjType();
			// putObjToContext("transportOrder.objType", type);
			// }
			Map<String, Object> result = transportOrderService.findTransportOrderBacteriaList(scId, startNum, limitNum,
					dir, sort);
			// .findTransportApplyBacteriaList(scId, startNum, limitNum,dir,
			// sort);
			Long total = (Long) result.get("total");
			List<TransportOrderBacteria> list = (List<TransportOrderBacteria>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			// map.put("id", "");
			// map.put("objName", "");
			// map.put("projectName", "");
			// map.put("concentration", "");
			// map.put("volume", "");
			// map.put("antibody", "");
			// map.put("od260", "");
			// map.put("od230", "");
			map.put("id", "");
			map.put("objName", "");
			map.put("projectName", "");
			map.put("bacteriaName", "");// 菌液名称
			map.put("concentration", "");// 浓度
			map.put("volume", "");// 体积
			map.put("antibody", "");// 抗体
			map.put("od260", "");
			map.put("od230", "");
			map.put("od600", "");
			map.put("resistance", "");// 抗性
			map.put("transportTemperature", "");// 运输温度
			map.put("note", "");// 备注
			map.put("transportOrder-id", "");
			map.put("transportOrder-name", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delTransportOrderBacteria")
	public void delTransportOrderBacteria() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			transportOrderService.delTransportOrderBacteria(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showTransportOrderAnimalList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTransportOrderAnimalList() throws Exception {
		String wid = getParameterFromRequest("wid");
		putObjToContext("wid", wid);
		return dispatcher("/WEB-INF/page/tra/transport/transportOrderAnimal.jsp");
	}

	@Action(value = "showTransportOrderAnimalListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTransportOrderAnimalListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			// String scId = getRequest().getParameter("wid");
			String scId = getRequest().getParameter("id");
			// String oederId = getRequest().getParameter("wid");
			// if (oederId != null && !oederId.equals("")) {
			// TransportApply sct =
			// transportApplyDao.get(TransportApply.class,scId);
			// TransportOrder mm =
			// transportOrderDao.get(TransportOrder.class,oederId);
			// mm.setObjType(sct.getObjType());
			// transportOrderDao.saveOrUpdate(mm);
			// }
			Map<String, Object> result = transportOrderService.findTransportOrderAnimalList(scId, startNum, limitNum,
					dir, sort);

			Long total = (Long) result.get("total");
			List<TransportOrderAnimal> list = (List<TransportOrderAnimal>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			// map.put("animalMain-name", "");
			map.put("animalId", "");
			// map.put("animalItem-id", "");
			// map.put("animalItem-birthDate", "yyyy-MM-dd");
			// map.put("project-name", "");
			map.put("projectId", "");
			map.put("mark", "");
			map.put("genetype", "");
			map.put("gender", "");
			map.put("clean", "");
			map.put("num", "");
			map.put("project-id", "");
			map.put("project-name", "");
			map.put("project-projectId", "");
			map.put("birthDate", "yyyy-MM-dd");
			map.put("packBoxNo", "");
			map.put("paId", "");
			map.put("maId", "");
			map.put("transportOrder-id", "");
			map.put("transportOrder-name", "");
			// map.put("transportOrder-project-id", "");
			// map.put("transportOrder-project-name", "");
			// map.put("transportOrder-project-projectId", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delTransportOrderAnimal")
	public void delTransportOrderAnimal() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			transportOrderService.delTransportOrderAnimal(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 在编辑页面显示质粒子表
	@Action(value = "showTransportOrderPlasmidList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTransportOrderPlasmidList() throws Exception {
		String wid = getParameterFromRequest("wid");
		putObjToContext("wid", wid);
		return dispatcher("/WEB-INF/page/tra/transport/transportOrderPlasmid.jsp");
	}

	@Action(value = "showTransportOrderPlasmidListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTransportOrderPlasmidListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			// String scId = getRequest().getParameter("wid");
			String scId = getRequest().getParameter("id");
			// String oederId = getRequest().getParameter("wid");
			// if (oederId != null && !oederId.equals("")) {
			// TransportApply sct =
			// transportApplyDao.get(TransportApply.class,scId);
			// TransportOrder mm =
			// transportOrderDao.get(TransportOrder.class,oederId);
			// mm.setObjType(sct.getObjType());
			// transportOrderDao.saveOrUpdate(mm);
			// }
			Map<String, Object> result = transportOrderService.findTransportOrderPlasmidList(scId, startNum, limitNum,
					dir, sort);
			Long total = (Long) result.get("total");
			List<TransportOrderPlasmid> list = (List<TransportOrderPlasmid>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("projectName", "");
			map.put("plasmidName", "");
			map.put("concentration", "");
			map.put("volume", "");
			// map.put("animalItem-birthDate", "yyyy-MM-dd");
			map.put("antibody", "");
			map.put("resistance", "");
			map.put("od230", "");
			map.put("od260", "");
			map.put("transportTemperature", "");
			map.put("note", "");
			map.put("transportOrder-id", "");
			map.put("transportOrder-name", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "showTransportOrderAnimalAllList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTransportOrderAnimalAllList() throws Exception {
		// String wid = getParameterFromRequest("wid");
		// putObjToContext("wid", wid);
		return dispatcher("/WEB-INF/page/tra/transport/transportOrderAnimalAll.jsp");
	}

	@Action(value = "showTransportOrderBacteriaAllList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTransportOrderBacteriaAllList() throws Exception {
		return dispatcher("/WEB-INF/page/tra/transport/transportOrderBacteriaAll.jsp");
	}

	@Action(value = "showTransportOrderCellAllList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTransportOrderCellAllList() throws Exception {
		// String wid = getParameterFromRequest("wid");
		// putObjToContext("wid", wid);
		return dispatcher("/WEB-INF/page/tra/transport/transportOrderCellAll.jsp");
	}

	@Action(value = "showTransportOrderTissueAllList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTransportOrderTissueAllList() throws Exception {
		// String wid = getParameterFromRequest("wid");
		// putObjToContext("wid", wid);
		return dispatcher("/WEB-INF/page/tra/transport/transportOrderTissueAll.jsp");
	}

	@Action(value = "showTransportOrderPlasmidAllList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTransportOrderPlasmidAllList() throws Exception {
		// String wid = getParameterFromRequest("wid");
		// putObjToContext("wid", wid);
		return dispatcher("/WEB-INF/page/tra/transport/transportOrderPlasmidAll.jsp");
	}

	// 设置isDialog
	@Action(value = "setIsDialog")
	public void setIsDialog() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String idStr = getParameterFromRequest("idStr");
			String type = getParameterFromRequest("type");
			if (idStr != null && !idStr.equals("")) {
				transportOrderService.setIsDialog(type, idStr);
				map.put("success", true);
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public TransportOrderService getTransportOrderService() {
		return transportOrderService;
	}

	public void setTransportOrderService(TransportOrderService transportOrderService) {
		this.transportOrderService = transportOrderService;
	}

	public TransportOrder getTransportOrder() {
		return transportOrder;
	}

	public void setTransportOrder(TransportOrder transportOrder) {
		this.transportOrder = transportOrder;
	}

	// 由细胞复苏实验单查找 实验结果
	@Action(value = "setOrderList")
	public void setOrderList() throws Exception {
		String id = getRequest().getParameter("id");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.transportOrderService.setOrderAnimalList(id);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	@Action(value = "setOrderList1")
	public void setOrderList1() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.transportApplyService.setOrderBacList(code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// @Action(value = "setOrderList2")
	// public void setOrderList2() throws Exception {
	// String code = getRequest().getParameter("code");
	// Map<String, Object> result = new HashMap<String, Object>();
	// try {
	// List<Map<String, String>> dataListMap = this.transportApplyService
	// .setOrderCellList(code);
	// result.put("success", true);
	// result.put("data", dataListMap);
	//
	// } catch (Exception e) {
	// result.put("success", false);
	// }
	// HttpUtils.write(JsonUtils.toJsonString(result));
	// }
	//
	// @Action(value = "setOrderList3")
	// public void setOrderList3() throws Exception {
	// String code = getRequest().getParameter("code");
	// Map<String, Object> result = new HashMap<String, Object>();
	// try {
	// List<Map<String, String>> dataListMap = this.transportApplyService
	// .setOrderTissueList(code);
	// result.put("success", true);
	// result.put("data", dataListMap);
	//
	// } catch (Exception e) {
	// result.put("success", false);
	// }
	// HttpUtils.write(JsonUtils.toJsonString(result));
	// }

	@Action(value = "showTransportItemSampleTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTransportItemSampleTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = transportOrderService.showTransportItemSampleTableJson(start, length, query, col,
				sort, id);
		List<SampleTransportItem> list = (List<SampleTransportItem>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("orderNo", "");
		map.put("createUser-name", "");
		map.put("createUser-id", "");
		map.put("createDate", "yyyy-MM-dd HH:mm");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("planTime", "HH:mm");
		map.put("actualTime", "");
		map.put("invalidState", "");
		map.put("samplingDate", "yyyy-MM-dd HH:mm");
		map.put("sampleOrder-inspectionDepartment-name", "");
		map.put("sampleOrder-crmCustomer-name", "");
		map.put("sampleOrder-name", "");
		map.put("sampleOrder-lymphoidCellSeries", "");
		map.put("sampleOrder-heparinTube", "");
		map.put("sampleOrder-id", "");
//		map.put("floor", "");

		map.put("sampleOrder-attendingDoctor", "");
		map.put("linkmanPhone", "");
		map.put("linkman", "");
		map.put("sampleOrder-round", "");
		map.put("sampleOrder-sampleTypeName", "");
		map.put("bloodCollection", "");
		map.put("drawBloodTime", "yyyy-MM-dd HH:mm");
		map.put("address", "");
		map.put("approvalUser-id", "");
		map.put("acceptDate", "yyyy-MM-dd HH:mm");
		map.put("confirmationDate", "");
		map.put("freight", "");

		map.put("sampleType-id", "");
		map.put("sampleType-name", "");
		map.put("expressCompany-name", "");
		map.put("expressCompany-id","");
		map.put("phoneNumber","");
		map.put("sequenceNumber", "");
		map.put("transportTemperature", "");
		map.put("notification", "");
		map.put("affirm", "");
		map.put("sampleOrder-filtrateCode", "");
		map.put("bloodSampling", "");
		map.put("transfusion", "");
		map.put("saline", "");
		map.put("oddNumber", "");
		map.put("transportTemperature", "");

		map.put("personFollowCar", "");//跟车人员
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	@Action(value = "showTransportStateSampleTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTransportStateSampleTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = transportOrderService.showTransportStateSampleTableJson(start, length, query, col,
				sort, id);
		List<SampleTransportState> list = (List<SampleTransportState>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("transportOrder-id", "");
		map.put("id", "");
		map.put("shippingNo", "");
		map.put("signatory", "");
		map.put("signatureDate", "yyyy-MM-dd HH:mm");
		map.put("confirmationUser", "");
		map.put("confirmationDate", "yyyy-MM-dd HH:mm");
		map.put("freight", "");
		map.put("note", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	@Action(value = "delTransportSampleState")
	public void delTransportSampleState() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			transportOrderService.delTransportSampleState(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "delTransportSampleItem")
	public void delTransportSampleItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			transportOrderService.delTransportSampleItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "saveTransportSampleItemTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveTransportSampleItemTable() throws Exception {
		Map aMap = new HashMap();
		Map lMap = new HashMap();

		String id = getParameterFromRequest("id");
		transportOrder = commonDAO.get(TransportOrder.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		aMap.put("sampleTransportItem", getParameterFromRequest("dataJson"));
		lMap.put("sampleTransportItem", getParameterFromRequest("changeLog"));
		try {
			transportOrderService.saveTransportSampleItemTable(transportOrder, aMap, "", lMap);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "saveTransportSampleStateTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveTransportSampleStateTable() throws Exception {
		Map aMap = new HashMap();
		Map lMap = new HashMap();

		String id = getParameterFromRequest("id");
		transportOrder = commonDAO.get(TransportOrder.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		aMap.put("sampleTransportState", getParameterFromRequest("dataJson"));
		lMap.put("sampleTransportState", getParameterFromRequest("changeLog"));
		try {
			transportOrderService.saveTransportSampleStateTable(transportOrder, aMap, "", lMap);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showQualityTestTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showQualityTestTable() throws Exception {
		String code = getParameterFromRequest("code");
		putObjToContext("flag", code);
		return dispatcher("/WEB-INF/page/tra/transport/showQualityTestTable.jsp");
	}

	@Action(value = "showQualityTestTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showQualityTestTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String batch = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = transportOrderService.showQualityTestTableJson(start, length, query, col, sort,
				batch);
		List<QualityTestInfo> list = (List<QualityTestInfo>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("sampleCode", "");
		map.put("qualityTest-id", "");
		map.put("sampleOrder-id", "");
		map.put("productId", "");
		map.put("productName", "");
		map.put("sampleType", "");
		map.put("dicSampleType-id", "");
		map.put("dicSampleType-name", "");
		map.put("experimentalSteps", "");
		map.put("sampleDeteyion-name", "");
		map.put("orderId", "");
		map.put("result", "");
		map.put("note", "");
		map.put("mark", "");
		map.put("qualityTest-id", "");
		map.put("batch", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

//	选择订单后带入日期和QA审核人,运输公司
	@Action(value = "showDateAndUser", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDateAndUser() throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {
			// String[] ids = getRequest().getParameterValues("ids[]");
			String orderId = getParameterFromRequest("id");
			String[] arr = transportOrderService.showDateAndUser(orderId);
			map.put("success", true);
			map.put("user", arr[0]);
			map.put("date", arr[1]);
			map.put("company", arr[2]);
			map.put("companyId", arr[3]);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));

	}
	//提交
	@Action(value = "submitFunction")
	public void submitFunction() throws Exception {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String[] ids = getRequest().getParameterValues("id[]");
			
			Boolean panduan = transportOrderService.submitFunction(ids);
			map.put("success", true);
			if(panduan) {
				map.put("panduan", true);
			}else {
				map.put("panduan", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
}
