package com.biolims.tra.transport.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.experiment.cell.revive.model.CellReviveInfo;
import com.biolims.experiment.plasma.model.PlasmaReceiveItem;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.sample.model.SampleReceive;
import com.biolims.tra.sampletransport.model.SampleTransportItem;
import com.biolims.tra.sampletransport.model.SampleTransportState;
import com.biolims.tra.transport.dao.TransportApplyDao;
import com.biolims.tra.transport.dao.TransportOrderDao;
import com.biolims.tra.transport.model.TransportOrder;
import com.biolims.tra.transport.model.TransportOrderAnimal;
import com.biolims.tra.transport.model.TransportOrderBacteria;
import com.biolims.tra.transport.model.TransportOrderCell;
import com.biolims.tra.transport.model.TransportOrderPlasmid;
import com.biolims.tra.transport.model.TransportOrderTemp;
import com.biolims.tra.transport.model.TransportOrderTissue;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class TransportOrderService {
	@Resource
	private TransportOrderDao transportOrderDao;
	@Resource
	private TransportApplyDao transportApplyDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private CommonService commonService;
	// @Resource
	// private ProjectDao projectDao;
	// @Resource
	// private AnimalMainDao animalMainDao;
	// @Resource
	// private AnimalOutDao animalOutDao;
	@Autowired
	private TransportApplyService transportApplyService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findTransportOrderList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return transportOrderDao.selectTransportOrderList(mapForQuery, startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(TransportOrder i) throws Exception {

		transportOrderDao.saveOrUpdate(i);

	}

	public TransportOrder get(String id) {
		TransportOrder transportOrder = commonDAO.get(TransportOrder.class, id);
		return transportOrder;
	}

	public Map<String, Object> findTransportOrderTissueList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = transportOrderDao.selectTransportOrderTissueList(scId, startNum, limitNum, dir,
				sort);
		List<TransportOrderTissue> list = (List<TransportOrderTissue>) result.get("list");
		return result;
	}

	public Map<String, Object> findTransportOrderBacteriaList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		Map<String, Object> result = transportOrderDao.selectTransportOrderBacteriaList(scId, startNum, limitNum, dir,
				sort);
		List<TransportOrderBacteria> list = (List<TransportOrderBacteria>) result.get("list");
		return result;
	}

	public Map<String, Object> findTransportOrderAnimalList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = transportOrderDao.selectTransportOrderAnimalList(scId, startNum, limitNum, dir,
				sort);
		List<TransportOrderAnimal> list = (List<TransportOrderAnimal>) result.get("list");
		return result;
	}

	public Map<String, Object> findTransportOrderPlasmidList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		Map<String, Object> result = transportOrderDao.selectTransportOrderPlasmidList(scId, startNum, limitNum, dir,
				sort);
		List<TransportOrderPlasmid> list = (List<TransportOrderPlasmid>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveTransportOrderCell(TransportOrder sc, String itemDataJson) throws Exception {
		List<TransportOrderCell> saveItems = new ArrayList<TransportOrderCell>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			TransportOrderCell scp = new TransportOrderCell();
			// 将map信息读入实体类
			scp = (TransportOrderCell) transportOrderDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setTransportOrder(sc);

			saveItems.add(scp);
		}
		transportOrderDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delTransportOrderCell(String[] ids) throws Exception {
		for (String id : ids) {
			TransportOrderCell scp = transportOrderDao.get(TransportOrderCell.class, id);
			if (scp != null) {
				String tempId = scp.getTempId();
				transportOrderDao.delete(scp);
				if (tempId != null && !"".equals(tempId)) {
					TransportOrderTemp temp = commonDAO.get(TransportOrderTemp.class, tempId);
					if (temp != null) {
						temp.setState("1");
						commonDAO.saveOrUpdate(temp);
					}
				}
			}
			
			if (ids.length>0) {
				LogInfo li = new LogInfo();
				User user = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(user.getId());
				li.setLogDate(new Date());
				li.setFileId(scp.getTransportOrder().getId());
				li.setClassName("TransportOrder");
				li.setModifyContent("运输明细:"+"产品批号:"+scp.getSampleOrder().getBarcode()+"的数据已删除");
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveTransportOrderTissue(TransportOrder sc, String itemDataJson) throws Exception {
		List<TransportOrderTissue> saveItems = new ArrayList<TransportOrderTissue>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			TransportOrderTissue scp = new TransportOrderTissue();
			// 将map信息读入实体类
			scp = (TransportOrderTissue) transportOrderDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setTransportOrder(sc);

			saveItems.add(scp);
		}
		transportOrderDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delTransportOrderTissue(String[] ids) throws Exception {
		for (String id : ids) {
			TransportOrderTissue scp = transportOrderDao.get(TransportOrderTissue.class, id);
			transportOrderDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveTransportOrderBacteria(TransportOrder sc, String itemDataJson) throws Exception {
		List<TransportOrderBacteria> saveItems = new ArrayList<TransportOrderBacteria>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			TransportOrderBacteria scp = new TransportOrderBacteria();
			// 将map信息读入实体类
			scp = (TransportOrderBacteria) transportOrderDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setTransportOrder(sc);

			saveItems.add(scp);
		}
		transportOrderDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delTransportOrderBacteria(String[] ids) throws Exception {
		for (String id : ids) {
			TransportOrderBacteria scp = transportOrderDao.get(TransportOrderBacteria.class, id);
			transportOrderDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveTransportOrderAnimal(TransportOrder sc, String itemDataJson) throws Exception {
		List<TransportOrderAnimal> saveItems = new ArrayList<TransportOrderAnimal>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			TransportOrderAnimal scp = new TransportOrderAnimal();
			// 将map信息读入实体类
			scp = (TransportOrderAnimal) transportOrderDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setTransportOrder(sc);

			saveItems.add(scp);
		}
		transportOrderDao.saveOrUpdateAll(saveItems);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveTransportOrderPlasmid(TransportOrder sc, String itemDataJson) throws Exception {
		List<TransportOrderPlasmid> saveItems = new ArrayList<TransportOrderPlasmid>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			TransportOrderPlasmid scp = new TransportOrderPlasmid();
			// 将map信息读入实体类
			scp = (TransportOrderPlasmid) transportOrderDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals("")) {
				scp.setId(null);
			}
			scp.setTransportOrder(sc);
			saveItems.add(scp);
		}
		transportOrderDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delTransportOrderAnimal(String[] ids) throws Exception {
		for (String id : ids) {
			TransportOrderAnimal scp = transportOrderDao.get(TransportOrderAnimal.class, id);
			transportOrderDao.delete(scp);
		}
	}

	/*
	 * @WriteOperLogTable
	 * 
	 * @WriteExOperLog
	 * 
	 * @Transactional(rollbackFor = Exception.class) public void save(TransportOrder
	 * sc, Map jsonMap, String applyId, String orderId) throws Exception { if (sc !=
	 * null) { transportOrderDao.saveOrUpdate(sc); String jsonStr = ""; jsonStr =
	 * (String) jsonMap.get("transportOrderAnimal"); if (jsonStr != null &&
	 * !jsonStr.equals("{}") && !jsonStr.equals("")) { saveTransportOrderAnimal(sc,
	 * jsonStr); } jsonStr = (String) jsonMap.get("transportOrderTissue"); if
	 * (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
	 * saveTransportOrderTissue(sc, jsonStr); } jsonStr = (String)
	 * jsonMap.get("transportOrderBacteria"); if (jsonStr != null &&
	 * !jsonStr.equals("{}") && !jsonStr.equals("")) {
	 * saveTransportOrderBacteria(sc, jsonStr); } jsonStr = (String)
	 * jsonMap.get("transportOrderCell"); if (jsonStr != null &&
	 * !jsonStr.equals("{}") && !jsonStr.equals("")) { saveTransportOrderCell(sc,
	 * jsonStr); } jsonStr = (String) jsonMap.get("transportOrderPlasmid"); if
	 * (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
	 * saveTransportOrderPlasmid(sc, jsonStr); } } // if (applyId != null &&
	 * !applyId.equals("")) // setObjType(applyId, orderId); }
	 */

	public void setObjType(String applyId, String orderId) {
		// TransportApply sct = transportApplyDao.get(TransportApply.class, applyId);
		TransportOrder mm = transportOrderDao.get(TransportOrder.class, orderId);
		// mm.setObjType(sct.getObjType());
		transportOrderDao.saveOrUpdate(mm);
	}

	// 审批完成
	public void chengeState(String applicationTypeActionId, String id) throws Exception {
		TransportOrder sct = transportOrderDao.get(TransportOrder.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		sct.setConfirmUser(user);
		sct.setConfirmDate(new Date());
		transportOrderDao.update(sct);
		List<SampleTransportItem> stiList = new ArrayList<SampleTransportItem>();
		stiList = transportOrderDao.showSampleTransportItemList(id);
		if (!stiList.isEmpty()) {
			for (SampleTransportItem sti : stiList) {
				if (!"1".equals(sti.getInvalidState())) {
					List<SampleOrderItem> soiList = new ArrayList<SampleOrderItem>();
					soiList = transportOrderDao.showSampleOrderItem(sti.getOrderNo());
					if (!soiList.isEmpty()) {
						for (SampleOrderItem soi : soiList) {
							soi.setSamplingDate(sti.getSamplingDate());
							commonDAO.saveOrUpdate(soi);
						}
					}
				} else {
					SampleOrder so = new SampleOrder();
					so = commonDAO.get(SampleOrder.class, sti.getOrderNo());
					if (so.getBarcode() != null && !"".equals(so.getBarcode())) {
						so.setBarcode("X-" + so.getBarcode());
					}
					if (so.getFiltrateCode() != null && !"".equals(so.getFiltrateCode())) {
						so.setFiltrateCode("X-" + so.getFiltrateCode());
					}
					if (so.getRound() != null && !"".equals(so.getRound())) {
						so.setRound("X-" + so.getRound());
					}
					if (so.getCcoi() != null && !"".equals(so.getCcoi())) {
						so.setCcoi("X-" + so.getCcoi());
					}
					so.setState("1");
					so.setStateName("作废");
					commonDAO.saveOrUpdate(so);
				}
			}
		}
		// if(sct.getObjType().equals("0")){//活体运输时
		// //获取运输子表的鼠ID改变老鼠的状态
		// Map<String, Object>
		// result=transportOrderDao.selectTransportOrderAnimalList(id, null, null, null,
		// null);
		// List<TransportOrderAnimal> list = (List<TransportOrderAnimal>) result
		// .get("list");
		// for (TransportOrderAnimal toa : list) {
		// if(toa.getAnimalId().indexOf("-")>0){
		// AnimalMain am = animalMainDao.get(AnimalMain.class, toa.getAnimalId());
		// if(am!=null){
		// am.setAnimalState("已运输");//动物状态设置成
		// am.setCageCode("");//笼位置空
		// }
		// }
		// }
		//
		// //出库单状态改为完成
		// AnimalOut ao = animalOutDao.get(AnimalOut.class, sct.getOutOrder());
		// if(ao!=null){
		// ao.setState("1");
		// ao.setStateName("完成");
		// }
		// }
		// Project sc = projectDao.get(Project.class, sct.getProject().getId());
		// saveToProgressReal(sc, id);
	}

	// @WriteOperLog
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void saveToProgressReal(Project sc, String id) {
	// ProjectProgressReal ppl = new ProjectProgressReal();
	// TransportOrder sct = transportOrderDao.get(TransportOrder.class, id);
	// ppl.setCode(sct.getId());
	// ppl.setProgress(sct.getName());
	// ppl.setEndDate(sct.getConfirmDate());
	// String modleName = "TransportOrder";
	// ppl.setStartDate(sct.getCreateDate());
	// // ppl.setProjectProgress(sct.getProjectProgress());
	// try {
	// ppl.setType(projectDao.selectApplicationTypeTableName(modleName));
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// ppl.setTypeCode(modleName);
	// ppl.setProject(sc);
	// ppl.setConfirmUser(sct.getCreateUser());
	// projectDao.saveOrUpdate(ppl);
	// }
	public void setIsDialog(String type, String idStr) throws Exception {
		transportApplyService.chengeIsDialog(type, idStr);
	}

	// 根据动物出库带出明细数据
	public List<Map<String, String>> setOrderAnimalList(String sid) throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = transportOrderDao.selectAnimalOutItemList(sid, null, null, null, null);
		List<CellReviveInfo> list = (List<CellReviveInfo>) result.get("list");

		if (list != null && list.size() > 0) {
			for (CellReviveInfo srai : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("code", srai.getCode());// 样本编号
				map.put("sampleCode", srai.getSampleCode());// 原始样本编号
				map.put("pronoun", srai.getPronoun());// 代次

				map.put("dicSampleTypeId", srai.getDicSampleType().getId());// 样本类型id
				map.put("dicSampleTypeName", srai.getDicSampleType().getName());// 样本类型名称
				// map.put("genetype", srai.getGenetype());
				// map.put("mark", srai.getMark());
				mapList.add(map);
			}
		}
		return mapList;
	}

	public Map<String, Object> showTransportOrderListJson(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return transportOrderDao.showTransportOrderListJson(start, length, query, col, sort);
	}

	public Map<String, Object> showTransportOrderItemListJson(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> result = transportOrderDao.showTransportOrderItemListJson(id, start, length, query, col, sort);
		if(result.get("list")!=null) {
			List<TransportOrderCell> list = (List<TransportOrderCell>) result.get("list");
			if(list.size()>0) {
				for(TransportOrderCell toc:list) {
					if(toc.getSampleCode()!=null
							&&!"".equals(toc.getSampleCode())) {
						List<PlasmaReceiveItem> pris = transportOrderDao.showPlasmaReceiveItems(toc.getSampleCode());
						if(pris.size()>0) {
							PlasmaReceiveItem pri = pris.get(0);
							if(pri!=null) {
								toc.setNumTubes(pri.getCellNumber());
							}
						}
					}
				}
				result.put("list", list);
			}
		}
		return result;//transportOrderDao.showTransportOrderItemListJson(id, start, length, query, col, sort);
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String save(String mainData, String changeLog, String itemData, String changeLogItem,
			String transportSampleItem, String transportSampleChangeLog, String sampleTransportState,
			String sampleTransportStateChangeLog) throws Exception {
		mainData = "[" + mainData + "]";
		List<Map<String, Object>> mainList = JsonUtils.toListByJsonArray(mainData, List.class);
		TransportOrder transportOrder = new TransportOrder();
		transportOrder = (TransportOrder) commonDAO.Map2Bean(mainList.get(0), transportOrder);
		String id = transportOrder.getId();
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		transportOrder.setCreateUser(u);
		String log = "";
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			log = "123";
			String modelName = "TransportOrder";
			String markCode = "TRO";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			transportOrder.setId(autoID);
		}
		commonDAO.saveOrUpdate(transportOrder);
		String mainId = transportOrder.getId();
		if (changeLog != null && !"".equals(changeLog)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			if (u != null) {
				li.setUserId(u.getId());
			}
			li.setFileId(id);
			li.setClassName("TransportOrder");
			li.setModifyContent(changeLog);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			commonDAO.saveOrUpdate(li);
		}
		if (itemData != null && !itemData.equals("{}") && !itemData.equals("")) {
			saveTransportOrderItem(transportOrder, itemData, changeLogItem,log);
		}

		if (transportSampleItem != null && !transportSampleItem.equals("{}") && !transportSampleItem.equals("")) {
			saveTransportSampleItemTable(transportOrder, transportSampleItem, transportSampleChangeLog,log);
		}

		if (sampleTransportState != null && !sampleTransportState.equals("{}") && !sampleTransportState.equals("")) {
			saveSampleTransportStateTable(transportOrder, sampleTransportState, sampleTransportStateChangeLog,log);
		}
		return mainId;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveSampleTransportStateTable(TransportOrder transportOrder, String sampleTransportState,
			String sampleTransportStateChangeLog,String log) throws Exception {
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(sampleTransportState, List.class);
		for (Map<String, Object> map : list) {
			// 将map信息读入实体类
			SampleTransportState scp = new SampleTransportState();
			scp = (SampleTransportState) commonDAO.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals("")) {
				scp.setId(null);
			}
			scp.setTransportOrder(transportOrder);
			commonDAO.saveOrUpdate(scp);
		}
		if (sampleTransportStateChangeLog != null && !"".equals(sampleTransportStateChangeLog)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setUserId(u.getId());
			li.setClassName("TransportOrder");
			li.setFileId(transportOrder.getId());
			li.setModifyContent(sampleTransportStateChangeLog);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			commonDAO.saveOrUpdate(li);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveTransportSampleItemTable(TransportOrder transportOrder, String transportSampleItem,
			String transportSampleChangeLog,String log) throws Exception {
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(transportSampleItem, List.class);
		for (Map<String, Object> map : list) {
			// 将map信息读入实体类
			SampleTransportItem scp = new SampleTransportItem();
			scp = (SampleTransportItem) commonDAO.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals("")) {
				scp.setId(null);
				scp.setCreateUser(u);
			}
			scp.setTransportOrder(transportOrder);

			if (scp.getSampleOrder() != null) {
				List<SampleOrderItem> sois = commonService.get(SampleOrderItem.class, "sampleOrder.id",
						scp.getSampleOrder().getId());
				if (sois.size() > 0) {
					SampleOrderItem soi = sois.get(0);
					scp.setSampleType(soi.getSampleType());
				} else {
					scp.setSampleType(null);
				}
			} else {
				scp.setSampleType(null);
			}

			commonDAO.saveOrUpdate(scp);
		}
		if (transportSampleChangeLog != null && !"".equals(transportSampleChangeLog)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setUserId(u.getId());
			li.setClassName("TransportOrder");
			li.setFileId(transportOrder.getId());
			li.setModifyContent(transportSampleChangeLog);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			commonDAO.saveOrUpdate(li);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveTransportOrderItem(TransportOrder transportOrder, String itemData, String changeLogItem,String log)
			throws Exception {
		List<TransportOrderCell> saveItems = new ArrayList<TransportOrderCell>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemData, List.class);
		for (Map<String, Object> map : list) {
			// 将map信息读入实体类
			TransportOrderCell scp = new TransportOrderCell();
			scp = commonDAO.get(TransportOrderCell.class,String.valueOf(map.get("id")));
			scp = (TransportOrderCell) commonDAO.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals("")) {
				scp.setId(null);
			}
			scp.setTransportOrder(transportOrder);

			saveItems.add(scp);
		}
		commonDAO.saveOrUpdateAll(saveItems);
		if (changeLogItem != null && !"".equals(changeLogItem)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setClassName("TransportOrder");
			li.setFileId(transportOrder.getId());
			li.setModifyContent(changeLogItem);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			commonDAO.saveOrUpdate(li);
		}
	}

	public Map<String, Object> showTransportOrderTempList(Integer start, Integer length, String query, String col,
			String sort, String flag) throws Exception {
		return transportOrderDao.showTransportOrderTempList(start, length, query, col, sort, flag);
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String addTransportOrderMakeUp(String mainJson, String[] ids) throws Exception {
		mainJson = "[" + mainJson + "]";
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(mainJson, List.class);
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		TransportOrder to = new TransportOrder();
		if (list.size() > 0) {
			to = (TransportOrder) commonDAO.Map2Bean(list.get(0), to);
		}
		if ((to.getId() != null && to.getId().equals("")) || to.getId().equals("NEW")) {
			String modelName = "TransportOrder";
			String markCode = "TRO";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			to.setId(autoID);
			to.setCreateUser(u);
		}
		commonDAO.saveOrUpdate(to);
		for (String tempId : ids) {
			TransportOrderTemp tt = commonDAO.get(TransportOrderTemp.class, tempId);
//			if (tt.getPlanState() != null && "1".equals(tt.getPlanState())) {
			TransportOrderCell item = new TransportOrderCell();
			item.setSampleCode(tt.getSampleCode());
			item.setCode(tt.getCode());
			item.setPronoun(tt.getPronoun());
			item.setSampleType(tt.getSampleType());
			item.setTempId(tt.getId());
			item.setTransportOrder(to);
			item.setSampleOrder(tt.getSampleOrder());
			item.setHospitalDepartment(tt.getSampleOrder().getInspectionDepartment().getName());

			
			item.setReinfusionPlanDate(tt.getReinfusionPlanDate());
			item.setState("2");
			commonDAO.saveOrUpdate(item);
			tt.setState("2");
			commonDAO.saveOrUpdate(tt);
			
			String kucun = "待运输样本:"+"产品批号:"+tt.getSampleOrder().getBarcode()+"的数据已添加到运输明细";
			if (kucun != null && !"".equals(kucun)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				li.setModifyContent(kucun);
				li.setUserId(u.getId());
				li.setFileId(to.getId());
				li.setClassName("TransportOrder");
				li.setState("3");
				li.setStateName("数据修改");
				commonDAO.saveOrUpdate(li);
			}
		}
		return to.getId();

	}

	public Map<String, Object> showTransportItemSampleTableJson(Integer start, Integer length, String query, String col,
			String sort, String id) {
		return transportOrderDao.showTransportItemSampleTableJson(start, length, query, col, sort, id);
	}

	public Map<String, Object> showTransportStateSampleTableJson(Integer start, Integer length, String query,
			String col, String sort, String id) {
		return transportOrderDao.showTransportStateSampleTableJson(start, length, query, col, sort, id);
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delTransportSampleState(String[] ids) {
		for (String id : ids) {
			SampleTransportState scp = commonDAO.get(SampleTransportState.class, id);
			commonDAO.delete(scp);
			if (ids.length > 0) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(scp.getTransportOrder().getId());
				li.setClassName("TransportOrder");
				li.setModifyContent("SampleTransportState删除信息" + scp.getTransportOrder().getId());
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}
		}

	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delTransportSampleItem(String[] ids) {
		for (String id : ids) {
			SampleTransportItem scp = commonDAO.get(SampleTransportItem.class, id);
			commonDAO.delete(scp);
			if (ids.length > 0) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setClassName("TransportOrder");
				li.setFileId(scp.getTransportOrder().getId());
				li.setModifyContent("来源样本运输:"+"订单编号:"+scp.getOrderNo()+"的数据已删除");
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}
		}

	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveTransportSampleItemTable(TransportOrder transportOrder, Map jsonMap, String logInfo, Map logMap)
			throws Exception {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String jsonStr = "";
		String logStr = "";
		logStr = (String) logMap.get("sampleTransportItem");
		jsonStr = (String) jsonMap.get("sampleTransportItem");
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(jsonStr, List.class);
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		for (Map<String, Object> map : list) {
			SampleTransportItem scp = new SampleTransportItem();
			// 将map信息读入实体类
			scp = (SampleTransportItem) commonDAO.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals("")) {
				scp.setId(null);
				scp.setCreateUser(u);
				scp.setCreateDate(format.parse(format.format(new Date())));

				String orderNo = scp.getOrderNo();
				SampleOrder sampleOrder = new SampleOrder();
				sampleOrder = commonDAO.get(SampleOrder.class, orderNo);
				scp.setDrawBloodTime(sampleOrder.getDrawBloodTime());
			} else {
				String orderNo = scp.getOrderNo();
				SampleOrder so = new SampleOrder();
				so = commonDAO.get(SampleOrder.class, orderNo);
				so.setDrawBloodTime(scp.getDrawBloodTime());
				commonDAO.saveOrUpdate(so);
			}

			if (scp.getSampleOrder() != null) {
				List<SampleOrderItem> sois = commonService.get(SampleOrderItem.class, "sampleOrder.id",
						scp.getSampleOrder().getId());
				if (sois.size() > 0) {
					SampleOrderItem soi = sois.get(0);
					scp.setSampleType(soi.getSampleType());
				} else {
					scp.setSampleType(null);
				}
			} else {
				scp.setSampleType(null);
			}

			scp.setTransportOrder(transportOrder);
			commonDAO.saveOrUpdate(scp);
			
			if (logStr != null && !"".equals(logStr)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				li.setUserId(u.getId());
				li.setClassName("TransportOrder");
				li.setFileId(transportOrder.getId());
				li.setModifyContent("样本运输："+"订单编号"+scp.getOrderNo()+"已保存");
				li.setState("3");
				li.setStateName("数据修改");
				commonDAO.saveOrUpdate(li);
			}
		}
		
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveTransportSampleStateTable(TransportOrder transportOrder, Map jsonMap, String logInfo, Map logMap)
			throws Exception {
		String jsonStr = "";
		String logStr = "";
		logStr = (String) logMap.get("sampleTransportState");
		jsonStr = (String) jsonMap.get("sampleTransportState");
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(jsonStr, List.class);
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		for (Map<String, Object> map : list) {
			SampleTransportState scp = new SampleTransportState();
			// 将map信息读入实体类
			scp = (SampleTransportState) commonDAO.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals("")) {
				scp.setId(null);
//				scp.setCreateUser(u);
			}
			scp.setTransportOrder(transportOrder);
			commonDAO.saveOrUpdate(scp);
		}
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setUserId(u.getId());
			li.setClassName("TransportOrder");
			li.setFileId(transportOrder.getId());
			li.setModifyContent(logStr);
			li.setState("3");
			li.setStateName("数据修改");
			commonDAO.saveOrUpdate(li);
		}
	}

	public Map<String, Object> showQualityTestTableJson(Integer start, Integer length, String query, String col,
			String sort, String batch) {
		return transportOrderDao.showQualityTestTableJson(start, length, query, col, sort, batch);
	}

	public List<TransportOrderCell> getTransportOrderCellsByIdAndCode(String id, String sampleCode) {
		return transportOrderDao.getTransportOrderCellsByIdAndCode(id, sampleCode);
	}

	// 选择订单后带入日期和QA审核人,运输公司
	public String[] showDateAndUser(String orderId) {
		List<SampleReceive> sReceive = new ArrayList<SampleReceive>();
		sReceive = transportOrderDao.showDateAndUser(orderId);
		String[] arr = new String[4];
		if (!sReceive.isEmpty()) {
			// 签收人
			if (sReceive.get(0).getApprovalUser() != null) {
				arr[0] = sReceive.get(0).getApprovalUser().getId();
			} else {
				arr[0] = "";
			}
			// 运输公司
			if (sReceive.get(0).getExpressCompany() != null) {
				arr[2] = sReceive.get(0).getExpressCompany().getName();
				arr[3] = sReceive.get(0).getExpressCompany().getId();
			} else {
				arr[2] = "";
				arr[3] = "";
			}
			// 签收日期
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			arr[1] = sdf.format(sReceive.get(0).getAcceptDate());
		}
		return arr;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveTransportOrderCells(TransportOrderCell sri) {
		sri.setTransportState("1");
		commonDAO.saveOrUpdate(sri);
		if(sri.getCode()!=null
				&&!"".equals(sri.getCode())) {
			List<SampleOrder> sos = commonService.get(SampleOrder.class, "barcode", sri.getCode());
			if(sos.size()>0) {
				for(SampleOrder so:sos) {
					so.setBatchState("4");
					so.setBatchStateName("运输完成");
					commonDAO.merge(so);
				}
			}
		}
	}
	//提交	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Boolean submitFunction(String[] ids) {
		Boolean panduan = true;
		String isd = "";
		for(String id:ids) {
			if(id!=null
					&&!"".equals(id)) {
				if("".equals(isd)) {
					isd = isd +  "'"+ id + "'";
				}else {
					isd = isd + ",'"+ id + "'";
				}
			}
		}
		List<TransportOrderCell> tocs = transportOrderDao.getTransportOrderCellsByids(isd);
		if(tocs.size()>0) {
			panduan = false;
		}
		if(panduan) {
			for(String id:ids) {
				if(id!=null
						&&!"".equals(id)) {
					TransportOrderCell toc = commonDAO.get(TransportOrderCell.class, id);
					if(toc!=null) {
						toc.setSubmitData("是");
						commonDAO.saveOrUpdate(toc);
					}
					
					if (ids.length>0) {
						LogInfo li = new LogInfo();
						User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
						li.setLogDate(new Date());
						li.setUserId(u.getId());
						li.setClassName("TransportOrder");
						li.setFileId(toc.getTransportOrder().getId());
						li.setModifyContent("运输明细："+toc.getSampleOrder().getBarcode()+"已提交");
						li.setState("3");
						li.setStateName("数据修改");
						commonDAO.saveOrUpdate(li);
					}
				}
			}
		}
		
		return panduan;
		
	}

	public List<PlasmaReceiveItem> getPlasmaReceiveItemByCode(String sampleCode) {
		return transportOrderDao.getPlasmaReceiveItemByCode(sampleCode);
	}
	
	public List<PlasmaReceiveItem> getPlasmaReceiveItemByCode1(String sampleCode) {
		return transportOrderDao.getPlasmaReceiveItemByCode1(sampleCode);
	}

}
