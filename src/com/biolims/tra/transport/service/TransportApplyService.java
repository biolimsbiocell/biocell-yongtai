package com.biolims.tra.transport.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.tra.transport.dao.TransportApplyDao;
import com.biolims.tra.transport.model.TransportApply;
import com.biolims.tra.transport.model.TransportApplyAnimal;
import com.biolims.tra.transport.model.TransportApplyBacteria;
import com.biolims.tra.transport.model.TransportApplyCell;
import com.biolims.tra.transport.model.TransportApplyTemp;
import com.biolims.tra.transport.model.TransportApplyTissue;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class TransportApplyService {
	@Resource
	private TransportApplyDao transportApplyDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;

	StringBuffer json = new StringBuffer();

	public Map<String, Object> findTransportApplyList(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return transportApplyDao.selectTransportApplyList(start, length, query, col, sort);
	}

	public Map<String, Object> findDialogTransportApplyList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return transportApplyDao.selectDialogTransportApplyList(mapForQuery, startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(TransportApply i) throws Exception {

		transportApplyDao.saveOrUpdate(i);

	}

	public TransportApply get(String id) {
		TransportApply transportApply = commonDAO.get(TransportApply.class, id);
		return transportApply;
	}

	public Map<String, Object> findTransportApplyAnimalList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = transportApplyDao.selectTransportApplyAnimalList(scId, startNum, limitNum, dir,
				sort);
		List<TransportApplyAnimal> list = (List<TransportApplyAnimal>) result.get("list");
		return result;
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String addTransportOrderMakeUp(String mainJson, String[] ids) throws Exception {
		mainJson = "[" + mainJson + "]";
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(mainJson, List.class);
		TransportApply to = new TransportApply();
		to = (TransportApply) commonDAO.Map2Bean(list.get(0), to);
		if ((to.getId() != null && to.getId().equals("")) || to.getId().equals("NEW")) {
			String modelName = "TransportApply";
			String markCode = "TRO";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			to.setId(autoID);
		}
		commonDAO.saveOrUpdate(to);
		for (String tempId : ids) {
			TransportApplyTemp tt = commonDAO.get(TransportApplyTemp.class, tempId);
			TransportApplyCell item = new TransportApplyCell();
			item.setSampleOrder(tt.getSampleOrder());
			// item.setSampleCode(tt.getSampleCode());
			// item.setCode(tt.getCode());
			// item.setPronoun(tt.getPronoun());
			// item.setSampleType(tt.getSampleType());
			item.setTempId(tt.getId());
			item.setTransportApply(to);
			commonDAO.saveOrUpdate(item);
			tt.setState("2");
			tt.setPlanState("2");
			commonDAO.saveOrUpdate(tt);
		}
		return to.getId();

	}

	public Map<String, Object> findTransportApplyBacteriaList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		Map<String, Object> result = transportApplyDao.selectTransportApplyBacteriaList(scId, startNum, limitNum, dir,
				sort);
		List<TransportApplyBacteria> list = (List<TransportApplyBacteria>) result.get("list");
		return result;
	}

	public Map<String, Object> findTransportApplyCellList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = transportApplyDao.selectTransportApplyCellList(scId, startNum, limitNum, dir,
				sort);
		List<TransportApplyCell> list = (List<TransportApplyCell>) result.get("list");
		return result;
	}

	public Map<String, Object> findTransportApplyTissueList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = transportApplyDao.selectTransportApplyTissueList(scId, startNum, limitNum, dir,
				sort);
		List<TransportApplyTissue> list = (List<TransportApplyTissue>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveTransportApplyAnimal(TransportApply sc, String itemDataJson) throws Exception {
		List<TransportApplyAnimal> saveItems = new ArrayList<TransportApplyAnimal>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			TransportApplyAnimal scp = new TransportApplyAnimal();
			// 将map信息读入实体类
			scp = (TransportApplyAnimal) transportApplyDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setTransportApply(sc);

			saveItems.add(scp);
		}
		transportApplyDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delTransportApplyAnimal(String[] ids) throws Exception {
		for (String id : ids) {
			TransportApplyAnimal scp = transportApplyDao.get(TransportApplyAnimal.class, id);
			transportApplyDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveTransportApplyBacteria(TransportApply sc, String itemDataJson) throws Exception {
		List<TransportApplyBacteria> saveItems = new ArrayList<TransportApplyBacteria>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			TransportApplyBacteria scp = new TransportApplyBacteria();
			// 将map信息读入实体类
			scp = (TransportApplyBacteria) transportApplyDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setTransportApply(sc);

			saveItems.add(scp);
		}
		transportApplyDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delTransportApplyBacteria(String[] ids) throws Exception {
		for (String id : ids) {
			TransportApplyBacteria scp = transportApplyDao.get(TransportApplyBacteria.class, id);
			transportApplyDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveTransportApplyCell(TransportApply sc, String itemDataJson) throws Exception {
		List<TransportApplyCell> saveItems = new ArrayList<TransportApplyCell>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			TransportApplyCell scp = new TransportApplyCell();
			// 将map信息读入实体类
			scp = (TransportApplyCell) transportApplyDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setTransportApply(sc);

			saveItems.add(scp);
		}
		transportApplyDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delTransportApplyCell(String[] ids) throws Exception {
		for (String id : ids) {
			TransportApplyCell scp = transportApplyDao.get(TransportApplyCell.class, id);
			String tempId = scp.getTempId();
			TransportApplyTemp tat = new TransportApplyTemp();
			tat = commonDAO.get(TransportApplyTemp.class, tempId);
			if (tat != null) {
				tat.setState("1");
				tat.setPlanState("1");
				transportApplyDao.saveOrUpdate(tat);
				transportApplyDao.delete(scp);
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveTransportApplyTissue(TransportApply sc, String itemDataJson) throws Exception {
		List<TransportApplyTissue> saveItems = new ArrayList<TransportApplyTissue>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			TransportApplyTissue scp = new TransportApplyTissue();
			// 将map信息读入实体类
			scp = (TransportApplyTissue) transportApplyDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setTransportApply(sc);

			saveItems.add(scp);
		}
		transportApplyDao.saveOrUpdateAll(saveItems);
	}

	// 审批完成
	public void chengeState(String applicationTypeActionId, String id) {
		// TransportApply sct = transportApplyDao.get(TransportApply.class, id);
		// sct.setState(SystemConstants.WORK_FLOW_COMPLETE);
		// sct.setStateName(SystemConstants.WORK_FLOW_COMPLETE_NAME);
		//
		// User user = (User) ServletActionContext.getRequest().getSession()
		// .getAttribute(SystemConstants.USER_SESSION_KEY);
		//
		// try {
		//
		// if (sct.getObjType().equals("0")) {// 活体运输
		// String bsId = projectService.findAutoID("AnimalOut", 5);
		// AnimalOut ao = new AnimalOut();// 主表
		// ao.setId(bsId);
		// ao.setProject(sct.getProject());
		// ao.setCreateDate(new Date());
		// ao.setCreateUser(user);
		// ao.setUsage("运输");
		// ao.setState(SystemConstants.WORK_FLOW_NEW);
		// ao.setStateName(SystemConstants.WORK_FLOW_NEW_NAME);
		// animalOutDao.saveOrUpdate(ao);
		// 子表
		// Map<String, Object> result = transportApplyDao
		// .selectTransportApplyAnimalList(id, null, null, null,
		// null);
		// List<TransportApplyAnimal> list = (List<TransportApplyAnimal>) result
		// .get("list");
		// for (TransportApplyAnimal taa : list) {
		// AnimalOutItem aoi = new AnimalOutItem();
		// aoi.setAnimalId(taa.getAnimalId());
		// aoi.setGender(taa.getGender());
		// aoi.setGenetype(taa.getGenetype());
		// aoi.setBirthDate(taa.getBirthDate());
		// aoi.setMark(taa.getMark());
		// aoi.setClean(taa.getClean());
		// aoi.setAnimalOut(ao);
		// animalOutDao.saveOrUpdate(aoi);
		// }
		// }

		// String obName[] = sct.getNextStep().split(",");
		// String nextTest = "";
		// for (int i = 0; i < obName.length; i++) {
		// if (obName[i].equals("TransportOrder")) {// 运输
		// String bsId = projectService
		// .findAutoID("TransportOrder", 5);
		// TransportOrder to = new TransportOrder();// 主表
		// to.setId(bsId);
		// to.setProject(sct.getProject());
		// to.setLastStep(sct.getId());
		// to.setObjType(sct.getObjType());
		// to.setLastStepName("运输申请");
		// to.setCreateDate(new Date());
		// to.setCreateUser(user);
		// to.setState(SystemConstants.WORK_FLOW_NEW);
		// to.setStateName(SystemConstants.WORK_FLOW_NEW_NAME);
		// transportOrderDao.saveOrUpdate(to);
		// // 子表
		// if (sct.getObjType().equals("0")) {// 活体运输
		// Map<String, Object> result = transportApplyDao
		// .selectTransportApplyAnimalList(id, null, null,
		// null, null);
		// List<TransportApplyAnimal> list = (List<TransportApplyAnimal>)
		// result
		// .get("list");
		// for (TransportApplyAnimal taa : list) {
		// TransportOrderAnimal toa = new TransportOrderAnimal();
		// toa.setAnimalId(taa.getAnimalId());
		// toa.setGender(taa.getGender());
		// toa.setGenetype(taa.getGenetype());
		// toa.setBirthDate(taa.getBirthDate());
		// toa.setMark(taa.getMark());
		// toa.setClean(taa.getClean());
		// toa.setTransportOrder(to);
		// transportOrderDao.saveOrUpdate(toa);
		// }
		// }
		//
		// continue;
		// }
		// }
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
		//
		// Project sc = projectDao.get(Project.class, sct.getProject().getId());
		// saveToProgressReal(sc, id);
	}

	// @WriteOperLog
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void saveToProgressReal(Project sc, String id) {
	// ProjectProgressReal ppl = new ProjectProgressReal();
	// TransportApply sct = transportApplyDao.get(TransportApply.class, id);
	// ppl.setCode(sct.getId());
	// ppl.setProgress(sct.getName());
	// ppl.setEndDate(sct.getConfirmDate());
	// String modleName = "TransportApply";
	// ppl.setStartDate(sct.getCreateDate());
	// // ppl.setProjectProgress(sct.getProjectProgress());
	// try {
	// ppl.setType(projectDao.selectApplicationTypeTableName(modleName));
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// ppl.setTypeCode(modleName);
	// ppl.setProject(sc);
	// ppl.setConfirmUser(sct.getCreateUser());
	// projectDao.saveOrUpdate(ppl);
	// }

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delTransportApplyTissue(String[] ids) throws Exception {
		for (String id : ids) {
			TransportApplyTissue scp = transportApplyDao.get(TransportApplyTissue.class, id);
			transportApplyDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(TransportApply sc, Map jsonMap) throws Exception {
		if (sc != null) {
			transportApplyDao.saveOrUpdate(sc);

			String jsonStr = "";
			// jsonStr = (String) jsonMap.get("transportApplyAnimal");
			// if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
			// saveTransportApplyAnimal(sc, jsonStr);
			//
			// String[] temp = jsonStr.split("},");
			// String isDialog = temp.length + "";
			// sc.setIsDialog(isDialog);
			// transportApplyDao.saveOrUpdate(sc);
			// }
			// jsonStr = (String) jsonMap.get("transportApplyBacteria");
			// if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
			// saveTransportApplyBacteria(sc, jsonStr);
			//
			// String[] temp = jsonStr.split("},");
			// String isDialog = temp.length + "";
			// sc.setIsDialog(isDialog);
			// transportApplyDao.saveOrUpdate(sc);
			// }
			jsonStr = (String) jsonMap.get("itemData");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveTransportApplyCell(sc, jsonStr);

				// String[] temp = jsonStr.split("},");
				// String isDialog = temp.length + "";
				// sc.setIsDialog(isDialog);
				// transportApplyDao.saveOrUpdate(sc);
			}
			// jsonStr = (String) jsonMap.get("transportApplyTissue");
			// if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
			// saveTransportApplyTissue(sc, jsonStr);
			//
			// String[] temp = jsonStr.split("},");
			// String isDialog = temp.length + "";
			// sc.setIsDialog(isDialog);
			// transportApplyDao.saveOrUpdate(sc);
			// }
		}
	}

	// 根据主表ID查询动物明细
	public List<Map<String, String>> setAnimalList(String code) throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		// TransportApply scp = transportApplyDao.get(TransportApply.class,
		// code);
		Map<String, Object> result = transportApplyDao.setAnimalList(code);
		List<TransportApplyAnimal> list = (List<TransportApplyAnimal>) result.get("list");

		if (list != null && list.size() > 0) {
			for (TransportApplyAnimal srai : list) {
				Map<String, String> map = new HashMap<String, String>();
				// map.put("itemId", srai.getId());
				// map.put("clean", srai.getClean());
				// map.put("gender", srai.getGender());
				// map.put("genetype", srai.getGenetype());
				// map.put("mark", srai.getMark());
				// map.put("animalId", srai.getAnimalMain().getId());
				// map.put("birthDate",
				// srai.getAnimalItem().getBirthDate().toString());
				// map.put("num", srai.getNum().toString());
				// map.put("projectId", srai.getProject().getId());
				mapList.add(map);
			}
		}
		return mapList;
	}

	// 根据运输申请带出明细数据
	// public List<Map<String, String>> setOrderAnimalList(String code)
	// throws Exception {
	// List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
	// // TransportApply scp = transportApplyDao.get(TransportApply.class,
	// // code);
	// Map<String, Object> result = transportApplyDao.setAnimalList(code);
	// List<TransportApplyAnimal> list = (List<TransportApplyAnimal>) result
	// .get("list");
	//
	// if (list != null && list.size() > 0) {
	// for (TransportApplyAnimal srai : list) {
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("itemId", srai.getAnimalId());
	// map.put("clean", srai.getClean());
	// map.put("gender", srai.getGender());
	// map.put("genetype", srai.getGenetype());
	// map.put("mark", srai.getMark());
	// if (srai.getNum() == null || srai.getNum().equals("")) {
	// map.put("num", "");
	// } else {
	// map.put("num", srai.getNum().toString());
	// }
	// // map.put("num", srai.getNum().toString());
	// map.put("projectId", srai.getProject().getId());
	// mapList.add(map);
	// }
	// }
	// return mapList;
	// }

	// @WriteOperLog
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void saveToOrderAnimal(TransportOrder sc, String id) {
	// List<TransportApplyAnimal> list = transportApplyDao
	// .saveToOrderAnimal(id);
	// for (TransportApplyAnimal mc : list) {
	// TransportOrderAnimal mmc = new TransportOrderAnimal();
	// mmc.setTransportOrder(sc);
	// transportApplyDao.saveOrUpdate(mmc);
	// }
	// }

	// 根据主表ID查询质粒菌液明细
	public List<Map<String, String>> setBacteriaList(String code) throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		// TransportApply scp = transportApplyDao.get(TransportApply.class,
		// code);
		Map<String, Object> result = transportApplyDao.setBacteriaList(code);
		List<TransportApplyBacteria> list = (List<TransportApplyBacteria>) result.get("list");

		if (list != null && list.size() > 0) {
			for (TransportApplyBacteria srai : list) {
				Map<String, String> map = new HashMap<String, String>();
				// map.put("itemId", srai.getId());
				// map.put("clean", srai.getProjectName());
				// map.put("gender", srai.getObjName());
				// map.put("genetype", srai.getConcentration().toString());
				// map.put("mark", srai.getOd230().toString());
				// map.put("animalId", srai.getOd260().toString());
				// map.put("birthDate", srai.getVolume().toString());
				// map.put("num", srai.getAntibody());
				mapList.add(map);
			}
		}
		return mapList;
	}

	// 根据运输申请带出明细数据
	public List<Map<String, String>> setOrderBacList(String code) throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		// TransportApply scp = transportApplyDao.get(TransportApply.class,
		// code);
		Map<String, Object> result = transportApplyDao.setBacteriaList(code);
		List<TransportApplyBacteria> list = (List<TransportApplyBacteria>) result.get("list");

		if (list != null && list.size() > 0) {
			for (TransportApplyBacteria srai : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", srai.getId());
				map.put("project", srai.getProjectName());
				map.put("objName", srai.getObjName());
				map.put("concentration", srai.getConcentration().toString());
				map.put("od230", srai.getOd230().toString());
				map.put("od260", srai.getOd260().toString());
				map.put("volume", srai.getVolume().toString());
				map.put("antibody", srai.getAntibody());
				mapList.add(map);
			}
		}
		return mapList;
	}

	// @WriteOperLog
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void saveToOrderBacteria(TransportOrder sc, String id) {
	// List<TransportApplyBacteria> list = transportApplyDao
	// .saveToOrderBacteria(id);
	// for (TransportApplyBacteria mc : list) {
	// TransportOrderBacteria mmc = new TransportOrderBacteria();
	// mmc.setTransportOrder(sc);
	// transportApplyDao.saveOrUpdate(mmc);
	// }
	// }

	// 根据主表ID查询细胞明细
	// public List<Map<String, String>> setCellList(String code) throws Exception {
	// List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
	// Map<String, Object> result = transportApplyDao.setCellList(code);
	// List<TransportApplyCell> list = (List<TransportApplyCell>) result
	// .get("list");
	//
	// if (list != null && list.size() > 0) {
	// for (TransportApplyCell srai : list) {
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", srai.getId());
	// map.put("cellName", srai.getMaterailsMainCell()
	// .getMaterailsMain().getName());
	// // map.put("cloneCode", srai.getMaterailsMainCell());
	// map.put("generation", srai.getMaterailsMainCell()
	// .getGeneration());
	// // map.put("background", srai.getOd230().toString());
	// map.put("num", srai.getNum().toString());
	// map.put("way", (srai.getWay()).toString());
	// mapList.add(map);
	// }
	// }
	// return mapList;
	// }

	// @WriteOperLog
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void saveToOrderCell(TransportOrder sc, String id) {
	// List<TransportApplyCell> list = transportApplyDao.saveToOrderCell(id);
	// for (TransportApplyCell mc : list) {
	// TransportOrderCell mmc = new TransportOrderCell();
	// mmc.setTransportOrder(sc);
	// transportApplyDao.saveOrUpdate(mmc);
	// }
	// }

	// 根据运输申请带出明细数据
	// public List<Map<String, String>> setOrderCellList(String code)
	// throws Exception {
	// List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
	// Map<String, Object> result = transportApplyDao.setCellList(code);
	// List<TransportApplyCell> list = (List<TransportApplyCell>) result
	// .get("list");
	//
	// if (list != null && list.size() > 0) {
	// for (TransportApplyCell srai : list) {
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", srai.getId());
	// map.put("cellName", srai.getMaterailsMainCell()
	// .getMaterailsMain().getName());
	// // map.put("cloneCode", srai.getMaterailsMainCell().);
	// map.put("generation", srai.getMaterailsMainCell()
	// .getGeneration());
	// // map.put("background", srai.getMaterailsMainCell().);
	// map.put("num", srai.getNum().toString());
	// map.put("way", (srai.getWay()).toString());
	// mapList.add(map);
	// }
	// }
	// return mapList;
	// }

	// 根据主表ID查询组织明细
	public List<Map<String, String>> setTissueList(String code) throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = transportApplyDao.setTissueList(code);
		List<TransportApplyTissue> list = (List<TransportApplyTissue>) result.get("list");

		if (list != null && list.size() > 0) {
			for (TransportApplyTissue srai : list) {
				Map<String, String> map = new HashMap<String, String>();
				mapList.add(map);
			}
		}
		return mapList;
	}

	// @WriteOperLog
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void saveToOrderTissue(TransportOrder sc, String id) {
	// List<TransportApplyTissue> list = transportApplyDao
	// .saveToOrderTissue(id);
	// for (TransportApplyTissue mc : list) {
	// TransportOrderTissue mmc = new TransportOrderTissue();
	// mmc.setTransportOrder(sc);
	// transportApplyDao.saveOrUpdate(mmc);
	// }
	// }

	// 根据运输申请带出明细数据
	// public List<Map<String, String>> setOrderTissueList(String code)
	// throws Exception {
	// List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
	// Map<String, Object> result = transportApplyDao.setTissueList(code);
	// List<TransportApplyTissue> list = (List<TransportApplyTissue>) result
	// .get("list");
	//
	// if (list != null && list.size() > 0) {
	// for (TransportApplyTissue srai : list) {
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", srai.getId());
	// map.put("name", srai.getName());
	// map.put("aniamlName", srai.getAnimalMain().getName());
	// map.put("animalId", srai.getAnimalMain().getId());
	// map.put("way", srai.getWay());
	// map.put("genetype", srai.getGenetype());
	// map.put("gender", srai.getGender());
	// map.put("num", srai.getNum().toString());
	// map.put("birthDate", srai.getAnimalItem().getBirthDate()
	// .toString());
	// mapList.add(map);
	// }
	// }
	// return mapList;
	// }

	/**
	 * 更改isDialog数据
	 * 
	 * @param type
	 * @param idStr
	 */
	public void chengeIsDialog(String type, String idStr) {
		Map<String, Object> result = null;
		String[] ids = idStr.split(",");
		String isDialog = "0";
		switch (Integer.parseInt(type)) {
		case 0:
			for (String id : ids) {
				result = transportApplyDao.findIsDialog(type, id);
				if (result != null) {
					TransportApplyAnimal taa = (TransportApplyAnimal) result.get("list");
					taa.setIsDialog(isDialog);
					transportApplyDao.saveOrUpdate(taa);

					TransportApply ta = transportApplyDao.get(TransportApply.class, taa.getTransportApply().getId());
					String isd = ta.getIsDialog();
					if (isd != null) {
						int oldis = Integer.parseInt(isd);
						if (oldis >= 1) {
							String newIsd = (oldis - 1) + "";
							ta.setIsDialog(newIsd);
							transportApplyDao.saveOrUpdate(ta);
						}

					}
				}
			}

			break;
		case 1:
			for (String id : ids) {
				result = transportApplyDao.findIsDialog(type, id);
				if (result != null) {
					TransportApplyBacteria taa = (TransportApplyBacteria) result.get("list");
					taa.setIsDialog(isDialog);
					transportApplyDao.saveOrUpdate(taa);

					TransportApply ta = transportApplyDao.get(TransportApply.class, taa.getTransportApply().getId());
					String isd = ta.getIsDialog();
					if (isd != null) {
						int oldis = Integer.parseInt(isd);
						if (oldis >= 1) {
							String newIsd = (oldis - 1) + "";
							ta.setIsDialog(newIsd);
							transportApplyDao.saveOrUpdate(ta);
						}

					}
				}
			}

			break;
		case 2:
			for (String id : ids) {
				result = transportApplyDao.findIsDialog(type, id);
				if (result != null) {
					TransportApplyCell taa = (TransportApplyCell) result.get("list");
					taa.setIsDialog(isDialog);
					transportApplyDao.saveOrUpdate(taa);

					TransportApply ta = transportApplyDao.get(TransportApply.class, taa.getTransportApply().getId());
					String isd = ta.getIsDialog();
					if (isd != null) {
						int oldis = Integer.parseInt(isd);
						if (oldis >= 1) {
							String newIsd = (oldis - 1) + "";
							ta.setIsDialog(newIsd);
							transportApplyDao.saveOrUpdate(ta);
						}

					}
				}
			}

			break;
		case 3:
			for (String id : ids) {
				result = transportApplyDao.findIsDialog(type, id);
				if (result != null) {
					TransportApplyTissue taa = (TransportApplyTissue) result.get("list");
					taa.setIsDialog(isDialog);
					transportApplyDao.saveOrUpdate(taa);

					TransportApply ta = transportApplyDao.get(TransportApply.class, taa.getTransportApply().getId());
					String isd = ta.getIsDialog();
					if (isd != null) {
						int oldis = Integer.parseInt(isd);
						if (oldis >= 1) {
							String newIsd = (oldis - 1) + "";
							ta.setIsDialog(newIsd);
							transportApplyDao.saveOrUpdate(ta);
						}

					}
				}
			}

			break;
		}
	}

	public Map<String, Object> showTransportApplyItemListJson(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return transportApplyDao.showTransportApplyItemListJson(id, start, length, query, col, sort);
	}

	public Map<String, Object> showTransportOrderTempList(Integer start, Integer length, String query, String col,
			String sort, String flag) throws Exception {
		return transportApplyDao.showTransportOrderTempList(start, length, query, col, sort, flag);
	}

	// 新的保存方法
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(TransportApply transportApply, Map jsonMap, String logInfo, Map logMap) {
		if (transportApply != null) {
			transportApplyDao.saveOrUpdate(transportApply);
			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(transportApply.getId());
				li.setModifyContent(logInfo);
				commonDAO.saveOrUpdate(li);
			}
		}
		// String jsonStr = "";
		// String logStr = "";
		// logStr = (String) logMap.get("sampleTransportItem");
		// jsonStr = (String) jsonMap.get("sampleTransportItem");
		// if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
		// saveSampleTransportItem(transportApply, jsonStr, logStr);
		// }
		// logStr = (String) logMap.get("sampleTrackingCondition");
		// jsonStr = (String) jsonMap.get("sampleTrackingCondition");
		// if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
		// saveSampleTrackingCondition(transportApply, jsonStr, logStr);
		// }
	}
}
