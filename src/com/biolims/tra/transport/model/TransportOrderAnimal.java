package com.biolims.tra.transport.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;

/**   
 * @Title: Model
 * @Description: 动物运输单
 * @author lims-platform
 * @date 2015-09-16 18:26:14
 * @version V1.0   
 *
 */
@Entity
@Table(name = "TRANSPORT_ORDER_ANIMAL")
@SuppressWarnings("serial")
public class TransportOrderAnimal extends EntityDao<TransportOrderAnimal> implements java.io.Serializable {
	/**序号*/
	private String id;
	/**动物编号*/
	private String animalId;
	/**课题编号*/
	private String projectId;
//	/**课题*/
//	private Project project;
	/**标记*/
	private String mark;
	/**基因型*/
	private String genetype;
	/**出生日期*/
	private Date birthDate;
	/**性别*/
	private String gender;
	/**清洁度*/
	private String clean;
	/**数量*/
	private Integer num;
	/**装箱数*/
	private Integer boxNum;
	/**运输方式*/
	private DicType transportType;
	/**运输温度*/
	private DicType transportTemperature;
	/**装箱单号*/
	private String packBoxNo;
	/**父本ID*/
	private String paId;
	/**母本ID*/
	private String maId;
	/**相关主表*/
	private TransportOrder transportOrder;

	/**
	 *方法: 取得String
	 *@return: String  序号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 *方法: 设置String
	 *@param: String  序号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 *方法: 取得String
	 *@return: String  动物编号
	 */
	@Column(name = "ANIMAL_ID", length = 36)
	public String getAnimalId() {
		return this.animalId;
	}

	/**
	 *方法: 设置String
	 *@param: String  动物编号
	 */
	public void setAnimalId(String animalId) {
		this.animalId = animalId;
	}

	/**
	 *方法: 取得String
	 *@return: String  课题编号
	 */
	@Column(name = "PROJECT_ID", length = 36)
	public String getProjectId() {
		return this.projectId;
	}

	/**
	 *方法: 设置String
	 *@param: String  课题编号
	 */
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	/**
	 *方法: 取得String
	 *@return: String  标记
	 */
	@Column(name = "MARK", length = 36)
	public String getMark() {
		return this.mark;
	}

	/**
	 *方法: 设置String
	 *@param: String  标记
	 */
	public void setMark(String mark) {
		this.mark = mark;
	}

	/**
	 *方法: 取得String
	 *@return: String  基因型
	 */
	@Column(name = "GENETYPE", length = 36)
	public String getGenetype() {
		return this.genetype;
	}

	/**
	 *方法: 设置String
	 *@param: String  基因型
	 */
	public void setGenetype(String genetype) {
		this.genetype = genetype;
	}

	/**
	 *方法: 取得String
	 *@return: String  性别
	 */
	@Column(name = "GENDER", length = 36)
	public String getGender() {
		return this.gender;
	}

	/**
	 *方法: 设置String
	 *@param: String  性别
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 *方法: 取得     Date
	 *@return: Date  出生日期
	 */
	@Column(name = "BIRTH_DATE", length = 255)
	public Date getBirthDate() {
		return birthDate;
	}

	/**
	 *方法: 设置  Date
	 *@param: Date  出生日期
	 */
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	/**
	 *方法: 取得String
	 *@return: String  清洁度
	 */
	@Column(name = "CLEAN", length = 36)
	public String getClean() {
		return this.clean;
	}

	/**
	 *方法: 设置String
	 *@param: String  清洁度
	 */
	public void setClean(String clean) {
		this.clean = clean;
	}

	/**
	 *方法: 取得Integer
	 *@return: Integer  数量
	 */
	@Column(name = "NUM", length = 36)
	public Integer getNum() {
		return this.num;
	}

	/**
	 *方法: 设置Integer
	 *@param: Integer  数量
	 */
	public void setNum(Integer num) {
		this.num = num;
	}

	/**
	 *方法: 取得TransportOrder
	 *@return: TransportOrder  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TRANSPORT_ORDER")
	public TransportOrder getTransportOrder() {
		return this.transportOrder;
	}

	/**
	 *方法: 设置TransportOrder
	 *@param: TransportOrder  相关主表
	 */
	public void setTransportOrder(TransportOrder transportOrder) {
		this.transportOrder = transportOrder;
	}

	/**
	 *方法: 取得  boxNum
	 *@param: boxNum  装箱数
	 */
	@Column(name = "BOX_NUM", length = 36)
	public Integer getBoxNum() {
		return boxNum;
	}

	/**
	 *方法: 设置  boxNum
	 *@param: boxNum  装箱数
	 */
	public void setBoxNum(Integer boxNum) {
		this.boxNum = boxNum;
	}

	/**
	 *方法: 取得  transportType
	 *@param: transportType  运输方式
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TRANSPORT_TYPE")
	public DicType getTransportType() {
		return transportType;
	}

	/**
	 *方法: 取得  transportType
	 *@param: transportType  运输方式
	 */
	public void setTransportType(DicType transportType) {
		this.transportType = transportType;
	}

	/**
	 *方法: 取得  transportTemperature
	 *@param: transportTemperature  运输温度
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TRANSPORT_TEMPERATURE")
	public DicType getTransportTemperature() {
		return transportTemperature;
	}

	/**
	 *方法: 取得  transportTemperature
	 *@param: transportTemperature  运输温度
	 */
	public void setTransportTemperature(DicType transportTemperature) {
		this.transportTemperature = transportTemperature;
	}

//	/**
//	 *方法: 取得     Project
//	 *@return: Project  课题
//	 */
//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "PROJECT")
//	public Project getProject() {
//		return project;
//	}
//
//	/**
//	 *方法: 设置     Project
//	 *@return: Project  课题
//	 */
//	public void setProject(Project project) {
//		this.project = project;
//	}

	/**
	 *方法: 取得  String
	 *@param: String  装箱单号
	 */
	@Column(name = "PACK_BOX_NO", length = 100)
	public String getPackBoxNo() {
		return packBoxNo;
	}

	/**
	 *方法: 设置  String
	 *@param: String  装箱单号
	 */
	public void setPackBoxNo(String packBoxNo) {
		this.packBoxNo = packBoxNo;
	}

	/**
	 *方法: 取得  String
	 *@param: String  父本ID
	 */
	@Column(name = "PA_ID", length = 100)
	public String getPaId() {
		return paId;
	}

	/**
	 *方法: 设置  String
	 *@param: String  父本ID
	 */
	public void setPaId(String paId) {
		this.paId = paId;
	}

	/**
	 *方法: 取得  String
	 *@param: String  母本ID
	 */
	@Column(name = "MA_ID", length = 100)
	public String getMaId() {
		return maId;
	}

	/**
	 *方法: 设置  String
	 *@param: String  母本ID
	 */
	public void setMaId(String maId) {
		this.maId = maId;
	}

}