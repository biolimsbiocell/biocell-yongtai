package com.biolims.tra.transport.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.system.express.model.ExpressCompany;
import com.biolims.dic.model.DicCurrencyType;
import com.biolims.common.model.user.User;

/**
 * @Title: Model
 * @Description: 运输单
 * @author lims-platform
 * @date 2015-08-03 11:32:20
 * @version V1.0
 * 
 */
@Entity
@Table(name = "TRANSPORT_ORDER")
@SuppressWarnings("serial")
public class TransportOrder extends EntityDao<TransportOrder> implements
		java.io.Serializable {
	/** 编码 */
	private String id;
	/** 描述 */
	private String name;
//	/** 课题 */
//	private Project project;
	/** 产品类型 */
	private String objType;
	/** 配送方式 */
	private String distributionType;
	/** 运送方式 */
	private String transportType;
	/** 运输负责人 */
	private User transportResponsibleUser;
	/** 项目来源地域 */
	private String projectSource;
	/** 上一步实验 */
	private String lastStep;
	/** 上一步实验名称 */
	private String lastStepName;
	/** 后续实验 */
	private String nextStep;
	/** 后续实验名称 */
	private String nextStepName;
	/** 创建人 */
	private User createUser;
	/** 创建日期 */
	private Date createDate;
	/** 批准人 */
	private User confirmUser;
	/** 批准人 */
	// private User confirmUserTwo;
	/** 批准人 */
	// private User confirmUserThree;
	/** 批准日期 */
	private Date confirmDate;
	/** 工作流状态 */
	private String state;
	/** 工作流状态名称 */
	private String stateName;
//	/** 任务进度 */
//	private ProjectProgress projectProgress;

	/** 运输申请 */
	private TransportApply transportApply;
	/** 分类 */
	private DicType type;
	/** 装箱数 */
	private String boxes;
	/** 目的地 */
	private String arrived;
	/** 出发日期 */
	private Date fromDate;

	/** 联系人 */
	private String linkman;
	/** 座机 */
	private String phone;
	/** 手机 */
	private String mobilePhone;
	/** 邮箱 */
	private String emall;
	/** 接收单位 */
	private String receiveCompany;
	/** 接收地址 */
	private String address;
	/** 接收要求 */
	private String receiveRequest;
	/** 备注 */
	private String note;

	/** 物流公司 */
	private ExpressCompany transCompany;
	/** 运送人 */
	private String transUser;
	/** 运送人手机 */
	private String transUserMobilePhone;
	/** 预计运送日期 */
	private Date expectedDeliveryDate;
	/** 实际运送日期 */
	private Date deliveryDate;
	/** 提货时间 */
	private Date pickUpTime;
	/** 预计运输时间 */
	private Date ExpectedDeliveryTime;
	/** 运单号 */
	private String transCode;
	/** 航班号 */
	private String flightCode;
	/** 预计到达日期 */
	private Date willDate;
	/** 代次 */
	private DicType gentype;
	// 样品运输方式
	private DicType ysfs;
	/** 运输类型 */
	private String orderType;
	/** 运输效果 */
	private String orderEffect;
	/** 结题日期 */
	private Date knotDate;
	/** 确认结题负责人 */
	private User knotUser;
	/** 出库单 */
	private String outOrder;
	/** 作废状态*/
	private String zuoState;
	
	/** 运输日期 */
	private Date transportDate;
	
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TRANS_COMPANY")
	public ExpressCompany getTransCompany() {
		return transCompany;
	}

	public void setTransCompany(ExpressCompany transCompany) {
		this.transCompany = transCompany;
	}

	public Date getTransportDate() {
		return transportDate;
	}

	public void setTransportDate(Date transportDate) {
		this.transportDate = transportDate;
	}

	/**
	 * 出库单
	 * 
	 * @return
	 */
	@Column(name = "OUT_ORDER", length = 36)
	public String getOutOrder() {
		return outOrder;
	}

	public void setOutOrder(String outOrder) {
		this.outOrder = outOrder;
	}

	/**
	 * 确认结题负责人
	 * 
	 * @return
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "KNOT_USER")
	public User getKnotUser() {
		return knotUser;
	}

	public void setKnotUser(User knotUser) {
		this.knotUser = knotUser;
	}

	/**
	 * 结题日期
	 * 
	 * @return
	 */
	@Column(name = "KNOT_DATE")
	public Date getKnotDate() {
		return knotDate;
	}

	public void setKnotDate(Date knotDate) {
		this.knotDate = knotDate;
	}

	/**
	 * 运输效果
	 * 
	 * @return
	 */
	@Column(name = "ORDER_EFFECT", length = 10)
	public String getOrderEffect() {
		return orderEffect;
	}

	public void setOrderEffect(String orderEffect) {
		this.orderEffect = orderEffect;
	}

	/**
	 * 运输类型
	 * 
	 * @return
	 */
	@Column(name = "ORDER_TYPE", length = 10)
	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GEN_TYPE")
	public DicType getGentype() {
		return gentype;
	}

	public void setGentype(DicType gentype) {
		this.gentype = gentype;
	}

	/** 签收人 */
	private String receiver;
	/** 签收日期 */
	private Date receiveDate;
	/** 签收确认人 */
	private User signConfirmationUser;
	/** 签收确认日期 */
	private Date signConfirmationDate;
	/** 运费 */
	private String transFee;

	/** 运输类型 */
	private String content1;
	/** 运输效果 */
	private String content2;
	/** content3 */
	private String content3;
	/** content4 */
	private String content4;
	/** content5 */
	private Double content5;
	/** content6 */
	private Double content6;
	/** content7 */
	private Double content7;
	/** content8 */
	private Double content8;
	/** content9 */
	private Date content9;
	/** content10 */
	private Date content10;
	/** content11 */
	private Date content11;
	/** content12 */
	private Date content12;

	/**
	 * 方法: 取得 String
	 * 
	 * @return: String 编码
	 */
	@Id
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 120)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得TransportApply
	 * 
	 * @return: TransportApply 运输申请
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TRANSPORT_APPLY")
	public TransportApply getTransportApply() {
		return this.transportApply;
	}

	/**
	 * 方法: 设置TransportApply
	 * 
	 * @param: TransportApply 运输申请
	 */
	public void setTransportApply(TransportApply transportApply) {
		this.transportApply = transportApply;
	}

	/**
	 * 方法: 取得DicType
	 * 
	 * @return: DicType 分类
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TYPE")
	public DicType getType() {
		return this.type;
	}

	/**
	 * 方法: 设置DicType
	 * 
	 * @param: DicType 分类
	 */
	public void setType(DicType type) {
		this.type = type;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 创建人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 创建日期
	 */
	@Column(name = "CREATE_DATE", length = 255)
	public Date getCreateDate() {
		return this.createDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 创建日期
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 批准人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CONFIRM_USER")
	public User getConfirmUser() {
		return this.confirmUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 批准人
	 */
	public void setConfirmUser(User confirmUser) {
		this.confirmUser = confirmUser;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 批准人
	 */
	// @ManyToOne(fetch = FetchType.LAZY)
	// @NotFound(action = NotFoundAction.IGNORE)
	// @JoinColumn(name = "CONFIRM_USERTWO")
	// public User getConfirmUserTwo() {
	// return this.confirmUserTwo;
	// }

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 批准人
	 */
	// public void setConfirmUserTwo(User confirmUserTwo) {
	// this.confirmUserTwo = confirmUserTwo;
	// }

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 批准人
	 */
	// @ManyToOne(fetch = FetchType.LAZY)
	// @NotFound(action = NotFoundAction.IGNORE)
	// @JoinColumn(name = "CONFIRM_USERTHREE")
	// public User getConfirmUserThree() {
	// return this.confirmUserThree;
	// }

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 批准人
	 */
	// public void setConfirmUserThree(User confirmUserThree) {
	// this.confirmUserThree = confirmUserThree;
	// }

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 批准日期
	 */
	@Column(name = "CONFIRM_DATE", length = 255)
	public Date getConfirmDate() {
		return this.confirmDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 批准日期
	 */
	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 运送人
	 */
	@Column(name = "TRANS_USER", length = 100)
	public String getTransUser() {
		return transUser;
	}

	public void setTransUser(String transUser) {
		this.transUser = transUser;
	}

//	/**
//	 * 方法: 取得String
//	 * 
//	 * @return: String 承运公司
//	 */
//	@Column(name = "TRANS_COMPANY", length = 60)
//	public String getTransCompany() {
//		return this.transCompany;
//	}
//
//	/**
//	 * 方法: 设置String
//	 * 
//	 * @param: String 承运公司
//	 */
//	public void setTransCompany(String transCompany) {
//		this.transCompany = transCompany;
//	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 运单号
	 */
	@Column(name = "TRANS_CODE", length = 40)
	public String getTransCode() {
		return this.transCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 运单号
	 */
	public void setTransCode(String transCode) {
		this.transCode = transCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 联系人
	 */
	@Column(name = "LINKMAN", length = 20)
	public String getLinkman() {
		return this.linkman;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 联系人
	 */
	public void setLinkman(String linkman) {
		this.linkman = linkman;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 座机
	 */
	@Column(name = "PHONE", length = 40)
	public String getPhone() {
		return this.phone;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 座机
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 接收单位
	 */
	@Column(name = "RECEIVE_COMPANY", length = 60)
	public String getReceiveCompany() {
		return this.receiveCompany;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 接收单位
	 */
	public void setReceiveCompany(String receiveCompany) {
		this.receiveCompany = receiveCompany;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 接收地址
	 */
	@Column(name = "ADDRESS", length = 60)
	public String getAddress() {
		return this.address;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 接收地址
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 邮箱
	 */
	@Column(name = "EMALL", length = 60)
	public String getEmall() {
		return this.emall;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 邮箱
	 */
	public void setEmall(String emall) {
		this.emall = emall;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 航班号
	 */
	@Column(name = "FLIGHT_CODE", length = 40)
	public String getFlightCode() {
		return this.flightCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 航班号
	 */
	public void setFlightCode(String flightCode) {
		this.flightCode = flightCode;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 预计送达日期
	 */
	@Column(name = "WILL_DATE", length = 255)
	public Date getWillDate() {
		return this.willDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 预计送达日期
	 */
	public void setWillDate(Date willDate) {
		this.willDate = willDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 目的地
	 */
	@Column(name = "ARRIVED", length = 80)
	public String getArrived() {
		return this.arrived;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 目的地
	 */
	public void setArrived(String arrived) {
		this.arrived = arrived;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 出发日期
	 */
	@Column(name = "FROM_DATE", length = 255)
	public Date getFromDate() {
		return this.fromDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 出发日期
	 */
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 签收日期
	 */
	@Column(name = "RECEIVE_DATE", length = 255)
	public Date getReceiveDate() {
		return this.receiveDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 签收日期
	 */
	public void setReceiveDate(Date receiveDate) {
		this.receiveDate = receiveDate;
	}

	/**
	 * 方法: 取得 String
	 * 
	 * @return: String 运费
	 */
	@Column(name = "TRANS_FEE", length = 60)
	public String getTransFee() {
		return this.transFee;
	}

	/**
	 * 方法: 设置 String
	 * 
	 * @param: String 运费
	 */
	public void setTransFee(String transFee) {
		this.transFee = transFee;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 工作流状态
	 */
	@Column(name = "STATE", length = 60)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 工作流状态
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 工作流状态名称
	 */
	@Column(name = "STATE_NAME", length = 60)
	public String getStateName() {
		return this.stateName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 工作流状态名称
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @param: String 物品类型
	 */
	@Column(name = "OBJ_TYPE", length = 10)
	public String getObjType() {
		return objType;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 物品类型
	 */
	public void setObjType(String objType) {
		this.objType = objType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String content1
	 */
	@Column(name = "CONTENT1", length = 50)
	public String getContent1() {
		return this.content1;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String content1
	 */
	public void setContent1(String content1) {
		this.content1 = content1;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String content2
	 */
	@Column(name = "CONTENT2", length = 50)
	public String getContent2() {
		return this.content2;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String content2
	 */
	public void setContent2(String content2) {
		this.content2 = content2;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String content3
	 */
	@Column(name = "CONTENT3", length = 50)
	public String getContent3() {
		return this.content3;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String content3
	 */
	public void setContent3(String content3) {
		this.content3 = content3;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String content4
	 */
	@Column(name = "CONTENT4", length = 50)
	public String getContent4() {
		return this.content4;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String content4
	 */
	public void setContent4(String content4) {
		this.content4 = content4;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double content5
	 */
	@Column(name = "CONTENT5", length = 50)
	public Double getContent5() {
		return this.content5;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double content5
	 */
	public void setContent5(Double content5) {
		this.content5 = content5;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double content6
	 */
	@Column(name = "CONTENT6", length = 50)
	public Double getContent6() {
		return this.content6;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double content6
	 */
	public void setContent6(Double content6) {
		this.content6 = content6;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double content7
	 */
	@Column(name = "CONTENT7", length = 50)
	public Double getContent7() {
		return this.content7;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double content7
	 */
	public void setContent7(Double content7) {
		this.content7 = content7;
	}

	/**
	 * 方法: 取得Double
	 * 
	 * @return: Double content8
	 */
	@Column(name = "CONTENT8", length = 50)
	public Double getContent8() {
		return this.content8;
	}

	/**
	 * 方法: 设置Double
	 * 
	 * @param: Double content8
	 */
	public void setContent8(Double content8) {
		this.content8 = content8;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date content9
	 */
	@Column(name = "CONTENT9", length = 255)
	public Date getContent9() {
		return this.content9;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date content9
	 */
	public void setContent9(Date content9) {
		this.content9 = content9;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date content10
	 */
	@Column(name = "CONTENT10", length = 255)
	public Date getContent10() {
		return this.content10;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date content10
	 */
	public void setContent10(Date content10) {
		this.content10 = content10;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date content11
	 */
	@Column(name = "CONTENT11", length = 255)
	public Date getContent11() {
		return this.content11;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date content11
	 */
	public void setContent11(Date content11) {
		this.content11 = content11;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date content12
	 */
	@Column(name = "CONTENT12", length = 255)
	public Date getContent12() {
		return this.content12;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date content12
	 */
	public void setContent12(Date content12) {
		this.content12 = content12;
	}

//	/**
//	 * 方法: 取得 Project
//	 * 
//	 * @return: Project 课题
//	 */
//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "PROJECT")
//	public Project getProject() {
//		return project;
//	}
//
//	/**
//	 * 方法: 设置 Project
//	 * 
//	 * @return: Project 课题
//	 */
//	public void setProject(Project project) {
//		this.project = project;
//	}

	/**
	 * 方法: 取得 Date
	 * 
	 * @return: Date 预计发出日期
	 */
	@Column(name = "DELIVERY_DATE", length = 255)
	public Date getDeliveryDate() {
		return deliveryDate;
	}

	/**
	 * 方法: 设置 Date
	 * 
	 * @return: Date 预计发出日期
	 */
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	/**
	 * 方法: 设置 String
	 * 
	 * @return: String 装箱数
	 */
	@Column(name = "BOXES", length = 30)
	public String getBoxes() {
		return boxes;
	}

	/**
	 * 方法: 设置 String
	 * 
	 * @return: String 装箱数
	 */
	public void setBoxes(String boxes) {
		this.boxes = boxes;
	}

	/**
	 * 方法: 取得 String
	 * 
	 * @return: String 运输方式
	 */
	@Column(name = "TRANSPORT_TYPE", length = 30)
	public String getTransportType() {
		return transportType;
	}

	/**
	 * 方法: 设置 String
	 * 
	 * @return: String 运输方式
	 */
	public void setTransportType(String transportType) {
		this.transportType = transportType;
	}

	/**
	 * 方法: 取得 String
	 * 
	 * @return: String 配送方式
	 */
	@Column(name = "DISTRIBUTION_TYPE", length = 60)
	public String getDistributionType() {
		return distributionType;
	}

	/**
	 * 方法: 设置 String
	 * 
	 * @return: String 配送方式
	 */
	public void setDistributionType(String distributionType) {
		this.distributionType = distributionType;
	}

	/**
	 * 方法: 取得 User
	 * 
	 * @return: User 运输负责人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TRANSPORT_RESPONSIBLE_USER")
	public User getTransportResponsibleUser() {
		return transportResponsibleUser;
	}

	/**
	 * 方法: 设置 User
	 * 
	 * @return: User 运输负责人
	 */
	public void setTransportResponsibleUser(User transportResponsibleUser) {
		this.transportResponsibleUser = transportResponsibleUser;
	}

	/**
	 * 方法: 取得 String
	 * 
	 * @return: String 上一步实验
	 */
	@Column(name = "LAST_STEP", length = 60)
	public String getLastStep() {
		return lastStep;
	}

	/**
	 * 方法: 设置 String
	 * 
	 * @return: String 上一步实验
	 */
	public void setLastStep(String lastStep) {
		this.lastStep = lastStep;
	}

	/**
	 * 方法: 取得 String
	 * 
	 * @return: String 后续实验名称
	 */
	@Column(name = "LAST_STEP_NAME", length = 60)
	public String getLastStepName() {
		return lastStepName;
	}

	/**
	 * 方法: 设置 String
	 * 
	 * @return: String 后续实验名称
	 */
	public void setLastStepName(String lastStepName) {
		this.lastStepName = lastStepName;
	}

	/**
	 * 方法: 取得 String
	 * 
	 * @return: String 后续实验
	 */
	@Column(name = "NEXT_STEP", length = 60)
	public String getNextStep() {
		return nextStep;
	}

	/**
	 * 方法: 设置 String
	 * 
	 * @return: String 后续实验
	 */
	public void setNextStep(String nextStep) {
		this.nextStep = nextStep;
	}

	/**
	 * 方法: 取得 String
	 * 
	 * @return: String 后续实验名称
	 */
	@Column(name = "NEXT_STEP_NAME", length = 60)
	public String getNextStepName() {
		return nextStepName;
	}

	/**
	 * 方法: 设置 String
	 * 
	 * @return: String 后续实验名称
	 */
	public void setNextStepName(String nextStepName) {
		this.nextStepName = nextStepName;
	}

//	/**
//	 * 方法: 取得 ProjectProgress
//	 * 
//	 * @return: ProjectProgress 任务进度
//	 */
//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "PROJECT_PROGRESS")
//	public ProjectProgress getProjectProgress() {
//		return projectProgress;
//	}
//
//	/**
//	 * 方法: 设置 ProjectProgress
//	 * 
//	 * @return: ProjectProgress 任务进度
//	 */
//	public void setProjectProgress(ProjectProgress projectProgress) {
//		this.projectProgress = projectProgress;
//	}

	/**
	 * 方法: 取得 String
	 * 
	 * @return: String 手机
	 */
	@Column(name = "MOBILE_PHONE", length = 60)
	public String getMobilePhone() {
		return mobilePhone;
	}

	/**
	 * 方法: 设置 String
	 * 
	 * @return: String 手机
	 */
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	/**
	 * 方法: 取得 String
	 * 
	 * @return: String 接收要求
	 */
	@Column(name = "RECEIVE_REQUEST", length = 200)
	public String getReceiveRequest() {
		return receiveRequest;
	}

	/**
	 * 方法: 设置 String
	 * 
	 * @return: String 接收要求
	 */
	public void setReceiveRequest(String receiveRequest) {
		this.receiveRequest = receiveRequest;
	}

	/**
	 * 方法: 取得 String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 200)
	public String getNote() {
		return note;
	}

	/**
	 * 方法: 设置 String
	 * 
	 * @return: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得 String
	 * 
	 * @return: String 运送人手机
	 */
	@Column(name = "TRANSUSER_MOBILE_PHONE", length = 100)
	public String getTransUserMobilePhone() {
		return transUserMobilePhone;
	}

	/**
	 * 方法: 设置 String
	 * 
	 * @return: String 运送人手机
	 */
	public void setTransUserMobilePhone(String transUserMobilePhone) {
		this.transUserMobilePhone = transUserMobilePhone;
	}

	/**
	 * 方法: 取得 Date
	 * 
	 * @return: Date 预计运送日期
	 */
	@Column(name = "EXPECTED_DELIVERY_DATE", length = 255)
	public Date getExpectedDeliveryDate() {
		return expectedDeliveryDate;
	}

	/**
	 * 方法: 设置 Date
	 * 
	 * @return: Date 预计运送日期
	 */
	public void setExpectedDeliveryDate(Date expectedDeliveryDate) {
		this.expectedDeliveryDate = expectedDeliveryDate;
	}

	/**
	 * 方法: 取得 Date
	 * 
	 * @return: Date 提货时间
	 */
	@Column(name = "PICK_UP_TIME", length = 255)
	public Date getPickUpTime() {
		return pickUpTime;
	}

	/**
	 * 方法: 设置 Date
	 * 
	 * @return: Date 提货时间
	 */
	public void setPickUpTime(Date pickUpTime) {
		this.pickUpTime = pickUpTime;
	}

	/**
	 * 方法: 取得 Date
	 * 
	 * @return: Date 预计运输时间
	 */
	@Column(name = "EXPECTED_DELIVERY_TIME", length = 255)
	public Date getExpectedDeliveryTime() {
		return ExpectedDeliveryTime;
	}

	/**
	 * 方法: 设置 Date
	 * 
	 * @return: Date 预计运输时间
	 */
	public void setExpectedDeliveryTime(Date expectedDeliveryTime) {
		ExpectedDeliveryTime = expectedDeliveryTime;
	}

	/**
	 * 方法: 取得 String
	 * 
	 * @return: String 签收人
	 */
	@Column(name = "RECEIVER", length = 100)
	public String getReceiver() {
		return receiver;
	}

	/**
	 * 方法: 设置 String
	 * 
	 * @return: String 签收人
	 */
	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	/**
	 * 方法: 取得 User
	 * 
	 * @return: User 签收确认人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SIGN_CONFIRMATION_USER")
	public User getSignConfirmationUser() {
		return signConfirmationUser;
	}

	/**
	 * 方法: 设置 User
	 * 
	 * @return: User 签收确认人
	 */
	public void setSignConfirmationUser(User signConfirmationUser) {
		this.signConfirmationUser = signConfirmationUser;
	}

	/**
	 * 方法: 取得 Date
	 * 
	 * @return: Date 签收确认日期
	 */
	@Column(name = "SIGN_CONFIRMATION_DATE", length = 255)
	public Date getSignConfirmationDate() {
		return signConfirmationDate;
	}

	/**
	 * 方法: 设置 Date
	 * 
	 * @return: Date 签收确认日期
	 */
	public void setSignConfirmationDate(Date signConfirmationDate) {
		this.signConfirmationDate = signConfirmationDate;
	}

	/**
	 * 方法: 取得 String
	 * 
	 * @param: String 项目来源地域
	 */
	@Column(name = "PROJECT_SOURCE", length = 200)
	public String getProjectSource() {
		return projectSource;
	}

	/**
	 * 方法: 设置 String
	 * 
	 * @param: String 项目来源地域
	 */
	public void setProjectSource(String projectSource) {
		this.projectSource = projectSource;
	}

	public String getZuoState() {
		return zuoState;
	}

	public void setZuoState(String zuoState) {
		this.zuoState = zuoState;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	public DicType getYsfs() {
		return ysfs;
	}

	public void setYsfs(DicType ysfs) {
		this.ysfs = ysfs;
	}


}