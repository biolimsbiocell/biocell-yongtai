package com.biolims.tra.transport.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;

/**   
 * @Title: Model
 * @Description: 运送物品(质粒/菌液)情况
 * @author lims-platform
 * @date 2015-08-03 11:32:07
 * @version V1.0   
 *
 */
@Entity
@Table(name = "TRANSPORT_APPLY_BACTERIA")
@SuppressWarnings("serial")
public class TransportApplyBacteria extends EntityDao<TransportApplyBacteria> implements java.io.Serializable {
	/**编码*/
	private String id;
	/**产品类型*/
	private String objName;
	/**项目名称*/
	private String projectName;
	/**浓度(ng/ul)*/
	private Integer concentration;
	/**体积*/
	private Double volume;
	/**抗体*/
	private String antibody;
	/**260/280*/
	private Integer od260;
	/**260/230*/
	private Integer od230;
	/**产品类型*/
	private String note;
	/**相关主表*/
	private TransportApply transportApply;
	/**是否在Dialog中显示*/
	private String isDialog;

	/**
	 *方法: 取得String
	 *@return: String  编码
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 *方法: 设置String
	 *@param: String  编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 *方法: 取得MaterailsMain
	 *@return: MaterailsMain  物品类型
	 */
	//	@ManyToOne(fetch = FetchType.LAZY)
	//	@NotFound(action = NotFoundAction.IGNORE)
	//	@JoinColumn(name = "MATERAILS_MAIN")
	//	public MaterailsMain getMaterailsMain() {
	//		return this.materailsMain;
	//	}
	//
	//	/**
	//	 *方法: 设置MaterailsMain
	//	 *@param: MaterailsMain  物品类型
	//	 */
	//	public void setMaterailsMain(MaterailsMain materailsMain) {
	//		this.materailsMain = materailsMain;
	//	}
	/**
	 *方法: 取得Integer
	 *@return: Integer  浓度(ng/ul)
	 */
	@Column(name = "CONCENTRATION", length = 40)
	public Integer getConcentration() {
		return this.concentration;
	}

	/**
	 *方法: 设置Integer
	 *@param: Integer  浓度(ng/ul)
	 */
	public void setConcentration(Integer concentration) {
		this.concentration = concentration;
	}

	/**
	 *方法: 取得Double
	 *@return: Double  体积
	 */
	@Column(name = "VOLUME", length = 40)
	public Double getVolume() {
		return this.volume;
	}

	/**
	 *方法: 设置Double
	 *@param: Double  体积
	 */
	public void setVolume(Double volume) {
		this.volume = volume;
	}

	/**
	 *方法: 取得String
	 *@return: String  抗体
	 */
	@Column(name = "ANTIBODY", length = 40)
	public String getAntibody() {
		return this.antibody;
	}

	/**
	 *方法: 设置String
	 *@param: String  抗体
	 */
	public void setAntibody(String antibody) {
		this.antibody = antibody;
	}

	/**产品类型*/
	public String getObjName() {
		return objName;
	}

	public void setObjName(String objName) {
		this.objName = objName;
	}

	/**项目名称*/
	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	/**
	 *方法: 取得Integer
	 *@return: Integer  260/280
	 */
	@Column(name = "OD_260", length = 40)
	public Integer getOd260() {
		return this.od260;
	}

	/**
	 *方法: 设置Integer
	 *@param: Integer  260/280
	 */
	public void setOd260(Integer od260) {
		this.od260 = od260;
	}

	/**
	 *方法: 取得Integer
	 *@return: Integer  260/230
	 */
	@Column(name = "OD_230", length = 40)
	public Integer getOd230() {
		return this.od230;
	}

	/**
	 *方法: 设置Integer
	 *@param: Integer  260/230
	 */
	public void setOd230(Integer od230) {
		this.od230 = od230;
	}

	/**
	 *方法: 取得TransportApply
	 *@return: TransportApply  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TRANSPORT_APPLY")
	public TransportApply getTransportApply() {
		return this.transportApply;
	}

	/**
	 *方法: 设置TransportApply
	 *@param: TransportApply  相关主表
	 */
	public void setTransportApply(TransportApply transportApply) {
		this.transportApply = transportApply;
	}

	/**
	 *方法: 取得String
	 *@return: String  isDialog
	 */
	@Column(name = "IS_DIALOG", length = 50)
	public String getIsDialog() {
		return isDialog;
	}

	public void setIsDialog(String isDialog) {
		this.isDialog = isDialog;
	}

	/**
	 * 备注
	 * @return
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
}