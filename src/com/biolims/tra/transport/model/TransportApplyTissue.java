package com.biolims.tra.transport.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;

/**   
 * @Title: Model
 * @Description: 运送物品(组织)情况
 * @author lims-platform
 * @date 2015-08-03 11:32:09
 * @version V1.0   
 *
 */
@Entity
@Table(name = "TRANSPORT_APPLY_TISSUE")
@SuppressWarnings("serial")
public class TransportApplyTissue extends EntityDao<TransportApplyTissue> implements java.io.Serializable {
	/**编码*/
	private String id;
	/**组织名称*/
	private String name;
	/**品系*/
	private String animalType;
	/**样品编号*/
	private String sampleNum;
	//	/**动物名称*/
	//	private String animalName;
	/**性别*/
	private String gender;
	/**性别*/
	private String note;
	/**数量*/
	private Integer num;
	/**基因型*/
	private String genetype;
	//	/**出生日期*/
	//	private Date birthDate;
	/**运输方式*/
	private String way;
	/**相关主表*/
	private TransportApply transportApply;
	/**是否在Dialog中显示*/
	private String isDialog;

	/**
	 *方法: 取得String
	 *@return: String  编码
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 *方法: 设置String
	 *@param: String  编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 *方法: 取得String
	 *@return: String  组织名称
	 */
	@Column(name = "NAME", length = 40)
	public String getName() {
		return this.name;
	}

	/**
	 *方法: 设置String
	 *@param: String  组织名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *方法: 设置String
	 *@param: String  品系
	 */
	@Column(name = "ANIMAL_TYPE", length = 40)
	public String getAnimalType() {
		return animalType;
	}

	public void setAnimalType(String animalType) {
		this.animalType = animalType;
	}

	/**
	 * 备注
	 * @return
	 */
	@Column(name = "NOTE", length = 500)
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}


	//	/**
	//	 *方法: 取得String
	//	 *@return: String  动物名称
	//	 */
	//	@Column(name = "ANIMAL_NAME", length = 40)
	//	public String getAnimalName() {
	//		return this.animalName;
	//	}
	//
	//	/**
	//	 *方法: 设置String
	//	 *@param: String  动物名称
	//	 */
	//	public void setAnimalName(String animalName) {
	//		this.animalName = animalName;
	//	}

	/**
	 *方法: 取得String
	 *@return: String  性别
	 */
	@Column(name = "GENDER", length = 10)
	public String getGender() {
		return this.gender;
	}

	/**
	 *方法: 设置String
	 *@param: String  性别
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * 样品编号
	 * @return
	 */
	@Column(name = "SAMPLE_NUM", length = 10)
	public String getSampleNum() {
		return sampleNum;
	}

	public void setSampleNum(String sampleNum) {
		this.sampleNum = sampleNum;
	}

	/**
	 *方法: 取得Integer
	 *@return: Integer  数量
	 */
	@Column(name = "NUM", length = 10)
	public Integer getNum() {
		return this.num;
	}

	/**
	 *方法: 设置Integer
	 *@param: Integer  数量
	 */
	public void setNum(Integer num) {
		this.num = num;
	}

	/**
	 *方法: 取得String
	 *@return: String  基因型
	 */
	@Column(name = "GENETYPE", length = 40)
	public String getGenetype() {
		return this.genetype;
	}

	/**
	 *方法: 设置String
	 *@param: String  基因型
	 */
	public void setGenetype(String genetype) {
		this.genetype = genetype;
	}

	//	/**
	//	 *方法: 取得Date
	//	 *@return: Date  出生日期
	//	 */
	//	@Column(name = "BIRTH_DATE", length = 255)
	//	public Date getBirthDate() {
	//		return this.birthDate;
	//	}
	//
	//	/**
	//	 *方法: 设置Date
	//	 *@param: Date  出生日期
	//	 */
	//	public void setBirthDate(Date birthDate) {
	//		this.birthDate = birthDate;
	//	}

	/**
	 *方法: 取得String
	 *@return: String  运输方式
	 */
	@Column(name = "WAY", length = 40)
	public String getWay() {
		return this.way;
	}

	/**
	 *方法: 设置String
	 *@param: String  运输方式
	 */
	public void setWay(String way) {
		this.way = way;
	}

	/**
	 *方法: 取得TransportApply
	 *@return: TransportApply  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TRANSPORT_APPLY")
	public TransportApply getTransportApply() {
		return this.transportApply;
	}

	/**
	 *方法: 设置TransportApply
	 *@param: TransportApply  相关主表
	 */
	public void setTransportApply(TransportApply transportApply) {
		this.transportApply = transportApply;
	}

	/**
	 *方法: 取得String
	 *@return: String  isDialog
	 */
	@Column(name = "IS_DIALOG", length = 50)
	public String getIsDialog() {
		return isDialog;
	}

	public void setIsDialog(String isDialog) {
		this.isDialog = isDialog;
	}
}