package com.biolims.tra.transport.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;

/**
 * @Title: Model
 * @Description: 运送物品(动物)情况
 * @author lims-platform
 * @date 2015-08-03 11:32:05
 * @version V1.0
 * 
 */
@Entity
@Table(name = "TRANSPORT_APPLY_ANIMAL")
@SuppressWarnings("serial")
public class TransportApplyAnimal extends EntityDao<TransportApplyAnimal>
		implements java.io.Serializable {
	/** 编码 */
	private String id;
	/** 动物编号 */
	private String animalId;
	/** 标记 */
	private String mark;
	/** 基因型 */
	private String genetype;
	/** 出生日期 */
	private Date birthDate;
	/** 性别 */
	private String gender;
	/** 清洁度 */
	private String clean;
	/** 数量 */
	private Integer num;
	/** 相关主表 */
	private TransportApply transportApply;
	/** 是否在Dialog中显示 */
	private String isDialog;

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 鼠ID
	 */
	@Column(name = "ANIMAL_ID", length = 40)
	public String getAnimalId() {
		return animalId;
	}

	public void setAnimalId(String animalId) {
		this.animalId = animalId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 出生日期
	 */
	@Column(name = "BIRTH_DATE", length = 40)
	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编码
	 */
	public void setId(String id) {
		this.id = id;
	}


	/**
	 * 方法: 取得String
	 * 
	 * @return: String 标记
	 */
	@Column(name = "MARK", length = 40)
	public String getMark() {
		return this.mark;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 标记
	 */
	public void setMark(String mark) {
		this.mark = mark;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 基因型
	 */
	@Column(name = "GENETYPE", length = 40)
	public String getGenetype() {
		return this.genetype;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 基因型
	 */
	public void setGenetype(String genetype) {
		this.genetype = genetype;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 出生日期
	 */
	// @Column(name = "BIRTH_DATE", length = 255)
	// public Date getBirthDate() {
	// return this.birthDate;
	// }
	//
	// /**
	// *方法: 设置Date
	// *@param: Date 出生日期
	// */
	// public void setBirthDate(Date birthDate) {
	// this.birthDate = birthDate;
	// }

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 性别
	 */
	@Column(name = "GENDER", length = 40)
	public String getGender() {
		return this.gender;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 性别
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 清洁度
	 */
	@Column(name = "CLEAN", length = 20)
	public String getClean() {
		return this.clean;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 清洁度
	 */
	public void setClean(String clean) {
		this.clean = clean;
	}

	/**
	 * 方法: 取得Integer
	 * 
	 * @return: Integer 数量
	 */
	@Column(name = "NUM", length = 40)
	public Integer getNum() {
		return this.num;
	}

	/**
	 * 方法: 设置Integer
	 * 
	 * @param: Integer 数量
	 */
	public void setNum(Integer num) {
		this.num = num;
	}

	/**
	 * 方法: 取得TransportApply
	 * 
	 * @return: TransportApply 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TRANSPORT_APPLY")
	public TransportApply getTransportApply() {
		return this.transportApply;
	}

	/**
	 * 方法: 设置TransportApply
	 * 
	 * @param: TransportApply 相关主表
	 */
	public void setTransportApply(TransportApply transportApply) {
		this.transportApply = transportApply;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String isDialog
	 */
	@Column(name = "IS_DIALOG", length = 50)
	public String getIsDialog() {
		return isDialog;
	}

	public void setIsDialog(String isDialog) {
		this.isDialog = isDialog;
	}

}