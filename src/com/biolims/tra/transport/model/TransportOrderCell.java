package com.biolims.tra.transport.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleOrder;
import com.biolims.system.express.model.ExpressCompany;

/**
 * @Title: Model
 * @Description: 细胞运输单
 * @author lims-platform
 * @date 2015-09-16 18:26:12
 * @version V1.0
 *
 */
@Entity
@Table(name = "TRANSPORT_ORDER_CELL")
@SuppressWarnings("serial")
public class TransportOrderCell extends EntityDao<TransportOrderCell> implements java.io.Serializable {
	/** 序号 */
	private String id;
	/** 细胞名称 */
	private String cellName;
	/** 课题名称 */
	private String projectName;
	/** 克隆号 */
	private String cloneCode;
	/** 种属 */
	private String genus;
	/** 代次 */
	private String generation;
	/** 品系 */
	private String strain;
	/** 基因型 */
	private String genoType;
	/** 细胞背景 */
	private String background;
	/** 细胞数/管 */
	private String numTubes;
	/** 支原体检测（有/无） */
	private String detectionMycoplasma;
	/** 运输温度 */
	private String transportTemperature;
	/** 管数 */
	private Integer num;
	/** 运输方式 */
	private String way;
	/** 电转次数 */
	private String electricTransTime;
	/** 备注 */
	private String note;
	/** 相关主表 */
	private TransportOrder transportOrder;
	/** 样本编号 */
	private String code;
	/** 原始样本编号 */
	private String sampleCode;
	/** 代次 */
	private String pronoun;
	/** 样本类型 */
	private DicSampleType dicSampleType;
	/** 样本类型 */
	private String sampleType;
	/** 左侧表ID */
	private String tempId;
	/** 订单 */
	private SampleOrder sampleOrder;
	/** 出发时间 */
	private Date transportDate;
	/** 到达时间 */
	private Date transportDate2;
	/** 生产确认 */
	private String state;
	/** 操作人 */
	@Column(name="temple_operator")
	private User templeOperator;
	/** 操作间名称 */
	@Column(name="operating_room_name")
	private String operatingRoomName;
	/** 操作间id */
	@Column(name="operating_room_id")
	private String operatingRoomId;
	/**预计回输日期*/
	private String reinfusionPlanDate;
	
	/**
	 * 新增
	 */
	private String lowTemperature;//最低温度
	private String highTemperature;//最高温度
	private String avgTemperature;//平均温度
	private String equimentNumber;//保温箱型号
	private String recorder;//温度计型号
	private String personFollowCar;//跟车人员
	
	public String getLowTemperature() {
		return lowTemperature;
	}

	public void setLowTemperature(String lowTemperature) {
		this.lowTemperature = lowTemperature;
	}

	public String getHighTemperature() {
		return highTemperature;
	}

	public void setHighTemperature(String highTemperature) {
		this.highTemperature = highTemperature;
	}

	public String getAvgTemperature() {
		return avgTemperature;
	}

	public void setAvgTemperature(String avgTemperature) {
		this.avgTemperature = avgTemperature;
	}

	public String getEquimentNumber() {
		return equimentNumber;
	}

	public void setEquimentNumber(String equimentNumber) {
		this.equimentNumber = equimentNumber;
	}

	public String getRecorder() {
		return recorder;
	}

	public void setRecorder(String recorder) {
		this.recorder = recorder;
	}

	public String getPersonFollowCar() {
		return personFollowCar;
	}

	public void setPersonFollowCar(String personFollowCar) {
		this.personFollowCar = personFollowCar;
	}

	public String getReinfusionPlanDate() {
		return reinfusionPlanDate;
	}

	public void setReinfusionPlanDate(String reinfusionPlanDate) {
		this.reinfusionPlanDate = reinfusionPlanDate;
	}

	public String getOperatingRoomName() {
		return operatingRoomName;
	}

	public void setOperatingRoomName(String operatingRoomName) {
		this.operatingRoomName = operatingRoomName;
	}

	public String getOperatingRoomId() {
		return operatingRoomId;
	}

	public void setOperatingRoomId(String operatingRoomId) {
		this.operatingRoomId = operatingRoomId;
	}

	/** 是否通知到医生 */
	private String doctorNotice;
	/** 医生回复 */
	private String doctorReply;
	/** 是否通知到患者 */
	private String patientNotice;
	/** 患者回复 */
	private String patientReply;
	/** 是否运输交接 */
	private String transportState;
	/** 医院 */
	private String hospital;
	/** 科室 */
	private String hospitalDepartment;
	/** 医生 */
	private String doctor;
	/** 医院电话 */
	private String hospitalTel;
	/** 医生电话 */
	private String doctorTel;
	/** 收获时间 */
	private Date harvestDate;
	/** 收获结束时间 */
	private Date endHarvestDate;

	/** 细胞时间 */
	private Date cellDate;

	/** 是否有效 */
	private String efficacious;

	/** 签收人联系电话 */
	private String phoneNumber;

	// 运输公司
	private String transportCompany;
	
	//输血器
	private String transfusion;
	//生理盐水
	private String saline;
	//是否提交
	private String submitData;
	
	
	/** 产品运输左侧表的状态 1放行审核通过0放行审核未通过 */
	private String planState;
	
	//是否通知
	private String notification;
	//是否确认
	private String affirm;
	//采血物品
	private String bloodSampling;
	//运输单号
	private String oddNumber;
	//联系人电话
	private String linkmanPhone;
	//联系人
	private String linkman;
	
	
	

	public String getLinkmanPhone() {
		return linkmanPhone;
	}

	public void setLinkmanPhone(String linkmanPhone) {
		this.linkmanPhone = linkmanPhone;
	}

	public String getLinkman() {
		return linkman;
	}

	public void setLinkman(String linkman) {
		this.linkman = linkman;
	}

	public String getOddNumber() {
		return oddNumber;
	}

	public void setOddNumber(String oddNumber) {
		this.oddNumber = oddNumber;
	}

	public String getNotification() {
		return notification;
	}

	public void setNotification(String notification) {
		this.notification = notification;
	}

	public String getAffirm() {
		return affirm;
	}

	public void setAffirm(String affirm) {
		this.affirm = affirm;
	}

	public String getBloodSampling() {
		return bloodSampling;
	}

	public void setBloodSampling(String bloodSampling) {
		this.bloodSampling = bloodSampling;
	}

	public String getPlanState() {
		return planState;
	}

	public void setPlanState(String planState) {
		this.planState = planState;
	}

	public String getSubmitData() {
		return submitData;
	}

	public void setSubmitData(String submitData) {
		this.submitData = submitData;
	}

	public String getTransfusion() {
		return transfusion;
	}

	public void setTransfusion(String transfusion) {
		this.transfusion = transfusion;
	}

	public String getSaline() {
		return saline;
	}

	public void setSaline(String saline) {
		this.saline = saline;
	}

	public String getTransportCompany() {
		return transportCompany;
	}

	public void setTransportCompany(String transportCompany) {
		this.transportCompany = transportCompany;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEfficacious() {
		return efficacious;
	}

	public void setEfficacious(String efficacious) {
		this.efficacious = efficacious;
	}

	public Date getEndHarvestDate() {
		return endHarvestDate;
	}

	public void setEndHarvestDate(Date endHarvestDate) {
		this.endHarvestDate = endHarvestDate;
	}

	public Date getCellDate() {
		return cellDate;
	}

	public void setCellDate(Date cellDate) {
		this.cellDate = cellDate;
	}

	/** 地址 */
	private String address;

	/** 运送单号 */
	private String shippingNo;
	/** 签收人 */
	private String signatory;
	/** 签收日期 */
	private Date signatureDate;
	/** 签收确认人 */
	private String confirmationUser;
	/** 确认日期 */
	private Date confirmationDate;
	/** 运输费 */
	private String freight;

	/** 细胞 */
	private String cellula;

	public String getCellula() {
		return cellula;
	}

	public void setCellula(String cellula) {
		this.cellula = cellula;
	}

	public String getShippingNo() {
		return shippingNo;
	}

	public void setShippingNo(String shippingNo) {
		this.shippingNo = shippingNo;
	}

	public String getSignatory() {
		return signatory;
	}

	public void setSignatory(String signatory) {
		this.signatory = signatory;
	}

	public Date getSignatureDate() {
		return signatureDate;
	}

	public void setSignatureDate(Date signatureDate) {
		this.signatureDate = signatureDate;
	}

	public String getConfirmationUser() {
		return confirmationUser;
	}

	public void setConfirmationUser(String confirmationUser) {
		this.confirmationUser = confirmationUser;
	}

	public Date getConfirmationDate() {
		return confirmationDate;
	}

	public void setConfirmationDate(Date confirmationDate) {
		this.confirmationDate = confirmationDate;
	}

	public String getFreight() {
		return freight;
	}

	public void setFreight(String freight) {
		this.freight = freight;
	}

	public Date getHarvestDate() {
		return harvestDate;
	}

	public void setHarvestDate(Date harvestDate) {
		this.harvestDate = harvestDate;
	}

	public String getHospital() {
		return hospital;
	}

	public void setHospital(String hospital) {
		this.hospital = hospital;
	}

	public String getHospitalDepartment() {
		return hospitalDepartment;
	}

	public void setHospitalDepartment(String hospitalDepartment) {
		this.hospitalDepartment = hospitalDepartment;
	}

	public String getDoctor() {
		return doctor;
	}

	public void setDoctor(String doctor) {
		this.doctor = doctor;
	}

	public String getHospitalTel() {
		return hospitalTel;
	}

	public void setHospitalTel(String hospitalTel) {
		this.hospitalTel = hospitalTel;
	}

	public String getDoctorTel() {
		return doctorTel;
	}

	public void setDoctorTel(String doctorTel) {
		this.doctorTel = doctorTel;
	}

	public String getTransportState() {
		return transportState;
	}

	public void setTransportState(String transportState) {
		this.transportState = transportState;
	}

	public String getDoctorNotice() {
		return doctorNotice;
	}

	public void setDoctorNotice(String doctorNotice) {
		this.doctorNotice = doctorNotice;
	}

	public String getDoctorReply() {
		return doctorReply;
	}

	public void setDoctorReply(String doctorReply) {
		this.doctorReply = doctorReply;
	}

	public String getPatientNotice() {
		return patientNotice;
	}

	public void setPatientNotice(String patientNotice) {
		this.patientNotice = patientNotice;
	}

	public String getPatientReply() {
		return patientReply;
	}

	public void setPatientReply(String patientReply) {
		this.patientReply = patientReply;
	}

	public Date getTransportDate2() {
		return transportDate2;
	}

	public void setTransportDate2(Date transportDate2) {
		this.transportDate2 = transportDate2;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Date getTransportDate() {
		return transportDate;
	}

	public void setTransportDate(Date transportDate) {
		this.transportDate = transportDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")
	public SampleOrder getSampleOrder() {
		return sampleOrder;
	}

	public void setSampleOrder(SampleOrder sampleOrder) {
		this.sampleOrder = sampleOrder;
	}

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	public String getSampleType() {
		return sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	public String getPronoun() {
		return pronoun;
	}

	public void setPronoun(String pronoun) {
		this.pronoun = pronoun;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSampleCode() {
		return sampleCode;
	}

	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_SAMPLE_TYPE")
	@ForeignKey(name = "null")
	public DicSampleType getDicSampleType() {
		return dicSampleType;
	}

	public void setDicSampleType(DicSampleType dicSampleType) {
		this.dicSampleType = dicSampleType;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 序号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 序号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 细胞名称
	 */
	@Column(name = "CELL_NAME", length = 36)
	public String getCellName() {
		return this.cellName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 细胞名称
	 */
	public void setCellName(String cellName) {
		this.cellName = cellName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 克隆号
	 */
	@Column(name = "CLONE_CODE", length = 36)
	public String getCloneCode() {
		return this.cloneCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 克隆号
	 */
	public void setCloneCode(String cloneCode) {
		this.cloneCode = cloneCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 代次
	 */
	@Column(name = "GENERATION", length = 36)
	public String getGeneration() {
		return this.generation;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 代次
	 */
	public void setGeneration(String generation) {
		this.generation = generation;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 细胞背景
	 */
	@Column(name = "BACKGROUND", length = 36)
	public String getBackground() {
		return this.background;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 细胞背景
	 */
	public void setBackground(String background) {
		this.background = background;
	}

	/**
	 * 方法: 取得Integer
	 * 
	 * @return: Integer 管数
	 */
	@Column(name = "NUM", length = 60)
	public Integer getNum() {
		return this.num;
	}

	/**
	 * 方法: 设置Integer
	 * 
	 * @param: Integer 管数
	 */
	public void setNum(Integer num) {
		this.num = num;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 运输方式
	 */
	@Column(name = "WAY", length = 36)
	public String getWay() {
		return this.way;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 运输方式
	 */
	public void setWay(String way) {
		this.way = way;
	}

	/**
	 * 方法: 取得TransportOrder
	 * 
	 * @return: TransportOrder 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TRANSPORT_ORDER")
	public TransportOrder getTransportOrder() {
		return this.transportOrder;
	}

	/**
	 * 方法: 设置TransportOrder
	 * 
	 * @param: TransportOrder 相关主表
	 */
	public void setTransportOrder(TransportOrder transportOrder) {
		this.transportOrder = transportOrder;
	}

	/**
	 * 方法: 取得 String
	 * 
	 * @return: String 课题名称
	 */
	@Column(name = "PROJECT_NAME", length = 36)
	public String getProjectName() {
		return projectName;
	}

	/**
	 * 方法: 设置 String
	 * 
	 * @return: String 课题名称
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	/**
	 * 方法: 取得 String
	 * 
	 * @return: String 种属
	 */
	@Column(name = "GENUS", length = 100)
	public String getGenus() {
		return genus;
	}

	/**
	 * 方法: 设置 String
	 * 
	 * @return: String 种属
	 */
	public void setGenus(String genus) {
		this.genus = genus;
	}

	/**
	 * 方法: 取得 String
	 * 
	 * @return: String 品系
	 */
	@Column(name = "STRAIN", length = 100)
	public String getStrain() {
		return strain;
	}

	/**
	 * 方法: 设置 String
	 * 
	 * @return: String 品系
	 */
	public void setStrain(String strain) {
		this.strain = strain;
	}

	/**
	 * 方法: 取得 String
	 * 
	 * @return: String 基因型
	 */
	@Column(name = "GENO_TYPE", length = 100)
	public String getGenoType() {
		return genoType;
	}

	/**
	 * 方法: 设置 String
	 * 
	 * @return: String 基因型
	 */
	public void setGenoType(String genoType) {
		this.genoType = genoType;
	}

	/**
	 * 方法: 取得 String
	 * 
	 * @return: String 细胞管数
	 */
	@Column(name = "NUM_TUBES", length = 60)
	public String getNumTubes() {
		return numTubes;
	}

	/**
	 * 方法: 设置 String
	 * 
	 * @return: String 细胞管数
	 */
	public void setNumTubes(String numTubes) {
		this.numTubes = numTubes;
	}

	/**
	 * 方法: 取得 String
	 * 
	 * @return: String 支原体检测（有/无）
	 */
	@Column(name = "DETECTION_MYCOPLASMA", length = 60)
	public String getDetectionMycoplasma() {
		return detectionMycoplasma;
	}

	/**
	 * 方法: 设置 String
	 * 
	 * @return: String 支原体检测（有/无）
	 */
	public void setDetectionMycoplasma(String detectionMycoplasma) {
		this.detectionMycoplasma = detectionMycoplasma;
	}

	/**
	 * 方法: 取得 String
	 * 
	 * @return: String 运输温度
	 */
	@Column(name = "TRANSPORT_TEMPERATURE", length = 60)
	public String getTransportTemperature() {
		return transportTemperature;
	}

	/**
	 * 方法: 设置 String
	 * 
	 * @return: String 运输温度
	 */
	public void setTransportTemperature(String transportTemperature) {
		this.transportTemperature = transportTemperature;
	}

	/**
	 * 方法: 取得 String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 200)
	public String getNote() {
		return note;
	}

	/**
	 * 方法: 设置 String
	 * 
	 * @return: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得 String
	 * 
	 * @return: String 电转次数
	 */
	@Column(name = "ELECTRIC_TRANS_TIME", length = 60)
	public String getElectricTransTime() {
		return electricTransTime;
	}

	/**
	 * 方法: 设置 String
	 * 
	 * @return: String 电转次数
	 */
	public void setElectricTransTime(String electricTransTime) {
		this.electricTransTime = electricTransTime;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "temple_operator")
	public User getTempleOperator() {
		return templeOperator;
	}
	public void setTempleOperator(User templeOperator) {
		this.templeOperator = templeOperator;
	}



}