package com.biolims.tra.transport.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;

/**   
 * @Title: Model
 * @Description: 质粒运输单
 * @author lims-platform
 * @date 2015-09-16 18:26:14
 * @version V1.0   
 *
 */
@Entity
@Table(name = "TRANSPORT_ORDER_PLASMID")
@SuppressWarnings("serial")
public class TransportOrderPlasmid extends EntityDao<TransportOrderPlasmid> implements java.io.Serializable {
	/**序号*/
	private String id;
	/**物品类型*/
	private String objName;
	/**质粒名称*/
	private String plasmidName;
	/**课题名称*/
	private String projectName;
	/**浓度*/
	private String concentration;
	/**体积*/
	private Integer volume;
	/**抗体*/
	private String antibody;
	/**抗性*/
	private String resistance;
	/**260/280*/
	private Integer od260;
	/**260/230*/
	private Integer od230;
	/**运输温度*/
	private String transportTemperature;
	/**备注*/
	private String note;
	/**相关主表*/
	private TransportOrder transportOrder;

	/**
	 *方法: 取得String
	 *@return: String  序号
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 *方法: 设置String
	 *@param: String  序号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 *方法: 取得String
	 *@return: String  物品类型
	 */
	@Column(name = "OBJ_NAME", length = 36)
	public String getObjName() {
		return this.objName;
	}

	/**
	 *方法: 设置String
	 *@param: String  物品类型
	 */
	public void setObjName(String objName) {
		this.objName = objName;
	}

	/**
	 *方法: 取得String
	 *@return: String  课题名称
	 */
	@Column(name = "PROJECT_NAME", length = 36)
	public String getProjectName() {
		return this.projectName;
	}

	/**
	 *方法: 设置String
	 *@param: String  课题名称
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	/**
	 *方法: 取得     String
	 *@return: String  浓度
	 */
	@Column(name = "CONCENTRATION", length = 60)
	public String getConcentration() {
		return this.concentration;
	}

	/**
	 *方法: 设置  String
	 *@param: String  浓度
	 */
	public void setConcentration(String concentration) {
		this.concentration = concentration;
	}

	/**
	 *方法: 取得Integer
	 *@return: Integer  体积
	 */
	@Column(name = "VOLUME", length = 36)
	public Integer getVolume() {
		return this.volume;
	}

	/**
	 *方法: 设置Integer
	 *@param: Integer  体积
	 */
	public void setVolume(Integer volume) {
		this.volume = volume;
	}

	/**
	 *方法: 取得String
	 *@return: String  抗体
	 */
	@Column(name = "ANTIBODY", length = 60)
	public String getAntibody() {
		return this.antibody;
	}

	/**
	 *方法: 设置String
	 *@param: String  抗体
	 */
	public void setAntibody(String antibody) {
		this.antibody = antibody;
	}

	/**
	 *方法: 取得Integer
	 *@return: Integer  260/280
	 */
	@Column(name = "OD_260", length = 36)
	public Integer getOd260() {
		return this.od260;
	}

	/**
	 *方法: 设置Integer
	 *@param: Integer  260/280
	 */
	public void setOd260(Integer od260) {
		this.od260 = od260;
	}

	/**
	 *方法: 取得Integer
	 *@return: Integer  260/230
	 */
	@Column(name = "OD_230", length = 36)
	public Integer getOd230() {
		return this.od230;
	}

	/**
	 *方法: 设置Integer
	 *@param: Integer  260/230
	 */
	public void setOd230(Integer od230) {
		this.od230 = od230;
	}

	/**
	 *方法: 取得TransportOrder
	 *@return: TransportOrder  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TRANSPORT_ORDER")
	public TransportOrder getTransportOrder() {
		return this.transportOrder;
	}

	/**
	 *方法: 设置TransportOrder
	 *@param: TransportOrder  相关主表
	 */
	public void setTransportOrder(TransportOrder transportOrder) {
		this.transportOrder = transportOrder;
	}

	/**
	 *方法: 取得     String
	 *@return: String  质粒名称
	 */
	@Column(name = "PLASMID_NAME", length = 200)
	public String getPlasmidName() {
		return plasmidName;
	}

	/**
	 *方法: 设置     String
	 *@return: String  质粒名称
	 */
	public void setPlasmidName(String plasmidName) {
		this.plasmidName = plasmidName;
	}

	/**
	 *方法: 取得     String
	 *@return: String  抗性
	 */
	@Column(name = "RESISTANCE", length = 100)
	public String getResistance() {
		return resistance;
	}

	/**
	 *方法: 设置     String
	 *@return: String  抗性
	 */
	public void setResistance(String resistance) {
		this.resistance = resistance;
	}

	/**
	 *方法: 取得     String
	 *@return: String  运输温度
	 */
	@Column(name = "TRANSPORT_TEMPERATURE", length = 100)
	public String getTransportTemperature() {
		return transportTemperature;
	}

	/**
	 *方法: 设置     String
	 *@return: String  运输温度
	 */
	public void setTransportTemperature(String transportTemperature) {
		this.transportTemperature = transportTemperature;
	}

	/**
	 *方法: 取得     String
	 *@return: String  备注
	 */
	@Column(name = "NOTE", length = 200)
	public String getNote() {
		return note;
	}

	/**
	 *方法: 设置     String
	 *@return: String  备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

}