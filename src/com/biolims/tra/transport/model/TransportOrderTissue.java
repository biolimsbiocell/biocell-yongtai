package com.biolims.tra.transport.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;

/**   
 * @Title: Model
 * @Description: 组织运输单
 * @author lims-platform
 * @date 2015-09-16 18:26:13
 * @version V1.0   
 *
 */
@Entity
@Table(name = "TRANSPORT_ORDER_TISSUE")
@SuppressWarnings("serial")
public class TransportOrderTissue extends EntityDao<TransportOrderTissue> implements java.io.Serializable {
	/**序号*/
	private String id;
	/**组织名称*/
	private String tissueName; 
	/**动物编号*/
	private String animalId;   
	/**动物名称*/
	private String animalName;
	/**品系*/
	private String strain;
	/**性别*/
	private String gender;
	/**出生日期*/
	private Date birthDate;    
	/**标记*/
	private String sign;
	/**基因型*/
	private String genetype; 
	/**运输温度*/
	private String transportTemperature; 
	/**备注*/
	private String note; 
	/**数量*/
	private Integer num; 
	/**运输方式*/
	private DicType way;    
	/**相关主表*/
	private TransportOrder transportOrder;

	/**
	 *方法: 取得     String
	 *@return: String  序号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 *方法: 设置  String
	 *@param: String  序号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 *方法: 取得     String
	 *@return: String  动物编号
	 */
	@Column(name = "ANIMAL_ID", length = 36)
	public String getAnimalId() {
		return this.animalId;
	}

	/**
	 *方法: 设置  String
	 *@param: String  动物编号
	 */
	public void setAnimalId(String animalId) {
		this.animalId = animalId;
	}

	/**
	 *方法: 取得     String
	 *@return: String  动物名称
	 */
	@Column(name = "ANIMAL_NAME", length = 36)
	public String getAnimalName() {
		return this.animalName;
	}

	/**
	 *方法: 设置  String
	 *@param: String  动物名称
	 */
	public void setAnimalName(String animalName) {
		this.animalName = animalName;
	}

	/**
	 *方法: 取得     String
	 *@return: String  性别
	 */
	@Column(name = "GENDER", length = 36)
	public String getGender() {
		return this.gender;
	}

	/**
	 *方法: 设置  String
	 *@param: String  性别
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 *方法: 取得     Integer
	 *@return: Integer  数量
	 */
	@Column(name = "NUM", length = 36)
	public Integer getNum() {
		return this.num;
	}

	/**
	 *方法: 设置  Integer
	 *@param: Integer  数量
	 */
	public void setNum(Integer num) {
		this.num = num;
	}

	/**
	 *方法: 取得     String
	 *@return: String  基因型
	 */
	@Column(name = "GENETYPE", length = 60)
	public String getGenetype() {
		return this.genetype;
	}

	/**
	 *方法: 设置  String
	 *@param: String  基因型
	 */
	public void setGenetype(String genetype) {
		this.genetype = genetype;
	}

	/**
	 *方法: 取得     String
	 *@return: String  运输方式
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "WAY")
	public DicType getWay() {
		return this.way;
	}

	/**
	 *方法: 设置  String
	 *@param: String  运输方式
	 */
	public void setWay(DicType way) {
		this.way = way;
	}

	/**
	 *方法: 取得     TransportOrder
	 *@return: TransportOrder  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TRANSPORT_ORDER")
	public TransportOrder getTransportOrder() {
		return this.transportOrder;
	}

	/**
	 *方法: 设置  TransportOrder
	 *@param: TransportOrder  相关主表
	 */
	public void setTransportOrder(TransportOrder transportOrder) {
		this.transportOrder = transportOrder;
	}

	/**
	 *方法: 取得String
	 *@return: String  出生日期
	 */
	@Column(name = "BIRTH_DATE", length = 255)
	public Date getBirthDate() {
		return this.birthDate;
	}

	/**
	 *方法: 设置String
	 *@param: String  出生日期
	 */
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	
	/**
	 *方法: 取得     String
	 *@return: String  组织名称
	 */
	@Column(name = "TISSUE_NAME", length = 100)
	public String getTissueName() {
		return tissueName;
	}
	
	/**
	 *方法: 设置     String
	 *@return: String  组织名称
	 */
	public void setTissueName(String tissueName) {
		this.tissueName = tissueName;
	}
	
	/**
	 *方法: 取得     String
	 *@return: String  品系
	 */
	@Column(name = "STRAIN", length = 100)
	public String getStrain() {
		return strain;
	}
	
	/**
	 *方法: 设置     String
	 *@return: String  品系
	 */
	public void setStrain(String strain) {
		this.strain = strain;
	}
	
	/**
	 *方法: 取得     String
	 *@return: String  标记
	 */
	@Column(name = "SIGN", length = 100)
	public String getSign() {
		return sign;
	}
	
	/**
	 *方法: 设置     String
	 *@return: String  标记
	 */
	public void setSign(String sign) {
		this.sign = sign;
	}
	
	/**
	 *方法: 取得     String
	 *@return: String  备注
	 */
	@Column(name = "NOTE", length = 255)
	public String getNote() {
		return note;
	}
	
	/**
	 *方法: 设置     String
	 *@return: String  备注
	 */
	public void setNote(String note) {
		this.note = note;
	}
	
	/**
	 *方法: 设置     String
	 *@return: String  运输温度
	 */
	@Column(name = "TRANSPORT_TEMPERATURE", length = 150)
	public String getTransportTemperature() {
		return transportTemperature;
	}
	
	/**
	 *方法: 取得     String
	 *@return: String  运输温度
	 */
	public void setTransportTemperature(String transportTemperature) {
		this.transportTemperature = transportTemperature;
	}
}