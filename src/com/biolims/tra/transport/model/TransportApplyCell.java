package com.biolims.tra.transport.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.sample.model.SampleOrder;

/**
 * @Title: Model
 * @Description: 运送物品(细胞)情况
 * @author lims-platform
 * @date 2015-08-03 11:32:08
 * @version V1.0
 *
 */
@Entity
@Table(name = "TRANSPORT_APPLY_CELL")
@SuppressWarnings("serial")
public class TransportApplyCell extends EntityDao<TransportApplyCell> implements java.io.Serializable {
	/** 编码 */
	private String id;

	/** 数量(管) */
	private Integer num;
	/** 运输方式 */
	private DicType way;
	/** 相关主表 */
	private TransportApply transportApply;
	/** 是否在Dialog中显示 */
	private String isDialog;
	/** 左侧表ID */
	private String tempId;
	/** 关联订单 */
	private SampleOrder sampleOrder;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	public SampleOrder getSampleOrder() {
		return sampleOrder;
	}

	public void setSampleOrder(SampleOrder sampleOrder) {
		this.sampleOrder = sampleOrder;
	}

	public String getTempId() {
		return tempId;
	}

	public void setTempId(String tempId) {
		this.tempId = tempId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得Integer
	 * 
	 * @return: Integer 数量(管)
	 */
	@Column(name = "NUM", length = 60)
	public Integer getNum() {
		return this.num;
	}

	/**
	 * 方法: 设置Integer
	 * 
	 * @param: Integer
	 *             数量(管)
	 */
	public void setNum(Integer num) {
		this.num = num;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 运输方式
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "WAY")
	public DicType getWay() {
		return this.way;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             运输方式
	 */
	public void setWay(DicType way) {
		this.way = way;
	}

	/**
	 * 方法: 取得TransportApply
	 * 
	 * @return: TransportApply 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TRANSPORT_APPLY")
	public TransportApply getTransportApply() {
		return this.transportApply;
	}

	/**
	 * 方法: 设置TransportApply
	 * 
	 * @param: TransportApply
	 *             相关主表
	 */
	public void setTransportApply(TransportApply transportApply) {
		this.transportApply = transportApply;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String isDialog
	 */
	@Column(name = "IS_DIALOG", length = 50)
	public String getIsDialog() {
		return isDialog;
	}

	public void setIsDialog(String isDialog) {
		this.isDialog = isDialog;
	}

}