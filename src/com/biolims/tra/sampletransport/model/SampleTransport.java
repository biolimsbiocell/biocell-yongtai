package com.biolims.tra.sampletransport.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.system.express.model.ExpressCompany;
import com.biolims.common.model.user.User;
/**   
 * @Title: Model
 * @Description: 样本运输
 * @author lims-platform
 * @date 2018-07-30 16:20:22
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SAMPLE_TRANSPORT")
@SuppressWarnings("serial")
public class SampleTransport extends EntityDao<SampleTransport> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
	/**创建人*/
	private User createUser;
	/**提交时间*/
	private Date createDate;
	/**状态id*/
	private String state;
	/**状态*/
	private String stateName;
	/**备注*/
	private String note;
	/**运输人*/
	private String transportUser;
	/**运输公司*/
	private String transportCompany;
	/**运输单号*/
	private String trackingNumber;
	/**开始时间*/
	private Date beginDate;
	/**结束时间*/
	private Date endDate;
	
	private String scopeId;
	private String scopeName;
	
	public String getScopeId() {
		return scopeId;
	}
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}
	public String getScopeName() {
		return scopeName;
	}
	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@Column(name ="ID", length = 255)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 255)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得User
	 *@return: User  创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser(){
		return this.createUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  创建人
	 */
	public void setCreateUser(User createUser){
		this.createUser = createUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  提交时间
	 */
	@Column(name ="CREATE_DATE", length = 255)
	public Date getCreateDate(){
		return this.createDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  提交时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态id
	 */
	@Column(name ="STATE", length = 255)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态id
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE_NAME", length = 255)
	public String getStateName(){
		return this.stateName;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setStateName(String stateName){
		this.stateName = stateName;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 255)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得User
	 *@return: User  运输人
	 */
	/*@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TRANSPORT_USER")*/
	public String getTransportUser(){
		return this.transportUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  运输人
	 */
	public void setTransportUser(String transportUser){
		this.transportUser = transportUser;
	}
	/**
	 *方法: 取得String
	 *@return: String  运输公司
	 */
	@Column(name ="TRANSPORT_COMPANY", length = 255)
	public String getTransportCompany(){
		return this.transportCompany;
	}
	/**
	 *方法: 设置String
	 *@param: String  运输公司
	 */
	public void setTransportCompany(String transportCompany){
		this.transportCompany = transportCompany;
	}
	/**
	 *方法: 取得String
	 *@return: String  运输单号
	 */
	@Column(name ="TRACKING_NUMBER", length = 255)
	public String getTrackingNumber(){
		return this.trackingNumber;
	}
	/**
	 *方法: 设置String
	 *@param: String  运输单号
	 */
	public void setTrackingNumber(String trackingNumber){
		this.trackingNumber = trackingNumber;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  开始时间
	 */
	@Column(name ="BEGIN_DATE", length = 255)
	public Date getBeginDate(){
		return this.beginDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  开始时间
	 */
	public void setBeginDate(Date beginDate){
		this.beginDate = beginDate;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  结束时间
	 */
	@Column(name ="END_DATE", length = 255)
	public Date getEndDate(){
		return this.endDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  结束时间
	 */
	public void setEndDate(Date endDate){
		this.endDate = endDate;
	}
}


/*


//中文JS配置文件
biolims.sampleTransport={};	
biolims.sampleTransport.id="编号";
biolims.sampleTransport.name="描述";
biolims.sampleTransport.createUser="创建人";
biolims.sampleTransport.createDate="提交时间";
biolims.sampleTransport.state="状态id";
biolims.sampleTransport.stateName="状态";
biolims.sampleTransport.note="备注";
biolims.sampleTransport.transportUser="运输人";
biolims.sampleTransport.transportCompany="运输公司";
biolims.sampleTransport.trackingNumber="运输单号";
biolims.sampleTransport.beginDate="开始时间";
biolims.sampleTransport.endDate="结束时间";




//英文JS配置文件
biolims.sampleTransport.id="id";
biolims.sampleTransport.name="name";
biolims.sampleTransport.createUser="createUser";
biolims.sampleTransport.createDate="createDate";
biolims.sampleTransport.state="state";
biolims.sampleTransport.stateName="stateName";
biolims.sampleTransport.note="note";
biolims.sampleTransport.transportUser="transportUser";
biolims.sampleTransport.transportCompany="transportCompany";
biolims.sampleTransport.trackingNumber="trackingNumber";
biolims.sampleTransport.beginDate="beginDate";
biolims.sampleTransport.endDate="endDate";


//中文配置文件
biolims.sampleTransport.id=编号
biolims.sampleTransport.name=描述
biolims.sampleTransport.createUser=创建人
biolims.sampleTransport.createDate=提交时间
biolims.sampleTransport.state=状态id
biolims.sampleTransport.stateName=状态
biolims.sampleTransport.note=备注
biolims.sampleTransport.transportUser=运输人
biolims.sampleTransport.transportCompany=运输公司
biolims.sampleTransport.trackingNumber=运输单号
biolims.sampleTransport.beginDate=开始时间
biolims.sampleTransport.endDate=结束时间


//英文配置文件
biolims.sampleTransport.id=id
biolims.sampleTransport.name=name
biolims.sampleTransport.createUser=createUser
biolims.sampleTransport.createDate=createDate
biolims.sampleTransport.state=state
biolims.sampleTransport.stateName=stateName
biolims.sampleTransport.note=note
biolims.sampleTransport.transportUser=transportUser
biolims.sampleTransport.transportCompany=transportCompany
biolims.sampleTransport.trackingNumber=trackingNumber
biolims.sampleTransport.beginDate=beginDate
biolims.sampleTransport.endDate=endDate

*/