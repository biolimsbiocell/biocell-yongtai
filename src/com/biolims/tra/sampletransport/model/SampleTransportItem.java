package com.biolims.tra.sampletransport.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleOrder;
import com.biolims.system.express.model.ExpressCompany;
import com.biolims.tra.transport.model.TransportOrder;

/**
 * @Title: Model
 * @Description: 样本运输明细
 * @author lims-platform
 * @date 2018-07-30 16:20:19
 * @version V1.0
 *
 */
@Entity
@Table(name = "SAMPLE_TRANSPORT_ITEM")
@SuppressWarnings("serial")
public class SampleTransportItem extends EntityDao<SampleTransportItem> implements java.io.Serializable {
	/** 编号 */
	private String id;
	/** 描述 */
	private String name;
	/** 创建人 */
	private User createUser;
	/** 提交时间 */
	private Date createDate;
	/** 状态id */
	private String state;
	/** 状态 */
	private String stateName;
	/** 备注 */
	private String note;
	/** 计划耗时 */
	private Date planTime;
	/** 实际耗时 */
	private String actualTime;
	/** 主表实体 */
	private SampleTransport sampleTransport;
	/** 订单号 **/
	private String orderNo;
	/** 作废状态 */
	private String invalidState;
	/** 采血时间 */
	private Date samplingDate;
	/** 楼层 */
	private String floor;
	/** 取血人 */
	private String bloodCollection;
	/** 订单 */
	private SampleOrder sampleOrder;
	/** 预计采血时间 */
	@Column(name = "draw_blood_time", length = 50)
	private Date drawBloodTime;

	/** 地址 */
	private String address;

	/** 签收确认人 */
	private User approvalUser;
	/** 签收日期 */
	private Date acceptDate;

	// 确认日期
	private String confirmationDate;

	// 运输费
	private String freight;

	// 样本类型 每次保存时更新
	private DicSampleType sampleType;

	// 运输公司
	private ExpressCompany expressCompany;
	
	//签收人联系电话
	private String phoneNumber;
	
	/**序号*/
	private String sequenceNumber;
	
	
	/***/
	//是否通知
	private String notification;
	//是否确认
	private String affirm;
	//采血物品
	private String bloodSampling;
	//输血器
	private String transfusion;
	//生理盐水
	private String saline;
	//运输单号
	private String oddNumber;
	//运输温度
	private String transportTemperature;
	//联系人电话
	private String linkmanPhone;
	//联系人
	private String linkman;
	
	
	/**
	 * 新增
	 */
	private String personFollowCar;//跟车人员
	

	public String getPersonFollowCar() {
		return personFollowCar;
	}

	public void setPersonFollowCar(String personFollowCar) {
		this.personFollowCar = personFollowCar;
	}

	public String getLinkmanPhone() {
		return linkmanPhone;
	}

	public void setLinkmanPhone(String linkmanPhone) {
		this.linkmanPhone = linkmanPhone;
	}

	public String getLinkman() {
		return linkman;
	}

	public void setLinkman(String linkman) {
		this.linkman = linkman;
	}

	public String getTransportTemperature() {
		return transportTemperature;
	}

	public void setTransportTemperature(String transportTemperature) {
		this.transportTemperature = transportTemperature;
	}

	public String getNotification() {
		return notification;
	}

	public void setNotification(String notification) {
		this.notification = notification;
	}

	public String getAffirm() {
		return affirm;
	}

	public void setAffirm(String affirm) {
		this.affirm = affirm;
	}

	public String getBloodSampling() {
		return bloodSampling;
	}

	public void setBloodSampling(String bloodSampling) {
		this.bloodSampling = bloodSampling;
	}

	public String getTransfusion() {
		return transfusion;
	}

	public void setTransfusion(String transfusion) {
		this.transfusion = transfusion;
	}

	public String getSaline() {
		return saline;
	}

	public void setSaline(String saline) {
		this.saline = saline;
	}

	public String getOddNumber() {
		return oddNumber;
	}

	public void setOddNumber(String oddNumber) {
		this.oddNumber = oddNumber;
	}

	public String getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	public ExpressCompany getExpressCompany() {
		return expressCompany;
	}

	public void setExpressCompany(ExpressCompany expressCompany) {
		this.expressCompany = expressCompany;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "SAMPLE_TYPE")
	public DicSampleType getSampleType() {
		return sampleType;
	}

	public void setSampleType(DicSampleType sampleType) {
		this.sampleType = sampleType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	public User getApprovalUser() {
		return approvalUser;
	}

	public void setApprovalUser(User approvalUser) {
		this.approvalUser = approvalUser;
	}

	public Date getAcceptDate() {
		return acceptDate;
	}

	public void setAcceptDate(Date acceptDate) {
		this.acceptDate = acceptDate;
	}

	public String getConfirmationDate() {
		return confirmationDate;
	}

	public void setConfirmationDate(String confirmationDate) {
		this.confirmationDate = confirmationDate;
	}

	public String getFreight() {
		return freight;
	}

	public void setFreight(String freight) {
		this.freight = freight;
	}

	public Date getDrawBloodTime() {
		return drawBloodTime;
	}

	public void setDrawBloodTime(Date drawBloodTime) {
		this.drawBloodTime = drawBloodTime;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "SAMPLE_ORDER")
	public SampleOrder getSampleOrder() {
		return sampleOrder;
	}

	public void setSampleOrder(SampleOrder sampleOrder) {
		this.sampleOrder = sampleOrder;
	}

	/***/
	private TransportOrder transportOrder;

	public Date getSamplingDate() {
		return samplingDate;
	}

	public void setSamplingDate(Date samplingDate) {
		this.samplingDate = samplingDate;
	}

	public String getInvalidState() {
		return invalidState;
	}

	public void setInvalidState(String invalidState) {
		this.invalidState = invalidState;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "TRANSPORT_ORDER")
	public TransportOrder getTransportOrder() {
		return transportOrder;
	}

	public void setTransportOrder(TransportOrder transportOrder) {
		this.transportOrder = transportOrder;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "SAMPLE_TRANSPORT")
	public SampleTransport getSampleTransport() {
		return sampleTransport;
	}

	public void setSampleTransport(SampleTransport sampleTransport) {
		this.sampleTransport = sampleTransport;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 255)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 描述
	 */
	@Column(name = "NAME", length = 255)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得User
	 * 
	 * @return: User 创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 * 方法: 设置User
	 * 
	 * @param: User 创建人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得Date
	 * 
	 * @return: Date 提交时间
	 */
	@Column(name = "CREATE_DATE", length = 255)
	public Date getCreateDate() {
		return this.createDate;
	}

	/**
	 * 方法: 设置Date
	 * 
	 * @param: Date 提交时间
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态id
	 */
	@Column(name = "STATE", length = 255)
	public String getState() {
		return this.state;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态id
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 状态
	 */
	@Column(name = "STATE_NAME", length = 255)
	public String getStateName() {
		return this.stateName;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 状态
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 255)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 计划耗时
	 */
	

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 实际耗时
	 */
	@Column(name = "ACTUAL_TIME", length = 255)
	public String getActualTime() {
		return this.actualTime;
	}
	@Column(name = "PLAN_TIME", length = 255)
	public Date getPlanTime() {
		return planTime;
	}

	public void setPlanTime(Date planTime) {
		this.planTime = planTime;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 实际耗时
	 */
	public void setActualTime(String actualTime) {
		this.actualTime = actualTime;
	}

	public String getFloor() {
		return floor;
	}

	public void setFloor(String floor) {
		this.floor = floor;
	}

	public String getBloodCollection() {
		return bloodCollection;
	}

	public void setBloodCollection(String bloodCollection) {
		this.bloodCollection = bloodCollection;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

}

/*
 * 
 * 
 * //中文JS配置文件 biolims.sampleTransportItem={};
 * biolims.sampleTransportItem.id="编号"; biolims.sampleTransportItem.name="描述";
 * biolims.sampleTransportItem.createUser="创建人";
 * biolims.sampleTransportItem.createDate="提交时间";
 * biolims.sampleTransportItem.state="状态id";
 * biolims.sampleTransportItem.stateName="状态";
 * biolims.sampleTransportItem.note="备注";
 * biolims.sampleTransportItem.planTime="计划耗时";
 * biolims.sampleTransportItem.actualTime="实际耗时";
 * 
 * 
 * 
 * 
 * //英文JS配置文件 biolims.sampleTransportItem.id="id";
 * biolims.sampleTransportItem.name="name";
 * biolims.sampleTransportItem.createUser="createUser";
 * biolims.sampleTransportItem.createDate="createDate";
 * biolims.sampleTransportItem.state="state";
 * biolims.sampleTransportItem.stateName="stateName";
 * biolims.sampleTransportItem.note="note";
 * biolims.sampleTransportItem.planTime="planTime";
 * biolims.sampleTransportItem.actualTime="actualTime";
 * 
 * 
 * //中文配置文件 biolims.sampleTransportItem.id=编号
 * biolims.sampleTransportItem.name=描述
 * biolims.sampleTransportItem.createUser=创建人
 * biolims.sampleTransportItem.createDate=提交时间
 * biolims.sampleTransportItem.state=状态id
 * biolims.sampleTransportItem.stateName=状态 biolims.sampleTransportItem.note=备注
 * biolims.sampleTransportItem.planTime=计划耗时
 * biolims.sampleTransportItem.actualTime=实际耗时
 * 
 * 
 * //英文配置文件 biolims.sampleTransportItem.id=id
 * biolims.sampleTransportItem.name=name
 * biolims.sampleTransportItem.createUser=createUser
 * biolims.sampleTransportItem.createDate=createDate
 * biolims.sampleTransportItem.state=state
 * biolims.sampleTransportItem.stateName=stateName
 * biolims.sampleTransportItem.note=note
 * biolims.sampleTransportItem.planTime=planTime
 * biolims.sampleTransportItem.actualTime=actualTime
 * 
 */