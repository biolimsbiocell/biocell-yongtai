package com.biolims.tra.sampletransport.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.sample.model.SampleOrder;
import com.biolims.tra.transport.model.TransportOrder;

/**
 * @Title: Model
 * @Description: 签收信息
 * @author lims-platform
 * @date 2018-07-30 16:20:19
 * @version V1.0
 *
 */
@Entity
@Table(name = "SAMPLE_TRANSPORT_STATE")
@SuppressWarnings("serial")
public class SampleTransportState extends EntityDao<SampleTransportState> implements java.io.Serializable {
	/** 编号 */
	private String id;
	/** 运送单号 */
	private String shippingNo;
	/** 签收人 */
	private String signatory;
	/** 签收日期 */
	private Date signatureDate;
	/** 签收确认人 */
	private String confirmationUser;
	/** 确认日期 */
	private Date confirmationDate;
	/** 运输费 */
	private String freight;
	/** 备注 */
	private String note;
	/** 运输计划主表 */
	private TransportOrder transportOrder;
	
	

	public String getShippingNo() {
		return shippingNo;
	}

	public void setShippingNo(String shippingNo) {
		this.shippingNo = shippingNo;
	}

	public String getSignatory() {
		return signatory;
	}

	public void setSignatory(String signatory) {
		this.signatory = signatory;
	}

	public Date getSignatureDate() {
		return signatureDate;
	}

	public void setSignatureDate(Date signatureDate) {
		this.signatureDate = signatureDate;
	}

	public String getConfirmationUser() {
		return confirmationUser;
	}

	public void setConfirmationUser(String confirmationUser) {
		this.confirmationUser = confirmationUser;
	}

	public Date getConfirmationDate() {
		return confirmationDate;
	}

	public void setConfirmationDate(Date confirmationDate) {
		this.confirmationDate = confirmationDate;
	}

	public String getFreight() {
		return freight;
	}

	public void setFreight(String freight) {
		this.freight = freight;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "none")
	@JoinColumn(name = "TRANSPORT_ORDER")
	public TransportOrder getTransportOrder() {
		return transportOrder;
	}

	public void setTransportOrder(TransportOrder transportOrder) {
		this.transportOrder = transportOrder;
	}


	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 255)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             编号
	 */
	public void setId(String id) {
		this.id = id;
	}


	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 255)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String
	 *             备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

}

/*
 * 
 * 
 * //中文JS配置文件 biolims.sampleTransportItem={};
 * biolims.sampleTransportItem.id="编号"; biolims.sampleTransportItem.name="描述";
 * biolims.sampleTransportItem.createUser="创建人";
 * biolims.sampleTransportItem.createDate="提交时间";
 * biolims.sampleTransportItem.state="状态id";
 * biolims.sampleTransportItem.stateName="状态";
 * biolims.sampleTransportItem.note="备注";
 * biolims.sampleTransportItem.planTime="计划耗时";
 * biolims.sampleTransportItem.actualTime="实际耗时";
 * 
 * 
 * 
 * 
 * //英文JS配置文件 biolims.sampleTransportItem.id="id";
 * biolims.sampleTransportItem.name="name";
 * biolims.sampleTransportItem.createUser="createUser";
 * biolims.sampleTransportItem.createDate="createDate";
 * biolims.sampleTransportItem.state="state";
 * biolims.sampleTransportItem.stateName="stateName";
 * biolims.sampleTransportItem.note="note";
 * biolims.sampleTransportItem.planTime="planTime";
 * biolims.sampleTransportItem.actualTime="actualTime";
 * 
 * 
 * //中文配置文件 biolims.sampleTransportItem.id=编号
 * biolims.sampleTransportItem.name=描述
 * biolims.sampleTransportItem.createUser=创建人
 * biolims.sampleTransportItem.createDate=提交时间
 * biolims.sampleTransportItem.state=状态id
 * biolims.sampleTransportItem.stateName=状态 biolims.sampleTransportItem.note=备注
 * biolims.sampleTransportItem.planTime=计划耗时
 * biolims.sampleTransportItem.actualTime=实际耗时
 * 
 * 
 * //英文配置文件 biolims.sampleTransportItem.id=id
 * biolims.sampleTransportItem.name=name
 * biolims.sampleTransportItem.createUser=createUser
 * biolims.sampleTransportItem.createDate=createDate
 * biolims.sampleTransportItem.state=state
 * biolims.sampleTransportItem.stateName=stateName
 * biolims.sampleTransportItem.note=note
 * biolims.sampleTransportItem.planTime=planTime
 * biolims.sampleTransportItem.actualTime=actualTime
 * 
 */