package com.biolims.tra.sampletransport.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.tra.sampletransport.model.*;

import com.biolims.common.model.user.User;
/**   
 * @Title: Model
 * @Description: 样本追踪情况
 * @author lims-platform
 * @date 2018-07-30 16:20:20
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SAMPLE_TRACKING_CONDITION")
@SuppressWarnings("serial")
public class SampleTrackingCondition extends EntityDao<SampleTrackingCondition> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
	/**创建人*/
	private User createUser;
	/**提交时间*/
	private Date createDate;
	/**状态id*/
	private String state;
	/**状态*/
	private String stateName;
	/**备注*/
	private String note;
	/**温度*/
	private String temperature;
	/**时间*/
	private Date changeTime;
	/**地点**/
	private String changeSite;
	
	/**主表实体*/
	private SampleTransport sampleTransport;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_TRANSPORT")
	public SampleTransport getSampleTransport() {
		return sampleTransport;
	}
	public void setSampleTransport(SampleTransport sampleTransport) {
		this.sampleTransport = sampleTransport;
	}
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 255)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	
	public String getChangeSite() {
		return changeSite;
	}
	public void setChangeSite(String changeSite) {
		this.changeSite = changeSite;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 255)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得User
	 *@return: User  创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser(){
		return this.createUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  创建人
	 */
	public void setCreateUser(User createUser){
		this.createUser = createUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  提交时间
	 */
	@Column(name ="CREATE_DATE", length = 255)
	public Date getCreateDate(){
		return this.createDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  提交时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态id
	 */
	@Column(name ="STATE", length = 255)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态id
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE_NAME", length = 255)
	public String getStateName(){
		return this.stateName;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setStateName(String stateName){
		this.stateName = stateName;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 255)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得String
	 *@return: String  温度
	 */
	@Column(name ="TEMPERATURE", length = 255)
	public String getTemperature(){
		return this.temperature;
	}
	/**
	 *方法: 设置String
	 *@param: String  温度
	 */
	public void setTemperature(String temperature){
		this.temperature = temperature;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  时间
	 */
	@Column(name ="CHANGE_TIME", length = 256)
	public Date getChangeTime(){
		return this.changeTime;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  时间
	 */
	public void setChangeTime(Date changeTime){
		this.changeTime = changeTime;
	}
}


/*


//中文JS配置文件
biolims.sampleTrackingCondition={};	
biolims.sampleTrackingCondition.id="编号";
biolims.sampleTrackingCondition.name="描述";
biolims.sampleTrackingCondition.createUser="创建人";
biolims.sampleTrackingCondition.createDate="提交时间";
biolims.sampleTrackingCondition.state="状态id";
biolims.sampleTrackingCondition.stateName="状态";
biolims.sampleTrackingCondition.note="备注";
biolims.sampleTrackingCondition.temperature="温度";
biolims.sampleTrackingCondition.changeTime="时间";
biolims.sampleTrackingCondition.changeTime="时间";




//英文JS配置文件
biolims.sampleTrackingCondition.id="id";
biolims.sampleTrackingCondition.name="name";
biolims.sampleTrackingCondition.createUser="createUser";
biolims.sampleTrackingCondition.createDate="createDate";
biolims.sampleTrackingCondition.state="state";
biolims.sampleTrackingCondition.stateName="stateName";
biolims.sampleTrackingCondition.note="note";
biolims.sampleTrackingCondition.temperature="temperature";
biolims.sampleTrackingCondition.changeTime="changeTime";
biolims.sampleTrackingCondition.changeTime="changeTime";


//中文配置文件
biolims.sampleTrackingCondition.id=编号
biolims.sampleTrackingCondition.name=描述
biolims.sampleTrackingCondition.createUser=创建人
biolims.sampleTrackingCondition.createDate=提交时间
biolims.sampleTrackingCondition.state=状态id
biolims.sampleTrackingCondition.stateName=状态
biolims.sampleTrackingCondition.note=备注
biolims.sampleTrackingCondition.temperature=温度
biolims.sampleTrackingCondition.changeTime=时间
biolims.sampleTrackingCondition.changeTime=时间


//英文配置文件
biolims.sampleTrackingCondition.id=id
biolims.sampleTrackingCondition.name=name
biolims.sampleTrackingCondition.createUser=createUser
biolims.sampleTrackingCondition.createDate=createDate
biolims.sampleTrackingCondition.state=state
biolims.sampleTrackingCondition.stateName=stateName
biolims.sampleTrackingCondition.note=note
biolims.sampleTrackingCondition.temperature=temperature
biolims.sampleTrackingCondition.changeTime=changeTime
biolims.sampleTrackingCondition.changeTime=changeTime

*/