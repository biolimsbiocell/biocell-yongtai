package com.biolims.tra.sampletransport.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.tra.sampletransport.service.SampleTransportService;

public class SampleTransportEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		SampleTransportService mbService = (SampleTransportService) ctx.getBean("sampleTransportService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
