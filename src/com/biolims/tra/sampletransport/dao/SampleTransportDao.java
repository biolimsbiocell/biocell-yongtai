package com.biolims.tra.sampletransport.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.tra.sampletransport.model.SampleTrackingCondition;
import com.biolims.tra.sampletransport.model.SampleTransport;
import com.biolims.tra.sampletransport.model.SampleTransportItem;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class SampleTransportDao extends BaseHibernateDao {

	public Map<String, Object> findSampleTransportTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from  SampleTransport where 1=1";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from SampleTransport where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SampleTransport> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> findSampleTransportItemTable(Integer start, Integer length, String query, String col,
			String sort, String id) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleTransportItem where 1=1 and sampleTransport.id='" + id + "'";
		String key = "";

		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from SampleTransportItem where 1=1 and sampleTransport.id='" + id + "' ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SampleTransportItem> list = new ArrayList<SampleTransportItem>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> findSampleTrackingConditionTable(Integer start, Integer length, String query, String col,
			String sort, String id) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleTrackingCondition where 1=1 and sampleTransport.id='" + id + "'";
		String key = "";

		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from SampleTrackingCondition where 1=1 and sampleTransport.id='" + id + "' ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SampleTrackingCondition> list = new ArrayList<SampleTrackingCondition>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public String showSampleTransportItem(String id) {
		String hql = "select invalidState from SampleTransportItem where 1=1 and id='" + id + "'";
		String result = (String) getSession().createQuery(hql).uniqueResult();
		return result;
	}

	public String showOrderNo(String id) {
		String hql = "select orderNo from SampleTransportItem where 1=1 and id='" + id + "'";
		String result = (String) getSession().createQuery(hql).uniqueResult();
		return result;
	}

	public List<SampleTransportItem> showSampleTransportItemList(String contentId) {
		String hql = "from SampleTransportItem where 1=1 and sampleTransport.id='" + contentId + "'";
		List<SampleTransportItem> list = new ArrayList<SampleTransportItem>();
		list = getSession().createQuery(hql).list();
		return list;
	}

	public List<SampleOrderItem> showSampleOrderItem(String orderNo) {
		String hql = "from SampleOrderItem where 1=1 and sampleOrder.id='" + orderNo + "'";
		List<SampleOrderItem> list = new ArrayList<SampleOrderItem>();
		list = getSession().createQuery(hql).list();
		return list;
	}
}