package com.biolims.tra.sampletransport.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.file.service.FileInfoService;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.tra.sampletransport.model.SampleTrackingCondition;
import com.biolims.tra.sampletransport.model.SampleTransport;
import com.biolims.tra.sampletransport.model.SampleTransportItem;
import com.biolims.tra.sampletransport.service.SampleTransportService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.opensymphony.xwork2.ActionContext;

@Namespace("/tra/sampletransport/sampleTransport")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
/**
 * @ClassName: SampleTransportAction
 * @Description: TODO
 * @author 孙灵达
 * @date 2018年7月31日
 */
public final class SampleTransportAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "tra02";
	@Autowired
	private SampleTransportService sampleTransportService;
	private SampleTransport sampleTransport = new SampleTransport();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CommonService commonService;
	@Resource
	private FieldService fieldService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private CommonDAO commonDAO;

	@Action(value = "showSampleTransportList")
	public String showSampleTransportList() throws Exception {
		rightsId = "tra02";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/tra/sampletransport/sampleTransport.jsp");
	}

	@Action(value = "showSampleTransportEditList")
	public String showSampleTransportEditList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/tra/sampletransport/sampleTransportEditList.jsp");
	}

	@Action(value = "showSampleTransportTableJson")
	public void showSampleTransportTableJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = sampleTransportService.findSampleTransportTable(start, length, query, col,
					sort);
			Long count = (Long) result.get("total");
			List<SampleTransport> list = (List<SampleTransport>) result.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("stateName", "");
			map.put("note", "");
			// map.put("transportUser-id", "");
			map.put("transportUser", "");
			map.put("transportCompany", "");
			map.put("trackingNumber", "");
			map.put("beginDate", "yyyy-MM-dd");
			map.put("endDate", "yyyy-MM-dd");

			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("SampleTransport");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "sampleTransportSelectTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogSampleTransportList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/tra/sampletransport/sampleTransportSelectTable.jsp");
	}

	@Action(value = "editSampleTransport")
	public String editSampleTransport() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			sampleTransport = sampleTransportService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "sampleTransport");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			sampleTransport.setId("NEW");
			sampleTransport.setCreateUser(user);
			sampleTransport.setCreateDate(new Date());
			sampleTransport.setState(SystemConstants.DIC_STATE_NEW);
			sampleTransport.setStateName(SystemConstants.DIC_STATE_NEW_NAME);
			sampleTransport.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			sampleTransport.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));

			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/tra/sampletransport/sampleTransportEdit.jsp");
	}

	@Action(value = "copySampleTransport")
	public String copySampleTransport() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		sampleTransport = sampleTransportService.get(id);
		sampleTransport.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/tra/sampletransport/sampleTransportEdit.jsp");
	}

	@Action(value = "save")
	public void save() throws Exception {

		Map<String, Object> mm = new HashMap<String, Object>();
		try {
			String dataValue = getParameterFromRequest("dataValue");
			String str = "[" + dataValue + "]";
			String changeLog = getParameterFromRequest("changeLog");
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str, List.class);

			for (Map<String, Object> map : list) {
				sampleTransport = (SampleTransport) commonDAO.Map2Bean(map, sampleTransport);
			}
			String id = sampleTransport.getId();
			if ((id != null && id.equals("")) || id.equals("NEW")) {
				String modelName = "SampleTransport";
				String markCode = "ST";
				String autoID = codingRuleService.genTransID(modelName, markCode);
				sampleTransport.setId(autoID);
			}
			Map aMap = new HashMap();
			Map lMap = new HashMap();
			aMap.put("sampleTransportItem", getParameterFromRequest("sampleTransportItemJson"));

			aMap.put("sampleTrackingCondition", getParameterFromRequest("sampleTrackingConditionJson"));

			sampleTransportService.save(sampleTransport, aMap, changeLog, lMap);
			mm.put("id", sampleTransport.getId());
			mm.put("success", true);

		} catch (Exception e) {
			e.printStackTrace();
			mm.put("success", false);
		}
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		if (bpmTaskId != null && !bpmTaskId.equals("") && !bpmTaskId.equals("null")) {
			mm.put("bpmTaskId", bpmTaskId);

		}
		HttpUtils.write(JsonUtils.toJsonString(mm));
	}

	@Action(value = "viewSampleTransport")
	public String toViewSampleTransport() throws Exception {
		String id = getParameterFromRequest("id");
		sampleTransport = sampleTransportService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/tra/sampletransport/sampleTransportEdit.jsp");
	}

	@Action(value = "showSampleTransportItemTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleTransportItemTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = sampleTransportService.findSampleTransportItemTable(start, length, query, col,
				sort, id);
		List<SampleTransportItem> list = (List<SampleTransportItem>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("orderNo", "");
		map.put("createUser-name", "");
		map.put("createUser-id", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("planTime", "HH:mm");
		map.put("actualTime", "");
		map.put("invalidState", "");
		map.put("samplingDate", "yyyy-MM-dd HH:mm");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSampleTransportItem")
	public void delSampleTransportItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			sampleTransportService.delSampleTransportItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "saveSampleTransportItemTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveSampleTransportItemTable() throws Exception {
		Map aMap = new HashMap();
		Map lMap = new HashMap();

		String id = getParameterFromRequest("id");
		sampleTransport = commonService.get(SampleTransport.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		aMap.put("sampleTransportItem", getParameterFromRequest("dataJson"));
		lMap.put("sampleTransportItem", getParameterFromRequest("changeLog"));
		try {
			sampleTransportService.save(sampleTransport, aMap, "", lMap);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showSampleTrackingConditionTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleTrackingConditionTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = sampleTransportService.findSampleTrackingConditionTable(start, length, query, col,
				sort, id);
		List<SampleTrackingCondition> list = (List<SampleTrackingCondition>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-name", "");
		map.put("createUser-id", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		map.put("temperature", "");
		map.put("changeTime", "yyyy-MM-dd");
		map.put("changeSite", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSampleTrackingCondition")
	public void delSampleTrackingCondition() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			sampleTransportService.delSampleTrackingCondition(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "saveSampleTrackingConditionTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveSampleTrackingConditionTable() throws Exception {
		Map aMap = new HashMap();
		Map lMap = new HashMap();

		String id = getParameterFromRequest("id");
		sampleTransport = commonService.get(SampleTransport.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		aMap.put("sampleTrackingCondition", getParameterFromRequest("dataJson"));
		lMap.put("sampleTrackingCondition", getParameterFromRequest("changeLog"));
		try {
			sampleTransportService.save(sampleTransport, aMap, "", lMap);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "saveSampleTransportTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveSampleTransportTable() throws Exception {
		String dataValue = getParameterFromRequest("dataJson");
		String changeLog = getParameterFromRequest("changeLog");

		String str = "[" + dataValue + "]";

		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str, List.class);

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			for (Map<String, Object> map1 : list) {

				SampleTransport a = new SampleTransport();

				Map aMap = new HashMap();
				Map lMap = new HashMap();
				a = (SampleTransport) commonDAO.Map2Bean(map1, a);
				a.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				a.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));

				sampleTransportService.save(a, aMap, changeLog, lMap);
				map.put("id", a.getId());
			}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SampleTransportService getSampleTransportService() {
		return sampleTransportService;
	}

	public void setSampleTransportService(SampleTransportService sampleTransportService) {
		this.sampleTransportService = sampleTransportService;
	}

	public SampleTransport getSampleTransport() {
		return sampleTransport;
	}

	public void setSampleTransport(SampleTransport sampleTransport) {
		this.sampleTransport = sampleTransport;
	}

	@Action(value = "invalidSampleOrder", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void invalidSampleOrder() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String[] ids = getRequest().getParameterValues("id[]");
		try {
			sampleTransportService.invalidSampleOrder(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
}
