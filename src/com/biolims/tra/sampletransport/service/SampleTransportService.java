package com.biolims.tra.sampletransport.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.dao.SampleOrderDao;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleOrderItem;
import com.biolims.tra.sampletransport.dao.SampleTransportDao;
import com.biolims.tra.sampletransport.model.SampleTrackingCondition;
import com.biolims.tra.sampletransport.model.SampleTransport;
import com.biolims.tra.sampletransport.model.SampleTransportItem;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SampleTransportService {
	@Resource
	private SampleTransportDao sampleTransportDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleOrderDao sampleOrderDao;

	StringBuffer json = new StringBuffer();

	public Map<String, Object> findSampleTransportTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return sampleTransportDao.findSampleTransportTable(start, length, query, col, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleTransport i) throws Exception {

		sampleTransportDao.saveOrUpdate(i);

	}

	public SampleTransport get(String id) {
		SampleTransport sampleTransport = commonDAO.get(SampleTransport.class, id);
		return sampleTransport;
	}

	public Map<String, Object> findSampleTransportItemTable(Integer start, Integer length, String query, String col,
			String sort, String id) throws Exception {
		Map<String, Object> result = sampleTransportDao.findSampleTransportItemTable(start, length, query, col, sort,
				id);
		List<SampleTransportItem> list = (List<SampleTransportItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findSampleTrackingConditionTable(Integer start, Integer length, String query, String col,
			String sort, String id) throws Exception {
		Map<String, Object> result = sampleTransportDao.findSampleTrackingConditionTable(start, length, query, col,
				sort, id);
		List<SampleTrackingCondition> list = (List<SampleTrackingCondition>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleTransportItem(SampleTransport sc, String itemDataJson, String logInfo) throws Exception {
		List<SampleTransportItem> saveItems = new ArrayList<SampleTransportItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		for (Map<String, Object> map : list) {
			SampleTransportItem scp = new SampleTransportItem();
			// 将map信息读入实体类
			scp = (SampleTransportItem) sampleTransportDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals("")) {
				scp.setId(null);
				scp.setInvalidState("0");
			}
			if ("1".equals(scp.getInvalidState())) {
				String orderId = sampleTransportDao.showOrderNo(scp.getId());
				if (!scp.getOrderNo().equals(orderId)) {
					scp.setInvalidState("0");
				}
			}
			scp.setSampleTransport(sc);
			scp.setCreateUser(u);

			saveItems.add(scp);
		}
		sampleTransportDao.saveOrUpdateAll(saveItems);
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSampleTransportItem(String[] ids) throws Exception {
		for (String id : ids) {
			SampleTransportItem scp = sampleTransportDao.get(SampleTransportItem.class, id);
			sampleTransportDao.delete(scp);
			if (ids.length > 0) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(scp.getId());
				li.setModifyContent("SampleTransportItem删除信息" + ids.toString());
				commonDAO.saveOrUpdate(li);
			}
		}

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleTrackingCondition(SampleTransport sc, String itemDataJson, String logInfo) throws Exception {
		List<SampleTrackingCondition> saveItems = new ArrayList<SampleTrackingCondition>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		for (Map<String, Object> map : list) {
			SampleTrackingCondition scp = new SampleTrackingCondition();
			// 将map信息读入实体类
			scp = (SampleTrackingCondition) sampleTransportDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSampleTransport(sc);
			scp.setCreateUser(u);

			saveItems.add(scp);
		}
		sampleTransportDao.saveOrUpdateAll(saveItems);
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSampleTrackingCondition(String[] ids) throws Exception {
		for (String id : ids) {
			SampleTrackingCondition scp = sampleTransportDao.get(SampleTrackingCondition.class, id);
			sampleTransportDao.delete(scp);
			if (ids.length > 0) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(scp.getId());
				li.setModifyContent("SampleTrackingCondition删除信息" + ids.toString());
				commonDAO.saveOrUpdate(li);
			}
		}

	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleTransport sc, Map jsonMap, String logInfo, Map logMap) throws Exception {
		if (sc != null) {
			sampleTransportDao.saveOrUpdate(sc);

			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(sc.getId());
				li.setModifyContent(logInfo);
				commonDAO.saveOrUpdate(li);
			}

			String jsonStr = "";
			String logStr = "";
			logStr = (String) logMap.get("sampleTransportItem");
			jsonStr = (String) jsonMap.get("sampleTransportItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSampleTransportItem(sc, jsonStr, logStr);
			}
			logStr = (String) logMap.get("sampleTrackingCondition");
			jsonStr = (String) jsonMap.get("sampleTrackingCondition");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSampleTrackingCondition(sc, jsonStr, logStr);
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String contentId) {
		SampleTransport sampleTransport = commonDAO.get(SampleTransport.class, contentId);
		sampleTransport.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sampleTransport.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_COMPLETE_NAME);
		commonDAO.saveOrUpdate(sampleTransport);
		List<SampleTransportItem> stiList = new ArrayList<SampleTransportItem>();
		stiList = sampleTransportDao.showSampleTransportItemList(contentId);
		if (!stiList.isEmpty()) {
			for (SampleTransportItem sti : stiList) {
				if (!"1".equals(sti.getInvalidState())) {
					List<SampleOrderItem> soiList = new ArrayList<SampleOrderItem>();
					soiList = sampleTransportDao.showSampleOrderItem(sti.getOrderNo());
					if (!soiList.isEmpty()) {
						for (SampleOrderItem soi : soiList) {
							soi.setSamplingDate(sti.getSamplingDate());
							commonDAO.saveOrUpdate(soi);
						}
					}
				} else {
					SampleOrder so = new SampleOrder();
					so = commonDAO.get(SampleOrder.class, sti.getOrderNo());
					if (so.getBarcode() != null && !"".equals(so.getBarcode())) {
						so.setBarcode("X-" + so.getBarcode());
					}
					if (so.getFiltrateCode() != null && !"".equals(so.getFiltrateCode())) {
						so.setFiltrateCode("X-" + so.getFiltrateCode());
					}
					if (so.getRound() != null && !"".equals(so.getRound())) {
						so.setRound("X-" + so.getRound());
					}
					if (so.getCcoi() != null && !"".equals(so.getCcoi())) {
						so.setCcoi("X-" + so.getCcoi());
					}
					so.setState("1");
					so.setStateName("作废");
					commonDAO.saveOrUpdate(so);
				}
			}
		}

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void invalidSampleOrder(String[] ids) {
		if (ids != null) {
			if (ids.length > 0) {
				for (String id : ids) {
					SampleTransportItem sti = new SampleTransportItem();
					sti = commonDAO.get(SampleTransportItem.class, id);
					if (sti != null) {
						SampleOrder so = new SampleOrder();
						so = commonDAO.get(SampleOrder.class, sti.getOrderNo());
						so.setState("1");
						so.setStateName("作废");
						commonDAO.saveOrUpdate(so);
						sti.setInvalidState("1");
						commonDAO.saveOrUpdate(sti);
					}
				}
			}
		}
	}
}
