/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：Action
 * 类描述：收款合同管理
 * 创建人：倪毅
 * 创建时间：2011-12
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.contract.collectionContract.action;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemCode;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.common.service.SystemCodeService;
import com.biolims.contract.common.constants.SystemConstants;
import com.biolims.contract.common.service.ContractService;
import com.biolims.contract.model.Contract;
import com.biolims.contract.model.ContractItem;
import com.biolims.contract.model.ContractPayItem;
import com.biolims.dic.model.DicType;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/contract/collectionContract")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public class CollectionContractAction extends BaseActionSupport {

	private static final long serialVersionUID = -164661537469123056L;
	@Autowired
	private ContractService collectionContractService;
	@Autowired
	private CommonService commonService;

	@Resource
	private SystemCodeService systemCodeService;
	//用于页面上显示模块名称
	private String title = "收款合同管理";
	//该action权限id
	private String rightsId = "2304";

	private Contract contract = new Contract();
	@Resource
	private FileInfoService fileInfoService;

	/**
	 * 访问列表
	 */

	@Action(value = "showContractList")
	public String showContractList() throws Exception {
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		map.put("id", new String[] { "", "string", "", "合同编号", "120", "true", "", "", "", "", "", "" });
		map.put("note", new String[] { "", "string", "", "描述", "250", "true", "", "", "", "", "", "" });
		//map.put("mainContract-id", new String[] { "", "string", "", "主合同号", "120", "true", "", "", "", "", "", "" });
		map.put("contractType-name", new String[] { "", "string", "", "合同类型", "200", "true", "", "", "", "", "", "" });
		map.put("supplier-id", new String[] { "", "string", "", "客户编号", "100", "true", "", "", "", "", "", "" });
		map.put("createDate", new String[] { "", "date", "dateFormat:'Y-m-d'", "创建日期", "100", "true", "", "", "", "",
				"", "" });
		map.put("stateName", new String[] { "", "string", "", "工作流状态", "100", "true", "", "", "", "", "", "" });
		map.put("startDate", new String[] { "", "string", "", "合同开始日期", "100", "true", "true", "", "", "", "", "" });
		map.put("confirmUser-name",
				new String[] { "", "string", "", "合同批准人", "100", "true", "true", "", "", "", "", "" });
		map.put("endDate", new String[] { "", "string", "", "合同结束日期", "100", "true", "true", "", "", "", "", "" });
		map
				.put("currencyType-name", new String[] { "", "string", "", "币种", "100", "true", "true", "", "", "", "",
						"" });
		map.put("supplier-name", new String[] { "", "string", "", "供应商", "100", "true", "true", "", "", "", "", "" });
		map
				.put("supplier-linkMan", new String[] { "", "string", "", "联系人", "100", "true", "true", "", "", "", "",
						"" });
		map.put("supplier-linkTel", new String[] { "", "string", "", "客户联系电话", "100", "true", "true", "", "", "", "",
				"" });
		map.put("supplier-fax", new String[] { "", "string", "", "传真", "100", "true", "true", "", "", "", "", "" });
		map.put("signDate", new String[] { "", "string", "", "合同签订日期", "100", "true", "true", "", "", "", "", "" });
		map.put("collectionType-name", new String[] { "", "string", "", "收款款形式", "100", "true", "true", "", "", "", "",
				"" });
		map.put("financePeriod", new String[] { "", "string", "", "收款周期", "100", "true", "true", "", "", "", "", "" });
		map.put("financeUnit-name",
				new String[] { "", "string", "", "收款单位", "100", "true", "true", "", "", "", "", "" });
		map
				.put("currencyType-name", new String[] { "", "string", "", "币种", "100", "true", "true", "", "", "", "",
						"" });
		map.put("fee", new String[] { "", "string", "", "合同金额", "100", "true", "true", "", "", "", "", "" });
		String type = generalexttype(map);
		String col = generalextcol(map);
		putObjToContext("type", type);
		putObjToContext("col", col);
		// 用于判断当前页面类型,LIST类型
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);

		putObjToContext("path", ServletActionContext.getRequest().getContextPath()
				+ "/contract/collectionContract/showContractListJson.action");
		return dispatcher("/WEB-INF/page/contract/collectionContract/showContractList.jsp");
	}

	@Action(value = "showContractListJson")
	public void showContractListJson() throws Exception {
		//开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		//limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		//字段
		String dir = getParameterFromRequest("dir");
		//排序方式
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		//		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		Map<String, Object> controlMap = collectionContractService.findContractList(map2Query, startNum, limitNum, dir,
				sort);
		Long count = (Long) controlMap.get("total");
		List<Contract> list = (List<Contract>) controlMap.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("note", "");
		map.put("mainContract-id", "");
		map.put("contractType-name", "");
		map.put("supplier-id", "");
		map.put("createDate", "");
		map.put("stateName", "");
		map.put("startDate", "yyyy-MM-dd");
		map.put("confirmUser-name", "");
		map.put("endDate", "yyyy-MM-dd");
		map.put("currencyType-name", "");
		map.put("supplier-name", "");
		//		map.put("supplier-linkMan", "");
		//		map.put("supplier-linkTel", "");
		//		map.put("supplier-fax", "");
		map.put("signDate", "yyyy-MM-dd");
		map.put("collectionType-name", "");
		map.put("financePeriod", "####");
		map.put("financeUnit-name", "");
		map.put("currencyType-name", "");
		map.put("fee", "####.####");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	/**
	 * 访问编辑页面
	 */

	@Action(value = "toEditContract", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String toEditContract() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		//		String handlemethod = "";
		if (id != null && !id.equals("")) {
			//			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "collectionContract");
			contract = collectionContractService.get(id);
			DecimalFormat df1 = new DecimalFormat("####");
			Double financePeriod = contract.getFinancePeriod();
			if (financePeriod != null)
				contract.setFinancePeriodStr(df1.format(financePeriod));
			putObjToContext("contractId", id);
			//			if (contract.getState().equals(SystemConstants.DIC_STATE_YES))
			//				handlemethod = SystemConstants.PAGE_HANDLE_METHOD_VIEW;
			//			else
			//				handlemethod = SystemConstants.PAGE_HANDLE_METHOD_MODIFY;
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toState(contract.getState());
			//			showContractItemList(id, SystemConstants.PAGE_HANDLE_METHOD_ADD);
			showContractPayItemList(SystemConstants.PAGE_HANDLE_METHOD_MODIFY, id);
		} else {
			this.contract.setId(SystemCode.DEFAULT_SYSTEMCODE);
			//			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			//			contract.setCreateUser(user);
			//			contract.setCreateDate(new Date());
			//			contract.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			//			contract.setType(SystemConstants.DIC_TYPE_CONTRACT_SHOU);
			//			contract.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			DicType regionType = commonService.get(DicType.class, "pekcityfr");
			contract.setRegionType(regionType);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toState(contract.getState());
			//			showContractItemList(id, SystemConstants.PAGE_HANDLE_METHOD_ADD);
			showContractPayItemList(SystemConstants.PAGE_HANDLE_METHOD_ADD, id);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/contract/collectionContract/editContract.jsp");
	}

	/**
	 * 复制页面
	 */
	@Action(value = "toCopyContract", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String toCopyContract() throws Exception {
		String id = getParameterFromRequest("id");
		User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		contract = collectionContractService.get(id);
		contract.setId("");
		contract.setCreateUser(user);
		contract.setCreateDate(new Date());
		contract.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
		contract.setType(SystemConstants.DIC_TYPE_CONTRACT_SHOU);
		contract.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		toState(contract.getState());
		toSetStateCopy();
		//		showContractItemList(id, SystemConstants.PAGE_HANDLE_METHOD_ADD);
		showContractPayItemList(SystemConstants.PAGE_HANDLE_METHOD_ADD, id);
		return dispatcher("/WEB-INF/page/contract/collectionContract/editContract.jsp");
	}

	/**
	 * 访问查看页面
	 */

	@Action(value = "toViewContract")
	public String toViewContract() throws Exception {
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			contract = collectionContractService.get(id);
			DecimalFormat df1 = new DecimalFormat("####");
			Double financePeriod = contract.getFinancePeriod();
			if (financePeriod != null)
				contract.setFinancePeriodStr(df1.format(financePeriod));
			putObjToContext("contractId", id);
			String departmentId = "";
			String userId = "";
			if (contract.getCreateUser() != null) {
				userId = contract.getCreateUser().getId();
				if (contract.getCreateUser().getDepartment() != null)
					departmentId = contract.getCreateUser().getDepartment().getId();
			}
			toToolBar(rightsId, departmentId, userId, SystemConstants.PAGE_HANDLE_METHOD_VIEW);
			toState(contract.getState());
			//			showContractItemList(id, SystemConstants.PAGE_HANDLE_METHOD_VIEW);
			showContractPayItemList(SystemConstants.PAGE_HANDLE_METHOD_VIEW, id);
		}
		return dispatcher("/WEB-INF/page/contract/collectionContract/editContract.jsp");
	}

	/**
	 * 保存
	 */
	@Action(value = "save")
	public String save() throws Exception {
		String id = this.contract.getId();
//		if (id == null || id.length() <= 0 || SystemCode.DEFAULT_SYSTEMCODE.equals(id)) {
//			DicType a = commonService.get(DicType.class, contract.getRegionType().getId());
//			String code = systemCodeService.getCodeByPrefix("Contract", "CO" + a.getSysCode()
//					+ DateUtil.dateFormatterByPattern(new Date(), "yyMMdd"), "Contract", 3, null);
//			this.contract.setId(code);
//		}

		collectionContractService.save(contract);
		String data = getParameterFromRequest("jsonDataStr");
		String data1 = getParameterFromRequest("jsonDataStr1");
		if (data != null && !data.equals(""))
			collectionContractService.saveContractItem(contract.getId(), data);
		if (data1 != null && !data1.equals(""))
			collectionContractService.saveContractPayItem(contract.getId(), data1);
		return redirect("/contract/collectionContract/toEditContract.action?id=" + contract.getId());
	}

	@Action(value = "showContractItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showContractItemList() throws Exception {
		return dispatcher("/WEB-INF/page/contract/collectionContract/showContractItemList.jsp");
	}

	@Action(value = "showContractItemListJson1", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showContractItemListJson1() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String contractId = getParameterFromRequest("contractId");
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");

		String scId = getRequest().getParameter("id");
		Map<String, Object> result = collectionContractService.findContractItemLists(scId, startNum, limitNum, dir,
				sort);
		Long total = (Long) result.get("total");
		List<ContractItem> list = (List<ContractItem>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("crmProduct-id", "");
		map.put("fee", "");
		map.put("price", "");
		map.put("num", "");
		map.put("contract-id", "");
		if (list != null)
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
	}

	//	public void showContractItemList(String contractId, String handlemethod) throws Exception {
	//
	//		String exttype = "";
	//		String extcol = "";
	//		String editflag = "true";
	//		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
	//		if (handlemethod.equals(SystemConstants.PAGE_HANDLE_METHOD_VIEW)) {
	//			editflag = "false";
	//		}
	//		map.put("id", new String[] { "", "string", "", "id", "150", "true", "true", "", "", "", "", "" });
	//		map.put("objId", new String[] { "", "string", "", "合同对象ID", "120", "true", "false", "", "", editflag, "",
	//				"objId" });
	//		map.put("name", new String[] { "", "string", "", "名称", "200", "true", "true", "", "", "", "", "" });
	//		map.put("num", new String[] { "", "float", "", "数量", "60", "true", "false", "", "", editflag, "", "num" });
	//		map.put("storage-id", new String[] { "", "string", "", "对象ID", "60", "true", "true", "", "", "", "", "" });
	//		map.put("storage-searchCode",
	//				new String[] { "", "string", "", "检索码", "60", "true", "true", "", "", "", "", "" });
	//		map.put("storage-spec", new String[] { "", "string", "", "规格", "60", "true", "false", "", "", "", "", "" });
	//		map
	//				.put("storage-unit-name", new String[] { "", "string", "", "单位", "60", "true", "false", "", "", "", "",
	//						"" });
	//		map.put("price", new String[] { "", "float", "", "单价", "60", "true", "false", "", "", editflag, "", "price" });
	//		map.put("fee", new String[] { "", "float", "", "合同金额", "60", "true", "false", "", "", editflag, "", "fee" });
	//		map.put("exRebate", new String[] { "", "float", "", "执行折扣", "60", "true", "false", "", "", editflag, "",
	//				"exRebate" });
	//		map.put("rebatePrice", new String[] { "", "float", "", "执行价格", "60", "true", "false", "", "", editflag, "",
	//				"rebatePrice" });
	//		map
	//				.put("exNum", new String[] { "", "float", "", "执行数量", "60", "true", "false", "", "", editflag, "",
	//						"exNum" });
	//		map.put("excFee",
	//				new String[] { "", "float", "", "执行金额", "60", "true", "false", "", "", editflag, "", "excFee" });
	//		//声明一个下拉框
	//		String statement = "";
	//		//声明一个lisener
	//		String statementLisener = "";
	//		//生成ext用type字符串
	//		exttype = generalexttype(map);
	//		//生成ext用col字符串
	//		extcol = generalextcol(map);
	//		putObjToContext("type", exttype);
	//		putObjToContext("col", extcol);
	//		putObjToContext("path", ServletActionContext.getRequest().getContextPath()
	//				+ "/contract/collectionContract/showContractItemListJson.action?contractId=" + contractId);
	//		putObjToContext("statement", statement);
	//		putObjToContext("statementLisener", statementLisener);
	//		if (contractId != null)
	//			putObjToContext("contractId", contractId);
	//		putObjToContext("handlemethod", handlemethod);
	//
	//	}

	/**
	 * 访问 明细列表11
	 */

	public void showContractPayItemList(String handlemethod, String contractId) throws Exception {

		String exttype = "";
		String extcol = "";
		String editflag = "true";
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		if (handlemethod.equals(SystemConstants.PAGE_HANDLE_METHOD_VIEW)) {
			editflag = "false";
		}
		map.put("id", new String[] { "", "string", "", "id", "150", "true", "true", "", "", "", "", "" });
		map.put("payCondition", new String[] { "", "string", "", "周期", "80", "true", "false", "", "", editflag, "",
				"payCondition" });
		map.put("fee", new String[] { "", "string", "", "应收金额", "100", "true", "false", "", "", editflag, "", "cfee" });
		//		map.put("invoiceNum", new String[] { "", "string", "", "发票号", "200", "true", "false", "", "", editflag, "",
		//				"invoiceNum" });
		//		map.put("payDate", new String[] { "", "date", "dateFormat:'Y-m-d'", "付款时间", "120", "true", "", "", "",
		//				editflag, "formatDate", "payDate" });
		//		map.put("note", new String[] { "", "string", "", "备注", "200", "true", "false", "", "", editflag, "", "note" });
		map.put("scale",
				new String[] { "", "string", "", "实收金额", "80", "true", "false", "", "", editflag, "", "scale" });
		map.put("handleDate", new String[] { "", "date", "dateFormat:'Y-m-d'", "收款时间", "80", "true", "", "", "",
				editflag, "formatDate", "handleDate" });

		//声明一个下拉框
		String statement = "";
		//声明一个lisener
		String statementLisener = "";
		//生成ext用type字符串
		exttype = generalexttype(map);
		//生成ext用col字符串
		extcol = generalextcol(map);
		putObjToContext("type1", exttype);
		putObjToContext("col1", extcol);
		putObjToContext("path1", ServletActionContext.getRequest().getContextPath()
				+ "/contract/collectionContract/showContractPayItemListJson.action?contractId=" + contractId);
		putObjToContext("statement", statement);
		putObjToContext("statementLisener", statementLisener);
		if (contractId != null)
			putObjToContext("contractId", contractId);
		putObjToContext("handlemethod", handlemethod);

	}

	@Action(value = "showContractItemListJson")
	public void showContractItemListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String contractId = getParameterFromRequest("contractId");
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		List<ContractItem> list = null;
		long totalCount = 0;
		if (contractId != null && !contractId.equals("")) {
			Map<String, Object> controlMap = collectionContractService.findContractItemList(startNum, limitNum, dir,
					sort, getContextPath(), contractId);
			totalCount = Long.parseLong(controlMap.get("totalCount").toString());
			list = (List<ContractItem>) controlMap.get("list");
		}
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("objId", "");
		map.put("name", "");
		map.put("searchCode", "");
		map.put("num", "");
		map.put("storage-spec", "");
		map.put("storage-unit-name", "");
		map.put("price", "");
		map.put("fee", "");
		map.put("exRebate", "");
		map.put("rebatePrice", "");
		map.put("exNum", "");
		map.put("excFee", "");
		if (list != null)
			new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}

	/**
	 * 保存明细
	 */
	@Action(value = "saveContractItem")
	public void saveProjectPlanItem() throws Exception {
		String contractId = getParameterFromRequest("contractId");
		String json = getParameterFromRequest("data");
		collectionContractService.saveContractItem(contractId, json);

	}

	/**
	 * 删除明细
	 */
	@Action(value = "delContractItem")
	public void delExperimentMainStorage() throws Exception {
		String id = getParameterFromRequest("id");
		collectionContractService.delContractItem(id);
	}

	@Action(value = "showContractPayItemListJson")
	public void showContractPayItemListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String contractId = getParameterFromRequest("contractId");
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		List<ContractPayItem> list = null;
		long totalCount = 0;
		if (contractId != null && !contractId.equals("")) {
			Map<String, Object> controlMap = collectionContractService.findContractPayItemList(startNum, limitNum, dir,
					sort, getContextPath(), contractId);
			totalCount = Long.parseLong(controlMap.get("totalCount").toString());
			list = (List<ContractPayItem>) controlMap.get("list");
		}
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");

		map.put("fee", "");
		//		map.put("invoiceNum", "");
		//		map.put("payDate", "yyyy-MM-dd");
		//		map.put("note", "note");
		map.put("payCondition", "");
		map.put("scale", "");
		map.put("handleDate", "yyyy-MM-dd");
		new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());

	}

	/**
	 * 保存明细
	 */
	@Action(value = "saveContractPayItem")
	public void saveContractPayItem() throws Exception {
		String contractId = getParameterFromRequest("contractId");
		String json = getParameterFromRequest("data");
		collectionContractService.saveContractPayItem(contractId, json);

	}

	/**
	 * 删除明细
	 */
	@Action(value = "delContractPayItem")
	public void delContractPayItem() throws Exception {
		String id = getParameterFromRequest("id");
		collectionContractService.delContractPayItem(id);

	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}
	
	public void test(){
		System.out.println("****************************");
	}
}
