package com.biolims.contract.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.contract.model.Contract;
import com.biolims.contract.model.ContractItem;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.crm.customer.patient.model.CrmPatientRadiology;
import com.biolims.dao.BaseHibernateDao;

@Repository
@SuppressWarnings("unchecked")
public class ContractDao extends BaseHibernateDao {

	public Map<String, Object> findContractList(Map<String, String> mapForQuery, Integer startNum, Integer limitNum,
			String dir, String sort) {
		String key = " ";
		String hql = " from Contract where 1=1 ";
		if (mapForQuery != null)
			key = map2where4Split(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<Contract> list = new ArrayList<Contract>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectContractItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		String hql = "from ContractItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and contract.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<ContractItem> list = new ArrayList<ContractItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
}
