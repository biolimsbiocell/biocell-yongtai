/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：
 * 类描述：供应商客户service
 * 创建人：倪毅
 * 创建时间：2011-11
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.contract.common.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.biolims.common.service.CommonService;
import com.biolims.contract.model.Contract;

@Service
public class ContractCommonService extends CommonService {

	/**
	 * 
	 * 检索list,采用map方式传递检索参数
	 * 
	 * 
	 * @return 参数值MAP，如果传递的为null返回空字符串
	 */
	public Map<String, Object> findContractList(int startNum, int limitNum, String dir, String sort,
			String contextPath, String type) throws Exception {
		//检索条件map,每个字段检索值
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		//检索方式map,每个字段的检索方式
		Map<String, String> mapForCondition = new HashMap<String, String>();
		if (type != null && !type.equals("")) {
			mapForQuery.put("type", type);
			mapForCondition.put("type", "=");
		}
		//将map值传入，获取列表
		Map<String, Object> controlMap = findObjectCondition(startNum, limitNum, dir, sort, Contract.class,
				mapForQuery, mapForCondition);
		return controlMap;
	}

}
