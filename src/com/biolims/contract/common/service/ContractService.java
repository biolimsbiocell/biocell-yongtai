/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：UserService
 * 类描述：用户管理service
 * 创建人：倪毅
 * 创建时间：2011-9
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.contract.common.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.contract.dao.ContractDao;
import com.biolims.contract.model.Contract;
import com.biolims.contract.model.ContractItem;
import com.biolims.contract.model.ContractPayItem;
import com.biolims.crm.customer.patient.model.CrmPatientPathology;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.util.BeanUtils;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
public class ContractService {
	@Resource
	private CommonDAO commonDAO;

	@Resource
	private ContractDao contractDao;

	StringBuffer json = new StringBuffer();

	/**
	 * 
	 * 检索list,采用map方式传递检索参数
	 * 
	 * 
	 * @return 参数值MAP，如果传递的为null返回空字符串
	 */
	public Map<String, Object> findContractList(Map<String, String> mapForQuery, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		return contractDao.findContractList(mapForQuery, startNum, limitNum, dir, sort);
		//		//检索条件map,每个字段检索值
		//		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		//		//检索方式map,每个字段的检索方式
		//		Map<String, String> mapForCondition = new HashMap<String, String>();
		//		String startDate = "";
		//		String endDate = "";
		//		if (data != null && !data.equals("")) {
		//			BeanUtils.data2Map(data, mapForQuery, mapForCondition);
		//		}
		//
		//		mapForCondition.put("type", "=");
		//		mapForQuery.put("type", type);
		//		//将map值传入，获取列表
		//
		//		Map<String, Object> criteriaMap = commonDAO.findObjectCriteria(startNum, limitNum, dir, sort, Contract.class,
		//				mapForQuery, mapForCondition);
		//		Criteria cr = (Criteria) criteriaMap.get("criteria");
		//		Criteria cc = (Criteria) criteriaMap.get("criteriaCount");
		//		Map<String, Object> map = JsonUtils.toObjectByJson(data, Map.class);
		//		if (data != null && !data.equals("")) {
		//			if (map.get("queryStartDate") != null && !map.get("queryStartDate").equals("")) {
		//
		//				startDate = (String) map.get("queryStartDate");
		//				cr.add(Restrictions.ge("signDate", DateUtil.parse(startDate)));
		//				cc.add(Restrictions.ge("signDate", DateUtil.parse(startDate)));
		//			}
		//			if (map.get("queryEndDate") != null && !map.get("queryEndDate").equals("")) {
		//
		//				endDate = (String) map.get("queryEndDate");
		//				cr.add(Restrictions.le("signDate", DateUtil.parse(endDate)));
		//				cc.add(Restrictions.le("signDate", DateUtil.parse(endDate)));
		//			}
		//
		//		}
		//		Map<String, Object> controlMap = commonDAO.findObjectList(startNum, limitNum, dir, sort, Contract.class,
		//				mapForQuery, mapForCondition, cc, cr);
		//
		//		return controlMap;
	}

	/**
	 * 
	 * 检索list,采用map方式传递检索参数
	 * 
	 * 
	 * @return 参数值MAP，如果传递的为null返回空字符串
	 */
	public Map<String, Object> findContractItemList(int startNum, int limitNum, String dir, String sort,
			String contextPath, String contractId) throws Exception {
		//检索条件map,每个字段检索值
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		//检索方式map,每个字段的检索方式
		Map<String, String> mapForCondition = new HashMap<String, String>();
		if (contractId != null && !contractId.equals("")) {
			mapForQuery.put("contract.id", contractId);
			mapForCondition.put("contract.id", "=");
		}
		//将map值传入，获取列表
		Map<String, Object> controlMap = commonDAO.findObjectCondition(startNum, limitNum, dir, sort,
				ContractItem.class, mapForQuery, mapForCondition);
		return controlMap;
	}

	public Contract get(String id) throws Exception {
		Contract ao = commonDAO.get(Contract.class, id);
		return ao;
	}

	public Map<String, Object> findContractItemLists(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = contractDao.selectContractItemList(scId, startNum, limitNum, dir, sort);
		List<ContractItem> list = (List<ContractItem>) result.get("list");
		return result;
	}

	/**
	 * 保存
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(Contract an) throws Exception {

		commonDAO.saveOrUpdate(an);

	}

	/**
	 * 
	 * 维护明细信息,由ext ajax调用
	 * 
	 * @param userId
	 *            用户id
	 * @param jsonString
	 *            用户认证jsonString
	 * @return void
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveContractItem(String contractId, String jsonString) throws Exception {

		// 将json读入map
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(jsonString, List.class);
		for (Map<String, Object> map : list) {
			ContractItem em = new ContractItem();
			// 将map信息读入实体类
			em = (ContractItem) commonDAO.Map2Bean(map, em);
			//			if (em.getId().length() <= 0)
			//				em.setId(null);
			Contract ep = new Contract();
			em.setFee(em.getPrice() * em.getNum());
			ep.setId(contractId);
			em.setContract(ep);
			commonDAO.saveOrUpdate(em);
		}
	}

	/**
	 * 
	 * 删除明细,由ext ajax调用
	 * 
	 * @param id
	 *            认证ID
	 * @return void
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delContractItem(String id) {
		ContractItem es = new ContractItem();
		es.setId(id);
		commonDAO.delete(es);
	}

	/**
	 * 
	 * 检索list,采用map方式传递检索参数
	 * 
	 * 
	 * @return 参数值MAP，如果传递的为null返回空字符串
	 */
	public Map<String, Object> findContractPayItemList(int startNum, int limitNum, String dir, String sort,
			String contextPath, String contractId) throws Exception {
		//检索条件map,每个字段检索值
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		//检索方式map,每个字段的检索方式
		Map<String, String> mapForCondition = new HashMap<String, String>();
		if (contractId != null && !contractId.equals("")) {
			mapForQuery.put("contract.id", contractId);
			mapForCondition.put("contract.id", "=");
		}
		//将map值传入，获取列表
		Map<String, Object> controlMap = commonDAO.findObjectCondition(startNum, limitNum, dir, sort,
				ContractPayItem.class, mapForQuery, mapForCondition);
		return controlMap;
	}

	/**
	 * 
	 * 维护明细信息,由ext ajax调用
	 * 
	 * @param userId
	 *            用户id
	 * @param jsonString
	 *            用户认证jsonString
	 * @return void
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveContractPayItem(String contractId, String jsonString) throws Exception {

		// 将json读入map
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(jsonString, List.class);
		for (Map<String, Object> map : list) {
			ContractPayItem em = new ContractPayItem();

			// 将map信息读入实体类
			em = (ContractPayItem) commonDAO.Map2Bean(map, em);
			/*			if (em.getId().equals("") && em.getId() == null)
							em.setId(null);*/
			Contract ep = new Contract();
			em.setPayCondition(em.getPayCondition().replace("<br>", "\\n\\r"));
			ep.setId(contractId);
			em.setContract(ep);
			commonDAO.saveOrUpdate(em);
		}
	}

	/**
	 * 
	 * 删除明细,由ext ajax调用
	 * 
	 * @param id
	 *            认证ID
	 * @return void
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delContractPayItem(String id) {
		ContractPayItem es = new ContractPayItem();
		es.setId(id);
		commonDAO.delete(es);
	}

	/**
	 *  查询任务明细列表
	 */
	public List<Contract> findContractList(String contractType) throws Exception {

		Object[] a = { contractType };
		List<Contract> list = commonDAO.find("from Contract where type = ? order by planStartDate asc", a);
		return list;

	}

	/**
	 *  查询下级有效的任务明细列表
	 */
	public long findProjectDownTaskCount(String projectTaskId) throws Exception {

		Object[] a = { projectTaskId };
		long listCount = commonDAO.getCount(
				"from ProjectTask where upProjectTask.id = ? and state = '1' order by planStartDate asc", a);
		return listCount;

	}

}
