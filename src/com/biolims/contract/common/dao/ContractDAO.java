package com.biolims.contract.common.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.biolims.common.dao.CommonDAO;

@Repository
public class ContractDAO extends CommonDAO {

	public Double getSumFeeFromFactPayItem(String id) throws Exception {
		String hql = "select sum(fee) from ContractFactPayItem where contractPayItem.id =?";
		List<Object> list = find(hql, id);
		Double s = 0.0;
		s = (Double) list.get(0);
		return s;
	}
}
