package com.biolims.contract.common.constants;

public class SystemConstants extends com.biolims.common.constants.SystemConstants {

	/**
	 * 字典表-合同类型-收款
	 */
	public static final String DIC_TYPE_CONTRACT_SHOU = "shou";

	/**
	 * 字典表-合同类型-付款
	 */
	public static final String DIC_TYPE_CONTRACT_FU = "fu";

}
