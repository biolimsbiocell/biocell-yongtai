/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：Action
 * 类描述：供应商管理
 * 创建人：倪毅
 * 创建时间：2011-11
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.contract.common.action;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.contract.common.constants.SystemConstants;
import com.biolims.contract.common.service.ContractCommonService;
import com.biolims.contract.model.Contract;
import com.biolims.util.SendData;

@Namespace("/contract/common")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public class ContractCommonAction extends BaseActionSupport {

	private static final long serialVersionUID = -164661537469123056L;
	@Autowired
	private ContractCommonService contractCommonService;

	/**
	 * 访问合同列表
	 */

	@Action(value = "contractSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showCollectionContractList() throws Exception {
		String contractType = getParameterFromRequest("type");
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		map.put("id", new String[] { "", "string", "", "合同编号", "120", "true", "", "", "", "", "", "" });
		map.put("note", new String[] { "", "string", "", "描述", "200", "true", "", "", "", "", "", "" });
		map.put("mainContract-id", new String[] { "", "string", "", "主合同号", "120", "true", "", "", "", "", "", "" });
		map.put("contractType-name", new String[] { "", "string", "", "合同类型", "100", "true", "", "", "", "", "", "" });
		map.put("supplier-id", new String[] { "", "string", "", "客户编号", "100", "true", "", "", "", "", "", "" });
		map.put("createDate", new String[] { "", "date", "dateFormat:'Y-m-d'", "创建日期", "100", "true", "", "", "", "",
				"", "" });
		String type = generalexttype(map);
		String col = generalextcol(map);
		putObjToContext("type", type);
		putObjToContext("col", col);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		putObjToContext("path", ServletActionContext.getRequest().getContextPath()
				+ "/contract/common/contractSelectJson.action?contractType=" + contractType);
		return dispatcher("/WEB-INF/page/contract/contractSelect.jsp");
	}

	@Action(value = "contractSelectJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showCollectionContractListJson() throws Exception {
		//开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		//limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));

		String type = "";
		//字段
		String dir = getParameterFromRequest("dir");
		//排序方式
		String sort = getParameterFromRequest("sort");
		//取出session中检索用的对象

		Map<String, Object> controlMap = contractCommonService.findContractList(startNum, limitNum, dir, sort,
				getContextPath(), type);
		long totalCount = Long.parseLong(controlMap.get("totalCount").toString());
		List<Contract> list = (List<Contract>) controlMap.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("note", "");
		map.put("mainContract-id", "");
		map.put("contractType-name", "");
		map.put("supplier-id", "");
		map.put("createDate", "");
		new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}

}