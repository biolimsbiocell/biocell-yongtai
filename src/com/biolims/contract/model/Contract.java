package com.biolims.contract.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dic.model.DicCurrencyType;
import com.biolims.dic.model.DicType;
import com.biolims.dic.model.DicUnit;
import com.biolims.common.model.user.User;
import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.dao.EntityDao;

/**
 * 合同管理
 * @author Vera
 */
@Entity
@Table(name = "T_CONTRACT")
public class Contract extends EntityDao<Contract> implements Serializable {

	private static final long serialVersionUID = 2191801684912559179L;
	@Id
	@Column(name = "ID", length = 32)
	private String id;//合同编号

	@Column(name = "NOTE", length = 110)
	private String note;//合同描述

	private Date startDate;//合同开始日期

	private Date endDate;//合同结束日期

	private Date signDate;//合同签订日期

	private Double period;//合同周期

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_CURRENCY_TYPE_TYPE_ID")
	private DicCurrencyType currencyType;//币种

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "USER_CONFIRM_USER_ID")
	private User confirmUser;//批准人

	@Column(name = "LINK_MAN", length = 100)
	private String linkMan;

	@Column(name = "LINK_TEL", length = 50)
	private String linkTel;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CONTRACT_ID")
	private Contract mainContract;//主合同编号

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SUPPLIER_ID")
	private CrmCustomer supplier;//供应商客户

	@Column(name = "ENTRUST_MAN", length = 200)
	private String entrustMan; //委托人

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_UNIT_UNIT_ID")
	private DicUnit unit;//单位

	@Column(name = "STATE", length = 32)
	private String state;//状态

	private Double fee;

	@Column(name = "REMARK", length = 500)
	private String remark;//备注

	@Column(name = "STATE_NAME", length = 200)
	private String stateName;
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_CONTRACT_TYPE_TYPE_ID")
	private DicType contractType;//合同类型
	@Column(name = "TYPE", length = 32)
	private String type;//收/付

	private Double financePeriod;//款项周期

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_FINANCE_UNIT_UNIT_ID")
	private DicUnit financeUnit;//款项周期单位

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "USER_CREATE_USER_ID")
	private User createUser;//创建人
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DUTY_MAN_ID")
	private User dutyManId;
	private Date createDate;//创建日期

	private Date confirmDate;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_COLLECTION_TYPE_TYPE_ID")
	private DicType collectionType;//形式

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "REGION_TYPE_ID")
	private DicType regionType;//城市

	@Transient
	private String financePeriodStr;

	public DicType getRegionType() {
		return regionType;
	}

	public void setRegionType(DicType regionType) {
		this.regionType = regionType;
	}

	public DicType getCollectionType() {
		return collectionType;
	}

	public void setCollectionType(DicType collectionType) {
		this.collectionType = collectionType;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public User getCreateUser() {
		return createUser;
	}

	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public DicUnit getFinanceUnit() {
		return financeUnit;
	}

	public void setFinanceUnit(DicUnit financeUnit) {
		this.financeUnit = financeUnit;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Double getPeriod() {
		return period;
	}

	public void setPeriod(Double period) {
		this.period = period;
	}

	public DicCurrencyType getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(DicCurrencyType currencyType) {
		this.currencyType = currencyType;
	}

	public User getConfirmUser() {
		return confirmUser;
	}

	public void setConfirmUser(User confirmUser) {
		this.confirmUser = confirmUser;
	}

	public Contract getMainContract() {
		return mainContract;
	}

	public void setMainContract(Contract mainContract) {
		this.mainContract = mainContract;
	}

	public DicUnit getUnit() {
		return unit;
	}

	public void setUnit(DicUnit unit) {
		this.unit = unit;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public DicType getContractType() {
		return contractType;
	}

	public void setContractType(DicType contractType) {
		this.contractType = contractType;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getSignDate() {
		return signDate;
	}

	public void setSignDate(Date signDate) {
		this.signDate = signDate;
	}

	public Double getFinancePeriod() {
		return financePeriod;
	}

	public void setFinancePeriod(Double financePeriod) {
		this.financePeriod = financePeriod;
	}

	public String getFinancePeriodStr() {
		return financePeriodStr;
	}

	public void setFinancePeriodStr(String financePeriodStr) {
		this.financePeriodStr = financePeriodStr;
	}

	public String getEntrustMan() {
		return entrustMan;
	}

	public void setEntrustMan(String entrustMan) {
		this.entrustMan = entrustMan;
	}

	public String getLinkMan() {
		return linkMan;
	}

	public void setLinkMan(String linkMan) {
		this.linkMan = linkMan;
	}

	public String getLinkTel() {
		return linkTel;
	}

	public void setLinkTel(String linkTel) {
		this.linkTel = linkTel;
	}

	public Double getFee() {
		return fee;
	}

	public void setFee(Double fee) {
		this.fee = fee;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public User getDutyManId() {
		return dutyManId;
	}

	public void setDutyManId(User dutyManId) {
		this.dutyManId = dutyManId;
	}

	public CrmCustomer getSupplier() {
		return supplier;
	}

	public void setSupplier(CrmCustomer supplier) {
		this.supplier = supplier;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

}
