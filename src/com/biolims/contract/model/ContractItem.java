package com.biolims.contract.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.storage.model.Storage;
import com.biolims.system.product.model.Product;

/**
 * 合同明细
 * @author Vera
 */
@Entity
@Table(name = "T_CONTRACT_ITEM")
public class ContractItem extends EntityDao<ContractItem> implements Serializable {

	private static final long serialVersionUID = 2191801684912559179L;
	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;//ID

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CRM_PRODUCT")
	private Product crmProduct;//产品

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CONTRACT_ID")
	private Contract contract;//合同ID

	@Column(name = "OBJ_ID", length = 32)
	private String objId;//ID

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_ID")
	private Storage storage;

	@Column(name = "SEARCH_CODE", length = 20)
	private String searchCode;//检索码

	private Double price;//单价
	private Double num;//数量
	private Double exNum;//执行数量
	private Double exRebate;//执行折扣
	private Double rebatePrice;//折扣价格

	@Column(name = "NAME", length = 110)
	private String name;//名称

	@Column(name = "UNIT", length = 32)
	private String unit;//单位

	//合同金额

	private Double fee;

	//执行金额
	private Double excFee;

	public Double getFee() {
		return fee;
	}

	public void setFee(Double fee) {
		this.fee = fee;
	}

	public Double getExcFee() {
		return excFee;
	}

	public void setExcFee(Double excFee) {
		this.excFee = excFee;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public String getSearchCode() {
		return searchCode;
	}

	public void setSearchCode(String searchCode) {
		this.searchCode = searchCode;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getNum() {
		return num;
	}

	public void setNum(Double num) {
		this.num = num;
	}

	public Double getExNum() {
		return exNum;
	}

	public void setExNum(Double exNum) {
		this.exNum = exNum;
	}

	public Double getExRebate() {
		return exRebate;
	}

	public void setExRebate(Double exRebate) {
		this.exRebate = exRebate;
	}

	public Double getRebatePrice() {
		return rebatePrice;
	}

	public void setRebatePrice(Double rebatePrice) {
		this.rebatePrice = rebatePrice;
	}

	public String getObjId() {
		return objId;
	}

	public void setObjId(String objId) {
		this.objId = objId;
	}

	public Storage getStorage() {
		return storage;
	}

	public void setStorage(Storage storage) {
		this.storage = storage;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Product getCrmProduct() {
		return crmProduct;
	}

	public void setCrmProduct(Product crmProduct) {
		this.crmProduct = crmProduct;
	}


}
