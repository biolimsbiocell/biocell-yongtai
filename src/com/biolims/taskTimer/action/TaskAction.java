/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：ACTION
 * 类描述：日志管理
 * 创建人：倪毅
 * 创建时间：2011-10
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.taskTimer.action;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.taskTimer.service.TimeTaskService;

@Namespace("/task")
@Controller
@Scope("prototype")
@ParentPackage("default")
public class TaskAction extends BaseActionSupport {

	private static final long serialVersionUID = -164661537469123056L;
	private String title = "提醒";
	private String rightsId = "";
	@Resource
	private TimeTaskService timeTaskService;

	@Action(value = "makeRemindByDay", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void makeRemindByDay() throws Exception {

	}

	//	@Action(value = "makeRemindByWeek", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	//	public void makeRemindByWeek() throws Exception {
	//		timeTaskService.makeRemindByWeek();
	//	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
