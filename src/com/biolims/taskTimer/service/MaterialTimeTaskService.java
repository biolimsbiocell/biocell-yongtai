/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：Service
 * 类描述：日志管理service
 * 创建人：倪毅
 * 创建时间：2011-10
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.taskTimer.service;

import java.util.Date;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.biolims.analysis.analy.service.AnalysisTaskService;
import com.biolims.analysis.desequencing.service.DeSequencingTaskService;
import com.biolims.common.service.CommonService;

@Service
public class MaterialTimeTaskService extends CommonService {
		@Resource
		private DeSequencingTaskService deSequencingTaskService;
		
		@Resource
		private AnalysisTaskService analysisTaskService;
		
		public void excuteAn() throws Exception {
//			purchaseOrderService.batchStorageOrderItem();
//			deSequencingTaskService.getCsvContent();//读取CSV
//			analysisTaskService.getCsvContent();//读取CSV
		}
}
