package com.biolims.taskTimer.servlet;

import java.util.Timer;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.roomManagement.service.RoomManagementService;
import com.biolims.taskTimer.service.TimeTaskService;
import com.biolims.taskTimer.time.TaskTimer;

/**
 * servlet 监听器
 */
public class TaskTimerServletListener implements ServletContextListener {
	private Timer time = null;

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		if (time != null) {
			time.cancel();
		}
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		time = new Timer(true);
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(arg0.getServletContext());
		TimeTaskService remindService = (TimeTaskService) ctx.getBean("timeTaskService");
//		RoomManagementService roomManagementService =(RoomManagementService) ctx.getBean("roomManagementService");
		
		TaskTimer bt = new TaskTimer(remindService);
//		TaskTimer bt1=new TaskTimer(roomManagementService);
		//时间间隔一 小时
		long period = 60 * 60 * 1000;
		
//		long a=60*60*1000;
		//long period=60*1000*5;
		time.schedule(bt, 0, period);
//		time.schedule(bt1, 0,a);
		//		try {
		//			if (!RSAUtil.validate())
		//				throw new RuntimeException("系统许可文件验证失败！请联系管理员！");
		//
		//		} catch (Exception e) {
		//			e.printStackTrace();
		//			throw new RuntimeException("系统许可文件验证失败！请联系管理员！");
		//		}
	}
}
