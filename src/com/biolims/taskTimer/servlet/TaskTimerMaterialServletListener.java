package com.biolims.taskTimer.servlet;

import java.util.Timer;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.taskTimer.service.MaterialTimeTaskService;
import com.biolims.taskTimer.time.MaterialTaskTimer;

/**
 * servlet 监听器
 */
public class TaskTimerMaterialServletListener implements ServletContextListener {
	private Timer time = null;

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		if (time != null) {
			time.cancel();
		}
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		time = new Timer(true);
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(arg0.getServletContext());
		MaterialTimeTaskService materialTimeTaskService = (MaterialTimeTaskService) ctx.getBean("materialTimeTaskService");
		
		MaterialTaskTimer bt = new MaterialTaskTimer(materialTimeTaskService);
		//时间间隔一 小时
		long period = 60 * 60 * 1000;
		time.schedule(bt, 0, period);
	}
}
