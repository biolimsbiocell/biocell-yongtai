package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

import com.biolims.experiment.qc.agros.custom.AgrosTaskEvent;

/**
 * 路由中，取得金额，金额字段名称应该为fee
 * 
 * @author niyi
 * 
 */
public class RoutingSetAgroswcEventListener implements ExecutionListener {

	public void notify(DelegateExecution execution) throws Exception {

		String businessKey = execution.getProcessBusinessKey();
		AgrosTaskEvent dtwf = new AgrosTaskEvent();
		dtwf.operation("", businessKey);

	}

}
