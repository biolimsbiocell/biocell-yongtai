package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.dna.model.DnaTask;
import com.biolims.experiment.dna.service.DnaTaskService;
import com.biolims.experiment.enmonitor.bacteria.model.CleanAreaBacteria;
import com.biolims.experiment.enmonitor.bacteria.service.CleanAreaBacteriaService;
import com.biolims.experiment.enmonitor.dust.model.CleanAreaDust;
import com.biolims.experiment.enmonitor.dust.service.DustParticleService;
import com.biolims.experiment.qpcr.model.QpcrTask;
import com.biolims.experiment.qpcr.service.QpcrTaskService;

/**
 * 洁净区尘埃粒子测试记录审核监听
 * 
 * @author
 * 
 */
public class UserTaskCleanAreaBacteriaApprovalSHListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		CleanAreaBacteriaService cleanAreaBacteriaService = (CleanAreaBacteriaService) ctx
				.getBean("cleanAreaBacteriaService");
		try {
			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			CleanAreaBacteria sct = cleanAreaBacteriaService.gets(businessKey);
			if (sct.getApprover() != null) {
				delegateTask.addCandidateUser(sct.getApprover().getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
