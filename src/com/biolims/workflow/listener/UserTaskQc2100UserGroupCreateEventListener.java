package com.biolims.workflow.listener;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.qc.model.Qc2100Task;
import com.biolims.experiment.qc.service.WKQualitySampleTaskService;

/**
 * 2100质控实验组监听器
 * @author
 *
 */
public class UserTaskQc2100UserGroupCreateEventListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		List<String> groupList = new ArrayList<String>();
		WKQualitySampleTaskService wKQualitySampleTaskService = (WKQualitySampleTaskService) ctx.getBean("WKQualitySampleTaskService");
		try {
			String businessKey = delegateTask.getExecution().getProcessBusinessKey();
			Qc2100Task sct = wKQualitySampleTaskService.get(businessKey);
			if (sct.getAcceptUser() != null) {
				groupList.add(sct.getAcceptUser().getId());
			}
			delegateTask.addCandidateGroups(groupList);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
