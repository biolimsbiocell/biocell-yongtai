package com.biolims.workflow.listener;

import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.activiti.engine.repository.ProcessDefinition;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.applicationType.model.ApplicationTypeAction;
import com.biolims.applicationType.model.ApplicationTypeTable;
import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.common.service.CommonService;
import com.biolims.util.BeanUtils;
import com.biolims.workflow.WorkflowConstants;
import com.biolims.workflow.entity.WorkflowBindForm;
import com.biolims.workflow.service.WorkflowService;

/**
 * 工作流取消
 * @author cong
 *
 */
public class RoutingYesTakeEventListener implements ExecutionListener {
	private org.activiti.engine.impl.el.FixedValue excuteMethod;

	public void notify(DelegateExecution execution) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		WorkflowService workflowService = (WorkflowService) ctx.getBean("workflowService");
		CommonService commonService = (CommonService) ctx.getBean("commonService");
		String businessKey = execution.getProcessBusinessKey();
		String definitionId = execution.getProcessDefinitionId();
		ProcessDefinition pd = workflowService.getRepositoryService().createProcessDefinitionQuery()
				.processDefinitionId(definitionId).singleResult();
		WorkflowBindForm form = workflowService.getWorkflowBindFormByDefinitionKey(pd.getKey());
		ApplicationTypeTable att = workflowService.get(ApplicationTypeTable.class, form.getFormName());

		Class<?> objClass = Class.forName(att.getClassPath());
		Object b = workflowService.get(objClass, businessKey);
		BeanUtils.setFieldValue(b, "state", WorkflowConstants.WORKFLOW_COMPLETE);
		BeanUtils.setFieldValue(b, "stateName", WorkflowConstants.WORKFLOW_COMPLETE_NAME);
		workflowService.save(b);

		ObjectEvent objectEvent = null;
		List<ApplicationTypeAction> ata = commonService
				.find("from ApplicationTypeAction where applicationTypeTable.id='" + att.getId()
						+ "' and state = '1' and type = 'end' ");
		if (ata.size() > 0) {
			if (ata.get(0).getClassPath() != null)
				objectEvent = (ObjectEvent) Class.forName(ata.get(0).getClassPath()).newInstance();
			if (objectEvent != null) {
				objectEvent.operation(ata.get(0).getId(), businessKey);
			}
		}
	}

}
