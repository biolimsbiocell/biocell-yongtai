package com.biolims.workflow.listener;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.deviation.plan.custom.DeviationSurveyPlanEvent;
import com.biolims.deviation.plan.model.DeviationSurveyPlan;
import com.biolims.deviation.plan.service.DeviationSurveyPlanService;

public class RoutingSetDHRReportWcListener implements ExecutionListener {
	@Override
	public void notify(DelegateExecution execution) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext.getServletContext());
		DeviationSurveyPlanService deviationSurveyPlanService = (DeviationSurveyPlanService) ctx
				.getBean("deviationSurveyPlanService");
		
		// 得到业务键
		String businessKey = execution.getProcessBusinessKey();
		DeviationSurveyPlanEvent dtwf = new DeviationSurveyPlanEvent();
		DeviationSurveyPlan sct = deviationSurveyPlanService.get(businessKey);
		if (!"".equals(sct.getApprovalUser().getName()) && sct.getApprovalUser().getName() != null) {
	
				 SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			      String date =sdf.format(new Date());
			       Date approvalDate =sdf.parse(date);
				  sct.setApprovalDate(approvalDate);
		}
		dtwf.operation("", businessKey);
	}
}
