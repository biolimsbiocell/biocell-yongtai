package com.biolims.workflow.listener;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.model.user.User;
import com.biolims.deviation.custom.DeviationHandlingReportEvent;
import com.biolims.deviation.model.DeviationHandlingReport;
import com.biolims.deviation.service.DeviationHandlingReportService;

public class RoutingSetPCCLEventListener implements ExecutionListener {

	public void notify(DelegateExecution execution) throws Exception {

		String businessKey = execution.getProcessBusinessKey();
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext.getServletContext());
		DeviationHandlingReportService deviationHandlingReportService = (DeviationHandlingReportService) ctx
				.getBean("deviationHandlingReportService");
		DeviationHandlingReport sct = deviationHandlingReportService.get(businessKey);
		if(sct.getType().equals("1")) {
			if (!"".equals(sct.getDepartmentUser().getId()) && sct.getDepartmentUser().getId() != null) {
				  SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			      String date =sdf.format(new Date());
			       Date departmentDate =sdf.parse(date);
				  sct.setDepartmentDate(departmentDate);
				  deviationHandlingReportService.saveD(sct);

			}
		}
			if (sct.getReportUser()!= null) {
				  SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			      String date =sdf.format(new Date());
			       Date reportDate =sdf.parse(date);
				  sct.setReportDate(reportDate);
				  deviationHandlingReportService.saveD(sct);

		}
		
		DeviationHandlingReportEvent dtwf = new DeviationHandlingReportEvent();
		dtwf.operation("", businessKey);

	}
}
