package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

import com.biolims.experiment.quality.custom.QualityTestResultManageEvent;
import com.biolims.report.custom.ReportEvent;

/**
 * 路由中，取得金额，金额字段名称应该为fee
 * 
 * @author niyi
 * 
 */
public class RoutingSetQualityTestResultManagewcEventListener implements ExecutionListener {

	public void notify(DelegateExecution execution) throws Exception {

		String businessKey = execution.getProcessBusinessKey();
		QualityTestResultManageEvent dtwf = new QualityTestResultManageEvent();
		dtwf.operation("", businessKey);

	}

}
