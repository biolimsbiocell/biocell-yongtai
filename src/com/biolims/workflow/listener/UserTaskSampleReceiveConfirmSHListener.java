package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.sample.model.SampleReceive;
import com.biolims.sample.service.SampleReceiveService;

/**
 * 样本接收审核监听
 * 
 * @author
 * 
 */
public class UserTaskSampleReceiveConfirmSHListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		// List<String> groupList = new ArrayList<String>();
		SampleReceiveService sampleReceiveService = (SampleReceiveService) ctx
				.getBean("sampleReceiveService");
		try {
			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			SampleReceive sct = sampleReceiveService.get(businessKey);
			if (sct.getConfirmUser() != null) {
				delegateTask.addCandidateUser(sct.getConfirmUser().getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
