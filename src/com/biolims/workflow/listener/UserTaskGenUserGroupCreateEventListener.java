package com.biolims.workflow.listener;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.generation.model.GenerationTask;
import com.biolims.experiment.generation.service.GenerationTestService;

/**
 * 一代测序实验组监听器
 * @author
 *
 */
public class UserTaskGenUserGroupCreateEventListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		List<String> groupList = new ArrayList<String>();
		GenerationTestService generationTestService = (GenerationTestService) ctx.getBean("generationTestService");
		try {
			String businessKey = delegateTask.getExecution().getProcessBusinessKey();
			GenerationTask sct = generationTestService.get(businessKey);
			if (sct.getAcceptUser() != null) {
				groupList.add(sct.getAcceptUser().getId());
			}
			delegateTask.addCandidateGroups(groupList);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
