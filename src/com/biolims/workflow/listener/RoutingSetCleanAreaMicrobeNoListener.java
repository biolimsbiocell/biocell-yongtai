package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.service.CommonService;
import com.biolims.experiment.enmonitor.microbe.model.CleanAreaMicrobe;
import com.biolims.experiment.enmonitor.microbe.model.SettlingMicrobe;
import com.biolims.experiment.enmonitor.microbe.service.SettlingMicrobeService;
import com.biolims.sample.model.SampleOrder;
import com.biolims.system.sample.service.SampleOrderMainService;

public class RoutingSetCleanAreaMicrobeNoListener implements ExecutionListener {

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		// 得到业务键
		String businessKey = execution.getProcessBusinessKey();
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		SettlingMicrobeService settlingMicrobeService = (SettlingMicrobeService) ctx
				.getBean("settlingMicrobeService");
		CommonService commonService = (CommonService) ctx
				.getBean("commonService");
		CleanAreaMicrobe sct = settlingMicrobeService.gets(businessKey);
		sct.setState("20");
		sct.setStateName("提交人修改");
		commonService.saveOrUpdate(sct);
	}
}
