package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

import com.biolims.experiment.enmonitor.dust.custom.DustParticleEvent;
import com.biolims.experiment.enmonitor.volume.custom.CleanAreaVolumeEvent;
import com.biolims.experiment.wk.custom.WkTaskEvent;

/**
 * 洁净区风量测试记录工作流完成
 * @author niyi
 *
 */
public class RoutingSetCleanAreaVolumewcEventListener implements ExecutionListener {

	public void notify(DelegateExecution execution) throws Exception {

		String businessKey = execution.getProcessBusinessKey();
		CleanAreaVolumeEvent dtwf = new CleanAreaVolumeEvent();
		dtwf.operation("", businessKey);

	}

}
