package com.biolims.workflow.listener;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.workflow.WorkflowConstants;
import com.biolims.workflow.entity.VoteResultEntity;
import com.biolims.workflow.entity.WorkflowVoteRuleSetEntity;
import com.biolims.workflow.service.VoteService;

/**
 * 多用户任务监听器，用户完成
 * 1.判断是否完成、保存会签结果
 * @author cong
 *
 */
public class UserTaskVoteMultiCompletEventListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		VoteService voteService = (VoteService) ctx.getBean("voteService");
		String taskId = delegateTask.getId();
		String activitiId = delegateTask.getExecution().getCurrentActivityId();
		String processDefinitionId = delegateTask.getProcessDefinitionId();
		String processInstanceId = delegateTask.getProcessInstanceId();
		String oper = (String) delegateTask.getVariable(WorkflowConstants.RESULT_IS_OPER_PROPERTY_KEY);

		try {
			//保存结果
			VoteResultEntity vre = new VoteResultEntity();
			vre.setActivitiId(activitiId);
			vre.setOperDate(new Date());
			vre.setProcessDefinitionId(processDefinitionId);
			vre.setTaskId(taskId);
			vre.setResult(oper);
			vre.setProcessInstanceId(processInstanceId);
			vre.setOperUserId(delegateTask.getAssignee());

			Map<String, String> result = isComplete(voteService, delegateTask.getExecution(), vre);

			//会签结果和会签结束条件结果
			//为一个退回，全部就退回的会签过程增加
			if (oper.equals("0")) {

				//设置会签循环条件的结果  
				delegateTask.getExecution().setVariable(WorkflowConstants.MULTI_COMPLETION_CONDITION_VARIABLE_NAME,
						"true");
				//投票结果
				delegateTask.getExecution().setVariable(WorkflowConstants.VOTE_REULT_KEY, "false");
			} else {

				//设置会签循环条件的结果  
				delegateTask.getExecution().setVariable(WorkflowConstants.MULTI_COMPLETION_CONDITION_VARIABLE_NAME,
						result.get("isComplet"));
				//投票结果
				delegateTask.getExecution().setVariable(WorkflowConstants.VOTE_REULT_KEY, result.get("voteResult"));

			}

			//设置会签循环条件的结果  
			//delegateTask.getExecution().setVariable(WorkflowConstants.MULTI_COMPLETION_CONDITION_VARIABLE_NAME,
			//	result.get("isComplet"));
			//投票结果
			//delegateTask.getExecution().setVariable(WorkflowConstants.VOTE_REULT_KEY, result.get("voteResult"));
			//voteService.save(vre);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 会签完成条件结果
	 * @param delegateExecution
	 * @return
	 * @throws Exception
	 */
	private Map<String, String> isComplete(VoteService voteService, DelegateExecution delegateExecution,
			VoteResultEntity vre) throws Exception {

		//完成会签的次数   
		Integer completeCount = (Integer) delegateExecution.getVariable("nrOfCompletedInstances");
		//总循环次数   
		Integer nrOfInstances = (Integer) delegateExecution.getVariable("nrOfInstances");

		//没有会签实例   
		if (nrOfInstances == null || nrOfInstances <= 0) {
			Map<String, String> result = new HashMap<String, String>();
			result.put("voteResult", "false");//投票结果
			result.put("isComplete", "true");//循环条件结束结果
			return result;
		}

		Map<String, String> isComplete = calculateResult(voteService, nrOfInstances, completeCount, delegateExecution,
				vre);
		return isComplete;
	}

	/*
	 * 计算结果
	 */
	private Map<String, String> calculateResult(VoteService voteService, Integer nrOfInstances, Integer completeCount,
			DelegateExecution execution, VoteResultEntity vre) throws Exception {

		String processDefinitionId = execution.getProcessDefinitionId();
		String activitiId = execution.getCurrentActivityId();
		String instanceId = execution.getProcessInstanceId();

		WorkflowVoteRuleSetEntity voteRule = voteService.getWorkflowVoteRuleSetEntity(processDefinitionId, activitiId);

		//同意数量
		//	Long agreeNum = voteService.getCountOperResultVote(instanceId, activitiId,
		//			WorkflowConstants.RESULT_IS_OPER_PROPERTY_VALUE_TRUE, null);
		//反对数量
		//Long oppsoitionNum = voteService.getCountOperResultVote(instanceId, activitiId,
		//		WorkflowConstants.RESULT_IS_OPER_PROPERTY_VALUE_FALSE, null);

		Long agreeNum = (Long) execution.getVariable("agreeNum");
		Long oppsoitionNum = (Long) execution.getVariable("oppsoitionNum");
		if (agreeNum == null)

			agreeNum = 0L;

		if (oppsoitionNum == null)

			oppsoitionNum = 0L;
		//累计本次审批结果
		if (WorkflowConstants.RESULT_IS_OPER_PROPERTY_VALUE_TRUE.equals(vre.getResult())) {
			agreeNum = agreeNum + 1;
		} else if (WorkflowConstants.RESULT_IS_OPER_PROPERTY_VALUE_FALSE.equals(vre.getResult())) {
			oppsoitionNum = oppsoitionNum + 1;
		}

		execution.setVariable("agreeNum", agreeNum);
		execution.setVariable("oppsoitionNum", oppsoitionNum);

		completeCount++;//将本次的完成加到完成总数中

		Map<String, String> result = new HashMap<String, String>();
		String voteResult = "false";

		result.put("voteResult", voteResult);//投票结果
		result.put("isComplete", "false");//循环条件结束结果

		if (voteRule == null) {
			//没有设置则必须全部会签才可结束
			if (agreeNum + oppsoitionNum >= nrOfInstances) {
				result.put("isComplete", "true");
				if (Integer.valueOf(agreeNum.toString()).equals(nrOfInstances))
					result.put("voteResult", "true");
			}
		} else {
			String oneVoteRights = voteRule.getOneVoteRights();//一票制
			String userIds = getOneVoteRightsUserIds(voteRule.getUserIds(), voteRule.getGroupIds());

			String oneVoteResult = null;
			if (oneVoteRights != null && oneVoteRights.length() > 0) {
				Long countUserVote = 0l;
				if (WorkflowConstants.ONE_VOTE_RIGHTS_0.equals(oneVoteRights)) {
					countUserVote = voteService.getCountOperResultVote(instanceId, activitiId,
							WorkflowConstants.RESULT_IS_OPER_PROPERTY_VALUE_FALSE, userIds);
					oneVoteResult = WorkflowConstants.RESULT_IS_OPER_PROPERTY_VALUE_FALSE;
				} else if (WorkflowConstants.ONE_VOTE_RIGHTS_1.equals(oneVoteRights)) {
					countUserVote = voteService.getCountOperResultVote(instanceId, activitiId,
							WorkflowConstants.RESULT_IS_OPER_PROPERTY_VALUE_TRUE, userIds);
					oneVoteResult = WorkflowConstants.RESULT_IS_OPER_PROPERTY_VALUE_TRUE;
				}

				if (userIds != null && userIds.length() > 0) {//加上本次结果
					if (userIds.indexOf("'" + vre.getOperUserId() + "'") != -1) {
						countUserVote = countUserVote + 1;
					}

				}

				//统计一票制结果
				voteResult = getOneVoteRightsResult(oneVoteRights, countUserVote, agreeNum, oppsoitionNum,
						nrOfInstances, oneVoteResult).toString();

			} else {
				//按决策方式统计
				voteResult = getRuleMethod(voteRule, agreeNum, oppsoitionNum, completeCount, nrOfInstances).toString();
			}

			if (agreeNum + oppsoitionNum >= nrOfInstances)
				result.put("isComplete", "true");
			result.put("voteResult", voteResult);//投票结果
		}

		return result;
	}

	private String getOneVoteRightsUserIds(String userIds, String groupIds) throws Exception {
		String result = null;
		if (userIds != null && userIds.length() > 0) {
			result = "";
			String[] array = userIds.split(",");
			for (String userId : array) {
				if (userId.trim().length() > 0) {
					result = result + "'" + userId + "',";
				}
			}
			if (result.endsWith(","))
				result = result.substring(0, result.length() - 1);
		}

		if (groupIds != null && groupIds.length() > 0) {
			//TODO
		}

		return result;
	}

	/*
	 * 一票制结果
	 */
	private Boolean getOneVoteRightsResult(String oneVoteRights, Long countNum, Long agreeNum, Long oppsoitionNum,
			Integer nrOfInstances, String oneVoteResult) throws Exception {
		boolean result = false;

		if (countNum == 0) {//没有形使权利或者没有投票
			if (Integer.valueOf(agreeNum.toString()) >= nrOfInstances) {
				result = true;
			} else if (agreeNum + oppsoitionNum >= nrOfInstances) {
				result = false;
			}
		} else if (countNum > 0) {//行使权力
			result = Boolean.valueOf(oneVoteResult);
		}
		return result;
	}

	/*
	 * 按决策方式获取投票结果
	 */
	private Boolean getRuleMethod(WorkflowVoteRuleSetEntity voteRule, long agreeNum, long oppsoitionNum,
			Integer completeCount, Integer nrOfInstances) throws Exception {

		boolean bool = false;

		String voteType = voteRule.getVoteType();
		Float voteNum = voteRule.getVoteNum();
		String ruleMethod = voteRule.getRuleMethod();//决策方式

		if (WorkflowConstants.VOTE_RULE_METHOD_AGREE.equals(ruleMethod)) {//决策方式同意
			if (WorkflowConstants.RULE_METHOD_TYPE_ABSOLUTE.equals(voteType)) {
				if (agreeNum >= voteNum) {
					bool = true;
				}
			} else if (WorkflowConstants.RULE_METHOD_TYPE_PERCENTAGE.equals(voteType)) {
				if (Float.valueOf(agreeNum) / Float.valueOf(nrOfInstances) > voteNum) {
					bool = true;
				}
			}

		} else if (WorkflowConstants.VOTE_RULE_METHOD_DISAGREE.equals(ruleMethod)) {//决策方式不同意
			if (WorkflowConstants.RULE_METHOD_TYPE_ABSOLUTE.equals(voteType)) {
				if (oppsoitionNum >= voteNum) {
					bool = true;
				}
			} else if (WorkflowConstants.RULE_METHOD_TYPE_PERCENTAGE.equals(voteType)) {
				if (Float.valueOf(oppsoitionNum) / Float.valueOf(nrOfInstances) > voteNum) {
					bool = true;
				}
			}
		}
		return bool;
	}

}
