package com.biolims.workflow.listener;


import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.equipment.fault.service.InstrumentFaultService;
import com.biolims.equipment.model.InstrumentFault;

/**
 * 设备报修审核监听
 * 
 * @author
 * 
 */
public class UserTaskInstrumentFaultcreateEventListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		// List<String> groupList = new ArrayList<String>();
		InstrumentFaultService instrumentFaultService = (InstrumentFaultService) ctx
				.getBean("instrumentFaultService");
		try {
			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			InstrumentFault sct = instrumentFaultService.getInstrumentFault(businessKey);
			if (sct.getConfirmUser() != null) {
				delegateTask.addCandidateUser(sct.getConfirmUser().getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
