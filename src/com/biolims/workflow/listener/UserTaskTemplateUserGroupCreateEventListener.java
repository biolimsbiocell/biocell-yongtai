package com.biolims.workflow.listener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.core.model.user.UserGroupUser;
import com.biolims.remind.model.SysRemind;
import com.biolims.sample.model.SampleReceive;
import com.biolims.sample.service.SampleReceiveService;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.service.TemplateService;

public class UserTaskTemplateUserGroupCreateEventListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext.getServletContext());
		TemplateService templateService = (TemplateService) ctx.getBean("templateService");
		List<String> groupList = new ArrayList<String>();
		try {
			String businessKey = delegateTask.getExecution().getProcessBusinessKey();
			Template sct = templateService.get(businessKey);
			if (!"".equals(sct.getConfirmUser().getId()) && sct.getConfirmUser().getId() != null) {
				delegateTask.addCandidateUser(sct.getConfirmUser().getId());
			}
			templateService.saveCreateUserAndTime(businessKey);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
