package com.biolims.workflow.listener;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.dna.model.DnaTask;
import com.biolims.experiment.dna.service.DnaTaskService;
import com.biolims.experiment.qpcr.model.QpcrTask;
import com.biolims.experiment.qpcr.service.QpcrTaskService;

/**
 * 核酸提取实验组监听器
 * 
 * @author
 * 
 */
public class UserTaskQpcrUserGroupCreateEventListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		QpcrTaskService qpcrTaskService = (QpcrTaskService) ctx
				.getBean("qpcrTaskService");
		try {
			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			QpcrTask sct = qpcrTaskService.get(businessKey);
			if (!"".equals(sct.getTestUserOneId())) {
				String[] uidOne = sct.getTestUserOneId().split(",");
				for (int i = 0; i < uidOne.length; i++) {
					delegateTask.addCandidateUser(uidOne[i]);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
