package com.biolims.workflow.listener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.workflow.entity.WorkflowProxyUserSiteEntity;
import com.biolims.workflow.service.WorkflowService;

/**
 * 多用户任务监听器，分配办理人
 * 判断被分配的办理人是否设置了代办人，如果设置代办人则将任务办理人改为代办人并设置OWNER
 * @author cong
 */
public class UserTaskVoteMultiAssignmentEventListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		WorkflowService workflowService = (WorkflowService) ctx.getBean("workflowService");
		try {
			Map<String, String> proxyUserMap = parseProxyUser(workflowService.findWorkflowProxyUserSiteEntityList("1"));
			String proxyUser = proxyUserMap.get(delegateTask.getAssignee());
			if (proxyUser != null) {
				delegateTask.setOwner(delegateTask.getAssignee());
				delegateTask.setAssignee(proxyUser);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private Map<String, String> parseProxyUser(List<WorkflowProxyUserSiteEntity> proxyUserList) throws Exception {
		Map<String, String> result = new HashMap<String, String>();
		if (proxyUserList != null && proxyUserList.size() > 0) {
			for (WorkflowProxyUserSiteEntity proxyUser : proxyUserList) {
				result.put(proxyUser.getAssignee(), proxyUser.getProxyUserId());
			}
		}
		return result;
	}

}
