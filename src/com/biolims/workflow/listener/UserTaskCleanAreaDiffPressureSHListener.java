package com.biolims.workflow.listener;

import java.util.ArrayList;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.dna.model.DnaTask;
import com.biolims.experiment.dna.service.DnaTaskService;
import com.biolims.experiment.enmonitor.differentialpressure.model.CleanAreaDiffPressure;
import com.biolims.experiment.enmonitor.differentialpressure.service.CleanAreaDiffPressureService;
import com.biolims.experiment.enmonitor.dust.model.CleanAreaDust;
import com.biolims.experiment.enmonitor.dust.service.DustParticleService;
import com.biolims.experiment.enmonitor.microorganism.model.CleanAreaMicroorganism;
import com.biolims.experiment.enmonitor.microorganism.service.CleanAreaMicroorganismService;
import com.biolims.experiment.enmonitor.volume.model.CleanAreaVolume;
import com.biolims.experiment.enmonitor.volume.service.CleanAreaVolumeService;
import com.biolims.experiment.qpcr.model.QpcrTask;
import com.biolims.experiment.qpcr.service.QpcrTaskService;

import antlr.collections.List;

/**
 * 压力记录审核监听
 * 
 * @author
 * 
 */
public class UserTaskCleanAreaDiffPressureSHListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext.getServletContext());
		CleanAreaDiffPressureService cleanAreaDiffPressureService = (CleanAreaDiffPressureService) ctx.getBean("cleanAreaDiffPressureService");
		try {
			String businessKey = delegateTask.getExecution().getProcessBusinessKey();
			CleanAreaDiffPressure sct = cleanAreaDiffPressureService.gets(businessKey);
			if (sct.getConfirmUser() != null) {
				delegateTask.addCandidateUser(sct.getConfirmUser().getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
			
		}

	}
}
