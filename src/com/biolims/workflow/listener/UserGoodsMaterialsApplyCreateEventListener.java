package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.goods.mate.model.GoodsMaterialsApply;
import com.biolims.goods.mate.service.GoodsMaterialsApplyService;

public class UserGoodsMaterialsApplyCreateEventListener implements TaskListener {

	@Override
	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		GoodsMaterialsApplyService goodsMaterialsApplyService = (GoodsMaterialsApplyService) ctx.getBean("goodsMaterialsApplyService");
		String businessKey = delegateTask.getExecution().getProcessBusinessKey();
		GoodsMaterialsApply sct = goodsMaterialsApplyService.get(businessKey);
//		delegateTask.setAssignee(sct.get);
	}

}
