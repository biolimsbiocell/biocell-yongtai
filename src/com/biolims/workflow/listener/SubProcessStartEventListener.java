package com.biolims.workflow.listener;

import java.util.Date;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.activiti.engine.repository.ProcessDefinition;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

import com.biolims.workflow.WorkflowConstants;
import com.biolims.workflow.entity.WorkflowProcesssInstanceEntity;
import com.biolims.workflow.service.WorkflowService;

/**
 * 子流程的启动
 * @author cong
 *
 */
public class SubProcessStartEventListener implements ExecutionListener {

	public void notify(DelegateExecution execution) throws Exception {
		WebApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
		WorkflowService workflowService = (WorkflowService) ctx.getBean("workflowService");

		WorkflowProcesssInstanceEntity wpie = new WorkflowProcesssInstanceEntity();
		wpie.setBusinessKey(execution.getProcessBusinessKey());
		wpie.setProcessInstanceId(execution.getId());
		wpie.setParentProcessId(execution.getProcessInstanceId());
		wpie.setProcessDefinitionId(execution.getProcessDefinitionId());
		wpie.setIsSubProcess(WorkflowConstants.IS_SUB_PROCESS_YES);
		ProcessDefinition pd = workflowService.getRepositoryService().createProcessDefinitionQuery()
				.processDefinitionId(execution.getProcessDefinitionId()).singleResult();
		wpie.setVersion(pd.getVersion());
		wpie.setStartDate(new Date());

		Object formName = execution.getVariable("formName");
		Object formId = execution.getVariable("formId");
		if (formName != null && formId != null) {
			wpie.setFormName(formName.toString());
			wpie.setBusinessKey(formId.toString());
		}

		workflowService.save(wpie);

	}

}
