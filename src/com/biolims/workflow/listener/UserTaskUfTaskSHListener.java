package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.uf.model.UfTask;
import com.biolims.experiment.uf.service.UfTaskService;

/**
 * 超声破碎审核监听
 * 
 * @author
 * 
 */
public class UserTaskUfTaskSHListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		// List<String> groupList = new ArrayList<String>();
		UfTaskService ufTaskService = (UfTaskService) ctx
				.getBean("ufTaskService");
		try {
			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			UfTask sct = ufTaskService.get(businessKey);
			
				delegateTask.addCandidateUser("admin");
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
