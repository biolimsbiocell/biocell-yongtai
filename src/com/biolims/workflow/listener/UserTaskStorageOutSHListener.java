package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.sample.storage.model.SampleOut;
import com.biolims.sample.storage.model.SampleOutApply;
import com.biolims.sample.storage.service.SampleOutApplyService;
import com.biolims.sample.storage.service.SampleOutService;
import com.biolims.storage.model.StorageOut;
import com.biolims.storage.out.service.StorageOutService;

/**
 * 库存出库审核
 * 
 * @author
 * 
 */
public class UserTaskStorageOutSHListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		StorageOutService storageOutService = (StorageOutService) ctx
				.getBean("storageOutService");
		try {
			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			StorageOut sct = storageOutService.findStorageOutById(businessKey);
//			if (sct.getAcceptUser() != null) {
//				delegateTask.addCandidateUser(sct.getAcceptUser().getId());
//			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
