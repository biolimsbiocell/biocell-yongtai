package com.biolims.workflow.listener;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.core.model.user.UserGroupUser;
import com.biolims.core.userGroup.service.UserGroupService;
import com.biolims.util.SendEmailUtil;
import com.biolims.workflow.entity.WorkflowProcessDefinitionTaskGroupConfigEntity;
import com.biolims.workflow.entity.WorkflowProcessDefinitionTaskUserConfigEntity;
import com.biolims.workflow.entity.WorkflowProcesssInstanceEntity;
import com.biolims.workflow.entity.WorkflowProxyUserSiteEntity;
import com.biolims.workflow.service.WorkflowProcessDefinitionService;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

/**
 * 用户任务CREATE事件监听器
 * @author cong
 *
 */
public class UserTaskCreateEmailEventListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		WorkflowProcessDefinitionService definitionService = (WorkflowProcessDefinitionService) ctx
				.getBean("workflowProcessDefinitionService");
		UserGroupService userGroupService = (UserGroupService) ctx.getBean("userGroupService");
		CommonService commonService = (CommonService) ctx.getBean("commonService");
		String definitionId = delegateTask.getExecution().getProcessDefinitionId();
		String activitiId = delegateTask.getExecution().getCurrentActivityId();

		WorkflowProcessInstanceService workflowProcessInstanceService = (WorkflowProcessInstanceService) ctx
				.getBean("workflowProcessInstanceService");
		String instanceId = delegateTask.getExecution().getProcessInstanceId();

		try {

			WorkflowProcesssInstanceEntity instance = workflowProcessInstanceService
					.getWorkflowProcesssInstanceEntityByInstanceId(instanceId);

			List<String> mailList = new ArrayList<String>();
			List<WorkflowProcessDefinitionTaskUserConfigEntity> tuceList = definitionService
					.findWorkflowProcessDefinitionTaskUserConfigEntityByKey(definitionId, activitiId);
			if (tuceList != null && tuceList.size() > 0) {
				Map<String, String> proxyUserMap = parseProxyUser(definitionService
						.findWorkflowProxyUserSiteEntityList("1"));
				List<String> userList = new ArrayList<String>();
				for (WorkflowProcessDefinitionTaskUserConfigEntity tuce : tuceList) {
					String proxyUser = proxyUserMap.get(tuce.getUserId());
					User u = commonService.get(User.class, tuce.getUserId());

					mailList.add(u.getEmail());
				}

			}

			List<WorkflowProcessDefinitionTaskGroupConfigEntity> tgceList = definitionService
					.findWorkflowProcessDefinitionTaskGroupConfigEntityByKey(definitionId, activitiId);

			if (tgceList != null && tgceList.size() > 0) {
				for (WorkflowProcessDefinitionTaskGroupConfigEntity tgce : tgceList) {

					List<UserGroupUser> l = userGroupService.findUserGroupUserList(tgce.getGroupId());
					if (l.size() > 0) {

						for (UserGroupUser ugu : l) {
							if (ugu.getUser() != null) {

								mailList.add(ugu.getUser().getEmail());

							}
						}
					}

				}

			}

			if (mailList.size() > 0) {
				InputStream is = Thread.currentThread().getContextClassLoader()
						.getResourceAsStream("system.properties");
				Properties pro = new Properties();
				pro.load(is);
				String emailForm = pro.getProperty("smtp.username");
				String emailFormPwd = pro.getProperty("smtp.password");
				String emailHostname = pro.getProperty("smtp.host");
				String emailTitle = "单号:" + delegateTask.getExecution().getProcessBusinessKey() + ","
						+ instance.getFormTitle() + "需要您审批！";
				String emailMsg = "单号:" + delegateTask.getExecution().getProcessBusinessKey() + ","
						+ instance.getFormTitle()
						+ "需要您审批！请点击<a href='http://106.2.184.86:55002/main/toWelcome.action'>进入系统</a>";
				SendEmailUtil.send(mailList, emailHostname, emailForm, emailFormPwd, emailTitle, emailMsg);
				if (is != null)
					is.close();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/*
	 * 代办人 
	 */
	private Map<String, String> parseProxyUser(List<WorkflowProxyUserSiteEntity> proxyUserList) throws Exception {
		Map<String, String> result = new HashMap<String, String>();
		if (proxyUserList != null && proxyUserList.size() > 0) {
			for (WorkflowProxyUserSiteEntity proxyUser : proxyUserList) {
				result.put(proxyUser.getAssignee(), proxyUser.getProxyUserId());
			}
		}
		return result;
	}

}
