package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.sequencing.model.SequencingTask;
import com.biolims.experiment.sequencing.service.SequencingTaskService;

/**
 * 上机测序实验组监听器
 * 
 * @author
 * 
 */
public class UserTaskSeqUserGroupCreateEventListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		SequencingTaskService sequencingService = (SequencingTaskService) ctx
				.getBean("sequencingTaskService");
		try {
			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			SequencingTask sct = sequencingService.get(businessKey);
			if (!"".equals(sct.getTestUserOneId())) {
				String[] uidOne = sct.getTestUserOneId().split(",");
				for (int i = 0; i < uidOne.length; i++) {
					delegateTask.addCandidateUser(uidOne[i]);
				}
			}
			// if (sct.getReciveUser() != null) {
			// delegateTask.addCandidateUser(sct.getReciveUser().getId());
			// } else {
			// if (sct.getAcceptUser() != null) {
			// groupList.add(sct.getAcceptUser().getId());
			// }
			// delegateTask.addCandidateGroups(groupList);
			// }
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
