package com.biolims.workflow.listener;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.analysis.assignment.model.AssignmentTaskItem;
import com.biolims.analysis.assignment.service.AssignmentTaskService;

/**
 * 一代测序实验组监听器
 * @author
 *
 */
public class UserTaskAssignmentGroupCreateEventListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		List<String> groupList = new ArrayList<String>();
		AssignmentTaskService assignmentTaskService = (AssignmentTaskService) ctx.getBean("assignmentTaskService");
		try {
			String businessKey = delegateTask.getExecution().getProcessBusinessKey();
			AssignmentTaskItem sct = assignmentTaskService.getItem(businessKey);
			if (sct.getAcceptUser() != null) {
				groupList.add(sct.getAcceptUser().getId());
			}
			delegateTask.addCandidateGroups(groupList);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
