package com.biolims.workflow.listener;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.sample.model.SampleReceive;
import com.biolims.sample.service.SampleReceiveService;

public class UserTaskSampleReceiveQCUserGroupCreateEventListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext.getServletContext());
		SampleReceiveService sampleReceiveService = (SampleReceiveService) ctx.getBean("sampleReceiveService");
		List<String> groupList = new ArrayList<String>();
		try {
			String businessKey = delegateTask.getExecution().getProcessBusinessKey();
			SampleReceive sct = sampleReceiveService.get(businessKey);
			if (!"".equals(sct.getQualityCheckUser().getId()) && sct.getQualityCheckUser().getId() != null) {
				delegateTask.addCandidateUser(sct.getQualityCheckUser().getId());
			}
			sampleReceiveService.createSampleOrderBatch(sct);
			// if (sct.getUserGroup() != null) {
			// groupList.add(sct.getUserGroup().getId());
			// }
			// delegateTask.addCandidateGroups(groupList);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
