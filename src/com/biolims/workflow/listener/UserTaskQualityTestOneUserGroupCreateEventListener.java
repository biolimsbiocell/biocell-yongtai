package com.biolims.workflow.listener;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.quality.model.QualityTest;
import com.biolims.experiment.quality.service.QualityTestService;

public class UserTaskQualityTestOneUserGroupCreateEventListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext.getServletContext());
		QualityTestService qualityTestService = (QualityTestService) ctx.getBean("qualityTestService");
		List<String> groupList = new ArrayList<String>();
		try {
			String businessKey = delegateTask.getExecution().getProcessBusinessKey();
			QualityTest sct = qualityTestService.get(businessKey);
			if (!"".equals(sct.getConfirmUserOne().getId()) && sct.getConfirmUserOne().getId() != null) {
				String[] userids = sct.getConfirmUserOne().getId().split(",");
				for(String userid:userids) {
					delegateTask.addCandidateUser(userid);
				}
			}
//			if (!"".equals(sct.getConfirmUserOne().getId()) && sct.getConfirmUserOne().getId() != null) {
//				delegateTask.addCandidateUser(sct.getConfirmUserOne().getId());
//			}
			// if (sct.getUserGroup() != null) {
			// groupList.add(sct.getUserGroup().getId());
			// }
			// delegateTask.addCandidateGroups(groupList);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
