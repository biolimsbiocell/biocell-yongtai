package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTask;
import com.biolims.experiment.sj.wklifeblend.service.WkLifeBlendTaskService;

/**
 * 文库审核审核监听
 * 
 * @author
 * 
 */
public class UserTaskWkLifeBlendSHListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		// List<String> groupList = new ArrayList<String>();
		WkLifeBlendTaskService wkLifeBlendTaskService = (WkLifeBlendTaskService) ctx
				.getBean("wkLifeBlendTaskService");
		try {
			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			WkLifeBlendTask sct = wkLifeBlendTaskService.get(businessKey);
			if (sct.getConfirmUser() != null) {
				delegateTask.addCandidateUser(sct.getConfirmUser().getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
