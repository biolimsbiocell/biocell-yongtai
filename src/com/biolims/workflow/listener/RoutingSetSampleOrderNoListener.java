package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.service.CommonService;
import com.biolims.sample.model.SampleOrder;
import com.biolims.system.sample.service.SampleOrderMainService;

public class RoutingSetSampleOrderNoListener implements ExecutionListener {

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		// 得到业务键
		String businessKey = execution.getProcessBusinessKey();
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		SampleOrderMainService sampleOrderMainService = (SampleOrderMainService) ctx
				.getBean("sampleOrderMainService");
		CommonService commonService = (CommonService) ctx
				.getBean("commonService");
		SampleOrder sct = sampleOrderMainService.get(businessKey);
		sct.setState("20");
		sct.setStateName("提交人修改");
		commonService.saveOrUpdate(sct);
	}
}
