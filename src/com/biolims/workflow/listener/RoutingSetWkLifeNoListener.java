package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.service.CommonService;
import com.biolims.experiment.wkLife.model.WkLifeTask;
import com.biolims.experiment.wkLife.service.WKLifeSampleTaskService;

public class RoutingSetWkLifeNoListener implements ExecutionListener {

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		// 得到业务键
		String businessKey = execution.getProcessBusinessKey();
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		WKLifeSampleTaskService wkLifeSampleTaskService = (WKLifeSampleTaskService) ctx
				.getBean("WKLifeSampleTaskService");
		CommonService commonService = (CommonService) ctx
				.getBean("commonService");
		WkLifeTask sct = wkLifeSampleTaskService.get(businessKey);
		sct.setState(com.biolims.workflow.WorkflowConstants.DIC_STATE_WORKFLOW_IN_PROCESS_EDIT);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_MODIFY_NAME);
		commonService.saveOrUpdate(sct);
//		List<WkLifeTaskItem> ptiList = wkLifeSampleTaskService
//				.findWkTaskItemList(businessKey);
//		for (WkTaskItem pti : ptiList) {
//			if (pti.getCode() != null && !pti.getCode().equals("")
//					&& !pti.getCode().contains("ZKP")
//					&& pti.getTempId() != null) {
//				WkTaskTemp ptt = commonService.get(WkTaskTemp.class,
//						pti.getTempId());
//				if (ptt != null && !ptt.equals("")) {
//
//					ptt.setState("1");
//					commonService.saveOrUpdate(ptt);
//				}
//			}
//
//		}

	}
}
