package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.sequencing.model.SequencingTask;
import com.biolims.experiment.sequencing.service.SequencingTaskService;

/**
 * 核酸提取审核监听
 * 
 * @author
 * 
 */
public class UserTaskSequencingSHListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		// List<String> groupList = new ArrayList<String>();
		SequencingTaskService sequencingService = (SequencingTaskService) ctx
				.getBean("sequencingTaskService");
		try {
			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			SequencingTask sct = sequencingService.get(businessKey);
			if (sct.getConfirmUser() != null) {
				delegateTask.addCandidateUser(sct.getConfirmUser().getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
