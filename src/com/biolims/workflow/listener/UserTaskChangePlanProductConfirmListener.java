package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.changeplan.model.ChangePlan;
import com.biolims.experiment.changeplan.service.ChangePlanService;
import com.biolims.sample.model.SampleReceive;
import com.biolims.sample.service.SampleReceiveService;

/**
 * 变更申请QA审核
 * 
 * @author
 * 
 */
public class UserTaskChangePlanProductConfirmListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		ChangePlanService changePlanService = (ChangePlanService) ctx
				.getBean("changePlanService");
		try {
			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			ChangePlan sct = changePlanService.get(businessKey);
			if (sct.getProductConfirmUser() != null) {
				delegateTask.addCandidateUser(sct.getProductConfirmUser().getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
