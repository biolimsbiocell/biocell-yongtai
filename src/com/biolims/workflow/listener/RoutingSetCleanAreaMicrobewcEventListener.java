package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

import com.biolims.experiment.enmonitor.dust.custom.DustParticleEvent;
import com.biolims.experiment.enmonitor.microbe.custom.SettlingMicrobeEvent;
import com.biolims.experiment.wk.custom.WkTaskEvent;

/**
 * 洁净区浮游菌测试记录完成方法
 * @author niyi
 *
 */
public class RoutingSetCleanAreaMicrobewcEventListener implements ExecutionListener {

	public void notify(DelegateExecution execution) throws Exception {

		String businessKey = execution.getProcessBusinessKey();
		SettlingMicrobeEvent dtwf = new SettlingMicrobeEvent();
		dtwf.operation("", businessKey);

	}

}
