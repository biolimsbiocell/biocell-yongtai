package com.biolims.workflow.listener;


import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.service.CommonService;
import com.biolims.equipment.fault.service.InstrumentFaultService;
import com.biolims.equipment.model.InstrumentFault;

public class RoutingSetInstrumentFaultNoListener implements
		ExecutionListener {

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		// 得到业务键
		String businessKey = execution.getProcessBusinessKey();
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		InstrumentFaultService instrumentFaultService = (InstrumentFaultService) ctx
				.getBean("instrumentFaultService");
		CommonService commonService = (CommonService) ctx
				.getBean("commonService");
		InstrumentFault insf = instrumentFaultService.getInstrumentFault(businessKey);
		insf.setState(com.biolims.workflow.WorkflowConstants.DIC_STATE_WORKFLOW_IN_PROCESS_EDIT);
		insf.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_MODIFY_NAME);
		// SampleInfoMain sc = commonService.get(SampleInfoMain.class, sct
		// .getSampleInfoMain().getId());
		// sc.setState("1");
		// commonService.saveOrUpdate(sc);
		commonService.saveOrUpdate(insf);
	}
}
