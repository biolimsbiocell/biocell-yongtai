package com.biolims.workflow.listener;

import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.service.CommonService;
import com.biolims.experiment.qc.model.Qc2100Task;
import com.biolims.experiment.qc.model.Qc2100TaskItem;
import com.biolims.experiment.qc.model.Qc2100TaskTemp;
import com.biolims.experiment.qc.service.WKQualitySampleTaskService;

public class RoutingSetQc2100NoListener implements ExecutionListener {

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		// 得到业务键
		String businessKey = execution.getProcessBusinessKey();
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		WKQualitySampleTaskService wkQualitySampleTaskService = (WKQualitySampleTaskService) ctx
				.getBean("WKQualitySampleTaskService");
		CommonService commonService = (CommonService) ctx
				.getBean("commonService");
		Qc2100Task sct = wkQualitySampleTaskService.get(businessKey);
		sct.setState(com.biolims.workflow.WorkflowConstants.DIC_STATE_WORKFLOW_IN_PROCESS_EDIT);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_MODIFY_NAME);
		List<Qc2100TaskItem> ptiList = wkQualitySampleTaskService
				.findQc2100ItemList(businessKey);
		for (Qc2100TaskItem pti : ptiList) {
			Qc2100TaskTemp ptt = commonService.get(Qc2100TaskTemp.class,
					pti.getTempId());
			ptt.setState("1");
		}
		commonService.saveOrUpdate(sct);
	}
}
