package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

import com.biolims.experiment.enmonitor.dust.custom.DustParticleEvent;
import com.biolims.experiment.wk.custom.WkTaskEvent;

/**
 * 洁净区尘埃粒子测试记录工作流完成
 * @author niyi
 *
 */
public class RoutingSetCleanAreaDustwcEventListener implements ExecutionListener {

	public void notify(DelegateExecution execution) throws Exception {

		String businessKey = execution.getProcessBusinessKey();
		DustParticleEvent dtwf = new DustParticleEvent();
		dtwf.operation("", businessKey);

	}

}
