package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

import com.biolims.equipment.fault.custom.InstrumentFaultSetInstrumentEvent;

/**
 * 路由中，
 * @author niyi
 *
 */
public class RoutingSetInstrumentFaultWcEventListener implements ExecutionListener {

	public void notify(DelegateExecution execution) throws Exception {

		String businessKey = execution.getProcessBusinessKey();
		InstrumentFaultSetInstrumentEvent ifs = new InstrumentFaultSetInstrumentEvent();
		ifs.operation("", businessKey);

	}

}
