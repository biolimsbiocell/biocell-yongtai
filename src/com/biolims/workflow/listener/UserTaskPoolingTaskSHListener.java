package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.pooling.model.Pooling;
import com.biolims.experiment.pooling.service.PoolingService;

/**
 * 富集审核监听
 * 
 * @author
 * 
 */
public class UserTaskPoolingTaskSHListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		// List<String> groupList = new ArrayList<String>();
		PoolingService poolingService = (PoolingService) ctx
				.getBean("poolingService");
		try {
			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			Pooling sct = poolingService.get(businessKey);
			if (sct.getConfirmUser() != null) {
				delegateTask.addCandidateUser(sct.getConfirmUser().getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
