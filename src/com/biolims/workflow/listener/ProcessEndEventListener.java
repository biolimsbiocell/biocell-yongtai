package com.biolims.workflow.listener;

import java.util.Date;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.workflow.WorkflowConstants;
import com.biolims.workflow.entity.WorkflowProcesssInstanceEntity;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

/**
 * 流程实例Ent事件监听器
 * @author cong
 *
 */
public class ProcessEndEventListener implements ExecutionListener {

	public void notify(DelegateExecution execution) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		WorkflowProcessInstanceService workflowProcessInstanceService = (WorkflowProcessInstanceService) ctx
				.getBean("workflowProcessInstanceService");

		WorkflowProcesssInstanceEntity wpie = workflowProcessInstanceService
				.getWorkflowProcesssInstanceEntityByInstanceId(execution.getProcessInstanceId());
		wpie.setEndDate(new Date());
		wpie.setIsEnd(WorkflowConstants.WORKFLOW_COMPLETE);
		workflowProcessInstanceService.modify(wpie);

	}

}
