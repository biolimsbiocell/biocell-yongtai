package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.wk.model.WkTask;
import com.biolims.experiment.wk.service.WkTaskService;

/**
 * 文库实验组监听器
 * 
 * @author
 * 
 */
public class UserTaskWkUserGroupCreateEventListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		WkTaskService wkTaskService = (WkTaskService) ctx
				.getBean("wkTaskService");
		try {
			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			WkTask sct = wkTaskService.get(businessKey);
			if (!"".equals(sct.getTestUserOneId())) {
				String[] uidOne = sct.getTestUserOneId().split(",");
				for (int i = 0; i < uidOne.length; i++) {
					delegateTask.addCandidateUser(uidOne[i]);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
