package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

import com.biolims.experiment.changeplan.custom.ChangePlanEvent;



/**
 * 变更申请完成
 * @author 
 *
 */
public class RoutingSetChangePlanwcEventListener implements ExecutionListener {

	public void notify(DelegateExecution execution) throws Exception {

		String businessKey = execution.getProcessBusinessKey();
		ChangePlanEvent dtwf = new ChangePlanEvent();
		dtwf.operation("", businessKey);

	}

}
