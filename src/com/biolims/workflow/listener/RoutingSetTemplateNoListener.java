package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.service.CommonService;
import com.biolims.experiment.enmonitor.dust.model.CleanAreaDust;
import com.biolims.experiment.enmonitor.dust.model.DustParticle;
import com.biolims.experiment.enmonitor.dust.service.DustParticleService;
import com.biolims.experiment.enmonitor.microbe.model.CleanAreaMicrobe;
import com.biolims.experiment.enmonitor.microbe.model.SettlingMicrobe;
import com.biolims.experiment.enmonitor.microbe.service.SettlingMicrobeService;
import com.biolims.experiment.enmonitor.volume.model.CleanAreaVolume;
import com.biolims.experiment.enmonitor.volume.service.CleanAreaVolumeService;
import com.biolims.sample.model.SampleOrder;
import com.biolims.system.sample.service.SampleOrderMainService;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.service.TemplateService;

public class RoutingSetTemplateNoListener implements ExecutionListener {

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		// 得到业务键
		String businessKey = execution.getProcessBusinessKey();
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		TemplateService templateService = (TemplateService) ctx
				.getBean("templateService");
		CommonService commonService = (CommonService) ctx
				.getBean("commonService");
		Template cad = templateService.get(businessKey);
		cad.setState("20");
		cad.setStateName("提交人修改");
		commonService.saveOrUpdate(cad);
	}
}
