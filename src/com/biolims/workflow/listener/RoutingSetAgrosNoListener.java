package com.biolims.workflow.listener;

import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.service.CommonService;
import com.biolims.experiment.qc.agros.model.AgrosTask;
import com.biolims.experiment.qc.agros.model.AgrosTaskItem;
import com.biolims.experiment.qc.agros.model.AgrosTaskTemp;
import com.biolims.experiment.qc.agros.service.AgrosTaskService;

public class RoutingSetAgrosNoListener implements ExecutionListener {

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		// 得到业务键
		String businessKey = execution.getProcessBusinessKey();
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		AgrosTaskService agrosTaskService = (AgrosTaskService) ctx
				.getBean("agrosTaskService");
		CommonService commonService = (CommonService) ctx
				.getBean("commonService");
		AgrosTask sct = agrosTaskService.get(businessKey);
		sct.setState(com.biolims.workflow.WorkflowConstants.DIC_STATE_WORKFLOW_IN_PROCESS_EDIT);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_MODIFY_NAME);
		List<AgrosTaskItem> ptiList = agrosTaskService.findDnaItemList(businessKey);
		for (AgrosTaskItem pti : ptiList) {
			AgrosTaskTemp dt = commonService
					.get(AgrosTaskTemp.class, pti.getTempId());
			if (dt != null) {
				dt.setState("1");
			}
		}
		commonService.saveOrUpdate(sct);
	}
}
