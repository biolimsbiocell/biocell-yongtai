package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.activiti.engine.repository.ProcessDefinition;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.applicationType.model.ApplicationTypeTable;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.util.BeanUtils;
import com.biolims.workflow.entity.WorkflowBindForm;
import com.biolims.workflow.service.WorkflowService;

/**
 * 样本检测任务CREATE事件监听器
 * @author
 *
 */
public class UserTaskTableSetUserCreateEventListener implements TaskListener {
	private org.activiti.engine.impl.el.FixedValue tableId;

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		CommonService commonService = (CommonService) ctx.getBean("commonService");

		try {
			WorkflowService workflowService = (WorkflowService) ctx.getBean("workflowService");
			String businessKey = delegateTask.getExecution().getProcessBusinessKey();
			String definitionId = delegateTask.getExecution().getProcessDefinitionId();
			ProcessDefinition pd = workflowService.getRepositoryService().createProcessDefinitionQuery()
					.processDefinitionId(definitionId).singleResult();
			WorkflowBindForm form = workflowService.getWorkflowBindFormByDefinitionKey(pd.getKey());
			ApplicationTypeTable att = workflowService.get(ApplicationTypeTable.class, form.getFormName());

			Class<?> objClass = Class.forName(att.getClassPath());
			Object b = commonService.get(objClass, businessKey);
			User sct = (User) BeanUtils.getFieldValue(b, "acceptUser");
			if (sct != null) {

				delegateTask.addCandidateUser(sct.getId());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
