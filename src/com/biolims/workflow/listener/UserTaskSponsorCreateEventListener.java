package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.workflow.entity.WorkflowProcesssInstanceEntity;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

/**
 * 设置办理人为任务发起人
 * @author cong
 */
public class UserTaskSponsorCreateEventListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		WorkflowProcessInstanceService instanceService = (WorkflowProcessInstanceService) ctx
				.getBean("workflowProcessInstanceService");
		String processInstanceId = delegateTask.getProcessInstanceId();
		try {
			WorkflowProcesssInstanceEntity wpe = instanceService
					.getWorkflowProcesssInstanceEntityByInstanceId(processInstanceId);
			delegateTask.setAssignee(wpe.getApplUserId());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
