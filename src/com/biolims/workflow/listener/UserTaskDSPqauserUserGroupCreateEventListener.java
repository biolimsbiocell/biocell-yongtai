package com.biolims.workflow.listener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.deviation.plan.model.DeviationSurveyPlan;
import com.biolims.deviation.plan.service.DeviationSurveyPlanService;

public class UserTaskDSPqauserUserGroupCreateEventListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext.getServletContext());
		DeviationSurveyPlanService deviationSurveyPlanService = (DeviationSurveyPlanService) ctx
				.getBean("deviationSurveyPlanService");
		List<String> groupList = new ArrayList<String>();
		try {
			String businessKey = delegateTask.getExecution().getProcessBusinessKey();
			DeviationSurveyPlan sct = deviationSurveyPlanService.get(businessKey);
			//Qa
			if (!"".equals(sct.getAuditQaUser().getId()) && sct.getAuditQaUser().getId() != null) {
				delegateTask.addCandidateUser(sct.getAuditQaUser().getId());
				 SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			      String date =sdf.format(new Date());
			       Date auditQaDate =sdf.parse(date);
				  sct.setAuditQaDate(auditQaDate);
				  deviationSurveyPlanService.saveD(sct);
			}
			// if (sct.getUserGroup() != null) {
			// groupList.add(sct.getUserGroup().getId());
			// }
			// delegateTask.addCandidateGroups(groupList);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
