package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

import com.biolims.experiment.enmonitor.bacteria.custom.CleanAreaBacteriaEvent;
import com.biolims.experiment.enmonitor.dust.custom.DustParticleEvent;
import com.biolims.experiment.enmonitor.microorganism.custom.CleanAreaMicroorganismEvent;
import com.biolims.experiment.enmonitor.volume.custom.CleanAreaVolumeEvent;
import com.biolims.experiment.wk.custom.WkTaskEvent;

/**
 *  表面微生物监测记录工作流完成
 * @author 
 *
 */
public class RoutingSetCleanAreaMicroorganismEventListener implements ExecutionListener {

	public void notify(DelegateExecution execution) throws Exception {

		String businessKey = execution.getProcessBusinessKey();
		CleanAreaMicroorganismEvent dtwf = new CleanAreaMicroorganismEvent();
		dtwf.operation("", businessKey);

	}

}
