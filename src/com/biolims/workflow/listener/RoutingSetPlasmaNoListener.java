package com.biolims.workflow.listener;

import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.service.CommonService;
import com.biolims.experiment.plasma.model.PlasmaTask;
import com.biolims.experiment.plasma.model.PlasmaTaskItem;
import com.biolims.experiment.plasma.model.PlasmaTaskTemp;
import com.biolims.experiment.plasma.service.BloodSplitService;

public class RoutingSetPlasmaNoListener implements ExecutionListener {

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		// 得到业务键
		String businessKey = execution.getProcessBusinessKey();
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		BloodSplitService bloodSplitService = (BloodSplitService) ctx
				.getBean("bloodSplitService");
		CommonService commonService = (CommonService) ctx
				.getBean("commonService");
		PlasmaTask sct = bloodSplitService.get(businessKey);
		sct.setState(com.biolims.workflow.WorkflowConstants.DIC_STATE_WORKFLOW_IN_PROCESS_EDIT);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_MODIFY_NAME);
		List<PlasmaTaskItem> ptiList = bloodSplitService
				.findPlasmaItemList(businessKey);
		for (PlasmaTaskItem pti : ptiList) {
			PlasmaTaskTemp ptt = commonService.get(PlasmaTaskTemp.class,
					pti.getTempId());
			ptt.setState("1");
		}
		commonService.saveOrUpdate(sct);
	}
}
