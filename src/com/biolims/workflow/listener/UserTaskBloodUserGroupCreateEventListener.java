package com.biolims.workflow.listener;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.plasma.model.PlasmaTask;
import com.biolims.experiment.plasma.service.BloodSplitService;

/**
 * 样本处理实验监听器
 * 
 * @author
 * 
 */
public class UserTaskBloodUserGroupCreateEventListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		List<String> groupList = new ArrayList<String>();
		BloodSplitService bloodSplitService = (BloodSplitService) ctx
				.getBean("bloodSplitService");
		try {
			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			PlasmaTask sct = bloodSplitService.get(businessKey);

			if (!"".equals(sct.getTestUserOneId()) && sct.getTestUserOneId()!=null) {
				String[] uidOne = sct.getTestUserOneId().split(",");
				for (int i = 0; i < uidOne.length; i++) {
					delegateTask.addCandidateUser(uidOne[i]);
				}
			}

			// delegateTask.addCandidateUsers(groupList);

			// else {
			// if (sct.getAcceptUser() != null) {
			// groupList.add(sct.getAcceptUser().getId());
			// }
			// delegateTask.addCandidateGroups(groupList);
			// }

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
