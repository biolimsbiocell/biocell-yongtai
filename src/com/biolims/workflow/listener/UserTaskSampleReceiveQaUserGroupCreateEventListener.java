package com.biolims.workflow.listener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.core.model.user.UserGroupUser;
import com.biolims.remind.model.SysRemind;
import com.biolims.sample.model.SampleReceive;
import com.biolims.sample.service.SampleReceiveService;

public class UserTaskSampleReceiveQaUserGroupCreateEventListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext.getServletContext());
		SampleReceiveService sampleReceiveService = (SampleReceiveService) ctx.getBean("sampleReceiveService");
		List<String> groupList = new ArrayList<String>();
		try {
			String businessKey = delegateTask.getExecution().getProcessBusinessKey();
			SampleReceive sct = sampleReceiveService.get(businessKey);
			if (!"".equals(sct.getApprovalUser().getId()) && sct.getApprovalUser().getId() != null) {
				delegateTask.addCandidateUser(sct.getApprovalUser().getId());
			}
			sampleReceiveService.tongzhi(businessKey);
			// if (sct.getUserGroup() != null) {
			// groupList.add(sct.getUserGroup().getId());
			// }
			// delegateTask.addCandidateGroups(groupList);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
