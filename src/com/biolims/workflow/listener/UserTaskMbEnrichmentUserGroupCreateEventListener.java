package com.biolims.workflow.listener;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.sj.mbenrichment.model.MbEnrichment;
import com.biolims.experiment.sj.mbenrichment.service.MbEnrichmentService;





/**
 * 文库实验组监听器
 * 
 * @author
 * 
 */
public class UserTaskMbEnrichmentUserGroupCreateEventListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		List<String> groupList = new ArrayList<String>();
		MbEnrichmentService mbEnrichmentService = (MbEnrichmentService) ctx
				.getBean("mbEnrichmentService");
		try {
			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			MbEnrichment sct = mbEnrichmentService.get(businessKey);
			if (!"".equals(sct.getTestUserOneId())) {
				String[] uidOne = sct.getTestUserOneId().split(",");
				for (int i = 0; i < uidOne.length; i++) {
					delegateTask.addCandidateUser(uidOne[i]);
				}
			}
//			if (!"".equals(sct.getTestUserTwoId())) {
//				String[] uidTwo = sct.getTestUserTwoId().split(",");
//				for (int i = 0; i < uidTwo.length; i++) {
//					delegateTask.addCandidateUser(uidTwo[i]);
//				}
//			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
