package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.check.model.TechCheckServiceTask;
import com.biolims.experiment.check.service.TechCheckServiceTaskService;

/**
 * 核酸质检审核监听
 * 
 * @author
 * 
 */
public class UserTaskCheckTaskSHListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		// List<String> groupList = new ArrayList<String>();
		TechCheckServiceTaskService techCheckServiceTaskService = (TechCheckServiceTaskService) ctx
				.getBean("techCheckServiceTaskService");
		try {
			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			TechCheckServiceTask sct = techCheckServiceTaskService
					.get(businessKey);
			if (sct.getConfirmUser() != null) {
				delegateTask.addCandidateUser(sct.getConfirmUser().getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
