package com.biolims.workflow.listener;

import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.service.CommonService;
import com.biolims.experiment.pooling.model.Pooling;
import com.biolims.experiment.pooling.model.PoolingItem;
import com.biolims.experiment.pooling.model.PoolingTemp;
import com.biolims.experiment.pooling.service.PoolingService;

public class RoutingSetPoolingXdListener implements ExecutionListener {

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		//得到业务键
		String businessKey = execution.getProcessBusinessKey();
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext.getServletContext());
		PoolingService poolingService =(PoolingService) ctx.getBean("poolingService");
		CommonService commonService = (CommonService) ctx.getBean("commonService");
		Pooling sct = poolingService.get(businessKey);
		sct.setState(com.biolims.workflow.WorkflowConstants.DIC_STATE_WORKFLOW_IN_PROCESS_EDIT);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.DIC_STATE_WORKFLOW_IN_PROCESS_NAME);
		List<PoolingItem> ptiList = poolingService.findPoolingItemList(businessKey);
		for(PoolingItem pti:ptiList){
			PoolingTemp ptt = commonService.get(PoolingTemp.class, pti.getTempId());
			ptt.setState("2");
		}
		commonService.saveOrUpdate(sct);
	}
}
