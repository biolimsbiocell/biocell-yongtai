package com.biolims.workflow.listener;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.massarray.model.MassarrayTask;
import com.biolims.experiment.massarray.service.MassarrayTaskService;

/**
 * 超声破碎实验组监听器
 * 
 * @author
 * 
 */
public class UserTaskMassUserGroupCreateEventListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		List<String> groupList = new ArrayList<String>();
		MassarrayTaskService qtTaskService = (MassarrayTaskService) ctx
				.getBean("massarrayTaskService");
		try {
			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			MassarrayTask sct = qtTaskService.get(businessKey);
			if (!"".equals(sct.getTestUserOneId())) {
				String[] uidOne = sct.getTestUserOneId().split(",");
				for (int i = 0; i < uidOne.length; i++) {
					delegateTask.addCandidateUser(uidOne[i]);
				}
			}
			delegateTask.addCandidateGroups(groupList);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
