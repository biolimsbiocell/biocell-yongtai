package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.enmonitor.bacteria.model.CleanAreaBacteria;
import com.biolims.experiment.enmonitor.bacteria.service.CleanAreaBacteriaService;

/**
 * 洁净区沉降菌测试记录审核监听
 * 
 * @author
 * 
 */
public class UserTaskCleanAreaBacteriaSHListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext.getServletContext());
		CleanAreaBacteriaService cleanAreaBacteriaService = (CleanAreaBacteriaService) ctx.getBean("cleanAreaBacteriaService");
		try {
			String businessKey = delegateTask.getExecution().getProcessBusinessKey();
			CleanAreaBacteria sct = cleanAreaBacteriaService.gets(businessKey);
			if (sct.getConfirmUser() != null) {
				delegateTask.addCandidateUser(sct.getConfirmUser().getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}


}
