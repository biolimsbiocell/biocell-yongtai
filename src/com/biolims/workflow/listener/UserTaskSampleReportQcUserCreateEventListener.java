package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.service.CommonService;
import com.biolims.report.model.SampleReport;

/**
 * 样本检测任务CREATE事件监听器
 * 
 * @author
 * 
 */
public class UserTaskSampleReportQcUserCreateEventListener implements
		TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		CommonService commonService = (CommonService) ctx
				.getBean("commonService");
		try {

			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			SampleReport sct = commonService.get(SampleReport.class,
					businessKey);

			if (sct.getAcceptUser() != null) {

				delegateTask.setAssignee(sct.getQcUser().getId());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
