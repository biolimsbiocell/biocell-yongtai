package com.biolims.workflow.listener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.workflow.entity.WorkflowProcessDefinitionTaskGroupConfigEntity;
import com.biolims.workflow.entity.WorkflowProcessDefinitionTaskUserConfigEntity;
import com.biolims.workflow.entity.WorkflowProxyUserSiteEntity;
import com.biolims.workflow.service.WorkflowProcessDefinitionService;

/**
 * 用户任务CREATE事件监听器
 * @author cong
 *
 */
public class UserTaskCreateEventListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		WorkflowProcessDefinitionService definitionService = (WorkflowProcessDefinitionService) ctx
				.getBean("workflowProcessDefinitionService");

		String definitionId = delegateTask.getExecution().getProcessDefinitionId();
		String activitiId = delegateTask.getExecution().getCurrentActivityId();
		try {

			List<WorkflowProcessDefinitionTaskUserConfigEntity> tuceList = definitionService
					.findWorkflowProcessDefinitionTaskUserConfigEntityByKey(definitionId, activitiId);
			if (tuceList != null && tuceList.size() > 0) {
				Map<String, String> proxyUserMap = parseProxyUser(definitionService
						.findWorkflowProxyUserSiteEntityList("1"));
				List<String> userList = new ArrayList<String>();
				for (WorkflowProcessDefinitionTaskUserConfigEntity tuce : tuceList) {
					String proxyUser = proxyUserMap.get(tuce.getUserId());
					if (proxyUser == null) {
						userList.add(tuce.getUserId());
					} else {
						delegateTask.setOwner(tuce.getUserId());
						userList.add(proxyUser);
					}
				}
				delegateTask.addCandidateUsers(userList);
			}

			List<WorkflowProcessDefinitionTaskGroupConfigEntity> tgceList = definitionService
					.findWorkflowProcessDefinitionTaskGroupConfigEntityByKey(definitionId, activitiId);
			if (tgceList != null && tgceList.size() > 0) {
				List<String> groupList = new ArrayList<String>();
				for (WorkflowProcessDefinitionTaskGroupConfigEntity tgce : tgceList) {
					groupList.add(tgce.getGroupId());
				}
				delegateTask.addCandidateGroups(groupList);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/*
	 * 代办人 
	 */
	private Map<String, String> parseProxyUser(List<WorkflowProxyUserSiteEntity> proxyUserList) throws Exception {
		Map<String, String> result = new HashMap<String, String>();
		if (proxyUserList != null && proxyUserList.size() > 0) {
			for (WorkflowProxyUserSiteEntity proxyUser : proxyUserList) {
				result.put(proxyUser.getAssignee(), proxyUser.getProxyUserId());
			}
		}
		return result;
	}

}
