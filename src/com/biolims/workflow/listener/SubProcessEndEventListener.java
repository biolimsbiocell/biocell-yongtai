package com.biolims.workflow.listener;

import java.util.Date;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

import com.biolims.workflow.WorkflowConstants;
import com.biolims.workflow.entity.WorkflowProcesssInstanceEntity;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

public class SubProcessEndEventListener implements ExecutionListener {

	public void notify(DelegateExecution execution) throws Exception {
		WebApplicationContext ctx = ContextLoader.getCurrentWebApplicationContext();
		WorkflowProcessInstanceService workflowProcessInstanceService = (WorkflowProcessInstanceService) ctx
				.getBean("workflowProcessInstanceService");

		WorkflowProcesssInstanceEntity wpie = workflowProcessInstanceService
				.getWorkflowProcesssInstanceEntityByInstanceId(execution.getId());
		wpie.setEndDate(new Date());
		wpie.setIsEnd(WorkflowConstants.WORKFLOW_COMPLETE);
		workflowProcessInstanceService.modify(wpie);

	}

}
