package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

import com.biolims.experiment.quality.custom.QualityTestEvent;

public class RoutingSetQTwcEventListener implements ExecutionListener {

	public void notify(DelegateExecution execution) throws Exception {

		String businessKey = execution.getProcessBusinessKey();
		QualityTestEvent dtwf = new QualityTestEvent();
		dtwf.operation("", businessKey);

	}
}
