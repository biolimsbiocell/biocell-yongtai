package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.pooling.model.Pooling;
import com.biolims.experiment.pooling.service.PoolingService;

/**
 * Pooling实验组监听器
 * 
 * @author
 * 
 */
public class UserTaskPoolingUserGroupCreateEventListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext.getServletContext());
		// List<String> groupList = new ArrayList<String>();
		PoolingService poolingService = (PoolingService) ctx.getBean("poolingService");
		try {
			String businessKey = delegateTask.getExecution().getProcessBusinessKey();
			Pooling sct = poolingService.get(businessKey);
			if (!"".equals(sct.getTestUserOneId())) {
				String[] uidOne = sct.getTestUserOneId().split(",");
				for (int i = 0; i < uidOne.length; i++) {
					delegateTask.addCandidateUser(uidOne[i]);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
