package com.biolims.workflow.listener;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.deviation.model.DeviationHandlingReport;
import com.biolims.deviation.service.DeviationHandlingReportService;

public class UserTaskDHRmtUserGroupCreateEventListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext.getServletContext());
		DeviationHandlingReportService deviationHandlingReportService = (DeviationHandlingReportService) ctx
				.getBean("deviationHandlingReportService");
		List<String> groupList = new ArrayList<String>();
		try {
			String businessKey = delegateTask.getExecution().getProcessBusinessKey();
			DeviationHandlingReport sct = deviationHandlingReportService.get(businessKey);
			if (!"".equals(sct.getMonitor().getId()) && sct.getMonitor().getId() != null) {
				delegateTask.addCandidateUser(sct.getMonitor().getId());
			}
			// if (sct.getUserGroup() != null) {
			// groupList.add(sct.getUserGroup().getId());
			// }
			// delegateTask.addCandidateGroups(groupList);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
