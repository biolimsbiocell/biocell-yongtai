package com.biolims.workflow.listener;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.sequencingLife.model.SequencingLifeTask;
import com.biolims.experiment.sequencingLife.service.SequencingLifeService;



/**
 * 上机测序实验组监听器
 * 
 * @author
 * 
 */
public class UserTaskSeqLifeUserGroupCreateEventListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		List<String> groupList = new ArrayList<String>();
		SequencingLifeService sequencingLifeService = (SequencingLifeService) ctx
				.getBean("sequencingLifeService");
		try {
			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			SequencingLifeTask sct = sequencingLifeService.get(businessKey);
			if (!"".equals(sct.getTestUserOneId())) {
				String[] uidOne = sct.getTestUserOneId().split(",");
				for (int i = 0; i < uidOne.length; i++) {
					delegateTask.addCandidateUser(uidOne[i]);
				}
			}
			if (!"".equals(sct.getTestUserTwoId())) {
				String[] uidTwo = sct.getTestUserTwoId().split(",");
				for (int i = 0; i < uidTwo.length; i++) {
					delegateTask.addCandidateUser(uidTwo[i]);
				}
			}
			// if (sct.getReciveUser() != null) {
			// delegateTask.addCandidateUser(sct.getReciveUser().getId());
			// } else {
			// if (sct.getAcceptUser() != null) {
			// groupList.add(sct.getAcceptUser().getId());
			// }
			// delegateTask.addCandidateGroups(groupList);
			// }
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
