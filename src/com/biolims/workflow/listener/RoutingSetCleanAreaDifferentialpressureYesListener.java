package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.service.CommonService;
import com.biolims.deviation.model.DeviationHandlingReport;
import com.biolims.deviation.service.DeviationHandlingReportService;
import com.biolims.experiment.enmonitor.differentialpressure.model.CleanAreaDiffPressure;
import com.biolims.experiment.enmonitor.differentialpressure.service.CleanAreaDiffPressureService;
import com.biolims.experiment.enmonitor.dust.model.CleanAreaDust;
import com.biolims.experiment.enmonitor.dust.service.DustParticleService;

public class RoutingSetCleanAreaDifferentialpressureYesListener implements ExecutionListener {

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		// 得到业务键
		String businessKey = execution.getProcessBusinessKey();
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext.getServletContext());
		CleanAreaDiffPressureService cleanAreaDiffPressureService = (CleanAreaDiffPressureService) ctx
				.getBean("cleanAreaDiffPressureService");
		CommonService commonService = (CommonService) ctx.getBean("commonService");
		CleanAreaDiffPressure sct = cleanAreaDiffPressureService.gets(businessKey);
		sct.setState(com.biolims.workflow.WorkflowConstants.DIC_STATE_WORKFLOW_IN_PROCESS_EDIT);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_APPROVALING_NAME);
		commonService.saveOrUpdate(sct);
	}
}
