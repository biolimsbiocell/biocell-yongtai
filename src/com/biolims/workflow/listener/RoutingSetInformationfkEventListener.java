package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.analysis.data.model.DataTask;
import com.biolims.common.service.CommonService;

/**
 * 路由中，取得金额，金额字段名称应该为fee
 * @author niyi
 *
 */
public class RoutingSetInformationfkEventListener implements ExecutionListener {

	public void notify(DelegateExecution execution) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		String businessKey = execution.getProcessBusinessKey();
		CommonService commonService = (CommonService) ctx.getBean("commonService");
		DataTask sct = commonService.get(DataTask.class, businessKey);
		sct.setState("20");
		sct.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_GDP_COMPLETE_NAME);
		commonService.saveOrUpdate(sct);

	}

}
