package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

import com.biolims.goods.mate.custom.GoodsMaterialsApplyEvent;

/**
 * 申请完成
 * @author niyi
 *
 */
public class RoutingSetGoodsApplywcEventListener implements ExecutionListener {

	public void notify(DelegateExecution execution) throws Exception {

		String businessKey = execution.getProcessBusinessKey();
		GoodsMaterialsApplyEvent dtwf = new GoodsMaterialsApplyEvent();
		dtwf.operation("", businessKey);

	}

}
