package com.biolims.workflow.listener;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.qaAudit.model.QaAudit;
import com.biolims.experiment.qaAudit.service.QaAuditService;

public class UserTaskQaloderUserGroupCreateEventListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext.getServletContext());
		QaAuditService qaAuditService = (QaAuditService) ctx.getBean("qaAuditService");
		List<String> groupList = new ArrayList<String>();
		try {
			String businessKey = delegateTask.getExecution().getProcessBusinessKey();
			QaAudit sct = qaAuditService.get(businessKey);
			if (!"".equals(sct.getGroupLeader().getId()) && sct.getGroupLeader().getId() != null) {
				delegateTask.addCandidateUser(sct.getGroupLeader().getId());
			}
			if (!"".equals(sct.getGroupMemberIds())) {
				String[] uidOne = sct.getGroupMemberIds().split(",");
				for (int i = 0; i < uidOne.length; i++) {
					delegateTask.addCandidateUser(uidOne[i]);
				}
			}
			// if (sct.getUserGroup() != null) {
			// groupList.add(sct.getUserGroup().getId());
			// }
			// delegateTask.addCandidateGroups(groupList);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
