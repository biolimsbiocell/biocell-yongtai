package com.biolims.workflow.listener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.deviation.model.DeviationHandlingReport;
import com.biolims.deviation.service.DeviationHandlingReportService;

public class UserTaskDHRdpUserGroupCreateEventListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext.getServletContext());
		DeviationHandlingReportService deviationHandlingReportService = (DeviationHandlingReportService) ctx
				.getBean("deviationHandlingReportService");
		List<String> groupList = new ArrayList<String>();
		try {
			String businessKey = delegateTask.getExecution().getProcessBusinessKey();
			DeviationHandlingReport sct = deviationHandlingReportService.get(businessKey);
			if (!"".equals(sct.getDepartmentUser().getId()) && sct.getDepartmentUser().getId() != null) {
				delegateTask.addCandidateUser(sct.getDepartmentUser().getId());
				 SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			      String date =sdf.format(new Date());
			       Date monitoringDate =sdf.parse(date);
				  sct.setMonitoringDate(monitoringDate);
				  deviationHandlingReportService.saveD(sct);
			}
			// if (sct.getUserGroup() != null) {
			// groupList.add(sct.getUserGroup().getId());
			// }
			// delegateTask.addCandidateGroups(groupList);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
