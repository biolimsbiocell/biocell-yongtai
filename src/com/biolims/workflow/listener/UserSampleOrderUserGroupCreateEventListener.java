package com.biolims.workflow.listener;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.model.user.User;
import com.biolims.sample.model.SampleOrder;
import com.biolims.system.sample.service.SampleOrderMainService;
/**
     * @ClassName: UserSampleOrderUserGroupCreateEventListener  
     * @Description: TODO  
     * @author 孙灵达  
     * @date 2018年8月21日
 */
public class UserSampleOrderUserGroupCreateEventListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		List<String> groupList = new ArrayList<String>();
		SampleOrderMainService sampleOrderMainService = (SampleOrderMainService) ctx
				.getBean("sampleOrderMainService");
		try {
			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			SampleOrder sct = sampleOrderMainService.get(businessKey);
			if (!"".equals(sct.getConfirmUser().getId())) {
				String[] uidOne = sct.getConfirmUser().getId().split(",");
				for (int i = 0; i < uidOne.length; i++) {
					delegateTask.addCandidateUser(uidOne[i]);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
