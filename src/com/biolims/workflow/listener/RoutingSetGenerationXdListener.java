package com.biolims.workflow.listener;

import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.service.CommonService;
import com.biolims.experiment.generation.model.GenerationTask;
import com.biolims.experiment.generation.model.GenerationTaskItem;
import com.biolims.experiment.generation.model.GenerationTaskTemp;
import com.biolims.experiment.generation.service.GenerationTestService;

public class RoutingSetGenerationXdListener implements ExecutionListener {

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		//得到业务键
		String businessKey = execution.getProcessBusinessKey();
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext.getServletContext());
		GenerationTestService generationTestService =(GenerationTestService) ctx.getBean("generationTestService");
		CommonService commonService = (CommonService) ctx.getBean("commonService");
		GenerationTask sct = generationTestService.get(businessKey);
		sct.setState(com.biolims.workflow.WorkflowConstants.DIC_STATE_WORKFLOW_IN_PROCESS_EDIT);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.DIC_STATE_WORKFLOW_IN_PROCESS_NAME);
		List<GenerationTaskItem> ptiList = generationTestService.findGenerationItemList(businessKey);
		for(GenerationTaskItem pti:ptiList){
			GenerationTaskTemp ptt = commonService.get(GenerationTaskTemp.class, pti.getTempId());
			ptt.setState("2");
		}
		commonService.saveOrUpdate(sct);
	}
}
