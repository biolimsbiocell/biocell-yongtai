package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.activiti.engine.repository.ProcessDefinition;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.applicationType.model.ApplicationTypeTable;
import com.biolims.common.service.CommonService;
import com.biolims.util.BeanUtils;
import com.biolims.workflow.WorkflowConstants;
import com.biolims.workflow.entity.WorkflowBindForm;
import com.biolims.workflow.service.WorkflowService;

/**
 * 路由中，取得金额，金额字段名称应该为fee
 * @author niyi
 *
 */
public class RoutingSetTableSetSendEventListener implements ExecutionListener {

	private org.activiti.engine.impl.el.FixedValue stateName;
	private org.activiti.engine.impl.el.FixedValue state;

	public void notify(DelegateExecution execution) throws Exception {

		String businessKey = execution.getProcessBusinessKey();
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		CommonService commonService = (CommonService) ctx.getBean("commonService");
		WorkflowService workflowService = (WorkflowService) ctx.getBean("workflowService");
		String definitionId = execution.getProcessDefinitionId();
		ProcessDefinition pd = workflowService.getRepositoryService().createProcessDefinitionQuery()
				.processDefinitionId(definitionId).singleResult();
		WorkflowBindForm form = workflowService.getWorkflowBindFormByDefinitionKey(pd.getKey());
		ApplicationTypeTable att = workflowService.get(ApplicationTypeTable.class, form.getFormName());

		Class<?> objClass = Class.forName(att.getClassPath());
		Object b = commonService.get(objClass, businessKey);

		if (state != null)
			BeanUtils.setFieldValue(b, "state", state.getExpressionText());
		else
			BeanUtils.setFieldValue(b, "state", WorkflowConstants.WORKFLOW_MODIFY);
		BeanUtils.setFieldValue(b, "stateName", stateName.getExpressionText());

		//		BeanUtils.setFieldValue(b, "confirmUser", (User) ServletActionContext.getRequest().getSession().getAttribute(
		//				SystemConstants.USER_SESSION_KEY));
		//		BeanUtils.setFieldValue(b, "confirmDate", new Date());
		commonService.saveOrUpdate(b);
	}
}
