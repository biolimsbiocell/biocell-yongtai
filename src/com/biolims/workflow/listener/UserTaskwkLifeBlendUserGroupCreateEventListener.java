package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTask;
import com.biolims.experiment.sj.wklifeblend.service.WkLifeBlendTaskService;



/**
 * 超声破碎实验组监听器
 * 
 * @author
 * 
 */
public class UserTaskwkLifeBlendUserGroupCreateEventListener implements
		TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		// List<String> groupList = new ArrayList<String>();
		WkLifeBlendTaskService wkLifeBlendTaskService = (WkLifeBlendTaskService) ctx
				.getBean("wkLifeBlendTaskService");
		try {
			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			WkLifeBlendTask sct = wkLifeBlendTaskService.get(businessKey);
			if (!"".equals(sct.getTestUserOneId())) {
				String[] uidOne = sct.getTestUserOneId().split(",");
				for (int i = 0; i < uidOne.length; i++) {
					delegateTask.addCandidateUser(uidOne[i]);
				}
			}
			if (!"".equals(sct.getTestUserTwoId())) {
				String[] uidTwo = sct.getTestUserTwoId().split(",");
				for (int i = 0; i < uidTwo.length; i++) {
					delegateTask.addCandidateUser(uidTwo[i]);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
