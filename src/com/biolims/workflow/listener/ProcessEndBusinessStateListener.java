package com.biolims.workflow.listener;

import java.util.Date;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.activiti.engine.repository.ProcessDefinition;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.applicationType.model.ApplicationTypeTable;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.util.BeanUtils;
import com.biolims.workflow.WorkflowConstants;
import com.biolims.workflow.entity.WorkflowBindForm;
import com.biolims.workflow.service.WorkflowService;

/**
 * 流程结束，更改业务state状态为已完成
 * @author cong
 */
public class ProcessEndBusinessStateListener implements ExecutionListener {

	public void notify(DelegateExecution execution) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		WorkflowService workflowService = (WorkflowService) ctx.getBean("workflowService");
		String businessKey = execution.getProcessBusinessKey();
		String definitionId = execution.getProcessDefinitionId();
		ProcessDefinition pd = workflowService.getRepositoryService().createProcessDefinitionQuery()
				.processDefinitionId(definitionId).singleResult();
		WorkflowBindForm form = workflowService.getWorkflowBindFormByDefinitionKey(pd.getKey());
		ApplicationTypeTable att = workflowService.get(ApplicationTypeTable.class, form.getFormName());

		Class<?> objClass = Class.forName(att.getClassPath());
		Object b = workflowService.get(objClass, businessKey);

		try {

			BeanUtils.setFieldValue(b, "state", WorkflowConstants.WORKFLOW_COMPLETE);
			BeanUtils.setFieldValue(b, "stateName", WorkflowConstants.WORKFLOW_COMPLETE_NAME);
			User user = (User) ServletActionContext.getRequest().getSession().getAttribute(
					SystemConstants.USER_SESSION_KEY);

			BeanUtils.setFieldValue(b, "confirmUser", user);
			BeanUtils.setFieldValue(b, "confirmDate", new Date());

		} catch (Exception e) {

		}

		workflowService.save(b);
	}

}
