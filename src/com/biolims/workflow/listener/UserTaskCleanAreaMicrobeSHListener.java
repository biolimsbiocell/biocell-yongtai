package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.dna.model.DnaTask;
import com.biolims.experiment.dna.service.DnaTaskService;
import com.biolims.experiment.enmonitor.dust.model.CleanAreaDust;
import com.biolims.experiment.enmonitor.dust.service.DustParticleService;
import com.biolims.experiment.enmonitor.microbe.model.CleanAreaMicrobe;
import com.biolims.experiment.enmonitor.microbe.service.SettlingMicrobeService;
import com.biolims.experiment.qpcr.model.QpcrTask;
import com.biolims.experiment.qpcr.service.QpcrTaskService;

/**
 * 洁净区浮游菌测试记录审核监听
 * 
 * @author
 * 
 */
public class UserTaskCleanAreaMicrobeSHListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		SettlingMicrobeService settlingMicrobeService = (SettlingMicrobeService) ctx
				.getBean("settlingMicrobeService");
		try {
			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			CleanAreaMicrobe sct = settlingMicrobeService.gets(businessKey);
			if (sct.getConfirmUser() != null) {
				delegateTask.addCandidateUser(sct.getConfirmUser().getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
