package com.biolims.workflow.listener;

import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.service.CommonService;
import com.biolims.experiment.wk.model.WkTask;
import com.biolims.experiment.wk.model.WkTaskItem;
import com.biolims.experiment.wk.model.WkTaskTemp;
import com.biolims.experiment.wk.service.WkTaskService;

public class RoutingSetWkNoListener implements ExecutionListener {

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		// 得到业务键
		String businessKey = execution.getProcessBusinessKey();
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		WkTaskService wkTaskService = (WkTaskService) ctx
				.getBean("wkTaskService");
		CommonService commonService = (CommonService) ctx
				.getBean("commonService");
		WkTask sct = wkTaskService.get(businessKey);
		sct.setState(com.biolims.workflow.WorkflowConstants.DIC_STATE_WORKFLOW_IN_PROCESS_EDIT);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_MODIFY_NAME);
		commonService.saveOrUpdate(sct);
		List<WkTaskItem> ptiList = wkTaskService
				.findWkTaskItemList(businessKey);
		for (WkTaskItem pti : ptiList) {
			if (pti.getCode() != null && !pti.getCode().equals("")
					&& !pti.getCode().contains("ZKP")
					&& pti.getTempId() != null) {
				WkTaskTemp ptt = commonService.get(WkTaskTemp.class,
						pti.getTempId());
				if (ptt != null && !ptt.equals("")) {

					ptt.setState("1");
					commonService.saveOrUpdate(ptt);
				}
			}

		}

	}
}
