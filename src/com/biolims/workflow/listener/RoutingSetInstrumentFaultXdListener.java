package com.biolims.workflow.listener;

import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.service.CommonService;
import com.biolims.dic.model.DicState;
import com.biolims.equipment.fault.service.InstrumentFaultService;
import com.biolims.equipment.main.service.InstrumentService;
import com.biolims.equipment.model.Instrument;
import com.biolims.equipment.model.InstrumentFault;
import com.biolims.equipment.model.InstrumentFaultDetail;

public class RoutingSetInstrumentFaultXdListener implements ExecutionListener {

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		// 得到业务键
		String businessKey = execution.getProcessBusinessKey();
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		InstrumentFaultService instrumentFaultService = (InstrumentFaultService) ctx
				.getBean("instrumentFaultService");
		InstrumentService instrumentService = (InstrumentService) ctx
				.getBean("instrumentService");
		CommonService commonService = (CommonService) ctx
				.getBean("commonService");
		InstrumentFault sct = instrumentFaultService.getInstrumentFault(businessKey);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_APPROVALING);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_APPROVALING_NAME);
		List<InstrumentFaultDetail> iftList = instrumentFaultService
				.findInstrumentFaultDetail(businessKey);
		Instrument ins=null;
		for (InstrumentFaultDetail pti : iftList) {
			DicState ds=new DicState();
			ins=instrumentService.getInstrument(pti.getInstrument().getId());
			ds.setId("0r4");
			ins.setState(ds);
			commonService.saveOrUpdate(ins);
		}
		commonService.saveOrUpdate(sct);
	}
}
