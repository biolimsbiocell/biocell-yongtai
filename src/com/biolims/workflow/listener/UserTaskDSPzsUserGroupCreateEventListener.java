package com.biolims.workflow.listener;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.deviation.plan.model.DeviationSurveyPlan;
import com.biolims.deviation.plan.service.DeviationSurveyPlanService;

public class UserTaskDSPzsUserGroupCreateEventListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext.getServletContext());
		DeviationSurveyPlanService deviationSurveyPlanService = (DeviationSurveyPlanService) ctx
				.getBean("deviationSurveyPlanService");
		List<String> groupList = new ArrayList<String>();
		try {
			String businessKey = delegateTask.getExecution().getProcessBusinessKey();
			DeviationSurveyPlan sct = deviationSurveyPlanService.get(businessKey);
			if (!"".equals(sct.getApprovalUser().getId()) && sct.getApprovalUser().getId() != null) {
				delegateTask.addCandidateUser(sct.getApprovalUser().getId());
			}
			// if (sct.getUserGroup() != null) {
			// groupList.add(sct.getUserGroup().getId());
			// }
			// delegateTask.addCandidateGroups(groupList);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
