package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.service.CommonService;
import com.biolims.sample.model.SampleReceive;
import com.biolims.sample.service.SampleReceiveService;

public class RoutingSetSampleReceiveConfirmOKListener implements
		ExecutionListener {

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		// 得到业务键
		String businessKey = execution.getProcessBusinessKey();
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		SampleReceiveService sampleReceiveService = (SampleReceiveService) ctx
				.getBean("sampleReceiveService");
		CommonService commonService = (CommonService) ctx
				.getBean("commonService");
		SampleReceive sct = sampleReceiveService.get(businessKey);
		sct.setState(com.biolims.workflow.WorkflowConstants.DIC_STATE_WORKFLOW_IN_PROCESS_EDIT);
		sct.setStateName("已下达");
		// SampleInfoMain sc = commonService.get(SampleInfoMain.class, sct
		// .getSampleInfoMain().getId());
		// sc.setState("1");
		// commonService.saveOrUpdate(sc);
		commonService.saveOrUpdate(sct);
	}
}
