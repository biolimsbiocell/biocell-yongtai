package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.service.CommonService;
import com.biolims.experiment.sj.wklifeblend.model.WkLifeBlendTask;
import com.biolims.experiment.sj.wklifeblend.service.WkLifeBlendTaskService;

public class RoutingSetwkLifeBlendXdListener implements ExecutionListener {

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		// 得到业务键
		String businessKey = execution.getProcessBusinessKey();
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		WkLifeBlendTaskService wkLifeBlendTaskService = (WkLifeBlendTaskService) ctx
				.getBean("wkLifeBlendTaskService");
		CommonService commonService = (CommonService) ctx
				.getBean("commonService");
//		SampleInService sampleInService = (SampleInService) ctx
//				.getBean("sampleInService");
		WkLifeBlendTask sct = wkLifeBlendTaskService.get(businessKey);
		sct.setState(com.biolims.workflow.WorkflowConstants.DIC_STATE_WORKFLOW_IN_PROCESS_EDIT);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.DIC_STATE_WORKFLOW_IN_PROCESS_NAME);
//		List<WkBlendTaskItem> ptiList = wkBlendTaskService
//				.findDnaItemList(businessKey);
//		boolean b = false;
//		for (WkBlendTaskItem pti : ptiList) {
//			if ("".equals(pti.getIsOut()) || pti.getIsOut() == null) {
//				if (sampleInService.getSampleInItemByCodeState(pti.getFjwk()) > 0) {
//					pti.setIsOut("2");// 执行过出库左侧添加
//					SampleOutTemp sot = new SampleOutTemp();
//					SampleInItem si = sampleInService.selectTempByCode(pti
//							.getFjwk());
//					sot.setCode(pti.getFjwk());
//					sot.setSampleCode(pti.getSampleCode());
////					sot.setSampleType(pti.getSampleType());
//					sot.setLocation(si.getLocation());
//					sot.setNum(si.getNum());
//					sot.setVolume(si.getVolume());
//					sot.setSumTotal(si.getSumTotal());
//					sot.setConcentration(si.getConcentration());
//					sot.setState("1");
//					sot.setType("0"); // 0：实验
//					sot.setApplyUser(sct.getCreateUser());
//					sot.setTaskId(sct.getId());
//					sot.setInfoFrom("WkBlendTaskItem");
//					sot.setSampleInItemId(si.getId());
////					sot.setTechJkServiceTask(pti.getTechJkServiceTask());
////					sot.setTjItem(pti.getTjItem());
////					sot.setBoxId(si.getBoxId());
////					if (pti.getTjItem() != null) {
////						sot.setIdCard(pti.getTjItem().getExternalCode());
////					}
//					commonService.saveOrUpdate(sot);
//					b = true;
//				}
//			}
//		}
//		if (b) {
//			// 添加到待出库任务单
//			// SampleOutTaskId soti = new SampleOutTaskId();
//			// soti.setTaskId(sct.getId());
//			// soti.setName("稀释混合");
//			// soti.setCreateUser(sct.getCreateUser());
//			// soti.setCreateDate(new Date());
//			// soti.setState("1");
//			// soti.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_WAIT_OUT_COMPLETE_NAME);
//			// soti.setType("0");// 0：实验
//			// commonService.saveOrUpdate(soti);
//			sampleInService.selIsSampleOutTaskId(sct.getId(), "稀释混合");
//		}
		commonService.saveOrUpdate(sct);
	}
}
