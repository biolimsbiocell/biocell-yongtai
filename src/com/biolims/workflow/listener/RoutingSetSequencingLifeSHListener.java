package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.service.CommonService;
import com.biolims.experiment.sequencingLife.model.SequencingLifeTask;
import com.biolims.experiment.sequencingLife.service.SequencingLifeService;


public class RoutingSetSequencingLifeSHListener implements ExecutionListener {

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		// 得到业务键
		String businessKey = execution.getProcessBusinessKey();
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		SequencingLifeService sequencingLifeService = (SequencingLifeService) ctx
				.getBean("sequencingLifeService");
		CommonService commonService = (CommonService) ctx
				.getBean("commonService");
		SequencingLifeTask sct = sequencingLifeService.get(businessKey);
		sct.setState(com.biolims.workflow.WorkflowConstants.DIC_STATE_WORKFLOW_IN_PROCESS_EDIT);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_APPROVALING_NAME);
		commonService.saveOrUpdate(sct);
	}
}
