package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.deviation.model.DeviationHandlingReport;
import com.biolims.deviation.service.DeviationHandlingReportService;

public class UserTaskDHRjgpdCreateEventListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext.getServletContext());
		DeviationHandlingReportService poolingService = (DeviationHandlingReportService) ctx
				.getBean("deviationHandlingReportService");
		try {
			String businessKey = delegateTask.getExecution().getProcessBusinessKey();
			DeviationHandlingReport sct = poolingService.get(businessKey);
			if ("1".equals(sct.getType())) {
				delegateTask.addCandidateUser(sct.getDepartmentUser().getId());
			}
			if ("2".equals(sct.getType()) || "3".equals(sct.getType())) {
				delegateTask.addCandidateUser(sct.getGroupLeader().getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
