package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.applicationType.model.ApplicationTypeTable;
import com.biolims.util.BeanUtils;
import com.biolims.workflow.WorkflowConstants;
import com.biolims.workflow.service.WorkflowService;

/**
 * 子流程结束，更改业务state状态为已完成
 * @author cong
 */
public class SubProcessEndBusinessStateListener implements ExecutionListener {

	public void notify(DelegateExecution execution) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		WorkflowService workflowService = (WorkflowService) ctx.getBean("workflowService");

		Object formName = execution.getVariable("formName");
		Object businessKey = execution.getVariable("formId");
		if (formName != null && businessKey != null) {
			ApplicationTypeTable att = workflowService.get(ApplicationTypeTable.class, formName.toString());
			Class<?> objClass = Class.forName(att.getClassPath());
			Object b = workflowService.get(objClass, businessKey.toString());
			BeanUtils.setFieldValue(b, "state", WorkflowConstants.WORKFLOW_COMPLETE);
			BeanUtils.setFieldValue(b, "stateName", WorkflowConstants.WORKFLOW_COMPLETE_NAME);
			workflowService.save(b);
		}
	}

}
