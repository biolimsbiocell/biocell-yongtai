package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.dna.model.DnaTask;
import com.biolims.experiment.dna.service.DnaTaskService;
import com.biolims.experiment.enmonitor.dust.model.CleanAreaDust;
import com.biolims.experiment.enmonitor.dust.service.DustParticleService;
import com.biolims.experiment.enmonitor.microorganism.model.CleanAreaMicroorganism;
import com.biolims.experiment.enmonitor.microorganism.service.CleanAreaMicroorganismService;
import com.biolims.experiment.qpcr.model.QpcrTask;
import com.biolims.experiment.qpcr.service.QpcrTaskService;

/**
 * 洁净区尘埃粒子测试记录创建人监听
 * 
 * @author
 * 
 */
public class UserTaskCleanAreaMicroorganismCreateListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		CleanAreaMicroorganismService cleanAreaMicroorganismService = (CleanAreaMicroorganismService) ctx
				.getBean("cleanAreaMicroorganismService");
		try {
			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			CleanAreaMicroorganism sct = cleanAreaMicroorganismService.gets(businessKey);
			if (sct.getCreateUser() != null) {
				delegateTask.addCandidateUser(sct.getCreateUser().getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
