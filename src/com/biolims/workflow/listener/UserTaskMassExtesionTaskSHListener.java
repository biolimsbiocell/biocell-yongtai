package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.massarrayextension.model.MassarrayExtensionTask;
import com.biolims.experiment.massarrayextension.service.MassarrayExtensionTaskService;

/**
 * 超声破碎审核监听
 * 
 * @author
 * 
 */
public class UserTaskMassExtesionTaskSHListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		// List<String> groupList = new ArrayList<String>();
		MassarrayExtensionTaskService ufTaskService = (MassarrayExtensionTaskService) ctx
				.getBean("massarrayExtensionTaskService");
		try {
			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			MassarrayExtensionTask sct = ufTaskService.get(businessKey);
			if (sct.getConfirmUser() != null) {
				delegateTask.addCandidateUser(sct.getConfirmUser().getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
