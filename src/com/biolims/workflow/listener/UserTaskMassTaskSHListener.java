package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.massarray.model.MassarrayTask;
import com.biolims.experiment.massarray.service.MassarrayTaskService;

/**
 * 超声破碎审核监听
 * 
 * @author
 * 
 */
public class UserTaskMassTaskSHListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		// List<String> groupList = new ArrayList<String>();
		MassarrayTaskService ufTaskService = (MassarrayTaskService) ctx
				.getBean("massarrayTaskService");
		try {
			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			MassarrayTask sct = ufTaskService.get(businessKey);
			if (sct.getConfirmUser() != null) {
				delegateTask.addCandidateUser(sct.getConfirmUser().getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
