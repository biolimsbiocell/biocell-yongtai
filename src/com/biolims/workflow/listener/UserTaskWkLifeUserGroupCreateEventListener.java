package com.biolims.workflow.listener;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.wkLife.model.WkLifeTask;
import com.biolims.experiment.wkLife.service.WKLifeSampleTaskService;



/**
 * 文库实验组监听器
 * 
 * @author
 * 
 */
public class UserTaskWkLifeUserGroupCreateEventListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		List<String> groupList = new ArrayList<String>();
		WKLifeSampleTaskService wKLifeSampleTaskService = (WKLifeSampleTaskService) ctx
				.getBean("WKLifeSampleTaskService");
		try {
			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			WkLifeTask sct = wKLifeSampleTaskService.get(businessKey);
			if (!"".equals(sct.getTestUserOneId())) {
				String[] uidOne = sct.getTestUserOneId().split(",");
				for (int i = 0; i < uidOne.length; i++) {
					delegateTask.addCandidateUser(uidOne[i]);
				}
			}
			if (!"".equals(sct.getTestUserTwoId())) {
				String[] uidTwo = sct.getTestUserTwoId().split(",");
				for (int i = 0; i < uidTwo.length; i++) {
					delegateTask.addCandidateUser(uidTwo[i]);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
