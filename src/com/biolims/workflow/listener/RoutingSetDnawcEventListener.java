package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

import com.biolims.experiment.dna.custom.DnaTaskEvent;

/**
 * 路由中，取得金额，金额字段名称应该为fee
 * @author niyi
 *
 */
public class RoutingSetDnawcEventListener implements ExecutionListener {

	public void notify(DelegateExecution execution) throws Exception {

		String businessKey = execution.getProcessBusinessKey();
		DnaTaskEvent dtwf = new DnaTaskEvent();
		dtwf.operation("", businessKey);

	}

}
