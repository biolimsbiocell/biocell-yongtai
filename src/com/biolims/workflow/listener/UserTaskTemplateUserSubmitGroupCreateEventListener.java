package com.biolims.workflow.listener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.core.model.user.UserGroupUser;
import com.biolims.remind.model.SysRemind;
import com.biolims.sample.model.SampleReceive;
import com.biolims.sample.service.SampleReceiveService;
import com.biolims.system.template.model.Template;
import com.biolims.system.template.service.TemplateService;
import com.biolims.workflow.entity.WorkflowProcesssInstanceEntity;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

public class UserTaskTemplateUserSubmitGroupCreateEventListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		WorkflowProcessInstanceService instanceService = (WorkflowProcessInstanceService) ctx
				.getBean("workflowProcessInstanceService");
		String processInstanceId = delegateTask.getProcessInstanceId();
		try {
			WorkflowProcesssInstanceEntity wpe = instanceService
					.getWorkflowProcesssInstanceEntityByInstanceId(processInstanceId);
			delegateTask.setAssignee(wpe.getApplUserId());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
