package com.biolims.workflow.listener;

import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.service.CommonService;
import com.biolims.experiment.wk.model.WkTask;
import com.biolims.experiment.wk.model.WkTaskItem;
import com.biolims.experiment.wk.service.WkTaskService;
import com.biolims.sample.storage.model.SampleInItem;
import com.biolims.sample.storage.model.SampleOutTemp;
import com.biolims.sample.storage.service.SampleInService;

public class RoutingSetWkXdListener implements ExecutionListener {

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		// 得到业务键
		String businessKey = execution.getProcessBusinessKey();
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		WkTaskService wkTaskService = (WkTaskService) ctx
				.getBean("WKTaskService");
		CommonService commonService = (CommonService) ctx
				.getBean("commonService");
		SampleInService sampleInService = (SampleInService) ctx
				.getBean("sampleInService");
		WkTask sct = wkTaskService.get(businessKey);
		sct.setState(com.biolims.workflow.WorkflowConstants.DIC_STATE_WORKFLOW_IN_PROCESS_EDIT);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.DIC_STATE_WORKFLOW_IN_PROCESS_NAME);
		commonService.saveOrUpdate(sct);
		List<WkTaskItem> ptiList = wkTaskService
				.findWkTaskItemList(businessKey);
		boolean b = false;
		for (WkTaskItem pti : ptiList) {
			if ("".equals(pti.getIsOut()) || pti.getIsOut() == null) {
				if (sampleInService.getSampleInItemByCodeState(pti.getCode()) > 0) {
					pti.setIsOut("2");// 执行过出库左侧添加
					SampleOutTemp sot = new SampleOutTemp();
					SampleInItem si = sampleInService.selectTempByCode(pti
							.getCode());
					sot.setCode(pti.getCode());
					sot.setSampleCode(pti.getSampleCode());
					sot.setSampleType(pti.getSampleType());
					sot.setLocation(si.getLocation());
					sot.setNum(si.getNum());
					sot.setVolume(si.getVolume());
					sot.setSumTotal(si.getSumTotal());
					sot.setConcentration(si.getConcentration());
					sot.setState("1");
					sot.setType("0"); // 0：实验
					sot.setApplyUser(sct.getCreateUser());
					sot.setTaskId(sct.getId());
					sot.setInfoFrom("WkTaskItem");
					sot.setSampleInItemId(si.getId());
//					sot.setTechJkServiceTask(pti.getTechJkServiceTask());
//					sot.setTjItem(pti.getTjItem());
//					sot.setBoxId(si.getBoxId());
//					if (pti.getTjItem() != null) {
//						sot.setIdCard(pti.getTjItem().getExternalCode());
//					}
					commonService.saveOrUpdate(sot);
					b = true;
				}
			}
		}
		if (b) {
			// 添加到待出库任务单
			// SampleOutTaskId soti = new SampleOutTaskId();
			// soti.setTaskId(sct.getId());
			// soti.setName("文库构建");
			// soti.setCreateUser(sct.getCreateUser());
			// soti.setCreateDate(new Date());
			// soti.setState("1");
			// soti.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_WAIT_OUT_COMPLETE_NAME);
			// soti.setType("0");// 0：实验
			// commonService.saveOrUpdate(soti);
			sampleInService.selIsSampleOutTaskId(sct.getId(), "文库构建");
		}
	}
}
