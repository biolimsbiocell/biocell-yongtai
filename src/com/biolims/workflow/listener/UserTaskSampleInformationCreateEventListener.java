package com.biolims.workflow.listener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.analysis.data.model.DataTaskItem;
import com.biolims.analysis.data.service.DataTaskService;

public class UserTaskSampleInformationCreateEventListener implements TaskListener {

	@Override
	public void notify(DelegateTask delegateTask) {
		try {
			WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
					.getServletContext());
			DataTaskService dataTaskService = (DataTaskService) ctx.getBean("dataTaskService");
			String businessKey = delegateTask.getExecution().getProcessBusinessKey();

			Map<String, String> mapForQuery = new HashMap<String, String>();
			mapForQuery.put("task.id", businessKey);

			List<DataTaskItem> list = dataTaskService.findDataTaskItemList(businessKey);
			List<String> userList = new ArrayList<String>();
			if (list.size()>0) {
				DataTaskItem sc = list.get(0);
				if (sc.getAcceptUser() != null) {
					if (sc.getAcceptUser().getId() != null) {
						if (!userList.contains(sc.getAcceptUser().getId()))
							userList.add(sc.getAcceptUser().getId());

						String v = (String) delegateTask.getExecution().getVariable("checkUserSessions");
						if (v != null && !v.equals("")) {
							String a = v;
							if (!a.contains(sc.getAcceptUser().getId())) {
								a += "," + sc.getAcceptUser().getId();
								delegateTask.getExecution().setVariable("checkUserSessions", a);
								delegateTask.setAssignee(sc.getAcceptUser().getId());
							}
						} else {
							String a = sc.getAcceptUser().getId();
							delegateTask.getExecution().setVariable("checkUserSessions", a);
							delegateTask.setAssignee(sc.getAcceptUser().getId());
						}
					}
				}
				if (sc.getAcceptUser2() != null) {
					if (sc.getAcceptUser2().getId() != null) {
						if (!userList.contains(sc.getAcceptUser2().getId()))
							userList.add(sc.getAcceptUser2().getId());

						String v = (String) delegateTask.getExecution().getVariable("checkUserSessions");
						if (v != null && !v.equals("")) {
							String a = v;
							if (!a.contains(sc.getAcceptUser2().getId())) {
								a += "," + sc.getAcceptUser2().getId();
								delegateTask.getExecution().setVariable("checkUserSessions", a);
								delegateTask.setAssignee(sc.getAcceptUser2().getId());
							}
						} else {
							String a = sc.getAcceptUser2().getId();
							delegateTask.getExecution().setVariable("checkUserSessions", a);
							delegateTask.setAssignee(sc.getAcceptUser2().getId());
						}
					}
				}
			}
			delegateTask.addCandidateUsers(userList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
