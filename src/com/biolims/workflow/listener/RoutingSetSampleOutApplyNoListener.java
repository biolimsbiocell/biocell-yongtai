package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.service.CommonService;
import com.biolims.sample.storage.model.SampleOutApply;
import com.biolims.sample.storage.service.SampleOutApplyService;

public class RoutingSetSampleOutApplyNoListener implements ExecutionListener {

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		// 得到业务键
		String businessKey = execution.getProcessBusinessKey();
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		SampleOutApplyService sampleOutApplyService = (SampleOutApplyService) ctx
				.getBean("sampleOutApplyService");
		CommonService commonService = (CommonService) ctx
				.getBean("commonService");
		SampleOutApply sct = sampleOutApplyService.get(businessKey);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_INVALID);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_INVALID_NAME);
		commonService.saveOrUpdate(sct);
	}
}
