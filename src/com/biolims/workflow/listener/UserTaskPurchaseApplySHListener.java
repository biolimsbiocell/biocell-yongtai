package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.purchase.apply.service.PurchaseApplyService;
import com.biolims.purchase.model.PurchaseApply;

/**
 * 采购申请审核监听
 * 
 * @author
 * 
 */
public class UserTaskPurchaseApplySHListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		PurchaseApplyService purchaseApplyService = (PurchaseApplyService) ctx
				.getBean("purchaseApplyService");
		try {
			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			PurchaseApply sct = purchaseApplyService.getPurchaseApply(businessKey);
			if (sct.getConfirmUser() != null) {
				delegateTask.addCandidateUser(sct.getConfirmUser().getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
