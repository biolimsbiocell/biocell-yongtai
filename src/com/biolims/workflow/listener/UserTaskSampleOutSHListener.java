package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.sample.storage.model.SampleOut;
import com.biolims.sample.storage.service.SampleOutService;

/**
 * 样本出库审核监听
 * 
 * @author
 * 
 */
public class UserTaskSampleOutSHListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		SampleOutService sampleOutService = (SampleOutService) ctx
				.getBean("sampleOutService");
		try {
			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			SampleOut sct = sampleOutService.get(businessKey);
			if (sct.getAcceptUser() != null) {
				delegateTask.addCandidateUser(sct.getAcceptUser().getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
