package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.sanger.model.SangerTask;
import com.biolims.experiment.sanger.service.SangerTaskService;

/**
 * Sanger审核监听
 * 
 * @author
 * 
 */
public class UserTaskSangerSHListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		// List<String> groupList = new ArrayList<String>();
		SangerTaskService sangerTaskService = (SangerTaskService) ctx
				.getBean("sangerTaskService");
		try {
			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			SangerTask sct = sangerTaskService.get(businessKey);
			if (sct.getConfirmUser() != null) {
				delegateTask.addCandidateUser(sct.getConfirmUser().getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
