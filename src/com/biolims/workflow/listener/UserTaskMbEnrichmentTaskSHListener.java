package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.sj.mbenrichment.model.MbEnrichment;
import com.biolims.experiment.sj.mbenrichment.service.MbEnrichmentService;




/**
 * 文库构建审核监听
 * 
 * @author
 * 
 */
public class UserTaskMbEnrichmentTaskSHListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		// List<String> groupList = new ArrayList<String>();
		MbEnrichmentService mbEnrichmentService = (MbEnrichmentService) ctx
				.getBean("mbEnrichmentService");
		try {
			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			MbEnrichment sct = mbEnrichmentService.get(businessKey);
			if (sct.getConfirmUser() != null) {
				delegateTask.addCandidateUser(sct.getConfirmUser().getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
