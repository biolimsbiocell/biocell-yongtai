package com.biolims.workflow.listener;

import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.service.CommonService;
import com.biolims.experiment.qc.qpcrjd.model.QpcrjdTask;
import com.biolims.experiment.qc.qpcrjd.model.QpcrjdTaskItem;
import com.biolims.experiment.qc.qpcrjd.model.QpcrjdTaskTemp;
import com.biolims.experiment.qc.qpcrjd.service.QpcrjdTaskService;

public class RoutingSetQpcrjdXdListener implements ExecutionListener {

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		// 得到业务键
		String businessKey = execution.getProcessBusinessKey();
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		QpcrjdTaskService qpcrjdTaskService = (QpcrjdTaskService) ctx
				.getBean("qpcrjdTaskService");
		CommonService commonService = (CommonService) ctx
				.getBean("commonService");
		QpcrjdTask sct = qpcrjdTaskService.get(businessKey);
		sct.setState(com.biolims.workflow.WorkflowConstants.DIC_STATE_WORKFLOW_IN_PROCESS_EDIT);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.DIC_STATE_WORKFLOW_IN_PROCESS_NAME);
		List<QpcrjdTaskItem> ptiList = qpcrjdTaskService.findDnaItemList(businessKey);
		for (QpcrjdTaskItem pti : ptiList) {
			QpcrjdTaskTemp dt = commonService
					.get(QpcrjdTaskTemp.class, pti.getTempId());
			if (dt != null) {
				dt.setState("2");
			}
		}
		commonService.saveOrUpdate(sct);
	}
}
