package com.biolims.workflow.listener;

import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.service.CommonService;
import com.biolims.experiment.sj.wknd.model.WkndTask;
import com.biolims.experiment.sj.wknd.model.WkndTaskItem;
import com.biolims.experiment.sj.wknd.model.WkndTaskTemp;
import com.biolims.experiment.sj.wknd.service.WkndTaskService;

public class RoutingSetwkndNoListener implements ExecutionListener {

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		// 得到业务键
		String businessKey = execution.getProcessBusinessKey();
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		WkndTaskService wkndTaskService = (WkndTaskService) ctx
				.getBean("wkndTaskService");
		CommonService commonService = (CommonService) ctx
				.getBean("commonService");
		WkndTask sct = wkndTaskService.get(businessKey);
		sct.setState(com.biolims.workflow.WorkflowConstants.DIC_STATE_WORKFLOW_IN_PROCESS_EDIT);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_MODIFY_NAME);
		List<WkndTaskItem> ptiList = wkndTaskService.findDnaItemList(businessKey);
		for (WkndTaskItem pti : ptiList) {
			WkndTaskTemp dt = commonService
					.get(WkndTaskTemp.class, pti.getTempId());
			if (dt != null) {
				dt.setState("1");
			}
		}
		commonService.saveOrUpdate(sct);
	}
}
