package com.biolims.workflow.listener;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.qc.qpcrjd.model.QpcrjdTask;
import com.biolims.experiment.qc.qpcrjd.service.QpcrjdTaskService;

/**
 * 超声破碎实验组监听器
 * 
 * @author
 * 
 */
public class UserTaskQpcrjdUserGroupCreateEventListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		List<String> groupList = new ArrayList<String>();
		QpcrjdTaskService qpcrjdTaskService = (QpcrjdTaskService) ctx
				.getBean("qpcrjdTaskService");
		try {
			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			QpcrjdTask sct = qpcrjdTaskService.get(businessKey);
			if (sct.getAcceptUser() != null) {
				groupList.add(sct.getAcceptUser().getId());
			}
			delegateTask.addCandidateGroups(groupList);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
