package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.check.model.TechCheckServiceTask;
import com.biolims.experiment.check.service.TechCheckServiceTaskService;

/**
 * 实验检测实验组监听器
 * 
 * @author
 * 
 */
public class UserTaskTechUserGroupCreateEventListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		// List<String> groupList = new ArrayList<String>();
		TechCheckServiceTaskService techCheckServiceTaskService = (TechCheckServiceTaskService) ctx
				.getBean("techCheckServiceTaskService");
		try {
			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			TechCheckServiceTask sct = techCheckServiceTaskService
					.get(businessKey);
			if (!"".equals(sct.getTestUserOneId())) {
				String[] uidOne = sct.getTestUserOneId().split(",");
				for (int i = 0; i < uidOne.length; i++) {
					delegateTask.addCandidateUser(uidOne[i]);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
