package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

import com.biolims.sample.custom.SampleReceiveEvent;

public class RoutingSetSampleReceiveEventListener implements ExecutionListener {

	public void notify(DelegateExecution execution) throws Exception {

		String businessKey = execution.getProcessBusinessKey();
		SampleReceiveEvent dtwf = new SampleReceiveEvent();
		dtwf.operation("", businessKey);

	}
}
