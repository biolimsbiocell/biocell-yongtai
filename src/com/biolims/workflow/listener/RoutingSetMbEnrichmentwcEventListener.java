package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

import com.biolims.experiment.sj.mbenrichment.custom.MbEnrichmentEvent;





/**
 * 路由中，取得金额，金额字段名称应该为fee
 * @author niyi
 *
 */
public class RoutingSetMbEnrichmentwcEventListener implements ExecutionListener {

	public void notify(DelegateExecution execution) throws Exception {

		String businessKey = execution.getProcessBusinessKey();
		MbEnrichmentEvent dtwf = new MbEnrichmentEvent();
		dtwf.operation("", businessKey);

	}

}
