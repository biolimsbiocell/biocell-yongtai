package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

import com.biolims.experiment.plasma.custom.BloodSplitEvent;

/**
 * 路由中，取得金额，金额字段名称应该为fee
 * @author niyi
 *
 */
public class RoutingSetPlasmawcEventListener implements ExecutionListener {

	public void notify(DelegateExecution execution) throws Exception {

		String businessKey = execution.getProcessBusinessKey();
		BloodSplitEvent dtwf = new BloodSplitEvent();
		dtwf.operation("", businessKey);

	}

}
