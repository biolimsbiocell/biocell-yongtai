package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.wk.model.WkTask;
import com.biolims.experiment.wk.service.WkTaskService;

/**
 * 文库构建审核监听
 * 
 * @author
 * 
 */
public class UserTaskWkTaskSHListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		// List<String> groupList = new ArrayList<String>();
		WkTaskService wkTaskService = (WkTaskService) ctx
				.getBean("wkTaskService");
		try {
			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			WkTask sct = wkTaskService.get(businessKey);
			if (sct.getConfirmUser() != null) {
				delegateTask.addCandidateUser(sct.getConfirmUser().getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
