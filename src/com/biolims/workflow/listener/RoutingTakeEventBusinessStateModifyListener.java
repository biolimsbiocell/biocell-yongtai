package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.activiti.engine.repository.ProcessDefinition;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.applicationType.model.ApplicationTypeTable;
import com.biolims.util.BeanUtils;
import com.biolims.workflow.WorkflowConstants;
import com.biolims.workflow.entity.WorkflowBindForm;
import com.biolims.workflow.service.WorkflowService;

/**
 * 审批中需修改
 * @author cong
 *
 */
public class RoutingTakeEventBusinessStateModifyListener implements ExecutionListener {

	public void notify(DelegateExecution execution) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		WorkflowService workflowService = (WorkflowService) ctx.getBean("workflowService");
		String businessKey = execution.getProcessBusinessKey();
		String definitionId = execution.getProcessDefinitionId();
		ProcessDefinition pd = workflowService.getRepositoryService().createProcessDefinitionQuery()
				.processDefinitionId(definitionId).singleResult();
		WorkflowBindForm form = workflowService.getWorkflowBindFormByDefinitionKey(pd.getKey());
		ApplicationTypeTable att = workflowService.get(ApplicationTypeTable.class, form.getFormName());

		Class<?> objClass = Class.forName(att.getClassPath());
		Object b = workflowService.get(objClass, businessKey);
		BeanUtils.setFieldValue(b, "state", WorkflowConstants.WORKFLOW_MODIFY);
		BeanUtils.setFieldValue(b, "stateName", WorkflowConstants.WORKFLOW_MODIFY_NAME);
		workflowService.save(b);

	}

}
