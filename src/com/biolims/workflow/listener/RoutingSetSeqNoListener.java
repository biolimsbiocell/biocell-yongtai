package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.service.CommonService;
import com.biolims.experiment.sequencing.model.SequencingTask;
import com.biolims.experiment.sequencing.service.SequencingTaskService;

public class RoutingSetSeqNoListener implements ExecutionListener {

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		// 得到业务键
		String businessKey = execution.getProcessBusinessKey();
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		SequencingTaskService sequencingService = (SequencingTaskService) ctx
				.getBean("sequencingTaskService");
		CommonService commonService = (CommonService) ctx
				.getBean("commonService");
		SequencingTask sct = sequencingService.get(businessKey);
		sct.setState(com.biolims.workflow.WorkflowConstants.DIC_STATE_WORKFLOW_IN_PROCESS_EDIT);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_MODIFY_NAME);
		
		commonService.saveOrUpdate(sct);
	}
}
