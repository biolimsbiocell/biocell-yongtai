package com.biolims.workflow.listener;

import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.service.CommonService;
import com.biolims.experiment.plasma.model.PlasmaTask;
import com.biolims.experiment.plasma.model.PlasmaTaskItem;
import com.biolims.experiment.plasma.service.BloodSplitService;
import com.biolims.sample.storage.model.SampleInItem;
import com.biolims.sample.storage.model.SampleOutTemp;
import com.biolims.sample.storage.service.SampleInService;

public class RoutingSetPlasmaXdListener implements ExecutionListener {

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		// 得到业务键
		String businessKey = execution.getProcessBusinessKey();
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		BloodSplitService bloodSplitService = (BloodSplitService) ctx
				.getBean("bloodSplitService");
		CommonService commonService = (CommonService) ctx
				.getBean("commonService");
		SampleInService sampleInService = (SampleInService) ctx
				.getBean("sampleInService");
		PlasmaTask sct = bloodSplitService.get(businessKey);
		sct.setState(com.biolims.workflow.WorkflowConstants.DIC_STATE_WORKFLOW_IN_PROCESS_EDIT);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.DIC_STATE_WORKFLOW_IN_PROCESS_NAME);
		List<PlasmaTaskItem> ptiList = bloodSplitService
				.findPlasmaItemList(businessKey);
		boolean b = false;
		for (PlasmaTaskItem pti : ptiList) {
			if ("".equals(pti.getIsOut()) || pti.getIsOut() == null) {
				if (sampleInService.getSampleInItemByCodeState(pti.getCode()) > 0) {
					pti.setIsOut("2");// 执行过出库左侧添加
					SampleOutTemp sot = new SampleOutTemp();
					SampleInItem si = sampleInService.selectTempByCode(pti
							.getCode());
					sot.setCode(pti.getCode());
					sot.setSampleCode(pti.getSampleCode());
					sot.setSampleType(pti.getSampleType());
					sot.setLocation(si.getLocation());
					sot.setNum(si.getNum());
					sot.setVolume(si.getVolume());
					sot.setSumTotal(si.getSumTotal());
					sot.setConcentration(si.getConcentration());
					sot.setState("1");
					sot.setType("0"); // 0：实验
					sot.setApplyUser(sct.getCreateUser());
					sot.setTaskId(sct.getId());
					sot.setInfoFrom("PlasmaTaskItem");
					sot.setSampleInItemId(si.getId());
//					sot.setTechJkServiceTask(pti.getTechJkServiceTask());
//					sot.setTjItem(pti.getTjItem());
//					sot.setBoxId(si.getBoxId());
//					if (pti.getTjItem() != null) {
//						sot.setIdCard(pti.getTjItem().getExternalCode());
//					}
					commonService.saveOrUpdate(sot);
					b = true;
				}
			}
		}
		
		if (b) {
			// 添加到待出库任务单
			// SampleOutTaskId soti = new SampleOutTaskId();
			// soti.setTaskId(sct.getId());
			// soti.setName("核酸提取");
			// soti.setCreateUser(sct.getCreateUser());
			// soti.setCreateDate(new Date());
			// soti.setState("1");
			// soti.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_WAIT_OUT_COMPLETE_NAME);
			// soti.setType("0");// 0：实验
			// commonService.saveOrUpdate(soti);
			sampleInService.selIsSampleOutTaskId(sct.getId(), "样本处理");
		}
		commonService.saveOrUpdate(sct);
	}
}
