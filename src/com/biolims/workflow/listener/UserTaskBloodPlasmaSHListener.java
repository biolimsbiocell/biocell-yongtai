package com.biolims.workflow.listener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.experiment.plasma.model.PlasmaTask;
import com.biolims.experiment.plasma.service.BloodSplitService;

/**
 * 样本处理审核监听
 * 
 * @author
 * 
 */
public class UserTaskBloodPlasmaSHListener implements TaskListener {

	public void notify(DelegateTask delegateTask) {
		WebApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(ServletActionContext
						.getServletContext());
		// List<String> groupList = new ArrayList<String>();
		BloodSplitService bloodSplitService = (BloodSplitService) ctx
				.getBean("bloodSplitService");
		try {
			String businessKey = delegateTask.getExecution()
					.getProcessBusinessKey();
			PlasmaTask sct = bloodSplitService.get(businessKey);
			if (sct.getConfirmUser() != null) {
				delegateTask.addCandidateUser(sct.getConfirmUser().getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
