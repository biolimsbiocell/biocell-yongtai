package com.biolims.workflow.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.util.SendData;
import com.biolims.workflow.entity.WorkflowProcesssInstanceEntity;
import com.biolims.workflow.service.WorkflowPCCheckService;

@Controller
@Scope("prototype")
@ParentPackage("default")
@Namespace("/workflow/check")
@SuppressWarnings("unchecked")
public final class WorkflowPCCheckAction extends BaseActionSupport {

	private static final long serialVersionUID = -7641420016722401908L;

	//该action权限id
	private String rightsId = "workflowPCCheck";

	@Resource
	private WorkflowPCCheckService workflowPCCheckService;

	/**
	 * 查询流程实例
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showPCCheckList")
	public String showPCCheckList() throws Exception {
		return dispatcher("/WEB-INF/page/pccheck/showPCCheckList.jsp");
	}

	@Action(value = "showPCCheckJson")
	public void showPCCheckJson() throws Exception {
		String _startNum = getParameterFromRequest("start");
		String _limitNum = getParameterFromRequest("limit");
		// 开始记录数
		int startNum = Integer.parseInt((_startNum == null || _startNum.length() <= 0) ? 0 + "" : _startNum);
		// limit
		int limitNum = Integer.parseInt((_limitNum == null || _limitNum.length() <= 0) ? 10 + "" : _limitNum);
		//排序方式
		String dir = getParameterFromRequest("dir");
		//字段
		String sort = getParameterFromRequest("sort");

		String sendUser = getRequest().getParameter("sendUser");

		String assignee = getRequest().getParameter("assignee");
		String formId = getRequest().getParameter("formId");
		String formName = getRequest().getParameter("formName");
		Map<String, Object> result = workflowPCCheckService.findProcessInstance(sendUser, assignee, formId, formName,
				startNum, limitNum, dir, sort);

		Long total = (Long) result.get("total");
		List<WorkflowProcesssInstanceEntity> list = (List<WorkflowProcesssInstanceEntity>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "id");
		map.put("processInstanceId", "processInstanceId");
		map.put("applUserId", "applUserId");
		map.put("applUserName", "applUserName");
		map.put("formName", "formName");
		map.put("formTitle", "formTitle");
		map.put("tableName", "tableName");
		map.put("tablePath", "tablePath");
		map.put("startDate", "yyyy-MM-dd HH:mm:ss");
		map.put("endDate", "yyyy-MM-dd HH:mm:ss");
		map.put("businessKey", "businessKey");
		map.put("version", "version");
		map.put("isEnd", "isEnd");
		map.put("isSubProcess", "isSubProcess");
		map.put("parentProcessId", "parentProcessId");

		new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

}
