package com.biolims.workflow.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.util.HttpUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.model.WorkflowDef;
import com.biolims.workflow.service.WorkflowDefService;

@Namespace("/workflow/def")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class WorkflowDefAction extends BaseActionSupport {

	private static final long serialVersionUID = -70875290036874990L;
	private String rightsId = "workflowdef";
	@Autowired
	private WorkflowDefService workflowDefService;
	private WorkflowDef workflowDef = new WorkflowDef();

	/**
	 * 
	 * @Title: showWorkflowDefTable @Description: 列表展示 @author : shengwei.wang @date
	 * 2018年3月8日上午11:21:14 @return @throws Exception String @throws
	 */
	@Action(value = "showWorkflowDefTable")
	public String showWorkflowDefTable() throws Exception {
		rightsId = "workflowdef";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/workflow/workflowdef/workflowDef.jsp");
	}

	@Action(value = "showWorkflowDefTableJson")
	public void showWorkflowDefTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = workflowDefService.showWorkflowDefTableJson(null, start, length, query, col,
					sort);
			List<WorkflowDef> list = (List<WorkflowDef>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("enName", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("cls", "");
			map.put("oper", "");
			map.put("orderBlock", "");
			map.put("orderBlockName", "");
			map.put("orderBlockEnName", "");
			map.put("note", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: selectWorkflowDef @Description: 弹框展示 @author : shengwei.wang @date
	 * 2018年3月8日上午11:21:44 @return String @throws
	 */
	@Action(value = "selectWorkflowDef")
	public String selectWorkflowDef() {
		String orderBlock = getParameterFromRequest("orderBlock");
		putObjToContext("orderBlock", orderBlock);
		return dispatcher("/WEB-INF/page/workflow/workflowdef/selectWorkflowDefTable.jsp");
	}

	@Action(value = "selectWorkflowDefTable")
	public void selectWorkflowDefTable() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String orderBlock = getParameterFromRequest("orderBlock");
		try {
			Map<String, Object> result = workflowDefService.showWorkflowDefTableJson(orderBlock, start, length, query,
					col, sort);
			List<WorkflowDef> list = (List<WorkflowDef>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("enName", "");
			map.put("cls", "");
			map.put("oper", "");
			map.put("orderBlock", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("note", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: editWorkflowDef @Description: 新建、编辑 @author : shengwei.wang @date
	 * 2018年3月8日下午4:19:53 @return @throws Exception String @throws
	 */
	@Action(value = "editWorkflowDef")
	public String editWorkflowDef() throws Exception {
		rightsId = "workflowdef";
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			this.workflowDef = workflowDefService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			workflowDef.setCreateUser(user);
			workflowDef.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		return dispatcher("/WEB-INF/page/workflow/workflowdef/workflowDefEdit.jsp");
	}

	/**
	 * 
	 * @Title: save @Description: 保存 @author : shengwei.wang @date
	 * 2018年3月8日下午4:27:25 @return String @throws
	 */
	@Action(value = "save")
	public String save() {
		if ("".equals(workflowDef.getId())) {
			workflowDef.setId(null);
		}
		workflowDefService.save(workflowDef);
		return redirect("/workflow/def/editWorkflowDef.action?id=" + workflowDef.getId());
	}

	/**
	 * @return the rightsId
	 */
	public String getRightsId() {
		return rightsId;
	}

	/**
	 * @param rightsId
	 *            the rightsId to set
	 */
	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	/**
	 * @return the workflowDefService
	 */
	public WorkflowDefService getWorkflowDefService() {
		return workflowDefService;
	}

	/**
	 * @param workflowDefService
	 *            the workflowDefService to set
	 */
	public void setWorkflowDefService(WorkflowDefService workflowDefService) {
		this.workflowDefService = workflowDefService;
	}

	/**
	 * @return the workflowDef
	 */
	public WorkflowDef getWorkflowDef() {
		return workflowDef;
	}

	/**
	 * @param workflowDef
	 *            the workflowDef to set
	 */
	public void setWorkflowDef(WorkflowDef workflowDef) {
		this.workflowDef = workflowDef;
	}

	@Action(value = "viewWorkflowDef")
	public String viewWorkflowDef() throws Exception {
		String id = getParameterFromRequest("id");
		workflowDef = workflowDefService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/workflow/workflowdef/workflowDefEdit.jsp");
	}
}
