package com.biolims.workflow.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.util.HttpUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.model.WorkflowProcess;
import com.biolims.workflow.service.WorkflowProcessService;

@Namespace("/workflow/process")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class WorkflowProcessAction extends BaseActionSupport {

	private static final long serialVersionUID = -5459327452946776382L;
	private String rightsId = "workflowProcess02";
	@Autowired
	private WorkflowProcessService workflowProcessService;
	private WorkflowProcess workflowProcess = new WorkflowProcess();

	@Action(value = "showWorkflowProcess")
	public String showWorkflowProcess() throws Exception {
		rightsId = "workflowProcess02";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/workflow/workflow/workflowProcess.jsp");
	}

	@Action(value = "showWorkflowProcessJson")
	public void showWorkflowProcessJson() {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = workflowProcessService
					.showWorkflowProcessJson(start, length, query, col, sort);
			List<WorkflowProcess> list = (List<WorkflowProcess>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("content", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("orderBlock", "");
			map.put("orderBlockName", "");
			map.put("note", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @Title: editWorkflowDef
	 * @Description: 新建、编辑
	 * @author : shengwei.wang
	 * @date 2018年3月8日下午4:19:53
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "editWorkflowProcess")
	public String editWorkflowProcess() throws Exception {
		rightsId = "workflowProcess01";
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			this.workflowProcess = workflowProcessService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		} else {
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			workflowProcess.setCreateUser(user);
			workflowProcess.setCreateDate(new Date());
			workflowProcess
					.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			workflowProcess
					.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		return dispatcher("/WEB-INF/page/workflow/workflow/workflowProcessEdit.jsp");
	}

	/**
	 * 
	 * @Title: save
	 * @Description: 保存
	 * @author : shengwei.wang
	 * @date 2018年3月8日下午4:27:25
	 * @return String
	 * @throws
	 */
	@Action(value = "save")
	public String save() {
		if ("".equals(workflowProcess.getId())) {
			workflowProcess.setId(null);
		}
		workflowProcessService.save(workflowProcess);
		return redirect("/workflow/process/editWorkflowProcess.action?id="
				+ workflowProcess.getId());
	}

	/**
	 * 
	 * @Title: viewWorkflowProcess
	 * @Description: 实验配置查看审批流
	 * @author : shengwei.wang
	 * @date 2018年4月11日下午3:48:53
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "viewWorkflowProcess")
	public String viewWorkflowProcess() throws Exception {
		rightsId = "workflowProcess01";
		String tableId = getParameterFromRequest("tableId");
		String tableName = getParameterFromRequest("tableName");
		String taName =new String( tableName.getBytes("iso-8859-1"),"UTF-8");// 解码
		if (workflowProcessService.viewWorkflowProcess(tableId) == null) {
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			workflowProcess.setCreateUser(user);
			workflowProcess.setOrderBlock(tableId);
			workflowProcess.setOrderBlockName(taName);
			workflowProcess.setCreateDate(new Date());
			workflowProcess
					.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			workflowProcess
					.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		} else {
			String id = workflowProcessService
					.viewWorkflowProcess(tableId);
			workflowProcess=workflowProcessService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		}
		return dispatcher("/WEB-INF/page/workflow/workflow/workflowProcessEdit.jsp");
	}

	/**
	 * @return the rightsId
	 */
	public String getRightsId() {
		return rightsId;
	}

	/**
	 * @param rightsId
	 *            the rightsId to set
	 */
	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	/**
	 * @return the workflowProcessService
	 */
	public WorkflowProcessService getWorkflowProcessService() {
		return workflowProcessService;
	}

	/**
	 * @param workflowProcessService
	 *            the workflowProcessService to set
	 */
	public void setWorkflowProcessService(
			WorkflowProcessService workflowProcessService) {
		this.workflowProcessService = workflowProcessService;
	}

	/**
	 * @return the workflowProcess
	 */
	public WorkflowProcess getWorkflowProcess() {
		return workflowProcess;
	}

	/**
	 * @param workflowProcess
	 *            the workflowProcess to set
	 */
	public void setWorkflowProcess(WorkflowProcess workflowProcess) {
		this.workflowProcess = workflowProcess;
	}
}
