package com.biolims.workflow.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.util.SendData;
import com.biolims.workflow.WorkflowConstants;
import com.biolims.workflow.entity.WorkflowHistoryTask;
import com.biolims.workflow.entity.WorkflowProcesssInstanceEntity;
import com.biolims.workflow.service.HistoryTaskService;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

@Controller
@Scope("prototype")
@ParentPackage("default")
@Namespace("/workflow/history")
@SuppressWarnings("unchecked")
public final class HistoryTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = -1081807121814201860L;

	//该action权限id
	private String rightsId = "workflowHistoryInstance";

	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;

	@Resource
	private HistoryTaskService historyTaskService;

	/**
	 * 查询流程实例
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showHistoryInstanceList")
	public String showHistoryInstanceList() throws Exception {
		return dispatcher("/WEB-INF/page/workflow/history/showHistoryInstanceList.jsp");
	}

	@Action(value = "showHistoryInstanceJson")
	public void showHistoryInstanceJson() throws Exception {
		String _startNum = getParameterFromRequest("start");
		String _limitNum = getParameterFromRequest("limit");
		// 开始记录数
		int startNum = Integer.parseInt((_startNum == null || _startNum.length() <= 0) ? 0 + "" : _startNum);
		// limit
		int limitNum = Integer.parseInt((_limitNum == null || _limitNum.length() <= 0) ? 10 + "" : _limitNum);
		//排序方式
		String dir = getParameterFromRequest("dir");
		//字段
		String sort = getParameterFromRequest("sort");

		String sendUser = getRequest().getParameter("sendUser");
		String formName = getParameterFromRequest("formName");
		String state = getParameterFromRequest("state");
		state = (state == null || state.length() <= 0) ? WorkflowConstants.WORKFLOW_COMPLETE : state;

		String fromId = getRequest().getParameter("formId");
		Map<String, Object> result = workflowProcessInstanceService.findProcessInstanceList(sendUser, fromId, null,
				formName, startNum, limitNum, dir, sort);

		Long total = (Long) result.get("total");
		List<WorkflowProcesssInstanceEntity> list = (List<WorkflowProcesssInstanceEntity>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "id");
		map.put("processInstanceId", "processInstanceId");
		map.put("applUserId", "applUserId");
		map.put("applUserName", "applUserName");
		map.put("formName", "formName");
		map.put("formTitle", "formTitle");
		map.put("tableName", "tableName");
		map.put("tablePath", "tablePath");
		map.put("startDate", "yyyy-MM-dd HH:mm:ss");
		map.put("endDate", "yyyy-MM-dd HH:mm:ss");
		map.put("businessKey", "businessKey");
		map.put("version", "version");
		map.put("isEnd", "isEnd");
		map.put("isSubProcess", "isSubProcess");
		map.put("parentProcessId", "parentProcessId");

		new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
	}

	/**
	 * 历史审批记录
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showTaskVariablesList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTaskVariablesList() throws Exception {
		String instanceId = getRequest().getParameter("instanceId");
		getRequest().setAttribute("instanceId", instanceId);
		return dispatcher("/WEB-INF/page/workflow/history/showTaskVariablesList.jsp");
	}

	@Action(value = "showTaskVariablesJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showTaskVariablesJson() throws Exception {
		String instanceId = getRequest().getParameter("instanceId");
		List<WorkflowHistoryTask> list = historyTaskService.findTaskVariablesListByInstanceId(instanceId);
		Long total = Long.parseLong(list.size() + "");
		Map<String, String> map = new HashMap<String, String>();
		map.put("taskName", "taskName");
		map.put("assigneeName", "assigneeName");
		map.put("modifyTime", "yyyy-MM-dd HH:mm:ss");
		map.put("operResult", "operResult");
		map.put("info", "info");
		map.put("proxyInfo", "proxyInfo");
		new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
	}

	/**
	 * 根据业务ID查询审批历史
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showTaskVariablesByFormIdList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showTaskVariablesByFormIdList() throws Exception {
		String fromId = getRequest().getParameter("formId");
		getRequest().setAttribute("instanceId", historyTaskService.getIntanceIdByFormId(fromId));
		return dispatcher("/WEB-INF/page/workflow/history/showTaskVariablesList.jsp");
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

}
