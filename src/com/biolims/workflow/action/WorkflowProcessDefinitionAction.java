package com.biolims.workflow.action;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;

import org.activiti.engine.repository.Deployment;
import org.apache.poi.util.IOUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import sun.misc.BASE64Decoder;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.bean.ActivitieBean;
import com.biolims.workflow.entity.CompletOperEntity;
import com.biolims.workflow.entity.WorkflowBindForm;
import com.biolims.workflow.entity.WorkflowProcessDefinitionEntity;
import com.biolims.workflow.entity.WorkflowVoteRuleSetEntity;
import com.biolims.workflow.service.VoteService;
import com.biolims.workflow.service.WorkflowProcessDefinitionService;
import com.biolims.workflow.service.WorkflowProcessService;

/**
 * 工作流定义
 * @author cong
 *
 */
@Namespace("/workflow/processdefinition")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class WorkflowProcessDefinitionAction extends BaseActionSupport {

	private static final long serialVersionUID = -5396270511948837699L;

	//该action权限id
	private String rightsId = "workflowProcessDefinition";
	@Autowired
	private WorkflowProcessService workflowProcessService;
	@Resource
	private WorkflowProcessDefinitionService workflowProcessDefinitionService;

	@Resource
	private VoteService voteService;

	/**
	 * 查看工作流定义
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showProcessDefinitionList")
	public String showProcessDefinitionList() throws Exception {
		return dispatcher("/WEB-INF/page/workflow/processdefinition/showProcessDefinitionList.jsp");
	}

	@Action(value = "showProcessDefinitionListJson")
	public void showProcessDefinitionListJson() throws Exception {
		String _startNum = getParameterFromRequest("start");
		String _limitNum = getParameterFromRequest("limit");
		// 开始记录数
		int startNum = Integer.parseInt((_startNum == null || _startNum.length() <= 0) ? 0 + "" : _startNum);
		// limit
		int limitNum = Integer.parseInt((_limitNum == null || _limitNum.length() <= 0) ? 10 + "" : _limitNum);
		// 排序方式
		String dir = getParameterFromRequest("dir");
		// 字段
		String sort = getParameterFromRequest("sort");

		String name = getRequest().getParameter("name");
		String key = getRequest().getParameter("key");
		try {
			Map<String, Object> result = workflowProcessDefinitionService.findProcessDefinitionList(name, key,
					startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<WorkflowProcessDefinitionEntity> list = (List<WorkflowProcessDefinitionEntity>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "id");
			map.put("processDefinitionId", "processDefinitionId");
			map.put("deploymentId", "deploymentId");
			map.put("name", "name");
			map.put("key", "key");
			map.put("version", "version");
			map.put("resourceName", "resourceName");
			map.put("diagramResourceName", "diagramResourceName");
			map.put("deploymentTime", "yyyy-MM-dd HH:mm:ss");
			map.put("isMailRemind", "isMailRemind");
			map.put("wpAdminId", "wpAdminId");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 部署流程定义
	 * @throws Exception
	 */
	@Action(value = "deploy")
	public void deploy() throws Exception {
		String xmlJson=getParameterFromRequest("xmlJson");
		String svg=getParameterFromRequest("svg");
		String id=getParameterFromRequest("id");
		String tableId=getParameterFromRequest("tableId");
		BASE64Decoder decoder = new BASE64Decoder();
		byte[] b = decoder.decodeBuffer(svg);
		InputStream ins=new ByteArrayInputStream(b);
		Map<String, Object> result = new HashMap<String, Object>();
			try {
				//部署流程定义文件
				Deployment deployment = workflowProcessDefinitionService.getRepositoryService().createDeployment().addString(tableId+".bpmn", xmlJson)
						.addInputStream(tableId+".png", ins).deploy();
				workflowProcessDefinitionService.saveProcessDefinitionEntity(id,deployment);
				workflowProcessDefinitionService.saveWorkflowBindForm(tableId);
				result.put("success", true);
				workflowProcessService.changeState(id);
			} catch (Throwable e) {
				result.put("success", false);
				e.printStackTrace();
				throw new Exception(e);
			} finally {
				
			}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 根据流程部署Id和资源名称加载流程资源
	 * @throws Exception
	 */
	@Action(value = "loadResourceByDeployment", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void loadResourceByDeployment() throws Exception {
		String deploymentId = getRequest().getParameter("deploymentId");
		String resourceName = getRequest().getParameter("resourceName");
		try {
			InputStream resourceAsStream = workflowProcessDefinitionService.getRepositoryService().getResourceAsStream(
					deploymentId, resourceName);
			byte[] byteArray = IOUtils.toByteArray(resourceAsStream);
			ServletOutputStream servletOutputStream = getResponse().getOutputStream();
			servletOutputStream.write(byteArray, 0, byteArray.length);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 流程定义设置
	 * @return
	 * @throws Exception
	 */
	@Action(value = "processDefinitionConfigView")
	public String processDefinitionConfigView() throws Exception {
		String definitionId = getRequest().getParameter("definitionId");
		getRequest().setAttribute("definitionId", definitionId);
		//获取该流程定义的所有节点
		return dispatcher("/WEB-INF/page/workflow/processdefinition/processDefinitionConfigView.jsp");
	}

	/**
	 * 节点信息数据
	 * @throws Exception
	 */
	@Action(value = "processDefinitionActivitis")
	public String processDefinitionActivitis() throws Exception {
		String definitionId = getRequest().getParameter("definitionId");
		getRequest().setAttribute("definitionId", definitionId);
		//获取该流程定义的所有节点
		return dispatcher("/WEB-INF/page/workflow/processdefinition/processDefinitionActivitis.jsp");
	}

	@Action(value = "processDefinitionActivitisJson")
	public void processDefinitionActivitisJson() throws Exception {
		String definitionId = getRequest().getParameter("definitionId");
		List<ActivitieBean> list = workflowProcessDefinitionService.findProcessDefinitionByDefinitionId(definitionId);
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "id");
		map.put("name", "name");
		map.put("type", "type");
		map.put("userIds", "userIds");
		map.put("userNames", "userNames");
		map.put("formNames", "formNames");
		map.put("groupIds", "groupIds");
		map.put("groupNames", "groupNames");
		new SendData().sendDateJson(map, list, list.size(), ServletActionContext.getResponse());
	}

	/**
	 * 保存用户任务的人员配置信息
	 * @throws Exception
	 */
	@Action(value = "saveDefinitionActivitisConfigUser")
	public void saveDefinitionActivitisConfigUser() throws Exception {
		Map<String, Boolean> result = new HashMap<String, Boolean>();
		try {
			String data = getRequest().getParameter("data");
			Map<String, Object> paramList = JsonUtils.toObjectByJson(data, Map.class);
			workflowProcessDefinitionService.saveDefinitionActivitisConfigUser(paramList);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 表单绑定列表
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showBindFormList")
	public String showBindFormList() throws Exception {
		return dispatcher("/WEB-INF/page/workflow/processdefinition/showBindFormList.jsp");
	}

	@Action(value = "showBindFormListJson")
	public void showBindFormListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 排序方式
		String dir = getParameterFromRequest("dir");
		// 字段
		String sort = getParameterFromRequest("sort");
		try {
			Map<String, Object> result = workflowProcessDefinitionService.findWorkflowBindFormList(startNum, limitNum,
					dir, sort);
			Long total = (Long) result.get("total");
			List<WorkflowBindForm> list = (List<WorkflowBindForm>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "id");
			map.put("formName", "formName");
			map.put("processDefinitionKey", "processDefinitionKey");

			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 保存表单绑定配置信息
	 * @throws Exception
	 */
	@Action(value = "saveBindForm")
	public void saveBindForm() throws Exception {
		Map<String, Boolean> result = new HashMap<String, Boolean>();
		try {
			String data = getRequest().getParameter("data");
			List<Map<String, String>> paramList = JsonUtils.toObjectByJson(data, List.class);
			workflowProcessDefinitionService.saveBindForm(paramList);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	@Action(value = "deleteBindForm")
	public void deleteBindForm() throws Exception {
		Map<String, Boolean> result = new HashMap<String, Boolean>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			workflowProcessDefinitionService.delete(WorkflowBindForm.class, ids);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 会签规则设置页面
	 * @throws Exception
	 */
	@Action(value = "showVoteRule")
	public String showVoteRule() throws Exception {
		String definitionId = getRequest().getParameter("definitionId");
		String activitiId = getRequest().getParameter("activitiId");
		getRequest().setAttribute("definitionId", definitionId);
		getRequest().setAttribute("activitiId", activitiId);
		WorkflowVoteRuleSetEntity voteRule = voteService.getWorkflowVoteRuleSetEntity(definitionId, activitiId);
		getRequest().setAttribute("voteRule", voteRule);
		return dispatcher("/WEB-INF/page/workflow/processdefinition/showVoteRule.jsp");
	}

	@Action(value = "saveVoteRule")
	public void saveVoteRule() throws Exception {
		Map<String, Boolean> result = new HashMap<String, Boolean>();
		try {
			String oneVote = getRequest().getParameter("oneVote");
			String voteMethod = getRequest().getParameter("voteMethod");
			String voteType = getRequest().getParameter("voteType");
			String voteNum = getRequest().getParameter("voteNum");
			String oneVoteUser = getRequest().getParameter("oneVoteUser");
			String activitiId = getRequest().getParameter("activitiId");
			String definitionId = getRequest().getParameter("definitionId");
			String id = getRequest().getParameter("id");
			id = id == null || id.length() <= 0 ? null : id;
			WorkflowVoteRuleSetEntity voteRule = new WorkflowVoteRuleSetEntity();
			voteRule.setId(id);
			voteRule.setActivitiId(activitiId);
			voteRule.setOneVoteRights(oneVote);
			voteRule.setRuleMethod(voteMethod);
			voteRule.setVoteType(voteType);
			if (voteNum != null && voteNum.length() > 0)
				voteRule.setVoteNum(Float.valueOf(voteNum));
			voteRule.setUserIds(oneVoteUser);
			voteRule.setProcessDefinitionId(definitionId);
			workflowProcessDefinitionService.saveOrUpdate(voteRule);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 设置操作按钮
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showOperButton")
	public String showOperButton() throws Exception {
		String activitiId = getRequest().getParameter("activitiId");
		getRequest().setAttribute("activitiId", activitiId);
		return dispatcher("/WEB-INF/page/workflow/processdefinition/showOperButton.jsp");
	}

	@Action(value = "showOperButtonJson")
	public void showOperButtonJson() throws Exception {
		String activitiId = getRequest().getParameter("activitiId");
		String definitionId = getRequest().getParameter("definitionId");
		try {
			List<CompletOperEntity> list = workflowProcessDefinitionService.findCompletOperEntity(activitiId,
					definitionId);
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "id");
			map.put("operTitle", "operTitle");
			map.put("operValue", "operValue");
			map.put("orderNumber", "orderNumber");
			new SendData().sendDateJson(map, list, list.size(), ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "saveOperButton")
	public void saveOperButton() throws Exception {
		Map<String, Boolean> result = new HashMap<String, Boolean>();
		try {
			String data = getRequest().getParameter("data");
			List<Map<String, String>> paramList = JsonUtils.toObjectByJson(data, List.class);
			workflowProcessDefinitionService.saveOperButton(paramList);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	@Action(value = "deleteOperButton")
	public void deleteOperButton() throws Exception {
		Map<String, Boolean> result = new HashMap<String, Boolean>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			workflowProcessDefinitionService.delete(CompletOperEntity.class, ids);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 邮件提醒
	 * @throws Exception
	 */
	@Action(value = "setMailRemind")
	public void setMailRemind() throws Exception {
		Map<String, Boolean> result = new HashMap<String, Boolean>();
		try {
			String id = getRequest().getParameter("id");
			workflowProcessDefinitionService.setMailRemind(id);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}
}
