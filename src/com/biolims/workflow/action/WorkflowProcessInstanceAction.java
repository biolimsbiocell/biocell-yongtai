package com.biolims.workflow.action;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;

import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.apache.poi.util.IOUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.applicationType.model.ApplicationTypeTable;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.dao.CommonDAO;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.bean.ProcessTaskBean;
import com.biolims.workflow.entity.WorkflowBindForm;
import com.biolims.workflow.entity.WorkflowHistoryTask;
import com.biolims.workflow.entity.WorkflowProcessDefinitionTaskFormConfigEntity;
import com.biolims.workflow.entity.WorkflowProcesssInstanceEntity;
import com.biolims.workflow.service.HistoryTaskService;
import com.biolims.workflow.service.WorkflowProcessInstanceService;

/**
 * 流程实例相关
 * @author cong
 *
 */
@Namespace("/workflow/processinstance")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class WorkflowProcessInstanceAction extends BaseActionSupport {

	private static final long serialVersionUID = -3188677511430451823L;

	//该action权限id
	private String rightsId = "workflowProcessInstance";

	@Resource
	private WorkflowProcessInstanceService workflowProcessInstanceService;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private HistoryTaskService historyTaskService;

	/**
	 * 查询流程实例
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showProcessInstanceList")
	public String showProcessInstanceList() throws Exception {
		return dispatcher("/WEB-INF/page/workflow/processinstance/showProcessInstanceList.jsp");
	}

	@Action(value = "showProcessInstanceJson")
	public void showProcessInstanceJson() throws Exception {
		String _startNum = getParameterFromRequest("start");
		String _limitNum = getParameterFromRequest("limit");
		// 开始记录数
		int startNum = Integer.parseInt((_startNum == null || _startNum.length() <= 0) ? 0 + "" : _startNum);
		// limit
		int limitNum = Integer.parseInt((_limitNum == null || _limitNum.length() <= 0) ? 10 + "" : _limitNum);
		// 排序方式
		String dir = getParameterFromRequest("dir");
		// 字段
		String sort = getParameterFromRequest("sort");
		String sendUser = getRequest().getParameter("sendUser");
		String formId = getParameterFromRequest("formId");
		String formName = getParameterFromRequest("formName");
		try {
			Map<String, Object> result = workflowProcessInstanceService.findProcessInstanceList(sendUser, formId, null,
					formName, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<WorkflowProcesssInstanceEntity> list = (List<WorkflowProcesssInstanceEntity>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "id");
			map.put("processInstanceId", "processInstanceId");
			map.put("applUserId", "applUserId");
			map.put("applUserName", "applUserName");
			map.put("formName", "formName");
			map.put("formTitle", "formTitle");
			map.put("tableName", "tableName");
			map.put("tablePath", "tablePath");
			map.put("startDate", "yyyy-MM-dd HH:mm:ss");
			map.put("endDate", "yyyy-MM-dd HH:mm:ss");
			map.put("businessKey", "businessKey");
			map.put("version", "version");
			map.put("isEnd", "isEnd");
			map.put("currentTaskName", "currentTaskName");
			map.put("currentTaskAssignee", "currentTaskAssignee");
			map.put("isSubProcess", "isSubProcess");
			map.put("parentProcessId", "parentProcessId");

			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 流程图跟综
	 * @throws Exception
	 */
	@Action(value = "traceProcessInstanceImage", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void traceProcessInstanceImage() throws Exception {

		String instanceId = getRequest().getParameter("instanceId");

		//根据流程实例id查询流程实例
		HistoricTaskInstance hti = workflowProcessInstanceService.getHistoryService().createHistoricTaskInstanceQuery()
				.processInstanceId(instanceId).list().get(0);

		//根据流程定义id查询流程定义
		ProcessDefinition processDefinition = workflowProcessInstanceService.getRepositoryService()
				.createProcessDefinitionQuery().processDefinitionId(hti.getProcessDefinitionId()).singleResult();

		String resourceName = processDefinition.getDiagramResourceName();

		//打开流程资源流

		InputStream resourceAsStream = workflowProcessInstanceService.getRepositoryService().getResourceAsStream(
				processDefinition.getDeploymentId(), resourceName);

		//输出到浏览器
		byte[] byteArray = IOUtils.toByteArray(resourceAsStream);
		ServletOutputStream servletOutputStream = getResponse().getOutputStream();
		servletOutputStream.write(byteArray, 0, byteArray.length);
		servletOutputStream.flush();
		servletOutputStream.close();
	}

	/**
	 * 跟综流程实例
	 * @throws Exception
	 */
	@Action(value = "toTraceProcessInstanceView", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String toTraceProcessInstanceView() throws Exception {
		String instanceId = getParameterFromRequest("instanceId");
		if(instanceId.equals("")){
			String formId = getRequest().getParameter("formId");
			ProcessInstance pi = workflowProcessInstanceService.getRuntimeService().createProcessInstanceQuery()
				.processInstanceBusinessKey(formId).singleResult();
			instanceId =  pi.getId();
		}
		getRequest().setAttribute("instanceId",instanceId);
		return dispatcher("/WEB-INF/page/workflow/processinstance/toTraceProcessInstanceView.jsp");
	}

	/**
	 * 获取流程各节点信息
	 * @throws Exception
	 */
	@Action(value = "traceProcessInstance", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void traceProcessInstance() throws Exception {
		try {
			String instanceId = getParameterFromRequest("instanceId");
			List<Map<String, Object>> result = workflowProcessInstanceService.traceProcessInstance(instanceId);
			HttpUtils.write(JsonUtils.toJsonString(result));
		} catch (Exception e) {
			HttpUtils.write(JsonUtils.toJsonString(new ArrayList<Map<String, Object>>()));
			e.printStackTrace();
		}
	}

	/**
	 * 跳转到启动页面
	 * @return
	 * @throws Exception
	 */
	@Action(value = "toStartView", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String toStartView() throws Exception {
		String formName = getRequest().getParameter("formName");

		WorkflowBindForm wfbf = workflowProcessInstanceService.getWorkflowBindForm(formName);
		ProcessDefinition pd = workflowProcessInstanceService.getRepositoryService().createProcessDefinitionQuery()
				.processDefinitionKey(wfbf.getProcessDefinitionKey()).latestVersion().singleResult();
		getRequest().setAttribute("deploymentId", pd.getDeploymentId());
		getRequest().setAttribute("diagramResourceName", pd.getDiagramResourceName());
		return dispatcher("/WEB-INF/page/workflow/processinstance/toStartView.jsp");
	}

	/**
	 * 跳转到启动页面
	 * @return
	 * @throws Exception
	 */
	@Action(value = "toStartViewByProcessKey", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String toStartViewByProcessKey() throws Exception {
		String processKey = getRequest().getParameter("processKey");

		ProcessDefinition pd = workflowProcessInstanceService.getRepositoryService().createProcessDefinitionQuery()
				.processDefinitionKey(processKey).latestVersion().singleResult();
		getRequest().setAttribute("deploymentId", pd.getDeploymentId());
		getRequest().setAttribute("diagramResourceName", pd.getDiagramResourceName());
		return dispatcher("/WEB-INF/page/workflow/processinstance/toStartView.jsp");
	}

	/**
	 * 启动工作流
	 * @throws Exception
	 */
	@Action(value = "start", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void start() throws Exception {
		String userId = getRequest().getParameter("userId");
		String userName = getRequest().getParameter("userName");
		String formId = getRequest().getParameter("formId");
		String formName = getRequest().getParameter("formName");
		String title = getRequest().getParameter("title");
		String dataJson = getRequest().getParameter("data");
		Map<String, Object> variables = new HashMap<String, Object>();
		if (dataJson != null && dataJson.length() > 0)
			variables = JsonUtils.toObjectByJson(dataJson, Map.class);

		Map<String, Boolean> result = new HashMap<String, Boolean>();
		try {
			workflowProcessInstanceService.start(userId, userName, formId, formName, title, variables);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 启动工作流
	 * @throws Exception
	 */
	@Action(value = "startByProcessKey", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void startByProcessKey() throws Exception {
		String userId = getRequest().getParameter("userId");
		String userName = getRequest().getParameter("userName");
		String formId = getRequest().getParameter("formId");
		String formName = getRequest().getParameter("formName");
		String processKey = getRequest().getParameter("processKey");
		String title = getRequest().getParameter("title");
		String dataJson = getRequest().getParameter("data");
		Map<String, Object> variables = new HashMap<String, Object>();
		if (dataJson != null && dataJson.length() > 0)
			variables = JsonUtils.toObjectByJson(dataJson, Map.class);

		Map<String, Boolean> result = new HashMap<String, Boolean>();
		try {
			workflowProcessInstanceService.startByProcessKey(userId, userName, formId, formName, processKey, title,
					variables);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 待办任务列表
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showToDoTaskList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showToDoTaskList() throws Exception {
		return dispatcher("/WEB-INF/page/workflow/processinstance/showToDoTaskList.jsp");
	}

	@Action(value = "showToDoTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showToDoTaskListJson() throws Exception {

		String userId = getRequest().getParameter("userId");
		String groupIds = getRequest().getParameter("groupIds");

		List<ProcessTaskBean> list = workflowProcessInstanceService.findToDoTaskListByUserId(userId,
				getGroupIdList(groupIds));
		Long total = Long.valueOf(list.size());
		Map<String, String> map = new HashMap<String, String>();
		map.put("taskId", "taskId");
		map.put("title", "title");
		map.put("formId", "formId");
		map.put("formName", "formName");
		map.put("taskName", "taskName");
		map.put("applUserName", "applUserName");
		map.put("startDate", "yyyy-MM-dd HH:mm:ss");
		new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
	}

	private List<String> getGroupIdList(String groupIds) throws Exception {
		List<String> list = null;
		if (groupIds != null && groupIds.trim().length() > 0) {
			list = new ArrayList<String>();
			String[] groupArray = groupIds.split(",");
			for (String id : groupArray) {
				if (id != null && id.trim().length() > 0) {
					list.add(id);
				}
			}
		}
		return list;
	}

	/**
	 * 获取表单对应的请求路径
	 * @throws Exception
	 */
	@Action(value = "getFormUrl", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void getFormUrl() throws Exception {
		String formName = getRequest().getParameter("formName");
		String taskId = getRequest().getParameter("taskId");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			TaskEntity t =  workflowProcessInstanceService.findTaskById(taskId);
			if(t!=null){
				String k = t.getTaskDefinitionKey();
				String s = t.getProcessDefinitionId();
				List<WorkflowProcessDefinitionTaskFormConfigEntity> l = commonDAO.find("from WorkflowProcessDefinitionTaskFormConfigEntity a where a.processDefinitionId='"+s+"' and a.activitiId='"+k+"'");
				if(l.size()>0){
				WorkflowProcessDefinitionTaskFormConfigEntity w = l.get(0);
					if(w!=null&&w.getFormName()!=null&&!w.getFormName().equals(""))
					formName = w.getFormName();
				}
			}
			ApplicationTypeTable att = workflowProcessInstanceService.getApplicationTypeTableByFormName(formName);
			
			
			result.put("data", att.getPathName());
			
			result.put("success", true);
			result.put("url", att.getPathName());
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 签收任务
	 * @throws Exception
	 */
	@Action(value = "claimTask", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void claimTask() throws Exception {
		String formId = getRequest().getParameter("formId");
		String userId = getRequest().getParameter("userId");

		Map<String, Object> result = new HashMap<String, Object>();
		try {
			workflowProcessInstanceService.claimTask(formId, userId);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
			result.put("error", e.getMessage());
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 完成页面
	 * @return
	 * @throws Exception
	 */
	@Action(value = "toCompleteTaskView", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String toCompleteTaskView() throws Exception {
		String taskId = getRequest().getParameter("taskId");
		String formId = getRequest().getParameter("formId");
		ProcessInstance pi = workflowProcessInstanceService.getRuntimeService().createProcessInstanceQuery()
				.processInstanceBusinessKey(formId).singleResult();

		if (pi == null) {
			//从子流程查找主流程
			WorkflowProcesssInstanceEntity wpe = workflowProcessInstanceService
					.getWorkflowProcesssInstanceEntityBySubProBizId(formId);
			pi = workflowProcessInstanceService.getRuntimeService().createProcessInstanceQuery()
					.processInstanceBusinessKey(wpe.getBusinessKey()).singleResult();
		}
		//拿到定义流Id
//		 = new HashMap<String, String>();
//		TaskEntity task = workflowProcessInstanceService.findTaskById(taskId);
//		if(!"".equals(task.getProcessDefinitionId())&&task.getProcessDefinitionId()!=null) {
//			if(task.getProcessDefinitionId().contains("SampleReceive")) {
//			 if("QA审核人审核".equals(task.getName())) {
////					operMap ={1=同意, 0=不同意}	;
//				
//					operMap.put("0", "不放行");
//					operMap.put("1", "放行");
//					}
//					
//			if("生产审核人审核".equals(task.getName())) {
////					operMap ={1=同意, 0=不同意}	;
//				  
//				    operMap.put("0", "不接收");
//				    operMap.put("1", "接收");
//					}
//			if("提交人修改".equals(task.getName()))	 {
//				operMap= workflowProcessInstanceService.findCompletOperEntityToMap(taskId);
//			}
//			}else {
		 Map<String, String> operMap= workflowProcessInstanceService.findCompletOperEntityToMap(taskId);
//
//			}
		

		List<WorkflowHistoryTask> historyTaskList = historyTaskService.findTaskVariablesListByInstanceId(pi.getId());
		getRequest().setAttribute("instanceId", pi.getId());
		getRequest().setAttribute("operMap", operMap);
		getRequest().setAttribute("historyTaskList", historyTaskList);
		return dispatcher("/WEB-INF/page/workflow/processinstance/toCompleteTaskView.jsp");
	}

	/**
	 * 完成
	 * @throws Exception
	 */
	@Action(value = "completeTask", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void completeTask() throws Exception {

		String data = getRequest().getParameter("data");
		String taskId = getRequest().getParameter("taskId");
		String userId = getRequest().getParameter("userId");
		
		String formId = getRequest().getParameter("formId");
		
		Map<String, Object> result = new HashMap<String, Object>();
		try {

			TaskEntity task = workflowProcessInstanceService.findTaskById(taskId);
			String assignee = task.getAssignee();
			if (assignee == null)
				workflowProcessInstanceService.getTaskService().claim(task.getId(), userId);
		    String a =task.getProcessDefinitionId();
			if(task.getProcessDefinitionId().contains("DeviationSurveyPlan")) {
				if("QA负责人".equals(task.getName())) {
					workflowProcessInstanceService.saveDeviationSurveyPlan(data,formId,"1");
				}else if("产品质量负责人".equals(task.getName())) {
					workflowProcessInstanceService.saveDeviationSurveyPlan(data,formId,"2");
				}else if("QA".equals(task.getName())) {
					workflowProcessInstanceService.saveDeviationSurveyPlan(data,formId,"3");
				}
				
			}

			workflowProcessInstanceService.completeTask(task, data, userId);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
			result.put("error", e.getMessage());
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 强行终止流程实例
	 * @throws Exception
	 */
	@Action(value = "stopProcessInstance")
	public void stopProcessInstance() throws Exception {
		String instanceId = getRequest().getParameter("instanceId");

		Map<String, Boolean> result = new HashMap<String, Boolean>();
		try {
			workflowProcessInstanceService.stopProcess(instanceId);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	@Action(value = "stopProcessInstanceByBKey", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void stopProcessInstanceByBKey() throws Exception {
		String bkey = getRequest().getParameter("bkey");

		Map<String, Boolean> result = new HashMap<String, Boolean>();
		try {
			WorkflowProcesssInstanceEntity wpie = workflowProcessInstanceService.findInstantIdByBKey(bkey);
			workflowProcessInstanceService.stopProcess(wpie.getProcessInstanceId());
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 转办
	 * @throws Exception
	 */
	@Action(value = "shiftHandleTask", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void shiftHandleTask() throws Exception {
		String taskId = getRequest().getParameter("taskId");
		String shiftUserId = getRequest().getParameter("shiftUserId");
		Map<String, Boolean> result = new HashMap<String, Boolean>();
		try {
			//workflowProcessInstanceService.transferAssignee(taskId, shiftUserId);
			workflowProcessInstanceService.shiftHandleTask(taskId, shiftUserId);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	
	
	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

}
