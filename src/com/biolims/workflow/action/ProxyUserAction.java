package com.biolims.workflow.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.workflow.entity.WorkflowProxyUserSiteEntity;
import com.biolims.workflow.service.ProxyUserService;

@Controller
@Scope("prototype")
@ParentPackage("default")
@Namespace("/workflow/proxy")
@SuppressWarnings("unchecked")
public class ProxyUserAction extends BaseActionSupport {

	private static final long serialVersionUID = -2126623949894126866L;

	//该action权限id
	private String rightsId = "workflowProxyUser";

	@Resource
	private ProxyUserService proxyUserService;

	/**
	 * 代办列表
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showProxyUserList")
	public String showProxyUserList() throws Exception {
		return dispatcher("/WEB-INF/page/workflow/proxy/showProxyUserList.jsp");
	}

	@Action(value = "showProxyUserListJson")
	public void showProxyUserListJson() throws Exception {
		List<WorkflowProxyUserSiteEntity> list = proxyUserService.findWorkflowProxyUserSiteEntity();
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "id");
		map.put("assignee", "assignee");
		map.put("proxyUserId", "proxyUserId");
		map.put("state", "state");

		new SendData().sendDateJson(map, list, list.size(), ServletActionContext.getResponse());
	}

	@Action(value = "saveProxyUser")
	public void saveProxyUser() throws Exception {
		Map<String, Boolean> result = new HashMap<String, Boolean>();
		try {
			String data = getRequest().getParameter("data");
			List<Map<String, String>> paramList = JsonUtils.toObjectByJson(data, List.class);
			proxyUserService.saveProxyUser(paramList);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	@Action(value = "deleteProxyUser")
	public void deleteProxyUser() throws Exception {
		Map<String, Boolean> result = new HashMap<String, Boolean>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			proxyUserService.delete(WorkflowProxyUserSiteEntity.class, ids);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}
}
