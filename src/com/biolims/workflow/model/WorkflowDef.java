package com.biolims.workflow.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;

/**
 * 状态
 * @author godcong
 *
 */
@Entity
@Table(name = "WORK_FLOW_DEF")
public class WorkflowDef extends EntityDao<WorkflowDef> implements Serializable {
	private static final long serialVersionUID = 1266645733315175911L;

	
	private String id;

	@Column(name = "NAME", length = 40)
	private String name;//名称
	
	@Column(name = "EN_NAME", length = 400)
	private String enName;//名称

	@Column(name = "STATE", length = 50)
	private String state;//状态
	/** 工作流状态 */
	private String stateName;
	
	/** 创建人 */
	private User createUser;
	/** 创建时间 */
	private Date createDate;

	@Column(name = "TYPE", length = 32)
	private String type;//类型

	@Column(name = "CLS", length = 200)
	private String cls;//类路径
	
	@Column(name = "OPER", length = 10)
	private String oper;//是非判断
	
	@Column(name = "NOTE", length = 200)
	private String note;//说明
	/**模块*/
	private String orderBlock;
	/**模块*/
	private String orderBlockName;
	/**英文名称*/
	private String orderBlockEnName;
	
	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}


	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * @return the cls
	 */
	public String getCls() {
		return cls;
	}

	/**
	 * @param cls the cls to set
	 */
	public void setCls(String cls) {
		this.cls = cls;
	}

	/**
	 * @return the oper
	 */
	public String getOper() {
		return oper;
	}

	/**
	 * @param oper the oper to set
	 */
	public void setOper(String oper) {
		this.oper = oper;
	}

	/**
	 * @return the stateName
	 */
	public String getStateName() {
		return stateName;
	}

	/**
	 * @param stateName the stateName to set
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
	 * @return the createUser
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return createUser;
	}

	/**
	 * @param createUser the createUser to set
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * @return the createDate
	 */
	@Column(name="CREATE_DATE")
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * @return the orderBlock
	 */
	@Column(name = "ORDER_BLOCK", length = 200)
	public String getOrderBlock() {
		return orderBlock;
	}

	/**
	 * @param orderBlock the orderBlock to set
	 */
	public void setOrderBlock(String orderBlock) {
		this.orderBlock = orderBlock;
	}

	/**
	 * @return the orderBlockName
	 */
	public String getOrderBlockName() {
		return orderBlockName;
	}

	/**
	 * @param orderBlockName the orderBlockName to set
	 */
	public void setOrderBlockName(String orderBlockName) {
		this.orderBlockName = orderBlockName;
	}

	/**
	 * @return the enName
	 */
	public String getEnName() {
		return enName;
	}

	/**
	 * @param enName the enName to set
	 */
	public void setEnName(String enName) {
		this.enName = enName;
	}

	/**
	 * @return the orderBlockEnName
	 */
	public String getOrderBlockEnName() {
		return orderBlockEnName;
	}

	/**
	 * @param orderBlockEnName the orderBlockEnName to set
	 */
	public void setOrderBlockEnName(String orderBlockEnName) {
		this.orderBlockEnName = orderBlockEnName;
	}

}
