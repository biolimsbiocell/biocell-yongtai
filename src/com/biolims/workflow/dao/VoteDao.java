package com.biolims.workflow.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.workflow.entity.VoteResultEntity;
import com.biolims.workflow.entity.WorkflowVoteRuleSetEntity;

@Repository
public class VoteDao extends BaseHibernateDao {

	public WorkflowVoteRuleSetEntity selectWorkflowVoteRuleSetEntityList(String processDefinitionId, String activitiId)
			throws Exception {
		String hql = "from WorkflowVoteRuleSetEntity where processDefinitionId='" + processDefinitionId
				+ "' and activitiId='" + activitiId + "'";
		return queryUniqueResult(hql);
	}

	public Long selectCountOperResultVote(String processInstanceId, String activitiId, String result, String userIds)
			throws Exception {
		String hql = "select count(*) from VoteResultEntity where processInstanceId = '" + processInstanceId
				+ "' and activitiId = '" + activitiId + "' and result='" + result + "'";
		if (userIds != null && userIds.length() > 0)
			hql = hql + " and operUserId in (" + userIds + ")";
		return queryUniqueResult(hql);
	}

	public List<VoteResultEntity> selectOperResultVoteList(String processInstanceId, String activitiId, String result,
			String userIds) throws Exception {
		String hql = "from VoteResultEntity where processInstanceId = '" + processInstanceId + "' and activitiId = '"
				+ activitiId + "' and result='" + result + "'";
		if (userIds != null && userIds.length() > 0)
			hql = hql + " and operUserId in (" + userIds + ")";
		return find(hql);
	}
}
