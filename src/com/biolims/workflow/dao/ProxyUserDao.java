package com.biolims.workflow.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.workflow.entity.WorkflowProxyUserSiteEntity;

@Repository
@SuppressWarnings("unchecked")
public class ProxyUserDao extends BaseHibernateDao {

	public List<WorkflowProxyUserSiteEntity> selectWorkflowProxyUserSiteEntity() throws Exception {
		String hql = " from WorkflowProxyUserSiteEntity";
		return find(hql);
	}
}
