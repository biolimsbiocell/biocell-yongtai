package com.biolims.workflow.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.workflow.entity.WorkflowHistoryTask;

@Repository
@SuppressWarnings("unchecked")
public class HistoryTaskDao extends BaseHibernateDao {

	public List<WorkflowHistoryTask> selectTaskVariablesList(String propertyName, String propertyVal) throws Exception {
		String hql = "from WorkflowHistoryTask where " + propertyName + " = '" + propertyVal
				+ "' order by modifyTime asc";
		return find(hql);
	}
}
