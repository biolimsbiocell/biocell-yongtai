package com.biolims.workflow.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.workflow.model.WorkflowDef;

@Repository
@SuppressWarnings("unchecked")
public class WorkflowDefDao extends BaseHibernateDao {

	public Map<String, Object> showWorkflowDefTableJson(String orderBlock, Integer start, Integer length, String query,
			String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from WorkflowDef where 1=1";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		if (orderBlock != null && !"".equals(orderBlock)) {
			key += " and (orderBlock='" + orderBlock + "' or orderBlock is null or orderBlock='')";
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from WorkflowDef where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				if (col.indexOf("-") != -1) {
					col = col.replace("-", ".");
				}
				key += " order by " + col + " " + sort;
			}
			List<WorkflowDef> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;

	}

}
