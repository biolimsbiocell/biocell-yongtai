package com.biolims.workflow.dao;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.workflow.entity.WorkflowProcessDefinitionEntity;
import com.biolims.workflow.model.WorkflowProcess;

@Repository
@SuppressWarnings("unchecked")
public class WorkflowProcessDao extends BaseHibernateDao {

	public Map<String, Object> showWorkflowProcessJson(Integer start,
			Integer length, String query, String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from WorkflowProcess where 1=1";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from WorkflowProcess where 1=1";
//			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
//				key+=" order by "+col+" "+sort;
//			}
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				if (col.indexOf("-") != -1) {
					col = col.replace("-", ".");
				}
				if (col.equals("id")) {
					key += " order by createDate desc ";
				} else {
					key += " order by " + col + " " + sort;
				}
			}
			List<WorkflowProcess> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	
	}

	public String viewWorkflowProcess(String tableId) {
		String hql=" from WorkflowProcessDefinitionEntity where 1=1 and key='"+tableId+"' order by version desc";
		List<WorkflowProcessDefinitionEntity> list=new ArrayList<WorkflowProcessDefinitionEntity>();
		list=this.getSession().createQuery(hql).list();
		if(list.size()>0){
			return list.get(0).getWorkflowProcessId();
		}
		return null;
	}
	
}
