package com.biolims.workflow.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.workflow.entity.WorkflowProcesssInstanceEntity;

@Repository
@SuppressWarnings("unchecked")
public class WorkflowPCCheckDao extends BaseHibernateDao {

	public Map<String, Object> selectProcessInstance(String sendUser, String assignee, String formId, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {

		String key = "";
		String hql = " from WorkflowProcesssInstanceEntity where 1=1 ";
		if (sendUser != null && sendUser.length() > 0)
			key = key + "and applUserId='" + sendUser + "'";
		if (formId != null && formId.length() > 0)
			key = key + " and businessKey='" + formId + "'";
		if (assignee != null && assignee.length() > 0) {
			key = key + " and processInstanceId in (select instanceId from WorkflowHistoryTask where assignee = '"
					+ assignee + "')";
		}

		List<WorkflowProcesssInstanceEntity> list = new ArrayList<WorkflowProcesssInstanceEntity>();
		Long total = queryUniqueResult("select count(*) " + hql + key);
		if (total > 0) {
			if (sort != null && sort.length() > 0 && dir != null && dir.length() > 0) {
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by startDate asc";
			}
			hql = hql + key;
			if (startNum != null && limitNum != null) {
				list = this.queryList(hql, startNum, limitNum);
			} else {
				list = find(hql);
			}

		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);

		return result;
	}

	public Map<String, Object> selectProcessInstance(String sendUser, String assignee, String formId, String formName,
			Integer startNum, Integer limitNum, String dir, String sort) throws Exception {

		String key = "";
		String hql = " from WorkflowProcesssInstanceEntity where 1=1 ";
		if (sendUser != null && sendUser.length() > 0)
			key = key + "and applUserId='" + sendUser + "'";
		if (formId != null && formId.length() > 0)
			key = key + " and businessKey='" + formId + "'";
		if (assignee != null && assignee.length() > 0) {
			key = key + " and processInstanceId in (select instanceId from WorkflowHistoryTask where assignee = '"
					+ assignee + "')";
		}
		if (formName != null && formName.length() > 0) {
			key = key + " and formName = '" + formName + "')";
		}
		List<WorkflowProcesssInstanceEntity> list = new ArrayList<WorkflowProcesssInstanceEntity>();
		Long total = queryUniqueResult("select count(*) " + hql + key);
		if (total > 0) {
			if (sort != null && sort.length() > 0 && dir != null && dir.length() > 0) {
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by startDate asc";
			}
			hql = hql + key;
			if (startNum != null && limitNum != null) {
				list = this.queryList(hql, startNum, limitNum);
			} else {
				list = find(hql);
			}

		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);

		return result;
	}
}
