package com.biolims.workflow.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.workflow.entity.WorkflowBindForm;
import com.biolims.workflow.entity.WorkflowProcessDefinitionEntity;
import com.biolims.workflow.entity.WorkflowProcessDefinitionTaskFormConfigEntity;
import com.biolims.workflow.entity.WorkflowProcessDefinitionTaskGroupConfigEntity;
import com.biolims.workflow.entity.WorkflowProcessDefinitionTaskUserConfigEntity;

@Repository
@SuppressWarnings("unchecked")
public class WorkflowProcessDefinitionDao extends BaseHibernateDao {

	public Map<String, Object> selectProcessDefinitionList(String name, String defKey, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String hql = "from WorkflowProcessDefinitionEntity where 1=1";
		String key = "";
		if (name != null && name.trim().length() > 0)
			key = key + " and name like '%" + name + "%'";
		if (defKey != null && defKey.trim().length() > 0)
			key = key + " and key like '%" + defKey + "%'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<WorkflowProcessDefinitionEntity> list = new ArrayList<WorkflowProcessDefinitionEntity>();
		if (total > 0) {
			if (sort != null && sort.length() > 0 && dir != null && dir.length() > 0) {
				key = key + " order by " + sort + " " + dir;
			} else {
				key = key + " order by deploymentTime asc";
			}

			hql = hql + key;
			if (startNum != null && limitNum != null) {
				list = this.getSession().createQuery(hql).setFirstResult(startNum).setMaxResults(limitNum).list();
			} else {
				list = this.getSession().createQuery(hql).list();
			}
		}

		Map<String, Object> result = new HashMap<String, Object>();

		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public List<WorkflowProcessDefinitionTaskUserConfigEntity> selectWorkflowProcessDefinitionTaskUserConfigEntityByKey(
			String definitionId, String activitiId) throws Exception {
		String hql = "from WorkflowProcessDefinitionTaskUserConfigEntity where processDefinitionId = '" + definitionId
				+ "'";
		if (activitiId != null)
			hql = hql + " and activitiId='" + activitiId + "'";
		return find(hql);
	}

	public List<WorkflowProcessDefinitionTaskFormConfigEntity> selectWorkflowProcessDefinitionTaskFormConfigEntityByKey(
			String definitionId, String activitiId) throws Exception {
		String hql = "from WorkflowProcessDefinitionTaskFormConfigEntity where processDefinitionId = '" + definitionId
				+ "'";
		if (activitiId != null)
			hql = hql + " and activitiId='" + activitiId + "'";
		return find(hql);
	}

	public List<WorkflowProcessDefinitionTaskGroupConfigEntity> selecWorkflowProcessDefinitionTaskGroupConfigEntityByKey(
			String definitionId, String activitiId) throws Exception {
		String hql = "from WorkflowProcessDefinitionTaskGroupConfigEntity where processDefinitionId = '" + definitionId
				+ "'";
		if (activitiId != null)
			hql = hql + " and activitiId='" + activitiId + "'";
		return find(hql);
	}

	public void deleteTaskGroupConfigByProcessDefinitionId(String definitionId) throws Exception {
		String hql = "delete from WorkflowProcessDefinitionTaskGroupConfigEntity where processDefinitionId = '"
				+ definitionId + "'";
		this.getSession().createQuery(hql).executeUpdate();
	}

	public void deleteTaskUserConfigByProcessDefinitionId(String definitionId) throws Exception {
		String hql = "delete from WorkflowProcessDefinitionTaskUserConfigEntity where processDefinitionId = '"
				+ definitionId + "'";
		this.getSession().createQuery(hql).executeUpdate();
	}

	public WorkflowProcessDefinitionEntity selectWorkflowProcessDefinitionEntityByDefId(String defId) throws Exception {
		String hql = "from WorkflowProcessDefinitionEntity where processDefinitionId='" + defId + "'";
		return queryUniqueResult(hql);
	}

	public WorkflowBindForm findWorkflowBindFormById(String tableId) {
		String hql = "from WorkflowBindForm where formName='" + tableId + "'";
		return queryUniqueResult(hql);
	}

}
