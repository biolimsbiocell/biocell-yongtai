package com.biolims.workflow.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.applicationType.model.ApplicationTypeTable;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.workflow.entity.CompletOperEntity;
import com.biolims.workflow.entity.WorkflowBindForm;
import com.biolims.workflow.entity.WorkflowProxyUserSiteEntity;

@Repository
@SuppressWarnings("unchecked")
public class WorkflowDao extends BaseHibernateDao {

	public WorkflowBindForm selectWorkflowBindForm(String formName) throws Exception {
		String hql = "from WorkflowBindForm where formName='" + formName + "'";
		return this.queryUniqueResult(hql);
	}

	public WorkflowBindForm selectWorkflowBindFormByDefinitionKey(String definitionKey) throws Exception {
		String hql = "from WorkflowBindForm where processDefinitionKey='" + definitionKey + "'";
		return this.queryUniqueResult(hql);
	}

	public Map<String, Object> selectWorkflowBindFormList(Integer start, Integer limit, String dir, String sort) {
		String hql = "from WorkflowBindForm ";

		Long total = (Long) this.getSession().createQuery("select count(*) " + hql).uniqueResult();
		String key = "";
		List<WorkflowBindForm> list = new ArrayList<WorkflowBindForm>();
		if (total > 0) {
			if (sort != null && dir != null)
				key = key + " orderby " + dir + " " + sort;

			hql = hql + key;
			if (start != null && limit != null) {
				list = this.getSession().createQuery(hql).setMaxResults(start).setMaxResults(limit).list();
			} else {
				list = this.getSession().createQuery(hql).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public List<WorkflowProxyUserSiteEntity> selectWorkflowProxyUserSiteEntityList(String state) throws Exception {
		String hql = "from WorkflowProxyUserSiteEntity where 1=1";
		if (state != null)
			hql = hql + " and state='" + state + "'";
		return find(hql);
	}

	public ApplicationTypeTable selectApplicationTypeTableByFormName(String formName) throws Exception {
		String hql = "from ApplicationTypeTable where id='" + formName + "'";
		return this.queryUniqueResult(hql);
	}

	public List<CompletOperEntity> selectCompletOperEntity(String activitiId, String definitionId) throws Exception {
		String hql = "from CompletOperEntity where activitiId='" + activitiId + "' and processDefinitionId='"
				+ definitionId + "' order by orderNumber ASC";
		return find(hql);
	}

	public List<CompletOperEntity> selectCompletOperEntityByOper(String activitiId, String definitionId, String oper)
			throws Exception {
		String hql = "from CompletOperEntity where activitiId='" + activitiId + "' and processDefinitionId='"
				+ definitionId + "' and operValue ='" + oper + "' order by orderNumber ASC";
		return find(hql);
	}
}
