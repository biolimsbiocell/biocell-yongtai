package com.biolims.workflow.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.biolims.applicationType.model.ApplicationTypeTable;
import com.biolims.workflow.WorkflowConstants;
import com.biolims.workflow.dao.WorkflowPCCheckDao;
import com.biolims.workflow.entity.WorkflowProcesssInstanceEntity;

@Service
@SuppressWarnings("unchecked")
public class WorkflowPCCheckService {

	@Resource
	private WorkflowPCCheckDao workflowPCCheckDao;

	public Map<String, Object> findProcessInstance(String sendUser, String assignee, String formId, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = workflowPCCheckDao.selectProcessInstance(sendUser, assignee, formId, startNum,
				limitNum, dir, sort);
		List<WorkflowProcesssInstanceEntity> list = (List<WorkflowProcesssInstanceEntity>) result.get("list");
		if (list != null && list.size() > 0) {
			for (WorkflowProcesssInstanceEntity instance : list) {

				if (instance.getFormName() != null && !instance.getFormName().equals("")) {
					ApplicationTypeTable at = workflowPCCheckDao
							.get(ApplicationTypeTable.class, instance.getFormName());
					instance.setTableName(at.getName());
				}
				if (WorkflowConstants.WORKFLOW_RUNNING.equals(instance.getIsEnd())) {
					instance.setIsEnd(WorkflowConstants.WORKFLOW_RUNNING_NAME);
				} else if (WorkflowConstants.WORKFLOW_COMPLETE.equals(instance.getIsEnd())) {
					instance.setIsEnd(WorkflowConstants.WORKFLOW_COMPLETE_NAME);
				}

				if (WorkflowConstants.IS_SUB_PROCESS_YES.equals(instance.getIsSubProcess())) {
					instance.setIsSubProcess(WorkflowConstants.IS_SUB_PROCESS_YES_NAME);
				} else {
					instance.setIsSubProcess(WorkflowConstants.IS_SUB_PROCESS_NO_NAME);
				}
			}
		}
		return result;
	}

	public Map<String, Object> findProcessInstance(String sendUser, String assignee, String formId, String formName,
			Integer startNum, Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> result = workflowPCCheckDao.selectProcessInstance(sendUser, assignee, formId, formName,
				startNum, limitNum, dir, sort);
		List<WorkflowProcesssInstanceEntity> list = (List<WorkflowProcesssInstanceEntity>) result.get("list");
		if (list != null && list.size() > 0) {
			for (WorkflowProcesssInstanceEntity instance : list) {
				if (instance.getFormName() != null && !instance.getFormName().equals("")) {
					ApplicationTypeTable at = workflowPCCheckDao
							.get(ApplicationTypeTable.class, instance.getFormName());
					instance.setTableName(at.getName());
					instance.setTablePath((at.getPathName() + instance.getBusinessKey()).replace("@@", "&"));
				}
				if (WorkflowConstants.WORKFLOW_RUNNING.equals(instance.getIsEnd())) {
					instance.setIsEnd(WorkflowConstants.WORKFLOW_RUNNING_NAME);
				} else if (WorkflowConstants.WORKFLOW_COMPLETE.equals(instance.getIsEnd())) {
					instance.setIsEnd(WorkflowConstants.WORKFLOW_COMPLETE_NAME);
				}

				if (WorkflowConstants.IS_SUB_PROCESS_YES.equals(instance.getIsSubProcess())) {
					instance.setIsSubProcess(WorkflowConstants.IS_SUB_PROCESS_YES_NAME);
				} else {
					instance.setIsSubProcess(WorkflowConstants.IS_SUB_PROCESS_NO_NAME);
				}
			}
		}
		return result;
	}
}
