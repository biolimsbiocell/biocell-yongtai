package com.biolims.workflow.service;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.workflow.dao.WorkflowProcessDao;
import com.biolims.workflow.model.WorkflowProcess;

@Service
public class WorkflowProcessService {
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private WorkflowProcessDao workflowProcessDao;


	public WorkflowProcess get(String id) {
		WorkflowProcess wd=commonDAO.get(WorkflowProcess.class, id);
		return wd;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(WorkflowProcess workflowProcess) {
		workflowProcessDao.saveOrUpdate(workflowProcess);
	}

	public Map<String, Object> showWorkflowProcessJson(Integer start,
			Integer length, String query, String col, String sort) throws Exception {
		return workflowProcessDao.showWorkflowProcessJson(start, length, query, col, sort);
	}
	/**
	 * 
	 * @Title: changeState  
	 * @Description: 发布工作流  
	 * @author : shengwei.wang
	 * @date 2018年3月9日下午6:25:01
	 * @param id
	 * void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String id) {
		WorkflowProcess wp=commonDAO.get(WorkflowProcess.class, id);
		wp.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		wp.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		workflowProcessDao.saveOrUpdate(wp);
	}
	public String viewWorkflowProcess(String tableId) {
		return workflowProcessDao.viewWorkflowProcess(tableId);
	}
}
