package com.biolims.workflow.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.activiti.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.workflow.dao.VoteDao;
import com.biolims.workflow.dao.WorkflowProcessDefinitionDao;
import com.biolims.workflow.entity.VoteResultEntity;
import com.biolims.workflow.entity.WorkflowProcessDefinitionTaskUserConfigEntity;
import com.biolims.workflow.entity.WorkflowVoteRuleSetEntity;

@Service
public class VoteService extends WorkflowService {

	@Resource
	private VoteDao voteDao;

	@Resource
	private WorkflowProcessDefinitionDao workflowProcessDefinitionDao;

	/**
	 *  查询会签设置规则
	 * @param processDefinitionId
	 * @param activitiId
	 * @return
	 * @throws Exception
	 */
	public WorkflowVoteRuleSetEntity getWorkflowVoteRuleSetEntity(String processDefinitionId, String activitiId)
			throws Exception {
		return voteDao.selectWorkflowVoteRuleSetEntityList(processDefinitionId, activitiId);
	}

	/**
	 * 获取指定流程的指定节点的会签指定结果数
	 * @param processInstanceId实例ID
	 * @param activitiId任务节点ID
	 * @param result 结果  "true"同意   "false"不同意
	 * @param userIds 用户ID:'a','b'  null查询所有
	 * @return
	 * @throws Exception
	 */
	public Long getCountOperResultVote(String processInstanceId, String activitiId, String result, String userIds)
			throws Exception {
		return voteDao.selectCountOperResultVote(processInstanceId, activitiId, result, userIds);
	}

	@Transactional(rollbackFor = Exception.class)
	public void updateResultVote(String processInstanceId, String activitiId, String result, String userIds)
			throws Exception {
		List<VoteResultEntity> list = voteDao.selectOperResultVoteList(processInstanceId, activitiId, result, userIds);
		for (VoteResultEntity vre : list) {
			vre.setResult("1");
			voteDao.saveOrUpdate(vre);
		}
	}

	/**
	 * 获取路由设置中的用户信息
	 * @param execution
	 * @return
	 * @throws Exception
	 */
	public List<String> getVoteUsers(DelegateExecution execution) throws Exception {
		List<String> result = new ArrayList<String>();
		String activitiId = execution.getCurrentActivityId();
		String processDefinitionId = execution.getProcessDefinitionId();
		List<WorkflowProcessDefinitionTaskUserConfigEntity> userList = workflowProcessDefinitionDao
				.selectWorkflowProcessDefinitionTaskUserConfigEntityByKey(processDefinitionId, activitiId);
		if (userList != null && userList.size() > 0) {
			for (WorkflowProcessDefinitionTaskUserConfigEntity user : userList) {
				result.add(user.getUserId());
			}
		}
		return result;
	}

}
