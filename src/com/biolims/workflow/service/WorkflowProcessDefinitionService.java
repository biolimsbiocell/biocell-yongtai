package com.biolims.workflow.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.activiti.engine.impl.RepositoryServiceImpl;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.workflow.WorkflowConstants;
import com.biolims.workflow.bean.ActivitieBean;
import com.biolims.workflow.dao.WorkflowProcessDefinitionDao;
import com.biolims.workflow.entity.CompletOperEntity;
import com.biolims.workflow.entity.WorkflowBindForm;
import com.biolims.workflow.entity.WorkflowProcessDefinitionEntity;
import com.biolims.workflow.entity.WorkflowProcessDefinitionTaskFormConfigEntity;
import com.biolims.workflow.entity.WorkflowProcessDefinitionTaskGroupConfigEntity;
import com.biolims.workflow.entity.WorkflowProcessDefinitionTaskUserConfigEntity;

@Service
@SuppressWarnings("unchecked")
public class WorkflowProcessDefinitionService extends WorkflowService {

	@Resource
	private WorkflowProcessDefinitionDao workflowProcessDefinitionDao;

	/**
	 * 查询流程定义
	 * @return
	 * @throws Excepiton
	 */
	public Map<String, Object> findProcessDefinitionList(String name, String key, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		Map<String, Object> result = workflowProcessDefinitionDao.selectProcessDefinitionList(name, key, startNum,
				limitNum, dir, sort);
		List<WorkflowProcessDefinitionEntity> list = (List<WorkflowProcessDefinitionEntity>) result.get("list");
		if (list != null && list.size() > 0) {
			for (WorkflowProcessDefinitionEntity def : list) {
				String isMailRemind = def.getIsMailRemind();
				if (WorkflowConstants.MAIL_REMIND_NO.equals(isMailRemind)) {
					def.setIsMailRemind(WorkflowConstants.MAIL_REMIND_NO_NAME);
				} else if (WorkflowConstants.MAIL_REMIND_YES.equals(isMailRemind)) {
					def.setIsMailRemind(WorkflowConstants.MAIL_REMIND_YES_NAME);
				}
			}
		}
		return result;

	}

	/**
	 * 保存流程定义
	 * @param deploymentId
	 * @throws Exception
	 */
	@Transactional(rollbackFor = Exception.class)
	public void saveProcessDefinitionEntity(String id,Deployment deployment) throws Exception {
		ProcessDefinition pd = getRepositoryService().createProcessDefinitionQuery().deploymentId(deployment.getId())
				.singleResult();
		WorkflowProcessDefinitionEntity wfpde = new WorkflowProcessDefinitionEntity();
		wfpde.setProcessDefinitionId(pd.getId());
		wfpde.setDeploymentId(deployment.getId());
		wfpde.setName(pd.getName());
		wfpde.setKey(pd.getKey());
		wfpde.setWorkflowProcessId(id);
		wfpde.setVersion(pd.getVersion());
		wfpde.setResourceName(pd.getResourceName());
		wfpde.setDiagramResourceName(pd.getDiagramResourceName());
		wfpde.setDeploymentTime(deployment.getDeploymentTime());
		workflowProcessDefinitionDao.save(wfpde);
	}

	/**
	 * 查询流程定义各节点信息
	 * @param definitionId
	 * @return
	 * @throws Exception
	 */
	public List<ActivitieBean> findProcessDefinitionByDefinitionId(String definitionId) throws Exception {
		List<ActivitieBean> result = new ArrayList<ActivitieBean>();

		ProcessDefinitionEntity pde = (ProcessDefinitionEntity) ((RepositoryServiceImpl) getRepositoryService())
				.getDeployedProcessDefinition(definitionId);
		//获取该流程定义的所有节点
		List<ActivityImpl> activitiList = pde.getActivities();//获得当前任务的所有节点
		for (ActivityImpl activiti : activitiList) {
			String id = activiti.getId();
			String name = (String) activiti.getProperty("name");
			String type = (String) activiti.getProperty("type");
			ActivitieBean ab = new ActivitieBean();
			ab.setId(id);
			ab.setName(name);
			ab.setType(type);

			Map<String, Map<String, String>> map = getUserTaskConfigUserByDefinitionId(id, definitionId);
			Map<String, String> userMap = map.get("user");
			if (userMap != null && userMap.size() > 0) {
				ab.setUserIds(userMap.get("userIds"));
				ab.setUserNames(userMap.get("userNames"));
			}
			Map<String, String> formMap = map.get("form");
			if (formMap != null && formMap.size() > 0) {
				ab.setFormNames(formMap.get("formNames"));
			}
			Map<String, String> groupMap = map.get("group");
			if (groupMap != null && groupMap.size() > 0) {
				ab.setGroupIds(groupMap.get("groupIds"));
				ab.setGroupNames(groupMap.get("groupNames"));
			}
			result.add(ab);
		}
		return result;
	}

	/**
	 * 获取流程定义任务配置的用户、用户组
	 * @param activitiId
	 * @param definitionId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Map<String, String>> getUserTaskConfigUserByDefinitionId(String activitiId, String definitionId)
			throws Exception {
		Map<String, Map<String, String>> result = new HashMap<String, Map<String, String>>();

		List<WorkflowProcessDefinitionTaskUserConfigEntity> tuceList = workflowProcessDefinitionDao
				.selectWorkflowProcessDefinitionTaskUserConfigEntityByKey(definitionId, activitiId);
		Map<String, String> tuceMap = parseTuce(tuceList);
		result.put("user", tuceMap);

		List<WorkflowProcessDefinitionTaskGroupConfigEntity> tgceList = workflowProcessDefinitionDao
				.selecWorkflowProcessDefinitionTaskGroupConfigEntityByKey(definitionId, activitiId);
		Map<String, String> tgceMap = parseTgce(tgceList);
		result.put("group", tgceMap);

		List<WorkflowProcessDefinitionTaskFormConfigEntity> formList = workflowProcessDefinitionDao
				.selectWorkflowProcessDefinitionTaskFormConfigEntityByKey(definitionId, activitiId);
		Map<String, String> formMap = parseForm(formList);
		result.put("form", formMap);
		return result;
	}

	/*
	 * 解析流程定义任务用户
	 */
	private Map<String, String> parseTuce(List<WorkflowProcessDefinitionTaskUserConfigEntity> tuceList)
			throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		if (tuceList != null && tuceList.size() > 0) {
			String userIds = "";
			String userNames = "";
			String formNames = "";
			for (WorkflowProcessDefinitionTaskUserConfigEntity tuce : tuceList) {
				userIds = userIds + tuce.getUserId() + ",";
				userNames = userNames + tuce.getUserName() + ",";
			}
			map.put("userIds", userIds);
			map.put("userNames", userNames);
		}
		return map;
	}

	private Map<String, String> parseForm(List<WorkflowProcessDefinitionTaskFormConfigEntity> tuceList)
			throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		if (tuceList != null && tuceList.size() > 0) {
			String formNames = "";
			for (WorkflowProcessDefinitionTaskFormConfigEntity tuce : tuceList) {
				formNames = formNames + tuce.getFormName() + ",";
			}
			map.put("formNames", formNames);
		}
		return map;
	}

	/*
	 * 解析流程定义任务用户组
	 */
	private Map<String, String> parseTgce(List<WorkflowProcessDefinitionTaskGroupConfigEntity> tugeList)
			throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		if (tugeList != null && tugeList.size() > 0) {
			String groupIds = "";
			String groupNames = "";
			for (WorkflowProcessDefinitionTaskGroupConfigEntity tuge : tugeList) {
				groupIds = groupIds + tuge.getGroupId() + ",";
				groupNames = groupNames + tuge.getGroupName() + ",";
			}
			map.put("groupIds", groupIds);
			map.put("groupNames", groupNames);
		}
		return map;
	}

	/**
	 * 获取流程定义用户任务配置用户信息
	 * @param activitiId
	 * @return
	 * @throws Exception
	 */
	public List<WorkflowProcessDefinitionTaskUserConfigEntity> findWorkflowProcessDefinitionTaskUserConfigEntityByKey(
			String definitionId, String activitiId) throws Exception {
		return workflowProcessDefinitionDao.selectWorkflowProcessDefinitionTaskUserConfigEntityByKey(definitionId,
				activitiId);
	}

	public List<WorkflowProcessDefinitionTaskFormConfigEntity> findWorkflowProcessDefinitionTaskFormConfigEntityByKey(
			String definitionId, String activitiId) throws Exception {
		return workflowProcessDefinitionDao.selectWorkflowProcessDefinitionTaskFormConfigEntityByKey(definitionId,
				activitiId);
	}

	/**
	 * 获取流程定义用户任务配置用户组信息
	 * @param activitiId
	 * @return
	 * @throws Exception
	 */
	public List<WorkflowProcessDefinitionTaskGroupConfigEntity> findWorkflowProcessDefinitionTaskGroupConfigEntityByKey(
			String definitionId, String activitiId) throws Exception {
		return workflowProcessDefinitionDao.selecWorkflowProcessDefinitionTaskGroupConfigEntityByKey(definitionId,
				activitiId);
	}

	/**
	 * 保存各用户任务节点设置的用户和用户组信息
	 * @param paramMaps
	 * @throws Exception
	 */
	@Transactional(rollbackFor = Exception.class)
	public void saveDefinitionActivitisConfigUser(Map<String, Object> paramMaps) throws Exception {
		String definitionId = (String) paramMaps.get("definitionId");
		List<Map<String, String>> paramList = (List<Map<String, String>>) paramMaps.get("data");
		workflowProcessDefinitionDao.deleteTaskGroupConfigByProcessDefinitionId(definitionId);
		workflowProcessDefinitionDao.deleteTaskUserConfigByProcessDefinitionId(definitionId);
		for (Map<String, String> paramMap : paramList) {
			String id = paramMap.get("id");
			String userIds = paramMap.get("userIds");
			String userNams = paramMap.get("userNames");
			String groupIds = paramMap.get("groupIds");
			String groupNames = paramMap.get("groupNames");
			String formNames = paramMap.get("formNames");

			if (groupIds != null && groupIds.trim().length() > 0) {

				String[] groupIdArray = groupIds.split(",");
				String[] groupNameArray = groupNames.split(",");
				for (int i = 0; i < groupIdArray.length; i++) {
					WorkflowProcessDefinitionTaskGroupConfigEntity tce = new WorkflowProcessDefinitionTaskGroupConfigEntity();
					tce.setActivitiId(id);
					tce.setProcessDefinitionId(definitionId);
					tce.setGroupId(groupIdArray[i]);
					tce.setGroupName(groupNameArray[i]);
					workflowProcessDefinitionDao.save(tce);
				}

			}

			if (userIds != null && userIds.trim().length() > 0) {
				String[] userIdArray = userIds.split(",");
				String[] userNameArray = userNams.split(",");

				for (int i = 0; i < userIdArray.length; i++) {

					WorkflowProcessDefinitionTaskUserConfigEntity tuce = new WorkflowProcessDefinitionTaskUserConfigEntity();
					tuce.setActivitiId(id);
					tuce.setProcessDefinitionId(definitionId);

					tuce.setUserId(userIdArray[i]);
					tuce.setUserName(userNameArray[i]);

					workflowProcessDefinitionDao.save(tuce);

				}
			}
			if (formNames != null && formNames.trim().length() > 0) {

				String[] formNameArray = formNames.split(",");

				for (int i = 0; i < formNameArray.length; i++) {
					WorkflowProcessDefinitionTaskFormConfigEntity tForm = new WorkflowProcessDefinitionTaskFormConfigEntity();
					tForm.setActivitiId(id);
					tForm.setProcessDefinitionId(definitionId);
					tForm.setFormName(formNameArray[i]);
					workflowProcessDefinitionDao.save(tForm);
				}

			}
		}

	}

	/**
	 * 保存表单绑定
	 * @param paramList
	 * @throws Exception
	 */
	@Transactional(rollbackFor = Exception.class)
	public void saveBindForm(List<Map<String, String>> paramList) throws Exception {
		for (Map<String, String> paramMap : paramList) {
			String id = paramMap.get("id");
			String formName = paramMap.get("formName");
			String processDefinitionKey = paramMap.get("processDefinitionKey");
			WorkflowBindForm bindForm = new WorkflowBindForm();
			bindForm.setId(id);
			bindForm.setFormName(formName);
			bindForm.setProcessDefinitionKey(processDefinitionKey);
			workflowProcessDefinitionDao.saveOrUpdate(bindForm);
		}
	}

	@Transactional(rollbackFor = Exception.class)
	public void saveOperButton(List<Map<String, String>> paramList) throws Exception {
		for (Map<String, String> paramMap : paramList) {
			String id = paramMap.get("id");
			String operTitle = paramMap.get("operTitle");
			String operValue = paramMap.get("operValue");
			String activitiId = paramMap.get("activitiId");
			String definitionId = paramMap.get("definitionId");
			String orderNumber = paramMap.get("orderNumber");
			CompletOperEntity oper = new CompletOperEntity();
			oper.setId(id);
			oper.setOperTitle(operTitle);
			oper.setOperValue(operValue);
			oper.setProcessDefinitionId(definitionId);
			oper.setActivitiId(activitiId);
			oper.setOrderNumber(Integer.valueOf(orderNumber));
			workflowProcessDefinitionDao.saveOrUpdate(oper);
		}
	}

	@Transactional(rollbackFor = Exception.class)
	public void setMailRemind(String id) throws Exception {
		WorkflowProcessDefinitionEntity def = get(WorkflowProcessDefinitionEntity.class, id);
		if (WorkflowConstants.MAIL_REMIND_NO.equals(def.getIsMailRemind())) {
			def.setIsMailRemind(WorkflowConstants.MAIL_REMIND_YES);
		} else if (WorkflowConstants.MAIL_REMIND_YES.equals(def.getIsMailRemind())) {
			def.setIsMailRemind(WorkflowConstants.MAIL_REMIND_NO);
		}
		workflowProcessDefinitionDao.update(def);
	}

	public WorkflowProcessDefinitionEntity getWorkflowProcessDefinitionEntityByDefId(String defId) throws Exception {
		return workflowProcessDefinitionDao.selectWorkflowProcessDefinitionEntityByDefId(defId);
	}
	/**
	 * 
	 * @Title: saveWorkflowBindForm  
	 * @Description: 保存配置表单 
	 * @author : shengwei.wang
	 * @date 2018年4月11日下午2:00:25
	 * @param tableId
	 * void
	 * @throws
	 */
	@Transactional(rollbackFor = Exception.class)
	public void saveWorkflowBindForm(String tableId) {
	WorkflowBindForm wbf=workflowProcessDefinitionDao.findWorkflowBindFormById(tableId);
		if(wbf==null){
			wbf=new WorkflowBindForm();
			wbf.setFormName(tableId);
			wbf.setProcessDefinitionKey(tableId);
			workflowProcessDefinitionDao.saveOrUpdate(wbf);
		}
	}
}
