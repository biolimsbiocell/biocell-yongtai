package com.biolims.workflow.service;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.applicationType.model.ApplicationTypeTable;
import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.workflow.dao.WorkflowDefDao;
import com.biolims.workflow.model.WorkflowDef;

@Service
public class WorkflowDefService {
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private WorkflowDefDao workflowDefDao;

	public Map<String, Object> showWorkflowDefTableJson(String orderBlock,Integer start,
			Integer length, String query, String col, String sort) throws Exception {
		return workflowDefDao.showWorkflowDefTableJson(orderBlock,start, length, query, col, sort);
	}

	public WorkflowDef get(String id) {
		WorkflowDef wd=commonDAO.get(WorkflowDef.class, id);
		return wd;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(WorkflowDef workflowDef) {
	String blockId=	workflowDef.getOrderBlock();
	if(blockId!=null&&!"".equals(blockId)){
		ApplicationTypeTable att=commonDAO.get(ApplicationTypeTable.class, blockId);
		workflowDef.setOrderBlockName(att.getName());
		workflowDef.setOrderBlockEnName(att.getEnName());
	}
		workflowDefDao.saveOrUpdate(workflowDef);
	}

}
