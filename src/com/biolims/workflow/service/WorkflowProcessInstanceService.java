package com.biolims.workflow.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.impl.RepositoryServiceImpl;
import org.activiti.engine.impl.bpmn.behavior.UserTaskActivityBehavior;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.activiti.engine.impl.pvm.delegate.ActivityBehavior;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.applicationType.model.ApplicationTypeTable;
import com.biolims.applicationType.service.ApplicationTypeService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.deviation.plan.model.DeviationSurveyPlan;
import com.biolims.experiment.changeplan.model.ChangePlan;
import com.biolims.util.JsonUtils;
import com.biolims.workflow.WorkflowConstants;
import com.biolims.workflow.bean.ProcessTaskBean;
import com.biolims.workflow.dao.WorkflowProcessInstanceDao;
import com.biolims.workflow.entity.WorkflowBindForm;
import com.biolims.workflow.entity.WorkflowHistoryTask;
import com.biolims.workflow.entity.WorkflowProcessDefinitionTaskFormConfigEntity;
import com.biolims.workflow.entity.WorkflowProcessDefinitionTaskUserConfigEntity;
import com.biolims.workflow.entity.WorkflowProcesssInstanceEntity;
import com.biolims.workflow.entity.WorkflowProxyUserSiteEntity;
import com.biolims.workflow.util.WorkflowUtils;

import net.sf.json.JSON;

@Service
@SuppressWarnings("unchecked")
public class WorkflowProcessInstanceService extends WorkflowService {

	@Resource
	private WorkflowProcessInstanceDao workflowProcessInstanceDao;

	@Resource
	private WorkflowProcessDefinitionService workflowProcessDefinitionService;

	@Resource
	private ApplicationTypeService applicationTypeService;
	
	@Resource
	private CommonDAO commonDAO;

	/**
	 * 查询实例
	 * 
	 * @throws Exception
	 */
	public Map<String, Object> findProcessInstanceList(String userId, String formId, String state, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {

		Map<String, Object> result = workflowProcessInstanceDao.selectProcessInstance(userId, formId, state, startNum,
				limitNum, dir, sort);
		List<WorkflowProcesssInstanceEntity> list = (List<WorkflowProcesssInstanceEntity>) result.get("list");
		if (list != null && list.size() > 0) {
			for (WorkflowProcesssInstanceEntity instance : list) {
				if (WorkflowConstants.WORKFLOW_RUNNING.equals(instance.getIsEnd())) {
					instance.setIsEnd(WorkflowConstants.WORKFLOW_RUNNING_NAME);
				} else if (WorkflowConstants.WORKFLOW_COMPLETE.equals(instance.getIsEnd())) {
					instance.setIsEnd(WorkflowConstants.WORKFLOW_COMPLETE_NAME);
				}

				if (WorkflowConstants.IS_SUB_PROCESS_YES.equals(instance.getIsSubProcess())) {
					instance.setIsSubProcess(WorkflowConstants.IS_SUB_PROCESS_YES_NAME);
				} else {
					instance.setIsSubProcess(WorkflowConstants.IS_SUB_PROCESS_NO_NAME);
				}
			}
		}
		return result;
	}

	/**
	 * 查询实例
	 * 
	 * @throws Exception
	 */
	public Map<String, Object> findProcessInstanceList(String userId, String formId, String state, String formName,
			Integer startNum, Integer limitNum, String dir, String sort) throws Exception {

		Map<String, Object> result = workflowProcessInstanceDao.selectProcessInstance(userId, formId, state, formName,
				startNum, limitNum, dir, sort);
		List<WorkflowProcesssInstanceEntity> list = (List<WorkflowProcesssInstanceEntity>) result.get("list");
		if (list != null && list.size() > 0) {

			for (WorkflowProcesssInstanceEntity instance : list) {

				if (instance.getFormName() != null && !instance.getFormName().equals("")) {
					ApplicationTypeTable at = workflowProcessInstanceDao.get(ApplicationTypeTable.class, instance
							.getFormName());
					instance.setTableName(at.getName());
					instance.setTablePath((at.getPathName() + instance.getBusinessKey()).replace("@@", "&"));
				}
				if (WorkflowConstants.WORKFLOW_RUNNING.equals(instance.getIsEnd())) {
					instance.setIsEnd(WorkflowConstants.WORKFLOW_RUNNING_NAME);
				} else if (WorkflowConstants.WORKFLOW_COMPLETE.equals(instance.getIsEnd())) {
					instance.setIsEnd(WorkflowConstants.WORKFLOW_COMPLETE_NAME);
				}

				if (WorkflowConstants.IS_SUB_PROCESS_YES.equals(instance.getIsSubProcess())) {
					instance.setIsSubProcess(WorkflowConstants.IS_SUB_PROCESS_YES_NAME);
				} else {
					instance.setIsSubProcess(WorkflowConstants.IS_SUB_PROCESS_NO_NAME);
				}
			}
		}
		return result;
	}

	/**
	 * 跟踪流程图
	 * 
	 * @param instanceId
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, Object>> traceProcessInstance(String instanceId) throws Exception {
		Execution execution = getRuntimeService().createExecutionQuery().executionId(instanceId).singleResult();// 执行实例
		Object property = PropertyUtils.getProperty(execution, "activityId");
		String activityId = null;
		if (property != null)
			activityId = property.toString();

		// 查询流程实例
		ProcessInstance processInstance = getRuntimeService().createProcessInstanceQuery()
				.processInstanceId(instanceId).singleResult();
		// 根据流程实例id查询流程定义
		ProcessDefinitionEntity processDefinition = (ProcessDefinitionEntity) ((RepositoryServiceImpl) getRepositoryService())
				.getDeployedProcessDefinition(processInstance.getProcessDefinitionId());
		// 获取该流程定义的所有节点
		List<ActivityImpl> activitiList = processDefinition.getActivities();// 获得当前任务的所有节点
		List<Map<String, Object>> activityInfos = new ArrayList<Map<String, Object>>();
		for (ActivityImpl activity : activitiList) {

			boolean isCurrentActiviti = false;
			String id = activity.getId();

			// 判断是否是当前节点
			if (id.equals(activityId))
				isCurrentActiviti = true;

			Map<String, Object> activityImageInfo = packageSingleActivitiInfo(activity, processInstance,
					isCurrentActiviti);

			activityInfos.add(activityImageInfo);
		}

		return activityInfos;
	}

	/*
	 * 封装输出信息，包括：当前节点的X、Y坐标、变量信息、任务类型、任务描述
	 * 
	 * @param activity
	 * 
	 * @param processInstance
	 * 
	 * @param currentActiviti
	 * 
	 * @return
	 */
	private Map<String, Object> packageSingleActivitiInfo(ActivityImpl activity, ProcessInstance processInstance,
			boolean isCurrentActiviti) throws Exception {
		Map<String, Object> vars = new HashMap<String, Object>();
		Map<String, Object> activityInfo = new HashMap<String, Object>();
		activityInfo.put("isCurrentActiviti", isCurrentActiviti);

		activityInfo.put("x", activity.getX());
		activityInfo.put("y", activity.getY());

		activityInfo.put("width", activity.getWidth());
		activityInfo.put("height", activity.getHeight());

		Map<String, Object> properties = activity.getProperties();
		vars.put("任务类型", WorkflowUtils.parseToZhType(properties.get("type").toString()));
		activityInfo.put("type", properties.get("type").toString());
		ActivityBehavior activityBehavior = activity.getActivityBehavior();
		if (activityBehavior instanceof UserTaskActivityBehavior) {

			List<Task> currentTask = null;

			/*
			 * 当前节点的task
			 */
			if (isCurrentActiviti)
				currentTask = getCurrentTaskInfo(processInstance);

			// 当前处理人
			if (currentTask != null) {
				String currentUsers = "";
				for (Task task : currentTask) {
					if (task.getAssignee() != null)
						currentUsers = currentUsers + task.getAssignee() + ",";
				}

				vars.put("当前处理人", currentUsers);
			}
			/*
			 * 当前任务的分配角色
			 */
			// UserTaskActivityBehavior userTaskActivityBehavior =
			// (UserTaskActivityBehavior) activityBehavior;
			// TaskDefinition taskDefinition = userTaskActivityBehavior
			// .getTaskDefinition();

			// 候选人
			// Set<Expression> candidateUserIdExpressions = taskDefinition
			// .getCandidateUserIdExpressions();
			// if (!candidateUserIdExpressions.isEmpty())
			// vars.put("候选人", getExpression(candidateUserIdExpressions));

			// 候选角色
			// Set<Expression> candidateGroupIdExpressions = taskDefinition
			// .getCandidateGroupIdExpressions();
			// if (!candidateGroupIdExpressions.isEmpty())
			// vars.put("候选角色", getExpression(candidateGroupIdExpressions));

			Map<String, Map<String, String>> map = workflowProcessDefinitionService
					.getUserTaskConfigUserByDefinitionId(activity.getId(), processInstance.getProcessDefinitionId());
			Map<String, String> userMap = map.get("user");
			if (userMap != null && userMap.size() > 0) {
				vars.put("候选人", userMap.get("userNames"));
			}

			Map<String, String> groupMap = map.get("group");
			if (groupMap != null && groupMap.size() > 0) {
				vars.put("候选角色", groupMap.get("groupIds"));
			}

		}

		vars.put("节点说明", properties.get("documentation"));

		String description = activity.getProcessDefinition().getDescription();
		vars.put("描述", description);

		activityInfo.put("vars", vars);
		return activityInfo;
	}

	//	private String getExpression(Set<Expression> expressions) throws Exception {
	//		String result = "";
	//		for (Expression e : expressions) {
	//			String text = e.getExpressionText();
	//			result += text + ",";
	//		}
	//		return result;
	//	}
	public String findInstanceIdById(String id) throws Exception {
		List<Map<String, Object>> bList = workflowProcessInstanceDao
				.findForJdbc("select t.id_ from ACT_HI_PROCINST t  where  t.business_key_ = '" + id + "'");
		String instanceId = "";
		if (bList.size() > 0) {

			Map a = bList.get(0);
			instanceId = (String) a.get("id_");

		}
		return instanceId;
	}

	public String findTaskNameById(String id) throws Exception {
		List<Map<String, Object>> bList = workflowProcessInstanceDao
				.findForJdbc("select t.name_ from ACT_RU_TASK t  where  t.id_ = '" + id + "'");
		String instanceId = "";
		if (bList.size() > 0) {

			Map a = bList.get(0);
			instanceId = (String) a.get("name_");

		}
		return instanceId;
	}

	/**
	 * 根据表单名称查询流程定义KEY
	 * 
	 * @param formName
	 * @return
	 * @throws Exception
	 */
	public WorkflowBindForm getWorkflowBindForm(String formName) throws Exception {
		return super.getWorkflowBindForm(formName);
	}

	/**
	 * 启动工作流
	 * 
	 * @throws Exception
	 */
	@Transactional(rollbackFor = Exception.class)
	public void start(String userId, String userName, String formId, String formName, String title,
			Map<String, Object> variables) throws Exception {
		WorkflowBindForm wfbf = super.getWorkflowBindForm(formName);

		getIdentityService().setAuthenticatedUserId(userId);

		ProcessInstance processInstance = null;
		if (variables.size() > 0) {
			processInstance = getRuntimeService().startProcessInstanceByKey(wfbf.getProcessDefinitionKey(), formId,
					variables);
		} else {
			processInstance = getRuntimeService().startProcessInstanceByKey(wfbf.getProcessDefinitionKey(), formId);
		}
		ProcessDefinition pd = getRepositoryService().createProcessDefinitionQuery().processDefinitionId(
				processInstance.getProcessDefinitionId()).singleResult();
		WorkflowProcesssInstanceEntity wpie = new WorkflowProcesssInstanceEntity();
		wpie.setApplUserId(userId);
		wpie.setApplUserName(userName);
		wpie.setFormName(formName);
		wpie.setFormTitle(title);
		wpie.setBusinessKey(formId);
		wpie.setProcessInstanceId(processInstance.getId());
		wpie.setProcessDefinitionId(processInstance.getProcessDefinitionId());
		wpie.setVersion(pd.getVersion());
		wpie.setStartDate(new Date());
		workflowProcessInstanceDao.save(wpie);
	}

	/**
	 * 启动工作流
	 * 
	 * @throws Exception
	 */
	@Transactional(rollbackFor = Exception.class)
	public void startByProcessKey(String userId, String userName, String formId, String formName, String processKey,
			String title, Map<String, Object> variables) throws Exception {

		getIdentityService().setAuthenticatedUserId(userId);

		ProcessInstance processInstance = null;
		if (variables.size() > 0) {
			processInstance = getRuntimeService().startProcessInstanceByKey(processKey, formId, variables);
		} else {
			processInstance = getRuntimeService().startProcessInstanceByKey(processKey, formId);
		}
		ProcessDefinition pd = getRepositoryService().createProcessDefinitionQuery().processDefinitionId(
				processInstance.getProcessDefinitionId()).singleResult();
		WorkflowProcesssInstanceEntity wpie = new WorkflowProcesssInstanceEntity();
		wpie.setApplUserId(userId);
		wpie.setApplUserName(userName);
		wpie.setFormName(formName);
		wpie.setFormTitle(title);
		wpie.setBusinessKey(formId);
		wpie.setProcessInstanceId(processInstance.getId());
		wpie.setProcessDefinitionId(processInstance.getProcessDefinitionId());
		wpie.setVersion(pd.getVersion());
		wpie.setStartDate(new Date());
		workflowProcessInstanceDao.save(wpie);
	}

	/**
	 * 根据实例 ID查询流程实例实体
	 * 
	 * @param instanceId
	 * @return
	 * @throws Exception
	 */
	public WorkflowProcesssInstanceEntity getWorkflowProcesssInstanceEntityByInstanceId(String instanceId)
			throws Exception {
		return workflowProcessInstanceDao.selectWorkflowProcesssInstanceEntityByInstanceId(instanceId);
	}

	/**
	 * 根据用户ID查询待办任务
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public List<ProcessTaskBean> findToDoTaskListByUserId(String userId, List<String> groupList) throws Exception {

		List<ProcessTaskBean> result = new ArrayList<ProcessTaskBean>();
		List<ProcessTaskBean> resultSort = new ArrayList<ProcessTaskBean>();
		// 存放当前用户的所有任务
		List<Task> tasks = new ArrayList<Task>();
		if (userId != null && userId.length() > 0) {
			// 根据当前用户的id查询代办任务列表(已经签收)
			List<Task> taskAssignees = getTaskService().createTaskQuery().taskAssignee(userId).orderByTaskCreateTime()
					.desc().list();
			// 根据当前用户id查询未签收的任务列表
			List<Task> taskCandidates = getTaskService().createTaskQuery().taskCandidateUser(userId)
					.orderByTaskCreateTime().desc().list();

			tasks.addAll(taskAssignees);
			tasks.addAll(taskCandidates);
		}
		if (groupList != null && groupList.size() > 0) {
			List<Task> taskGroupCandidates = getTaskService().createTaskQuery().taskCandidateGroupIn(groupList)
					.orderByTaskPriority().desc().orderByTaskCreateTime().desc().list();
			tasks.addAll(taskGroupCandidates);
		}
		if (tasks.size() > 0) {
			for (Task task : tasks) {
				ProcessTaskBean ptb = new ProcessTaskBean();
				String processInstanceId = task.getProcessInstanceId();
				WorkflowProcesssInstanceEntity wpie = workflowProcessInstanceDao
						.selectWorkflowProcesssInstanceEntityByInstanceId(processInstanceId);
				ptb.setTaskId(task.getId());
				if (wpie == null || WorkflowConstants.IS_SUB_PROCESS_YES.equals(wpie.getIsSubProcess())
						|| task.getTaskDefinitionKey().toUpperCase().startsWith("SUB-")) {
					// 子流程查出最主流程
					wpie = workflowProcessInstanceDao
							.selectWorkflowProcesssInstanceEntityByInstanceId(getRootProcessInstance(processInstanceId));
					Object formName = getRuntimeService().getVariable(task.getExecutionId(), "formName");
					Object formId = getRuntimeService().getVariable(task.getExecutionId(), "formId");

					if (formName != null && formId != null) {
						ptb.setFormId(formId.toString());
						ptb.setFormName(formName.toString());
					} else {
						ptb.setFormId(wpie.getBusinessKey());
						ptb.setFormName(wpie.getFormName());
					}
				} else {

					String formName = wpie.getFormName();
					String formId = wpie.getBusinessKey();

					List<WorkflowProcessDefinitionTaskFormConfigEntity> taskList = workflowProcessDefinitionService
							.findWorkflowProcessDefinitionTaskFormConfigEntityByKey(wpie.getProcessDefinitionId(), task
									.getTaskDefinitionKey());

					if (taskList.size() > 0) {
						WorkflowProcessDefinitionTaskFormConfigEntity we = (WorkflowProcessDefinitionTaskFormConfigEntity) taskList
								.get(0);

						if (we.getFormName() != null) {

							formName = (String) getRuntimeService().getVariable(task.getExecutionId(), "formName");
							formId = (String) getRuntimeService().getVariable(task.getExecutionId(), "formId");

						}
					}

					ptb.setFormId(formId);
					ptb.setFormName(formName);
				}

				/*		ApplicationTypeTable ata = applicationTypeService.getApplicationTypeTable(ptb.getFormName());

						if (ata.getClassPath() != null && !ata.getClassPath().equals("")) {

							Object o = Class.forName(ata.getClassPath()).newInstance();

							o = workflowProcessInstanceDao.get(o.getClass(), ptb.getFormId());

							try {
								BeanUtils.getDeclaredField(o, "state");
								Object stateN = BeanUtils.getFieldValue(o, "stateName");

								ptb.setStateName((String) stateN);

							} catch (NoSuchFieldException n) {

							}

						}*/
				ptb.setApplUserName(wpie.getApplUserName());
				ptb.setApplUserId(wpie.getApplUserId());
				ptb.setTitle(wpie.getFormTitle());
				ptb.setStartDate(wpie.getStartDate());
				ptb.setTaskName(task.getName());
				result.add(ptb);
			}
		}
		Collections.sort(result, new Comparator() {
			public int compare(Object a, Object b) {
				Date one = ((ProcessTaskBean) a).getStartDate();
				Date two = ((ProcessTaskBean) b).getStartDate();
				return two.compareTo(one);
			}
		});
		return result;
	}

	private String getRootProcessInstance(String processInstanceId) throws Exception {
		HistoricProcessInstance pi = getHistoryService().createHistoricProcessInstanceQuery().processInstanceId(
				processInstanceId).singleResult();
		String superProcessInstanceId = pi.getSuperProcessInstanceId();
		if (superProcessInstanceId != null) {
			return getRootProcessInstance(superProcessInstanceId);
		}
		return pi.getId();
	}

	/**
	 * 根据表单ID签收当前执行的任务
	 * 
	 * @param formId
	 * @throws Exception
	 */
	public void claimTask(String formId, String userId) throws Exception {
		ProcessInstance pi = getRuntimeService().createProcessInstanceQuery().processInstanceBusinessKey(formId)
				.singleResult();
		Task task = getCurrentTaskInfo(pi).get(0);
		getTaskService().claim(task.getId(), userId);
	}

	/**
	 * 完成任务
	 * 
	 * @param task
	 *            任务
	 * @param paramMap
	 *            表单值
	 * @throws Exception
	 */
	@Transactional(rollbackFor = Exception.class)
	public void completeTask(TaskEntity task, String data, String userId) throws Exception {

		if (data != null && data.length() > 0) {
			Date modifyTime = new Date();
			WorkflowHistoryTask his = new WorkflowHistoryTask();
			his.setTaskId(task.getId());
			his.setTaskName(task.getName());
			his.setInstanceId(task.getProcessInstanceId());
			his.setDefinitionId(task.getProcessDefinitionId());
			his.setModifyTime(modifyTime);
			String assignee = task.getAssignee();
			if (assignee == null)
				assignee = userId;
			his.setAssignee(assignee);
			his.setPropertyInfo(data);

			// 代办设置
			String owner = task.getOwner();
			if (owner != null) {
				Map<String, String> proxyUserMap = parseProxyUser(findWorkflowProxyUserSiteEntityList("1"));
				String _assignee = proxyUserMap.get(his.getAssignee());
				if (_assignee != null) {
					his.setProxyInfo(his.getAssignee() + " 代 " + _assignee + " 办理");
				}
			}

			workflowProcessInstanceDao.save(his);
			Map<String, Object> paramMap = JsonUtils.toObjectByJson(data, Map.class);
			getTaskService().complete(task.getId(), paramMap);
		} else {
			getTaskService().complete(task.getId());
		}
	}

	private Map<String, String> parseProxyUser(List<WorkflowProxyUserSiteEntity> proxyUserList) throws Exception {
		Map<String, String> result = new HashMap<String, String>();
		if (proxyUserList != null && proxyUserList.size() > 0) {
			for (WorkflowProxyUserSiteEntity proxyUser : proxyUserList) {
				result.put(proxyUser.getProxyUserId(), proxyUser.getAssignee());
			}
		}
		return result;
	}

	/**
	 * 转办任务并记录历史
	 * @param taskId
	 * @param shiftUserId
	 * @throws Exception
	 */
	@Transactional(rollbackFor = Exception.class)
	public void shiftHandleTask(String taskId, String shiftUserId) throws Exception {
		TaskEntity task = findTaskById(taskId);

		WorkflowHistoryTask his = new WorkflowHistoryTask();
		his.setTaskId(task.getId());
		his.setTaskName(task.getName());
		his.setInstanceId(task.getProcessInstanceId());
		his.setDefinitionId(task.getProcessDefinitionId());
		his.setModifyTime(new Date());
		String assignee = task.getAssignee();
		his.setAssignee(assignee);
		his.setProxyInfo(his.getAssignee() + " 转办给 " + shiftUserId + " 办理");
		workflowProcessInstanceDao.save(his);
		transferAssignee(taskId, shiftUserId);
	}

	/**
	 * 根据子流程业务ID查找主流程
	 * @param subProBizId
	 * @return
	 * @throws Exception
	 */
	public WorkflowProcesssInstanceEntity getWorkflowProcesssInstanceEntityBySubProBizId(String subProBizId)
			throws Exception {
		return workflowProcessInstanceDao.selectWorkflowProcesssInstanceEntityBySubProBizId(subProBizId);
	}

	public String findToDoTaskNameByFormId(String formId) throws Exception {

		List<ProcessTaskBean> result = new ArrayList<ProcessTaskBean>();
		List<ProcessTaskBean> resultSort = new ArrayList<ProcessTaskBean>();
		// 存放当前用户的所有任务
		String taskName = "";
		List<Task> tasks = getTaskService().createTaskQuery().processInstanceBusinessKey(formId)
				.orderByTaskCreateTime().desc().list();

		if (tasks.size() > 0) {
			taskName = tasks.get(0).getName();
		}

		return taskName;
	}

	public String findTaskIdById(String id) throws Exception {
		List<Map<String, Object>> bList = workflowProcessInstanceDao
				.findForJdbc("select a.id_ from ACT_HI_PROCINST t ,act_ru_task a where a.proc_inst_id_ = t.id_ and t.business_key_ = '"
						+ id + "'");
		String taskId = "";
		if (bList.size() > 0) {

			Map a = bList.get(0);
			taskId = (String) a.get("id_");

		}
		return taskId;
	}

	public String findWpAdminById(String id) throws Exception {
		List<Map<String, Object>> bList = workflowProcessInstanceDao
				.findForJdbc("select b.WP_ADMIN_ID from ACT_HI_PROCINST t ,act_ru_task a,T_BPMN_PD_ENT b where a.proc_inst_id_ = t.id_ and b.ACT_PROCESS_DEFINITION_ID = t.PROC_DEF_ID_ and t.business_key_ = '"
						+ id + "'");
		String adminId = "";
		if (bList.size() > 0) {

			Map a = bList.get(0);
			adminId = (String) a.get("WP_ADMIN_ID");

		}
		return adminId;
	}

	public WorkflowProcesssInstanceEntity findInstantIdByBKey(String bKey) throws Exception {
		WorkflowProcesssInstanceEntity wpib = workflowProcessInstanceDao
				.selectWorkflowProcesssInstanceEntityByBkey(bKey);
		return wpib;
	}

	public void saveDeviationSurveyPlan(String data, String formId,String index) throws Exception {
		DeviationSurveyPlan dsp = commonDAO.get(DeviationSurveyPlan.class, formId);
		if(dsp!=null) {
			if("1".equals(index)) {
				Map<String, Object> paramMap = JsonUtils.toObjectByJson(data, Map.class);
				String a = (String) paramMap.get("info");
				if(a!=null&&a!="") {
					dsp.setAuditOpinion(dsp.getAuditOpinion()+a);
					commonDAO.saveOrUpdate(dsp);
				}
			}else if("2".equals(index)) {
				Map<String, Object> paramMap = JsonUtils.toObjectByJson(data, Map.class);
				String a = (String) paramMap.get("info");
				if(a!=null
						&&a!="") {
					dsp.setApprovalOpinion(dsp.getApprovalOpinion()+a);
					commonDAO.saveOrUpdate(dsp);
				}
			}else if("3".equals(index)) {
				Map<String, Object> paramMap = JsonUtils.toObjectByJson(data, Map.class);
				String a = (String) paramMap.get("info");
				if(a!=null
						&&a!="") {
					dsp.setAuditQaOpinion(dsp.getAuditQaOpinion()+a);
					commonDAO.saveOrUpdate(dsp);
				}
			}
		}
	}
	

}
