package com.biolims.workflow.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.activiti.engine.HistoryService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.springframework.stereotype.Service;

import com.biolims.common.model.user.User;
import com.biolims.util.JsonUtils;
import com.biolims.workflow.WorkflowConstants;
import com.biolims.workflow.dao.HistoryTaskDao;
import com.biolims.workflow.dao.WorkflowDao;
import com.biolims.workflow.entity.CompletOperEntity;
import com.biolims.workflow.entity.WorkflowHistoryTask;

@Service
@SuppressWarnings("unchecked")
public class HistoryTaskService extends WorkflowService {

	@Resource
	private HistoryTaskDao historyTaskDao;

	@Resource
	private WorkflowDao workflowDao;
	@Resource
	private HistoryService historyService;

	/**
	 * 根据流程实例ID查询任务变量
	 * @param instanceId
	 * @return
	 * @throws Exception
	 */

	public List<WorkflowHistoryTask> findTaskVariablesListByInstanceId(String instanceId) throws Exception {

		List<WorkflowHistoryTask> list = historyTaskDao.selectTaskVariablesList("instanceId", instanceId);
		if (list != null && list.size() > 0) {
			for (WorkflowHistoryTask his : list) {
				if (his.getAssignee() != null) {
					User u = historyTaskDao.get(User.class, his.getAssignee());
					his.setAssigneeName(u.getName());
				}
				String propertyInfo = his.getPropertyInfo();
				if (propertyInfo != null && propertyInfo.length() > 0) {
					Map<String, Object> paramMap = JsonUtils.toObjectByJson(propertyInfo, Map.class);
					String oper = (String) paramMap.get(WorkflowConstants.RESULT_IS_OPER_PROPERTY_KEY);

					if (oper != null && oper.length() > 0) {

						List<CompletOperEntity> listOper = workflowDao.selectCompletOperEntityByOper(historyService
								.createHistoricTaskInstanceQuery().taskId(his.getTaskId()).singleResult()
								.getTaskDefinitionKey(), historyService.createHistoricTaskInstanceQuery().taskId(
								his.getTaskId()).singleResult().getProcessDefinitionId(), oper);

						if (listOper.size() == 0) {
							if (oper.equals(WorkflowConstants.RESULT_IS_OPER_PROPERTY_VALUE_TRUE)) {
								his.setOperResult("同意");
							} else if (oper.equals(WorkflowConstants.RESULT_IS_OPER_PROPERTY_VALUE_FALSE)) {
								his.setOperResult("不同意");
							}
						} else {
							his.setOperResult(listOper.get(0).getOperTitle());
						}
					}
					String info = (String) paramMap.get("info");
					if (info != null && info.length() > 0)
						his.setInfo(info);
				}
			}
		}

		return list;
	}

	/**"
	 * 根据FormId查询流程实例ID
	 * @param formId
	 * @return
	 * @throws Exception
	 */
	public String getIntanceIdByFormId(String formId) throws Exception {
		HistoricProcessInstance hpi = getHistoryService().createHistoricProcessInstanceQuery()
				.processInstanceBusinessKey(formId).singleResult();
		return hpi.getId();
	}

}
