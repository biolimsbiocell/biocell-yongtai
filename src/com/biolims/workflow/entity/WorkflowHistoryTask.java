package com.biolims.workflow.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

/**
 * 历史任务
 * @author cong
 *
 */
@Entity
@Table(name = "T_BPMN_HISTORY_TASK")
public class WorkflowHistoryTask implements Serializable {

	private static final long serialVersionUID = 3413933301238180819L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@Column(name = "TASK_ID", length = 40)
	private String taskId;

	@Column(name = "TASK_NAME", length = 50)
	private String taskName;

	@Column(name = "INSTANCE_ID", length = 40)
	private String instanceId;

	@Column(name = "DEFINITION_ID", length = 40)
	private String definitionId;

	@Column(name = "MODIFY_TIME")
	private Date modifyTime;

	@Column(name = "PROPERTY_INFO", length = 2000)
	private String propertyInfo;

	@Column(name = "ASSIGNEE_", length = 40)
	private String assignee;//任务办理人

	@Column(name = "PROXY_INFO", length = 40)
	private String proxyInfo;//代办/转办信息

	@Transient
	private String info;//办理意见
	@Transient
	private String operResult;//办理结果
	@Transient
	private String assigneeName;//办理人姓名

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public String getDefinitionId() {
		return definitionId;
	}

	public void setDefinitionId(String definitionId) {
		this.definitionId = definitionId;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	public String getPropertyInfo() {
		return propertyInfo;
	}

	public void setPropertyInfo(String propertyInfo) {
		this.propertyInfo = propertyInfo;
	}

	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getOperResult() {
		return operResult;
	}

	public void setOperResult(String operResult) {
		this.operResult = operResult;
	}

	public String getProxyInfo() {
		return proxyInfo;
	}

	public void setProxyInfo(String proxyInfo) {
		this.proxyInfo = proxyInfo;
	}

	public String getAssigneeName() {
		return assigneeName;
	}

	public void setAssigneeName(String assigneeName) {
		this.assigneeName = assigneeName;
	}

}
