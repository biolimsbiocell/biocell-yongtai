package com.biolims.workflow.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "T_BPMN_COMPLET_OPER_ENT")
public class CompletOperEntity implements Serializable {

	private static final long serialVersionUID = -2594144756670429043L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@Column(name = "PROCESS_DEFINITION_ID", length = 32)
	private String processDefinitionId;

	@Column(name = "ACTIVITI_ID", length = 40)
	private String activitiId;

	@Column(name = "OPER_TITLE", length = 20)
	private String operTitle;

	@Column(name = "OPER_VALUE", length = 3)
	private String operValue;

	private Integer orderNumber;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

	public String getActivitiId() {
		return activitiId;
	}

	public void setActivitiId(String activitiId) {
		this.activitiId = activitiId;
	}

	public String getOperTitle() {
		return operTitle;
	}

	public void setOperTitle(String operTitle) {
		this.operTitle = operTitle;
	}

	public String getOperValue() {
		return operValue;
	}

	public void setOperValue(String operValue) {
		this.operValue = operValue;
	}

	public Integer getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}

}
