package com.biolims.workflow.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.biolims.workflow.WorkflowConstants;

/**
 * 流程实例
 * @author cong
 *
 */
@Entity
@Table(name = "T_BPMN_PI_ENT")
public class WorkflowProcesssInstanceEntity implements Serializable {

	private static final long serialVersionUID = -4979622891817906927L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@Column(name = "PROCESS_DEFINITION_ID", length = 40)
	private String processDefinitionId;

	@Column(name = "PROCESS_INSTANCE_ID", length = 40)
	private String processInstanceId;

	@Column(name = "APPL_USER_ID", length = 40)
	private String applUserId;

	@Column(name = "APPL_USER_NAME", length = 40)
	private String applUserName;

	@Column(name = "FORM_NAME", length = 40)
	private String formName;

	@Column(name = "FORM_TITLE", length = 400)
	private String formTitle;

	@Column(name = "BUSINESS_KEY", length = 40)
	private String businessKey;

	@Column(name = "START_DATE")
	private Date startDate;

	@Column(name = "END_DATE")
	private Date endDate;

	@Column(name = "IS_END", length = 2)
	private String isEnd = WorkflowConstants.WORKFLOW_RUNNING;

	@Column(name = "IS_SUB_PROCESS", length = 2)
	private String isSubProcess = WorkflowConstants.IS_SUB_PROCESS_NO;

	@Column(name = "parent_Process_Id", length = 40)
	private String parentProcessId;//主流程ID

	@Column(name = "VERSION_")
	private int version;

	@Transient
	private String currentTaskName;

	@Transient
	private String currentTaskAssignee;

	@Transient
	private String tableName;

	@Transient
	private String tablePath;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getApplUserId() {
		return applUserId;
	}

	public void setApplUserId(String applUserId) {
		this.applUserId = applUserId;
	}

	public String getApplUserName() {
		return applUserName;
	}

	public void setApplUserName(String applUserName) {
		this.applUserName = applUserName;
	}

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public String getFormTitle() {
		return formTitle;
	}

	public void setFormTitle(String formTitle) {
		this.formTitle = formTitle;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getBusinessKey() {
		return businessKey;
	}

	public void setBusinessKey(String businessKey) {
		this.businessKey = businessKey;
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getIsEnd() {
		return isEnd;
	}

	public void setIsEnd(String isEnd) {
		this.isEnd = isEnd;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getCurrentTaskName() {
		return currentTaskName;
	}

	public void setCurrentTaskName(String currentTaskName) {
		this.currentTaskName = currentTaskName;
	}

	public String getCurrentTaskAssignee() {
		return currentTaskAssignee;
	}

	public void setCurrentTaskAssignee(String currentTaskAssignee) {
		this.currentTaskAssignee = currentTaskAssignee;
	}

	public String getIsSubProcess() {
		return isSubProcess;
	}

	public void setIsSubProcess(String isSubProcess) {
		this.isSubProcess = isSubProcess;
	}

	public String getParentProcessId() {
		return parentProcessId;
	}

	public void setParentProcessId(String parentProcessId) {
		this.parentProcessId = parentProcessId;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getTablePath() {
		return tablePath;
	}

	public void setTablePath(String tablePath) {
		this.tablePath = tablePath;
	}

}
