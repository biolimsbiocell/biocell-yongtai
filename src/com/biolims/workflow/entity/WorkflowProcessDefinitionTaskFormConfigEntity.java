package com.biolims.workflow.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * 任务人员关系
 * @author cong
 */
@Entity
@Table(name = "T_BPMN_PD_T_F_CONFIG")
public class WorkflowProcessDefinitionTaskFormConfigEntity implements Serializable {

	private static final long serialVersionUID = -8480833033691012974L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@Column(name = "PROCESS_DEFINITION_ID", length = 40)
	private String processDefinitionId;

	@Column(name = "ACTIVITI_ID", length = 40)
	private String activitiId;

	@Column(name = "FORM_NAME", length = 40)
	private String formName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

	public String getActivitiId() {
		return activitiId;
	}

	public void setActivitiId(String activitiId) {
		this.activitiId = activitiId;
	}

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

}
