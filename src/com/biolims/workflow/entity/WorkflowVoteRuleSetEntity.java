package com.biolims.workflow.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * 路由规则设置
 * @author cong
 *
 */
@Entity
@Table(name = "T_BPMN_VOTE_RULE_SET_ENT")
public class WorkflowVoteRuleSetEntity implements Serializable {

	private static final long serialVersionUID = 2002344493895354525L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@Column(name = "ACTIVITI_ID", length = 40)
	private String activitiId;//节点ID

	@Column(name = "PROCESS_DEFINITION_ID", length = 40)
	private String processDefinitionId;

	@Column(name = "ONE_VOTE_RIGHTS", length = 40)
	private String oneVoteRights;//一票权 1:通过 0：否绝

	@Column(name = "RULE_METHOD", length = 5)
	private String ruleMethod;//决策方式 ：同意，不同意

	@Column(name = "VOTE_TYPE", length = 5)
	private String voteType;//投票类型 ：绝对票数、百分比

	@Column(name = "VOTE_NUM")
	private Float voteNum;//票数

	@Column(name = "USER_IDS", length = 200)
	private String userIds;//一票通过\否决的权限用户ID 多个间用逗号隔开

	@Column(name = "USER_NAMES", length = 200)
	private String userNames;//一票通过\否决的权限用户名称  多个间用逗号隔开

	@Column(name = "GROUP_IDS", length = 200)
	private String groupIds;//一票通过\否决的权限用户组ID

	@Column(name = "GROUP_NAMES", length = 200)
	private String groupNames;//一票通过\否决的权限用户组名称

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getActivitiId() {
		return activitiId;
	}

	public void setActivitiId(String activitiId) {
		this.activitiId = activitiId;
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

	public String getRuleMethod() {
		return ruleMethod;
	}

	public void setRuleMethod(String ruleMethod) {
		this.ruleMethod = ruleMethod;
	}

	public String getVoteType() {
		return voteType;
	}

	public void setVoteType(String voteType) {
		this.voteType = voteType;
	}

	public Float getVoteNum() {
		return voteNum;
	}

	public void setVoteNum(Float voteNum) {
		this.voteNum = voteNum;
	}

	public String getOneVoteRights() {
		return oneVoteRights;
	}

	public void setOneVoteRights(String oneVoteRights) {
		this.oneVoteRights = oneVoteRights;
	}

	public String getUserIds() {
		return userIds;
	}

	public void setUserIds(String userIds) {
		this.userIds = userIds;
	}

	public String getUserNames() {
		return userNames;
	}

	public void setUserNames(String userNames) {
		this.userNames = userNames;
	}

	public String getGroupIds() {
		return groupIds;
	}

	public void setGroupIds(String groupIds) {
		this.groupIds = groupIds;
	}

	public String getGroupNames() {
		return groupNames;
	}

	public void setGroupNames(String groupNames) {
		this.groupNames = groupNames;
	}

}
