package com.biolims.workflow.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * 会签结果
 * @author cong
 *
 */
@Entity
@Table(name = "T_BPMN_VOTE_RESULT_ENT")
public class VoteResultEntity implements Serializable {

	private static final long serialVersionUID = 8272840804647028003L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@Column(name = "TASK_ID", length = 40)
	private String taskId;//任务创建ID

	@Column(name = "ACTIVITI_ID", length = 40)
	private String activitiId;//任务定义ID

	@Column(name = "process_Instance_Id", length = 40)
	private String processInstanceId;

	@Column(name = "PROCESS_DEFINITION_ID", length = 40)
	private String processDefinitionId;//流程定义ID

	@Column(name = "RESULT_", length = 5)
	private String result;//会签结果

	@Column(name = "OPER_USER_ID", length = 50)
	private String operUserId;//操作人

	@Column(name = "OPER_DATE")
	private Date operDate;//操作时间

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getActivitiId() {
		return activitiId;
	}

	public void setActivitiId(String activitiId) {
		this.activitiId = activitiId;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getOperUserId() {
		return operUserId;
	}

	public void setOperUserId(String operUserId) {
		this.operUserId = operUserId;
	}

	public Date getOperDate() {
		return operDate;
	}

	public void setOperDate(Date operDate) {
		this.operDate = operDate;
	}

}
