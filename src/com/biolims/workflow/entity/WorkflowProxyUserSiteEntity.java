package com.biolims.workflow.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * 代办人员
 * @author cong
 *
 */
@Entity
@Table(name = "T_BPMN_PROXY_USER_SITE_ENT")
public class WorkflowProxyUserSiteEntity implements Serializable {

	private static final long serialVersionUID = -7941566123004237332L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@Column(name = "ASSIGNEE_", length = 40)
	private String assignee;//办理人

	@Column(name = "PROXY_USER_ID", length = 40)
	private String proxyUserId;//代办人ID

	@Column(name = "STATE_", length = 2)
	private String state;//0失效 1有效

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public String getProxyUserId() {
		return proxyUserId;
	}

	public void setProxyUserId(String proxyUserId) {
		this.proxyUserId = proxyUserId;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}
