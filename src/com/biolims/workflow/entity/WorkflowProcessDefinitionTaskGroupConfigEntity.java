package com.biolims.workflow.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * 流程定义节点人员配置
 * @author cong
 *
 */
@Entity
@Table(name = "T_BPMN_PD_T_G_CONFIG")
public class WorkflowProcessDefinitionTaskGroupConfigEntity implements Serializable {

	private static final long serialVersionUID = 1699626450156020967L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@Column(name = "PROCESS_DEFINITION_ID", length = 40)
	private String processDefinitionId;

	@Column(name = "ACTIVITI_ID", length = 40)
	private String activitiId;//activitiId

	@Column(name = "GROUP_ID", length = 40)
	private String groupId;

	@Column(name = "GROUP_NAME", length = 40)
	private String groupName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

	public String getActivitiId() {
		return activitiId;
	}

	public void setActivitiId(String activitiId) {
		this.activitiId = activitiId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

}
