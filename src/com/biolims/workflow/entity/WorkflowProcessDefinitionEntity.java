package com.biolims.workflow.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.biolims.workflow.WorkflowConstants;

/**
 * 流程定义实体
 * 
 * @author cong
 */

@Entity
@Table(name = "T_BPMN_PD_ENT")
public class WorkflowProcessDefinitionEntity implements Serializable {

	private static final long serialVersionUID = -132316730328797844L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@Column(name = "ACT_PROCESS_DEFINITION_ID", length = 40)
	private String processDefinitionId;

	@Column(name = "DEPLOYMENT_ID", length = 40)
	private String deploymentId;

	@Column(name = "NAME_", length = 40)
	private String name;

	@Column(name = "KEY_", length = 40)
	private String key;

	@Column(name = "VERSION_")
	private int version;

	@Column(name = "RESOURCE_NAME", length = 40)
	private String resourceName;

	@Column(name = "DIAGRAM_RESOURCE_NAME", length = 40)
	private String diagramResourceName;

	@Column(name = "DEPLOYMENT_TIME")
	private Date deploymentTime;

	@Column(name = "IS_MAIL_REMIND")
	private String isMailRemind = WorkflowConstants.MAIL_REMIND_NO;// 是否邮件提醒

	@Column(name = "WP_ADMIN_ID")
	private String wpAdminId;// 是否邮件提醒
	/** 工作流配置ID */
	@Column(name = "WORKFLOW_PROCESS_ID")
	private String workflowProcessId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

	public String getDeploymentId() {
		return deploymentId;
	}

	public void setDeploymentId(String deploymentId) {
		this.deploymentId = deploymentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	public String getDiagramResourceName() {
		return diagramResourceName;
	}

	public void setDiagramResourceName(String diagramResourceName) {
		this.diagramResourceName = diagramResourceName;
	}

	public Date getDeploymentTime() {
		return deploymentTime;
	}

	public void setDeploymentTime(Date deploymentTime) {
		this.deploymentTime = deploymentTime;
	}

	public String getIsMailRemind() {
		return isMailRemind;
	}

	public void setIsMailRemind(String isMailRemind) {
		this.isMailRemind = isMailRemind;
	}

	public String getWpAdminId() {
		return wpAdminId;
	}

	public void setWpAdminId(String wpAdminId) {
		this.wpAdminId = wpAdminId;
	}

	/**
	 * @return the workflowProcessId
	 */
	public String getWorkflowProcessId() {
		return workflowProcessId;
	}

	/**
	 * @param workflowProcessId
	 *            the workflowProcessId to set
	 */
	public void setWorkflowProcessId(String workflowProcessId) {
		this.workflowProcessId = workflowProcessId;
	}

}
