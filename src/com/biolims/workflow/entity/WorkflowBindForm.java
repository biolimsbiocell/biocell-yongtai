package com.biolims.workflow.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * 流程绑定表单
 * @author cong
 *
 */
@Entity
@Table(name = "T_BPMN_WORKFLOW_BIND_FORM")
public class WorkflowBindForm implements Serializable {

	private static final long serialVersionUID = 761275802401691340L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@Column(name = "FORM_NAME", length = 40)
	private String formName;

	@Column(name = "PROCESS_DEFINITION_KEY", length = 40)
	private String processDefinitionKey;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public String getProcessDefinitionKey() {
		return processDefinitionKey;
	}

	public void setProcessDefinitionKey(String processDefinitionKey) {
		this.processDefinitionKey = processDefinitionKey;
	}

}
