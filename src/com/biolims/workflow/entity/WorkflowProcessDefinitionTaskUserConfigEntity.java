package com.biolims.workflow.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * 任务人员关系
 * @author cong
 */
@Entity
@Table(name = "T_BPMN_PD_T_U_CONFIG")
public class WorkflowProcessDefinitionTaskUserConfigEntity implements Serializable {

	private static final long serialVersionUID = -8480833033691012974L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@Column(name = "PROCESS_DEFINITION_ID", length = 40)
	private String processDefinitionId;

	@Column(name = "ACTIVITI_ID", length = 40)
	private String activitiId;

	@Column(name = "USER_ID", length = 40)
	private String userId;

	@Column(name = "USER_NAME", length = 40)
	private String userName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

	public String getActivitiId() {
		return activitiId;
	}

	public void setActivitiId(String activitiId) {
		this.activitiId = activitiId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
