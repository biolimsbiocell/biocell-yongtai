package com.biolims.workflow;

import com.biolims.util.ConfigurableContants;

import org.apache.struts2.ServletActionContext;
public class WorkflowConstants extends ConfigurableContants {
	
	static {
		String lan = (String)ServletActionContext.getRequest().getSession().getAttribute("lan");
		System.err.println("========================"+lan);
		init("/ResouseInternational/msg_"+lan+".properties");
	}

	/**
	 * 工作流状态：新建
	 */
	public static final String WORKFLOW_NEW = "3";
	public static final String WORKFLOW_NEW_NAME = getProperty("biolims.workflow.newName","新建");

	/**
	 * 工作流状态：运行中
	 */
	public static final String WORKFLOW_RUNNING = "2";
	public static final String WORKFLOW_RUNNING_NAME = getProperty("biolims.workflow.processName","流程中");
	
	public static final String WORK_FLOW_ISSUED_NAME = getProperty("biolims.workflow.issueName","已下达");

	
	public static final String WORK_FLOW_MODIFY_NAME =  getProperty("biolims.workflow.modifyName","待修改");
	public static final String WORK_FLOW_EDIT_NAME = getProperty("biolims.workflow.editName","编辑");
	/**
	 * 工作流状态：完成
	 */
	public static final String WORKFLOW_COMPLETE = "1";
	public static final String WORKFLOW_COMPLETE_NAME = getProperty("biolims.workflow.completeName","完成");

	/**
	 * 工作流状态：取消
	 */
	public static final String WORKFLOW_CANCEL = "0";
	public static final String WORKFLOW_CANCEL_NAME = getProperty("biolims.workflow.cancelName","取消");
	/**
	 * 工作流状态：审批中需修改
	 */
	public static final String WORKFLOW_MODIFY = "20";
	public static final String WORKFLOW_MODIFY_NAME =  getProperty("biolims.workflow.processName","流程中");

	/**
	 * 工作流办理操作：操作KEY
	 */
	public static final String RESULT_IS_OPER_PROPERTY_KEY = "oper";

	/**
	 * 工作流办理操作:肯定
	 */
	public static final String RESULT_IS_OPER_PROPERTY_VALUE_TRUE = "1";
	/**
	 * 工作流办理操作：否定
	 */
	public static final String RESULT_IS_OPER_PROPERTY_VALUE_FALSE = "0";
	/**
	 * 工作流办理操作：转办
	 */
	public static final String RESULT_IS_OPER_PROPERTY_VALUE_TURN_TO_DO = "2";

	/**
	 * 工作流办理意见KEY
	 */
	public static final String RESULT_INFO_KEY = "biolims.workflow." +
			"biolims.workflow.";

	/**
	 * 会签结果变量key
	 */
	public static final String VOTE_REULT_KEY = "voteResult";

	/**
	 * 一票通过权
	 */
	public static final String ONE_VOTE_RIGHTS_1 = "1";

	/**
	 * 一票否决权
	 */
	public static final String ONE_VOTE_RIGHTS_0 = "0";

	/**
	 * 决策方式：同意
	 */
	public static final String VOTE_RULE_METHOD_AGREE = "1";
	/**
	 * 决策方式：不同意
	 */
	public static final String VOTE_RULE_METHOD_DISAGREE = "0";

	/**
	 * 投票类型：决对票数
	 */
	public static final String RULE_METHOD_TYPE_ABSOLUTE = "1";

	/**
	 * 投票类型：百分比
	 */
	public static final String RULE_METHOD_TYPE_PERCENTAGE = "2";

	/**
	 * 多任务中的完成条件变量name
	 */
	public static final String MULTI_COMPLETION_CONDITION_VARIABLE_NAME = "isComplete";

	/**
	 * 是否是子流程：是
	 */
	public static final String IS_SUB_PROCESS_YES = "1";
	public static final String IS_SUB_PROCESS_YES_NAME = getProperty("biolims.workflow.yesName","是");
	/**
	 * 是否是子流程：否
	 */
	public static final String IS_SUB_PROCESS_NO = "0";
	public static final String IS_SUB_PROCESS_NO_NAME = getProperty("biolims.workflow.noName","否");

	/**
	 * 是否邮件提醒：是
	 */
	public static final String MAIL_REMIND_YES = "1";
	public static final String MAIL_REMIND_YES_NAME = getProperty("biolims.workflow.yesName","是");;

	/**
	 * 是否邮件提醒：否
	 */
	public static final String MAIL_REMIND_NO = "0";
	public static final String MAIL_REMIND_NO_NAME =  getProperty("biolims.workflow.noName","否");
	
	
	/**
	 * 0工作流状态-无效
	 */
	public static final String WORK_FLOW_INVALID = "0";
	public static final String WORK_FLOW_INVALID_NAME = getProperty("biolims.workflow.invalid","无效");

	/**
	 * 1工作流状态-完成
	 */
	public static final String WORK_FLOW_COMPLETE = "1";
	/**
	 * 工作流状态名称-完成
	 */
	public static final String WORK_FLOW_COMPLETE_NAME =  getProperty("biolims.workflow.completeName","完成");
	/**
	 * 2工作流状态-审批中
	 */
	
	/**
	 * 20状态 审批中，需编辑
	 */
	public static final String DIC_STATE_WORKFLOW_IN_PROCESS_EDIT = "20";
	public static final String DIC_STATE_WORKFLOW_IN_PROCESS_NAME =getProperty("biolims.workflow.processName","流程中");
	public static final String WORK_FLOW_APPROVALING = "2";

	/**
	 * 2审批中
	 */
	public static final String WORK_FLOW_APPROVALING_NAME = getProperty("biolims.workflow.examination","审批中");

	/**
	 * 3工作流状态-新建
	 */
	public static final String WORK_FLOW_NEW = "3";
	public static final String WORK_FLOW_NEW_NAME =  getProperty("biolims.workflow.newName","新建");

	/**
	 * 物料申请中
	 */
	public static final String GOODS_APPLY_NEW = "4";
	public static final String GOODS_APPLY_NEW_NAME = getProperty("biolims.workflow.application","申请中");

	/**
	 * 物料申请完成
	 */
	public static final String GOODS_APPLY_COMPLETE = "5";
	public static final String GOODS_APPLY_COMPLETE_NAME = getProperty("biolims.workflow.completeAppction","完成申请");
	
	public static final String SAMPLE_STORAGE_OUT_COMPLETE_NAME = getProperty("biolims.workflow.completeOutBank","完成出库");
	
	public static final String SAMPLE_PRODUCT_SPLIT_NAME = getProperty("biolims.workflow.openPrijected","已拆产品");

	
	public static final String SAMPLE_TQ_DATA_NAME =getProperty("biolims.workflow.extractedData", "已提取数据");
	
	public static final String SAMPLE_PT_ONE_NAME = getProperty("biolims.workflow.theOneTemp","单一平台");
	/**
	 * 物料准备中
	 */
	public static final String GOODS_READY_NEW = "6";
	public static final String GOODS_READY_NEW_NAME =getProperty("biolims.workflow.reading","准备中") ;

	/**
	 * 物料准备完成
	 */
	public static final String GOODS_READY_COMPLETE = "7";
	public static final String GOODS_READY_COMPLETE_NAME = getProperty("biolims.workflow.completeReady","完成准备");

	/**
	 * 物料发放中
	 */
	public static final String GOODS_SEND_NEW = "8";
	public static final String GOODS_SEND_NEW_NAME = getProperty("biolims.workflow.putOuting","发放中");

	/**
	 * 物料发放完成
	 */
	public static final String GOODS_SEND_COMPLETE ="9";
	public static final String GOODS_SEND_COMPLETE_NAME = getProperty("biolims.workflow.completePutout","完成发放");

	/**
	 * 物料寄送中
	 */
	public static final String GOODS_POST_NEW = "10";
	public static final String GOODS_POST_NEW_NAME = getProperty("biolims.workflow.mailing","寄送中");

	/**
	 * 物料寄送完成
	 */
	public static final String GOODS_POST_COMPLETE = "11";
	public static final String GOODS_POST_COMPLETE_NAME = getProperty("biolims.workflow.completeMail","完成寄送");

	/**
	 * 物料运输
	 */
	public static final String GOODS_SIGN_NEW = "12";
	public static final String GOODS_SIGN_NEW_NAME = getProperty("biolims.workflow.transporting","运输中");
	/**
	 * 物料签收
	 */
	public static final String GOODS_SIGN_COMPLETE = "13";
	public static final String GOODS_SIGN_COMPLETE_NAME = getProperty("biolims.workflow.haveSigned","已签收");

	/**
	 * 样本打包中
	 */
	public static final String SAMPLE_PACK_NEW = "14";
	public static final String SAMPLE_PACK_NEW_NAME = getProperty("biolims.workflow.packing","打包中");

	/**
	 * 物料寄送完成
	 */
	public static final String SAMPLE_PACK_COMPLETE = "15";
	public static final String SAMPLE_PACK_COMPLETE_NAME = getProperty("biolims.workflow.completePackage","完成打包");

	/**
	 * 样本运输
	 */
	public static final String SAMPLE_SIGN_NEW = "16";
	public static final String SAMPLE_SIGN_NEW_NAME =getProperty("biolims.workflow.transporting","运输中") ;
	/**
	 * 样本签收
	 */
	public static final String SAMPLE_SIGN_COMPLETE = "17";
	public static final String SAMPLE_SIGN_COMPLETE_NAME = getProperty("biolims.workflow.","已签收");

	/**
	 * 样本开箱完成
	 */
	public static final String SAMPLE_RECEIVE_COMPLETE = "59";
	public static final String SAMPLE_RECEIVE_COMPLETE_NAME = getProperty("biolims.workflow.sampleReceived","样本已接收");

	/**
	 * 样本信息待审核
	 */
	public static final String SAMPLE_INPUT_NEW = "18";
	public static final String SAMPLE_INPUT_NEW_NAME = getProperty("biolims.workflow.infoAudit","信息待审核");
	/**
	 * 样本信息录入完成
	 */
	public static final String SAMPLE_INPUT_COMPLETE = "19";
	public static final String SAMPLE_INPUT_COMPLETE_NAME = getProperty("biolims.workflow.infoInput","信息已录入");

	/**
	 * 样本信息录入异常
	 */
	public static final String SAMPLE_FALSE = "70";
	public static final String SAMPLE_FALSE_NAME = getProperty("biolims.workflow.sampleError","样本异常");

	/**
	 * 样本信息录入未认领任务
	 */
	public static final String SAMPLE_CLAIM_NOT_TASK = "79";
	public static final String SAMPLE_CLAIM_NOT_TASK_NAME = getProperty("biolims.workflow.sampleNoClaim","样本未认领");

	/**
	 * 样本信息录入认领任务
	 */
	public static final String SAMPLE_CLAIM_TASK = "80";
	public static final String SAMPLE_CLAIM_TASK_NAME =getProperty("biolims.workflow.sampleYesClaim","样本已认领") ;

	/**
	 * 样本信息已录入并认领任务
	 */
	public static final String SAMPLE_CLAIM_AND＿INPUT = "81";
	public static final String SAMPLE_CLAIM_AND＿INPUT_NAME = getProperty("biolims.workflow.sampleClaimInput","样本认领并已录入");


	/**
	 * 样本待入库
	 */
	public static final String SAMPLE_IN_NEW = "21";
	public static final String SAMPLE_IN_NEW_NAME = getProperty("biolims.workflow.sampleWaitInputBank","样本待入库");
	/**
	 * 样本已入库
	 */
	public static final String SAMPLE_IN_COMPLETE = "22";
	public static final String SAMPLE_IN_COMPLETE_NAME = getProperty("biolims.workflow.sampleAlreadyBank","样本已入库");

	/**
	 * 样本待返库
	 */
	public static final String SAMPLE_RETURN_NEW = "23";
	public static final String SAMPLE_RETURN_NEW_NAME = getProperty("biolims.workflow.sampleBackBank","样本待返库");
	/**
	 * 样本已返库
	 */
	public static final String SAMPLE_RETURN_COMPLETE = "24";
	public static final String SAMPLE_RETURN_COMPLETE_NAME = getProperty("biolims.workflow.sampleAlreadyBackBank","样本已返库");
	/**
	 * 样本待出库
	 */
	public static final String SAMPLE_OUT_NEW = "25";
	public static final String SAMPLE_OUT_NEW_NAME = getProperty("biolims.workflow.infoNoInput","信息未录入");
	/**
	 * 样本已出库
	 */
	public static final String SAMPLE_OUT_COMPLETE = "26";
	public static final String SAMPLE_OUT_COMPLETE_NAME = getProperty("biolims.workflow.sanpleAlrOutputBank","样本已出库");

	
	public static final String SAMPLE_WAIT_OUT_COMPLETE_NAME = getProperty("biolims.workflow.sampleWaitOutputBank","样本待出库");
	/**
	 * 样本待分离
	 */
	public static final String SAMPLE_PLASMA_NEW = "27";
	public static final String SAMPLE_PLASMA_NEW_NAME = getProperty("biolims.workflow.waitPlasmapheresis","待血浆分离");
	/**
	 * 血浆分离完成
	 */
	public static final String SAMPLE_PLASMA_COMPLETE = "28";
	public static final String SAMPLE_PLASMA_COMPLETE_NAME =getProperty("biolims.workflow.completePlasmapheresis","完成血浆分离") ;

	/**
	 * 样本待提取核酸
	 */
	public static final String SAMPLE_DNA_NEW = "29";
	public static final String SAMPLE_DNA_NEW_NAME = getProperty("biolims.workflow.waitHSTQ","待提取核酸");
	/**
	 * 核酸提取完成
	 */
	public static final String SAMPLE_DNA_COMPLETE = "30";
	public static final String SAMPLE_DNA_COMPLETE_NAME =getProperty("biolims.workflow.completeHSTQ", "完成核酸提取");
	/**
	 * 样本待构建文库
	 */
	public static final String SAMPLE_WK_NEW = "31";
	public static final String SAMPLE_WK_NEW_NAME = getProperty("biolims.workflow.waitLibrary","待文库构建");
	/**
	 * 文库构建完成
	 */
	public static final String SAMPLE_WK_COMPLETE = "32";
	public static final String SAMPLE_WK_COMPLETE_NAME = getProperty("biolims.workflow.completeLibrary","完成文库构建");
	/**
	 * 样本待质检
	 */
	public static final String SAMPLE_QC_NEW = "33";
	public static final String SAMPLE_QC_NEW_NAME = getProperty("biolims.workflow.wait2100QualityTest","待2100质检");
	/**
	 * 质检完成
	 */
	public static final String SAMPLE_QC_COMPLETE = "34";
	public static final String SAMPLE_QC_COMPLETE_NAME = getProperty("biolims.workflow.complete2100QualityTest","完成2100质检");
	/**
	 * 样本待POOLING
	 */
	public static final String SAMPLE_POOLING_NEW = "35";
	public static final String SAMPLE_POOLING_NEW_NAME = getProperty("biolims.workflow.waitPooling","待pooling");
	/**
	 * 样本POOLING完成
	 */
	public static final String SAMPLE_POOLING_COMPLETE = "36";
	public static final String SAMPLE_POOLING_COMPLETE_NAME = getProperty("biolims.workflow.biolims.workflow.completePooling","完成pooling");

	/**
	 * 样本待上机
	 */
	public static final String SAMPLE_SEQUENCING_NEW = "37";
	public static final String SAMPLE_SEQUENCING_NEW_NAME = getProperty("biolims.workflow.waitComputer","待上机");
	/**
	 * 样本已上机
	 */
	public static final String SAMPLE_SEQUENCING_COMPLETE = "38";
	public static final String SAMPLE_SEQUENCING_COMPLETE_NAME = getProperty("biolims.workflow.completeComputer","完成上机");

	/**
	 * 数据质控
	 */
	public static final String SAMPLE_SEQC_NEW = "3";
	public static final String SAMPLE_SEQC_NEW_NAME = getProperty("biolims.workflow.waitDownComputer","待下机质控");
	/**
	 * 下机质控完成
	 */
	public static final String SAMPLE_SEQC_COMPLETE = "1";
	public static final String SAMPLE_SEQC_COMPLETE_NAME = getProperty("biolims.workflow.completeDownComputer","完成下机质控");

	/**
	 * 待信息分析
	 */
	public static final String SAMPLE_DATA_NEW = "41";
	public static final String SAMPLE_DATA_NEW_NAME = getProperty("biolims.workflow.waitInfo","待信息分析");
	/**
	 * 完成信息分析
	 */
	public static final String SAMPLE_DATA_COMPLETE = "42";
	public static final String SAMPLE_DATA_COMPLETE_NAME = getProperty("biolims.workflow.completeInfo","完成信息分析");

	/**
	 * 待数据解读
	 */
	public static final String SAMPLE_GDP_NEW = "43";
	public static final String SAMPLE_GDP_NEW_NAME = getProperty("biolims.workflow.waitData","待数据解读");
	/**
	 * 数据解读完成
	 */
	public static final String SAMPLE_GDP_COMPLETE = "44";
	public static final String SAMPLE_GDP_COMPLETE_NAME = getProperty("biolims.workflow.completeData","完成数据解读");

	/**
	 * 待出报告
	 */
	public static final String SAMPLE_REPORT_NEW = "45";
	public static final String SAMPLE_REPORT_NEW_NAME = getProperty("biolims.workflow.waitReport","待出报告");
	/**
	 * 报告完成
	 */
	public static final String SAMPLE_REPORT_COMPLETE = "46";
	public static final String SAMPLE_REPORT_COMPLETE_NAME = getProperty("biolims.workflow.completeReport","完成报告");

	/**
	 * 报告已审核
	 */
	public static final String SAMPLE_REPORT_AUDIT = "47";
	public static final String SAMPLE_INPUT_AUDIT_NAME = getProperty("biolims.workflow.throughReport","报告已审核");
	/**
	 * 报告已发送
	 */
	public static final String SAMPLE_REPORT_SEND = "48";
	public static final String SAMPLE_REPORT_SEND_NAME = getProperty("biolims.workflow.goReport","报告已发送");

	/**
	 * 样本待QPCR
	 */
	public static final String SAMPLE_QPCR_NEW = "49";
	public static final String SAMPLE_QPCR_NEW_NAME = getProperty("biolims.workflow.waitQPCR","待QPCR质控");
	/**
	 * 完成QPCR质控
	 */
	public static final String SAMPLE_QPCR_COMPLETE = "50";
	public static final String SAMPLE_QPCR_COMPLETE_NAME = getProperty("biolims.workflow.completeQPCR","完成QPCR质控");

	/**
	 * 样本待一代测序
	 */
	public static final String SAMPLE_GENERATION_NEW = "51";
	public static final String SAMPLE_GENERATION_NEW_NAME = getProperty("biolims.workflow.waitOnecx","待一代测序");
	/**
	 * 完成一代测序
	 */
	public static final String SAMPLE_GENERATION_COMPLETE = "52";
	public static final String SAMPLE_GENERATION_COMPLETE_NAME = getProperty("biolims.workflow.completeOnecx","完成一代测序");


	/**
	 * 22工作流状态已反馈
	 */
	public static final String WORK_FLOW_FEEDBACK = "54";
	public static final String WORK_FLOW_FEEDBACK_NAME = getProperty("biolims.workflow.AlreadyTask","已实验");

	/**
	 * 
	 * 样本状态：已退费
	 */
	public static final String SAMPLE_RETURNMONEY = "55";
	public static final String SAMPLE_RETURNMONEY_NAME =getProperty("biolims.workflow.biolims.workflow.alreadyReturns","已退费") ;

	/**
	 * 
	 * 样本状态：实验终止
	 */
	//1215
	public static final String EXPERIMENT_STOP = "56";
	public static final String EXPERIMENT_STOP_NAME =getProperty("biolims.workflow.testEnd","实验终止") ;

	/**
	 * 
	 * 样本状态：实验暂停
	 */
	public static final String EXPERIMENT_PAUSE = "57";
	public static final String EXPERIMENT_PAUSE_NAME =getProperty("biolims.workflow.testStop","实验暂停") ;

	/**
	 * 
	 * 样本状态：重抽血
	 */
	public static final String BLOOD_AGAIN = "58";
	public static final String BLOOD_AGAIN_NAME =getProperty("biolims.workflow.heavyExtraction","重抽血") ;

	/**
	 * 
	 * 样本状态：重建库
	 */
	public static final String CREATE_WK_AGAIN = "69";
	public static final String CREATE_WK_AGAIN_NAME = getProperty("biolims.workflow.heavyLibrary","重建库");

	/**
	 * 
	 * 样本状态：重复建库
	 */
	public static final String CREATE_WK_MORE = "60";
	public static final String CREATE_WK_MORE_NAME =getProperty("biolims.workflow.repeatLibrary","重复建库") ;
	/**
	 * 
	 * 样本状态：重复测序
	 */
	public static final String CREATE_SEQ_MORE = "64";
	public static final String CREATE_SEQ_MORE_NAME =getProperty("biolims.workflow.heavySequencing","重测序") ;

	/**
	 * 
	 * 样本状态：完成入库
	 */
	public static final String IN_STORAGE_END = "65";
	public static final String IN_STORAGE_END_NAME = getProperty("biolims.workflow.completeStorage","完成入库");



	/**
	 * 
	 * 信息分析：结果明细
	 */
	public static final String RESULT_DETAIL = "61";
	public static final String RESULT_DETAIL_NAME =getProperty("biolims.workflow.resultDetailPro","结果明细处理") ;
	/**
	 * 信息分析：结果明细待审核
	 */
	public static final String RESULT_DETAIL_CHECK = "62";
	public static final String RESULT_DETAIL_CHECK_NAME = getProperty("biolims.workflow.resultDetailBreak","结果明细待审核");
	/**
	 * 信息分析：结果明细待反馈
	 */
	public static final String RESULT_DETAIL_WAIT = "63";
	public static final String RESULT_DETAIL_WAIT_NAME = getProperty("biolims.workflow.resultDetailFeedback","结果明细待反馈");
	/**
	 * 待审核
	 */
	public static final String DO_WAIT_CONFIRM = "66";
	public static final String DO_WAIT_CONFIRM_NAME = getProperty("biolims.workflow.pending","待审核");

	/**
	 * 已审核
	 */
	public static final String DO_CONFIRMED = "67";
	public static final String DO_CONFIRMED_NAME = getProperty("biolims.workflow.actived","已审核");

	/**
	 * 科技服务待建库样本
	 */
	public static final String TECH_WAIT_JK = "68";
	public static final String TECH_WAIT_JK_NAME = getProperty("biolims.workflow.servicesBuilt","科技服务待建库");
	
	public static final String DIC_STATE_YES = "1";
	public static final String DIC_STATE_NO = "0";
	
	public static final String SAMPLE_WAIT_OUT_APPLY_HANDLE = getProperty("biolims.common.sampleHandle","样本处理");
	public static final String SAMPLE_WAIT_OUT_APPLY_NUCLEIC = getProperty("biolims.dna.dnaGet","核酸提取");
	public static final String SAMPLE_WAIT_OUT_APPLY_WK = getProperty("biolims.common.libraryBuilding","文库构建");
	public static final String SAMPLE_WAIT_OUT_APPLY_REPORT = getProperty("biolims.common.Report","检测报告");
	
	public static final String SAMPLE_STATE_DID_RECEIVE = getProperty("biolims.common.didnotreceive","未接收");
	public static final String SAMPLE_STATE_PART_OFTHE_RECEIVE = getProperty("biolims.common.partofthereceive","部分接收");
	public static final String SAMPLE_STATE_ALL_RECEIVING = getProperty("biolims.common.allreceiving","全部接收");
	
	/**
	 * 文库混合csv模板国际化
	 */
	public static final String WK_INFO_TEMP_WKID = getProperty("biolims.wk.wkCode","文库编号");
	public static final String WK_INFO_TEMP_SPLITID = getProperty("biolims.common.splitcode","拆分后编号");
	public static final String WK_INFO_TEMP_SAMPLECODE = getProperty("biolims.common.sampleCode","原始样本编号");
	public static final String WK_INFO_TEMP_SAMPLETYPE = getProperty("biolims.common.sampleType","样本类型");
	public static final String WK_INFO_TEMP_PRODUCTTYPE = getProperty("biolims.common.productType","产物类型");
	public static final String WK_INFO_TEMP_PRODUCTNAME = getProperty("biolims.common.productName","检测项目");
	public static final String WK_INFO_TEMP_DXPDSUMTOTAL = getProperty("biolims.wk.dxpdSumTotal","PCR前总量");
	public static final String WK_INFO_TEMP_LOOPNUM = getProperty("biolims.wk.loopNum","循环数");
	public static final String WK_INFO_TEMP_WKCONCENTRATION = getProperty("biolims.wk.wkConcentration","文库浓度(ng/ul)");
	public static final String WK_INFO_TEMP_WKVOLUME = getProperty("biolims.wk.wkVolume","文库体积(ul)");
	public static final String WK_INFO_TEMP_WKSAMLL = getProperty("biolims.common.wKsamll","文库片段大小");
	
	/**
	 * 富集csv模板国际化
	 */
	public static final String POOLING_INFO_TEMP_POOLINGWK = getProperty("biolims.pooling.poolingWk","富集文库");
	public static final String POOLING_INFO_TEMP_PROBECODENAME = getProperty("biolims.pooling.probeCodeName","探针名称");
	public static final String POOLING_INFO_TEMP_PCRLOOPNUM = getProperty("biolims.pooling.pcrLoopNum","PCR循环数");
	public static final String POOLING_INFO_TEMP_CONCENTRATION = getProperty("biolims.common.concentration","浓度(ng/ul)");
	public static final String POOLING_INFO_TEMP_VOLUME = getProperty("biolims.common.volume","体积(μL)");
	public static final String POOLING_INFO_TEMP_SUMNUM = getProperty("biolims.common.sumNum","总量");
	public static final String POOLING_INFO_TEMP_WKSAMLL = getProperty("biolims.common.wKsamll","文库片段大小(bp)");
	public static final String POOLING_INFO_TEMP_COUNTS = getProperty("biolims.common.counts","板号");
	
	/**
	 * 核酸提取csv模板国际化
	 */
	public static final String DNA_INFO_TEMP_CODE = getProperty("biolims.common.code", "样本编号");
	public static final String DNA_INFO_TEMP_SAMPLECODE = getProperty("biolims.common.sampleCode", "原始样本编号");
	public static final String DNA_INFO_TEMP_BARCODE = getProperty("biolims.common.barcode", "条形码");
	public static final String DNA_INFO_TEMP_OUTCODE = getProperty("biolims.common.outCode", "外部样本编号");
	public static final String DNA_INFO_TEMP_SAMPLETYPE = getProperty("biolims.common.sampleType", "样本类型");
	public static final String DNA_INFO_TEMP_PRODUCTTYPE = getProperty("biolims.common.productType", "产物类型");
	public static final String DNA_INFO_TEMP_PRODUCTNAME = getProperty("biolims.common.productName", "检测项目");
	public static final String DNA_INFO_TEMP_CONCENTRATION = getProperty("biolims.user.concentration", "浓度");
	public static final String DNA_INFO_TEMP_QBCONCENTRATION = getProperty("biolims.common.qubitConcentration", "Qubit浓度");
	public static final String DNA_INFO_TEMP_SUMNUM = getProperty("biolims.common.sumNum", "总量");
	public static final String DNA_INFO_TEMP_BILK = getProperty("biolims.common.bulk", "体积");
	
	/**
	 * 其他国际化
	 */
	public static final String BIOLIM_COM_RESULT_QUALIFIED = getProperty("biolims.common.qualified", "合格");
	public static final String BIOLIM_COM_RESULT_UNQUALIFIED = getProperty("biolims.common.unqualified", "不合格");
}
