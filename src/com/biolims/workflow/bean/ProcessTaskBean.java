package com.biolims.workflow.bean;

import java.util.Date;

public class ProcessTaskBean {

	private String taskId;

	private String taskName;

	private String title;

	private String applUserId;

	private String applUserName;

	private String formId;

	private String formName;

	private Date startDate;

	private Date endDate;

	private String stateName;

	private String note;

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getApplUserId() {
		return applUserId;
	}

	public void setApplUserId(String applUserId) {
		this.applUserId = applUserId;
	}

	public String getApplUserName() {
		return applUserName;
	}

	public void setApplUserName(String applUserName) {
		this.applUserName = applUserName;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getFormId() {
		return formId;
	}

	public void setFormId(String formId) {
		this.formId = formId;
	}

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

}
