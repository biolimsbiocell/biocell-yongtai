package com.biolims.stamp.birtVersion.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.crm.customer.customer.model.CrmCustomer;
import com.biolims.dao.EntityDao;



@Entity
@Table(name = "BIRT_VERSION_ITEM")
@SuppressWarnings("serial")
public class BirtVersionItem extends EntityDao<BirtVersionItem> implements java.io.Serializable, Cloneable   {
	
	
	//id
	private String id;
	
	//报告名称
	private String reportN ;
	
	//版本号
	private String version;
	
	//开始时间
	private Date startTime;
	

    //结束时间
	private Date endTime;
	
	//状态
	private String state;

	
	@Id
	@Column(name = "ID", length = 50)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

    
	public String getReportN() {
		return reportN;
	}

	public void setReportN(String reportN) {
		this.reportN = reportN;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	public BirtVersion getBirtVersion() {
		return birtVersion;
	}

	public void setBirtVersion(BirtVersion birtVersion) {
		this.birtVersion = birtVersion;
	}


	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}


	/**关联主表*/
	private BirtVersion birtVersion;
	
	
	
	


}
