package com.biolims.stamp.birtVersion.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.experiment.roomManagement.model.RoomManagement;

/**
 * @Title: Model
 * @Description: 报表版本管理主数据实体
 */

@Entity
@Table(name = "BIRT_VERSION")
@SuppressWarnings("serial")
public class BirtVersion extends EntityDao<BirtVersion> implements java.io.Serializable, Cloneable  {
	
	//id
	private String id;
	
	//名称
	private String name;
	
	//模块
	private DicType models;
	
	/** 状态 */
	private String state;
	/** 状态名称 */
	private String stateName;
	/** 创建日期 */
	private Date createDate;
	/** 创建人 */
	private User createUser;


	@Id
	@Column(name = "ID", length = 50)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return createUser;
	}

	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	public DicType getModels() {
		return models;
	}

	public void setModels(DicType models) {
		this.models = models;
	}

}
