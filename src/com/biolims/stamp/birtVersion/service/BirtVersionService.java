package com.biolims.stamp.birtVersion.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.SystemCodeService;
import com.biolims.crm.customer.customer.model.CrmCustomerIteam;
import com.biolims.equipment.common.constants.SystemConstants;
import com.biolims.experiment.roomManagement.dao.RoomManagementDao;
import com.biolims.experiment.roomManagement.model.RoomManagement;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.stamp.birtVersion.dao.BirtVersionRepository;
import com.biolims.stamp.birtVersion.model.BirtVersion;
import com.biolims.stamp.birtVersion.model.BirtVersionItem;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class BirtVersionService {

	@Resource
	private CommonDAO commonDAO;
	@Resource
	private BirtVersionRepository birtVersionRepository;
	@Resource
	private SystemCodeService systemCodeService;

	public BirtVersion get(String id) {
		BirtVersion birtVersion = commonDAO.get(BirtVersion.class, id);
		return birtVersion;
	}

	// 新建
	public Map<String, Object> findBirtVersionTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {

		return birtVersionRepository.findBirtVersionTable(start, length, query, col, sort);
	}

	// 保存
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String save(String dataJson ,BirtVersion birtVersion, String logInfo, List<Map<String, Object>> birtV) throws Exception {
		String id = "";
		if (birtVersion != null) {

			birtVersionRepository.saveOrUpdate(birtVersion);

			// 向子表添加数据
			if (!"".equals(dataJson) && !"[{}]".equals(dataJson)) {
				
	

				if (!birtV.isEmpty()) {
					for (Map<String, Object> maps : birtV) {
						BirtVersionItem birtVersionItem = new BirtVersionItem();
						birtVersionItem = (BirtVersionItem) commonDAO.Map2Bean(maps, birtVersionItem);
						if (!"".equals(birtVersionItem.getId()) && birtVersionItem.getId() != null) {
							BirtVersionItem bv = birtVersionRepository.findBirtVersionTableById(birtVersionItem.getId());
							if (!"".equals(birtVersionItem.getReportN()) && birtVersionItem.getReportN() != null) {
								bv.setReportN(birtVersionItem.getReportN());
							}
							if (!"".equals(birtVersionItem.getVersion()) && birtVersionItem.getVersion() != null) {
								bv.setVersion(birtVersionItem.getVersion());
							}
							if (!"".equals(birtVersionItem.getStartTime()) && birtVersionItem.getStartTime() != null) {
								bv.setStartTime(birtVersionItem.getStartTime());
							}
							if (!"".equals(birtVersionItem.getEndTime()) && birtVersionItem.getEndTime() != null) {
								bv.setEndTime(birtVersionItem.getEndTime());
							}
							if (!"".equals(birtVersionItem.getState()) && birtVersionItem.getState()!= null) {
								bv.setState(birtVersionItem.getState());
							}
							birtVersionRepository.saveOrUpdate(bv);
						} else {
							String ksId = systemCodeService.getCodeByPrefix("BirtVersionItem", "bv", 0000, 4, null);
							birtVersionItem.setId(ksId);
							birtVersionItem.setBirtVersion(birtVersion);
							birtVersionItem.setState("否");
							birtVersionRepository.saveOrUpdate(birtVersionItem);
						}

					}
					
				}
			}
			
			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(birtVersion.getId());
				li.setClassName("BirtVersion");
				li.setModifyContent(logInfo);
				li.setState("1");
				li.setStateName("数据新增");
				commonDAO.saveOrUpdate(li);
			}
			id = birtVersion.getId();
		}
		return id;
	}

	// 子表战术数据
	public Map<String, Object> findBirtVersionItemTable(Integer start, Integer length, String query, String col,
			String sort, String id) throws Exception {
		return birtVersionRepository.findBirtVersionItemTable(start, length, query, col, sort, id);
	}

	// 子表删除
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delbv(String[] ids) {
		for (String id : ids) {
			BirtVersionItem scp = birtVersionRepository.get(BirtVersionItem.class, id);
			birtVersionRepository.delete(scp);
			
			if (ids.length>0) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext
						.getRequest()
						.getSession()
						.getAttribute(
								SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(scp.getBirtVersion().getId());
				li.setClassName("BirtVersion");
				li.setModifyContent("打印报表:"+"版本号:"+scp.getVersion()+",名称:"+scp.getReportN()+"的数据已删除");
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}
		}
	}

	// 判断录入的模块名称是否有重复
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public Boolean findModel(String name) throws Exception {
		boolean s =true;
		List<BirtVersion> birtVersions =new ArrayList<BirtVersion>();
		birtVersions =birtVersionRepository.findModel(name);
		if(birtVersions.size()>0) {
			s=false;
		}
		return s;
		
	}
	
	//查询报表模块
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String findBirts(String modelId,String id,String idconfirmDate) throws ParseException {
//		String name=sampleReceiveDao.findRights(modelId);
	    BirtVersion birtVersion=birtVersionRepository.findBirt(modelId);
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	    Date a = sdf.parse(idconfirmDate);
        long dateToSecond = sdf.parse(idconfirmDate).getTime();
       
       
	   List<BirtVersionItem> birtVersionItems = new ArrayList<BirtVersionItem>();
	   birtVersionItems =  birtVersionRepository.findBirtItem(birtVersion.getId());
		String reportN = "";
	   List<String> birtVersionI = new ArrayList<String>();
			long startTime = 0;
			long endTime = 0;
		
			for (BirtVersionItem birtVersionItem : birtVersionItems) {
				if (!"".equals(birtVersionItem.getStartTime()) && birtVersionItem.getStartTime() != null) {
					startTime = birtVersionItem.getStartTime().getTime();

				}
				if (!"".equals(birtVersionItem.getEndTime()) && birtVersionItem.getEndTime() != null) {
					endTime = birtVersionItem.getEndTime().getTime();

				}
				if ((dateToSecond <= endTime) && (dateToSecond >= startTime)) {
					birtVersionI.add(birtVersionItem.getReportN());
					

				}

			}
			if(birtVersionI.size()>1) {
			String version = birtVersionRepository.findMax(birtVersion.getId());
			BirtVersionItem birtVI =  new BirtVersionItem();
			birtVI = birtVersionRepository.findMaxReport(version,birtVersion.getId());
		    reportN = birtVI.getReportN();
			}else {
				if(birtVersionI.size() ==1)
				reportN= birtVersionI.get(0);
			}
			
			  return reportN;
		}

	 
	    
	    
	
	
	public String findBirtsv(String modelId) throws ParseException {
		String reportN = "";
		BirtVersion birtVersion=birtVersionRepository.findBirt(modelId);
		 String version = birtVersionRepository.findMax(birtVersion.getId());
		  BirtVersionItem birtVersionItem =  new BirtVersionItem();
		  birtVersionItem = birtVersionRepository.findMaxReport(version,birtVersion.getId());
		  reportN = birtVersionItem.getReportN();
		  return reportN;
		
	}
	public Boolean findC(String version,String birtVId) throws ParseException {
		Boolean c =true;
		List<BirtVersionItem> birtVersionItems =new ArrayList<BirtVersionItem>();

		birtVersionItems=birtVersionRepository.findC(version,birtVId);
		if(birtVersionItems.size()>0) {
			c=false;
		}
		  return c;
		
	}
	public BirtVersion get1(String id) {
		BirtVersion birtVersion = commonDAO.get(BirtVersion.class, id);
		return birtVersion;
	}

}
