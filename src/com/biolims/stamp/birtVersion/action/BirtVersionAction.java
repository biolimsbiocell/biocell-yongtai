package com.biolims.stamp.birtVersion.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.crm.customer.customer.model.CrmCustomerIteam;
import com.biolims.experiment.roomManagement.action.RoomManagementAction;
import com.biolims.experiment.roomManagement.model.RoomManagement;
import com.biolims.experiment.roomManagement.service.RoomManagementService;
import com.biolims.file.service.FileInfoService;
import com.biolims.sample.service.SampleSearchService;
import com.biolims.stamp.birtVersion.dao.BirtVersionRepository;
import com.biolims.stamp.birtVersion.model.BirtVersion;
import com.biolims.stamp.birtVersion.model.BirtVersionItem;
import com.biolims.stamp.birtVersion.service.BirtVersionService;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.sun.org.apache.bcel.internal.generic.NEWARRAY;


@Namespace("/stamp/birtVersion")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class BirtVersionAction extends BaseActionSupport  {
	
	private String rightsId = "24019961010";
	@Autowired
	private BirtVersionService birtVersionService;
	@Autowired
	private SampleSearchService sampleSearchService;
	@Autowired
	private BirtVersionRepository birtVersionRepository;
	
    private BirtVersion birtVersion=new BirtVersion();
    
	@Resource
	private FieldService fieldService;
	@Resource
	private FileInfoService fileInfoService;

	
	
	@Action(value = "showBirtVersion")
	public String showVersion() throws Exception {
		String id = getParameterFromRequest("id");
		birtVersion = birtVersionService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/stamp/birtVersion/birtVersion.jsp");
	}
	
	//点击之后的页面
	@Action(value = "showBirtVersionTableJson")
	public void showBirtVersionTableJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = birtVersionService.findBirtVersionTable(start, length, query, col, sort);

			List<BirtVersion> list = (List<BirtVersion>) result.get("list");
		
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("models-name", "");
			
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("BirtVersion");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	//新建
	@Action(value = "editBirtVersionTable")
	public String editBirtVersionTable() throws Exception {
		String id = getParameterFromRequest("id");
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		rightsId = "24019961010";
		long num = 0;
		if (id != null && !id.equals("")) {
			this.birtVersion = birtVersionService.get(id);
			if (birtVersion == null) {
				birtVersion = new BirtVersion();
				birtVersion.setId(id);
				User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
				birtVersion.setCreateUser(user);
				birtVersion.setCreateDate(new Date());
				birtVersion.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
				birtVersion.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
				putObjToContext("bpmTaskId", bpmTaskId);
				putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
				toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			} else {
				putObjToContext("bpmTaskId", bpmTaskId);
				putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
				toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
				num = fileInfoService.findFileInfoCount(id, "birtVersion");
			}

		} else {
			birtVersion.setId("");
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			birtVersion.setCreateUser(user);
			birtVersion.setCreateDate(new Date());
			birtVersion.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			birtVersion.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		toState(birtVersion.getState());
		return dispatcher("/WEB-INF/page/stamp/birtVersion/birtVersionEdit.jsp");
	}
	//保存
	@Action(value = "save")
	public String save() throws Exception {
		synchronized (BirtVersionAction.class) {
			String id = birtVersion.getId();
			String changeLog = getParameterFromRequest("changeLog");
			//子表
			String dataJson = getParameterFromRequest("itemJson");
			List<Map<String, Object>> birtV = JsonUtils.toListByJsonArray(
					dataJson, List.class);
			
			String string = birtVersionService.save(dataJson,birtVersion, changeLog,birtV);
			String[] split = string.split(",");
			String samId = split[0];

			String bpmTaskId = getParameterFromRequest("bpmTaskId");
			String url = "/stamp/birtVersion/editBirtVersionTable.action?id=" + samId;
			if (bpmTaskId != null && !bpmTaskId.equals("") && !bpmTaskId.equals("null")) {
				url += "&bpmTaskId=" + bpmTaskId;
			}
			return redirect(url);
		}
	}
	//子表展示数据
	@Action(value = "showBirtVersionItemTableJson")
	public void showBirtVersionItemTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = birtVersionService.findBirtVersionItemTable(start, length, query, col, sort, id);
		List<BirtVersionItem> list = (List<BirtVersionItem>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("reportN", "");
		map.put("version", "");
		map.put("startTime", "yyyy-MM-dd");
		map.put("endTime", "yyyy-MM-dd");
		map.put("state", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
  }
	
	
	//子表删除
	@Action(value = "delbv", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void delbv() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			birtVersionService.delbv(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	//查询模块是否重复
	@Action(value = "findModel", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void findModel() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String name = getParameterFromRequest("name");
		boolean s =birtVersionService.findModel(name);
		if(s) {
			map.put("success", true);
		}else {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
		
	}
	
	//查询报表模块
	@Action(value = "selectBirt")
	public void selectBirt() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = getParameterFromRequest("id");
		String idconfirmDate = getParameterFromRequest("confirmDate");
		String modelId = getParameterFromRequest("modelId");
		String oNum = getParameterFromRequest("oNum");
		String reportN = birtVersionService.findBirts(modelId,id,idconfirmDate);
//		if(modelId.equals("scrwlb")) {
//			if(oNum!=null&&!"".equals(oNum)) {
//				String[] split= reportN.split("\\.");
//				String s =split[0]+oNum+"."+split[1];
//				map.put("reportN", s);
//			}
//			
//		}else {
			map.put("reportN", reportN);
//		}
		
		
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	@Action(value = "bijiaoTime")
	public void bijiaoTime() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String id = getParameterFromRequest("id");
		long startTime = 0;
		long endTime = 0;
		List<BirtVersionItem> birtVersionItems =new ArrayList<BirtVersionItem>(); 
		birtVersionItems = birtVersionRepository.findBirtItem(id);
		for (BirtVersionItem birtVersionItem : birtVersionItems) {
			if (!"".equals(birtVersionItem.getStartTime()) && birtVersionItem.getStartTime() != null) {
				startTime = birtVersionItem.getStartTime().getTime();

			}
			if (!"".equals(birtVersionItem.getEndTime()) && birtVersionItem.getEndTime() != null) {
				endTime = birtVersionItem.getEndTime().getTime();

			}
			if(startTime<endTime) {
				map.put("success", true);
			}else {
				map.put("success", false);
				break;
			}
		}		
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	@Action(value = "selectBirtv", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selectBirtv() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String modelId = getParameterFromRequest("modelId");
		String barCod = getParameterFromRequest("barCod");
		String buttonName = getParameterFromRequest("buttonName");
		String reportN = birtVersionService.findBirtsv(modelId);
		if(!"".equals(reportN)&&reportN!=null) {
			sampleSearchService.insertLog(barCod, buttonName);
		}

	    map.put("reportN", reportN);
		
		HttpUtils.write(JsonUtils.toJsonString(map));

	}
	@Action(value = "findC", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void findC() throws Exception {
		Boolean reportN =true;
		Map<String, Object> map = new HashMap<String, Object>();
		String[] arr=getRequest().getParameterValues("arr[]");
		String birtVId = getParameterFromRequest("birtVId");
		if(arr!=null) {
        for (int i = 0; i < arr.length; i++) {
        	if(reportN) {
            	reportN = birtVersionService.findC(arr[i],birtVId);

        	}else {
        		break;
        	}

        }
		}
	    map.put("reportN", reportN);
		
		HttpUtils.write(JsonUtils.toJsonString(map));

	}
	
	@Action(value = "viewOrder")
	public String viewOrder() throws Exception {
		String id = getParameterFromRequest("id");
		birtVersion = birtVersionService.get1(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/stamp/birtVersion/birtVersionEdit.jsp");
	}
	

	public BirtVersion getBirtVersion() {
		return birtVersion;
	}

	public void setBirtVersion(BirtVersion birtVersion) {
		this.birtVersion = birtVersion;
	}

	
}
