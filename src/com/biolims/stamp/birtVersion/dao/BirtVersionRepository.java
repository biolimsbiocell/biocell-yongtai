package com.biolims.stamp.birtVersion.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.crm.customer.customer.model.CrmCustomerIteam;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.roomManagement.model.RoomManagement;
import com.biolims.stamp.birtVersion.model.BirtVersion;
import com.biolims.stamp.birtVersion.model.BirtVersionItem;

@Repository
@SuppressWarnings("unchecked")
public class BirtVersionRepository extends BaseHibernateDao {
	
	public Map<String, Object> findBirtVersionTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from BirtVersion where 1=1 ";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from BirtVersion where 1=1 ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<BirtVersion> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
	
	
	//子表展示数据
	public Map<String, Object> findBirtVersionItemTable(Integer start, Integer length, String query, String col,
			String sort, String id) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from BirtVersionItem where 1=1 and birtVersion.id='" + id + "'";
		String key = "";

		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from BirtVersionItem where 1=1 and birtVersion.id='" + id + "' ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<BirtVersionItem> list = new ArrayList<BirtVersionItem>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
	
	
	//子表根据子表iD
		public BirtVersionItem findBirtVersionTableById(String id) throws Exception {
			String hql = "from BirtVersionItem where 1=1 and id='"+id+"' ";
			BirtVersionItem birtVersionItem =(BirtVersionItem) getSession().createQuery(hql).uniqueResult();
			return birtVersionItem;
			}
		
		public List<BirtVersion> findModel(String name) throws Exception {
			String hql = "from BirtVersion where 1=1 and models.name='"+name+"'";
			List<BirtVersion> birtVersions =new ArrayList<BirtVersion>();
			birtVersions =getSession().createQuery(hql).list();
			return birtVersions;
			}
		
		
		public BirtVersion findBirt(String modelId) {
			String hql = "from BirtVersion where 1=1 and models.id='"+modelId+"'";
			BirtVersion birtVersion = (BirtVersion) getSession().createQuery(hql).uniqueResult();
			return birtVersion;
		}
		public List<BirtVersionItem> findBirtItem(String id) {
			String hql = "from BirtVersionItem where 1=1 and birtVersion.id='"+id+"' and state='否'";
			List<BirtVersionItem> birtVersionItems =new ArrayList<BirtVersionItem>();
			birtVersionItems = getSession().createQuery(hql).list();
			return birtVersionItems;
		}
		
		
		public String findMax(String id) {
			String hql = "select max(version) from BirtVersionItem where 1=1 and birtVersion.id='"+id+"'";
			String v  = (String) getSession().createQuery(hql).uniqueResult();
			
			return v;
		}
		public BirtVersionItem findMaxReport(String v,String id) {
			String hql = "from BirtVersionItem where 1=1 and version='"+v+"'and birtVersion.id='"+id+"'";
			BirtVersionItem birtVersionItem =(BirtVersionItem) getSession().createQuery(hql).uniqueResult();
			
			return birtVersionItem;
		}
		
		public List<BirtVersionItem> findC(String version,String id) {
			String hql = "from BirtVersionItem where 1=1 and version='"+version+"'and birtVersion.id='"+id+"'";
			List<BirtVersionItem> birtVersionItems =new ArrayList<BirtVersionItem>();
			birtVersionItems = getSession().createQuery(hql).list();
			
			return birtVersionItems;
		}


}
