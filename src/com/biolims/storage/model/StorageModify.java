package com.biolims.storage.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.storage.position.model.StoragePosition;

/**
 * 退库申请
 *
 */
@Entity
@Table(name = "T_STORAGE_MODIFY")
public class StorageModify extends EntityDao<StorageModify> implements Serializable {

	private static final long serialVersionUID = 6245195670352622374L;

	@Id
	@Column(name = "ID", length = 32)
	private String id;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_TYPE_ID")
	private DicType type;//类型

	//	@ManyToOne(fetch = FetchType.LAZY)
	//	@NotFound(action = NotFoundAction.IGNORE)
	//	@JoinColumn(name = "STORAGE_CHECK_ID")
	//	private StorageCheck storageCheck;//盘点单

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "USER_HANDLE_USER_ID")
	private User handleUser;//盘点人

	@Column(name = "HANDLE_DATE")
	private Date handleDate;//盘点时间

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "USER_CREATE_USER_ID")
	private User createUser;//创建人

	@Column(name = "CREATE_DATE")
	private Date createDate;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "USER_CONFIRM_USER_ID")
	private User confirmUser;//批准人

	@Column(name = "CONFIRM_DATE")
	private Date confirmDate;//批准时间

	@Column(name = "END_DATE")
	private Date endDate;//结束时间

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_POSITION_ID")
	private StoragePosition position;
	@Column(name = "STATE", length = 32)
	private String state;//工作流状态
	@Column(name = "STATE_NAME", length = 200)
	private String stateName;
	@Column(name = "NOTE", length = 200)
	private String note;
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "REGION_TYPE_ID")
	private DicType regionType;//城市

	/*集团Id*/
	private String scopeId;
	/*集团名称*/
	private String scopeName;
	/**
	 * 库存调整类兴
	 * @return
	 */
	private String storageModifyType;
	
	
	public String getStorageModifyType() {
		return storageModifyType;
	}

	public void setStorageModifyType(String storageModifyType) {
		this.storageModifyType = storageModifyType;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public User getConfirmUser() {
		return confirmUser;
	}

	public void setConfirmUser(User confirmUser) {
		this.confirmUser = confirmUser;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	public DicType getType() {
		return type;
	}

	public void setType(DicType type) {
		this.type = type;
	}

	public User getHandleUser() {
		return handleUser;
	}

	public void setHandleUser(User handleUser) {
		this.handleUser = handleUser;
	}

	public Date getHandleDate() {
		return handleDate;
	}

	public void setHandleDate(Date handleDate) {
		this.handleDate = handleDate;
	}

	public User getCreateUser() {
		return createUser;
	}

	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public StoragePosition getPosition() {
		return position;
	}

	public void setPosition(StoragePosition position) {
		this.position = position;
	}

	//	public StorageCheck getStorageCheck() {
	//		return storageCheck;
	//	}
	//
	//	public void setStorageCheck(StorageCheck storageCheck) {
	//		this.storageCheck = storageCheck;
	//	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public DicType getRegionType() {
		return regionType;
	}

	public void setRegionType(DicType regionType) {
		this.regionType = regionType;
	}

	/**
	 * @return the scopeId
	 */
	public String getScopeId() {
		return scopeId;
	}

	/**
	 * @param scopeId the scopeId to set
	 */
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	/**
	 * @return the scopeName
	 */
	public String getScopeName() {
		return scopeName;
	}

	/**
	 * @param scopeName the scopeName to set
	 */
	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}



}
