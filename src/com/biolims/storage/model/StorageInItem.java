package com.biolims.storage.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.core.model.user.DicCostCenter;
import com.biolims.dao.EntityDao;
import com.biolims.storage.position.model.StoragePosition;
import com.biolims.system.work.model.UnitGroupNew;

//import com.biolims.purchase.model.PurchaseApplyItem;
//import com.biolims.purchase.model.PurchaseOrderItem;

/**
 * 入库明细
 * 
 * @author 倪毅
 */
@Entity
@Table(name = "T_STORAGE_IN_ITEM")
public class StorageInItem extends EntityDao<StorageInItem> implements
		Serializable {

	private static final long serialVersionUID = 2191801684912559179L;
	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;// ID

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_IN_ID")
	private StorageIn storageIn;// 入库单号

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_ID")
	private Storage storage;// 入库项目ID

	@Column(name = "CODE", length = 32)
	private String code;// 入库项目检索码

	@Column(name = "NAME", length = 110)
	private String name;// 入库项目名称

	@Column(name = "SPEC", length = 110)
	private String spec;// 入库项目名称

	@Column(name = "EXPIRE_DATE")
	private String expireDate;// 保质日期

	@Column(name = "PRODUCT_DATE")
	private Date productDate;// 生产日期

	@Column(name = "PRICE")
	private Double price;// 入库单价

	@Column(name = "NUM")
	private Double num;// 入库数量

	@Column(name = "SERIAL", length = 32)
	private String serial;// 批次
	@Column(name = "QC_PASS", length = 32)
	private String qcPass;// qc状态

	@Column(name = "NOTE", length = 4000)
	private String note;// (sn)
	@Column(name = "NOTE2", length = 4000)
	private String note2;// 备注
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_POSITION_ID")
	private StoragePosition position;// 位置

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_COST_CENTER_ID")
	private DicCostCenter costCenter;// 成本中心

	// @ManyToOne(fetch = FetchType.LAZY)
	// @NotFound(action = NotFoundAction.IGNORE)
	// @JoinColumn(name = "PURCHASE_ORDER_ITEM_ID")
	// private PurchaseOrderItem purchaseOrderItem;
	//
	// @ManyToOne(fetch = FetchType.LAZY)
	// @NotFound(action = NotFoundAction.IGNORE)
	// @JoinColumn(name = "PURCHASE_APPLY_ITEM_ID")
	// private PurchaseApplyItem purchaseApplyItem;

	// 非持久化属性
	@Transient
	private Double outPrice;// 出库单价

	@Transient
	private Double inScale;// 入库比例

	@Transient
	private String inCondition;// 入库情况

	@Transient
	private Double purchaseNum;

	@Transient
	private Double inNum;// 入库数量

	@Transient
	private Double cancelNum;// 退货数量

	@Transient
	private Double purchaseOrderNum;// 采购数量
	
	@Column(name = "PACKING_NUM")
	private Double packingNum;// 入库数量(主单位)

	
	
	public Double getPackingNum() {
		return packingNum;
	}

	public void setPackingNum(Double packingNum) {
		this.packingNum = packingNum;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "UNIT_GROUP_NEW")
	private UnitGroupNew unitGroupNew;// 单位组
	
	
	
	public UnitGroupNew getUnitGroupNew() {
		return unitGroupNew;
	}

	public void setUnitGroupNew(UnitGroupNew unitGroupNew) {
		this.unitGroupNew = unitGroupNew;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public StorageIn getStorageIn() {
		return storageIn;
	}

	public void setStorageIn(StorageIn storageIn) {
		this.storageIn = storageIn;
	}

	public Double getPurchaseOrderNum() {
		return purchaseOrderNum;
	}

	public void setPurchaseOrderNum(Double purchaseOrderNum) {
		this.purchaseOrderNum = purchaseOrderNum;
	}

	public Storage getStorage() {
		return storage;
	}

	public void setStorage(Storage storage) {
		this.storage = storage;
	}

	public Double getNum() {
		return num;
	}

	public void setNum(Double num) {
		this.num = num;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}

	public Date getProductDate() {
		return productDate;
	}

	public void setProductDate(Date productDate) {
		this.productDate = productDate;
	}

	public Double getPurchaseNum() {
		return purchaseNum;
	}

	public void setPurchaseNum(Double purchaseNum) {
		this.purchaseNum = purchaseNum;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getOutPrice() {
		return outPrice;
	}

	public void setOutPrice(Double outPrice) {
		this.outPrice = outPrice;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public Double getInScale() {
		return inScale;
	}

	public void setInScale(Double inScale) {
		this.inScale = inScale;
	}

	public String getInCondition() {
		return inCondition;
	}

	public void setInCondition(String inCondition) {
		this.inCondition = inCondition;
	}

	public Double getInNum() {
		return inNum;
	}

	public void setInNum(Double inNum) {
		this.inNum = inNum;
	}

	public Double getCancelNum() {
		return cancelNum;
	}

	public void setCancelNum(Double cancelNum) {
		this.cancelNum = cancelNum;
	}

	public StoragePosition getPosition() {
		return position;
	}

	public void setPosition(StoragePosition position) {
		this.position = position;
	}

	public DicCostCenter getCostCenter() {
		return costCenter;
	}

	public void setCostCenter(DicCostCenter costCenter) {
		this.costCenter = costCenter;
	}

	// public PurchaseOrderItem getPurchaseOrderItem() {
	// return purchaseOrderItem;
	// }
	//
	// public void setPurchaseOrderItem(PurchaseOrderItem purchaseOrderItem) {
	// this.purchaseOrderItem = purchaseOrderItem;
	// }
	//
	// public PurchaseApplyItem getPurchaseApplyItem() {
	// return purchaseApplyItem;
	// }
	//
	// public void setPurchaseApplyItem(PurchaseApplyItem purchaseApplyItem) {
	// this.purchaseApplyItem = purchaseApplyItem;
	// }

	public String getSpec() {
		return spec;
	}

	public void setSpec(String spec) {
		this.spec = spec;
	}

	/**
	 * @return the qcPass
	 */
	public String getQcPass() {
		return qcPass;
	}

	/**
	 * @param qcPass
	 *            the qcPass to set
	 */
	public void setQcPass(String qcPass) {
		this.qcPass = qcPass;
	}

	/**
	 * @return the note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * @param note
	 *            the note to set
	 */
	public void setNote(String note) {
		this.note = note;
	}

	public String getNote2() {
		return note2;
	}

	public void setNote2(String note2) {
		this.note2 = note2;
	}

}
