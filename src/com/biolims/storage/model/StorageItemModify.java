package com.biolims.storage.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dic.model.DicType;
import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;

/**
 * 调拨申请
 * @author 倪毅
 *
 */
@Entity
@Table(name = "T_STORAGE_ITEM_MODIFY")
public class StorageItemModify extends EntityDao<StorageItemModify> implements Serializable {

	private static final long serialVersionUID = 6245195670352622374L;
	@Id
	@Column(name = "ID", length = 32)
	private String id;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_TYPE_ID")
	private DicType type;//类型

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "USER_CREATE_USER_ID")
	private User createUser;//创建人

	@Column(name = "CREATE_DATE")
	private Date createDate;//创建时间

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "USER_CONFIRM_USER_ID")
	private User confirmUser;//批准人

	@Column(name = "CONFIRM_DATE")
	private Date confirmDate;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_ID")
	private Storage storage;//库存主数据

	@Column(name = "STATE", length = 32)
	private String state;//工作流状态

	@Column(name = "STATE_NAME", length = 200)
	private String stateName;//工作流名称

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public User getConfirmUser() {
		return confirmUser;
	}

	public void setConfirmUser(User confirmUser) {
		this.confirmUser = confirmUser;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	public DicType getType() {
		return type;
	}

	public void setType(DicType type) {
		this.type = type;
	}

	public User getCreateUser() {
		return createUser;
	}

	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Storage getStorage() {
		return storage;
	}

	public void setStorage(Storage storage) {
		this.storage = storage;
	}

}
