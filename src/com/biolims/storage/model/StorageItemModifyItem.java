package com.biolims.storage.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.core.model.user.DicCostCenter;
import com.biolims.dao.EntityDao;

/**
 * 退库明细对象
 *
 */
@Entity
@Table(name = "T_STORAGE_ITEM_MODIFY_ITEM")
public class StorageItemModifyItem extends EntityDao<StorageItemModifyItem> implements Serializable {

	private static final long serialVersionUID = -4481968484077357614L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@Column(name = "NUM")
	private Double num;//数量

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_ITEM_ID")
	private StorageReagentBuySerial storageItem;//对象信息

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_ITEM_MODIFY_ID")
	private StorageItemModify storageItemModify;//调拨申请

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "COST_CENTER_ID")
	private DicCostCenter costCenter;//成本中心

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public StorageReagentBuySerial getStorageItem() {
		return storageItem;
	}

	public void setStorageItem(StorageReagentBuySerial storageItem) {
		this.storageItem = storageItem;
	}

	public StorageItemModify getStorageItemModify() {
		return storageItemModify;
	}

	public void setStorageItemModify(StorageItemModify storageItemModify) {
		this.storageItemModify = storageItemModify;
	}

	public DicCostCenter getCostCenter() {
		return costCenter;
	}

	public void setCostCenter(DicCostCenter costCenter) {
		this.costCenter = costCenter;
	}

	public Double getNum() {
		return num;
	}

	public void setNum(Double num) {
		this.num = num;
	}

}
