package com.biolims.storage.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.storage.position.model.StoragePosition;

/**
 * 退库明细对象
 *
 */
@Entity
@Table(name = "T_STORAGE_MOVE_ITEM")
public class StorageMoveItem extends EntityDao<StorageMoveItem> implements Serializable {

	private static final long serialVersionUID = -4481968484077357614L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_ID")
	private Storage storage;//对象 关联出库申请中的对象信息

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_POSITION_ID")
	private StoragePosition position;// 存储位置

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_MOVE_ID")
	private StorageMove storageMove;//退库申请

	public StorageMove getStorageMove() {
		return storageMove;
	}

	public void setStorageMove(StorageMove storageMove) {
		this.storageMove = storageMove;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Storage getStorage() {
		return storage;
	}

	public void setStorage(Storage storage) {
		this.storage = storage;
	}

	public StoragePosition getPosition() {
		return position;
	}

	public void setPosition(StoragePosition position) {
		this.position = position;
	}

}
