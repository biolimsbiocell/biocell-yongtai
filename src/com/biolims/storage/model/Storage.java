package com.biolims.storage.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.core.model.user.Department;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicCurrencyType;
import com.biolims.dic.model.DicState;
import com.biolims.dic.model.DicType;
import com.biolims.dic.model.DicUnit;
import com.biolims.storage.position.model.StoragePosition;
import com.biolims.supplier.model.Supplier;
import com.biolims.system.work.model.UnitGroupNew;

//import com.biolims.supplier.model.Supplier;

/**
 * 仓库
 * 
 */
@Entity
@Table(name = "T_STORAGE")
public class Storage extends EntityDao<Storage> implements Serializable {

	private static final long serialVersionUID = -2175588066049478314L;

	@Id
	@Column(name = "ID", length = 32)
	private String id;// 编码

	@Column(name = "NAME", length = 110)
	private String name;// 名称

	@Column(name = "ENG_NAME", length = 200)
	private String engName;// 英文名称

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "USER_DUTY_USER_ID")
	private User dutyUser;// 负责人

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_STORAGE_MAIN_TYPE_ID")
	private DicStorageMainType mainType;// 分类

	@Column(name = "SEARCH_CODE", length = 15)
	private String searchCode;// Component Cat No.

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_STUDY_TYPE_ID")
	private DicType studyType;// 子类型

	@Column(name = "CURRENT_INDEX", length = 2)
	private Integer currentIndex;// 当前INDEX索引（排序号）

	@Column(name = "USE_DESC", length = 110)
	private String useDesc;// 用途

	@Column(name = "BAR_CODE", length = 32)
	private String barCode;// Catalog No.

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_SOURCE_TYPE_ID")
	private DicType source;// 来源

	@Column(name = "SPEC", length = 110)
	private String spec;// 规格

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_STATE_ID")
	private DicState state;// 状态

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "USER_CREATE_USER_ID")
	private User createUser;// 创建人

	@Column(name = "CREATE_DATE", length = 30)
	private Date createDate;// 创建时间

	@Column(name = "IF_CALL", length = 32)
	private String ifCall;// 是否失效提醒

	@Column(name = "PIC_PATH", length = 200)
	private String picPath;// 图片

	@Column(name = "NUM")
	private Double num;// 库存数量

	@Column(name = "PURCHASE_NUM")
	private Double purchaseNum;// 采购数量

	@Column(name = "ON_WAY_NUM")
	private Double onWayNum;// 在途数量

	@Column(name = "FACT_STORAGE_NUM")
	private Double factStorageNum;// 实际库存数量(Not Use)

	@Column(name = "OUT_PRICE")
	private Double outPrice;// 出库价格

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_UNIT_ID")
	private DicUnit unit;// 单位
	
	
	private String medium;//是否是培养基
	
	private String biaoShiMa;
	
	
	
	
	public String getBiaoShiMa() {
		return biaoShiMa;
	}

	public void setBiaoShiMa(String biaoShiMa) {
		this.biaoShiMa = biaoShiMa;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "UNIT_GROUP")
	private UnitGroupNew unitGroup;//单位组
	
	//出库单位组
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "OUT_UNIT_GROUP")
	private UnitGroupNew outUnitGroup;//单位组

	public UnitGroupNew getOutUnitGroup() {
		return outUnitGroup;
	}

	public void setOutUnitGroup(UnitGroupNew outUnitGroup) {
		this.outUnitGroup = outUnitGroup;
	}

	public UnitGroupNew getUnitGroup() {
		return unitGroup;
	}

	public void setUnitGroup(UnitGroupNew unitGroup) {
		this.unitGroup = unitGroup;
	}

	@Column(name = "SAVE_CONDITION", length = 100)
	private String saveCondition;// 存储条件

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_POSITION_ID")
	private StoragePosition position;// 存储位置

	private Double prepareNum;// 预留数量

	@Column(name = "IF_SECONDBUY", length = 32)
	private String ifSecondBuy;// 是否重定货

	@Column(name = "SAFE_NUM")
	private Double safeNum;// 安全库存数量

	@Column(name = "HAS_SUBMIT_NUM")
	private Double hasSubmitNum;// 已经提交库存数量(Not Use)

	@Column(name = "LY_NUM")
	private Double lyNum;// 已经提交库存数量(Not Use)

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_STORAGE_TYPE_ID")
	private DicStorageType type;// 类型

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DEPARTMENT_ID")
	private Department department;// 部门

	@Column(name = "BREED", length = 60)
	private String breed;// 品牌

	@Column(name = "PRODUCT_ADDRESS", length = 120)
	private String productAddress;// 产地

	@Column(name = "PRICE")
	private Double price;// 价格

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_CURRENCY_TYPE_ID")
	private DicCurrencyType currencyType;// 币种

	@Column(name = "DURABLE_DAYS", length = 110)
	private Integer durableDays;// 耐用天数

	@Column(name = "REMIND_LEAD_TIME", length = 110)
	private Integer remindLeadTime;// 提醒提前期

	@Column(name = "NOTE", length = 110)
	private String note;// 备注

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SUPPLIER_ID")
	private Supplier producer;// 生产商

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "KIT_ID")
	private DicType kit;// 试剂盒

	@Column(name = "I5")
	private String i5;
	@Column(name = "I7")
	private String i7;
	@Column(name = "I5INDEX")
	private String i5Index;
	@Column(name = "I7INDEX")
	private String i7Index;

	@Column(name = "P_TYPE", length = 1)
	private String p_type;

	@Column(name = "JDE_CODE", length = 32)
	private String jdeCode;

	@Column(name = "PURCHASE_STATE", length = 32)
	private String purchaseState;// 采购状态
	// 判断index使用到哪个的状态
	private String indexState;

	// 反应系数
	private Double reactionNum;
	
	/** scopeId*/
	private String scopeId;
	/** scopeName*/
	private String scopeName;
	/** 自定义字段*/
	private String fieldContent;
	
	@Column(name = "other_name", length = 110)
	private String otherName;// 别名
	
	
	
	public String getOtherName() {
		return otherName;
	}

	public void setOtherName(String otherName) {
		this.otherName = otherName;
	}

	@Column(name = "FIELD_CONTENT", length = 255)
	public String getFieldContent() {
		return fieldContent;
	}

	public void setFieldContent(String fieldContent) {
		this.fieldContent = fieldContent;
	}

	@Column(name = "SCOPE_ID", length = 255)
	public String getScopeId() {
		return scopeId;
	}

	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	@Column(name = "SCOPE_NAME", length = 255)
	public String getScopeName() {
		return scopeName;
	}

	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

	public Double getReactionNum() {
		return reactionNum;
	}

	public void setReactionNum(Double reactionNum) {
		this.reactionNum = reactionNum;
	}

	public String getIndexState() {
		return indexState;
	}

	public void setIndexState(String indexState) {
		this.indexState = indexState;
	}

	public String getI5() {
		return i5;
	}

	public void setI5(String i5) {
		this.i5 = i5;
	}

	public String getI7() {
		return i7;
	}

	public void setI7(String i7) {
		this.i7 = i7;
	}

	// 非持久化属性
	@Transient
	private Double needNum;// 需求数量

	@Transient
	private String hasSubmitNumStr;// 已经提交库存数量

	@Transient
	private String lyNumStr;// 已经提交库存数量

	@Transient
	private String prepareNumStr;// 预留数量

	@Transient
	private String numStr;// 预留数量

	@Transient
	private String studyTypeStr;

	@Transient
	private String mainTypeStr;

	@Transient
	private String stateStr;

	@Transient
	private String typeStr;

	@Transient
	private String unitStr;

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getBreed() {
		return breed;
	}

	public DicCurrencyType getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(DicCurrencyType currencyType) {
		this.currencyType = currencyType;
	}

	public Double getLyNum() {
		return lyNum;
	}

	public void setLyNum(Double lyNum) {
		this.lyNum = lyNum;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public void setBreed(String breed) {
		this.breed = breed;
	}

	public Double getHasSubmitNum() {
		return hasSubmitNum;
	}

	public void setHasSubmitNum(Double hasSubmitNum) {
		this.hasSubmitNum = hasSubmitNum;
	}

	// public Supplier getProducer() {
	// return producer;
	// }
	//
	// public void setProducer(Supplier producer) {
	// this.producer = producer;
	// }

	public String getProductAddress() {
		return productAddress;
	}

	public void setProductAddress(String productAddress) {
		this.productAddress = productAddress;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEngName() {
		return engName;
	}

	public void setEngName(String engName) {
		this.engName = engName;
	}

	public User getDutyUser() {
		return dutyUser;
	}

	public void setDutyUser(User dutyUser) {
		this.dutyUser = dutyUser;
	}

	public DicStorageMainType getMainType() {
		return mainType;
	}

	public void setMainType(DicStorageMainType mainType) {
		this.mainType = mainType;
	}

	public String getSearchCode() {
		return searchCode;
	}

	public void setSearchCode(String searchCode) {
		this.searchCode = searchCode;
	}

	public DicType getStudyType() {
		return studyType;
	}

	public void setStudyType(DicType studyType) {
		this.studyType = studyType;
	}

	public String getUseDesc() {
		return useDesc;
	}

	public void setUseDesc(String useDesc) {
		this.useDesc = useDesc;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public DicType getSource() {
		return source;
	}

	public void setSource(DicType source) {
		this.source = source;
	}

	public String getSpec() {
		return spec;
	}

	public void setSpec(String spec) {
		this.spec = spec;
	}

	public DicState getState() {
		return state;
	}

	public void setState(DicState state) {
		this.state = state;
	}

	public User getCreateUser() {
		return createUser;
	}

	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getIfCall() {
		return ifCall;
	}

	public void setIfCall(String ifCall) {
		this.ifCall = ifCall;
	}

	public String getPicPath() {
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}

	public DicUnit getUnit() {
		return unit;
	}

	public void setUnit(DicUnit unit) {
		this.unit = unit;
	}

	public String getSaveCondition() {
		return saveCondition;
	}

	public void setSaveCondition(String saveCondition) {
		this.saveCondition = saveCondition;
	}

	public String getIfSecondBuy() {
		return ifSecondBuy;
	}

	public void setIfSecondBuy(String ifSecondBuy) {
		this.ifSecondBuy = ifSecondBuy;
	}

	public Double getNum() {
		return num;
	}

	public void setNum(Double num) {
		this.num = num;
	}

	public Double getFactStorageNum() {
		return factStorageNum;
	}

	public void setFactStorageNum(Double factStorageNum) {
		this.factStorageNum = factStorageNum;
	}

	public Double getPrepareNum() {
		return prepareNum;
	}

	public void setPrepareNum(Double prepareNum) {
		this.prepareNum = prepareNum;
	}

	public Double getSafeNum() {
		return safeNum;
	}

	public void setSafeNum(Double safeNum) {
		this.safeNum = safeNum;
	}

	public DicStorageType getType() {
		return type;
	}

	public void setType(DicStorageType type) {
		this.type = type;
	}

	public String getStudyTypeStr() {
		return studyTypeStr;
	}

	public void setStudyTypeStr(String studyTypeStr) {
		this.studyTypeStr = studyTypeStr;
	}

	public String getMainTypeStr() {
		return mainTypeStr;
	}

	public void setMainTypeStr(String mainTypeStr) {
		this.mainTypeStr = mainTypeStr;
	}

	public String getStateStr() {
		return stateStr;
	}

	public void setStateStr(String stateStr) {
		this.stateStr = stateStr;
	}

	public String getTypeStr() {
		return typeStr;
	}

	public void setTypeStr(String typeStr) {
		this.typeStr = typeStr;
	}

	public String getUnitStr() {
		return unitStr;
	}

	public void setUnitStr(String unitStr) {
		this.unitStr = unitStr;
	}

	public StoragePosition getPosition() {
		return position;
	}

	public void setPosition(StoragePosition position) {
		this.position = position;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public Double getNeedNum() {
		return needNum;
	}

	public void setNeedNum(Double needNum) {
		this.needNum = needNum;
	}

	public String getHasSubmitNumStr() {
		return hasSubmitNumStr;
	}

	public void setHasSubmitNumStr(String hasSubmitNumStr) {
		this.hasSubmitNumStr = hasSubmitNumStr;
	}

	public String getLyNumStr() {
		return lyNumStr;
	}

	public void setLyNumStr(String lyNumStr) {
		this.lyNumStr = lyNumStr;
	}

	public String getPrepareNumStr() {
		return prepareNumStr;
	}

	public void setPrepareNumStr(String prepareNumStr) {
		this.prepareNumStr = prepareNumStr;
	}

	public String getNumStr() {
		return numStr;
	}

	public void setNumStr(String numStr) {
		this.numStr = numStr;
	}

	public Double getOutPrice() {
		return outPrice;
	}

	public void setOutPrice(Double outPrice) {
		this.outPrice = outPrice;
	}

	public Double getOnWayNum() {
		return onWayNum;
	}

	public void setOnWayNum(Double onWayNum) {
		this.onWayNum = onWayNum;
	}

	public Integer getCurrentIndex() {
		return currentIndex;
	}

	public void setCurrentIndex(Integer currentIndex) {
		this.currentIndex = currentIndex;
	}

	public Double getPurchaseNum() {
		return purchaseNum;
	}

	public void setPurchaseNum(Double purchaseNum) {
		this.purchaseNum = purchaseNum;
	}

	public Integer getDurableDays() {
		return durableDays;
	}

	public void setDurableDays(Integer durableDays) {
		this.durableDays = durableDays;
	}

	public Integer getRemindLeadTime() {
		return remindLeadTime;
	}

	public void setRemindLeadTime(Integer remindLeadTime) {
		this.remindLeadTime = remindLeadTime;
	}

	public String getI5Index() {
		return i5Index;
	}

	public void setI5Index(String i5Index) {
		this.i5Index = i5Index;
	}

	public String getI7Index() {
		return i7Index;
	}

	public void setI7Index(String i7Index) {
		this.i7Index = i7Index;
	}

	public Supplier getProducer() {
		return producer;
	}

	public void setProducer(Supplier producer) {
		this.producer = producer;
	}

	public String getP_type() {
		return p_type;
	}

	public void setP_type(String p_type) {
		this.p_type = p_type;
	}

	public String getJdeCode() {
		return jdeCode;
	}

	public void setJdeCode(String jdeCode) {
		this.jdeCode = jdeCode;
	}

	/**
	 * @return the kit
	 */
	public DicType getKit() {
		return kit;
	}

	/**
	 * @param kit
	 *            the kit to set
	 */
	public void setKit(DicType kit) {
		this.kit = kit;
	}

	/**
	 * @return the purchaseState
	 */
	public String getPurchaseState() {
		return purchaseState;
	}

	/**
	 * @param purchaseState
	 *            the purchaseState to set
	 */
	public void setPurchaseState(String purchaseState) {
		this.purchaseState = purchaseState;
	}

	public String getMedium() {
		return medium;
	}

	public void setMedium(String medium) {
		this.medium = medium;
	}

}
