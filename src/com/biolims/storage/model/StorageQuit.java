package com.biolims.storage.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dic.model.DicType;
import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;

/**
 * 退库申请
 *
 */
@Entity
@Table(name = "T_STORAGE_QUIT")
public class StorageQuit extends EntityDao<StorageQuit> implements Serializable {

	private static final long serialVersionUID = 6245195670352622374L;

	@Id
	@Column(name = "ID", length = 32)
	private String id;

	@Column(name = "REASON", length = 100)
	private String reason;//退库原因

	@Column(name = "CONFIRM_DATE")
	private Date confirmDate;//批准时间

	@Column(name = "QUIT_DATE")
	private Date quitDate;//退库时间

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "USER_QUIT_USER_ID")
	private User quitUser;//退库人

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_TYPE_ID")
	private DicType type;//退库类型

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_OUT_ID")
	private StorageOut storageOut;//出库申请

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "USER_ACCEPT_USER_ID")
	private User acceptUser;//接收人

	@Column(name = "IS_SYS_SEND", length = 32)
	private String isSysSend; //是否是系统自动创建

	@Column(name = "STATE", length = 32)
	private String state;//工作流状态

	@Column(name = "STATE_NAME", length = 200)
	private String stateName;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "USER_CONFIRM_USER_ID")
	private User confirmUser;//退库人

	//非持久属性
	@Transient
	private String quitUserStr;
	@Transient
	private String typeStr;
	@Transient
	private String acceptUserStr;
	@Transient
	private String storageOutIdStr;
	@Transient
	private String projectStr;
	@Transient
	private String stateStr;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public DicType getType() {
		return type;
	}

	public void setType(DicType type) {
		this.type = type;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public StorageOut getStorageOut() {
		return storageOut;
	}

	public void setStorageOut(StorageOut storageOut) {
		this.storageOut = storageOut;
	}

	public User getQuitUser() {
		return quitUser;
	}

	public void setQuitUser(User quitUser) {
		this.quitUser = quitUser;
	}

	public Date getQuitDate() {
		return quitDate;
	}

	public void setQuitDate(Date quitDate) {
		this.quitDate = quitDate;
	}

	public User getAcceptUser() {
		return acceptUser;
	}

	public void setAcceptUser(User acceptUser) {
		this.acceptUser = acceptUser;
	}

	public String getQuitUserStr() {
		return quitUserStr;
	}

	public void setQuitUserStr(String quitUserStr) {
		this.quitUserStr = quitUserStr;
	}

	public String getTypeStr() {
		return typeStr;
	}

	public void setTypeStr(String typeStr) {
		this.typeStr = typeStr;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getAcceptUserStr() {
		return acceptUserStr;
	}

	public void setAcceptUserStr(String acceptUserStr) {
		this.acceptUserStr = acceptUserStr;
	}

	public String getStorageOutIdStr() {
		return storageOutIdStr;
	}

	public void setStorageOutIdStr(String storageOutIdStr) {
		this.storageOutIdStr = storageOutIdStr;
	}

	public String getProjectStr() {
		return projectStr;
	}

	public void setProjectStr(String projectStr) {
		this.projectStr = projectStr;
	}

	public String getStateStr() {
		return stateStr;
	}

	public void setStateStr(String stateStr) {
		this.stateStr = stateStr;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	public String getIsSysSend() {
		return isSysSend;
	}

	public void setIsSysSend(String isSysSend) {
		this.isSysSend = isSysSend;
	}

	public User getConfirmUser() {
		return confirmUser;
	}

	public void setConfirmUser(User confirmUser) {
		this.confirmUser = confirmUser;
	}

}
