package com.biolims.storage.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.core.model.user.DicCostCenter;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicCurrencyType;
import com.biolims.dic.model.DicType;
import com.biolims.dic.model.DicUnit;
import com.biolims.storage.position.model.StoragePosition;

//import com.biolims.supplier.model.Supplier;

/**
 * 组成成分
 * 
 */

@Entity
@Table(name = "t_STORAGE_REAGENT_COMPONENTS")
public class StorageReagentComponents extends EntityDao<StorageReagentComponents>
		implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4923478264834061065L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;// id

	@Column(name = "COMPOUND_NAME", length = 255)
	private String compoundName;// 化合物名称
	
	@Column(name = "CONTENT", length = 255)
	private String content;//含量	
	
	@Column(name = "ACCREDITATION", length = 255)
	private String accreditation;//是否认证
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CERTIFICATION_BODY")
	private DicType certificationBody;//认证机构
	

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_ID")
	private Storage storage; // 库存主数据


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getCompoundName() {
		return compoundName;
	}


	public void setCompoundName(String compoundName) {
		this.compoundName = compoundName;
	}


	public String getContent() {
		return content;
	}


	public void setContent(String content) {
		this.content = content;
	}


	public String getAccreditation() {
		return accreditation;
	}


	public void setAccreditation(String accreditation) {
		this.accreditation = accreditation;
	}


	public DicType getCertificationBody() {
		return certificationBody;
	}


	public void setCertificationBody(DicType certificationBody) {
		this.certificationBody = certificationBody;
	}


	public Storage getStorage() {
		return storage;
	}


	public void setStorage(Storage storage) {
		this.storage = storage;
	}


	
}
