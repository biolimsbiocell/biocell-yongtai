package com.biolims.storage.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;

/**
 * 采购试剂
 *
 */
@Entity
@Table(name = "T_STORAGE_REAGENT_BUY")
public class StorageReagentBuy extends EntityDao<StorageReagentBuy> implements Serializable {

	private static final long serialVersionUID = 6024018966809089686L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@Column(name = "STORAGE_BREED", length = 30)
	private String storageBreed;//品牌

	@Column(name = "STORAGE_PRODUCT_ADDRESS", length = 60)
	private String storageProductAddress;//产地

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_ID")
	private Storage storageId;//库存主数据

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStorageBreed() {
		return storageBreed;
	}

	public void setStorageBreed(String storageBreed) {
		this.storageBreed = storageBreed;
	}

	public String getStorageProductAddress() {
		return storageProductAddress;
	}

	public void setStorageProductAddress(String storageProductAddress) {
		this.storageProductAddress = storageProductAddress;
	}

	public Storage getStorageId() {
		return storageId;
	}

	public void setStorageId(Storage storageId) {
		this.storageId = storageId;
	}

}
