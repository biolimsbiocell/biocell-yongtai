package com.biolims.storage.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;

/**
 * 退库明细对象
 *
 */
@Entity
@Table(name = "T_STORAGE_CHECK_ITEM")
public class StorageCheckItem extends EntityDao<StorageCheckItem> implements Serializable {

	private static final long serialVersionUID = -4481968484077357614L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_ID")
	private Storage storage;//对象信息

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_CHECK_ID")
	private StorageCheck storageCheck;//退库申请

	@Column(name = "NUM")
	private Double num;//盘点数量

	@Column(name = "MODIFY_NUM")
	private Double modifyNum;//调整数量

	@Column(name = "SERIAL", length = 32)
	private String serial;//批次编码

	//非持久化对象
	@Transient
	private Date productDate;//生产日期

	@Transient
	private Date expireDate;//有效期

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Storage getStorage() {
		return storage;
	}

	public void setStorage(Storage storage) {
		this.storage = storage;
	}

	public Double getNum() {
		return num;
	}

	public void setNum(Double num) {
		this.num = num;
	}

	public StorageCheck getStorageCheck() {
		return storageCheck;
	}

	public void setStorageCheck(StorageCheck storageCheck) {
		this.storageCheck = storageCheck;
	}

	public Date getProductDate() {
		return productDate;
	}

	public void setProductDate(Date productDate) {
		this.productDate = productDate;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public Double getModifyNum() {
		return modifyNum;
	}

	public void setModifyNum(Double modifyNum) {
		this.modifyNum = modifyNum;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}
}
