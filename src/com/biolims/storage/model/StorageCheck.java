package com.biolims.storage.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.storage.position.model.StoragePosition;

/**
 * 退库申请
 *
 */
@Entity
@Table(name = "T_STORAGE_CHECK")
public class StorageCheck extends EntityDao<StorageCheck> implements Serializable {

	private static final long serialVersionUID = 6245195670352622374L;
	@Id
	@Column(name = "ID", length = 32)
	private String id;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_TYPE_ID")
	private DicType type;//类型

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "USER_HANDLE_USER_ID")
	private User handleUser;//盘点人

	@Column(name = "HANDLE_DATE")
	private Date handleDate;//盘点时间

	@Column(name = "METHOD", length = 200)
	private String method;//方式

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "USER_CREATE_USER_ID")
	private User createUser;//创建人

	@Column(name = "CREATE_DATE")
	private Date createDate;//创建时间

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "USER_CONFIRM_USER_ID")
	private User confirmUser;//批准人

	@Column(name = "CONFIRM_DATE")
	private Date confirmDate;//批准时间

	@Column(name = "START_DATE")
	private Date startDate;//开始时间

	@Column(name = "END_DATE")
	private Date endDate;//结束时间

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_POSITION_ID")
	private StoragePosition position;//存储位置

	@Column(name = "STATE", length = 32)
	private String state;//工作流状态

	@Column(name = "STATE_NAME", length = 200)
	private String stateName;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "S_C_P_ID")
	private StorageCheckPlan storageCheckPlan;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public User getConfirmUser() {
		return confirmUser;
	}

	public void setConfirmUser(User confirmUser) {
		this.confirmUser = confirmUser;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	public DicType getType() {
		return type;
	}

	public void setType(DicType type) {
		this.type = type;
	}

	public User getHandleUser() {
		return handleUser;
	}

	public void setHandleUser(User handleUser) {
		this.handleUser = handleUser;
	}

	public Date getHandleDate() {
		return handleDate;
	}

	public void setHandleDate(Date handleDate) {
		this.handleDate = handleDate;
	}

	public User getCreateUser() {
		return createUser;
	}

	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public StoragePosition getPosition() {
		return position;
	}

	public void setPosition(StoragePosition position) {
		this.position = position;
	}

	public StorageCheckPlan getStorageCheckPlan() {
		return storageCheckPlan;
	}

	public void setStorageCheckPlan(StorageCheckPlan storageCheckPlan) {
		this.storageCheckPlan = storageCheckPlan;
	}

}
