package com.biolims.storage.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.core.model.user.DicCostCenter;
import com.biolims.dao.EntityDao;

/**
 * 领用明细单
 */
@Entity
@Table(name = "T_STORAGE_APPLY_ITEM")
public class StorageApplyItem extends EntityDao<StorageApplyItem> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5024684930978412626L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_APPLY_ID")
	private StorageApply storageApply;//领用申请

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_ID")
	private Storage storage;//领用对象

	@Column(name = "NUM")
	private Double num;//需求数量

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_COST_CENTER_ID")
	private DicCostCenter costCenter;//成本中心

	@Column(name = "STORAGE_NUM")
	private Double storageNum;//需求数量

	//非持久属性
	@Transient
	private String storageName;
	@Transient
	private Double needNum;//需求数量
	@Transient
	private String storageId;
	@Transient
	private String searchCode;//检索码
	@Transient
	private String unitStr;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Storage getStorage() {
		return storage;
	}

	public void setStorage(Storage storage) {
		this.storage = storage;
	}

	public String getSearchCode() {
		return searchCode;
	}

	public void setSearchCode(String searchCode) {
		this.searchCode = searchCode;
	}

	public Double getNum() {
		return num;
	}

	public void setNum(Double num) {
		this.num = num;
	}

	public String getStorageName() {
		return storageName;
	}

	public void setStorageName(String storageName) {
		this.storageName = storageName;
	}

	public String getStorageId() {
		return storageId;
	}

	public void setStorageId(String storageId) {
		this.storageId = storageId;
	}

	public String getUnitStr() {
		return unitStr;
	}

	public void setUnitStr(String unitStr) {
		this.unitStr = unitStr;
	}

	public StorageApply getStorageApply() {
		return storageApply;
	}

	public void setStorageApply(StorageApply storageApply) {
		this.storageApply = storageApply;
	}

	public Double getNeedNum() {
		return needNum;
	}

	public void setNeedNum(Double needNum) {
		this.needNum = needNum;
	}

	public DicCostCenter getCostCenter() {
		return costCenter;
	}

	public void setCostCenter(DicCostCenter costCenter) {
		this.costCenter = costCenter;
	}

	public Double getStorageNum() {
		return storageNum;
	}

	public void setStorageNum(Double storageNum) {
		this.storageNum = storageNum;
	}

}
