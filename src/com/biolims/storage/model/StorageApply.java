package com.biolims.storage.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.core.model.user.DicCostCenter;
import com.biolims.dic.model.DicFinanceItem;
import com.biolims.dic.model.DicType;
import com.biolims.core.model.user.Department;
import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;

/**
 * 领用申请单
 */
@Entity
@Table(name = "T_STORAGE_APPLY")
public class StorageApply extends EntityDao<StorageApply> implements Serializable {

	private static final long serialVersionUID = -9205393071273106229L;
	@Id
	@Column(name = "ID", length = 32)
	private String id;//领用编号

	@Column(name = "NAME", length = 110)
	private String name;//领用名称

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_APPLY_TYPE_ID")
	private DicType useType;//领用类型

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "USER_APPLY_USER_ID")
	private User applyUser;//申请人

	@Column(name = "APPLY_DATE")
	private Date applyDate;//申请时间

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "USER_CREATE_USER_ID")
	private User createUser;//创建人

	@Column(name = "CREATE_DATE")
	private Date createDate;//创建时间

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DEPARTMENT_ID")
	private Department department;//所属组织

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "USER_USE_USER_ID")
	private User useUser;//领用人

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_FINANCE_ITEM_ID")
	private DicFinanceItem financeCode;//财务科目

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "USER_CONFIRM_USER_ID")
	private User confirmUser;//批准人

	@Column(name = "CONFIRM_DATE")
	private Date confirmDate;//批准时间

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "COST_CENTER_ID")
	private DicCostCenter costCenter;//成本中心

	@Column(name = "IS_SYS_SEND", length = 32)
	private String isSysSend; //是否是系统自动创建

	@Column(name = "STATE", length = 32)
	private String state;//工作流状态

	@Column(name = "STATE_NAME", length = 200)
	private String stateName;

	//非持久属性
	@Transient
	private String useTypeStr;//领用类型名称
	@Transient
	private String applyUserStr;//申请人
	@Transient
	private String projectStr;//所属项目
	@Transient
	private String experimentStr;//所属实验
	@Transient
	private String stateStr;//所属实验
	@Transient
	private String useUserId;//
	@Transient
	private String useUserName;

	public String getUseUserId() {
		return useUserId;
	}

	public void setUseUserId(String useUserId) {
		this.useUserId = useUserId;
	}

	public String getUseUserName() {
		return useUserName;
	}

	public void setUseUserName(String useUserName) {
		this.useUserName = useUserName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public DicType getUseType() {
		return useType;
	}

	public void setUseType(DicType useType) {
		this.useType = useType;
	}

	public User getApplyUser() {
		return applyUser;
	}

	public void setApplyUser(User applyUser) {
		this.applyUser = applyUser;
	}

	public Date getApplyDate() {
		return applyDate;
	}

	public void setApplyDate(Date applyDate) {
		this.applyDate = applyDate;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public User getUseUser() {
		return useUser;
	}

	public void setUseUser(User useUser) {
		this.useUser = useUser;
	}

	public String getState() {
		return state;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public User getCreateUser() {
		return createUser;
	}

	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getUseTypeStr() {
		return useTypeStr;
	}

	public void setUseTypeStr(String useTypeStr) {
		this.useTypeStr = useTypeStr;
	}

	public String getApplyUserStr() {
		return applyUserStr;
	}

	public void setApplyUserStr(String applyUserStr) {
		this.applyUserStr = applyUserStr;
	}

	public String getProjectStr() {
		return projectStr;
	}

	public void setProjectStr(String projectStr) {
		this.projectStr = projectStr;
	}

	public String getExperimentStr() {
		return experimentStr;
	}

	public void setExperimentStr(String experimentStr) {
		this.experimentStr = experimentStr;
	}

	public String getStateStr() {
		return stateStr;
	}

	public void setStateStr(String stateStr) {
		this.stateStr = stateStr;
	}

	public DicFinanceItem getFinanceCode() {
		return financeCode;
	}

	public void setFinanceCode(DicFinanceItem financeCode) {
		this.financeCode = financeCode;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public User getConfirmUser() {
		return confirmUser;
	}

	public void setConfirmUser(User confirmUser) {
		this.confirmUser = confirmUser;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	public DicCostCenter getCostCenter() {
		return costCenter;
	}

	public void setCostCenter(DicCostCenter costCenter) {
		this.costCenter = costCenter;
	}

	public String getIsSysSend() {
		return isSysSend;
	}

	public void setIsSysSend(String isSysSend) {
		this.isSysSend = isSysSend;
	}

}
