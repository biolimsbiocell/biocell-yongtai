package com.biolims.storage.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.core.model.user.DicCostCenter;
import com.biolims.dic.model.DicFinanceItem;
import com.biolims.dic.model.DicType;
import com.biolims.core.model.user.Department;
import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;

/**
 * 出库申请
 *
 */
@Entity
@Table(name = "T_STORAGE_OUT")
public class StorageOut extends EntityDao<StorageOut> implements Serializable {

	private static final long serialVersionUID = -4007784866661336388L;

	@Id
	@Column(name = "ID", length = 32)
	private String id;//出库单号
	@Column(name = "NOTE")
	private String note;//描述
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "USER_OUT_USER_ID")
	private User outUser;//出库人

	@Column(name = "OUT_DATE")
	private Date outDate;//出库时间

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "USER_USE_USER_ID")
	private User useUser;//领用人

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_APPLY_ID")
	private StorageApply storageApply;//领用申请

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DEPARTMENT_ID")
	private Department department;//所属组织

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_TYPE_ID")
	private DicType type;//出库类型

	@Column(name = "CONFIRM_DATE")
	private Date confirmDate;//批准时间

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_FINANCE_ITEM_ID")
	private DicFinanceItem financeCode;//财务科目

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "USER_CONFIRM_USER_ID")
	private User confirmUser;//负责人

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "COST_CENTER_ID")
	private DicCostCenter costCenter;//成本中心

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "REGION_TYPE_ID")
	private DicType regionType;//城市

	@Column(name = "STATE", length = 32)
	private String state;//工作流状态

	@Column(name = "STATE_NAME", length = 200)
	private String stateName;
	//非持久属性
	@Transient
	private String typeStr;
	@Transient
	private String useUserStr;
	@Transient
	private String outUserStr;
	@Transient
	private String applyId;//领用单号
	@Transient
	private String deptName;//所属组织
	@Transient
	private String experStr;//所属实验
	@Transient
	private String pjoStr;//所属项目
	@Transient
	private String stateStr;//工作流状态
	
	//出库类型
	private String ctype;
	
	
	private String scopeId;
	private String scopeName;
	
	public String getScopeId() {
		return scopeId;
	}

	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	public String getScopeName() {
		return scopeName;
	}

	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

	public DicFinanceItem getFinanceCode() {
		return financeCode;
	}

	public void setFinanceCode(DicFinanceItem financeCode) {
		this.financeCode = financeCode;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public User getOutUser() {
		return outUser;
	}

	public void setOutUser(User outUser) {
		this.outUser = outUser;
	}

	public User getUseUser() {
		return useUser;
	}

	public void setUseUser(User useUser) {
		this.useUser = useUser;
	}

	public StorageApply getStorageApply() {
		return storageApply;
	}

	public void setStorageApply(StorageApply storageApply) {
		this.storageApply = storageApply;
	}

	public Date getOutDate() {
		return outDate;
	}

	public void setOutDate(Date outDate) {
		this.outDate = outDate;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public DicType getType() {
		return type;
	}

	public void setType(DicType type) {
		this.type = type;
	}

	public String getTypeStr() {
		return typeStr;
	}

	public void setTypeStr(String typeStr) {
		this.typeStr = typeStr;
	}

	public String getUseUserStr() {
		return useUserStr;
	}

	public void setUseUserStr(String useUserStr) {
		this.useUserStr = useUserStr;
	}

	public String getOutUserStr() {
		return outUserStr;
	}

	public void setOutUserStr(String outUserStr) {
		this.outUserStr = outUserStr;
	}

	public String getApplyId() {
		return applyId;
	}

	public void setApplyId(String applyId) {
		this.applyId = applyId;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getExperStr() {
		return experStr;
	}

	public void setExperStr(String experStr) {
		this.experStr = experStr;
	}

	public String getPjoStr() {
		return pjoStr;
	}

	public void setPjoStr(String pjoStr) {
		this.pjoStr = pjoStr;
	}

	public String getStateStr() {
		return stateStr;
	}

	public void setStateStr(String stateStr) {
		this.stateStr = stateStr;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public User getConfirmUser() {
		return confirmUser;
	}

	public void setConfirmUser(User confirmUser) {
		this.confirmUser = confirmUser;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	public DicCostCenter getCostCenter() {
		return costCenter;
	}

	public void setCostCenter(DicCostCenter costCenter) {
		this.costCenter = costCenter;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public DicType getRegionType() {
		return regionType;
	}

	public void setRegionType(DicType regionType) {
		this.regionType = regionType;
	}

	public String getCtype() {
		return ctype;
	}

	public void setCtype(String ctype) {
		this.ctype = ctype;
	}

}
