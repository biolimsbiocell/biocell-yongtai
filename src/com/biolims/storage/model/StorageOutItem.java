package com.biolims.storage.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.core.model.user.DicCostCenter;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicCurrencyType;
import com.biolims.storage.position.model.StoragePosition;
import com.biolims.system.work.model.UnitGroupNew;

import sun.net.www.content.text.plain;

/**
 * 出库明细
 *
 */
@Entity
@Table(name = "T_STORAGE_OUT_ITEM")
public class StorageOutItem extends EntityDao<StorageOutItem> implements Serializable {

	private static final long serialVersionUID = -1191844750007521159L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_OUT_ID")
	private StorageOut storageOut;//出库申请

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_ID")
	private Storage storage;//对象

	@Column(name = "SERIAL", length = 32)
	private String serial;//批次编号

	@Column(name = "CODE", length = 32)
	private String code;//批次编号

	@Column(name = "NUM")
	private Double num;//出库数量

	@Column(name = "PRICE")
	private Double price;//出库价格

	@Column(name = "NEED_NUM")
	private Double needNum;//需求数量

	@Column(name = "STORAGE_NUM")
	private Double storageNum;//库存数量
	@Column(name = "NOTE",length=4000)
	private String note;//sn
	
	@Column(name = "NOTE_TWO")
	private String note2;//备注
	@Column(name = "QC_STATE")
	private String qcState;//QC状态
	@Column(name = "EXPIRE_DATE")
	private Date expireDate;//过期日期
	@Column(name = "BATCH")
	private String batch;//批次号
	
	
	@Column(name = "PACKING_NUM")
	private Double packingNum;//出库数量 主单位
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "UNIT_GROUP_NEW")
	private UnitGroupNew unitGroupNew;// 单位组

	public UnitGroupNew getUnitGroupNew() {
		return unitGroupNew;
	}

	public Double getPackingNum() {
		return packingNum;
	}

	public void setPackingNum(Double packingNum) {
		this.packingNum = packingNum;
	}

	public void setUnitGroupNew(UnitGroupNew unitGroupNew) {
		this.unitGroupNew = unitGroupNew;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_COST_CENTER_ID")
	private DicCostCenter costCenter;//成本中心

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_CURRENCY_TYPE_ID")
	private DicCurrencyType currencyType;//币种

	//非持久对象	
	@Transient
	private String storageId;
	@Transient
	private String storageName;
	@Transient
	private String searchCode;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_POSITON_ID")
	private StoragePosition position;

	
	/**
	 * 是否用完
	 */
	private String batchNum;
	//生产单号
	private String productMark;
	@Column(name = "used_finish")
	private String usedFinish;//是否用完
	
	


	public String getUsedFinish() {
		return usedFinish;
	}

	public void setUsedFinish(String usedFinish) {
		this.usedFinish = usedFinish;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public StorageOut getStorageOut() {
		return storageOut;
	}

	public void setStorageOut(StorageOut storageOut) {
		this.storageOut = storageOut;
	}

	public Storage getStorage() {
		return storage;
	}

	public void setStorage(Storage storage) {
		this.storage = storage;
	}

	public Double getStorageNum() {
		return storageNum;
	}

	public void setStorageNum(Double storageNum) {
		this.storageNum = storageNum;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public String getStorageId() {
		return storageId;
	}

	public void setStorageId(String storageId) {
		this.storageId = storageId;
	}

	public String getStorageName() {
		return storageName;
	}

	public void setStorageName(String storageName) {
		this.storageName = storageName;
	}

	public String getSearchCode() {
		return searchCode;
	}

	public void setSearchCode(String searchCode) {
		this.searchCode = searchCode;
	}

	public Double getNum() {
		return num;
	}

	public void setNum(Double num) {
		this.num = num;
	}

	public Double getNeedNum() {
		return needNum;
	}

	public void setNeedNum(Double needNum) {
		this.needNum = needNum;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public DicCurrencyType getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(DicCurrencyType currencyType) {
		this.currencyType = currencyType;
	}

	public StoragePosition getPosition() {
		return position;
	}

	public void setPosition(StoragePosition position) {
		this.position = position;
	}

	public DicCostCenter getCostCenter() {
		return costCenter;
	}

	public void setCostCenter(DicCostCenter costCenter) {
		this.costCenter = costCenter;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * @param note the note to set
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * @return the note2
	 */
	public String getNote2() {
		return note2;
	}

	/**
	 * @param note2 the note2 to set
	 */
	public void setNote2(String note2) {
		this.note2 = note2;
	}

	/**
	 * @return the qcState
	 */
	public String getQcState() {
		return qcState;
	}

	/**
	 * @param qcState the qcState to set
	 */
	public void setQcState(String qcState) {
		this.qcState = qcState;
	}

	/**
	 * @return the expireDate
	 */
	public Date getExpireDate() {
		return expireDate;
	}

	/**
	 * @param expireDate the expireDate to set
	 */
	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public String getProductMark() {
		return productMark;
	}

	public void setProductMark(String productMark) {
		this.productMark = productMark;
	}

}
