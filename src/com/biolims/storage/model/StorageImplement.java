package com.biolims.storage.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dic.model.DicCurrencyType;
import com.biolims.dao.EntityDao;

/**
 * 器具
 *
 */
@Entity
@Table(name = "T_STORAGE_IMPLEMENT")
public class StorageImplement extends EntityDao<StorageImplement> implements Serializable {

	private static final long serialVersionUID = 7142910679758466482L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_ID")
	private Storage storageId;//库存主数据

	@Column(name = "STORAGE_BREED", length = 30)
	private String storageBreed;//品牌

	@Column(name = "STORAGE_PRODUCT_ADDRESS", length = 60)
	private String storageProductAddress;//产地

	@Column(name = "OUT_PRICE")
	private Double outPrice;//出库价格

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_CURRENCY_TYPE_ID")
	private DicCurrencyType currencyType;//币种

	@Column(name = "REMARK", length = 50)
	private String remark;//备注

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Storage getStorageId() {
		return storageId;
	}

	public void setStorageId(Storage storageId) {
		this.storageId = storageId;
	}

	public String getStorageBreed() {
		return storageBreed;
	}

	public void setStorageBreed(String storageBreed) {
		this.storageBreed = storageBreed;
	}

	public String getStorageProductAddress() {
		return storageProductAddress;
	}

	public void setStorageProductAddress(String storageProductAddress) {
		this.storageProductAddress = storageProductAddress;
	}

	public Double getOutPrice() {
		return outPrice;
	}

	public void setOutPrice(Double outPrice) {
		this.outPrice = outPrice;
	}

	public DicCurrencyType getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(DicCurrencyType currencyType) {
		this.currencyType = currencyType;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}
