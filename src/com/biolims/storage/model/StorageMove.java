package com.biolims.storage.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.storage.position.model.StoragePosition;

/**
 * 退库申请
 *
 */
@Entity
@Table(name = "T_STORAGE_MOVE")
public class StorageMove extends EntityDao<StorageMove> implements Serializable {

	private static final long serialVersionUID = 6245195670352622374L;

	@Id
	@Column(name = "ID", length = 32)
	private String id;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_TYPE_ID")
	private DicType type;//类型

	@Column(name = "REASON", length = 100)
	private String reason;//原因

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "USER_HANDLE_USER_ID")
	private User handleUser;//处理人

	@Column(name = "HANDLE_DATE")
	private Date handleDate;//处理时间

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_POSITION_ID")
	private StoragePosition oldPosition;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "USER_CREATE_USER_ID")
	private User createUser;//创建人

	@Column(name = "CREATE_DATE")
	private Date createDate;//创建时间

	@Column(name = "CONFIRM_DATE")
	private Date confirmDate;//批准时间

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public DicType getType() {
		return type;
	}

	public void setType(DicType type) {
		this.type = type;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public User getHandleUser() {
		return handleUser;
	}

	public void setHandleUser(User handleUser) {
		this.handleUser = handleUser;
	}

	public Date getHandleDate() {
		return handleDate;
	}

	public void setHandleDate(Date handleDate) {
		this.handleDate = handleDate;
	}

	public User getCreateUser() {
		return createUser;
	}

	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public StoragePosition getOldPosition() {
		return oldPosition;
	}

	public void setOldPosition(StoragePosition oldPosition) {
		this.oldPosition = oldPosition;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

}
