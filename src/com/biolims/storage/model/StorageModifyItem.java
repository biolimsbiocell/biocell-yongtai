package com.biolims.storage.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;

/**
 * 退库明细对象
 *
 */
@Entity
@Table(name = "T_STORAGE_MODIFY_ITEM")
public class StorageModifyItem extends EntityDao<StorageModifyItem> implements Serializable {

	private static final long serialVersionUID = -4481968484077357614L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_ID")
	private Storage storage;//对象信息

	@Column(name = "NUM")
	private Double num;//数量

	@Column(name = "SERIAL", length = 32)
	private String serial;//批次编码

	@Column(name = "CODE", length = 32)
	private String code;//批号

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_MODIFY_ID")
	private StorageModify storageModify;//退库申请

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "REGION_TYPE_ID")
	private DicType regionType;//城市

	//非持久化对象
	private Double outPrice;//出库价格

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Storage getStorage() {
		return storage;
	}

	public void setStorage(Storage storage) {
		this.storage = storage;
	}

	public Double getNum() {
		return num;
	}

	public void setNum(Double num) {
		this.num = num;
	}

	public StorageModify getStorageModify() {
		return storageModify;
	}

	public void setStorageModify(StorageModify storageModify) {
		this.storageModify = storageModify;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public Double getOutPrice() {
		return outPrice;
	}

	public void setOutPrice(Double outPrice) {
		this.outPrice = outPrice;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public DicType getRegionType() {
		return regionType;
	}

	public void setRegionType(DicType regionType) {
		this.regionType = regionType;
	}

}
