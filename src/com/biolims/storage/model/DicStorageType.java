package com.biolims.storage.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

/**
 * 库存数据类型
 */
@Entity
@Table(name = "T_DIC_STORAGE_TYPE")
public class DicStorageType implements Serializable {
	private static final long serialVersionUID = 663439888743391745L;
	@Id
	@Column(name = "ID", length = 30)
	private String id;
	@Column(name = "NAME", length = 10)
	private String name;//名称
	@Column(name = "STATE", length = 2)
	private String state;//状态

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_STORAGE_MAIN_TYPE_ID")
	private DicStorageMainType type;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public DicStorageMainType getType() {
		return type;
	}

	public void setType(DicStorageMainType type) {
		this.type = type;
	}

}
