package com.biolims.storage.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.core.model.user.DicCostCenter;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicCurrencyType;
import com.biolims.dic.model.DicType;
import com.biolims.dic.model.DicUnit;
import com.biolims.storage.position.model.StoragePosition;

//import com.biolims.supplier.model.Supplier;

/**
 * 采购试剂明细
 * 
 */

@Entity
@Table(name = "t_STORAGE_REAGENT_BUY_SERIAL")
public class StorageReagentBuySerial extends EntityDao<StorageReagentBuySerial>
		implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4923478264834061065L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;// id

	@Column(name = "SERIAL", length = 32)
	private String serial;// 批号

	@Column(name = "CODE", length = 32)
	private String code;// 证书号

	@Column(name = "PRODUCT_DATE")
	private Date productDate;// 生产日期

	@Column(name = "EXPIRE_DATE")
	private Date expireDate;// 失效期

	@Column(name = "REMIND_DATE")
	private Date remindDate;// 提醒日期

	@Column(name = "IN_DATE")
	private Date inDate;// 入库日期

	@Column(name = "NUM")
	private Double num;// 本批次存量

	@Column(name = "USE_NUM")
	private Double useNum;// 本批次存量

	@Column(name = "PURCHASE_PRICE")
	private Double purchasePrice;// 本批次采购存量

	@Column(name = "SUPPLIER_NAME", length = 200)
	private String supplierName;// 供应商

	@Column(name = "SUPPLIER_LINK_TEL", length = 100)
	private String supplierLinkTel;// 联系电话

	@Column(name = "SUPPLIER_FAX", length = 100)
	private String supplierFax;// 传真

	@Column(name = "SPEC", length = 100)
	private String spec;// 传真

	@Column(name = "IS_GOOD", length = 32)
	private String isGood;// 0:不合格,1:合格

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_ID")
	private Storage storage; // 库存主数据

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_COST_CENTER_ID")
	private DicCostCenter costCenter;// 成本中心

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_POSITION_ID")
	private StoragePosition position;// 位置

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_IN_ITEM_ID")
	private StorageInItem storageInItem;// 入库明细

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "REGION_TYPE_ID")
	private DicType regionType;// 城市

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "LINK_STORAGE_ITEM_ID")
	private StorageReagentBuySerial linkStorageItem;// 关联明细

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_RANK_TYPE_ID")
	private DicType rankType;// 币种

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_CURRENCY_TYPE_ID")
	private DicCurrencyType currencyType;// 币种

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_TIME_UNIT_ID")
	private DicUnit timeUnit;// 时间单位

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_UNIT_ID")
	private DicUnit unit;// 单位

	@Column(name = "QC_STATE", length = 32)
	private String qcState;// QC状态
	@Column(name = "NOTE", length = 4000)
	private String note;// sn
	@Column(name = "NOTE2", length = 4000)
	private String note2;// 备注
	@Column(name = "QC_PASS_DATE", length = 32)
	private Date qcPassDate;// QC通过日期
	// 反应总数
	private Double recationSum;

	
	@Column(name = "SCOPE_ID", length = 200)
	private String scopeId;

	@Column(name = "SCOPE_NAME", length = 200)
	private String scopeName;
	public Double getRecationSum() {
		return recationSum;
	}

	public void setRecationSum(Double recationSum) {
		this.recationSum = recationSum;
	}

	public String getQcState() {
		return qcState;
	}

	public void setQcState(String qcState) {
		this.qcState = qcState;
	}

	// @ManyToOne(fetch = FetchType.LAZY)
	// @NotFound(action = NotFoundAction.IGNORE)
	// @JoinColumn(name = "SUPPLIER_ID")
	// private Supplier supplier;//供应商
	// 非持 久对象
	@Transient
	private Double outPrice;// 出库价格

	@Transient
	private String currencyTypeStr;// 币种

	@Transient
	private String unitStr;// 单位

	public String getId() {
		return id;
	}

	public Double getNum() {
		return num;
	}

	public void setNum(Double num) {
		this.num = num;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getProductDate() {
		return productDate;
	}

	public void setProductDate(Date productDate) {
		this.productDate = productDate;
	}

	public DicUnit getTimeUnit() {
		return timeUnit;
	}

	public void setTimeUnit(DicUnit timeUnit) {
		this.timeUnit = timeUnit;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public Date getRemindDate() {
		return remindDate;
	}

	public void setRemindDate(Date remindDate) {
		this.remindDate = remindDate;
	}

	public Date getInDate() {
		return inDate;
	}

	public void setInDate(Date inDate) {
		this.inDate = inDate;
	}

	public Double getOutPrice() {
		return outPrice;
	}

	public void setOutPrice(Double outPrice) {
		this.outPrice = outPrice;
	}

	public DicCurrencyType getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(DicCurrencyType currencyType) {
		this.currencyType = currencyType;
	}

	public DicUnit getUnit() {
		return unit;
	}

	public void setUnit(DicUnit unit) {
		this.unit = unit;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getSupplierLinkTel() {
		return supplierLinkTel;
	}

	public void setSupplierLinkTel(String supplierLinkTel) {
		this.supplierLinkTel = supplierLinkTel;
	}

	public String getSupplierFax() {
		return supplierFax;
	}

	public void setSupplierFax(String supplierFax) {
		this.supplierFax = supplierFax;
	}

	public Storage getStorage() {
		return storage;
	}

	public void setStorage(Storage storage) {
		this.storage = storage;
	}

	public String getCurrencyTypeStr() {
		return currencyTypeStr;
	}

	public void setCurrencyTypeStr(String currencyTypeStr) {
		this.currencyTypeStr = currencyTypeStr;
	}

	public String getUnitStr() {
		return unitStr;
	}

	public void setUnitStr(String unitStr) {
		this.unitStr = unitStr;
	}

	public Double getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(Double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public StoragePosition getPosition() {
		return position;
	}

	public void setPosition(StoragePosition position) {
		this.position = position;
	}

	public StorageInItem getStorageInItem() {
		return storageInItem;
	}

	public void setStorageInItem(StorageInItem storageInItem) {
		this.storageInItem = storageInItem;
	}

	public String getIsGood() {
		return isGood;
	}

	public void setIsGood(String isGood) {
		this.isGood = isGood;
	}

	public DicCostCenter getCostCenter() {
		return costCenter;
	}

	public void setCostCenter(DicCostCenter costCenter) {
		this.costCenter = costCenter;
	}

	public StorageReagentBuySerial getLinkStorageItem() {
		return linkStorageItem;
	}

	public void setLinkStorageItem(StorageReagentBuySerial linkStorageItem) {
		this.linkStorageItem = linkStorageItem;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public DicType getRankType() {
		return rankType;
	}

	public void setRankType(DicType rankType) {
		this.rankType = rankType;
	}

	public String getSpec() {
		return spec;
	}

	public void setSpec(String spec) {
		this.spec = spec;
	}

	//
	// public Supplier getSupplier() {
	// return supplier;
	// }
	//
	// public void setSupplier(Supplier supplier) {
	// this.supplier = supplier;
	// }

	public Double getUseNum() {
		return useNum;
	}

	public void setUseNum(Double useNum) {
		this.useNum = useNum;
	}

	public DicType getRegionType() {
		return regionType;
	}

	public void setRegionType(DicType regionType) {
		this.regionType = regionType;
	}

	/**
	 * @return the note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * @param note
	 *            the note to set
	 */
	public void setNote(String note) {
		this.note = note;
	}

	public String getNote2() {
		return note2;
	}

	public void setNote2(String note2) {
		this.note2 = note2;
	}

	/**
	 * @return the qcPassDate
	 */
	public Date getQcPassDate() {
		return qcPassDate;
	}

	/**
	 * @param qcPassDate
	 *            the qcPassDate to set
	 */
	public void setQcPassDate(Date qcPassDate) {
		this.qcPassDate = qcPassDate;
	}

	public String getScopeId() {
		return scopeId;
	}

	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	public String getScopeName() {
		return scopeName;
	}

	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

}
