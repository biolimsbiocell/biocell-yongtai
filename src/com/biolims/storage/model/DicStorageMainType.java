package com.biolims.storage.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 库存数据大类
 *
 */
@Entity
@Table(name = "T_DIC_STORAGE_MAIN_TYPE")
public class DicStorageMainType implements Serializable {

	private static final long serialVersionUID = 2709884145587588210L;

	@Id
	@Column(name = "ID", length = 30)
	private String id;

	@Column(name = "NAME", length = 40)
	private String name;

	@Column(name = "STATE", length = 2)
	private String state;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}
