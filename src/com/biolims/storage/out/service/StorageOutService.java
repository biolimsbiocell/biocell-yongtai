package com.biolims.storage.out.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.applicationType.service.ApplicationTypeService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.SystemCodeService;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.model.LogInfo;
import com.biolims.storage.common.constants.SystemConstants;
import com.biolims.storage.common.dao.StorageCommonDao;
import com.biolims.storage.main.service.StorageService;
import com.biolims.storage.model.DicStorageType;
import com.biolims.storage.model.Storage;
import com.biolims.storage.model.StorageApply;
import com.biolims.storage.model.StorageApplyItem;
import com.biolims.storage.model.StorageIn;
import com.biolims.storage.model.StorageInItem;
import com.biolims.storage.model.StorageOut;
import com.biolims.storage.model.StorageOutItem;
import com.biolims.storage.model.StorageReagentBuySerial;
import com.biolims.storage.out.dao.StorageOutDao;
import com.biolims.system.work.model.UnitGroupNew;
import com.biolims.util.BeanUtils;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@Transactional
@SuppressWarnings({ "unchecked" })
public class StorageOutService extends ApplicationTypeService {

	@Resource
	private StorageOutDao storageOutDao;

	@Resource
	private StorageCommonDao storageCommonDao;
	@Resource
	private StorageService storageService;

	@Resource
	private CommonDAO commonDAO;

	@Resource
	private SystemCodeService systemCodeService;

	/**
	 * 领用申请列表
	 * 
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param so
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findStorageOutList(int startNum, int limitNum, String dir, String sort, String data)
			throws Exception {
		// 检索条件map,每个字段检索值
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		// 检索方式map,每个字段的检索方式
		Map<String, String> mapForCondition = new HashMap<String, String>();
		String startDate = "";
		String endDate = "";
		if (data != null && !data.equals("")) {
			BeanUtils.data2Map(data, mapForQuery, mapForCondition);
			mapForQuery.remove("queryStartDate");
			mapForQuery.remove("queryEndDate");
			mapForCondition.remove("queryStartDate");
			mapForCondition.remove("queryEndDate");
		}
		Map<String, Object> criteriaMap = storageOutDao.findObjectCriteria(startNum, limitNum, dir, sort,
				StorageOut.class, mapForQuery, mapForCondition);
		Criteria cr = (Criteria) criteriaMap.get("criteria");
		Criteria cc = (Criteria) criteriaMap.get("criteriaCount");
		Map<String, Object> map = JsonUtils.toObjectByJson(data, Map.class);
		if (data != null && !data.equals("")) {
			if (map.get("queryStartDate") != null && !map.get("queryStartDate").equals("")) {
				startDate = (String) map.get("queryStartDate");
				cr.add(Restrictions.ge("outDate", DateUtil.parse(startDate)));
				cc.add(Restrictions.ge("outDate", DateUtil.parse(startDate)));
			}
			if (map.get("queryEndDate") != null && !map.get("queryEndDate").equals("")) {
				endDate = (String) map.get("queryEndDate");
				cr.add(Restrictions.le("outDate", DateUtil.parse(endDate)));
				cc.add(Restrictions.le("outDate", DateUtil.parse(endDate)));
			}
		}
		Map<String, Object> controlMap = storageOutDao.findObjectList(startNum, limitNum, dir, sort, StorageOut.class,
				mapForQuery, mapForCondition, cc, cr);
		return controlMap;
	}

	/**
	 * 根据ID查询指定的出库申请
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public StorageOut findStorageOutById(String id) throws Exception {
		return this.storageOutDao.get(StorageOut.class, id);
	}

	/**
	 * 保存修改出库申请
	 * 
	 * @param so
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveOrModifyStorageOut(StorageOut so, String data) throws Exception {
		this.storageOutDao.insertOrUpdateStorageOut(so);
		this.saveOrModifyStorageOutItem(so.getId(), data);
	}

	/**
	 * 保存修改出库申请
	 * 
	 * @param so
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(StorageOut so) throws Exception {
		this.storageOutDao.insertOrUpdateStorageOut(so);
	}

	/**
	 * 保存ByJSon
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void save(String jsonString, String outId) throws Exception {
		jsonString = jsonString.replaceAll("so\\.", "");
		jsonString = jsonString.replaceAll("\\.id", "-id");
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(jsonString, List.class);
		for (Map<String, Object> map : list) {
			StorageOut em = new StorageOut();
			// 将map信息读入实体类
			em = (StorageOut) storageOutDao.Map2Bean(map, em);
			if (outId != null)
				em.setId(outId);
			storageOutDao.saveOrUpdate(em);
		}
	}

	/**
	 * 出库明细对象
	 * 
	 * @param applyId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 */
	public Map<String, Object> findStorageOutItemList(String outId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map = this.storageOutDao.selectStorageOutItemList(outId, startNum, limitNum, dir, sort);
		List<StorageOutItem> list = (List<StorageOutItem>) map.get("result");
		map.put("result", list);
		return map;
	}

	public List<StorageOutItem> findStorageOutItemList(String outId) throws Exception {
		List<StorageOutItem> list = this.storageOutDao.findStorageOutItemList(outId);
		return list;
	}

	/**
	 * 出库明细对象
	 * 
	 * @param applyId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 */

	/**
	 * 查询所有领用申请
	 * 
	 * @param startNum
	 * @param limitNum
	 * @return
	 */
	public Map<String, Object> findAllStorageApply(Integer startNum, Integer limitNum) {
		Map<String, Object> map = this.storageOutDao.selectAllStorageApply(startNum, limitNum);
		if (map != null) {
			List<StorageApply> list = (List<StorageApply>) map.get("result");
			for (StorageApply sa : list) {
				User useUser = sa.getUseUser();
				if (useUser != null) {
					sa.setUseUserId(useUser.getId());
					sa.setUseUserName(useUser.getName());
				}
			}
		}
		return map;
	}

	/**
	 * 领用明细
	 * 
	 * @param applyId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findStorageApplyItemList(String applyId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();

		StorageApplyItem sai = this.storageOutDao.selectStorageApplyItemByApplyId(applyId);
		if (sai != null) {
			Storage s = sai.getStorage();
			DicStorageType dst = s.getType();
			map = this.storageOutDao.selectStorageApplyItemList(dst.getId(), s.getId(), startNum, limitNum, dir, sort);

		}
		return map;
	}

	/**
	 * 保存或添加出库明细数据
	 * 
	 * @param outId
	 * @param data
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveOrModifyStorageOutItem(String outId, String data) throws Exception {
		this.storageOutDao.delStorageOutItemByOutId(outId);

	}

	/**
	 * 维护明细信息,由ext ajax调用
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String saveStorageOutItem(String outId, String jsonString) throws Exception {
		// 将json读入map
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(jsonString, List.class);
		for (Map<String, Object> map : list) {
			StorageOutItem em = new StorageOutItem();
			// 将map信息读入实体类
			em = (StorageOutItem) storageOutDao.Map2Bean(map, em);
			if (em.getId() != null && em.getId().equals("")) {
				em.setId(null);
			}
			if (em.getNote() != null && !"".equals(em.getNote())) {
				StorageReagentBuySerial sbs = storageService.getReagentSerialBySn(em.getNote());
				if (sbs != null) {
					em.setPosition(sbs.getPosition());
					em.setSerial(sbs.getId());
					em.setQcState(sbs.getQcState());
					em.setCode(sbs.getSerial());
					if (sbs.getStorage() != null) {
						em.setStorageNum(sbs.getStorage().getNum());
					}
					em.setExpireDate(sbs.getExpireDate());
					em.setStorage(sbs.getStorage());
					Double d = 1.00;
					em.setNum(d);
				}
			}
			StorageOut ep = new StorageOut();
			ep.setId(outId);
			em.setStorageOut(ep);
			storageOutDao.saveOrUpdate(em);

		}
		return jsonString;
	}

	/**
	 * 删除出库申请中指定的明细数据
	 * 
	 * @param outId
	 * @param storageId
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delStorageOutItemByOutIdAndStorageId(String outId, String storageId) throws Exception {
		StorageOutItem soi = this.storageOutDao.selectStorageOutItemByOutIdAndStorageId(outId, storageId);
		this.storageOutDao.delete(soi);
	}

	/**
	 * 
	 * 删除明细,由ext ajax调用
	 * 
	 * @param id
	 * @return void
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void deStorageOutItem(String[] ids) {
		for (String id : ids) {
			StorageOutItem ppi = commonDAO.get(StorageOutItem.class, id);
			ppi.setId(id);
			storageOutDao.delete(ppi);

			if (ids.length > 0) {
				LogInfo li = new LogInfo();
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setLogDate(new Date());
				li.setFileId(ppi.getStorage().getId());
				li.setUserId(u.getId());
				li.setClassName("StorageOut");
				li.setModifyContent(
						"出库管理:" + "原辅料编号:" + ppi.getStorage().getId() + "名称:" + ppi.getStorage().getName() + "的数据已删除");
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}
		}
	}

	/**
	 * 
	 * @param applicationTypeActionId
	 * @param formId
	 * @return
	 * @throws Exception
	 */
	@Transactional(rollbackFor = Exception.class)
	public boolean storageOutSetStorage(String applicationTypeActionId, String formId) throws Exception {
		StorageOut sc = this.storageOutDao.get(StorageOut.class, formId);
		sc.setState("1");
		sc.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_COMPLETE_NAME);
		List<StorageOutItem> list = findStorageOutItemList(formId);
		for (StorageOutItem em : list) {
			Storage s = get(em.getStorage().getClass(), em.getStorage().getId());
			Double nn = 0.0;
			if (em.getPackingNum() != null) {

				// 库存数量=原数量-出库数量
				if (s.getNum() != null) {
					nn = s.getNum();
					nn -= em.getPackingNum();
					s.setNum(nn);
				}

				// 领用数量=原数量-出库数量
				Double l = 0.0;
				if (s.getLyNum() != null) {
					l = s.getLyNum();
					s.setLyNum(l - em.getPackingNum());
				}

			}
			saveOrUpdate(sc);
			saveOrUpdate(s);

			if (SystemConstants.DIC_STORAGE_PRICE_METHOD.equals("moveAverage")) {
				Double rn = 0.0;
				Double srn = 0.0;
				if (s.getType().getId().startsWith(SystemConstants.DIC_STORAGE_SHIYANCAILIAO_YPC)) {
					if (em.getSerial() != null && !em.getSerial().equals("")) {
						StorageReagentBuySerial srb = get(StorageReagentBuySerial.class, em.getSerial());

						if (em.getPackingNum() != null && srb.getNum() != null) {
							rn = em.getPackingNum();

							srn = srb.getNum();
							srb.setNum(srn - rn);

						}
						// 库存数量=原数量-出库数量

						saveOrUpdate(srb);

					}
				}
				if (s.getType().getId().startsWith(SystemConstants.DIC_STORAGE_SHIYANCAILIAO_NPC)) {
					this.updateStorageNPC(em);
				}
			}
//			if (SystemConstants.DIC_STORAGE_PRICE_METHOD.equals("groupPrice")) {
//				Double rn = 0.0;
//				Double srn = 0.0;
//				if (s.getType().getId().startsWith(SystemConstants.DIC_STORAGE_SHIYANCAILIAO_YPC)) {
//					if (em.getSerial() != null && !em.getSerial().equals("")) {
//						StorageReagentBuySerial srb = get(StorageReagentBuySerial.class, em.getSerial());
//
//						if (em.getNum() != null && srb.getNum() != null) {
//							rn = em.getNum();
//
//							srn = srb.getNum();
//							srb.setNum(srn - rn);
//							if (srb.getUseNum() != null)
//								srb.setUseNum(srb.getUseNum() + rn);
//							else
//								srb.setUseNum(rn);
//						}
//						// 库存数量=原数量-出库数量
//
//						saveOrUpdate(srb);
//
//					} else {
//						List<StorageReagentBuySerial> srbsListForSave = new ArrayList<StorageReagentBuySerial>();
//						// 批价格算法
//						Double emNum = em.getNum();
//
//						List<StorageReagentBuySerial> srbsList = storageCommonDao
//								.selectStorageReagentSerialByStorageId(em.getStorage().getId());
//						for (StorageReagentBuySerial srbs : srbsList) {
//							if (srbs.getNum() > 0) {
//								if (srbs.getNum() > emNum) {
//									srbs.setNum(srbs.getNum() - em.getNum());
//									srbsListForSave.add(srbs);
//									break;
//								} else {
//									emNum = emNum - srbs.getNum();
//									srbs.setNum(0d);
//									srbsListForSave.add(srbs);
//								}
//
//							}
//						}
//						this.storageOutDao.saveOrUpdateAll(srbsListForSave);
//					}
//				}
//
//			}
			if (SystemConstants.DIC_STORAGE_PRICE_METHOD.equals("groupPrice")) {
				Double rn = 0.0;
				Double srn = 0.0;
				if (s.getType().getId().startsWith(SystemConstants.DIC_STORAGE_SHIYANCAILIAO_YPC)) {
					if (em.getSerial() != null && !em.getSerial().equals("")) {
						StorageReagentBuySerial srb = get(StorageReagentBuySerial.class, em.getSerial());

						if (em.getPackingNum() != null && srb.getNum() != null) {
							rn = em.getPackingNum();

							srn = srb.getNum();
							srb.setNum(srn - rn);
							if (srb.getUseNum() != null)
								srb.setUseNum(srb.getUseNum() + rn);
							else
								srb.setUseNum(rn);
						}
						// 库存数量=原数量-出库数量

						saveOrUpdate(srb);

					} else {
						List<StorageReagentBuySerial> srbsListForSave = new ArrayList<StorageReagentBuySerial>();
						// 批价格算法
						Double emNum = em.getPackingNum();

						List<StorageReagentBuySerial> srbsList = storageCommonDao
								.selectStorageReagentSerialByStorageId(em.getStorage().getId());
						for (StorageReagentBuySerial srbs : srbsList) {
							if (srbs.getNum() > 0) {
								if (srbs.getNum() > emNum) {
									srbs.setNum(srbs.getNum() - em.getPackingNum());
									srbsListForSave.add(srbs);
									break;
								} else {
									emNum = emNum - srbs.getNum();
									srbs.setNum(0d);
									srbsListForSave.add(srbs);
								}

							}
						}
						this.storageOutDao.saveOrUpdateAll(srbsListForSave);
					}
				}

			}
		}
		return true;
	}

	public Double groupCompute(StorageOutItem soi) throws Exception {
		Double price = null;
		// 移动平均算法
		Storage storage = this.get(Storage.class, soi.getStorage().getId());
		// 批次算法
		if (storage.getType().getId().startsWith(SystemConstants.DIC_STORAGE_SHIYANCAILIAO_YPC)) {
			price = this.getPriceByYPC(soi);
		} else if (storage.getType().getId().startsWith(SystemConstants.DIC_STORAGE_SHIYANCAILIAO_NPC)) {
			price = this.getPriceByNPC(soi);
		}
		return price;
	}

	/**
	 * 有批次-出库价格
	 */
	public Double getPriceByYPC(StorageOutItem soi) throws Exception {
		Double price = 0d;
		if (soi != null) {
			Map<String, String> mapForQuery = new HashMap<String, String>();
			mapForQuery.put("id", soi.getSerial());
			StorageReagentBuySerial srbs = this.storageOutDao.get(StorageReagentBuySerial.class, soi.getSerial());
			price = srbs.getOutPrice();
		}
		return price;
	}

	/**
	 * 无批次-计算出库单价
	 */
	public Double getPriceByNPC(StorageOutItem soi) throws Exception {
		List<StorageReagentBuySerial> list = this.storageOutDao.findByProperty(StorageReagentBuySerial.class,
				"storage.id", soi.getStorage().getId());
		double totalPrice = 0d;
		double outNum = soi.getNum();
		if (outNum > 0) {
			double price = 0d;
			for (StorageReagentBuySerial srbs : list) {
				double num = srbs.getNum();
				double outPrice = srbs.getOutPrice();
				if (outNum == 0) {
					break;
				} else if (outNum > 0 && outNum >= num) {
					price = price + num * outPrice;
					outNum -= num;
				} else if (outNum > 0 && outNum < num) {
					srbs.setNum(num - outNum);
					price = price + outNum * outPrice;
					outNum = 0;
				}
			}
			totalPrice = price / soi.getNum();
		}
		return totalPrice;
	}

	/**
	 * 无批次-动作
	 * 
	 * @param list
	 * @param outNum
	 */
//	public void updateStorageNPC(StorageOutItem soi) throws Exception {
//		List<StorageReagentBuySerial> list = this.storageOutDao.findByProperty(StorageReagentBuySerial.class,
//				"storage.id", soi.getStorage().getId());
//		List<StorageReagentBuySerial> srbslist = new ArrayList<StorageReagentBuySerial>();
//		double outNum = soi.getNum();
//		double price = 0d;
//		for (StorageReagentBuySerial srbs : list) {
//			double num = srbs.getNum();
//			double outPrice = srbs.getOutPrice();
//			if (outNum == 0) {
//				break;
//			} else if (outNum > 0 && outNum >= num) {
//				srbs.setNum(0d);
//				srbslist.add(srbs);
//				price = price + num * outPrice;
//				outNum -= num;
//			} else if (outNum > 0 && outNum < num) {
//				srbs.setNum(num - outNum);
//				srbslist.add(srbs);
//				price = price + outNum * outPrice;
//				outNum = 0;
//			}
//		}
//		this.storageOutDao.saveOrUpdateAll(srbslist);
//	}
	// 后修改
	public void updateStorageNPC(StorageOutItem soi) throws Exception {
		List<StorageReagentBuySerial> list = this.storageOutDao.findByProperty(StorageReagentBuySerial.class,
				"storage.id", soi.getStorage().getId());
		List<StorageReagentBuySerial> srbslist = new ArrayList<StorageReagentBuySerial>();
		double outNum = soi.getPackingNum();
		double price = 0d;
		for (StorageReagentBuySerial srbs : list) {
			double num = srbs.getNum();
			double outPrice = srbs.getOutPrice();
			if (outNum == 0) {
				break;
			} else if (outNum > 0 && outNum >= num) {
				srbs.setNum(0d);
				srbslist.add(srbs);
				price = price + num * outPrice;
				outNum -= num;
			} else if (outNum > 0 && outNum < num) {
				srbs.setNum(num - outNum);
				srbslist.add(srbs);
				price = price + outNum * outPrice;
				outNum = 0;
			}
		}
		this.storageOutDao.saveOrUpdateAll(srbslist);
	}

	/**
	 * 根据sn查询出库明细
	 */
	public StorageOutItem findStorageOutItem(String sn) {

		return this.storageOutDao.findStorageOutItem(sn);

	}

	@Transactional(rollbackFor = Exception.class)
	public void alertStorageOut(String id) throws Exception {
		List<StorageOutItem> list = findStorageOutItemList(id);
		StorageOut so = get(StorageOut.class, id);
		so.setState("3");
		so.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_EDIT_NAME);
		for (StorageOutItem soi : list) {
			Storage s = soi.getStorage();
			Double nn = 0.0;
			if (s.getNum() != null) {
				nn = s.getNum();
				nn += soi.getNum();
				s.setNum(nn);
			}
			StorageReagentBuySerial srb = storageService.getReagentSerialBySn(soi.getNote());
			Double sn = 0.0;
			if (srb.getNum() != null) {
				sn = srb.getNum();
				sn += soi.getNum();
				srb.setNum(sn);
			}
			saveOrUpdate(so);
			saveOrUpdate(srb);
			saveOrUpdate(soi);
			saveOrUpdate(s);
		}
	}

	public Map<String, Object> addStorageItembyNote(String note) {
		Map<String, Object> map = new HashMap<String, Object>();
		StorageReagentBuySerial srbs = storageOutDao.addStorageItembyNote(note);

		if (srbs != null) {

			map.put("storageId", srbs.getStorage().getId());
			map.put("storageName", srbs.getStorage().getName());
			map.put("storageUnitName", srbs.getStorage().getUnit().getName());
			map.put("price", srbs.getStorage().getOutPrice());
//			map.put("storage-currencyType-name", srbs.getStorage().getCurrencyType().getName());
			map.put("storageTypeId", srbs.getStorage().getType().getId());
			map.put("storageTypeAame", srbs.getStorage().getType().getName());
			map.put("storageNum", srbs.getStorage().getNum());
			map.put("serial", srbs.getId());
			map.put("code", srbs.getSerial());
			map.put("qcState", srbs.getQcState());
			map.put("positionId", srbs.getPosition().getId());
			map.put("positionName", srbs.getPosition().getName());
			map.put("price", srbs.getPurchasePrice());
			map.put("storageJdeCode", srbs.getStorage().getJdeCode());

		}
		return map;
	}

	public Map<String, Object> selStorageOutItemTable(String scId, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return storageOutDao.findStorageOutItemTable(scId, start, length, query, col, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveStorageOutItemTable(String id, String item, String logInfo) throws Exception {
		// 将json读入map
		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(item, List.class);
		StorageOut ep = commonDAO.get(StorageOut.class, id);

		for (Map<String, Object> map : list) {
			StorageOutItem em = new StorageOutItem();

			// 将map信息读入实体类
			em = (StorageOutItem) commonDAO.Map2Bean(map, em);
			if (em.getId() != null && em.getId().equals("")) {
				em.setId(null);
			}
//			if (em.getStorage() == null) {
//				Storage s = storageService.getStorageByName((String) map
//						.get("storage-name"));
//				em.setStorage(s);
//			}
			em.setStorageOut(ep);
			if (em.getUnitGroupNew() != null) {
				UnitGroupNew unitGroupNew = commonDAO.get(UnitGroupNew.class, em.getUnitGroupNew().getId());
				Integer cycle = unitGroupNew.getCycle();
				if (cycle != null) {
					Integer integer = new Integer(cycle);
					int intValue = integer.intValue();
					Double valueOf = Double.valueOf(String.valueOf(intValue));
					if (em.getPackingNum() != null) {
						Double double1 = valueOf * em.getPackingNum();
						em.setNum(double1);
					}
				}

			}
			commonDAO.saveOrUpdate(em);
		}

		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setUserId(u.getId());
			li.setFileId(id);
			li.setClassName("StorageOut");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}

	}

	public Map<String, Object> selStorageOutTable(Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return storageOutDao.findStorageOutTable(start, length, query, col, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String addStorageOutItem(String note, String useUserId, String id, String outDate, String outUserId,
			String batchIds) throws Exception {

		// 保存主表信息
		String log="";
		StorageOut so = new StorageOut();
		if (id == null || "".equals(id) || "NEW".equals(id)) {
			log = "123";
			String code = systemCodeService.getCodeByPrefix("StorageOut",
					"ME" + DateUtil.dateFormatterByPattern(new Date(), "yyMMdd"), 000, 3, null);
			so.setId(code);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			User useUser = commonDAO.get(User.class, useUserId);
			User outUser = commonDAO.get(User.class, outUserId);
			so.setUseUser(useUser);
			so.setOutDate(sdf.parse(outDate));
			so.setOutUser(outUser);
			so.setState("3");
			so.setStateName("NEW");
			so.setNote(note);
			id = so.getId();
			commonDAO.saveOrUpdate(so);
		} else {
			so = commonDAO.get(StorageOut.class, id);
		}
		String[] batchId = batchIds.split(",");
		for (int i = 0; i < batchId.length; i++) {
			String batch = batchId[i];
			// 通过id查询库存主数据
			StorageReagentBuySerial srbs = commonDAO.get(StorageReagentBuySerial.class, batch);
			Storage s = commonDAO.get(Storage.class, srbs.getStorage().getId());
			StorageOutItem soi = new StorageOutItem();
			soi.setStorage(srbs.getStorage());
			soi.setSerial(srbs.getId());
			soi.setCode(srbs.getSerial());
			soi.setStorageNum(s.getNum());
			if (s.getOutUnitGroup() != null) {
				UnitGroupNew unitGroupNew = commonDAO.get(UnitGroupNew.class, s.getOutUnitGroup().getId());
				soi.setUnitGroupNew(unitGroupNew);
			}

			soi.setExpireDate(srbs.getExpireDate());
			if (srbs.getNum() != null) {
				soi.setBatchNum(srbs.getNum().toString());
			}
			soi.setNote(srbs.getNote());
			soi.setNote2(srbs.getNote2());
			soi.setStorageOut(so);
			commonDAO.saveOrUpdate(soi);

			String kucun = "出库管理:" + "原辅料编号:" + srbs.getStorage().getId() + "名称:" + srbs.getStorage().getName()
					+ "产品ID:'" + srbs.getSerial() + "产品批号:" + srbs.getCode() + "'的数据已添加到明细";
			if (kucun != null && !"".equals(kucun)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(srbs.getStorage().getId());
				li.setClassName("StorageOut");
				li.setModifyContent(kucun);
				if("123".equals(log)) {
					li.setState("1");
					li.setStateName("数据新增");
				}else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				commonDAO.saveOrUpdate(li);
			}
		}
		return id;

	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public StorageOut saveStorageOutById(StorageOut so, String id, String note, String useUserId, String logInfo,
			String ctype) {
		StorageOut soo = commonDAO.get(StorageOut.class, id);
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(soo.getId());
			li.setClassName("StorageOut");
			li.setModifyContent(logInfo);
			if("NEW".equals(id)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			commonDAO.saveOrUpdate(li);
		}
		soo.setNote(note);
		User useUser = commonDAO.get(User.class, useUserId);
		soo.setUseUser(useUser);
		soo.setCtype(ctype);
		commonDAO.saveOrUpdate(soo);
		return soo;
	}

	public void saveStorageOutAndItem(StorageOut newSo, Map jsonMap, String logInfo, String logInfoItem)
			throws Exception {
		if (newSo != null) {
			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(newSo.getId());
				li.setClassName("StorageOut");
				li.setModifyContent(logInfo);
				li.setState("3");
				li.setStateName("数据修改");
				commonDAO.saveOrUpdate(li);
			}

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("ImteJson");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveItem(newSo, jsonStr, logInfoItem);
			}
		}
	}

	private void saveItem(StorageOut newSo, String itemDataJson, String logInfo) throws Exception {
		List<StorageOutItem> saveItems = new ArrayList<StorageOutItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			StorageOutItem scp = new StorageOutItem();
			// 将map信息读入实体类
			scp = (StorageOutItem) commonDAO.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setStorageOut(newSo);

			if (scp.getUnitGroupNew() != null) {
				UnitGroupNew unitGroupNew = commonDAO.get(UnitGroupNew.class, scp.getUnitGroupNew().getId());
				Integer cycle = unitGroupNew.getCycle();
				if (cycle != null) {
					Integer integer = new Integer(cycle);
					int intValue = integer.intValue();
					Double valueOf = Double.valueOf(String.valueOf(intValue));
					if (scp.getPackingNum() != null) {
						Double double1 = valueOf * scp.getPackingNum();
						scp.setNum(double1);
					}
				}

			}

			saveItems.add(scp);
		}
		commonDAO.saveOrUpdateAll(saveItems);
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(newSo.getId());
			li.setClassName("StorageOut");
			li.setModifyContent(logInfo);
			li.setState("3");
			li.setStateName("数据修改");
			commonDAO.saveOrUpdate(li);
		}
	}
}
