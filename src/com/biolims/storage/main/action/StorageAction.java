package com.biolims.storage.main.action;

import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.dic.model.DicUnit;
import com.biolims.equipment.model.Instrument;
import com.biolims.openapi4.exception.OpenAPIException;
import com.biolims.openapi4.service.InventoryService;
import com.biolims.storage.common.constants.SystemConstants;
import com.biolims.storage.main.service.StorageService;
import com.biolims.storage.model.Storage;
import com.biolims.storage.model.StorageApply;
import com.biolims.storage.model.StorageImplement;
import com.biolims.storage.model.StorageOutItem;
import com.biolims.storage.model.StorageReagentBuy;
import com.biolims.storage.model.StorageReagentBuySerial;
import com.biolims.storage.model.StorageReagentComponents;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.system.work.model.UnitGroupNew;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.opensymphony.xwork2.ActionContext;

/**
 * 库存主数据
 */
@Controller
@Scope("prototype")
@ParentPackage("default")
@Namespace("/storage")
@SuppressWarnings({ "unchecked" })
public class StorageAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;

	private String rightsId = "1011";

	private Storage storage = new Storage();// 库存主数据

	private StorageImplement si;// 器具

	private StorageReagentBuy srb;// 采购试剂

	@Autowired
	private StorageService storageService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private FieldService fieldService;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CommonService commonService;
	private String log="";

	@Action(value = "showStorageDialogList")
	public String showStorageDialogList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/storage/main/showStorageDialog.jsp");

	}

//	/**
//	 * 接口调用U8 openapi的方法 获取试剂信息  并保存数据 
//	 * @throws OpenAPIException 
//	 */
////	public void getU8OpenApiMaterial() {
//	public static void main(String[] args) throws OpenAPIException{
//		InventoryService ds = new InventoryService();
//		String to_account = "yongtai"; //提供方id
//		String id = "test_yongttai"; //帐套号
//		com.alibaba.fastjson.JSONObject record = ds.get(id, to_account);
//		System.out.println(record);
//	}

	@Action(value = "showStorageDialogListJson")
	public void showStorageDialogListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = storageService.findStorageDialogList(start, length, query, col, sort);
			List<StorageReagentBuySerial> list = (List<StorageReagentBuySerial>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("storage-name", "");
			map.put("serial", "");
//			map.put("expireDate", "yyyy-mm-dd");
			map.put("num", "");
			map.put("unit-name", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 显示产品运输物资明细 @Title: showInstrumentDialogList @Description:
	 * TODO @param @return @param @throws Exception @return String @author 孙灵达 @date
	 * 2018年8月3日 @throws
	 */
	@Action(value = "showStorageDialogListForGoods", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showStorageDialogListForGoods() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/storage/main/showStorageDialogForGoods.jsp");

	}

	@Action(value = "showStorageDialogListForGoodsJson")
	public void showStorageDialogListForGoodsJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = storageService.showStorageDialogListForGoodsJson(start, length, query, col,
					sort);
			List<Storage> list = (List<Storage>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("jdeCode", "");
			map.put("producer-id", "");
			map.put("producer-name", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("Instrument");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 库存主数据列表表头
	 * 
	 * @return
	 */
	@Action(value = "showStorageList")
	public String showStorageList() throws Exception {
		String p_type = getParameterFromRequest("p_type");
		putObjToContext("p_type", p_type);
		// 用于判断当前页面类型,LIST类型
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);

		return dispatcher("/WEB-INF/page/storage/main/storageTable.jsp");
	}

	/**
	 * 
	 * @Title: showStorageTableJson @Description: TODO(查询主数据列表信息) @author 尹标舟 @date
	 *         2018-3-15下午5:50:38 @throws Exception @throws
	 */
	@Action(value = "showStorageTableJson")
	public void showStorageTableJson() throws Exception {
		// 显示的数据类型
		String p_type = getParameterFromRequest("p_type");

		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = storageService.findStorageTable(start, length, query, col, sort, p_type, null);
			List<Storage> list = (List<Storage>) result.get("list");

			Map<String, String> map = new HashMap<String, String>();
			map.put("name", "name");
			map.put("id", "id");
			map.put("type-id", "");
			map.put("type-name", "");
			map.put("studyType-id", "");
			map.put("studyType-name", "");
			map.put("num", "");
			map.put("unit-name", "");
			map.put("unitGroup-id", "");
			map.put("unitGroup-name", "");
			map.put("unitGroup-mark2-name", "");
			map.put("outUnitGroup-id", "");
			map.put("outUnitGroup-cycle", "");
			map.put("outUnitGroup-name", "");
			map.put("outUnitGroup-mark2-name", "");
			map.put("state-id", "");
			map.put("state-name", "");
			map.put("dutyUser-name", "");
			map.put("type-name", "");
			map.put("useDesc", "");
			map.put("searchCode", "");
			map.put("source-name", "");
			map.put("spec", "");
			map.put("barCode", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("ifCall", "");
			map.put("position-name", "");
			map.put("note", "");
			map.put("kit-name", "");
			map.put("producer-name", "");
			map.put("i5", "");
			map.put("i7", "");
			map.put("i5Index", "");
			map.put("i7Index", "");
			map.put("breed", "");
			map.put("purchaseState", "");
			map.put("jdeCode", "");

			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("Storage");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "findMedium", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void findMedium() throws Exception {
		String id = getParameterFromRequest("id");
		Map<String, Object> map = new HashMap<String, Object>();
		Boolean findMedium = fieldService.findMedium(id);
		map.put("success", findMedium);
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 库存主数据列表
	 * 
	 * @throws Exception
	 */
	/*
	 * @Action(value = "getStorageList") public void getStorageList() throws
	 * Exception { // 开始记录数 int startNum =
	 * Integer.parseInt(getParameterFromRequest("start")); // limit int limitNum =
	 * Integer.parseInt(getParameterFromRequest("limit")); // 字段 String dir =
	 * getParameterFromRequest("dir"); // 排序方式 String sort =
	 * getParameterFromRequest("sort"); // 显示的数据类型 String p_type =
	 * getParameterFromRequest("p_type");
	 * 
	 * if (dir.equals("")) { dir = "ASC"; } if (sort.equals("")) { sort = "type"; }
	 * 
	 * String data = getParameterFromRequest("data");
	 * 
	 * Map<String, Object> result = null; Map<String, String> map2Query = new
	 * HashMap<String, String>(); map2Query = JsonUtils.toObjectByJson(data,
	 * Map.class); try { result = this.storageService.findStorageList(map2Query,
	 * startNum, limitNum, dir, sort, p_type, null); } catch (Exception e) {
	 * e.printStackTrace(); }
	 * 
	 * List<Storage> list = (List<Storage>) result.get("list"); Long count = (Long)
	 * result.get("total"); Map<String, String> map = new HashMap<String, String>();
	 * map.put("name", "name"); map.put("id", "id"); map.put("type-id", "");
	 * map.put("type-name", ""); map.put("studyType-id", "");
	 * map.put("studyType-name", ""); map.put("num", ""); map.put("unit-name", "");
	 * map.put("state-id", ""); map.put("state-name", ""); map.put("dutyUser-name",
	 * ""); map.put("type-name", ""); map.put("useDesc", ""); map.put("searchCode",
	 * ""); map.put("source-name", ""); map.put("spec", ""); map.put("barCode", "");
	 * map.put("createUser-name", ""); map.put("createDate", "yyyy-MM-dd");
	 * map.put("ifCall", ""); map.put("position-name", ""); map.put("note", "");
	 * map.put("kit-name", ""); map.put("producer-name", ""); map.put("i5", "");
	 * map.put("i7", ""); map.put("i5Index", ""); map.put("i7Index", "");
	 * map.put("breed", ""); map.put("purchaseState", ""); map.put("jdeCode", "");
	 * new SendData().sendDateJson(map, list, count,
	 * ServletActionContext.getResponse()); }
	 */

	/**
	 * 添加和修改页面
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "toEditStorage")
	public String toEditStorage() throws Exception {
		String id = super.getRequest().getParameter("id");
		String p_type = getParameterFromRequest("p_type");
		if (id != null) {
			this.storage = this.storageService.findStorageById(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			// showStoragePostionList(SystemConstants.PAGE_HANDLE_METHOD_MODIFY,
			// id);
		} else {
			this.storage.setId("NEW");
			User aUser = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			this.storage.setCreateUser(aUser);
			this.storage.setCreateDate(new Date());
			this.storage.setNum(0.0);
			this.storage.setPrepareNum(0.0);
			this.storage.setHasSubmitNum(0.0);
			this.storage.setFactStorageNum(0.0);
			this.storage.setOnWayNum(0.0);
			this.storage.setLyNum(0.0);

			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			// showStoragePostionList(SystemConstants.PAGE_HANDLE_METHOD_ADD,
			// id);
		}
		putObjToContext("p_type", p_type);
		putObjToContext("cgsj", SystemConstants.DIC_STORAGE_TYPE_HAOCAI_CGSJ);
		return dispatcher("/WEB-INF/page/storage/main/storageEditTable.jsp");
	}

	/**
	 * 复制页面
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "toCopyStorage")
	public String toCopyStorage() throws Exception {
		String id = super.getRequest().getParameter("id");
		User aUser = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		this.storage = this.storageService.findStorageById(id);
		this.storage.setId("");
		this.storage.setCreateUser(aUser);
		this.storage.setCreateDate(new Date());
		this.storage.setNum(0.0);
		this.storage.setPrepareNum(0.0);
		this.storage.setHasSubmitNum(0.0);
		this.storage.setFactStorageNum(0.0);
		this.storage.setLyNum(0.0);
		// showStoragePostionList(SystemConstants.PAGE_HANDLE_METHOD_ADD, id);
		toSetStateCopy();
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		putObjToContext("cgsj", SystemConstants.DIC_STORAGE_TYPE_HAOCAI_CGSJ);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		return dispatcher("/WEB-INF/page/storage/main/storageEditTable.jsp");
	}

	/**
	 * 访问查看页面
	 */

	@Action(value = "toViewStorage")
	public String toViewStorage() throws Exception {
		String id = getParameterFromRequest("id");
		String p_type = getParameterFromRequest("p_type");
		if (id != null && !id.equals("")) {
			this.storage = this.storageService.findStorageById(id);
			putObjToContext("cgsj", SystemConstants.DIC_STORAGE_TYPE_HAOCAI_CGSJ);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
			// dicState2State(storage.getState().getId());
		}
		putObjToContext("p_type", p_type);
		return dispatcher("/WEB-INF/page/storage/main/storageEditTable.jsp");
	}

	/*
	 * public void showStoragePostionList(String handlemethod, String id) throws
	 * Exception {
	 * 
	 * String exttype = ""; String extcol = ""; // String editflag = "false";
	 * LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
	 * // 属性map<字段名称, ext-list属性数组说明,值为空""则没有该属性> // id type format header width
	 * sort hide align xtype editable renderer // editor // 列的id 当"common" 时该列为扩展列
	 * type类型 format格式 header列头 width列宽 sort是否排序 // hide是否隐藏 align左右格式 xtype
	 * ediable是否可编辑 renderer渲染 editor编辑器 if
	 * (handlemethod.equals(SystemConstants.PAGE_HANDLE_METHOD_MODIFY)) { //
	 * editflag = "true"; } map.put("id", new String[] { "", "string", "", "id",
	 * "150", "true", "true", "", "", "", "", "" });
	 * 
	 * map.put("position-id", new String[] { "", "string", "", "位置ID", "120",
	 * "true", "false", "", "", "", "", "" }); map.put("position-name", new String[]
	 * { "", "string", "", "位置名称", "120", "true", "false", "", "", "true", "",
	 * "position" }); // 声明 String statement = ""; // 声明一个lisener String
	 * statementLisener = "";
	 * 
	 * // 生成ext用type字符串 exttype = generalexttype(map); // 生成ext用col字符串 extcol =
	 * generalextcol(map);
	 * 
	 * putObjToContext("type", exttype); putObjToContext("col", extcol);
	 * putObjToContext("path", ServletActionContext.getRequest() .getContextPath() +
	 * "/storage/getStoragePostionList.action?id=" + id);
	 * putObjToContext("statement", statement); putObjToContext("statementLisener",
	 * statementLisener); putObjToContext("handlemethod", handlemethod); }
	 * 
	 * @Action(value = "getStoragePostionList") public void getStorageItemList()
	 * throws Exception { int startNum =
	 * Integer.parseInt(getParameterFromRequest("start")); int limitNum =
	 * Integer.parseInt(getParameterFromRequest("limit")); String storageId =
	 * getParameterFromRequest("id"); String dir = getParameterFromRequest("dir");
	 * String sort = getParameterFromRequest("sort"); List<StoragePositionStorage>
	 * list = null; long totalCount = 0; if (storageId != null &&
	 * !storageId.equals("")) { Map<String, Object> controlMap = storageService
	 * .getStoragePostionList(startNum, limitNum, dir, sort, getContextPath(),
	 * storageId); totalCount = Long
	 * .parseLong(controlMap.get("totalCount").toString()); list =
	 * (List<StoragePositionStorage>) controlMap.get("list"); } Map<String, String>
	 * map = new HashMap<String, String>(); map.put("id", "");
	 * map.put("position-id", ""); map.put("position-name", "");
	 * 
	 * if (list != null) new SendData().sendDateJson(map, list, totalCount,
	 * ServletActionContext.getResponse()); }
	 */

	/**
	 * 保存明细
	 */
	@Action(value = "saveStoragePostion")
	public void saveStoragePostion() throws Exception {
		String purchaseApplyId = getParameterFromRequest("storageId");
		String json = getParameterFromRequest("data");
		storageService.saveStoragePostion(purchaseApplyId, json);

	}

	/**
	 * 删除明细
	 */
	@Action(value = "delStoragePostion")
	public void delPurchaseApplyItem() throws Exception {
		String id = getParameterFromRequest("id");
		storageService.delStoragePostion(id);
		// 具体操作，如删除，填加动作，应用redirect
		// return redirect("/sysmanage/user/userVaccineShow.action");
	}

	/**
	 * 添加或修改库存主数据
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "save")
	public String editStorage() throws Exception {
		String id = getParameterFromRequest("id");
		String data = getParameterFromRequest("data");
		String p_type = getParameterFromRequest("p_type");
		String autoID = null;
		if (id.equals("NEW")) {
			log="123";
			String modelName = "Storage";
			String markCode = "SJ_MAN";
			autoID = codingRuleService.genTransID(modelName, markCode);
			this.storage.setId(autoID);
		}
		this.storage.setP_type(p_type);
		// Map aMap = new HashMap();
		// aMap.put("storageReagentItem",
		// getParameterFromRequest("storageReagentJson"));

		this.storageService.save(storage);
		return redirect("/storage/toEditStorage.action?id=" + this.storage.getId() + "&p_type=" + p_type);
	}

	@Action(value = "saveStorageTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveStorageTable() throws Exception {
		String dataValue = getParameterFromRequest("dataValue");
		// 主表changeLog
		String changeLog = getParameterFromRequest("changeLog");
		// 子表changeLogItem
		String changeLogItem = getParameterFromRequest("changeLogItem");

		// 子表changeLogItem
		String changeLogComponentsItem = getParameterFromRequest("changeLogComponentsItem");

		String p_type = getParameterFromRequest("p_type");

		String str = "[" + dataValue + "]";

		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str, List.class);

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			for (Map<String, Object> map1 : list) {

				Storage a = new Storage();

				Map aMap = new HashMap();
				a = (Storage) commonDAO.Map2Bean(map1, a);
				if (a.getUnitGroup() != null) {
					UnitGroupNew unitGroupNew = commonDAO.get(UnitGroupNew.class, a.getUnitGroup().getId());
					if (unitGroupNew != null) {
						DicUnit mark2 = unitGroupNew.getMark2();
//						a.setUnit(mark2);

					}
				}
				String autoID = null;
				if (a.getId().equals("NEW")) {
					log="123";
					String modelName = "Storage";
					String markCode = "SJ_MAN";
					autoID = codingRuleService.genTransID(modelName, markCode);
					a.setId(autoID);
				}
				a.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				a.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));

				a.setP_type(p_type);

				aMap.put("storageReagentBuySerial", getParameterFromRequest("storageReagentBuySerialJson"));
				aMap.put("storageReagentComponents", getParameterFromRequest("storageReagentComponentsJson"));
				storageService.save(a, aMap, changeLog, changeLogItem, changeLogComponentsItem,log);
				map.put("id", a.getId());
			}
			map.put("success", true);

		} catch (Exception e) {
			map.put("success", false);
			map.put("msg", e.getMessage());
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "saveStorageReagentBuySerialTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveStorageReagentBuySerialTable() throws Exception {
		String id = getParameterFromRequest("id");
		String changeLog = getParameterFromRequest("changeLog");
		String dataJson = getParameterFromRequest("dataJson");
		storage = commonService.get(Storage.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			storageService.saveStorageReagentBuySerial(storage, dataJson, changeLog,log);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "saveStorageReagentComponentsTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveStorageReagentComponentsTable() throws Exception {
		String id = getParameterFromRequest("id");
		String changeLog = getParameterFromRequest("changeLog");
		String dataJson = getParameterFromRequest("dataJson");
		storage = commonService.get(Storage.class, id);
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			storageService.saveStorageReagentComponents(storage, dataJson, changeLog,log);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 添加ByJson
	 */
	public void saveByJson(String autoID, String data, String p_type) throws Exception {
		storageService.save(autoID, data, p_type);
	}

	/**
	 * 器具页面
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "storageImplementView")
	public String storageImplementView() throws Exception {
		String storageId = super.getRequest().getParameter("storageId");
		this.si = this.storageService.findStorageImplementByStorageId(storageId);
		return dispatcher("/WEB-INF/page/storage/main/storageImplement.jsp");
	}

	/**
	 * 保存器具数据
	 * 
	 * @return
	 */
	@Action(value = "editStorageImplement")
	public String editStorageImplement() throws Exception {
		String storageId = super.getRequest().getParameter("storageId");
		String storageBreed = super.getRequest().getParameter("storageBreed");
		String storageProducter = super.getRequest().getParameter("storageProducter");
		String storageProductAddress = super.getRequest().getParameter("storageProductAddress");
		String remark = super.getRequest().getParameter("remark");
		this.si = new StorageImplement();
		Storage s = new Storage();
		s.setId(storageId);
		si.setStorageId(s);
		si.setRemark(remark);
		si.setStorageBreed(storageBreed);
		si.setStorageProductAddress(storageProductAddress);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", true);
		try {
			this.storageService.saveStorageImplement(this.si);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		return super.renderJson(map);
	}

	/**
	 * 采购试剂页面
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "storageReagentView")
	public String storageReagentView() throws Exception {
		String storageId = getParameterFromRequest("storageId");
		String handlemethod = getParameterFromRequest("handlemethod");
		String exttype = "";
		String extcol = "";
		String editflag = "true";

		putObjToContext("path", ServletActionContext.getRequest().getContextPath()
				+ "/storage/storageReagentSerial.action?storageId=" + storageId);
		putObjToContext("storageId", storageId);
		putObjToContext("handlemethod", handlemethod);

		this.srb = this.storageService.findStorageReagentBuyByStorageId(storageId);
		return dispatcher("/WEB-INF/page/storage/main/storageReagent.jsp");
	}

	/**
	 * 试剂明细数据
	 * 
	 * @throws Exception
	 */
	/*
	 * @Action(value = "storageReagentSerial") public void storageReagentSerial()
	 * throws Exception { String storageId =
	 * super.getRequest().getParameter("storageId"); // 开始记录数 int startNum =
	 * Integer.parseInt(getParameterFromRequest("start")); // limit int limitNum =
	 * Integer.parseInt(getParameterFromRequest("limit")); // 字段 String dir =
	 * getParameterFromRequest("dir"); // 排序方式 String sort =
	 * getParameterFromRequest("sort");
	 * 
	 * Map<String, Object> result = this.storageService
	 * .findStorageReagentSerial(storageId, startNum, limitNum, dir, sort);
	 * 
	 * List<StorageReagentBuySerial> list = (List<StorageReagentBuySerial>) result
	 * .get("result");
	 * 
	 * Long count = (Long) result.get("count"); Map<String, String> map = new
	 * HashMap<String, String>(); map.put("id", "id"); map.put("serial", "");
	 * map.put("rankType-id", ""); map.put("code", ""); map.put("productDate",
	 * "yyyy-MM-dd"); map.put("expireDate", "yyyy-MM-dd"); map.put("remindDate",
	 * "yyyy-MM-dd"); map.put("inDate", "yyyy-MM-dd"); map.put("outPrice",
	 * "#.####"); map.put("storage-unit-name", ""); map.put("num", "");
	 * map.put("isGood", ""); map.put("position-id", ""); map.put("position-name",
	 * ""); map.put("purchasePrice", ""); map.put("qcState", "");
	 * map.put("qcPassDate", ""); map.put("recationSum", ""); map.put("note", "");
	 * map.put("note2", ""); new SendData().sendDateJson(map, list, count,
	 * ServletActionContext.getResponse()); }
	 */

	@Action(value = "showStorageReagentBuySerialTableJson")
	public void showStorageReagentBuySerialTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = storageService.findStorageReagentBuySerialTable(start, length, query, col, sort,
				id);
		List<StorageReagentBuySerial> list = (List<StorageReagentBuySerial>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "id");
		map.put("serial", "");
		map.put("rankType-id", "");
		map.put("code", "");
		map.put("productDate", "yyyy-MM-dd");
		map.put("expireDate", "yyyy-MM-dd");
		map.put("remindDate", "yyyy-MM-dd");
		map.put("inDate", "yyyy-MM-dd");
		map.put("outPrice", "#.####");
		map.put("storage-unitGroup-mark2-name", "");
		map.put("num", "");
		map.put("isGood", "");
		map.put("position-id", "");
		map.put("position-name", "");
		map.put("purchasePrice", "");
		map.put("qcState", "");
		map.put("qcPassDate", "");
		map.put("recationSum", "");
		map.put("note", "");
		map.put("note2", "");
		map.put("scopeId", "");
		map.put("scopeName", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	@Action(value = "showStorageReagentComponentsTableJson")
	public void showStorageReagentComponentsTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = storageService.findStorageReagentComponentsTable(start, length, query, col, sort,
				id);
		List<StorageReagentComponents> list = (List<StorageReagentComponents>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");

		map.put("compoundName", "");
		map.put("content", "");
		map.put("accreditation", "");
		map.put("certificationBody-name", "");
		map.put("certificationBody-id", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * 保存试剂和试剂明细
	 * 
	 * @param <T>
	 * @return
	 * @throws Exception
	 */
	@Action(value = "addStorageReagent")
	public String addStorageReagent() throws Exception {
		String storageId = super.getRequest().getParameter("storageId");
		String data = super.getRequest().getParameter("data");

		this.storageService.saveStorageReagent(storageId, data);

		return redirect("/storage/storageReagentView.action");
	}

	/**
	 * 删除指定的试剂明细信息
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "delStorageReagentBuySerial")
	public String delStorageReagentBuySerial() throws Exception {
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", true);
		try {
			this.storageService.delStorageReagentBuySerialById(ids);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		return super.renderJson(map);
	}

	/**
	 * 删除库存主数据
	 * 
	 * @return
	 */
	public String delStorage() throws Exception {
		String storageId = super.getRequest().getParameter("storageId");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", true);
		try {
			this.storageService.modifyStorageStateById(storageId, SystemConstants.DIC_STATE_NO);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		return renderJson(map);
	}

	/**
	 * 查询所有存储位置
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "getAllPositionShow")
	public String getAllPositionShow() throws Exception {
		String type = "{name: 'id'},{name: 'name'}";
		String col = "{header:'位置编码',sortable: true,dataIndex: 'id',width: 100},"
				+ "{header:'位置名称',sortable: true,dataIndex: 'name',width: 100},"
				+ "  {xtype: 'actioncolumn',width: 50,items: [{icon: '"
				+ ServletActionContext.getRequest().getContextPath() + "/images/accept.png',"
				+ "  tooltip: '选中',handler: function(grid, rowIndex, colIndex) {"
				+ "	var rec = store.getAt(rowIndex);" + "   setvalue(rec.get('id'),rec.get('name'));}}]}";

		putObjToContext("type", type);
		putObjToContext("col", col);
		putObjToContext("path", ServletActionContext.getRequest().getContextPath() + "/storage/getAllPosition.action");
		return dispatcher("/WEB-INF/page/storage/haocai/showAllPositionList.jsp");
	}

	@Action(value = "getAllPosition")
	public void getAllPosition() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		Map<String, Object> result = this.storageService.findAllPosition(startNum, limitNum);
		Long count = (Long) result.get("count");
		List<StorageApply> list = (List<StorageApply>) result.get("result");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "id");
		map.put("name", "name");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "findSearchCode", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void findSearchCode() throws Exception {

		String mmRsName = getParameterFromRequest("mmRsName");
		// mmRsName = new String(mmRsName.getBytes("iso-8859-1"), "utf-8");
		// mmRsName = URLDecoder.decode(mmRsName, "UTF-8");
		mmRsName = URLDecoder.decode(mmRsName, "UTF-8");
		String outStr = storageService.findSerchCode(mmRsName);

		HttpUtils.write(outStr);
	}

	@Action(value = "findStorageByName", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void findStorageByName() throws Exception {

		String mmRsName = getParameterFromRequest("name");
		// mmRsName = new String(mmRsName.getBytes("ISO-8859-1"), "UTF-8");
		// mmRsName = URLDecoder.decode(mmRsName, "UTF-8");
		ServletActionContext.getRequest().setCharacterEncoding("utf-8");
		// String title = "";
		// if (ServletActionContext.getRequest().getParameter("name") != null) {

		// mmRsName = ServletActionContext.getRequest().getParameter("name");
		// }

		// title = URLDecoder.decode(title,"utf-8");

		mmRsName = URLDecoder.decode(mmRsName, "UTF-8");
		String outStr = storageService.findStorageByName(mmRsName);

		HttpUtils.write(outStr);
	}

	@Action(value = "findStorageByNames", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void findStorageByNames() throws Exception {

		String mmRsName = getParameterFromRequest("name");
		// mmRsName = new String(mmRsName.getBytes("ISO-8859-1"), "UTF-8");
		// mmRsName = URLDecoder.decode(mmRsName, "UTF-8");
		ServletActionContext.getRequest().setCharacterEncoding("utf-8");
		// String title = "";
		// if (ServletActionContext.getRequest().getParameter("name") != null) {

		// mmRsName = ServletActionContext.getRequest().getParameter("name");
		// }

		// title = URLDecoder.decode(title,"utf-8");

		mmRsName = URLDecoder.decode(mmRsName, "UTF-8");
		String outStr = storageService.findStorageByNames(mmRsName);

		HttpUtils.write(outStr);
	}

	// 加载库存主数据
//	@Action(value = "showSpecSelectList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showSpecSelectList() throws Exception {
//		String type = getParameterFromRequest("type");
//		String p_type = getParameterFromRequest("p_type");
//		putObjToContext("type", type);
//		putObjToContext("p_type", p_type);
//		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		return dispatcher("/WEB-INF/page/storage/main/showSpecSelectList.jsp");
//	}
//
//	@Action(value = "showSpecSelectListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showSpecSelectListJson() throws Exception {
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		String dir = getParameterFromRequest("dir");
//		String sort = getParameterFromRequest("sort");
//		String data = getParameterFromRequest("data");
//		String type = getParameterFromRequest("type");
//		String p_type = getParameterFromRequest("p_type");
//		Map<String, String> map2Query = new HashMap<String, String>();
//		if (data != null && data.length() > 0)
//			map2Query = JsonUtils.toObjectByJson(data, Map.class);
//		Map<String, Object> result = storageService.findStorageList(map2Query,
//				startNum, limitNum, dir, sort, p_type, type);
//		Long count = (Long) result.get("total");
//
//		List<Storage> list = (List<Storage>) result.get("list");
//
//		Map<String, String> map = new HashMap<String, String>();
//		map.put("id", "");
//		map.put("name", "");
//		map.put("spec", "");
//		map.put("type-name", "");
//		map.put("studyType-name", "");
//		map.put("position-name", "");
//		map.put("producer-name", "");
//		map.put("num", "");
//		map.put("unit-name", "");
//		map.put("note", "");
//		map.put("state-name", "");
//		map.put("dutyUser-name", "");
//		map.put("useDesc", "");
//		map.put("searchCode", "");
//		map.put("source-name", "");
//		map.put("barCode", "");
//		map.put("createUser-name", "");
//		map.put("createDate", "");
//		map.put("ifCall", "");
//		map.put("kit-id", "");
//		map.put("kit-name", "");
//		map.put("i5", "");
//		map.put("i7", "");
//		map.put("currentIndex", "");
//		new SendData().sendDateJson(map, list, count,
//				ServletActionContext.getResponse());
//	}

	@Action(value = "showSpecSelectListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSpecSelectListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		String type = getParameterFromRequest("type");
		String p_type = getParameterFromRequest("p_type");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = storageService.findStorageList(map2Query, startNum, limitNum, dir, sort, p_type,
				type);
		Long count = (Long) result.get("total");

		List<Storage> list = (List<Storage>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("spec", "");
		map.put("type-name", "");
		map.put("studyType-name", "");
		map.put("position-name", "");
		map.put("producer-name", "");
		map.put("num", "");
		map.put("unitGroup-id", "");
		map.put("unitGroup-name", "");
		map.put("unitGroup-mark2-name", "");
		map.put("note", "");
		map.put("state-name", "");
		map.put("dutyUser-name", "");
		map.put("useDesc", "");
		map.put("searchCode", "");
		map.put("source-name", "");
		map.put("barCode", "");
		map.put("createUser-name", "");
		map.put("createDate", "");
		map.put("ifCall", "");
		map.put("kit-id", "");
		map.put("kit-name", "");
		map.put("i5", "");
		map.put("i7", "");
		map.put("currentIndex", "");

		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	// 文库构建自动排序查询index
	@Action(value = "setindex", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selectindex() throws Exception {
		String type = getRequest().getParameter("type");
		String id = getRequest().getParameter("id");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, Object>> dataListMap = this.storageService.selectindex(type, id);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 查询index最大排序号
	@Action(value = "maxCurrentIndex", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void maxCurrentIndex() throws Exception {

		Map<String, Object> result = new HashMap<String, Object>();
		try {
			Integer max = this.storageService.maxCurrentIndex();
			if (max == null) {
				max = 0;
			}
			max += 1;
			result.put("success", true);
			result.put("data", max);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	@Action(value = "saveStorageItemList")
	public void saveStorageItemList() throws Exception {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String dataJson = getParameterFromRequest("itemDataJson");
			String p_type = getParameterFromRequest("p_type");
			storageService.saveStorageItem(p_type, dataJson);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: getStorageBySn @Description: 扫码获取试剂 @author : shengwei.wang @date
	 *         2018年1月26日上午10:11:08 @throws Exception void @throws
	 */
	@Action(value = "getStorageBySn")
	public void getStorageBySn() throws Exception {
		String sn = getParameterFromRequest("sn");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			StorageOutItem soi = storageService.getStorageBySn(sn);
			map.put("soi", soi);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	/**
	 * 
	 * @Title: getStorageBySn @Description: 扫码获取试剂 @author : shengwei.wang @date
	 *         2018年1月26日上午10:11:08 @throws Exception void @throws
	 */
	@Action(value = "getStorageByPihao")
	public void getStorageByPihao() throws Exception {
		String sn = getParameterFromRequest("sn");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			StorageOutItem soi = storageService.getStorageByPihao(sn);
			map.put("soi", soi);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}


	/**
	 * 确认试剂用完 @throws
	 */
	@Action(value = "queren")
	public void queren() throws Exception {
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			storageService.queren(ids);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: getStrogeReagent @Description: 选择试剂明细 @author : shengwei.wang @date
	 *         2018年1月26日上午11:33:23 @throws Exception void @throws
	 */
	@Action(value = "getStrogeReagent")
	public String getStrogeReagent() {
		String id = getParameterFromRequest("id");
		String pici = getParameterFromRequest("pici");
		String state = getParameterFromRequest("state");
		String cellPassageId = getParameterFromRequest("cellPassageId");
		putObjToContext("id", id);
		putObjToContext("pici", pici);
		putObjToContext("state", state);
		String batch = getParameterFromRequest("batch");
		putObjToContext("batch", batch);
		putObjToContext("cellPassageId", cellPassageId);
		return dispatcher("/WEB-INF/page/system/template/reagentTemplateDialog.jsp");
	}

	@Action(value = "getStrogeReagentJson")
	public void getStrogeReagentJson() throws Exception {
		String id = getParameterFromRequest("id");
		String reagentId = getParameterFromRequest("reagentId");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String batch = getParameterFromRequest("batch");
//		Map<String, Object> result = storageService.getStrogeReagent(id, start,
//				length, query, col, sort);
		Map<String, Object> result = new HashMap<String, Object>();
		if (!"".equals(reagentId) && reagentId != null) {
			Storage st = commonDAO.get(Storage.class, reagentId);
			if (st != null) {
				if (!"1".equals(st.getMedium())) {
					result = storageService.getStrogeReagent(id, start, length, query, col, sort, batch);

				} else {
					result = storageService.getStrogeReagentIsOne(id, start, length, query, col, sort, batch);

				}

			}

		}

		List<StorageOutItem> list = (List<StorageOutItem>) result.get("list");
		if (list != null) {
			Iterator<StorageOutItem> it = list.iterator();
			Date now = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

			Date dt = sdf.parse(sdf.format(now));
			long time = dt.getTime();

			while (it.hasNext()) {
				StorageOutItem storageOutItem = it.next();
				if (!"".equals(storageOutItem.getExpireDate()) && storageOutItem.getExpireDate() != null) {
					if (time > storageOutItem.getExpireDate().getTime()) {
						it.remove();
						String  rf =String.valueOf(result.get("recordsFiltered"));
						int recordsFiltered = Integer.parseInt( rf ); 
						recordsFiltered=recordsFiltered-1;
						String  rt =String.valueOf(result.get("recordsTotal"));
						int recordsTotal = Integer.parseInt( rt ); 
						recordsTotal=recordsTotal-1;
						result.put("recordsTotal", recordsTotal);
						result.put("recordsFiltered", recordsFiltered);
					}
				}
			}
			
		}

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("storage-id", "");
		map.put("storage-name", "");
		map.put("note", "");
		map.put("code", "");
		map.put("expireDate", "yyyy-MM-dd");
		map.put("batch", "");

		map.put("num", "");
		map.put("unitGroupNew-mark2-name", "");
		map.put("storageOut-outDate", "yyyy-MM-dd");
		map.put("usedFinish", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	@Action(value = "getStrogeReagentJsonByPici")
	public void getStrogeReagentJsonByPici() throws Exception {
		String id = getParameterFromRequest("id");
		String reagentId = getParameterFromRequest("reagentId");
		String pici = getParameterFromRequest("pici");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String batch = getParameterFromRequest("batch");
//		Map<String, Object> result = storageService.getStrogeReagent(id, start,
//				length, query, col, sort);
		Map<String, Object> result = new HashMap<String, Object>();
		if (!"".equals(reagentId) && reagentId != null) {
			Storage st = commonDAO.get(Storage.class, reagentId);
			if (st != null) {
				if (!"1".equals(st.getMedium())) {
					result = storageService.getStrogeReagentByPici(id, start, length, query, col, sort, batch, pici);

				} else {
					result = storageService.getStrogeReagentByPiciIsOne(id, start, length, query, col, sort, batch,
							pici);
				}
			}
		}
		List<StorageOutItem> list = (List<StorageOutItem>) result.get("list");
		if (list != null) {
			Iterator<StorageOutItem> it = list.iterator();
			Date now = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

			Date dt = sdf.parse(sdf.format(now));
			long time = dt.getTime();

			while (it.hasNext()) {
				StorageOutItem storageOutItem = it.next();
				if (!"".equals(storageOutItem.getExpireDate()) && storageOutItem.getExpireDate() != null) {
					if (time > storageOutItem.getExpireDate().getTime()) {
						it.remove();
					}
				}
			}
		}

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("storage-id", "");
		map.put("storage-name", "");
		map.put("note", "");
		map.put("code", "");
		map.put("expireDate", "yyyy-MM-dd");
		map.put("batch", "");

		map.put("num", "");
		map.put("unitGroupNew-mark2-name", "");
		map.put("storageOut-outDate", "yyyy-MM-dd");
		map.put("usedFinish", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

	/**
	 * 
	 * @Title: getStroge @Description: 选择试剂 @author : shengwei.wang @date
	 *         2018年2月8日下午3:52:25 @return String @throws
	 */
	@Action(value = "getStorage")
	public String getStroge() {
		String type = getParameterFromRequest("type");
		putObjToContext("type", type);
		return dispatcher("/WEB-INF/page/storage/main/showStorageDialogTable.jsp");
	}

	@Action(value = "getStorageJson")
	public void getStrogeJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String type = getParameterFromRequest("type");
		try {
			Map<String, Object> result = storageService.getStrogeJson(start, length, query, col, sort, type);
			List<Storage> list = (List<Storage>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("note", "");
			map.put("spec", "");
			map.put("outUnitGroup-mark2-name", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public Storage getStorage() {
		return storage;
	}

	public void setStorage(Storage storage) {
		this.storage = storage;
	}

	public StorageImplement getSi() {
		return si;
	}

	public void setSi(StorageImplement si) {
		this.si = si;
	}

	public StorageReagentBuy getSrb() {
		return srb;
	}

	public void setSrb(StorageReagentBuy srb) {
		this.srb = srb;
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	@Action(value = "selStorageBatch", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String dicTypeSelectTable() throws Exception {
		String id = super.getRequest().getParameter("id");
		putObjToContext("id", id);
		return dispatcher("/WEB-INF/page/storage/main/storageBatch.jsp");
	}

	@Action(value = "storageBatchJson")
	public void storageBatchJson() throws Exception {
		String ids = getParameterFromRequest("id");
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = storageService.findStorageBatchByIds(start, length, query, col, sort, ids);
		List<StorageReagentBuySerial> list = (List<StorageReagentBuySerial>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "id");
		map.put("serial", "");
		map.put("rankType-id", "");
		map.put("code", "");
		map.put("productDate", "yyyy-MM-dd");
		map.put("expireDate", "yyyy-MM-dd");
		map.put("remindDate", "yyyy-MM-dd");
		map.put("inDate", "yyyy-MM-dd");
		map.put("outPrice", "#.####");
		map.put("storage-unitGroup-mark2-name", "");
		map.put("storage-id", "");
		map.put("num", "");
		map.put("isGood", "");
		map.put("position-id", "");
		map.put("position-name", "");
		map.put("purchasePrice", "");
		map.put("qcState", "");
		map.put("qcPassDate", "");
		map.put("recationSum", "");
		map.put("note", "");
		map.put("note2", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}

}
