package com.biolims.storage.main.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.commons.httpclient.HttpClient;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.struts2.ServletActionContext;
import org.apache.xmlbeans.SystemProperties;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.common.service.SystemCodeService;
import com.biolims.core.model.user.Department;
import com.biolims.dic.model.DicMainType;
import com.biolims.dic.model.DicState;
import com.biolims.dic.model.DicType;
import com.biolims.dic.model.DicUnit;
import com.biolims.experiment.dna.model.DnaTask;
import com.biolims.experiment.dna.model.DnaTaskAbnormal;
import com.biolims.experiment.dna.model.DnaTaskInfo;
import com.biolims.experiment.qt.model.QtTask;
import com.biolims.experiment.qt.model.QtTaskInfoManager;
import com.biolims.experiment.qt.model.QtTaskItem;
import com.biolims.experiment.qt.model.QtTaskTemp;
import com.biolims.experiment.quality.model.QualityTestTemp;
import com.biolims.experiment.wk.model.WkTaskTemp;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.openapi4.exception.OpenAPIException;
import com.biolims.openapi4.service.DepartmentU8Service;
import com.biolims.openapi4.service.InventoryService;
import com.biolims.openapi4.util.HttpUtil;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.storage.container.model.StorageContainer;
import com.biolims.storage.main.dao.StorageDao;
import com.biolims.storage.model.DicStorageType;
import com.biolims.storage.model.Storage;
import com.biolims.storage.model.StorageImplement;
import com.biolims.storage.model.StorageInItem;
import com.biolims.storage.model.StorageOutItem;
import com.biolims.storage.model.StoragePositionStorage;
import com.biolims.storage.model.StorageReagentBuy;
import com.biolims.storage.model.StorageReagentBuySerial;
import com.biolims.storage.model.StorageReagentComponents;
import com.biolims.storage.pleaseverifyplan.model.PleaseVerifyPlan;
import com.biolims.storage.pleaseverifyplan.model.PleaseVerifyPlanItem;
import com.biolims.supplier.common.service.SupplierCustomerService;
import com.biolims.supplier.model.Supplier;
import com.biolims.system.detecyion.model.SampleDeteyion;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.template.model.Template;
import com.biolims.system.work.model.UnitGroupNew;
import com.biolims.util.BeanUtils;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import org.apache.commons.io.IOUtils;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;



@Service
@SuppressWarnings({ "unchecked" })
@Transactional
public class StorageService {

	@Resource
	private StorageDao storageDao;

	@Resource
	private SystemCodeService systemCodeService;
	@Resource
	private SupplierCustomerService supplierCustomerService;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CommonService commonService;

	/**
	 * 
	 * @Title: findStorageTable
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @author Yin
	 * @date 2018-3-15下午5:50:58
	 * @param start
	 * @param length
	 * @param query
	 * @param col
	 * @param sort
	 * @return Map<String,Object>
	 * @throws Exception
	 * @throws
	 */
	public Map<String, Object> findStorageTable(Integer start, Integer length,
			String query, String col, String sort, String p_type, String type)
			throws Exception {
		return storageDao.findStorageTable(start, length, query, col, sort,
				p_type, type);
	}

	/**
	 * 查询耗材主数据
	 * 
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param storage
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findStorage(int startNum, int limitNum,
			String dir, String sort, String data) throws Exception {
		// 检索条件map,每个字段检索值
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		// 检索方式map,每个字段的检索方式
		Map<String, String> mapForCondition = new HashMap<String, String>();
		if (data != null && !data.equals("")) {

			BeanUtils.data2Map(data, mapForQuery, mapForCondition);
			mapForQuery.remove("queryStartDate");
			mapForQuery.remove("queryEndDate");
			mapForCondition.remove("queryStartDate");
			mapForCondition.remove("queryEndDate");
		}
		Map<String, Object> map = this.storageDao.findObjectCondition(startNum,
				limitNum, dir, sort, Storage.class, mapForQuery,
				mapForCondition);

		return map;
	}

	// 加载库存主数据
	/*
	 * public Map<String, Object> findStorageList(Map<String, String>
	 * mapForQuery, Integer startNum, Integer limitNum, String dir, String sort,
	 * String p_type, String type) { return
	 * storageDao.selectStorageList(mapForQuery, startNum, limitNum, dir, sort,
	 * p_type, type); }
	 */

	public List<Map<String, Object>> selectindex(String type, String id) {
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
		Map<String, Object> result = storageDao.selectindex(type, id);
		List<Storage> list = (List<Storage>) result.get("list");
		if (list != null && list.size() > 0) {
			for (Storage ti : list) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("id", ti.getId());
				map.put("name", ti.getName());
				map.put("i5", ti.getI5());
				map.put("i7", ti.getI7());
				mapList.add(map);
			}
		}
		return mapList;
	}

	/**
	 * 根据ID查询库存数据
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public Storage findStorageById(String id) throws Exception {
		return this.storageDao.selectStorageById(id);
	}

	public Map<String, Object> getStoragePostionList(int startNum,
			int limitNum, String dir, String sort, String contextPath,
			String storageId) throws Exception {
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		Map<String, String> mapForCondition = new HashMap<String, String>();
		if (storageId != null && !storageId.equals("")) {
			mapForQuery.put("storage.id", storageId);
			mapForCondition.put("storage.id", "=");
		}
		Map<String, Object> controlMap = storageDao.findObjectCondition(
				startNum, limitNum, dir, sort, StoragePositionStorage.class,
				mapForQuery, mapForCondition);
		return controlMap;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveStoragePostion(String id, String jsonString)
			throws Exception {
		// 将json读入map
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				jsonString, List.class);
		for (Map<String, Object> map : list) {
			StoragePositionStorage em = new StoragePositionStorage();
			// 将map信息读入实体类
			em = (StoragePositionStorage) storageDao.Map2Bean(map, em);
			Storage ep = new Storage();
			ep.setId(id);
			if (em.getId() != null && em.getId().equals(""))
				em.setId(null);
			em.setStorage(ep);
			storageDao.saveOrUpdate(em);
		}
	}

	/**
	 * 
	 * 删除明细,由ext ajax调用
	 * 
	 * @param id
	 *            认证ID
	 * @return void
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delStoragePostion(String id) {
		StoragePositionStorage ppi = new StoragePositionStorage();
		ppi.setId(id);
		storageDao.delete(ppi);
	}

	/**
	 * 保存或修改数据
	 * 
	 * @param s
	 * @throws Excepion
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(Storage s) throws Exception {

		if (s != null) {
			storageDao.saveOrUpdate(s);

			// String jsonStr = "";
			// jsonStr = (String) jsonMap.get("storageReagentItem");
			// if (jsonStr != null && !jsonStr.equals("{}") &&
			// !jsonStr.equals("")) {
			// savestorageReagentItemItem(s, jsonStr);
			// }
		}
	}

	/**
	 * 保存ByJSon
	 * 
	 * @param p_type
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void save(String id, String jsonString, String p_type)
			throws Exception {
		jsonString = jsonString.replaceAll("storage\\.", "");
		jsonString = jsonString.replaceAll("\\.id", "-id");
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				jsonString, List.class);
		for (Map<String, Object> map : list) {
			Storage em = new Storage();
			// 将map信息读入实体类
			em = (Storage) storageDao.Map2Bean(map, em);
			em.setId(id);
			em.setP_type(p_type);
			storageDao.saveOrUpdate(em);
		}
	}

	/**
	 * 根据库存主数据查询器具
	 * 
	 * @param storageId
	 * @return
	 * @throws Exception
	 */
	public StorageImplement findStorageImplementByStorageId(String storageId)
			throws Exception {
		return this.storageDao.selectStorageImplementByStorageId(storageId);
	}

	/**
	 * 保存器具
	 * 
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveStorageImplement(StorageImplement si) throws Exception {
		Storage storageId = si.getStorageId();
		StorageImplement _si = this.storageDao
				.selectStorageImplementByStorageId(storageId.getId());
		if (_si != null) {
			_si.setRemark(si.getRemark());
			_si.setStorageBreed(si.getStorageBreed());
			_si.setStorageProductAddress(si.getStorageProductAddress());
			this.storageDao.update(_si);
		} else {
			this.storageDao.save(si);
		}
	}

	/**
	 * 按照库存主数据Id查询器具明细
	 * 
	 * @param storageId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findStorageReagentSerial(String storageId,
			String costCenterId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		Map<String, Object> map = this.storageDao
				.selectStorageReagentSerialByCostCenter(storageId,
						costCenterId, startNum, limitNum, dir, sort);
		return map;
	}

	public Map<String, Object> findStorageReagentSerial(String storageId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> map = this.storageDao.selectStorageReagentSerial(
				storageId, startNum, limitNum, dir, sort);
		List<StorageReagentBuySerial> list = (List<StorageReagentBuySerial>) map
				.get("result");
		List<StorageReagentBuySerial> alist = new ArrayList<StorageReagentBuySerial>();
		for (StorageReagentBuySerial srbs : list) {
			if (srbs.getNum() != null && srbs.getNum() > 0) {

				alist.add(srbs);
			}

		}
		for (StorageReagentBuySerial srbs : list) {
			if (srbs.getNum() == 0) {

				alist.add(srbs);
			}

		}
		map.put("result", alist);

		return map;
	}

	/**
	 * 按照库存主数据ID查询试剂
	 * 
	 * @param storageId
	 * @return
	 * @throws Exception
	 */
	public StorageReagentBuy findStorageReagentBuyByStorageId(String storageId)
			throws Exception {
		return this.storageDao.selectStorageReagentBuyByStorageId(storageId);
	}

	/**
	 * 保存试剂和试剂明细
	 * 
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveStorageReagent(String storageId, String data)
			throws Exception {

		// 添加明细
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(data,
				List.class);
		for (Map<String, Object> map : list) {
			Storage s = new Storage();
			s.setId(storageId);
			StorageReagentBuySerial srbs = new StorageReagentBuySerial();
			srbs = (StorageReagentBuySerial) this.storageDao
					.Map2Bean(map, srbs);
			srbs.setStorage(s);
			this.storageDao.saveOrUpdate(srbs);
		}
	}

	/**
	 * 删除试剂的明细信息
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delStorageReagentBuySerialById(String[] ids) throws Exception {
		for (String id : ids) {
			StorageReagentBuySerial sr = storageDao.get(
					StorageReagentBuySerial.class, id);
			if (sr.getNum() != null) {
				Storage s = storageDao.get(Storage.class, sr.getStorage()
						.getId());
				Double d = s.getNum();
				s.setNum(d - sr.getNum());
				storageDao.saveOrUpdate(s);
			}
			storageDao.delete(sr);
		}
	}

	/**
	 * 更新库存主数据状态
	 * 
	 * @param storgaeId
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void modifyStorageStateById(String storageId, String status)
			throws Exception {
		this.storageDao.updateStorageStateById(storageId, status);
	}

	/**
	 * 查询所有存储位置
	 * 
	 * @param startNum
	 * @param limitNum
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findAllPosition(int startNum, int limitNum)
			throws Exception {
		return this.storageDao.selectAllPosition(startNum, limitNum);
	}

	public String findSerchCode(String name) throws Exception {
		List<Storage> list = storageDao.findSearchCode(name);
		String all = "[";
		for (Storage a : list) {
			if (all.equals("["))
				all += "{\"code\":\"" + a.getId() + "\",\"name\":\""
						+ a.getName() + "\",\"spec\":\"" + a.getSpec() + "\"}";
			// all += "\"" + a + "\"";
			else
				all += ",{\"code\":\"" + a.getId() + "\",\"name\":\""
						+ a.getName() + "\",\"spec\":\"" + a.getSpec() + "\"}";
			// all += ",\"" + a + "\"";
		}
		all += "]";
		return all;
	}

	public String findStorageByName(String name) throws Exception {
		List<Storage> list = storageDao.findStorageByName(name);
		String all = "[";
		for (Storage a : list) {
			if (all.equals("["))
				all += "{\"code\":\"" + a.getId() + "\",\"name\":\""
						+ a.getName() + "\",\"spec\":\""
						+ ((a.getSpec() == null) ? "" : a.getSpec()) + "\"}";
			// all += "\"" + a + "\"";
			else
				all += ",{\"code\":\"" + a.getId() + "\",\"name\":\""
						+ a.getName() + "\",\"spec\":\""
						+ ((a.getSpec() == null) ? "" : a.getSpec()) + "\"}";
			// all += ",\"" + a + "\"";
		}
		all += "]";
		return all;
	}

	public Storage getStorageByName(String name) throws Exception {
		Storage s = storageDao.getStorageByName(name);
		return s;
	}

	public String findStorageByNames(String name) throws Exception {
		List<Storage> list = storageDao.findStorageByName(name);
		String all = "[";
		for (Storage a : list) {
			if (all.equals("["))
				all += "{\"name\":\"" + a.getName() + "\"}";
			// all += "\"" + a + "\"";
			else
				all += ",{\"name\":\"" + a.getName() + "\"}";
			// all += ",\"" + a + "\"";
		}
		all += "]";
		return all;
	}

	// 根据试剂编号、批次号、试剂用量 查询主数据
	public void UpdateStorageAndItemNum(String code, String batch, Double num) {
		// 查询主数据
		Storage s = this.storageDao.get(Storage.class, code);
		// // 库存数量
		// Double sNum = s.getNum() - num;
		// s.setNum(sNum);
		// this.storageDao.saveOrUpdate(s);
		// 根据试剂编号、批次编号查询采购试剂明细
		StorageReagentBuySerial srb = this.storageDao
				.selectStorageReagentBuySerial(code, batch);
		// 试剂数量
		if (srb != null) {
			if (srb.getUseNum() != null) {
				Double itemNum = srb.getUseNum() - num;
				srb.setUseNum(itemNum);
			} else {
				srb.setUseNum(0.00);
			}
			this.storageDao.saveOrUpdate(srb);
		}
	}

	// public boolean sampleStorageOutSetStorage(String formId) throws Exception
	// {
	// List<SampleStorageItem> list =
	// sampleStorageDao.selectSampleStorageItemList(formId);
	// for (SampleStorageItem em : list) {
	// Storage s = get(em.getStorage().getClass(), em.getStorage().getId());
	// Double nn = 0.0;
	// if (em.getNum() != null) {
	//
	// //库存数量=原数量-出库数量
	// if (s.getNum() != null) {
	// nn = s.getNum();
	// nn -= em.getNum();
	// s.setNum(nn);
	// }
	// }
	//
	// saveOrUpdate(s);
	//
	// Double rn = 0.0;
	// Double srn = 0.0;
	// if
	// (s.getType().getId().startsWith(SystemConstants.DIC_STORAGE_SHIYANCAILIAO_YPC))
	// {
	// if (em.getSerials() != null && !em.getSerials().equals("")) {
	// StorageReagentBuySerial srb = get(StorageReagentBuySerial.class,
	// em.getSerials());
	//
	// if (em.getNum() != null && srb.getNum() != null) {
	// rn = em.getNum();
	//
	// srn = srb.getNum();
	// srb.setNum(srn - rn);
	//
	// }
	// //库存数量=原数量-出库数量
	//
	// saveOrUpdate(srb);
	//
	// }
	// }
	// if
	// (s.getType().getId().startsWith(SystemConstants.DIC_STORAGE_SHIYANCAILIAO_NPC))
	// {
	// this.updateStorageNPC(em);
	// }
	//
	// }
	// return true;
	// }

	// 最大排序号
	public Integer maxCurrentIndex() {
		return this.storageDao.maxCurrentIndex();
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveStorageItem(String p_type, String itemDataJson)
			throws Exception {
		List<Storage> saveItems = new ArrayList<Storage>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {

			Storage st = new Storage();
			// 将map信息读入实体类
			st = (Storage) storageDao.Map2Bean(map, st);
			if (st.getId() == null) {
				String code = systemCodeService.getCodeByPrefix(
						"Storage",
						"SJ_MAN"
								+ DateUtil.dateFormatterByPattern(new Date(),
										"yyMMdd"), 00000, 5, null);
				st.setId(code);
			}
			if (map.get("producer-name") != null
					&& !map.get("producer-name").equals("")) {
				Supplier sup = supplierCustomerService
						.getSupplierByName((String) map.get("producer-name"));
				st.setProducer(sup);
			}
			if (map.get("kit-name") != null && !map.get("kit-name").equals("")) {
				DicType kit = storageDao.getKitByName((String) map
						.get("kit-name"));
				st.setKit(kit);
			}
			st.setP_type(p_type);
			saveItems.add(st);
		}
		storageDao.saveOrUpdateAll(saveItems);
		;
	}

	public StorageReagentBuySerial getReagentSerialBySn(String sn)
			throws Exception {

		return storageDao.getReagentSerialBySn(sn);

	}

	public StorageReagentBuySerial findStorageReagentBuyByInItemId(String id) {
		return storageDao.findStorageReagentBuyByInItemId(id);
	}

	public void save(Storage storage, String data) throws Exception {
		storageDao.saveOrUpdate(storage);
		// 添加明细
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(data,
				List.class);
		for (Map<String, Object> map : list) {
			Storage s = new Storage();
			s.setId(storage.getId());
			StorageReagentBuySerial srbs = new StorageReagentBuySerial();
			srbs = (StorageReagentBuySerial) this.storageDao
					.Map2Bean(map, srbs);
			srbs.setStorage(s);
			this.storageDao.saveOrUpdate(srbs);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savestorageReagentItemItem(Storage sc, String itemDataJson)
			throws Exception {
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		List<StorageReagentBuySerial> saveItems = new ArrayList<StorageReagentBuySerial>();
		for (Map<String, Object> map : list) {
			StorageReagentBuySerial scp = new StorageReagentBuySerial();
			// 将map信息读入实体类
			scp = (StorageReagentBuySerial) storageDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);

			// }
			scp.setStorage(sc);

			saveItems.add(scp);

		}
		storageDao.saveOrUpdateAll(saveItems);

	}

	public StorageOutItem getStorageBySn(String sn) {
		return storageDao.getStorageBySn(sn);
	}
	
	public StorageOutItem getStorageByPihao(String sn) {
		return storageDao.getStorageByPihao(sn);
	}

	public Map<String, Object> getStrogeReagent(String id, Integer start,
			Integer length, String query, String col, String sort) {
		return storageDao.getStrogeReagent(id, start, length, query, col, sort);
	}
	
	public Map<String, Object> getStrogeReagent(String id, Integer start,
			Integer length, String query, String col, String sort, String batch) {
		return storageDao.getStrogeReagent(id, start, length, query, col, sort, batch);
	}
	public Map<String, Object> getStrogeReagentIsOne(String id, Integer start,
			Integer length, String query, String col, String sort, String batch) {
		return storageDao.getStrogeReagentIsOne(id, start, length, query, col, sort, batch);
	}
	public Map<String, Object> getStrogeReagentByPici(String id, Integer start,
			Integer length, String query, String col, String sort, String batch,String pici) {
		return storageDao.getStrogeReagentByPici(id, start, length, query, col, sort, batch,pici);
	}
	public Map<String, Object> getStrogeReagentByPiciIsOne(String id, Integer start,
			Integer length, String query, String col, String sort, String batch,String pici) {
		return storageDao.getStrogeReagentByPiciIsOne(id, start, length, query, col, sort, batch,pici);
	}
	
	public Map<String, Object> getStrogeReagents(String id, Integer start,
			Integer length, String query, String col, String sort) {
		return storageDao.getStrogeReagents(id, start, length, query, col, sort);
	}

	public Map<String, Object> getStrogeJson(Integer start, Integer length,
			String query, String col, String sort, String type) throws Exception {
		return storageDao.getStrogeJson(start, length, query, col, sort,type);
	}

	public Map<String, Object> findStorageReagentBuySerialTable(Integer start,
			Integer length, String query, String col, String sort, String id) {
		return storageDao.findStorageReagentBuySerialTable(start, length,
				query, col, sort, id);
	}
	
	public Map<String, Object> findStorageReagentComponentsTable(Integer start,
			Integer length, String query, String col, String sort, String id) {
		return storageDao.findStorageReagentComponentsTable(start, length,
				query, col, sort, id);
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(Storage sc, Map jsonMap, String logInfo,String logInfoItem,String changeLogComponentsItem,String log)
			throws Exception {
		if (sc != null) {
			storageDao.merge(sc);

			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(sc.getId());
				li.setClassName("Storage");
				li.setModifyContent(logInfo);
				if("123".equals(log)) {
					li.setState("1");
					li.setStateName("数据新增");
				}else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				commonDAO.saveOrUpdate(li);
			}

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("storageReagentBuySerial");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveStorageReagentBuySerial(sc, jsonStr, logInfoItem,log);
			}
			
			jsonStr = (String) jsonMap.get("storageReagentComponents");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveStorageReagentComponents(sc, jsonStr, changeLogComponentsItem,log);
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveStorageReagentBuySerial(Storage sc, String itemDataJson,
			String logInfo,String log) throws Exception {
		List<StorageReagentBuySerial> saveItems = new ArrayList<StorageReagentBuySerial>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			StorageReagentBuySerial scp = new StorageReagentBuySerial();
			// 将map信息读入实体类
			scp = (StorageReagentBuySerial) storageDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setStorage(sc);

			saveItems.add(scp);
		}
		storageDao.saveOrUpdateAll(saveItems);
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("Storage");
			li.setModifyContent(logInfo);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			commonDAO.saveOrUpdate(li);
		}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveStorageReagentComponents(Storage sc, String itemDataJson,
			String logInfo,String log) throws Exception {
		List<StorageReagentComponents> saveItems = new ArrayList<StorageReagentComponents>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			StorageReagentComponents scp = new StorageReagentComponents();
			// 将map信息读入实体类
			scp = (StorageReagentComponents) storageDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setStorage(sc);

			saveItems.add(scp);
		}
		storageDao.saveOrUpdateAll(saveItems);
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("Storage");
			li.setModifyContent(logInfo);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			commonDAO.saveOrUpdate(li);
		}
	}
	
	
	public Map<String, Object> findStorageBatchByIds(Integer start,
			Integer length, String query, String col, String sort, String ids) {
		return storageDao.selStorageBatchByIds(start, length,
				query, col, sort, ids);
	}
	
	public Map<String, Object> findStorageBatchByIds1(Integer start,
			Integer length, String query, String col, String sort, String ids) {
		return storageDao.selStorageBatchByIds1(start, length,
				query, col, sort, ids);
	}

	public Map<String, Object> findStorageList(Map<String, String> mapForQuery,
			Integer startNum, Integer limitNum, String dir, String sort,
			String p_type, String type) {
		return storageDao.selectStorageList(mapForQuery, startNum, limitNum,
				dir, sort, p_type, type);
	}

	public Map<String, Object> findStorageDialogList(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return storageDao.findStorageDialogList(start, length, query, col,
				sort);
	}

	public Map<String, Object> showStorageDialogListForGoodsJson(Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return storageDao.showStorageDialogListForGoodsJson(start, length, query, col, sort);
	}

	/**
	 * 
	 * @Title: submitSample
	 * @Description: 提交样本
	 * @author :
	 * @date 
	 * @param id
	 * @param ids
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submitSample(String id, String[] ids) throws Exception {
		// 获取主表信息
		PleaseVerifyPlan sc = this.storageDao.get(PleaseVerifyPlan.class, id);
		// 获取结果表样本信息
		List<PleaseVerifyPlanItem> list;
		if (ids == null)
			list = this.storageDao.selectAllResultListById(id);
		else
			list = this.storageDao.selectAllResultListByIds(ids);

		for (PleaseVerifyPlanItem scp : list) {
			if (scp != null
					&& (scp.getSubmit() == null || (scp.getSubmit() != null && scp
							.getSubmit().equals("")))) {
				QualityTestTemp qtt = new QualityTestTemp();
				qtt.setId(null);
				qtt.setStorage(scp.getStorage());
				qtt.setCode(scp.getCode());
				qtt.setSerial(scp.getSerial());
				qtt.setState("1");
				if("1".equals(scp.getSampleDeteyionType())){
					qtt.setCellType("3");
				}else if("2".equals(scp.getSampleDeteyionType())){
					qtt.setCellType("7");
				}
				SampleDeteyion sd = commonDAO.get(SampleDeteyion.class, scp.getSampleDeteyionId());
				if(sd!=null){
					qtt.setSampleDeteyion(sd);
				}
				storageDao.saveOrUpdate(qtt);
				scp.setSubmit("1");
				storageDao.saveOrUpdate(scp);
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void queren(String[] ids) {
		if(ids.length>0) {
			for(String id:ids) {
				if(id!=null
						&&!"".equals(id)) {
					StorageOutItem soi = commonDAO.get(StorageOutItem.class, id);
					if(soi!=null) {
						soi.setUsedFinish("1");
						commonDAO.saveOrUpdate(soi);
					}
				}
			}
		}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void MaterialMainSynchro() {
		User user = commonDAO.get(User.class, "admin");
		DicStorageType dst = commonDAO.get(DicStorageType.class, "12");
		DicState dsd = commonDAO.get(DicState.class, "1s");
				
		InputStream is = Thread.currentThread().getContextClassLoader()
			    .getResourceAsStream("system.properties");
		Properties prop = new Properties();
		try {
			prop.load(is);
		} catch (IOException e) {
			e.printStackTrace();
		}
		String from_account = prop.getProperty("from_account");
		String to_account = prop.getProperty("to_account");
		Map<String, String> paramMap = new HashMap();
		InventoryService ds = new InventoryService();
		com.alibaba.fastjson.JSONObject record = null;
		try {
			paramMap.put("from_account", from_account);
			paramMap.put("to_account", to_account);
			record = ds.batchGet(paramMap);
		} catch (OpenAPIException e) {
			e.printStackTrace();
		}
		if("0".equals(record.get("errcode"))) {
			Object objArray = (Object) record.get("inventory");
			String str = ""+objArray+"";
	        //方式1:转换成JSONArray对象形式
			com.alibaba.fastjson.JSONArray list = JSON.parseArray(str);
	        //方式1:转换成JSONArray对象形式
//	        com.alibaba.fastjson.JSONArray list = JSON.parseArray((String)record.get("inventory"));
	        if(list.size()>0) {
		        for (int i = 0; i < list.size(); i++) {
		            com.alibaba.fastjson.JSONObject map = JSON.parseObject(list.get(i)+"");
//		        }
////			List<com.alibaba.fastjson.JSONObject> list = (List<com.alibaba.fastjson.JSONObject>) record.get("inventory");
//			
//				for(com.alibaba.fastjson.JSONObject map:list) {
//					code	string		存货编码
					Storage sts = commonDAO.get(Storage.class, (String)map.get("code"));
					if(sts!=null) {
						
					}else {
						Storage st = new Storage();
						st.setCreateDate(new Date());
						st.setCreateUser(user);
						st.setType(dst);
						st.setState(dsd);
						st.setId((String)map.get("code"));
						st.setP_type("1");
//					name	string		存货名称
						if(map.get("name")!=null) {
							st.setOtherName((String)map.get("name"));
							st.setName((String)map.get("name"));
						}
//						invaddcode	string		存货代码
//						specs	string		规格型号
						if(map.get("specs")!=null) {
							st.setSpec((String)map.get("specs"));
						}
//						sort_code	string		所属分类码
						if(map.get("sort_code")!=null) {
							List<DicType> dts = commonService.get(DicType.class,"code" ,(String)map.get("sort_code"));
							if(dts.size()>0) {
								st.setStudyType(dts.get(0));
							}else {
								DicType dt = new DicType();
								dt.setId(null);
								dt.setCode((String)map.get("sort_code"));
								if(map.get("sort_name")!=null) {
									dt.setName((String)map.get("sort_name"));
								}
								DicMainType dmt = commonDAO.get(DicMainType.class, "yjlx");
								if(dmt!=null) {
									dt.setType(dmt);
								}
								dt.setState("1");
								commonDAO.saveOrUpdate(dt);
								st.setStudyType(dt);
							}
						}
//						main_measure	string		主计量单位
						if(map.get("main_measure")!=null) {
							UnitGroupNew dts = commonDAO.get(UnitGroupNew.class, (String)map.get("main_measure"));
							if(dts!=null) {
								st.setUnitGroup(dts);
							}else {
								UnitGroupNew dt = new UnitGroupNew();
								dt.setId((String)map.get("main_measure"));
								dt.setCycle(1);
								dt.setState("1");
								if(map.get("ccomunitname")!=null) {
									dt.setName((String)map.get("ccomunitname"));
									List<DicUnit> du = commonService.get(DicUnit.class, "name", (String)map.get("ccomunitname")) ; 
									if(du.size()>0) {
										dt.setMark(du.get(0));
										dt.setMark2(du.get(0));
									}else {
										DicUnit duu = new DicUnit();
										duu.setId(null);
										duu.setName((String)map.get("ccomunitname"));
										commonDAO.saveOrUpdate(duu);
										dt.setMark(duu);
										dt.setMark2(duu);
									}
								}
								commonDAO.saveOrUpdate(dt);
								st.setUnitGroup(dt);
							}
						}
						commonDAO.saveOrUpdate(st);
//						puunit_code	string		采购默认单位编码
//						puunit_name	string		采购默认单位名称
//						puunit_ichangrate	number		采购默认单位换算率
//						saunit_code	string		销售默认单位编码
//						saunit_name	string		销售默认单位名称
//						saunit_ichangrate	number		销售默认单位换算率
//						stunit_code	string		库存默认单位编码
//						stunit_name	string		库存默认单位名称
//						stunit_ichangrate	number		库存默认单位换算率
//						unitgroup_code	string		计量单位组编码
//						unitgroup_type	number		计量单位组类型(0=无换算;1=固定;2=浮动)
//						bbarcode	boolean		条形码管理
//						barcode	string		条形码
//						ref_sale_price	number		参考售价
//						bsuitretail	boolean		适用零售(0:否 1：是)
//						bottom_sale_price	number		最低售价
//						start_date	date		启用日期
//						end_date	date		停用日期
//						ModifyDate	date		变更日期
//						defwarehouse	string		默认仓库
//						defwarehousename	string		默认仓库名称
//						iSupplyType	string		供应类型
//						drawtype	string		领料方式
//						iimptaxrate	number		进项税率
//						tax_rate	number		销项税率
//						self_define1	string		自定义项1
//						self_define2	string		自定义项2
//						self_define3	string		自定义项3
//						self_define4	string		自定义项4
//						self_define5	string		自定义项5
//						self_define6	string		自定义项6
//						self_define7	string		自定义项7
//						self_define8	string		自定义项8
//						self_define9	string		自定义项9
//						self_define10	string		自定义项10
//						self_define11	string		自定义项11
//						self_define12	string		自定义项12
//						self_define13	string		自定义项13
//						self_define14	string		自定义项14
//						self_define15	string		自定义项15
//						self_define16	string		自定义项16
//						invcode	string	entry	存货编码
					}
				}
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void getDepartmentU8() {
		
		InputStream is = Thread.currentThread().getContextClassLoader()
	    .getResourceAsStream("system.properties");
		Properties prop = new Properties();
		try {
			prop.load(is);
		} catch (IOException e) {
			e.printStackTrace();
		}
		String from_account = prop.getProperty("from_account");
		String to_account = prop.getProperty("to_account");
		DepartmentU8Service ds = new DepartmentU8Service();
		Map<String, String> paramMap = new HashMap();
		com.alibaba.fastjson.JSONObject record = null;
		try {
			paramMap.put("from_account", from_account);
			paramMap.put("to_account", to_account);
			record = ds.batchGet(paramMap);
		} catch (OpenAPIException e) {
			e.printStackTrace();
		}
		if("0".equals(record.get("errcode"))) {
			Object objArray = (Object) record.get("department");
			String str = ""+objArray+"";
	        //方式1:转换成JSONArray对象形式
			com.alibaba.fastjson.JSONArray list = JSON.parseArray(str);
	        //方式1:转换成JSONArray对象形式
//	        com.alibaba.fastjson.JSONArray list = JSON.parseArray((String)record.get("department"));
	        if(list.size()>0) {
		        for (int i = 0; i < list.size(); i++) {
		            com.alibaba.fastjson.JSONObject map = JSON.parseObject(list.get(i)+"");
//			List<com.alibaba.fastjson.JSONObject> list = (List<com.alibaba.fastjson.JSONObject>) record.get("department");
//			if(list.size()>0) {
//				for(com.alibaba.fastjson.JSONObject map:list) {
					Department sts = commonDAO.get(Department.class, (String)map.get("code"));
					if(sts!=null) {
						
					}else {
						Department dtt = new Department();
						dtt.setId((String)map.get("code"));
						if(map.get("name")!=null) {
							dtt.setBriefName((String)map.get("name"));
						}
						if(map.get("name")!=null) {
							dtt.setBriefName((String)map.get("name"));
						}
						if(map.get("remark")!=null) {
							dtt.setNote((String)map.get("remark"));
						}
						if(map.get("ddependdate")!=null) {
							if(compare_date((String)map.get("ddependdate"),new Date())==1) {
								dtt.setState("1");
							}else {
								dtt.setState("0");
							}
						}
						commonDAO.saveOrUpdate(dtt);
					}
//					errcode	string	 	错误码，0 为正常。
//					errmsg	string	 	错误信息。
//					page_index	string	 	页号
//					rows_per_page	string	 	每页行数
//					row_count	string	 	总行数
//					page_count	string	 	页数
//					code	string		部门编码
//					name	string		部门名称
//					endflag	boolean		是否末级
//					rank	number		编码级次
//					manager	string		负责人
//					managername	string		负责人名称
//					cdepleader	string		分管领导编码
//					cdepleadername	string		分管领导名称
//					timestamp	number		时间戳
//					remark	string		备注
//					ddependdate	date		撤销日期
				}
			}
		}
	}
	
	public int compare_date(String DATE1, Date DATE2) {
	     DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	     try {
	       Date dt1 = df.parse(DATE1);
	       Date dt2 = df.parse(df.format(DATE2));
	       if (dt1.getTime() > dt2.getTime()) {
	         System.out.println("dt1 在dt2前");
	         return 1;
	       } else if (dt1.getTime() < dt2.getTime()) {
	         System.out.println("dt1在dt2后");
	         return -1;
	       } else {
	         return 0;
	       }
	     } catch (Exception exception) {
	       exception.printStackTrace();
	     }
	     return 0;
	   }
	
}
