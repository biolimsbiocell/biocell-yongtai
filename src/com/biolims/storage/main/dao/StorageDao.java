package com.biolims.storage.main.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.common.dao.CommonDAO;
import com.biolims.core.model.user.DicCostCenter;
import com.biolims.dic.model.DicType;
import com.biolims.experiment.plasma.model.PlasmaTaskTemp;
import com.biolims.storage.common.constants.SystemConstants;
import com.biolims.storage.model.Storage;
import com.biolims.storage.model.StorageImplement;
import com.biolims.storage.model.StorageOutItem;
import com.biolims.storage.model.StorageReagentBuy;
import com.biolims.storage.model.StorageReagentBuySerial;
import com.biolims.storage.model.StorageReagentComponents;
import com.biolims.storage.pleaseverifyplan.model.PleaseVerifyPlanItem;
import com.biolims.storage.position.model.StoragePosition;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings({ "unchecked" })
public class StorageDao extends CommonDAO {

	/**
	 * 
	 * @Title: findStorageTable @Description: TODO(这里用一句话描述这个方法的作用) @author
	 * Yin @date 2018-3-15下午5:51:55 @param start @param length @param query @param
	 * col @param sort @return Map<String,Object> @throws Exception @throws
	 */
	public Map<String, Object> findStorageTable(Integer start, Integer length, String query, String col, String sort,
			String p_type, String type) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from  Storage where 1=1 and state='1s' ";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		if (p_type != null && !"".equals(p_type)) {
			key += " and p_type='" + p_type + "' ";
		}
		if (type != null) {
			key += " and studyType='" + type + "' ";
		}

		// String scopeId=(String)
		// ActionContext.getContext().getSession().get("scopeId");
		// if(!"all".equals(scopeId)){
		// key+=" and scopeId='"+scopeId+"'";
		// }
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from Storage where 1=1 and state='1s' ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<Storage> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	/**
	 * 查询耗材主数据
	 * 
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param storage
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectStorageCount(int startNum, int limitNum, String dir, String sort, Storage storage)
			throws Exception {

		String key = "";
		if (storage != null) {
			String id = storage.getId();
			if (id != null)
				key = key + " and id='" + id + "'";
		}
		String hql = "from Storage where 1=1 " + key + " and mainType.id='" + SystemConstants.DIC_STORAGE_HAOCAI
				+ "' and mainType.state='" + SystemConstants.DIC_STATE_YES + "' and state='1s' ";

		if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0)
			hql = hql + " order by " + sort + " " + dir;

		List<Storage> list = this.getSession().createQuery(hql).setFirstResult(startNum).setMaxResults(limitNum).list();

		hql = "select count(id) from Storage where 1=1 " + key + " and mainType.id='"
				+ SystemConstants.DIC_STORAGE_HAOCAI + "' and mainType.state='" + SystemConstants.DIC_STATE_YES
				+ "' and state='1s'  ";
		;
		Long count = (Long) this.getSession().createQuery(hql).uniqueResult();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("count", count);
		map.put("result", list);
		return map;
	}

	/**
	 * 根据ID查询库存数据
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public Storage selectStorageById(String id) throws Exception {
		Storage s = super.get(Storage.class, id);
		return s;
	}

	/**
	 * 根据库存ID查义器具
	 * 
	 * @return
	 * @throws Exception
	 */
	public StorageImplement selectStorageImplementByStorageId(String storageId) throws Exception {
		String hql = "from StorageImplement where storageId.id='" + storageId + "'";
		Object obj = this.getSession().createQuery(hql).uniqueResult();
		StorageImplement si = null;
		if (obj != null)
			si = (StorageImplement) obj;
		return si;
	}

	/**
	 * 按照库存主数据Id查询器具明细
	 * 
	 * @param storageId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectStorageReagentSerial(String storageId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from StorageReagentBuySerial where storage.id='" + storageId + "'";

		Long count = (Long) this.getSession().createQuery("select count(id) " + hql).uniqueResult();
		List<StorageReagentBuySerial> list = new ArrayList<StorageReagentBuySerial>();
		if (count != null && !new Long(0).equals(count)) {
			if (sort != null && sort.trim().length() > 0 && dir != null && dir.trim().length() > 0)
				hql = hql + " " + sort + " " + dir;
			list = this.getSession().createQuery(hql).setFirstResult(startNum).setMaxResults(limitNum).list();
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("count", count);
		map.put("result", list);
		return map;
	}

	/**
	 * 按照用户所属部门查询明细
	 * 
	 * @param storageId
	 * @param departmentId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectStorageReagentSerialByCostCenter(String storageId, String costCenterId,
			Integer startNum, Integer limitNum, String dir, String sort) throws Exception {
		Long count = 0L;
		List<StorageReagentBuySerial> list = new ArrayList<StorageReagentBuySerial>();

		DicCostCenter dcc = this.get(DicCostCenter.class, costCenterId);

		String hql = "";
		String costCenterHql = "and costCenter is null";
		if (dcc != null && dcc.getLevelId() != null) {
			costCenterHql = " and costCenter.levelId like '" + dcc.getLevelId() + "%'";

		}
		hql = "from StorageReagentBuySerial where storage.id='" + storageId + "' " + costCenterHql;

		count = (Long) this.getSession().createQuery("select count(id) " + hql).uniqueResult();

		if (count != null && !new Long(0).equals(count)) {
			if (sort != null && sort.trim().length() > 0 && dir != null && dir.trim().length() > 0)
				hql = hql + " " + sort + " " + dir;
			list = this.getSession().createQuery(hql).setFirstResult(startNum).setMaxResults(limitNum).list();
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("count", count);
		map.put("result", list);
		return map;
	}

	/**
	 * & 按照库存主数据ID查询试剂
	 * 
	 * @param storageId
	 * @return
	 * @throws Exception
	 */
	public StorageReagentBuy selectStorageReagentBuyByStorageId(String storageId) throws Exception {
		String hql = "from StorageReagentBuy where storageId.id='" + storageId + "'";
		StorageReagentBuy srb = (StorageReagentBuy) this.getSession().createQuery(hql).uniqueResult();
		return srb;
	}

	/**
	 * 查库存明细数量
	 * 
	 * @param storageId
	 * @param costCenterId
	 * @return
	 * @throws Exception
	 */
	public Double selectStorageReagentBuyCount(String storageId, String costCenterId) throws Exception {

		DicCostCenter dcc = this.get(DicCostCenter.class, costCenterId);

		String hql = "select sum(num) from StorageReagentBuySerial where storage.id='" + storageId
				+ "' and costCenter.levelId like '" + dcc.getLevelId() + "%'";
		Double srb = (Double) this.getSession().createQuery(hql).uniqueResult();
		return srb;
	}

	/**
	 * 根据主数据id和试剂批次号查询
	 * 
	 * @param id
	 * @param code
	 * @return
	 */
	public StorageReagentBuySerial selectStorageReagentBuySerial(String id, String code) {
		String hql = "from StorageReagentBuySerial where 1=1 and storage.id='" + id + "' and code='" + code + "'";
		List<StorageReagentBuySerial> l = this.getSession().createQuery(hql).list();

		StorageReagentBuySerial srb = null;

		if (l.size() > 0) {
			srb = l.get(0);

		}

		return srb;
	}

	/**
	 * 更新库存主数据状态
	 * 
	 * @param storageId
	 * @param status
	 * @throws Exception
	 */
	public void updateStorageStateById(String storageId, String state) throws Exception {
		Storage s = this.get(Storage.class, storageId);
		// DicState ds = new DicState();
		// ds.setId(status);
		// s.setState(state);
		this.update(s);
	}

	/**
	 * 查询所有位置信息
	 * 
	 * @param startNum
	 * @param limitNum
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectAllPosition(int startNum, int limitNum) throws Exception {
		String hql = "from StoragePosition where isUse='" + SystemConstants.POSITION_USE_NO + "'";
		Long count = (Long) this.getSession().createQuery("select count(id) " + hql).uniqueResult();
		List<StoragePosition> list = new ArrayList<StoragePosition>();
		if (count != null && !new Long(0).equals(count)) {
			list = this.getSession().createQuery(hql).setFirstResult(startNum).setMaxResults(limitNum).list();
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("count", count);
		map.put("result", list);
		return map;
	}

	public List<Storage> findSearchCode(String name) {
		return this.find("from Storage where name like '%" + name + "%' order by searchCode ASC");
		// return this.find("from Storage order by searchCode ASC");
	}

	public List<Storage> findStorage() {
		return this.find("from Storage where state like '" + SystemConstants.DIC_STATE_YES_ID + "%'");
	}

	public List<Storage> findStorageByName(String name) {
		return this.find("from Storage where name like '%" + name + "%' and  state='1s' order by searchCode ASC");
		// return this.find("from Storage order by searchCode ASC");
	}

	public Storage getStorageByName(String name) {
		String hql = "from Storage where name like '%" + name + "%' and  state='1s' ";
		List<Storage> list = this.getSession().createQuery(hql).list();
		Storage s = new Storage();
		if (list.size() > 0) {
			s = list.get(0);
		}
		return s;
		// return this.find("from Storage order by searchCode ASC");
	}
	// 加载库存主数据
	/*
	 * public Map<String, Object> selectStorageList( Map<String, String>
	 * mapForQuery, Integer startNum, Integer limitNum, String dir, String sort,
	 * String p_type, String type) { String key = ""; String hql =
	 * " from Storage where 1=1 "; if(p_type!=null){ key+="and p_type='"+p_type+"'";
	 * } if (mapForQuery != null) key = map2where(mapForQuery); if (type != null) {
	 * key += "and studyType='" + type + "'"; }
	 * 
	 * Long total = (Long) this.getSession() .createQuery(" select count(*) " + hql
	 * + key).uniqueResult(); List<Storage> list = new ArrayList<Storage>(); if
	 * (total > 0) { if (dir != null && dir.length() > 0 && sort != null &&
	 * sort.length() > 0) { if (sort.indexOf("-") != -1) { sort = sort.substring(0,
	 * sort.indexOf("-")); } key = key + " order by " + sort + " " + dir; } else {
	 * if (type!=null&&type.equals("index")) { key = key + "order by currentIndex";
	 * } } if (startNum == null || limitNum == null) { list =
	 * this.getSession().createQuery(hql + key).list(); } else { list =
	 * this.getSession().createQuery(hql + key)
	 * .setFirstResult(startNum).setMaxResults(limitNum) .list(); } } Map<String,
	 * Object> result = new HashMap<String, Object>(); result.put("total", total);
	 * result.put("list", list); return result; }
	 */

	// 加载库存主数据
	public Map<String, Object> selectindex(String type, String id) {
		String key = " ";
		String hql = " from Storage where 1=1 and id='" + id + "' and studyType='" + type + "'";
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<Storage> list = new ArrayList<Storage>();
		if (total > 0) {
			list = this.getSession().createQuery(hql + key).list();
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 查询全部index最大排序号
	public Integer maxCurrentIndex() {
		String hql = "from Storage where 1=1 and studyType='index'";
		Integer total = (Integer) this.getSession().createQuery(" select max(currentIndex) " + hql).uniqueResult();
		return total;
	}

	// 查询试剂盒index最大排序号
	public Integer maxCurrentIndexByKitId(String kitId) {
		String hql = "from Storage where 1=1 and studyType='index' and kit.id='" + kitId + "'";
		Integer total = (Integer) this.getSession().createQuery(" select max(currentIndex) " + hql).uniqueResult();
		return total;
	}

	// 查询index使用到哪个
	public Storage selStorageGetIndex() {
		String hql = "from Storage where 1=1 and studyType='index' and indexState='1'";
		Storage s = (Storage) this.getSession().createQuery(hql).uniqueResult();
		return s;
	}

	// 根据排序号查询index
	public Storage selStoragebycurrentIndex(Integer n) {
		String hql = "from Storage where 1=1 and studyType='index' and currentIndex='" + n + "'";
		Storage s = (Storage) this.getSession().createQuery(hql).uniqueResult();
		return s;
	}

	// 根据试剂盒查询index
	public List<Storage> selStorageBykitId(String kitId) {
		String hql = "from Storage where 1=1 and studyType='index' and kit.id='" + kitId + "'";
		List<Storage> s = this.getSession().createQuery(hql).list();
		return s;
	}

	// 查询试剂盒编号
	public DicType getKitByName(String kitName) {
		String hql = "from DicType where 1=1 and name='" + kitName + "'";
		List<DicType> list = this.getSession().createQuery(hql).list();
		return list.get(0);
	}

	public StorageReagentBuySerial getReagentSerialBySn(String sn) {
		String hql = "from StorageReagentBuySerial where 1=1 and instr(note,'" + sn + "')>0";
		String key = "";
		List<StorageReagentBuySerial> list = this.getSession().createQuery(hql + key).list();
		StorageReagentBuySerial sbs = null;
		if (list.size() > 0) {
			sbs = list.get(0);
		}
		return sbs;
	}

	public StorageReagentBuySerial findStorageReagentBuyByInItemId(String id) {
		String hql = "from StorageReagentBuySerial where 1=1 and storageInItem.id='" + id + "'";
		String key = "";
		List<StorageReagentBuySerial> list = this.getSession().createQuery(hql + key).list();
		StorageReagentBuySerial sbs = new StorageReagentBuySerial();
		if (list.size() > 0) {
			sbs = list.get(0);
		}
		return sbs;
	}

	public StorageOutItem getStorageBySn(String sn) {
		String hql = "from StorageOutItem where note='" + sn + "' and storageOut.state='1'";
		StorageOutItem soi = (StorageOutItem) this.getSession().createQuery(hql).uniqueResult();
		if (soi != null) {
			return soi;
		} else {
			return null;
		}
	}
	
	public StorageOutItem getStorageByPihao(String sn) {
		String hql = "from StorageOutItem where code='" + sn + "' and storageOut.state='1'";
		List<StorageOutItem> soi = this.getSession().createQuery(hql).list();
		if (soi.size()>0) {
			return soi.get(0);
		} else {
			return null;
		}
	}

	public Map<String, Object> getStrogeReagent(String id, Integer start, Integer length, String query, String col,
			String sort) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from StorageOutItem where 1=1 and storage.id='" + id
				+ "' and storageOut.state='1'";
		String key = "";
		// if(queryMap!=null){
		// key=map2where(queryMap);
		// }
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from StorageOutItem where storage.id='" + id + "' and storageOut.state='1'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<PlasmaTaskTemp> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
	
	public Map<String, Object> getStrogeReagent(String id, Integer start, Integer length, String query, String col,
			String sort, String batch) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from StorageOutItem where 1=1 and storage.id='" + id
				+ "' and storageOut.state='1' and (usedFinish is null or usedFinish='0') and storageOut.ctype='0' ";
		String key = "";
		// if(queryMap!=null){
		// key=map2where(queryMap);
		// }
//		if(batch!=null
//				&&!"".equals(batch)) {
//			key = key +" and batch = '"+batch+"' ";
//		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from StorageOutItem where storage.id='" + id + "' and storageOut.state='1' and (usedFinish is null or usedFinish='0') and storageOut.ctype='0'  ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<PlasmaTaskTemp> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
	
	public Map<String, Object> getStrogeReagentIsOne(String id, Integer start, Integer length, String query, String col,
			String sort, String batch) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from StorageOutItem where 1=1 and storage.id='" + id
				+ "' and storageOut.state='1' and (usedFinish is null or usedFinish='0') and storageOut.ctype='0' and productMark='"+batch+"' ";
		String key = "";
		// if(queryMap!=null){
		// key=map2where(queryMap);
		// }
//		if(batch!=null
//				&&!"".equals(batch)) {
//			key = key +" and batch = '"+batch+"' ";
//		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from StorageOutItem where storage.id='" + id + "' and storageOut.state='1' and (usedFinish is null or usedFinish='0') and storageOut.ctype='0' and productMark='"+batch+"'  ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<PlasmaTaskTemp> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
	public Map<String, Object> getStrogeReagentByPici(String id, Integer start, Integer length, String query, String col,
			String sort, String batch,String pici) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from StorageOutItem where 1=1 and storage.id='" + id
				+ "' and storageOut.state='1' and (usedFinish is null or usedFinish='0') and storageOut.ctype='0' and code='"+pici+"' ";
		String key = "";
		// if(queryMap!=null){
		// key=map2where(queryMap);
		// }
//		if(batch!=null
//				&&!"".equals(batch)) {
//			key = key +" and batch = '"+batch+"' ";
//		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from StorageOutItem where storage.id='" + id + "' and storageOut.state='1' and (usedFinish is null or usedFinish='0') and storageOut.ctype='0' and code='"+pici+"'  ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<PlasmaTaskTemp> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
	public Map<String, Object> getStrogeReagentByPiciIsOne(String id, Integer start, Integer length, String query, String col,
			String sort, String batch,String pici) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from StorageOutItem where 1=1 and storage.id='" + id
				+ "' and storageOut.state='1' and (usedFinish is null or usedFinish='0') and storageOut.ctype='0' and code='"+pici+"' and productMark='"+batch+"'  ";
		String key = "";
		// if(queryMap!=null){
		// key=map2where(queryMap);
		// }
//		if(batch!=null
//				&&!"".equals(batch)) {
//			key = key +" and batch = '"+batch+"' ";
//		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from StorageOutItem where storage.id='" + id + "' and storageOut.state='1' and (usedFinish is null or usedFinish='0') and storageOut.ctype='0' and code='"+pici+"' and productMark='"+batch+"'  ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<PlasmaTaskTemp> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
	
	public Map<String, Object> getStrogeReagents(String id, Integer start, Integer length, String query, String col,
			String sort) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from StorageReagentBuySerial where 1=1 and storage.id='" + id
				+ "' ";
		String key = "";
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from StorageReagentBuySerial where 1=1 and  storage.id='" + id + "' ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<StorageReagentBuySerial> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length)
					.list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> getStrogeJson(Integer start, Integer length, String query, String col, String sort,
			String type) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from Storage where 1=1 and state='1s'";
		String key = "";
		 if(query!=null){
			 key=map2Where(query);
		 }
		if (type != null && !"".equals(type)) {
			key += " and studyType.id='" + type + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from Storage where 1=1 and state='1s'";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<Storage> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> findStorageReagentBuySerialTable(Integer start, Integer length, String query, String col,
			String sort, String id) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from StorageReagentBuySerial where 1=1 and storage.id='" + id + "'";
		String key = "";
		key += " and num >= 0 ";
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from StorageReagentBuySerial where 1=1 and storage.id='" + id + "' ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<StorageReagentBuySerial> list = new ArrayList<StorageReagentBuySerial>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}
	
	public Map<String, Object> findStorageReagentComponentsTable(Integer start, Integer length, String query, String col,
			String sort, String id) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from StorageReagentComponents where 1=1 and storage.id='" + id + "'";
		String key = "";
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from StorageReagentComponents where 1=1 and storage.id='" + id + "' ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<StorageReagentComponents> list = new ArrayList<StorageReagentComponents>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> selStorageBatchByIds(Integer start, Integer length, String query, String col,
			String sort, String ids) {
		String[] id1 = ids.split(",");
		String id = "";
		for (int i = 0; i < id1.length; i++) {
			if (i != id1.length - 1) {
				id += "'" + id1[i] + "',";
			} else {
				id += "'" + id1[i] + "'";
			}
		}
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from StorageReagentBuySerial where 1=1 and storage.id in (" + id + ")";
		String key = "";
		key += " and num > 0 ";
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from StorageReagentBuySerial where 1=1 and storage.id in (" + id + ")";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<StorageReagentBuySerial> list = new ArrayList<StorageReagentBuySerial>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> selStorageBatchByIds1(Integer start, Integer length, String query, String col,
			String sort, String ids) {
		String[] id1 = ids.split(",");
		String id = "";
		for (int i = 0; i < id1.length; i++) {
			if (i != id1.length - 1) {
				id += "'" + id1[i] + "',";
			} else {
				id += "'" + id1[i] + "'";
			}
		}
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from StorageReagentBuySerial where 1=1 and storage.id in (" + id
				+ ") and  (qcState is null or qcState='0') ";
		String key = "";
		key += " and num >= 0 ";
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from StorageReagentBuySerial where 1=1 and storage.id in (" + id
					+ ") and  (qcState is null or qcState='0') ";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<StorageReagentBuySerial> list = new ArrayList<StorageReagentBuySerial>();
			list = this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> findStorageDialogList(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from StorageReagentBuySerial where 1=1";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from StorageReagentBuySerial where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<StorageReagentBuySerial> list = getSession().createQuery(hql + key).setFirstResult(start)
					.setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> selectStorageList(Map<String, String> mapForQuery, Integer startNum, Integer limitNum,
			String dir, String sort, String p_type, String type) {
		String key = "";
		String hql = " from Storage where 1=1 ";
		if (p_type != null) {
			key += "and p_type='" + p_type + "'";
		}
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		if (type != null) {
			key += "and studyType='" + type + "'";
		}

		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<Storage> list = new ArrayList<Storage>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {
				if (type != null && type.equals("index")) {
					key = key + "order by currentIndex";
				}
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> showStorageDialogListForGoodsJson(Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from Storage where 1=1";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		Long sumCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key).uniqueResult();
			String hql = "from Storage where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort) && sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<Storage> list = getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<PleaseVerifyPlanItem> selectAllResultListById(String id) {
		String hql = "from PleaseVerifyPlanItem  where (submit is null or submit='') and pleaseVerifyPlan.id='" + id
				+ "'";
		List<PleaseVerifyPlanItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<PleaseVerifyPlanItem> selectAllResultListByIds(String[] codes) {
		String insql = "''";
		for (String code : codes) {
			insql += ",'" + code + "'";
		}
		String hql = "from PleaseVerifyPlanItem t where (submit is null or submit='') and id in (" + insql + ")";
		List<PleaseVerifyPlanItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

}
