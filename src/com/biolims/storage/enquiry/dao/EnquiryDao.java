package com.biolims.storage.enquiry.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.purchase.model.PurchaseApply;
import com.biolims.storage.enquiry.model.Enquiry;
import com.biolims.storage.enquiry.model.EnquiryItem;
import com.biolims.storage.enquiry.model.SupplierEnquiryItem;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class EnquiryDao extends BaseHibernateDao {
		public Map<String, Object> selectEnquiryItemList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from EnquiryItem where 1=1 ";
		String key = "";
		if (scId != null&&!"".equals(scId)){
			key = key + " and enquiry.id='" + scId + "'";}
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<EnquiryItem> list = new ArrayList<EnquiryItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> findSupplierEnquiryItemList(String scId,
				Integer startNum, Integer limitNum, String dir, String sort,String storageId) {
			String hql = "from SupplierEnquiryItem where 1=1 ";
			String key = "";
			if (scId != null&&!"".equals(scId)){
				key = key + " and enquiry.id='" + scId + "'";}
			
			if(storageId!=null&&!"".equals(storageId)){
				key=key+" and enquiryItemId='"+storageId+"'";
			}
			Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
			List<SupplierEnquiryItem> list = new ArrayList<SupplierEnquiryItem>();
			if (total > 0) {
				if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
					if (sort.indexOf("-") != -1) {
						sort = sort.substring(0, sort.indexOf("-"));
					}
					key = key + " order by " + sort + " " + dir;
				} 
				if (startNum == null || limitNum == null) {
					list = this.getSession().createQuery(hql + key).list();
				} else {
					list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
				}
			}
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("total", total);
			result.put("list", list);
			return result;
		}
		public List<SupplierEnquiryItem> findSupplierEnquiryItem(String id) {
			String hql=" from SupplierEnquiryItem where 1=1 and enquiry.id='"+id+"'";
			List<SupplierEnquiryItem> list=null;
			list=this.getSession().createQuery(hql).list();
			if(list.size()>0){
				return list;
			}
			return null;
		}
		public Map<String, Object> showEnquiryListJson(Integer start, Integer length, String query, String col,
				String sort) throws Exception {
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from Enquiry where 1=1 ";
			String key = "";
			if (query != null && !"".equals(query)) {
				key = map2Where(query);
			}
			String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
			if(!"all".equals(scopeId)){
				key+=" and scopeId='"+scopeId+"'";
			}
					
			Long sumCount = (Long) getSession().createQuery(countHql+key)
					.uniqueResult();
			if (0l != sumCount) {
				Long filterCount = (Long) getSession().createQuery(countHql + key)
						.uniqueResult();
				String hql = "from Enquiry where 1=1 ";
				if (col != null && !"".equals(col) && !"".equals(sort)
						&& sort != null) {
					col = col.replace("-", ".");
					key += " order by " + col + " " + sort;
				}
				List<Enquiry> list = getSession().createQuery(hql + key)
						.setFirstResult(start).setMaxResults(length).list();
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
			}
			return map;
		}
		public Map<String, Object> showEnquiryItemListJson(String id, Integer start, Integer length, String query,
				String col, String sort) throws Exception {
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from EnquiryItem where 1=1 and enquiry.id = '"+id+"'";
			String key = "";
			if (query != null && !"".equals(query)) {
				key = map2Where(query);
			}
			String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
			if(!"all".equals(scopeId)){
				key+=" and scopeId='"+scopeId+"'";
			}
					
			Long sumCount = (Long) getSession().createQuery(countHql+key)
					.uniqueResult();
			if (0l != sumCount) {
				Long filterCount = (Long) getSession().createQuery(countHql + key)
						.uniqueResult();
				String hql = "from EnquiryItem where 1=1 and enquiry.id = '"+id+"'";
				if (col != null && !"".equals(col) && !"".equals(sort)
						&& sort != null) {
					col = col.replace("-", ".");
					key += " order by " + col + " " + sort;
				}
				List<EnquiryItem> list = getSession().createQuery(hql + key)
						.setFirstResult(start).setMaxResults(length).list();
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
			}
			return map;
		}
		public Map<String, Object> showSupplierEnquiryItemListJson(String id, String itemId, Integer start,
				Integer length, String query, String col, String sort) throws Exception {
			Map<String, Object> map = new HashMap<String, Object>();
			String countHql = "select count(*) from SupplierEnquiryItem where 1=1 and enquiry.id = '"+id+"'";
			String key = "";
			if (query != null && !"".equals(query)) {
				key = map2Where(query);
			}
			if(itemId!=null&&!"".equals(itemId)) {
				key = key + " and enquiryItemId = '"+itemId+"'";
			}
			String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
			if(!"all".equals(scopeId)){
				key+=" and scopeId='"+scopeId+"'";
			}
					
			Long sumCount = (Long) getSession().createQuery(countHql+key)
					.uniqueResult();
			if (0l != sumCount) {
				Long filterCount = (Long) getSession().createQuery(countHql + key)
						.uniqueResult();
				String hql = "from SupplierEnquiryItem where 1=1 and enquiry.id = '"+id+"'";
				if (col != null && !"".equals(col) && !"".equals(sort)
						&& sort != null) {
					col = col.replace("-", ".");
					key += " order by " + col + " " + sort;
				}
				List<SupplierEnquiryItem> list = getSession().createQuery(hql + key)
						.setFirstResult(start).setMaxResults(length).list();
				map.put("recordsTotal", sumCount);
				map.put("recordsFiltered", filterCount);
				map.put("list", list);
			}
			return map;
		}
		public List<EnquiryItem> findEnquiryItemListById(String id) {
			String hql = "from EnquiryItem where 1=1 and enquiry.id = '"+id+"'";
			List<EnquiryItem> list = new ArrayList<EnquiryItem>();
			list = this.getSession().createQuery(hql).list();
			return list;
		}
}