package com.biolims.storage.enquiry.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.storage.enquiry.model.Enquiry;
import com.biolims.storage.enquiry.model.EnquiryItem;
import com.biolims.storage.enquiry.model.SupplierEnquiryItem;
import com.biolims.storage.enquiry.service.EnquiryService;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.util.HttpUtils;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.purchase.model.PurchaseApply;
@Namespace("/storage/enquiry")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class EnquiryAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "";
	@Resource
	private CodingRuleService codingRuleService;
	@Autowired
	private EnquiryService enquiryService;
	private Enquiry enquiry = new Enquiry();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private FieldService fieldService;
	
	
	@Action(value = "showEnquiryItemList")
	public void showEnquiryItemList() throws Exception {
		String id = getParameterFromRequest("id");
		Map<String,Object>map = new HashMap<String,Object>();
		try {
			List<EnquiryItem>list = enquiryService.findEnquiryItemListById(id);
			if(list.size()<0) {
				map.put("msg", true);
			}
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
	}
	
	@Action(value = "showEnquiryList")
	public String showEnquiryList() throws Exception {
		rightsId = "11202";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/storage/enquiry/enquiry.jsp");
	}

	@Action(value = "showEnquiryListJson")
	public void showEnquiryListJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = enquiryService.showEnquiryListJson(start, length, query, col, sort);
			List<Enquiry> list = (List<Enquiry>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			
			map.put("id", "");
			map.put("name", "");
			map.put("createUser-id", "");
			map.put("createUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("confirmUser-id", "");
			map.put("confirmUser-name", "");
			map.put("confirmDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("stateName", "");
			map.put("note", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("Enquiry");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "enquirySelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogEnquiryList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/storage/enquiry/enquiryDialog.jsp");
	}



	@Action(value = "editEnquiry")
	public String editEnquiry() throws Exception {
		rightsId = "11201";
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			enquiry = enquiryService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "enquiry");
		} else {
			enquiry.setId("NEW");
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			enquiry.setCreateUser(user);
			enquiry.setCreateDate(new Date());
			enquiry.setState(SystemConstants.DIC_STATE_NEW);
			enquiry.setStateName(SystemConstants.DIC_STATE_NEW_NAME);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		toState(enquiry.getState());
		return dispatcher("/WEB-INF/page/storage/enquiry/enquiryEdit.jsp");
	}

	@Action(value = "copyEnquiry")
	public String copyEnquiry() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		enquiry = enquiryService.get(id);
		enquiry.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/storage/enquiry/enquiryEdit.jsp");
	}


	@Action(value = "save")
	public void save() throws Exception {
		String mainData = getParameterFromRequest("main");
		String changeLog = getParameterFromRequest("changeLog");
		String enquiryJson = getParameterFromRequest("enquiryJson");
		String enquiryChangeLogItem = getParameterFromRequest("enquiryChangeLogItem");
		String supplierEnquiryJson = getParameterFromRequest("supplierEnquiryJson");
		String supplierEnquiryChangeLogItem = getParameterFromRequest("supplierEnquiryChangeLogItem");
		Map<String,Object>jsonMap = new HashMap<String,Object>();
		jsonMap.put("enquiry", enquiryJson);
		jsonMap.put("enquiryChangeLog", enquiryChangeLogItem);
		jsonMap.put("supplier", supplierEnquiryJson);
		jsonMap.put("supplierChangeLog", supplierEnquiryChangeLogItem);
		
		Map<String,Object>map = new HashMap<String,Object>();
		
		try {
			String id = enquiryService.save(mainData,changeLog,jsonMap);
			map.put("success", true);
			map.put("id", id);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		
		HttpUtils.write(JsonUtils.toJsonString(map));

	}

	@Action(value = "viewEnquiry")
	public String toViewEnquiry() throws Exception {
		String id = getParameterFromRequest("id");
		enquiry = enquiryService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/storage/enquiry/enquiryEdit.jsp");
	}
	

	/**
		 *	展示询价明细
	     * @Title: showEnquiryItemListJson  
	     * @Description: TODO  
	     * @param @throws Exception    
	     * @return void  
		 * @author 孙灵达  
	     * @date 2018年8月7日
	     * @throws
	 */
	@Action(value = "showEnquiryItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showEnquiryItemListJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String id = getParameterFromRequest("id");
		try {
			Map<String, Object> result = enquiryService.showEnquiryItemListJson(id,start, length, query, col, sort);
			List<EnquiryItem> list = (List<EnquiryItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("storageId", "");
			map.put("storageName", "");
			map.put("num", "");
			map.put("type", "");
			map.put("note", "");
			map.put("enquiry-name", "");
			map.put("enquiry-id", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("EnquiryItem");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
		 *	询价供应商明细
	     * @Title: showSupplierEnquiryItemListJson  
	     * @Description: TODO  
	     * @param @throws Exception    
	     * @return void  
		 * @author 孙灵达  
	     * @date 2018年8月7日
	     * @throws
	 */
	@Action(value = "showSupplierEnquiryItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSupplierEnquiryItemListJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		
		String id = getParameterFromRequest("id");
		String itemId = getParameterFromRequest("itemId");
		try {
			Map<String, Object> result = enquiryService.showSupplierEnquiryItemListJson(id, itemId, start, length, query,
					col, sort);
			List<SupplierEnquiryItem> list = (List<SupplierEnquiryItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("supplierId", "");
			map.put("supplierName", "");
			map.put("enquiryItemId", "");
			map.put("price", "");
			map.put("fee", "");
			map.put("num", "");
			map.put("type", "");
			map.put("note", "");
			map.put("enquiry-name", "");
			map.put("enquiry-id", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			Map<String, Object> mapField = fieldService.findFieldByModuleValue("SupplierEnquiryItem");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delEnquiryItem")
	public void delEnquiryItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			enquiryService.delEnquiryItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delSupplierEnquiryItem")
	public void delSupplierEnquiryItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			enquiryService.delSupplierEnquiryItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public EnquiryService getEnquiryService() {
		return enquiryService;
	}

	public void setEnquiryService(EnquiryService enquiryService) {
		this.enquiryService = enquiryService;
	}

	public Enquiry getEnquiry() {
		return enquiry;
	}

	public void setEnquiry(Enquiry enquiry) {
		this.enquiry = enquiry;
	}


}
