package com.biolims.storage.enquiry.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.dic.model.DicType;
import com.biolims.equipment.model.Instrument;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.purchase.model.PurchaseOrder;
import com.biolims.purchase.model.PurchaseOrderEquipmentItem;
import com.biolims.purchase.model.PurchaseOrderItem;
import com.biolims.purchase.model.PurchaseOrderServiceItem;
import com.biolims.storage.enquiry.dao.EnquiryDao;
import com.biolims.storage.enquiry.model.Enquiry;
import com.biolims.storage.enquiry.model.EnquiryItem;
import com.biolims.storage.enquiry.model.SupplierEnquiryItem;
import com.biolims.storage.model.Storage;
import com.biolims.supplier.model.Supplier;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class EnquiryService {
	@Resource
	private EnquiryDao enquiryDao;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(Enquiry i) throws Exception {

		enquiryDao.saveOrUpdate(i);

	}

	public Enquiry get(String id) {
		Enquiry enquiry = commonDAO.get(Enquiry.class, id);
		return enquiry;
	}

	public Map<String, Object> findEnquiryItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = enquiryDao.selectEnquiryItemList(scId,
				startNum, limitNum, dir, sort);
		List<EnquiryItem> list = (List<EnquiryItem>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveEnquiryItem(Enquiry sc, String itemDataJson)
			throws Exception {
		List<EnquiryItem> saveItems = new ArrayList<EnquiryItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			EnquiryItem scp = new EnquiryItem();
			// 将map信息读入实体类
			scp = (EnquiryItem) enquiryDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setEnquiry(sc);

			saveItems.add(scp);
		}
		enquiryDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delEnquiryItem(String[] ids) throws Exception {
		for (String id : ids) {
			EnquiryItem scp = enquiryDao.get(EnquiryItem.class, id);
			enquiryDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(Enquiry sc, Map jsonMap) throws Exception {
		if (sc != null) {
			enquiryDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("enquiryItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveEnquiryItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("supplierEnquiryItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSupplierEnquiryItem(sc, jsonStr);
			}
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveSupplierEnquiryItem(Enquiry sc, String jsonStr)
			throws Exception {
		List<SupplierEnquiryItem> saveItems = new ArrayList<SupplierEnquiryItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(jsonStr,
				List.class);
		for (Map<String, Object> map : list) {
			SupplierEnquiryItem scp = new SupplierEnquiryItem();
			// 将map信息读入实体类
			scp = (SupplierEnquiryItem) enquiryDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setFee(scp.getPrice()*scp.getNum());
			scp.setEnquiry(sc);

			saveItems.add(scp);
		}
		enquiryDao.saveOrUpdateAll(saveItems);

	}

	public Map<String, Object> findSupplierEnquiryItemList(String scId,
			int startNum, int limitNum, String dir, String sort,
			String storageId) {
		return enquiryDao.findSupplierEnquiryItemList(scId, startNum, limitNum,
				dir, sort, storageId);
	}

	public void delSupplierEnquiryItem(String[] ids) {
		for (String id : ids) {
			SupplierEnquiryItem scp = enquiryDao.get(SupplierEnquiryItem.class,
					id);
			enquiryDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String formId) throws Exception {
		Enquiry e = enquiryDao.get(Enquiry.class, formId);
		e.setState("1");
		e.setStateName("完成");
		e.setConfirmDate(new Date());
		enquiryDao.saveOrUpdate(e);
		List<SupplierEnquiryItem> enquiryItem = enquiryDao.findSupplierEnquiryItem(e.getId());
		if (enquiryItem.size() > 0) {
			PurchaseOrder po = new PurchaseOrder();
			String autoID = codingRuleService.genTransID("PurchaseOrder", "PO");
			po.setId(autoID);
//			enquiryDao.saveOrUpdate(po);
			for (SupplierEnquiryItem ei : enquiryItem) {
				DicType dt = commonDAO.get(DicType.class, ei.getType());
				po.setType(dt);
				po.setCreateDate(new Date());
				po.setCreateUser(e.getCreateUser());
				po.setState("3");
				po.setStateName("新建");
				enquiryDao.saveOrUpdate(po);
				if("2".equals(ei.getType())){
					PurchaseOrderItem poi=new PurchaseOrderItem();
					if(ei.getEnquiryItemId()!=null&&!"".equals(ei.getEnquiryItemId())) {
						EnquiryItem enquiry = commonDAO.get(EnquiryItem.class,ei.getEnquiryItemId());
						if(enquiry!=null) {
							Storage storage = commonDAO.get(Storage.class, enquiry.getStorageId());
							poi.setStorage(storage);
						}
					}
					Supplier supplier = commonDAO.get(Supplier.class, ei.getSupplierId());
					poi.setNum(ei.getNum());
					poi.setPrice(ei.getPrice());
					poi.setFee(ei.getFee());
					poi.setPurchaseOrder(po);
					poi.setSupplier(supplier);
					enquiryDao.saveOrUpdate(poi);
				}else if("77".equals(ei.getType())){
					PurchaseOrderEquipmentItem poei=new PurchaseOrderEquipmentItem();
					if(ei.getEnquiryItemId()!=null&&!"".equals(ei.getEnquiryItemId())) {
						EnquiryItem enquiry = commonDAO.get(EnquiryItem.class,ei.getEnquiryItemId());
						if(enquiry!=null) {
							Instrument instrument = commonDAO.get(Instrument.class, enquiry.getStorageId());
							poei.setEquipment(instrument);
						}
					}
					Supplier supplier = commonDAO.get(Supplier.class, ei.getSupplierId());
					poei.setNum(ei.getNum());
					poei.setFee(ei.getFee());
					poei.setPrice(ei.getPrice());
					poei.setPurchaseOrder(po);
					poei.setSupplier(supplier);
					enquiryDao.saveOrUpdate(poei);
				}else if("88".equals(ei.getType())){
					PurchaseOrderServiceItem posi=new PurchaseOrderServiceItem();
					if(ei.getEnquiryItemId()!=null&&!"".equals(ei.getEnquiryItemId())) {
						EnquiryItem enquiry = commonDAO.get(EnquiryItem.class,ei.getEnquiryItemId());
						if(enquiry!=null) {
							String serviceCode = enquiry.getStorageId();
							posi.setServiceCode(serviceCode);
						}
					}
					Supplier supplier = commonDAO.get(Supplier.class, ei.getSupplierId());
					posi.setNum(ei.getNum());
					posi.setFee(ei.getFee());
					posi.setPrice(ei.getPrice());
					posi.setPurchaseOrder(po);
					posi.setSupplier(supplier);
					enquiryDao.saveOrUpdate(posi);
				}
			}
			
		}
	}

	public Map<String, Object> showEnquiryListJson(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return enquiryDao.showEnquiryListJson(start, length, query, col, sort);
	}

	public Map<String, Object> showEnquiryItemListJson(String id, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return enquiryDao.showEnquiryItemListJson(id,start, length, query, col, sort);
	}

	public Map<String, Object> showSupplierEnquiryItemListJson(String id, String itemId, Integer start, Integer length,
			String query, String col, String sort) throws Exception {
		return enquiryDao.showSupplierEnquiryItemListJson(id, itemId, start, length, query,
				col, sort);
	}

	public List<EnquiryItem> findEnquiryItemListById(String id) {
		return enquiryDao.findEnquiryItemListById(id);
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String save(String mainData, String changeLog, Map<String, Object> jsonMap) throws Exception {
		mainData = "["+mainData+"]";
		List<Map<String,Object>> list = JsonUtils.toListByJsonArray(mainData, List.class);
		Enquiry en = new Enquiry();
		en = (Enquiry) commonDAO.Map2Bean(list.get(0), en);
		if((en.getId()!=null&&"".equals(en.getId()))||en.getId().equals("NEW")) {
			String modelName = "Enquiry";
			String markCode = "XJGL";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			en.setId(autoID);
		}
		commonDAO.saveOrUpdate(en);
		if (changeLog != null && !"".equals(changeLog)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
			.getRequest()
			.getSession()
			.getAttribute(
					SystemConstants.USER_SESSION_KEY);
			if(u!=null) {
				li.setUserId(u.getId());
			}
			li.setFileId(en.getId());
			li.setModifyContent(changeLog);
			commonDAO.saveOrUpdate(li);
		}
		
		String enquiryJson = (String) jsonMap.get("enquiry");
		String enquiryChangeLog = (String) jsonMap.get("enquiryChangeLog");
		if(enquiryJson!=null&&!enquiryJson.equals("")&&!enquiryJson.equals("[]")) {
			saveEnquiryItemTable(en,enquiryJson,enquiryChangeLog);
		}
		
		String supplierJson = (String) jsonMap.get("supplier");
		String supplierChangeLog = (String) jsonMap.get("supplierChangeLog");
		if(supplierJson!=null&&!supplierJson.equals("")&&!supplierJson.equals("[]")) {
			saveSupplierEnquiryItemTable(en,supplierJson,supplierChangeLog);
		}
		return en.getId();
		
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveSupplierEnquiryItemTable(Enquiry en, String supplierJson, String supplierChangeLog) throws Exception {
		List<SupplierEnquiryItem> saveItems = new ArrayList<SupplierEnquiryItem>();
		List<Map<String,Object>> list = JsonUtils.toListByJsonArray(supplierJson, List.class);
		for (Map<String, Object> map : list) {
			SupplierEnquiryItem enquiryItem = new SupplierEnquiryItem();
			enquiryItem = (SupplierEnquiryItem) commonDAO.Map2Bean(map, enquiryItem);
			if(enquiryItem.getId()!=null&&"".equals(enquiryItem.getId())) {
				enquiryItem.setId(null);
			}
			enquiryItem.setEnquiry(en);
			if((enquiryItem.getNum()!=null&&!"".equals(enquiryItem.getNum()))&&
					(enquiryItem.getPrice()!=null&&!"".equals(enquiryItem.getPrice()))) {
				enquiryItem.setFee(enquiryItem.getNum()*enquiryItem.getPrice());
			}
			saveItems.add(enquiryItem);
		}
		commonDAO.saveOrUpdateAll(saveItems);
		if (supplierChangeLog != null && !"".equals(supplierChangeLog)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
			.getRequest()
			.getSession()
			.getAttribute(
					SystemConstants.USER_SESSION_KEY);
			if(u!=null) {
				li.setUserId(u.getId());
			}
			li.setFileId(en.getId());
			li.setModifyContent(supplierChangeLog);
			commonDAO.saveOrUpdate(li);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	private void saveEnquiryItemTable(Enquiry en, String enquiryJson, String enquiryChangeLog) throws Exception {
		List<EnquiryItem> saveItems = new ArrayList<EnquiryItem>();
		List<Map<String,Object>> list = JsonUtils.toListByJsonArray(enquiryJson, List.class);
		for (Map<String, Object> map : list) {
			EnquiryItem enquiryItem = new EnquiryItem();
			enquiryItem = (EnquiryItem) commonDAO.Map2Bean(map, enquiryItem);
			if(enquiryItem.getId()!=null&&"".equals(enquiryItem.getId())) {
				enquiryItem.setId(null);
			}
			enquiryItem.setEnquiry(en);
			saveItems.add(enquiryItem);
		}
		commonDAO.saveOrUpdateAll(saveItems);
		if (enquiryChangeLog != null && !"".equals(enquiryChangeLog)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext
			.getRequest()
			.getSession()
			.getAttribute(
					SystemConstants.USER_SESSION_KEY);
			if(u!=null) {
				li.setUserId(u.getId());
			}
			li.setFileId(en.getId());
			li.setModifyContent(enquiryChangeLog);
			commonDAO.saveOrUpdate(li);
		}
	}
}
