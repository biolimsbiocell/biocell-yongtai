package com.biolims.storage.enquiry.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.GeneratedValue;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 库存盘点明细
 * @author lims-platform
 * @date 2017-11-28 14:25:28
 * @version V1.0   
 *
 */
@Entity
@Table(name = "T_ENQUIRY_ITEM")
@SuppressWarnings("serial")
public class EnquiryItem extends EntityDao<EnquiryItem> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**物品编号*/
	private String storageId;
	/**物品描述*/
	private String storageName;
	/**类型*/
	private String type;
	/**采购数量*/
	private Double num;
	/**备注*/
	private String note;
	/**相关主表*/
	private Enquiry enquiry;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  物品编号
	 */
	@Column(name ="STORAGE_ID", length = 100)
	public String getStorageId(){
		return this.storageId;
	}
	/**
	 *方法: 设置String
	 *@param: String  物品编号
	 */
	public void setStorageId(String storageId){
		this.storageId = storageId;
	}
	/**
	 *方法: 取得String
	 *@return: String  物品描述
	 */
	@Column(name ="STORAGE_NAME", length = 50)
	public String getStorageName(){
		return this.storageName;
	}
	/**
	 *方法: 设置String
	 *@param: String  物品描述
	 */
	public void setStorageName(String storageName){
		this.storageName = storageName;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得Enquiry
	 *@return: Enquiry  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "T_ENQUIRY")
	@ForeignKey(name="null")
	public Enquiry getEnquiry(){
		return this.enquiry;
	}
	/**
	 *方法: 设置Enquiry
	 *@param: Enquiry  相关主表
	 */
	public void setEnquiry(Enquiry enquiry){
		this.enquiry = enquiry;
	}
	/**
	 * @return the num
	 */
	public Double getNum() {
		return num;
	}
	/**
	 * @param num the num to set
	 */
	public void setNum(Double num) {
		this.num = num;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	
}