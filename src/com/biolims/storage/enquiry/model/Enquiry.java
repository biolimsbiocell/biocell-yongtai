package com.biolims.storage.enquiry.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.GeneratedValue;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.common.model.user.User;
import com.biolims.common.model.user.User;
/**   
 * @Title: Model
 * @Description: 库存盘点
 * @author lims-platform
 * @date 2017-11-28 14:25:36
 * @version V1.0   
 *
 */
@Entity
@Table(name = "T_ENQUIRY")
@SuppressWarnings("serial")
public class Enquiry extends EntityDao<Enquiry> implements java.io.Serializable {
	/**询价编号*/
	private String id;
	/**描述*/
	private String name;
	/**创建人*/
	private User createUser;
	/**创建时间*/
	private Date createDate;
	/**审核人*/
	private User confirmUser;
	/**审批时间*/
	private Date confirmDate;
	/**状态*/
	private String state;
	/**状态名称*/
	private String stateName;
	/**备注*/
	private String note;
	/**
	 *方法: 取得String
	 *@return: String  询价编号
	 */
	
	@Id
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  询价编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 100)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得User
	 *@return: User  创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	@ForeignKey(name="null")
	public User getCreateUser(){
		return this.createUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  创建人
	 */
	public void setCreateUser(User createUser){
		this.createUser = createUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  创建时间
	 */
	@Column(name ="CREATE_DATE", length = 50)
	public Date getCreateDate(){
		return this.createDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  创建时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 *方法: 取得User
	 *@return: User  审核人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CONFIRM_USER")
	@ForeignKey(name="null")
	public User getConfirmUser(){
		return this.confirmUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  审核人
	 */
	public void setConfirmUser(User confirmUser){
		this.confirmUser = confirmUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  审批时间
	 */
	@Column(name ="CONFIRM_DATE", length = 50)
	public Date getConfirmDate(){
		return this.confirmDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  审批时间
	 */
	public void setConfirmDate(Date confirmDate){
		this.confirmDate = confirmDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态名称
	 */
	@Column(name ="STATE_NAME", length = 50)
	public String getStateName(){
		return this.stateName;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态名称
	 */
	public void setStateName(String stateName){
		this.stateName = stateName;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 100)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
}