package com.biolims.storage.enquiry.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.GeneratedValue;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 库存盘点明细
 * @author lims-platform
 * @date 2017-11-28 14:25:28
 * @version V1.0   
 *
 */
@Entity
@Table(name = "T_SUPPLIER_ENQUIRY_ITEM")
@SuppressWarnings("serial")
public class SupplierEnquiryItem extends EntityDao<SupplierEnquiryItem> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**供应商编号*/
	private String supplierId;
	/**供应商名称*/
	private String supplierName;
	/**价格*/
	private Double price;
	/**总价*/
	private Double fee;
	/**采购数量*/
	private Double num;
	/**类型*/
	private String type;
	/**备注*/
	private String note;
	/**询价明细ID*/
	private String enquiryItemId;
	/**相关主表*/
	private Enquiry enquiry;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得Enquiry
	 *@return: Enquiry  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "T_ENQUIRY")
	@ForeignKey(name="null")
	public Enquiry getEnquiry(){
		return this.enquiry;
	}
	/**
	 *方法: 设置Enquiry
	 *@param: Enquiry  相关主表
	 */
	public void setEnquiry(Enquiry enquiry){
		this.enquiry = enquiry;
	}
	/**
	 * @return the num
	 */
	public Double getNum() {
		return num;
	}
	/**
	 * @param num the num to set
	 */
	public void setNum(Double num) {
		this.num = num;
	}
	/**
	 * @return the supplierId
	 */
	public String getSupplierId() {
		return supplierId;
	}
	/**
	 * @param supplierId the supplierId to set
	 */
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}
	/**
	 * @return the supplierName
	 */
	public String getSupplierName() {
		return supplierName;
	}
	/**
	 * @param supplierName the supplierName to set
	 */
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	/**
	 * @return the price
	 */
	public Double getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(Double price) {
		this.price = price;
	}
	/**
	 * @return the enquiryItemId
	 */
	public String getEnquiryItemId() {
		return enquiryItemId;
	}
	/**
	 * @param enquiryItemId the enquiryItemId to set
	 */
	public void setEnquiryItemId(String enquiryItemId) {
		this.enquiryItemId = enquiryItemId;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the fee
	 */
	public Double getFee() {
		return fee;
	}
	/**
	 * @param fee the fee to set
	 */
	public void setFee(Double fee) {
		this.fee = fee;
	}
	
}