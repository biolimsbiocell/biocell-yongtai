package com.biolims.storage.position.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.storage.position.service.StorageTransferService;

public class StorageTransferEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		StorageTransferService stService = (StorageTransferService) ctx.getBean("storageTransferService");
		//		stService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
