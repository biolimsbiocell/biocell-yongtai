package com.biolims.storage.position.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.common.dao.CommonDAO;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.dic.model.DicState;
import com.biolims.sample.model.DicSampleType;
import com.biolims.storage.position.model.StoragePosition;
import com.biolims.system.storage.model.StorageBox;
import com.biolims.system.work.model.WorkOrderItem;
import com.opensymphony.xwork2.ActionContext;

@Repository
public class StoragePositionDao extends CommonDAO {

	/**
	 * 查询存储位置
	 * 
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> selectStoragePosition(int startNum,
			int limitNum, String dir, String sort) throws Exception {
		String key = "";
		List<StoragePosition> list = new ArrayList<StoragePosition>();
		String hql = "select count(id) from StoragePosition where  1=1 " + key;
		Long count = (Long) this.getSession().createQuery(hql).uniqueResult();
		if (count != null && !new Long(0).equals(count)) {
			hql = "from StoragePosition where 1=1 " + key;
			if (dir != null && sort != null)
				hql = hql + " " + sort + " " + dir;
			list = this.getSession().createQuery(hql).setFirstResult(startNum)
					.setMaxResults(limitNum).list();
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("result", list);
		map.put("count", count);
		return map;
	}

	public Long selectSampleInfoLikePositionId(String ids) throws Exception {
		String key = "";
		key = " and location like '" + ids + "%')";
		String hql = "select count(id) from SampleBloodInfo where 1=1 " + key;
		Long count = (Long) this.getSession().createQuery(hql).uniqueResult();

		return count;
	}

	/**
	 * 查询物品原来的存储位置
	 * 
	 * @param classType
	 *            存放物品的类别
	 * @param type
	 *            操作类型：材料入库（0）、材料主数据(1)、材料归还(2)、动物子表（3）、动物主数据(4)、动物受体鼠合笼(5)、
	 *            动物供体鼠合笼(6)、冷冻保种(8)、组织冻存(9)
	 * @param id
	 *            物品ID
	 * @return 物品原来的存储位置
	 */
	// public String findOldPositon(String classType, String type, String id) {
	// String result = null;
	// String hql = "";
	// String key = "";
	// if (id != null && id != "") {
	// key = " and id = '" + id + "'";
	// }
	// if (type.equals("0")) {
	// switch (Integer.parseInt(classType)) {
	// case 0:
	// hql = " from MaterailsInCell where 1=1 ";
	// List<MaterailsInCell> list0 = this.getSession().createQuery(hql +
	// key).list();
	// if (list0.size() > 0 && list0.get(0).getFrozenLocation() != null
	// && list0.get(0).getFrozenLocation().length() > 1) {
	// result = list0.get(0).getFrozenLocation();
	// }
	// break;
	// case 1:
	// hql = " from MaterailsInPlasmid where 1=1 ";
	// List<MaterailsInPlasmid> list1 = this.getSession().createQuery(hql +
	// key).list();
	// if (list1.size() > 0 && list1.get(0).getFrozenLocation() != null
	// && list1.get(0).getFrozenLocation().length() > 1) {
	// result = list1.get(0).getFrozenLocation();
	// }
	// break;
	// case 2:
	// hql = " from MaterailsInBAC where 1=1 ";
	// List<MaterailsInBAC> list2 = this.getSession().createQuery(hql +
	// key).list();
	// if (list2.size() > 0 && list2.get(0).getFrozenLocation() != null
	// && list2.get(0).getFrozenLocation().length() > 1) {
	// result = list2.get(0).getFrozenLocation();
	// }
	// break;
	// }
	// } else if (type.equals("1")) {
	// switch (Integer.parseInt(classType)) {
	// case 0:
	// hql = " from MaterailsMainCell where 1=1 ";
	// List<MaterailsMainCell> list0 = this.getSession().createQuery(hql +
	// key).list();
	// if (list0.size() > 0 && list0.get(0).getFrozenLocation() != null
	// && list0.get(0).getFrozenLocation().length() > 1) {
	// result = list0.get(0).getFrozenLocation();
	// }
	// break;
	// case 1:
	// hql = " from MaterailsMainPlasmid where 1=1 ";
	// List<MaterailsMainPlasmid> list1 = this.getSession().createQuery(hql +
	// key).list();
	// if (list1.size() > 0 && list1.get(0).getFrozenLocation() != null
	// && list1.get(0).getFrozenLocation().length() > 1) {
	// result = list1.get(0).getFrozenLocation();
	// }
	// break;
	// case 2:
	// hql = " from MaterailsMainBAC where 1=1 ";
	// List<MaterailsMainBAC> list2 = this.getSession().createQuery(hql +
	// key).list();
	// if (list2.size() > 0 && list2.get(0).getFrozenLocation() != null
	// && list2.get(0).getFrozenLocation().length() > 1) {
	// result = list2.get(0).getFrozenLocation();
	// }
	// break;
	// }
	// } else if (type.equals("2")) {
	// switch (Integer.parseInt(classType)) {
	// case 0:
	// hql = " from MaterailsReturnCell where 1=1 ";
	// List<MaterailsReturnCell> list0 = this.getSession().createQuery(hql +
	// key).list();
	// if (list0.size() > 0 && list0.get(0).getFrozenLocation() != null
	// && list0.get(0).getFrozenLocation().length() > 1) {
	// result = list0.get(0).getFrozenLocation();
	// }
	// break;
	// case 1:
	// hql = " from MaterailsReturnPlasmid where 1=1 ";
	// List<MaterailsReturnPlasmid> list1 = this.getSession().createQuery(hql +
	// key).list();
	// if (list1.size() > 0 && list1.get(0).getFrozenLocation() != null
	// && list1.get(0).getFrozenLocation().length() > 1) {
	// result = list1.get(0).getFrozenLocation();
	// }
	// break;
	// case 2:
	// hql = " from MaterailsReturnBAC where 1=1 ";
	// List<MaterailsReturnBAC> list2 = this.getSession().createQuery(hql +
	// key).list();
	// if (list2.size() > 0 && list2.get(0).getFrozenLocation() != null
	// && list2.get(0).getFrozenLocation().length() > 1) {
	// result = list2.get(0).getFrozenLocation();
	// }
	// break;
	// }
	// } else if (type.equals("3")) {
	// hql = " from AnimalInItem where 1=1 ";
	// List<AnimalInItem> list3 = this.getSession().createQuery(hql +
	// key).list();
	// if (list3.size() > 0 && list3.get(0).getCageCode() != null &&
	// list3.get(0).getCageCode().length() > 1) {
	// result = list3.get(0).getCageCode();
	// }
	// } else if (type.equals("4")) {
	// hql = " from AnimalMain where 1=1 ";
	// List<AnimalMain> list3 = this.getSession().createQuery(hql + key).list();
	// if (list3.size() > 0 && list3.get(0).getCageCode() != null &&
	// list3.get(0).getCageCode().length() > 1) {
	// result = list3.get(0).getCageCode();
	// }
	// } else if (type.equals("5")) {
	// hql = " from InjectNaturalMate where 1=1 ";
	// List<InjectNaturalMate> list3 = this.getSession().createQuery(hql +
	// key).list();
	// if (list3.size() > 0 && list3.get(0).getCageCode() != null &&
	// list3.get(0).getCageCode().length() > 1) {
	// result = list3.get(0).getCageCode();
	// }
	// } else if (type.equals("6")) {
	// hql = " from InjectHormoneMate where 1=1 ";
	// List<InjectHormoneMate> list3 = this.getSession().createQuery(hql +
	// key).list();
	// if (list3.size() > 0 && list3.get(0).getCageCode() != null &&
	// list3.get(0).getCageCode().length() > 1) {
	// result = list3.get(0).getCageCode();
	// }
	// } else if (type.equals("7")) {
	// hql = " from InjectGomMate where 1=1 ";
	// List<InjectGomMate> list3 = this.getSession().createQuery(hql +
	// key).list();
	// if (list3.size() > 0 && list3.get(0).getCageCode() != null &&
	// list3.get(0).getCageCode().length() > 1) {
	// result = list3.get(0).getCageCode();
	// }
	// } else if (type.equals("8")) {
	// hql = " from InjectSaveFrozenItem where 1=1 ";
	// List<InjectSaveFrozenItem> list3 = this.getSession().createQuery(hql +
	// key).list();
	// if (list3.size() > 0 && list3.get(0).getFrozenLocation() != null
	// && list3.get(0).getFrozenLocation().length() > 1) {
	// result = list3.get(0).getFrozenLocation();
	// }
	// } else if (type.equals("9")) {
	// hql = " from MaterailsMain where 1=1 ";
	// List<MaterailsMain> list3 = this.getSession().createQuery(hql +
	// key).list();
	// if (list3.size() > 0 && list3.get(0).getFrozenLocation() != null
	// && list3.get(0).getFrozenLocation().length() > 1) {
	// result = list3.get(0).getFrozenLocation();
	// }
	// }
	// return result;
	// }

	/**
	 * 更改材料主数据中的存储位置
	 * 
	 * @param type
	 * @param id
	 * @param value
	 */
	public void updatePosition(String type, String id, String value) {
		if (type != null) {
			String hql = null;
			switch (Integer.parseInt(type)) {
			case 0:
				hql = "update  MaterailsMainCell m set m.frozenLocation = '"
						+ value + "' where m.id='" + id + "'";
				break;
			case 1:
				hql = "update  MaterailsMainPlasmid m set m.frozenLocation = '"
						+ value + "' where m.id='" + id + "'";
				break;
			case 2:
				hql = "update  MaterailsMainBAC m set m.frozenLocation = '"
						+ value + "' where m.id='" + id + "'";
				break;
			}
			this.getSession().createQuery(hql).executeUpdate();
		}
	}
	//根据样本类型ID查询下一步流向
	public List<DicSampleType> selectNextFlowBySampleTypeId(String sampleTypeId)
			throws Exception {
		String hql = "from DicSampleType where id='" + sampleTypeId
				+ "' order by orderNumber ASC";
		List<DicSampleType> list = new ArrayList<DicSampleType>();
		list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	
	//selSampleCodeBySampleInfo
	//根据样本编号查询原始样本号
	public String selSampleCodeBySampleInfo(String code) throws Exception {
		String sql = "select t.sample_code from sample_info t where 1=1 and t.code='"+code+"'";
		String sampleCode= this.getSession().createSQLQuery(sql).uniqueResult().toString();
		return sampleCode;
	}


	public Long selectSampleInfoInCount(String ids) throws Exception {
		String key = "";
		key = " and location like '" + ids + "%')";
		String hql = "select count(id) from SampleInfoIn where 1=1 " + key;
		Long count = (Long) this.getSession().createQuery(hql).uniqueResult();

		return count;
	}

	// 查询有效的状态（1cs：有效；0cs：无效；）
	public DicState findDicState() {
		String hql = "from DicState where 1=1 ";
		String key = " and id='1p' ";
		List<DicState> list = this.getSession().createQuery(hql + key).list();
		return list.get(0);
	}

	// 冰箱
	public List<StoragePosition> findName() throws Exception {
//		String hql = "from StoragePosition t where t.level='0' and t.type.sysCode='BX' and t.state.id='1p'";
		String hql = "from StoragePosition t where t.level='0' and t.type.sysCode like '%BX%' and t.state.id='1p'";
//		select t.*, t.rowid from t_storage_position t where t.leaf='0' and t.dic_type_id like '%BX%' and t.dic_state_id='1p'
		List<StoragePosition> list = this.getSession().createQuery(hql).list();
		for (StoragePosition sp : list) {
			sp.setIsUse(String.valueOf(selectSampleInfoInCount(sp.getId())));
		}
		return list;
	}

	// 冰箱内共多少个位置
	public List<StoragePosition> findName2() throws Exception {
//		String hql = "from StoragePosition t where t.level='2' and t.state.id='1p' and t.type.sysCode='BX'";
		String hql = "from StoragePosition t where t.level='2' and t.type.sysCode like '%BX%' and t.state.id='1p'";
		List<StoragePosition> list = this.getSession().createQuery(hql).list();
		for (StoragePosition sp : list) {
			sp.setIsUse(String.valueOf(selectSampleInfoInCount(sp.getId())));
		}

		return list;

	}
	// 查询容器
	// public List<StorageContainer> findName3(){
	// String hql = "from StorageContainer t where t.";
	// List<StorageContainer> list = this.getSession().createQuery(hql).list();
	// }

	public List<StorageBox> selStorageBoxByLocation(String id1) {
		String hql = "from StorageBox where 1=1 and locationId='"+id1+"'";
		List<StorageBox>list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<StoragePosition> selStoragePositionById(String id2) {
		String hql = "from StoragePosition where 1=1 and id like '"+id2+"%' and leaf='1'";
		List<StoragePosition>list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<WorkOrderItem> selWorkOrderItemByModel() {
		String hql = "from WorkOrderItem where 1=1  and applicationTypeTable='SampleOut'";
		List<WorkOrderItem> list = new ArrayList<WorkOrderItem>();
		list = this.getSession().createQuery(hql).list();
		return list;
	
	}

	public Map<String, Object> findStoragePositionDialogList(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from StoragePosition where 1=1";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		
		Long sumCount = (Long) getSession().createQuery(countHql+key)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from StoragePosition where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<StoragePosition> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

}
