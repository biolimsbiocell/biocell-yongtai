package com.biolims.storage.position.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.activiti.engine.impl.util.json.JSONArray;
import org.activiti.engine.impl.util.json.JSONObject;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.experiment.cell.passage.model.CellPassageTemp;
import com.biolims.experiment.dna.model.DnaTaskTemp;
import com.biolims.experiment.freeze.model.FreezeTaskTemp;
import com.biolims.experiment.massarray.model.MassarrayTaskTemp;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.main.service.MainService;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.service.SampleInputService;
import com.biolims.sample.service.SampleStateService;
import com.biolims.sample.storage.dao.SampleInDao;
import com.biolims.sample.storage.dao.SampleOutDao;
import com.biolims.sample.storage.model.SampleIn;
import com.biolims.sample.storage.model.SampleInItem;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.sample.storage.model.SampleInfoIn;
import com.biolims.sample.storage.model.SampleOut;
import com.biolims.sample.storage.model.SampleOutItem;
import com.biolims.storage.container.dao.ContainerDao;
import com.biolims.storage.container.service.ContainerService;
import com.biolims.storage.position.model.StoragePosition;
import com.biolims.system.nextFlow.dao.NextFlowDao;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.product.model.Product;
import com.biolims.system.storage.model.StorageBox;
import com.opensymphony.xwork2.ActionContext;

@Service
@Transactional(rollbackFor = Exception.class)
@SuppressWarnings({ "unchecked" })
public class SaveSampleInService {
	@Autowired
	private ContainerService containerService;
	@Autowired
	private CommonService commonService;

	@Autowired
	private ContainerDao containerDao;

	@Autowired
	private MainService mainService;
	@Autowired
	private StoragePositionService storagePositionService;
	@Resource
	private SampleInDao sampleInDao;
	@Resource
	private SampleOutDao sampleOutDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInputService sampleInputService;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private NextFlowDao nextFlowDao;
	@Resource
	private SampleStateService sampleStateService;
	@Resource
	private SaveSampleInService saveSampleInService;
	@Resource
	private CodingRuleService codingRuleService;

	private SampleOut sampleOut = new SampleOut();
	private SampleIn sampleIn = new SampleIn();

	// 样本入库

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String saveSampleIn(String position1, String code1, User user)
			throws Exception {

		String mark = "";
		JSONObject position1Json = new JSONObject(position1);
		JSONArray positionArr = position1Json.getJSONArray("position");

		JSONObject code1Json = new JSONObject(code1);
		JSONArray codeArr = code1Json.getJSONArray("code");
		if(codeArr.length()==0) {
			mark = "EXIST";
			return mark;
		}else {
			int size = codeArr.length();

			// 新建一张任务并保存
			String modelName = "SampleIn";
			String markCode = "YBRK";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			sampleIn.setId(autoID);
			// User user = (User) this
			// .getObjFromSession(SystemConstants.USER_SESSION_KEY);
			sampleIn.setCreateUser(user);
			sampleIn.setCreateDate(new Date());
			sampleIn.setState(SystemConstants.DIC_STATE_NEW);
			sampleIn.setStateName(SystemConstants.DIC_STATE_NEW_NAME);
			sampleIn.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			sampleIn.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			// saveItemsNotNull.add(sampleIn);
			commonService.saveOrUpdate(sampleIn);
			for (int i = 0; i < size; i++) {

				// 位置
				String position = positionArr.getString(i);
				//获取存储位置
				String[] split = position.split("-");
				String positionType = null;
				if(split.length>0) {
					StoragePosition storagePosition = commonDAO.get(StoragePosition.class, split[0]);
					if(storagePosition!=null) {
						positionType = storagePosition.getPositionType();
					}
				}
				// 样本编号
				String code = codeArr.getString(i);
				// 通过样本编号查询左侧表数据
				List<SampleInItemTemp> list = containerDao
						.selSampleInTempByCode(code);
				// //查询到的数据不在显示在左侧表
				 for(int j=0;j<list.size();j++){
				 list.get(j).setState("2");
				 }
				// 将入库明细表数据与主表关联
				SampleInItem sii = new SampleInItem();
				SampleIn s = mainService.get(SampleIn.class, autoID);
				sii.setStockType(positionType);
				sii.setParentId(list.get(0).getParentId());
				sii.setSampleType(list.get(0).getSampleType());
				sii.setBarCode(list.get(0).getBarCode());
				sii.setSamplingDate(list.get(0).getSamplingDate());
				sii.setSampleTypeId(list.get(0).getSampleTypeId());
				sii.setDicSampleType(list.get(0).getDicSampleType());
				sii.setConcentration(list.get(0).getConcentration());
				sii.setVolume(list.get(0).getVolume());
				sii.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				sii.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
				sii.setSumTotal(list.get(0).getSumTotal());
				sii.setTempId(list.get(0).getId());
				sii.setProductId(list.get(0).getProductId());
				sii.setProductName(list.get(0).getProductName());
				sii.setSampleInfo(list.get(0).getSampleInfo());
				sii.setSampleIn(s);
				sii.setState("1");
				sii.setLocation(position);
				sii.setCode(code);
				sii.setPronoun(list.get(0).getPronoun());
				// 根据样本编号查询原始样本号
				// String sampleCode =
				// this.storagePositionService.finSampleCodeBySampleInfo(code);
				sii.setSampleCode(list.get(0).getSampleCode());
				if(list.get(0).getSampleOrder()!=null) {
					sii.setSampleOrder(commonDAO.get(SampleOrder.class,list.get(0).getSampleOrder().getId()));
				}
				
				
				sii.setNum(list.get(0).getNum());
				sii.setSampleDeteyionId(list.get(0).getSampleDeteyionId());
				sii.setSampleDeteyionName(list.get(0).getSampleDeteyionName());
				sii.setInfoFrom(list.get(0).getInfoFrom());
				sii.setInfoFromId(list.get(0).getInfoFromId());
				sii.setSampleOrder(list.get(0).getSampleOrder());
				
				
				commonService.saveOrUpdate(sii);
			}

			SampleIn sr = sampleInDao.get(SampleIn.class, autoID);
			sr.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
			sr.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
			User user2 = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			sr.setAcceptDate(new Date());
			sr.setAcceptUser(user2);
			sampleInDao.update(sr);
			changeTempState(autoID);// -----------改变左侧表的状态

			return autoID;
		}

	}

	// 血袋入库
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String saveSampleInByBlood(String position1, String code1, User user)
			throws Exception {

		// JSONObject position1Json=new JSONObject(position1);
		// JSONArray positionArr=position1Json.getJSONArray("position");

		JSONObject code1Json = new JSONObject(code1);
		JSONArray codeArr = code1Json.getJSONArray("code");

		int size = codeArr.length();

		// 新建一张任务并保存
		String modelName = "SampleIn";
		String markCode = "YBRK";
		String autoID = codingRuleService.genTransID(modelName, markCode);
		sampleIn.setId(autoID);
		// User user = (User) this
		// .getObjFromSession(SystemConstants.USER_SESSION_KEY);
		sampleIn.setCreateUser(user);
		sampleIn.setCreateDate(new Date());
		sampleIn.setState(SystemConstants.DIC_STATE_NEW);
		sampleIn.setStateName(SystemConstants.DIC_STATE_NEW_NAME);
		// saveItemsNotNull.add(sampleIn);
		commonService.saveOrUpdate(sampleIn);

		// 位置
		String position = position1;
		for (int i = 0; i < size; i++) {

			// 样本编号
			String code = codeArr.getString(i);
			// 通过样本编号查询左侧表数据
			List<SampleInItemTemp> list = containerDao
					.selSampleInTempByCode(code);
			// //查询到的数据不在显示在左侧表
			 for(int j=0;j<list.size();j++){
			 list.get(j).setState("2");
			 }
			// 将入库明细表数据与主表关联
			SampleInItem sii = new SampleInItem();
			SampleIn s = mainService.get(SampleIn.class, autoID);
			sii.setParentId(list.get(0).getParentId());
			sii.setSampleType(list.get(0).getSampleType());
			sii.setSampleTypeId(list.get(0).getSampleTypeId());
			sii.setConcentration(list.get(0).getConcentration());
			sii.setVolume(list.get(0).getVolume());
			sii.setProductId(list.get(0).getProductId());
			sii.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			sii.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			sii.setProductName(list.get(0).getProductName());
			sii.setSampleInfo(list.get(0).getSampleInfo());
			sii.setSumTotal(list.get(0).getSumTotal());
			sii.setTempId(list.get(0).getId());
			sii.setSamplingDate(list.get(0).getSamplingDate());
			sii.setBarCode(list.get(0).getBarCode());
			sii.setQpcrConcentration(list.get(0).getQpcrConcentration());
			sii.setIndexa(list.get(0).getIndexa());
			sii.setSampleIn(s);
			sii.setState("1");
			sii.setLocation(position);
			sii.setCode(code);
			// 根据样本编号查询原始样本号
			// String sampleCode =
			// this.storagePositionService.finSampleCodeBySampleInfo(code);
			sii.setSampleCode(list.get(0).getSampleCode());
			commonService.saveOrUpdate(sii);
		}

		SampleIn sr = sampleInDao.get(SampleIn.class, autoID);
		sr.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sr.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		User user2 = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		sr.setAcceptDate(new Date());
		sr.setAcceptUser(user2);
		sampleInDao.update(sr);
		changeTempState(autoID);// -----------改变左侧表的状态
		return autoID;

	}

	/**
	 * 根据完成的入库单改变左侧表的状态
	 * 
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeTempState(String id) throws Exception {
		// 获取子表集合
		List<SampleInItem> list = sampleInDao.getSampleInItemList(id);
		if (list.size() > 0) {
			// 如果子表的数据不为空就循环读取子表集合
			for (SampleInItem si : list) {
				// 查询样本是否是返库
				SampleInItem item = this.sampleInDao.selectTempByCode(si
						.getCode());
				// 改变左侧状态
				SampleInItemTemp siit = commonDAO.get(SampleInItemTemp.class,
						si.getTempId());
				siit.setState("2");
				if (item == null) {// 新入库样本
					// 把子表对象的数据改成“1”
					si.setState("1");
					// 样本状态 4:在库
					si.setOutState("4");
				} else {// 返库样本
					item.setConcentration(si.getConcentration());
					item.setVolume(si.getVolume());
					item.setSumTotal(si.getSumTotal());
					// 样本状态 4:在库
					item.setOutState("4");
					commonDAO.saveOrUpdate(item);
				}
				// 出库样本信息
				SampleInfoIn sif = null;
				sif = this.sampleInDao.getSampleInItemByCode(si.getCode());
				if (sif == null) {
					sif = new SampleInfoIn();
					sampleInputService.copy(sif, si);
					sif.setParentId(si.getParentId());
				} else {
					sif.setState("1");
					sif.setConcentration(si.getConcentration());
					sif.setVolume(si.getVolume());
					sif.setSumTotal(si.getSumTotal());
					
					sif.setCode(si.getCode());
					sif.setSampleCode(si.getSampleCode());
					sif.setProductId(si.getProductId());
					sif.setProductName(si.getProductName());
					sif.setSampleType(si.getSampleType());
					sif.setNum(si.getNum());
					sif.setSampleDeteyionId(si.getSampleDeteyionId());
					sif.setSampleDeteyionName(si.getSampleDeteyionName());
					sif.setInfoFrom(si.getInfoFrom());
					sif.setInfoFromId(si.getInfoFromId());
					sif.setSampleOrder(si.getSampleOrder());
				}
				
				sif.setStockType(si.getStockType());
				sif.setPronoun(si.getPronoun());
				if (si.getSampleCode() != null) {
					String[] sampleCode = si.getSampleCode().split(",");
					for (int i = 0; i < sampleCode.length; i++) {
						List<SampleInfo> sifis = commonDAO.findByProperty(
								SampleInfo.class, "code", sampleCode[i]);
						if(sifis.size()>0){
							SampleInfo sifi = sifis.get(0);
							if (sifi.getCrmCustomer() != null) {
								sif.setCustomer(sifi.getCrmCustomer());
								si.setCustomer(sifi.getCrmCustomer());
							}
							sif.setSampleInfo(sifi);
							si.setSampleInfo(sifi);
						}
					}
				}
//				if (si.getSampleType() != null
//						&& si.getSampleType().equals("SampleDnaInfo")) {
//					if (si.getCode() != null) {
//						// SampleDnaInfo sdif = commonDAO.findByProperty(
//						// SampleDnaInfo.class, "code", si.getCode()).get(
//						// 0);
//						// sif.setSampleDnaInfo(sdif);
//					}
//				}
				
				//通过si中位置查找盒子信息bx01-01-01-G01-C05
				String location = si.getLocation();
				int n = location.length()-location.replaceAll("-", "").length();
				sif.setLocation(location);
				//n为4则为样本入库
				if(n==4){
					String local = location.substring(0, location.length()-4);
					List<StorageBox> sb = sampleInDao.selectStorageBoxByLocation(local);
					if(sb.size()>0){
						if(sif.getStorageBox()==null || "".equals(sif.getStorageBox())){
							sif.setStorageBox(sb.get(0));
						}
						if(sif.getBoxLocation()==null || "".equals(sif.getBoxLocation())){
							sif.setBoxLocation(location.substring(location.length()-3, location.length()));
						}
						
					}
				}
				commonDAO.saveOrUpdate(sif);

			}
		}
	}

	// 样本出库
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String saveSampleOut(String sJson, String outType, User user)
			throws Exception {

		ArrayList<SampleOutItem> SampleOutItem1 = new ArrayList<SampleOutItem>();
		ArrayList<SampleInfoIn> sampleInfoIn1 = new ArrayList<SampleInfoIn>();

		JSONObject dataJson = new JSONObject(sJson);
		JSONArray data = dataJson.getJSONArray("strobj");
		int size = data.length();

		// 新建一张出库任务单并保存
		String modelName = "SampleOut";
		String markCode = "YBCK";
		String autoID = codingRuleService.genTransID(modelName, markCode);
		sampleOut.setId(autoID);
		if (outType.equals("实验")) {
			sampleOut.setOutType("0");
		} else if (outType.equals("返回客户")) {
			sampleOut.setOutType("1");
		} else if (outType.equals("外包出库")) {
			sampleOut.setOutType("2");
		} else if (outType.equals("样本销毁")) {
			sampleOut.setOutType("3");
		}

		// User user = (User) this
		// .getObjFromSession(SystemConstants.USER_SESSION_KEY);
		sampleOut.setCreateUser(user);
		sampleOut.setCreateDate(new Date());
		sampleOut.setState(SystemConstants.DIC_STATE_NEW);
		sampleOut.setStateName(SystemConstants.DIC_STATE_NEW_NAME);
		sampleOut.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
		sampleOut.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
		// sampleOut1.add(sampleOut);
		commonService.saveOrUpdate(sampleOut);
		for (int i = 0; i < size; i++) {
			JSONObject info = data.getJSONObject(i);
			String code = info.getString("code");
			String nextFlowId = info.getString("nextFlowId");
			String nextFlowName = info.getString("nextFlowName");
			String isDepot = info.getString("isDepot");
			String note = info.getString("note");

			// 通过样本编号查询出库申请页面选择样本的状态
			List<SampleInfoIn> sii = sampleOutDao.selSampleInfoInByCode(code);
			// 选择样本内不再显示样本
			SampleInfoIn sr = sampleOutDao.get(SampleInfoIn.class, sii.get(0)
					.getId());
			sr.setState("0");
			sampleInfoIn1.add(sr);
			commonService.saveOrUpdate(sr);
			SampleInfo si = new SampleInfo();
			if (sr.getSampleInfo() != null) {
				// 根据sampleInfoIn中数据查询样本主数据sampleInfo表
				si = sampleOutDao.get(SampleInfo.class, sr.getSampleInfo()
						.getId());
			}

			// 将出库明细表数据与主表关联
			SampleOutItem soi = new SampleOutItem();
			// SampleOut s =mainService.get(SampleOut.class,autoID);

			soi.setCode(code);
			soi.setSampleCode(sr.getSampleCode());
			if (si.getProductId() == null || "".equals(si.getProductId())) {
				soi.setProductId(si.getProductId());
			}
			if (si.getProductName() == null || "".equals(si.getProductName())) {
				soi.setProductName(si.getProductName());
			}

			soi.setParentId(sr.getParentId());
			soi.setSampleType(sr.getSampleType());
			soi.setLocation(sr.getLocation());
			soi.setConcentration(sr.getConcentration());
			soi.setVolume(sr.getVolume());
			soi.setSumTotal(sr.getSumTotal());
			soi.setSampleOut(sampleOut);
			soi.setState("1");
			soi.setNextFlowId(nextFlowId);
			soi.setNextFlow(nextFlowName);
			if(sr.getSampleOrder()!=null) {
				soi.setSampleOrder(commonDAO.get(SampleOrder.class,
						sr.getSampleOrder().getId()));
			}
			soi.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			soi.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			if (isDepot.equals("返库")) {
				soi.setIsDepot("2");
			} else if (isDepot.equals("不返库")) {
				soi.setIsDepot("0");
			}
			// 2反库0不反库
			soi.setNote(note);
			SampleOutItem1.add(soi);
			commonService.saveOrUpdate(soi);
		}

		// SampleOut so = sampleOutDao.get(SampleOut.class, autoID);
		sampleOut
				.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sampleOut
				.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		sampleOut.setAcceptDate(new Date());

		submit(autoID, sampleOut, SampleOutItem1);// 下一步流向
		updateMainData(autoID, sampleOut, SampleOutItem1);

		commonService.saveOrUpdate(sampleOut);
		// sampleOutDao.saveOrUpdateAll(SampleOutItem1);
		// sampleOutDao.saveOrUpdateAll(sampleInfoIn1);
		return autoID;

	}

	// 样本出库
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void updateMainData(String id, SampleOut so,
			ArrayList<SampleOutItem> list) throws Exception {
		// List<SampleOutItem> list = sampleOutDao.getSampleOutItemList(id);
		for (SampleOutItem mc : list) {
			List<SampleInfo> spil = commonService.get(SampleInfo.class, "code",
					mc.getCode());
			if (spil.size() > 0) {
				SampleInfo s = spil.get(0);
				s.setLocation(null);
				commonService.saveOrUpdate(s);
			}

			// 改变库存样本的样本状态
			SampleInItem inItem = this.sampleInDao.selectTempByCode(mc
					.getCode());
			if (inItem != null) {
				inItem.setOutState(so.getOutType());
				commonService.saveOrUpdate(inItem);
			}
		}
	}

	/**
	 * 样本出库下一步流向方法
	 * 
	 * @throws ParseException
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void submit(String id, SampleOut so, ArrayList<SampleOutItem> list)
			throws Exception {
		// SampleOut so = sampleOutDao.get(SampleOut.class, id);

		// 根据主表ID获取相关子表集合
		// List<SampleOutItem> list = sampleOutDao.getSampleOutItemList(id);
		if (list.size() > 0) {
			// 如果相关子表数据不为空，就循环读取子表数据
			for (SampleOutItem scp : list) {
				// if (scp.getInfoFrom().equals("SampleOutApply")) {
				SampleInfo sf = sampleInfoMainDao.findSampleInfo(scp
						.getSampleCode());
				if (scp != null) {
					if (scp.getNextFlowId() != null) {
						if (scp.getNextFlowId().equals("0009")) {// 样本入库
							SampleInItemTemp st = new SampleInItemTemp();
							st.setParentId(scp.getParentId());
							st.setCode(scp.getCode());
							st.setSampleCode(scp.getSampleCode());
							st.setNum(scp.getSampleNum());
							st.setSampleType(scp.getSampleType());
							st.setState("1");
							if(scp.getSampleOrder()!=null) {
								st.setSampleOrder(commonDAO.get(SampleOrder.class,
										scp.getSampleOrder().getId()));
							}
							sampleOutDao.saveOrUpdate(st);
						} else if (scp.getNextFlowId().equals("0002")) {// 核酸提取样本接收
							// DnaReceiveTemp d = new DnaReceiveTemp();
							// scp.setState("1");
							// sampleInputService.copy(d, scp);
							// d.setSampleInfo(sf);
							// d.setSampleNum(scp.getSumTotal());
							// sampleOutDao.saveOrUpdate(d);
						} else if (scp.getNextFlowId().equals("0029")) {// 样本处理
							MassarrayTaskTemp d = new MassarrayTaskTemp();
							scp.setState("1");
							sampleInputService.copy(d, scp);
							d.setTechJkServiceTask(scp.getSampleOut()
									.getTechJkServiceTask());
							d.setSampleInfo(sf);
							sampleOutDao.saveOrUpdate(d);
						} else if (scp.getNextFlowId().equals("0017")) {// 核酸提取
							DnaTaskTemp d = new DnaTaskTemp();
							scp.setState("1");
							if (scp.getProductId() != null) {
								Product p = commonDAO.get(Product.class,
										scp.getProductId());
							}

							sampleInputService.copy(d, scp);
							// d.setSampleInfo(sf);
							// d.setSampleNum(scp.getSumTotal());
							// d.setSampleStyle(scp.getSampleStyle());
							sampleOutDao.saveOrUpdate(d);
						} /*else if (scp.getNextFlowId().equals("0018")) {// 文库构建
							WkTaskTemp d = new WkTaskTemp();
							scp.setState("1");
							sampleInputService.copy(d, scp);
							if (scp.getProductId() != null) {
								Product p = commonDAO.get(Product.class,
										scp.getProductId());
							}
							d.setSampleInfo(sf);
							sampleOutDao.saveOrUpdate(d);
						}*/ else if (scp.getNextFlowId().equals("0071")) {//冻存
							FreezeTaskTemp d = new FreezeTaskTemp();
							scp.setState("1");
							sampleInputService.copy(d, scp);
							if (scp.getProductId() != null) {
								Product p = commonDAO.get(Product.class,
										scp.getProductId());
							}
							d.setPronoun(scp.getPronoun());
							d.setSampleInfo(sf);
							sampleOutDao.saveOrUpdate(d);
						}/*else if (scp.getNextFlowId().equals("0072")) {// 复苏
							CellReviveTemp d = new CellReviveTemp();
							scp.setState("1");
							sampleInputService.copy(d, scp);
							if (scp.getProductId() != null) {
								Product p = commonDAO.get(Product.class,
										scp.getProductId());
							}
							d.setPronoun(scp.getPronoun());
							d.setSampleInfo(sf);
							sampleOutDao.saveOrUpdate(d);
						}*/else if (scp.getNextFlowId().equals("0073")) {// 传代
							CellPassageTemp d = new CellPassageTemp();
							scp.setState("1");
							sampleInputService.copy(d, scp);
							if (scp.getProductId() != null) {
								Product p = commonDAO.get(Product.class,
										scp.getProductId());
							}
							d.setPronoun(scp.getPronoun());
							d.setSampleInfo(sf);
							sampleOutDao.saveOrUpdate(d);
						}else if (scp.getNextFlowId().equals("0012")) {// 暂停
						} else if (scp.getNextFlowId().equals("0013")) {// 终止
						} else {
							// 得到下一步流向的相关表单
							List<NextFlow> list_nextFlow = nextFlowDao
									.seletNextFlowById(scp.getNextFlowId());
							for (NextFlow n : list_nextFlow) {
								Object o = Class.forName(
										n.getApplicationTypeTable()
												.getClassPath()).newInstance();
								scp.setState("1");
								// scp.setReportDate(so.getReportDate());
								sampleInputService.copy(o, scp);
							}
						}
					}
				}
				// }

				DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String[] sampleCode = scp.getSampleCode().split(",");
				for (int i = 0; i < sampleCode.length; i++) {
					sampleStateService.saveSampleState(
							scp.getCode(),
							sampleCode[i],
							scp.getProductId(),
							scp.getProductName(),
							"",
							format.format(so.getAcceptDate()),
							format.format(new Date()),
							"SampleOut",
							"样本出库",
							(User) ServletActionContext
									.getRequest()
									.getSession()
									.getAttribute(
											SystemConstants.USER_SESSION_KEY),
							so.getId(), scp.getNextFlow(), "1", null, null,
							null, null, null, null, null, null);
				}

			}
		}
	}

	// 移库操作
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void moveSampleLocation(String newPosition, String oldPosition,
			String code1, User user,String boxId) throws Exception {

		// 样本编号
		// {"code":["DA1706150008","DA1706150009","DA1706150010","DA1706150011"]}
		// 旧盒子位置
		// {"oldPosition":["A0001-01-01-A02"}
		// 新盒子位置
		// {"newPosition":["A0001-01-01-A05"}
		// 或{"newPosition":["A0001-01-01-A02-B03","A0001-01-01-A02-B04","A0001-01-01-A02-B05","A0001-01-01-A02-B06"]}
		if (!"".equals(code1)) {
			JSONObject newPositionJson = new JSONObject(newPosition);
			JSONArray newPositionArr = newPositionJson.getJSONArray("newPosition");
			
			JSONObject code1Json = new JSONObject(code1);
			JSONArray codeArr = code1Json.getJSONArray("code");
			// 有样本编号--即为样本移库
			int size = codeArr.length();
			for (int i = 0; i < size; i++) {
				// 位置
				String position = newPositionArr.getString(i);
				// 样本编号
				String code = codeArr.getString(i);
				// 通过样本编号查询左侧表数据
				List<SampleInItemTemp> list = containerDao
						.selSampleInTempByCode(code);
				// 建信息放入入库明细表中
				//通过样本编号查询样本入库表，改变样本入库内location
				List<SampleInItem> inItem = containerDao.selSampleInItemByCode(code);
				SampleInItem sii = new SampleInItem();
				if(inItem.size()>0){
					sii = inItem.get(0);
					sii.setLocation(position);
				}
//				sii.setSampleType(list.get(0).getSampleType());
//				sii.setSampleTypeId(list.get(0).getSampleTypeId());
//				sii.setConcentration(list.get(0).getConcentration());
//				sii.setVolume(list.get(0).getVolume());
//				sii.setSumTotal(list.get(0).getSumTotal());
//				sii.setTempId(list.get(0).getId());
//				sii.setState("1");
				
//				sii.setCode(code);
//				sii.setSampleCode(list.get(0).getSampleCode());
//				sii.setOutState("4");// 在库
				commonService.saveOrUpdate(sii);

				// 保存样本信息到样本主数据表
				SampleInfoIn sif = null;
				sif = this.sampleInDao.getSampleInItemByCode(sii.getCode());
				sif.setState("1");
				sif.setLocation(position);
				if (sii.getSampleCode() != null) {
					String[] sampleCode = sii.getSampleCode().split(",");
					for (int j = 0; j < sampleCode.length; j++) {
						SampleInfo sifi = commonDAO.findByProperty(
								SampleInfo.class, "code", sampleCode[j]).get(0);
						if (sifi.getCrmCustomer() != null) {
							sif.setCustomer(sifi.getCrmCustomer());
							sii.setCustomer(sifi.getCrmCustomer());
						}
						sif.setSampleInfo(sifi);
						sii.setSampleInfo(sifi);
					}
				}
				commonDAO.saveOrUpdate(sif);
			}

		} else {
			// 无样本编号---即为整个盒子移动位置
			// 通过旧位置查询所有该位置内样本
			List<SampleInItem> list = containerDao
					.selSampleInItemByOldLocation(oldPosition);
			for (int i = 0; i < list.size(); i++) {
				// 取出原来位置内的 盒子内位置 ，例 BX01-02-02-A01-B01; 即为B01
				String position = list.get(i).getLocation();
				String endLocation = position.substring(position.length() - 4,
						position.length());
				//修改样本入库明细表细信息
				SampleInItem sii = commonDAO.get(SampleInItem.class,list.get(i).getId());
				// 放入新的位置
				sii.setLocation(newPosition + endLocation);
				// 保存
				commonDAO.saveOrUpdate(sii);
				//修改样本主数据内位置
				SampleInfoIn sif = null;
				sif = this.sampleInDao.getSampleInItemByCode(sii.getCode());
				sif.setState("1");
				sif.setLocation(newPosition + endLocation);
				//将要移动的盒子放置到新位置上面
				StorageBox sb = commonDAO.get(StorageBox.class, boxId);
				sb.setNewLocationId(newPosition);
				// 保存
				commonDAO.saveOrUpdate(sb);
			}
		}
	}
	// 移库操作-血袋
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void moveSampleByBlood(String newPosition, String code1, User user) {
		
		// 有样本编号--即为样本移库
		
		String codeArr[] = code1.split(",");
		int size = codeArr.length;
		for (int i = 0; i < size; i++) {
			// 位置
			String position = newPosition;
			// 样本编号
			String code = codeArr[i];
			// 通过样本编号查询左侧表数据
			List<SampleInItemTemp> list = containerDao
					.selSampleInTempByCode(code);
			// 建信息放入入库明细表中
			SampleInItem sii = new SampleInItem();
			sii.setSampleType(list.get(0).getSampleType());
			sii.setSampleTypeId(list.get(0).getSampleTypeId());
			sii.setConcentration(list.get(0).getConcentration());
			sii.setVolume(list.get(0).getVolume());
			sii.setSumTotal(list.get(0).getSumTotal());
			sii.setTempId(list.get(0).getId());
			sii.setState("1");
			sii.setLocation(position);
			sii.setCode(code);
			sii.setSampleCode(list.get(0).getSampleCode());
			sii.setOutState("4");// 在库
			commonService.saveOrUpdate(sii);

			// 保存样本信息到样本主数据表
			SampleInfoIn sif = null;
			sif = this.sampleInDao.getSampleInItemByCode(sii.getCode());
			sif.setState("1");
			sif.setLocation(position);
			if (sii.getSampleCode() != null) {
				String[] sampleCode = sii.getSampleCode().split(",");
				for (int j = 0; j < sampleCode.length; j++) {
					SampleInfo sifi = commonDAO.findByProperty(
							SampleInfo.class, "code", sampleCode[j]).get(0);
					if (sifi.getCrmCustomer() != null) {
						sif.setCustomer(sifi.getCrmCustomer());
						sii.setCustomer(sifi.getCrmCustomer());
					}
					sif.setSampleInfo(sifi);
					sii.setSampleInfo(sifi);
				}
			}
			commonDAO.saveOrUpdate(sif);
		}

	}

	public List<SampleInfoIn> selectSampleByBox(String boxId) {
		return sampleInDao.findSampleByBox(boxId);
	}

	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleByBox(String code2, String position2,
			String boxId) {
		//通过boxId查询盒子
		StorageBox sb = commonDAO.get(StorageBox.class,boxId);
//		JSONObject code1Json = new JSONObject(code2);
//		JSONArray codeArr = code1Json.getJSONArray("code");
//		JSONObject position1Json = new JSONObject(position2);
//		JSONArray positionArr = code1Json.getJSONArray("position");
		String[] code1 = code2.split(",");
		String[] position1 = position2.split(",");
		int size = code1.length;
		
		for(int i=0;i<size;i++){
			String code = code1[i];
			String position = position1[i];
			//通过code查询SampleInfoIn表中是否已存在
			List<SampleInfoIn> list = sampleInDao.selectSampleInfoInByCode(code);
			if(list.size()>0){
				SampleInfoIn sii = list.get(0);
				sii.setStorageBox(sb);
				sii.setBoxLocation(position);
				commonDAO.saveOrUpdate(sii);
			}else{
				SampleInfoIn sii = new SampleInfoIn();
				//通过code查询待入库样本信息取值
				List<SampleInItemTemp> siit = sampleInDao.selectSampleInItemTempByCode(code);
				if(siit.size()>0){
					sii.setCode(code);
					sii.setBoxLocation(position);
					sii.setSampleType(siit.get(0).getSampleType());
					sii.setSampleTypeId(siit.get(0).getSampleTypeId());
					sii.setSampleCode(siit.get(0).getSampleCode());
					sii.setSampleInfo(siit.get(0).getSampleInfo());
					sii.setStorageBox(sb);
					sii.setState("1");
					sii.setSampleOrder(siit.get(0).getSampleOrder());
					commonDAO.saveOrUpdate(sii);
					
				}
				
			
			}
		}
	}

	public List<String> selectPicCodeBySampleTypeId(List<SampleInfoIn> list) {
		List<String> picCodes = new ArrayList<String>();
		for(int i=0;i<list.size();i++){
			String sampleTypeId = list.get(i).getSampleTypeId();
			if(sampleTypeId!=null && !"".equals(sampleTypeId)){
				//通过sampleTypeId查询picCode
				String picCode = sampleInDao.selectPicCode(sampleTypeId);
				picCodes.add(picCode);
			}else{
				picCodes.add("");
			}
		}
		return picCodes;
	}

	
}
