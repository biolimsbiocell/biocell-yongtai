package com.biolims.storage.position.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.sample.model.DicSampleType;
import com.biolims.storage.container.model.StorageContainer;
import com.biolims.storage.position.dao.StoragePositionDao;
import com.biolims.storage.position.model.StoragePosition;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.storage.dao.StorageBoxDao;
import com.biolims.system.storage.model.StorageBox;
import com.biolims.system.storage.model.StorageBoxItem;
import com.biolims.system.work.dao.WorkOrderDao;
import com.biolims.system.work.model.WorkOrderItem;
import com.biolims.util.JsonUtils;
import com.opensymphony.xwork2.ActionContext;

@Service
@Transactional(rollbackFor = Exception.class)
@SuppressWarnings({ "unchecked" })
public class StoragePositionService {

	@Resource
	private StoragePositionDao storagePositionDao;

	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private StorageBoxDao storageBoxDao;

	@Resource
	private CommonDAO commonDAO;
	
	@Resource
	private WorkOrderDao workOrderDao;
	

	StringBuffer json = new StringBuffer();

	/**
	 * 查询存储位置数据
	 * 
	 * @param sp
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findStoragePosition(int startNum, int limitNum,
			String dir, String sort) throws Exception {
		Map<String, Object> map = this.storagePositionDao
				.selectStoragePosition(startNum, limitNum, dir, sort);
		List<StoragePosition> list = (List<StoragePosition>) map.get("result");
		map.put("result", list);
		return map;
	}

	/**
	 * 根据ID查询存储位置
	 * 
	 * @return
	 * @throws Exception
	 */
	public StoragePosition findStoragePositionById(String id) throws Exception {
		return this.storagePositionDao.get(StoragePosition.class, id);
	}

	/**
	 * 保存存储位置数据
	 * 
	 * @param sp
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveOrModifyStoragePosition(StoragePosition pt, String info,
			String delItemsPosi) throws Exception {
		int level = 0;

		if (pt.getUpStoragePosition() != null
				&& pt.getUpStoragePosition().getId() != null
				&& !pt.getUpStoragePosition().getId().equals("")) {
			StoragePosition up = storagePositionDao.get(StoragePosition.class,
					pt.getUpStoragePosition().getId());
			level = Integer.valueOf(up.getLevel());
			level += 1;
		} else {
			level = 0;
		}
		if(pt.getLevel()==null){
			pt.setLevel(level);
			this.storagePositionDao.saveOrUpdate(pt);
		}
	}

	/**
	 * 删除旧的存储记录
	 * 
	 * @param id
	 *            要删除的位置ID
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void deleteOldPosition(String id) throws Exception {
		String pd[] = id.split("-");
		String posId = pd[0];
		for (int i = 1; i < (pd.length - 1); i++) {
			posId = posId + "-" + pd[i];
		}
		String posAdr = pd[pd.length - 1];
		StoragePosition sp = storagePositionDao.get(StoragePosition.class,
				posId);
		StorageContainer sc = sp.getStorageContainer();
		int m = sc.getRowNum();
		int n = sc.getColNum();
		int row = posAdr.substring(0, 1).hashCode() - 65;
		int col = Integer.parseInt(posAdr.substring(1)) - 1;
		if (!sp.getIsUse().equals("0")) {
			String[][] container = toArray(sp.getIsUse());
			String res = "";
			for (int i = 0; i < m; i++) {
				for (int j = 0; j < n; j++) {
					if (i == row && j == col) {
						res = res + "0" + ",";
					} else {
						res = res + container[i][j] + ",";
					}
				}
				res = res.substring(0, (res.length() - 1)) + ";";
			}
			sp.setIsUse(res);
			this.storagePositionDao.saveOrUpdate(sp);
		}
	}

	/**
	 * 设置位置存放记录
	 * 
	 * @param adr
	 *            存储位置(存储位置ID-格子编号)
	 * @param content
	 *            存放的物品ID
	 * @param type
	 *            操作类型：材料入库（0）、材料主数据(1)、材料归还(2)、动物入库(3)、动物主数据(4)、动物受体鼠合笼(5)、
	 *            动物供体鼠合笼(6)、嵌合鼠合笼(7)、冷冻保种(8)、组织冻存(9)
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public boolean setPositionContent(String adr, String content, String type)
			throws Exception {
		String pd[] = adr.split("-");
		String posId = pd[0];
		for (int i = 1; i < (pd.length - 1); i++) {
			posId = posId + "-" + pd[i];
		}
		String posAdr = pd[pd.length - 1];

		StoragePosition sp = storagePositionDao.get(StoragePosition.class,
				posId);
		// 查看原来的存放位置，如有，则去掉存储记录
		// String oldPosition =
		// storagePositionDao.findOldPositon(sp.getStorageType(), type,
		// content);
		// if (oldPosition != null && oldPosition.equals(adr)) {
		// return true;
		// } else if (oldPosition != null) {
		// deleteOldPosition(oldPosition);
		// }
		/*
		 * if(type.equals("1")){ String positionId = posId+"-"+posAdr;
		 * storagePositionDao
		 * .updatePosition(sp.getStorageType(),positionId,content); }
		 */
		String isUse = setContent(sp.getIsUse(), sp.getStorageContainer(),
				posAdr, content, type);
		if (!isUse.equals("0")) {
			// 转入新的位置
			sp.setIsUse(isUse);
			this.storagePositionDao.saveOrUpdate(sp);
		}
		return true;
	}

	/**
	 * 设置位置存放记录
	 * 
	 * @param adr
	 *            存储位置(存储位置ID-格子编号)
	 * @param oldPosition
	 *            旧的存放位置
	 * @param content
	 *            存放的物品ID
	 * @param type
	 *            操作类型：材料入库（0）、材料主数据(1)、材料归还(2)、动物入库(3)、动物主数据(4)、动物受体鼠合笼(5)、
	 *            动物供体鼠合笼(6)，嵌合鼠合笼(7)、冷冻保种(8)、组织冻存(9)
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public boolean setPositionContent(String adr, String oldPosition,
			String content, String type) throws Exception {
		String pd[] = adr.split("-");
		String posId = pd[0];
		for (int i = 1; i < (pd.length - 1); i++) {
			posId = posId + "-" + pd[i];
		}
		String posAdr = pd[pd.length - 1];

		StoragePosition sp = storagePositionDao.get(StoragePosition.class,
				posId);
		// 查看原来的存放位置，如有，则去掉存储记录
		if (oldPosition != null && oldPosition.equals(adr)) {
			return true;
		} else if (oldPosition != null) {
			deleteOldPosition(oldPosition);
		}
		String isUse = setContent(sp.getIsUse(), sp.getStorageContainer(),
				posAdr, content, type);
		if (!isUse.equals("0")) {
			// 转入新的位置
			sp.setIsUse(isUse);
			this.storagePositionDao.saveOrUpdate(sp);
		}
		return true;
	}

	/**
	 * 拼接存储位置上的值
	 * 
	 * @param oldValue
	 *            存储位置(isUse)上原有的值
	 * @param sc
	 *            存储位置上的存储容器
	 * @param adr
	 *            存储位置上具体的格子
	 * @param str
	 *            要存放的物品ID
	 * @param type
	 *            操作类型：材料入库（0）、材料主数据(1)、材料归还(2)、动物入库(3)、动物主数据(4)、动物受体鼠合笼(5)、
	 *            动物供体鼠合笼(6)，嵌合鼠合笼(7)、冷冻保种(8)、组织冻存(9)
	 * @return 存储位置(isUse)上新的值：类别号（入库（！）、主数据（#）、动物(*)）+物品ID
	 */
	public String setContent(String oldValue, StorageContainer sc, String adr,
			String str, String type) {
		String value = "0";
		int m = sc.getRowNum();
		int n = sc.getColNum();
		int row = adr.substring(0, 1).hashCode() - 65;
		int col = Integer.parseInt(adr.substring(1)) - 1;
		if (type.equals("0") || type.equals("2")) {
			str = "!" + str;
		} else if (type.equals("1")) {
			str = "#" + str;
		} else if (type.equals("3")) {
			str = "^" + str;
		} else if (type.equals("4")) {
			str = "*" + str;
		} else if (type.equals("5")) {
			str = "&" + str;
		} else if (type.equals("6")) {
			str = "$" + str;
		} else if (type.equals("7")) {
			str = "%" + str;
		} else if (type.equals("8")) {
			str = "(" + str;
		} else if (type.equals("9")) {
			str = ")" + str;
		}
		if (oldValue == null || oldValue.equals("0")) {
			String[][] container = new String[m][n];
			String res = "";
			for (int i = 0; i < m; i++) {
				for (int j = 0; j < n; j++) {
					if (i == row && j == col) {
						// container[i][j] = str;
						res = res + str + ",";
					} else {
						// container[i][j] = "0";
						res = res + "0" + ",";
					}
				}
				res = res.substring(0, (res.length() - 1)) + ";";
			}
			value = res;
		} else {
			String[][] container = toArray(oldValue);
			if (container[row][col].equals("0")) {
				container[row][col] = str;
				String res = "";
				for (int i = 0; i < m; i++) {
					for (int j = 0; j < n; j++) {
						res = res + container[i][j] + ",";
					}
					res = res.substring(0, (res.length() - 1)) + ";";
				}
				value = res;
			} else if (container[row][col].equals(str)) {
				value = oldValue;
			}
		}
		return value;
	}

	/**
	 * 将String类型二维数组转换为二维数组
	 * 
	 * @param str
	 * @return
	 */
	public static String[][] toArray(String str) {
		String[] split = str.split(";");
		String[][] array = new String[split.length][];
		for (int i = 0; i < split.length; i++) {
			array[i] = split[i].split(",");
		}
		return array;
	}

	/**
	 * 改变存储位置的状态
	 * 
	 * @param id
	 *            存储位置ID
	 * @param mainId
	 *            物品ID
	 * @return
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void chengeLocationState(String id, String mainId) throws Exception {
		String pd[] = id.split("-");
		String posId = pd[0];
		for (int i = 1; i < (pd.length - 1); i++) {
			posId = posId + "-" + pd[i];
		}
		String posAdr = pd[pd.length - 1];
		StoragePosition sp = storagePositionDao.get(StoragePosition.class,
				posId);
		StorageContainer sc = sp.getStorageContainer();
		int m = sc.getRowNum();
		int n = sc.getColNum();
		int row = posAdr.substring(0, 1).hashCode() - 65;
		int col = Integer.parseInt(posAdr.substring(1)) - 1;
		String res = "";
		String prefix = "";
		if (!sp.getStorageType().equals("3")) {
			prefix = "#";
		} else {
			prefix = "*";
		}
		if (!sp.getIsUse().equals("0")) {
			String[][] container = toArray(sp.getIsUse());
			for (int i = 0; i < m; i++) {
				for (int j = 0; j < n; j++) {
					if (i == row && j == col) {
						res = res + prefix + mainId + ",";
					} else {
						res = res + container[i][j] + ",";
					}
				}
				res = res.substring(0, (res.length() - 1)) + ";";
			}
			sp.setIsUse(res);
			this.storagePositionDao.saveOrUpdate(sp);
		} else {
			for (int i = 0; i < m; i++) {
				for (int j = 0; j < n; j++) {
					if (i == row && j == col) {
						res = res + prefix + mainId + ",";
					} else {
						res = res + "0" + ",";
					}
				}
				res = res.substring(0, (res.length() - 1)) + ";";
			}
			sp.setIsUse(res);
			this.storagePositionDao.saveOrUpdate(sp);
		}
	}

	/**
	 * 
	 * 查询任务明细列表
	 * 
	 * 
	 */
	public List<StoragePosition> findStoragePositionList(String type)
			throws Exception {
		String hql = "from StoragePosition where state.id ='1p' and level = 0  ";
		String key = "";
		// if (type != null && !type.equals("")) {
		// key = key + " and storageType = '" + type + "'";
		// }
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		key = key + " order by id asc";
		List<StoragePosition> list = storagePositionDao.find(hql + key);
		return list;
	}

	public StoragePosition getUnUseStoragePositionListById(String id)
			throws Exception {

		StoragePosition sp = findStoragePositionById(id);
		StoragePosition sp1 = null;
		StoragePosition usp = null;
		if (sp != null) {

			if (sp.getUpStoragePosition() != null)
				usp = sp.getUpStoragePosition();

			String sql = " from StoragePosition where state.id ='1p' and leaf = 'true'  and id like '"
					+ id
					+ "-%' and id not in(select location from SampleBloodInfo where location is not null) ";

			sql += " order by id asc";
			List<StoragePosition> list = storagePositionDao.find(sql);

			if (list.size() > 0)
				sp1 = (StoragePosition) list.get(0);
		}
		return sp1;
	}

	public StoragePosition getUnUseStorageCqPositionListById(String id)
			throws Exception {

		StoragePosition sp = findStoragePositionById(id);
		StoragePosition sp1 = null;
		StoragePosition usp = null;
		if (sp != null) {

			if (sp.getUpStoragePosition() != null)
				usp = sp.getUpStoragePosition();

			String sql = " from StoragePosition where state.id ='1p' and leaf = 'true'  and id like '"
					+ id
					+ "-%'  and id not in(select cqlocation from SampleBloodInfo where cqlocation is not null) ";
			if (usp != null) {

				sql += " and  id like '" + usp.getId() + "-%' ";
			}
			sql += " order by id asc";
			List<StoragePosition> list = storagePositionDao.find(sql);

			if (list.size() > 0)
				sp1 = (StoragePosition) list.get(0);
		}
		return sp1;
	}

	public StoragePosition getUnUseStoragePositionListById2100(String id)
			throws Exception {

		StoragePosition sp = findStoragePositionById(id);
		StoragePosition sp1 = null;
		StoragePosition usp = null;
		if (sp != null) {

			if (sp.getUpStoragePosition() != null)
				usp = sp.getUpStoragePosition();

			String sql = " from StoragePosition where state.id ='1p' and leaf = 'true' and id like '"
					+ id
					+ "-%'  and id not in(select location from SampleDNAInfo where location is not null) and id not in (select dnalocation from SampleBloodInfo where dnalocation is not null) ";
			if (usp != null) {

				sql += " and  id like '" + usp.getId() + "-%' ";
			}
			sql += " order by id asc";
			List<StoragePosition> list = storagePositionDao.find(sql);

			if (list.size() > 0)
				sp1 = (StoragePosition) list.get(0);
		}
		return sp1;
	}

	public StoragePosition getUnUseStoragePositionListByDnaId(String id)
			throws Exception {

		StoragePosition sp = findStoragePositionById(id);
		StoragePosition sp1 = null;
		StoragePosition usp = null;
		if (sp != null) {

			if (sp.getUpStoragePosition() != null)
				usp = sp.getUpStoragePosition();

			String sql = " from StoragePosition where state.id ='1p' and leaf = 'true'  and id like '"
					+ id
					+ "-%'  and id not in(select location from SampleDNAInfo where location is not null) and id not in (select dnalocation from SampleBloodInfo where dnalocation is not null)";
			if (usp != null) {

				sql += " and  id  like '" + usp.getId() + "-%' ";
			}
			sql += " order by id asc";
			List<StoragePosition> list = storagePositionDao.find(sql);

			if (list.size() > 0)
				sp1 = (StoragePosition) list.get(0);
		}
		return sp1;
	}

	public StoragePosition getUnUseStoragePositionListByWkId(String id)
			throws Exception {

		StoragePosition sp = findStoragePositionById(id);
		StoragePosition sp1 = null;
		StoragePosition usp = null;
		if (sp != null) {

			if (sp.getUpStoragePosition() != null)
				usp = sp.getUpStoragePosition();

			String sql = " from StoragePosition where state.id ='1p' and leaf = 'true'  and id like '"
					+ id
					+ "-%'  and id not in(select location from SampleWKInfo where location is not null) ";
			if (usp != null) {

				sql += " and  id like '" + usp.getId() + "-%' ";
			}
			sql += " order by id asc";
			List<StoragePosition> list = storagePositionDao.find(sql);

			if (list.size() > 0)
				sp1 = (StoragePosition) list.get(0);
		}
		return sp1;
	}

	public StoragePosition getUnUseStorageCqPositionListByWkId(String id)
			throws Exception {

		StoragePosition sp = findStoragePositionById(id);
		StoragePosition sp1 = null;
		StoragePosition usp = null;
		if (sp != null) {

			if (sp.getUpStoragePosition() != null)
				usp = sp.getUpStoragePosition();

			String sql = " from StoragePosition where state.id ='1p' and leaf = 'true'  and id like '"
					+ id
					+ "-%'  and id not in(select pdLocation from SampleWKTaskItem where pdLocation is not null) ";
			if (usp != null) {

				sql += " and  id like '" + usp.getId() + "-%' ";
			}
			sql += " order by id asc";
			List<StoragePosition> list = storagePositionDao.find(sql);

			if (list.size() > 0)
				sp1 = (StoragePosition) list.get(0);
		}
		return sp1;
	}

	public List<StoragePosition> findStoragePositionList(String upId,
			String type) throws Exception {
		String hql = " from StoragePosition where state.id ='1p' ";
		String key = "";
		// if (type != null && !type.equals("")) {
		// key = key + " and storageType = '" + type + "'";
		// }
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		List<StoragePosition> list = null;
		if (upId.equals("0")) {
			key = key + "and level = 0 order by id asc";
			list = storagePositionDao.find(hql + key);
		} else {
			key = key + " and upStoragePosition.id='" + upId
					+ "' order by id asc";
			list = storagePositionDao.find(hql + key);
		}
		return list;
	}

	public List<StoragePosition> findNewStoragePositionList(String upId ,String type)
			throws Exception {
		String hql = " from StoragePosition where state.id ='1p' ";
		String key = "";
		String scopeId = (String) ActionContext.getContext().getSession()
				.get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		if(type!=null&&!"".equals(type)) {
			key += " and positionType='"+type+"'";
		}
		List<StoragePosition> list = null;
		if (upId.equals("0")) {
			key = key + "and level = 0 order by id asc";
			list = storagePositionDao.find(hql + key);
		} else {
			key = key + " and upStoragePosition.id='" + upId
					+ "' order by id asc";
			list = storagePositionDao.find(hql + key);
		}
		return list;
	}

	/**
	 * 构建Json文件
	 * 
	 * @param list
	 * @param node
	 */

	public void constructorJson(List<StoragePosition> list,
			StoragePosition treeNode) throws Exception {
		// if (hasChild(list, treeNode)) {
		// json.append("{\"id\":'");
		// json.append(treeNode.getId() + "'");
		// json.append(",");
		// json.append("\"scName\":'");
		// json.append(treeNode.getStorageContainer() != null ?
		// treeNode.getStorageContainer().getName() : "");
		// json.append("',");
		// json.append("\"scId\":'");
		// json.append(treeNode.getStorageContainer() != null ?
		// treeNode.getStorageContainer().getId() : "");
		// json.append("',");
		// json.append("\"name\":'");
		// json.append(JsonUtils.formatStr(treeNode.getName() == null ? "" :
		// treeNode.getName()) + "");
		// json.append("',\"typeSysCode\":'");
		// if (treeNode.getType() != null)
		// if (treeNode.getType().getSysCode() != null)
		// json.append(JsonUtils.formatStr(treeNode.getType().getSysCode()) +
		// "");
		// json.append("',\"type-name\":'");
		// json.append(JsonUtils.formatStr(treeNode.getType() == null ? "" :
		// treeNode.getType().getName()) + "");
		// json.append("',\"state-name\":'");
		// json.append((treeNode.getState() == null ? "" :
		// treeNode.getState().getName()) + "");
		//
		// json.append("',\"stateId\":'");
		// json.append((treeNode.getState() == null ? "" :
		// treeNode.getState().getId()) + "");
		//
		// json.append("',\"leaf\":false");
		//
		// json.append(",\"upId\":'");
		// json.append((treeNode.getUpStoragePosition() == null ? "" :
		// treeNode.getUpStoragePosition().getId()) + "");
		//
		// json.append("',\"isUseStr\":'");
		//
		// json.append("'");
		// json.append(",\"storageObj\":'");
		// json.append("'");
		// json.append(",\"children\":[");
		// List<StoragePosition> childList = getChildList(list, treeNode);
		// Iterator<StoragePosition> iterator = childList.iterator();
		// while (iterator.hasNext()) {
		// StoragePosition node = (StoragePosition) iterator.next();
		// constructorJson(list, node);
		// }
		// json.append("]");
		// json.append("},");
		// } else {
		json.append("{\"id\":'");
		json.append(treeNode.getId() + "'");
		json.append(",");
		try {
			json.append("\"scName\":'");
			json.append(treeNode.getStorageContainer() != null ? treeNode
					.getStorageContainer().getName() : "");
			json.append("',");
			json.append("\"scId\":'");
			json.append(treeNode.getStorageContainer() != null ? treeNode
					.getStorageContainer().getId() : "");
			json.append("',");
			json.append("\"colNum\":'");
			json.append(treeNode.getStorageContainer() != null ? treeNode
					.getStorageContainer().getColNum() : "");
			json.append("',");
			json.append("\"rowNum\":'");
			json.append(treeNode.getStorageContainer() != null ? treeNode
					.getStorageContainer().getRowNum() : "");
			json.append("',");
		} catch (Exception e) {
			e.printStackTrace();
		}
		json.append("\"name\":'");
		json.append(JsonUtils.formatStr(treeNode.getName() == null ? ""
				: treeNode.getName()) + "");
		json.append("',\"typeSysCode\":'");
		if (treeNode.getType() != null)
			if (treeNode.getType().getSysCode() != null)
				json.append(JsonUtils
						.formatStr(treeNode.getType().getSysCode()) + "");
		json.append("',\"type-name\":'");
		json.append(JsonUtils.formatStr(treeNode.getType() == null ? ""
				: treeNode.getType().getName()) + "");
		json.append("',\"stateId\":'");
		json.append((treeNode.getState() == null ? "" : treeNode.getState()
				.getId()) + "");
		json.append("',\"state-name\":'");
		json.append((treeNode.getState() == null ? "" : treeNode.getState()
				.getName()) + "");

		if (hasChildCount(treeNode)) {

			json.append("',\"leaf\":false");
		} else {

			json.append("',\"leaf\":true");
			json.append(",\"storageObj\":'");

			json.append((treeNode.getIsUse().equals("0") ? ""
					: showStorageNum(treeNode.getIsUse())) + "");

			json.append("'");

		}

		json.append(",\"upId\":'");
		json.append((treeNode.getUpStoragePosition() == null ? "" : treeNode
				.getUpStoragePosition().getId()) + "");
		json.append("',\"isUseStr\":'");
		json.append("'");
		json.append("},");
		// }
	}

	/**
	 * 获得子节点列表信息
	 * 
	 * @param list
	 * @param node
	 * @return
	 */
	public List<StoragePosition> getChildList(List<StoragePosition> list,
			StoragePosition node) throws Exception { // 得到子节点列表
		List<StoragePosition> li = new ArrayList<StoragePosition>();
		Iterator<StoragePosition> it = list.iterator();
		while (it.hasNext()) {
			StoragePosition n = (StoragePosition) it.next();
			if (n.getUpStoragePosition() != null
					&& n.getUpStoragePosition().getId().equals(node.getId())) {
				li.add(n);
			}
		}
		return li;
	}

	/**
	 * 判断是否有子节点
	 * 
	 * @param list
	 * @param node
	 * @return
	 */
	public boolean hasChild(List<StoragePosition> list, StoragePosition node)
			throws Exception {
		return getChildList(list, node).size() > 0 ? true : false;
	}

	public boolean hasChildCount(StoragePosition node) throws Exception {

		Long c = storagePositionDao
				.getCount("from StoragePosition where state.id ='1p' and upStoragePosition.id='"
						+ node.getId() + "'");
		return c > 0 ? true : false;
	}

	public String getJson(List<StoragePosition> list) throws Exception {
		if (list != null && list.size() > 0) {
		}
		json = new StringBuffer();
		List<StoragePosition> nodeList0 = new ArrayList<StoragePosition>();
		Iterator<StoragePosition> it1 = list.iterator();
		while (it1.hasNext()) {
			StoragePosition node = (StoragePosition) it1.next();
			// if (node.getLevel() == 0) {
			nodeList0.add(node);
			// }
		}
		Iterator<StoragePosition> it = nodeList0.iterator();
		while (it.hasNext()) {
			StoragePosition node = (StoragePosition) it.next();
			constructorJson(list, node);
		}
		String jsonDate = json.toString();
		return ("[" + jsonDate + "]").replaceAll(",]", "]");
	}

	/**
	 * 查询str位置上细胞、质粒、BAC的库存个数
	 * 
	 * @param str
	 *            位置ID
	 * @return 入库单占用个数/主数据占用个数/被占用总个数
	 */
	public String showStorageNum(String str) {
		String result = "0/0/0";
		int inNum = 0;
		int mainNum = 0;
		int allNum = 0;
		if (!str.equals("0")) {
			String[][] container = toArray(str);
			for (int i = 0; i < container.length; i++) {
				for (int j = 0; j < container[0].length; j++) {
					if (!container[i][j].equals("0")) {
						String mark = container[i][j].substring(0, 1);
						if (mark.equals("!")) {
							inNum++;
						} else if (mark.equals("#")) {
							mainNum++;
						}
					}
				}
			}
			allNum = inNum + mainNum;
			result = mainNum + "/" + inNum + "/" + allNum;
		}
		return result;
	}

	/**
	 * 查询str位置上动物的库存个数
	 * 
	 * @param str
	 *            位置ID
	 * @return 动物占用总个数
	 */
	public String showAnimalStorageNum(String str) {
		String result = "0";
		int allNum = 0;
		if (!str.equals("0")) {
			String[][] container = toArray(str);
			for (int i = 0; i < container.length; i++) {
				for (int j = 0; j < container[0].length; j++) {
					if (!container[i][j].equals("0")) {
						allNum++;
					}
				}
			}
			result = allNum + "";
		}
		return result;
	}

	/**
	 * 自动生成存储位置sp下级存储位置
	 * 
	 * @param sp
	 *            存储位置
	 * @param user2 
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void autoStoragePositionInfo(StoragePosition sp, User user2) throws Exception {
		List<StoragePosition> saveItems = new ArrayList<StoragePosition>();
		int rowNum = Integer.parseInt(sp.getRowNum());
		int colNum = Integer.parseInt(sp.getColNum());

		for (int i = 0; i < rowNum; i++) {
			String id1 = sp.getId();
			// + "-" + (i + 1);

			if ((i + 1) >= 10)
				id1 = id1 + "-" + (i + 1);
			else
				id1 = id1 + "-" + "0" + (i + 1);

			String name1 = sp.getId() + "-" + (i + 1) + sp.getRowName();
			StoragePosition newsp1 = this.storagePositionDao.get(
					StoragePosition.class, id1);
			if (newsp1 == null || newsp1.equals("")) {
				StoragePosition newsp = new StoragePosition();
				newsp.setId(id1);
				newsp.setIsUse("0");
				newsp.setName(name1);
				newsp.setType(sp.getType());
				newsp.setColNum(sp.getColNum());
				newsp.setColName(sp.getColName());
				newsp.setRowName(sp.getRowName());
				newsp.setRowNum(sp.getRowNum());
				newsp.setScopeId(sp.getScopeId());
				newsp.setScopeName(sp.getScopeName());
				newsp.setObjectId(sp.getId());
				newsp.setStorageContainer(sp.getStorageContainer());
				newsp.setSubStorageContainer(sp.getSubStorageContainer());
				newsp.setUpStoragePosition(sp);
				newsp.setDepartment(sp.getDepartment());
				newsp.setStorageType(sp.getStorageType()); // 上一级存储什么类型，这一级也存储什么类型
				newsp.setLevel(1);
				newsp.setState(this.storagePositionDao.findDicState());
				newsp.setLeaf("0");
				newsp.setUpId(sp.getId());
				this.storagePositionDao.saveOrUpdate(newsp);

				for (int j = 0; j < colNum; j++) {
					String id2 = sp.getId();
					// + "-" + (i + 1) + "-" + (j + 1);
					if ((i + 1) >= 10)
						id2 = id2 + "-" + (i + 1);
					else
						id2 = id2 + "-" + "0" + (i + 1);

					if ((j + 1) >= 10)
						id2 = id2 + "-" + (j + 1);
					else
						id2 = id2 + "-" + "0" + (j + 1);

					StoragePosition newsp2 = this.storagePositionDao.get(
							StoragePosition.class, id2);
					if (newsp2 == null || newsp2.equals("")) {
						StoragePosition newsp3 = new StoragePosition();
						newsp3.setId(id2);
						newsp3.setIsUse("0");
						String name2 = sp.getId() + "-" + (i + 1)
								+ sp.getRowName() + "-" + (j + 1)
								+ sp.getColName();
						newsp3.setName(name2);
						newsp3.setType(sp.getType());
						newsp3.setObjectId(sp.getId());
						// 创建盒子
						if (sp.getSubStorageContainer() != null && !"".equals(sp.getSubStorageContainer())) {
							StorageContainer upSc = null;
							if (sp.getStorageContainer() != null) {
								upSc = commonDAO.get(StorageContainer.class, sp
										.getStorageContainer().getId());
							}

							for (int m = 0; m < upSc.getRowNum(); m++) {
								for (int k = 0; k <= upSc.getColNum(); k++) {
									String hzId = "";
									if (k < 10) {
										hzId = id2
												+ "-"
												+ String.valueOf((char) (65 + m))
												+ "0" + String.valueOf(k);
									} else {
										hzId = id2
												+ "-"
												+ String.valueOf((char) (65 + m))
												+ String.valueOf(k);
									}
									// 判断架子上是否有盒子-没有-创建盒子
									if (this.storageBoxDao.findIsUse(hzId) == 0) {
										StorageBox sbi = new StorageBox();
										sbi.setStorageContainer(sp
												.getSubStorageContainer());
										String modelName = "StorageBox";
										Date date = new Date();
										DateFormat format = new SimpleDateFormat(
												"yy");
										String stime = format.format(date);
										String markCode = "B" + stime;
										String autoID = codingRuleService.get(
												modelName, markCode, 00000, 5,
												null, "id");
										sbi.setId(autoID);
										sbi.setName(autoID);
										sbi.setNewLocationId(hzId);
										sbi.setOldLocationId(hzId);
										sbi.setLocationId(hzId);
										sbi.setState("1");
										this.storagePositionDao
												.saveOrUpdate(sbi);
										//修改盒子迁移记录
										StorageBoxItem sbi1 = new StorageBoxItem();
										SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
										Date d = new Date();
										sbi1.setChangeDate(sdf.parse(sdf.format(d)));
										sbi1.setChangeUser(user2.getName());
										sbi1.setLocationId(newsp.getId());
										sbi1.setLocationName(newsp.getName());
										sbi1.setStorageBox(sbi);
										sbi1.setState("1");
										storagePositionDao.saveOrUpdate(sbi);
										
									}
								}
							}
						}
						newsp3.setStorageContainer(sp.getStorageContainer());
						newsp3.setSubStorageContainer(sp
								.getSubStorageContainer());
						newsp3.setColNum(sp.getColNum());
						newsp3.setColName(sp.getColName());
						newsp3.setRowName(sp.getRowName());
						newsp3.setRowNum(sp.getRowNum());
						newsp3.setScopeId(sp.getScopeId());
						newsp3.setScopeName(sp.getScopeName());
						newsp3.setUpStoragePosition(newsp);
						newsp3.setDepartment(sp.getDepartment());
						newsp3.setStorageType(sp.getStorageType()); // 上一级存储什么类型，这一级也存储什么类型
						newsp3.setLevel(2);
						newsp3.setState(this.storagePositionDao.findDicState());
						newsp3.setLeaf("1");
						newsp3.setUpId(id1);
						this.storagePositionDao.saveOrUpdate(newsp3);
					}
				}
			}

		}
	}

	// }

	public void autoStoragePositionInfo1(StoragePosition sp) throws Exception {
		List<StoragePosition> list = this.storagePositionDao.findName();
		for (int m = 0; m < list.size(); m++) {
			sp = list.get(m);

			int rowNum = Integer.parseInt(sp.getRowNum());
			int colNum = Integer.parseInt(sp.getColNum());
			for (int i = 0; i < rowNum; i++) {
				String id1 = sp.getId();
				// + "-" + (i + 1);

				if ((i + 1) >= 10)
					id1 = id1 + "-" + (i + 1);
				else
					id1 = id1 + "-" + "0" + (i + 1);

				String name1 = sp.getId() + "-" + (i + 1) + sp.getRowName();
				StoragePosition newsp1 = this.storagePositionDao.get(
						StoragePosition.class, id1);
				if (newsp1 == null || newsp1.equals("")) {
					StoragePosition newsp = new StoragePosition();
					newsp.setId(id1);
					newsp.setIsUse("0");
					newsp.setName(name1);
					newsp.setType(sp.getType());
					newsp.setObjectId(sp.getId());
					newsp.setUpStoragePosition(sp);
					newsp.setDepartment(sp.getDepartment());
					// newsp.setStorageType(sp.getStorageType()); //
					// 上一级存储什么类型，这一级也存储什么类型
					newsp.setLevel(1);
					newsp.setState(this.storagePositionDao.findDicState());
					newsp.setLeaf("0");
					newsp.setUpId(sp.getId());
					this.storagePositionDao.saveOrUpdate(newsp);

					for (int j = 0; j < colNum; j++) {
						String id2 = list.get(m).getId();
						// + "-" + (i + 1) + "-" + (j + 1);
						if ((i + 1) >= 10)
							id2 = id2 + "-" + (i + 1);
						else
							id2 = id2 + "-" + "0" + (i + 1);

						if ((j + 1) >= 10)
							id2 = id2 + "-" + (j + 1);
						else
							id2 = id2 + "-" + "0" + (j + 1);

						StoragePosition newsp2 = this.storagePositionDao.get(
								StoragePosition.class, id2);
						if (newsp2 == null || newsp2.equals("")) {
							StoragePosition newsp3 = new StoragePosition();
							newsp3.setId(id2);
							newsp3.setIsUse("0");
							String name2 = sp.getId() + "-" + (i + 1)
									+ sp.getRowName() + "-" + (j + 1)
									+ sp.getColName();
							newsp3.setName(name2);
							newsp3.setType(sp.getType());
							newsp3.setObjectId(sp.getId());
							newsp3.setUpStoragePosition(newsp);
							newsp3.setDepartment(sp.getDepartment());
							// newsp3.setStorageType(sp.getStorageType()); //
							// 上一级存储什么类型，这一级也存储什么类型
							newsp3.setLevel(2);
							newsp3.setState(this.storagePositionDao
									.findDicState());
							newsp3.setLeaf("1");
							newsp3.setUpId(id1);
							this.storagePositionDao.saveOrUpdate(newsp3);
						}
					}
				}

			}
		}

	}

	public StoragePosition get(String id) {
		return commonDAO.get(StoragePosition.class, id);
	}

	public void selStorageContainerBox(StoragePosition storagePosition, User user) throws Exception {

		//位置id bx01-01  或 bx01-01-01
		String id2 = storagePosition.getId();
		int n = id2.length()-id2.replaceAll("-", "").length();
		if(n==1){
			//通过ID查找该层冰箱共几排
			List<StoragePosition> list = storagePositionDao.selStoragePositionById(id2);
			for(int y=0;y<list.size();y++){
				String id = list.get(y).getId();
				// 创建盒子  若id为bx01-01-01则创建盒子
				if (storagePosition.getStorageContainer() != null) {
					StorageContainer upSc = null;
					if(storagePosition.getStorageContainer()!=null){
						upSc = commonDAO.get(StorageContainer.class, storagePosition.getStorageContainer().getId());
					}
					for (int m = 0; m < upSc.getRowNum(); m++) {
						for (int k = 0; k <= upSc.getColNum(); k++) {
							String hzId = "";
							if (k < 10) {
								hzId = id + "-" + String.valueOf((char) (65 + m))
										+ "0" + String.valueOf(k);
							} else {
								hzId = id + "-" + String.valueOf((char) (65 + m))
										+ String.valueOf(k);
							}
							if(storagePosition.getSubStorageContainer()!=null && !"".equals(storagePosition.getSubStorageContainer())){

								// 判断架子上是否有盒子-没有-创建盒子
								if (this.storageBoxDao.findIsUse(hzId) == 0) {
									StorageBox sbi = new StorageBox();
									StorageContainer sc = commonDAO.get(StorageContainer.class, storagePosition.getSubStorageContainer().getId());
									sbi.setStorageContainer(sc);
									String modelName = "StorageBox";
									Date date = new Date();
									DateFormat format = new SimpleDateFormat("yy");
									String stime = format.format(date);
									String markCode = "B" + stime;
									String autoID = codingRuleService.get(modelName,
											markCode, 00000, 5, null, "id");
									sbi.setId(autoID);
									sbi.setName(autoID);
									sbi.setNewLocationId(hzId);
									sbi.setOldLocationId(hzId);
									sbi.setLocationId(hzId);
									sbi.setState("1");
									this.storagePositionDao.saveOrUpdate(sbi);
									
									//修改盒子迁移记录
									StorageBoxItem sbi1 = new StorageBoxItem();
									SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
									Date d = new Date();
									sbi1.setChangeDate(sdf.parse(sdf.format(d)));
									sbi1.setChangeUser(user.getName());
									sbi1.setLocationId(hzId);
									sbi1.setLocationName(hzId);
									sbi1.setStorageBox(sbi);
									sbi1.setState("1");
									storagePositionDao.saveOrUpdate(sbi);
									
								}
							}
							
						}
					}
				}
			}
		}else if(n==2){
			// 创建盒子  若id为bx01-01-01则创建盒子
			if (storagePosition.getStorageContainer() != null) {
				StorageContainer upSc = null;
				if(storagePosition.getStorageContainer()!=null){
					upSc = commonDAO.get(StorageContainer.class, storagePosition.getStorageContainer().getId());
				}
				for (int m = 0; m < upSc.getRowNum(); m++) {
					for (int k = 0; k <= upSc.getColNum(); k++) {
						String hzId = "";
						if (k < 10) {
							hzId = id2 + "-" + String.valueOf((char) (65 + m))
									+ "0" + String.valueOf(k);
						} else {
							hzId = id2 + "-" + String.valueOf((char) (65 + m))
									+ String.valueOf(k);
						}
						
						if(storagePosition.getSubStorageContainer()!=null && !"".equals(storagePosition.getSubStorageContainer())){
							// 判断架子上是否有盒子-没有-创建盒子
							if (this.storageBoxDao.findIsUse(hzId) == 0) {
								StorageContainer sc = commonDAO.get(StorageContainer.class, storagePosition.getSubStorageContainer().getId());
								StorageBox sbi = new StorageBox();
								sbi.setStorageContainer(sc);
								String modelName = "StorageBox";
								Date date = new Date();
								DateFormat format = new SimpleDateFormat("yy");
								String stime = format.format(date);
								String markCode = "B" + stime;
								String autoID = codingRuleService.get(modelName,
										markCode, 00000, 5, null, "id");
								sbi.setId(autoID);
								sbi.setName(autoID);
								sbi.setNewLocationId(hzId);
								sbi.setOldLocationId(hzId);
								sbi.setLocationId(hzId);
								sbi.setState("1");
								this.storagePositionDao.saveOrUpdate(sbi);
								//修改盒子迁移记录
								StorageBoxItem sbi1 = new StorageBoxItem();
								SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
								Date d = new Date();
								sbi1.setChangeDate(sdf.parse(sdf.format(d)));
								sbi1.setChangeUser(user.getName());
								sbi1.setLocationId(hzId);
								sbi1.setLocationName(hzId);
								sbi1.setStorageBox(sbi);
								sbi1.setState("1");
								storagePositionDao.saveOrUpdate(sbi);
							}
						}
					
					}
				}
			}
		}


	}

	public List<NextFlow> selNextFlowByWorkOrderItem(List<WorkOrderItem> list, String sampleType) throws Exception {
	List<NextFlow> list_nextFlow = new ArrayList<NextFlow>();
	List<String> nfId = new ArrayList<String>();
	for (WorkOrderItem w : list) {
		String nextFlowJson = w.getNextStepId();
		List<Map<String, Object>> nlist = JsonUtils.toListByJsonArray(
				nextFlowJson, List.class);
		for (Map<String, Object> map : nlist) {
			String nextFlowId = (String) map.get("NextFlowId");
			if (nextFlowId != null && !nextFlowId.equals("")) {
				String[] ids = nextFlowId.split(",");
				for (int i = 0; i < ids.length; i++) {
					List<NextFlow> nextFlow = workOrderDao
							.selectNextFlowNameById(ids[i]);
					for (NextFlow n : nextFlow) {
						if (!nfId.contains(n.getId())) {
							NextFlow nf = new NextFlow();
							nf.setId(n.getId());
							nf.setName(n.getName());
							nf.setApplicationTypeTable(n
									.getApplicationTypeTable());
							nf.setCreateUser(n.getCreateUser());
							nf.setCreateDate(n.getCreateDate());
							list_nextFlow.add(nf);
							nfId.add(n.getId());
						}
					}
				}
			} else {
				if (sampleType != null && !"".equals(sampleType)&&!"null".equals(sampleType)) {
					DicSampleType dst = commonDAO.get(DicSampleType.class,
							sampleType);
					String typeId = dst.getType().getId();
					if (map.containsKey(typeId)) {
						String typeFlow = (String) map.get(typeId);
						String[] ids = typeFlow.split(",");
						for (int i = 0; i < ids.length; i++) {
							List<NextFlow> nextFlow = workOrderDao
									.selectNextFlowNameById(ids[i]);
							for (NextFlow n : nextFlow) {
								if (!nfId.contains(n.getId())) {
									NextFlow nf = new NextFlow();
									nf.setId(n.getId());
									nf.setName(n.getName());
									nf.setApplicationTypeTable(n
											.getApplicationTypeTable());
									nf.setCreateUser(n.getCreateUser());
									nf.setCreateDate(n.getCreateDate());
									list_nextFlow.add(nf);
									nfId.add(n.getId());
								}
							}
						}
					}
				}

			}
		}

	}
	return list_nextFlow;
	}

	public Map<String, Object> findStoragePositionDialogList(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return storagePositionDao.findStoragePositionDialogList(start, length, query, col,
				sort);
	}
}
