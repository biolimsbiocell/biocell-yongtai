package com.biolims.storage.position.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.storage.container.dao.ContainerDao;
import com.biolims.storage.position.dao.StorageTransferDao;
import com.biolims.storage.position.model.StorageTransfer;
import com.biolims.storage.position.model.StorageTransferItem;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class StorageTransferService {
	@Resource
	private StorageTransferDao storageTransferDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private ContainerDao containerDao;
	@Resource
	private StoragePositionService storagePositionService;

	StringBuffer json = new StringBuffer();

	public Map<String, Object> findStorageTransferList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return storageTransferDao.selectStorageTransferList(mapForQuery, startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(StorageTransfer i) throws Exception {

		storageTransferDao.saveOrUpdate(i);

	}

	public StorageTransfer get(String id) {
		StorageTransfer storageTransfer = commonDAO.get(StorageTransfer.class, id);
		return storageTransfer;
	}

	public Map<String, Object> findStorageTransferItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = storageTransferDao.selectStorageTransferItemList(scId, startNum, limitNum, dir,
				sort);
		List<StorageTransferItem> list = (List<StorageTransferItem>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveStorageTransferItem(StorageTransfer sc, String itemDataJson) throws Exception {
		List<StorageTransferItem> saveItems = new ArrayList<StorageTransferItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			StorageTransferItem scp = new StorageTransferItem();
			// 将map信息读入实体类
			scp = (StorageTransferItem) storageTransferDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setStorageTransfer(sc);

			saveItems.add(scp);
		}
		storageTransferDao.saveOrUpdateAll(saveItems);
	}

	//	//审批完成
	//	@Transactional(rollbackFor = Exception.class)
	//	public void changeState(String applicationTypeActionId, String id) {
	//		StorageTransfer sct = storageTransferDao.get(StorageTransfer.class, id);
	//		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
	//		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
	//
	//		User user = (User) ServletActionContext.getRequest().getSession()
	//				.getAttribute(SystemConstants.USER_SESSION_KEY);
	//		sct.setConfirmUser(user);
	//		sct.setConfirmDate(new Date());
	//
	//		storageTransferDao.update(sct);
	//
	//		try {
	//			String type = sct.getType();
	//			if (type.equals("3")) {
	//				chengePosition(id, "4");
	//			} else {
	//				chengePosition(id, "1");
	//			}
	//
	//		} catch (Exception e) {
	//			e.printStackTrace();
	//		}
	//	}

	/**
	 * 进行冻存管理中的移位操作
	 * @param id 移位子表ID
	 * @param type 操作类型：材料入库（0）、材料主数据(1)、材料归还(2)、动物入库(3)、动物主数据(4)、动物自然合笼(5)
	 * @throws Exception
	 */
	//	@Transactional(rollbackFor = Exception.class)
	//	public void chengePosition(String id, String type) throws Exception {
	//		StorageTransfer st = storageTransferDao.get(StorageTransfer.class, id);
	//		Map<String, Object> result = storageTransferDao.selectStorageTransferItemList(id, null, null, null, null);
	//		List<StorageTransferItem> list = (List<StorageTransferItem>) result.get("list");
	//		for (StorageTransferItem srai : list) {
	//			if (srai.getNewLocation() != null && !srai.getNewLocation().trim().equals(""))
	//				storagePositionService.setPositionContent(srai.getNewLocation(), srai.getCellId(), type);
	//		}
	//	}

	/**
	 * 批量分配存储位置
	 * @param ids 要存储的物品ID数组
	 * @param type 存放物品的类型
	 * @param pid 存放位置ID
	 * @return
	 * @throws Exception
	 */
	//	@Transactional(rollbackFor = Exception.class)
	//	public boolean batchLocation(String[] ids, String type, String pid) throws Exception {
	//		String[] str = pid.split("-");
	//		int num = str.length;
	//		for (int i = 0; i < ids.length; i++) {
	//			String adr = "";
	//			adr = containerDao.findLocation(pid, type, num);
	//			if (adr != null) {
	//				storagePositionService.setPositionContent(adr, ids[i], "0");
	//
	//				StorageTransferItem sti = storageTransferDao.get(StorageTransferItem.class, ids[i]);
	//				sti.setNewLocation(adr);
	//				storageTransferDao.saveOrUpdate(sti);
	//			}
	//		}
	//
	//		return true;
	//	}

	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delStorageTransferItem(String[] ids) throws Exception {
		for (String id : ids) {
			StorageTransferItem scp = storageTransferDao.get(StorageTransferItem.class, id);
			storageTransferDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(StorageTransfer sc, Map jsonMap) throws Exception {
		if (sc != null) {
			storageTransferDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("storageTransferItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveStorageTransferItem(sc, jsonStr);
			}
		}
	}
}
