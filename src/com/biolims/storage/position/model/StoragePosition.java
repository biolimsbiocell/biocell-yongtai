package com.biolims.storage.position.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.core.model.user.Department;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicState;
import com.biolims.dic.model.DicType;
import com.biolims.storage.container.model.StorageContainer;

/**
 * 存储位置
 */
@Entity
@Table(name = "T_STORAGE_POSITION")
public class StoragePosition extends EntityDao<StoragePosition> implements
		Serializable {

	private static final long serialVersionUID = 2734219436493312328L;

	@Id
	@Column(name = "ID", length = 30)
	private String id;

	@Column(name = "NAME", length = 110)
	private String name;// 位置名称

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_TYPE_ID")
	private DicType type;// 温度

	@Column(name = "STORAGE_TYPE", length = 110)
	private String storageType;// 存储类型

	@Column(name = "UP_ID", length = 32)
	private String upId;// 上级编码

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_POSITION_UP_ID")
	private StoragePosition upStoragePosition;// 上级位置

	@Column(name = "LEVEL_NUMBER", length = 3)
	private Integer level;// 树级别

	@Column(name = "LEAF", length = 32)
	private String leaf;// 是否叶子节点

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DEPARTMENT_ID")
	private Department department;// 所属组织机构

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_CONTAINER_ID")
	private StorageContainer storageContainer;// 存储容器

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SUB_STORAGE_CONTAINER_ID")
	private StorageContainer subStorageContainer;// 存储容器

	@Column(name = "IS_USE", length = 50000)
	private String isUse = "0";// 是否占用 CLOB类型

	@Column(name = "OBJECT_ID", length = 32)
	private String objectId;// 是否占用

	@Column(name = "ROW_NAME", length = 32)
	private String rowName;// 层名称
	
	@Column(name = "ROW_NUM", length = 32)
	private String rowNum;// 冰箱层数

	@Column(name = "COL_NAME", length = 32)
	private String colName;// 排名称
	
	
	@Column(name = "COL_NUM", length = 32)
	private String colNum;// 每层排数

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DIC_STATE_ID")
	private DicState state;// 状态
	
	//位置类型
	@Column(name = "POSITION_TYPE", length = 32)
	private String positionType;
	
	/** scopeId*/
	private String scopeId;
	/** scopeName*/
	private String scopeName;
	// 非持久化属性
	@Transient
	private String typedStr;
	@Transient
	private String dicStateStr;
	@Transient
	private String isUseStr;
	@Transient
	private String storageObj;
	@Transient
	private String scName;
	@Transient
	private String scId;
	

	public String getPositionType() {
		return positionType;
	}

	public void setPositionType(String positionType) {
		this.positionType = positionType;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public DicType getType() {
		return type;
	}

	public void setType(DicType type) {
		this.type = type;
	}

	public String getStorageType() {
		return storageType;
	}

	public void setStorageType(String storageType) {
		this.storageType = storageType;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getUpId() {
		return upId;
	}

	public void setUpId(String upId) {
		this.upId = upId;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public DicState getState() {
		return state;
	}

	public void setState(DicState state) {
		this.state = state;
	}

	public String getIsUse() {
		return isUse;
	}

	public void setIsUse(String isUse) {
		this.isUse = isUse;
	}

	public StoragePosition getUpStoragePosition() {
		return upStoragePosition;
	}

	public void setUpStoragePosition(StoragePosition upStoragePosition) {
		this.upStoragePosition = upStoragePosition;
	}

	public String getTypedStr() {
		return typedStr;
	}

	public void setTypedStr(String typedStr) {
		this.typedStr = typedStr;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getLeaf() {
		return leaf;
	}

	public void setLeaf(String leaf) {
		this.leaf = leaf;
	}

	public String getDicStateStr() {
		return dicStateStr;
	}

	public void setDicStateStr(String dicStateStr) {
		this.dicStateStr = dicStateStr;
	}

	public String getIsUseStr() {
		return isUseStr;
	}

	public void setIsUseStr(String isUseStr) {
		this.isUseStr = isUseStr;
	}

	public String getStorageObj() {
		return storageObj;
	}

	public void setStorageObj(String storageObj) {
		this.storageObj = storageObj;
	}

	public StorageContainer getStorageContainer() {
		return storageContainer;
	}

	public void setStorageContainer(StorageContainer storageContainer) {
		this.storageContainer = storageContainer;
	}

	public String getScName() {
		return scName;
	}

	public void setScName(String scName) {
		this.scName = scName;
	}

	public String getScId() {
		return scId;
	}

	public void setScId(String scId) {
		this.scId = scId;
	}

	public String getObjectId() {
		return objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public String getRowNum() {
		return rowNum;
	}

	public void setRowNum(String rowNum) {
		this.rowNum = rowNum;
	}

	public String getColNum() {
		return colNum;
	}

	public void setColNum(String colNum) {
		this.colNum = colNum;
	}

	public StorageContainer getSubStorageContainer() {
		return subStorageContainer;
	}

	public void setSubStorageContainer(StorageContainer subStorageContainer) {
		this.subStorageContainer = subStorageContainer;
	}

	public String getRowName() {
		return rowName;
	}

	public void setRowName(String rowName) {
		this.rowName = rowName;
	}

	public String getColName() {
		return colName;
	}

	public void setColName(String colName) {
		this.colName = colName;
	}

	/**
	 * @return the scopeId
	 */
	public String getScopeId() {
		return scopeId;
	}

	/**
	 * @param scopeId the scopeId to set
	 */
	public void setScopeId(String scopeId) {
		this.scopeId = scopeId;
	}

	/**
	 * @return the scopeName
	 */
	public String getScopeName() {
		return scopeName;
	}

	/**
	 * @param scopeName the scopeName to set
	 */
	public void setScopeName(String scopeName) {
		this.scopeName = scopeName;
	}

}
