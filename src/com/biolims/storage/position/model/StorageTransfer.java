package com.biolims.storage.position.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.dic.model.DicCurrencyType;
import com.biolims.common.model.user.User;
import com.biolims.common.model.user.User;

/**   
 * @Title: Model
 * @Description: 材料转移
 * @author lims-platform
 * @date 2015-12-09 14:28:29
 * @version V1.0   
 *
 */
@Entity
@Table(name = "STORAGE_TRANSFER")
@SuppressWarnings("serial")
public class StorageTransfer extends EntityDao<StorageTransfer> implements java.io.Serializable {
	/**编码*/
	private String id;
	/**描述*/
	private String name;
	/**类型*/
	private String type;
	/**原因*/
	private DicType reason;
	/**创建人*/
	private User createUser;
	/**创建日期*/
	private Date createDate;
	/**审核人*/
	private User confirmUser;
	/**审核日期*/
	private Date confirmDate;
	/**工作流状态id*/
	private String state;
	/**工作流状态*/
	private String stateName;
	/**备注*/
	private String note;

	/**
	 *方法: 取得String
	 *@return: String  编码
	 */

	@Id
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 *方法: 设置String
	 *@param: String  编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name = "NAME", length = 120)
	public String getName() {
		return this.name;
	}

	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *方法: 取得String
	 *@return: String  类型
	 */
	@Column(name = "TYPE", length = 255)
	public String getType() {
		return this.type;
	}

	/**
	 *方法: 设置String
	 *@param: String  类型
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 *方法: 取得DicType
	 *@return: DicType  原因
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "REASON")
	public DicType getReason() {
		return this.reason;
	}

	/**
	 *方法: 设置DicType
	 *@param: DicType  原因
	 */
	public void setReason(DicType reason) {
		this.reason = reason;
	}

	/**
	 *方法: 取得User
	 *@return: User  创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return this.createUser;
	}

	/**
	 *方法: 设置User
	 *@param: User  创建人
	 */
	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 *方法: 取得Date
	 *@return: Date  创建日期
	 */
	@Column(name = "CREATE_DATE", length = 255)
	public Date getCreateDate() {
		return this.createDate;
	}

	/**
	 *方法: 设置Date
	 *@param: Date  创建日期
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 *方法: 取得User
	 *@return: User  审核人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CONFIRM_USER")
	public User getConfirmUser() {
		return this.confirmUser;
	}

	/**
	 *方法: 设置User
	 *@param: User  审核人
	 */
	public void setConfirmUser(User confirmUser) {
		this.confirmUser = confirmUser;
	}

	/**
	 *方法: 取得Date
	 *@return: Date  审核日期
	 */
	@Column(name = "CONFIRM_DATE", length = 255)
	public Date getConfirmDate() {
		return this.confirmDate;
	}

	/**
	 *方法: 设置Date
	 *@param: Date  审核日期
	 */
	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	/**
	 *方法: 取得String
	 *@return: String  工作流状态id
	 */
	@Column(name = "STATE", length = 60)
	public String getState() {
		return this.state;
	}

	/**
	 *方法: 设置String
	 *@param: String  工作流状态id
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 *方法: 取得String
	 *@return: String  工作流状态
	 */
	@Column(name = "STATE_NAME", length = 60)
	public String getStateName() {
		return this.stateName;
	}

	/**
	 *方法: 设置String
	 *@param: String  工作流状态
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name = "NOTE", length = 120)
	public String getNote() {
		return this.note;
	}

	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note) {
		this.note = note;
	}
}