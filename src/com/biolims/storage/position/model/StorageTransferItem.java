package com.biolims.storage.position.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;

/**   
 * @Title: Model
 * @Description: 材料转移详细信息
 * @author lims-platform
 * @date 2015-12-09 14:28:24
 * @version V1.0   
 *
 */
@Entity
@Table(name = "STORAGE_TRANSFER_ITEM")
@SuppressWarnings("serial")
public class StorageTransferItem extends EntityDao<StorageTransferItem> implements java.io.Serializable {
	/**编码*/
	private String id;
	/**材料编号*/
	private String cellId;
	/**材料名称*/
	private String cellName;
	/**保管人*/
	private String saveUser;
	/**项目编号*/
	private String project;
	/**原位置*/
	private String oldLocation;
	/**新位置*/
	private String newLocation;
	/**相关主表*/
	private StorageTransfer storageTransfer;

	/**
	 *方法: 取得String
	 *@return: String  编码
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 36)
	public String getId() {
		return this.id;
	}

	/**
	 *方法: 设置String
	 *@param: String  编码
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 *方法: 取得String
	 *@return: String  材料编号
	 */
	@Column(name = "CELL_ID", length = 60)
	public String getCellId() {
		return this.cellId;
	}

	/**
	 *方法: 设置String
	 *@param: String  材料编号
	 */
	public void setCellId(String cellId) {
		this.cellId = cellId;
	}

	/**
	 *方法: 取得String
	 *@return: String  材料名称
	 */
	@Column(name = "CELL_NAME", length = 60)
	public String getCellName() {
		return this.cellName;
	}

	/**
	 *方法: 设置String
	 *@param: String  材料名称
	 */
	public void setCellName(String cellName) {
		this.cellName = cellName;
	}

	/**
	 *方法: 取得String
	 *@return: String  保管人
	 */
	@Column(name = "SAVE_USER", length = 40)
	public String getSaveUser() {
		return this.saveUser;
	}

	/**
	 *方法: 设置String
	 *@param: String  保管人
	 */
	public void setSaveUser(String saveUser) {
		this.saveUser = saveUser;
	}

	/**
	 *方法: 取得String
	 *@return: String  项目编号
	 */
	@Column(name = "PROJECT", length = 60)
	public String getProject() {
		return this.project;
	}

	/**
	 *方法: 设置String
	 *@param: String  项目编号
	 */
	public void setProject(String project) {
		this.project = project;
	}

	/**
	 *方法: 取得String
	 *@return: String  原位置
	 */
	@Column(name = "OLD_LOCATION", length = 60)
	public String getOldLocation() {
		return this.oldLocation;
	}

	/**
	 *方法: 设置String
	 *@param: String  原位置
	 */
	public void setOldLocation(String oldLocation) {
		this.oldLocation = oldLocation;
	}

	/**
	 *方法: 取得String
	 *@return: String  新位置
	 */
	@Column(name = "NEW_LOCATION", length = 60)
	public String getNewLocation() {
		return this.newLocation;
	}

	/**
	 *方法: 设置String
	 *@param: String  新位置
	 */
	public void setNewLocation(String newLocation) {
		this.newLocation = newLocation;
	}

	/**
	 *方法: 取得StorageTransfer
	 *@return: StorageTransfer  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_TRANSFER")
	public StorageTransfer getStorageTransfer() {
		return this.storageTransfer;
	}

	/**
	 *方法: 设置StorageTransfer
	 *@param: StorageTransfer  相关主表
	 */
	public void setStorageTransfer(StorageTransfer storageTransfer) {
		this.storageTransfer = storageTransfer;
	}
}