package com.biolims.storage.position.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.SystemConstants;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.model.user.User;
import com.biolims.sample.storage.model.SampleInItem;
import com.biolims.sample.storage.model.SampleInfoIn;
import com.biolims.storage.container.service.ContainerService;
import com.biolims.storage.position.service.SaveSampleInService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

/**
 * 新页面样本出入库
 * 
 */

@Controller
@Scope("prototype")
@ParentPackage("default")
@Namespace("/storage")
public class SaveSampleInAction extends BaseActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2912107013637879850L;
	@Autowired
	private ContainerService containerService;
	@Resource
	private SaveSampleInService saveSampleInService;
	

	// 保存入库样本明细--样本入库
	@Action(value = "saveSampleIn", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveSampleIn() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			// 样本编号 和位置
			// 位置
			// {"position":["A0001-01-01-A02-B03","A0001-01-01-A02-B04","A0001-01-01-A02-B05","A0001-01-01-A02-B06"]}
			// 样本编号
			// {"code":["DA1706150008","DA1706150009","DA1706150010","DA1706150011"]}
			String position1 = getRequest().getParameter("position");
			String code1 = getRequest().getParameter("code");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			String autoID = saveSampleInService.saveSampleIn(position1, code1,
					user);
			map.put("id", autoID);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 保存入库样本明细--血袋入库
	@Action(value = "saveSampleInByBlood", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveSampleInByBlood() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			// 样本编号 和位置
			// 位置 {"position":["A0001-01-01-A02"]}
			// 样本编号
			// {"code":["DA1706150008","DA1706150009","DA1706150010","DA1706150011"]}
			String position1 = getRequest().getParameter("position");
			String code1 = getRequest().getParameter("code");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			String autoID = saveSampleInService.saveSampleInByBlood(position1,
					code1, user);
			map.put("id", autoID);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 血袋出库-查询位置内样本详细信息
	@Action(value = "selSampleByLocation", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selSampleByLocation() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String position = getParameterFromRequest("position");

		try {
			Map<String, Object> result = containerService.selSampleByLocation(
					start, length, query, col, sort, position);

			List<SampleInItem> list = (List<SampleInItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			// 样本名称 样本编号 样本类型 浓度 体积 总量
			// patientName code sampleType concentration volume sumTotal
			map.put("sampleCode", "");
			map.put("code", "");
			map.put("location", "");
			map.put("sampleType", "");
			map.put("concentration", "");
			map.put("volume", "");
			map.put("sumTotal", "");
			map.put("sampleTypeId", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			// Map<String,Object> mapField =
			// fieldService.findFieldByModuleValue("SampleOrder");
			// String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// 样本出库并改变状态至完成
	@Action(value = "saveSampleOut", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveSampleOut() throws Exception {
		// SampleInfoIn sampleInfoIn = new SampleInfoIn();
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			// {"strobj":[{"code":"DA1706200004","isDepot":"返库","nextFlowName":"核酸提取","nextFlowId":"0017","note":""},
			// {"code":"DA1706200005","isDepot":"返库","nextFlowName":"核酸提取","nextFlowId":"0017","note":""}]}
			String sJson = getRequest().getParameter("strobj");
			String outType = getRequest().getParameter("outType");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			String autoID = saveSampleInService.saveSampleOut(sJson, outType,
					user);
			map.put("id", autoID);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));

	}

	// 移库操作
	@Action(value = "moveSample", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void moveSample() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			// 样本编号
			// {"code":["DA1706150008","DA1706150009","DA1706150010","DA1706150011"]}
			// 旧盒子位置
			// {"oldPosition":["A0001-01-01-A02"}
			// 新盒子位置
			// {"newPosition":["A0001-01-01-A05"}  或{"newPosition":["A0001-01-01-A02-B03","A0001-01-01-A02-B04","A0001-01-01-A02-B05","A0001-01-01-A02-B06"]}
			String newPosition = getRequest().getParameter("newPosition");
			String code1 = getRequest().getParameter("code");
			String oldPosition = getRequest().getParameter("oldPosition");
			String boxId = getRequest().getParameter("boxId");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			saveSampleInService.moveSampleLocation(newPosition,oldPosition, code1, user,boxId);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	// 移库操作
		@Action(value = "moveSampleByBlood", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
		public void moveSampleByBlood() throws Exception {
			Map<String, Object> map = new HashMap<String, Object>();
			try {
				String newPosition = getRequest().getParameter("newPosition");
				String code1 = getRequest().getParameter("code");
				User user = (User) this
						.getObjFromSession(SystemConstants.USER_SESSION_KEY);
				saveSampleInService.moveSampleByBlood(newPosition, code1, user);
				map.put("success", true);
			} catch (Exception e) {
				e.printStackTrace();
				map.put("success", false);
			}
			HttpUtils.write(JsonUtils.toJsonString(map));
		}
	
		
	

// 查看盒内样本信息
	@Action(value = "selectSampleByBox", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selectSampleByBox() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List<String> picCode = new ArrayList<String>();
			String boxId = getRequest().getParameter("boxId");
			List<SampleInfoIn> list = saveSampleInService.selectSampleByBox(boxId);
			if(list.size()>0){
				//通过样本类型编号查询picCode
				picCode = saveSampleInService.selectPicCodeBySampleTypeId(list);
			}
			map.put("data", list);
			map.put("picCode", picCode);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
		
// 样本放入盒内保存
	@Action(value = "saveSampleByBox", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveSampleByBox() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			// 样本编号 和位置
			// 位置
			// {"position":["A0001-01-01-A02-B03","A0001-01-01-A02-B04","A0001-01-01-A02-B05","A0001-01-01-A02-B06"]}
			// 样本编号
			// {"code":["DA1706150008","DA1706150009","DA1706150010","DA1706150011"]}
			String code = getRequest().getParameter("code");
			String position = getRequest().getParameter("position");
			String boxId = getRequest().getParameter("boxId");
			saveSampleInService.saveSampleByBox(code,position,boxId);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	

}
