package com.biolims.storage.position.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.storage.position.model.StorageTransfer;
import com.biolims.storage.position.model.StorageTransferItem;
import com.biolims.storage.position.service.StorageTransferService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/storage/position/storageTransfer")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class StorageTransferAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "261";
	@Autowired
	private StorageTransferService storageTransferService;
	private StorageTransfer storageTransfer = new StorageTransfer();
	@Resource
	private FileInfoService fileInfoService;
//	@Resource
//	private ProjectService projectService;

	//	@Resource
	//	private ProjectService projectService;

	@Action(value = "showStorageTransferList")
	public String showStorageTransferList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/storage/position/storageTransfer.jsp");
	}

	@Action(value = "showStorageTransferListJson")
	public void showStorageTransferListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = storageTransferService.findStorageTransferList(map2Query, startNum, limitNum, dir,
				sort);
		Long count = (Long) result.get("total");
		List<StorageTransfer> list = (List<StorageTransfer>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("type", "");
		map.put("reason-id", "");
		map.put("reason-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd HH:mm:ss");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd HH:mm:ss");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "storageTransferSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogStorageTransferList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/storage/position/storageTransferDialog.jsp");
	}

	@Action(value = "showDialogStorageTransferListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogStorageTransferListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = storageTransferService.findStorageTransferList(map2Query, startNum, limitNum, dir,
				sort);
		Long count = (Long) result.get("total");
		List<StorageTransfer> list = (List<StorageTransfer>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("type", "");
		map.put("reason-id", "");
		map.put("reason-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd HH:mm:ss");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd HH:mm:ss");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "editStorageTransfer")
	public String editStorageTransfer() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			storageTransfer = storageTransferService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "storageTransfer");
		} else {
			storageTransfer.setId("NEW");
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			storageTransfer.setCreateUser(user);
			storageTransfer.setCreateDate(new Date());
			storageTransfer.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			storageTransfer.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		if (storageTransfer.getState() != null)
			toState(storageTransfer.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/storage/position/storageTransferEdit.jsp");
	}

	@Action(value = "copyStorageTransfer")
	public String copyStorageTransfer() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		storageTransfer = storageTransferService.get(id);
		storageTransfer.setId("NEW");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/storage/position/storageTransferEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = storageTransfer.getId();
		if (id != null && id.equals("") || id.equals("NEW")) {
			String modelName = "StorageTransfer";
//			String autoID = projectService.findAutoID(modelName, 5);
			storageTransfer.setId("");
		}
		Map aMap = new HashMap();
		aMap.put("storageTransferItem", getParameterFromRequest("storageTransferItemJson"));

		storageTransferService.save(storageTransfer, aMap);
		return redirect("/storage/position/storageTransfer/editStorageTransfer.action?id=" + storageTransfer.getId());

	}

	@Action(value = "viewStorageTransfer")
	public String toViewStorageTransfer() throws Exception {
		String id = getParameterFromRequest("id");
		storageTransfer = storageTransferService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/storage/position/storageTransferEdit.jsp");
	}

	@Action(value = "showStorageTransferItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showStorageTransferItemList() throws Exception {
		return dispatcher("/WEB-INF/page/storage/position/storageTransferItem.jsp");
	}

	@Action(value = "showStorageTransferItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showStorageTransferItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = storageTransferService.findStorageTransferItemList(scId, startNum, limitNum,
					dir, sort);
			Long total = (Long) result.get("total");
			List<StorageTransferItem> list = (List<StorageTransferItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("cellId", "");
			map.put("cellName", "");
			map.put("saveUser", "");
			map.put("project", "");
			map.put("oldLocation", "");
			map.put("newLocation", "");
			map.put("storageTransfer-name", "");
			map.put("storageTransfer-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//	//批量分配存储位置
	//	@Action(value = "batchLocationFun")
	//	public void batchLocationFun() throws Exception {
	//		String idstr = getRequest().getParameter("idstr");
	//		String[] ids = idstr.split(",");
	//		String type = getRequest().getParameter("type");
	//		String pid = getRequest().getParameter("pid");
	//
	//		Map<String, Object> result = new HashMap<String, Object>();
	//		try {
	//			boolean dataListMap = false;//mapForQuery, 
	//			if (ids.length != 0 && pid != null) {
	//				dataListMap = this.storageTransferService.batchLocation(ids, type, pid);
	//			}
	//			result.put("success", true);
	//			result.put("data", dataListMap);
	//		} catch (Exception e) {
	//			result.put("success", false);
	//		}
	//		HttpUtils.write(JsonUtils.toJsonString(result));
	//	}

	/**
	* 删除明细信息
	* @throws Exception
	*/
	@Action(value = "delStorageTransferItem")
	public void delStorageTransferItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			storageTransferService.delStorageTransferItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public StorageTransferService getStorageTransferService() {
		return storageTransferService;
	}

	public void setStorageTransferService(StorageTransferService storageTransferService) {
		this.storageTransferService = storageTransferService;
	}

	public StorageTransfer getStorageTransfer() {
		return storageTransfer;
	}

	public void setStorageTransfer(StorageTransfer storageTransfer) {
		this.storageTransfer = storageTransfer;
	}

}
