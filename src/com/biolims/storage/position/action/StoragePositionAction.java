package com.biolims.storage.position.action;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.storage.position.dao.StoragePositionDao;
import com.biolims.storage.position.model.StoragePosition;
import com.biolims.storage.position.service.StoragePositionService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

/**
 * 存储位置
 * 
 */
@Controller
@Scope("prototype")
@ParentPackage("default")
@Namespace("/storage/position")
public class StoragePositionAction extends BaseActionSupport {

	@Resource
	private StoragePositionDao storagePositionDao;
	private static final long serialVersionUID = -3274634299474255783L;

	// 用于页面上显示模块名称
	private String title = "存储位置";

	private StoragePosition sp = new StoragePosition();

	@Autowired
	private StoragePositionService storagePositionService;

	// 该action权限id
	private String rightsId = "102";

	// 存储位置二维图

	@Action(value = "showStoragePicture", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showStoragePicture() throws Exception {

		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		// putObjToContext("handlemethod",
		// SystemConstants.PAGE_HANDLE_METHOD_LIST);

		// 传值到jsp页面
		putObjToContext("list", this.storagePositionDao.findName());
		putObjToContext("list2", this.storagePositionDao.findName2());
		return dispatcher("/WEB-INF/page/storage/position/showStoragePicture.jsp");
	}

	/**
	 * 访问 任务树
	 */
	@Action(value = "showStoragePositionTree")
	public String showStoragePositionTree() throws Exception {

		putObjToContext("path", ServletActionContext.getRequest()
				.getContextPath()
				+ "/storage/position/showStoragePositionTreeJson.action");
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);

		return dispatcher("/WEB-INF/page/storage/position/showStoragePositionTree.jsp");
	}

	@Action(value = "showStoragePositionTreeDialog")
	public String showStoragePositionTreeDialog() throws Exception {
		String type = getParameterFromRequest("typeId");
		String types = getParameterFromRequest("type");
		// putObjToContext("path",
		// ServletActionContext.getRequest().getContextPath()
		// + "/storage/position/showStoragePositionTreeJson.action?type=" +
		// type);
		// toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		// putObjToContext("handlemethod",
		// SystemConstants.PAGE_HANDLE_METHOD_LIST);
		putObjToContext("type", types);
		putObjToContext("list", this.storagePositionDao.findName());
		putObjToContext("list2", this.storagePositionDao.findName2());
		return dispatcher("/WEB-INF/page/storage/position/showStoragePictureDialog.jsp");

		// return
		// dispatcher("/WEB-INF/page/storage/position/showStoragePositionTreeDialog.jsp");
	}

	@Action(value = "showStoragePositionTreeDialog2")
	public String showStoragePositionTreeDialog2() throws Exception {
		String type = getParameterFromRequest("typeId");
		putObjToContext("path", ServletActionContext.getRequest()
				.getContextPath()
				+ "/storage/position/showStoragePositionTreeJson.action?type="
				+ type);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);

		return dispatcher("/WEB-INF/page/storage/position/showStoragePositionTreeDialog.jsp");
	}

	@Action(value = "showStoragePositionTreeJson")
	public void showStoragePositionTreeJson() throws Exception {
		String upId = getParameterFromRequest("treegrid_id");
		String type = getParameterFromRequest("type");
		List<StoragePosition> aList = null;
		if (upId.equals("")) {

			aList = storagePositionService.findStoragePositionList(type);
		} else {

			aList = storagePositionService.findStoragePositionList(upId, type);
		}

		String a = storagePositionService.getJson(aList);
		new SendData().sendDataJson(a, ServletActionContext.getResponse());
	}

	@Action(value = "showStorageChildPositionTreeJson")
	public void showStorageChildPositionTreeJson() throws Exception {
		String upId = getParameterFromRequest("treegrid_id");

		List<StoragePosition> aList = storagePositionService
				.findStoragePositionList(upId);
		String a = storagePositionService.getJson(aList);

		new SendData().sendDataJson(a, ServletActionContext.getResponse());
	}

	/**
	 * 添加位置VIEW
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "toEditStoragePosition")
	public String editStoragePositionView() throws Exception {
		String id = getParameterFromRequest("id");
		String upId = getParameterFromRequest("upId");
		if (id != null && !id.equals("")) {
			this.sp = this.storagePositionService.findStoragePositionById(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			this.getPositionStorageView();
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		} else {
			if (upId != null && !upId.equals("")) {
				StoragePosition upSp = storagePositionService
						.findStoragePositionById(upId);
				sp.setUpStoragePosition(upSp);
			}

			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		return dispatcher("/WEB-INF/page/storage/position/editStoragePosition.jsp");
	}

	/**
	 * 添加位置VIEW
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "toViewStoragePosition")
	public String toViewStoragePosition() throws Exception {
		String id = getParameterFromRequest("id");

		this.sp = this.storagePositionService.findStoragePositionById(id);
		this.getPositionStorageView();
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);

		return dispatcher("/WEB-INF/page/storage/position/editStoragePosition.jsp");
	}

	/**
	 * 保存数据
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "editStoragePosition")
	public String editStoragePosition() throws Exception {
		String info = getRequest().getParameter("info");
		String delItemsPosi = getRequest().getParameter("delItemsPosi");
		try {
			this.storagePositionService.saveOrModifyStoragePosition(this.sp,
					info, delItemsPosi);

			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			// 根据冰箱的层数和排数自动生成对应的数据
			if (this.sp.getUpId() == null)
				this.storagePositionService.autoStoragePositionInfo(this.sp,user);
		} catch (Exception e) {
			e.printStackTrace();
		}

		String upId = "";
		StoragePosition up = sp.getUpStoragePosition();
		if (up != null) {
			upId = up.getId();
		}

		return redirect("/storage/position/toEditStoragePosition.action?id="
				+ sp.getId() + "&upId=" + upId);
	}

	/**
	 * 存储位置数据VIEW
	 * 
	 * @return
	 * @throws Exception
	 */
	public void getPositionStorageView() throws Exception {

		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		map.put("id", new String[] { "", "string", "", "对象编码", "150", "true",
				"", "", "", "", "", "" });
		map.put("name", new String[] { "", "string", "", "对象名称", "150", "true",
				"", "", "", "", "", "" });
		String type = generalexttype(map);
		String col = generalextcol(map);
		super.getRequest().setAttribute("type", type);
		super.getRequest().setAttribute("col", col);
		String id = super.getRequest().getParameter("id");
		super.getRequest().setAttribute(
				"path",
				super.getRequest().getContextPath()
						+ "/storage/position/showPositionStorage.action?id="
						+ id);
	}

	// 查看二维的存储位置
	@Action(value = "editStoragePosition2w")
	public String editStoragePosition2w() throws Exception {
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			this.sp = this.storagePositionService.findStoragePositionById(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			this.getPositionStorageView();
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
		}

		return dispatcher("/WEB-INF/page/storage/position/editStoragePosition2w.jsp");
	}

	/**
	 * 取得存储位置数据，用like方法
	 * 
	 * @return
	 * @throws Exception
	 */
	// @Action(value = "getPositionStorage", interceptorRefs =
	// @InterceptorRef("biolimsDefaultStack"))
	// public void getPositionStorage() throws Exception {
	// String id = super.getRequest().getParameter("id");
	// if (id != null) {
	// List<Storage> result =
	// this.storagePositionService.findStorageLikePositionId(id);
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "id");
	// map.put("name", "name");
	// new SendData().sendDateJson(map, result, result.size(),
	// ServletActionContext.getResponse());
	// }
	//
	// }

	/**
	 * 取得存储位置数据，用=
	 * 
	 * @return
	 * @throws Exception
	 */
	// @Action(value = "showPositionStorage")
	// public void showPositionStorage() throws Exception {
	// String id = super.getRequest().getParameter("id");
	// if (id != null) {
	// List<Storage> result =
	// this.storagePositionService.findStorageByPositionId(id);
	// Map<String, String> map = new HashMap<String, String>();
	// map.put("id", "id");
	// map.put("name", "name");
	// new SendData().sendDateJson(map, result, result.size(),
	// ServletActionContext.getResponse());
	// }
	//
	// }

	// 设置位置存放记录
	@Action(value = "setPosition")
	public void setPosition() throws Exception {
		String idstr = super.getRequest().getParameter("idstr");
		String type = super.getRequest().getParameter("type");

		Map<String, Object> result = new HashMap<String, Object>();
		try {
			boolean dataListMap = false;
			if (idstr != null) {
				String[] ids = idstr.split(";");
				for (int i = 0; i < ids.length; i++) {
					String[] idpos = ids[i].split(",");
					if (idpos != null && idpos.length > 1) {
						String id = idpos[0];
						String pos = idpos[1];
						if (pos != null && pos.length() > 4) {
							dataListMap = storagePositionService
									.setPositionContent(pos, id, type);
						}
					}
				}
			}
			result.put("success", dataListMap);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public StoragePosition getSp() {
		return sp;
	}

	public void setSp(StoragePosition sp) {
		this.sp = sp;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
