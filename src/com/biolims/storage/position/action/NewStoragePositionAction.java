package com.biolims.storage.position.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONArray;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.crm.customer.patient.model.CrmPatient;
import com.biolims.sample.model.DicSampleType;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.storage.model.SampleInItem;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.sample.storage.model.SampleInfoIn;
import com.biolims.storage.container.dao.ContainerDao;
import com.biolims.storage.container.model.StorageContainer;
import com.biolims.storage.container.service.ContainerService;
import com.biolims.storage.position.dao.StoragePositionDao;
import com.biolims.storage.position.model.StoragePosition;
import com.biolims.storage.position.service.StoragePositionService;
import com.biolims.system.nextFlow.model.NextFlow;
import com.biolims.system.storage.model.StorageBox;
import com.biolims.system.storage.service.StorageBoxService;
import com.biolims.system.work.model.WorkOrderItem;
import com.biolims.system.work.service.WorkOrderService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

/**
 * 新页面存储位置
 * 
 */
@Controller
@Scope("prototype")
@ParentPackage("default")
@Namespace("/storage/newPosition")
@SuppressWarnings("unchecked")
public class NewStoragePositionAction extends BaseActionSupport {

	private static final long serialVersionUID = -292745719889741439L;
	@Autowired
	private StoragePositionService storagePositionService;
	@Autowired
	private StoragePositionDao storagePositionDao;
	@Autowired
	private StorageBoxService storageBoxService;
	@Autowired
	private ContainerService containerService;
	@Autowired
	private CommonService commonService;
	@Resource
	private WorkOrderService workOrderService;

	@Autowired
	private ContainerDao containerDao;


	StoragePosition sp = new StoragePosition();

	private String rightsId = "";

	/*
	 * // 跳转新页面JSP路径
	 * 
	 * @Action(value = "showNewPosition") public String showNewPosition() throws
	 * Exception { putObjToContext("handlemethod",
	 * SystemConstants.PAGE_HANDLE_METHOD_LIST); toToolBar(rightsId, "", "",
	 * SystemConstants.PAGE_HANDLE_METHOD_LIST); return
	 * dispatcher("/lims/pages/3d/3d.jsp"); }
	 */

	// 跳转新页面JSP路径样本入库
	@Action(value = "sampleIn")
	public String sampleIn() throws Exception {
		rightsId = "18040201";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/lims/pages/3d/SampleIn.jsp");
	}

	// 跳转新页面JSP路径样本管理
	@Action(value = "sampleManager")
	public String sampleManager() throws Exception {
		rightsId = "18040202";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/lims/pages/3d/SampleManager.jsp");
	}

	// 根据样本类型ID查询下一步流向
	@Action(value = "selNextFlowBySampleTypeId", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selNextFlowBySampleTypeId() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String sampleType = getParameterFromRequest("sampleTypeId");
			String model = getParameterFromRequest("model");
			Map<String, Object> result = workOrderService.shownextFlowSampleOutTableJson(sampleType,model);
			List<NextFlow> list = (List<NextFlow>) result.get("list");
			//通过模块名称 SampleOut查询
			/*List<WorkOrderItem> list = storagePositionDao.selWorkOrderItemByModel();
			List<NextFlow> next = storagePositionService.selNextFlowByWorkOrderItem(list,sampleTypeId);*/
			
			
//			List<DicSampleType> list = storagePositionDao
//					.selectNextFlowBySampleTypeId(sampleTypeId);
			map.put("data", list);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 查询存储位置
	@Action(value = "showNewStoragePosition", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showNewStoragePosition() throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String upId = getParameterFromRequest("id");
			String type = getParameterFromRequest("kuType");
			List<StoragePosition> list = storagePositionService
					.findNewStoragePositionList(upId,type);
			map.put("data", list);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	/**
	 * 存储位置的Dialog
	 * @Title: showStoragePositionDialogList  
	 * @Description: TODO  
	 * @param @return
	 * @param @throws Exception 
	 * @return String
	 * @author 孙灵达  
	 * @date 2018年6月26日
	 * @throws
	 */
	@Action(value = "showStoragePositionDialogList")
	public String showStoragePositionDialogList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/storage/position/showStoragePositionDialog.jsp");
	}
	@Action(value = "showStoragePositionDialogListJson")
	public void showStoragePositionDialogListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = storagePositionService.findStoragePositionDialogList(start, length, query, col, sort);
			List<StoragePosition> list = (List<StoragePosition>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 查询位置内样本数量
	@Action(value = "showNewStoragePositionData", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showNewStoragePositionData() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			// BX01-01-01
			String id = getRequest().getParameter("id");
			StoragePosition sp = storagePositionDao.get(StoragePosition.class,
					id);
			StorageContainer sc = null;
			StorageContainer subsc = null;
			if (sp.getStorageContainer() != null
					&& sp.getStorageContainer().getId() != null
					&& sp.getStorageContainer().getId().length() > 0) {
				sc = sp.getStorageContainer();
			} else {
				sc = new StorageContainer();
				sc.setRowNum(0);
				sc.setColNum(0);
			}

			if (sp.getSubStorageContainer() != null
					&& sp.getSubStorageContainer().getId() != null
					&& sp.getSubStorageContainer().getId().length() > 0) {
				subsc = sp.getSubStorageContainer();
			} else {
				subsc = new StorageContainer();
				subsc.setRowNum(0);
				subsc.setColNum(0);
			}

			String name = sp.getId();
			long totalCount = 0;
			StringBuffer str = new StringBuffer();
			StringBuffer str1 = new StringBuffer();
			for (int i = 0; i < sc.getRowNum(); i++) {
				for (int j = 1; j <= sc.getColNum(); j++) {
					String boxName = "";
					long isUse = 0;
					if (name != null && j != 0) {
						if (j < 10) {
							isUse = containerService.findIsUse(name + "-"
									+ String.valueOf((char) (65 + i)) + "0"
									+ String.valueOf(j));
						} else {
							isUse = containerService.findIsUse(name + "-"
									+ String.valueOf((char) (65 + i))
									+ String.valueOf(j));
						}
					}

					String spId = "";
					if (j < 10) {
						// 1-01-01-A01 spId
						spId = name + "-" + String.valueOf((char) (65 + i))
								+ "0" + String.valueOf(j);
					} else {
						spId = name + "-" + String.valueOf((char) (65 + i))
								+ String.valueOf(j);
					}

					List<StorageBox> sbxl = commonService.get(StorageBox.class,
							"newLocationId", spId);
					if (sbxl.size() > 0) {
						boxName = sbxl.get(0).getName();
					}

					String sampleNum = "\"" + spId + "\":\"" + isUse + "\"";
					String box = "\"" + spId + "\":\"" + boxName + "\"";

					if (j == sc.getColNum() && (i + 1) == sc.getRowNum()) {
						str.append(sampleNum);
						str1.append(box);
					} else {
						str.append(sampleNum + ",");
						str1.append(box + ",");
					}
				}
			}

			// 样本数量
			StringBuffer a = new StringBuffer("").append("[{").append(str)
					.append("}]");
			// 盒子名称
			StringBuffer b = new StringBuffer("").append("[{").append(str1)
					.append("}]");

			map.put("data", a);
			map.put("data1", b);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 查询盒子
	@Action(value = "showNewStorageBox", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showNewStorageBox() throws Exception {
		Map<String, Object> map1 = new HashMap<String, Object>();
		try {
			// 1-01-01
			String id = getRequest().getParameter("id");
			// 1-01-01-A01
			String subId = getRequest().getParameter("subId");

			StoragePosition sp = storagePositionDao.get(StoragePosition.class,
					id);
			// 盒子信息
			StorageContainer sc = null;

			List<StorageBox> l = storageBoxService.findStorageBoxList(subId);

			if (l.size() > 0) {
				sc = l.get(0).getStorageContainer();

			} else {
				if (sc != null && sc.getId() != null && sc.getId().length() > 0) {
					sc = sp.getSubStorageContainer();
				} else {

					sc = new StorageContainer();
					sc.setRowNum(0);
					sc.setColNum(0);
				}

			}
			String name = subId;
			long totalCount = 0;
			StringBuffer str = new StringBuffer();
			StringBuffer str1 = new StringBuffer();
			// String style1 = "background:red;";
			// String style2 = "background:red;align:center;";
			for (int i = 0; i < sc.getRowNum(); i++) {
				// str.append("{");
				for (int j = 1; j <= sc.getColNum(); j++) {
					int isUse = 0;
					Map<String, Object> map = new HashMap<String, Object>();
					if (name != null && j != 0) {

						map = containerService.findMainStorage(name, i, j);

						if (map != null && map.size() > 0) {

							isUse = 1;
						}
					}

					// {"strobj":[{"cord":"A1","code":"JB1704050004"}]}
					// String code = (String) map.get("mid");

					String sampleCode;
					// 根据样本编号查询主数据
					List<SampleInItem> list = new ArrayList<SampleInItem>();

					if (map != null && !"".equals(map)) {
						sampleCode = (map.get("mid")).toString();
						list = containerService.selSampleInItem(sampleCode);
					} else {
						sampleCode = "";
					}
					String code;
					String code1;
					String picCode = "";
					if (list.size() > 0) {
						String typeId = list.get(0).getSampleTypeId();
						if(list.get(0).getSampleTypeId()!=null
								&&!"".equals(list.get(0).getSampleTypeId())) {
							
						}else {
							if(list.get(0).getSampleType()!=null
									&&!"".equals(list.get(0).getSampleType())) {
								List<DicSampleType> dsts = commonService.get(DicSampleType.class, "name", list.get(0).getSampleType());
								if(dsts.size()>0) {
									typeId = dsts.get(0).getId();
								}
							}
						}
						// 根据样本编号查询图标编码
						picCode = containerDao.finSampleTypePicCode(typeId);
						String o = JSONArray.fromObject(list).toString();

						code = "\"" + String.valueOf((char) (65 + i)) + j
								+ "\":" + o;
						code1 = "\"" + String.valueOf((char) (65 + i)) + j
								+ "\":" + "\"" + picCode + "\"";
					} else {
						code = "\"" + String.valueOf((char) (65 + i)) + j
								+ "\":\"" + sampleCode + "\"";
						code1 = "\"" + String.valueOf((char) (65 + i)) + j
								+ "\":\"" + picCode + "\"";
					}

					if (j == sc.getColNum() && (i + 1) == sc.getRowNum()) {
						str.append(code);
						str1.append(code1);
					} else {
						str.append(code + ",");
						str1.append(code1 + ",");
					}
					// if (j != sc.getColNum() && isUse == 1) {
					// str.append("\""+String.valueOf((char) (65 + i)) + j +
					// "\":\"" + map.get("mid")+"\",");
					// } else if (j == sc.getColNum() && isUse == 1) {
					// str.append("\""+String.valueOf((char) (65 + i)) + j +
					// "\":\"" + map.get("mid")+"\",");
					// } else if (j == sc.getColNum() && isUse == 0) {
					// str.append(String.valueOf((char) (65 + i)) + j+ "\":\"" +
					// map.get("mid")+"\",");
					// } else {
					// str.append(String.valueOf((char) (65 + i)) + j + "\":\""
					// + map.get("mid")+"\",");
					// }

					// if (j != sc.getColNum() && isUse == 1) {
					// str.append("\""+String.valueOf((char) (65 + i)) + j +
					// "\":\"" + list.get(0) +"\",");
					// } else if (j == sc.getColNum() && isUse == 1) {
					// str.append("\""+String.valueOf((char) (65 + i)) + j +
					// "\":\"" + list.get(0) +"\",");
					// } else if (j == sc.getColNum() && isUse == 0) {
					// str.append(String.valueOf((char) (65 + i)) + j+ "\":\"" +
					// list.get(0) +"\",");
					// } else {
					// str.append(String.valueOf((char) (65 + i)) + j + "\":\""
					// + list.get(0) +"\",");
					// }
				}
			}

			StringBuffer a = new StringBuffer("").append("[{").append(str)
					.append("}]");
			StringBuffer b = new StringBuffer("").append("[{").append(str1)
					.append("}]");

			// 盒子内行数 12
			String rowNum = sc.getRowNum().toString();
			// 盒子内列数 8
			String colNum = sc.getColNum().toString();
			map1.put("rowNum", rowNum);
			map1.put("colNum", colNum);
			map1.put("data", a);
			map1.put("picCode", b);
			map1.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map1.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map1));
	}

	// 根据样本编号查询主数据内样本信息
	@Action(value = "selSampleInfoByCode", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selSampleInfoByCode() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String code = getParameterFromRequest("code");
			List<SampleInfo> list = containerService.selSampleInfo(code);
			map.put("data", list);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 查询待入库样本 即样本入库左侧表
	@Action(value = "selSampleInTemp", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selSampleInTemp() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String codes = getParameterFromRequest("strObj");
		String sType = getParameterFromRequest("sType");

		try {
			Map<String, Object> result = containerService.selSampleInTemp(
					start, length, query, col, sort, codes, sType);

			List<SampleInItemTemp> list = (List<SampleInItemTemp>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			// 样本名称 样本编号 样本类型 浓度 体积 总量
			// patientName code sampleType concentration volume sumTotal
			map.put("patientName", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("sampleType", "");
			map.put("concentration", "");
			map.put("volume", "");
			map.put("sumTotal", "");
			map.put("sampleTypeId", "");
			
			map.put("productName", "");
			map.put("sampleDeteyionName", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			// Map<String,Object> mapField =
			// fieldService.findFieldByModuleValue("SampleOrder");
			// String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// 点击盒内样本列表显示
	@Action(value = "selSampleInItemByBox", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selSampleInItemByBox() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		String location = getParameterFromRequest("location");

		try {
			Map<String, Object> result = containerService.selSampleInItemByBox(
					start, length, query, col, sort, location);

			List<SampleInItem> list = (List<SampleInItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			// 样本名称 样本编号 样本类型 浓度 体积 总量
			// patientName code sampleType concentration volume sumTotal
			map.put("patientName", "");
			map.put("code", "");
			map.put("sampleCode", "");
			map.put("location", "");
			map.put("sampleType", "");
			map.put("concentration", "");
			map.put("volume", "");
			map.put("sumTotal", "");
			map.put("sampleTypeId", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			// Map<String,Object> mapField =
			// fieldService.findFieldByModuleValue("SampleOrder");
			// String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// 跳转盒子页面
	@Action(value = "showNewBox")
	public String showNewBox() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/system/storage/3dStorageBoxDialog.jsp");
	}

	// 加载盒子页面
	@Action(value = "selNewBox", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selNewBox() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = containerService.selStorageNewBox(
					start, length, query, col, sort);

			List<StorageBox> list = (List<StorageBox>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("storageContainer-id", "");
			map.put("storageContainer-name", "");
			map.put("storageContainer-rowNum", "");
			map.put("storageContainer-colNum", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			// 根据模块查询自定义字段数据
			// Map<String,Object> mapField =
			// fieldService.findFieldByModuleValue("SampleOrder");
			// String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// 保存入库样本明细 --选择盒子入库
	@Action(value = "saveSampleInTask", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveSampleInTask() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
//			// 样本编号
//			String code = getRequest().getParameter("code");
//			// 位置
//			String position = getRequest().getParameter("position");
			// 盒子ID
			String id = getRequest().getParameter("id");
			// 冰箱ID
			String iceBox = getRequest().getParameter("iceBox");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);

			String autoID = containerService.saveSampleInTask(iceBox,user, id);
			map.put("id", autoID);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/*
	 * // 样本入库核对编码--待入库样本上方查询
	 * 
	 * @Action(value = "selSampleInTempByCode", interceptorRefs =
	 * 
	 * @InterceptorRef("biolimsDefaultStack")) public void
	 * selSampleInTempByCode() throws Exception { String query =
	 * getRequest().getParameter("query"); String colNum =
	 * getParameterFromRequest("order[0][column]"); String col =
	 * getParameterFromRequest("columns[" + colNum + "][data]"); String sort =
	 * getParameterFromRequest("order[0][dir]"); Integer start =
	 * Integer.valueOf(getParameterFromRequest("start")); Integer length =
	 * Integer.valueOf(getParameterFromRequest("length")); String draw =
	 * getParameterFromRequest("draw"); String codes =
	 * getParameterFromRequest("strObj"); try { Map<String, Object> result =
	 * containerService.selSampleInTempByCode( start, length, query, col,
	 * sort,codes); List<SampleInItemTemp> list = (List<SampleInItemTemp>)
	 * result .get("list"); Map<String, String> map = new HashMap<String,
	 * String>(); // 样本名称 样本编号 样本类型 浓度 体积 总量 // patientName code sampleType
	 * concentration volume sumTotal map.put("patientName", ""); map.put("code",
	 * ""); map.put("sampleType", ""); map.put("concentration", "");
	 * map.put("volume", ""); map.put("sumTotal", ""); String data = new
	 * SendData().getDateJsonForDatatable(map, list); // 根据模块查询自定义字段数据 //
	 * Map<String,Object> mapField = //
	 * fieldService.findFieldByModuleValue("SampleOrder"); // String dataStr =
	 * PushData.pushFieldData(data, mapField);
	 * HttpUtils.write(PushData.pushData(draw, result, data)); } catch
	 * (Exception e) { e.printStackTrace(); } }
	 */

	// 输入一个或者多个编码又一个或者多个位置查询
	@Action(value = "selCodeOrLocation", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void selSampleOrPosition() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String strobj = getParameterFromRequest("strobj");
			// List<SampleInfoIn> list = new ArrayList<SampleInfoIn>();
			// list = containerService.selCodeOrLocation(strobj);
			List<SampleInfoIn> list = new ArrayList<SampleInfoIn>();
			List<String> locations = new ArrayList<String>();
			List<String> codes = new ArrayList<String>();
			if (strobj.indexOf("-") != -1) {
				String[] location = strobj.split(",");
				for (int j = 0; j < location.length; j++) {
					List<SampleInfoIn> list2 = containerDao
							.findCode(location[j]);
					if (list2.size() != 0) {
						list.addAll(list2);
					} else {
						locations.add(location[j]);
					}

				}
			} else {
				String[] code = strobj.split(",");
				for (int i = 0; i < code.length; i++) {
					List<SampleInfoIn> list3 = containerDao
							.findPositon(code[i]);
					if (list3.size() != 0) {
						list.addAll(list3);
					} else {
						codes.add(code[i]);
					}
				}
			}
			map.put("codes", codes);
			map.put("locations", locations);
			map.put("data", list);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 
	 * @Title: newIceBox
	 * @Description: 样本管理界面 冰箱新建按钮跳转页面
	 * @author qi.yan
	 * @date 2018-4-4下午2:43:44
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "newIceBox")
	public String newIceBox() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		putObjToContext("states", "3");
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/storage/common/newIceBox.jsp");
	}

	/**
	 * 
	 * @Title: editIceBox
	 * @Description: 样本管理界面 冰箱编辑按钮跳转页面
	 * @author qi.yan
	 * @date 2018-4-4下午2:45:45
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "editIceBox")
	public String editIceBox() throws Exception {
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			sp = storagePositionService.get(id);
		} 
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		putObjToContext("states", "2");
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/storage/common/newIceBox.jsp");
	}
	
	

	/**
	 * 
	 * @Title: save
	 * @Description: 保存冰箱信息
	 * @author qi.yan
	 * @date 2018-4-4下午4:44:33
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "save")
	public void save() throws Exception {

		User user = (User) this
				.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		StoragePosition storagePosition = new StoragePosition();
		String msg = "";
		String zId = "";
		boolean bool = true;
		String upId = "";
		try {
			String dataValue = getParameterFromRequest("dataValue");
			String str = "[" + dataValue + "]";
			String changeLog = getParameterFromRequest("changeLog");
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str,
					List.class);

			for (Map<String, Object> map : list) {
				// storagePosition = (StoragePosition) commonDao.Map2Bean(map,
				// storagePosition);
				storagePosition = containerService.selStoragePosition(map,
						storagePosition);
			}

			String info = getRequest().getParameter("info");
			String delItemsPosi = getRequest().getParameter("delItemsPosi");
			storagePositionService.saveOrModifyStoragePosition(
					storagePosition, info, delItemsPosi);

			// 根据冰箱的层数和排数自动生成对应的数据
			if (storagePosition.getUpStoragePosition() == null) {
				storagePositionService.autoStoragePositionInfo(storagePosition,user);
			}
			//编辑单个层排
			if(storagePosition.getId().indexOf("-")!=-1){
				storagePositionService.selStorageContainerBox(storagePosition,user);
			}

			StoragePosition up = storagePosition.getUpStoragePosition();
			if (up != null) {
				upId = up.getId();
			}
			Map aMap = new HashMap();
			Map lMap = new HashMap();
			containerService.save(storagePosition, aMap, changeLog, lMap);
			zId = storagePosition.getId();
		} catch (Exception e) {
			e.printStackTrace();
			msg = "保存失败！";
			bool = false;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("msg", msg);
		map.put("success", bool);
		map.put("id", zId);
		map.put("upId", upId);
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 存储位置数据VIEW
	 * 
	 * @return
	 * @throws Exception
	 */
	public void getPositionStorageView() throws Exception {

		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		map.put("id", new String[] { "", "string", "", "对象编码", "150", "true",
				"", "", "", "", "", "" });
		map.put("name", new String[] { "", "string", "", "对象名称", "150", "true",
				"", "", "", "", "", "" });
		String type = generalexttype(map);
		String col = generalextcol(map);
		super.getRequest().setAttribute("type", type);
		super.getRequest().setAttribute("col", col);
		String id = super.getRequest().getParameter("id");
		super.getRequest().setAttribute(
				"path",
				super.getRequest().getContextPath()
						+ "/storage/position/showPositionStorage.action?id="
						+ id);
	}

	public StoragePosition getSp() {
		return sp;
	}

	public void setSp(StoragePosition sp) {
		this.sp = sp;
	}
	
	/**
	 * 
	 * @Title: moveBox  
	 * @Description:移动架子上面盒子
	 * @author qi.yan
	 * @date 2018-4-8下午2:41:41
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value = "moveBox", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void moveBox() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String oldPosition = getRequest().getParameter("oldPosition");
			String newPosition = getRequest().getParameter("newPosition");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			this.containerService.moveBox(oldPosition,newPosition,user);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}


}
