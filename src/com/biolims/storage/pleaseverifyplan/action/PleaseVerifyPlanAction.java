package com.biolims.storage.pleaseverifyplan.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.common.service.SystemCodeService;
import com.biolims.dic.model.DicType;
import com.biolims.storage.common.constants.SystemCode;
import com.biolims.storage.common.constants.SystemConstants;
import com.biolims.storage.main.service.StorageService;
import com.biolims.storage.model.StorageApply;
import com.biolims.storage.model.StorageOut;
import com.biolims.storage.model.StorageOutItem;
import com.biolims.storage.model.StorageReagentBuySerial;
import com.biolims.storage.pleaseverifyplan.model.PleaseVerifyPlan;
import com.biolims.storage.pleaseverifyplan.model.PleaseVerifyPlanItem;
import com.biolims.storage.pleaseverifyplan.service.PleaseVerifyPlanService;
import com.biolims.util.DateUtil;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.opensymphony.xwork2.ActionContext;

/**
 * 出库管理
 * 
 */
@Controller
@Scope("prototype")
@ParentPackage("default")
@Namespace("/storage/pleaseVerifyPlan")
@SuppressWarnings("unchecked")
public class PleaseVerifyPlanAction extends BaseActionSupport {

	private static final long serialVersionUID = -44186381342141077L;

	private String title = "请验计划";

	private String rightsId="10409";

	private PleaseVerifyPlan pleaseVerifyPlan;// 请验计划

	@Autowired
	private PleaseVerifyPlanService pleaseVerifyPlanService;

	@Resource
	private SystemCodeService systemCodeService;

	@Autowired
	private CommonService commonService;
	@Autowired
	private CommonDAO commonDAO;
	@Resource
	private StorageService storageService;
	
	
	/**
	 * @throws Exception
	 * 
	 * @Title: submitSample
	 * @Description: 提交质检
	 * @author : 
	 * @date 
	 * @throws
	 */
	@Action(value = "submitSample")
	public void submitSample() throws Exception {
		String id = getParameterFromRequest("id");
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			storageService.submitSample(id, ids);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
			e.printStackTrace();
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	@Action(value = "selStorageBatch1", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String dicTypeSelectTable() throws Exception {
		String id = super.getRequest().getParameter("id");
		putObjToContext("id", id);
		return dispatcher("/WEB-INF/page/storage/main/storageBatch1.jsp");
	}
	
	@Action(value="storageBatchJson1")
	public void storageBatchJson()throws Exception{
		String ids=getParameterFromRequest("id");
		String query=getParameterFromRequest("query");
		String colNum=getParameterFromRequest("order[0][column]");
		String col=getParameterFromRequest("columns["+colNum+"][data]");
		String sort=getParameterFromRequest("order[0][dir]");
		Integer start=Integer.valueOf(getParameterFromRequest("start"));
		Integer length=Integer.valueOf(getParameterFromRequest("length"));
		String draw=getParameterFromRequest("draw");
		Map<String, Object> result = storageService
				.findStorageBatchByIds1(start, length, query, col,
						sort, ids);
		List<StorageReagentBuySerial> list = (List<StorageReagentBuySerial>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "id");
		map.put("serial", "");
		map.put("rankType-id", "");
		map.put("code", "");
		map.put("productDate", "yyyy-MM-dd");
		map.put("expireDate", "yyyy-MM-dd");
		map.put("remindDate", "yyyy-MM-dd");
		map.put("inDate", "yyyy-MM-dd");
		map.put("outPrice", "#.####");
		map.put("storage-unitGroup-mark2-name", "");
		map.put("storage-id", "");
		map.put("num", "");
		map.put("isGood", "");
		map.put("position-id", "");
		map.put("position-name", "");
		map.put("purchasePrice", "");
		map.put("qcState", "");
		map.put("qcPassDate", "");
		map.put("recationSum", "");
		map.put("note", "");
		map.put("note2", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}
	
	
	/**
	 * 
	 * @Title: 	  
	 * @Description: TODO(这里用一句话描述这个方法的作用)  
	 * @author qi.yan
	 * @date 2018-4-3上午10:25:51
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value = "showPleaseVerifyPlanItemTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showPleaseVerifyPlanItemTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = pleaseVerifyPlanService
					.selPleaseVerifyPlanItemTable(scId, start, length, query, col,
							sort);
			List<PleaseVerifyPlanItem> list = (List<PleaseVerifyPlanItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			
			map.put("expireDate", "yyyy-MM-dd");
			map.put("pleaseVerifyPlan-id", "");

			map.put("serial", "");

			map.put("code", "");

			map.put("note", "");
			
			map.put("note2", "");
			map.put("qcState", "");
			
			map.put("batchNum", "");
			
			map.put("storage-id", "");
			map.put("storage-name", "");
			map.put("storage-barCode", "");
			
			map.put("sampleDeteyionType", "");
			map.put("sampleDeteyionId", "");
			map.put("sampleDeteyionName", "");
			
			map.put("submit", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 删除明细
	 */
	@Action(value = "delPleaseVerifyPlanItem")
	public void delPleaseVerifyPlanItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
		String[] ids = getRequest().getParameterValues("ids[]");
		pleaseVerifyPlanService.delPleaseVeriPlanItem(ids);
		map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	/**
	 * 
	 * @Title: savePleaseVerifyPlanItemTable  
	 * @Description: TODO(这里用一句话描述这个方法的作用)  
	 * @author qi.yan
	 * @date 2018-4-3上午10:32:40
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value = "savePleaseVerifyPlanItemTable")
	public void savePleaseVerifyPlanItemTable() throws Exception {
		String pleaseVerifyPlan_id = getParameterFromRequest("id");
		String item = getParameterFromRequest("dataJson");
		String logInfo=getParameterFromRequest("logInfo");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			pleaseVerifyPlanService.savePleaseVerifyPlanItemTable(pleaseVerifyPlan_id, item,logInfo);
			result.put("success", true);
			result.put("id", pleaseVerifyPlan_id);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	/**
	 * 
	 * @Title: savePleaseVerifyPlanAndItem
	 * @Description: 大保存方法
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "savePleaseVerifyPlanAndItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void savePleaseVerifyPlanAndItem() throws Exception {
		String dataValue = getParameterFromRequest("dataValue");
		// 主表changeLog
		String changeLog = getParameterFromRequest("changeLog");
		// 子表changeLogItem
		String changeLogItem = getParameterFromRequest("changeLogItem");
		
		String id = getParameterFromRequest("id");
		String note = getParameterFromRequest("note");
//		String useUser = getParameterFromRequest("useUser");

		String str = "[" + dataValue + "]";

		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str,
				List.class);

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			for (Map<String, Object> map1 : list) {
				PleaseVerifyPlan so = new PleaseVerifyPlan();
				Map aMap = new HashMap();
				so = (PleaseVerifyPlan) commonDAO.Map2Bean(map1, so);
				//保存主表信息
				PleaseVerifyPlan newSo = pleaseVerifyPlanService.savePleaseVerifyPlanById(so,id,note,changeLog);//,useUser
				
				newSo.setScopeId((String) ActionContext.getContext().getSession()
						.get("scopeId"));
				newSo.setScopeName((String) ActionContext.getContext().getSession()
						.get("scopeName"));

				//保存子表信息
				aMap.put("ImteJson",getParameterFromRequest("ImteJson"));
				pleaseVerifyPlanService.savePleaseVerifyPlanAndItem(newSo, aMap, changeLog, changeLogItem);
				map.put("id",newSo.getId());
			}
			map.put("success", true);
			
		} catch (Exception e) {
			map.put("success", false);
			map.put("msg", e.getMessage());
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	
	/**
	 * 
	 * @Title: addPleaseVerifyPlanItem  
	 * @Description:  添加主数据到出库明细表
	 * @author qi.yan
	 * @date 2018-4-9上午10:01:39
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value = "addPleaseVerifyPlanItem")
	public void addPleaseVerifyPlanItem() throws Exception {
		String note = getParameterFromRequest("note");
//		String useUser = getParameterFromRequest("useUser");
		String id = getParameterFromRequest("id");
		String outDate = getParameterFromRequest("outDate");
		String outUser = getParameterFromRequest("outUser");
		String batchIds = getRequest().getParameter("batchIds");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String pleaseVerifyPlanId = pleaseVerifyPlanService.addPleaseVerifyPlanItem(note,id,outDate,outUser,batchIds);
			result.put("success", true);
			result.put("data", pleaseVerifyPlanId);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	@Action(value = "toViewPleaseVerifyPlan")
	public String toViewPleaseVerifyPlan() throws Exception {
		String id = getParameterFromRequest("id");
		this.pleaseVerifyPlan = this.pleaseVerifyPlanService.findPleaseVerifyPlanById(id);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toState(pleaseVerifyPlan.getState());
		showPleaseVerifyPlanItemList(SystemConstants.PAGE_HANDLE_METHOD_VIEW, id);

		return dispatcher("/WEB-INF/page/storage/pleaseverifyplan/editPleaseVerifyPlan.jsp");
	}
	
	/**
	 * 出库明细对象
	 * 
	 * @return
	 * @throws Exception
	 */

	public void showPleaseVerifyPlanItemList(String handleMethod, String outId)
			throws Exception {

		// String exttype = "";
		// String extcol = "";
		// // String editflag = "true";
		// LinkedHashMap<String, String[]> map = new LinkedHashMap<String,
		// String[]>();
		// map.put("id", new String[] { "", "string", "", "ID", "150", "false",
		// "true", "", "", "", "", "" });
		// map.put("storage-name", new String[] { "", "string", "", "对象名",
		// "200", "false", "false", "", "", "false", "",
		// "" });
		// map.put("storage-id",
		// new String[] { "", "string", "", "对象编码", "200", "false", "false", "",
		// "", "false", "", "" });
		// map.put("storage-type-id", new String[] { "", "string", "", "库存类型ID",
		// "150", "false", "true", "", "", "false",
		// "", "" });
		// map.put("storage-type-name", new String[] { "", "string", "", "库存类型",
		// "150", "false", "false", "", "", "false",
		// "", "" });
		// map.put("storage-studyType-name", new String[] { "", "string", "",
		// "库存类别", "150", "false", "false", "", "",
		// "false", "", "" });
		// map.put("storage-unit-name", new String[] { "", "string", "", "单位",
		// "75", "false", "false", "", "", "false",
		// "", "" });
		// map.put("serial", new String[] { "", "string", "", "批次ID", "150",
		// "false", "false", "", "", "true", "",
		// "serial" });
		// map.put("code", new String[] { "", "string", "", "批号", "150",
		// "false", "false", "", "", "true", "", "serial" });
		//
		// map.put("price",
		// new String[] { "", "string", "", "批次价格", "150", "false", "false", "",
		// "", "true", "", "price" });
		//
		// map.put("storageNum",
		// new String[] { "", "float", "", "库存数量", "150", "false", "false", "",
		// "", "false", "", "" });
		// map.put("num", new String[] { "", "float", "", "出库数量", "150",
		// "false", "false", "", "", "true", "formatCss",
		// "num" });
		// map.put("position-id", new String[] { "", "string", "", "存储位置ID",
		// "150", "false", "true", "", "", "false", "",
		// "" });
		// map.put("position-name", new String[] { "", "string", "", "存储位置",
		// "150", "false", "false", "", "", "false", "",
		// "" });
		// // 生成ext用type字符串
		// exttype = generalexttype(map);
		// // 生成ext用col字符串
		// extcol = generalextcol(map);
		// putObjToContext("type", exttype);
		// putObjToContext("col", extcol);
		// putObjToContext("path",
		// ServletActionContext.getRequest().getContextPath()
		// + "/storage/out/getStorageOutItemList.action?outId=" + outId);
		putObjToContext("outId", outId);
		putObjToContext("handlemethod", handleMethod);

	}
	
	/**
	 * 添加、修改出库申请页面
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "toEditPleaseVerifyPlan")
	public String editStorageOutShow() throws Exception {
		rightsId="10409";
		String outId = super.getRequest().getParameter("id");
		String handlemethod = "";
		if (outId != null&&!"".equals(outId)) {
			this.pleaseVerifyPlan = this.pleaseVerifyPlanService.findPleaseVerifyPlanById(outId);
			toState(pleaseVerifyPlan.getState());
			if (pleaseVerifyPlan.getState().equals(SystemConstants.DIC_STATE_YES))
				handlemethod = SystemConstants.PAGE_HANDLE_METHOD_VIEW;
			else
				handlemethod = SystemConstants.PAGE_HANDLE_METHOD_MODIFY;
			toToolBar(rightsId, "", "", handlemethod);
			showPleaseVerifyPlanItemList(handlemethod, outId);

		} else {
			this.pleaseVerifyPlan = new PleaseVerifyPlan();
			this.pleaseVerifyPlan.setId(SystemCode.DEFAULT_SYSTEMCODE);
			User outUser = (User) super.getSession().getAttribute(
					SystemConstants.USER_SESSION_KEY);
			pleaseVerifyPlan.setOutUser(outUser);
			pleaseVerifyPlan.setOutDate(new Date());
			pleaseVerifyPlan.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			pleaseVerifyPlan.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toState(pleaseVerifyPlan.getState());
			showPleaseVerifyPlanItemList(SystemConstants.PAGE_HANDLE_METHOD_ADD,
					outId);
		}
		return dispatcher("/WEB-INF/page/storage/pleaseverifyplan/editPleaseVerifyPlan.jsp");
	}
	
	/**
	 * 
	 * @Title: 
	 * @Description:展示主表
	 * @author
	 * @date
	 * @return
	 * @throws Exception
	 *             String
	 * @throws
	 */
	@Action(value = "showPleaseVerifyPlanTable")
	public String showPleaseVerifyPlanTable() throws Exception {
		rightsId="10409";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/storage/pleaseverifyplan/pleaseVerifyPlanTable.jsp");
	}
	
	
	
	@Action(value = "showPleaseVerifyPlanTableJson")
	public void showPleaseVerifyPlanTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = pleaseVerifyPlanService.selPleaseVerifyPlanTable(
					start, length, query, col, sort);
			List<PleaseVerifyPlan> list = (List<PleaseVerifyPlan>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("outUser-id", "");
			map.put("outUser-name", "");
			map.put("outDate", "yyyy-MM-dd");
			map.put("state", "");
			map.put("stateName", "");
			map.put("note", "");
			map.put("scopeId", "");
			map.put("scopeName", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String getRightsId() {
		return rightsId;
	}
	
	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}
	
	
	public PleaseVerifyPlan getPleaseVerifyPlan() {
		return pleaseVerifyPlan;
	}
	
	public void setPleaseVerifyPlan(PleaseVerifyPlan pleaseVerifyPlan) {
		this.pleaseVerifyPlan = pleaseVerifyPlan;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}

//	/**
//	 * 出库申请GRID
//	 * 
//	 * @return
//	 * @throws Exception
//	 */
//	@Action(value = "showStorageOutList")
//	public String showStorageOutList() throws Exception {
//
//		// LinkedHashMap<String, String[]> map = new LinkedHashMap<String,
//		// String[]>();
//		// map.put("id", new String[] { "", "string", "", "出库单号", "120", "true",
//		// "", "", "", "", "", "" });
//		// map.put("note", new String[] { "", "string", "", "描述", "175", "true",
//		// "", "", "", "", "", "" });
//		// map.put("useUser-name", new String[] { "", "string", "", "领用人",
//		// "175", "true", "", "", "", "", "", "" });
//		// map.put("outUser-name", new String[] { "", "string", "", "出库人",
//		// "175", "true", "", "", "", "", "", "" });
//		// map.put("outDate", new String[] { "", "string", "", "出库日期", "80",
//		// "true", "", "", "", "", "", "" });
//		// map.put("type-name", new String[] { "", "string", "", "出库类型", "180",
//		// "true", "", "", "", "", "", "" });
//		// map
//		// .put("confirmUser-name", new String[] { "", "string", "", "批准人",
//		// "100", "true", "true", "", "", "", "",
//		// "" });
//		// map.put("stateName", new String[] { "", "string", "", "工作流状态", "75",
//		// "true", "", "", "", "", "", "" });
//		// String type = generalexttype(map);
//		// String col = generalextcol(map);
//		// super.getRequest().setAttribute("type", type);
//		// super.getRequest().setAttribute("col", col);
//		// 用于判断当前页面类型,LIST类型
//		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		//
//		// super.getRequest().setAttribute(
//		// "path",
//		// super.getRequest().getContextPath() +
//		// "/storage/out/getStorageOutList.action?queryMethod="
//		// + getParameterFromRequest("queryMethod"));
//		return dispatcher("/WEB-INF/page/storage/out/showStorageOutList.jsp");
//	}
//
//	/**
//	 * 出库申请DataList
//	 * 
//	 * @throws Exception
//	 */
//	@Action(value = "getStorageOutList")
//	public void getStorageOutList() throws Exception {
//		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//		String data = getParameterFromRequest("data");
//
//		String queryMethod = getParameterFromRequest("queryMethod");
//		Map r = this.toMakeSessionQuery(data, "StorageOut", dir, sort,
//				queryMethod);
//		dir = (String) r.get("dir");
//		sort = (String) r.get("sort");
//		data = (String) r.get("queryData");
//		Map<String, Object> result = this.pleaseVerifyPlanService.findStorageOutList(
//				startNum, limitNum, dir, sort, data);
//		Long count = (Long) result.get("totalCount");
//		List<StorageApply> list = (List<StorageApply>) result.get("list");
//		Map<String, String> map = new HashMap<String, String>();
//		map.put("id", "");
//		map.put("note", "");
//		map.put("useUser-name", "useUser-name");
//		map.put("outUser-name", "");
//		map.put("outDate", "yyyy-MM-dd");
//		map.put("type-name", "");
//		map.put("confirmUser-name", "");
//		map.put("stateName", "");
//		new SendData().sendDateJson(map, list, count,
//				ServletActionContext.getResponse());
//	}
//
//	/**
//	 * 添加、修改出库申请页面
//	 * 
//	 * @return
//	 * @throws Exception
//	 */
//	@Action(value = "toEditStorageOut")
//	public String editStorageOutShow() throws Exception {
//		rightsId="10402";
//		String outId = super.getRequest().getParameter("id");
//		String handlemethod = "";
//		if (outId != null) {
//			this.storageOut = this.pleaseVerifyPlanService.findStorageOutById(outId);
//			toState(storageOut.getState());
//			if (storageOut.getState().equals(SystemConstants.DIC_STATE_YES))
//				handlemethod = SystemConstants.PAGE_HANDLE_METHOD_VIEW;
//			else
//				handlemethod = SystemConstants.PAGE_HANDLE_METHOD_MODIFY;
//			toToolBar(rightsId, "", "", handlemethod);
//			showStorageOutItemList(handlemethod, outId);
//
//		} else {
//			this.storageOut = new StorageOut();
//			this.storageOut.setId(SystemCode.DEFAULT_SYSTEMCODE);
//			User outUser = (User) super.getSession().getAttribute(
//					SystemConstants.USER_SESSION_KEY);
//			storageOut.setOutUser(outUser);
//			storageOut.setOutDate(new Date());
//			storageOut.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
//			storageOut.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
//			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
//			toState(storageOut.getState());
//			showStorageOutItemList(SystemConstants.PAGE_HANDLE_METHOD_ADD,
//					outId);
//			DicType regionType = commonService.get(DicType.class, "pekcity");
//			storageOut.setRegionType(regionType);
//		}
//		return dispatcher("/WEB-INF/page/storage/out/editStorageOut.jsp");
//	}
//
//	@Action(value = "toViewStorageOut")
//	public String toViewStorageOut() throws Exception {
//		String id = getParameterFromRequest("id");
//		this.storageOut = this.pleaseVerifyPlanService.findStorageOutById(id);
//		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
//		toState(storageOut.getState());
//		showStorageOutItemList(SystemConstants.PAGE_HANDLE_METHOD_VIEW, id);
//
//		return dispatcher("/WEB-INF/page/storage/out/editStorageOut.jsp");
//	}
//	
//	@Action(value = "alertStorageOut")
//	public void alertStorageOut() throws Exception {
//		String id = getParameterFromRequest("id");
//		pleaseVerifyPlanService.alertStorageOut(id);
//		Map<String, Object> map = new HashMap<String, Object>();
//		map.put("success", true);
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
//	/**
//	 * 添加、修改出库申请
//	 * 
//	 * @return
//	 * @throws Exception
//	 */
//	@Action(value = "save")
//	public String save() throws Exception {
//		String id = this.storageOut.getId();
//		String json = getParameterFromRequest("jsonDataStr");
//		if (id == null || id.length() <= 0
//				|| SystemCode.DEFAULT_SYSTEMCODE.equals(id)) {
//			String code = systemCodeService.getCodeByPrefix(
//					SystemCode.STORAGE_OUT_NAME,
//					"ME"
//							+ DateUtil.dateFormatterByPattern(new Date(),
//									"yyMMdd"), SystemCode.STORAGE_OUT_CODE, 3,
//					null);
//			this.storageOut.setId(code);
//		}
//		this.pleaseVerifyPlanService.save(storageOut);
//		if (json != null && !json.equals(""))
//			saveStorageOutItem(storageOut.getId(), json);
//		return redirect("/storage/out/toEditStorageOut.action?id=" + storageOut.getId());
//
//	}
//
//	/**
//	 * 添加、修改出库申请
//	 * 
//	 * @return
//	 * @throws Exception
//	 */
//	@Action(value = "saveAjax")
//	public void saveAjax() throws Exception {
//		String id = getParameterFromRequest("id");
//		String json = getParameterFromRequest("data");
//		if (json != null && !json.equals(""))
//			saveStorageOutItem(id, json);
//		Map<String, Object> map = new HashMap<String, Object>();
//		map.put("success", true);
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
//
//	/**
//	 * 添加ByJson
//	 */
//	@Action(value = "saveByJson")
//	public String saveByJson() throws Exception {
//		String data = getParameterFromRequest("data");
//		String id = getRequest().getParameter("soId");
//
//		if (id == null || id.length() <= 0
//				|| SystemCode.DEFAULT_SYSTEMCODE.equals(id)) {
//			id = systemCodeService.getSystemCode(SystemCode.STORAGE_OUT_NAME,
//					SystemCode.STORAGE_OUT_CODE, null, null);
//		}
//		try {
//			pleaseVerifyPlanService.save(data, id);
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("message", id);
//			return this.renderText(JsonUtils.toJsonString(map));
//		} catch (Exception e) {
//			e.printStackTrace();
//			throw new Exception(e);
//		}
//	}
//
//	/**
//	 * 出库申请明细数据
//	 * 
//	 * @throws Exception
//	 */
//	@Action(value = "getStorageOutItemList")
//	public void getStorageOutItemList() throws Exception {
//		String outId = super.getRequest().getParameter("outId");
//		String applyId = getParameterFromRequest("applyId");
//		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//
//		List<StorageOutItem> list = new ArrayList<StorageOutItem>();
//		Long count = new Long(0);
//		if (outId != null && outId.trim().length() > 0) {
//			Map<String, Object> result = this.pleaseVerifyPlanService
//					.findStorageOutItemList(outId, startNum, limitNum, dir,
//							sort);
//
//			list = (List<StorageOutItem>) result.get("result");
//			count = (Long) result.get("count");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("storage-id", "");
//			map.put("storage-name", "");
//			map.put("storage-type-id", "");
//			map.put("storage-type-name", "");
//			map.put("storage-studyType-name", "");
//			map.put("storage-jdeCode", "");
//			map.put("qcState", "");
//			map.put("serial", "");
//			map.put("code", "");
//			map.put("num", "");
//			map.put("storageNum", "");
//			map.put("price", "");
//			map.put("expireDate", "yyyy-MM-dd");
//			map.put("storage-unit-name", "");
//			map.put("position-id", "");
//			map.put("position-name", "");
//			map.put("note", "");
//			map.put("note2", "");
//
//			new SendData().sendDateJson(map, list, count,
//					ServletActionContext.getResponse());
//		}
//
//	}
//
//	/**
//	 * 出库明细对象
//	 * 
//	 * @return
//	 * @throws Exception
//	 */
//
//	public void showStorageOutItemList(String handleMethod, String outId)
//			throws Exception {
//
//		// String exttype = "";
//		// String extcol = "";
//		// // String editflag = "true";
//		// LinkedHashMap<String, String[]> map = new LinkedHashMap<String,
//		// String[]>();
//		// map.put("id", new String[] { "", "string", "", "ID", "150", "false",
//		// "true", "", "", "", "", "" });
//		// map.put("storage-name", new String[] { "", "string", "", "对象名",
//		// "200", "false", "false", "", "", "false", "",
//		// "" });
//		// map.put("storage-id",
//		// new String[] { "", "string", "", "对象编码", "200", "false", "false", "",
//		// "", "false", "", "" });
//		// map.put("storage-type-id", new String[] { "", "string", "", "库存类型ID",
//		// "150", "false", "true", "", "", "false",
//		// "", "" });
//		// map.put("storage-type-name", new String[] { "", "string", "", "库存类型",
//		// "150", "false", "false", "", "", "false",
//		// "", "" });
//		// map.put("storage-studyType-name", new String[] { "", "string", "",
//		// "库存类别", "150", "false", "false", "", "",
//		// "false", "", "" });
//		// map.put("storage-unit-name", new String[] { "", "string", "", "单位",
//		// "75", "false", "false", "", "", "false",
//		// "", "" });
//		// map.put("serial", new String[] { "", "string", "", "批次ID", "150",
//		// "false", "false", "", "", "true", "",
//		// "serial" });
//		// map.put("code", new String[] { "", "string", "", "批号", "150",
//		// "false", "false", "", "", "true", "", "serial" });
//		//
//		// map.put("price",
//		// new String[] { "", "string", "", "批次价格", "150", "false", "false", "",
//		// "", "true", "", "price" });
//		//
//		// map.put("storageNum",
//		// new String[] { "", "float", "", "库存数量", "150", "false", "false", "",
//		// "", "false", "", "" });
//		// map.put("num", new String[] { "", "float", "", "出库数量", "150",
//		// "false", "false", "", "", "true", "formatCss",
//		// "num" });
//		// map.put("position-id", new String[] { "", "string", "", "存储位置ID",
//		// "150", "false", "true", "", "", "false", "",
//		// "" });
//		// map.put("position-name", new String[] { "", "string", "", "存储位置",
//		// "150", "false", "false", "", "", "false", "",
//		// "" });
//		// // 生成ext用type字符串
//		// exttype = generalexttype(map);
//		// // 生成ext用col字符串
//		// extcol = generalextcol(map);
//		// putObjToContext("type", exttype);
//		// putObjToContext("col", extcol);
//		// putObjToContext("path",
//		// ServletActionContext.getRequest().getContextPath()
//		// + "/storage/out/getStorageOutItemList.action?outId=" + outId);
//		putObjToContext("outId", outId);
//		putObjToContext("handlemethod", handleMethod);
//
//	}
//
//	/**
//	 * 保存明细
//	 */
//	@Action(value = "saveStorageOutItem")
//	public void saveStorageOutItem() throws Exception {
//		String storageApplyId = getParameterFromRequest("outId");
//		String json = getParameterFromRequest("data");
//		pleaseVerifyPlanService.saveStorageOutItem(storageApplyId, json);
//	}
//
//	public void saveStorageOutItem(String storageApplyId, String json)
//			throws Exception {
//		pleaseVerifyPlanService.saveStorageOutItem(storageApplyId, json);
//	}
//
//	/**
//	 * 删除明细
//	 */
//	@Action(value = "delOutStorageItem")
//	public void delOutStorageItem() throws Exception {
//		Map<String, Object> map = new HashMap<String, Object>();
//		try {
//		String[] ids = getRequest().getParameterValues("ids[]");
//		pleaseVerifyPlanService.deStorageOutItem(ids);
//		map.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			map.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
//
//	@Action(value = "storageOutSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String storageOutSelect() throws Exception {
//
//		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
//		map.put("id", new String[] { "", "string", "", "出库单号", "150", "true",
//				"", "", "", "", "", "" });
//		map.put("type-name", new String[] { "", "string", "", "出库类型", "150",
//				"true", "", "", "", "", "", "" });
//		map.put("storageApply-id", new String[] { "", "string", "", "领用单号",
//				"150", "false", "", "", "", "", "", "" });
//		map.put("useUser-name", new String[] { "", "string", "", "领用人", "75",
//				"false", "", "", "", "", "", "" });
//		map.put("outUser-name", new String[] { "", "string", "", "出库人", "75",
//				"true", "", "", "", "", "", "" });
//		map.put("outDate", new String[] { "", "string", "", "出库日期", "80",
//				"true", "", "", "", "", "", "" });
//		map.put("department-id", new String[] { "", "string", "", "组织ID",
//				"100", "false", "", "", "", "", "", "" });
//		map.put("department-name", new String[] { "", "string", "", "所属组织",
//				"100", "false", "", "", "", "", "", "" });
//
//		String type = generalexttype(map);
//		String col = generalextcol(map);
//		super.getRequest().setAttribute("type", type);
//		super.getRequest().setAttribute("col", col);
//
//		super.getRequest().setAttribute(
//				"path",
//				super.getRequest().getContextPath()
//						+ "/storage/out/storageOutSelectJson.action");
//		return dispatcher("/WEB-INF/page/storage/out/storageOutSelect.jsp");
//	}
//
//	/**
//	 * 出库申请DataList
//	 * 
//	 * @throws Exception
//	 */
//	@Action(value = "storageOutSelectJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void storageOutSelectJson() throws Exception {
//		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//
//		String data = getParameterFromRequest("data");
//
//		Map<String, Object> result = this.pleaseVerifyPlanService.findStorageOutList(
//				startNum, limitNum, dir, sort, data);
//		Long count = (Long) result.get("totalCount");
//		List<StorageOut> list = (List<StorageOut>) result.get("list");
//		Map<String, String> map = new HashMap<String, String>();
//		map.put("id", "");
//		map.put("type-name", "");
//		map.put("storageApply-id", "");
//		map.put("useUser-name", "");
//		map.put("outUser-name", "");
//		map.put("outDate", "yyyy-MM-dd");
//		map.put("department-id", "");
//		map.put("department-name", "");
//
//		new SendData().sendDateJson(map, list, count,
//				ServletActionContext.getResponse());
//	}
//
//	/**
//	 * 查询是否有员工信息
//	 * */
//	@Action(value = "searchUser", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void searchUser() throws Exception {
//		String id = getParameterFromRequest("id");
//		Map<String, Object> data = new HashMap<String, Object>();
//		try {
//			User user = pleaseVerifyPlanService.get(User.class, id);
//			if (user == null) {
//				data.put("message", true);
//			} else {
//				data.put("message", false);
//			}
//		} catch (Exception e) {
//
//		}
//		String json = JsonUtils.toJsonString(data);
//		HttpUtils.write(json);
//
//	}
//
//	public String getRightsId() {
//		return rightsId;
//	}
//
//	public void setRightsId(String rightsId) {
//		this.rightsId = rightsId;
//	}
//
//
//	public StorageOut getStorageOut() {
//		return storageOut;
//	}
//
//	public void setStorageOut(StorageOut storageOut) {
//		this.storageOut = storageOut;
//	}
//
//	public String getTitle() {
//		return title;
//	}
//
//	public void setTitle(String title) {
//		this.title = title;
//	}
//
//	/**
//	 * 
//	 * */
//	@Action(value = "addStorageItembyNote", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void addStorageItembyNote() throws Exception {
//		String note = getParameterFromRequest("note");
//		Map<String, Object> data = new HashMap<String, Object>();
//		List<StorageReagentBuySerial> sr = new ArrayList<StorageReagentBuySerial>();
//		try {
//			data = pleaseVerifyPlanService.addStorageItembyNote(note);
//			
//			if (data.size()>0) {
//				data.put("success", true);
//			} else {
//				data.put("message", false);
//			}
//		} catch (Exception e) {
//			data.put("message", false);
//		}
//		String json = JsonUtils.toJsonString(data);
//		HttpUtils.write(json);
//
//	}
//	
//	
//	/**
//	 * 
//	 * @Title: showStorageOutItemTableJson  
//	 * @Description: TODO(这里用一句话描述这个方法的作用)  
//	 * @author qi.yan
//	 * @date 2018-4-3上午10:25:51
//	 * @throws Exception
//	 * void
//	 * @throws
//	 */
//	@Action(value = "showStorageOutItemTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showStorageOutItemTableJson() throws Exception {
//		String query = getParameterFromRequest("query");
//		String colNum = getParameterFromRequest("order[0][column]");
//		String col = getParameterFromRequest("columns[" + colNum + "][data]");
//		String sort = getParameterFromRequest("order[0][dir]");
//		Integer start = Integer.valueOf(getParameterFromRequest("start"));
//		Integer length = Integer.valueOf(getParameterFromRequest("length"));
//		String draw = getParameterFromRequest("draw");
//		try {
//			String scId = getRequest().getParameter("id");
//			Map<String, Object> result = pleaseVerifyPlanService
//					.selStorageOutItemTable(scId, start, length, query, col,
//							sort);
//			List<StorageOutItem> list = (List<StorageOutItem>) result
//					.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("storage-id", "");
//			map.put("storage-name", "");
//			map.put("storage-barCode", "");
//			map.put("storage-spec", "");
//			map.put("storage-type-id", "");
//			map.put("storage-type-name", "");
//			map.put("storage-studyType-name", "");
//			map.put("storage-jdeCode", "");
//			map.put("qcState", "");
//			map.put("serial", "");
//			map.put("code", "");
//			map.put("num", "");
//			map.put("storageNum", "");
//			map.put("price", "");
//			map.put("expireDate", "yyyy-MM-dd");
//			map.put("storage-unit-name", "");
//			map.put("position-id", "");
//			map.put("position-name", "");
//			map.put("note", "");
//			map.put("note2", "");
//			map.put("batchNum", "");
//			String data = new SendData().getDateJsonForDatatable(map, list);
//			HttpUtils.write(PushData.pushData(draw, result, data));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//	/**
//	 * 
//	 * @Title: saveStorageOutItemTable  
//	 * @Description: TODO(这里用一句话描述这个方法的作用)  
//	 * @author qi.yan
//	 * @date 2018-4-3上午10:32:40
//	 * @throws Exception
//	 * void
//	 * @throws
//	 */
//	@Action(value = "saveStorageOutItemTable")
//	public void saveStorageOutItemTable() throws Exception {
//		String storageOut_id = getParameterFromRequest("id");
//		String item = getParameterFromRequest("dataJson");
//		String logInfo=getParameterFromRequest("logInfo");
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			pleaseVerifyPlanService.saveStorageOutItemTable(storageOut_id, item,logInfo);
//			result.put("success", true);
//			result.put("id", storageOut_id);
//		} catch (Exception e) {
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}
//	
//	
//	/**
//	 * 
//	 * @Title: 
//	 * @Description:展示主表
//	 * @author
//	 * @date
//	 * @return
//	 * @throws Exception
//	 *             String
//	 * @throws
//	 */
//	@Action(value = "showPleaseVerifyPlanTable")
//	public String showPleaseVerifyPlanTable() throws Exception {
//		rightsId="10409";
//		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		return dispatcher("/WEB-INF/page/storage/pleaseverifyplan/pleaseVerifyPlanTable.jsp");
//	}
//	
//	
//	
//	@Action(value = "showPleaseVerifyPlanTableJson")
//	public void showPleaseVerifyPlanTableJson() throws Exception {
//		String query = getParameterFromRequest("query");
//		String colNum = getParameterFromRequest("order[0][column]");
//		String col = getParameterFromRequest("columns[" + colNum + "][data]");
//		String sort = getParameterFromRequest("order[0][dir]");
//		Integer start = Integer.valueOf(getParameterFromRequest("start"));
//		Integer length = Integer.valueOf(getParameterFromRequest("length"));
//		String draw = getParameterFromRequest("draw");
//		try {
//			Map<String, Object> result = pleaseVerifyPlanService.selStorageOutTable(
//					start, length, query, col, sort);
//			List<StorageOut11>1 list = (List<StorageOut>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("type-name", "");
//			map.put("storageApply-id", "");
//			map.put("useUser-id", "");
//			map.put("outUser-id", "");
//			map.put("useUser-name", "");
//			map.put("outUser-name", "");
//			map.put("outDate", "yyyy-MM-dd");
//			map.put("department-id", "");
//			map.put("department-name", "");
//			map.put("state", "");
//			map.put("stateName", "");
//			map.put("note", "");
//			map.put("scopeId", "");
//			map.put("scopeName", "");
//			String data = new SendData().getDateJsonForDatatable(map, list);
//			HttpUtils.write(PushData.pushData(draw, result, data));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//	
//	
//	/**
//	 * 
//	 * @Title: addStorageOutItem  
//	 * @Description:  添加主数据到出库明细表
//	 * @author qi.yan
//	 * @date 2018-4-9上午10:01:39
//	 * @throws Exception
//	 * void
//	 * @throws
//	 */
//	@Action(value = "addStorageOutItem")
//	public void addStorageOutItem() throws Exception {
//		String note = getParameterFromRequest("note");
//		String useUser = getParameterFromRequest("useUser");
//		String id = getParameterFromRequest("id");
//		String outDate = getParameterFromRequest("outDate");
//		String outUser = getParameterFromRequest("outUser");
//		String batchIds = getRequest().getParameter("batchIds");
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			String storageOutId = pleaseVerifyPlanService.addStorageOutItem(note,useUser,id,outDate,outUser,batchIds);
//			result.put("success", true);
//			result.put("data", storageOutId);
//		} catch (Exception e) {
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}
//	
//	
//	/**
//	 * 
//	 * @Title: saveStorageOutAndItem
//	 * @Description: 大保存方法
//	 * @author : 
//	 * @date 
//	 * @throws Exception
//	 *             void
//	 * @throws
//	 */
//	@Action(value = "saveStorageOutAndItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void saveStorageOutAndItem() throws Exception {
//		String dataValue = getParameterFromRequest("dataValue");
//		// 主表changeLog
//		String changeLog = getParameterFromRequest("changeLog");
//		// 子表changeLogItem
//		String changeLogItem = getParameterFromRequest("changeLogItem");
//		
//		String id = getParameterFromRequest("id");
//		String note = getParameterFromRequest("note");
//		String useUser = getParameterFromRequest("useUser");
//
//		String str = "[" + dataValue + "]";
//
//		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str,
//				List.class);
//
//		Map<String, Object> map = new HashMap<String, Object>();
//		try {
//
//			for (Map<String, Object> map1 : list) {
//				StorageOut so = new StorageOut();
//				Map aMap = new HashMap();
//				so = (StorageOut) commonDAO.Map2Bean(map1, so);
//				//保存主表信息
//				StorageOut newSo = pleaseVerifyPlanService.saveStorageOutById(so,id,note,useUser,changeLog);
//				
//				newSo.setScopeId((String) ActionContext.getContext().getSession()
//						.get("scopeId"));
//				newSo.setScopeName((String) ActionContext.getContext().getSession()
//						.get("scopeName"));
//
//				//保存子表信息
//				aMap.put("ImteJson",getParameterFromRequest("ImteJson"));
//				pleaseVerifyPlanService.saveStorageOutAndItem(newSo, aMap, changeLog, changeLogItem);
//				map.put("id",newSo.getId());
//			}
//			map.put("success", true);
//			
//		} catch (Exception e) {
//			map.put("success", false);
//			map.put("msg", e.getMessage());
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}

	
	
	
}
