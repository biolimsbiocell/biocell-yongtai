package com.biolims.storage.pleaseverifyplan.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.core.model.user.DicCostCenter;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicCurrencyType;
import com.biolims.storage.model.Storage;
import com.biolims.storage.position.model.StoragePosition;

/**
 * 请验计划明细
 *
 */
@Entity
@Table(name = "PLEASE_VERIFY_PLAN_ITEM")
public class PleaseVerifyPlanItem extends EntityDao<PleaseVerifyPlanItem> implements Serializable {

	private static final long serialVersionUID = -1191844750007521159L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PLEASE_VERIFY_PLAN")
	private PleaseVerifyPlan pleaseVerifyPlan;//请验计划主表

	@Column(name = "SERIAL", length = 32)
	private String serial;//批次编号

	@Column(name = "CODE", length = 32)
	private String code;//批次编号

	@Column(name = "NOTE",length=4000)
	private String note;//sn
	
	@Column(name = "NOTE_TWO")
	private String note2;//备注
	@Column(name = "QC_STATE")
	private String qcState;//QC状态
	@Column(name = "EXPIRE_DATE")
	private Date expireDate;//过期日期
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_ID")
	private Storage storage;//对象
	
	/** 批次内数量 */
	private String batchNum;
	
	
	/** 质检类型 */
	private String sampleDeteyionType;
	/** 质检项id */
	private String sampleDeteyionId;
	/** 质检项名称 */
	private String sampleDeteyionName;
	
	/** 是否提交 */
	private String submit;
	
	
	
	public String getSubmit() {
		return submit;
	}

	public void setSubmit(String submit) {
		this.submit = submit;
	}

	public String getSampleDeteyionType() {
		return sampleDeteyionType;
	}

	public void setSampleDeteyionType(String sampleDeteyionType) {
		this.sampleDeteyionType = sampleDeteyionType;
	}

	public String getSampleDeteyionId() {
		return sampleDeteyionId;
	}

	public void setSampleDeteyionId(String sampleDeteyionId) {
		this.sampleDeteyionId = sampleDeteyionId;
	}

	public String getSampleDeteyionName() {
		return sampleDeteyionName;
	}

	public void setSampleDeteyionName(String sampleDeteyionName) {
		this.sampleDeteyionName = sampleDeteyionName;
	}

	public Storage getStorage() {
		return storage;
	}

	public void setStorage(Storage storage) {
		this.storage = storage;
	}

	public PleaseVerifyPlan getPleaseVerifyPlan() {
		return pleaseVerifyPlan;
	}

	public void setPleaseVerifyPlan(PleaseVerifyPlan pleaseVerifyPlan) {
		this.pleaseVerifyPlan = pleaseVerifyPlan;
	}

	public String getBatchNum() {
		return batchNum;
	}

	public void setBatchNum(String batchNum) {
		this.batchNum = batchNum;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}


	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * @param note the note to set
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * @return the note2
	 */
	public String getNote2() {
		return note2;
	}

	/**
	 * @param note2 the note2 to set
	 */
	public void setNote2(String note2) {
		this.note2 = note2;
	}

	/**
	 * @return the qcState
	 */
	public String getQcState() {
		return qcState;
	}

	/**
	 * @param qcState the qcState to set
	 */
	public void setQcState(String qcState) {
		this.qcState = qcState;
	}

	/**
	 * @return the expireDate
	 */
	public Date getExpireDate() {
		return expireDate;
	}

	/**
	 * @param expireDate the expireDate to set
	 */
	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

}
