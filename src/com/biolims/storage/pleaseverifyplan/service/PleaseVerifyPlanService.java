package com.biolims.storage.pleaseverifyplan.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.applicationType.service.ApplicationTypeService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.common.service.SystemCodeService;
import com.biolims.experiment.quality.model.QualityTestTemp;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.model.LogInfo;
import com.biolims.storage.common.constants.SystemConstants;
import com.biolims.storage.common.dao.StorageCommonDao;
import com.biolims.storage.main.service.StorageService;
import com.biolims.storage.model.DicStorageType;
import com.biolims.storage.model.Storage;
import com.biolims.storage.model.StorageApply;
import com.biolims.storage.model.StorageApplyItem;
import com.biolims.storage.model.StorageIn;
import com.biolims.storage.model.StorageInItem;
import com.biolims.storage.model.StorageOut;
import com.biolims.storage.model.StorageOutItem;
import com.biolims.storage.model.StorageReagentBuySerial;
import com.biolims.storage.out.dao.StorageOutDao;
import com.biolims.storage.pleaseverifyplan.dao.PleaseVerifyPlanDao;
import com.biolims.storage.pleaseverifyplan.model.PleaseVerifyPlan;
import com.biolims.storage.pleaseverifyplan.model.PleaseVerifyPlanItem;
import com.biolims.system.detecyion.model.SampleDeteyion;
import com.biolims.util.BeanUtils;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@Transactional
@SuppressWarnings({ "unchecked" })
public class PleaseVerifyPlanService extends ApplicationTypeService {

	@Resource
	private PleaseVerifyPlanDao pleaseVerifyPlanDao;

	@Resource
	private StorageCommonDao storageCommonDao;
	@Resource
	private StorageService storageService;
	
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CommonService commonService;
	
	@Resource
	private SystemCodeService systemCodeService;
	
	/**
	 * 
	 * 删除明细,由ext ajax调用
	 * 
	 * @param id
	 * @return void
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delPleaseVeriPlanItem(String[] ids) {
		for (String id : ids) {
			PleaseVerifyPlanItem ppi = commonDAO.get(PleaseVerifyPlanItem.class, id);
			if(ppi!=null){
				pleaseVerifyPlanDao.delete(ppi);
			}
			
			if (ids.length>0) {
				LogInfo li = new LogInfo();
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setLogDate(new Date());
				li.setUserId(u.getId());
				li.setFileId(ppi.getPleaseVerifyPlan().getId());
				li.setClassName("PleaseVerifyPlan");
				li.setModifyContent("请验计划:"+"原辅料编号:"+ppi.getStorage().getId()+"名称:"+ppi.getStorage().getName()+"的数据已删除");
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}
		}
	}
	
	
	public Map<String, Object> selPleaseVerifyPlanItemTable(String scId,
	Integer start, Integer length, String query, String col, String sort) throws Exception {
		return pleaseVerifyPlanDao.findPleaseVerifyPlanItemTable(scId, start, length, query,
			col, sort);
	}
		
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePleaseVerifyPlanItemTable(String id, String item,
			String logInfo) throws Exception {
		// 将json读入map
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(item,
				List.class);
		PleaseVerifyPlan ep = commonDAO.get(PleaseVerifyPlan.class,id);
		
		for (Map<String, Object> map : list) {
			PleaseVerifyPlanItem em = new PleaseVerifyPlanItem();

			// 将map信息读入实体类
			em = (PleaseVerifyPlanItem) commonDAO.Map2Bean(map, em);
			if (em.getId() != null && em.getId().equals("")) {
				em.setId(null);
			}
//			if (em.getStorage() == null) {
//				Storage s = storageService.getStorageByName((String) map
//						.get("storage-name"));
//				em.setStorage(s);
//			}
			em.setPleaseVerifyPlan(ep);
			commonDAO.saveOrUpdate(em);
		}

		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setUserId(ep.getOutUser().getId());
			li.setFileId(ep.getId());
			li.setClassName("PleaseVerifyPlan");
			li.setModifyContent(logInfo);
			li.setState("3");
			li.setStateName("数据修改");
			commonDAO.saveOrUpdate(li);
		}

	}
	
	public void savePleaseVerifyPlanAndItem(PleaseVerifyPlan newSo, Map jsonMap,
	String logInfo, String logInfoItem) throws Exception {
		if (newSo != null) {
			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(newSo.getId());
				li.setClassName("PleaseVerifyPlan");
				li.setModifyContent(logInfo);
				li.setState("3");
				li.setStateName("数据修改");
				commonDAO.saveOrUpdate(li);
			}
		
			String jsonStr = "";
			jsonStr = (String) jsonMap.get("ImteJson");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveItem(newSo, jsonStr, logInfoItem);
			}
		}
	}
	
	private void saveItem(PleaseVerifyPlan newSo, String itemDataJson, String logInfo) throws Exception {
		List<PleaseVerifyPlanItem> saveItems = new ArrayList<PleaseVerifyPlanItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			PleaseVerifyPlanItem scp = new PleaseVerifyPlanItem();
			// 将map信息读入实体类
			scp = (PleaseVerifyPlanItem) commonDAO.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setPleaseVerifyPlan(newSo);
	
			saveItems.add(scp);
		}
		commonDAO.saveOrUpdateAll(saveItems);
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(newSo.getId());
			li.setClassName("PleaseVerifyPlan");
			li.setModifyContent(logInfo);
			li.setState("3");
			li.setStateName("数据修改");
			commonDAO.saveOrUpdate(li);
		}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public PleaseVerifyPlan savePleaseVerifyPlanById(PleaseVerifyPlan so, String id, String note,
			 String logInfo) {//String useUserId,
		PleaseVerifyPlan soo = commonDAO.get(PleaseVerifyPlan.class, id);
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(soo.getId());
			li.setClassName("PleaseVerifyPlan");
			li.setModifyContent(logInfo);
			if("NEW".equals(id)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			commonDAO.saveOrUpdate(li);
		}
		soo.setNote(note);
//		User useUser = commonDAO.get(User.class, useUserId);
//		soo.setUseUser(useUser);
		commonDAO.saveOrUpdate(soo);
		return soo;
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String addPleaseVerifyPlanItem(String note,  String id,
			String outDate, String outUserId, String batchIds) throws Exception {

		// 保存主表信息
		PleaseVerifyPlan so = new PleaseVerifyPlan();
		String log = "";
		if (id == null || "".equals(id)
				|| "NEW".equals(id)) {
			log = "123";
			String code = systemCodeService.getCodeByPrefix("PleaseVerifyPlan", "PV"
					+ DateUtil.dateFormatterByPattern(new Date(), "yyMMdd"),
					000, 3, null);
			so.setId(code);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			User outUser = commonDAO.get(User.class, outUserId);
			so.setOutDate(sdf.parse(outDate));
			so.setOutUser(outUser);
			so.setState("3");
			so.setStateName("新建");
			so.setNote(note);
			id = so.getId();
			commonDAO.saveOrUpdate(so);
		}else{
			so = commonDAO.get(PleaseVerifyPlan.class, id);
		}
		String[] batchId =batchIds.split(","); 
		for (int i = 0; i < batchId.length; i++) {
			String batch = batchId[i];
			// 通过id查询库存主数据
			StorageReagentBuySerial srbs = commonDAO.get(StorageReagentBuySerial.class, batch);
			Storage s = commonDAO.get(Storage.class, srbs.getStorage().getId());
			PleaseVerifyPlanItem soi = new PleaseVerifyPlanItem();
			soi.setSerial(srbs.getId());
			soi.setCode(srbs.getSerial());
			if(srbs.getNum()!=null){
				soi.setBatchNum(srbs.getNum().toString());	
			}
			soi.setNote(srbs.getNote());
			soi.setNote2(srbs.getNote2());
			soi.setPleaseVerifyPlan(so);
			if(s!=null){
				soi.setStorage(s);
			}
			commonDAO.saveOrUpdate(soi);
			
			String kucun = "请验计划:" + "原辅料编号:" + s.getId() + "名称:" + s.getName() + "的数据已添加到明细";
			if (kucun != null && !"".equals(kucun)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setClassName("PleaseVerifyPlan");
				li.setFileId(id);
				li.setModifyContent(kucun);
				if("123".equals(log)) {
					li.setState("1");
					li.setStateName("数据新增");
				}else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				commonDAO.saveOrUpdate(li);
			}
		}
		return id;

	}
	
	/**
	 * 根据ID查询指定的请验计划
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public PleaseVerifyPlan findPleaseVerifyPlanById(String id) throws Exception {
		return this.pleaseVerifyPlanDao.get(PleaseVerifyPlan.class, id);
	}
	
	public Map<String, Object> selPleaseVerifyPlanTable(Integer start,
	Integer length, String query, String col, String sort) throws Exception {
		return pleaseVerifyPlanDao.findPleaseVerifyPlanTable(start, length, query, col, sort);
	}
	/**
	 * 请验计划完成
	 * 
	 * @param so
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String so, String data)
			throws Exception {
		if(data!=null
				&&!"".equals(data)){
			PleaseVerifyPlan pvp = commonDAO.get(PleaseVerifyPlan.class, data);
			pvp.setState("1");
			pvp.setStateName("完成");
			commonDAO.saveOrUpdate(pvp);
			List<PleaseVerifyPlanItem> pvpis = commonService.get(PleaseVerifyPlanItem.class, "pleaseVerifyPlan.id", data);
			if(pvpis.size()>0){
				for(PleaseVerifyPlanItem scp:pvpis){
					if("1".equals(scp.getSubmit())){
						
					}else{
						QualityTestTemp qtt = new QualityTestTemp();
						qtt.setId(null);
						qtt.setStorage(scp.getStorage());
						qtt.setCode(scp.getCode());
						qtt.setSerial(scp.getSerial());
						qtt.setState("1");
						if("1".equals(scp.getSampleDeteyionType())){
							qtt.setCellType("3");
						}else if("2".equals(scp.getSampleDeteyionType())){
							qtt.setCellType("7");
						}
						SampleDeteyion sd = commonDAO.get(SampleDeteyion.class, scp.getSampleDeteyionId());
						if(sd!=null){
							qtt.setSampleDeteyion(sd);
						}
						commonDAO.saveOrUpdate(qtt);
					}
				}
			}
		}
	}

//	/**
//	 * 领用申请列表
//	 * 
//	 * @param startNum
//	 * @param limitNum
//	 * @param dir
//	 * @param sort
//	 * @param so
//	 * @return
//	 * @throws Exception
//	 */
//	public Map<String, Object> findStorageOutList(int startNum, int limitNum,
//			String dir, String sort, String data) throws Exception {
//		// 检索条件map,每个字段检索值
//		Map<String, Object> mapForQuery = new HashMap<String, Object>();
//		// 检索方式map,每个字段的检索方式
//		Map<String, String> mapForCondition = new HashMap<String, String>();
//		String startDate = "";
//		String endDate = "";
//		if (data != null && !data.equals("")) {
//			BeanUtils.data2Map(data, mapForQuery, mapForCondition);
//			mapForQuery.remove("queryStartDate");
//			mapForQuery.remove("queryEndDate");
//			mapForCondition.remove("queryStartDate");
//			mapForCondition.remove("queryEndDate");
//		}
//		Map<String, Object> criteriaMap = storageOutDao.findObjectCriteria(
//				startNum, limitNum, dir, sort, StorageOut.class, mapForQuery,
//				mapForCondition);
//		Criteria cr = (Criteria) criteriaMap.get("criteria");
//		Criteria cc = (Criteria) criteriaMap.get("criteriaCount");
//		Map<String, Object> map = JsonUtils.toObjectByJson(data, Map.class);
//		if (data != null && !data.equals("")) {
//			if (map.get("queryStartDate") != null
//					&& !map.get("queryStartDate").equals("")) {
//				startDate = (String) map.get("queryStartDate");
//				cr.add(Restrictions.ge("outDate", DateUtil.parse(startDate)));
//				cc.add(Restrictions.ge("outDate", DateUtil.parse(startDate)));
//			}
//			if (map.get("queryEndDate") != null
//					&& !map.get("queryEndDate").equals("")) {
//				endDate = (String) map.get("queryEndDate");
//				cr.add(Restrictions.le("outDate", DateUtil.parse(endDate)));
//				cc.add(Restrictions.le("outDate", DateUtil.parse(endDate)));
//			}
//		}
//		Map<String, Object> controlMap = storageOutDao.findObjectList(startNum,
//				limitNum, dir, sort, StorageOut.class, mapForQuery,
//				mapForCondition, cc, cr);
//		return controlMap;
//	}
//
//	/**
//	 * 根据ID查询指定的出库申请
//	 * 
//	 * @param id
//	 * @return
//	 * @throws Exception
//	 */
//	public StorageOut findStorageOutById(String id) throws Exception {
//		return this.storageOutDao.get(StorageOut.class, id);
//	}
//
//	/**
//	 * 保存修改出库申请
//	 * 
//	 * @param so
//	 * @throws Exception
//	 */
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void saveOrModifyStorageOut(StorageOut so, String data)
//			throws Exception {
//		this.storageOutDao.insertOrUpdateStorageOut(so);
//		this.saveOrModifyStorageOutItem(so.getId(), data);
//	}
//
//	/**
//	 * 保存修改出库申请
//	 * 
//	 * @param so
//	 * @throws Exception
//	 */
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void save(StorageOut so) throws Exception {
//		this.storageOutDao.insertOrUpdateStorageOut(so);
//	}
//
//	/**
//	 * 保存ByJSon
//	 */
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
//	public void save(String jsonString, String outId) throws Exception {
//		jsonString = jsonString.replaceAll("so\\.", "");
//		jsonString = jsonString.replaceAll("\\.id", "-id");
//		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
//				jsonString, List.class);
//		for (Map<String, Object> map : list) {
//			StorageOut em = new StorageOut();
//			// 将map信息读入实体类
//			em = (StorageOut) storageOutDao.Map2Bean(map, em);
//			if (outId != null)
//				em.setId(outId);
//			storageOutDao.saveOrUpdate(em);
//		}
//	}
//
//	/**
//	 * 出库明细对象
//	 * 
//	 * @param applyId
//	 * @param startNum
//	 * @param limitNum
//	 * @param dir
//	 * @param sort
//	 * @return
//	 */
//	public Map<String, Object> findStorageOutItemList(String outId,
//			Integer startNum, Integer limitNum, String dir, String sort)
//			throws Exception {
//		Map<String, Object> map = new HashMap<String, Object>();
//		map = this.storageOutDao.selectStorageOutItemList(outId, startNum,
//				limitNum, dir, sort);
//		List<StorageOutItem> list = (List<StorageOutItem>) map.get("result");
//		map.put("result", list);
//		return map;
//	}
//
//	public List<StorageOutItem> findStorageOutItemList(String outId)
//			throws Exception {
//		List<StorageOutItem> list = this.storageOutDao
//				.findStorageOutItemList(outId);
//		return list;
//	}
//
//	/**
//	 * 出库明细对象
//	 * 
//	 * @param applyId
//	 * @param startNum
//	 * @param limitNum
//	 * @param dir
//	 * @param sort
//	 * @return
//	 */
//
//	/**
//	 * 查询所有领用申请
//	 * 
//	 * @param startNum
//	 * @param limitNum
//	 * @return
//	 */
//	public Map<String, Object> findAllStorageApply(Integer startNum,
//			Integer limitNum) {
//		Map<String, Object> map = this.storageOutDao.selectAllStorageApply(
//				startNum, limitNum);
//		if (map != null) {
//			List<StorageApply> list = (List<StorageApply>) map.get("result");
//			for (StorageApply sa : list) {
//				User useUser = sa.getUseUser();
//				if (useUser != null) {
//					sa.setUseUserId(useUser.getId());
//					sa.setUseUserName(useUser.getName());
//				}
//			}
//		}
//		return map;
//	}
//
//	/**
//	 * 领用明细
//	 * 
//	 * @param applyId
//	 * @param startNum
//	 * @param limitNum
//	 * @param dir
//	 * @param sort
//	 * @return
//	 * @throws Exception
//	 */
//	public Map<String, Object> findStorageApplyItemList(String applyId,
//			Integer startNum, Integer limitNum, String dir, String sort)
//			throws Exception {
//		Map<String, Object> map = new HashMap<String, Object>();
//
//		StorageApplyItem sai = this.storageOutDao
//				.selectStorageApplyItemByApplyId(applyId);
//		if (sai != null) {
//			Storage s = sai.getStorage();
//			DicStorageType dst = s.getType();
//			map = this.storageOutDao.selectStorageApplyItemList(dst.getId(),
//					s.getId(), startNum, limitNum, dir, sort);
//
//		}
//		return map;
//	}
//
//	/**
//	 * 保存或添加出库明细数据
//	 * 
//	 * @param outId
//	 * @param data
//	 * @throws Exception
//	 */
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void saveOrModifyStorageOutItem(String outId, String data)
//			throws Exception {
//		this.storageOutDao.delStorageOutItemByOutId(outId);
//
//	}
//
//	/**
//	 * 维护明细信息,由ext ajax调用
//	 */
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public String saveStorageOutItem(String outId, String jsonString)
//			throws Exception {
//		// 将json读入map
//		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
//				jsonString, List.class);
//		for (Map<String, Object> map : list) {
//			StorageOutItem em = new StorageOutItem();
//			// 将map信息读入实体类
//			em = (StorageOutItem) storageOutDao.Map2Bean(map, em);
//			if (em.getId() != null && em.getId().equals("")) {
//				em.setId(null);
//			}
//			if (em.getNote() != null && !"".equals(em.getNote())) {
//				StorageReagentBuySerial sbs = storageService
//						.getReagentSerialBySn(em.getNote());
//				if(sbs != null){
//					em.setPosition(sbs.getPosition());
//					em.setSerial(sbs.getId());
//					em.setQcState(sbs.getQcState());
//					em.setCode(sbs.getSerial());
//					if (sbs.getStorage() != null) {
//						em.setStorageNum(sbs.getStorage().getNum());
//					}
//					em.setExpireDate(sbs.getExpireDate());
//					em.setStorage(sbs.getStorage());
//					Double d = 1.00;
//					em.setNum(d);
//				}
//			}
//			StorageOut ep = new StorageOut();
//			ep.setId(outId);
//			em.setStorageOut(ep);
//			storageOutDao.saveOrUpdate(em);
//
//		}
//		return jsonString;
//	}
//
//	/**
//	 * 删除出库申请中指定的明细数据
//	 * 
//	 * @param outId
//	 * @param storageId
//	 * @throws Exception
//	 */
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void delStorageOutItemByOutIdAndStorageId(String outId,
//			String storageId) throws Exception {
//		StorageOutItem soi = this.storageOutDao
//				.selectStorageOutItemByOutIdAndStorageId(outId, storageId);
//		this.storageOutDao.delete(soi);
//	}
//
//	/**
//	 * 
//	 * 删除明细,由ext ajax调用
//	 * 
//	 * @param id
//	 * @return void
//	 */
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void deStorageOutItem(String[] ids) {
//		for (String id : ids) {
//			StorageOutItem ppi = new StorageOutItem();
//			ppi.setId(id);
//			storageOutDao.delete(ppi);
//		}
//	}
//
//	/**
//	 * 
//	 * @param applicationTypeActionId
//	 * @param formId
//	 * @return
//	 * @throws Exception
//	 */
//	@Transactional(rollbackFor = Exception.class)
//	public boolean storageOutSetStorage(String applicationTypeActionId,
//			String formId) throws Exception {
//		StorageOut sc = this.storageOutDao.get(StorageOut.class, formId);
//		sc.setState("1");
//		sc.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_COMPLETE_NAME);
//		List<StorageOutItem> list = findStorageOutItemList(formId);
//		for (StorageOutItem em : list) {
//			Storage s = get(em.getStorage().getClass(), em.getStorage().getId());
//			Double nn = 0.0;
//			if (em.getNum() != null) {
//
//				// 库存数量=原数量-出库数量
//				if (s.getNum() != null) {
//					nn = s.getNum();
//					nn -= em.getNum();
//					s.setNum(nn);
//				}
//
//				// 领用数量=原数量-出库数量
//				Double l = 0.0;
//				if (s.getLyNum() != null) {
//					l = s.getLyNum();
//					s.setLyNum(l - em.getNum());
//				}
//
//			}
//			saveOrUpdate(sc);
//			saveOrUpdate(s);
//
//			if (SystemConstants.DIC_STORAGE_PRICE_METHOD.equals("moveAverage")) {
//				Double rn = 0.0;
//				Double srn = 0.0;
//				if (s.getType()
//						.getId()
//						.startsWith(
//								SystemConstants.DIC_STORAGE_SHIYANCAILIAO_YPC)) {
//					if (em.getSerial() != null && !em.getSerial().equals("")) {
//						StorageReagentBuySerial srb = get(
//								StorageReagentBuySerial.class, em.getSerial());
//
//						if (em.getNum() != null && srb.getNum() != null) {
//							rn = em.getNum();
//
//							srn = srb.getNum();
//							srb.setNum(srn - rn);
//
//						}
//						// 库存数量=原数量-出库数量
//
//						saveOrUpdate(srb);
//
//					}
//				}
//				if (s.getType()
//						.getId()
//						.startsWith(
//								SystemConstants.DIC_STORAGE_SHIYANCAILIAO_NPC)) {
//					this.updateStorageNPC(em);
//				}
//			}
//			if (SystemConstants.DIC_STORAGE_PRICE_METHOD.equals("groupPrice")) {
//				Double rn = 0.0;
//				Double srn = 0.0;
//				if (s.getType()
//						.getId()
//						.startsWith(
//								SystemConstants.DIC_STORAGE_SHIYANCAILIAO_YPC)) {
//					if (em.getSerial() != null && !em.getSerial().equals("")) {
//						StorageReagentBuySerial srb = get(
//								StorageReagentBuySerial.class, em.getSerial());
//
//						if (em.getNum() != null && srb.getNum() != null) {
//							rn = em.getNum();
//
//							srn = srb.getNum();
//							srb.setNum(srn - rn);
//							if (srb.getUseNum() != null)
//								srb.setUseNum(srb.getUseNum() + rn);
//							else
//								srb.setUseNum(rn);
//						}
//						// 库存数量=原数量-出库数量
//
//						saveOrUpdate(srb);
//
//					} else {
//						List<StorageReagentBuySerial> srbsListForSave = new ArrayList<StorageReagentBuySerial>();
//						// 批价格算法
//						Double emNum = em.getNum();
//
//						List<StorageReagentBuySerial> srbsList = storageCommonDao
//								.selectStorageReagentSerialByStorageId(em
//										.getStorage().getId());
//						for (StorageReagentBuySerial srbs : srbsList) {
//							if (srbs.getNum() > 0) {
//								if (srbs.getNum() > emNum) {
//									srbs.setNum(srbs.getNum() - em.getNum());
//									srbsListForSave.add(srbs);
//									break;
//								} else {
//									emNum = emNum - srbs.getNum();
//									srbs.setNum(0d);
//									srbsListForSave.add(srbs);
//								}
//
//							}
//						}
//						this.storageOutDao.saveOrUpdateAll(srbsListForSave);
//					}
//				}
//
//			}
//		}
//		return true;
//	}
//
//	public Double groupCompute(StorageOutItem soi) throws Exception {
//		Double price = null;
//		// 移动平均算法
//		Storage storage = this.get(Storage.class, soi.getStorage().getId());
//		// 批次算法
//		if (storage.getType().getId()
//				.startsWith(SystemConstants.DIC_STORAGE_SHIYANCAILIAO_YPC)) {
//			price = this.getPriceByYPC(soi);
//		} else if (storage.getType().getId()
//				.startsWith(SystemConstants.DIC_STORAGE_SHIYANCAILIAO_NPC)) {
//			price = this.getPriceByNPC(soi);
//		}
//		return price;
//	}
//
//	/**
//	 * 有批次-出库价格
//	 */
//	public Double getPriceByYPC(StorageOutItem soi) throws Exception {
//		Double price = 0d;
//		if (soi != null) {
//			Map<String, String> mapForQuery = new HashMap<String, String>();
//			mapForQuery.put("id", soi.getSerial());
//			StorageReagentBuySerial srbs = this.storageOutDao.get(
//					StorageReagentBuySerial.class, soi.getSerial());
//			price = srbs.getOutPrice();
//		}
//		return price;
//	}
//
//	/**
//	 * 无批次-计算出库单价
//	 */
//	public Double getPriceByNPC(StorageOutItem soi) throws Exception {
//		List<StorageReagentBuySerial> list = this.storageOutDao.findByProperty(
//				StorageReagentBuySerial.class, "storage.id", soi.getStorage()
//						.getId());
//		double totalPrice = 0d;
//		double outNum = soi.getNum();
//		if (outNum > 0) {
//			double price = 0d;
//			for (StorageReagentBuySerial srbs : list) {
//				double num = srbs.getNum();
//				double outPrice = srbs.getOutPrice();
//				if (outNum == 0) {
//					break;
//				} else if (outNum > 0 && outNum >= num) {
//					price = price + num * outPrice;
//					outNum -= num;
//				} else if (outNum > 0 && outNum < num) {
//					srbs.setNum(num - outNum);
//					price = price + outNum * outPrice;
//					outNum = 0;
//				}
//			}
//			totalPrice = price / soi.getNum();
//		}
//		return totalPrice;
//	}
//
//	/**
//	 * 无批次-动作
//	 * 
//	 * @param list
//	 * @param outNum
//	 */
//	public void updateStorageNPC(StorageOutItem soi) throws Exception {
//		List<StorageReagentBuySerial> list = this.storageOutDao.findByProperty(
//				StorageReagentBuySerial.class, "storage.id", soi.getStorage()
//						.getId());
//		List<StorageReagentBuySerial> srbslist = new ArrayList<StorageReagentBuySerial>();
//		double outNum = soi.getNum();
//		double price = 0d;
//		for (StorageReagentBuySerial srbs : list) {
//			double num = srbs.getNum();
//			double outPrice = srbs.getOutPrice();
//			if (outNum == 0) {
//				break;
//			} else if (outNum > 0 && outNum >= num) {
//				srbs.setNum(0d);
//				srbslist.add(srbs);
//				price = price + num * outPrice;
//				outNum -= num;
//			} else if (outNum > 0 && outNum < num) {
//				srbs.setNum(num - outNum);
//				srbslist.add(srbs);
//				price = price + outNum * outPrice;
//				outNum = 0;
//			}
//		}
//		this.storageOutDao.saveOrUpdateAll(srbslist);
//	}
//
//	/**
//	 * 根据sn查询出库明细
//	 * */
//	public StorageOutItem findStorageOutItem(String sn) {
//
//		return this.storageOutDao.findStorageOutItem(sn);
//
//	}
//
//	@Transactional(rollbackFor = Exception.class)
//	public void alertStorageOut(String id) throws Exception {
//		List<StorageOutItem> list = findStorageOutItemList(id);
//		StorageOut so=get(StorageOut.class, id);
//		so.setState("3");
//		so.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_EDIT_NAME);
//		for (StorageOutItem soi : list) {
//			Storage s = soi.getStorage();
//			Double nn = 0.0;
//			if (s.getNum() != null) {
//				nn = s.getNum();
//				nn += soi.getNum();
//				s.setNum(nn);
//			}
//			StorageReagentBuySerial srb = storageService
//					.getReagentSerialBySn(soi.getNote());
//			Double sn = 0.0;
//			if (srb.getNum() != null) {
//				sn = srb.getNum();
//				sn += soi.getNum();
//				srb.setNum(sn);
//			}
//			saveOrUpdate(so);
//			saveOrUpdate(srb);
//			saveOrUpdate(soi);
//			saveOrUpdate(s);
//		}
//	}
//	
//	public Map<String, Object> addStorageItembyNote(String note){
//		Map<String, Object> map = new HashMap<String, Object>();
//		StorageReagentBuySerial srbs = storageOutDao.addStorageItembyNote(note);
//		
//		if(srbs != null){
//			
//			map.put("storageId", srbs.getStorage().getId());
//			map.put("storageName", srbs.getStorage().getName());
//			map.put("storageUnitName", srbs.getStorage().getUnit().getName());
//			map.put("price", srbs.getStorage().getOutPrice());
////			map.put("storage-currencyType-name", srbs.getStorage().getCurrencyType().getName());
//			map.put("storageTypeId", srbs.getStorage().getType().getId());
//			map.put("storageTypeAame", srbs.getStorage().getType().getName());
//			map.put("storageNum", srbs.getStorage().getNum());
//			map.put("serial", srbs.getId());
//			map.put("code", srbs.getSerial());
//			map.put("qcState", srbs.getQcState());
//			map.put("positionId", srbs.getPosition().getId());
//			map.put("positionName", srbs.getPosition().getName());
//			map.put("price", srbs.getPurchasePrice());
//			map.put("storageJdeCode", srbs.getStorage().getJdeCode());
//			
//		}
//		return map;
//	}
//
//	public Map<String, Object> selStorageOutItemTable(String scId,
//			Integer start, Integer length, String query, String col, String sort) throws Exception {
//		return storageOutDao.findStorageOutItemTable(scId, start, length, query,
//				col, sort);
//	}
//
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void saveStorageOutItemTable(String id, String item,
//			String logInfo) throws Exception {
//		// 将json读入map
//		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(item,
//				List.class);
//		StorageOut ep = commonDAO.get(StorageOut.class,id);
//		
//		for (Map<String, Object> map : list) {
//			StorageOutItem em = new StorageOutItem();
//
//			// 将map信息读入实体类
//			em = (StorageOutItem) commonDAO.Map2Bean(map, em);
//			if (em.getId() != null && em.getId().equals("")) {
//				em.setId(null);
//			}
////			if (em.getStorage() == null) {
////				Storage s = storageService.getStorageByName((String) map
////						.get("storage-name"));
////				em.setStorage(s);
////			}
//			em.setStorageOut(ep);
//			commonDAO.saveOrUpdate(em);
//		}
//
//		if (logInfo != null && !"".equals(logInfo)) {
//			LogInfo li = new LogInfo();
//			li.setLogDate(new Date());
//			li.setUserId(ep.getUseUser().getId());
//			li.setFileId(id);
//			li.setModifyContent(logInfo);
//			commonDAO.saveOrUpdate(li);
//		}
//
//	}
//
//	public Map<String, Object> selStorageOutTable(Integer start,
//			Integer length, String query, String col, String sort) throws Exception {
//		return storageOutDao.findStorageOutTable(start, length, query, col, sort);
//	}
//	
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public String addStorageOutItem(String note, String useUserId, String id,
//			String outDate, String outUserId, String batchIds) throws Exception {
//
//		// 保存主表信息
//		StorageOut so = new StorageOut();
//		if (id == null || "".equals(id)
//				|| "NEW".equals(id)) {
//			String code = systemCodeService.getCodeByPrefix("StorageOut", "ME"
//					+ DateUtil.dateFormatterByPattern(new Date(), "yyMMdd"),
//					000, 3, null);
//			so.setId(code);
//			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//			User useUser = commonDAO.get(User.class, useUserId);
//			User outUser = commonDAO.get(User.class, outUserId);
//			so.setUseUser(useUser);
//			so.setOutDate(sdf.parse(outDate));
//			so.setOutUser(outUser);
//			so.setState("3");
//			so.setStateName("NEW");
//			so.setNote(note);
//			id = so.getId();
//			commonDAO.saveOrUpdate(so);
//		}else{
//			so = commonDAO.get(StorageOut.class, id);
//		}
//		String[] batchId =batchIds.split(","); 
//		for (int i = 0; i < batchId.length; i++) {
//			String batch = batchId[i];
//			// 通过id查询库存主数据
//			StorageReagentBuySerial srbs = commonDAO.get(StorageReagentBuySerial.class, batch);
//			Storage s = commonDAO.get(Storage.class, srbs.getStorage().getId());
//			StorageOutItem soi = new StorageOutItem();
//			soi.setStorage(srbs.getStorage());
//			soi.setSerial(srbs.getId());
//			soi.setCode(srbs.getSerial());
//			soi.setStorageNum(s.getNum());
//			if(srbs.getNum()!=null){
//				soi.setBatchNum(srbs.getNum().toString());	
//			}
//			soi.setNote(srbs.getNote());
//			soi.setNote2(srbs.getNote2());
//			soi.setStorageOut(so);
//			commonDAO.saveOrUpdate(soi);
//		}
//		return id;
//
//	}
//
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public StorageOut saveStorageOutById(StorageOut so, String id, String note,
//			String useUserId, String logInfo) {
//		StorageOut soo = commonDAO.get(StorageOut.class, id);
//		if (logInfo != null && !"".equals(logInfo)) {
//			LogInfo li = new LogInfo();
//			li.setLogDate(new Date());
//			User u = (User) ServletActionContext.getRequest().getSession()
//					.getAttribute(SystemConstants.USER_SESSION_KEY);
//			li.setUserId(u.getId());
//			li.setFileId(soo.getId());
//			li.setModifyContent(logInfo);
//			commonDAO.saveOrUpdate(li);
//		}
//		soo.setNote(note);
//		User useUser = commonDAO.get(User.class, useUserId);
//		soo.setUseUser(useUser);
//		commonDAO.saveOrUpdate(soo);
//		return soo;
//	}
//
//	public void saveStorageOutAndItem(StorageOut newSo, Map jsonMap,
//			String logInfo, String logInfoItem) throws Exception {
//		if (newSo != null) {
//			if (logInfo != null && !"".equals(logInfo)) {
//				LogInfo li = new LogInfo();
//				li.setLogDate(new Date());
//				User u = (User) ServletActionContext.getRequest().getSession()
//						.getAttribute(SystemConstants.USER_SESSION_KEY);
//				li.setUserId(u.getId());
//				li.setFileId(newSo.getId());
//				li.setModifyContent(logInfo);
//				commonDAO.saveOrUpdate(li);
//			}
//
//			String jsonStr = "";
//			jsonStr = (String) jsonMap.get("ImteJson");
//			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
//				saveItem(newSo, jsonStr, logInfoItem);
//			}
//		}
//	}
//
//	private void saveItem(StorageOut newSo, String itemDataJson, String logInfo) throws Exception {
//		List<StorageOutItem> saveItems = new ArrayList<StorageOutItem>();
//		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
//				itemDataJson, List.class);
//		for (Map<String, Object> map : list) {
//			StorageOutItem scp = new StorageOutItem();
//			// 将map信息读入实体类
//			scp = (StorageOutItem) commonDAO.Map2Bean(map, scp);
//			if (scp.getId() != null && scp.getId().equals(""))
//				scp.setId(null);
//			scp.setStorageOut(newSo);
//
//			saveItems.add(scp);
//		}
//		commonDAO.saveOrUpdateAll(saveItems);
//		if (logInfo != null && !"".equals(logInfo)) {
//			LogInfo li = new LogInfo();
//			li.setLogDate(new Date());
//			User u = (User) ServletActionContext.getRequest().getSession()
//					.getAttribute(SystemConstants.USER_SESSION_KEY);
//			li.setUserId(u.getId());
//			li.setFileId(newSo.getId());
//			li.setModifyContent(logInfo);
//			commonDAO.saveOrUpdate(li);
//		}
//	}
}
