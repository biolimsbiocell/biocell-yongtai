package com.biolims.storage.container.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.biolims.dao.EntityDao;

/**
 * 容器
 * @author cong
 *
 */
@Entity
@Table(name = "T_STORAGE_CONTAINER")
public class StorageContainer extends EntityDao<StorageContainer> implements Serializable {

	private static final long serialVersionUID = -2754374659924606187L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@Column(name = "ROW_NUM", length = 3)
	private Integer rowNum;//行数
	@Column(name = "COL_NUM", length = 3)
	private Integer colNum;//列数

	@Column(name = "NAME", length = 100)
	private String name;//容器名称

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getRowNum() {
		return rowNum;
	}

	public void setRowNum(Integer rowNum) {
		this.rowNum = rowNum;
	}

	public Integer getColNum() {
		return colNum;
	}

	public void setColNum(Integer colNum) {
		this.colNum = colNum;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
