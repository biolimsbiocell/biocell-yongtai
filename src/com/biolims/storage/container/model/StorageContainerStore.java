package com.biolims.storage.container.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.storage.position.model.StoragePosition;

/**
 * 容器存储表
 * @author cong
 *
 */
@Entity
@Table(name = "T_STORAGE_CONTAINER_STORE")
public class StorageContainerStore extends EntityDao<StorageContainerStore> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1240725903184277613L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID")
	private String id;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_POSITION_ID")
	private StoragePosition position;//存储位置

	/*
	 * [{'num':'(1,1)','itemId':'XXXXXXXXX','objName':'Storage','itemName':'XXXXX','ldDate':'XXXXXX'},{}...]
	 */
	@Column(name = "INFO", length = 2000)
	private String info;//存储容器信息

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public StoragePosition getPosition() {
		return position;
	}

	public void setPosition(StoragePosition position) {
		this.position = position;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

}
