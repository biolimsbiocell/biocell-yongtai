package com.biolims.storage.container.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.dic.model.DicType;
import com.biolims.equipment.model.Instrument;
import com.biolims.experiment.plasma.model.PlasmaTask;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.storage.model.SampleInItem;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.sample.storage.model.SampleInfoIn;
import com.biolims.storage.container.model.StorageContainer;
import com.biolims.storage.container.model.StorageContainerStore;
import com.biolims.storage.position.dao.StoragePositionDao;
import com.biolims.storage.position.model.StoragePosition;
import com.biolims.system.storage.model.StorageBox;
import com.biolims.system.template.model.Template;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class ContainerDao extends BaseHibernateDao {

	@Resource
	private StoragePositionDao storagePositionDao;

	/*
	 * public Map<String, Object> selectContainer(String name, int start, int
	 * limit, String dir, String sort) {
	 * 
	 * String hql = "from StorageContainer where 1=1"; if (name != null &&
	 * name.length() > 0) hql = hql + " and name like '" + name + "'"; if (dir
	 * != null && dir.length() > 0 && sort != null && sort.length() > 0) hql =
	 * hql + " order by " + sort + " " + dir;
	 * 
	 * List<StorageContainer> dataList = new ArrayList<StorageContainer>(); Long
	 * count = (Long) this.getSession() .createQuery("select count(*)" +
	 * hql).uniqueResult(); if (count > 0) { dataList =
	 * this.getSession().createQuery(hql).setFirstResult(start)
	 * .setMaxResults(limit).list(); } Map<String, Object> map = new
	 * HashMap<String, Object>(); map.put("count", count); map.put("dataList",
	 * dataList); return map; }
	 */
	public Map<String, Object> selectContainer(String classify, Integer start,
			Integer length, String query, String col, String sort)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from StorageContainer where 1=1";
		String key = "";
		if (query != null && !"".equals(query)) {
			key = map2Where(query);
		}
		// String scopeId=(String)
		// ActionContext.getContext().getSession().get("scopeId");
		// if(!"all".equals(scopeId)){
		// key+=" and scopeId='"+scopeId+"'";
		// }
		/*
		 * if("1".equals(classify)){ key+=
		 * " and state='2' and techJkServiceTask is null"; }else{ key+=
		 * " and state='2'"; }
		 */
		Long sumCount = (Long) getSession().createQuery(countHql + key)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from StorageContainer where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<DicType> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public StorageContainerStore selectStorageContainerStore(String posiId,
			String contId) {
		// String hql = "from StorageContainerStore where position.id='" +
		// posiId + "' and position.storageContainer.id='"
		// + contId + "'";
		String hql = "from StorageContainerStore where position.id='" + posiId
				+ "'";
		return (StorageContainerStore) this.getSession().createQuery(hql)
				.uniqueResult();
	}

	/**
	 * 查询存储位置上每个格子的存储内容
	 * 
	 * @param id
	 *            存储位置ID
	 * @param row
	 *            格子所在的行
	 * @param col
	 *            格子所在的列
	 * @param type
	 *            存储容器存放的类型
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findMainStorage(String id, int row, int col) {
		List<Map<String, Object>> list = null;
		Map<String, Object> m = new HashMap();
		String mainId = findMainId(id, row, col);
		String hql = "from SampleInfoIn m where m.location='" + mainId
				+ "' and (state = '1' or state = '2')";
		List<SampleInfoIn> l = this.getSession().createQuery(hql).list();

		// if (!mainId.equals("0") && mainId.length() > 1) {
		// sql = "select m.CODE as mid from SAMPLE_IN_ITEM m where m.LOCATION='"
		// + mainId + "' and STATE = '1'";
		//
		// list = this.findForJdbc(sql);
		// if (list.size() > 0)
		// return list.get(0);
		//
		// }
		if (l.size() > 0) {
			m.put("mid", l.get(0).getCode());
			m.put("info", l.get(0).getNote());
			// if (l.get(0).getInfoFrom() != null
			// && l.get(0).getInfoFrom().equals("SampleDnaInfo")) {
			// SampleDnaInfo sd = this.findByProperty(SampleDnaInfo.class,
			// "code", l.get(0).getCode()).get(0);
			// m.put("info", "OD:" + sd.getOd260());
			// }
			return m;
		}
		return null;
	}

	/**
	 * 96孔展示
	 * 
	 * @param id
	 * @param row
	 * @param col
	 * @param rows
	 * @param cols
	 * @param type
	 * @param sinble
	 * @param maxNum
	 * @param counts
	 * @return
	 */
	public Map<String, Object> findMainStorageTest(String id, int row, int col,
			int rows, int cols, String type, String sinble, String maxNum,
			String counts) {

		List<Map<String, Object>> list = null;
		String sql = "";
		if (type != null && !type.equals("") && type.equals("plasma")) {
			sql = "select m.SAMPLE_CODE as mid from PLASMA_TASK_ITEM m where m.BLOOD_SAMPLE_TASK='"
					+ id
					+ "' and m.ROW_CODE='"
					+ String.valueOf((char) (65 + row))
					+ "' and m.COL_CODE='"
					+ (col + 1)
					+ "' and m.COUNTS='"
					+ (Integer.parseInt(maxNum) + 1) + "'";
		} else if (type != null && !type.equals("") && type.equals("dna")) {
			sql = "select m.CODE as mid from DNA_TASK_ITEM m where m.DNA_TASK='"
					+ id
					+ "' and m.ROW_CODE='"
					+ String.valueOf((char) (65 + row))
					+ "' and m.COL_CODE='"
					+ (col + 1) + "' and m.COUNTS='" + counts + "'";
		} else if (type != null && !type.equals("") && type.equals("dnaInfo")) {
			sql = "select m.CODE as mid from SAMPLE_DNA_INFO m where m.DNA_TASK='"
					+ id
					+ "' and m.ROW_CODE='"
					+ String.valueOf((char) (65 + row))
					+ "' and m.COL_CODE='"
					+ (col + 1) + "' and m.COUNTS='" + counts + "'";
		} else if (type != null && !type.equals("") && type.equals("wk")) {
			sql = "select m.CODE as mid from WK_TASK_ITEM m where m.WK_SAMPLE_TASK='"
					+ id
					+ "' and m.ROW_CODE='"
					+ String.valueOf((char) (65 + row))
					+ "' and m.COL_CODE='"
					+ (col + 1) + "' and m.COUNTS='" + counts + "'";
		} else if (type != null && !type.equals("") && type.equals("wkInfo")) {
			sql = "select m.CODE as mid from SAMPLE_WK_INFO m where m.WK_SAMPLE_TASK='"
					+ id
					+ "' and m.ROW_CODE='"
					+ String.valueOf((char) (65 + row))
					+ "' and m.COL_CODE='"
					+ (col + 1) + "' and m.COUNTS='" + counts + "'";
		} else if (type != null && !type.equals("") && type.equals("pooling")) {
			sql = "select m.SAMPLE_CODE as mid from POOLING_ITEM m where m.POOLING_TASK='"
					+ id
					+ "' and m.ROW_CODE='"
					+ String.valueOf((char) (65 + row))
					+ "' and m.COL_CODE='"
					+ (col + 1)
					+ "' and m.COUNTS='"
					+ (Integer.parseInt(maxNum) + 1) + "'";
		} else if (type != null && !type.equals("") && type.equals("qc2100")) {
			sql = "select m.WK_ID as mid from QC_2100_TASK_ITEM m where m.WK_QUALITY_SAMPLE_TASK='"
					+ id
					+ "' and m.ROW_CODE='"
					+ String.valueOf((char) (65 + row))
					+ "' and m.COL_CODE='"
					+ (col + 1)
					+ "' and m.COUNTS='"
					+ (Integer.parseInt(maxNum) + 1) + "'";
		} else if (type != null && !type.equals("") && type.equals("qcQpcr")) {
			sql = "select m.WK_ID as mid from QC_QPCR_TASK_ITEM m where m.QC_QPCR_TASK='"
					+ id
					+ "' and m.ROW_CODE='"
					+ String.valueOf((char) (65 + row))
					+ "' and m.COL_CODE='"
					+ (col + 1)
					+ "' and m.COUNTS='"
					+ (Integer.parseInt(maxNum) + 1) + "'";
		} else if (type != null && !type.equals("")
				&& type.equals("generation")) {
			sql = "select m.PL_ID as mid from GENERATION_TASK_ITEM m where m.GENERATION_TASK='"
					+ id
					+ "' and m.ROW_CODE='"
					+ String.valueOf((char) (65 + row))
					+ "' and m.COL_CODE='"
					+ (col + 1)
					+ "' and m.COUNTS='"
					+ (Integer.parseInt(maxNum) + 1) + "'";
		} else if (type != null && !type.equals("")
				&& type.equals("sangerTask")) {
			sql = "select m.CODE as mid,m.chromosomal_location as mcl from SANGER_TASK_ITEM m where m.SANGER_TASK='"
					+ id
					+ "' and m.ROW_CODE='"
					+ String.valueOf((char) (65 + row))
					+ "' and m.COL_CODE='"
					+ (col + 1) + "' and m.COUNTS='" + counts + "'";
		} else if (type != null && !type.equals("")
				&& type.equals("sangerTaskInfo")) {
			sql = "select m.CODE as mid,m.chromosomal_location as mcl from SANGER_TASK_INFO m where m.SANGER_TASK='"
					+ id
					+ "' and m.ROW_CODE='"
					+ String.valueOf((char) (65 + row))
					+ "' and m.COL_CODE='"
					+ (col + 1) + "' and m.COUNTS='" + counts + "'";
		} else if (type != null && !type.equals("") && type.equals("qtTask")) {
			sql = "select m.CODE as mid from QT_TASK_ITEM m where m.QT_TASK='"
					+ id + "' and m.ROW_CODE like '%"
					+ String.valueOf((char) (65 + row))
					+ "%' and m.COL_CODE like'%" + (col + 1)
					+ "%' and m.COUNTS='" + counts + "'";
		} else if (type != null && !type.equals("")
				&& type.equals("qtTaskInfo")) {
			sql = "select m.CODE as mid from QT_TASK_INFO m where m.QT_TASK='"
					+ id + "' and m.ROW_CODE='"
					+ String.valueOf((char) (65 + row)) + "' and m.COL_CODE='"
					+ (col + 1) + "' and m.COUNTS='" + counts + "'";
		} else if (type != null && !type.equals("") && type.equals("snpSample")) {
			sql = "select m.CODE as mid,m.FIVE_VOLUME as vl,m.FIVE_XSY_VOLUME as xsy  from SNP_SAMPLE_ITEM m where m.SNP_SAMPLE='"
					+ id
					+ "' and m.ROW_CODE='"
					+ String.valueOf((char) (65 + row))
					+ "' and m.COL_CODE='"
					+ (col + 1)
					+ "' and m.COUNTS='"
					+ (Integer.parseInt(maxNum) + 1) + "'";
		} else if (type != null && !type.equals("")
				&& type.equals("sangerPcrTask")) {
			sql = "select m.CODE as mid from SANGER_PCR_TASK_ITEM m where m.SANGER_PCR_TASK='"
					+ id
					+ "' and m.ROW_CODE like '"
					+ String.valueOf((char) (65 + row))
					+ "' and m.COL_CODE like'"
					+ (col + 1)
					+ "' and m.COUNTS='"
					+ counts + "'";
		} else if (type != null && !type.equals("")
				&& type.equals("sangerChTask")) {
			sql = "select m.CODE as mid from SANGER_CH_TASK_ITEM m where m.SANGER_CH_TASK='"
					+ id
					+ "' and m.ROW_CODE like '"
					+ String.valueOf((char) (65 + row))
					+ "' and m.COL_CODE like'"
					+ (col + 1)
					+ "' and m.COUNTS='"
					+ counts + "'";
		}
		list = this.findForJdbc(sql);
		if (list.size() > 0) {

			// if(sinble!=null && !sinble.equals("") && sinble.equals("0"))
			return list.get(0);
			// else
			// return list.get(col+row*cols);
		}
		return null;
	}

	/**
	 * 查询库存位置上的物品ID
	 * 
	 * @param id
	 * @param row
	 * @param col
	 * @return 物品ID
	 */
	public String findMainId(String id, int row, int col, int rows, int cols,
			String type, String sinble) {
		if (type != null && !type.equals("") && type.equals("plasma")) {
			PlasmaTask pt = storagePositionDao.get(PlasmaTask.class, id);
		} else if (type != null && !type.equals("") && type.equals("dna")) {
			PlasmaTask pt = storagePositionDao.get(PlasmaTask.class, id);
		} else if (type != null && !type.equals("") && type.equals("wk")) {
			PlasmaTask pt = storagePositionDao.get(PlasmaTask.class, id);
		} else if (type != null && !type.equals("") && type.equals("pooling")) {
			PlasmaTask pt = storagePositionDao.get(PlasmaTask.class, id);
		} else if (type != null && !type.equals("") && type.equals("qc2100")) {
			PlasmaTask pt = storagePositionDao.get(PlasmaTask.class, id);
		} else if (type != null && !type.equals("") && type.equals("qcQpcr")) {
			PlasmaTask pt = storagePositionDao.get(PlasmaTask.class, id);
		}
		PlasmaTask pt = storagePositionDao.get(PlasmaTask.class, id);
		Template sp = storagePositionDao.get(Template.class, pt.getTemplate()
				.getId());
		String isUse = sp.getId();
		// if (isUse != null && !isUse.equals("0")) {
		// String[][] contener = toArray(isUse);
		// return contener[row][col];
		// } else {
		return "0";
		// }
	}

	/**
	 * 查询库存位置上的物品ID
	 * 
	 * @param id
	 * @param row
	 * @param col
	 * @return 物品ID
	 */
	public String findMainId(String id, int row, int col) {
		String code = "";
		// return id + "-" + String.valueOf((char) (65 + row))
		// + String.valueOf(col);
		if (col < 10) {
			code = id + "-" + String.valueOf((char) (65 + row)) + "0"
					+ String.valueOf(col);
		} else {
			code = id + "-" + String.valueOf((char) (65 + row))
					+ String.valueOf(col);
		}
		return code;
	}

	/**
	 * 查询存储位置上是否放有物品
	 * 
	 * @param id
	 *            存储位置ID
	 * @param row
	 *            格子所在的行
	 * @param col
	 *            格子所在的列
	 * @param type
	 *            存储容器存放的类型
	 * @return 0:没有存放东西;1:存放的是入库单上的物品;2:存放的是主数据中的类型;
	 * @throws Exception
	 */
	public long findIsUse(String id) throws Exception {

		String hql = "from SampleInfoIn m where m.location like '" + id
				+ "%' and (state = '1' or state = '2')";

		Long count = (Long) this.getSession()
				.createQuery("select count(*)" + hql).uniqueResult();

		return count;
	}

	// 查询材料入库单中每个表格的存储内容
	public Map<String, Object> findInStorage(String id, String type) {
		List<Map<String, Object>> list = null;
		String sql = "";
		if (type.equals("0")) {
			sql = "select m.id as mid,ma.id,ma.name from MATERAILS_IN_CELL m left join MATERAILS_IN  ma on m.ID=ma.id where m.FROZEN_LOCATION='"
					+ id + "'";
		} else if (type.equals("1")) {
			sql = "select m.id as mid,m.id,m.name from MATERAILS_IN_PLASMID m left join MATERAILS_IN  ma on m.ID=ma.id where m.FROZEN_LOCATION='"
					+ id + "'";
		} else if (type.equals("2")) {
			sql = "select m.id as mid,m.id,m.name from MATERAILS_IN_BAC m left join MATERAILS_IN  ma on m.ID=ma.id where m.FROZEN_LOCATION='"
					+ id + "'";
		} else if (type.equals("3")) {
			sql = "select m.id,m.name from ANIMAL_IN_ITEM m where m.ID='" + id
					+ "'";
		}
		list = this.findForJdbc(sql);
		if (list.size() > 0)
			return list.get(0);
		else
			return null;
	}

	// 材料的转存操作
	public void changeLocation(String id, String type, String newStr) {
		String hql = "";
		if (type.equals("0")) {
			hql = "update  MaterailsMainCell m set m.frozenLocation = '"
					+ newStr + "' where m.id='" + id + "'";
		} else if (type.equals("1")) {
			hql = "update  MaterailsMainPlasmid m set m.frozenLocation = '"
					+ newStr + "' where m.id='" + id + "'";
		} else if (type.equals("2")) {
			hql = "update  MaterailsMainBAC m set m.frozenLocation = '"
					+ newStr + "' where m.id='" + id + "'";
		} else if (type.equals("3")) {
			hql = "update  AnimalItem m set m.CAGE_CODE='" + newStr
					+ "' where m.id = '" + id + "'";
		}
		this.getSession().createQuery(hql).executeUpdate();
	}

	/**
	 * 查找存储位置上的空格子
	 * 
	 * @param str
	 *            位置ID
	 * @param type
	 *            位置类型
	 * @param num
	 *            位置级别
	 * @return 空格子的ID
	 */
	public String findLocation(String str, String type, int num) {
		String result = null;
		String hql_1 = "";
		String hql_2 = "";
		List<StoragePosition> list_1 = new ArrayList<StoragePosition>();
		List<StoragePosition> list_2 = new ArrayList<StoragePosition>();
		switch (num) {
		case 1:
			hql_1 = " from StoragePosition t where t.upStoragePosition.id='"
					+ str + "'";
			list_1 = this.getSession().createQuery(hql_1).list();
			for (int i = 0; i < list_1.size(); i++) {
				hql_2 = " from StoragePosition t where t.upStoragePosition.id='"
						+ list_1.get(i).getId() + "'";
				list_2 = this.getSession().createQuery(hql_2).list();
				for (int j = 0; j < list_2.size(); j++) {
					result = findLocationAll(list_2.get(j));
					if (!result.equals("0"))
						return result;
					else
						result = list_2.get(j).getId() + "-" + "A1";
				}
			}
			break;
		case 2:
			hql_2 = " from StoragePosition t where t.upStoragePosition.id='"
					+ str + "'";
			list_2 = this.getSession().createQuery(hql_2).list();
			for (int j = 0; j < list_2.size(); j++) {
				result = findLocationAll(list_2.get(j));
				if (!result.equals("0"))
					return result;
				else
					result = list_2.get(j).getId() + "-" + "A1";
			}
			break;
		case 3:
			StoragePosition sp = storagePositionDao.get(StoragePosition.class,
					str);
			result = findLocationAll(sp);
			if (!result.equals("0"))
				return result;
			else
				result = sp.getId() + "-" + "A1";
			break;
		}

		return result;
	}

	/**
	 * 查找存储位置上的空格子
	 * 
	 * @param sp
	 *            位置实例
	 * @param type
	 *            位置类型
	 * @param num
	 *            位置级别
	 * @return 空格子的ID
	 */
	public String findLocationAll(StoragePosition sp) {
		String result = "0";
		// StoragePosition sp =
		// storagePositionDao.get(StoragePosition.class,str);

		String oldValue = sp.getIsUse();
		if (!oldValue.equals("0")) {
			String[][] contener = toArray(oldValue);
			int m = contener.length;
			int n = contener[0].length;
			for (int i = 0; i < m; i++) {
				for (int j = 0; j < n; j++) {
					if (contener[i][j].equals("0")) {
						result = sp.getId() + "-"
								+ String.valueOf((char) (65 + i)) + (j + 1);
						return result;
					}
				}
			}
		}
		return result;
	}

	/*
	 * public String findLocation(String str, String type, int num) { String
	 * hql_1 = ""; String hql_2 = ""; List<StoragePosition> list_1 = new
	 * ArrayList<StoragePosition>(); List<StoragePosition> list_2 = new
	 * ArrayList<StoragePosition>(); switch (num) { case 1: hql_1 =
	 * " from StoragePosition t where t.upStoragePosition.id='" + str + "'";
	 * list_1 = this.getSession().createQuery(hql_1).list(); for (int i = 0; i <
	 * list_1.size(); i++) { hql_2 =
	 * " from StoragePosition t where t.upStoragePosition.id='" +
	 * list_1.get(i).getId() + "'"; list_2 =
	 * this.getSession().createQuery(hql_2).list(); for (int j = 0; j <
	 * list_2.size(); j++) { String id_3 = list_2.get(j).getId();
	 * StorageContainer sc = list_2.get(j).getStorageContainer(); for (int row =
	 * 0; row < sc.getRowNum(); row++) { for (int col = 1; col <=
	 * sc.getColNum(); col++) { String locId = id_3 + "-" +
	 * String.valueOf((char) (65 + row)) + col; Map<String, Object> isUse =
	 * findMainStorage(locId, type); Map<String, Object> isUse2 =
	 * findInStorage(locId, type); if (isUse == null && isUse2 == null) { return
	 * locId; } } } } } break; case 2: hql_2 =
	 * " from StoragePosition t where t.upStoragePosition.id='" + str + "'";
	 * list_2 = this.getSession().createQuery(hql_2).list(); for (int j = 0; j <
	 * list_2.size(); j++) { String id_3 = list_2.get(j).getId();
	 * StorageContainer sc = list_2.get(j).getStorageContainer(); for (int row =
	 * 0; row < sc.getRowNum(); row++) { for (int col = 1; col <=
	 * sc.getColNum(); col++) { String locId = id_3 + "-" +
	 * String.valueOf((char) (65 + row)) + col; Map<String, Object> isUse =
	 * findMainStorage(locId, type); Map<String, Object> isUse2 =
	 * findInStorage(locId, type); if (isUse == null && isUse2 == null) { return
	 * locId; } } } } break; case 3: StoragePosition sp =
	 * storagePositionDao.get(StoragePosition.class, str); StorageContainer sc =
	 * sp.getStorageContainer(); String id_3 = str; for (int row = 0; row <
	 * sc.getRowNum(); row++) { for (int col = 1; col <= sc.getColNum(); col++)
	 * { String locId = id_3 + "-" + String.valueOf((char) (65 + row)) + col;
	 * Map<String, Object> isUse = findMainStorage(locId, type); Map<String,
	 * Object> isUse2 = findInStorage(locId, type); if (isUse == null && isUse2
	 * == null) { return locId; } } } break; }
	 * 
	 * return null; }
	 */

	/**
	 * 实验材料的批量入库
	 * 
	 * @param id
	 *            物品ID
	 * @param type
	 *            物品类型
	 * @param newStr
	 *            存储位置
	 */
	public void batchLocationIn(String id, String type, String newStr) {
		String hql = "";
		if (type.equals("0")) {
			hql = "update  MaterailsInCell m set m.frozenLocation = '" + newStr
					+ "' where m.id='" + id + "'";
		} else if (type.equals("1")) {
			hql = "update  MaterailsInPlasmid m set m.frozenLocation = '"
					+ newStr + "' where m.id='" + id + "'";
		} else if (type.equals("2")) {
			hql = "update  MaterailsInBAC m set m.frozenLocation = '" + newStr
					+ "' where m.id='" + id + "'";
		}
		this.getSession().createQuery(hql).executeUpdate();
	}

	/**
	 * 实验材料的批量归还
	 * 
	 * @param id
	 *            物品ID
	 * @param type
	 *            物品类型
	 * @param newStr
	 *            存储位置
	 */
	public void batchLocationReturn(String id, String type, String newStr) {
		String hql = "";
		if (type.equals("0")) {
			hql = "update  MaterailsReturnCell m set m.frozenLocation = '"
					+ newStr + "' where m.id='" + id + "'";
		} else if (type.equals("1")) {
			hql = "update  MaterailsReturnPlasmid m set m.frozenLocation = '"
					+ newStr + "' where m.id='" + id + "'";
		} else if (type.equals("2")) {
			hql = "update  MaterailsReturnBAC m set m.frozenLocation = '"
					+ newStr + "' where m.id='" + id + "'";
		}
		this.getSession().createQuery(hql).executeUpdate();
	}

	/**
	 * 将String类型二维数组转换为二维数组
	 * 
	 * @param str
	 * @return
	 */
	public static String[][] toArray(String str) {
		String[] split = str.split(";");
		String[][] array = new String[split.length][];
		for (int i = 0; i < split.length; i++) {
			array[i] = split[i].split(",");
		}
		return array;
	}

	// 根据样本编号查询样本主数据
	public List<SampleInfo> findSampleInfo(String code) throws Exception {
		String hql = "from SampleInfo where 1=1 and code='" + code + "'";
		List<SampleInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 查询待入库样本 即样本入库左侧表
	public Map<String, Object> findSampleInTemp(Integer start, Integer length,
			String query, String col, String sort, String codes,String sampleType)
			throws Exception {
		String code = "";
		if (codes != null && !"".equals(codes)) {
			String codeStr[] = codes.split(",");
			for (int i = 0; i < codeStr.length; i++) {
				if (i + 1 != codeStr.length) {
					code += "'" + codeStr[i] + "',";
				} else {
					code += "'" + codeStr[i] + "'";
				}
			}
		}

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "";
		if (!"".equals(code)) {
			countHql = "select count(*) from SampleInItemTemp where 1=1 and state='1'  and code in("
					+ code + ")";
		} else {
			countHql = "select count(*) from SampleInItemTemp where 1=1 and state='1' ";
		}
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		if(sampleType!=null&&!"".equals(sampleType)) {
			key += " and sampleType like '%"+sampleType+"%'";
		}
		String scopeId = (String) ActionContext.getContext().getSession()
				.get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "";
			if (!"".equals(code)) {
				hql = "from SampleInItemTemp where 1=1 and state='1' and code in("
						+ code + ")";
			} else {
				hql = "from SampleInItemTemp where 1=1 and state='1'";
			}
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SampleInItemTemp> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	// 根据样本编号查询样本入库明细表
	public List<SampleInItem> findSampleInItem(String code) throws Exception {
		String hql = "from SampleInItem where 1=1 and code='" + code + "'";
		List<SampleInItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据样本类型id查询图标编码
	public String finSampleTypePicCode(String code) throws Exception {
		String sql = "select t.pic_code from dic_sample_type t where 1=1 and t.id='"
				+ code + "'";

		Object picCode = this.getSession().createSQLQuery(sql).uniqueResult();
		if (picCode != null)
			return picCode.toString();
		else
			return null;
	}

	/*
	 * // 根木样本编号查询待入库样本selectSampleInTempByCode public Map<String, Object>
	 * selectSampleInTempByCode(Integer start, Integer length, String query,
	 * String col, String sort, String codes) throws Exception {
	 * 
	 * String codeStr[] = codes.split(","); String code = ""; for(int
	 * i=0;i<codeStr.length;i++){ if(i+1 != codeStr.length){ code +=
	 * "'"+codeStr[i]+"',"; }else{ code +="'"+codeStr[i]+"'"; } } Map<String,
	 * Object> map = new HashMap<String, Object>(); String countHql =
	 * "select count(*) from SampleInItemTemp where 1=1 and code in("+code+")";
	 * String key = ""; if(query!=null&&!"".equals(query)){
	 * key=map2Where(query); } Long sumCount = (Long)
	 * getSession().createQuery(countHql).uniqueResult(); if(0l!= sumCount){
	 * Long filterCount = (Long)
	 * getSession().createQuery(countHql+key).uniqueResult(); String hql =
	 * "from SampleInItemTemp where 1=1 and state='1'";
	 * if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
	 * col=col.replace("-", "."); key+=" order by "+col+" "+sort; }
	 * List<SampleInItemTemp> list =
	 * getSession().createQuery(hql+key).setFirstResult
	 * (start).setMaxResults(length).list(); map.put("recordsTotal", sumCount);
	 * map.put("recordsFiltered", filterCount); map.put("list", list); } return
	 * map; }
	 */
	// 根据编码查看位置
	public List<SampleInfoIn> findPositon(String code) throws Exception {
		String hql = "from SampleInfoIn where 1=1 and code='" + code + "'";
		List<SampleInfoIn> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据位置查看编码
	public List<SampleInfoIn> findCode(String location) throws Exception {
		String hql = "from SampleInfoIn where 1=1 and location='" + location
				+ "'";
		List<SampleInfoIn> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public Map<String, Object> showContainerTableJson(Integer start,
			Integer length, String query, String col, String sort) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from StorageContainer where 1=1 ";
		String key = "";
		// if(queryMap!=null){
		// key=map2where(queryMap);
		// }
		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from StorageContainer where 1=1";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<StorageContainer> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> showStorageNewBox(Integer start, Integer length,
			String query, String col, String sort) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from StorageBox where 1=1 and (newLocationId='' or newLocationId is null)";
		String key = "";
		// if(queryMap!=null){
		// key=map2where(queryMap);
		// }
		// 集团的查询条件
		String scopeId = (String) ActionContext.getContext().getSession()
				.get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from StorageBox where 1=1 and (newLocationId='' or newLocationId is null)";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<StorageBox> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<SampleInItemTemp> selSampleInTempByCode(String code) {
		String hql = "from SampleInItemTemp where 1=1 and code='" + code + "'";
		List<SampleInItemTemp> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 查询待入库样本 即样本入库左侧表
	public Map<String, Object> findSampleByLocation(Integer start,
			Integer length, String query, String col, String sort,
			String position) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleInItem where 1=1 and outState='4' and location= '"
				+ position + "'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from SampleInItem where 1=1  and outState='4' and location= '"
					+ position + "'";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SampleInItem> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> finSampleInItemByBox(Integer start,
			Integer length, String query, String col, String sort,
			String location) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from SampleInItem where 1=1 and outState='4' and location like '"
				+ location + "%'";
		String key = "";
		if (query != null) {
			key = map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from SampleInItem where 1=1  and outState='4'and location like '"
					+ location + "%'";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				col = col.replace("-", ".");
				key += " order by " + col + " " + sort;
			}
			List<SampleInItem> list = getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public List<SampleInItem> selSampleInItemByOldLocation(String oldLocation) {
		String hql = "from SampleInItem where 1=1 and location like '"
				+ oldLocation + "%'";
		List<SampleInItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<SampleInItem> selSampleInItemByCode(String code) {
		String hql = "from SampleInItem where 1=1 and code = '" + code + "'";
		List<SampleInItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<StorageBox> selStorageBoxByOld(String oldPosition) {
		String hql = "from StorageBox where 1=1 and newLocationId = '"
				+ oldPosition + "'";
		List<StorageBox> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<SampleInItem> selSampleInItemByOld(String oldPosition) {
		String hql = "from SampleInItem where 1=1 and location like '"
				+ oldPosition + "%'";
		List<SampleInItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<SampleInfoIn> selSampleInfoInByOld(String oldPosition) {
		String hql = "from SampleInfoIn where 1=1 and location like '"
				+ oldPosition + "%'";
		List<SampleInfoIn> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<SampleInfoIn> selSampleInfoInByStorageBox(String id) {
		String hql = "from SampleInfoIn where 1=1 and storageBox.id ='" + id
				+ "'";
		List<SampleInfoIn> list = this.getSession().createQuery(hql).list();
		return list;
	}

}
