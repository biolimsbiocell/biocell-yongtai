package com.biolims.storage.container.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.dic.model.DicType;
import com.biolims.equipment.model.Instrument;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.storage.model.SampleIn;
import com.biolims.sample.storage.model.SampleInItem;
import com.biolims.sample.storage.model.SampleInItemTemp;
import com.biolims.sample.storage.model.SampleInfoIn;
import com.biolims.storage.container.dao.ContainerDao;
import com.biolims.storage.container.model.StorageContainer;
import com.biolims.storage.container.model.StorageContainerStore;
import com.biolims.storage.position.model.StoragePosition;
import com.biolims.storage.position.service.SaveSampleInService;
import com.biolims.storage.position.service.StoragePositionService;
import com.biolims.system.storage.model.StorageBox;
import com.biolims.system.storage.model.StorageBoxItem;
import com.biolims.util.JsonUtils;
import com.opensymphony.xwork2.ActionContext;

@Service
@SuppressWarnings({ "rawtypes", "unchecked" })
public class ContainerService {

	@Resource
	private ContainerDao containerDao;
	@Resource
	private StoragePositionService storagePositionService;
	@Resource
	private SaveSampleInService saveSampleInService;
	@Resource
	private CodingRuleService codingRuleService;

	@Resource
	private CommonDAO commonDAO;

	/**
	 * 查询容器列表
	 * 
	 * @param name
	 *            容器名称
	 * @return
	 * @throws Exception 
	 */
	/*public Map<String, Object> queryContainer(String name, int start,
			int limit, String dir, String sort) {
		return containerDao.selectContainer(name, start, limit, dir, sort);
	}*/
	public Map<String, Object> queryContainer(String classify, Integer start,
			Integer length, String query, String col,String sort) throws Exception {
		return containerDao.selectContainer(classify,start, length, query, col, sort);
	}

	/**
	 * 
	 * 增加用户认证,由ext ajax调用
	 * 
	 * @param userId
	 *            用户id
	 * @param jsonString
	 *            用户认证jsonString
	 * @return void
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void addOrUpdate(String jsonString) throws Exception {
		List<Map> list = JsonUtils.toListByJsonArray(jsonString, List.class);
		for (Map map : list) {
			StorageContainer uct = new StorageContainer();
			uct = (StorageContainer) containerDao.Map2Bean(map, uct);
			containerDao.saveOrUpdate(uct);
		}
	}

	public StorageContainerStore queryStorageContainerStore(String posiId,
			String contId) {
		return containerDao.selectStorageContainerStore(posiId, contId);
	}

	public StorageContainer queryStorageContainerById(String contId) {
		return containerDao.get(StorageContainer.class, contId);
	}

	/**
	 * 实验室96孔展示
	 * 
	 * @param id
	 * @param row
	 * @param col
	 * @param rows
	 * @param cols
	 * @param type
	 * @param sinble
	 * @param maxNum
	 * @param counts
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findMainStorageTest(String id, int row, int col,
			int rows, int cols, String type, String sinble, String maxNum,
			String counts, int n) throws Exception {
		return containerDao.findMainStorageTest(id, row, col, rows, cols, type,
				sinble, maxNum, counts);
	}

	/**
	 * 查询存储位置上每个格子的存储内容
	 * 
	 * @param id
	 *            存储位置ID
	 * @param row
	 *            格子所在的行
	 * @param col
	 *            格子所在的列
	 * @param type
	 *            存储容器存放的类型
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findMainStorage(String id, int row, int col)
			throws Exception {
		return containerDao.findMainStorage(id, row, col);
	}

	/**
	 * 查询存储位置上是否放有物品
	 * 
	 * @param id
	 *            存储位置ID
	 * @param row
	 *            格子所在的行
	 * @param col
	 *            格子所在的列
	 * @param type
	 *            存储容器存放的类型
	 * @return 0:没有存放东西；1:存放的是入库单上的物品；2:存放的是主数据中的类型；
	 * @throws Exception
	 */
	public long findIsUse(String id) throws Exception {
		return containerDao.findIsUse(id);
	}

	/**
	 * 存放物品转存操作
	 * 
	 * @param id
	 *            物品ID
	 * @param type
	 *            操作类型：材料入库（0）、材料主数据(1)、材料归还(2)
	 * @param newStr
	 *            新的存储位置classType
	 * @param classType
	 *            物品类型
	 * @return
	 * @throws Exception
	 */
	/*@Transactional(rollbackFor = Exception.class)
	public boolean changeLocation(String id, String type, String newStr,
			String classType) throws Exception {
		storagePositionService.setPositionContent(newStr, id, type);

		containerDao.changeLocation(id, classType, newStr);
		return true;
	}*/
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeLocation(String id, String dataJson, String logInfo) throws Exception {
		List<StorageContainer> saveItems = new ArrayList<StorageContainer>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(dataJson,
				List.class);
		StorageContainer pt = commonDAO.get(StorageContainer.class, id);
		for (Map<String, Object> map : list) {
			StorageContainer uct = new StorageContainer();
			// 将map信息读入实体类
			uct = (StorageContainer) commonDAO.Map2Bean(map, uct);
			if(logInfo!=null&&!"".equals(logInfo)){
				User u = (User) ServletActionContext.getRequest().getSession()
					      .getAttribute(SystemConstants.USER_SESSION_KEY);
				LogInfo li=new LogInfo();
				li.setLogDate(new Date());
				li.setFileId(uct.getId());
				li.setClassName("StorageContainer");
				li.setUserId(u.getId());
				if("".equals(uct.getId())) {
					li.setState("1");
					li.setStateName("数据新增");
				}else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				
				li.setModifyContent(logInfo);
				commonDAO.saveOrUpdate(li);
			}
			if (uct.getId() != null && uct.getId().equals("")) {
				uct.setId(null);
			}
			/*if (uct.getState() == null)
				uct.setState("1");*/
			commonDAO.saveOrUpdate(uct);
			saveItems.add(uct);
			
			if(logInfo!=null&&!"".equals(logInfo)){
				User u = (User) ServletActionContext.getRequest().getSession()
					      .getAttribute(SystemConstants.USER_SESSION_KEY);
				LogInfo li=new LogInfo();
				li.setLogDate(new Date());
				li.setFileId(uct.getId());
				li.setClassName("StorageContainer");
				li.setUserId(u.getId());
				li.setState("1");
				li.setStateName("数据新增");
				li.setModifyContent(logInfo);
				commonDAO.saveOrUpdate(li);
			}
		}
		
		containerDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 批量分配存储位置
	 * 
	 * @param ids
	 *            要存储的物品ID数组
	 * @param type
	 *            操作类型：材料入库（0）、材料主数据(1)、材料归还(2)
	 * @param classType
	 *            物品类型
	 * @param pid
	 *            存放位置ID
	 * @return
	 * @throws Exception
	 */
	@Transactional(rollbackFor = Exception.class)
	public boolean batchLocation(String[] ids, String type, String pid,
			String classType) throws Exception {
		String[] str = pid.split("-");
		int num = str.length;
		for (int i = 0; i < ids.length; i++) {
			String adr = "";
			adr = containerDao.findLocation(pid, classType, num);
			if (adr != null) {
				storagePositionService.setPositionContent(adr, ids[i], type);
				switch (Integer.parseInt(type)) {
				case 0:
					containerDao.batchLocationIn(ids[i], classType, adr);
					break;
				case 2:
					containerDao.batchLocationReturn(ids[i], classType, adr);
					break;
				}

			}
		}

		return true;
	}

	// 根据样本编号查询样本主数据
	public List<SampleInfo> selSampleInfo(String code) throws Exception {
		return containerDao.findSampleInfo(code);
	}

	// 查询待入库样本 即样本入库左侧表
	public Map<String, Object> selSampleInTemp(Integer start, Integer length,
			String query, String col, String sort, String codes, String sampleType)
			throws Exception {
		return containerDao.findSampleInTemp(start, length, query, col, sort,
				codes, sampleType);
	}

	// 根据样本编号查询样本入库明细表
	public List<SampleInItem> selSampleInItem(String code) throws Exception {
		return containerDao.findSampleInItem(code);
	}

	/*
	 * // 根据样本编号查询待入库样本 即样本入库左侧表 public Map<String, Object>
	 * selSampleInTempByCode(Integer start, Integer length, String query, String
	 * col, String sort, String codes) throws Exception { return
	 * containerDao.selectSampleInTempByCode(start, length, query, col,
	 * sort,codes); }
	 */

	// 输入多个编码或者一个位置查询
	public List<SampleInfoIn> selCodeOrLocation(String strobj) throws Exception {
		List<SampleInfoIn> list = new ArrayList<SampleInfoIn>();
		if (strobj.indexOf("-") != -1) {
			String[] location = strobj.split(",");
			for (int j = 0; j < location.length; j++) {
				List<SampleInfoIn> list2 = containerDao.findCode(location[j]);
				list.addAll(list2);
			}
		} else {
			String[] code = strobj.split(",");
			for (int i = 0; i < code.length; i++) {
				List<SampleInfoIn> list3 = containerDao.findPositon(code[i]);
				list.addAll(list3);
			}
		}
		return list;
	}

	public Map<String, Object> showContainerTableJson(Integer start,
			Integer length, String query, String col, String sort) {
		return containerDao.showContainerTableJson(start, length, query, col,
				sort);
	}

	public Map<String, Object> selStorageNewBox(Integer start, Integer length,
			String query, String col, String sort) {
		return containerDao.showStorageNewBox(start, length, query, col, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String saveSampleInTask(String iceBox2, User user, String id2) throws Exception {

		String[] iceBox1 = iceBox2.split(",");
		String[] id1 = id2.split(",");
		int size = id1.length;
		SampleIn sampleIn = new SampleIn();
		// 新建一张任务并保存
		String modelName = "SampleIn";
		String markCode = "YBRK";
		String autoID = codingRuleService.genTransID(modelName, markCode);
		sampleIn.setId(autoID);
		sampleIn.setCreateUser(user);
		sampleIn.setCreateDate(new Date());
		sampleIn.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
		sampleIn.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
		sampleIn.setState(SystemConstants.DIC_STATE_NEW);
		sampleIn.setStateName(SystemConstants.DIC_STATE_NEW_NAME);
		containerDao.saveOrUpdate(sampleIn);
		for(int i=0;i<size;i++){
			//盒子编号
			String id =id1[i];
			//冰箱编号
			String iceBox = iceBox1[i];
			//通过盒子编号查询SampleInfoIn盒内是否有样本
			List<SampleInfoIn> listSii = containerDao.selSampleInfoInByStorageBox(id);
			// 将盒子放入冰箱
			StorageBox sb = containerDao.get(StorageBox.class, id);
			sb.setNewLocationId(iceBox);
			sb.setOldLocationId(iceBox);
			sb.setLocationId(iceBox);
			containerDao.saveOrUpdate(sb);
			//修改盒子迁移记录
			StorageBoxItem sbi = new StorageBoxItem();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date d = new Date();
			sbi.setChangeDate(sdf.parse(sdf.format(d)));
			sbi.setChangeUser(user.getName());
			sbi.setLocationId(iceBox);
			sbi.setLocationName(iceBox);
			sbi.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			sbi.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			sbi.setStorageBox(sb);
			sbi.setState("1");
			containerDao.saveOrUpdate(sbi);
			if(listSii.size()>0){//盒内有样本
				for(int k=0;k<listSii.size();k++){
					// 位置
					String position = iceBox + "-" +listSii.get(k).getBoxLocation();
					// 样本编号
					String code = listSii.get(k).getCode();
					// 通过样本编号查询左侧表数据
					List<SampleInItemTemp> list = containerDao
							.selSampleInTempByCode(code);
					// 将入库明细表数据与主表关联
					SampleInItem sii = new SampleInItem();
					SampleIn s = containerDao.get(SampleIn.class, autoID);
					sii.setSampleType(list.get(0).getSampleType());
					sii.setSampleTypeId(list.get(0).getSampleTypeId());
					sii.setConcentration(list.get(0).getConcentration());
					sii.setVolume(list.get(0).getVolume());
					sii.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
					sii.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
					sii.setSumTotal(list.get(0).getSumTotal());
					sii.setTempId(list.get(0).getId());
					sii.setSampleIn(s);
					sii.setState("1");
					sii.setLocation(position);
					sii.setCode(code);
					sii.setSampleCode(list.get(0).getSampleCode());
					containerDao.saveOrUpdate(sii);
				}
			}
		}
		// 完成
		SampleIn sr = containerDao.get(SampleIn.class, autoID);
		sr.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sr.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
		User user2 = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		sr.setAcceptDate(new Date());
		sr.setAcceptUser(user2);
		containerDao.update(sr);
		saveSampleInService.changeTempState(autoID);// -----------改变左侧表的状态
		return autoID;
	}

	// 查询待入库样本 即样本入库左侧表
	public Map<String, Object> selSampleByLocation(Integer start,
			Integer length, String query, String col, String sort,
			String position) throws Exception {
		return containerDao.findSampleByLocation(start, length, query, col,
				sort, position);
	}

	public Map<String, Object> selSampleInItemByBox(Integer start,
			Integer length, String query, String col, String sort,
			String location) throws Exception {
		return containerDao.finSampleInItemByBox(start, length, query, col,
				sort, location);
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(StoragePosition sc, Map jsonMap, String logInfo, Map logMap)
			throws Exception {
		if (sc != null) {
			containerDao.saveOrUpdate(sc);

			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(sc.getId());
				li.setClassName("StorageContainer");
				li.setModifyContent(logInfo);
				li.setState("1");
				li.setStateName("数据新增");
				containerDao.saveOrUpdate(li);
			}

		}
	}

	public StoragePosition selStoragePosition(Map<String, Object> map,
			StoragePosition storagePosition) {
		return (StoragePosition) commonDAO.Map2Bean(map, storagePosition);
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void moveBox(String oldPosition2, String newPosition2,User user) throws ParseException {

		String[] oldPosition = oldPosition2.split(",");
		String[] newPosition = newPosition2.split(",");
		for(int i=0;i<oldPosition.length;i++){
			StorageBox sb = new StorageBox();
			//根据旧位置查询盒子信息
			List<StorageBox> list = containerDao.selStorageBoxByOld(oldPosition[i]);
			if(list.size()>0){
				for(int j=0;j<list.size();j++){
					sb = list.get(j);
					sb.setNewLocationId(newPosition[i]);
					commonDAO.saveOrUpdate(sb);
					//插入一条盒子迁移记录
					StorageBoxItem sbi = new StorageBoxItem();
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date d = new Date();
					sbi.setChangeDate(sdf.parse(sdf.format(d)));
					sbi.setChangeUser(user.getName());
					sbi.setLocationId(newPosition[i]);
					sbi.setLocationName(newPosition[i]);
					sbi.setStorageBox(sb);
					sbi.setState("1");
					containerDao.saveOrUpdate(sbi);
				}
				
			}
			//根据旧位置查询sampleInItem 入库信息
			SampleInItem si = new SampleInItem();
			List<SampleInItem> list2 = containerDao.selSampleInItemByOld(oldPosition[i]);
			if(list2.size()>0){
				for(int k=0;k<list2.size();k++){
					si = list2.get(k);
					String last = si.getLocation().substring(si.getLocation().length()-3, si.getLocation().length());
					si.setLocation(newPosition[i]+"-"+last);
					commonDAO.saveOrUpdate(si);
				}
			}
			//根据旧位置查询sampleInfoIn 样本信息
			SampleInfoIn sii = new SampleInfoIn();
			List<SampleInfoIn> list3 = containerDao.selSampleInfoInByOld(oldPosition[i]);
			if(list3.size()>0){
				for(int m=0;m<list3.size();m++){
					sii = list3.get(m);
					String last = sii.getLocation().substring(sii.getLocation().length()-3, sii.getLocation().length());
					sii.setLocation(newPosition[i]+"-"+last);
					sii.setStorageBox(sb);
					commonDAO.saveOrUpdate(sii);
				}
			}
			
		}
	}
}
