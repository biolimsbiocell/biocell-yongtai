package com.biolims.storage.container.action;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.service.CommonService;
import com.biolims.dic.model.DicType;
import com.biolims.experiment.massarray.dao.MassarrayTaskDao;
import com.biolims.experiment.qt.dao.QtTaskDao;
import com.biolims.experiment.sanger.dao.SangerTaskDao;
import com.biolims.experiment.wk.dao.WkTaskDao;
import com.biolims.storage.container.model.StorageContainer;
import com.biolims.storage.container.model.StorageContainerStore;
import com.biolims.storage.container.service.ContainerService;
import com.biolims.storage.position.dao.StoragePositionDao;
import com.biolims.storage.position.model.StoragePosition;
import com.biolims.storage.position.service.StoragePositionService;
import com.biolims.system.storage.model.StorageBox;
import com.biolims.system.storage.service.StorageBoxService;
import com.biolims.system.template.dao.TemplateDao;
import com.biolims.system.template.model.Template;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

/**
 * 容器控制
 * 
 * @author cong
 * 
 */

@Controller
@Scope("prototype")
@ParentPackage("default")
@Namespace("/storage/container")
@SuppressWarnings("unchecked")
public class ContainerAction extends BaseActionSupport {
	@Resource
	private StoragePositionDao storagePositionDao;
	@Resource
	private TemplateDao templateDao;
	@Resource
	private SangerTaskDao sangerTaskDao;
	@Resource
	private QtTaskDao qtTaskDao;
	@Resource
	private WkTaskDao wkTaskDao;
	@Resource
	private MassarrayTaskDao massarrayTaskDao;
	@Autowired
	private StoragePositionService storagePositionService;
	@Autowired
	private CommonService commonService;
	private static final long serialVersionUID = -2032408093356636483L;
	@Autowired
	private StorageBoxService storageBoxService;
	// 该action权限id
	private String rightsId = "1111";

	@Resource
	private ContainerService containerService;

	private StorageContainer sc;

	// 用于页面上显示模块名称
	private String title = "容器管理";

	/**
	 * 访问列表
	 */
	/*@Action(value = "storageContainerListView", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String storageContainerListView() throws Exception {
		String handlemethod = getParameterFromRequest("handlemethod");
		String exttype = "";
		String extcol = "";
		String editflag = "true";
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		// 属性map<字段名称, ext-list属性数组说明,值为空""则没有该属性>
		// id type format header width sort hide align xtype editable renderer
		// editor
		// 列的id 当"common" 时该列为扩展列 type类型 format格式 header列头 width列宽 sort是否排序
		// hide是否隐藏 align左右格式 xtype ediable是否可编辑 renderer渲染 editor编辑器
		map.put("id", new String[] { "", "string", "", "ID", "32", "true",
				"true", "", "", "", "", "" });
		map.put("name", new String[] { "", "string", "", "容器名称", "110", "true",
				"false", "", "", editflag, "", "name" });
		map.put("rowNum", new String[] { "", "string", "", "行数", "80", "true",
				"false", "", "", editflag, "", "rowNum" });
		map.put("colNum", new String[] { "", "string", "", "列数", "80", "true",
				"false", "", "", editflag, "", "colNum" });

		// 生成ext用type字符串
		exttype = generalexttype(map);
		// 生成ext用col字符串
		extcol = generalextcol(map);
		putObjToContext("type", exttype);
		putObjToContext("col", extcol);
		putObjToContext("path", ServletActionContext.getRequest()
				.getContextPath()
				+ "/storage/container/storageContainerList.action");
		putObjToContext("statement", "");
		putObjToContext("statementLisener", "");

		putObjToContext("handlemethod", handlemethod);
		return dispatcher("/WEB-INF/page/storage/container/showStorageContainerList.jsp");
	}*/
	@Action(value = "storageContainerListView", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showStorageContainerList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		String scId = getRequest().getParameter("id");  
		putObjToContext("id", scId);
		return dispatcher("/WEB-INF/page/storage/container/showStorageContainerList.jsp");
	}

	@Action(value = "storageContainerListJson")
	public void storageContainerList() {
		/*int start = Integer.parseInt(getParameterFromRequest("start"));
		int limit = Integer.parseInt(getParameterFromRequest("limit"));

		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		*/
//		long totalCount = 0;
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String classify=getParameterFromRequest("id");
		String draw = getParameterFromRequest("draw");
		try{
		Map<String, Object> result = this.containerService.queryContainer(classify,start, length, query, col, sort);
//		totalCount = (Long) result.get("count");
		List<StorageContainer> list = (List<StorageContainer>) result
				.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("rowNum", "");
		map.put("colNum", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	

	// 材料转存操作
	/*@Action(value = "changeLocation", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void changeLocation() throws Exception {
		String id = getRequest().getParameter("id");
		String type = getRequest().getParameter("type");
		String newAdr = getRequest().getParameter("newAdr");
		String classType = getRequest().getParameter("classType");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			boolean dataListMap = false;// mapForQuery,
			if (id != null && newAdr != null) {
				dataListMap = this.containerService.changeLocation(id, type,
						newAdr, classType);
			}
			result.put("success", true);
			result.put("data", dataListMap);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}*/
	@Action(value = "changeLocation", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void changeLocation() throws Exception {
		String id = getParameterFromRequest("id");
		String dataJson = getParameterFromRequest("dataJson");
		String logInfo = getParameterFromRequest("logInfo");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			containerService.changeLocation(id, dataJson, logInfo);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 批量分配存储位置
	@Action(value = "batchLocationFun", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void batchLocationFun() throws Exception {
		String idstr = getRequest().getParameter("idstr");
		String[] ids = idstr.split(",");
		String type = getRequest().getParameter("type");
		String classType = getRequest().getParameter("classType");
		String pid = getRequest().getParameter("pid");

		Map<String, Object> result = new HashMap<String, Object>();
		try {
			boolean dataListMap = false;// mapForQuery,
			if (ids.length != 0 && pid != null) {
				dataListMap = this.containerService.batchLocation(ids, type,
						pid, classType);
			}
			result.put("success", true);
			result.put("data", dataListMap);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 添加
	 */
	@Action(value = "saveContainer", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveContainer() throws Exception {
		String jsonString = getParameterFromRequest("data");
		containerService.addOrUpdate(jsonString);
	}

	/**
	 * 选择容器
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showContainerList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showContainerList() throws Exception {
		// 用于接收通用查询的参数
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		// 属性map<字段名称, ext-list属性数组说明,值为空""则没有该属性>
		// id type format header width sort hide align xtype editable renderer
		// editor
		// 列的id 当"common" 时该列为扩展列 type类型 format格式 header列头 width列宽 sort是否排序
		// hide是否隐藏 align左右格式 xtype ediable是否可编辑 renderer渲染 editor编辑器
		map.put("id", new String[] { "", "string", "", "编码", "150", "true",
				"true", "", "", "", "", "" });
		map.put("name", new String[] { "", "string", "", "名称", "150", "true",
				"", "", "", "", "", "" });
		map.put("rowNum", new String[] { "", "string", "", "行数", "80", "true",
				"", "", "", "", "", "" });
		map.put("colNum", new String[] { "", "string", "", "列数", "80", "true",
				"", "", "", "", "", "" });

		String type = generalexttype(map);
		String col = generalextcol(map);
		// 用于ext显示
		putObjToContext("type", type);
		putObjToContext("col", col);

		putObjToContext("path", ServletActionContext.getRequest()
				.getContextPath()
				+ "/storage/container/showContainerJson.action");
		// 导向jsp页面显示
		return dispatcher("/WEB-INF/page/storage/container/showContainerList.jsp");
	}

	@Action(value = "showContainerJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showContainerJson() throws Exception {
		// 开始记录数
		/*int start = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limit = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");

		String data = getParameterFromRequest("type");
		// 取出session中检索用的对象
*/
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String classify=getParameterFromRequest("id");
		String draw = getParameterFromRequest("draw");
		Map<String, Object> controlMap = containerService.queryContainer(classify,start, length, query, col, sort);

		long totalCount = Long.parseLong(controlMap.get("count").toString());
		List<StorageContainer> list = (List<StorageContainer>) controlMap
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("rowNum", "");
		map.put("colNum", "");
		new SendData().sendDateJson(map, list, totalCount,
				ServletActionContext.getResponse());
	}

	@Action(value = "scanContainer", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String scanContainer() {
		String rowNum = (String) getRequest().getParameter("rowNum");
		String colNum = (String) getRequest().getParameter("colNum");

		getRequest().setAttribute("rowNum", Integer.valueOf(rowNum));
		getRequest().setAttribute("colNum", Integer.valueOf(colNum));

		return dispatcher("/WEB-INF/page/storage/container/scanContainer.jsp");
	}

	/**
	 * 查询指定位置的容器使用情况
	 * 
	 * @return
	 */
	@Action(value = "scanPositionContainer", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String scanPositionContainer() {
		String posiId = getRequest().getParameter("posiId");
		String contId = getRequest().getParameter("contId");
		String storageId = getRequest().getParameter("storageId");
		String showName = getRequest().getParameter("showName");

		StorageContainerStore scs = containerService
				.queryStorageContainerStore(posiId, contId);
		getRequest().setAttribute("storageId", storageId);
		getRequest().setAttribute("contId", contId);
		getRequest().setAttribute("showName", showName);
		getRequest().setAttribute("showId", posiId);
		getRequest().setAttribute("scs", scs);
		getRequest().setAttribute("cont",
				containerService.queryStorageContainerById(contId));
		return dispatcher("/WEB-INF/page/storage/container/scanPositionContainer.jsp");
	}

	/**
	 * 查询指定位置的容器使用情况(批量设置实验材料位置时调用)
	 * 
	 * @return
	 */
	@Action(value = "scanPositionContainerBat", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String scanPositionContainerBat() {
		String posiId = getRequest().getParameter("posiId");
		String contId = getRequest().getParameter("contId");
		String storageId = getRequest().getParameter("storageId");
		String showName = getRequest().getParameter("showName");

		StorageContainerStore scs = containerService
				.queryStorageContainerStore(posiId, contId);
		getRequest().setAttribute("storageId", storageId);
		getRequest().setAttribute("contId", contId);
		getRequest().setAttribute("showName", showName);
		getRequest().setAttribute("showId", posiId);
		getRequest().setAttribute("scs", scs);

		getRequest().setAttribute("cont",
				containerService.queryStorageContainerById(contId));
		return dispatcher("/WEB-INF/page/page/container/scanPositionContainerBat.jsp");
	}

	/* 容器显示 */

	@Action(value = "sampleContainerTest", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String sampleContainerTest() {
		String id = getRequest().getParameter("id");
		String type = getRequest().getParameter("type");
		String maxNum = getRequest().getParameter("maxNum");
		if (maxNum == null || maxNum.equals("")) {
			maxNum = "4";
		}
		String counts = getRequest().getParameter("counts");
		Template tl = templateDao.get(Template.class, id);
		StorageContainer sc = null;
		if (id != null && id.length() > 0 && tl.getStorageContainer() != null) {
			sc = containerService.queryStorageContainerById(tl
					.getStorageContainer().getId());
		} else {
			sc = new StorageContainer();
			sc.setRowNum(0);
			sc.setColNum(0);
		}
		getRequest().setAttribute("rowNum", sc.getRowNum());
		getRequest().setAttribute("colNum", sc.getColNum());
		getRequest().setAttribute("type", type);
		getRequest().setAttribute("maxNum", maxNum);
		getRequest().setAttribute("counts", counts);
		if (Integer.parseInt(maxNum) == 0)
			return dispatcher("/WEB-INF/page/storage/container/gridContainerTest.jsp");
		else if (Integer.parseInt(maxNum) == 1)
			return dispatcher("/WEB-INF/page/storage/container/gridContainerTest1.jsp");
		else
			return null;
	}

	/**
	 * 血浆分离
	 * 
	 * @throws Exception
	 */
//	@Action(value = "showGridTestDataJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showGridTestDataJson() throws Exception {
//		String id = getRequest().getParameter("id");
//		String type = getRequest().getParameter("type");
//		String sinble = getRequest().getParameter("sinble");
//		String maxNum = getRequest().getParameter("maxNum");
//		String counts = getRequest().getParameter("counts");
//		Template sp = null;
//		String templateId = "";
//		String codeId = "";
//		if (type != null && !type.equals("") && type.equals("plasma")) {
//			PlasmaTask pt = storagePositionDao.get(PlasmaTask.class, id);
//			templateId = pt.getTemplate().getId();
//			codeId = pt.getId();
//		} else if (type != null && !type.equals("") && type.equals("dna")) {
////			DnaTask pt = storagePositionDao.get(DnaTask.class, id);
////			templateId = pt.getTemplate().getId();
////			codeId = pt.getId();
////			List<DnaTaskItem> item = experimentDnaGetDao.setDnaItemList(id);
////			if (item.size() > 0)
////				counts = item.get(0).getCounts();
//		} else if (type != null && !type.equals("") && type.equals("dnaInfo")) {
////			DnaTask pt = storagePositionDao.get(DnaTask.class, id);
////			templateId = pt.getTemplate().getId();
////			codeId = pt.getId();
////			List<SampleDnaInfo> info = experimentDnaGetDao
////					.setExperimentDnaGetResult(id);
////			if (info.size() > 0)
////				counts = info.get(0).getCounts();
//		}  else if (type != null && !type.equals("") && type.equals("pooling")) {
//			Pooling pt = storagePositionDao.get(Pooling.class, id);
//			templateId = pt.getTemplate().getId();
//			codeId = pt.getId();
//		} else if (type != null && !type.equals("") && type.equals("qc2100")) {
//			Qc2100Task pt = storagePositionDao.get(Qc2100Task.class, id);
//			templateId = pt.getTemplate().getId();
//			codeId = pt.getId();
//		} else if (type != null && !type.equals("") && type.equals("qcQpcr")) {
//			QcQpcrTask pt = storagePositionDao.get(QcQpcrTask.class, id);
//			templateId = pt.getTemplate().getId();
//			codeId = pt.getId();
//		} else if (type != null && !type.equals("")
//				&& type.equals("generation")) {
//			GenerationTask gt = storagePositionDao
//					.get(GenerationTask.class, id);
//			templateId = gt.getTemplate().getId();
//			codeId = gt.getId();
//		} else if (type != null && !type.equals("")
//				&& type.equals("sangerTask")) {
//			SangerTask pt = storagePositionDao.get(SangerTask.class, id);
//			templateId = pt.getTemplate().getId();
//			codeId = pt.getId();
//			List<SangerTaskItem> item = sangerTaskDao.setSangerTaskItemList(id);
//			if (item.size() > 0)
//				counts = item.get(0).getCounts();
//		} else if (type != null && !type.equals("")
//				&& type.equals("sangerTaskInfo")) {
//			SangerTask pt = storagePositionDao.get(SangerTask.class, id);
//			templateId = pt.getTemplate().getId();
//			codeId = pt.getId();
//			List<SangerTaskInfo> info = sangerTaskDao.getSangerTaskInfoList(id);
//			if (info.size() > 0)
//				counts = info.get(0).getCounts();
//		} else if (type != null && !type.equals("") && type.equals("qtTask")) {
//			QtTask pt = qtTaskDao.get(QtTask.class, id);
//			templateId = pt.getTemplate().getId();
//			codeId = pt.getId();
//			List<QtTaskItem> item = qtTaskDao.setQtTaskItemList(id);
//			if (item.size() > 0)
//				counts = item.get(0).getCounts();
//		} else if (type != null && !type.equals("")
//				&& type.equals("qtTaskInfo")) {
//			QtTask pt = qtTaskDao.get(QtTask.class, id);
//			templateId = pt.getTemplate().getId();
//			codeId = pt.getId();
//			List<QtTaskInfo> info = qtTaskDao.getQtTaskInfoList(id);
//			if (info.size() > 0)
//				counts = info.get(0).getCounts();
//		} else if (type != null && !type.equals("")
//				&& type.equals("massarrayTask")) {// 质谱
//			MassarrayTask pt = qtTaskDao.get(MassarrayTask.class, id);
//			templateId = pt.getTemplate().getId();
//			codeId = pt.getId();
//			List<MassarrayTaskItem> item = massarrayTaskDao
//					.getMassarrayTaskItemList(id);
//			if (item.size() > 0)
//				counts = item.get(0).getCounts();
//		} else if (type != null && !type.equals("")
//				&& type.equals("massarrayTaskInfo")) {
//			MassarrayTask pt = qtTaskDao.get(MassarrayTask.class, id);
//			templateId = pt.getTemplate().getId();
//			codeId = pt.getId();
//			List<MassarrayTaskInfo> info = massarrayTaskDao
//					.setMassarrayTaskInfo(id);
//			if (info.size() > 0)
//				counts = info.get(0).getCounts();
//		} else if (type != null && !type.equals("")
//				&& type.equals("sangerPcrTask")) {
//			SangerPcrTask pt = sangerPcrTaskDao.get(SangerPcrTask.class, id);
//			templateId = pt.getTemplate().getId();
//			codeId = pt.getId();
//			List<SangerPcrTaskItem> info = sangerPcrTaskDao.setItemList(id);
//			if (info.size() > 0)
//				counts = info.get(0).getCounts();
//		} else if (type != null && !type.equals("")
//				&& type.equals("sangerChTask")) {
//			SangerChTask pt = sangerChTaskDao.get(SangerChTask.class, id);
//			templateId = pt.getTemplate().getId();
//			codeId = pt.getId();
//			List<SangerChTaskItem> info = sangerChTaskDao
//					.setSangerChTaskItemList(id);
//			if (info.size() > 0)
//				counts = info.get(0).getCounts();
//		}
//
//		sp = storagePositionDao.get(Template.class, templateId);
//		StorageContainer sc = null;
//		if (sp.getStorageContainer() != null
//				&& sp.getStorageContainer().getId() != null
//				&& sp.getStorageContainer().getId().length() > 0) {
//			sc = sp.getStorageContainer();
//		} else {
//			sc = new StorageContainer();
//			sc.setRowNum(0);
//			sc.setColNum(0);
//		}
//		long totalCount = 0;
//		StringBuffer str = new StringBuffer();
//		for (int i = 0; i < sc.getRowNum(); i++) {
//			str.append("{");
//			for (int j = 0; j <= sc.getColNum(); j++) {
//				// String sid = "";
//				Map<String, Object> map = new HashMap<String, Object>();
//				if (codeId != null && j != 0) {
//					// sid = name + "-" + String.valueOf((char) (65 + i)) + j;
//					map = containerService.findMainStorageTest(codeId, i,
//							j - 1, sc.getRowNum(), sc.getColNum(), type,
//							sinble, maxNum, counts, j);
//				}
//
//				if (j == 0) {
//					str.append("'key':'" + String.valueOf((char) (65 + i))
//							+ "',");
//				} else if (map != null) {
//					if (type != null
//							&& !type.equals("")
//							&& (type.equals("sangerTask") || type
//									.equals("sangerTaskInfo"))) {
//						str.append("'" + j + "':'")
//								.append(map.get("mid") + "-" + map.get("mcl"))
//								.append("',");
//					} else if (type != null
//							&& !type.equals("")
//							&& (type.equals("massarrayTask") || type
//									.equals("massarrayTaskInfo"))) {
//						str.append("'" + j + "':'")
//								.append(map.get("mid") + "-" + map.get("mcl"))
//								.append("',");
//					} else if (type != null
//							&& !type.equals("")
//							&& (type.equals("qtTask") || type
//									.equals("qtTaskInfo"))) {
//						str.append("'" + j + "':'").append(map.get("mid"))
//								.append("',");
//					} else {
//						str.append("'" + j + "':'").append(map.get("mid"))
//								.append("',");
//					}
//					/*
//					 * str.append("'" + j + "':'").append(map.get("mid") + "-" +
//					 * map.get("mcl")) .append("',");
//					 */
//				}
//			}
//			str = new StringBuffer(str.substring(0, str.length() - 1))
//					.append("}");
//		}
//		/*
//		 * if (type != null && !type.equals("") && (type.equals("qtTask") ||
//		 * type.equals("qtTaskInfo"))) { for (int i = 0; i < sc.getRowNum();
//		 * i++) { str.append("{"); for (int j = 0; j <= sc.getColNum(); j++) {
//		 * List<QtTaskItem> item = qtTaskDao.setQtTaskItemList(id); for (int a =
//		 * 0; a < item.size(); a++) { QtTaskItem qi = item.get(a); for (int b =
//		 * 1; b <= qi.getNum(); b++) { // String sid = ""; Map<String, Object>
//		 * map = new HashMap<String, Object>(); if (codeId != null && j != 0) {
//		 * // sid = name + "-" + String.valueOf((char) (65 // + i)) + // j;
//		 * 
//		 * map = containerService.findMainStorageTest( codeId, i, j - 1,
//		 * sc.getRowNum(), sc.getColNum(), type, sinble, maxNum, counts, b); }
//		 * 
//		 * if (j == 0) { str.append("'key':'" + String.valueOf((char) (65 + i))
//		 * + "',"); } else if (map != null) { if (type != null &&
//		 * !type.equals("") && (type.equals("sangerTask") || type
//		 * .equals("sangerTaskInfo"))) { str.append("'" + j + "':'")
//		 * .append(map.get("mid") + "" + map.get("mcl")) .append("',"); } else {
//		 * str.append("'" + j + "':'") .append(map.get("mid")) .append("',"); }
//		 * 
//		 * } } } } str = new StringBuffer(str.substring(0, str.length() - 1))
//		 * .append("}"); } } else { for (int i = 0; i < sc.getRowNum(); i++) {
//		 * str.append("{"); for (int j = 0; j <= sc.getColNum(); j++) { //
//		 * String sid = ""; Map<String, Object> map = new HashMap<String,
//		 * Object>(); if (codeId != null && j != 0) { // sid = name + "-" +
//		 * String.valueOf((char) (65 + i)) + // j; map =
//		 * containerService.findMainStorageTest(codeId, i, j - 1,
//		 * sc.getRowNum(), sc.getColNum(), type, sinble, maxNum, counts, 0); }
//		 * 
//		 * if (j == 0) { str.append("'key':'" + String.valueOf((char) (65 + i))
//		 * + "',"); } else if (map != null) { if (type != null &&
//		 * !type.equals("") && (type.equals("sangerTask") || type
//		 * .equals("sangerTaskInfo"))) { str.append("'" + j + "':'")
//		 * .append(map.get("mid") + "" + map.get("mcl")).append("',"); } else {
//		 * str.append("'" + j + "':'").append(map.get("mid")) .append("',"); }
//		 * 
//		 * } } str = new StringBuffer(str.substring(0, str.length() - 1))
//		 * .append("}"); } }
//		 */
//
//		StringBuffer sb = new StringBuffer("{'total':").append(totalCount)
//				.append(",'results':[").append(str).append("]}");
//		String a = sb.toString().replace("}{", "},{");
//		new SendData().sendDataJson(a, ServletActionContext.getResponse());
//	}

	/* 容器显示 */

	@Action(value = "sampleContainer", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String sampleContainer() {
		String id = getRequest().getParameter("id");
		StorageContainer sc = null;
		if (id != null && id.length() > 0) {
			sc = containerService.queryStorageContainerById(id);
		} else {
			sc = new StorageContainer();
			sc.setRowNum(0);
			sc.setColNum(0);
		}
		getRequest().setAttribute("rowNum", sc.getRowNum());
		getRequest().setAttribute("colNum", sc.getColNum());

		return dispatcher("/WEB-INF/page/storage/container/gridContainer.jsp");
	}

	/* 二维容器显示 */
	@Action(value = "sampleContainer2w", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String sampleContainer2w() {
		String rowNum = getRequest().getParameter("rowNum");
		String colNum = getRequest().getParameter("colNum");
		getRequest().setAttribute("rowNum", rowNum);
		getRequest().setAttribute("colNum", colNum);

		return dispatcher("/WEB-INF/page/storage/container/gridContainer2w.jsp");
	}

	/*
	 * @Action(value = "showGridDataJson", interceptorRefs =
	 * 
	 * @InterceptorRef("biolimsDefaultStack")) public void showGridDataJson()
	 * throws Exception { String id = getRequest().getParameter("id");
	 * StoragePosition sp = storagePositionDao.get(StoragePosition.class, id);
	 * StorageContainer sc = null; if (sp.getStorageContainer() != null &&
	 * sp.getStorageContainer().getId() != null &&
	 * sp.getStorageContainer().getId().length() > 0) { sc =
	 * sp.getStorageContainer(); } else { sc = new StorageContainer();
	 * sc.setRowNum(0); sc.setColNum(0); } String name = sp.getId(); long
	 * totalCount = 0; StringBuffer str = new StringBuffer(); String center =
	 * "align:center;font-color:red;font-size:15;"; String style1 =
	 * "height:100%;width:100%;background:#ccc;"; String color = "red"; String
	 * bold = "bold"; for (int i = 0; i < sc.getRowNum(); i++) {
	 * str.append("{"); for (int j = 0; j <= sc.getColNum(); j++) { // String
	 * sid = ""; Map<String, Object> map = new HashMap<String, Object>(); if
	 * (name != null && j != 0) { // sid = name + "-" + String.valueOf((char)
	 * (65 + i)) + j; map = containerService.findMainStorage(name, i, j - 1,
	 * sp.getStorageType()); }
	 * 
	 * if (j == 0) { str.append("'key':'" + String.valueOf((char) (65 + i)) +
	 * "',"); } else if (j != sc.getColNum() && map != null) { str.append( "'" +
	 * j + "':'<table  style=" + style1 +
	 * "><tr height=15px><td><input type=checkbox name=box value=").append(
	 * map.get
	 * ("mid")).append("><a href=javascript:FrozenLocationFun(\"").append(
	 * map.get("mid")) .append(
	 * "\")>转存</a></td></tr><tr height=30px><td><font color=" + color +
	 * " font-weight=" + bold +
	 * ">").append(map.get("id")).append("</td></tr><tr><td>").append(
	 * map.get("name")).append("</td></tr></table>',"); } else if (j ==
	 * sc.getColNum() && map != null) { str.append( "'" + j +
	 * "':'<table  style=" + style1 +
	 * "><tr height=15px><td><input type=checkbox name=box value=").append(
	 * map.get
	 * ("mid")).append("><a href=javascript:FrozenLocationFun(\"").append(
	 * map.get("mid")) .append("\")>转存</a></td></tr><tr height=30px><td style="
	 * + center + ">").append(
	 * map.get("id")).append("</td></tr><tr><td>").append
	 * (map.get("name")).append( "</td></tr></table>'}"); } else if (j ==
	 * sc.getColNum() && map == null) { str.append("'" + j +
	 * "':'<table><tr height=30px><td></td></tr><tr><td></td></tr></table>'}");
	 * } else { str.append("'" + j +
	 * "':'<table><tr height=30px><td></td></tr><tr><td></td></tr></table>',");
	 * } } }
	 * 
	 * StringBuffer sb = new
	 * StringBuffer("{'total':").append(totalCount).append(
	 * ",'results':[").append(str).append( "]}"); String a =
	 * sb.toString().replace("}{", "},{"); new SendData().sendDataJson(a,
	 * ServletActionContext.getResponse()); }
	 */
	/**
	 * type:0:细胞;1:质粒;2:BAC;3:动物;4、精子;5、胚胎;6、组织;
	 * 
	 * @throws Exception
	 */
	@Action(value = "showGridDataJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showGridDataJson() throws Exception {
		String id = getRequest().getParameter("id");
		StoragePosition sp = storagePositionDao.get(StoragePosition.class, id);
		StorageContainer sc = null;
		if (sp.getStorageContainer() != null
				&& sp.getStorageContainer().getId() != null
				&& sp.getStorageContainer().getId().length() > 0) {
			sc = sp.getStorageContainer();
		} else {
			sc = new StorageContainer();
			sc.setRowNum(0);
			sc.setColNum(0);
		}
		String name = sp.getId();
		String type = sp.getStorageType();
		long totalCount = 0;
		StringBuffer str = new StringBuffer();
		for (int i = 0; i < sc.getRowNum(); i++) {
			str.append("{");
			for (int j = 0; j <= sc.getColNum(); j++) {
				// String sid = "";
				Map<String, Object> map = new HashMap<String, Object>();
				if (name != null && j != 0) {
					// sid = name + "-" + String.valueOf((char) (65 + i)) + j;
					map = containerService.findMainStorage(name, i, j - 1);
				}

				if (j == 0) {
					str.append("'key':'" + String.valueOf((char) (65 + i))
							+ "',");
				} else if (map != null) {
					str.append("'" + j + "':'").append(map.get("mid"))
							.append("',");
				}
			}
			str = new StringBuffer(str.substring(0, str.length() - 1))
					.append("}");
		}

		StringBuffer sb = new StringBuffer("{'total':").append(totalCount)
				.append(",'results':[").append(str).append("]}");
		String a = sb.toString().replace("}{", "},{");
		new SendData().sendDataJson(a, ServletActionContext.getResponse());
	}

	// 加载位置架子页面
	@Action(value = "showGridMidDataDialog", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showGridMidDataDialog() {
		String colNum = (String) getRequest().getParameter("colNum");
		String rowNum = (String) getRequest().getParameter("rowNum");
		String idStr = getRequest().getParameter("idStr");
		String type = getRequest().getParameter("type");
		StoragePosition sp = storagePositionDao.get(StoragePosition.class,
				idStr);
		StorageContainer sc = null;
		if (sp.getStorageContainer() != null
				&& sp.getStorageContainer().getId() != null
				&& sp.getStorageContainer().getId().length() > 0) {
			sc = sp.getStorageContainer();
			getRequest()
					.setAttribute("colNum", Integer.valueOf(sc.getColNum()));
			getRequest()
					.setAttribute("rowNum", Integer.valueOf(sc.getRowNum()));
		} else {
			getRequest().setAttribute("colNum", Integer.valueOf(0));
			getRequest().setAttribute("rowNum", Integer.valueOf(0));
		}

		getRequest().setAttribute("tree_Id", idStr);
		getRequest().setAttribute("type", type);
		return dispatcher("/WEB-INF/page/storage/container/gridContainerMidDialog.jsp");
	}

	@Action(value = "showGridDataMidDialogJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showGridDataMidDialogJson() throws Exception {
		String id = getRequest().getParameter("id");
		StoragePosition sp = storagePositionDao.get(StoragePosition.class, id);
		StorageContainer sc = null;
		StorageContainer subsc = null;
		if (sp.getStorageContainer() != null
				&& sp.getStorageContainer().getId() != null
				&& sp.getStorageContainer().getId().length() > 0) {
			sc = sp.getStorageContainer();
		} else {
			sc = new StorageContainer();
			sc.setRowNum(0);
			sc.setColNum(0);
		}

		if (sp.getSubStorageContainer() != null
				&& sp.getSubStorageContainer().getId() != null
				&& sp.getSubStorageContainer().getId().length() > 0) {
			subsc = sp.getSubStorageContainer();
		} else {
			subsc = new StorageContainer();
			subsc.setRowNum(0);
			subsc.setColNum(0);
		}

		String name = sp.getId();
		long totalCount = 0;
		StringBuffer str = new StringBuffer();
		String style1 = "height:20px;width:30px;background:red;";
		String style2 = "height:20px;width:30px;background:#888;";
		for (int i = 0; i < sc.getRowNum(); i++) {
			str.append("{");
			for (int j = 0; j <= sc.getColNum(); j++) {
				String boxName = "";
				long isUse = 0;
				if (name != null && j != 0) {
					if (j < 10) {
						isUse = containerService.findIsUse(name + "-"
								+ String.valueOf((char) (65 + i)) + "0"
								+ String.valueOf(j));
					} else {
						isUse = containerService.findIsUse(name + "-"
								+ String.valueOf((char) (65 + i))
								+ String.valueOf(j));
					}
				}

				String spId = "";
				if (j < 10) {
					spId = name + "-" + String.valueOf((char) (65 + i)) + "0"
							+ String.valueOf(j);
				} else {
					spId = name + "-" + String.valueOf((char) (65 + i))
							+ String.valueOf(j);
				}

				String t = "<table>";
				if (isUse > 0) {

					t = "<table  style=" + style2 + ">";

				}
				List<StorageBox> sbxl = commonService.get(StorageBox.class,
						"newLocationId", spId);
				if (sbxl.size() > 0) {

					boxName = sbxl.get(0).getName();

				}
				String sphref = t + "<tr><td> <a href=javascript:showDialog(\""
						+ spId + "\",\"" + subsc.getId() + "\")>" + isUse
						+ "</a></td></tr>"
						+ "<tr><td><a href=javascript:showBox(\"" + spId
						+ "\")>" + boxName + "</a></td></tr></table>";

				if (j == 0) {
					str.append("'key':'" + String.valueOf((char) (65 + i))
							+ "',");
				} else if (j != sc.getColNum()) {
					str.append("'" + j + "':'" + sphref + "',");
				} else if (j == sc.getColNum()) {
					str.append("'" + j + "':'" + sphref + "'}");

				}
			}
		}

		StringBuffer sb = new StringBuffer("{'total':").append(totalCount)
				.append(",'results':[").append(str).append("]}");
		String a = sb.toString().replace("}{", "},{");
		new SendData().sendDataJson(a, ServletActionContext.getResponse());
	}

	// 加载位置的dialog页面
	@Action(value = "showGridDataDialog", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showGridDataDialog() {
		String colNum = (String) getRequest().getParameter("colNum");
		String rowNum = (String) getRequest().getParameter("rowNum");
		String type = (String) getRequest().getParameter("type");
		String cType = (String) getRequest().getParameter("cType");
		StorageContainer sc = commonService.get(StorageContainer.class, cType);
		String idStr = getRequest().getParameter("idStr");
		List<StorageBox> l = storageBoxService.findStorageBoxList(idStr);
		if (l.size() > 0) {
			sc = l.get(0).getStorageContainer();
			if (sc != null) {
				getRequest().setAttribute("subColNum",
						Integer.valueOf(sc.getColNum()));
				getRequest().setAttribute("subRowNum",
						Integer.valueOf(sc.getRowNum()));
			}
		} else {
			getRequest().setAttribute("subColNum", 0);
			getRequest().setAttribute("subRowNum", 0);
		}
		getRequest().setAttribute("sub_tree_Id", idStr);
		getRequest().setAttribute("type", type);
		return dispatcher("/WEB-INF/page/storage/container/gridContainerDialog.jsp");
	}

	// 查询存放位置信息
	@Action(value = "showGridDataDialogJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showGridDataDialogJson() throws Exception {
		String id = getRequest().getParameter("id");
		String subId = getRequest().getParameter("subId");

		StoragePosition sp = storagePositionDao.get(StoragePosition.class, id);
		StorageContainer sc = null;

		List<StorageBox> l = storageBoxService.findStorageBoxList(subId);

		if (l.size() > 0) {
			sc = l.get(0).getStorageContainer();

		} else {
			if (sc != null && sc.getId() != null && sc.getId().length() > 0) {
				sc = sp.getSubStorageContainer();
			} else {

				sc = new StorageContainer();
				sc.setRowNum(0);
				sc.setColNum(0);
			}

		}
		String name = subId;
		long totalCount = 0;
		StringBuffer str = new StringBuffer();
		String style1 = "background:red;";
		String style2 = "background:red;align:center;";
		for (int i = 0; i < sc.getRowNum(); i++) {
			str.append("{");
			for (int j = 0; j <= sc.getColNum(); j++) {
				int isUse = 0;
				Map<String, Object> map = new HashMap<String, Object>();
				if (name != null && j != 0) {

					map = containerService.findMainStorage(name, i, j);

					if (map != null && map.size() > 0) {

						isUse = 1;
					}
				}

				if (j == 0) {
					str.append("'key':'" + String.valueOf((char) (65 + i))
							+ "',");
				} else if (j != sc.getColNum() && isUse == 1) {
					str.append("'" + j + "':'<table  style=" + style2
							+ "><tr><td>" + map.get("mid")
							+ "</td></tr><tr><td>" + map.get("info")
							+ "</td></tr></table>',");
				} else if (j == sc.getColNum() && isUse == 1) {
					str.append("'" + j + "':'<table  style=" + style2
							+ "><tr><td>" + map.get("mid")
							+ "</td></tr><tr><td>" + map.get("info")
							+ "</td></tr></table>'}");
				} else if (j == sc.getColNum() && isUse == 0) {
					str.append("'" + j
							+ "':'<table><tr><td></td></tr></table>'}");
				} else {
					str.append("'" + j
							+ "':'<table><tr><td></td></tr></table>',");
				}
			}
		}

		StringBuffer sb = new StringBuffer("{'total':").append(totalCount)
				.append(",'results':[").append(str).append("]}");
		String a = sb.toString().replace("}{", "},{");
		new SendData().sendDataJson(a, ServletActionContext.getResponse());
	}

	/* 容器类别 */
	@Action(value = "showSampleContainerList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleContainerList() throws Exception {
		return dispatcher("/WEB-INF/page/storage/container/showStorageContainerViewList.jsp");
	}

	@Action(value = "showSampleContainerListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleContainerListJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String classify=getParameterFromRequest("id");
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = containerService.queryContainer(classify,start, length, query, col, sort);
			List<DicType> list = (List<DicType>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("rowNum", "");
			map.put("colNum", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*int start = Integer.parseInt(getParameterFromRequest("start"));// 字段
		int limit = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		// 取出session中检索用的对象

		Map<String, Object> controlMap = containerService.queryContainer(null,
				start, limit, dir, sort);
		long count = Long.parseLong(controlMap.get("count").toString());
		List<StorageContainer> sclist = (List<StorageContainer>) controlMap
				.get("dataList");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("rowNum", "");
		map.put("colNum", "");
		new SendData().sendDateJson(map, sclist, count,
				ServletActionContext.getResponse());*/
	}

	/**
	 * type:0:细胞;1:质粒;2:BAC;3:动物
	 * 
	 * @throws Exception
	 */
	@Action(value = "showGridData2wJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showGridData2wJson() throws Exception {
		String id = getRequest().getParameter("id");
		StoragePosition sp = storagePositionDao.get(StoragePosition.class, id);
		long totalCount = 0;
		int rowNum = Integer.parseInt(sp.getRowNum());
		int colNum = Integer.parseInt(sp.getColNum());
		StringBuffer str = new StringBuffer();
		for (int i = 0; i < rowNum; i++) {
			str.append("{");
			for (int j = 0; j <= colNum; j++) {
				Map<String, Object> map = new HashMap<String, Object>();

				if (j == 0) {
					str.append("'key':'第" + (i + 1) + "层',");
				} else if (j != 0) {
					String ids = id + "-" + (i + 1) + "-" + j;
					StoragePosition sps = storagePositionDao.get(
							StoragePosition.class, ids);
					String type = sps.getStorageType();
					if (sps.getIsUse() != null && !sps.getIsUse().equals("0")) {
						if (type.equals("3")) {
							str.append("'第" + j + "排':'")
									.append(storagePositionService
											.showAnimalStorageNum(sps
													.getIsUse())).append("',");
						} else {
							str.append("'第" + j + "排':'")
									.append(storagePositionService
											.showStorageNum(sps.getIsUse()))
									.append("',");
						}
					} else {
						str.append("'第" + j + "排':'',");
					}
				}
			}
			str = new StringBuffer(str.substring(0, str.length() - 1))
					.append("}");
		}

		StringBuffer sb = new StringBuffer("{'total':").append(totalCount)
				.append(",'results':[").append(str).append("]}");
		String a = sb.toString().replace("}{", "},{");
		new SendData().sendDataJson(a, ServletActionContext.getResponse());
	}

	// 盒子储位展示
	@Action(value = "showGridBoxDataDialogByLocation", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showGridBoxDataDialogByLocation() {
		String idStr = getRequest().getParameter("idStr");
		String type = getRequest().getParameter("type");
		List<StorageBox> l = storageBoxService.findStorageBoxList(idStr);
		if (l.size() > 0) {
			StorageBox sp = l.get(0);
			StorageContainer sc = null;
			if (sp.getStorageContainer() != null
					&& sp.getStorageContainer().getId() != null
					&& sp.getStorageContainer().getId().length() > 0) {
				sc = sp.getStorageContainer();
				getRequest().setAttribute("colNum",
						Integer.valueOf(sc.getColNum()));
				getRequest().setAttribute("rowNum",
						Integer.valueOf(sc.getRowNum()));
			} else {
				getRequest().setAttribute("colNum", Integer.valueOf(0));
				getRequest().setAttribute("rowNum", Integer.valueOf(0));
			}

			getRequest().setAttribute("tree_Id", idStr);
			getRequest().setAttribute("box_Id", sp.getId());
			getRequest().setAttribute("type", type);
		}
		return dispatcher("/WEB-INF/page/storage/container/gridContainerBoxDialog.jsp");
	}

	// 盒子储位展示
	@Action(value = "showGridBoxDataDialog", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showGridBoxDataDialog() {
		String idStr = getRequest().getParameter("idStr");
		String type = getRequest().getParameter("type");
		StorageBox sp = storagePositionDao.get(StorageBox.class, idStr);
		StorageContainer sc = null;
		if (sp.getStorageContainer() != null
				&& sp.getStorageContainer().getId() != null
				&& sp.getStorageContainer().getId().length() > 0) {
			sc = sp.getStorageContainer();
			getRequest()
					.setAttribute("colNum", Integer.valueOf(sc.getColNum()));
			getRequest()
					.setAttribute("rowNum", Integer.valueOf(sc.getRowNum()));
		} else {
			getRequest().setAttribute("colNum", Integer.valueOf(0));
			getRequest().setAttribute("rowNum", Integer.valueOf(0));
		}

		getRequest().setAttribute("tree_Id", idStr);
		getRequest().setAttribute("type", type);
		return dispatcher("/WEB-INF/page/storage/container/gridContainerBoxDialog.jsp");
	}

	@Action(value = "showGridDataBoxDialogJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showGridDataBoxDialogJson() throws Exception {

		String id = getRequest().getParameter("id");
		StorageBox sp = storagePositionDao.get(StorageBox.class, id);
		StorageContainer sc = null;
		if (sp.getStorageContainer() != null
				&& sp.getStorageContainer().getId() != null
				&& sp.getStorageContainer().getId().length() > 0) {
			sc = sp.getStorageContainer();
		} else {
			sc = new StorageContainer();
			sc.setRowNum(0);
			sc.setColNum(0);
		}
		String name = "";
		if (sp.getNewLocationId() == null || sp.getNewLocationId() != null
				&& sp.getNewLocationId().equals("")) {
			if (sp.getLocationId() == null || sp.getLocationId() != null
					&& sp.getLocationId().equals("")) {
				name = sp.getId();
			} else {
				name = sp.getLocationId();
			}
		} else {
			name = sp.getNewLocationId();

		}
		// String type = sp.getStorageType();
		long totalCount = 0;
		StringBuffer str = new StringBuffer();
		for (int i = 0; i < sc.getRowNum(); i++) {
			str.append("{");
			for (int j = 0; j <= sc.getColNum(); j++) {
				// String sid = "";
				Map<String, Object> map = new HashMap<String, Object>();
				if (name != null) {
					// sid = name + "-" + String.valueOf((char) (65 + i)) + j;
					map = containerService.findMainStorage(name, i, j);
				}

				if (j == 0) {
					str.append("'key':'" + String.valueOf((char) (65 + i))
							+ "',");
				} else if (map != null) {
					str.append("'" + j + "':'").append(map.get("mid"))
							.append("',");
				}
			}
			str = new StringBuffer(str.substring(0, str.length() - 1))
					.append("}");
		}

		StringBuffer sb = new StringBuffer("{'total':").append(totalCount)
				.append(",'results':[").append(str).append("]}");
		String a = sb.toString().replace("}{", "},{");
		new SendData().sendDataJson(a, ServletActionContext.getResponse());

		// String id = getRequest().getParameter("id");
		// StorageBox sp = storagePositionDao.get(StorageBox.class, id);
		// StorageContainer sc = null;
		// StorageContainer subsc = null;
		// if (sp.getStorageContainer() != null
		// && sp.getStorageContainer().getId() != null
		// && sp.getStorageContainer().getId().length() > 0) {
		// sc = sp.getStorageContainer();
		// } else {
		// sc = new StorageContainer();
		// sc.setRowNum(0);
		// sc.setColNum(0);
		// }

		// String name = sp.getId();
		// long totalCount = 0;
		// StringBuffer str = new StringBuffer();
		// String style1 = "height:20px;width:30px;background:red;";
		// String style2 = "height:20px;width:30px;background:#888;";
		// for (int i = 0; i < sc.getRowNum(); i++) {
		// str.append("{");
		// for (int j = 0; j <= sc.getColNum(); j++) {
		// String boxName = "盒";
		// long isUse = 0;
		// if (name != null && j != 0) {
		// if (j < 10) {
		// isUse = containerService.findIsUse(name + "-"
		// + String.valueOf((char) (65 + i)) + "0"
		// + String.valueOf(j));
		// } else {
		// isUse = containerService.findIsUse(name + "-"
		// + String.valueOf((char) (65 + i))
		// + String.valueOf(j));
		// }
		// }
		//
		// String spId = "";
		// if (j < 10) {
		// spId = name + "-" + String.valueOf((char) (65 + i)) + "0"
		// + String.valueOf(j);
		// } else {
		// spId = name + "-" + String.valueOf((char) (65 + i))
		// + String.valueOf(j);
		// }
		//
		// String t = "<table>";
		// if (isUse > 0) {
		// t = "<table  style=" + style2 + ">";
		// }
		// List<StorageBox> sbxl = commonService.get(StorageBox.class,
		// "locationId", spId);
		// if (sbxl.size() > 0) {
		// boxName = sbxl.get(0).getName();
		// }
		// String sphref = t+"</table>";
		//
		// if (j == 0) {
		// str.append("'key':'" + String.valueOf((char) (65 + i))
		// + "',");
		// } else if (j != sc.getColNum()) {
		// str.append("'" + j + "':'" + sphref + "',");
		// } else if (j == sc.getColNum()) {
		// str.append("'" + j + "':'" + sphref + "'}");
		//
		// }
		// }
		// }
		//
		// StringBuffer sb = new StringBuffer("{'total':").append(totalCount)
		// .append(",'results':[").append(str).append("]}");
		// String a = sb.toString().replace("}{", "},{");
		// new SendData().sendDataJson(a, ServletActionContext.getResponse());
	}

	/**
	 * 
	 * @Title: showContainerTable
	 * @Description: 选择容器类型
	 * @author : shengwei.wang
	 * @date 2018年2月8日下午5:27:38
	 * @return String
	 * @throws
	 */
	@Action(value = "showContainerTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showContainerTable() {
		return dispatcher("/WEB-INF/page/storage/container/showStorageContainerSelTable.jsp");

	}

	@Action(value = "showContainerTableJson")
	public void showContainerTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = containerService
					.showContainerTableJson(start, length, query, col, sort);
			List<StorageContainer> list = (List<StorageContainer>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("colNum", "");
			map.put("rowNum", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public StorageContainer getSc() {
		return sc;
	}

	public void setSc(StorageContainer sc) {
		this.sc = sc;
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	
	
	/*//新容器显示
	@Action(value = "showContainerListTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String dicTypeSelectTable() throws Exception {
		String flag = super.getRequest().getParameter("flag");
//
//		String jsonUrl = ServletActionContext.getRequest().getContextPath()
//				+ "/dic/type/dicTypeSelectJson.action?flag=" + flag;
		putObjToContext("flag", flag);
		return dispatcher("/WEB-INF/page/storage/container/showContainerListTable.jsp");
	}
	@Action(value="showContainerTableJson")
	public void showContainerTableJson()throws Exception{
		String flag=getParameterFromRequest("flag");
		String query=getParameterFromRequest("query");
		String colNum=getParameterFromRequest("order[0][column]");
		String col=getParameterFromRequest("columns["+colNum+"][data]");
		String sort=getParameterFromRequest("order[0][dir]");
		Integer start=Integer.valueOf(getParameterFromRequest("start"));
		Integer length=Integer.valueOf(getParameterFromRequest("length"));
		String draw=getParameterFromRequest("draw");
		Map<String,Object> result=dicService.dicTypeSelectTableJson(start,length,query,col,sort,flag);
		List<DicType> list = (List<DicType>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("rowNum", "");
		map.put("colNum", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}
	*/
	

}
