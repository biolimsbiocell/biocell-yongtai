package com.biolims.storage.modify.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.common.service.SystemCodeService;
import com.biolims.file.service.FileInfoService;
import com.biolims.storage.common.constants.SystemCode;
import com.biolims.storage.common.constants.SystemConstants;
import com.biolims.storage.model.Storage;
import com.biolims.storage.model.StorageModify;
import com.biolims.storage.model.StorageModifyItem;
import com.biolims.storage.modify.service.StorageModifyService;
import com.biolims.system.customfields.service.FieldService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.opensymphony.xwork2.ActionContext;

/**
 * 调整管理
 * 
 */
@Controller
@Scope("prototype")
@ParentPackage("default")
@Namespace("/storage/modify")
@SuppressWarnings("unchecked")
public class StorageModifyAction extends BaseActionSupport {

	private static final long serialVersionUID = 2606046923827000471L;

	private String title = "调整管理";
	private StorageModify storageModify = new StorageModify();// 调整
	private StorageModifyItem sai = new StorageModifyItem();// 调整明细
	// 该action权限id
	private String rightsId = "109";
	@Autowired
	private StorageModifyService storageModifyService;
	@Resource
	private SystemCodeService systemCodeService;
/*new*/
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private CommonService commonService;	
	@Resource
	private FieldService fieldService;
	@Resource
	private CommonDAO commonDAO;
	private String log = "";
	
	@Action(value = "showStorageModifyList")
	public String showStorageModifyList() throws Exception {
		rightsId = "109";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/storage/modify/storageModify.jsp");
	}

	@Action(value = "showStorageModifyEditList")
	public String showStorageModifyEditList() throws Exception {
		rightsId = "109";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/storage/modify/storageModifyEditList.jsp");
	}
	@Action(value = "showStorageModifyTableJson")
	public void showStorageModifyTableJson() throws Exception {
		String query = getRequest().getParameter("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {	
			Map<String, Object> result = storageModifyService.findStorageModifyTable(
					start, length, query, col, sort);
			List<StorageModify> list = (List<StorageModify>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("type-id", "");
			map.put("type-name", "");
			map.put("handleUser-id", "");
			map.put("handleUser-name", "");
			map.put("handleDate", "yyyy-MM-dd");
			map.put("endDate", "yyyy-MM-dd");
			map.put("stateName", "");
			map.put("state", "");
			map.put("note", "");
			map.put("confirmUser-id", "");
			map.put("confirmUser-name", "");
				
			String data=new SendData().getDateJsonForDatatable(map, list);
			//根据模块查询自定义字段数据
			Map<String,Object> mapField = fieldService.findFieldByModuleValue("StorageModify");
			String dataStr = PushData.pushFieldData(data, mapField);
			HttpUtils.write(PushData.pushData(draw, result,dataStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Action(value = "storageModifySelectTable",interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogStorageModifyList() throws Exception {
		rightsId = "109";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/storage/modify/storageModifySelectTable.jsp");
	}
	@Action(value = "editStorageModify")
	public String editStorageModify() throws Exception {
		rightsId = "109";
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			storageModify = storageModifyService.findStorageModifyById(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "storageModify");
			this.storageModify = this.storageModifyService
					.findStorageModifyById(id);
			toState(storageModify.getState());
		} else {
			this.storageModify = new StorageModify();
			this.storageModify.setId(SystemCode.DEFAULT_SYSTEMCODE);
			// if (mainType != null && !mainType.equals("")) {
		
			User user = (User) super.getSession().getAttribute(
					SystemConstants.USER_SESSION_KEY);
			this.storageModify.setHandleUser(user);
			storageModify.setCreateUser(user);
			storageModify.setCreateDate(new Date());
			storageModify.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			storageModify.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			storageModify.setHandleDate(new Date());
			/*storageModify.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			storageModify.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));*/
		
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/storage/modify/storageModifyEdit.jsp");
	}
	@Action(value = "copyStorageModify")
	public String copyStorageModify() throws Exception {
		rightsId = "109";
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		storageModify = storageModifyService.findStorageModifyById(id);
		storageModify.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/storage/modify/storageModifyEdit.jsp");
	}
	@Action(value = "save")
	public void save() throws Exception {
		
		
		
		String msg = "";
		String zId = "";
		boolean bool = true;	
		
		try {
			String dataValue = getParameterFromRequest("dataValue");
			String str = "["+dataValue+"]";
			String changeLog = getParameterFromRequest("changeLog");
			String changeLogItem = getParameterFromRequest("changeLogItem");
			List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					str, List.class);
			
			for (Map<String, Object> map : list) {
				storageModify = (StorageModify)commonDAO.Map2Bean(map, storageModify);
			}
			String id = storageModify.getId();
			if(id!=null&&id.equals("")){
				storageModify.setId(null);
			}
			
			if (id == null || id.length() <= 0
					|| SystemCode.DEFAULT_SYSTEMCODE.equals(id)) {
				log = "123";
				String modelName = "StorageModify";
				String markCode = "SM";
				String code = codingRuleService.genTransID(modelName, markCode);
				this.storageModify.setId(code);
			}
			Map aMap = new HashMap();
			Map lMap = new HashMap();
				aMap.put("storageModifyItem",getParameterFromRequest("storageModifyItemJson"));
			
			
			storageModifyService.save(storageModify,aMap,changeLog,lMap,changeLogItem,log);
			
			zId = storageModify.getId();
		} catch (Exception e) {
			e.printStackTrace();
			msg = "保存失败！";
			bool = false;
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("msg", msg);
		map.put("success", bool);
		map.put("id", zId);
	
		String bpmTaskId = getParameterFromRequest("bpmTaskId");
		if (bpmTaskId != null && !bpmTaskId.equals("")
				&& !bpmTaskId.equals("null")) {
			map.put("bpmTaskId", bpmTaskId);
			
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	@Action(value = "viewStorageModify")
	public String toViewStorageModify() throws Exception {
		String id = getParameterFromRequest("id");
		rightsId = "109";
		storageModify = storageModifyService.findStorageModifyById(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/storage/modify/storageModifyEdit.jsp");
	}

	@Action(value = "showStorageModifyItemTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showStorageModifyItemTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = storageModifyService
				.findStorageModifyItemTable(start, length, query, col, sort,
						id);
		List<StorageModifyItem> list=(List<StorageModifyItem>) result.get("list");
		Map<String, String> map=new HashMap<String, String>();
			map.put("id", "");
			map.put("storageModify-id", "");
			map.put("storage-id", "");
			map.put("storage-name", "");
			map.put("storage-searchCode", "");
			map.put("storage-type-id", "");
			map.put("storage-type-name", "");
			map.put("storage-kit-id", "");
			map.put("storage-kit-name", "");
			map.put("storage-unit-name", "");
			map.put("serial", "");
			map.put("code", "");
			map.put("outPrice", "");
			map.put("num", "");
		String data=new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw,result,data));
	}
	/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delStorageModifyItem")
	public void delStorageModifyItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			storageModifyService.delStorageModifyItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	@Action(value = "saveStorageModifyItemTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveStorageModifyItemTable() throws Exception {
		Map aMap = new HashMap();
		Map lMap = new HashMap();
		
		String id=getParameterFromRequest("id");
		storageModify=commonService.get(StorageModify.class, id);
		Map<String, Object> map=new HashMap<String, Object>();
		aMap.put("storageModifyItem",
				getParameterFromRequest("dataJson"));
		lMap.put("storageModifyItem",
				getParameterFromRequest("changeLog"));
		try {
			storageModifyService.save(storageModify, aMap,"",lMap,"",log);
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}



	@Action(value = "saveStorageModifyTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveStorageModifyTable() throws Exception {
		String dataValue = getParameterFromRequest("dataJson");
		String changeLog = getParameterFromRequest("changeLog");
		String changeLogItem = getParameterFromRequest("changeLogItem");
		String str = "["+dataValue+"]";
			
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
					str, List.class);
			
		Map<String, Object> map=new HashMap<String, Object>();
		try {
			
		for (Map<String, Object> map1 : list) {
		
			StorageModify a = new StorageModify();
		
			Map aMap = new HashMap();
			Map lMap = new HashMap();
			a = (StorageModify)commonDAO.Map2Bean(map1, a);
			a.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			a.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
		
			storageModifyService.save(a,aMap,changeLog,lMap,changeLogItem,log);
			map.put("id", a.getId());
		}
			map.put("success", true);
		} catch (Exception e) {
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	@Action(value = "showStorageModifyTempTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showStorageModifyTempTableJson() throws Exception {
		
		String query = getParameterFromRequest("query");
		String id = getParameterFromRequest("id");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		Map<String, Object> result = storageModifyService.findStorageTable(start, length, query, col, sort, id);
		List<Storage> list=(List<Storage>) result.get("list");
		Map<String, String> map=new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("spec", "");
		map.put("type-name", "");
		map.put("studyType-name", "");
		map.put("position-name", "");
		map.put("producer-name", "");
		map.put("num", "");
		map.put("unit-name", "");
		map.put("note", "");
		map.put("state-name", "");
		map.put("dutyUser-name", "");
		map.put("useDesc", "");
		map.put("searchCode", "");
		map.put("source-name", "");
		map.put("barCode", "");
		map.put("createUser-name", "");
		map.put("createDate", "");
		map.put("ifCall", "");
		map.put("kit-id", "");
		map.put("kit-name", "");
		map.put("i5", "");
		map.put("i7", "");
		map.put("currentIndex", "");
		String data=new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw,result,data));
	}
	/**
	 * 
	 * @Title: addStorageModifyItem
	 * @Description: 添加数据到库存调整明细
	 * @author : 
	 * @date 
	 * @throws Exception
	 *             void
	 * @throws
	 */
	@Action(value = "addStorageModifyItem")
	public void addStorageModifyItem() throws Exception {
		String note = getParameterFromRequest("note");
		String instrId = getParameterFromRequest("id");
		String createDate = getParameterFromRequest("createDate");
		String createUser = getParameterFromRequest("createUser");
		String state = getParameterFromRequest("state");
		String typeId = getParameterFromRequest("typeId");
		String endDate = getParameterFromRequest("endDate");
		String  confirmUser = getParameterFromRequest("confirmUser");
		String batchIds = getRequest().getParameter("batchIds");
		
		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String instrumentRepairId= storageModifyService.addStorage(ids, note, instrId, createUser, createDate, state, typeId,endDate,confirmUser,batchIds);
			result.put("success", true);
			result.put("data", instrumentRepairId);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
/*odl
	*//**
	 * 领用申请GRID
	 * 
	 * @return
	 * @throws Exception
	 *//*
	@Action(value = "showStorageModifyList")
	public String getStorageModifyListShow() throws Exception {

		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		map.put("id", new String[] { "", "string", "", "编号", "150", "true", "",
				"", "", "", "", "" });
		map.put("type-name", new String[] { "", "string", "", "类型", "75",
				"true", "", "", "", "", "", "" });
		map.put("handleUser-name", new String[] { "", "string", "", "操作人",
				"150", "true", "", "", "", "", "", "" });

		map.put("handleDate", new String[] { "", "date", "dateFormat:'Y-m-d'",
				"调整时间", "80", "true", "false", "", "", "false", "formatDate",
				"" });
		map.put("endDate", new String[] { "", "string", "", "结束时间", "150",
				"true", "true", "", "", "", "", "" });
		map.put("storageCheck-id", new String[] { "", "string", "", "盘点编号",
				"150", "true", "true", "", "", "", "", "" });
		map.put("createUser-name", new String[] { "", "string", "", "申请人",
				"150", "true", "true", "", "", "", "", "" });
		map.put("stateName", new String[] { "", "string", "", "状态", "150",
				"true", "", "", "", "", "", "" });
		String type = generalexttype(map);
		String col = generalextcol(map);
		super.getRequest().setAttribute("type", type);
		super.getRequest().setAttribute("col", col);
		// 用于判断当前页面类型,LIST类型
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		super.getRequest().setAttribute(
				"path",
				super.getRequest().getContextPath()
						+ "/storage/modify/getStorageModifyList.action");
		return dispatcher("/WEB-INF/page/storage/modify/showStorageModifyList.jsp");
	}

	*//**
	 * 领用申请DataList
	 * 
	 * @throws Exception
	 *//*
	@Action(value = "getStorageModifyList")
	public void getStorageModifyList() throws Exception {
		// 开始记录数
		int startNum = 0;
		// limit
		int limitNum = 23;
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String mainType = getParameterFromRequest("mainType");
		// 取出检索用的对象
		String data = getParameterFromRequest("data");

		Map<String, Object> result = this.storageModifyService
				.findStorageModify(startNum, limitNum, dir, sort, data,
						mainType);
		Long count = (Long) result.get("totalCount");
		List<StorageModify> list = (List<StorageModify>) result.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("type-name", "");
		map.put("handleUser-name", "");
		map.put("handleDate", "yyyy-MM-dd");
		map.put("endDate", "yyyy-MM-dd");
		map.put("storageCheck-id", "");
		map.put("createUser-name", "");
		map.put("stateName", "");
		map.put("endDate", "yyyy-MM-dd");
		map.put("storageCheck-id", "");
		map.put("createUser-name", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	*//**
	 * 添加、修改领用申请页面
	 * 
	 * @return
	 * @throws Exception
	 *//*
	@Action(value = "toEditStorageModify")
	public String toEditStorageModify() throws Exception {
		String id = super.getRequest().getParameter("id");
		String handlemethod = "";
		String mainType = super.getRequest().getParameter("mainType");
		if (id != null) {
			this.storageModify = this.storageModifyService
					.findStorageModifyById(id);
			if (storageModify.getState().equals(SystemConstants.DIC_STATE_YES))
				handlemethod = SystemConstants.PAGE_HANDLE_METHOD_VIEW;
			else
				handlemethod = SystemConstants.PAGE_HANDLE_METHOD_MODIFY;
			showStorageModifyItemList(handlemethod, id);
			toState(storageModify.getState());
			toToolBar(rightsId, "", "", handlemethod);
		} else {
			this.storageModify = new StorageModify();
			this.storageModify.setId(SystemCode.DEFAULT_SYSTEMCODE);
			// if (mainType != null && !mainType.equals("")) {
			// DicStorageMainType dmt = new DicStorageMainType();
			// dmt.setId(mainType);
			// storageModify.setMainType(dmt);
			// }
			User user = (User) super.getSession().getAttribute(
					SystemConstants.USER_SESSION_KEY);
			this.storageModify.setHandleUser(user);
			storageModify.setCreateUser(user);
			storageModify.setCreateDate(new Date());
			storageModify.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			storageModify.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			storageModify.setHandleDate(new Date());
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			showStorageModifyItemList(SystemConstants.PAGE_HANDLE_METHOD_ADD,
					id);
		}
		return dispatcher("/WEB-INF/page/storage/modify/editStorageModify.jsp");
	}
		
		
	*//**
	 * 复制页面
	 * 
	 * @return
	 * @throws Exception
	 *//*
	@Action(value = "toCopyStorageModify")
	public String toCopyStorageModify() throws Exception {
		String id = super.getRequest().getParameter("id");
		User user = (User) super.getSession().getAttribute(
				SystemConstants.USER_SESSION_KEY);
		storageModify = this.storageModifyService.findStorageModifyById(id);
		storageModify.setId(SystemCode.DEFAULT_SYSTEMCODE);
		storageModify.setHandleUser(user);
		storageModify.setHandleDate(new Date());
		storageModify.setCreateUser(user);
		storageModify.setCreateDate(new Date());
		storageModify.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
		storageModify.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
		showStorageModifyItemList(SystemConstants.PAGE_HANDLE_METHOD_ADD, id);
		toSetStateCopy();
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		return dispatcher("/WEB-INF/page/storage/modify/editStorageModify.jsp");
	}

	@Action(value = "toViewStorageModify")
	public String toViewStorageModify() throws Exception {
		String id = super.getRequest().getParameter("id");
		String handlemethod = "";
		if (id != null) {
			this.storageModify = this.storageModifyService
					.findStorageModifyById(id);
			handlemethod = SystemConstants.PAGE_HANDLE_METHOD_VIEW;
			showStorageModifyItemList(handlemethod, id);
			toToolBar(rightsId, "", "", handlemethod);
		}

		return dispatcher("/WEB-INF/page/storage/modify/editStorageModify.jsp");
	}

	*//**
	 * 领用申请明细GRID
	 * 
	 * @return
	 * @throws Exception
	 *//*

	public void showStorageModifyItemList(String handleMethod,
			String storageModifyId) throws Exception {

		String exttype = "";
		String extcol = "";
		String editflag = "true";

		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		map.put("id", new String[] { "", "string", "", "ID", "150", "true",
				"true", "", "", "", "", "" });
		map.put("storage-id", new String[] { "", "string", "", "调整对象", "150",
				"true", "false", "", "", "", "", "" });
		map.put("storage-name", new String[] { "", "string", "", "名称", "200",
				"true", "false", "", "", "false", "", "" });

		map.put("storage-searchCode", new String[] { "", "string", "", "检索码",
				"100", "true", "true", "", "", "false", "", "" });

		map.put("storage-type-id", new String[] { "", "string", "", "对象类型",
				"100", "true", "true", "", "", "false", "", "" });

		map.put("storage-unit-name", new String[] { "", "string", "", "单位",
				"75", "true", "false", "", "", "", "", "" });
		map.put("serial", new String[] { "", "string", "", "批次编码", "75",
				"true", "false", "", "", editflag, "", "serial" });

		map.put("storage-outPrice", new String[] { "", "string", "", "出库价格",
				"75", "true", "true", "", "", "", "", "" });
		map.put("storage-num", new String[] { "", "string", "", "库存总数量", "75",
				"true", "false", "", "", "", "", "" });
		map.put("num", new String[] { "", "float", "", "调整值", "75", "true",
				"false", "", "", editflag, "", "num" });

		map.put("code", new String[] { "", "string", "", "批号", "75", "true",
				"false", "", "", editflag, "", "code" });

		map.put("supplierName", new String[] { "", "string", "", "生产商", "100",
				"true", "", "", "", editflag, "", "supplierName" });

		map.put("expireDate", new String[] { "", "date", "dateFormat:'Y-m-d'",
				"失效日期", "150", "true", "false", "", "", editflag, "formatDate",
				"new Ext.form.DateField({format: 'Y-m-d'})" });

		// 生成ext用type字符串
		exttype = generalexttype(map);
		// 生成ext用col字符串
		extcol = generalextcol(map);
		putObjToContext("type", exttype);
		putObjToContext("col", extcol);
		putObjToContext(
				"path",
				ServletActionContext.getRequest().getContextPath()
						+ "/storage/modify/getStorageModifyItemList.action?storageModifyId="
						+ storageModifyId);
		putObjToContext("storageModifyId", storageModifyId);
		putObjToContext("handlemethod", handleMethod);

	}

	*//**
	 * 领用申请明细DataList
	 * 
	 * @throws Exception
	 *//*
	@Action(value = "getStorageModifyItemList")
	public void getStorageModifyItemList() throws Exception {
		String storageModifyId = super.getRequest().getParameter(
				"storageModifyId");
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");

		if (storageModifyId != null) {
			Map<String, Object> result = this.storageModifyService
					.findStorageModifyItemBySaId(storageModifyId, startNum,
							limitNum, dir, sort);
			List<StorageReagentBuySerial> list = (List<StorageReagentBuySerial>) result
					.get("result");
			Long count = (Long) result.get("count");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("storage-id", "");
			map.put("storage-name", "");
			map.put("storage-searchCode", "");
			map.put("storage-type-id", "");
			map.put("storage-type-name", "");
			map.put("storage-kit-id", "");
			map.put("storage-kit-name", "");
			map.put("storage-type-id", "");
			map.put("storage-unit-name", "");
			map.put("serial", "");
			map.put("code", "");
			map.put("supplierName", "");
			map.put("expireDate", "yyyy-MM-dd");
			map.put("storage-outPrice", "#.####");
			map.put("storage-num", "#.##");
			map.put("num", "");

			new SendData().sendDateJson(map, list, count,
					ServletActionContext.getResponse());
		}
	}

	*//**
	 * 添加、修改领用申请
	 * 
	 * @return
	 * @throws Exception
	 *//*
	@Action(value = "save")
	public String save() throws Exception {

		String data = getParameterFromRequest("jsonDataStr");
		String id = this.storageModify.getId();
		if (id == null || id.length() <= 0
				|| SystemCode.DEFAULT_SYSTEMCODE.equals(id)) {
			String code = systemCodeService.getSystemCode(
					SystemCode.STORAGE_MODIFY_NAME,
					SystemCode.STORAGE_MODIFY_CODE, null, null);
			this.storageModify.setId(code);
		}
		storageModifyService.save(storageModify);
		if (data != null && !data.equals(""))
			saveStorageModifyItem(storageModify.getId(), data);

		return redirect("/storage/modify/toEditStorageModify.action?id="
				+ storageModify.getId());
	}

	*//**
	 * 保存明细
	 *//*

	public void saveStorageModifyItem(String id, String json) throws Exception {

		storageModifyService.saveStorageModifyItem(id, json);

	}

	*//**
	 * 保存明细
	 *//*
	@Action(value = "saveStorageModifyItem")
	public void saveStorageModifyItem() throws Exception {
		String checkId = getParameterFromRequest("modifyId");
		String json = getParameterFromRequest("data");
		saveStorageModifyItem(checkId, json);

	}

	*//**
	 * 删除明细
	 *//*
	@Action(value = "delModifyStorageItem")
	public void delModifyStorageItem() throws Exception {
		String id = getParameterFromRequest("id");
		storageModifyService.deStorageModifyItem(id);

	}

	 @Action(value = "getStorageCheckItemList")
	 public void getStorageCheckItemList() throws Exception {
	 String storageCheckId = super.getRequest().getParameter("id");
	 // 开始记录数
	 int startNum = 0;
	 // limit
	 int limitNum = 23;
	 // 字段
	 String dir = getParameterFromRequest("dir");
	 // 排序方式
	 String sort = getParameterFromRequest("sort");
	
	 Map<String, Object> result = storageModifyService
	 .findStorageModify(startNum, limitNum, dir, sort, null,
				null);
	 List<StorageReagentBuySerial> list = (List<StorageReagentBuySerial>)
	 result
	 .get("result");
	
	 Long count = (Long) result.get("count");
	 Map<String, String> map = new HashMap<String, String>();
	 map.put("id", "");
	 map.put("storage-id", "");
	 map.put("storage-name", "");
	 map.put("storage-searchCode", "");
	 map.put("storage-type-id", "");
	 map.put("storage-outPrice", "#.####");
	 map.put("storage-unit-name", "");
	 // map.put("productDate", "yyyy-MM-dd");
	 // map.put("expireDate", "yyyy-MM-dd");
	 map.put("storage-position-id", "");
	 map.put("num", "");
	 map.put("modifyNum", "");
	 map.put("storage-unit-name", "");
	
	
	 new SendData().sendDateJson(map, list, count,
	 ServletActionContext.getResponse());
	 }*/

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public StorageModify getStorageModify() {
		return storageModify;
	}

	public void setStorageModify(StorageModify storageModify) {
		this.storageModify = storageModify;
	}

	public StorageModifyItem getSai() {
		return sai;
	}

	public void setSai(StorageModifyItem sai) {
		this.sai = sai;
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}
}
