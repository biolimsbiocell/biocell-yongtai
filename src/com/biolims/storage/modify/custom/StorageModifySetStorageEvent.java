package com.biolims.storage.modify.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.storage.common.constants.SystemConstants;
import com.biolims.storage.modify.service.StorageModifyService;

public class StorageModifySetStorageEvent implements ObjectEvent {
	public String operation(String applicationTypeActionId, String formId) throws Exception {

		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		StorageModifyService storageModifyService = (StorageModifyService) ctx.getBean("storageModifyService");
		storageModifyService.storageModifySetStorage(applicationTypeActionId, formId);
/*		storageModifyService.commonSetState(applicationTypeActionId, formId, SystemConstants.DIC_STATE_YES_ID);
*/
		return "";
	}
}
