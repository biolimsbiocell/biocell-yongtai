package com.biolims.storage.modify.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.applicationType.service.ApplicationTypeService;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.SystemCodeService;
import com.biolims.dic.model.DicType;

import com.biolims.equipment.model.Instrument;
import com.biolims.equipment.model.InstrumentFault;
import com.biolims.equipment.model.InstrumentFaultDetail;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.storage.common.constants.SystemCode;
import com.biolims.storage.common.constants.SystemConstants;
import com.biolims.storage.model.Storage;
import com.biolims.storage.model.StorageModify;
import com.biolims.storage.model.StorageModifyItem;
import com.biolims.storage.model.StorageOut;
import com.biolims.storage.model.StorageOutItem;
import com.biolims.storage.model.StorageReagentBuySerial;
import com.biolims.storage.modify.dao.StorageModifyDao;
import com.biolims.util.BeanUtils;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.biolims.workflow.WorkflowConstants;
import com.opensymphony.xwork2.ActionContext;

@Service
@Transactional
public class StorageModifyService extends ApplicationTypeService {

	@Resource
	private StorageModifyDao storageModifyDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SystemCodeService systemCodeService;
	@Resource
	private CodingRuleService codingRuleService;

	/**
	 * 查询领用申请列表
	 * 
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param sa
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findStorageModify(int startNum, int limitNum, String dir, String sort, String data,
			String type) throws Exception {
		// 检索条件map,每个字段检索值
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		// 检索方式map,每个字段的检索方式
		Map<String, String> mapForCondition = new HashMap<String, String>();
		String startDate = "";
		String endDate = "";
		if (data != null && !data.equals("")) {
			BeanUtils.data2Map(data, mapForQuery, mapForCondition);
		}
		if (type != null && !type.equals("")) {
			mapForCondition.put("mainType.id", "=");
			mapForQuery.put("mainType.id", type);
		}
		// Map<String, Object> map = this.storageModifyDao.findStorageModify(
		// startNum, limitNum, dir, sort, sa);
		// 将map值传入，获取列表
		Map<String, Object> criteriaMap = storageModifyDao.findObjectCriteria(startNum, limitNum, dir, sort,
				StorageModify.class, mapForQuery, mapForCondition);
		Criteria cr = (Criteria) criteriaMap.get("criteria");
		Criteria cc = (Criteria) criteriaMap.get("criteriaCount");
		Map<String, Object> map = JsonUtils.toObjectByJson(data, Map.class);
		if (data != null && !data.equals("")) {
			if (map.get("queryStartDate") != null && !map.get("queryStartDate").equals("")) {
				startDate = (String) map.get("queryStartDate");
				cr.add(Restrictions.ge("outDate", DateUtil.parse(startDate)));
				cc.add(Restrictions.ge("outDate", DateUtil.parse(startDate)));
			}
			if (map.get("queryEndDate") != null && !map.get("queryEndDate").equals("")) {
				endDate = (String) map.get("queryEndDate");
				cr.add(Restrictions.le("outDate", DateUtil.parse(endDate)));
				cc.add(Restrictions.le("outDate", DateUtil.parse(endDate)));
			}
		}
		Map<String, Object> controlMap = storageModifyDao.findObjectList(startNum, limitNum, dir, sort,
				StorageModify.class, mapForQuery, mapForCondition, cc, cr);

		return controlMap;
	}

	/**
	 * 根据ID查询领用申请
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public StorageModify findStorageModifyById(String id) throws Exception {
		StorageModify sa = storageModifyDao.get(StorageModify.class, id);
		return sa;
	}

	/**
	 * 根据领用申请查询领用明细对象
	 * 
	 * @param storageModifyId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findStorageModifyItemBySaId(String storageModifyId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		Map<String, Object> map = this.storageModifyDao.findStorageModifyItemBySaId(storageModifyId, startNum, limitNum,
				dir, sort);
		// List<StorageModifyItem> list = (List<StorageModifyItem>)
		// map.get("result");
		return map;
	}

	public Map<String, Object> findStorageModifyItemByStorageId(String storageModifyId, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		Map<String, Object> map = this.storageModifyDao.findStorageModifyItemBySaId(storageModifyId, startNum, limitNum,
				dir, sort);
		// List<StorageModifyItem> list = (List<StorageModifyItem>)
		// map.get("result");
		return map;
	}

	public List<StorageModifyItem> findStorageModifyItemById(String id) throws Exception {
		List<StorageModifyItem> list = this.storageModifyDao.findStorageModifyItemByStorageId(id);
		return list;
	}

	/**
	 * 保存或更新领用申请信息
	 * 
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(StorageModify sa) throws Exception {
		this.storageModifyDao.saveOrUpdate(sa);
	}

	/**
	 * 
	 * 维护明细信息,由ext ajax调用
	 * 
	 * @param userId     用户id
	 * @param jsonString 用户认证jsonString
	 * @return void
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveStorageModifyItem(String storageModifyId, String jsonString) throws Exception {

		// 将json读入map
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(jsonString, List.class);
		for (Map<String, Object> map : list) {
			StorageModifyItem em = new StorageModifyItem();

			// 将map信息读入实体类
			em = (StorageModifyItem) storageModifyDao.Map2Bean(map, em);

			StorageModify ep = new StorageModify();
			ep.setId(storageModifyId);
			if (em.getId() != null && em.getId().equals(""))
				em.setId(null);
			em.setStorageModify(ep);
			storageModifyDao.saveOrUpdate(em);
		}
	}

	/**
	 * 
	 * 删除明细,由ext ajax调用
	 * 
	 * @param id 认证ID
	 * @return void
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void deStorageModifyItem(String id) {
		StorageModifyItem ppi = new StorageModifyItem();
		ppi.setId(id);
		storageModifyDao.delete(ppi);
	}

	/**
	 * 库存调整动作
	 * 
	 * @param formId
	 * @return
	 * @throws Exception
	 */
	@Transactional(rollbackFor = Exception.class)
	public void storageModifySetStorage(String applicationTypeActionId, String formId) throws Exception {
		// int addNum = 0;
		StorageModify in = new StorageModify();
		in = commonDAO.get(StorageModify.class, formId);
		in.setState(WorkflowConstants.WORKFLOW_COMPLETE);
		in.setStateName(WorkflowConstants.WORKFLOW_COMPLETE_NAME);
		commonDAO.saveOrUpdate(in);
		String storageModifyType = in.getStorageModifyType();
		List<StorageModifyItem> list = findStorageModifyItemById(formId);
		for (StorageModifyItem em : list) {
			Storage s = get(em.getStorage().getClass(), em.getStorage().getId());
			Double sn = 0.0;
			Double nn = 0.0;
			if (em.getNum() != null) {
				// 库存数量=原数量+调整数量
				if (s.getNum() != null) {
					if (storageModifyType.equals("1")) {
						nn = s.getNum();
						sn = nn;
						nn += em.getNum();
						s.setNum(nn);
					} else {
						if (s.getNum() >= em.getNum()) {
							nn = s.getNum();
							sn = nn;
							nn -= em.getNum();

							s.setNum(nn);
						}
					}
					// 调整价格
					if (s.getOutPrice() != null) {

						s.setOutPrice((s.getOutPrice() * sn) / nn);
					}
				}
				saveOrUpdate(s);
				Double rn = 0.0;
				Double srn = 0.0;

				if (s.getType().getId().startsWith(SystemConstants.DIC_STORAGE_SHIYANCAILIAO_YPC)) {
					StorageReagentBuySerial srb = get(StorageReagentBuySerial.class, em.getSerial());

					if (em.getNum() != null && srb.getNum() != null) {
						rn = em.getNum();
						srn = srb.getNum();
						if ("1".equals(in.getStorageModifyType())) {

							srb.setNum(srn + rn);
						} else if ("0".equals(in.getStorageModifyType())) {

							srb.setNum(srn - rn);
						}
					}

					if (em.getSerial() != null && !em.getSerial().equals("")) {
						srb.setSerial(em.getSerial());
					}
					// if (em.getExpireDate() != null) {
					// srb.setExpireDate(em.getExpireDate());
					// }

					saveOrUpdate(srb);

				}
			}

		}
	}

	public Map<String, Object> findStorageModifyTable(Integer start, Integer length, String query, String col,
			String sort) throws Exception {
		return storageModifyDao.findStorageModifyTable(start, length, query, col, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveStorageModifyItem(StorageModify sc, String itemDataJson, String logInfo, String changeLogItem,String log)
			throws Exception {
		List<StorageModifyItem> saveItems = new ArrayList<StorageModifyItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			StorageModifyItem scp = new StorageModifyItem();
			// 将map信息读入实体类
			scp = (StorageModifyItem) storageModifyDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setStorageModify(sc);

			saveItems.add(scp);
		}
		storageModifyDao.saveOrUpdateAll(saveItems);
		if (changeLogItem != null && !"".equals(changeLogItem)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setFileId(sc.getId());
			li.setClassName("StorageModify");
			li.setModifyContent(changeLogItem);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			commonDAO.saveOrUpdate(li);
		}

	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delStorageModifyItem(String[] ids) throws Exception {
		for (String id : ids) {
			StorageModifyItem scp = storageModifyDao.get(StorageModifyItem.class, id);
			storageModifyDao.delete(scp);
			if (ids.length > 0) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setClassName("StorageModify");
				li.setFileId(scp.getStorageModify().getId());
				li.setModifyContent("库存调整:"+"原辅料盒ID:"+scp.getStorage().getId()+"名称:"+scp.getStorage().getName()+"的数据已删除");
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}
			
		}

	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(StorageModify sc, Map jsonMap, String logInfo, Map logMap, String changeLogItem,String log) throws Exception {
		if (sc != null) {
			sc.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			sc.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			storageModifyDao.saveOrUpdate(sc);

			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(sc.getId());
				li.setClassName("StorageModify");
				li.setModifyContent(logInfo);
				if("123".equals(log)) {
					li.setState("1");
					li.setStateName("数据新增");
				}else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				commonDAO.saveOrUpdate(li);
			}

			String jsonStr = "";
			String logStr = "";
			logStr = (String) logMap.get("storageModifyItem");
			jsonStr = (String) jsonMap.get("storageModifyItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveStorageModifyItem(sc, jsonStr, logStr, changeLogItem,log);
			}
		}
	}

	public Map<String, Object> findStorageModifyItemTable(Integer start, Integer length, String query, String col,
			String sort, String id) throws Exception {
		Map<String, Object> result = storageModifyDao.findStorageModifyItemTable(start, length, query, col, sort, id);
		List<StorageModifyItem> list = (List<StorageModifyItem>) result.get("list");
		return result;
	}

	public Map<String, Object> findStorageTable(Integer start, Integer length, String query, String col, String sort,
			String id) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> result = storageModifyDao.findStorageTable(start, length, query, col, sort);
		List<Storage> list = (List<Storage>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String addStorage(String[] ids, String note, String instrId, String createUser, String createDate,
			String state, String typeId, String endDate, String confirmUser, String batchIds) throws Exception {

		// 保存主表信息
		StorageModify si = new StorageModify();
		String log = "";
		if (instrId == null || instrId.length() <= 0 || "NEW".equals(instrId)) {
			log = "123";
			String modelName = "StorageModify";
			String markCode = "SM";
			String code = codingRuleService.genTransID(modelName, markCode);
			si.setId(code);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			DicType type = commonDAO.get(DicType.class, typeId);
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			User confirmU = commonDAO.get(User.class, confirmUser);
			si.setConfirmUser(confirmU);
			si.setHandleUser(u);
			si.setCreateUser(u);
			si.setType(type);
			si.setCreateDate(sdf.parse(createDate));
			if (null != endDate && !"".equals(endDate)) {
				si.setEndDate(sdf.parse(endDate));
			}
			si.setState("3");
			si.setStateName("NEW");
			si.setNote(note);
			instrId = si.getId();
			si.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
			si.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));
			commonDAO.saveOrUpdate(si);
		} else {
			si = commonDAO.get(StorageModify.class, instrId);
		}
		for (int i = 0; i < ids.length; i++) {
			String id = ids[i];

			String[] batchId = batchIds.split(",");
			for (int t = 0; t < batchId.length; t++) {
				String batch = batchId[t];
				// 通过id查询库存主数据
				StorageModifyItem sii = new StorageModifyItem();

				StorageReagentBuySerial srbs = commonDAO.get(StorageReagentBuySerial.class, batch);

				sii.setStorage(srbs.getStorage());
				sii.setSerial(srbs.getId());
				sii.setCode(srbs.getSerial());
				sii.setNum(srbs.getNum());

				sii.setStorageModify(si);
				commonDAO.saveOrUpdate(sii);
				
				String kucun = "库存调整:" +srbs.getStorage().getId() + "的数据已添加到明细";//+ "原辅料盒名称:" + srbs.getStorage().getKit().getName()
				if (kucun != null && !"".equals(kucun)) {
					LogInfo li = new LogInfo();
					li.setLogDate(new Date());
					User u = (User) ServletActionContext.getRequest().getSession()
							.getAttribute(SystemConstants.USER_SESSION_KEY);
					li.setUserId(u.getId());
					li.setClassName("StorageModify");
					li.setFileId(instrId);
					li.setModifyContent(kucun);
					if("123".equals(log)) {
						li.setState("1");
						li.setStateName("数据新增");
					}else {
						li.setState("3");
						li.setStateName("数据修改");
					}
					commonDAO.saveOrUpdate(li);
				}
			}
		}
		return instrId;
	}
}
