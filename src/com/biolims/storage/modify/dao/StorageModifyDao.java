package com.biolims.storage.modify.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.common.dao.CommonDAO;
import com.biolims.equipment.model.Instrument;
import com.biolims.storage.model.StorageModify;
import com.biolims.storage.model.StorageModifyItem;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class StorageModifyDao extends CommonDAO {

	/**
	 * 查询领用申请列表
	 * 
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param sa
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findStorageModify(int startNum, int limitNum, String dir, String sort, StorageModify sa)
			throws Exception {
		String key = " ";
		if (sa != null) {
			String id = sa.getId();
			if (id != null && id.trim().length() > 0)
				key = key + " and id = ' " + id + " ' ";
		}
		String hql = " from StorageModify where 1=1 ";
		hql = hql + key;

		hql = hql + " order by " + sort + " " + dir;

		List<StorageModify> list = this.getSession().createQuery(hql).setFirstResult(startNum).setMaxResults(limitNum)
				.list();
		hql = " select count(id) from StorageModify where 1=1 " + key;
		Long count = (Long) this.getSession().createQuery(hql).uniqueResult();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("count", count);
		map.put("result", list);
		return map;
	}

	/**
	 * 根据领用申请查询领用明细对象
	 * 
	 * @param storageModifyId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findStorageModifyItemBySaId(String storageModifyId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from StorageModifyItem where storageModify.id='" + storageModifyId + "'";
		List<StorageModifyItem> list = this.getSession().createQuery(hql).setFirstResult(startNum).setMaxResults(
				limitNum).list();
		hql = "select count(id) " + hql;
		Long count = (Long) this.getSession().createQuery(hql).uniqueResult();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("count", count);
		map.put("result", list);
		return map;
	}

	public List<StorageModifyItem> findStorageModifyItemByStorageId(String storageModifyId, String storageId)
			throws Exception {
		String hql = "from StorageModifyItem where storageModify.id='" + storageModifyId + "' and storage.id='"
				+ storageId + "'";
		List<StorageModifyItem> list = this.getSession().createQuery(hql).list();

		return list;
	}

	public List<StorageModifyItem> findStorageModifyItemByStorageId(String storageModifyId) throws Exception {
		String hql = "from StorageModifyItem where storageModify.id='" + storageModifyId + "'";
		List<StorageModifyItem> list = this.getSession().createQuery(hql).list();

		return list;
	}

	public Map<String, Object> findStorageModifyTable(Integer start,Integer length, String query, String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from  StorageModify where 1=1";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from StorageModify where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<StorageModify> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> findStorageModifyItemTable(Integer start, Integer length, String query, String col, String sort, String id) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from StorageModifyItem where 1=1 and storageModify.id='"+id+"'";
		String key = "";

		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from StorageModifyItem where 1=1 and storageModify.id='"+id+"' ";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<StorageModifyItem> list = new ArrayList<StorageModifyItem>();
			list=this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

	public Map<String, Object> findStorageTable(Integer start, Integer length, String query, String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from Storage where 1=1 ";
		String key = "";
		if(query!=null){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from Storage where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			
			List<Instrument> list = new ArrayList<Instrument>();
			list=this.getSession().createQuery(hql + key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}



}
