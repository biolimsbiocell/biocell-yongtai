package com.biolims.storage.common.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.storage.common.constants.SystemConstants;
import com.biolims.storage.common.service.DicStorageMainTypeService;
import com.biolims.storage.common.service.DicStorageTypeService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/dicStorageMainType")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public class DicStorageMainTypeAction extends BaseActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9057642774915177182L;

	private String rightsId = "1123";

	private DicStorageMainTypeAction dsmta;

	@Resource
	private DicStorageMainTypeService dicStorageMainTypeService;

	/**
	 * 字典主类别列表
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showDicStorageMainTypeList")
	public String showDicStorageMainTypeList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/dic/showDicStorageMainTypeList.jsp");
	}

	/**
	 * 字典主类别列表
	 * @return
	 * @throws Exception 
	 */
	@Action(value = "showDicStorageMainTypeListJson")
	public void showDicStorageMainTypeListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");

		String queryData = getRequest().getParameter("data");

		Map<String, String> mapForQuery = null;
		if (queryData != null && queryData.length() > 0) {
			mapForQuery = JsonUtils.toObjectByJson(queryData, Map.class);
		}
		try {
			Map<String, Object> result = this.dicStorageMainTypeService.findDicStorageMainTypeList(mapForQuery,
					startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<DicStorageTypeService> list = (List<DicStorageTypeService>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "id");
			map.put("name", "name");
			map.put("state", "state");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 保存字典主类别
	 * @throws Exception
	 */
	@Action(value = "saveDicStorageMainTypeList")
	public void saveDicStorageMainTypeList() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String itemDataJson = getRequest().getParameter("itemDataJson");
			dicStorageMainTypeService.saveDicStorageMainTypeList(itemDataJson);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 *删除表单类型
	 * @throws Exception
	 */
	@Action(value = "delDicStorageMainType")
	public void delDicStorageMainType() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			dicStorageMainTypeService.delDicStorageMainType(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showdicStorageMainTypeList")
	public String showdicStorageMainTypeList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/dic/showDialogDicStorageMainTypeList.jsp");
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public DicStorageMainTypeAction getDsmta() {
		return dsmta;
	}

	public void setDsmta(DicStorageMainTypeAction dsmta) {
		this.dsmta = dsmta;
	}

	public DicStorageMainTypeService getDicStorageMainTypeService() {
		return dicStorageMainTypeService;
	}

	public void setDicStorageMainTypeService(DicStorageMainTypeService dicStorageMainTypeService) {
		this.dicStorageMainTypeService = dicStorageMainTypeService;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
