/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：Action
 * 类描述：通用库存管理
 * 创建人：倪毅
 * 创建时间：2013-01
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.storage.common.action;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.dic.model.DicState;
import com.biolims.storage.common.service.StorageCommonService;
import com.biolims.storage.main.service.StorageService;
import com.biolims.storage.model.DicStorageType;
import com.biolims.storage.model.Storage;
import com.biolims.storage.model.StorageReagentBuySerial;
import com.biolims.storage.position.model.StoragePosition;
import com.biolims.storage.position.service.StoragePositionService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/storage/common")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public class StorageCommonAction extends BaseActionSupport {

	private static final long serialVersionUID = -164661537469123056L;
	@Autowired
	private StorageCommonService storageCommonService;
	@Autowired
	private StorageService storageService;
	@Autowired
	private StoragePositionService storagePositionService;

	@Action(value = "storageTypeSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showStorageTypeList() throws Exception {
		String type = getParameterFromRequest("type");
		String jsonUrl = ServletActionContext.getRequest().getContextPath()
				+ "/storage/common/storageTypeSelectJson.action?type=" + type;
		String showUrl = "/WEB-INF/page/storage/common/storageTypeSelect.jsp";
		return dispatcher(commonShowNotNote(jsonUrl, showUrl));
	}

	@Action(value = "storageTypeSelectJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void storageTypeSelectJson() throws Exception {
		String type = getParameterFromRequest("type");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));

		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");

		Map<String, Object> controlMap = storageCommonService.findStorageType(map, type, startNum, limitNum, dir, sort);

		long totalCount = Long.parseLong(controlMap.get("totalCount").toString());
		List<DicStorageType> list = (List<DicStorageType>) controlMap.get("list");

		new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}
	
	@Action(value = "storageTypeSelectTable", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String storageTypeSelectTable() throws Exception {
		String type = getParameterFromRequest("type");
		putObjToContext("type", type);
		String showUrl = "/WEB-INF/page/storage/common/storageTypeSelectTable.jsp";
		return dispatcher(showUrl);
	}
	
	@Action(value="storageTypeSelectTableJson")
	public void storageTypeSelectTableJson()throws Exception{
		String type=getParameterFromRequest("type");
		String query=getParameterFromRequest("query");
		String colNum=getParameterFromRequest("order[0][column]");
		String col=getParameterFromRequest("columns["+colNum+"][data]");
		String sort=getParameterFromRequest("order[0][dir]");
		Integer start=Integer.valueOf(getParameterFromRequest("start"));
		Integer length=Integer.valueOf(getParameterFromRequest("length"));
		String draw=getParameterFromRequest("draw");
		Map<String,Object> result=storageCommonService.storageTypeSelectTableJson(start,length,query,col,sort,type);
		List<DicStorageType> list = (List<DicStorageType>) result
				.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("state", "");
		String data = new SendData().getDateJsonForDatatable(map, list);
		HttpUtils.write(PushData.pushData(draw, result, data));
	}
	
	

	public String commonShowNotNote(String jsonUrl, String showUrl) throws Exception {

		String type = "{name: 'id'},{name: 'name'},{name: 'note'}";
		String col = "{header:'ID',sortable: true,hidden:true,dataIndex: 'id',width: 100},"
				+ "{header:'名称',sortable: true,dataIndex: 'name',width: 140},{id:'note',header:'说明',sortable: true,dataIndex: 'note',width: 200}";

		putObjToContext("type", type);
		putObjToContext("col", col);
		putObjToContext("path", jsonUrl);
		return (showUrl);

	}

	@Action(value = "showMainStorageAllSelectList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showMainStorageAllSelectList() throws Exception {
 
//		String atype = getParameterFromRequest("type");
//		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
//
//		map.put("id", new String[] { "", "string", "", "编码", "280", "true", "true", "", "", "", "", "" });
//		map.put("name", new String[] { "", "string", "", "组分名称", "280", "true", "", "", "", "", "", "" });
//		map.put("kit-id", new String[] { "", "string", "", "试剂盒编号", "280", "true", "true", "", "", "", "", "" });
//		map.put("kit-name", new String[] { "", "string", "", "试剂盒名称", "280", "true", "", "", "", "", "", "" });
//		map.put("barCode", new String[] { "", "string", "", "Catalog No.", "280", "true", "", "", "", "", "", "" });
//		map.put("breed", new String[] { "", "string", "", "品牌", "200", "true", "true", "", "", "", "", "" });
//		map.put("type-id", new String[] { "", "string", "", "类型ID", "150", "true", "true", "", "", "", "", "" });
//		map.put("type-name", new String[] { "", "string", "", "类型", "150", "true", "true", "", "", "", "", "" });
//		map.put("department-id", new String[] { "", "string", "", "部门ID", "150", "true", "true", "", "", "", "", "" });
//		map.put("department-name", new String[] { "", "string", "", "部门名称", "150", "true", "true", "", "", "", "","" });
//		map.put("spec", new String[] { "", "string", "", "规格参数", "150", "true", "true", "", "", "", "", "" });
//		map.put("num", new String[] { "", "float", "", "库存数量", "150", "true", "", "", "", "", "", "" });
//		map.put("prepareNum", new String[] { "", "float", "", "已预留数量", "150", "true", "true", "", "", "", "", "" });
//		map.put("outPrice", new String[] { "", "float", "", "价格", "150", "true", "true", "", "", "", "", "" });
//		map.put("unit-name", new String[] { "", "string", "", "单位", "150", "false", "", "", "", "", "", "" });
//		map.put("producer-name", new String[] { "", "string", "", "生产商", "150", "true", "true", "", "", "", "", "" });
//		map.put("source-name", new String[] { "", "string", "", "来源", "150", "true", "true", "", "", "", "", "" });
//		map.put("searchCode", new String[] { "", "string", "", "Component Cat No.", "250", "true", "", "", "", "", "", "" });
//		map.put("jdeCode", new String[] { "", "string", "", "JDE编码", "150", "true", "", "", "", "", "", "" });
//		map.put("currencyType-name", new String[] { "", "string", "", "币种", "100", "true", "true", "", "", "", "","" });
//		map.put("position-id", new String[] { "", "string", "", "存储位置ID", "100", "true", "true", "", "", "", "", "" });
//		map.put("position-name", new String[] { "", "string", "", "存储位置", "100", "true", "true", "", "", "", "", "" });
//
//		String type = generalexttype(map);
//		String col = generalextcol(map);
//		super.getRequest().setAttribute("type", type);
//		super.getRequest().setAttribute("col", col);
//		// 用于判断当前页面类型,LIST类型
//		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		super.getRequest().setAttribute(
//				"path",
//				super.getRequest().getContextPath() + "/storage/common/showMainStorageAllSelectListJson.action"
//						+ (atype != null ? "?type=" + atype : ""));
		return dispatcher("/WEB-INF/page/storage/common/showMainStorageAllSelectList.jsp");
	}

	@Action(value = "showMainStorageAllSelectListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showMainStorageAllSelectListJson() throws Exception {
		// 开始记录数
				int startNum = Integer.parseInt(getParameterFromRequest("start"));
				// limit
				int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
				// 字段
				String dir = getParameterFromRequest("dir");
				// 排序方式
				String sort = getParameterFromRequest("sort");
				String data = getParameterFromRequest("data");
				Map<String, Object> result = null;
				Map<String, String> map2Query = new HashMap<String, String>();
				map2Query = JsonUtils.toObjectByJson(data, Map.class);
				result = this.storageService.findStorageList(map2Query, startNum,
						limitNum, dir, sort, null, null);

				List<Storage> list = (List<Storage>) result.get("list");
				Long count = (Long) result.get("total");
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", "");
				map.put("name", "");
				map.put("type-name", "");
				map.put("kit-id", "");
				map.put("kit-name", "");
				map.put("barCode", "");
				map.put("department-id", "");
				map.put("department-name", "");
				map.put("spec", "");
				map.put("num", "");
				map.put("prepareNum", "");
				map.put("type-id", "");
				map.put("outPrice", "#.####");
				map.put("unit-name", "");
				map.put("source-name", "");
				map.put("searchCode", "");
				map.put("currencyType-name", "");
				map.put("position-id", "");
				map.put("position-name", "");
				map.put("jdeCode", "");
				map.put("producer-name", "");
				new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "showMainStorageAllCostCenterSelectList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showMainStorageAllCostCenterSelectList() throws Exception {

		String atype = getParameterFromRequest("type");
		String costCenterId = getParameterFromRequest("costCenterId");

		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();

		map.put("id", new String[] { "", "string", "", "编码", "280", "true", "", "", "", "", "", "" });
		map.put("name", new String[] { "", "string", "", "名称", "280", "true", "", "", "", "", "", "" });
		map.put("breed", new String[] { "", "string", "", "品牌", "200", "true", "", "", "", "", "", "" });
		map.put("type-id", new String[] { "", "string", "", "类型ID", "150", "true", "true", "", "", "", "", "" });
		map.put("type-name", new String[] { "", "string", "", "类型", "150", "true", "", "", "", "", "", "" });
		map.put("department-id", new String[] { "", "string", "", "部门ID", "150", "true", "true", "", "", "", "", "" });
		map
				.put("department-name", new String[] { "", "string", "", "部门名称", "150", "true", "true", "", "", "", "",
						"" });
		map.put("spec", new String[] { "", "string", "", "规格参数", "250", "true", "", "", "", "", "", "" });
		map.put("num", new String[] { "", "float", "", "库存数量", "150", "true", "", "", "", "", "", "" });
		map.put("outPrice", new String[] { "", "float", "", "价格", "150", "true", "true", "", "", "", "", "" });
		map.put("unit-name", new String[] { "", "string", "", "单位", "150", "false", "", "", "", "", "", "" });
		map.put("supplierName", new String[] { "", "string", "", "生产商", "150", "true", "true", "", "", "", "", "" });
		map.put("source-name", new String[] { "", "string", "", "来源", "150", "true", "true", "", "", "", "", "" });
		map.put("searchCode", new String[] { "", "string", "", "检索码", "150", "true", "true", "", "", "", "", "" });
		map
				.put("currencyType-name", new String[] { "", "string", "", "币种", "100", "true", "true", "", "", "", "",
						"" });
		map.put("position-id", new String[] { "", "string", "", "存储位置ID", "100", "true", "true", "", "", "", "", "" });
		map.put("position-name", new String[] { "", "string", "", "存储位置", "100", "true", "true", "", "", "", "", "" });

		String type = generalexttype(map);
		String col = generalextcol(map);
		super.getRequest().setAttribute("type", type);
		super.getRequest().setAttribute("col", col);
		// 用于判断当前页面类型,LIST类型
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		super.getRequest().setAttribute(
				"path",
				super.getRequest().getContextPath() + "/common/showMainStorageAllCostCenterSelectListJson.action"
						+ (atype != null ? "?type=" + atype : "") + "&costCenterId=" + costCenterId);
		return dispatcher("/WEB-INF/page/storage/common/showMainStorageAllSelectList.jsp");
	}

	/**
	 * 库存主数据列表
	 * 
	 * @throws Exception
	 */
	@Action(value = "showMainStorageAllCostCenterSelectListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showMainStorageAllCostCenterSelectListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		String type = getParameterFromRequest("type");
		String costCenterId = getParameterFromRequest("costCenterId");
		Map<String, Object> controlMap = storageCommonService.findCostCenterStorageList(startNum, limitNum, dir, sort,
				getContextPath(), data, type, costCenterId);

		List<Storage> list = (List<Storage>) controlMap.get("list");
		Long count = (Long) controlMap.get("totalCount");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("kit-name", "");
		map.put("type-name", "");
		map.put("department-id", "");
		map.put("department-name", "");
		map.put("spec", "");
		map.put("num", "");
		//map.put("factStorageNum", "");
		map.put("type-id", "");
		map.put("outPrice", "#.####");
		map.put("unit-name", "");
		map.put("supplierName", "");
		map.put("source-name", "");
		map.put("searchCode", "");
		map.put("currencyType-name", "");
		map.put("position-id", "");
		map.put("position-name", "");

		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	/**
	 * 位置
	 */
	@Action(value = "showStoragePositionTree", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showStoragePositionTree() throws Exception {

		putObjToContext("path", ServletActionContext.getRequest().getContextPath()
				+ "/storage/common/showStoragePositionTreeJson.action");

		return dispatcher("/WEB-INF/page/storage/common/showStoragePositionTree.jsp");
	}

	@Action(value = "showStoragePositionTreeJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showStoragePositionTreeJson() throws Exception {

		String upId = getParameterFromRequest("treegrid_id");
		List<StoragePosition> aList = null;
		if (upId.equals("")) {

//			aList = storagePositionService.findStoragePositionList();
		} else {

			aList = storagePositionService.findStoragePositionList(upId);
		}

		String a = storagePositionService.getJson(aList);

		new SendData().sendDataJson(a, ServletActionContext.getResponse());
	}

	/**
	 * 采购试剂页面
	 * 
	 * @return
	 * @throws Exception
	 */

	@Action(value = "showMainStorageItemAllSelectList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showMainStorageItemAllSelectList() throws Exception {
		String storageId = getParameterFromRequest("storageId");
		String costCenterId = getParameterFromRequest("costCenterId");
		String exttype = "";
		String extcol = "";
		String editflag = "false";

		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		map.put("id", new String[] { "", "string", "", "ID", "100", "true", "false", "", "", editflag, "", "" });
		map.put("code", new String[] { "", "string", "", "批号", "100", "true", "false", "", "", editflag, "", "" });
		map.put("num", new String[] { "", "string", "", "本批次存量", "100", "true", "false", "", "", "false", "", "" });
		map.put("purchasePrice", new String[] { "", "string", "", "本批次价格", "80", "true", "false", "", "", "false", "",
				"" });
		map.put("productDate", new String[] { "", "date", "dateFormat:'Y-m-d'", "生产日期", "80", "true", "false", "", "",
				editflag, "formatDate", "" });
		map.put("expireDate", new String[] { "", "date", "dateFormat:'Y-m-d'", "失效期", "80", "true", "false", "", "",
				editflag, "formatDate", "" });
		map.put("remindDate", new String[] { "", "date", "dateFormat:'Y-m-d'", "提醒提日", "80", "true", "true", "", "",
				editflag, "formatDate", "" });
		map.put("inDate", new String[] { "", "date", "dateFormat:'Y-m-d'", "入库日期", "80", "true", "true", "", "",
				"false", "formatDate", "" });

		map.put("unit-name", new String[] { "", "string", "", "单位", "75", "true", "false", "", "", "false", "", "" });
		map.put("storage-id", new String[] { "", "string", "", "库存ID", "75", "true", "true", "", "", "false", "", "" });
		map.put("storage-name",
				new String[] { "", "string", "", "库存名称", "75", "true", "true", "", "", "false", "", "" });
		map.put("storage-source-name", new String[] { "", "string", "", "来源", "75", "true", "true", "", "", "false",
				"", "" });
		// 生成ext用type字符串
		exttype = generalexttype(map);
		// 生成ext用col字符串
		extcol = generalextcol(map);
		putObjToContext("type", exttype);
		putObjToContext("col", extcol);
		putObjToContext("path", ServletActionContext.getRequest().getContextPath()
				+ "/storage/common/showMainStorageItemAllSelectListJson.action?costCenterId=" + costCenterId
				+ "&storageId=" + storageId);
		putObjToContext("storageId", storageId);
		return dispatcher("/WEB-INF/page/storage/common/showMainStorageItemAllSelectList.jsp");
	}

	/**
	 * 试剂明细数据
	 * 
	 * @throws Exception
	 */
	@Action(value = "showMainStorageItemAllSelectListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void storageReagentSerial() throws Exception {
		String storageId = getParameterFromRequest("storageId");
		String costCenterId = getParameterFromRequest("costCenterId");
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");

		Map<String, Object> result = this.storageCommonService.findStorageReagentSerial(storageId, costCenterId, startNum,
				limitNum, dir, sort);

		List<StorageReagentBuySerial> list = (List<StorageReagentBuySerial>) result.get("result");

		Long count = (Long) result.get("count");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("productDate", "yyyy-MM-dd");
		map.put("expireDate", "yyyy-MM-dd");
		map.put("remindDate", "yyyy-MM-dd");
		map.put("inDate", "yyyy-MM-dd");
		map.put("outPrice", "");
		map.put("purchasePrice", "");
		map.put("currencyType-name", "");
		map.put("num", "#.##");
		map.put("unit-name", "");
		map.put("purchasePrice", "");
		map.put("storage-id", "");
		map.put("storage-name", "");
		map.put("storage-searchCode", "");
		map.put("storage-source-name", "");
		map.put("code", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "showMainStorageItemAllSelectListByCheck", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showMainStorageItemAllSelectListByCheck() throws Exception {
		String storageId = getParameterFromRequest("storageId");
		String costCenterId = getParameterFromRequest("costCenterId");
		String exttype = "";
		String extcol = "";
		String editflag = "false";

		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		map.put("id", new String[] { "", "string", "", "ID", "100", "true", "true", "", "", editflag, "", "" });
		map.put("serial", new String[] { "", "string", "", "批号", "100", "true", "false", "", "", editflag, "", "" });
		map.put("num", new String[] { "", "string", "", "本批次存量", "100", "true", "false", "", "", "false", "", "" });
		map.put("purchasePrice", new String[] { "", "string", "", "本批次价格", "80", "true", "false", "", "", "false", "",
				"" });
		map.put("productDate", new String[] { "", "date", "dateFormat:'Y-m-d'", "生产日期", "80", "true", "false", "", "",
				editflag, "formatDate", "" });
		map.put("expireDate", new String[] { "", "date", "dateFormat:'Y-m-d'", "失效期", "80", "true", "false", "", "",
				editflag, "formatDate", "" });
		map.put("remindDate", new String[] { "", "date", "dateFormat:'Y-m-d'", "提醒提日", "80", "true", "true", "", "",
				editflag, "formatDate", "" });
		map.put("inDate", new String[] { "", "date", "dateFormat:'Y-m-d'", "入库日期", "80", "true", "true", "", "",
				"false", "formatDate", "" });

		map.put("unit-name", new String[] { "", "string", "", "单位", "75", "true", "false", "", "", "false", "", "" });
		map.put("storage-id", new String[] { "", "string", "", "库存ID", "75", "true", "true", "", "", "false", "", "" });
		map.put("storage-name", new String[] { "", "string", "", "库存名称", "100", "true", "false", "", "", "false", "",
				"" });
		map.put("qcState",
				new String[] { "", "string", "", "QC状态", "100", "true", "false", "", "", "false", "", "" });
		map.put("storage-spec",
				new String[] { "", "string", "", "规格", "100", "true", "false", "", "", "false", "", "" });
		map.put("storage-producer-name", new String[] { "", "string", "", "生产商名称", "175", "true", "true", "", "", "",
				"", "" });
		map.put("position-id",
				new String[] { "", "string", "", "位置ID", "75", "true", "false", "", "", "false", "", "" });
		map.put("position-name", new String[] { "", "string", "", "位置名称", "75", "true", "false", "", "", "false", "",
				"" });

		// 生成ext用type字符串
		exttype = generalexttype(map);
		// 生成ext用col字符串
		extcol = generalextcol(map);
		putObjToContext("type", exttype);
		putObjToContext("col", extcol);
		putObjToContext("path", ServletActionContext.getRequest().getContextPath()
				+ "/storage/common/showMainStorageItemAllSelectListByCheckJson.action?costCenterId=" + costCenterId
				+ "&storageId=" + storageId);
		putObjToContext("storageId", storageId);
		return dispatcher("/WEB-INF/page/storage/common/showMainStorageItemAllSelectListByCheck.jsp");
	}

	/**
	 * 试剂明细数据
	 * 
	 * @throws Exception
	 */
	@Action(value = "showMainStorageItemAllSelectListByCheckJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showMainStorageItemAllSelectListByCheckJson() throws Exception {
		String storageId = getParameterFromRequest("storageId");
		String costCenterId = getParameterFromRequest("costCenterId");
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");

		if (dir.equals("") && sort.equals("")) {

			dir = "DESC";
			sort = "id";
		}

		Map<String, Object> result = this.storageCommonService.findStorageReagentSerial(storageId, costCenterId, startNum,
				limitNum, dir, sort);

		List<StorageReagentBuySerial> list = (List<StorageReagentBuySerial>) result.get("result");

		Long count = (Long) result.get("count");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("productDate", "yyyy-MM-dd");
		map.put("expireDate", "yyyy-MM-dd");
		map.put("remindDate", "yyyy-MM-dd");
		map.put("inDate", "yyyy-MM-dd");
		map.put("outPrice", "");
		map.put("purchasePrice", "");
		map.put("currencyType-name", "");
		map.put("num", "#.##");
		map.put("unit-name", "");
		map.put("qcState", "");
		map.put("purchasePrice", "");
		map.put("storage-id", "");
		map.put("storage-name", "");
		map.put("storage-searchCode", "");
		map.put("storage-spec", "");

		map.put("storage-source-name", "");
		map.put("serial", "");
		map.put("position-id", "");
		map.put("position-name", "");

		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "storageSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String selectMainStorage() throws Exception {

		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();

		map.put("id", new String[] { "", "string", "", "耗材编码", "150", "true", "", "", "", "", "", "" });
		map.put("name", new String[] { "", "string", "", "耗材名称", "150", "true", "", "", "", "", "", "" });
		map.put("engName", new String[] { "", "string", "", "英文名称", "75", "true", "", "", "", "", "", "" });
		map.put("typeStr", new String[] { "", "string", "", "类型", "150", "false", "", "", "", "", "", "" });
		map.put("studyTypeStr", new String[] { "", "string", "", "研究类型", "150", "false", "", "", "", "", "", "" });
		map.put("num", new String[] { "", "string", "", "库存数量", "150", "true", "", "", "", "", "", "" });
		map.put("price", new String[] { "", "string", "", "价格", "150", "true", "true", "", "", "", "", "" });
		map.put("unitStr", new String[] { "", "string", "", "单位", "150", "false", "", "", "", "", "", "" });
		map.put("source-name", new String[] { "", "string", "", "来源", "150", "false", "", "", "", "", "", "" });
		map.put("searchCode", new String[] { "", "string", "", "检索码", "150", "true", "true", "", "", "", "", "" });
		map.put("department-id", new String[] { "", "string", "", "部门ID", "150", "false", "", "", "", "", "", "" });
		map.put("department-name", new String[] { "", "string", "", "部门名称", "150", "false", "", "", "", "", "", "" });
		map.put("spec", new String[] { "", "string", "", "规格参数", "250", "false", "", "", "", "", "", "" });
		map.put("breed", new String[] { "", "string", "", "品牌", "150", "false", "", "", "", "", "", "" });
		map.put("producer-name", new String[] { "", "string", "", "生产商", "150", "false", "", "", "", "", "", "" });
		map.put("productAddress", new String[] { "", "string", "", "产地", "150", "false", "", "", "", "", "", "" });

		String type = generalexttype(map);
		String col = generalextcol(map);
		super.getRequest().setAttribute("type", type);
		super.getRequest().setAttribute("col", col);
		// 用于判断当前页面类型,LIST类型
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		super.getRequest().setAttribute("path",
				super.getRequest().getContextPath() + "/storage/common/storageSelectJson.action");
		return dispatcher("/WEB-INF/page/storage/common/storageSelect.jsp");
	}

	@Action(value = "storageSelectJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void storageSelectJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");

		//Storage _storage = (Storage) super.getSession().getAttribute("storage");

		String id = getParameterFromRequest("id");

		Storage aStorage = new Storage();
		if (!id.equals(""))
			aStorage.setId(id);

		Map<String, Object> controlMap = storageCommonService.findObjectList(startNum, limitNum, dir, sort, getContextPath(),
				aStorage);

		List<Storage> list = (List<Storage>) controlMap.get("list");
		Long count = (Long) controlMap.get("totalCount");
		Map<String, String> map = new HashMap<String, String>();
		map.put("name", "name");
		map.put("id", "id");
		map.put("engName", "");
		map.put("typeStr", "typeStr");
		map.put("studyTypeStr", "");
		map.put("price", "");
		map.put("num", "");
		map.put("source-name", "");
		map.put("department-id", "");
		map.put("department-name", "");
		map.put("searchCode", "");
		map.put("spec", "");
		map.put("unitStr", "");
		map.put("breed", "");
		map.put("producer-name", "");
		map.put("productAddress", "");

		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	/**
	 * 试剂明细数据
	 * 
	 * @throws Exception
	 */
	@Action(value = "showMainStorageItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showMainStorageItemListJson() throws Exception {
		String storageId = getParameterFromRequest("storageId");

		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");

		Map<String, Object> result = this.storageCommonService.findStorageReagentSerial(storageId, startNum, limitNum, dir,
				sort);

		List<StorageReagentBuySerial> list = (List<StorageReagentBuySerial>) result.get("result");

		Long count = (Long) result.get("count");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("productDate", "yyyy-MM-dd");
		map.put("expireDate", "yyyy-MM-dd");
		map.put("remindDate", "yyyy-MM-dd");
		map.put("inDate", "yyyy-MM-dd");
		map.put("outPrice", "");
		map.put("purchasePrice", "");
		map.put("currencyType-name", "");
		map.put("num", "");
		map.put("costCenter-id", "");
		map.put("costCenter-name", "");
		map.put("unit-name", "");
		map.put("storage-id", "");
		map.put("storage-name", "");
		map.put("storage-searchCode", "");
		map.put("storage-producer-id", "");
		map.put("storage-source-name", "");
		map.put("storage-supplierName", "");
		map.put("storage-searchCode", "");
		map.put("supplier-name", "");
		map.put("supplier-linkTel", "");
		map.put("supplier-fax", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "ifInStorageCheckProcess", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void ifInStorageCheckProcess() throws Exception {

		Map<String, Boolean> result = new HashMap<String, Boolean>();
		try {
			result.put("success", storageCommonService.ifInStorageCheckProcess());
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}

	/**
	 * 获取当前库存主数据是否盘点中...
	 * @throws Exception
	 */
	@Action(value = "showStroageId", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showStroageId() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String data = getRequest().getParameter("data");
		String stroageIdItem = this.storageCommonService.findStorageId(data);
		map.put("message", stroageIdItem);
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
}
