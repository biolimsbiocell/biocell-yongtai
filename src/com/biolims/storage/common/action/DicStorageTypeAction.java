package com.biolims.storage.common.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.storage.common.service.DicStorageTypeService;
import com.biolims.storage.model.DicStorageType;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

;

@Namespace("/dicStorageType")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public class DicStorageTypeAction extends BaseActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5978717475280115710L;

	private String rightsId = "1122";

	private DicStorageTypeAction dsta;

	@Resource
	private DicStorageTypeService dicStorageTypeService;

	/**
	 * 字典类别列表
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showDicStorageTypeList")
	public String showDicStorageTypeList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/dic/showDicStorageTypeList.jsp");
	}

	/**
	 * 字典类别列表
	 * @return
	 * @throws Exception 
	 */
	@Action(value = "showDicStorageTypeListJson")
	public void showDicStorageTypeListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");

		String queryData = getRequest().getParameter("data");

		Map<String, String> mapForQuery = null;
		if (queryData != null && queryData.length() > 0) {
			mapForQuery = JsonUtils.toObjectByJson(queryData, Map.class);
		}
		try {
			Map<String, Object> result = this.dicStorageTypeService.findDicStorageTypeList(mapForQuery, startNum,
					limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<DicStorageType> list = (List<DicStorageType>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "id");
			map.put("name", "name");
			map.put("state", "state");
			map.put("type-id", "type-id");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 保存字典主类别
	 * @throws Exception
	 */
	@Action(value = "saveDicStorageTypeList")
	public void saveDicStorageTypeList() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String itemDataJson = getRequest().getParameter("itemDataJson");
			dicStorageTypeService.saveDicStorageTypeList(itemDataJson);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 *删除表单类型
	 * @throws Exception
	 */
	@Action(value = "delDicMainType")
	public void delDicMainType() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			dicStorageTypeService.delDicStorageType(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public DicStorageTypeAction getDsta() {
		return dsta;
	}

	public void setDsta(DicStorageTypeAction dsta) {
		this.dsta = dsta;
	}

	public DicStorageTypeService getDicStorageTypeService() {
		return dicStorageTypeService;
	}

	public void setDicStorageTypeService(DicStorageTypeService dicStorageTypeService) {
		this.dicStorageTypeService = dicStorageTypeService;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
