package com.biolims.storage.common.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.common.dao.CommonDAO;

import com.biolims.core.model.user.DicCostCenter; //import com.biolims.equipment.model.Instrument;
import com.biolims.dic.model.DicState;

import com.biolims.storage.common.constants.SystemConstants;
import com.biolims.storage.model.Storage;
import com.biolims.storage.model.StorageCheckItem;
import com.biolims.storage.model.StorageReagentBuySerial;

@Repository
@SuppressWarnings("unchecked")
public class StorageCommonDao extends CommonDAO {

	public List<Storage> getNeedNumStorageList(String dir, String sort) {
		String hql = "from Storage where state.id like '1%' and (num - safeNum)<=0";
		if (dir != null && !dir.equals("") && sort != null && !sort.equals("")) {
			hql += " order by " + sort + " " + dir;
		}
		return find(hql);
	}

	public List<Storage> getSafeNumStorageList() {
		String hql = "from Storage where state.id like '1%' and ((num+onWayNum)<safeNum) and ifSecondBuy ='1'";
		return find(hql);
	}

	public List<StorageReagentBuySerial> getNeedDateStorageReagentBuySerialList(String dir, String sort) {
		String hql = "from StorageReagentBuySerial where storage.state.id like '1%' and storage.ifCall='1' and remindDate<=?";
		if (dir != null && !dir.equals("") && sort != null && !sort.equals("")) {
			hql += " order by " + sort + " " + dir;
		}
		try {
			return find(hql, new Date());
		} catch (Exception e) {

			throw new RuntimeException(e);
		}

	}

	/**
	 * 查库存明细数量
	 * @param storageId
	 * @param costCenterId
	 * @return
	 * @throws Exception
	 */
	public Double selectStorageReagentBuyCount(String storageId, String costCenterId) throws Exception {

		DicCostCenter dcc = this.get(DicCostCenter.class, costCenterId);

		String hql = "select sum(num) from StorageReagentBuySerial where storage.id='" + storageId
				+ "' and costCenter.levelId like '" + dcc.getLevelId() + "%'";
		Double srb = (Double) this.getSession().createQuery(hql).uniqueResult();
		return srb;
	}

	public Map<String, Object> selectStorageReagentSerialByCostCenter(String storageId, String costCenterId,
			Integer startNum, Integer limitNum, String dir, String sort) throws Exception {
		Long count = 0L;
		List<StorageReagentBuySerial> list = new ArrayList<StorageReagentBuySerial>();

		DicCostCenter dcc = this.get(DicCostCenter.class, costCenterId);

		String hql = "";
		//String costCenterHql = "and costCenter is null";
		String costCenterHql = " ";
		if (dcc != null && dcc.getLevelId() != null) {
			costCenterHql = " and costCenter.levelId like '" + dcc.getLevelId() + "%'";

		}
		hql = "from StorageReagentBuySerial where storage.id='" + storageId + "' " + costCenterHql;

		count = (Long) this.getSession().createQuery("select count(id) " + hql).uniqueResult();

		if (count != null && !new Long(0).equals(count)) {
			if (sort != null && sort.trim().length() > 0 && dir != null && dir.trim().length() > 0)
				hql = hql + " order by " + sort + " " + dir;
			list = this.getSession().createQuery(hql).setFirstResult(startNum).setMaxResults(limitNum).list();
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("count", count);
		map.put("result", list);
		return map;
	}

	public Map<String, Object> selectStorageReagentSerial(String storageId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from StorageReagentBuySerial where storage.id='" + storageId + "'";

		Long count = (Long) this.getSession().createQuery("select count(id) " + hql).uniqueResult();
		List<StorageReagentBuySerial> list = new ArrayList<StorageReagentBuySerial>();
		if (count != null && !new Long(0).equals(count)) {
			if (sort != null && sort.trim().length() > 0 && dir != null && dir.trim().length() > 0)
				hql = hql + " order by " + sort + " " + dir;
			list = this.getSession().createQuery(hql).setFirstResult(startNum).setMaxResults(limitNum).list();
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("count", count);
		map.put("result", list);
		return map;
	}

	public List<StorageReagentBuySerial> selectStorageReagentSerialByStorageId(String storageId) throws Exception {
		String hql = "from StorageReagentBuySerial where storage.id='" + storageId + "' order by id DESC";

		List<StorageReagentBuySerial> list = new ArrayList<StorageReagentBuySerial>();
		list = this.getSession().createQuery(hql).list();
		return list;
	}

	public long findInStorageCheckProcess() throws Exception {
		long s = 0;
		Object[] a = { SystemConstants.DIC_STATE_WORKFLOW_IN_PROCESS };
		List<Object> list = this.find("select count(id) from StorageCheck where  state =?", a);
		if (list.iterator().hasNext()) {
			if (list.iterator().next() != null) {
				s = (Long) list.iterator().next();
			}
		}
		return s;

	}

	public List<Storage> findStorage() {
		return this.find("from Storage where state like '" + SystemConstants.DIC_STATE_YES_ID + "%'");
	}

	public List<Storage> getNeedNumStorageList() {
		String hql = "from Storage where state.id like '1%' and ifSecondBuy = '1' and safeNum is not null and (num - safeNum)<=0";

		return find(hql);
	}

	public List<StorageReagentBuySerial> getNeedDateStorageReagentBuySerialList() {
		String hql = "from StorageReagentBuySerial where storage.state.id like '1%' and storage.ifCall='1' and num>0 and remindDate<=? order by id";

		try {
			return find(hql, new Date());
		} catch (Exception e) {

			throw new RuntimeException(e);
		}
	}

	/**
	 * 查询盘点明细
	 * @param mapForQuery
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectStorageCheckItem(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String key = "";
		String hql = "from StorageCheckItem where 1=1 ";
		if (mapForQuery != null) {
			key = map2where(mapForQuery);
		}

		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<StorageCheckItem> list = new ArrayList<StorageCheckItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	/*
	 * 根据类型查询
	 */
	public List<Storage> selectStorageByDicType(String dicType) throws Exception {
		String hql = "from Storage where 1=1";

		String key = "";
		if (dicType != null) {
			key = key + " and studyType.sysCode = '" + dicType + "' and state like '1%'";
		}
		List<Storage> list = new ArrayList<Storage>();
		list = this.getSession().createQuery(hql + key).list();
		return list;
	}

	public Map<String, Object> selectStorageByDicType(Integer start,
			Integer length, String query, String col, String sort, String type) {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from DicStorageType where 1=1 and type='"
				+ type + "'";
		String key = "";
		// if(queryMap!=null){
		// key=map2where(queryMap);
		// }
		Long sumCount = (Long) getSession().createQuery(countHql)
				.uniqueResult();
		if (0l != sumCount) {
			Long filterCount = (Long) getSession().createQuery(countHql + key)
					.uniqueResult();
			String hql = "from DicStorageType where 1=1 and type='" + type + "' ";
			if (col != null && !"".equals(col) && !"".equals(sort)
					&& sort != null) {
				key += " order by " + col + " " + sort;
			}
			List<DicState> list = new ArrayList<DicState>();
			list = this.getSession().createQuery(hql + key)
					.setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
		}
		return map;
	}

}
