package com.biolims.storage.common.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.storage.model.DicStorageMainType;

@Repository
public class DicStorageMainTypeDao extends BaseHibernateDao {
	public Map<String, Object> selectDicStorageMainTypeList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		String key = "";
		String hql = "from DicStorageMainType where state='1'";
		if (mapForQuery != null) {
			key = map2where(mapForQuery);
		}
		Long total = (Long) queryUniqueResult("select count(*) " + hql + key);
		List<DicStorageMainType> list = new ArrayList<DicStorageMainType>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			list = queryList(hql + key, startNum, limitNum);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
}
