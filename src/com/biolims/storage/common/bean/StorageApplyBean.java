package com.biolims.storage.common.bean;

/**
 * 领用申请和领用申请对象明细BEAN
 * 
 */
public class StorageApplyBean {

	private String applyId;// 领用单
	private String applyUser;// 领用人
	private String storageName;// 对象名
	private String storageId;// 对象编号
	private String searchCode;// 检索码
	private Integer num;// 需求数量
	private String unit;// 单位
	private String outNum;// 出库数量
	private Double outPrice;// 出库价格
	private String currencyType;// 币种
	private String position;// 存储位置

	public StorageApplyBean() {
	}

	public StorageApplyBean(String storageName, String storageId, String searchCode, String unit, Double outPrice,
			String currencyType, String position) {
		this.storageName = storageName;
		this.storageId = storageId;
		this.searchCode = searchCode;
		this.unit = unit;
		this.outPrice = outPrice;
		this.currencyType = currencyType;
		this.position = position;
	}

	public String getApplyId() {
		return applyId;
	}

	public void setApplyId(String applyId) {
		this.applyId = applyId;
	}

	public String getApplyUser() {
		return applyUser;
	}

	public void setApplyUser(String applyUser) {
		this.applyUser = applyUser;
	}

	public String getStorageName() {
		return storageName;
	}

	public void setStorageName(String storageName) {
		this.storageName = storageName;
	}

	public String getStorageId() {
		return storageId;
	}

	public void setStorageId(String storageId) {
		this.storageId = storageId;
	}

	public String getSearchCode() {
		return searchCode;
	}

	public void setSearchCode(String searchCode) {
		this.searchCode = searchCode;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getOutNum() {
		return outNum;
	}

	public void setOutNum(String outNum) {
		this.outNum = outNum;
	}

	public Double getOutPrice() {
		return outPrice;
	}

	public void setOutPrice(Double outPrice) {
		this.outPrice = outPrice;
	}

	public String getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(String currencyType) {
		this.currencyType = currencyType;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}
}
