/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：
 * 类描述：库存通用service
 * 创建人：倪毅
 * 创建时间：2013-01
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.storage.common.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.service.CommonService;
import com.biolims.remind.model.SysRemind;
import com.biolims.remind.service.RemindService;
import com.biolims.storage.common.dao.StorageCommonDao;
import com.biolims.storage.model.DicStorageType;
import com.biolims.storage.model.Storage;
import com.biolims.storage.model.StorageCheckItem;
import com.biolims.storage.model.StorageReagentBuySerial;
import com.biolims.storage.position.dao.StoragePositionDao;
import com.biolims.storage.position.model.StoragePosition;
import com.biolims.util.BeanUtils;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.biolims.util.StringUtils;

@Service
@SuppressWarnings("unchecked")
public class StorageCommonService extends CommonService {
	StringBuffer json = new StringBuffer();
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private StorageCommonDao storageCommonDAO;
	@Resource
	private RemindService remindService;
	@Resource
	private StoragePositionDao storagePositionDao;

	public Map<String, Object> findStorageList(Map<String, String> map, String type, int start, int limit, String dir,
			String sort) throws Exception {

		Map<String, Object> controlMap = findCommonObject(DicStorageType.class.getName(), " type = '" + type + "'",
				start, limit, dir, sort, "");
		return controlMap;

	}

	public Map<String, Object> findStorageList(int startNum, int limitNum, String dir, String sort, String contextPath,
			String data, String type) throws Exception {
		//检索条件map,每个字段检索值
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		Map<String, String> mapForCondition = new HashMap<String, String>();
		if (data != null && !data.equals("")) {

			BeanUtils.data2Map(data, mapForQuery, mapForCondition);
			mapForQuery.remove("queryStartDate");
			mapForQuery.remove("queryEndDate");
			mapForCondition.remove("queryStartDate");
			mapForCondition.remove("queryEndDate");
		}
		//检索方式map,每个字段的检索方式
		mapForCondition.put("mainType.id", "=");
		mapForCondition.put("state.id", "plike");
		//检索有效的
		//	if (type != null && !type.equals("")) {
		//	mapForQuery.put("mainType.id", type);
		//}
		mapForQuery.put("state.id", SystemConstants.DIC_STATE_YES);
		//将map值传入，获取列表
		Map<String, Object> controlMap = findObjectCondition(startNum, limitNum, dir, sort, Storage.class, mapForQuery,
				mapForCondition);
		List<Storage> list = (List<Storage>) controlMap.get("list");

		controlMap.put("list", list);
		return controlMap;
	}

	public Map<String, Object> findCostCenterStorageList(int startNum, int limitNum, String dir, String sort,
			String contextPath, String data, String type, String costCenterId) throws Exception {
		//检索条件map,每个字段检索值
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		Map<String, String> mapForCondition = new HashMap<String, String>();

		if (data != null && !data.equals("")) {

			BeanUtils.data2Map(data, mapForQuery, mapForCondition);
		}
		//检索方式map,每个字段的检索方式

		mapForCondition.put("mainType.id", "=");
		mapForCondition.put("state.id", "plike");

		if (type != null && !type.equals("")) {
			mapForQuery.put("mainType.id", type);
		}
		mapForQuery.put("state.id", SystemConstants.DIC_STATE_YES);
		//将map值传入，获取列表
		Map<String, Object> controlMap = commonDAO.findObjectCondition(startNum, limitNum, dir, sort, Storage.class,
				mapForQuery, mapForCondition);
		List<Storage> list = (List<Storage>) controlMap.get("list");
		for (Storage s : list) {

			Double n = 0.0;

			n = storageCommonDAO.selectStorageReagentBuyCount(s.getId(), costCenterId);

			s.setNum(n);

		}
		controlMap.put("list", list);
		return controlMap;
	}

	/**
	 * 
	 *  查询任务明细列表
	 * 
	 * 
	 */
	public List<StoragePosition> findStoragePositionList() throws Exception {

		List<StoragePosition> list = commonDAO.find("from StoragePosition order by id asc");
		return list;
	}

	/**
	 * 构建Json文件
	 * @param list
	 * @param node
	 */

	public void constructorJson(List<StoragePosition> list, StoragePosition treeNode) throws Exception {
		if (hasChild(list, treeNode)) {
			json.append("{\"id\":'");
			json.append(treeNode.getId() + "'");
			json.append(",");
			json.append("\"scName\":'");
			json.append(treeNode.getStorageContainer() != null ? treeNode.getStorageContainer().getName() : "");
			json.append("',");
			json.append("\"scId\":'");
			json.append(treeNode.getStorageContainer() != null ? treeNode.getStorageContainer().getId() : "");
			json.append("',");
			json.append("\"name\":'");
			json.append(JsonUtils.formatStr(treeNode.getName() == null ? "" : treeNode.getName()) + "");
			json.append("',\"typeSysCode\":'");
			if (treeNode.getType() != null)
				if (treeNode.getType().getSysCode() != null)
					json.append(JsonUtils.formatStr(treeNode.getType().getSysCode()) + "");
			json.append("',\"type-name\":'");
			json.append(JsonUtils.formatStr(treeNode.getType() == null ? "" : treeNode.getType().getName()) + "");
			json.append("',\"state-name\":'");
			json.append((treeNode.getState() == null ? "" : treeNode.getState().getName()) + "");
			json.append("',\"stateId\":'");
			json.append((treeNode.getState() == null ? "" : treeNode.getState().getId()) + "");
			json.append("',\"leaf\":false");
			json.append(",\"upId\":'");
			json.append((treeNode.getUpStoragePosition() == null ? "" : treeNode.getUpStoragePosition().getId()) + "");
			json.append("',\"isUseStr\":'");
			json.append("'");
			json.append(",\"storageObj\":'");
			json.append("'");
			json.append(",\"children\":[");
			List<StoragePosition> childList = getChildList(list, treeNode);
			Iterator<StoragePosition> iterator = childList.iterator();
			while (iterator.hasNext()) {
				StoragePosition node = (StoragePosition) iterator.next();
				constructorJson(list, node);
			}
			json.append("]");
			json.append("},");
		} else {
			json.append("{\"id\":'");
			json.append(treeNode.getId() + "'");
			json.append(",");

			try {
				json.append("\"scName\":'");
				json.append(treeNode.getStorageContainer() != null ? treeNode.getStorageContainer().getName() : "");
				json.append("',");
				json.append("\"scId\":'");
				json.append(treeNode.getStorageContainer() != null ? treeNode.getStorageContainer().getId() : "");
				json.append("',");

			} catch (Exception e) {
				e.printStackTrace();
			}

			json.append("\"name\":'");
			json.append(JsonUtils.formatStr(treeNode.getName() == null ? "" : treeNode.getName()) + "");
			json.append("',\"typeSysCode\":'");
			if (treeNode.getType() != null)
				if (treeNode.getType().getSysCode() != null)
					json.append(JsonUtils.formatStr(treeNode.getType().getSysCode()) + "");
			json.append("',\"type-name\":'");
			json.append(JsonUtils.formatStr(treeNode.getType() == null ? "" : treeNode.getType().getName()) + "");
			json.append("',\"stateId\":'");
			json.append((treeNode.getState() == null ? "" : treeNode.getState().getId()) + "");
			json.append("',\"state-name\":'");
			json.append((treeNode.getState() == null ? "" : treeNode.getState().getName()) + "");
			json.append("',\"leaf\":true");
			json.append(",\"upId\":'");
			json.append((treeNode.getUpStoragePosition() == null ? "" : treeNode.getUpStoragePosition().getId()) + "");
			json.append("',\"isUseStr\":'");
			json.append("'");
			json.append(",\"storageObj\":'");
			json.append(storagePositionDao.selectSampleInfoLikePositionId(treeNode.getId()));
			json.append("'");
			json.append("},");

		}

	}

	/**
	* 获得子节点列表信息
	* @param list
	* @param node
	* @return
	*/
	public List<StoragePosition> getChildList(List<StoragePosition> list, StoragePosition node) throws Exception { //得到子节点列表   
		List<StoragePosition> li = new ArrayList<StoragePosition>();
		Iterator<StoragePosition> it = list.iterator();
		while (it.hasNext()) {
			StoragePosition n = (StoragePosition) it.next();
			if (n.getUpStoragePosition() != null && n.getUpStoragePosition().getId().equals(node.getId())) {
				li.add(n);
			}
		}
		return li;
	}

	/**
	 * 判断是否有子节点
	 * @param list
	 * @param node
	 * @return
	 */
	public boolean hasChild(List<StoragePosition> list, StoragePosition node) throws Exception {
		return getChildList(list, node).size() > 0 ? true : false;
	}

	public String getJson(List<StoragePosition> list) throws Exception {
		json = new StringBuffer();
		List<StoragePosition> nodeList0 = new ArrayList<StoragePosition>();
		Iterator<StoragePosition> it1 = list.iterator();
		while (it1.hasNext()) {
			StoragePosition node = (StoragePosition) it1.next();
			if (node.getLevel() == 0) {
				nodeList0.add(node);
			}
		}
		Iterator<StoragePosition> it = nodeList0.iterator();
		while (it.hasNext()) {
			StoragePosition node = (StoragePosition) it.next();
			constructorJson(list, node);
		}
		String jsonDate = json.toString();
		return ("[" + jsonDate + "]").replaceAll(",]", "]");
	}

	/**
	 * 按照库存主数据Id查询器具明细
	 * 
	 * @param storageId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findStorageReagentSerial(String storageId, String costCenterId, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {

		Map<String, Object> map = this.storageCommonDAO.selectStorageReagentSerial(storageId, startNum, limitNum, dir,
				sort);

		return map;
	}

	public Map<String, Object> findStorageReagentSerial(String storageId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {

		Map<String, Object> map = this.storageCommonDAO.selectStorageReagentSerial(storageId, startNum, limitNum, dir,
				sort);
		List<StorageReagentBuySerial> list = (List<StorageReagentBuySerial>) map.get("result");
		List<StorageReagentBuySerial> alist = new ArrayList<StorageReagentBuySerial>();
		for (StorageReagentBuySerial srbs : list) {
			if (srbs.getNum() > 0) {

				alist.add(srbs);
			}

		}
		for (StorageReagentBuySerial srbs : list) {
			if (srbs.getNum() == 0) {

				alist.add(srbs);
			}

		}
		map.put("result", alist);

		return map;
	}

	public boolean ifInStorageCheckProcess() throws Exception {

		if (storageCommonDAO.findInStorageCheckProcess() > 0)
			return true;

		return false;

	}

	public <T> String hasId(String id, String obj) throws Exception {
		Map<String, String> messageMap = new HashMap<String, String>();
		Object[] a = { id };

		if (!id.trim().equals(id)) {
			messageMap.put("message", "编码前后不能有空格！");
		} else {

			if (StringUtils.isOnlyDitalLetter(id)) {
				long count = commonDAO.getCount("from " + obj + " where id =?", a);
				if (count > 0)
					messageMap.put("message", "编码已经存在，请重新设置编码！");
				else
					messageMap.put("message", "");
			} else
				messageMap.put("message", "编码只能是字母，#-字符或者数字！");
		}

		return JsonUtils.toJsonString(messageMap);
	}

	public <T> Map<String, Object> findStorageType(Map<String, String> map, String type, int start, int limit,
			String dir, String sort) throws Exception {

		Map<String, Object> controlMap = findCommonDic(DicStorageType.class.getName(), " type = '" + type + "'", start,
				limit, dir, sort, "");
		return controlMap;

	}

	public <T> Map<String, Object> findCommonDic(String className, String condition, int startNum, int limitNum,
			String dir, String sort, String contextPath) throws Exception {
		String where = "where 1=1 ";
		if (condition != null && !condition.equals("")) {
			where = where + " and " + condition;
		}
		String hql = "from DicStorageType ";
		Map<String, Object> controlMap = commonDAO.findObject(hql + where, startNum, limitNum, dir, sort);
		List<T> list = (List<T>) controlMap.get("list");
		controlMap.put("list", list);
		return controlMap;
	}

	@Transactional(rollbackFor = Exception.class)
	public void makeRemind() throws Exception {

		List<StorageReagentBuySerial> listRemind = storageCommonDAO.getNeedDateStorageReagentBuySerialList();

		for (int i = 0; i < listRemind.size(); i++) {

			StorageReagentBuySerial s = listRemind.get(i);
			if (remindService.selectSysRemindByTableIdCount("StorageReagentBuySerial", s.getId()) == 0) {
				SysRemind sr = new SysRemind();
				sr.setHandleUser(s.getStorage().getDutyUser());
				sr.setTitle(new StringBuffer(s.getId()).append("批次").append(s.getStorage().getId()).append(
						s.getStorage().getName()).append("已经接近失效期,请注意,失效期为").append(
						DateUtil.dateFormatter(s.getExpireDate())).toString());
				sr.setState("0");
				sr.setTableId("StorageReagentBuySerial");
				sr.setContentId(s.getId());
				sr.setType("1");
				sr.setRemindUser("SYSTEM");
				sr.setStartDate(new Date());
				sr.setContent("/storage/toEditStorage.action?id=" + s.getStorage().getId());

				storageCommonDAO.saveOrUpdate(sr);
			}
		}
		List<Storage> listRemind1 = storageCommonDAO.getNeedNumStorageList();

		for (int i = 0; i < listRemind1.size(); i++) {

			Storage s = listRemind1.get(i);
			if (remindService.selectSysRemindByTableIdCount("Storage", s.getId()) == 0) {
				SysRemind sr = new SysRemind();
				sr.setHandleUser(s.getDutyUser());
				sr.setTitle(new StringBuffer(s.getId()).append(s.getName()).append("已经接近库存量报警").toString());
				sr.setState("0");
				sr.setTableId("Storage");
				sr.setContentId(s.getId());
				sr.setType("1");
				sr.setRemindUser("SYSTEM");
				sr.setStartDate(new Date());
				sr.setContent("/storage/toEditStorage.action?id=" + s.getId());

				storageCommonDAO.saveOrUpdate(sr);
			}
		}

	}

	public String findStorageId(String json) throws Exception {
		String stroageIdItem = "";
		Map<String, String> mapForQuery = new HashMap<String, String>();
		mapForQuery.put("storageCheck.state", SystemConstants.DIC_STATE_WORKFLOW_IN_PROCESS);
		mapForQuery.put("storage.id", "in##@@##(" + json + ")");
		Map<String, Object> map = storageCommonDAO.selectStorageCheckItem(mapForQuery, null, null, null, null);
		List<StorageCheckItem> list = (List<StorageCheckItem>) map.get("list");
		for (int i = 0; i < list.size(); i++) {
			StorageCheckItem sri = list.get(i);
			if (i > 1) {
				stroageIdItem = stroageIdItem + "," + sri.getStorage().getId();
			} else {
				stroageIdItem = sri.getStorage().getId();
			}

		}
		return stroageIdItem;
	}

	public List<Storage> findStorageByDicType(String dicType) throws Exception {
		List<Storage> list = new ArrayList<Storage>();
		list = storageCommonDAO.selectStorageByDicType(dicType);
		return list;
	}

	public Map<String, Object> storageTypeSelectTableJson(Integer start,
			Integer length, String query, String col, String sort, String type) {
		return storageCommonDAO.selectStorageByDicType(start,length,query,col,sort,type);
	}
}
