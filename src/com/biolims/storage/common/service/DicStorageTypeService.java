package com.biolims.storage.common.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.storage.common.dao.DicStorageTypeDao;
import com.biolims.storage.model.DicStorageType;
import com.biolims.util.JsonUtils;

@Service
@Transactional
public class DicStorageTypeService {
	@Resource
	private DicStorageTypeDao dicStorageTypeDao;

	/**
	 * 物资类别的列表
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findDicStorageTypeList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		return dicStorageTypeDao.selectDicStorageTypeList(mapForQuery, startNum, limitNum, dir, sort);
	}

	/**
	 * 物资类别的保存和修改
	 * @param sbr
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveDicStorageTypeList(String itemDataJson) throws Exception {
		List<DicStorageType> saveItems = new ArrayList<DicStorageType>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			DicStorageType dmt = new DicStorageType();
			// 将map信息读入实体类
			dmt = (DicStorageType) dicStorageTypeDao.Map2Bean(map, dmt);
			saveItems.add(dmt);
		}
		dicStorageTypeDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 物资类别的删除
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDicStorageType(String[] ids) throws Exception {
		for (String id : ids) {
			DicStorageType at = dicStorageTypeDao.get(DicStorageType.class, id);
			at.setState("0");
			dicStorageTypeDao.saveOrUpdate(at);
		}
	}
}
