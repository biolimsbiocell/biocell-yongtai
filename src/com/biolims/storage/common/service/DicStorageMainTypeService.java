package com.biolims.storage.common.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.storage.common.dao.DicStorageMainTypeDao;
import com.biolims.storage.model.DicStorageMainType;
import com.biolims.util.JsonUtils;

@Service
public class DicStorageMainTypeService {
	@Resource
	private DicStorageMainTypeDao dicStorageMainTypeDao;

	/**
	 * 物资类别的列表
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findDicStorageMainTypeList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) throws Exception {
		return dicStorageMainTypeDao.selectDicStorageMainTypeList(mapForQuery, startNum, limitNum, dir, sort);
	}

	/**
	 * 物资主类别的保存和修改
	 * @param sbr
	 * @param itemDataJson
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveDicStorageMainTypeList(String itemDataJson) throws Exception {
		List<DicStorageMainType> saveItems = new ArrayList<DicStorageMainType>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			DicStorageMainType dmt = new DicStorageMainType();
			// 将map信息读入实体类
			dmt = (DicStorageMainType) dicStorageMainTypeDao.Map2Bean(map, dmt);
			saveItems.add(dmt);
		}
		dicStorageMainTypeDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 物资主类别的删除
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDicStorageMainType(String[] ids) throws Exception {
		for (String id : ids) {
			DicStorageMainType at = dicStorageMainTypeDao.get(DicStorageMainType.class, id);
			at.setState("0");
			dicStorageMainTypeDao.saveOrUpdate(at);
		}
	}

	public DicStorageMainTypeDao getDicStorageMainTypeDao() {
		return dicStorageMainTypeDao;
	}

	public void setDicStorageMainTypeDao(DicStorageMainTypeDao dicStorageMainTypeDao) {
		this.dicStorageMainTypeDao = dicStorageMainTypeDao;
	}

}
