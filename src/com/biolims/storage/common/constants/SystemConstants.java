package com.biolims.storage.common.constants;

public class SystemConstants extends com.biolims.common.constants.SystemConstants {

	//耗材
	public static final String DIC_STORAGE_HAOCAI = "1";

	/**
	 * 库存位置是否占用
	 * 1:否
	 */
	public static final String POSITION_USE_NO = "0";

	/**
	 * 1器具（无批次）
	 */
	public static final String DIC_STORAGE_TYPE_HAOCAI_QIJU = "11";

	/**
	 * 2试剂（有批次）
	 */
	public static final String DIC_STORAGE_TYPE_HAOCAI_CGSJ = "12";

	/**
	 * 1（无批次）
	 */
	public static final String DIC_STORAGE_SHIYANCAILIAO_NPC = "11";
	/**
	 * 2试剂（有批次）
	 */
	public static final String DIC_STORAGE_SHIYANCAILIAO_YPC = "12";

	/**
	 * 类型-库存领用-项目
	 */

	public static final String DIC_STORAGE_APPLY_PROJECT = "project";

	/**
	 * 类型-库存领用-实验
	 */
	public static final String DIC_STORAGE_APPLY_EXPERIMENT_ID = "21";

	public static final String DIC_STORAGE_APPLY_EXPERIMENT = "experiment";

	/**
	 * 类型-库存领用-样本
	 */
	public static final String DIC_STORAGE_APPLY_SAMPLE = "sample";

	/**
	 * 类型-库存领用-一般领用
	 */
	public static final String DIC_STORAGE_APPLY_GENERAL = "general";
	/**
	 * moveAverage 移动平均
	 * groupPrice 批价格
	 */
	public static final String DIC_STORAGE_PRICE_METHOD = "moveAverage";

	/**
	 * 耗材物资
	 */
	public static final String DIC_STORAGE_MATERIAL = "spz001";

}