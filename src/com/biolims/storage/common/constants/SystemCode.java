package com.biolims.storage.common.constants;

//系统业务数据的编码
public class SystemCode {

	public static final String DEFAULT_SYSTEMCODE = "NEW";
	/**
	 * 库存主数据-调整管理
	 */
	public static final String STORAGE_ITEM_MODIFY_NAME = "StorageItemModify";
	/**
	 * 库存主数据-明细 0000-9999
	 */
	public static final long STORAGE_ITEM_CODE = 000000;
	/**
	 * 库存主数据-明细
	 */
	public static final String STORAGE_ITEM_NAME = "StorageReagentBuySerial";
	/**
	 * 库存主数据-领用管理100000-199999
	 */
	public static final long STORAGE_APPLE_CODE = 100000;
	/**
	 * 库存主数据-领用管理
	 */
	public static final String STORAGE_APPLE_NAME = "StorageApply";

	/**
	 * 库存主数据-出库管理300000-399999
	 */
	public static final long STORAGE_OUT_CODE = 000;
	/**
	 * 库存主数据-出库管理
	 */
	public static final String STORAGE_OUT_NAME = "StorageOut";
	/**
	 * 库存主数据-退库管理400000-499999
	 */
	public static final long STORAGE_QUIT_CODE = 400000;
	/**
	 * 库存主数据-退库管理
	 */
	public static final String STORAGE_QUIT_NAME = "StorageQuit";
	/**
	 * 库存主数据-报废管理10000-19999
	 */
	public static final long STORAGE_CANCEL_CODE = 10000;
	/**
	 * 库存主数据-报废管理
	 */
	public static final String STORAGE_CANCEL_NAME = "StorageCancel";
	/**
	 * 库存主数据-移库管理20000-29999
	 */
	public static final long STORAGE_MOVE_CODE = 20000;
	/**
	 * 库存主数据-移库管理
	 */
	public static final String STORAGE_MOVE_NAME = "StorageMove";
	/**
	 * 库存主数据-盘点管理9000-9999
	 */
	public static final long STORAGE_CHECK_CODE = 9000;
	/**
	 * 库存主数据-盘点管理
	 */
	public static final String STORAGE_CHECK_NAME = "StorageCheck";
	/**
	 * 库存主数据-调整管理8000-8999
	 */
	public static final long STORAGE_MODIFY_CODE = 000;
	/**
	 * 库存主数据-调整管理
	 */
	public static final String STORAGE_MODIFY_NAME = "StorageModify";
	/**
	 * 库存主数据-调度管理9000-9999
	 */
	public static final long STORAGE_ITEM_MODIFY_CODE = 9000;
	/**
	 * 库存主数据-调整管理
	 */

	/**
	 * 库存主数据-盘点计划10000-10999
	 */
	public static final long STORAGE_CHECK_PLAN_CODE = 10000;
	/**
	 * 库存主数据-盘点计划
	 */
	public static final String STORAGE_CHECK_PLAN_NAME = "StorageCheckPlan";
	/**
	 * 库存-采购入库管理900000-999999
	 */
	public static final long PURCHASE_STORAGEIN_CODE = 000;
	/**
	 * 库存-采购入库
	 */
	public static final String PURCHASE_STORAGEIN_NAME = "StorageIn";

}
