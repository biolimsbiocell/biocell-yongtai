/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：EVENT
 * 类描述：入库管理事件类
 * 创建人：倪毅
 * 创建时间：2012-04
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.storage.storageIn.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.storage.common.constants.SystemConstants;
import com.biolims.storage.storageIn.service.StorageInService;

public class StorageInSetStorageEvent implements ObjectEvent {
	public String operation(String applicationTypeActionId, String formId) throws Exception {
		String r = "";

		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		StorageInService storageInService = (StorageInService) ctx.getBean("storageInService");
		storageInService.storageInSetStorage(applicationTypeActionId, formId);
//		storageInService.commonSetState(applicationTypeActionId, formId, SystemConstants.DIC_STATE_YES_ID);

		return r;
	}
}
