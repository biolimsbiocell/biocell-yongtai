/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：Action
 * 类描述：入库管理
 * 创建人：倪毅
 * 创建时间：2012-04
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.storage.storageIn.action;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.PushData;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.common.service.SystemCodeService;
import com.biolims.dic.model.DicType; //import com.biolims.purchase.model.PurchaseOrderItem;
import com.biolims.experiment.dna.model.DnaTask;
import com.biolims.experiment.dna.model.DnaTaskItem;
import com.biolims.storage.common.constants.SystemCode;
import com.biolims.storage.common.constants.SystemConstants;
import com.biolims.storage.model.Storage;
import com.biolims.storage.model.StorageIn;
import com.biolims.storage.model.StorageInItem;
import com.biolims.storage.model.StorageReagentBuySerial;
import com.biolims.storage.storageIn.dao.StorageInDao;
import com.biolims.storage.storageIn.item.model.StorageInInfoItem;
import com.biolims.storage.storageIn.service.StorageInService;
import com.biolims.system.syscode.model.CodeMain;
import com.biolims.system.syscode.service.CodeMainService;
import com.biolims.util.DateUtil;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.opensymphony.xwork2.ActionContext;

@Namespace("/storage/storageIn")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public class StorageInAction extends BaseActionSupport {

	private static final long serialVersionUID = 6480754183198344386L;
	@Autowired
	private StorageInService paService;

	@Autowired
	private CommonService commonService;

	@Resource
	private SystemCodeService systemCodeService;
	@Resource
	private CodeMainService codeMainService;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private StorageInDao storageInDao;

	// 用于页面上显示模块名称
	private String title = "入库管理";
	// 该action权限id
	private String rightsId = "30301";
	private StorageIn storageIn = new StorageIn();

	/**
	 * 申请列表
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "showStorageInList")
	public String showApplyList() throws Exception {
		// 用于接收通用查询的参数
		// LinkedHashMap<String, String[]> map = new LinkedHashMap<String,
		// String[]>();
		// 属性map<字段名称, ext-list属性数组说明,值为空""则没有该属性>
		// id type format header width sort hide align xtype editable renderer
		// editor
		// 列的id 当"common" 时该列为扩展列 type类型 format格式 header列头 width列宽 sort是否排序
		// hide是否隐藏 align左右格式 xtype ediable是否可编辑 renderer渲染 editor编辑器
		// map.put("id", new String[] { "", "string", "", "入库编号", "150", "true",
		// "", "", "", "", "", "" });
		// map.put("purchaseOrder-id", new String[] { "", "string", "", "采购申请",
		// "150", "true", "", "", "", "", "", "" });
		// map.put("handelUser-name", new String[] { "", "string", "", "入库人员",
		// "150", "true", "", "", "", "", "", "" });
		// map.put("handelDate", new String[] { "", "date",
		// "dateFormat:'Y-m-d'",
		// "入库日期", "100", "true", "", "", "", "", "formatDate", "" });
		// map.put("stateName", new String[] { "", "string", "", "状态", "100",
		// "true", "false", "", "", "", "", "" });
		// String type = generalexttype(map);
		// String col = generalextcol(map);
		// // 用于ext显示
		// putObjToContext("type", type);
		// putObjToContext("col", col);
		// 用于判断当前页面类型,LIST类型
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		// 生成toolbar
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);

		putObjToContext("path",
				ServletActionContext.getRequest().getContextPath()
						+ "/storage/storageIn/showStorageInListJson.action?queryMethod="
						+ getParameterFromRequest("queryMethod"));
		// 导向jsp页面显示
		return dispatcher("/WEB-INF/page/storage/storageIn/showStorageInList.jsp");
	}

	@Action(value = "showStorageInListJson")
	public void showApplyListJson() throws Exception {

		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		String queryMethod = getParameterFromRequest("queryMethod");
		Map r = this.toMakeSessionQuery(data, "StorageIn", dir, sort, queryMethod);
		dir = (String) r.get("dir");
		sort = (String) r.get("sort");
		data = (String) r.get("queryData");
		Map<String, Object> controlMap = paService.findStorageInList(startNum, limitNum, dir, sort, getContextPath(),
				data);
		long totalCount = Long.parseLong(controlMap.get("totalCount").toString());
		List<StorageIn> list = (List<StorageIn>) controlMap.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("handelUser-name", "");
		map.put("handelDate", "yyyy-MM-dd");
		map.put("financeCode-name", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}

	@Action(value = "showStorageInTableJson")
	public void showStorageInTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			Map<String, Object> result = paService.selStorageInTable(start, length, query, col, sort);
			List<StorageIn> list = (List<StorageIn>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("note", "");
			map.put("handelUser-id", "");
			map.put("handelUser-name", "");
			map.put("createDate", "yyyy-MM-dd");
			map.put("handelDate", "yyyy-MM-dd");
			map.put("financeCode-name", "");
			map.put("stateName", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 编辑页面
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "toEditStorageIn")
	public String toEditStorageIn() throws Exception {
		rightsId = "30302";
		String id = getParameterFromRequest("id");
		String handlemethod = "";
		if (id != null && !id.equals("")) {
			storageIn = paService.getStorageIn(id);
			putObjToContext("storageInId", id);

			toState(storageIn.getState());
			if (storageIn.getState().equals(SystemConstants.DIC_STATE_YES))
				handlemethod = SystemConstants.PAGE_HANDLE_METHOD_VIEW;
			else
				handlemethod = SystemConstants.PAGE_HANDLE_METHOD_MODIFY;
			toToolBar(rightsId, "", "", handlemethod);
			showStorageInItemList(id, handlemethod);
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			this.storageIn.setId(SystemCode.DEFAULT_SYSTEMCODE);
			storageIn.setCreateUser(user);
			storageIn.setCreateDate(new Date());
			storageIn.setHandelUser(user);
			storageIn.setHandelDate(new Date());
//			storageIn.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
//			storageIn.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			storageIn.setState("3");
			storageIn.setStateName("新建");
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toState(storageIn.getState());
			showStorageInItemList(id, SystemConstants.PAGE_HANDLE_METHOD_ADD);
			DicType regionType = commonService.get(DicType.class, "pekcity");
			storageIn.setRegionType(regionType);
		}
		return dispatcher("/WEB-INF/page/storage/storageIn/editStorageIn.jsp");
	}

	public void showStorageInItemList(String paId, String handlemethod) throws Exception {

		String exttype = "";
		String extcol = "";
		String editflag = "true";
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		if (handlemethod.equals(SystemConstants.PAGE_HANDLE_METHOD_VIEW)) {
			editflag = "false";
		}
		map.put("id", new String[] { "", "string", "", "id", "150", "false", "true", "", "", "", "", "" });
		map.put("storage-id", new String[] { "", "string", "", "申请对象", "120", "false", "false", "", "", "", "", "" });
		map.put("storage-name", new String[] { "", "string", "", "名称", "120", "false", "false", "", "", "", "", "" });
		map.put("storage-searchCode",
				new String[] { "", "string", "", "检索码", "100", "false", "true", "", "", "", "", "" });
		map.put("storage-spec", new String[] { "", "string", "", "规格", "100", "false", "false", "", "", "", "", "" });
		map.put("storage-jdeCode",
				new String[] { "", "string", "", "JDE编码", "120", "false", "false", "", "", "", "", "" });
		map.put("code", new String[] { "", "string", "", "批号", "100", "false", "false", "", "", editflag, "", "code" });
		map.put("storage-productAddress",
				new String[] { "", "string", "", "产地", "100", "false", "true", "", "", "", "", "" });
		map.put("productDate", new String[] { "", "date", "dateFormat:'Y-m-d'", "生产日期", "100", "false", "", "", "",
				editflag, "formatDate", "productDate" });
		map.put("expireDate", new String[] { "", "date", "dateFormat:'Y-m-d'", "失效日期", "100", "false", "", "", "",
				editflag, "formatDate", "expireDate" });
		map.put("num", new String[] { "", "float", "", "入库数量", "100", "false", "false", "", "", editflag, "formatCss",
				"inNum" });
		map.put("storage-unit-name",
				new String[] { "", "string", "", "单位", "100", "false", "false", "", "", "", "", "" });
		map.put("price",
				new String[] { "", "float", "", "入库价格", "100", "false", "false", "", "", editflag, "", "inPrice" });

		map.put("position-id",
				new String[] { "", "string", "", "存储位置ID", "120", "false", "true", "", "", editflag, "", "position" });
		map.put("position-name",
				new String[] { "", "string", "", "存储位置名称", "120", "false", "false", "", "", editflag, "", "position" });
		map.put("inScale", new String[] { "", "float", "", "入库比例(%)", "100", "false", "true", "", "", "", "", "" });
		map.put("inNum", new String[] { "", "float", "", "已入库数量", "100", "false", "true", "", "", "", "", "" });
		map.put("cancelNum", new String[] { "", "float", "", "已退货数量", "100", "false", "true", "", "", "", "", "" });
		map.put("inCondition", new String[] { "", "string", "", "入库情况", "100", "false", "true", "", "", "", "", "" });
		// 声明
		String statement = "";
		// 声明一个lisener
		String statementLisener = "";
		// 生成ext用type字符串
		exttype = generalexttype(map);
		// 生成ext用col字符串
		extcol = generalextcol(map);
		putObjToContext("type", exttype);
		putObjToContext("col", extcol);
		putObjToContext("path", ServletActionContext.getRequest().getContextPath()
				+ "/storage/storageIn/showStorageInItemListJson.action?storageInId=" + paId);
		putObjToContext("statement", statement);
		putObjToContext("statementLisener", statementLisener);
		if (paId != null)
			putObjToContext("storageInId", paId);
		putObjToContext("handlemethod", handlemethod);

	}

	/**
	 * 访问查看页面
	 */
	@Action(value = "toViewStorageIn")
	public String toViewStorageIn() throws Exception {
		rightsId = "30302";
		String id = getParameterFromRequest("id");
		if (id != null && !id.equals("")) {
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
			storageIn = paService.getStorageIn(id);
			putObjToContext("storageInId", id);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
			showStorageInItemList(id, SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		}

		return dispatcher("/WEB-INF/page/storage/storageIn/editStorageIn.jsp");
	}

	@Action(value = "showStorageInItemListJson")
	public void showStorageInItemListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String storageInId = getParameterFromRequest("storageInId");
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		List<StorageInItem> list = null;
		long totalCount = 0;
		if (storageInId != null && !storageInId.equals("")) {
			Map<String, Object> controlMap = paService.showStorageInItemList(startNum, limitNum, dir, sort,
					getContextPath(), storageInId);
			totalCount = Long.parseLong(controlMap.get("totalCount").toString());
			list = (List<StorageInItem>) controlMap.get("list");
		}
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("storage-id", "");
		map.put("storage-name", "");
		map.put("storage-searchCode", "");
		map.put("storage-spec", "");
		map.put("storage-jdeCode", "");
		map.put("storage-kit-id", "");
		map.put("storage-kit-name", "");
		map.put("storage-reactionNum", "");
		map.put("code", "");
		map.put("storage-unit-name", "");
		map.put("productDate", "yyyy-MM-dd");
		map.put("expireDate", "yyyy-MM-dd");
		map.put("num", "");
		map.put("price", "");
		map.put("serial", "");
		map.put("inScale", "#.####");
		map.put("inNum", "");
		map.put("cancelNum", "");
		map.put("inCondition", "");
		map.put("position-id", "");
		map.put("position-name", "");
		map.put("qcPass", "");
		map.put("note", "");
		map.put("note2", "");
		if (list != null)
			new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delStorageInItems")
	public void delStorageInItems() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			paService.delStorageInItems(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 新增采购申请。
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "save")
	public String save() throws Exception {
		String data = getParameterFromRequest("jsonDataStr");
		String id = this.storageIn.getId();
		if (id == null || id.length() <= 0 || SystemCode.DEFAULT_SYSTEMCODE.equals(id)) {
			String code = systemCodeService.getCodeByPrefix(SystemCode.PURCHASE_STORAGEIN_NAME,
					"MI" + DateUtil.dateFormatterByPattern(new Date(), "yyMMdd"), SystemCode.PURCHASE_STORAGEIN_CODE, 3,
					null);
			this.storageIn.setId(code);
		}
		paService.save(storageIn);
		if (data != null && !data.equals(""))
			saveStorageInItem(storageIn.getId(), data);
		// 具体操作，如删除，填加动作，应用redirect
		return redirect("/storage/storageIn/toEditStorageIn.action?id=" + storageIn.getId());
	}

	public void saveStorageInItem(String storageInId, String json) throws Exception {
		paService.saveStorageInItem(storageInId, json);

	}

	/**
	 * 保存明细
	 */
	@Action(value = "saveStorageInItem")
	public void saveStorageInItem() throws Exception {
		String storageInId = getParameterFromRequest("storageInId");
		String json = getParameterFromRequest("data");
		paService.saveStorageInItem(storageInId, json);
	}

	/**
	 * 删除明细
	 */
	@Action(value = "delStorageInItem")
	public void delStorageInItem() throws Exception {
		String id = getParameterFromRequest("id");
		paService.delStorageInItem(id);
		// 具体操作，如删除，填加动作，应用redirect
	}

	@Action(value = "showPurchaseOrderItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showPurchaseOrderItemListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String purchaseOrderId = getParameterFromRequest("purchaseOrderId");
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		// List<PurchaseOrderItem> list = null;
		long totalCount = 0;
		// if (purchaseOrderId != null && !purchaseOrderId.equals("")) {
		// Map<String, Object> controlMap =
		// paService.showPurchaseOrderItemList(startNum, limitNum, dir, sort,
		// getContextPath(), purchaseOrderId);
		// totalCount = Long.parseLong(controlMap.get("totalCount").toString());
		// list = (List<PurchaseOrderItem>) controlMap.get("list");
		// }
		StringBuffer str = new StringBuffer();
		// for (int i = 0; i < list.size(); i++) {
		// // PurchaseOrderItem poi = list.get(i);
		// str.append("{");
		// str.append("'id':'").append(poi.getId()).append("',");
		// str.append("'storage-id':'").append(poi.getStorage().getId()).append("',");
		// str.append("'storage-name':'").append(poi.getStorage().getName() ==
		// null ? "" : poi.getStorage().getName())
		// .append("',");
		// str.append("'storage-searchCode':'").append(
		// poi.getStorage().getSearchCode() == null ? "" :
		// poi.getStorage().getSearchCode()).append("',");
		// str.append("'storage-spec':'").append(poi.getStorage().getSpec() ==
		// null ? "" : poi.getStorage().getSpec())
		// .append("',");
		// str.append("'storage-breed':'").append(
		// poi.getStorage().getBreed() == null ? "" :
		// poi.getStorage().getBreed()).append("',");
		//
		// str.append("'storage-unit-name':'").append(
		// poi.getStorage().getUnit() == null ? "" :
		// poi.getStorage().getUnit().getName()).append("',");
		// str.append("'costCenter-id':'").append(poi.getCostCenter() == null ?
		// "" : poi.getCostCenter().getId())
		// .append("',");
		// str.append("'costCenter-name':'").append(poi.getCostCenter() == null
		// ? "" : poi.getCostCenter().getName())
		// .append("',");
		// str.append("'price':'").append(poi.getPrice() == null ? "" :
		// poi.getPrice()).append("',");
		// str.append("'purchaseOrderNum':'").append(poi.getNum() == null ? "" :
		// poi.getNum()).append("',");
		// str.append("'inNum':'").append(
		// paService.getStorageInSum(poi.getPurchaseOrder().getId(),
		// poi.getStorage().getId())).append("',");
		// str.append("'cancelNum':'").append(
		// paService.getPurchaseCancelSum(poi.getPurchaseOrder().getId(),
		// poi.getStorage().getId())).append(
		// "'");
		//
		// str.append("}");
		// }
		StringBuffer sb = new StringBuffer("{'total':").append(totalCount).append(",'results':[").append(str)
				.append("]}");
		String a = sb.toString().replace("}{", "},{");
		new SendData().sendDataJson(a, ServletActionContext.getResponse());
	}

	@Action(value = "getStorageInItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void getStorageInItemListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String storageInId = getParameterFromRequest("storageInId");
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		List<StorageInItem> list = null;
		long totalCount = 0;
		if (storageInId != null && !storageInId.equals("")) {
			Map<String, Object> controlMap = paService.showStorageInItemList(startNum, limitNum, dir, sort,
					getContextPath(), storageInId);
			totalCount = Long.parseLong(controlMap.get("totalCount").toString());
			list = (List<StorageInItem>) controlMap.get("list");
		}
		StringBuffer str = new StringBuffer();
		// for (int i = 0; i < list.size(); i++) {
		// StorageInItem poi = list.get(i);
		// str.append("{");
		// str.append("'id':'").append(poi.getId()).append("',");
		// str.append("'storage-id':'").append(poi.getStorage().getId()).append("',");
		// str.append("'storage-name':'").append(poi.getStorage().getName() ==
		// null ? "" : poi.getStorage().getName())
		// .append("',");
		// str.append("'storage-searchCode':'").append(
		// poi.getStorage().getSearchCode() == null ? "" :
		// poi.getStorage().getSearchCode()).append("',");
		// str.append("'storage-spec':'").append(poi.getStorage().getSpec() ==
		// null ? "" : poi.getStorage().getSpec())
		// .append("',");
		// str.append("'storage-breed':'").append(
		// poi.getStorage().getBreed() == null ? "" :
		// poi.getStorage().getBreed()).append("',");
		// str.append("'storage-unit-name':'").append(
		// poi.getStorage().getUnit() == null ? "" :
		// poi.getStorage().getUnit().getName()).append("',");
		// str.append("'price':'").append(poi.getPrice() == null ? "" :
		// poi.getPrice()).append("',");
		// str.append("'purchaseOrderNum':'").append(
		// poi.getPurchaseOrderItem() == null ? "" :
		// poi.getPurchaseOrderItem().getNum()).append("',");
		// str.append("'purchaseOrderItem-id':'").append(
		// poi.getPurchaseOrderItem() == null ? "" :
		// poi.getPurchaseOrderItem().getId()).append("',");
		// str.append("'inNum':'").append(poi.getNum()).append("',");
		// str.append("'storageNum':'").append(
		// paService.selectStorageReagentBuyCount(poi.getStorage().getId(),
		// poi.getId())).append("',");
		// str.append("'storage-num':'").append(poi.getStorage().getNum()).append("',");
		// str.append("'storage-type-id':'").append(poi.getStorage().getType().getId()).append("',");
		// str.append("'costCenter-id':'").append(poi.getCostCenter() == null ?
		// "" : poi.getCostCenter().getId())
		// .append("',");
		// str.append("'costCenter-name':'").append(poi.getCostCenter() == null
		// ? "" : poi.getCostCenter().getName())
		// .append("'");
		//
		// str.append("}");
		// }
		StringBuffer sb = new StringBuffer("{'total':").append(totalCount).append(",'results':[").append(str)
				.append("]}");
		String a = sb.toString().replace("}{", "},{");
		new SendData().sendDataJson(a, ServletActionContext.getResponse());
	}

	@Action(value = "storageInSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showStorageInList() throws Exception {
		// 用于接收通用查询的参数
		LinkedHashMap<String, String[]> map = new LinkedHashMap<String, String[]>();
		map.put("id", new String[] { "", "string", "", "入库编号", "150", "true", "", "", "", "", "", "" });
		map.put("purchaseOrder-id", new String[] { "", "string", "", "采购订单", "150", "true", "", "", "", "", "", "" });
		map.put("handelUser-name", new String[] { "", "string", "", "入库人员", "150", "true", "", "", "", "", "", "" });
		map.put("handelDate", new String[] { "", "date", "dateFormat:'Y-m-d'", "入库日期", "100", "true", "", "", "", "",
				"formatDate", "" });
		map.put("reachDate", new String[] { "", "date", "dateFormat:'Y-m-d'", "到货日期", "100", "true", "", "", "", "",
				"formatDate", "" });
		map.put("financeCode-name",
				new String[] { "", "string", "", "财务科目", "100", "true", "true", "", "", "", "", "" });
		map.put("currencyType-name",
				new String[] { "", "string", "", "币种", "100", "true", "true", "", "", "", "", "" });
		map.put("stateName", new String[] { "", "string", "", "状态", "100", "true", "true", "", "", "", "", "" });
		map.put("supplier-name", new String[] { "", "string", "", "供应商", "150", "true", "true", "", "", "", "", "" });
		map.put("supplier-linkMan",
				new String[] { "", "string", "", "供应商联系人", "150", "true", "true", "", "", "", "", "" });
		map.put("supplier-linkTel",
				new String[] { "", "string", "", "供应商联系电话", "150", "true", "true", "", "", "", "", "" });
		map.put("purchaseOrder-createUser-name",
				new String[] { "", "string", "", "采购人", "150", "true", "true", "", "", "", "", "" });
		map.put("purchaseOrder-reachDate",
				new String[] { "", "string", "", "采购日期", "150", "true", "true", "", "", "", "", "" });
		String type = generalexttype(map);
		String col = generalextcol(map);
		// 用于ext显示
		putObjToContext("type", type);
		putObjToContext("col", col);
		// 用于判断当前页面类型,LIST类型
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		putObjToContext("path",
				ServletActionContext.getRequest().getContextPath() + "/storage/storageIn/storageInSelectJson.action");
		// 导向jsp页面显示
		return dispatcher("/WEB-INF/page/storage/storageIn/storageInSelect.jsp");
	}

	@Action(value = "storageInSelectJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void storageInSelectJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, Object> controlMap = paService.findStorageInList(startNum, limitNum, dir, sort, getContextPath(),
				data);
		long totalCount = Long.parseLong(controlMap.get("totalCount").toString());
		List<StorageIn> list = (List<StorageIn>) controlMap.get("list");
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("purchaseOrder-id", "");
		map.put("handelUser-name", "");
		map.put("handelDate", "yyyy-MM-dd");
		map.put("purchaseOrder-reachDate", "yyyy-MM-dd");
		map.put("financeCode-name", "");
		map.put("stateName", "");
		map.put("purchaseOrder-createUser-name", "");
		map.put("purchaseOrder-reachDate", "yyyy-MM-dd");

		map.put("scopeId", "");
		map.put("scopeName", "");

		new SendData().sendDateJson(map, list, totalCount, ServletActionContext.getResponse());
	}

	// 打印
	@Action(value = "makeCode", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void makeCode() throws Exception {
		String[] ids = getRequest().getParameterValues("ids[]");
		// String[] sampleCode =
		// getRequest().getParameterValues("sampleCode[]");
		// String[] expireDate =
		// getRequest().getParameterValues("expireDate[]");
		String id = getParameterFromRequest("id");// 打印机id

		CodeMain c = this.codeMainService.get(id);
		String printStr = "";
		String context = dy(ids, c);
		printStr = context;
		Socket socket = null;
		OutputStream os;
		try {
			socket = new Socket();
			// SocketAddress sa = new InetSocketAddress("10.21.9.172", 9100);
			// SocketAddress sa = new InetSocketAddress("10.21.9.222", 9100);
			SocketAddress sa = new InetSocketAddress(c.getIp(), 9100);
			socket.connect(sa);
			os = socket.getOutputStream();
			os.write(printStr.getBytes("GBK"));
			os.flush();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
				}
			}
		}
	}

	public String dy(String[] ids, CodeMain c) {

		String context = "";
		int a = 0;
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MMM.yyyy", Locale.ENGLISH);

		for (String id : ids) {
			StorageInItem item = this.commonService.get(StorageInItem.class, id);
			String[] code = item.getNote().split(",");
			String s = "";
			if (item.getExpireDate() != null) {
				s = sdf.format(item.getExpireDate());
			}
			int length = code.length;
			int n = 0;
			if (length % 2 > 0) {
				n = length / 2 + 1;
			} else {
				n = length / 2;
			}

			for (int i = 0; i < n; i++) {

				String str = "";
				if (length > a) {

					str += "#" + code[a] + "&" + code[a] + "&" + s;
					a++;
					if (length > a) {
						str += "&" + code[a] + "&" + code[a] + "&" + s;
						a++;
					} else {
						str += "&&&";
					}
				}

				String dyCode = c.getCode();
				String d = dyCode.replaceAll("~~~AAA~~~", str);
				context += d;
			}
		}
		return context;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public StorageIn getStorageIn() {
		return storageIn;
	}

	public void setStorageIn(StorageIn storageIn) {
		this.storageIn = storageIn;
	}

	/**
	 * 
	 * @Title: saveStorageInItem @Description: 保存库存主数据入库明细 @author : @date @throws
	 * Exception void @throws
	 */
	@Action(value = "saveStorageInItemTable")
	public void saveStorageInItemTable() throws Exception {
		String storageIn_id = getParameterFromRequest("id");
		String item = getParameterFromRequest("dataJson");
		String logInfo = getParameterFromRequest("logInfo");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			paService.saveStorageInItemTable(storageIn_id, item, logInfo);
			result.put("success", true);
			result.put("id", storageIn_id);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 
	 * @Title: showDnaTaskItemAfTableJson @Description: 排板后样本展示 @author
	 * : @date @throws Exception void @throws
	 */

	@Action(value = "showStorageInItemTableJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showStorageInItemTableJson() throws Exception {
		String query = getParameterFromRequest("query");
		String colNum = getParameterFromRequest("order[0][column]");
		String col = getParameterFromRequest("columns[" + colNum + "][data]");
		String sort = getParameterFromRequest("order[0][dir]");
		Integer start = Integer.valueOf(getParameterFromRequest("start"));
		Integer length = Integer.valueOf(getParameterFromRequest("length"));
		String draw = getParameterFromRequest("draw");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = paService.selStorageInItemTable(scId, start, length, query, col, sort);
			List<StorageInItem> list = (List<StorageInItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("storage-id", "");
			map.put("storage-name", "");
			map.put("storage-barCode", "");
			map.put("storage-searchCode", "");
			map.put("storage-spec", "");
			map.put("storage-jdeCode", "");
			map.put("storage-kit-id", "");
			map.put("storage-kit-name", "");
			map.put("storage-reactionNum", "");
			map.put("code", "");
			map.put("storage-unit-name", "");
			map.put("productDate", "yyyy-MM-dd HH:mm");
			map.put("expireDate", "yyyy-MM-dd HH:mm");
			map.put("num", "");
			map.put("price", "");
			map.put("serial", "");
			map.put("inScale", "#.####");
			map.put("inNum", "");
			map.put("cancelNum", "");
			map.put("inCondition", "");
			map.put("position-id", "");
			map.put("position-name", "");
			map.put("qcPass", "");
			map.put("note", "");
			map.put("note2", "");

			map.put("unitGroupNew-name", "");
			map.put("unitGroupNew-id", "");
			map.put("packingNum", "");
			map.put("unitGroupNew-mark-name", "");
			map.put("unitGroupNew-mark2-name", "");
			map.put("unitGroupNew-cycle", "");

			map.put("storage-position-id", "");
			map.put("storage-position-name", "");
			String data = new SendData().getDateJsonForDatatable(map, list);
			HttpUtils.write(PushData.pushData(draw, result, data));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title: showDnaTaskList @Description:展示主表 @author @date @return @throws
	 * Exception String @throws
	 */
	@Action(value = "showStorageInTable")
	public String showStorageInTable() throws Exception {
		rightsId = "30301";
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/storage/storageIn/storageInTable.jsp");
	}

	/**
	 * 
	 * @Title: addStorageInItem @Description: 添加主数据到入库明细表 @author : @date @throws
	 * Exception void @throws
	 */
	@Action(value = "addStorageInItem")
	public void addStorageInItem() throws Exception {
		String note = getParameterFromRequest("note");
		String handelDate = getParameterFromRequest("handelDate");
		String id = getParameterFromRequest("id");
		String createDate = getParameterFromRequest("createDate");

		String createUser = getParameterFromRequest("createUser");

		String[] ids = getRequest().getParameterValues("ids[]");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String storageId = paService.addStorageInItem(ids, note, handelDate, id, createUser, createDate);
			result.put("success", true);
			result.put("data", storageId);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 
	 * @Title: saveStorageInAndItem @Description: 大保存方法 @author : @date @throws
	 * Exception void @throws
	 */
	@Action(value = "saveStorageInAndItem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveStorageInAndItem() throws Exception {
		String dataValue = getParameterFromRequest("dataValue");
		// 主表changeLog
		String changeLog = getParameterFromRequest("changeLog");
		// 子表changeLogItem
		String changeLogItem = getParameterFromRequest("changeLogItem");

		String id = getParameterFromRequest("id");
		String createDate = getParameterFromRequest("createDate");
		String createUser = getParameterFromRequest("createUser");

		String str = "[" + dataValue + "]";

		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(str, List.class);

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			for (Map<String, Object> map1 : list) {
				StorageIn si = new StorageIn();
				Map aMap = new HashMap();
				si = (StorageIn) commonDAO.Map2Bean(map1, si);
				// 保存主表信息
				StorageIn newSi = paService.saveStorageInById(si, id, createUser, createDate, changeLog);

				newSi.setScopeId((String) ActionContext.getContext().getSession().get("scopeId"));
				newSi.setScopeName((String) ActionContext.getContext().getSession().get("scopeName"));

				// 保存子表信息
				aMap.put("ImteJson", getParameterFromRequest("ImteJson"));
				paService.saveStorageInAndItem(newSi, aMap, changeLog, changeLogItem);
				map.put("id", newSi.getId());
			}
			map.put("success", true);

		} catch (Exception e) {
			map.put("success", false);
			map.put("msg", e.getMessage());
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 判断U8输入的单号在系统中是否存在
	 * 
	 * @throws Exception
	 */
	@Action(value = "findU8StorageIn")
	public void findU8StorageIn() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String id = getParameterFromRequest("sampleCode");
			StorageIn si = commonDAO.get(StorageIn.class, id);
			if (si != null) {
				map.put("cunzai", true);
			} else {
				map.put("cunzai", false);
			}
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 判断U8输入的单号在系统中是否存在
	 * 
	 * @throws Exception
	 */
	@Action(value = "saveU8StorageIn")
	public void saveU8StorageIn() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String id = getParameterFromRequest("sampleCode");
			String z = paService.getMaterialoutService(id);
			if ("".equals(z)) {
				map.put("cunzai", false);
			} else {
				map.put("cunzai", true);
			}
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 判断试剂批次是否重复，
	 * 
	 * @throws Exception
	 */
	@Action(value = "findwhile")
	public void findwhile() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String id = getParameterFromRequest("id");
			String z = "";
			// 根据入库单的id 查询该入库单下的样本是否存在重复批次号
			List<StorageInItem> siis = commonService.get(StorageInItem.class, "storageIn.id", id);
			if (siis.size() > 0) {
				for (StorageInItem sii : siis) {
					List<StorageReagentBuySerial> srbs = storageInDao.getStoragentBuySerials(sii.getSerial(),
							sii.getStorage().getId());
					if (srbs.size() > 0) {
						z = z + "原辅料：" + srbs.get(0).getStorage().getName() + "，批号重复：" + sii.getSerial() + "。";
					} else {

					}
				}
			}
			if ("".equals(z)) {
				map.put("cunzai", false);
				map.put("cunzaineirong", z);
			} else {
				map.put("cunzai", true);
				map.put("cunzaineirong", z);
			}
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

}
