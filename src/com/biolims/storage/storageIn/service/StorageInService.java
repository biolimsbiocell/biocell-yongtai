/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：Servie
 * 类描述：入库管理
 * 创建人：倪毅
 * 创建时间：2012-04
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.storage.storageIn.service;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.biolims.applicationType.service.ApplicationTypeService;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.common.service.SystemCodeService;
import com.biolims.core.model.user.Department;
import com.biolims.dic.model.DicMainType;
import com.biolims.dic.model.DicState;
import com.biolims.dic.model.DicType;
import com.biolims.dic.model.DicUnit;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.log.model.LogInfo;
import com.biolims.openapi4.exception.OpenAPIException;
import com.biolims.openapi4.service.DepartmentU8Service;
import com.biolims.openapi4.service.InventoryService;
import com.biolims.openapi4.service.MaterialoutService;
import com.biolims.storage.common.constants.SystemCode;
import com.biolims.storage.common.constants.SystemConstants;
import com.biolims.storage.main.service.StorageService;
import com.biolims.storage.model.DicStorageType;
import com.biolims.storage.model.Storage;
import com.biolims.storage.model.StorageIn;
import com.biolims.storage.model.StorageInItem;
import com.biolims.storage.model.StorageReagentBuySerial;
import com.biolims.storage.storageIn.dao.StorageInDao;
import com.biolims.system.work.model.UnitGroupNew;
import com.biolims.util.BeanUtils;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;
import com.biolims.core.model.user.Department;

@Service
@SuppressWarnings({ "rawtypes", "unchecked" })
public class StorageInService extends ApplicationTypeService {

	@Resource
	private CommonDAO commonDAO;
	@Resource
	private StorageInDao storageInDao;
	@Resource
	private StorageService storageService;
	@Resource
	private SystemCodeService systemCodeService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private CommonService commonService;

	// @Resource
	// private PurchaseOrderDao purchaseOrderDAO;

	/**
	 * 检索list,采用map方式传递检索参数
	 * 
	 * @return 参数值MAP，如果传递的为null返回空字符串
	 */
	public Map<String, Object> findStorageInList(int startNum, int limitNum, String dir, String sort,
			String contextPath, String data) throws Exception {
		// 检索条件map,每个字段检索值
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		// 检索方式map,每个字段的检索方式
		Map<String, String> mapForCondition = new HashMap<String, String>();
		String startDate = "";
		String endDate = "";
		if (data != null && !data.equals("")) {

			BeanUtils.data2Map(data, mapForQuery, mapForCondition);
			mapForQuery.remove("queryStartDate");
			mapForQuery.remove("queryEndDate");
			mapForCondition.remove("queryStartDate");
			mapForCondition.remove("queryEndDate");
		}

		// 将map值传入，获取列表
		Map<String, Object> criteriaMap = commonDAO.findObjectCriteria(startNum, limitNum, dir, sort, StorageIn.class,
				mapForQuery, mapForCondition);
		Criteria cr = (Criteria) criteriaMap.get("criteria");
		Criteria cc = (Criteria) criteriaMap.get("criteriaCount");
		Map map = JsonUtils.toObjectByJson(data, Map.class);
		if (data != null && !data.equals("")) {
			if (map.get("queryStartDate") != null && !map.get("queryStartDate").equals("")) {

				startDate = (String) map.get("queryStartDate");
				cr.add(Restrictions.ge("handelDate", DateUtil.parse(startDate)));
				cc.add(Restrictions.ge("handelDate", DateUtil.parse(startDate)));
			}
			if (map.get("queryEndDate") != null && !map.get("queryEndDate").equals("")) {

				endDate = (String) map.get("queryEndDate");
				cr.add(Restrictions.le("handelDate", DateUtil.parse(endDate)));
				cc.add(Restrictions.le("handelDate", DateUtil.parse(endDate)));
			}

		}
		Map<String, Object> controlMap = commonDAO.findObjectList(startNum, limitNum, dir, sort, StorageIn.class,
				mapForQuery, mapForCondition, cc, cr);
		return controlMap;
	}

	/**
	 * 根据id获得申请单对象
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public StorageIn getStorageIn(String id) throws Exception {
		return commonDAO.get(StorageIn.class, id);
	}

	/**
	 * 检索申请单明细
	 * 
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param contextPath
	 * @param storageInId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> showStorageInItemList(int startNum, int limitNum, String dir, String sort,
			String contextPath, String storageInId) throws Exception {
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		Map<String, String> mapForCondition = new HashMap<String, String>();
		if (storageInId != null && !storageInId.equals("")) {
			mapForQuery.put("storageIn.id", storageInId);
			mapForCondition.put("storageIn.id", "=");
		}

		Map<String, Object> controlMap = commonDAO.findObjectCondition(startNum, limitNum, dir, sort,
				StorageInItem.class, mapForQuery, mapForCondition);

		List<StorageInItem> list = (List<StorageInItem>) controlMap.get("list");
		for (StorageInItem sii : list) {
			// if (sii.getStorageIn().getPurchaseOrder() != null) {
			// Double sum =
			// purchaseOrderDAO.getPurchaseOrderSum(sii.getStorageIn().getPurchaseOrder().getId(),
			// sii
			// .getStorage().getId());
			//
			// //计算采购数量
			// sii.setPurchaseOrderNum(sum);
			// sii.setInNum(getStorageInSum(sii.getStorageIn().getPurchaseOrder().getId(),
			// sii.getStorage().getId()));
			// if ((sii.getNum() != null && sii.getNum() != 0.0)
			// && (sii.getPurchaseOrderNum() != null &&
			// sii.getPurchaseOrderNum() != 0)) {
			//
			// //计算入库比例=入库数量/采购数量*100%
			// if (sii.getInNum() != null && sii.getPurchaseOrderNum() != null)
			// sii.setInScale((sii.getInNum() / sii.getPurchaseOrderNum()) *
			// 100);
			//
			// //系统自动生成，如果入库比例=100%，入库情况为“完整入库”，如果入库比例≠100%，入库情况为“不完整入库”；
			// if (sii.getInNum() != null && sii.getPurchaseOrderNum() != null)
			// {
			// if (sii.getInNum() / sii.getPurchaseOrderNum() < 1) {
			// sii.setInCondition("不完整入库");
			// } else {
			// sii.setInCondition("完整入库");
			// }
			// }
			// }
			// }

		}
		controlMap.put("list", list);
		return controlMap;
	}

	public Double getStorageInSum(String orderId, String storageId) throws Exception {

		return storageInDao.getStorageInSum(orderId, storageId);
	}

	public Double getPurchaseCancelSum(String orderId, String storageId) throws Exception {

		return storageInDao.getPurchaseCancelSum(orderId, storageId);
	}

	public Double selectStorageReagentBuyCount(String storageId, String inItemId) throws Exception {
		return storageInDao.selectStorageReagentBuyCount(storageId, inItemId);
	}

	/**
	 * 检索申请单明细
	 * 
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param contextPath
	 * @param purchaseOrderId
	 * @return
	 * @throws Exception
	 */
	// public Map<String, Object> showPurchaseOrderItemList(int startNum, int
	// limitNum, String dir, String sort,
	// String contextPath, String purchaseOrderId) throws Exception {
	// Map<String, Object> mapForQuery = new HashMap<String, Object>();
	// Map<String, String> mapForCondition = new HashMap<String, String>();
	// if (purchaseOrderId != null && !purchaseOrderId.equals("")) {
	// mapForQuery.put("purchaseOrder.id", purchaseOrderId);
	// mapForCondition.put("purchaseOrder.id", "=");
	// }
	//
	// // Map<String, Object> controlMap =
	// commonDAO.findObjectCondition(startNum, limitNum, dir, sort,
	// // PurchaseOrderItem.class, mapForQuery, mapForCondition);
	// //
	// // List<PurchaseOrderItem> list = (List<PurchaseOrderItem>)
	// controlMap.get("list");
	//
	// // controlMap.put("list", list);
	// // return controlMap;
	// }

	public List<StorageInItem> showStorageInItemList(String storageInId) throws Exception {
		Map<String, Object> mapForQuery = new HashMap<String, Object>();
		Map<String, String> mapForCondition = new HashMap<String, String>();
		if (storageInId != null && !storageInId.equals("")) {
			mapForQuery.put("storageIn.id", storageInId);
			mapForCondition.put("storageIn.id", "=");
		}

		Map<String, Object> controlMap = commonDAO.findObjectCondition(-1, -1, "", "", StorageInItem.class, mapForQuery,
				mapForCondition);

		List<StorageInItem> list = (List<StorageInItem>) controlMap.get("list");

		return list;
	}

	/**
	 * 新增或更新采购申请
	 * 
	 * @param pa
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void save(StorageIn pa) throws Exception {
		commonDAO.saveOrUpdate(pa);
	}

	/**
	 * 
	 * 维护明细信息,由ext ajax调用
	 * 
	 * @param userId     用户id
	 * @param jsonString 用户认证jsonString
	 * @return void
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveStorageInItem(String id, String jsonString) throws Exception {

		// 将json读入map
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(jsonString, List.class);
		for (Map<String, Object> map : list) {
			StorageInItem em = new StorageInItem();

			// 将map信息读入实体类
			em = (StorageInItem) commonDAO.Map2Bean(map, em);
			if (em.getId() != null && em.getId().equals("")) {
				em.setId(null);
			}
			if (em.getStorage() == null) {
				Storage s = storageService.getStorageByName((String) map.get("storage-name"));
				em.setStorage(s);
			}
			StorageIn ep = new StorageIn();
			ep.setId(id);
			em.setStorageIn(ep);
			commonDAO.saveOrUpdate(em);
		}
	}

	/**
	 * 
	 * 删除明细,由ext ajax调用
	 * 
	 * @param id 认证ID
	 * @return void
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delStorageInItem(String id) {
		StorageInItem ppi = new StorageInItem();
		ppi.setId(id);
		commonDAO.delete(ppi);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delStorageInItems(String[] ids) throws Exception {
		for (String id : ids) {
			StorageInItem scp = commonDAO.get(StorageInItem.class, id);
			commonDAO.delete(scp);
			
			if (ids.length>0) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				li.setFileId(scp.getStorageIn().getId());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setClassName("StorageIn");
				li.setModifyContent("入库管理:"+"原辅料编号:"+scp.getStorage().getId()+"名称:"+scp.getStorage().getName()+"的数据已删除");
				li.setState("2");
				li.setStateName("数据删除");
				commonDAO.saveOrUpdate(li);
			}
		}
	}

	/**
	 * 入库动作
	 * 
	 * @param formId
	 * @return
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void storageInSetStorage(String applicationTypeActionId, String formId) throws Exception {
		StorageIn si = this.storageInDao.get(StorageIn.class, formId);
//		if (si.getStateName().equals("修改")) {
//			List<StorageInItem> list1 = showStorageInItemList(formId);
//			int addNum = 0;
//			for (StorageInItem sii : list1) {
//				StorageReagentBuySerial sr = storageService
//						.findStorageReagentBuyByInItemId(sii.getId());
//				Storage aStorage = commonDAO.get(sii.getStorage().getClass(),
//						sii.getStorage().getId());
//				Double num = 0.0;
//				if (aStorage.getNum() != null)
//					num = aStorage.getNum();// 原库存数量
//				if (sr != null) {
//					if (sr.getNum() != null) {
//						num = num - sr.getNum();// 新库存数量
//					}
//				}
//				num = num + sii.getNum();
//				aStorage.setNum(num);
//				storageInDao.delete(sr);
//				commonDAO.saveOrUpdate(aStorage);
//
//				String code = systemCodeService.getSystemCodeByNumber(
//						SystemCode.STORAGE_ITEM_NAME, addNum);
//				addNum++;
//
//				StorageReagentBuySerial srb = new StorageReagentBuySerial();
//				srb.setId(code);
//				if (aStorage.getCurrencyType() != null)
//					srb.setCurrencyType(aStorage.getCurrencyType());
//				srb.setStorage(aStorage);
//				if (sii.getNum() != null)
//					srb.setNum(sii.getNum());
//				srb.setInDate(new Date());
//				if (sii.getProductDate() != null)
//					srb.setProductDate(sii.getProductDate());
//
//				srb.setSerial(sii.getSerial());
//				srb.setCode(sii.getCode());
//				srb.setSpec(sii.getSpec());
//
//				StorageIn aStorageIn = commonDAO.get(sii.getStorageIn()
//						.getClass(), sii.getStorageIn().getId());
//				Date date = aStorageIn.getHandelDate();
//				Integer durableDays = aStorage.getDurableDays();
//				if (date != null && durableDays != null) {
//					long day = (long) (durableDays * 24 * 60 * 60 * 1000);
//					long time = date.getTime();
//					time += day;
//					srb.setExpireDate(new Date(time));
//				} else {
//					if (sii.getExpireDate() != null)
//						srb.setExpireDate(sii.getExpireDate());
//				}
//				Date expireDate = srb.getExpireDate();
//				Integer remindLeadTime = aStorage.getRemindLeadTime();
//				if (expireDate != null && remindLeadTime != null) {
//					long day = (long) (remindLeadTime * 24 * 60 * 60 * 1000);
//					long time = expireDate.getTime();
//					time -= day;
//					srb.setRemindDate(new Date(time));
//				}
//				if (sii.getPrice() != null)
//					srb.setPurchasePrice(sii.getPrice());
//				if (sii.getNote() != null)
//					srb.setNote(sii.getNote());
//				if (sii.getNote2() != null)
//					srb.setNote2(sii.getNote2());
//				if (sii.getCostCenter() != null)
//					srb.setCostCenter(sii.getCostCenter());
//				srb.setIsGood(SystemConstants.DIC_STATE_NO_ID);
//				if (aStorage.getUnit() != null)
//					srb.setUnit(aStorage.getUnit());
//				if (sii.getPosition() != null)
//					srb.setPosition(sii.getPosition());
//				
//				srb.setRegionType(sii.getStorageIn().getRegionType());
//				// srb.setSupplier(em.getStorageIn().getPurchaseOrder().getSupplier());
//				srb.setStorageInItem(sii);
//				srb.setQcState("未QC");
//				commonDAO.saveOrUpdate(srb);
//			}
//		} else {
		List<StorageInItem> list = showStorageInItemList(formId);
		int addNum = 0;
		for (StorageInItem em : list) {
			Storage aStorage = commonDAO.get(em.getStorage().getClass(), em.getStorage().getId());
			Double num = 0.0;
			Double oldNum = 0.0;
			oldNum = aStorage.getNum();
			if (aStorage.getNum() != null)
				num = aStorage.getNum();// 原库存数量
			if (em.getNum() != null) {
				num = num + em.getNum();// 新库存数量
				aStorage.setNum(num);

			}

			// 更新在途数量
//				Double onWayNum = 0.0;
//				Double oldOnWayNum = 0.0;
//
//				if (aStorage.getOnWayNum() != null)
//					oldOnWayNum = aStorage.getOnWayNum();// 原在途数量
//				if (em.getNum() != null && oldOnWayNum != 0.0) {
//					onWayNum = oldOnWayNum - em.getNum();// 新在途数量
//					aStorage.setOnWayNum(onWayNum);
//
//				}

			commonDAO.saveOrUpdate(aStorage);

			// 假如有批次
			if (aStorage.getType().getId().startsWith(SystemConstants.DIC_STORAGE_SHIYANCAILIAO_YPC)) {

//					String code = systemCodeService.getSystemCodeByNumber(
//							SystemCode.STORAGE_ITEM_NAME, addNum);
//					String markCode = "SJMX";
//					String code = codingRuleService.genTransID(
//							"StorageReagentBuySerial", markCode);
				addNum++;

				StorageReagentBuySerial srb = new StorageReagentBuySerial();
				if(em.getSerial()!=null
						&&!"".equals(em.getSerial())
						&&em.getStorage()!=null) {
					List<StorageReagentBuySerial> srbs = storageInDao.getStoragentBuySerials(em.getSerial(), em.getStorage().getId());
					if(srbs.size()>0) {
						srb = srbs.get(0);
//						srb.setId(code);
						if (aStorage.getCurrencyType() != null)
							srb.setCurrencyType(aStorage.getCurrencyType());
						srb.setStorage(aStorage);
						if (em.getNum() != null) {
							if(srb.getNum()!=null) {
								srb.setNum(srb.getNum()+em.getNum());
							}else {
								srb.setNum(em.getNum());
							}
						}
						srb.setInDate(new Date());
						if (em.getProductDate() != null)
							srb.setProductDate(em.getProductDate());

						srb.setSerial(em.getSerial());
						srb.setCode(em.getCode());
						srb.setSpec(em.getSpec());
						SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						StorageIn aStorageIn = commonDAO.get(em.getStorageIn().getClass(), em.getStorageIn().getId());

						Date date = aStorageIn.getHandelDate();
						Integer durableDays = aStorage.getDurableDays();
						// 入库日期
						srb.setInDate(date);

//							if (date != null && durableDays != null) {
//								long day = (long) (durableDays * 24 * 60 * 60 * 1000);
//								long time = date.getTime();
//								time += day;
//								srb.setInDate(new Date(time));
//								
////								srb.setExpireDate(new Date(time));
//							} else {
//								if (em.getExpireDate() != null)
//									srb.setExpireDate(format.parse(em.getExpireDate()));
//							}

						// 失效日期
						Date expireDate = srb.getExpireDate();

						srb.setExpireDate(expireDate);

						// 失效提前提醒日期
						Integer remindLeadTime = aStorage.getRemindLeadTime();
						if (expireDate != null && remindLeadTime != null) {
							long day = (long) (remindLeadTime * 24 * 60 * 60 * 1000);
							long time = expireDate.getTime();
							time -= day;
							// 提醒日期
							srb.setRemindDate(new Date(time));
						}

						if (em.getPrice() != null)
							srb.setPurchasePrice(em.getPrice());
						if (em.getNote() != null)
							srb.setNote(em.getNote());
						if (em.getNote2() != null)
							srb.setNote2(em.getNote2());
						if (em.getCostCenter() != null)
							srb.setCostCenter(em.getCostCenter());
						srb.setIsGood(SystemConstants.DIC_STATE_NO_ID);
						if (aStorage.getUnit() != null)
							srb.setUnit(aStorage.getUnit());
						if (em.getStorage().getPosition() != null)
							srb.setPosition(em.getStorage().getPosition());

						srb.setScopeId(si.getScopeId());
						srb.setScopeName(si.getScopeName());

						// if (em.getStorageIn().getPurchaseOrder() != null &&
						// em.getStorage() != null) {
						// PurchaseOrderItem poi =
						// findPurchaseOrderItem(em.getStorageIn().getPurchaseOrder().getId(),
						// em
						// .getStorage().getId());
						// if (poi.getPrice() != null)
						// srb.setPurchasePrice(poi.getPrice());
						// }
						srb.setRegionType(em.getStorageIn().getRegionType());
						// srb.setSupplier(em.getStorageIn().getPurchaseOrder().getSupplier());
						srb.setStorageInItem(em);
						srb.setQcState("0");
						if (null == srb.getId() || srb.getId().equals("")) {
							Random random = new Random();
							srb.setId((random.toString()));
							commonDAO.save(srb);
						} else {
							commonDAO.saveOrUpdate(srb);
						}
					}else {
//						srb.setId(code);
						if (aStorage.getCurrencyType() != null)
							srb.setCurrencyType(aStorage.getCurrencyType());
						srb.setStorage(aStorage);
						if (em.getNum() != null)
							srb.setNum(em.getNum());
						srb.setInDate(new Date());
						if (em.getProductDate() != null)
							srb.setProductDate(em.getProductDate());

						srb.setSerial(em.getSerial());
						srb.setCode(em.getCode());
						srb.setSpec(em.getSpec());
						SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm");
						StorageIn aStorageIn = commonDAO.get(em.getStorageIn().getClass(), em.getStorageIn().getId());

						Date date = aStorageIn.getHandelDate();
						Integer durableDays = aStorage.getDurableDays();
						// 入库日期
						srb.setInDate(date);

//							if (date != null && durableDays != null) {
//								long day = (long) (durableDays * 24 * 60 * 60 * 1000);
//								long time = date.getTime();
//								time += day;
//								srb.setInDate(new Date(time));
//								
////								srb.setExpireDate(new Date(time));
//							} else {
//								if (em.getExpireDate() != null)
//									srb.setExpireDate(format.parse(em.getExpireDate()));
//							}

						// 失效日期
						String exexpireDateTime = em.getExpireDate();
						if(exexpireDateTime!=null
								&&!"".equals(exexpireDateTime)) {
							
							Date expireDate = format.parse(exexpireDateTime);
							
							srb.setExpireDate(expireDate);
							
							// 失效提前提醒日期
							Integer remindLeadTime = aStorage.getRemindLeadTime();
							if (expireDate != null && remindLeadTime != null) {
								long day = (long) (remindLeadTime * 24 * 60 * 60 * 1000);
								long time = expireDate.getTime();
								time -= day;
								// 提醒日期
								srb.setRemindDate(new Date(time));
							}
						}

						

						if (em.getPrice() != null)
							srb.setPurchasePrice(em.getPrice());
						if (em.getNote() != null)
							srb.setNote(em.getNote());
						if (em.getNote2() != null)
							srb.setNote2(em.getNote2());
						if (em.getCostCenter() != null)
							srb.setCostCenter(em.getCostCenter());
						srb.setIsGood(SystemConstants.DIC_STATE_NO_ID);
						if (aStorage.getUnit() != null)
							srb.setUnit(aStorage.getUnit());
						if (em.getStorage().getPosition() != null)
							srb.setPosition(em.getStorage().getPosition());

						srb.setScopeId(si.getScopeId());
						srb.setScopeName(si.getScopeName());

						// if (em.getStorageIn().getPurchaseOrder() != null &&
						// em.getStorage() != null) {
						// PurchaseOrderItem poi =
						// findPurchaseOrderItem(em.getStorageIn().getPurchaseOrder().getId(),
						// em
						// .getStorage().getId());
						// if (poi.getPrice() != null)
						// srb.setPurchasePrice(poi.getPrice());
						// }
						srb.setRegionType(em.getStorageIn().getRegionType());
						// srb.setSupplier(em.getStorageIn().getPurchaseOrder().getSupplier());
						srb.setStorageInItem(em);
						srb.setQcState("0");
						if (null == srb.getId() || srb.getId().equals("")) {
							Random random = new Random();
							srb.setId((random.toString()));
							commonDAO.save(srb);
						} else {
							commonDAO.saveOrUpdate(srb);
						}
					}
				}else {
//					srb.setId(code);
					if (aStorage.getCurrencyType() != null)
						srb.setCurrencyType(aStorage.getCurrencyType());
					srb.setStorage(aStorage);
					if (em.getNum() != null)
						srb.setNum(em.getNum());
					srb.setInDate(new Date());
					if (em.getProductDate() != null)
						srb.setProductDate(em.getProductDate());
	
					srb.setSerial(em.getSerial());
					srb.setCode(em.getCode());
					srb.setSpec(em.getSpec());
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm");
					StorageIn aStorageIn = commonDAO.get(em.getStorageIn().getClass(), em.getStorageIn().getId());
	
					Date date = aStorageIn.getHandelDate();
					Integer durableDays = aStorage.getDurableDays();
					// 入库日期
					srb.setInDate(date);
	
	//					if (date != null && durableDays != null) {
	//						long day = (long) (durableDays * 24 * 60 * 60 * 1000);
	//						long time = date.getTime();
	//						time += day;
	//						srb.setInDate(new Date(time));
	//						
	////						srb.setExpireDate(new Date(time));
	//					} else {
	//						if (em.getExpireDate() != null)
	//							srb.setExpireDate(format.parse(em.getExpireDate()));
	//					}
	
					// 失效日期
					String exexpireDateTime = em.getExpireDate();
					if(exexpireDateTime!=null
							&&!"".equals(exexpireDateTime)) {
						
						Date expireDate = format.parse(exexpireDateTime);
						
						srb.setExpireDate(expireDate);
						
						// 失效提前提醒日期
						Integer remindLeadTime = aStorage.getRemindLeadTime();
						if (expireDate != null && remindLeadTime != null) {
							long day = (long) (remindLeadTime * 24 * 60 * 60 * 1000);
							long time = expireDate.getTime();
							time -= day;
							// 提醒日期
							srb.setRemindDate(new Date(time));
						}
					}
//					// 失效日期
//					Date expireDate = em.getExpireDate();
//	
//					srb.setExpireDate(expireDate);
//	
//					// 失效提前提醒日期
//					Integer remindLeadTime = aStorage.getRemindLeadTime();
//					if (expireDate != null && remindLeadTime != null) {
//						long day = (long) (remindLeadTime * 24 * 60 * 60 * 1000);
//						long time = expireDate.getTime();
//						time -= day;
//						// 提醒日期
//						srb.setRemindDate(new Date(time));
//					}
	
					if (em.getPrice() != null)
						srb.setPurchasePrice(em.getPrice());
					if (em.getNote() != null)
						srb.setNote(em.getNote());
					if (em.getNote2() != null)
						srb.setNote2(em.getNote2());
					if (em.getCostCenter() != null)
						srb.setCostCenter(em.getCostCenter());
					srb.setIsGood(SystemConstants.DIC_STATE_NO_ID);
					if (aStorage.getUnit() != null)
						srb.setUnit(aStorage.getUnit());
					if (em.getStorage().getPosition() != null)
						srb.setPosition(em.getStorage().getPosition());
	
					srb.setScopeId(si.getScopeId());
					srb.setScopeName(si.getScopeName());
	
					// if (em.getStorageIn().getPurchaseOrder() != null &&
					// em.getStorage() != null) {
					// PurchaseOrderItem poi =
					// findPurchaseOrderItem(em.getStorageIn().getPurchaseOrder().getId(),
					// em
					// .getStorage().getId());
					// if (poi.getPrice() != null)
					// srb.setPurchasePrice(poi.getPrice());
					// }
					srb.setRegionType(em.getStorageIn().getRegionType());
					// srb.setSupplier(em.getStorageIn().getPurchaseOrder().getSupplier());
					srb.setStorageInItem(em);
					srb.setQcState("0");
					if (null == srb.getId() || srb.getId().equals("")) {
						Random random = new Random();
						srb.setId((random.toString()));
						commonDAO.save(srb);
					} else {
						commonDAO.saveOrUpdate(srb);
					}
				}
			}

		}

		si.setState("1");
		si.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_COMPLETE_NAME);
		commonDAO.saveOrUpdate(si);
		commonSetState(applicationTypeActionId, formId, SystemConstants.DIC_STATE_YES);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveStorageInItemTable(String id, String item, String logInfo) throws Exception {
		// 将json读入map
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(item, List.class);
		StorageIn ep = commonDAO.get(StorageIn.class, id);

		for (Map<String, Object> map : list) {
			StorageInItem em = new StorageInItem();

			// 将map信息读入实体类
			em = (StorageInItem) commonDAO.Map2Bean(map, em);
			if (em.getId() != null && em.getId().equals("")) {
				em.setId(null);
			}
//			if (em.getStorage() == null) {
//				Storage s = storageService.getStorageByName((String) map
//						.get("storage-name"));
//				em.setStorage(s);
//			}
			em.setStorageIn(ep);
			commonDAO.saveOrUpdate(em);
		}

		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			li.setUserId(ep.getHandelUser().getId());
			li.setFileId(ep.getId());
			li.setClassName("StorageIn");
			li.setModifyContent(logInfo);
			commonDAO.saveOrUpdate(li);
		}

	}

	public Map<String, Object> selStorageInItemTable(String scId, Integer start, Integer length, String query,
			String col, String sort) throws Exception {
		return storageInDao.findStorageInItemTable(scId, start, length, query, col, sort);
	}

	public Map<String, Object> selStorageInTable(Integer start, Integer length, String query, String col, String sort)
			throws Exception {
		return storageInDao.findStorageInTable(start, length, query, col, sort);
	}

	// 主数据内选择数据添加到入库明细表
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String addStorageInItem(String[] ids, String note, String handelDate, String storageInId, String createUser,
			String createDate) throws Exception {

		// 保存主表信息
		StorageIn si = new StorageIn();
		String log = "";
		if (storageInId == null || storageInId.length() <= 0 || "NEW".equals(storageInId)) {
			log="123";
			String code = systemCodeService.getCodeByPrefix("StorageIn",
					"MI" + DateUtil.dateFormatterByPattern(new Date(), "yyMMdd"), 000, 3, null);
			si.setId(code);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			User u = commonDAO.get(User.class, createUser);
			si.setHandelUser(u);
			si.setCreateDate(sdf.parse(createDate));
			si.setHandelDate(sdf.parse(handelDate));
			si.setState("3");
			si.setStateName("NEW");
			si.setNote(note);
			storageInId = si.getId();
			commonDAO.saveOrUpdate(si);
		} else {
			si = commonDAO.get(StorageIn.class, storageInId);
		}
		for (int i = 0; i < ids.length; i++) {
			String id = ids[i];
			// 通过id查询库存主数据
			Storage s = commonDAO.get(Storage.class, id);
			StorageInItem sii = new StorageInItem();
			sii.setStorage(s);
			sii.setStorageIn(si);
			if(s.getUnitGroup()!=null) {
				UnitGroupNew unitGroupNew = commonDAO.get(UnitGroupNew.class, s.getUnitGroup().getId());
				sii.setUnitGroupNew(unitGroupNew);

			}
			commonDAO.saveOrUpdate(sii);

			String kucun = "入库管理:" + "原辅料编号:" + s.getId() + "名称:" + s.getName() + "的数据已添加到明细";
			if (kucun != null && !"".equals(kucun)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setFileId(storageInId);
				li.setClassName("StorageIn");
				li.setModifyContent(kucun);
				if("123".equals(log)) {
					li.setState("1");
					li.setStateName("数据新增");
				}else {
					li.setState("3");
					li.setStateName("数据修改");
				}
				commonDAO.saveOrUpdate(li);
			}
		}
		return storageInId;

	}

	// 保存主表
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveStorageIn(StorageIn si) throws Exception {
		String code = systemCodeService.getCodeByPrefix("StorageIn",
				"MI" + DateUtil.dateFormatterByPattern(new Date(), "yyMMdd"), 000, 3, null);
		si.setId(code);
		commonDAO.saveOrUpdate(si);
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveStorageInAndItem(StorageIn si, Map jsonMap, String logInfo, String logInfoItem) throws Exception {
		if (si != null) {
			if (logInfo != null && !"".equals(logInfo)) {
				LogInfo li = new LogInfo();
				li.setLogDate(new Date());
				User u = (User) ServletActionContext.getRequest().getSession()
						.getAttribute(SystemConstants.USER_SESSION_KEY);
				li.setUserId(u.getId());
				li.setClassName("StorageIn");
				li.setFileId(si.getId());
				li.setModifyContent(logInfo);
				li.setState("3");
				li.setStateName("数据修改");
				commonDAO.saveOrUpdate(li);
			}

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("ImteJson");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveItem(si, jsonStr, logInfoItem);
			}
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveItem(StorageIn si, String itemDataJson, String logInfo) throws Exception {
		List<StorageInItem> saveItems = new ArrayList<StorageInItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			StorageInItem scp = new StorageInItem();
			// 将map信息读入实体类
			scp = (StorageInItem) commonDAO.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setStorageIn(si);

			if (scp.getNum() == null || "".equals(scp.getNum())) {
				if (scp.getPackingNum() != null && !"".equals(scp.getPackingNum()) && scp.getUnitGroupNew() != null) {
					UnitGroupNew ugn = commonDAO.get(UnitGroupNew.class, scp.getUnitGroupNew().getId());
					if(ugn!=null) {
						BigDecimal num1 = new BigDecimal(String.valueOf(scp.getPackingNum()));
						BigDecimal num2 = new BigDecimal(String.valueOf(ugn.getCycle()));
						BigDecimal num3 = num1.multiply(num2);
						scp.setNum(num3.doubleValue());	
					}
					
				}
			}

			saveItems.add(scp);
		}
		commonDAO.saveOrUpdateAll(saveItems);
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setClassName("StorageIn");
			li.setFileId(si.getId());
			li.setModifyContent(logInfo);
			li.setState("3");
			li.setStateName("数据修改");
			commonDAO.saveOrUpdate(li);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public StorageIn saveStorageInById(StorageIn si, String id, String createUser, String createDate, String logInfo)
			throws Exception {

		StorageIn newSi = new StorageIn();
		// 若主表未保存则保存主表
		String log = "";
		if (id == null || id.length() <= 0 || "NEW".equals(id)) {
			log="123";
			String code = systemCodeService.getCodeByPrefix("StorageIn",
					"MI" + DateUtil.dateFormatterByPattern(new Date(), "yyMMdd"), 000, 3, null);
			si.setId(code);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			User u = commonDAO.get(User.class, createUser);
			si.setHandelUser(u);
			si.setCreateDate(sdf.parse(createDate));
			si.setHandelDate(si.getHandelDate());
			si.setState("3");
			si.setStateName("新建");
			si.setNote(si.getNote());
			si.setDepartmnet(si.getDepartmnet());
			commonDAO.saveOrUpdate(si);

			newSi = si;

		} else {
			StorageIn sii = commonDAO.get(StorageIn.class, id);
			sii.setNote(si.getNote());
			sii.setHandelDate(si.getHandelDate());
			sii.setDepartmnet(si.getDepartmnet());
			newSi = sii;
		}
		if (logInfo != null && !"".equals(logInfo)) {
			LogInfo li = new LogInfo();
			li.setLogDate(new Date());
			User u = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			li.setUserId(u.getId());
			li.setClassName("StorageIn");
			li.setFileId(newSi.getId());
			li.setModifyContent(logInfo);
			if("123".equals(log)) {
				li.setState("1");
				li.setStateName("数据新增");
			}else {
				li.setState("3");
				li.setStateName("数据修改");
			}
			commonDAO.saveOrUpdate(li);
		}
		return newSi;
	}

	// public PurchaseOrderItem findPurchaseOrderItem(String orderId, String
	// storageId) throws Exception {
	// String[] a = { orderId, storageId };
	// // List<PurchaseOrderItem> list = commonDAO.find(
	// // "from PurchaseOrderItem where purchaseOrder.id=? and storage.id=?",
	// (Object[]) a);
	// // return (list.size() > 0 ? (PurchaseOrderItem) list.get(0) : null);
	//
	// }

	/**
	 * 从U8获取出库单，转成入库单
	 * 
	 * @throws OpenAPIException
	 * @throws ParseException
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public String getMaterialoutService(String id) throws OpenAPIException, ParseException {

		String z = "";

		User u = (User) ServletActionContext.getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		User user = commonDAO.get(User.class, "admin");
		DicStorageType dst = commonDAO.get(DicStorageType.class, "12");
		DicState dsd = commonDAO.get(DicState.class, "1s");

		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("system.properties");
		Properties prop = new Properties();
		try {
			prop.load(is);
		} catch (IOException e) {
			e.printStackTrace();
		}
		String to_account = prop.getProperty("to_account");
		MaterialoutService ds = new MaterialoutService();
		com.alibaba.fastjson.JSONObject record = ds.get(id, to_account);
		if ("0".equals(record.get("errcode"))) {
			Object objArray = (Object) record.get("materialout");
			String str = "[" + objArray + "]";
			// 方式1:转换成JSONArray对象形式
			JSONArray list = JSON.parseArray(str);
			if (list.size() > 0) {
				for (int i = 0; i < list.size(); i++) {
					com.alibaba.fastjson.JSONObject map = JSON.parseObject(list.get(i) + "");
//			List<com.alibaba.fastjson.JSONObject> list = (List<com.alibaba.fastjson.JSONObject>) record.get("materialout");
//			if(list.size()>0) {
//				for(com.alibaba.fastjson.JSONObject map:list) {
					StorageIn sts = commonDAO.get(StorageIn.class, (String) map.get("code"));
					if (sts != null) {

					} else {
						StorageIn dtt = new StorageIn();
						dtt.setId((String) map.get("code"));
						if (map.get("receivename") != null) {
							dtt.setNote((String) map.get("receivename"));
						}
						dtt.setHandelUser(u);
						dtt.setCreateDate(new Date());
						dtt.setState("3");
						dtt.setStateName("新建");
						dtt.setHandelDate(new Date());
						if (map.get("departmentcode") != null) {
							String departmnet = (String) map.get("departmentcode");
							Department dd = commonDAO.get(Department.class, departmnet);
							if (dd != null) {
								dtt.setDepartmnet(dd);
							} else {
								DepartmentU8Service dss = new DepartmentU8Service();
								com.alibaba.fastjson.JSONObject recordde = null;
								recordde = dss.get(departmnet, to_account);
								if ("0".equals(recordde.get("errcode"))) {
									Department dt1 = new Department();
									dt1.setId(departmnet);
									if (map.get("name") != null) {
										dt1.setBriefName((String) map.get("name"));
										dt1.setName((String) map.get("name"));
									}
									if (map.get("remark") != null) {
										dtt.setNote((String) map.get("remark"));
									}
									if (map.get("ddependdate") != null) {
										if (compare_date((String) map.get("ddependdate"), new Date()) == 1) {
											dtt.setState("1");
										} else {
											dtt.setState("0");
										}
									}
									commonDAO.saveOrUpdate(dt1);
									dtt.setDepartmnet(dt1);
								}
							}
						}

						commonDAO.saveOrUpdate(dtt);
						Object objArray1 = (Object) map.get("entry");
						String str1 = "" + objArray1 + "";
						// 方式1:转换成JSONArray对象形式
						JSONArray itemlist = JSON.parseArray(str1);
//						List<Map<String, Object>> itemlist = (List<Map<String, Object>>) map.get("entry");
						if (itemlist.size() > 0) {
							for (int zz = 0; zz < itemlist.size(); zz++) {
//							for(Map<String,Object> itemmap:itemlist) {
								com.alibaba.fastjson.JSONObject itemmap = JSON.parseObject(itemlist.get(zz) + "");
								StorageInItem sii = new StorageInItem();
								sii.setId(null);
								if (itemmap.get("inventorycode") != null) {
									Storage st = commonDAO.get(Storage.class, (String) itemmap.get("inventorycode"));
									if (st != null) {
										sii.setStorage(st);
									} else {
										InventoryService dsa = new InventoryService();
										com.alibaba.fastjson.JSONObject recorda = dsa
												.get((String) itemmap.get("inventorycode"), to_account);
										if ("0".equals(recorda.get("errcode"))) {
											com.alibaba.fastjson.JSONObject mapa = (com.alibaba.fastjson.JSONObject) recorda
													.get("inventory");
											// code string 存货编码
											Storage stsq = commonDAO.get(Storage.class, (String) mapa.get("code"));
											if (stsq != null) {
												sii.setStorage(stsq);
											} else {
												Storage st1 = new Storage();
												st1.setCreateDate(new Date());
												st1.setCreateUser(user);
												st1.setType(dst);
												st1.setState(dsd);
												st1.setId((String) mapa.get("code"));
												// name string 存货名称
												if (mapa.get("name") != null) {
													st1.setName((String) mapa.get("name"));
												}
												// invaddcode string 存货代码
												// specs string 规格型号
												if (mapa.get("specs") != null) {
													st1.setSpec((String) mapa.get("specs"));
												}
												// sort_code string 所属分类码
												if (mapa.get("sort_code") != null) {
													List<DicType> dts = commonService.get(DicType.class, "code",
															(String) mapa.get("sort_code"));
													if (dts.size() > 0) {
														st1.setStudyType(dts.get(0));
													} else {
														DicType dt = new DicType();
														dt.setId(null);
														dt.setCode((String) mapa.get("sort_code"));
														if (mapa.get("sort_name") != null) {
															dt.setName((String) mapa.get("sort_name"));
														}
														DicMainType dmt = commonDAO.get(DicMainType.class, "yjlx");
														if (dmt != null) {
															dt.setType(dmt);
														}
														dt.setState("1");
														commonDAO.saveOrUpdate(dt);
														st1.setStudyType(dt);
													}
												}
												// main_measure string 主计量单位
												if (mapa.get("main_measure") != null) {
													UnitGroupNew dts = commonDAO.get(UnitGroupNew.class,
															(String) mapa.get("main_measure"));
													if (dts != null) {
														st1.setUnitGroup(dts);
													} else {
														UnitGroupNew dt = new UnitGroupNew();
														dt.setId((String) mapa.get("main_measure"));
														dt.setCycle(1);
														dt.setState("1");
														if (mapa.get("ccomunitname") != null) {
															dt.setName((String) mapa.get("ccomunitname"));
															List<DicUnit> du = commonService.get(DicUnit.class, "name",
																	(String) mapa.get("ccomunitname"));
															if (du.size() > 0) {
																dt.setMark(du.get(0));
																dt.setMark2(du.get(0));
															} else {
																DicUnit duu = new DicUnit();
																duu.setId(null);
																duu.setName((String) mapa.get("ccomunitname"));
																commonDAO.saveOrUpdate(duu);
																dt.setMark(duu);
																dt.setMark2(duu);
															}
														}
														commonDAO.saveOrUpdate(dt);
														st1.setUnitGroup(dt);
													}
												}
												commonDAO.saveOrUpdate(st1);
												sii.setStorage(st1);
											}

										}
									}
								}
								SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
								if (itemmap.get("makedate") != null) {
									sii.setProductDate(formatter.parse((String) itemmap.get("makedate")));
								}
								if (itemmap.get("serial") != null) {
									sii.setSerial((String) itemmap.get("serial"));
								}
								if (itemmap.get("expireDate") != null) {
									sii.setExpireDate((String) itemmap.get("expireDate"));
								}
								if (itemmap.get("cmassunitname") != null) {
									List<UnitGroupNew> dts = commonService.get(UnitGroupNew.class, "name",
											(String) map.get("main_measure"));
									if (dts.size() > 0) {
										sii.setUnitGroupNew(dts.get(0));
									} else {

									}
								}
//								sii.setUnitGroupNew(unitGroupNew);cmassunitname
								if (itemmap.get("quantity") != null) {
									String qu = (String) itemmap.get("quantity");
									if (qu != null) {
										sii.setNum(Double.valueOf(qu));
									}
								}
								sii.setStorageIn(dtt);
								commonDAO.saveOrUpdate(sii);
							}
						}

//						errcode	string	 	错误码，0 为正常。
//						errmsg	string	 	错误信息。
//						receiveflag	number		收发标志
//						vouchtype	string		单据类型编码
//						businesstype	string		业务类型
//						source	string		单据来源
//						cmpocode	string		生产订单号
//						serial	string		生产批号
//						businesscode	string		对应业务单号
//						warehousecode	string		仓库编码
//						warehousename	string		仓库名称
//						date	string		单据日期
//						code	string		出库单号
//						receivecode	string		收发类别编码
//						receivename	string		收发类别名称
//						departmentcode	string		部门编码
//						departmentname	string		部门名称
//						personcode	string		业务员编码
//						personname	string		业务员名称
//						vendorcode	string		供应商编码
//						vendorabbname	string		供应商简称
//						quantity	number		产量
//						arrivedate	string		到货日期
//						checkcode	string		检验单号
//						checkdate	string		检验日期
//						checkperson	string		检验员
//						templatenumber	number		单据模版号
//						handler	string		审核人
//						memory	string		备注
//						maker	string		制单人
//						define1	string		自定义项1
//						define2	string		自定义项2
//						define3	string		自定义项3
//						define4	date		自定义项4
//						define5	number		自定义项5
//						define6	date		自定义项6
//						define7	number		自定义项7
//						define8	string		自定义项8
//						define9	string		自定义项9
//						define10	string		自定义项10
//						define11	string		自定义项11
//						define12	string		自定义项12
//						define13	string		自定义项13
//						define14	string		自定义项14
//						define15	number		自定义项15
//						define16	number		自定义项16
//						auditdate	string		审核日期
//						id	number		收发记录主表标识
//						id	number	entry	收发记录主表标识
//						barcode	string	entry	条形码
//						inventorycode	string	entry	存货编码
//						free1	string	entry	存货自由项1
//						free2	string	entry	存货自由项2
//						free3	string	entry	存货自由项3
//						free4	string	entry	存货自由项4
//						free5	string	entry	存货自由项5
//						free6	string	entry	存货自由项6
//						free7	string	entry	存货自由项7
//						free8	string	entry	存货自由项8
//						free9	string	entry	存货自由项9
//						free10	string	entry	存货自由项10
//						shouldquantity	number	entry	应发数量
//						shouldnumber	number	entry	应发件数
//						quantity	number	entry	数量
//						cmassunitname	string	entry	主计量单位
//						assitantunit	string	entry	库存单位码
//						assitantunitname	string	entry	库存单位
//						irate	number	entry	换算率
//						number	number	entry	件数
//						price	number	entry	单价
//						cost	number	entry	金额
//						plancost	number	entry	计划单价/售价
//						planprice	number	entry	计划金额/售价金额
//						serial	string	entry	批号
//						makedate	date	entry	生产日期
//						validdate	date	entry	失效日期
//						subbillcode	number	entry	发票子表ID
//						position	string	entry	货位编码
//						itemclasscode	string	entry	项目大类编码
//						itemclassname	string	entry	项目大类名称
//						itemcode	string	entry	项目编码
//						itemname	string	entry	项目
//						define22	string	entry	表体自定义项1
//						define23	string	entry	表体自定义项2
//						define24	string	entry	表体自定义项3
//						define25	string	entry	表体自定义项4
//						define26	number	entry	表体自定义项5
//						define27	number	entry	表体自定义项6
//						define28	string	entry	表体自定义项7
//						define29	string	entry	表体自定义项8
//						define30	string	entry	表体自定义项9
//						define31	string	entry	表体自定义项10
//						define32	string	entry	表体自定义项11
//						define33	string	entry	表体自定义项12
//						define34	number	entry	表体自定义项13
//						define35	number	entry	表体自定义项14
//						define36	date	entry	表体自定义项15
//						define37	date	entry	表体自定义项16
//						subconsignmentid	number	entry	发货单子表ID
//						delegateconsignmentid	number	entry	委托子表id
//						subcheckid	number	entry	检验单子表ID
//						iid	number	entry	子表标识
//						rowno	number	entry	行号
					}
				}
			}
			z = "1";
		} else {
			z = "";
		}
		return z;
	}

	public int compare_date(String DATE1, Date DATE2) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			Date dt1 = df.parse(DATE1);
			Date dt2 = df.parse(df.format(DATE2));
			if (dt1.getTime() > dt2.getTime()) {
				System.out.println("dt1 在dt2前");
				return 1;
			} else if (dt1.getTime() < dt2.getTime()) {
				System.out.println("dt1在dt2后");
				return -1;
			} else {
				return 0;
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return 0;
	}
}
