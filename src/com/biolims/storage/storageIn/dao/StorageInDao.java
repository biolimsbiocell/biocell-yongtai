/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：DAO
 * 类描述：入库管理
 * 创建人：倪毅
 * 创建时间：2012-04
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.storage.storageIn.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.experiment.dna.model.DnaTask;
import com.biolims.experiment.dna.model.DnaTaskItem;
import com.biolims.storage.common.bean.StorageApplyBean;
import com.biolims.storage.common.constants.SystemConstants;
import com.biolims.storage.model.StorageApply;
import com.biolims.storage.model.StorageApplyItem;
import com.biolims.storage.model.StorageIn;
import com.biolims.storage.model.StorageInItem;
import com.biolims.storage.model.StorageOut;
import com.biolims.storage.model.StorageOutItem;
import com.biolims.storage.model.StorageReagentBuySerial;
import com.opensymphony.xwork2.ActionContext;

@Repository
@SuppressWarnings("unchecked")
public class StorageInDao extends BaseHibernateDao {

	/**
	 * 领用申请列表
	 * 
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @param so
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectStorageOut(Integer startNum, Integer limitNum, String dir, String sort,
			StorageOut so) throws Exception {
		String key = "";
		if (so != null) {
			String id = so.getId();
			if (id != null)
				key = " and id='" + id + "'";
		}
		String hql = "from StorageOut where 1=1 " + key;
		if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0)
			hql = hql + " order by " + sort + " " + dir;

		List<StorageOut> list = this.getSession().createQuery(hql).setFirstResult(startNum).setMaxResults(limitNum)
				.list();
		hql = "select count(id) from StorageOut where 1=1 " + key;
		Long count = (Long) this.getSession().createQuery(hql).uniqueResult();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("count", count);
		map.put("result", list);
		return map;

	}

	/**
	 * 查询所有领用申请列表
	 * 
	 * @param startNum
	 * @param limitNum
	 * @return
	 */
	public Map<String, Object> selectAllStorageApply(Integer startNum, Integer limitNum) {
		String hql = "from StorageApply where 1=1 ";
		List<StorageApply> list = this.getSession().createQuery(hql).setFirstResult(startNum).setMaxResults(limitNum)
				.list();
		hql = "select count(id) from StorageApply where 1=1 ";
		Long count = (Long) this.getSession().createQuery(hql).uniqueResult();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("count", count);
		map.put("result", list);
		return map;
	}

	/**
	 * 根据领用申请查询明细对象
	 * 
	 * @param applyId
	 * @return
	 * @throws Exception
	 */
	public StorageApplyItem selectStorageApplyItemByApplyId(String applyId) throws Exception {
		String hql = "from StorageApplyItem where storageApply.id='" + applyId + "'";
		StorageApplyItem sai = (StorageApplyItem) this.getSession().createQuery(hql).uniqueResult();
		return sai;
	}

	/**
	 * 插入/更新出库申请
	 * 
	 * @param so
	 * @throws Exception
	 */
	public void insertOrUpdateStorageOut(StorageOut so) throws Exception {
		this.saveOrUpdate(so);
	}

	/**
	 * 领用明细对象
	 * 
	 * @param applyId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 */
	public Map<String, Object> selectStorageApplyItemList(String objType, String storageId, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String hql = "";
		String key = "";
		if (SystemConstants.DIC_STORAGE_TYPE_HAOCAI_QIJU.equals(objType)) {
			// 器具
			key = "select new com.biolims.storage.out.bean.StorageApplyBean(storageId.name,storageId.id,storageId.searchCode,'',outPrice,currencyType.name,storageId.position.name) ";
			hql = "from StorageImplement where storageId.id='" + storageId + "'";
		} else if (SystemConstants.DIC_STORAGE_TYPE_HAOCAI_CGSJ.equals(objType)) {
			// 采购试剂
			key = "select new com.biolims.storage.out.bean.StorageApplyBean(storageId.name,storageId.id,storageId.searchCode,unit.name,outPrice,currencyType.name,storageId.position.name) ";
			hql = "from StorageReagentBuySerial where storageReagentBuy.storageId.id='" + storageId + "'";
		}

		List<StorageApplyBean> list = this.getSession().createQuery(key + hql).setFirstResult(startNum).setMaxResults(
				limitNum).list();
		hql = "select count(id)" + hql;
		Long count = (Long) this.getSession().createQuery(hql).uniqueResult();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("count", count);
		map.put("result", list);
		return map;
	}

	/**
	 * 出库明细对象
	 * 
	 * @param outId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 */
	public Map<String, Object> selectStorageOutItemList(String outId, Integer startNum, Integer limitNum, String dir,
			String sort) {
		String hql = "from StorageOutItem where storageOut.id='" + outId + "'";
		if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0)
			hql = hql + " order by " + sort + " " + dir;

		List<StorageOutItem> list = this.getSession().createQuery(hql).setFirstResult(startNum).setMaxResults(limitNum)
				.list();
		hql = "select count(id) from StorageOutItem where storageOut.id='" + outId + "'";
		Long count = (Long) this.getSession().createQuery(hql).uniqueResult();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("count", count);
		map.put("result", list);
		return map;
	}

	/**
	 * 出库明细对象
	 * 
	 * @param outId
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 */
	public List<StorageOutItem> findStorageOutItemList(String outId) {
		String hql = "from StorageOutItem where storageOut.id='" + outId + "'";

		List<StorageOutItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	/**
	 * 根据出库申请ID和对象ID查询出库申请明细对象
	 * 
	 * @param outId
	 * @param storageId
	 * @return
	 * @throws Exception
	 */
	public StorageOutItem selectStorageOutItemByOutIdAndStorageId(String outId, String storageId) throws Exception {
		String hql = "from StorageOutItem where storageOut.id='" + outId + "' and storage.id='" + storageId + "'";
		StorageOutItem soi = (StorageOutItem) this.getSession().createQuery(hql).uniqueResult();
		return soi;
	}

	/**
	 * 删除指定申请下的所有明细
	 * @param outId
	 * @throws Exception
	 */
	public void delStorageOutItemByOutId(String outId) throws Exception {
		String hql = "delete from StorageOutItem where storageOut.id='" + outId + "'";
		this.getSession().createQuery(hql).executeUpdate();
	}

	public Double getStorageInSum(String orderId, String storageId) throws Exception {
		String hql = "from StorageInItem where storageIn.purchaseOrder.id = '" + orderId + "' and storageIn.state = '"
				+ SystemConstants.DIC_STATE_YES_ID + "' and storage.id='" + storageId + "'";

		hql = "select sum(num) " + hql;
		Double sum = (Double) this.getSession().createQuery(hql).uniqueResult();

		return sum;
	}

	public Double getPurchaseCancelSum(String orderId, String storageId) throws Exception {
		String hql = "from PurchaseCancelItem where purchaseCancel.purchaseOrder.id = '" + orderId
				+ "' and purchaseCancel.state = '" + SystemConstants.DIC_STATE_YES_ID + "' and storage.id='"
				+ storageId + "'";
		hql = "select sum(num) " + hql;
		Double sum = (Double) this.getSession().createQuery(hql).uniqueResult();
		return sum;
	}

	public Double selectStorageReagentBuyCount(String storageId, String storageInItemId) throws Exception {

		String hql = "select sum(num) from StorageReagentBuySerial where storage.id='" + storageId
				+ "' and storageInItem.id ='" + storageInItemId + "'";
		Double srb = (Double) this.getSession().createQuery(hql).uniqueResult();
		return srb;
	}

	public Map<String, Object> findStorageInItemTable(String id,
			Integer start, Integer length, String query, String col, String sort) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from StorageInItem where 1=1 and storageIn.id='"+id+"'";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from StorageInItem  where 1=1  and storageIn.id='"+id+"'";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<StorageInItem> list =this.getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
		}

	public Map<String, Object> findStorageInTable(Integer start,
			Integer length, String query, String col, String sort) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		String countHql = "select count(*) from StorageIn where 1=1";
		String key = "";
		if(query!=null&&!"".equals(query)){
			key=map2Where(query);
		}
		String scopeId=(String) ActionContext.getContext().getSession().get("scopeId");
		if(!"all".equals(scopeId)){
			key+=" and scopeId='"+scopeId+"'";
		}
		
		Long sumCount = (Long) getSession().createQuery(countHql).uniqueResult();
		if(0l!= sumCount){
			Long filterCount = (Long) getSession().createQuery(countHql+key).uniqueResult();
			String hql = "from StorageIn where 1=1";
			if(col!=null&&!"".equals(col)&&!"".equals(sort)&&sort!=null){
				col=col.replace("-", ".");
				key+=" order by "+col+" "+sort;
			}
			List<StorageIn> list = getSession().createQuery(hql+key).setFirstResult(start).setMaxResults(length).list();
			map.put("recordsTotal", sumCount);
			map.put("recordsFiltered", filterCount);
			map.put("list", list);
			}
		return map;
	}

	public List<StorageReagentBuySerial> getStoragentBuySerials(String serial, String id) {
		String hql = "from StorageReagentBuySerial where serial='"+serial+"' and  storage.id='" + id + "'";

		List<StorageReagentBuySerial> list = this.getSession().createQuery(hql).list();
		return list;
	}

}
