package com.biolims.storage.storageIn.item.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.storage.model.Storage;
import com.biolims.storage.model.StorageInItem;
import com.biolims.storage.storageIn.item.dao.StorageInInfoDao;
import com.biolims.storage.storageIn.item.model.StorageInInfo;
import com.biolims.storage.storageIn.item.model.StorageInInfoItem;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class StorageInInfoService {
	@Resource
	private StorageInInfoDao storageInInfoDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private CodingRuleService codingRuleService;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findStorageInInfoList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return storageInInfoDao.selectStorageInInfoList(mapForQuery, startNum,
				limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(StorageInInfo i) throws Exception {

		storageInInfoDao.saveOrUpdate(i);

	}

	public StorageInInfo get(String id) {
		StorageInInfo storageInInfo = commonDAO.get(StorageInInfo.class, id);
		return storageInInfo;
	}

	public Map<String, Object> findStorageInInfoItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String serial, String storageInId) throws Exception {
		Map<String, Object> result = storageInInfoDao
				.selectStorageInInfoItemList(scId, startNum, limitNum, dir,
						sort, serial, storageInId);
		List<StorageInInfoItem> list = (List<StorageInInfoItem>) result
				.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveStorageInInfoItem(StorageInInfo sc, String itemDataJson)
			throws Exception {
		List<StorageInInfoItem> saveItems = new ArrayList<StorageInInfoItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			StorageInInfoItem scp = new StorageInInfoItem();
			// 将map信息读入实体类
			scp = (StorageInInfoItem) storageInInfoDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setStorageInInfo(sc);

			saveItems.add(scp);
		}
		storageInInfoDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delStorageInInfoItem(String[] ids) throws Exception {
		for (String id : ids) {
			StorageInInfoItem scp = storageInInfoDao.get(
					StorageInInfoItem.class, id);
			storageInInfoDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(StorageInInfo sc, Map jsonMap) throws Exception {
		if (sc != null) {
			storageInInfoDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("storageInInfoItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveStorageInInfoItem(sc, jsonStr);
			}
		}
	}

	public void saveItem(Storage s, StorageInItem si,
			StorageInInfo storageInInfo) throws Exception {
		storageInInfoDao.saveOrUpdate(storageInInfo);
		String codes = "";
		double b = si.getNum();
		int num = (int) b;
		boolean flg = false;
		// 根据试剂编号和批次号查询
		List<StorageInInfoItem> list = this.storageInInfoDao.getListItem(
				storageInInfo.getId(), si.getSerial(), si.getStorageIn()
						.getId());
		// 试剂组分信息数量等于添加批次数量 不生成
		if (list.size() == num) {
			flg = false;
		} else if (list.size() > num) {// 试剂组分信息数量大于添加批次数量 删除多余的组分sn号
			flg = false;
			for (int i = 0; i < list.size(); i++) {
				if (list.size() - num > i) {
					storageInInfoDao.delete(list.get(i));
				} else {
					codes += list.get(i).getCode() + ",";
				}
			}
			si.setNote(codes);
			this.commonDAO.saveOrUpdate(si);
		} else if (list.size() < num) {// 试剂组分信息数量小于添加批次数量 生产信息
			flg = true;
		}
		// if (list.size() > 0) {
		// for (StorageInInfoItem sii : list) {
		// CharSequence c = sii.getCode();
		// if (si.getNote() == null || "".equals(si.getNote())) {
		// flg = false;
		// } else {
		// flg = si.getNote().contains(c);
		// }
		//
		// }
		// } else {
		// flg = false;
		// }
		if (flg) {
			for (int i = 0; i < num - list.size(); i++) {
				StorageInInfoItem item = new StorageInInfoItem();
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				String code = codingRuleService.get("code",
						"StorageInInfoItem", "R", 0000, 4, null);
				codes += code + ",";
				item.setCode(code);
				item.setSpec(s.getSpec());
				item.setJdeCode(s.getJdeCode());
				item.setSerial(si.getSerial());
				item.setExpireDate(format.parse(si.getExpireDate()));
				item.setStorageInId(si.getStorageIn().getId());
				if (s.getReactionNum() != null) {
					item.setReactionNum(s.getReactionNum().toString());
				}
				item.setQcPass(si.getQcPass());
				item.setPosition(si.getPosition());
				item.setStorageInInfo(storageInInfo);
				this.commonDAO.saveOrUpdate(item);
			}
			if (si.getNote() != null)
				si.setNote(si.getNote() + codes);
			else
				si.setNote(codes);
			this.commonDAO.saveOrUpdate(si);
		}

	}
}
