package com.biolims.storage.storageIn.item.action;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.file.service.FileInfoService;
import com.biolims.storage.model.Storage;
import com.biolims.storage.model.StorageInItem;
import com.biolims.storage.storageIn.item.model.StorageInInfo;
import com.biolims.storage.storageIn.item.model.StorageInInfoItem;
import com.biolims.storage.storageIn.item.service.StorageInInfoService;
import com.biolims.system.syscode.model.CodeMain;
import com.biolims.system.syscode.service.CodeMainService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/storage/storageIn/item/storageInInfo")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class StorageInInfoAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "3031";
	@Autowired
	private StorageInInfoService storageInInfoService;
	private StorageInInfo storageInInfo = new StorageInInfo();
	@Resource
	private FileInfoService fileInfoService;
	@Autowired
	private CommonService commonService;
	@Resource
	private CodeMainService codeMainService;

	@Action(value = "showStorageInInfoList")
	public String showStorageInInfoList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/storage/storageIn/item/storageInInfo.jsp");
	}

	@Action(value = "showStorageInInfoListJson")
	public void showStorageInInfoListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = storageInInfoService
				.findStorageInInfoList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<StorageInInfo> list = (List<StorageInInfo>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("kit-id", "");
		map.put("kit-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "storageInInfoSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogStorageInInfoList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/storage/storageIn/item/storageInInfoDialog.jsp");
	}

	@Action(value = "showDialogStorageInInfoListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogStorageInInfoListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = storageInInfoService
				.findStorageInInfoList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<StorageInInfo> list = (List<StorageInInfo>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("kit-id", "");
		map.put("kit-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editStorageInInfo", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String editStorageInInfo() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			storageInInfo = storageInInfoService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "storageInInfo");
		} else {
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			storageInInfo.setCreateUser(user);
			storageInInfo.setCreateDate(new Date());
			storageInInfo.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			storageInInfo.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/storage/storageIn/item/storageInInfoEdit.jsp");
	}

	@Action(value = "copyStorageInInfo", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String copyStorageInInfo() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		storageInInfo = storageInInfoService.get(id);
		storageInInfo.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/storage/storageIn/item/storageInInfoEdit.jsp");
	}

	@Action(value = "save", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String save() throws Exception {
		String id = storageInInfo.getId();
		if (id != null && id.equals("")) {
			storageInInfo.setId(null);
		}
		Map aMap = new HashMap();
		aMap.put("storageInInfoItem",
				getParameterFromRequest("storageInInfoItemJson"));

		storageInInfoService.save(storageInInfo, aMap);
		return redirect("/storage/storageIn/item/storageInInfo/editStorageInInfo.action?id="
				+ storageInInfo.getId());

	}

	@Action(value = "viewStorageInInfo")
	public String toViewStorageInInfo() throws Exception {
		String id = getParameterFromRequest("id");
		storageInInfo = storageInInfoService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/storage/storageIn/item/storageInInfoEdit.jsp");
	}

	@Action(value = "showStorageInInfoItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showStorageInInfoItemList() throws Exception {
		return dispatcher("/WEB-INF/page/storage/storageIn/item/storageInInfoItem.jsp");
	}

	@Action(value = "showStorageInInfoItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showStorageInInfoItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			String serial = getRequest().getParameter("serial");// 批次号
			String storageInId = getRequest().getParameter("storageInId");// 入库单号
			Map<String, Object> result = storageInInfoService
					.findStorageInInfoItemList(scId, startNum, limitNum, dir,
							sort, serial, storageInId);
			Long total = (Long) result.get("total");
			List<StorageInInfoItem> list = (List<StorageInInfoItem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("spec", "");
			map.put("jdeCode", "");
			map.put("serial", "");
			map.put("expireDate", "yyyy-MM-dd");
			map.put("reactionNum", "");
			map.put("qcPass", "");
			map.put("position-name", "");
			map.put("position-id", "");
			map.put("note", "");
			map.put("storageInInfo-name", "");
			map.put("storageInInfo-id", "");
			map.put("storageInId", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delStorageInInfoItem")
	public void delStorageInInfoItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			storageInInfoService.delStorageInInfoItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "newStorageInInfo", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String newStorageInInfo() throws Exception {
		String id = getParameterFromRequest("id");// 入库子表id
		String sid = getParameterFromRequest("sid");// 入库组分id
		long num = 0;
		Storage s = this.commonService.get(Storage.class, sid);
		StorageInItem si = this.commonService.get(StorageInItem.class, id);
		User user = (User) this
				.getObjFromSession(SystemConstants.USER_SESSION_KEY);
		storageInInfo.setId(s.getId());
		storageInInfo.setName(s.getName());
		storageInInfo.setCreateUser(user);
		storageInInfo.setCreateDate(new Date());
		storageInInfo.setKit(s.getKit());
		storageInInfo.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
		storageInInfo.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
		this.storageInInfoService.saveItem(s, si, storageInInfo);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		putObjToContext("storageInId", si.getStorageIn().getId());
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/storage/storageIn/item/storageInInfoEdit.jsp");
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public StorageInInfoService getStorageInInfoService() {
		return storageInInfoService;
	}

	public void setStorageInInfoService(
			StorageInInfoService storageInInfoService) {
		this.storageInInfoService = storageInInfoService;
	}

	public StorageInInfo getStorageInInfo() {
		return storageInInfo;
	}

	public void setStorageInInfo(StorageInInfo storageInInfo) {
		this.storageInInfo = storageInInfo;
	}

	// 打印
	@Action(value = "makeCode", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void makeCode() throws Exception {
		String[] ids = getRequest().getParameterValues("ids[]");
		// String[] sampleCode =
		// getRequest().getParameterValues("sampleCode[]");
		// String[] expireDate =
		// getRequest().getParameterValues("expireDate[]");
		String id = getParameterFromRequest("id");// 打印机id
		CodeMain c = this.codeMainService.get(id);
		String printStr = "";
		String context = dy(ids, c);
		printStr = context;
		Socket socket = null;
		OutputStream os;
		try {
			socket = new Socket();
			// SocketAddress sa = new InetSocketAddress("10.21.9.172", 9100);
			// SocketAddress sa = new InetSocketAddress("10.21.9.222", 9100);
			SocketAddress sa = new InetSocketAddress(c.getIp(), 9100);
			socket.connect(sa);
			os = socket.getOutputStream();
			os.write(printStr.getBytes("GBK"));
			os.flush();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
				}
			}
		}
	}

	public String dy(String[] ids, CodeMain c) {
		int length = ids.length;
		int n = 0;
		if (length % 2 > 0) {
			n = length / 2 + 1;
		} else {
			n = length / 2;
		}
		String context = "";
		int a = 0;
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MMM.yyyy",
				Locale.ENGLISH);

		for (int i = 0; i < n; i++) {

			String str = "";
			if (length > a) {
				StorageInInfoItem item = this.commonService.get(
						StorageInInfoItem.class, ids[a]);
				String s = "";
				if (item.getExpireDate() != null) {
					s = sdf.format(item.getExpireDate());
				}
				str += "#" + item.getCode() + "&" + item.getCode() + "&" + s;
				a++;
				if (length > a) {
					StorageInInfoItem item2 = this.commonService.get(
							StorageInInfoItem.class, ids[a]);
					String s2 = "";
					if (item2.getExpireDate() != null) {
						s2 = sdf.format(item2.getExpireDate());
					}
					str += "&" + item2.getCode() + "&" + item2.getCode() + "&"
							+ s2;
					a++;
				} else {
					str += "&&&";
				}
			}

			String dyCode = c.getCode();
			String d = dyCode.replaceAll("~~~AAA~~~", str);
			context += d;
		}

		return context;
	}
}
