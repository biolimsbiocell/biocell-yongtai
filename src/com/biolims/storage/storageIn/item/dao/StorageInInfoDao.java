package com.biolims.storage.storageIn.item.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.storage.storageIn.item.model.StorageInInfo;
import com.biolims.storage.storageIn.item.model.StorageInInfoItem;

@Repository
@SuppressWarnings("unchecked")
public class StorageInInfoDao extends BaseHibernateDao {
	public Map<String, Object> selectStorageInInfoList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from StorageInInfo where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<StorageInInfo> list = new ArrayList<StorageInInfo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectStorageInInfoItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort,
			String serial, String storageInId) throws Exception {
		String hql = "from StorageInInfoItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and storageInInfo.id='" + scId + "'";
		if (!"null".equals(serial))
			key = key + " and serial='" + serial + "'";
		if (!"".equals(storageInId)) {
			key = key + " and storageInId='" + storageInId + "'";
		}
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<StorageInInfoItem> list = new ArrayList<StorageInInfoItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 根据试剂编号和批次号查询
	public List<StorageInInfoItem> getListItem(String id, String serial,
			String storageInId) {
		String hql = "from StorageInInfoItem where 1=1 and storageInInfo.id='"
				+ id + "'and serial='" + serial + "' and storageInId='"
				+ storageInId + "'";
		List<StorageInInfoItem> list = new ArrayList<StorageInInfoItem>();
		list = this.getSession().createQuery(hql).list();
		return list;
	}
}