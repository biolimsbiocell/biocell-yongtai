package com.biolims.storage.storageIn.item.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.storage.position.model.StoragePosition;

/**
 * @Title: Model
 * @Description: 入库试剂明细
 * @author lims-platform
 * @date 2016-12-25 14:37:36
 * @version V1.0
 * 
 */
@Entity
@Table(name = "STORAGE_IN_INFO_ITEM")
@SuppressWarnings("serial")
public class StorageInInfoItem extends EntityDao<StorageInInfoItem> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 编码 */
	private String code;
	/** 型号 */
	private String spec;
	/** jde编码 */
	private String jdeCode;
	/** 批号 */
	private String serial;
	/** 过期日期 */
	private Date expireDate;
	/** 反应数 */
	private String reactionNum;
	/** qc状态 */
	private String qcPass;
	/** 存储位置 */
	private StoragePosition position;
	/** 备注 */
	private String note;
	/** 相关主表 */
	private StorageInInfo storageInInfo;
	// 入库单号
	private String storageInId;

	public String getStorageInId() {
		return storageInId;
	}

	public void setStorageInId(String storageInId) {
		this.storageInId = storageInId;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编码
	 */
	@Column(name = "CODE", length = 50)
	public String getCode() {
		return this.code;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编码
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 型号
	 */
	@Column(name = "SPEC", length = 50)
	public String getSpec() {
		return this.spec;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 型号
	 */
	public void setSpec(String spec) {
		this.spec = spec;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String jde编码
	 */
	@Column(name = "JDE_CODE", length = 50)
	public String getJdeCode() {
		return this.jdeCode;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String jde编码
	 */
	public void setJdeCode(String jdeCode) {
		this.jdeCode = jdeCode;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 批号
	 */
	@Column(name = "SERIAL", length = 50)
	public String getSerial() {
		return this.serial;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 批号
	 */
	public void setSerial(String serial) {
		this.serial = serial;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 反应数
	 */
	@Column(name = "REACTION_NUM", length = 50)
	public String getReactionNum() {
		return this.reactionNum;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 过期日期
	 */
	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 反应数
	 */
	public void setReactionNum(String reactionNum) {
		this.reactionNum = reactionNum;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String qc状态
	 */
	@Column(name = "QC_PASS", length = 50)
	public String getQcPass() {
		return this.qcPass;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String qc状态
	 */
	public void setQcPass(String qcPass) {
		this.qcPass = qcPass;
	}

	/**
	 * 方法: 取得StoragePosition
	 * 
	 * @return: StoragePosition 存储位置
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "POSITION")
	public StoragePosition getPosition() {
		return this.position;
	}

	/**
	 * 方法: 设置StoragePosition
	 * 
	 * @param: StoragePosition 存储位置
	 */
	public void setPosition(StoragePosition position) {
		this.position = position;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 备注
	 */
	@Column(name = "NOTE", length = 50)
	public String getNote() {
		return this.note;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 备注
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 方法: 取得StorageInInfo
	 * 
	 * @return: StorageInInfo 相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "STORAGE_IN_INFO")
	public StorageInInfo getStorageInInfo() {
		return this.storageInInfo;
	}

	/**
	 * 方法: 设置StorageInInfo
	 * 
	 * @param: StorageInInfo 相关主表
	 */
	public void setStorageInInfo(StorageInInfo storageInInfo) {
		this.storageInInfo = storageInInfo;
	}
}