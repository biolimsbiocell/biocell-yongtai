/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：ACTION
 * 类描述：基础框架,登录
 * 创建人：倪毅
 * 创建时间：2011-8-13
 * 修改人：倪毅
 * 修改时间：2010-12-09
 * 修改备注：
 * @version 1.0
 */
package com.biolims.main.action;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.Rights;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.core.model.user.DicCostCenter;
import com.biolims.core.model.user.UserGroupUser;
import com.biolims.core.model.user.UserRole;
import com.biolims.core.user.service.UserService;
import com.biolims.crm.project.dao.ProjectDao;
import com.biolims.crm.project.model.Project;
import com.biolims.deviation.model.InfluenceEquipment;
import com.biolims.deviation.model.InfluenceMateriel;
import com.biolims.deviation.model.InfluenceProduct;
import com.biolims.equipment.model.InstrumentRepairPlanTask;
import com.biolims.experiment.cell.passage.model.CellPassage;
import com.biolims.experiment.cell.passage.model.CellPassageItem;
import com.biolims.experiment.cell.passage.model.CellProductionRecord;
import com.biolims.experiment.noticeInformation.model.NoticeInformation;
import com.biolims.experiment.reinfusionPlan.model.ReinfusionPlanItem;
import com.biolims.experiment.roomManagement.model.RoomManagement;
import com.biolims.main.dao.MainDao;
import com.biolims.main.service.MainService;
import com.biolims.remind.model.SysRemind;
import com.biolims.rsa.RSAUtils;
import com.biolims.sample.model.SampleOrder;
import com.biolims.storage.model.Storage;
import com.biolims.storage.model.StorageReagentBuySerial;
import com.biolims.system.product.model.Product;
import com.biolims.tra.transport.model.TransportOrderCell;
import com.biolims.util.ConfigurableContants;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.MD5Util;
import com.biolims.util.MsgLocale;
import com.biolims.util.SendData;
import com.biolims.web.action.BaseActionSupport;
import com.biolims.workflow.bean.ProcessTaskBean;

@Namespace("/main")
@Controller
@Scope("prototype")
@ParentPackage("default")
public class MainAction extends BaseActionSupport {
	private static final long serialVersionUID = -164661537469123056L;

	@Autowired
	private MainService mainService;
	@Autowired
	private UserService userService;
	@Autowired
	private ProjectDao projectDao;

	@Resource
	private CommonService commonService;
	@Resource
	private CodingRuleService codingRuleService;
	private List<DicCostCenter> listDicCostCenter;
	private User loginUser = new User();

	private DicCostCenter dicCostCenter = new DicCostCenter();
	protected static Properties props = new Properties();
	@Resource
	private MainDao mainDao;

	protected static void init(String propertyFileName) {
		InputStream in = null;
		try {
			in = ConfigurableContants.class.getResourceAsStream(propertyFileName);
			if (in != null)
				props.load(in);
		} catch (IOException e) {

		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
				}
			}
		}
	}

	protected static String getProperty(String key, String defaultValue) {
		return props.getProperty(key, defaultValue);
	}

	/**
	 * 访问登陆页面
	 */
	@Action(value = "toWelcome", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public String toLogin() throws Exception {

		// listDicCostCenter = userService.findDicCostCenter();
		super.getSession().invalidate();
		return dispatcher("/lims/pages/login.jsp");

	}

	/**
	 * 访问导航header
	 */
	public String toNav() {
		return "";
	}

	/**
	 * 访问tree导航区
	 */
	@Action(value = "toTree", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public String toTree() throws Exception {
		return dispatcher("/WEB-INF/page/main/tree/tree.jsp");
	}

	@Action(value = "treeShowJson", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void userShowJsonByQuery() throws Exception {
		User user = (User) getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		List<Rights> aList = mainService.findUserRightsTree(user.getId());
		String a = mainService.getJson(aList);
		new SendData().sendDataJson(a, ServletActionContext.getResponse());
	}

	// 新页面 左侧列表树
	@Action(value = "newTreeShow", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void newTreeShow() throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String userID = getRequest().getParameter("user");
			List<Rights> list = mainService.finNewTree(userID);
			String a = mainService.getJson(list);
			// String b = a.replace("\'", "\"");
			map.put("data", a);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 新前台页面登录功能
	@Action(value = "newToMain", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public String newToMain() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String userID = getRequest().getParameter("user");
			String passWords = getRequest().getParameter("pad");
			String language = getRequest().getParameter("language");
			User users = this.mainService.selUsers(userID);
			if (users != null && users.getState() != null && users.getState().getId().startsWith("1")) {
				if (!RSAUtils.rsaDecrypt()) {
					putObjToContext("errmsg", "系统许可文件验证失败！请联系管理员！");
					return dispatcher("/lims/pages/login.jsp");
				} else {
					if (MD5Util.getMD5Lower(passWords).equals(users.getUserPassword())) {
//						if(!users.getId().equals("admin")){
//							Date date=new Date();
//							if(users.getMaturityDate()!=null){
//								if(date.getTime()>users.getMaturityDate().getTime()){
//									//用户有效期到期
//									putObjToContext("errmsg", "账号到期！");
//									return dispatcher("/lims/pages/login.jsp");
//								};
//							}else{
//								//用户有效期到期
//								putObjToContext("errmsg", "账号到期！");
//								return dispatcher("/lims/pages/login.jsp");
//							}
//							
//						}
						putObjToSession(SystemConstants.USER_SESSION_KEY, users);
						List<Rights> userRightsList = mainService.findUserRights(users.getId());
						putObjToSession(SystemConstants.USER_SESSION_RIGHTS, userRightsList);
						putObjToSession(SystemConstants.USER_SESSION_IS_SALE_MANAGE,
								mainService.findUserSaleManageJob(users.getId()));
						putObjToSession(SystemConstants.USER_SESSION_DEPARTMENT,
								mainService.findUserRightsDepratment(userRightsList, users));
						// 用户portlet
						// 中英文版本控制
//							if(language =="1"||language.equals("1")){
//								putObjToSession("lan","zh_CN");
//							}else if(language == "0" ||language.equals("0")){
//								putObjToSession("lan","en");
//							}
						init("/system.properties");

						putObjToSession("systemLan", getProperty("systemLan", "zh_CN"));

						putObjToSession("userName", users.getName());
						putObjToSession("scopeId", users.getScopeId());
						putObjToSession("scopeName", users.getScopeName());
						putObjToSession("lan", language);
						putObjToSession(SystemConstants.USER_SESSION_PORTLET_CONTENT,
								mainService.findRolePortletContent(users.getId()));
						putObjToSession(SystemConstants.USER_SESSION_GROUPS,
								mainService.findUserGroupStr(users.getId()));
						// 用户权限对应管理范围权限:只有部门范围动作
						putObjToSession(SystemConstants.USER_SESSION_RIGHTS_ACTION,
								mainService.findUserRightsAction(users));
//						mainService.saveLogInfo(users,getIpAddress(ServletActionContext.getRequest()));
						mainService.saveLogInfo(users);
						return dispatcher("/lims/pages/main.jsp");
						// 用户名密码匹配
						// 登录之后将试剂和仓库满足条件数据插入到SysRemind
						// Date date = new Date();
						// storageService.insertDataToWarning(date);
					} else {
						// 用户名密码不匹配
						putObjToContext("errmsg", "用户名密码不匹配!");
						return dispatcher("/lims/pages/login.jsp");
					}
				}
			} else {
				// 用户名不存在
				putObjToContext("errmsg", "用户名不存在!");
				return dispatcher("/lims/pages/login.jsp");
			}
		} catch (Exception e) {
			e.printStackTrace();
			putObjToContext("errmsg", "登录失败!");
			return dispatcher("/lims/pages/login.jsp");
		}
	}

	// 新前台页面登录功能
//	@Action(value = "newToMain", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
//	public void newToMain() throws Exception {
//
//		Map<String, Object> map = new HashMap<String, Object>();
//		try {
//			String userID = getRequest().getParameter("user");
//			String passWords = getRequest().getParameter("pad");
//			String language = getRequest().getParameter("language");
//			User users = this.mainService.selUsers(userID);
//			if (users != null && users.getState() != null && users.getState().getId().startsWith("1")) {
//				putObjToSession(SystemConstants.USER_SESSION_KEY, users);
//				List<Rights> userRightsList = mainService.findUserRights(users.getId());
//				putObjToSession(SystemConstants.USER_SESSION_RIGHTS, userRightsList);
//				putObjToSession(SystemConstants.USER_SESSION_IS_SALE_MANAGE,
//						mainService.findUserSaleManageJob(users.getId()));
//				putObjToSession(SystemConstants.USER_SESSION_DEPARTMENT,
//						mainService.findUserRightsDepratment(userRightsList, users));
//				// 用户portlet
//
//				// 中英文版本控制
//				// if(language =="1"||language.equals("1")){
//				// putObjToSession("lan","zh_CN");
//				// }else if(language == "0" ||language.equals("0")){
//				// putObjToSession("lan","en");
//				// }
//
//				init("/system.properties");
//
//				putObjToSession("systemLan", getProperty("systemLan", "zh_CN"));
//
//				putObjToSession("scopeId", users.getScopeId());
//				putObjToSession("scopeName", users.getScopeName());
//				putObjToSession("lan", language);
//				putObjToSession(SystemConstants.USER_SESSION_PORTLET_CONTENT,
//						mainService.findRolePortletContent(users.getId()));
//				putObjToSession(SystemConstants.USER_SESSION_GROUPS, mainService.findUserGroupStr(users.getId()));
//				// 用户权限对应管理范围权限:只有部门范围动作
//				putObjToSession(SystemConstants.USER_SESSION_RIGHTS_ACTION, mainService.findUserRightsAction(users));
//				mainService.saveLogInfo(users);
//				if(!RSAUtils.rsaDecrypt()) {
//					map.put("data", "5");
//				}else {
//					if (MD5Util.getMD5Lower(passWords).equals(users.getUserPassword())) {
//						// 用户名密码匹配
//						map.put("data", "1");
//					} else {
//						// 用户名密码不匹配
//						map.put("data", "0");
//					}
//				}
//			} else {
//				// 用户名不存在
//				map.put("data", "2");
//			}
//			String name = users.getName();
//			map.put("name", name);
//			map.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			map.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}

	// 项目主数据显示
	@Action(value = "showProject", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void showProject() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			// 样本
			List<Project> list = mainService.findProject();
			//// //条数
			//// String projectCounts = mainService.findProjectCounts();
			// for(int i=0;i<list.size();i++){
			// String id = list.get(i).getId();
			// String num= projectDao.selectProjectNum(id);
			// list.get(i).setSampleCodeNum(num);
			// }
			map.put("data", list);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 产品主数据显示
	@Action(value = "showProduct", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void showProduct() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			// 数据
			List<Product> list = mainService.findProduct();
			// 条数
			// String productCounts = mainService.findProductCounts();
			map.put("data", list);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 主页上方四个页面条数显示
	@Action(value = "showCounts", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void showCounts() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String sampleOrderCounts = mainService.findSampleOrderCounts();
			String SampleInfoCounts = mainService.findSampleInfoCounts();
			String TechReportCounts = mainService.findTechReportCounts();
			String ProjectCounts = mainService.findProjectCounts();
			map.put("counts1", sampleOrderCounts);
			map.put("counts2", SampleInfoCounts);
			map.put("counts3", TechReportCounts);
			map.put("counts4", ProjectCounts);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 主页信息预警列表显示
	@Action(value = "showInformationInfo", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void showInformationInfi() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String userID = getRequest().getParameter("userId");
			User users = this.mainService.selUsers(userID);
			List list = mainService.findInformation(userID);
			map.put("data", list);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 主页信息预警列表显示
	@Action(value = "showInformation", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void showInformation() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Date date = new Date();
		List<Object> list = new ArrayList<Object>();
		try {
			String userID = getRequest().getParameter("userId");
			/*
			 * User users = this.mainService.selUsers(userID); List list =
			 * mainService.findInformation(userID); map.put("data",list);
			 */
			// 库存到失效日期数据list
			List<StorageReagentBuySerial> bsList = mainService.getInventoryDataList(userID, date);
			// 安全数量小于库存数量
			List<Storage> sList = mainService.getInventoryDataListForSafeNum(userID);
			// 仪器中过期的数据
			List<InstrumentRepairPlanTask> ptList = mainService.getInstrumentDataList(userID, date);
			list.addAll(bsList);
			list.addAll(sList);
			list.addAll(ptList);
			map.put("bsList", bsList);
			map.put("sList", sList);
			map.put("ptList", ptList);
			map.put("data", list);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 主页定单预计采血时间到期提醒
	@Action(value = "showOrdersDue", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void showOrdersDue() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
//			String userID = getRequest().getParameter("userId");
			List<Map<String, Object>> findSampleOrderDue = mainService.findSampleOrderDue();
			map.put("sampleOrderDue", findSampleOrderDue);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 主页传递窗提醒
	@Action(value = "showDeliveryWindow", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void showDeliveryWindow() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List<Map<String, Object>> findDeliveryWindow = mainService.findDeliveryWindow();
			map.put("deliveryWindow", findDeliveryWindow);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	
	// 主页信息出具结果提醒
		@Action(value = "showResultAll", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
		public void showResultAll() throws Exception {
			Map<String, Object> map = new HashMap<String, Object>();
			try {
				List<Map<String, Object>> findResultAll = mainService.findResultAll();
				map.put("resultAll", findResultAll);
				map.put("success", true);
			} catch (Exception e) {
				e.printStackTrace();
				map.put("success", false);
			}
			HttpUtils.write(JsonUtils.toJsonString(map));
		}
		
		// 主页信息出具结果提醒
				@Action(value = "showTestItem", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
				public void showTestItem() throws Exception {
					Map<String, Object> map = new HashMap<String, Object>();
					try {
						List<Map<String, Object>> testItem = mainService.findTestItem();
						map.put("testItem", testItem);
						map.put("success", true);
					} catch (Exception e) {
						e.printStackTrace();
						map.put("success", false);
					}
					HttpUtils.write(JsonUtils.toJsonString(map));
				}

	// 主页定单预计采血时间到期提醒
	@Action(value = "showNotSampleReceive", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void showNotSampleReceive() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
//			String userID = getRequest().getParameter("userId");
			List<Map<String, Object>> findNotSampleReceive = mainService.findNotSampleReceive();
			map.put("notSampleReceive", findNotSampleReceive);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 主页回输日期未确认提醒
	@Action(value = "showReturnDateConfirm", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void showReturnDateConfirm() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List<Map<String, Object>> findReturnDateConfirm = mainService.findReturnDateConfirm();
			map.put("returnDateConfirm", findReturnDateConfirm);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 主页未进入运输计划的回输或者订单
	@Action(value = "showNoTransportPlan", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void showNoTransportPlan() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List<Map<String, Object>> findNoTransportPlan = mainService.findNoTransportPlan();
			map.put("noTransportPlan", findNoTransportPlan);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

//	运 输 计 划 完 成 （运输计划后面项目不完)
	@Action(value = "showTransportPlanBug", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void showTransportPlanBug() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List<Map<String, Object>> findTransportPlanBug = mainService.findTransportPlanBug();
			map.put("transportPlanBugList", findTransportPlanBug);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
//	运 输 计 划 完 成 （运输计划后面项目不完)
	@Action(value = "zhiJianSurplus", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void zhiJianSurplus() throws Exception {
		String userID = getRequest().getParameter("userId");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List<Map<String, Object>> zhiJianSurplus = mainService.findZhiJianSurplus(userID);
			map.put("zhiJianSurplus", zhiJianSurplus);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 主页显示未完成的偏差
	 * 
	 * @throws Exception
	 */
	@Action(value = "showDeviationInfo", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void showDeviationInfo() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Date date = new Date();
		List<Object> list = new ArrayList<Object>();
		try {
//			String userID = getRequest().getParameter("userId");
			/*
			 * User users = this.mainService.selUsers(userID); List list =
			 * mainService.findInformation(userID); map.put("data",list);
			 */
			// 偏差产品
			List<InfluenceProduct> pcList1 = mainService.getInfluenceProductList();
			// 偏差物料
			List<InfluenceMateriel> pcList2 = mainService.getPlanInfluenceMaterielList();
			// 偏差设备
			List<InfluenceEquipment> pcList3 = mainService.getInfluenceEquipmentList();
			list.addAll(pcList1);
			list.addAll(pcList2);
			list.addAll(pcList3);
			map.put("pcList1", pcList1);
			map.put("pcList2", pcList2);
			map.put("pcList3", pcList3);
			map.put("data", list);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 主页显示公告栏
	 * 
	 * @throws Exception
	 */
	@Action(value = "showGglList", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void showGglList() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Date date = new Date();
		List<Object> list = new ArrayList<Object>();
		try {
			// 偏差产品
			List<NoticeInformation> ggl = mainService.getNoticeInformationList();
			map.put("ggl", ggl);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 主页显示收获时间
	 * 
	 * @throws Exception
	 */
	@Action(value = "showHarvestDate", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void showHarvestDate() throws Exception {
		// 时间格式化
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

		Map<String, Object> map = new HashMap<String, Object>();
		Date date = new Date();
		List<Object> list = new ArrayList<Object>();
		try {
			// 运输子表查询
			List<TransportOrderCell> pcList1 = mainService.getTransportOrderCell();
			List<Map<String, String>> lis = new ArrayList<Map<String, String>>();
			if (pcList1.size() > 0) {
				for (TransportOrderCell toc : pcList1) {
					Map<String, String> map1 = new HashMap<String, String>();
					map1.put("code", toc.getCode());
					if (toc.getHarvestDate() != null) {
						map1.put("harvestDate", sdf.format(toc.getHarvestDate()));
					} else {
						map1.put("harvestDate", "");
					}
					if (toc.getTransportDate() != null) {
						map1.put("transportDate", sdf.format(toc.getTransportDate()));
					} else {
						map1.put("transportDate", "");
					}
					if (toc.getEndHarvestDate() != null) {
						map1.put("endHarvestDate", sdf.format(toc.getEndHarvestDate()));
					} else {
						map1.put("endHarvestDate", "");
					}

					if (toc.getCellDate() != null) {
						map1.put("cellDate", sdf.format(toc.getCellDate()));
					} else {
						map1.put("cellDate", "");
					}

					List<CellPassageItem> cpis = mainService.getCellPassageItems(toc.getCode());
					if (cpis.size() > 0) {
						CellPassageItem cpi = cpis.get(0);

						if (cpi.getCounts() != null && !"".equals(cpi.getCounts())) {
							map1.put("counts", cpi.getCounts());
						} else {
							map1.put("counts", "");
						}
						if (cpi.getPosId() != null && !"".equals(cpi.getPosId())) {
							map1.put("posId", cpi.getPosId());
						} else {
							map1.put("posId", "");
						}
					} else {
						map1.put("counts", "");
						map1.put("posId", "");
					}

					List<SampleOrder> sos = commonService.get(SampleOrder.class, "barcode", toc.getCode());
					if (sos.size() > 0) {
						SampleOrder so = sos.get(0);
						if (so.getCcoi() != null && !"".equals(so.getCcoi())) {
							map1.put("ccoi", so.getCcoi());
						} else {
							map1.put("ccoi", "");
						}
					} else {
						map1.put("ccoi", "");
					}

					lis.add(map1);
				}
			}
			map.put("pcList1", lis);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 主页显示回输计划
	 * 
	 * @throws Exception
	 */
	@Action(value = "showHsjhList", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void showHsjhList() throws Exception {
		// 时间格式化
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

		Map<String, Object> map = new HashMap<String, Object>();
		Date date = new Date();
		List<Object> list = new ArrayList<Object>();
		try {
			// 查询待回输确认的数据
			List<ReinfusionPlanItem> pcList1 = mainService.getReinfusionPlanItemUnconfirm();
			List<Map<String, String>> lis = new ArrayList<Map<String, String>>();
			if (pcList1.size() > 0) {
				for (ReinfusionPlanItem toc : pcList1) {
					Map<String, String> map1 = new HashMap<String, String>();
					map1.put("batch", toc.getBatch());
					if (toc.getReinfusionPlanDate() != null) {
						map1.put("reinfusionPlanDate", toc.getReinfusionPlanDate());
					} else {
						map1.put("reinfusionPlanDate", "");
					}
					lis.add(map1);
				}
			}
			// 回输计划未生成（预回输时间前5天）
			List<SampleOrder> pcList2 = mainService.getSampleOrderUnconfirm();
			List<Map<String, String>> lis2 = new ArrayList<Map<String, String>>();
			SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
			if (pcList1.size() > 0) {
				for (SampleOrder toc : pcList2) {
					Date dt = sdff.parse(sdff.format(new Date()));
					Calendar rightNow = Calendar.getInstance();
					rightNow.setTime(dt);
					rightNow.add(Calendar.DAY_OF_YEAR, 5);// 日期加10天
					Date dt1 = rightNow.getTime();
					if (toc.getFeedBackTime() != null) {
						if (toc.getFeedBackTime().getTime() <= dt1.getTime()) {
							Map<String, String> map1 = new HashMap<String, String>();
							map1.put("barcode", toc.getBarcode());
							map1.put("id", toc.getId());
							if (toc.getFeedBackTime() != null) {
								map1.put("feedBackTime", sdf.format(toc.getFeedBackTime()));
							} else {
								map1.put("feedBackTime", "");
							}
							lis2.add(map1);
						}
					}
				}
			}
			map.put("hsjhList1", lis);
			map.put("hsjhList2", lis2);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "selectUserId", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void selectUserId() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String userID = getRequest().getParameter("userId");
			// User users = this.mainService.selUsers(userID);
			UserGroupUser ug = this.mainService.selUserGroupId(userID);
			if (ug != null) {
				map.put("result", true);
			} else {
				map.put("result", false);
			}
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 点击信息预警跳转界面
	@Action(value = "showInformationItem", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void showInformationItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			// 主键ID
			String id = getRequest().getParameter("id");
			// 表单ID
			String contentId = getRequest().getParameter("contentId");
			// tableID
			String tableId = getRequest().getParameter("tableId");

			// 查询明细表信息
			String item = this.mainService.findInformationItem(tableId);
			System.out.println(item);// 跳转路径
			// 改变state的值并保存
			SysRemind sr = mainService.get(SysRemind.class, id);
			sr.setState("1");
			mainService.saveOrUpdate(sr);
			String pathName = item + contentId;
			map.put("data", pathName);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		System.out.println(JsonUtils.toJsonString(map));
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 修改密码
	@Action(value = "updPwd", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void updPassword() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String userId = getRequest().getParameter("userId");
			String oldPwd = getRequest().getParameter("oldPwd");
			String newPwd = getRequest().getParameter("newPwd");
			User users = this.mainService.selUsers(userId);
			if (MD5Util.getMD5Lower(oldPwd).equals(users.getUserPassword())) {
				// 用户名密码匹配
				users.setUserPassword(MD5Util.getMD5Lower(newPwd));
				mainService.saveOrUpdate(users);
				map.put("data", "1");
			} else {
				// 用户名密码不匹配
				map.put("data", "0");
			}
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 主页待办事项列表显示
	@Action(value = "showListOne", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void showListOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String userIds = getRequest().getParameter("user");
			String passWords = getRequest().getParameter("pad");
			// 根据用户名ID查询当前用户所在组
			String str1 = "";
			List<UserGroupUser> groupIds = this.mainService.selGroupIds(userIds);
			for (int i = 0; i < groupIds.size(); i++) {
				String str = groupIds.get(i).getUserGroup().getId();
				if (i < groupIds.size() - 1) {
					str1 += str + ",";
				} else {
					str1 += str;
				}
			}
			List<ProcessTaskBean> list = mainService.findToDoTaskListByUserId(userIds, getGroupIdList(str1));
			System.out.println(list);
			for (int j = 0; j < list.size(); j++) {
				String str2 = list.get(j).getTitle();
				System.out.println(str2);
			}
			map.put("data", list);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 导航栏待办事项显示列表
	@Action(value = "showListTwo", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void showListTwo() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String userIds = getRequest().getParameter("user");
			// 根据用户名ID查询当前用户所在组
			String str1 = "";
			List<UserGroupUser> groupIds = this.mainService.selGroupIds(userIds);
			for (int i = 0; i < groupIds.size(); i++) {
				String str = groupIds.get(i).getUserGroup().getId();
				if (i < groupIds.size() - 1) {
					str1 += str + ",";
				} else {
					str1 += str;
				}
			}
			List<ProcessTaskBean> list = mainService.findToDoTaskListByUserId1(userIds, getGroupIdList(str1));
			map.put("data", list);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 用户组
	private List<String> getGroupIdList(String str1) throws Exception {
		List<String> list = null;
		if (str1 != null && str1.trim().length() > 0) {
			list = new ArrayList<String>();
			String[] groupArray = str1.split(",");
			for (String id : groupArray) {
				if (id != null && id.trim().length() > 0) {
					list.add(id);
				}
			}
		}
		return list;
	}

	// 点击待办事项跳转至老页面
	@Action(value = "showOldTaskItem", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void showOldTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			// //任务单ID
			// String formId = getRequest().getParameter("formId");
			// 表单ID
			String formName = getRequest().getParameter("formName");
			// //taskID
			// String taskId = getRequest().getParameter("taskId");

			// 查询明细表信息
			String item = this.mainService.findTable(formName);
			System.out.println(item);
			map.put("data", item);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		System.out.println(JsonUtils.toJsonString(map));
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 点击待办事项跳转至新页面
	@Action(value = "showNewTaskItem", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void showNewTaskItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			// //任务单ID
			// String formId = getRequest().getParameter("formId");
			// 表单ID
			String formName = getRequest().getParameter("formName");
			// //taskID
			// String taskId = getRequest().getParameter("taskId");

			// 查询明细表信息
			String item = this.mainService.findNewTable(formName);
			System.out.println(item);
			map.put("data", item);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		System.out.println(JsonUtils.toJsonString(map));
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 注销方法
	@Action(value = "newLogOut", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void NewLogOut() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			super.getSession().invalidate();
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 访问主框架
	 */
	@Action(value = "toMain", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public String toMainFrame() throws Exception {

		if (getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY) != null) {
			loginUser = (User) getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		}

		if (loginUser.getId() == null) {
			// listDicCostCenter = userService.findDicCostCenter();
			return dispatcher("/WEB-INF/page/main/welcome.jsp");
		}
		User user = mainService.loginUser(loginUser.getId(), loginUser.getUserPassword());

		if (user != null && user.getState() != null && user.getState().getId().startsWith("1")) {
			if (!MD5Util.getMD5Lower(loginUser.getUserPassword()).equals(user.getUserPassword())) {
				super.getSession().invalidate();
				displayMsg(MsgLocale.getLocale("user.main.password.error"));

				// listDicCostCenter = userService.findDicCostCenter();

				return dispatcher("/WEB-INF/page/main/welcome.jsp");
			} else {
				if (dicCostCenter.getId() != null) {
					putObjToSession(SystemConstants.USER_SESSION_SCOPE_KEY, dicCostCenter.getId());
					List<User> list = mainService.findUserWhereList(loginUser.getId(), dicCostCenter.getId());
					if (list.size() <= 0 || list == null) {
						// listDicCostCenter = userService.findDicCostCenter();
						return dispatcher("/WEB-INF/page/main/welcome.jsp");
					}
				}
			}

			// 用户信息
			putObjToSession(SystemConstants.USER_SESSION_KEY, user);
			List<Rights> userRightsList = mainService.findUserRights(user.getId());

			// 用户功能权限
			putObjToSession(SystemConstants.USER_SESSION_RIGHTS, userRightsList);

			// putObjToSession(SystemConstants.USER_SESSION_ACTION_RIGHTS,mainService.findUserRightsActionRights(user.getId()));
			// 用户功能动作权限:没有部门范围动作
			// putObjToSession(SystemConstants.USER_SESSION_RIGHTS_ACTION,mainService.findUserRightsAction(userRightsList,user.getId()));
			// 用户管理范围权限:权限对应范围

			// 是否销售主管
			putObjToSession(SystemConstants.USER_SESSION_IS_SALE_MANAGE,
					mainService.findUserSaleManageJob(user.getId()));

			putObjToSession(SystemConstants.USER_SESSION_DEPARTMENT,
					mainService.findUserRightsDepratment(userRightsList, user));
			// 用户portlet
			putObjToSession(SystemConstants.USER_SESSION_PORTLET_CONTENT,
					mainService.findRolePortletContent(user.getId()));
			putObjToSession(SystemConstants.USER_SESSION_GROUPS, mainService.findUserGroupStr(user.getId()));
			// 用户权限对应管理范围权限:只有部门范围动作
			// putObjToSession(SystemConstants.USER_SESSION_RIGHTS_DEPARTMENT,
			// mainService.findUserRightsDepartmentRightsAction(user));

			// putObjToSession(SystemConstants.USER_SESSION_RIGHTS_APPLICATION_TYPE_ACTION,mainService.findUserRightsApplicationTypeAction(userRightsList));

			// 用户权限对应管理范围权限:只有部门范围动作
			putObjToSession(SystemConstants.USER_SESSION_RIGHTS_ACTION, mainService.findUserRightsAction(user));

			mainService.saveLogInfo(user);

		} else {
			// listDicCostCenter = userService.findDicCostCenter();
			displayMsg(MsgLocale.getLocale("user.main.name.error"));
			return dispatcher("/WEB-INF/page/main/welcome.jsp");
		}
		return dispatcher("/WEB-INF/page/main/main.jsp");
	}

	/**
	 * 访问主框架
	 */
	@Action(value = "toPortal", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String toPortal() throws Exception {
		return dispatcher("/lims/pages/main.jsp");
	}

	/**
	 * 访问offline页面
	 */
	@Action(value = "toOffline", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public String toOffline() {
		return dispatcher("/WEB-INF/page/error/offline.jsp");
	}

	/**
	 * 访问offline页面
	 */
	@Action(value = "logOut", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public String logOut() throws Exception {
		super.getSession().invalidate();
		return dispatcher("/lims/pages/login.jsp");
	}

	@Action(value = "userSetView", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String userSetView() throws Exception {
		loginUser = (User) getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		// 具体操作，如删除，填加动作，应用redirect
		return dispatcher("/WEB-INF/page/main/userSet.jsp");
	}

	@Action(value = "userSet", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String userSet() throws Exception {
		String id = getRequest().getParameter("id");
		String pwd = getRequest().getParameter("pwd");
		String oldPwd = getRequest().getParameter("oldPwd");
		String mobile = getRequest().getParameter("mobile");
		String address = getRequest().getParameter("address");
		String emailPassword = getRequest().getParameter("emailPassword");
		User modifyUser = new User();
		modifyUser.setAddress(address);
		modifyUser.setId(id);
		modifyUser.setOldPassword(oldPwd);
		modifyUser.setMobile(mobile);
		modifyUser.setPassword(pwd);
		modifyUser.setEmailPassword(emailPassword);

		loginUser = (User) getRequest().getSession().getAttribute(SystemConstants.USER_SESSION_KEY);
		User _modifyUser = this.mainService.modifyUser(modifyUser, loginUser);
		if (_modifyUser != null) {
			getRequest().getSession().setAttribute(SystemConstants.USER_SESSION_KEY, _modifyUser);
			return this.renderText("true");
		} else {
			return this.renderText("false");
		}

	}

	public User getLoginUser() {
		return loginUser;
	}

	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}

	public List<DicCostCenter> getListDicCostCenter() {
		return listDicCostCenter;
	}

	public void setListDicCostCenter(List<DicCostCenter> listDicCostCenter) {
		this.listDicCostCenter = listDicCostCenter;
	}

	public DicCostCenter getDicCostCenter() {
		return dicCostCenter;
	}

	public void setDicCostCenter(DicCostCenter dicCostCenter) {
		this.dicCostCenter = dicCostCenter;
	}

	// 按登录人查询待办情况
	@Action(value = "selBacklog", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void selBacklog() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			// 定义一个去重后的模块名称
			ArrayList<String> modelN = new ArrayList<String>();
			// 定义一个去重后的模块Id
			ArrayList<String> modelI = new ArrayList<String>();
			// 用户Id
			String userId = getRequest().getParameter("userId");
			// 通过用户ID查询用户角色
			List<UserRole> list = this.mainService.selUserRole(userId);
			// 通过角色查询所选择的待办数量模块并去重
			for (int i = 0; i < list.size(); i++) {
				String roleId = list.get(i).getRole().getId();
				String modelName = this.mainService.selModelName(roleId);
				String modelId = this.mainService.selModelId(roleId);
				String modelNames[];
				String modelIds[];
				if (modelName != null && modelId != null) {
					modelNames = modelName.split(",");
					modelIds = modelId.split(",");
					for (int k = 0; k < modelNames.length; k++) {
						modelN.add(modelNames[k]);
						modelI.add(modelIds[k]);
					}
				}
			}
			List counts = this.mainService.selBacklog(modelI, modelN);
			map.put("data", counts);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 按登录人查询异常情况
	@Action(value = "selAbnormal", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void selAbnormal() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {

			// 用户Id
			String userId = getRequest().getParameter("userId");
			ArrayList<String> abnormalN = new ArrayList<String>();
			ArrayList<String> abnormalI = new ArrayList<String>();
			// 通过用户ID查询用户角色
			List<UserRole> list = this.mainService.selUserRole(userId);
			// 通过角色查询所选择的待办数量模块并去重
			for (int i = 0; i < list.size(); i++) {
				String roleId = list.get(i).getRole().getId();
				String abnormalName = this.mainService.selAbnormalName(roleId);
				String abnormalId = this.mainService.selAbnormalId(roleId);
				String abnormalNames[];
				String abnormalIds[];
				if (abnormalName != null && abnormalId != null) {
					abnormalNames = abnormalName.split(",");
					abnormalIds = abnormalId.split(",");
					for (int k = 0; k < abnormalNames.length; k++) {
						abnormalN.add(abnormalNames[k]);
						abnormalI.add(abnormalIds[k]);
					}
				}
			}
			List counts = new ArrayList<Object>();
			if (abnormalI != null && abnormalI.size() > 0) {
				if (abnormalI.get(0) != null && !abnormalI.get(0).equals(""))
					counts = this.mainService.selAbnormal(abnormalI, abnormalN);
			}
			map.put("data", counts);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 点击待办数量或者异常数量跳转页面
	@Action(value = "selUrl", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void selUrl() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String name = getRequest().getParameter("name");
			String type = getRequest().getParameter("type");
			String url = this.mainService.selUrl(name, type);
			map.put("data", url);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "checkU", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void checkU() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String userID = getRequest().getParameter("user");
		String passWords = getRequest().getParameter("pad");
		User users = this.mainService.selUsers(userID);
		if (users != null && users.getState() != null && users.getState().getId().startsWith("1")) {
			if (MD5Util.getMD5Lower(passWords).equals(users.getUserPassword())) {
				map.put("id", users.getId());
				map.put("name", users.getName());
				map.put("success", "1");
			} else {
				// 用户名密码不匹配
				map.put("success", "0");
			}

		} else {
			map.put("success", "0");
		}
		HttpUtils.write(JsonUtils.toJsonString(map));

	}

	// 查询当前批次号是否是当前登陆人的任务
	@Action(value = "checkProject", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void checkProject() throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String pici = getRequest().getParameter("pici");
			List<CellProductionRecord> list = mainService.findCellProductionRecordByPici(pici);
			if (list.size() > 0) {
				map.put("checkYes", true);
				CellProductionRecord cpo = list.get(0);
				map.put("id", cpo.getCellPassage().getId());
				map.put("ordernum", cpo.getOrderNum());
			} else {
				map.put("checkYes", false);
			}
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 查询当前房间对应批号是否是当前登陆人的任务
	@Action(value = "checkProjectRoom", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void checkProjectRoom() throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {
//			String pici = "";
			String fjbh = getRequest().getParameter("fjbh");
			// 查询该房间状态
			RoomManagement rms = commonService.get(RoomManagement.class, fjbh);
			if (rms != null) {
				map.put("roomnull", true);
				if ("1".equals(rms.getRoomState())) {
					map.put("roomstate", true);
					// 按照房间编号查询正在生产批次
					List<CellProductionRecord> list = mainService.findCellProductionRecordByFjbh(fjbh);

//					List<CellProductionRecord> list = mainService.findCellProductionRecordByPici(pici);
					if (list.size() > 0) {
						map.put("checkYes", true);
						CellProductionRecord cpo = list.get(0);
						map.put("id", cpo.getCellPassage().getId());
						map.put("ordernum", cpo.getOrderNum());
					} else {
						map.put("checkYes", false);
					}
				} else {
					map.put("roomstate", false);
				}
			} else {
				map.put("roomnull", false);
			}
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 是否是他的任务，判断样本是否在外周血步骤 ，并且样本是否是一个实验单，样本数量是否正确
	@Action(value = "showCellpassageItem", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public void showCellpassageItem() throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			User user = (User) ServletActionContext.getRequest().getSession()
					.getAttribute(SystemConstants.USER_SESSION_KEY);
			// 查询样本是否是当前登陆人的任务
			List<CellPassageItem> cpis = mainService.findCellProductionRecordBySamples(ids);
			if (cpis.size() > 0) {
				if (ids.length == cpis.size()) {
					Boolean panduan = true;
					String checkYesNote = "";
					List<CellProductionRecord> cprs = null;
					String arrs = "";
					for (String ar : ids) {
						if ("".equals(arrs)) {
							arrs = arrs + "" + ar + "";
						} else {
							arrs = arrs + "," + ar + "";
						}
					}

					// 取其中一条，查询该实验单，是否是他的实验单，并且实验单下样本数量
					CellPassageItem cpi = cpis.get(0);
					if (cpi.getBatch() != null && !"".equals(cpi.getBatch())) {
						// 查下这个任务单下第一步样本
						List<CellProductionRecord> list = mainService
								.findCellProductionRecordByPiciStepOne(cpi.getBatch());
						if (list.size() > 0) {
							List<CellPassageItem> cpiss = mainDao
									.getCellPassageItemsByOne(list.get(0).getCellPassage().getId(), "1");
							if (ids.length == cpiss.size()) {
								for (CellPassageItem cp : cpiss) {
									if (arrs.indexOf(cp.getCode()) == -1) {
										panduan = false;
										checkYesNote = checkYesNote + "-" + cp.getCode();
									}
								}
								if (panduan) {
									map.put("checkYes", true);
									map.put("checkYesId", list.get(0).getCellPassage().getId());
								} else {
									map.put("checkYes", false);
									map.put("checkYesNote", "存在不同实验样本！请核查！");
								}
							} else if (ids.length > cpiss.size()) {
								map.put("checkYes", false);
								map.put("checkYesNote", "存在不同实验样本！请核查！");
							} else if (ids.length < cpiss.size()) {
								map.put("checkYes", false);
								map.put("checkYesNote", "样本数量不足！请核查！");
							}
						} else {
							map.put("checkYes", false);
							map.put("checkYesNote", "存在非当前用户样本！请核查！");
						}
					} else {
						map.put("checkYes", false);
						map.put("checkYesNote", "存在未分配样本！请核查！");
					}
				} else {
					map.put("checkYes", false);
					map.put("checkYesNote", "存在未分配样本！请核查！");
				}
			} else {
				map.put("checkYes", false);
				map.put("checkYesNote", "当前样本未分配！请核查！");
			}
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
}
