/**
 * 
 * 项目名称：BIO-LIMS
 * 类名称：MainService
 * 类描述：主框架service
 * 创建人：倪毅
 * 创建时间：2011-9
 * 修改人： 
 * 修改时间：
 * 修改备注：
 * @version 1.0
 */
package com.biolims.main.service;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.activiti.engine.task.Task;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.applicationType.service.ApplicationTypeService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.Rights;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.core.model.user.Department;
import com.biolims.core.model.user.Role;
import com.biolims.core.model.user.RoleRights;
import com.biolims.core.model.user.UserGroup;
import com.biolims.core.model.user.UserGroupUser;
import com.biolims.core.model.user.UserJobUser;
import com.biolims.core.model.user.UserRole;
import com.biolims.crm.project.model.Project;
import com.biolims.deviation.model.InfluenceEquipment;
import com.biolims.deviation.model.InfluenceMateriel;
import com.biolims.deviation.model.InfluenceProduct;
import com.biolims.equipment.model.InstrumentRepairPlanTask;
import com.biolims.experiment.cell.passage.model.CellPassage;
import com.biolims.experiment.cell.passage.model.CellPassageItem;
import com.biolims.experiment.cell.passage.model.CellPassageQualityItem;
import com.biolims.experiment.cell.passage.model.CellProductionRecord;
import com.biolims.experiment.noticeInformation.model.NoticeInformation;
import com.biolims.experiment.quality.model.QualityTestInfo;
import com.biolims.experiment.reinfusionPlan.model.ReinfusionPlanItem;
import com.biolims.experiment.transferWindowManage.model.TransferWindowManage;
import com.biolims.experiment.transferWindowManage.model.TransferWindowState;
import com.biolims.file.service.FileInfoService;
import com.biolims.log.model.LogInfo;
import com.biolims.main.dao.MainDao;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleReceive;
import com.biolims.storage.model.Storage;
import com.biolims.storage.model.StorageReagentBuySerial;
import com.biolims.system.detecyion.model.SampleDeteyion;
import com.biolims.system.product.model.Product;
import com.biolims.tra.transport.model.TransportOrderCell;
import com.biolims.tra.transport.model.TransportOrderTemp;
import com.biolims.util.MD5Util;
import com.biolims.workflow.bean.ProcessTaskBean;
import com.biolims.workflow.dao.WorkflowProcessInstanceDao;
import com.biolims.workflow.entity.WorkflowProcessDefinitionTaskFormConfigEntity;
import com.biolims.workflow.entity.WorkflowProcesssInstanceEntity;
import com.biolims.workflow.service.WorkflowProcessDefinitionService;
import com.biolims.workflow.service.WorkflowService;

@Service
public class MainService extends WorkflowService {

	@Resource
	private WorkflowProcessInstanceDao workflowProcessInstanceDao;
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private WorkflowProcessDefinitionService workflowProcessDefinitionService;

	@Resource
	private ApplicationTypeService applicationTypeService;

	@Resource
	private CommonDAO commonDAO;
	@Resource
	private MainDao mainDao;
	private StringBuffer json = new StringBuffer();
	@Resource
	private CommonService commonService;

	/**
	 * 验证登录
	 */
	public User loginUser(String id, String password) {
		User user = commonDAO.get(User.class, id);
		return user;
	}

	// 验证登陆用户名
	public User selUsers(String userID) {
		User user = commonDAO.get(User.class, userID);
		return user;
	}

	// 查询项目主数据
	public List<Project> findProject() throws Exception {
		return mainDao.selProject();
	}

	// 查询产品主数据
	public List<Product> findProduct() throws Exception {
		return mainDao.selProduct();
	}

	// //查询项目主数据条数
	// public String findPrjectCounts()throws Exception {
	// return mainDao.selProjectCounts1();
	// }
	// //查询产品主数据条数
	// public String findProductCounts()throws Exception {
	// return mainDao.selProductCounts();
	// }

	// 根据用户名ID查询该用户所在组
	public List<UserGroupUser> selGroupIds(String userIds) throws Exception {
		return mainDao.selGroupIds(userIds);

	}

	// 信息预警
	public List findInformation(String userId) throws Exception {
		return mainDao.selInformation(userId);

	}

	// 主页上方四个页面条数显示
	public String findSampleOrderCounts() throws Exception {
		return mainDao.selSampleOrderCounts();
	}

	public String findSampleInfoCounts() throws Exception {
		return mainDao.selSampleInfoCounts();
	}

	public String findTechReportCounts() throws Exception {
		return mainDao.selTechReportCounts();
	}

	public String findProjectCounts() throws Exception {
		return mainDao.selProjectCounts();
	}

	// 获取待办事项列表
	public List<ProcessTaskBean> findToDoTaskListByUserId(String userId, List<String> groupList) throws Exception {

		List<ProcessTaskBean> result = new ArrayList<ProcessTaskBean>();
		List<ProcessTaskBean> resultSort = new ArrayList<ProcessTaskBean>();
		// 存放当前用户的所有任务
		List<Task> tasks = new ArrayList<Task>();
		if (userId != null && userId.length() > 0) {
			// 根据当前用户的id查询代办任务列表(已经签收)
			List<Task> taskAssignees = getTaskService().createTaskQuery().taskAssignee(userId).orderByTaskCreateTime()
					.desc().list();
			// 根据当前用户id查询未签收的任务列表
			List<Task> taskCandidates = getTaskService().createTaskQuery().taskCandidateUser(userId)
					.orderByTaskCreateTime().desc().list();

			tasks.addAll(taskAssignees);
			tasks.addAll(taskCandidates);
		}
		if (groupList != null && groupList.size() > 0) {
			List<Task> taskGroupCandidates = getTaskService().createTaskQuery().taskCandidateGroupIn(groupList)
					.orderByTaskPriority().desc().orderByTaskCreateTime().desc().list();
			tasks.addAll(taskGroupCandidates);
		}
		if (tasks.size() > 0) {
			for (Task task : tasks) {
				ProcessTaskBean ptb = new ProcessTaskBean();
				String processInstanceId = task.getProcessInstanceId();
				WorkflowProcesssInstanceEntity wpie = workflowProcessInstanceDao
						.selectWorkflowProcesssInstanceEntityByInstanceId(processInstanceId);
				ptb.setTaskId(task.getId());

				String formName = wpie.getFormName();
				String formId = wpie.getBusinessKey();

				List<WorkflowProcessDefinitionTaskFormConfigEntity> taskList = workflowProcessDefinitionService
						.findWorkflowProcessDefinitionTaskFormConfigEntityByKey(wpie.getProcessDefinitionId(),
								task.getTaskDefinitionKey());

				// if (taskList.size() > 0) {
				// WorkflowProcessDefinitionTaskFormConfigEntity we =
				// (WorkflowProcessDefinitionTaskFormConfigEntity) taskList
				// .get(0);
				//
				// if (we.getFormName() != null) {
				//
				// formName = (String) getRuntimeService().getVariable(task.getExecutionId(),
				// "formName");
				// formId = (String) getRuntimeService().getVariable(task.getExecutionId(),
				// "formId");
				//
				// }
				// }

				ptb.setFormId(formId);
				ptb.setFormName(formName);

				ptb.setApplUserName(wpie.getApplUserName());
				ptb.setApplUserId(wpie.getApplUserId());
				ptb.setTitle(wpie.getFormTitle());
				ptb.setStartDate(wpie.getStartDate());
				ptb.setTaskName(task.getName());
				result.add(ptb);
			}
		}
		Collections.sort(result, new Comparator() {
			public int compare(Object a, Object b) {
				Date one = ((ProcessTaskBean) a).getStartDate();
				Date two = ((ProcessTaskBean) b).getStartDate();
				return two.compareTo(one);
			}
		});
		return result;
	}

	// 导航栏办事项列表
	public List<ProcessTaskBean> findToDoTaskListByUserId1(String userId, List<String> groupList) throws Exception {

		List<ProcessTaskBean> result = new ArrayList<ProcessTaskBean>();
		List<ProcessTaskBean> resultSort = new ArrayList<ProcessTaskBean>();
		// 存放当前用户的所有任务
		List<Task> tasks = new ArrayList<Task>();
		if (userId != null && userId.length() > 0) {
			// 根据当前用户的id查询代办任务列表(已经签收)
			List<Task> taskAssignees = getTaskService().createTaskQuery().taskAssignee(userId).orderByTaskCreateTime()
					.desc().list();
			// 根据当前用户id查询未签收的任务列表
			List<Task> taskCandidates = getTaskService().createTaskQuery().taskCandidateUser(userId)
					.orderByTaskCreateTime().desc().list();

			tasks.addAll(taskAssignees);
			tasks.addAll(taskCandidates);
		}
		if (groupList != null && groupList.size() > 0) {
			List<Task> taskGroupCandidates = getTaskService().createTaskQuery().taskCandidateGroupIn(groupList)
					.orderByTaskPriority().desc().orderByTaskCreateTime().desc().list();
			tasks.addAll(taskGroupCandidates);
		}
		if (tasks.size() > 0) {
			for (Task task : tasks) {
				ProcessTaskBean ptb = new ProcessTaskBean();
				String processInstanceId = task.getProcessInstanceId();
				WorkflowProcesssInstanceEntity wpie = workflowProcessInstanceDao
						.selectWorkflowProcesssInstanceEntityByInstanceId(processInstanceId);
				ptb.setTaskId(task.getId());

				String formName = wpie.getFormName();
				String formId = wpie.getBusinessKey();

				List<WorkflowProcessDefinitionTaskFormConfigEntity> taskList = workflowProcessDefinitionService
						.findWorkflowProcessDefinitionTaskFormConfigEntityByKey(wpie.getProcessDefinitionId(),
								task.getTaskDefinitionKey());

				if (taskList.size() > 0) {
					WorkflowProcessDefinitionTaskFormConfigEntity we = (WorkflowProcessDefinitionTaskFormConfigEntity) taskList
							.get(0);

					if (we.getFormName() != null) {

						formName = (String) getRuntimeService().getVariable(task.getExecutionId(), "formName");
						formId = (String) getRuntimeService().getVariable(task.getExecutionId(), "formId");

					}
				}

				ptb.setFormId(formId);
				ptb.setFormName(formName);

				ptb.setApplUserName(wpie.getApplUserName());
				ptb.setApplUserId(wpie.getApplUserId());
				ptb.setTitle(wpie.getFormTitle());
				ptb.setStartDate(wpie.getStartDate());
				ptb.setTaskName(task.getName());
				result.add(ptb);
			}
		}
		Collections.sort(result, new Comparator() {
			public int compare(Object a, Object b) {
				Date one = ((ProcessTaskBean) a).getStartDate();
				Date two = ((ProcessTaskBean) b).getStartDate();
				return two.compareTo(one);
			}
		});
		return result;
	}

	// 点击待办事项跳转至老页面
	public String findTable(String formName) throws Exception {
		return mainDao.selTable(formName);
	}

	// 点击信息预警跳转至老页面
	public String findInformationItem(String tableId) throws Exception {
		return mainDao.selInformationItem(tableId);

	}

	// 点击待办事项跳转至新页面
	public String findNewTable(String formName) throws Exception {
		return mainDao.selNewTable(formName);
	}

	/**
	 * 检索权限树,返回map
	 */
	public List<Rights> findRights() throws Exception {
		List<Rights> rightList = commonDAO
				.find("from Rights where state = '" + SystemConstants.DIC_STATE_YES_ID + "' order by orderNumber");
		return rightList;
	}

	/**
	 * 检索权限树,返回map
	 */
	public String findUserGroupStr(String userId) throws Exception {
		String groupIds = "";
		List<UserGroup> userGrouptList = commonDAO.find("select a.userGroup from UserGroupUser a where a.user.id =?",
				new Object[] { userId });
		for (UserGroup ug : userGrouptList) {

			groupIds += ug.getId();
			groupIds += ",";
		}

		return groupIds;
	}

	public List<Role> findRightsRole(String rightsId, String userId) throws Exception {
		List<Role> rightList = commonDAO
				.find("select distinct a.role from UserRole a,RoleRights b where a.role = b.role and b.rights.id ='"
						+ rightsId + "' and a.user.id='" + userId + "'");
		return rightList;
	}

	/**
	 * 检索权限树,返回List
	 */
	public List<Rights> findUserRights(String userId) throws Exception {
		List<Rights> rightList = commonDAO.find(
				"select distinct b.rights from UserRole a,RoleRights b where a.role = b.role and a.user.id=?  ",
				new Object[] { userId });
		return rightList;
	}

	/**
	 * 生成用户权限树,返回List
	 */
	public List<Rights> findUserRightsTree(String userId) throws Exception {
		List<Rights> allRights = findRights();
		List<Rights> userRights = findUserRights(userId);
		List<Rights> buildRights = new ArrayList<Rights>();
		for (int i = 0; i < allRights.size(); i++) {
			Rights rt = allRights.get(i);
			for (int j = 0; j < userRights.size(); j++) {
				Rights rt1 = userRights.get(j);
				// if (rt.getLevelId().startsWith(rt1.getLevelId() + ".")
				// || rt1.getLevelId().startsWith(rt.getLevelId() + ".")
				// || rt1.getLevelId().equals(rt.getLevelId())) {
				if (rt.getId().equals(rt1.getId())) {
					buildRights.add(rt);
					break;
				}
			}
		}
		return buildRights;
	}

	// 新页面生成树并返回值
	public List<Rights> finNewTree(String userId) throws Exception {
		List<Rights> allRights = findRights();
		List<Rights> userRights = findUserRights(userId);
		List<Rights> buildRights = new ArrayList<Rights>();
		for (int i = 0; i < allRights.size(); i++) {
			Rights rt = allRights.get(i);
			for (int j = 0; j < userRights.size(); j++) {
				Rights rt1 = userRights.get(j);
				if (rt.getId().equals(rt1.getId())) {
					buildRights.add(rt);
					break;
				}
			}
		}
		return buildRights;
	}

	/**
	 * 检索权限动作,返回List
	 */
	public List<RoleRights> findUserRightsActionRights(String userId) throws Exception {
		List<RoleRights> rightList = commonDAO
				.find("select b from UserRole a,RoleRights b where a.role = b.role  and a.user.id='" + userId + "'");
		return rightList;
	}

	public String findUserSaleManageJob(String userId) throws Exception {
		String re = "false";
		List<UserJobUser> rightList = commonDAO
				.find("from UserJobUser a where a.job.isSaleManage = '1' and a.user.id='" + userId + "'");
		if (rightList.size() > 0) {

			re = "true";
		}

		return re;
	}

	/**
	 * 检索一个权限的动作,返回String
	 */
	public String findRoleRightsStr(String rightId, String userId) throws Exception {
		List<RoleRights> rightList = commonDAO
				.find("select b from UserRole a,RoleRights b where a.role = b.role  and b.rights.id='" + rightId
						+ "' and a.user.id='" + userId + "'");
		String rs = "";
		for (int i = 0; i < rightList.size(); i++) {
			rs += rightList.get(i).getRightsAction();

		}
		return rs;
	}

	/**
	 * 检索一个portlet的字符串,返回String
	 */
	public String findRolePortletContent(String userId) throws Exception {
		List<Role> roleList = commonDAO.find("select b.role from UserRole b where  b.user.id='" + userId + "'");
		String rs = "";
		for (int i = 0; i < roleList.size(); i++) {
			rs += roleList.get(i).getPortletContent();
		}
		return rs;
	}

	/**
	 * 检索权限及动作,返回List
	 */
	public Map<String, String> findUserRightsAction(List<Rights> userRightsList, String userId) throws Exception {
		List<Rights> rightList = userRightsList;

		Map<String, String> rightsMap = new HashMap<String, String>();
		for (int i = 0; i < rightList.size(); i++) {
			String a = findRoleRightsStr(rightList.get(i).getId(), userId);
			if (a != null && !a.equals(""))
				rightsMap.put(rightList.get(i).getId(), findRoleRightsStr(rightList.get(i).getId(), userId));
		}

		return rightsMap;
	}

	/**
	 * 检索权限部门范围,返回List
	 */
	public List<Department> findUserRoleDepartment(User user) throws Exception {
		String userId = user.getId();
		Object[] a = { userId };
		List<Department> departmentList = commonDAO.find(
				"select distinct b.department from UserRole a,RoleDepartment b where a.role.id = b.role.id and a.user.id=?",
				a);
		Department userDeparment = user.getDepartment();
		// 加入自己所属部门
		if (!departmentList.contains(userDeparment))
			departmentList.add(userDeparment);
		return departmentList;
	}

	/**
	 * 检索权限部门范围,返回List
	 */
	public Map<String, Object> findUserRoleDepartment(String rightsId, User user) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String userId = user.getId();
		Object[] a = { rightsId, userId };
		String flag = "";
		List<Role> roleList = findRightsRole(rightsId, userId);
		for (Role r : roleList) {
			if (r.getDepartmentType() != null)
				flag += r.getDepartmentType();
		}

		List<Department> departmentList = new ArrayList<Department>();
		// 部分部门
		if (flag.contains("2")) {
			departmentList = commonDAO.find(
					"select distinct b.department from UserRole a,RoleDepartment b,RoleRights c where a.role.id = b.role.id and c.role.id = b.role.id and c.rights.id=? and a.user.id=?",
					a);

		}
		// 部分部门或本部门
		if (flag.contains("3") || flag.contains("2")) {

			Department userDeparment = user.getDepartment();
			// 加入自己所属部门
			if (!departmentList.contains(userDeparment))
				departmentList.add(userDeparment);
		}

		map.put("departmentList", departmentList);
		map.put("departmentType", flag);
		return map;
	}

	/**
	 * 检索权限相关部门范围,返回List
	 */
	public Map<String, Object> findUserRightsDepratment(List<Rights> userRightsList, User user) throws Exception {
		List<Rights> rightList = userRightsList;

		Map<String, Object> rightsMap = new HashMap<String, Object>();

		for (int i = 0; i < rightList.size(); i++) {
			rightsMap.put(rightList.get(i).getId(), findUserRoleDepartment(rightList.get(i).getId(), user));
		}
		return rightsMap;
	}

	public List<Object[]> findUserRightsDepartmentRightsAction(User user) throws Exception {
		String userId = user.getId();
		Object[] a = { userId };
		List<Object[]> departmentList = commonDAO.find(
				"select a.role.id,b.rights.id,c.department.id,b.rightsAction from UserRole a,RoleRights b,RoleDepartment c where a.role = b.role and a.role = c.role and a.user.id=?",
				a);
		return departmentList;
	}

	public List<Object[]> findUserRightsDepartmentRightsActionOther(User user) throws Exception {
		String userId = user.getId();
		Object[] a = { userId };
		List<Object[]> departmentList = commonDAO.find(
				"select a.role.id,b.rights.id,a.role.departmentType,b.rightsAction from UserRole a,RoleRights b where a.role = b.role   and a.user.id=?",
				a);
		return departmentList;
	}

	public List<Object[]> findUserRightsAction(User user) throws Exception {
		String userId = user.getId();
		Object[] a = { userId };
		List<Object[]> rl1 = commonDAO.find(
				"select a.role.id,b.rights.id,b.rightsAction from UserRole a,RoleRights b where a.role = b.role   and a.user.id=?",
				a);
		List<Object[]> rl2 = commonDAO.find(
				"select a.role.id,b.rights.levelId,b.rightsAction from UserRole a,RoleRights b where a.role = b.role   and a.user.id=?",
				a);
		rl1.addAll(rl2);

		return rl1;
	}

	/**
	 * 构建目录树Json文件
	 * 
	 * @param list
	 * @param node
	 */
	public void constructorJson(List<Rights> list, Rights treeNode) {
		String lan = (String) ServletActionContext.getRequest().getSession().getAttribute("lan");

		if (hasChild(list, treeNode)) {
			json.append("{\"id\":\"");
			json.append(treeNode.getId() + "\"");
			json.append(",\"text\":\"");

			if (lan.equals("en"))
				json.append(treeNode.getEnName() + "\"");
			else
				json.append(treeNode.getName() + "\"");

			json.append(",\"qtip\":\"");
			json.append(treeNode.getLeaf() + "\"");

			json.append(",\"leaf\":false");
			json.append(",\"contenthref\":\"");
			json.append(treeNode.getContenthref() + "\"");
			String orderNumber = String.valueOf(treeNode.getOrderNumber());
			if (orderNumber.length() == 1) {
				orderNumber = "0" + String.valueOf(treeNode.getOrderNumber());
			}
			json.append(",\"orderNumber\":\"");
			json.append(orderNumber + "\"");
			json.append(",\"children\":[");
			List<Rights> childList = getChildList(list, treeNode);
			Iterator<Rights> iterator = childList.iterator();
			while (iterator.hasNext()) {
				Rights node = (Rights) iterator.next();
				constructorJson(list, node);
			}
			json.append("]},");
		} else {
			json.append("{\"id\":\"");
			json.append(treeNode.getId() + "\"");
			json.append(",\"text\":\"");
			if (lan.equals("en"))
				json.append(treeNode.getEnName() + "\"");
			else
				json.append(treeNode.getName() + "\"");
			json.append(",\"qtip\":\"");
			json.append(treeNode.getLeaf() + "\"");
			json.append(",\"leaf\":true");
			// json.append(treeNode.getLeaf()+"");
			json.append(",\"contenthref\":\"");
			json.append(treeNode.getContenthref() + "\"");
			String orderNumber = String.valueOf(treeNode.getOrderNumber());

			if (orderNumber.length() == 1) {
				orderNumber = "0" + String.valueOf(treeNode.getOrderNumber());
			}
			json.append(",\"orderNumber\":\"");
			json.append(orderNumber + "\"");
			json.append("},");
		}
	}

	/**
	 * 获得子节点列表信息
	 * 
	 * @param list
	 * @param node
	 * @return
	 */
	public List<Rights> getChildList(List<Rights> list, Rights node) { // 得到子节点列表
		List<Rights> li = new ArrayList<Rights>();
		Iterator<Rights> it = list.iterator();
		while (it.hasNext()) {
			Rights n = (Rights) it.next();
			if (n.getParentId() != null && n.getParentId().equals(node.getId())) {
				li.add(n);
			}
		}
		return li;
	}

	/**
	 * 判断是否有子节点
	 * 
	 * @param list
	 * @param node
	 * @return
	 */
	public boolean hasChild(List<Rights> list, Rights node) {
		return getChildList(list, node).size() > 0 ? true : false;
	}

	public String getJson(List<Rights> list) {
		json = new StringBuffer();
		List<Rights> nodeList0 = new ArrayList<Rights>();
		// commonDAO.find("from Department where level='0'");
		Iterator<Rights> it1 = list.iterator();
		while (it1.hasNext()) {
			Rights node = (Rights) it1.next();
			if (node.getLevel() == 0) {
				nodeList0.add(node);
			}
		}
		Iterator<Rights> it = nodeList0.iterator();
		while (it.hasNext()) {
			Rights node = (Rights) it.next();
			constructorJson(list, node);
		}

		String jsonDate = json.toString();
		return ("[" + jsonDate + "]").replaceAll(",]", "]");
	}

	@Transactional(rollbackFor = Exception.class)
	public User modifyUser(User modifyUser, User user) throws Exception {
		String pwd = modifyUser.getPassword();

		String oldPwd = modifyUser.getOldPassword();
		if (MD5Util.getMD5Lower(oldPwd).equals(user.getUserPassword())) {

			if (pwd != null && pwd.length() > 0) {

				if (modifyUser.getPassword() != null && !modifyUser.getPassword().equals("")) {
					user.setUserPassword(MD5Util.getMD5Lower(pwd));
				}

				// user.setUserPassword(pwd);
			}
			user.setMobile(modifyUser.getMobile());
			user.setAddress(modifyUser.getAddress());
			if (modifyUser.getEmailPassword() != null && modifyUser.getEmailPassword().length() > 0) {
				user.setEmailPassword(modifyUser.getEmailPassword());
			}
			this.commonDAO.update(user);
			return user;
		} else {
			return null;
		}

	}

	@Transactional(rollbackFor = Exception.class)
	public void saveLogInfo(User user) throws Exception {

		LogInfo li = new LogInfo();
		li.setUserId(user.getId());
		li.setLogDate(new Date());
		li.setFileId(user.getId());
		li.setClassName("User");
		li.setState("4");
		li.setStateName("登录");
		li.setModifyContent("用户" + user.getName() + "登录系统。");
		this.commonDAO.saveOrUpdate(li);

	}

	public List<User> findUserWhereList(String user, String type) throws Exception {
		return mainDao.getUserWhereList(user, type);
	}

	// 按登录人查询待办情况
	public List selBacklog(ArrayList<String> modelI, ArrayList<String> modelN) throws Exception {
		return mainDao.findBacklog(modelI, modelN);
	}

	// 按登录人查询待办情况
	public List selAbnormal(ArrayList<String> bnormalI, ArrayList<String> bnormalN) throws Exception {
		return mainDao.findAbnormal(bnormalI, bnormalN);
	}

	// 通过用户ID查询用户角色
	public List<UserRole> selUserRole(String userId) throws Exception {
		return mainDao.findUserRole(userId);
	}

	// 通过角色查询所选择的待办数量模块
	public String selModelName(String roleId) throws Exception {
		return mainDao.finModelName(roleId);
	}

	// 通过角色查询所选择的待办数量模块Id
	public String selModelId(String roleId) throws Exception {
		return mainDao.finModelId(roleId);
	}

	// 通过角色查询所选择的异常信息模块
	public String selAbnormalName(String roleId) throws Exception {
		return mainDao.finAbnormalName(roleId);
	}

	// 通过角色查询所选择的异常信息模块Id
	public String selAbnormalId(String roleId) throws Exception {
		return mainDao.finAbnormalId(roleId);
	}

	// 点击待办数量或者异常数量跳转页面
	public String selUrl(String name, String type) throws Exception {
		return mainDao.finUrl(name, type);
	}

	// 查询当前的人是否为样本接收组
	public UserGroupUser selUserGroupId(String userID) {
		return mainDao.selUserGroupId(userID);
	}

	public List<StorageReagentBuySerial> getInventoryDataList(String userID, Date date) {
		return mainDao.getInventoryDataList(userID, date);
	}

	public List<Storage> getInventoryDataListForSafeNum(String userID) {
		return mainDao.getInventoryDataListForSafeNum(userID);
	}

	public List<InstrumentRepairPlanTask> getInstrumentDataList(String userID, Date date) {
		return mainDao.getInstrumentDataList(userID, date);
	}

	public List<Map<String, Object>> findSampleOrderDue() throws ParseException {
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		List<SampleOrder> SampleOrderList = mainDao.findSampleOrderDue();

		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Date date = new Date();
		long now = date.getTime();

		for (SampleOrder sampleOrder : SampleOrderList) {
			if (sampleOrder.getDrawBloodTime() != null) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(sampleOrder.getDrawBloodTime());
				calendar.add(Calendar.DAY_OF_MONTH, -1);
				System.out.println(sampleOrder.getDrawBloodTime());
				System.out.println(calendar.getTime());
				String format = fmt.format(calendar.getTime());
				format += " 09:00:00.0";
				System.out.println(format);
				Date sd1 = df.parse(format);
				long drawBloodTime = sampleOrder.getDrawBloodTime().getTime();
				long yesterdayDrawBloodTime = sd1.getTime();
				Map<String, Object> map = new HashMap<String, Object>();

				if (yesterdayDrawBloodTime <= now && now <= drawBloodTime) {
					// 提醒
					System.out.println("提醒");
					map.put("id", sampleOrder.getId());
					map.put("drawBloodTime", df.format(sampleOrder.getDrawBloodTime()));

					if (!"".equals(sampleOrder.getBarcode()) && sampleOrder.getBarcode() != null) {
						map.put("barcode", sampleOrder.getBarcode());
					} else {
						map.put("barcode", "");
					}
					if (!"".equals(sampleOrder.getCcoi()) && sampleOrder.getCcoi() != null) {
						map.put("ccoi", sampleOrder.getCcoi());
					} else {
						map.put("ccoi", "");
					}
					map.put("state", "提醒");
					list.add(map);

				}
				if (now >= drawBloodTime) {
					// 作废或者变更
					System.out.println("作废或者变更");

					map.put("id", sampleOrder.getId());
					map.put("drawBloodTime", df.format(sampleOrder.getDrawBloodTime()));
					if (!"".equals(sampleOrder.getBarcode()) && sampleOrder.getBarcode() != null) {
						map.put("barcode", sampleOrder.getBarcode());
					} else {
						map.put("barcode", "");
					}
					if (!"".equals(sampleOrder.getCcoi()) && sampleOrder.getCcoi() != null) {
						map.put("ccoi", sampleOrder.getCcoi());
					} else {
						map.put("ccoi", "");
					}
					map.put("state", "变更/作废");
					list.add(map);
				}

			}
		}
		return list;
	}

	public List<Map<String, Object>> findNotSampleReceive() throws ParseException {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		List<SampleOrder> sampleOrderList = mainDao.findNotSampleReceive();
		if (!sampleOrderList.isEmpty()) {
			for (SampleOrder sampleOrder : sampleOrderList) {
				Map<String, Object> map = new HashMap<String, Object>();
				if (!"".equals(sampleOrder.getId()) && sampleOrder.getId() != null) {
					List<SampleReceive> findSampleReceive = mainDao.findSampleReceive(sampleOrder.getId());
					if (!findSampleReceive.isEmpty()) {
						map.put("sampleReceiveId", findSampleReceive.get(0).getId());
					} else {
						map.put("sampleReceiveId", "");
					}
					map.put("id", sampleOrder.getId());
				} else {
					map.put("id", "");
				}
				if (!"".equals(sampleOrder.getBarcode()) && sampleOrder.getBarcode() != null) {
					map.put("barcode", sampleOrder.getBarcode());
				} else {
					map.put("barcode", "");
				}
				if (!"".equals(sampleOrder.getBarcode()) && sampleOrder.getBarcode() != null) {
					map.put("barcode", sampleOrder.getBarcode());
				} else {
					map.put("barcode", "");
				}
				if (sampleOrder.getDrawBloodTime() != null) {
					map.put("drawBloodTime", df.format(sampleOrder.getDrawBloodTime()));
				} else {
					map.put("drawBloodTime", "");
				}
				map.put("state", "接收未完成");
				list.add(map);
			}
		}

		return list;
	}

	public List<Map<String, Object>> findReturnDateConfirm() throws ParseException {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

		List<ReinfusionPlanItem> findReturnDateConfirm = mainDao.findReturnDateConfirm();
		if (!findReturnDateConfirm.isEmpty()) {
			for (ReinfusionPlanItem reinfusionPlanItem : findReturnDateConfirm) {
				Map<String, Object> map = new HashMap<String, Object>();
				if (!"".equals(reinfusionPlanItem.getBatch()) && reinfusionPlanItem.getBatch() != null) {
					map.put("batch", reinfusionPlanItem.getBatch());
				} else {
					map.put("batch", "");
				}
				if (!"".equals(reinfusionPlanItem.getReinfusionPlanDate())
						&& reinfusionPlanItem.getReinfusionPlanDate() != null) {
					map.put("reinfusionPlanDate", reinfusionPlanItem.getReinfusionPlanDate());
				} else {
					map.put("reinfusionPlanDate", "");
				}
				map.put("state", "日期待确认");
				list.add(map);
			}
		}

		return list;
	}

	public List<Map<String, Object>> findNoTransportPlan() throws ParseException {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat ff = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		long now = date.getTime();
		List<TransportOrderTemp> findNoTransportPlan = mainDao.findNoTransportPlan();
		if (!findNoTransportPlan.isEmpty()) {
			for (TransportOrderTemp transportOrderTemp : findNoTransportPlan) {
				Map<String, Object> map = new HashMap<String, Object>();
				if (!"".equals(transportOrderTemp.getReinfusionPlanDate())
						&& transportOrderTemp.getReinfusionPlanDate() != null) {
					String rfp = transportOrderTemp.getReinfusionPlanDate();
					rfp = rfp + " 10:00:00.0";
					Date reinfusionPlanDate = df.parse(rfp);
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(reinfusionPlanDate);
					calendar.add(Calendar.DAY_OF_MONTH, -1);
					String format = df.format(calendar.getTime());

					long rfoTime = reinfusionPlanDate.getTime();
					if (now >= rfoTime) {
						map.put("tixingTime", format);
						map.put("reinfusionPlanDate", transportOrderTemp.getReinfusionPlanDate());
						if (transportOrderTemp.getSampleOrder() != null) {
							if (!"".equals(transportOrderTemp.getSampleOrder().getBarcode())
									&& transportOrderTemp.getSampleOrder().getBarcode() != null) {
								map.put("barcode", transportOrderTemp.getSampleOrder().getBarcode());
							} else {
								map.put("barcode", "");
							}
						}
						map.put("state", "回输");
						list.add(map);

					}

				}

			}
		}
		List<SampleOrder> findNotSampleReceive = mainDao.findNotSampleReceive();
		if (!findNotSampleReceive.isEmpty()) {
			for (SampleOrder sampleOrder : findNotSampleReceive) {
				Map<String, Object> map = new HashMap<String, Object>();
				// 预计回输开始时间
				if (sampleOrder.getDrawBloodTime() != null) {
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(sampleOrder.getDrawBloodTime());
					calendar.add(Calendar.DAY_OF_MONTH, -1);
					String format = ff.format(calendar.getTime());
					format += " 10:00:00.0";
					Date planStimeDate = df.parse(format);
					long planStime = planStimeDate.getTime();
					if (now >= planStime) {
						map.put("tixingTime", format);
						map.put("reinfusionPlanDate", df.format(sampleOrder.getDrawBloodTime()));
						if (!"".equals(sampleOrder.getBarcode()) && sampleOrder.getBarcode() != null) {
							map.put("barcode", sampleOrder.getBarcode());
						} else {
							map.put("barcode", "");
						}
						map.put("state", "订单");
						list.add(map);
					}
				}

			}
		}
		return list;

	}

	public static boolean isObjectFieldEmpty(Object obj) throws Exception {
		Class<?> clazz = obj.getClass(); // 得到类对象
		Field[] fs = clazz.getDeclaredFields(); // 得到属性集合
		List<String> list = new ArrayList<String>();
		for (Field field : fs) { // 遍历属性
			String fname = (String) field.getName();
			// 送达楼层 最低温度 最高温度 平均温度 温度要求
			if (fname.equals("address") || fname.equals("lowTemperature") || fname.equals("highTemperature")
					|| fname.equals("avgTemperature") || fname.equals("transportTemperature") || fname.equals("cellula")
					|| fname.equals("confirmationUser") || fname.equals("phoneNumber")
					|| fname.equals("confirmationDate") || fname.equals("confirmationDate")) {
				field.setAccessible(true); // 设置属性是可以访问的（私有的也可以）
				if ("".equals(field.get(obj)) || field.get(obj) == null || "null".equals(field.get(obj))) {
//				String name = (String) field.getName();
//				System.out.println(name);
//				list.add(name);
					return true;
				}

			}
		}
		return false;
	}

	public List<Map<String, Object>> findTransportPlanBug() throws Exception {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
//		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		SimpleDateFormat ff = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		long now = date.getTime();
		List<TransportOrderCell> findTransportPlanBug = mainDao.findTransportPlanBug();
		for (TransportOrderCell transportOrderCell : findTransportPlanBug) {
			Map<String, Object> map = new HashMap<String, Object>();
			if (transportOrderCell.getTransportDate2() != null) {
				long time = transportOrderCell.getTransportDate2().getTime();
				if (now > time) {
					long findFileInfoCount = fileInfoService.findFileInfoCount(transportOrderCell.getId(),
							"transportOrderItem");
					if (findFileInfoCount == 0) {

						if (isObjectFieldEmpty(transportOrderCell)) {
							map.put("id", transportOrderCell.getTransportOrder().getId());
							if (!"".equals(transportOrderCell.getCode()) && transportOrderCell.getCode() != null) {
								map.put("samplecode", transportOrderCell.getCode());

							} else {
								map.put("samplecode", "");
							}
							map.put("state", "内容不完整");

							list.add(map);
						}

					}
				}

			}
		}

		return list;
	}
	public List<Map<String, Object>> findZhiJianSurplus(String userId) throws Exception {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		List<UserGroupUser> findUserGroupUser = mainDao.findUserGroupUser(userId);
		if(findUserGroupUser.size()>0) {
			
		
		for (UserGroupUser userGroupUser : findUserGroupUser) {
			if(userGroupUser!=null) {
				if(userGroupUser.getUserGroup()!=null) {
					String groupId = userGroupUser.getUserGroup().getId();
					
					List<Object[]> findZhiJianSurplus = mainDao.findZhiJianSurplus(groupId);
					if(findZhiJianSurplus.size()>0) {
					
					for (Object[] objects : findZhiJianSurplus) {
						Map<String, Object> map = new HashMap<String, Object>();
						map.put("sum",objects[0]);
						map.put("name",objects[1]);
						list.add(map);
					}
					
					}
				}
				
			}
			
		}
		}
		
		return list;
	}

	public List<InfluenceProduct> getInfluenceProductList() throws Exception {
		return mainDao.getInfluenceProductList();
	}

	public List<InfluenceMateriel> getPlanInfluenceMaterielList() {
		return mainDao.getPlanInfluenceMaterielList();
	}

	public List<InfluenceEquipment> getInfluenceEquipmentList() {
		return mainDao.getInfluenceEquipmentList();
	}

	public List<TransportOrderCell> getTransportOrderCell() {
		return mainDao.getTransportOrderCell();
	}

	public List<ReinfusionPlanItem> getReinfusionPlanItemUnconfirm() {
		return mainDao.getReinfusionPlanItemUnconfirm();
	}

	public List<SampleOrder> getSampleOrderUnconfirm() {
		return mainDao.getSampleOrderUnconfirm();
	}

	public List<CellPassageItem> getCellPassageItems(String code) {
		List<CellPassageItem> cpis = new ArrayList<CellPassageItem>();
		List<CellPassage> cps = commonService.get(CellPassage.class, "batch", code);
		if (cps.size() > 0) {
			CellPassage cp = cps.get(0);

			List<CellProductionRecord> cprs = mainDao.getCellProductionRecords(cp.getId());
			if (cprs.size() > 0) {
				CellProductionRecord cpr = cprs.get(0);
				cpis = mainDao.getCellPassageItems(cp.getId(), cpr.getOrderNum());
			}
		}
		return cpis;
	}

	public List<NoticeInformation> getNoticeInformationList() {
		return mainDao.getNoticeInformationList();
	}

	public List<CellProductionRecord> findCellProductionRecordByPici(String pici) {
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		List<CellProductionRecord> cprs = mainDao.findCellProductionRecordByPici(pici, user.getId());
		return cprs;
	}

	public List<CellProductionRecord> findCellProductionRecordByFjbh(String fjbh) {
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		List<CellProductionRecord> cprs = mainDao.findCellProductionRecordByFjbh(fjbh, user.getId());
		return cprs;
	}

	public List<CellProductionRecord> findCellProductionRecordByPiciStepOne(String pici) {
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		List<CellProductionRecord> cprs = mainDao.findCellProductionRecordByPiciStepOne(pici, user.getId());
		return cprs;
	}

	public List<CellPassageItem> findCellProductionRecordBySamples(String[] arr) {
		String arrs = "";
		for (String ar : arr) {
			if ("".equals(arrs)) {
				arrs = arrs + "'" + ar + "'";
			} else {
				arrs = arrs + ",'" + ar + "'";
			}
		}
		List<CellPassageItem> cpis = mainDao.findCellProductionRecordBySamples(arrs);
		return cpis;
	}
	/**
	 * 传递窗
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, Object>> findDeliveryWindow() throws Exception {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		List<TransferWindowState> transferWindowState = mainDao.findDeliveryWindow();
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> list2 = new ArrayList<Map<String, Object>>();
		for (TransferWindowState tws : transferWindowState) {
			if (tws.getQualityReceiveTime() == null) {
				Map<String, Object> map = new HashMap<String, Object>();

				// 检验样本编号
				if (!"".equals(tws.getCode()) && tws.getCode() != null) {
					map.put("code", tws.getCode());
				}
				// 批次号
				if (!"".equals(tws.getBatch()) && tws.getBatch() != null) {
					map.put("batch", tws.getBatch());
				}
				// 检测项
				if (!"".equals(tws.getSampleDetecyion().getName()) && tws.getSampleDetecyion().getName() != null) {
					map.put("jcxName", tws.getSampleDetecyion().getName());
				}
				// 提交时间
				if (tws.getQualitySubmitTime() != null) {
					map.put("tjName", df.format(tws.getQualitySubmitTime()));
				}
				// 状态
				if (!"".equals(tws.getSampleState()) && tws.getSampleState() != null) {
					map.put("sampleState", tws.getSampleState());
				}
				// 获取系统时间
				Date date = new Date();
				String newDate = df.format(date);
				long a = df.parse(newDate).getTime();
				// 提交时间
				Date sjDate = tws.getQualitySubmitTime();
				String tjDate = df1.format(sjDate);
				long b = df1.parse(tjDate).getTime();
				if (((a - b) / (1000 * 60)) < 30) {
					map.put("okko", "待接收");
					list.add(map);
				} else {
					map.put("okko", "超时");
					list2.add(map);
				}
				
			}
		}
		list2.addAll(list);
		return list2;
	}
	
	/**
	 * 细胞计数
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, Object>> findResultAll() throws Exception {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		// 生产质检明细表(提交)
		List<CellPassageQualityItem> cellPassageQualityItem = mainDao.findResultAll();
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> list2 = new ArrayList<Map<String, Object>>();
		for (CellPassageQualityItem cqi : cellPassageQualityItem) {

			if (cqi.getQualityFinishTime() == null) {
				Map<String, Object> map = new HashMap<String, Object>();
				// 传递窗子表
				if (cqi.getSampleNumber() != null) {
					TransferWindowState tws = mainDao.findTransferWindowState(cqi.getSampleNumber());
					if (tws != null) {
						TransferWindowManage twm = commonDAO.get(TransferWindowManage.class,
								tws.getTransferWindowManage().getId());
						if (twm != null) {
							// 区域
							if (twm.getRegionalManagement().getName() != null
									&& !"".equals(twm.getRegionalManagement().getName())) {
								map.put("qy", twm.getRegionalManagement().getName());
							}
							// 样本编号
							if (tws.getCode() != null && !"".equals(tws.getCode())) {
								map.put("code", tws.getCode());
							}
							// 检测项
							if (!"".equals(tws.getSampleDetecyion().getName())
									&& tws.getSampleDetecyion().getName() != null) {
								map.put("jcxName", tws.getSampleDetecyion().getName());
							}
							// 提交时间
							if (tws.getQualitySubmitTime() != null) {
								map.put("tjName", df.format(tws.getQualitySubmitTime()));
							}
							// 获取系统时间
							Date date = new Date();
							String newDate = df.format(date);
							long a = df.parse(newDate).getTime();
							// 提交时间
							Date sjDate = tws.getQualitySubmitTime();
							String tjDate = df1.format(sjDate);
							long b = df1.parse(tjDate).getTime();
							System.out.println(a);
							System.out.println((a - b) / (1000 * 60));
							if (((a - b) / (1000 * 60)) < 15) {
								map.put("okko", "待接收");
								list.add(map);
							} else {
								map.put("okko", "超时");
								list2.add(map);
							}

						}
					}

				}
			}
		}
		list2.addAll(list);
		return list2;
	}

	/**
	 * 检测项信息提示窗
	 * 
	 * @return
	 * @throws ParseException
	 */
	public List<Map<String, Object>> findTestItem() throws Exception {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		// 查询生产明细表
		List<CellPassageQualityItem> cqli = mainDao.findTestItem();
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> list2 = new ArrayList<Map<String, Object>>();
		for (CellPassageQualityItem cqqi : cqli) {
			Map<String, Object> map = new HashMap<String, Object>();
			//查质检结果表
			List<QualityTestInfo> qti = mainDao.findResult(cqqi.getSampleNumber());
			if(!qti.isEmpty()) {
				
			}else {
				if (cqqi.getSampleDeteyion() != null) {
					// 查询
					SampleDeteyion sd = mainDao.findSampleDeteyion(cqqi.getSampleDeteyion().getId());

					// 样本编号
					if (cqqi.getSampleNumber() != null && !"".equals(cqqi.getSampleNumber())) {
						map.put("code", cqqi.getSampleNumber());
					}

					// 批号
					if (!"".equals(cqqi.getBatch()) && cqqi.getBatch() != null) {
						map.put("batch", cqqi.getBatch());
					}

					// 检测项
					if (!"".equals(cqqi.getSampleDeteyion().getName()) && cqqi.getSampleDeteyion().getName() != null) {
						map.put("jcxName", cqqi.getSampleDeteyion().getName());
					}
					// 提交时间
					if (cqqi.getQualitySubmitTime() != null) {
						map.put("tjName", df.format(cqqi.getQualitySubmitTime()));
					}
					/**
					 * BPC,SPC,细胞总数，每袋细胞数，CD8每袋数量，细胞活率，细菌内毒素检查，快速无菌为3小时开始提醒 生物学活性，细胞表型要求为12个小时开始
					 * 最终成品的无菌，支原体快速无菌为30天开始提醒
					 */
					// 获取系统时间
					Date date = new Date();
					String newDate = df.format(date);
					long a = df.parse(newDate).getTime();
					// 提交时间
					Date sjDate = cqqi.getQualitySubmitTime();
					String tjDate = df1.format(sjDate);
					long b = df1.parse(tjDate).getTime();
					// 检测项提醒日期
					if (!"".equals(sd.getRemindDate()) && sd.getRemindDate() != null) {
						long remindTime = Long.valueOf(sd.getRemindDate());
						// 提醒状态
						if (((a - b) / (1000 * 60 * 60)) < remindTime) {
							map.put("okko", "待接收");
							list.add(map);
						} else {
							map.put("okko", "超时");
							list2.add(map);
						}

					}else {
						map.put("okko", "待接收");
					}

				}
				
			}

		}
		list2.addAll(list);
		return list2;
	}

}
