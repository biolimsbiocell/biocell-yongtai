package com.biolims.main.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;

import org.springframework.stereotype.Repository;

import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.core.model.user.UserGroupUser;
import com.biolims.core.model.user.UserRole;
import com.biolims.crm.project.model.Project;
import com.biolims.deviation.model.InfluenceEquipment;
import com.biolims.deviation.model.InfluenceMateriel;
import com.biolims.deviation.model.InfluenceProduct;
import com.biolims.equipment.model.InstrumentRepairPlanTask;
import com.biolims.experiment.cell.passage.model.CellPassageItem;
import com.biolims.experiment.cell.passage.model.CellPassageQualityItem;
import com.biolims.experiment.cell.passage.model.CellProductionRecord;
import com.biolims.experiment.noticeInformation.model.NoticeInformation;
import com.biolims.experiment.quality.model.QualityTestInfo;
import com.biolims.experiment.reinfusionPlan.model.ReinfusionPlanItem;
import com.biolims.experiment.transferWindowManage.model.TransferWindowState;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.model.SampleReceive;
import com.biolims.storage.model.Storage;
import com.biolims.storage.model.StorageReagentBuySerial;
import com.biolims.system.detecyion.model.SampleDeteyion;
import com.biolims.system.product.model.Product;
import com.biolims.tra.transport.model.TransportOrderCell;
import com.biolims.tra.transport.model.TransportOrderTemp;
import com.opensymphony.xwork2.ActionContext;

@Repository
public class MainDao extends CommonDAO {
	public List<User> getUserWhereList(String user, String type) throws Exception {
		return this.find("from User where id='" + user + "' and  scopeId like '%" + type + "%'");
	}

	// 根据用户名查询该用户的组
	public List<UserGroupUser> selGroupIds(String userIds) throws Exception {
		String hql = "from UserGroupUser t where 1=1 and user.id = '" + userIds + "'";
		List<UserGroupUser> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 查询项目主数据
	public List<Project> selProject() throws Exception {
		String hql = "from Project t where 1=1 ";
		String key = "";
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}

		List<Project> list = this.getSession().createQuery(hql + key).list();
		return list;
	}
	// //查询项目主数据条数
	// public String selProjectCounts1() throws Exception {
	// String sql = "select count(*) from t_Project t where 1=1";
	// List<Map<String, Object>> list = this.findForJdbc(sql);
	// String projectCounts = list.get(0).get("count(*)").toString();
	// return projectCounts;
	// }

	// 查询产品主数据
	public List<Product> selProduct() throws Exception {
		String hql = "from Product t where 1=1 ";
		List<Product> list = this.getSession().createQuery(hql).list();
		return list;
	}
	// //查询产品主数据条数
	// public String selProductCounts() throws Exception {
	// String sql = "select count(*) from sys_product t where 1=1";
	// List<Map<String, Object>> list = this.findForJdbc(sql);
	// String productCounts = list.get(0).get("count(*)").toString();
	// return productCounts;
	// }

	// 点击待办事项跳转至老页面
	public String selTable(String formName) throws Exception {
		String sql = "select t.pathName from ApplicationTypeTable t where 1=1 and t.id = '" + formName + "'";
		List<String> list = this.getSession().createQuery(sql).list();
		String result = "";
		if (list != null && list.size() > 0 && list.get(0) != null) {
			result = list.get(0).toString();
		}
		return result;
	}

	// 点击待办事项跳转至新页面
	public String selNewTable(String formName) throws Exception {
		String sql = "select t.newPathName from ApplicationTypeTable t where 1=1 and t.id = '" + formName + "'";
		List<String> list = this.getSession().createQuery(sql).list();
		String result = "";
		if (list != null && list.size() > 0 && list.get(0) != null) {
			result = list.get(0).toString();
		}
		return result;
	}

	// 点击信息预警查询明细
	public String selInformationItem(String tableId) throws Exception {
		String sql = "select t.pathName from ApplicationTypeTable t where 1=1 and t.id = '" + tableId + "'";
		List<String> list = this.getSession().createQuery(sql).list();
		String result = "";
		if (list != null && list.size() > 0 && list.get(0) != null) {
			result = list.get(0).toString();
		}
		return result;
	}

	// 信息预警
	public List selInformation(String userId) throws Exception {
		String hql = "from SysRemind t where 1=1 and t.handleUser = '" + userId + "' and t.state='0'";
		List list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 主页上方四个页面条数显示
	public String selSampleOrderCounts() throws Exception {
		String sql = "select count(*) from SampleOrder t where 1=1";
		String key = "";
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		String sampleOrderCounts = this.getSession().createQuery(sql + key).uniqueResult().toString();
		return sampleOrderCounts;
	}

	public String selSampleInfoCounts() throws Exception {
		String sql = "select count(*) from SampleInfo t where 1=1";
		String key = "";
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		String sampleOrderCounts = this.getSession().createQuery(sql + key).uniqueResult().toString();
		return sampleOrderCounts;
	}

	public String selTechReportCounts() throws Exception {
		String sql = "select count(*) from SampleReportItem  where 1=1 and state='2' and arc is null or arc !='1'";
		String key = "";
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and report.scopeId='" + scopeId + "'";
		}
		String sampleOrderCounts = this.getSession().createQuery(sql + key).uniqueResult().toString();
		return sampleOrderCounts;
	}

	public String selProjectCounts() throws Exception {
		String sql = "select count(*) from Project t where 1=1";
		String key = "";
		String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
		if (!"all".equals(scopeId)) {
			key += " and scopeId='" + scopeId + "'";
		}
		String sampleOrderCounts = this.getSession().createQuery(sql + key).uniqueResult().toString();
		return sampleOrderCounts;
	}

	// 按登录人查询待办情况
	public List findAbnormal(ArrayList<String> abnormalI, ArrayList<String> abnormalN) throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		for (int i = 0; i < abnormalI.size(); i++) {
			String sql = "select count(*) from " + abnormalI.get(i) + " t  where 1=1 and t.state = '1' ";
			String key = "";
			String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
			if (!"all".equals(scopeId)) {
				key += " and scopeId='" + scopeId + "'";
			}
			Object result = this.getSession().createQuery(sql + key).uniqueResult();
			String count = "";
			if (result != null && !"".equals(result)) {
				count = result.toString();
			}
			String abnormalName = abnormalN.get(i);
			map.put(abnormalName, count);
		}
		JSONArray json = JSONArray.fromObject(map);
		return json;
	}

	// 按登录人异常信息情况
	public List findBacklog(ArrayList<String> modelI, ArrayList<String> modelN) throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		for (int i = 0; i < modelI.size(); i++) {
			String sql = "select count(*) from " + modelI.get(i) + " t  where 1=1 and t.state = '1' ";
			String key = "";
			String scopeId = (String) ActionContext.getContext().getSession().get("scopeId");
			if (!"all".equals(scopeId)) {
				key += " and scope_id='" + scopeId + "'";
			}

			Object result = this.getSession().createSQLQuery(sql + key).uniqueResult();
			String count = "";
			if (result != null && !"".equals(result)) {
				count = result.toString();
			}
			String modelName = modelN.get(i);
			map.put(modelName, count);
		}
		JSONArray json = JSONArray.fromObject(map);
		return json;
	}

	// 通过用户ID查询用户角色
	public List<UserRole> findUserRole(String userId) throws Exception {

		String hql = "from UserRole t where 1=1 and t.user = '" + userId + "'";
		List<UserRole> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 通过角色查询所选择的待办数量模块
	public String finModelName(String roleId) throws Exception {
		String sql = "select t.modelName from Role t where t.id='" + roleId + "'";
		String modelName = (String) this.getSession().createQuery(sql).uniqueResult();
		return modelName;
	}

	// 通过角色查询所选择的待办数量模块
	public String finModelId(String roleId) throws Exception {
		String sql = "select t.modelId from Role t where t.id='" + roleId + "'";
		String modelId = (String) this.getSession().createQuery(sql).uniqueResult();
		return modelId;
	}

	// 通过角色查询所选择的异常信息模块
	public String finAbnormalName(String roleId) throws Exception {
		String sql = "select t.abnormalName from Role t where t.id='" + roleId + "'";
		String abnormalName = (String) this.getSession().createQuery(sql).uniqueResult();
		return abnormalName;
	}

	// 通过角色查询所选择的异常信息模块
	public String finAbnormalId(String roleId) throws Exception {
		String sql = "select t.abnormalId from Role t where t.id='" + roleId + "'";
		String abnormalId = (String) this.getSession().createQuery(sql).uniqueResult();
		return abnormalId;
	}

	// 点击待办数量或者异常数量跳转页面
	public String finUrl(String name, String type) throws Exception {
		String sql = "select t.note from DicType t where t.name='" + name + "' and t.type='" + type
				+ "' and t.state='1'";
		String abnormalId = (String) this.getSession().createQuery(sql).uniqueResult();
		return abnormalId;
	}

	public UserGroupUser selUserGroupId(String userID) {
		String hql = "from UserGroupUser where 1=1 and user.id='" + userID + "' and userGroup.id='SampleReceive'";
		UserGroupUser ugu = new UserGroupUser();
		ugu = (UserGroupUser) getSession().createQuery(hql).uniqueResult();
		return ugu;
	}

	public List<StorageReagentBuySerial> getInventoryDataList(String userID, Date date) {
		String sql = " from StorageReagentBuySerial "
				+ "where remindDate <= '"+date+"' and remindDate is not null and remindDate <> '' "
				+ "and storage.ifCall = '1'";
		List<StorageReagentBuySerial> list = this.getSession().createQuery(sql).list();
		return list;
	}
	
	public List<Storage> getInventoryDataListForSafeNum(String userID) {
		String sql ="from Storage where safeNum > num";
		List<Storage> list = this.getSession().createQuery(sql).list();
		return list;
	}

	public List<InstrumentRepairPlanTask> getInstrumentDataList(String userID, Date date) {
		String sql = "from InstrumentRepairPlanTask "
				+ "where instrument.tsDate <='"+date+"' and instrument.tsDate is not null and instrument.tsDate <> ''";
		List<InstrumentRepairPlanTask> list = this.getSession().createQuery(sql).list();
		return list;
	}
	public List<SampleOrder> findSampleOrderDue() {
		String sql = "from SampleOrder "
				+ "where stateName ='预订单'or  stateName ='新建'";
		List<SampleOrder> list = this.getSession().createQuery(sql).list();
		return list;
	}
	public List<SampleOrder> findNotSampleReceive() {
		String sql = "from SampleOrder "
				+ "where stateName ='预订单'";
		List<SampleOrder> list = this.getSession().createQuery(sql).list();
		return list;
	}
	public List<ReinfusionPlanItem> findReturnDateConfirm() {
		String sql = "from ReinfusionPlanItem "
				+ "where state ='2' order by createDateTime desc ";
		List<ReinfusionPlanItem> list = this.getSession().createQuery(sql).list();
		return list;
	}
	public List<TransportOrderTemp> findNoTransportPlan() {
		String sql = "from TransportOrderTemp where state='1' ";
		List<TransportOrderTemp> list = this.getSession().createQuery(sql).list();
		return list;
	}
	public List<TransportOrderCell> findTransportPlanBug() {
		String sql = "from TransportOrderCell where transportState='1'";
		List<TransportOrderCell> list = this.getSession().createQuery(sql).list();
		return list;
	}
	public List<Object[]> findZhiJianSurplus(String groupId) {
		String sql = "select count(qtt.sampleDeteyion.name),qtt.sampleDeteyion.name from QualityTestTemp qtt  where 1=1 and qtt.state='1' and qtt.cellType = '1' and qtt.sampleDeteyion.template.acceptUser.id='"+groupId+"'  group by qtt.sampleDeteyion.id";
		List<Object[]> list = this.getSession().createQuery(sql).list();
		return list;
	}
	public List<UserGroupUser> findUserGroupUser(String userID) {
		String sql = "from UserGroupUser where 1=1 and  user.id='"+userID+"'";
		List<UserGroupUser> list = this.getSession().createQuery(sql).list();
		return list;
	}
	public List<SampleReceive> findSampleReceive(String sampleOrderId) {
		String sql = "from SampleReceive "
				+ "where sampleOrder ='"+sampleOrderId+"'";
		List<SampleReceive> list = this.getSession().createQuery(sql).list();
		return list;
	}
	
	public List<InfluenceProduct> getInfluenceProductList() throws Exception {
		String hql = "from InfluenceProduct t where 1=1 and (deviationHandlingReport.approvalStatus is null or deviationHandlingReport.approvalStatus='' or deviationHandlingReport.approvalStatus!='1') ";
		List<InfluenceProduct> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<InfluenceMateriel> getPlanInfluenceMaterielList() {
		String hql = "from InfluenceMateriel t where 1=1 and (deviationHandlingReport.approvalStatus is null or deviationHandlingReport.approvalStatus='' or deviationHandlingReport.approvalStatus!='1') ";
		List<InfluenceMateriel> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<InfluenceEquipment> getInfluenceEquipmentList() {
		String hql = "from InfluenceEquipment t where 1=1 and (deviationHandlingReport.approvalStatus is null or deviationHandlingReport.approvalStatus='' or deviationHandlingReport.approvalStatus!='1') ";
		List<InfluenceEquipment> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<TransportOrderCell> getTransportOrderCell() {
		String hql = "from TransportOrderCell where 1=1 and state='1' and (transportState is null or transportState='' or transportState='0') and (efficacious is null or efficacious='' )";
		List<TransportOrderCell> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<ReinfusionPlanItem> getReinfusionPlanItemUnconfirm() {
		String hql = "from ReinfusionPlanItem where 1=1 and (feedBack is null or feedBack='' or feedBack='0') ";
		List<ReinfusionPlanItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<SampleOrder> getSampleOrderUnconfirm() {
		String hql = "from SampleOrder t where t.state='1' and t.stateName='完成' and t.barcode not in(select z.batch from ReinfusionPlanItem z) ";
		List<SampleOrder> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<CellProductionRecord> getCellProductionRecords(String id) {
		String hql = "from CellProductionRecord where 1=1 and cellPassage.id='"+id+"' and templeItem.harvest='1' ORDER BY orderNum desc ";
		List<CellProductionRecord> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<CellPassageItem> getCellPassageItems(String id, String orderNum) {
		String hql = "from CellPassageItem where 1=1 and cellPassage.id='"+id+"' and stepNum='"+orderNum+"' ";
		List<CellPassageItem> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	public List<CellPassageItem> getCellPassageItemsByOne(String id, String orderNum) {
		String hql = "from CellPassageItem where 1=1 and cellPassage.id='"+id+"' and stepNum='"+orderNum+"' and blendState='0' ";
		List<CellPassageItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<NoticeInformation> getNoticeInformationList() {
		String hql = "from NoticeInformation where 1=1 and stateName='已发布' and date_format(startDate,'%Y-%m-%d')<=date_format(NOW(),'%Y-%m-%d') and date_format(endDate,'%Y-%m-%d')>=date_format(NOW(),'%Y-%m-%d') ";
		List<NoticeInformation> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	public List<CellProductionRecord> findCellProductionRecordByPici(String pici,String userid) {
		String hql = "from CellProductionRecord where 1=1 and cellPassage.batch='"+pici+"' and templeOperator.id='"+userid+"' and (endTime is null or endTime='') ORDER BY orderNum asc ";
		List<CellProductionRecord> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	public List<CellProductionRecord> findCellProductionRecordByFjbh(String fjbh,String userid) {
		String hql = "from CellProductionRecord where 1=1 and operatingRoomId='"+fjbh+"' and templeOperator.id='"+userid+"' and (endTime is null or endTime='') ORDER BY orderNum asc ";
		List<CellProductionRecord> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	public List<CellPassageItem> findCellProductionRecordBySamples(String arr) {
		String hql = "from CellPassageItem where 1=1 and code in ("+arr+") and blendState='0' and stepNum='1' ORDER BY stepNum asc ";
		List<CellPassageItem> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
	public List<CellProductionRecord> findCellProductionRecordByPiciStepOne(String pici,String userid) {
		String hql = "from CellProductionRecord where 1=1 and cellPassage.batch='"+pici+"' and templeOperator.id='"+userid+"' and orderNum='1' and (endTime is null or endTime='') ORDER BY orderNum asc ";
		List<CellProductionRecord> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<TransferWindowState> findDeliveryWindow() {
		String hql = "from TransferWindowState where 1=1 and qualityReceiveTime is null";
		List<TransferWindowState> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public List<CellPassageQualityItem> findResultAll() {
		String hql = "from CellPassageQualityItem where 1=1 and qualityFinishTime is null and qualitySubmitTime is not null and sampleDeteyion.yesOrNo='1' ";
		List<CellPassageQualityItem> list = this.getSession().createQuery(hql).list();
		return list;
	}


	public TransferWindowState findTransferWindowState(String sampleNumber) {
		String hql = "from TransferWindowState where 1=1 and code='"+sampleNumber+"'";
		TransferWindowState list = (TransferWindowState) this.getSession().createQuery(hql).uniqueResult();
		return list;
	}

	public List<CellPassageQualityItem> findTestItem() {
		String hql = "from CellPassageQualityItem where 1=1 and qualityFinishTime is null and qualitySubmitTime is not null and sampleDeteyion.yesOrNo2='1' and stepNum = '8'";
		List<CellPassageQualityItem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	public SampleDeteyion findSampleDeteyion(String id) {
		String hql = "from SampleDeteyion where 1=1 and id='"+id+"'";
		SampleDeteyion list = (SampleDeteyion) this.getSession().createQuery(hql).uniqueResult();
		return list;
	}

	public List<QualityTestInfo> findResult(String sampleNumber) {
		String hql = "from QualityTestInfo where 1=1 and sampleNumber='"+sampleNumber+"'";
		List<QualityTestInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	

}
