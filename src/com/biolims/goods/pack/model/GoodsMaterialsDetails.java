package com.biolims.goods.pack.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.dic.model.DicUnit;
/**   
 * @Title: Model
 * @Description: 物料明细
 * @author lims-platform
 * @date 2015-11-12 11:07:15
 * @version V1.0   
 *
 */
@Entity
@Table(name = "GOODS_MATERIALS_DETAILS")
@SuppressWarnings("serial")
public class GoodsMaterialsDetails extends EntityDao<GoodsMaterialsDetails> implements java.io.Serializable {
	/**序号*/
	private String id;
	/**数量*/
	private Integer count;
	/**描述*/
	private String name;
	/**备注*/
	private String note;
	//编号
	private String code;
	
	//单位
	private DicUnit unit;
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "UNIT")
	public DicUnit getUnit() {
		return unit;
	}
	public void setUnit(DicUnit unit) {
		this.unit = unit;
	}
	private DicType mateType;
	/**相关主表*/
	private GoodsMaterialsPack goodsMaterialsPack;
	/**
	 *方法: 取得String
	 *@return: String  序号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  序号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  数量
	 */

	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 150)
	public String getName(){
		return this.name;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得GoodsMaterialsDetails
	 *@return: GoodsMaterialsDetails  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GOODS_MATERIALS_PACK")
	public GoodsMaterialsPack getGoodsMaterialsPack(){
		return this.goodsMaterialsPack;
	}
	/**
	 *方法: 设置GoodsMaterialsDetails
	 *@param: GoodsMaterialsDetails  相关主表
	 */
	public void setGoodsMaterialsPack(GoodsMaterialsPack goodsMaterialsPack){
		this.goodsMaterialsPack = goodsMaterialsPack;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TYPE")
	public DicType getMateType() {
		return mateType;
	}
	public void setMateType(DicType mateType) {
		this.mateType = mateType;
	}
	
}