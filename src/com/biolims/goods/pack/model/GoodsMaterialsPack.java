package com.biolims.goods.pack.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.system.product.model.Product;
import com.biolims.system.work.model.WorkType;

/**
 * @Title: Model
 * @Description: 物料包
 * @author lims-platform
 * @date 2015-11-12 11:07:27
 * @version V1.0
 * 
 */
@Entity
@Table(name = "GOODS_MATERIALS_PACK")
@SuppressWarnings("serial")
public class GoodsMaterialsPack extends EntityDao<GoodsMaterialsPack> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 名称 */
	private String name;
	/** 创建人 */
	private User createUser;
	/** 创建日期 */
	private Date createDate;
	/** 业务类型 */
	private Product product;
	/** 样本类型 */
	private DicType sampleType;
	/** 采血管类型 */
	private DicType bloodTube;
	/** 是否需要条码 */
	private String isCode;
	/** 默认数量 */
	private Integer num;
	/** 检测项目编号 */
	private String productId;
	/** 检测项目名称 */
	private String productName;

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	// 状态
	private String state;
	private String stateName;

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 编号
	 */

	@Id
	@Column(name = "ID", length = 50)
	public String getId() {
		return this.id;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 名称
	 */
	@Column(name = "NAME", length = 100)
	public String getName() {
		return this.name;
	}

	/**
	 * 方法: 设置String
	 * 
	 * @param: String 名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser() {
		return createUser;
	}

	public void setCreateUser(User createUser) {
		this.createUser = createUser;
	}

	/**
	 * 方法: 取得String
	 * 
	 * @return: String 创建日期
	 */
	@Column(name = "CREATE_DATE", length = 50)
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PRODUCT")
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_TYPE")
	public DicType getSampleType() {
		return sampleType;
	}

	public void setSampleType(DicType sampleType) {
		this.sampleType = sampleType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "BLOOD_TUBE")
	public DicType getBloodTube() {
		return bloodTube;
	}

	public void setBloodTube(DicType bloodTube) {
		this.bloodTube = bloodTube;
	}

	public String getIsCode() {
		return isCode;
	}

	public void setIsCode(String isCode) {
		this.isCode = isCode;
	}

}