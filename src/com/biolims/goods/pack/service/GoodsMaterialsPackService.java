package com.biolims.goods.pack.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.dic.model.DicType;
import com.biolims.goods.pack.dao.GoodsMaterialsPackDao;
import com.biolims.goods.pack.model.GoodsMaterialsDetails;
import com.biolims.goods.pack.model.GoodsMaterialsPack;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.system.organize.model.ApplyOrganizeAddress;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class GoodsMaterialsPackService {
	@Resource
	private GoodsMaterialsPackDao goodsMaterialsPackDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findGoodsMaterialsPackList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return goodsMaterialsPackDao.selectGoodsMaterialsPackList(mapForQuery, startNum, limitNum, dir, sort);
	}
	public Map<String, Object> findGoodsMaterialsPackListByState(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return goodsMaterialsPackDao.selectGoodsMaterialsPackListByState(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(GoodsMaterialsPack i) throws Exception {

		goodsMaterialsPackDao.saveOrUpdate(i);

	}
	public GoodsMaterialsPack get(String id) {
		GoodsMaterialsPack goodsMaterialsPack = commonDAO.get(GoodsMaterialsPack.class, id);
		return goodsMaterialsPack;
	}
	public Map<String, Object> findGoodsMaterialsDetailsList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = goodsMaterialsPackDao.selectGoodsMaterialsDetailsList(scId, startNum, limitNum, dir, sort);
		List<GoodsMaterialsDetails> list = (List<GoodsMaterialsDetails>) result.get("list");
		return result;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveGoodsMaterialsDetails(GoodsMaterialsPack sc, String itemDataJson) throws Exception {
		List<GoodsMaterialsDetails> saveItems = new ArrayList<GoodsMaterialsDetails>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			GoodsMaterialsDetails scp = new GoodsMaterialsDetails();
			// 将map信息读入实体类
			scp = (GoodsMaterialsDetails) goodsMaterialsPackDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setGoodsMaterialsPack(sc);
			saveItems.add(scp);
		}
		goodsMaterialsPackDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delGoodsMaterialsDetails(String[] ids) throws Exception {
		for (String id : ids) {
			GoodsMaterialsDetails scp =  goodsMaterialsPackDao.get(GoodsMaterialsDetails.class, id);
			 goodsMaterialsPackDao.delete(scp);
		}
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(GoodsMaterialsPack sc, Map jsonMap) throws Exception {
		if (sc != null) {
			goodsMaterialsPackDao.saveOrUpdate(sc);
		
			String jsonStr = "";
			jsonStr = (String)jsonMap.get("goodsMaterialsDetails");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveGoodsMaterialsDetails(sc, jsonStr);
			}
		}
   }
	//ajax加载物料包明细
	public List<Map<String, String>> selectSamplePack(String code) throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
			Map<String, Object> result = goodsMaterialsPackDao.selectSamplePack(code);
			List<GoodsMaterialsPack> list = (List<GoodsMaterialsPack>) result.get("list");

			if (list != null && list.size() > 0) {
				for (GoodsMaterialsPack srai : list) {
					Map<String, String> map = new HashMap<String, String>();
					map.put("isCode", srai.getIsCode());
					mapList.add(map);
				}
			}
		return mapList;
		
	}
	
	//ajax加载物料包明细
		public List<Map<String, String>> showPack(String code) throws Exception {
			List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
				Map<String, Object> result = goodsMaterialsPackDao.showPack(code);
				List<GoodsMaterialsDetails> list = (List<GoodsMaterialsDetails>) result.get("list");

				if (list != null && list.size() > 0) {
					for (GoodsMaterialsDetails srai : list) {
						Map<String, String> map = new HashMap<String, String>();
						map.put("mid", srai.getGoodsMaterialsPack().getId());
						map.put("mname", srai.getGoodsMaterialsPack().getName());
						if(srai.getGoodsMaterialsPack().getNum()!=null){
							map.put("mnum",srai.getGoodsMaterialsPack().getNum().toString());
						}else{
							map.put("mnum","");
						}
						
						map.put("id", srai.getId());
						map.put("name", srai.getName());
						map.put("code", srai.getCode());
						
						if(srai.getCount()!=null){
							map.put("count", srai.getCount().toString());
						}else{
							map.put("count", "");
						}
						
						if(srai.getMateType()!=null){
							map.put("mateType", srai.getMateType().getName());
						}else{
							map.put("mateType", "");
						}
					
						if(srai.getUnit()!=null){
							map.put("unit", srai.getUnit().getName());
						}else{
							map.put("unit", "");
						}
						mapList.add(map);
					}
				}
			return mapList;
			
		}
}
