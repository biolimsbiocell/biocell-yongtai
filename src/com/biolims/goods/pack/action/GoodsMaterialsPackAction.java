﻿package com.biolims.goods.pack.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.annotation.Resource;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.goods.mate.model.GoodsMaterialsApply;
import com.biolims.goods.pack.model.GoodsMaterialsDetails;
import com.biolims.goods.pack.model.GoodsMaterialsPack;
import com.biolims.goods.pack.service.GoodsMaterialsPackService;
import com.biolims.system.organize.model.ApplyOrganize;
import com.biolims.util.DateHelper;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.util.HttpUtils;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;

@Namespace("/goods/pack/goodsMaterialsPack")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class GoodsMaterialsPackAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "9006";
	@Autowired
	private GoodsMaterialsPackService goodsMaterialsPackService;
	private GoodsMaterialsPack goodsMaterialsPack = new GoodsMaterialsPack();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private CodingRuleService codingRuleService;

	@Action(value = "showGoodsMaterialsPackList")
	public String showGoodsMaterialsPackList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/goods/pack/goodsMaterialsPack.jsp");
	}

	@Action(value = "showGoodsMaterialsPackListJson")
	public void showGoodsMaterialsPackListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = goodsMaterialsPackService
				.findGoodsMaterialsPackList(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<GoodsMaterialsPack> list = (List<GoodsMaterialsPack>) result
				.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("sampleType-id", "");
		map.put("sampleType-name", "");
		map.put("product-id", "");
		map.put("product-name", "");
		map.put("bloodTube-id", "");
		map.put("bloodTube-name", "");
		map.put("isCode", "");
		map.put("num", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("productId", "");
		map.put("productName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "goodsMaterialsPackSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogGoodsMaterialsPackList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/goods/pack/goodsMaterialsPackDialog.jsp");
	}

	@Action(value = "showDialogGoodsMaterialsPackListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogGoodsMaterialsPackListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		else
			map2Query.put("state", "1");
		Map<String, Object> result = goodsMaterialsPackService
				.findGoodsMaterialsPackList(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<GoodsMaterialsPack> list = (List<GoodsMaterialsPack>) result
				.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("sampleType-id", "");
		map.put("sampleType-name", "");
		map.put("product-id", "");
		map.put("product-name", "");
		map.put("bloodTube-id", "");
		map.put("bloodTube-name", "");
		map.put("isCode", "");
		map.put("num", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("productId", "");
		map.put("productName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	// 查询完成状态的物料包主数据
	@Action(value = "goodsMaterialsPackSelectByState", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogGoodsMaterialsPackListByState() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/goods/pack/goodsMaterialsPackDialogByState.jsp");
	}

	@Action(value = "showDialogGoodsMaterialsPackListByStateJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogGoodsMaterialsPackListByStateJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		// else
		// map2Query.put("state", "1");
		Map<String, Object> result = goodsMaterialsPackService
				.findGoodsMaterialsPackListByState(map2Query, startNum,
						limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<GoodsMaterialsPack> list = (List<GoodsMaterialsPack>) result
				.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("sampleType-id", "");
		map.put("sampleType-name", "");
		map.put("product-id", "");
		map.put("product-name", "");
		map.put("bloodTube-id", "");
		map.put("bloodTube-name", "");
		map.put("num", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("productId", "");
		map.put("productName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editGoodsMaterialsPack")
	public String editGoodsMaterialsPack() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			goodsMaterialsPack = goodsMaterialsPackService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "goodsMaterialsPack");
		} else {
			goodsMaterialsPack.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			goodsMaterialsPack.setCreateUser(user);
			goodsMaterialsPack.setCreateDate(new Date());
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/goods/pack/goodsMaterialsPackEdit.jsp");
	}

	@Action(value = "copyGoodsMaterialsPack")
	public String copyGoodsMaterialsPack() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		goodsMaterialsPack = goodsMaterialsPackService.get(id);
		goodsMaterialsPack.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/goods/pack/goodsMaterialsPackEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = goodsMaterialsPack.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "GoodsMaterialsPack";
			String markCode = "WLB";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			goodsMaterialsPack.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("goodsMaterialsDetails",
				getParameterFromRequest("goodsMaterialsDetailsJson"));

		goodsMaterialsPackService.save(goodsMaterialsPack, aMap);
		return redirect("/goods/pack/goodsMaterialsPack/editGoodsMaterialsPack.action?id="
				+ goodsMaterialsPack.getId());

	}

	@Action(value = "viewGoodsMaterialsPack")
	public String toViewGoodsMaterialsPack() throws Exception {
		String id = getParameterFromRequest("id");
		goodsMaterialsPack = goodsMaterialsPackService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/goods/pack/goodsMaterialsPackEdit.jsp");
	}

	@Action(value = "showGoodsMaterialsDetailsList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showGoodsMaterialsDetailsList() throws Exception {
		return dispatcher("/WEB-INF/page/goods/pack/goodsMaterialsDetails.jsp");
	}

	@Action(value = "showGoodsMaterialsDetailsListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showGoodsMaterialsDetailsListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = goodsMaterialsPackService
					.findGoodsMaterialsDetailsList(scId, startNum, limitNum,
							dir, sort);
			Long total = (Long) result.get("total");
			List<GoodsMaterialsDetails> list = (List<GoodsMaterialsDetails>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("count", "");
			map.put("name", "");
			map.put("code", "");
			map.put("mateType-id", "");
			map.put("mateType-name", "");
			map.put("unit-id", "");
			map.put("unit-name", "");
			map.put("note", "");
			map.put("goodsMaterialsPack-name", "");
			map.put("goodsMaterialsPack-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "showGoodsMaterialsDetailsListById", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showGoodsMaterialsDetailsListById() throws Exception {
		String code = getRequest().getParameter("code");
		putObjToContext("code", code);
		return dispatcher("/WEB-INF/page/goods/pack/goodsMaterialsDetails1.jsp");
	}

	@Action(value = "showGoodsMaterialsDetailsListJsonById", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showGoodsMaterialsDetailsListJsonById() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("code");
			Map<String, Object> result = goodsMaterialsPackService
					.findGoodsMaterialsDetailsList(scId, startNum, limitNum,
							dir, sort);
			Long total = (Long) result.get("total");
			List<GoodsMaterialsDetails> list = (List<GoodsMaterialsDetails>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("count", "");
			map.put("name", "");
			map.put("code", "");
			map.put("mateType-id", "");
			map.put("mateType-name", "");
			map.put("unit-id", "");
			map.put("unit-name", "");
			map.put("note", "");
			map.put("goodsMaterialsPack-name", "");
			map.put("goodsMaterialsPack-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delGoodsMaterialsDetails")
	public void delGoodsMaterialsDetails() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			goodsMaterialsPackService.delGoodsMaterialsDetails(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public GoodsMaterialsPackService getGoodsMaterialsPackService() {
		return goodsMaterialsPackService;
	}

	public void setGoodsMaterialsPackService(
			GoodsMaterialsPackService goodsMaterialsPackService) {
		this.goodsMaterialsPackService = goodsMaterialsPackService;
	}

	public GoodsMaterialsPack getGoodsMaterialsPack() {
		return goodsMaterialsPack;
	}

	public void setGoodsMaterialsPack(GoodsMaterialsPack goodsMaterialsPack) {
		this.goodsMaterialsPack = goodsMaterialsPack;
	}

	// 双击物料包，加载明细数据
	@Action(value = "selectSamplePack")
	public void selectSamplePack() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String code = getParameterFromRequest("code");
			List<Map<String, String>> dataListMap = this.goodsMaterialsPackService
					.selectSamplePack(code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 双击物料包，加载明细数据
	@Action(value = "showPack")
	public void showPack() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String code = getParameterFromRequest("idsss");
			List<Map<String, String>> dataListMap = this.goodsMaterialsPackService
					.showPack(code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
