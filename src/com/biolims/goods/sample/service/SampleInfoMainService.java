package com.biolims.goods.sample.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.goods.mate.dao.GoodsMaterialsApplyDao;
import com.biolims.goods.mate.dao.GoodsMaterialsReadyDao;
import com.biolims.goods.mate.model.GoodsMaterialsApply;
import com.biolims.goods.mate.model.GoodsMaterialsReady;
import com.biolims.goods.mate.model.GoodsMaterialsReadyDetails;
import com.biolims.goods.mate.model.GoodsMaterialsSend;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.goods.sample.model.GoodsSampleWay;
import com.biolims.goods.sample.model.SampleInfoMain;
import com.biolims.goods.sample.model.SampleInfoSystem;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.system.work.model.WorkType;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class SampleInfoMainService {
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	@Resource
	private GoodsMaterialsReadyDao goodsMaterialsReadyDao;
	@Resource
	private GoodsMaterialsApplyDao goodsMaterialsApplyDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();

	public Map<String, Object> findSampleInfoMainList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return sampleInfoMainDao.selectSampleInfoMainList(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	public Map<String, Object> findSampleInfoMainList1(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return sampleInfoMainDao.selectSampleInfoMainList1(mapForQuery,
				startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleInfoMain i) throws Exception {

		sampleInfoMainDao.saveOrUpdate(i);

	}

	public SampleInfoMain get(String id) {
		SampleInfoMain sampleInfoMain = commonDAO.get(SampleInfoMain.class, id);
		return sampleInfoMain;
	}

	public Map<String, Object> findSampleInfoSystemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = sampleInfoMainDao
				.selectSampleInfoSystemList(scId, startNum, limitNum, dir, sort);
		List<SampleInfoSystem> list = (List<SampleInfoSystem>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findSampleInfoList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = sampleInfoMainDao.selectSampleInfoList(
				scId, startNum, limitNum, dir, sort);
		List<SampleOrder> list = (List<SampleOrder>) result.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleInfoSystem(SampleInfoMain sc, String itemDataJson)
			throws Exception {
		List<SampleInfoSystem> saveItems = new ArrayList<SampleInfoSystem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			SampleInfoSystem scp = new SampleInfoSystem();
			// 将map信息读入实体类
			scp = (SampleInfoSystem) sampleInfoMainDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setSampleInfoMain(sc);

			saveItems.add(scp);
		}
		sampleInfoMainDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSampleInfoSystem(String[] ids) throws Exception {
		for (String id : ids) {
			SampleInfoSystem scp = sampleInfoMainDao.get(
					SampleInfoSystem.class, id);
			sampleInfoMainDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveSampleInfo(SampleInfoMain sc, String itemDataJson)
			throws Exception {
		List<SampleOrder> saveItems = new ArrayList<SampleOrder>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		for (Map<String, Object> map : list) {
			SampleOrder scp = new SampleOrder();
			// 将map信息读入实体类
			scp = (SampleOrder) sampleInfoMainDao.Map2Bean(map, scp);
			scp.setSampleInfoMain(sc.getId());
			scp.setCreateDate(new Date());
			scp.setCreateUser(user);
			saveItems.add(scp);
		}
		sampleInfoMainDao.saveOrUpdateAll(saveItems);
		// String d1 = sc.getId();
		// List<SampleInfo> list1 = sampleInfoMainDao.setSampleInfo(d1);
		// // String type="";
		// for (SampleInfo gs : list1) {
		// String id = gs.getId();
		// String code = gs.getCode().substring(0, 1);
		//
		// saveWorkType(code, id);
		// }
		// //根据检测方法获取主数据
		// String type=list1.get(0).getBusinessType().getId();
		// List<SamplePackSystem> list2=sampleInfoMainDao.selectSystem(type);
		// if(list2.size()>0){
		// String ssId=list2.get(0).getId();
		// //根据取样数据获取体系明细
		// List<SamplePackSystemItem> list3=sampleInfoMainDao.saveSystem(ssId);
		// List<SampleInfoSystem>
		// list4=sampleInfoMainDao.setSampleInfoSystem(d1);
		// if(list4.size()==0){
		// for(SamplePackSystemItem wt:list3){
		// SampleInfoSystem gs = new SampleInfoSystem();
		// gs.setName(wt.getName());
		// gs.setNum(wt.getNum());
		// gs.setSampleInfoMain(sc);
		// sampleInfoMainDao.saveOrUpdate(gs);
		// }
		// }
		// }

	}

	//
	// // 根据标识符保存检测方法
	// @WriteOperLog
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void saveWorkType(String code, String id) throws Exception {
	// List<WorkType> list = sampleInfoMainDao.saveWorkType(code);
	// for (WorkType wt : list) {
	// SampleInfo gs = sampleInfoMainDao.get(SampleInfo.class, id);
	// gs.setBusinessType(wt);
	// }
	// }

	// //根据检测方法保存体系
	// @WriteOperLog
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void saveSystem(String code) throws Exception{
	// List<SamplePackSystemItem> list=sampleInfoMainDao.saveSystem(code);
	// for(SamplePackSystemItem wt:list){
	// SampleInfoSystem gs = new SampleInfoSystem();
	// gs.setName(wt.getName());
	// gs.setNum(wt.getNum());
	// }
	// }
	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delSampleInfo(String[] ids) throws Exception {
		for (String id : ids) {
			SampleInfo scp = sampleInfoMainDao.get(SampleInfo.class, id);
			sampleInfoMainDao.delete(scp);
		}
	}

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(SampleInfoMain sc, Map jsonMap) throws Exception {
		if (sc != null) {
			sampleInfoMainDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("sampleInfoSystem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSampleInfoSystem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("sampleInfo");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveSampleInfo(sc, jsonStr);
			}
		}
	}

	// 打包保存
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void savePackage(SampleInfoMain sc) throws Exception {
		sampleInfoMainDao.saveOrUpdate(sc);
	}

	// 根据打包单加载子表
	public List<Map<String, String>> showSamplePackItemList(String code)
			throws Exception {

		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = sampleInfoMainDao.setSampleInfoList(code);
		List<SampleOrder> list = (List<SampleOrder>) result.get("list");

		if (list != null && list.size() > 0) {
			for (SampleOrder srai : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", srai.getId());
				map.put("name", srai.getName());
				map.put("sampleCode", srai.getId());
				map.put("productName", srai.getProductName());
				map.put("productId", srai.getProductId());
				mapList.add(map);
			}
		}

		return mapList;
	}

	// 根据打包单加载体系
	public List<Map<String, String>> showSampleSystemItemList(String code)
			throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = sampleInfoMainDao
				.setSampleInfoSystemList(code);
		List<SampleInfoSystem> list = (List<SampleInfoSystem>) result
				.get("list");

		if (list != null && list.size() > 0) {
			for (SampleInfoSystem srai : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", srai.getId());
				map.put("name", srai.getName());
				map.put("num", srai.getNum().toString());
				mapList.add(map);
			}
		}
		return mapList;
	}

	// 审批完成
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void changeState(String applicationTypeActionId, String id) {
		SampleInfoMain sct = sampleInfoMainDao.get(SampleInfoMain.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		// 格式化日期
		Date d = new Date();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String stime = format.format(d);
		sct.setSendTime(stime);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);

		sampleInfoMainDao.update(sct);

		saveToGoodsSampleWay(sct.getId());

		saveToSampleInfoState(sct.getId());
		// -========ly 2015-12-10
		saveToSampleInfoStatePack(sct.getId());

		// saveToSampleReceive(sct.getId());
	}

	// ========
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveToSampleInfoStatePack(String id) {
		List<SampleOrder> list = sampleInfoMainDao.setSampleOrder(id);
		for (SampleOrder si : list) {
			String code = si.getId();
			List<GoodsMaterialsReadyDetails> readDetailList = goodsMaterialsReadyDao
					.getReadDetailsCode(code);
			for (GoodsMaterialsReadyDetails rd : readDetailList) {
				rd.setState("55");
				// 设置已使用状态
				rd.setUseState("1");
				// 获得有效期进行比较
				// Date date=rd.getValidity();
				// if(!date.equals("")){
				// Date d=new Date();
				// Calendar c1 = Calendar.getInstance();
				// Calendar c2 = Calendar.getInstance();
				// c1.setTime(date);
				// c2.setTime(d);
				//
				// int result = c1.compareTo(c2);

				// if (result <=0){
				// //设置超期状态
				// rd.setOutState("1");
				// }
				// }
				// 根据取样打包编号依次找到物料申请的物料包的未使用数量，减去取样使用的数量
				GoodsMaterialsSend gr = rd.getGoodsMaterialsSend();
				GoodsMaterialsApply ga = gr.getGoodsMaterialsApply();
				String packCode = rd.getPackCode();
				// GoodsMaterialsApplyItem
				// gai=goodsMaterialsApplyDao.getMaterialsPack(ga.getId(),packCode);
				// gai.setUnuseNum(gai.getUnuseNum()-1);
			}
			// GoodsMaterialsReadyDetails
			// grd=goodsMaterialsReadyDao.getReadDetailsByCode(code);
		}
	}

	// =======
	// 取样打包传值到物流跟踪
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveToGoodsSampleWay(String id) {
		List<SampleInfoMain> list = sampleInfoMainDao.setPackToWay(id);
		for (SampleInfoMain mc : list) {
			GoodsSampleWay mmc = new GoodsSampleWay();
			mmc.setCode(mc.getId());
			mmc.setName(mc.getName());
			mmc.setPackUser(mc.getSampleUser());
			mmc.setExpressOrder(mc.getExpressCode());
			if (mc.getCompany() != null) {
				mmc.setExpressCompany(mc.getCompany().getName());
			} else {
				mmc.setExpressCompany("");
			}
			mmc.setSendTime(mc.getSendTime());
			mmc.setStateId(mc.getState());
			mmc.setStateName(mc.getStateName());

			sampleInfoMainDao.saveOrUpdate(mmc);
		}
	}

	// //根据取样主数据Id获取明细信息,传递到样本录入页面
	// @WriteOperLog
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void saveToSampleReceive(String id) {
	// List<SampleInfo> list=sampleInfoMainDao.setSampleInfo(id);
	// for(SampleInfo gspi:list){
	// SampleInput sr=new SampleInput();
	// sr.setSid(gspi.getId());
	// sr.setId(gspi.getCode());
	// sr.setName(gspi.getName());
	// sr.setMoney(gspi.getPrice());
	// sr.setReceiptType(gspi.getType());
	// sr.setHospital(gspi.getHospital());
	//
	// sampleInfoMainDao.saveOrUpdate(sr);
	// }
	// }

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveToSampleInfoState(String id) {
		List<SampleOrder> list = sampleInfoMainDao.setSampleOrder(id);
		for (SampleOrder si : list) {
			si.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE);
			si.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_PACK_COMPLETE_NAME);
		}

	}

}
