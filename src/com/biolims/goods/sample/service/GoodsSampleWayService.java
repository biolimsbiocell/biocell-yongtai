package com.biolims.goods.sample.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.goods.sample.dao.GoodsSampleWayDao;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.goods.sample.model.GoodsSampleWay;
import com.biolims.goods.sample.model.SampleInfoMain;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.model.SampleAbnormal;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class GoodsSampleWayService {
	@Resource
	private GoodsSampleWayDao goodsSampleWayDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;
	
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findGoodsSampleWayList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return goodsSampleWayDao.selectGoodsSampleWayList(mapForQuery, startNum, limitNum, dir, sort);
	}

	/**
	 * 保存物流跟踪列表
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveToWay(String itemDataJson) throws Exception {
		List<GoodsSampleWay> saveItems = new ArrayList<GoodsSampleWay>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			GoodsSampleWay sbi = new GoodsSampleWay();
			sbi = (GoodsSampleWay) goodsSampleWayDao.Map2Bean(map, sbi);
			//if (sbi.getId() != null && sbi.getId().equals(""))
			//	sbi.setId(null);
			
			saveItems.add(sbi);
			//向取样打包页面反馈值yq
			if(sbi!=null){
				List<SampleInfoMain> list1=sampleInfoMainDao.setPackToWay(sbi.getCode());
				if(list1.size()>0){
					for(SampleInfoMain sim:list1){
						if(sbi.getIsQualified()!=null){
							sim.setIsQualified(sbi.getIsQualified());
						}else{
							sim.setIsQualified("");
						}
						if(sbi.getIsReceive()!=null){
							sim.setIsReceive(sbi.getIsReceive());
						}else{
							sim.setIsReceive("");
						}
						String code2=sim.getId();
//						List<SampleOrder> info = sampleInfoMainDao.setSampleOrder(code2);
//						if(info.size()>0){
//							for(SampleOrder si:info){
//								String time=si.getReceiveDate();
//								if(time==null){
//									Date date=new Date();
//									DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//									String stime=format.format(date);
//									si.setReceiveDate(stime);
//								}
//							}
//						}
					}
				}
				//快递确认签收不合格的反馈至样本中心异常yq
				if(!sbi.getIsQualified().equals("") && sbi.getIsQualified().equals("0")
						&& !sbi.getIsReceive().equals("") && sbi.getIsReceive().equals("1")){
//					String mId=sbi.getCode();
//					//根据取样打包编号获得相关样本yq
//					List<SampleOrder> sList=sampleInfoMainDao.setSampleOrder(mId);
//					for(SampleOrder si:sList){
//						SampleAbnormal sa=new SampleAbnormal();
//						sa.setSampleCode(si.getCode());
//						if(si.getSampleType()!=null){
//							sa.setSampleType(si.getSampleType().getName());
//						}else{
//							sa.setSampleType("");
//						}
//						sa.setPatientName(si.getPatientName());
//						sa.setIdCard(si.getIdCard());
//						sa.setFeedBackTime(new Date());
//						
//						commonDAO.saveOrUpdate(sa);
//					}
				}
			}
		}
		goodsSampleWayDao.saveOrUpdateAll(saveItems);
	}
	public GoodsSampleWay get(String id) {
		GoodsSampleWay goodsSampleWay = commonDAO.get(GoodsSampleWay.class, id);
		return goodsSampleWay;
	}
	
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(GoodsSampleWay sc, Map jsonMap) throws Exception {
		if (sc != null) {
			goodsSampleWayDao.saveOrUpdate(sc);
			
			String jsonStr = "";
		}
   }

	
//	//将物流跟踪的是否签收信息反馈到取样打包信息列表页面
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public boolean setIsReceiveOfWayToSample(String id) throws Exception {
//		List<GoodsSampleWay> way = goodsSampleWayDao.setOrderList(id);
//		for (GoodsSampleWay gsl : way) {
//			String code=gsl.getCode();
//			List<SampleInfoMain> list=sampleInfoMainDao.setPackToWay(code);
//			for(SampleInfoMain sim:list){
//				if(gsl.getIsQualified()!=null){
//					sim.setIsQualified(gsl.getIsQualified());
//				}else{
//					sim.setIsQualified("");
//				}
//				if(gsl.getIsReceive()!=null){
//					sim.setIsReceive(gsl.getIsReceive());
//				}else{
//					sim.setIsReceive("");
//				}
//				String code2=sim.getId();
//				List<SampleInfo> info = sampleInfoMainDao.setSampleInfo(code2);
//				for(SampleInfo si:info){
//					String time=si.getReceiveDate();
//					if(time==null){
//						Date date=new Date();
//						DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//						String stime=format.format(date);
//						si.setReceiveDate(stime);
//					}
//				}
//			}
//			
//		}
//		return true;
//	}

}
