package com.biolims.goods.sample.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.dao.CommonDAO;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.goods.sample.model.GoodsSampleWay;

@Repository
@SuppressWarnings("unchecked")
public class GoodsSampleWayDao extends BaseHibernateDao {
	public Map<String, Object> selectGoodsSampleWayList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from GoodsSampleWay where 1=1";
		if (mapForQuery.size()>0){
			key = map2where(mapForQuery);
		}else{
			key="  and id not in (select id from GoodsSampleWay  where isQualified='1' and isReceive='1')";
		}
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<GoodsSampleWay> list = new ArrayList<GoodsSampleWay>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}else{
				
				key = key + " order by id desc";
				
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	/**
	 * 将物流跟踪的是否签收信息反馈到取样打包信息列表页面
	 * @param id
	 * @return
	 */
	public List<GoodsSampleWay> setOrderList(String id) {
		String hql = "from GoodsSampleWay t where t.code='" + id + "'";
		List<GoodsSampleWay> list = this.getSession().createQuery(hql).list();
		return list;
	}
}