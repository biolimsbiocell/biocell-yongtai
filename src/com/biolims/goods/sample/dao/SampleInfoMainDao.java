package com.biolims.goods.sample.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.goods.sample.model.SampleInfoMain;
import com.biolims.goods.sample.model.SampleInfoSystem;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.storage.model.SampleInfoIn;
import com.biolims.system.sys.model.SamplePackSystem;
import com.biolims.system.sys.model.SamplePackSystemItem;
import com.biolims.system.work.model.WorkType;

@Repository
@SuppressWarnings("unchecked")
public class SampleInfoMainDao extends BaseHibernateDao {
	public Map<String, Object> selectSampleInfoMainList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from SampleInfoMain where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SampleInfoMain> list = new ArrayList<SampleInfoMain>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} else {

				key = key + " order by id desc";

			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	/**
	 * 查询状态为‘1’的取样打包主数据
	 * 
	 * @param mapForQuery
	 * @param startNum
	 * @param limitNum
	 * @param dir
	 * @param sort
	 * @return
	 */
	public Map<String, Object> selectSampleInfoMainList1(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from SampleInfoMain where 1=1";
		if (mapForQuery.size() > 0) {
			key = map2where(mapForQuery);
		}
		Long total = (Long) this.getSession()
				.createQuery(" select count(*) " + hql + key).uniqueResult();
		List<SampleInfoMain> list = new ArrayList<SampleInfoMain>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}

	public Map<String, Object> selectSampleInfoSystemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SampleInfoSystem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sampleInfoMain.id='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleInfoSystem> list = new ArrayList<SampleInfoSystem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public Map<String, Object> selectSampleInfoList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		String hql = "from SampleOrder where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and sampleInfoMain='" + scId + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleOrder> list = new ArrayList<SampleOrder>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null
					&& sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key)
						.setFirstResult(startNum).setMaxResults(limitNum)
						.list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 选择打包单加载子表信息
	public Map<String, Object> setSampleInfoList(String code) throws Exception {
		String hql = "from SampleOrder t where 1=1 ";
		String key = "";
		if (code != null)
			key = key + " and  t.sampleInfoMain='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleOrder> list = this.getSession().createQuery(hql + key)
				.list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	// 选择打包单加载子表体系
	public Map<String, Object> setSampleInfoSystemList(String code)
			throws Exception {
		String hql = "from SampleInfoSystem t where 1=1 ";
		String key = "";
		if (code != null)
			key = key + " and  t.sampleInfoMain='" + code + "'";
		Long total = (Long) this.getSession()
				.createQuery("select count(*) " + hql + key).uniqueResult();
		List<SampleInfoSystem> list = this.getSession().createQuery(hql + key)
				.list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}

	public List<SampleInfoMain> setPackToWay(String code) {
		String hql = "from SampleInfoMain t where 1=1 and t.id='" + code + "'";
		List<SampleInfoMain> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据主表Id查询子表信息
	public List<SampleOrder> setSampleOrder(String code) {
		String hql = "from SampleOrder t where 1=1 and t.sampleInfoMain='"
				+ code + "'";
		List<SampleOrder> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据样本编号查询子表信息
	public List<SampleInfo> setSampleInfoBySampleCode(String code) {
		String hql = "from SampleInfo t where 1=1 and t.code='" + code + "'";
		List<SampleInfo> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据Code查询子表的信息
	public SampleInfo setSamleInfoById(String id) {
		String hql = "from SampleInfo info where 1=1 and info.id ='" + id + "'";
		SampleInfo info = (SampleInfo) this.getSession().createQuery(hql)
				.uniqueResult();
		return info;
	}

	// 根据主表Id查询子表体系信息
	public List<SampleInfoSystem> setSampleInfoSystem(String code) {
		String hql = "from SampleInfoSystem t where 1=1 and t.sampleInfoMain='"
				+ code + "'";
		List<SampleInfoSystem> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据标识码保存检测方法
	public List<WorkType> saveWorkType(String code) {
		String hql = "from WorkType t where 1=1 and t.mark='" + code + "'";
		List<WorkType> list = this.getSession().createQuery(hql).list();
		return list;
	}

	// 根据检测方法保存体系
	// public List<SampleInfoSystem> saveSystem(String code){
	// String hql =
	// "from SampleInfoSystem t where 1=1 and t.sampleInfo='"+code+"'";
	// List<SampleInfoSystem> list = this.getSession().createQuery(hql).list();
	// return list;
	// }
	public List<SamplePackSystemItem> saveSystem(String code) {
		String hql = "from SamplePackSystemItem t where 1=1 and t.samplePackSystem='"
				+ code + "'";
		List<SamplePackSystemItem> list = this.getSession().createQuery(hql)
				.list();
		return list;
	}

	// 根据检测方法查询打包体系主数据
	public SamplePackSystem selectSystem(String type, String sampleType,
			String bloodTube) {
		String hql = "from SamplePackSystem t where 1=1 and t.product.id='"
				+ type + "'and t.sampleType.id='" + sampleType
				+ "' and t.state='1' and t.bloodTube.id='" + bloodTube + "'";
		List<SamplePackSystem> list = this.getSession().createQuery(hql).list();
		if (list.size() > 0) {
			return list.get(0);
		} else {
			return null;
		}
	}

	/**
	 * 根据样本编号查出SampleInfo对象
	 * 
	 * @param code
	 * @return
	 */
	public SampleInfo findSampleInfo(String code) {
		String hql = "from SampleInfo t where 1=1 and t.code = '" + code + "'";
		SampleInfo sampleInfo = (SampleInfo) this.getSession().createQuery(hql)
				.uniqueResult();
		return sampleInfo;
	}
	/**
	 * 根据样本编号查出SampleInfoIn对象
	 * 
	 * @param code
	 * @return
	 */
	public SampleInfoIn findSampleInfoIn(String code) {
		String hql = "from SampleInfoIn t where 1=1 and t.code = '" + code + "'";
		SampleInfoIn sampleInfoIn = (SampleInfoIn) this.getSession().createQuery(hql)
				.uniqueResult();
		return sampleInfoIn;
	}

	public List<SampleInfo> findSampleInfoByOrderNum(String orderNum) {
		String hql = "from SampleInfo t where 1=1 and t.orderNum='"
				+ orderNum + "'";
		List<SampleInfo> list = this.getSession().createQuery(hql)
				.list();
		return list;
		
	}
}