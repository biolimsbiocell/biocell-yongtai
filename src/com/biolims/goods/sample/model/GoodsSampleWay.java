package com.biolims.goods.sample.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 物流跟踪
 * @author lims-platform
 * @date 2015-11-03 16:21:27
 * @version V1.0   
 *
 */
@Entity
@Table(name = "GOODS_SAMPLE_WAY")
@SuppressWarnings("serial")
public class GoodsSampleWay extends EntityDao<GoodsSampleWay> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**取样打包编号*/
	private String code;
	/**描述*/
	private String name;
	/**打包人*/
	private String packUser;
	/**是否合格*/
	private String isQualified;
	/**快递单号*/
	private String expressOrder;
	/**快递公司*/
	private String expressCompany;
	/**发货时间*/
	private String sendTime;
	
	public String getSendTime() {
		return sendTime;
	}
	public void setSendTime(String sendTime) {
		this.sendTime = sendTime;
	}
	/**签收确认*/
	private String isReceive;
	/**签收时间*/
	private Date receiveDate;
	/**快递状态*/
	private String stateId;
	/**快递状态*/
	private String stateName;
	//备注
	private String note;
	
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	private SampleInfoMain sampleInfoMain;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO_MAIN")
	public SampleInfoMain getSampleInfoMain() {
		return sampleInfoMain;
	}
	public void setSampleInfoMain(SampleInfoMain sampleInfoMain) {
		this.sampleInfoMain = sampleInfoMain;
	}

	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 36)
	public String getId(){
		return this.id;
	}

	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  取样打包编号
	 */
	@Column(name ="CODE", length = 120)
	public String getCode(){
		return this.code;
	}
	/**
	 *方法: 设置String
	 *@param: String  取样打包编号
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 120)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  打包人
	 */
	@Column(name ="PACK_USER", length = 60)
	public String getPackUser(){
		return this.packUser;
	}
	/**
	 *方法: 设置String
	 *@param: String  打包人
	 */
	public void setPackUser(String packUser){
		this.packUser = packUser;
	}
	/**
	 *方法: 取得String
	 *@return: String  是否合格
	 */
	@Column(name ="IS_QUALIFIED", length = 60)
	public String getIsQualified(){
		return this.isQualified;
	}
	/**
	 *方法: 设置String
	 *@param: String  是否合格
	 */
	public void setIsQualified(String isQualified){
		this.isQualified = isQualified;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  发货时间
	 */
	
	/**
	 *方法: 取得String
	 *@return: String  签收确认
	 */
	@Column(name ="IS_RECEIVE", length = 120)
	public String getIsReceive(){
		return this.isReceive;
	}
	/**
	 *方法: 设置String
	 *@param: String  签收确认
	 */
	public void setIsReceive(String isReceive){
		this.isReceive = isReceive;
	}
	/**
	 *方法: 取得String
	 *@return: String  快递状态
	 */
	
	/**
	 *方法: 取得String
	 *@return: String  快递状态
	 */
	@Column(name ="STATE_NAME", length = 60)
	public String getStateName(){
		return this.stateName;
	}
	
	public String getExpressOrder() {
		return expressOrder;
	}
	public void setExpressOrder(String expressOrder) {
		this.expressOrder = expressOrder;
	}
	public String getExpressCompany() {
		return expressCompany;
	}
	public void setExpressCompany(String expressCompany) {
		this.expressCompany = expressCompany;
	}
	public String getStateId() {
		return stateId;
	}
	public void setStateId(String stateId) {
		this.stateId = stateId;
	}
	
	@Column(name ="RECEIVE_DATE", length = 60)
	public Date getReceiveDate() {
		return receiveDate;
	}
	public void setReceiveDate(Date receiveDate) {
		this.receiveDate = receiveDate;
	}
	/**
	 *方法: 设置String
	 *@param: String  快递状态
	 */
	public void setStateName(String stateName){
		this.stateName = stateName;
	}
}