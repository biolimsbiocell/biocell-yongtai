package com.biolims.goods.sample.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 取样打包体系
 * @author lims-platform
 * @date 2015-11-03 16:21:31
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SAMPLE_INFO_SYSTEM")
@SuppressWarnings("serial")
public class SampleInfoSystem extends EntityDao<SampleInfoSystem> implements java.io.Serializable {
	/**编码*/
	private String id;
	/**描述*/
	private String name;
	/**冰袋数量*/
	private Integer num;
	/**温度*/
	private Double temperature;
	/**范围*/
	private String scope;
	/**相关主表*/
	private SampleInfoMain sampleInfoMain;
	/**
	 *方法: 取得String
	 *@return: String  编码
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 60)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编码
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 60)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得Integer
	 *@return: Integer  冰袋数量
	 */
	@Column(name ="NUM", length = 60)
	public Integer getNum(){
		return this.num;
	}
	/**
	 *方法: 设置Integer
	 *@param: Integer  冰袋数量
	 */
	public void setNum(Integer num){
		this.num = num;
	}
	/**
	 *方法: 取得Double
	 *@return: Double  温度
	 */
	@Column(name ="TEMPERATURE", length = 60)
	public Double getTemperature(){
		return this.temperature;
	}
	/**
	 *方法: 设置Double
	 *@param: Double  温度
	 */
	public void setTemperature(Double temperature){
		this.temperature = temperature;
	}

	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	/**
	 *方法: 取得GoodsSamplePack
	 *@return: GoodsSamplePack  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "SAMPLE_INFO_MAIN")
	public SampleInfoMain getSampleInfoMain() {
		return sampleInfoMain;
	}
	public void setSampleInfoMain(SampleInfoMain sampleInfoMain) {
		this.sampleInfoMain = sampleInfoMain;
	}

}