package com.biolims.goods.sample.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.system.express.model.ExpressCompany;
import com.biolims.system.product.model.Product;
/**   
 * @Title: Model
 * @Description: 取样打包
 * @author lims-platform
 * @date 2015-11-03 16:21:52
 * @version V1.0   
 *
 */
@Entity
@Table(name = "SAMPLE_INFO_MAIN")
@SuppressWarnings("serial")
public class SampleInfoMain extends EntityDao<SampleInfoMain> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
//	/**送检医院*/
//	private String hospital;
//	/**项目*/
//	private Product project;
	/**取样人*/
	private String sampleUser;
	/**取样时间*/
	private String sampleTime;
	/**发货时间*/
	private String sendTime;
	/**快递公司*/
	private ExpressCompany company;
	/**快递单号*/
	private String expressCode;
	/**创建人*/
	private User createUser;
	/**创建日期*/
	private Date createDate;
	/**工作流状态*/
	private String state;
	/**工作流状态*/
	private String stateName;
	/**是否合格*/
	private String isQualified;
	//是否签收
	private String isReceive;
	public String getIsReceive() {
		return isReceive;
	}
	public void setIsReceive(String isReceive) {
		this.isReceive = isReceive;
	}
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@Column(name ="ID", length = 36)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 120)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  送检医院
	 */
//	@Column(name ="HOSPITAL", length = 120)
//	public String getHospital(){
//		return this.hospital;
//	}
//	/**
//	 *方法: 设置String
//	 *@param: String  送检医院
//	 */
//	public void setHospital(String hospital){
//		this.hospital = hospital;
//	}
//	/**
//	 *方法: 取得Project
//	 *@return: Project  项目
//	 */
//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "PROJECT")
//	public Product getProject() {
//		return project;
//	}
//	public void setProject(Product project) {
//		this.project = project;
//	}
	/**
	 *方法: 取得String
	 *@return: String  取样人
	 */
	@Column(name ="SAMPLE_USER", length = 60)
	public String getSampleUser(){
		return this.sampleUser;
	}

	/**
	 *方法: 设置String
	 *@param: String  取样人
	 */
	public void setSampleUser(String sampleUser){
		this.sampleUser = sampleUser;
	}
//	/**
//	 *方法: 取得String
//	 *@return: String  取样时间
//	 */
//	@Column(name ="SAMPLE_TIME", length = 60)
//	public String getSampleTime(){
//		return this.sampleTime;
//	}
//	/**
//	 *方法: 设置String
//	 *@param: String  取样时间
//	 */
//	public void setSampleTime(String sampleTime){
//		this.sampleTime = sampleTime;
//	}
//	/**
//	 *方法: 取得String
//	 *@return: String  发货时间
//	 */
//	@Column(name ="SEND_TIME", length = 60)
//	public String getSendTime(){
//		return this.sendTime;
//	}
//	/**
//	 *方法: 设置String
//	 *@param: String  发货时间
//	 */
//	public void setSendTime(String sendTime){
//		this.sendTime = sendTime;
//	}
	/**
	 *方法: 取得DicType
	 *@return: DicType  快递公司
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "COMPANY")
	public ExpressCompany getCompany(){
		return this.company;
	}
	public String getSampleTime() {
		return sampleTime;
	}
	public void setSampleTime(String sampleTime) {
		this.sampleTime = sampleTime;
	}

	public String getSendTime() {
		return sendTime;
	}
	public void setSendTime(String sendTime) {
		this.sendTime = sendTime;
	}
	/**
	 *方法: 设置DicType
	 *@param: DicType  快递公司
	 */
	public void setCompany(ExpressCompany company){
		this.company = company;
	}
	/**
	 *方法: 取得String
	 *@return: String  快递单号
	 */
	@Column(name ="EXPRESS_CODE", length = 120)
	public String getExpressCode(){
		return this.expressCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  快递单号
	 */
	public void setExpressCode(String expressCode){
		this.expressCode = expressCode;
	}
	/**
	 *方法: 取得User
	 *@return: User  创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser(){
		return this.createUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  创建人
	 */
	public void setCreateUser(User createUser){
		this.createUser = createUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  创建日期
	 */
	@Column(name ="CREATE_DATE", length = 255)
	public Date getCreateDate(){
		return this.createDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  创建日期
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  工作流状态
	 */
	@Column(name ="STATE", length = 60)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  工作流状态
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  工作流状态
	 */
	@Column(name ="STATE_NAME", length = 60)
	public String getStateName(){
		return this.stateName;
	}
	/**
	 *方法: 设置String
	 *@param: String  工作流状态
	 */
	public void setStateName(String stateName){
		this.stateName = stateName;
	}
	
	/**
	 * 方法： 获取IsQualified 
	 * @return String 是否合格
	 */
	@Column(name="IS_QUALIFIED",length=60)
	public String getIsQualified() {
		return isQualified;
	}
	
	/**
	 * 方法:设置IsQualified
	 * @param isQualified 是否合格
	 */
	public void setIsQualified(String isQualified) {
		this.isQualified = isQualified;
	}
}