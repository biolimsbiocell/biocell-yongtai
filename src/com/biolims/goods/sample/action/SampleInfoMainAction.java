﻿package com.biolims.goods.sample.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.file.model.FileInfo;
import com.biolims.file.service.FileInfoService;
import com.biolims.file.service.OperFileService;
import com.biolims.goods.sample.model.SampleInfoMain;
import com.biolims.goods.sample.model.SampleInfoSystem;
import com.biolims.goods.sample.service.SampleInfoMainService;
import com.biolims.sample.model.SampleInfo;
import com.biolims.sample.model.SampleOrder;
import com.biolims.system.work.service.WorkTypeService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;

@Namespace("/goods/sample/sampleInfoMain")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class SampleInfoMainAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "20021";
	@Autowired
	private SampleInfoMainService sampleInfoMainService;
	private SampleInfoMain sampleInfoMain = new SampleInfoMain();

	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private WorkTypeService workTypeService;
	@Resource
	private OperFileService operFileService;
	@Resource
	private CodingRuleService codingRuleService;

	private FileInfo fileInfo = new FileInfo();

	public FileInfo getFileInfo() {
		return fileInfo;
	}

	public void setFileInfo(FileInfo fileInfo) {
		this.fileInfo = fileInfo;
	}

	@Action(value = "showSampleInfoMainList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleInfoMainList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/goods/sample/sampleInfoMain.jsp");
	}

	@Action(value = "showSampleInfoMainListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleInfoMainListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = sampleInfoMainService
				.findSampleInfoMainList(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<SampleInfoMain> list = (List<SampleInfoMain>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		// map.put("hospital", "");
		// map.put("project-id", "");
		// map.put("project-name", "");
		map.put("sampleUser", "");
		map.put("isQualified", "");
		map.put("isReceive", "");
		map.put("sampleTime", "");
		map.put("sendTime", "");
		map.put("company-id", "");
		map.put("company-name", "");
		map.put("expressCode", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "sampleInfoMainSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogSampleInfoList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/goods/sample/sampleInfoMainDialog.jsp");
	}

	@Action(value = "showDialogSampleInfoMainListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogSampleInfoMainListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		map2Query.put("isReceive", "1");
		map2Query.put("state", "1");
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = sampleInfoMainService
				.findSampleInfoMainList1(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<SampleInfoMain> list = (List<SampleInfoMain>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		// map.put("hospital", "");
		// map.put("project-id", "");
		// map.put("project-name", "");
		map.put("sampleUser", "");
		map.put("sampleTime", "");
		map.put("sendTime", "");
		map.put("company-id", "");
		map.put("company-name", "");
		map.put("expressCode", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("isQualified", "");
		map.put("isReceive", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editSampleInfoMain")
	public String editSampleInfoMain() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			sampleInfoMain = sampleInfoMainService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "sampleInfoMain");
		} else {
			sampleInfoMain.setId("NEW");
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			sampleInfoMain.setCreateUser(user);
			sampleInfoMain.setCreateDate(new Date());
			sampleInfoMain.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			sampleInfoMain.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(sampleInfoMain.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/goods/sample/sampleInfoMainEdit.jsp");
	}

	@Action(value = "copySampleInfoMain")
	public String copySampleInfoMain() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		sampleInfoMain = sampleInfoMainService.get(id);
		sampleInfoMain.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/goods/sample/sampleInfoMainEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = sampleInfoMain.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "SampleInfoMain";
			String markCode = "QYDB";
			String autoID = codingRuleService.genTransID(modelName, markCode);
			sampleInfoMain.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("sampleInfoSystem",
				getParameterFromRequest("sampleInfoSystemJson"));

		aMap.put("sampleInfo", getParameterFromRequest("sampleInfoJson"));

		sampleInfoMainService.save(sampleInfoMain, aMap);
		return redirect("/goods/sample/sampleInfoMain/editSampleInfoMain.action?id="
				+ sampleInfoMain.getId());

	}

	@Action(value = "viewSampleInfoMain")
	public String toViewSampleInfoMain() throws Exception {
		String id = getParameterFromRequest("id");
		sampleInfoMain = sampleInfoMainService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/goods/sample/sampleInfoMainEdit.jsp");
	}

	@Action(value = "showSampleInfoSystemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleInfoSystemList() throws Exception {
		return dispatcher("/WEB-INF/page/goods/sample/sampleInfoSystem.jsp");
	}

	@Action(value = "showSampleInfoSystemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleInfoSystemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = sampleInfoMainService
					.findSampleInfoSystemList(scId, startNum, limitNum, dir,
							sort);
			Long total = (Long) result.get("total");
			List<SampleInfoSystem> list = (List<SampleInfoSystem>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("num", "");
			map.put("scope", "");
			map.put("sampleInfoMain-name", "");
			map.put("sampleInfoMain-id", "");
			// map.put("createUser-id", "");
			// map.put("createUser-name", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSampleInfoSystem", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void delSampleInfoSystem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			sampleInfoMainService.delSampleInfoSystem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showSampleInfoList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showSampleInfoList() throws Exception {
		return dispatcher("/WEB-INF/page/goods/sample/sampleInfo.jsp");
	}

	@Action(value = "showSampleInfoListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleInfoListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = sampleInfoMainService
					.findSampleInfoList(scId, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<SampleOrder> list = (List<SampleOrder>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("state", "");
			map.put("stateName", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("sampleInfoMain", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delSampleInfo", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void delSampleInfo() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			sampleInfoMainService.delSampleInfo(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	// 根据打包加载子表明细
	@Action(value = "showSamplePackItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showGoodsMaterialsApplyItemList() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.sampleInfoMainService
					.showSamplePackItemList(code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 根据打包加载体系明细
	@Action(value = "showSampleSystemItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showSampleSystemItemList() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.sampleInfoMainService
					.showSampleSystemItemList(code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public SampleInfoMainService getSampleInfoMainService() {
		return sampleInfoMainService;
	}

	public void setSampleInfoMainService(
			SampleInfoMainService sampleInfoMainService) {
		this.sampleInfoMainService = sampleInfoMainService;
	}

	public SampleInfoMain getSampleInfoMain() {
		return sampleInfoMain;
	}

	public void setSampleInfoMain(SampleInfoMain sampleInfoMain) {
		this.sampleInfoMain = sampleInfoMain;
	}

	// 根据标识码加载业务类型
	@Action(value = "setWorkType", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void setWorkType() throws Exception {
		String code = getRequest().getParameter("code");
		String mark = code.substring(0, 1);
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.workTypeService
					.setWorkType(mark);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	// 上传单个图片
	@Action(value = "toSampeInfoUpload", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String toSampeUpload() {
		String fileId = getRequest().getParameter("fileId");
		String isUpload = getRequest().getParameter("isUpload");
		if (fileId != null && fileId.length() > 0)
			fileInfo = this.operFileService.queryFileInfoById(fileId);
		String module = getRequest().getParameter("module");
		getRequest().setAttribute("module", module);
		getRequest().setAttribute("isUpload", isUpload);
		return dispatcher("/WEB-INF/page/sample/task/sampleUpload.jsp");
	}

	// 批量上传图片（未完成）
	@Action(value = "upLoads", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String upLoads() throws Exception {
		String code = getRequest().getParameter("code");
		if (!code.equals("")) {

		}

		return null;
	}

}
