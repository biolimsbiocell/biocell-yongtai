﻿
package com.biolims.goods.sample.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.goods.sample.model.GoodsSampleWay;
import com.biolims.goods.sample.service.GoodsSampleWayService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/goods/sample/goodsSampleWay")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class GoodsSampleWayAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "20022";
	@Autowired
	private GoodsSampleWayService goodsSampleWayService;
	private GoodsSampleWay goodsSampleWay = new GoodsSampleWay();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showGoodsSampleWayList")
	public String showGoodsSampleWayList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/goods/sample/goodsSampleWay.jsp");
	}

	@Action(value = "showGoodsSampleWayListJson")
	public void showGoodsSampleWayListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = goodsSampleWayService.findGoodsSampleWayList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<GoodsSampleWay> list = (List<GoodsSampleWay>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("name", "");
		map.put("packUser", "");
		map.put("isQualified", "");
		map.put("expressOrder", "");
		map.put("expressCompany", "");
		map.put("sendTime", "");
		map.put("isReceive", "");
		map.put("stateId", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

//	@Action(value = "goodsSampleWaySelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showDialogGoodsSampleWayList() throws Exception {
//		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
//		return dispatcher("/WEB-INF/page/goods/sample/goodsSampleWayDialog.jsp");
//	}
//
//	@Action(value = "showDialogGoodsSampleWayListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showDialogGoodsSampleWayListJson() throws Exception {
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		String dir = getParameterFromRequest("dir");
//		String sort = getParameterFromRequest("sort");
//		String data = getParameterFromRequest("data");
//		Map<String, String> map2Query = new HashMap<String, String>();
//		if (data != null && data.length() > 0)
//			map2Query = JsonUtils.toObjectByJson(data, Map.class);
//		Map<String, Object> result = goodsSampleWayService.findGoodsSampleWayList(map2Query, startNum, limitNum, dir, sort);
//		Long count = (Long) result.get("total");
//		List<GoodsSampleWay> list = (List<GoodsSampleWay>) result.get("list");
//
//		Map<String, String> map = new HashMap<String, String>();
//		map.put("id", "");
//		map.put("code", "");
//		map.put("name", "");
//		map.put("packUser", "");
//		map.put("isQualified", "");
//		map.put("expressOrder", "");
//		map.put("expressCompany", "");
//		map.put("sendTime", "");
//		map.put("isReceive", "");
//		map.put("stateId", "");
//		map.put("stateName", "");
//		map.put("note", "");
//		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
//	}



//	@Action(value = "editGoodsSampleWay")
//	public String editGoodsSampleWay() throws Exception {
//		String id = getParameterFromRequest("id");
//		long num = 0;
//		if (id != null && !id.equals("")) {
//			goodsSampleWay = goodsSampleWayService.get(id);
//			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
//			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
//			num = fileInfoService.findFileInfoCount(id, "goodsSampleWay");
//		} else {
//			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
////			goodsSampleWay.setCreateUser(user);
////			goodsSampleWay.setCreateDate(new Date());
//			goodsSampleWay.setStateId(SystemConstants.DIC_STATE_NEW);
//			goodsSampleWay.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
//			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
//			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
//		}
//		putObjToContext("fileNum", num);
//		return dispatcher("/WEB-INF/page/goods/sample/goodsSampleWayEdit.jsp");
//	}
//
//	@Action(value = "copyGoodsSampleWay")
//	public String copyGoodsSampleWay() throws Exception {
//		String id = getParameterFromRequest("id");
//		String handlemethod = getParameterFromRequest("handlemethod");
//		goodsSampleWay = goodsSampleWayService.get(id);
//		goodsSampleWay.setId("");
//		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
//		toToolBar(rightsId, "", "", handlemethod);
//		toSetStateCopy();
//		return dispatcher("/WEB-INF/page/goods/sample/goodsSampleWayEdit.jsp");
//	}


//	@Action(value = "save")
//	public String save() throws Exception {
//		String id = goodsSampleWay.getId();
//		if(id!=null&&id.equals("")){
//			goodsSampleWay.setId(null);
//		}
//		Map aMap = new HashMap();
//		goodsSampleWayService.save(goodsSampleWay,aMap);
//		return redirect("/goods/sample/goodsSampleWay/editGoodsSampleWay.action?id=" + goodsSampleWay.getId());
//		//return dispatcher("/WEB-INF/page/goods/sample/goodsSampleWay.jsp");
//	}
//
//	@Action(value = "viewGoodsSampleWay")
//	public String toViewGoodsSampleWay() throws Exception {
//		String id = getParameterFromRequest("id");
//		goodsSampleWay = goodsSampleWayService.get(id);
//		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
//		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
//		return dispatcher("/WEB-INF/page/goods/sample/goodsSampleWayEdit.jsp");
//	}
//	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public GoodsSampleWayService getGoodsSampleWayService() {
		return goodsSampleWayService;
	}

	public void setGoodsSampleWayService(GoodsSampleWayService goodsSampleWayService) {
		this.goodsSampleWayService = goodsSampleWayService;
	}

	public GoodsSampleWay getGoodsSampleWay() {
		return goodsSampleWay;
	}

	public void setGoodsSampleWay(GoodsSampleWay goodsSampleWay) {
		this.goodsSampleWay = goodsSampleWay;
	}
	//将快递信息返回到物流跟踪页面
	@Action(value = "setSampleToWay")
	public void setSampleToWay() throws Exception {
		//String id=getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getRequest().getParameter("itemDataJson");
			goodsSampleWayService.saveToWay(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
//	//将物流跟踪的是否签收信息反馈到取样打包信息列表页面
//	@Action(value = "setIsReceiveOfWayToSample")
//	public void setIsReceiveOfWayToSample() throws Exception {
//		String id = getParameterFromRequest("code");
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			boolean dataListMap = this.goodsSampleWayService.setIsReceiveOfWayToSample(id);
//			result.put("success", true);
//			result.put("data", dataListMap);
//		} catch (Exception e) {
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}
}
