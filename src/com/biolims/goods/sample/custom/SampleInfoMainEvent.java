package com.biolims.goods.sample.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.goods.sample.service.SampleInfoMainService;

public class SampleInfoMainEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		SampleInfoMainService mbService = (SampleInfoMainService) ctx.getBean("sampleInfoMainService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
