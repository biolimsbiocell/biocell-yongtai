﻿
package com.biolims.goods.mate.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.goods.mate.model.GoodsMaterialsPost;
import com.biolims.goods.mate.model.GoodsMaterialsPostItem;
import com.biolims.goods.mate.service.GoodsMaterialsPostService;
import com.biolims.goods.mate.service.GoodsMaterialsSendService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/goods/mate/goodsMaterialsPost")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class GoodsMaterialsPostAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "20014";
	@Autowired
	private GoodsMaterialsPostService goodsMaterialsPostService;
	private GoodsMaterialsPost goodsMaterialsPost = new GoodsMaterialsPost();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private GoodsMaterialsSendService goodsMaterialsSendService;
	@Action(value = "showGoodsMaterialsPostList")
	public String showGoodsMaterialsPostList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsPost.jsp");
	}

	@Action(value = "showGoodsMaterialsPostListJson")
	public void showGoodsMaterialsPostListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = goodsMaterialsPostService.findGoodsMaterialsPostList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<GoodsMaterialsPost> list = (List<GoodsMaterialsPost>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("type-id", "");
		map.put("type-name", "");
		map.put("goodsMaterialsSend-id", "");
		map.put("goodsMaterialsSend-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "goodsMaterialsPostSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogGoodsMaterialsPostList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsPostDialog.jsp");
	}

	@Action(value = "showDialogGoodsMaterialsPostListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogGoodsMaterialsPostListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = goodsMaterialsPostService.findGoodsMaterialsPostList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<GoodsMaterialsPost> list = (List<GoodsMaterialsPost>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("type-id", "");
		map.put("type-name", "");
		map.put("goodsMaterialsSend-id", "");
		map.put("goodsMaterialsSend-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editGoodsMaterialsPost")
	public String editGoodsMaterialsPost() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			goodsMaterialsPost = goodsMaterialsPostService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "goodsMaterialsPost");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			goodsMaterialsPost.setCreateUser(user);
			goodsMaterialsPost.setCreateDate(new Date());
			goodsMaterialsPost.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			goodsMaterialsPost.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(goodsMaterialsPost.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsPostEdit.jsp");
	}

	@Action(value = "copyGoodsMaterialsPost")
	public String copyGoodsMaterialsPost() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		goodsMaterialsPost = goodsMaterialsPostService.get(id);
		goodsMaterialsPost.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsPostEdit.jsp");
	}

//
//	@Action(value = "save")
//	public String save() throws Exception {
//		String id = goodsMaterialsPost.getId();
//		if(id!=null&&id.equals("")){
//			goodsMaterialsPost.setId(null);
//		}
//		Map aMap = new HashMap();
//			aMap.put("goodsMaterialsPostItem",getParameterFromRequest("goodsMaterialsPostItemJson"));
//		
//		goodsMaterialsPostService.save(goodsMaterialsPost,aMap);
//		return redirect("/goods/mate/goodsMaterialsPost/editGoodsMaterialsPost.action?id=" + goodsMaterialsPost.getId());
//
//	}

	@Action(value = "viewGoodsMaterialsPost")
	public String toViewGoodsMaterialsPost() throws Exception {
		String id = getParameterFromRequest("id");
		goodsMaterialsPost = goodsMaterialsPostService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsPostEdit.jsp");
	}
	

	@Action(value = "showGoodsMaterialsPostItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showGoodsMaterialsPostItemList() throws Exception {
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsPostItem.jsp");
	}

	@Action(value = "showGoodsMaterialsPostItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showGoodsMaterialsPostItemListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
			Map<String, Object> result = goodsMaterialsPostService.findGoodsMaterialsPostItemList(map2Query, startNum, limitNum, dir, sort);
			Long total = (Long) result.get("total");
			List<GoodsMaterialsPostItem> list = (List<GoodsMaterialsPostItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("expressOrder", "");
			map.put("checked", "");
			map.put("address", "");
			map.put("company", "");
			map.put("expectDate", "yyyy-MM-dd");
			map.put("realDate", "yyyy-MM-dd");
			map.put("way", "");
			map.put("note", "");
			map.put("submit", "");
			map.put("goodsMaterialsSend-name", "");
			map.put("goodsMaterialsSend-id", "");
			map.put("goodsMaterialsSendExpress-id", "");
			map.put("receiver", "");
			map.put("dateOfLodgment", "yyyy-MM-dd");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
	}
//		/**
//	 * 删除明细信息
//	 * @throws Exception
//	 */
//	@Action(value = "delGoodsMaterialsPostItem")
//	public void delGoodsMaterialsPostItem() throws Exception {
//		Map<String, Object> map = new HashMap<String, Object>();
//		try {
//			String[] ids = getRequest().getParameterValues("ids[]");
//			goodsMaterialsPostService.delGoodsMaterialsPostItem(ids);
//			map.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			map.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
//	//===============2015-12-01 ly=======================
//	/**
//	 * 根据ID删除信息
//	 * @throws Exception
//	 */
//	@Action(value = "delGoodsMaterialsPostOne")
//	public void delGoodsMaterialsPostOne() throws Exception {
//		Map<String, Object> map = new HashMap<String, Object>();
//		try {
//			String ids=getParameterFromRequest("ids");
//			goodsMaterialsPostService.delGoodsMaterialsPostOne(ids);
//			map.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			map.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
//	//=================================================

	//根据准备单加载子表明细
	@Action(value = "showGoodsMaterialsSendExpressList")
	public void showGoodsMaterialsSendExpressList() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.goodsMaterialsSendService.showGoodsMaterialsSendExpressList(code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}


	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public GoodsMaterialsPostService getGoodsMaterialsPostService() {
		return goodsMaterialsPostService;
	}

	public void setGoodsMaterialsPostService(GoodsMaterialsPostService goodsMaterialsPostService) {
		this.goodsMaterialsPostService = goodsMaterialsPostService;
	}

	public GoodsMaterialsPost getGoodsMaterialsPost() {
		return goodsMaterialsPost;
	}

	public void setGoodsMaterialsPost(GoodsMaterialsPost goodsMaterialsPost) {
		this.goodsMaterialsPost = goodsMaterialsPost;
	}

	//将快递信息返回到物流跟踪页面
	@Action(value = "saveGoodsMaterialsPostItemGrid")
	public void saveGoodsMaterialsPostItemGrid() throws Exception {
		//String id=getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getRequest().getParameter("itemDataJson");
			goodsMaterialsPostService.saveGoodsMaterialsPostItemGrid(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
