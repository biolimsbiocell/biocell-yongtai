﻿
package com.biolims.goods.mate.action;


import java.text.DateFormat;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.dic.model.DicType;
import com.biolims.file.service.FileInfoService;
import com.biolims.goods.mate.dao.GoodsMaterialsApplyDao;
import com.biolims.goods.mate.dao.GoodsMaterialsReadyDao;
import com.biolims.goods.mate.model.GoodsMaterialsApply;
import com.biolims.goods.mate.model.GoodsMaterialsApplyItem;
import com.biolims.goods.mate.model.GoodsMaterialsApplyWay;
import com.biolims.goods.mate.model.GoodsMaterialsReadyDetails;
import com.biolims.goods.mate.service.GoodsMaterialsApplyService;
import com.biolims.goods.mate.service.GoodsMaterialsReadyService;
import com.biolims.system.code.SystemConstants;
import com.biolims.system.express.model.ExpressCompany;
import com.biolims.system.organize.dao.ApplyOrganizeDao;
import com.biolims.system.organize.model.ApplyOrganize;
import com.biolims.system.organize.model.ApplyOrganizeAddress;
import com.biolims.system.organize.service.ApplyOrganizeService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/goods/mate/goodsMaterialsApply")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class GoodsMaterialsApplyAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "20011";
	@Autowired
	private GoodsMaterialsApplyService goodsMaterialsApplyService;
	private GoodsMaterialsApply goodsMaterialsApply = new GoodsMaterialsApply();
	//private DicType dicType = new DicType();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private ApplyOrganizeDao applyOrganizeDao;
	@Resource
	private GoodsMaterialsApplyDao goodsMaterialsApplyDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private GoodsMaterialsReadyService goodsMaterialsReadyService;
	@Resource
	private GoodsMaterialsReadyDao goodsMaterialsReadyDao;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private ApplyOrganizeService applyOrganizeService;
	@Action(value = "showGoodsMaterialsApplyList")
	public String showGoodsMaterialsApplyList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsApply.jsp");
	}

	@Action(value = "showGoodsMaterialsApplyListJson")
	public void showGoodsMaterialsApplyListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		try {
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = goodsMaterialsApplyService.findGoodsMaterialsApplyList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<GoodsMaterialsApply> list = (List<GoodsMaterialsApply>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("applyOrganize-id", "");
		map.put("applyOrganize-name", "");
		//map.put("applyDept-id", "");
		map.put("applyDept", "");
		map.put("address-id", "");
		map.put("address-name", "");
		map.put("company-id", "");
		map.put("company-name", "");
		//map.put("type-id", "");
		map.put("type", "");
		//map.put("receiveUser-id", "");
		map.put("receiveUser", "");
		map.put("phone", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("sendDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("priority", "0");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}catch (Exception e) {
		e.printStackTrace();
	}
}

	@Action(value = "goodsMaterialsApplySelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogGoodsMaterialsApplyList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsApplyDialog.jsp");
	}

	@Action(value = "showDialogGoodsMaterialsApplyListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogGoodsMaterialsApplyListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		try{
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		else
			map2Query.put("stateName", SystemConstants.DIC_STATE_NEW_NAME);
		Map<String, Object> result = goodsMaterialsApplyService.findGoodsMaterialsApplyList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<GoodsMaterialsApply> list = (List<GoodsMaterialsApply>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("applyOrganize-id", "");
		map.put("applyOrganize-name", "");
		//map.put("applyDept-id", "");
		map.put("applyDept", "");
		map.put("address-id", "");
		map.put("address-name", "");
		map.put("company-id", "");
		map.put("company-name", "");
		//map.put("type-id", "");
		map.put("type", "");
		//map.put("receiveUser-id", "");
		map.put("receiveUser", "");
		map.put("phone", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("sendDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("priority", "0");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}catch (Exception e) {
		e.printStackTrace();
	}
	}
//根据主数据状态展示主数据
	@Action(value = "goodsMaterialsApplySelectByState", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogGoodsMaterialsApplyListByState() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsApplyDialogByState.jsp");
	}

	@Action(value = "showDialogGoodsMaterialsApplyListByStateJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogGoodsMaterialsApplyListByStateJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		try{
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
/*		else
			map2Query.put("stateName", SystemConstants.DIC_STATE_NEW_NAME);*/
		Map<String, Object> result = goodsMaterialsApplyService.findGoodsMaterialsApplyListByState(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<GoodsMaterialsApply> list = (List<GoodsMaterialsApply>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("applyOrganize-id", "");
		map.put("applyOrganize-name", "");
		//map.put("applyDept-id", "");
		map.put("applyDept", "");
		map.put("address-id", "");
		map.put("address-name", "");
		//map.put("type-id", "");
		map.put("type", "");
		//map.put("receiveUser-id", "");
		map.put("receiveUser", "");
		map.put("phone", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("priority", "0");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}catch (Exception e) {
		e.printStackTrace();
	}
	}
	@Action(value = "editGoodsMaterialsApply")
	public String editGoodsMaterialsApply() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			goodsMaterialsApply = goodsMaterialsApplyService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "goodsMaterialsApply");
		} else {
			goodsMaterialsApply.setId("NEW");
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			goodsMaterialsApply.setCreateUser(user);
			//获取办事处对象
			
			ApplyOrganize ao=applyOrganizeDao.showApplyOrganizeList(user.getId());
			goodsMaterialsApply.setApplyOrganize(ao);
			if(ao!=null)
			goodsMaterialsApply.setApplyDept(ao.getDept());
			goodsMaterialsApply.setCreateDate(new Date());
			//新建的时候默认加载中通快递
//			ExpressCompany ec = commonDAO.get(ExpressCompany.class, "ZT");
//			goodsMaterialsApply.setCompany(ec);
			//
		
//			//获取地区码
//			String markCode=ao.getMarkCode().getId();
//			//获取所有的地区编码一致的准备的物料明细
//			List<GoodsMaterialsReadyDetails>  list=goodsMaterialsReadyDao.selectReadDetailsByCode(markCode);
//			List<String> slist=goodsMaterialsReadyDao.stringList(markCode);
//			//采血管数量
//			int num1=0;
//			boolean flag=true;
//			//for(GoodsMaterialsReadyDetails grd:list){
//			for(int i=0;i<slist.size();i++){
//				//获取物料包编号
//				String pack=slist.get(i);
//				List<GoodsMaterialsReadyDetails>  list1=goodsMaterialsReadyDao.selectReadDetails(markCode,pack);
//				for(GoodsMaterialsReadyDetails gr:list1){
//					//获取有效期
//					Date d=gr.getValidity();
//					DateFormat format=new SimpleDateFormat("yyyy-MM-dd");
//					String stime=format.format(d);
//					//物料包编号
//					//String pCode=grd.getPackCode();
//					//判断当前日期加三个月是否等于有效期
//					Date date = new Date();
//					Calendar cal = Calendar.getInstance();
//					cal.setTime(date);
//					cal.add(Calendar.MONTH,3);
//					String s=(new SimpleDateFormat("yyyy-MM-dd")).format(cal.getTime());
//					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//					Date sd = sdf.parse(s);
//					int m=d.getMonth()-date.getMonth();
//					if(m>=3){
//						flag=true;
//						num1++;		
//					}else{
//						flag=false;
//					}
//				}
//				if(flag){
//					putObjToContext("count",num1);
//					putObjToContext("list",slist);
//				}
//			}
			
				//
			goodsMaterialsApply.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			goodsMaterialsApply.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(goodsMaterialsApply.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsApplyEdit.jsp");
	}
//	//根据办事处ID查询对应的地址
//	@Action(value = "showAddress")
//    public void showAddress() throws Exception {
//		String aid = getParameterFromRequest("aid");		
//		List<ApplyOrganizeAddress> list = applyOrganizeDao.showApplyOrganizeAddress(aid);			
//		putObjToContext("list" , list);
//    }
	@Action(value = "copyGoodsMaterialsApply")
	public String copyGoodsMaterialsApply() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		goodsMaterialsApply = goodsMaterialsApplyService.get(id);
		goodsMaterialsApply.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsApplyEdit.jsp");
	}
	
	


	@Action(value = "save")
	public String save() throws Exception {
		String id = goodsMaterialsApply.getId();
		String aId = getParameterFromRequest("aId");
		String aName = getParameterFromRequest("aName");
		String phone = getParameterFromRequest("phone");
		String receiver=getParameterFromRequest("receiver");
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "GoodsMaterialsApply";
			String markCode="WLSQ";
			String autoID = codingRuleService.genTransID(modelName,markCode);
			goodsMaterialsApply.setId(autoID);
		}
		Map aMap = new HashMap();
			aMap.put("goodsMaterialsApplyWay",getParameterFromRequest("goodsMaterialsApplyWayJson"));
		
			aMap.put("goodsMaterialsApplyItem",getParameterFromRequest("goodsMaterialsApplyItemJson"));
		
		goodsMaterialsApplyService.save(goodsMaterialsApply,aMap,aId,aName,phone,receiver);
		return redirect("/goods/mate/goodsMaterialsApply/editGoodsMaterialsApply.action?id=" + goodsMaterialsApply.getId());

	}

	@Action(value = "viewGoodsMaterialsApply")
	public String toViewGoodsMaterialsApply() throws Exception {
		String id = getParameterFromRequest("id");
		goodsMaterialsApply = goodsMaterialsApplyService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsApplyEdit.jsp");
	}
	

	@Action(value = "showGoodsMaterialsApplyWayList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showGoodsMaterialsApplyWayList() throws Exception {
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsApplyWay.jsp");
	}

	@Action(value = "showGoodsMaterialsApplyWayListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showGoodsMaterialsApplyWayListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = goodsMaterialsApplyService.findGoodsMaterialsApplyWayList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<GoodsMaterialsApplyWay> list = (List<GoodsMaterialsApplyWay>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("goodsMaterialsSendExpress-expressOrder", "");
			map.put("goodsMaterialsSendExpress-company", "");
			map.put("state", "");
			map.put("way", "");
			map.put("isReceive", "");
			map.put("goodsMaterialsApply-name", "");
			map.put("goodsMaterialsApply-id", "");
			map.put("predictDate", "yyyy-MM-dd");
			map.put("goodsMaterialsSendExpress-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delGoodsMaterialsApplyWay")
	public void delGoodsMaterialsApplyWay() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			goodsMaterialsApplyService.delGoodsMaterialsApplyWay(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	

	@Action(value = "showGoodsMaterialsApplyItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showGoodsMaterialsApplyItemList() throws Exception {
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsApplyItem.jsp");
	}

	@Action(value = "showGoodsMaterialsApplyItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showGoodsMaterialsApplyItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			
			
			Map<String, Object> result = goodsMaterialsApplyService.findGoodsMaterialsApplyItemList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<GoodsMaterialsApplyItem> list = (List<GoodsMaterialsApplyItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("materialsId", "");
			map.put("materialsName", "");
			map.put("goodsMaterialsDetails-code", "");//物料明细编号
			map.put("goodsMaterialsDetails-name", "");//物料明细name
			map.put("goodsMaterialsPack-id", "");//物料包id
			map.put("goodsMaterialsPack-name", "");//物料包name
			map.put("goodsMaterialsPack-num", "");//物料包num
			map.put("goodsMaterialsDetails-id", "");//物料明细id
			//map.put("goodsMaterialsDetails-name", "");
			map.put("goodsMaterialsDetails-unit-name", "");
			map.put("applyNum", "");
			map.put("wayNum", "");
			map.put("unuseNum", "");
			map.put("usedNum", "");
			map.put("state", "");
			map.put("goodsMaterialsApply-name", "");//物料申请name
			map.put("goodsMaterialsApply-id", "");//物料申请id
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delGoodsMaterialsApplyItem")
	public void delGoodsMaterialsApplyItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			goodsMaterialsApplyService.delGoodsMaterialsApplyItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public GoodsMaterialsApplyService getGoodsMaterialsApplyService() {
		return goodsMaterialsApplyService;
	}

	public void setGoodsMaterialsApplyService(GoodsMaterialsApplyService goodsMaterialsApplyService) {
		this.goodsMaterialsApplyService = goodsMaterialsApplyService;
	}

	public GoodsMaterialsApply getGoodsMaterialsApply() {
		return goodsMaterialsApply;
	}

	public void setGoodsMaterialsApply(GoodsMaterialsApply goodsMaterialsApply) {
		this.goodsMaterialsApply = goodsMaterialsApply;
	}

	//ajax
	@Action(value = "showApplyOrganize")
	public void showApplyOrganize() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.applyOrganizeService.showApplyOrganizeList(code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	//由地址加载收件人信息
	@Action(value = "showApplyOrganizeReceive")
	public void showApplyOrganizeReceive() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.applyOrganizeService.showApplyOrganizeReceiveList(code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	//保存快递信息
	@Action(value = "setToWay")
	public void setToWay() throws Exception {
		//String id=getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getRequest().getParameter("itemDataJson");
			goodsMaterialsApplyService.saveToWay(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	//判断快递是否已签收
	@Action(value = "selectCodeCount")
	public void selectCodeCount() throws Exception {
		String code = getParameterFromRequest("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			boolean flag=true;
			List<GoodsMaterialsApplyWay> list=goodsMaterialsApplyDao.showMaterialsApplyWayList1(code);
			if(list.size()>0){
				for(GoodsMaterialsApplyWay ga:list){
					if(ga.getIsReceive()==null || ga.getIsReceive().equals("0")){
						flag=false;
						break;
					}
				}
			}else{
				flag=false;
			}			
			result.put("success", true);
			result.put("data", flag);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	//获取最近一次完成的申请的明细的未使用数量
	@Action(value = "selectUnseNum")
	public void selectUnseNum() throws Exception {
		//获取物料包编号
		String packCode = getParameterFromRequest("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			//查询准备明细中未使用状态的数量
//			GoodsMaterialsApply ga=goodsMaterialsApplyDao.getGoodsMaterialsApply();
//			String id=ga.getId();
//			GoodsMaterialsApplyItem gai=goodsMaterialsApplyDao.getMaterialsPack(id,packCode);
//			if(gai!=null){
//				if(gai.getUnuseNum()!=null){
//					num=gai.getUnuseNum();
//				}else{
//					num=0;
//				}	
//			}else{
//				num=0;
//			}
			int num=Integer.valueOf(goodsMaterialsReadyDao.getReadDetailsNum(packCode).toString());
			int num1=Integer.valueOf(goodsMaterialsReadyDao.getReadDetailsOutNum(packCode).toString());
			result.put("success", true);
			result.put("data", num);
			result.put("data1", num1);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	//查询将要过期的物料包和数量
//	@Action(value = "selectApplyOrganize")
//	public void selectApplyOrganize() throws Exception {
//		String id= getParameterFromRequest("code");
//		//String date= getParameterFromRequest("date");
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			//获取办事处对象
//			ApplyOrganize ao=applyOrganizeDao.selectApplyOrganize(id);
//			//获取地区码
//			String markCode=ao.getMarkCode().getId();
//			//获取所有的地区编码一致的准备的物料明细
//			List<GoodsMaterialsReadyDetails>  list=goodsMaterialsReadyDao.selectReadDetailsByCode(markCode);
//			int num=0;
//			boolean flag=true;
//			for(GoodsMaterialsReadyDetails grd:list){
//				//获取有效期
//				Date d=grd.getValidity();
//				DateFormat format=new SimpleDateFormat("yyyy-MM-dd");
//				String stime=format.format(d);
//				//物料包编号
//				//String pCode=grd.getPackCode();
//				//判断当前日期加三个月是否等于有效期
//				Date date = new Date();
//				Calendar cal = Calendar.getInstance();
//				cal.setTime(date);
//				cal.add(Calendar.MONTH,3);
//				String s=(new SimpleDateFormat("yyyy-MM-dd")).format(cal.getTime());
//				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//				Date sd = sdf.parse(s);
//				int m=d.getMonth()-date.getMonth();
//				if(m>=3){
//					flag=true;
//					num++;		
//				}else{
//					flag=false;
//				}
//			}
//			if(flag){
//				result.put("success", true);
//				result.put("data", num);
//			}else{
//				result.put("success", false);
//			}
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}
}
