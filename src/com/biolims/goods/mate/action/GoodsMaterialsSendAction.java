﻿
package com.biolims.goods.mate.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.goods.mate.dao.GoodsMaterialsApplyDao;
import com.biolims.goods.mate.dao.GoodsMaterialsSendDao;
import com.biolims.goods.mate.model.GoodsMaterialsApply;
import com.biolims.goods.mate.model.GoodsMaterialsSend;
import com.biolims.goods.mate.model.GoodsMaterialsSendBox;
import com.biolims.goods.mate.model.GoodsMaterialsSendExpress;
import com.biolims.goods.mate.model.GoodsMaterialsSendItemTwo;
import com.biolims.goods.mate.service.GoodsMaterialsReadyService;
import com.biolims.goods.mate.service.GoodsMaterialsSendService;
import com.biolims.system.box.service.TransBoxService;
import com.biolims.system.express.model.ExpressCompany;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/goods/mate/goodsMaterialsSend")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class GoodsMaterialsSendAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "20013";
	@Autowired
	private GoodsMaterialsSendService goodsMaterialsSendService;
	@Resource
	private GoodsMaterialsSendDao goodsMaterialsSendDao;
	@Resource
	private GoodsMaterialsApplyDao goodsMaterialsApplyDao;
	@Resource
	private CodingRuleService codingRuleService;
	private GoodsMaterialsSend goodsMaterialsSend = new GoodsMaterialsSend();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private GoodsMaterialsReadyService goodsMaterialsReadyService;
	@Resource
	private TransBoxService transBoxService;
	@Resource
	private CommonDAO commonDAO;
	
	@Action(value = "showGoodsMaterialsSendList")
	public String showGoodsMaterialsSendList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsSend.jsp");
	}

	@Action(value = "showGoodsMaterialsSendListJson")
	public void showGoodsMaterialsSendListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = goodsMaterialsSendService.findGoodsMaterialsSendList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<GoodsMaterialsSend> list = (List<GoodsMaterialsSend>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("goodsMaterialsApply-id", "");
		map.put("goodsMaterialsApply-name", "");
		map.put("goodsMaterialsApply-applyOrganize-name", "");
		map.put("goodsMaterialsApply-receiveUser", "");
		map.put("goodsMaterialsApply-phone", "");
		map.put("goodsMaterialsApply-address-name", "");
		map.put("company-id", "");
		map.put("company-name", "");
		map.put("num", "");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("companyId", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "goodsMaterialsSendSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogGoodsMaterialsSendList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsSendDialog.jsp");
	}

	@Action(value = "showDialogGoodsMaterialsSendListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogGoodsMaterialsSendListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = goodsMaterialsSendService.findGoodsMaterialsSendList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<GoodsMaterialsSend> list = (List<GoodsMaterialsSend>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("goodsMaterialsApply-id", "");
		map.put("goodsMaterialsApply-name", "");
		map.put("goodsMaterialsApply-applyOrganize-name", "");
		map.put("goodsMaterialsApply-receiveUser", "");
		map.put("goodsMaterialsApply-phone", "");
		map.put("goodsMaterialsApply-address-name", "");
		map.put("company-id", "");
		map.put("company-name", "");
		map.put("num", "");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		map.put("companyId", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}
	//根据状态显示主数据
	@Action(value = "goodsMaterialsSendSelectByState", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogGoodsMaterialsSendListByState() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsSendDialogByState.jsp");
	}

	@Action(value = "showDialogGoodsMaterialsSendListByStateJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogGoodsMaterialsSendListByStateJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = goodsMaterialsSendService.findGoodsMaterialsSendListByState(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<GoodsMaterialsSend> list = (List<GoodsMaterialsSend>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("goodsMaterialsApply-id", "");
		map.put("goodsMaterialsApply-name", "");
		map.put("goodsMaterialsApply-applyOrganize-name", "");
//		map.put("goodsMaterialsApply-receiveUser", "");
//		map.put("goodsMaterialsApply-phone", "");
//		map.put("goodsMaterialsApply-address-name", "");
//		map.put("company-id", "");
//		map.put("company-name", "");
		map.put("num", "");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editGoodsMaterialsSend")
	public String editGoodsMaterialsSend() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			goodsMaterialsSend = goodsMaterialsSendService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "goodsMaterialsSend");
		} else {
			goodsMaterialsSend.setId("NEW");
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			goodsMaterialsSend.setCreateUser(user);
			goodsMaterialsSend.setCreateDate(new Date());
			goodsMaterialsSend.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			goodsMaterialsSend.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(goodsMaterialsSend.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsSendEdit.jsp");
	}

	@Action(value = "copyGoodsMaterialsSend")
	public String copyGoodsMaterialsSend() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		goodsMaterialsSend = goodsMaterialsSendService.get(id);
		goodsMaterialsSend.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsSendEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = goodsMaterialsSend.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "GoodsMaterialsSend";
			String markCode="WLFF";
			String autoID = codingRuleService.genTransID(modelName,markCode);
			goodsMaterialsSend.setId(autoID);
		}
		Map aMap = new HashMap();
		aMap.put("goodsMaterialsSendTwoItem",getParameterFromRequest("goodsMaterialsSendItemTwoJson"));
	
		aMap.put("goodsMaterialsSendBox",getParameterFromRequest("goodsMaterialsSendBoxJson"));
	
		aMap.put("goodsMaterialsSendExpress",getParameterFromRequest("goodsMaterialsSendExpressJson"));
		
		goodsMaterialsSendService.save(goodsMaterialsSend,aMap);
		//goodsMaterialsSendService.setExpressToApply(id);
		return redirect("/goods/mate/goodsMaterialsSend/editGoodsMaterialsSend.action?id=" + goodsMaterialsSend.getId());

	}

	@Action(value = "viewGoodsMaterialsSend")
	public String toViewGoodsMaterialsSend() throws Exception {
		String id = getParameterFromRequest("id");
		goodsMaterialsSend = goodsMaterialsSendService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsSendEdit.jsp");
	}
	

	@Action(value = "showGoodsMaterialsSendItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showGoodsMaterialsSendItemList() throws Exception {
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsSendItem.jsp");
	}

//	@Action(value = "showGoodsMaterialsSendItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showGoodsMaterialsSendItemListJson() throws Exception {
//		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//		try {
//			String scId = getRequest().getParameter("id");
//			String appId = getRequest().getParameter("appId");
//			Map<String, Object> result = goodsMaterialsSendService.findGoodsMaterialsSendItemList(scId,appId, startNum, limitNum, dir,
//					sort);
//			Long total = (Long) result.get("total");
//			List<GoodsMaterialsSendItem> list = (List<GoodsMaterialsSendItem>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("goodsMaterialsApplyItem-id", "");
//			map.put("goodsMaterialsApplyItem-goodsMaterialsPack-id", "");
//			map.put("goodsMaterialsApplyItem-goodsMaterialsPack-name", "");
//			map.put("goodsMaterialsApplyItem-applyNum", "");
//			map.put("goodsMaterialsApplyItem-sendNum", "");
//			map.put("goodsMaterialsApplyItem-wayNum", "");
//			map.put("goodsMaterialsApplyItem-unuseNum", "");
//			map.put("goodsMaterialsApplyItem-usedNum", "");
////			map.put("goodsMaterialsApplyItem-state", "");
//			map.put("state", "");
//			//map.put("goodsMaterialsApplyItem-name", "");
//			map.put("goodsMaterialsApply-name", "");
//			map.put("goodsMaterialsApply-id", "");
//			map.put("goodsMaterialsSend-name", "");
//			map.put("goodsMaterialsSend-id", "");
//			map.put("goodsMaterialsValidity", "yyyy-MM-dd");
//			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//		/**
//	 * 删除明细信息
//	 * @throws Exception
//	 */
//	@Action(value = "delGoodsMaterialsSendItem")
//	public void delGoodsMaterialsSendItem() throws Exception {
//		Map<String, Object> map = new HashMap<String, Object>();
//		try {
//			String[] ids = getRequest().getParameterValues("ids[]");
//			goodsMaterialsSendService.delGoodsMaterialsSendItem(ids);
//			map.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			map.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
	/**
	 * 加载明细2
	 * @return2
	 * @throws Exception
	 */
	@Action(value = "showGoodsMaterialsSendItemListTwo", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showGoodsMaterialsSendItemListTwo() throws Exception {
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsSendItem2.jsp");
	}

	@Action(value = "showGoodsMaterialsSendItemListTwoJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showGoodsMaterialsSendItemListTwoJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			String appId = getRequest().getParameter("appId");
			Map<String, Object> result = goodsMaterialsSendService.findGoodsMaterialsSendItemList(scId,appId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<GoodsMaterialsSendItemTwo> list = (List<GoodsMaterialsSendItemTwo>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("materialsId", "");
			map.put("materialsName", "");
			map.put("goodsMaterialsDetails-code", "");//物料明细编号
			map.put("goodsMaterialsDetails-name", "");//物料明细name
			map.put("goodsMaterialsPack-id", "");//物料包id
			map.put("goodsMaterialsPack-name", "");//物料包name
			map.put("goodsMaterialsPack-num", "");//物料包num
			map.put("goodsMaterialsDetails-id", "");//物料明细id
			//map.put("goodsMaterialsDetails-name", "");
			map.put("goodsMaterialsDetails-unit-name", "");
			map.put("applyNum", "");
			map.put("wayNum", "");
			map.put("unuseNum", "");
			map.put("usedNum", "");
			map.put("state", "");
			map.put("goodsMaterialsSend-name", "");//物料发送name
			map.put("goodsMaterialsSend-id", "");//物料发送id
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delGoodsMaterialsSendItem")
	public void delGoodsMaterialsSendItem2() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			goodsMaterialsSendService.delGoodsMaterialsSendItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	

	@Action(value = "showGoodsMaterialsSendBoxList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showGoodsMaterialsSendBoxList() throws Exception {
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsSendBox.jsp");
	}

	@Action(value = "showGoodsMaterialsSendBoxListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showGoodsMaterialsSendBoxListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = goodsMaterialsSendService.findGoodsMaterialsSendBoxList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<GoodsMaterialsSendBox> list = (List<GoodsMaterialsSendBox>) result.get("list");
//			Map<String, String> map2Query = new HashMap<String, String>();
//			Map<String, Object> result = transBoxService.findTransBoxList(map2Query, startNum, limitNum, dir, sort);
//			Long total = (Long) result.get("total");
//			List<TransBox> list = (List<TransBox>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("transBox-name", "");
			map.put("transBox-id", "");
			map.put("boxSize", "");
			map.put("num", "0");
			map.put("goodsMaterialsSend-name", "");
			map.put("goodsMaterialsSend-id", "");
//			map.put("id", "");
//			map.put("name", "");
//			map.put("id", "");
//			map.put("boxSize", "");
//			map.put("num", "");
			
			
			map.put("boxType", "");
			map.put("boxName", "");
			map.put("boxSpec", "");
			map.put("boxNum", "");
		
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delGoodsMaterialsSendBox")
	public void delGoodsMaterialsSendBox() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			goodsMaterialsSendService.delGoodsMaterialsSendBox(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	

	@Action(value = "showGoodsMaterialsSendExpressList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showGoodsMaterialsSendExpressList() throws Exception {
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsSendExpress.jsp");
	}

	@Action(value = "showGoodsMaterialsSendExpressListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showGoodsMaterialsSendExpressListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			String appId = getRequest().getParameter("appId");
			Map<String, Object> result = goodsMaterialsSendService.findGoodsMaterialsSendExpressList(scId, appId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<GoodsMaterialsSendExpress> list = (List<GoodsMaterialsSendExpress>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("linkUser", "");
			map.put("phone", "");
			map.put("receiveUser", "");
			map.put("receiveDate", "yyyy-MM-dd");
			map.put("planDate", "yyyy-MM-dd");
			map.put("realDate", "yyyy-MM-dd");
			map.put("note", "");
			map.put("port", "");
			map.put("address", "");
			//map.put("company-id", "");
			map.put("company", "");
			map.put("goodsMaterialsSend-name", "");
			map.put("goodsMaterialsSend-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delGoodsMaterialsSendExpress")
	public void delGoodsMaterialsSendExpress() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			goodsMaterialsSendService.delGoodsMaterialsSendExpress(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	

	//===========2015-12-01 ly============
	/**
	 * 根据ID删除
	 * @throws Exception
	 */
	@Action(value = "delGoodsMaterialsSendOne")
	public void delGoodsMaterialsSendOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids=getParameterFromRequest("ids");
			goodsMaterialsSendService.delGoodsMaterialsSendOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	//=======================
	//根据准备单加载子表明细
	@Action(value = "showGoodsMaterialsReadyItemList")
	public void showGoodsMaterialsReadyItemList() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.goodsMaterialsReadyService.showGoodsMaterialsReadyItemList(code);
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public GoodsMaterialsSendService getGoodsMaterialsSendService() {
		return goodsMaterialsSendService;
	}

	public void setGoodsMaterialsSendService(GoodsMaterialsSendService goodsMaterialsSendService) {
		this.goodsMaterialsSendService = goodsMaterialsSendService;
	}

	public GoodsMaterialsSend getGoodsMaterialsSend() {
		return goodsMaterialsSend;
	}

	public void setGoodsMaterialsSend(GoodsMaterialsSend goodsMaterialsSend) {
		this.goodsMaterialsSend = goodsMaterialsSend;
	}

	//加载子表明细
	@Action(value = "showTransBoxList")
	public void showTransBoxList() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.transBoxService.showTransBoxList();
			result.put("success", true);
			result.put("data", dataListMap);

		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
//	//将快递信息返回到申请页面
//	@Action(value = "setExpressToWay")
//	public void setExpressToWay() throws Exception {
//		String id=getRequest().getParameter("code");
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			List<Map<String, String>> dataListMap = this.goodsMaterialsSendService.setExpressToApply(id);
//			result.put("success", true);
//			result.put("data", dataListMap);
//
//		} catch (Exception e) {
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}
	//生成快递单
//	@Action(value = "editExpress")
//	public void editExpress() throws Exception {
//		String id=getRequest().getParameter("code");
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			List<Map<String, String>> dataListMap = this.goodsMaterialsSendService.editExpress(id);
//			result.put("success", true);
//			result.put("data", dataListMap);
//
//		} catch (Exception e) {
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}
	@Action(value = "selectPrintPath")
	public void selectPrintPath() throws Exception {
		//获取快递公司编号
		String cid = getParameterFromRequest("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			ExpressCompany ec=commonDAO.get(ExpressCompany.class, cid);
			String path=ec.getPrintPath();
			result.put("success", true);
			result.put("data", path);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	
	/**
	 * 查询申请数据
	 * 
	 */
	@Action(value = "showGoodsMaterialsApplyListJsonById")
	public void showGoodsMaterialsApplyListJsonById() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		String appId = getRequest().getParameter("appid");
		GoodsMaterialsApply app = this.goodsMaterialsSendService.getApp(appId);
		result.put("data", app);
		result.put("address", app.getAddress().getName());
		result.put("company", app.getCompany().getName());
		
		HttpUtils.write(JsonUtils.toJsonString(result));

}
	
}
