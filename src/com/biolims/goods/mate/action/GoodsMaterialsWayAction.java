﻿
package com.biolims.goods.mate.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.file.service.FileInfoService;
import com.biolims.goods.mate.model.GoodsMaterialsWay;
import com.biolims.goods.mate.service.GoodsMaterialsWayService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/goods/mate/goodsMaterialsWay")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class GoodsMaterialsWayAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "20015";
	@Autowired
	private GoodsMaterialsWayService goodsMaterialsWayService;
	private GoodsMaterialsWay goodsMaterialsWay = new GoodsMaterialsWay();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showGoodsMaterialsWayList")
	public String showGoodsMaterialsWayList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsWay.jsp");
	}

	@Action(value = "showGoodsMaterialsWayListJson")
	public void showGoodsMaterialsWayListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = goodsMaterialsWayService.findGoodsMaterialsWayList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<GoodsMaterialsWay> list = (List<GoodsMaterialsWay>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("isSend", "");
		map.put("expressOrder", "");
		map.put("checked", "");
		map.put("address", "");
		map.put("company", "");
		map.put("expectDate", "yyyy-MM-dd");
		map.put("realDate", "yyyy-MM-dd");
		map.put("sendDate", "yyyy-MM-dd");
		map.put("tackDate", "yyyy-MM-dd");
		map.put("predictTackDate", "yyyy-MM-dd");
		map.put("way", "");
		map.put("note", "");
		//map.put("goodsMaterialsPost-name", "");
		map.put("goodsMaterialsPostItem-id", "");
		map.put("receiver", "");
		map.put("dateOfLodgment", "yyyy-MM-dd");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public GoodsMaterialsWayService getGoodsMaterialsWayService() {
		return goodsMaterialsWayService;
	}

	public void setGoodsMaterialsWayService(GoodsMaterialsWayService goodsMaterialsWayService) {
		this.goodsMaterialsWayService = goodsMaterialsWayService;
	}

	public GoodsMaterialsWay getGoodsMaterialsWay() {
		return goodsMaterialsWay;
	}

	public void setGoodsMaterialsWay(GoodsMaterialsWay goodsMaterialsWay) {
		this.goodsMaterialsWay = goodsMaterialsWay;
	}
	//将快递信息返回到物流跟踪页面
	@Action(value = "setMaterialsToWay")
	public void setMaterialsToWay() throws Exception {
		//String id=getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getRequest().getParameter("itemDataJson");
			goodsMaterialsWayService.saveToWay(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
