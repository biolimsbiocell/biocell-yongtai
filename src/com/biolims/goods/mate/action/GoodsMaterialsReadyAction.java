﻿
package com.biolims.goods.mate.action;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.rmi.UnknownHostException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.SystemConstants;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.code.service.CodingRuleService;
import com.biolims.common.model.user.User;
import com.biolims.common.service.CommonService;
import com.biolims.common.service.SystemCodeService;
import com.biolims.dic.model.DicType;
import com.biolims.file.service.FileInfoService;
import com.biolims.goods.mate.model.GoodsMaterialsReady;
import com.biolims.goods.mate.model.GoodsMaterialsReadyDetails;
import com.biolims.goods.mate.model.GoodsMaterialsReadyItem;
import com.biolims.goods.mate.model.GoodsMaterialsSend;
import com.biolims.goods.mate.service.GoodsMaterialsApplyService;
import com.biolims.goods.mate.service.GoodsMaterialsReadyService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/goods/mate/goodsMaterialsReady")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class GoodsMaterialsReadyAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "20012";
	@Autowired
	private GoodsMaterialsReadyService goodsMaterialsReadyService;
	@Autowired
	private CommonService commonService;
	private GoodsMaterialsReady goodsMaterialsReady = new GoodsMaterialsReady();
	private GoodsMaterialsSend goodsMaterialsSend = new GoodsMaterialsSend();
	@Resource
	private FileInfoService fileInfoService;
	@Resource
	private SystemCodeService systemCodeService;
	@Resource
	private CodingRuleService codingRuleService;
	@Resource
	private GoodsMaterialsApplyService goodsMaterialsApplyService;
	@Action(value = "showGoodsMaterialsReadyList")
	public String showGoodsMaterialsReadyList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsReady.jsp");
	}

	@Action(value = "showGoodsMaterialsReadyListJson")
	public void showGoodsMaterialsReadyListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = goodsMaterialsReadyService.findGoodsMaterialsReadyList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<GoodsMaterialsReady> list = (List<GoodsMaterialsReady>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("goodsMaterialsApply-id", "");
		map.put("goodsMaterialsApply-name", "");
		map.put("goodsMaterialsApply-applyOrganize-name", "");
		map.put("goodsMaterialsApply-receiveUser", "");
		map.put("goodsMaterialsApply-phone", "");
		map.put("goodsMaterialsApply-address-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "goodsMaterialsReadySelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogGoodsMaterialsReadyList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsReadyDialog.jsp");
	}

	@Action(value = "showDialogGoodsMaterialsReadyListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogGoodsMaterialsReadyListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = goodsMaterialsReadyService.findGoodsMaterialsReadyList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<GoodsMaterialsReady> list = (List<GoodsMaterialsReady>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("goodsMaterialsApply-id", "");
		map.put("goodsMaterialsApply-name", "");
		map.put("goodsMaterialsApply-applyOrganize-name", "");
		map.put("goodsMaterialsApply-receiveUser", "");
		map.put("goodsMaterialsApply-phone", "");
		map.put("goodsMaterialsApply-address-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}
		//===
	@Action(value = "goodsMaterialsReadyDeSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogGoodsMaterialsReadyDeList() throws Exception {
		String flag = getParameterFromRequest("flag");
		putObjToContext("flag",flag);		
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsReadyDetailsAll.jsp");
	}
	
	@Action(value = "showDialogGoodsMaterialsDeListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogGoodsMaterialsDeListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		String flag = getParameterFromRequest("flag");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		map2Query.put("state", "1");
		Map<String, Object> result = goodsMaterialsReadyService.findGoodsMaterialsReadyDetailList(map2Query, startNum, limitNum, dir, sort,flag);
		Long count = (Long) result.get("total");
		List<GoodsMaterialsReadyDetails> list = (List<GoodsMaterialsReadyDetails>) result.get("list");
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("code", "");
		map.put("packCode", "");
		map.put("num", "");
		map.put("state", "");
		map.put("goodsMaterialsReady-name", "");
		map.put("goodsMaterialsReady-id", "");
		map.put("validity", "yyyy-MM-dd");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}
	//===
	//根据状态显示主数据
	@Action(value = "goodsMaterialsReadySelectByState", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogGoodsMaterialsReadyListByState() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsReadyDialogByState.jsp");
	}

	@Action(value = "showDialogGoodsMaterialsReadyListByStateJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogGoodsMaterialsReadyListByStateJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = goodsMaterialsReadyService.findGoodsMaterialsReadyListByState(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<GoodsMaterialsReady> list = (List<GoodsMaterialsReady>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("goodsMaterialsApply-id", "");
		map.put("goodsMaterialsApply-name", "");
		map.put("goodsMaterialsApply-applyOrganize-name", "");
		map.put("goodsMaterialsApply-receiveUser", "");
		map.put("goodsMaterialsApply-phone", "");
		map.put("goodsMaterialsApply-address-name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}


	@Action(value = "editGoodsMaterialsReady")
	public String editGoodsMaterialsReady() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			goodsMaterialsReady = goodsMaterialsReadyService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "goodsMaterialsReady");
		} else {
			goodsMaterialsReady.setId("NEW");
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			goodsMaterialsReady.setCreateUser(user);
			goodsMaterialsReady.setCreateDate(new Date());
			goodsMaterialsReady.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			goodsMaterialsReady.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		toState(goodsMaterialsReady.getState());
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsReadyEdit.jsp");
	}

	@Action(value = "copyGoodsMaterialsReady")
	public String copyGoodsMaterialsReady() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		goodsMaterialsReady = goodsMaterialsReadyService.get(id);
		goodsMaterialsReady.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsReadyEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = goodsMaterialsSend.getId();
		if ((id != null && id.equals("")) || id.equals("NEW")) {
			String modelName = "GoodsMaterialsReady";
			String markCode="WLZB";
			String autoID = codingRuleService.genTransID(modelName,markCode);
			goodsMaterialsReady.setId(autoID);
		}
		Map aMap = new HashMap();
			aMap.put("goodsMaterialsReadyDetails",getParameterFromRequest("goodsMaterialsReadyDetailsJson"));
		
			aMap.put("goodsMaterialsReadyItem",getParameterFromRequest("goodsMaterialsReadyItemJson"));
		
		goodsMaterialsReadyService.save(goodsMaterialsSend,aMap);
		return redirect("/goods/mate/goodsMaterialsReady/editGoodsMaterialsReady.action?id=" + goodsMaterialsReady.getId());

	}

	@Action(value = "viewGoodsMaterialsReady")
	public String toViewGoodsMaterialsReady() throws Exception {
		String id = getParameterFromRequest("id");
		goodsMaterialsReady = goodsMaterialsReadyService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsReadyEdit.jsp");
	}
	

	@Action(value = "showGoodsMaterialsReadyDetailsList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showGoodsMaterialsReadyDetailsList() throws Exception {
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsReadyDetails.jsp");
	}
	@Action(value = "showDetailsListIsNotUse", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDetailsListIsNotUse() throws Exception {
		String code = getParameterFromRequest("code");
		putObjToContext("code",code);		
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsReadyDetailsNotUse.jsp");
	}
	@Action(value = "showDetailsListOut", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDetailsListOut() throws Exception {
		String code = getParameterFromRequest("code");
		putObjToContext("code",code);		
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsReadyDetailsOutDate.jsp");
	}
	@Action(value = "showGoodsMaterialsReadyDetailsListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showGoodsMaterialsReadyDetailsListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			//String type = getParameterFromRequest("type");
			Map<String, Object> result = new HashMap<String, Object>();
			//if(type.equals("1")){
				result = goodsMaterialsReadyService.findGoodsMaterialsSendDetailsList(scId, startNum, limitNum, dir,
						sort);
			Long total = (Long) result.get("total");
			List<GoodsMaterialsReadyDetails> list = (List<GoodsMaterialsReadyDetails>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("packCode", "");
			map.put("num", "");
			map.put("state", "");
			map.put("useState", "");
			map.put("outState", "");
			map.put("goodsMaterialsReady-name", "");
			map.put("goodsMaterialsReady-id", "");
			map.put("validity", "yyyy-MM-dd");
			
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Action(value = "showGoodsMaterialsReadyDetailsList1Json", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showGoodsMaterialsReadyDetailsList1Json() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			String code = getParameterFromRequest("code");
			Map<String, Object> result = new HashMap<String, Object>();
			//未使用的采血管
			result = goodsMaterialsReadyService.findGoodsMaterialsReadyDetailsState(scId, startNum, limitNum, dir,
						sort,code);
			Long total = (Long) result.get("total");
			List<GoodsMaterialsReadyDetails> list = (List<GoodsMaterialsReadyDetails>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("packCode", "");
			map.put("num", "");
			map.put("state", "");
			map.put("useState", "");
			map.put("outState", "");
			map.put("goodsMaterialsReady-name", "");
			map.put("goodsMaterialsReady-id", "");
			map.put("validity", "yyyy-MM-dd");
			
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Action(value = "showGoodsMaterialsReadyDetailsList2Json", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showGoodsMaterialsReadyDetailsList2Json() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			String code = getParameterFromRequest("code");
			Map<String, Object> result = new HashMap<String, Object>();
			//过期的采血管
			result = goodsMaterialsReadyService.findGoodsMaterialsReadyDetailsOut(scId, startNum, limitNum, dir,
						sort,code);
			Long total = (Long) result.get("total");
			List<GoodsMaterialsReadyDetails> list = (List<GoodsMaterialsReadyDetails>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("code", "");
			map.put("packCode", "");
			map.put("num", "");
			map.put("state", "");
			map.put("useState", "");
			map.put("outState", "");
			map.put("goodsMaterialsReady-name", "");
			map.put("goodsMaterialsReady-id", "");
			map.put("validity", "yyyy-MM-dd");
			
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delGoodsMaterialsReadyDetails")
	public void delGoodsMaterialsReadyDetails() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			goodsMaterialsReadyService.delGoodsMaterialsReadyDetails(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	//=================================
	/**
	 * 根据ID删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delGoodsMaterialsReadyDetailsOne")
	public void delGoodsMaterialsReadyDetailsOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String ids=getParameterFromRequest("ids");
			goodsMaterialsReadyService.delGoodsMaterialsReadyDetailsOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	//=================================

	@Action(value = "showGoodsMaterialsReadyItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showGoodsMaterialsReadyItemList() throws Exception {
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsReadyItem.jsp");
	}

	@Action(value = "showGoodsMaterialsReadyItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showGoodsMaterialsReadyItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = goodsMaterialsReadyService.findGoodsMaterialsReadyItemList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<GoodsMaterialsReadyItem> list = (List<GoodsMaterialsReadyItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("goodsMaterialsApplyItem-id", "");
			map.put("goodsMaterialsApplyItem-goodsMaterialsPack-id", "");
			map.put("goodsMaterialsApplyItem-goodsMaterialsPack-name", "");
			map.put("goodsMaterialsApplyItem-goodsMaterialsPack-num", "");
//			map.put("goodsMaterialsApplyItem-goodsMaterialsDetails-code", "");
//			map.put("goodsMaterialsApplyItem-goodsMaterialsDetails-name", "");
			map.put("goodsMaterialsApplyItem-applyNum", "");
//			map.put("goodsMaterialsApplyItem-goodsMaterialsDetails-unit-name", "");
			map.put("sendNum", "");
			map.put("goodsMaterialsApplyItem-wayNum", "");
			map.put("goodsMaterialsApplyItem-unuseNum", "");
			map.put("goodsMaterialsApplyItem-usedNum", "");
			map.put("state", "");
			map.put("note", "");
			map.put("goodsMaterialsReady-name", "");
			map.put("goodsMaterialsReady-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delGoodsMaterialsReadyItem")
	public void delGoodsMaterialsReadyItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			goodsMaterialsReadyService.delGoodsMaterialsReadyItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	@Action(value = "showGoodsMaterialsReadyItemList1", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showGoodsMaterialsReadyItemLis1t() throws Exception {
		return dispatcher("/WEB-INF/page/goods/mate/goodsMaterialsReadyItem1.jsp");
	}

	@Action(value = "showGoodsMaterialsReadyItemListJson1", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showGoodsMaterialsReadyItemListJson1() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = goodsMaterialsReadyService.findGoodsMaterialsReadyItemList1(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<GoodsMaterialsReadyItem> list = (List<GoodsMaterialsReadyItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("goodsMaterialsApplyItem-id", "");
			map.put("goodsMaterialsApplyItem-goodsMaterialsDetails-goodsMaterialsPack-id", "");
			map.put("goodsMaterialsApplyItem-goodsMaterialsDetails-goodsMaterialsPack-name", "");
			map.put("goodsMaterialsApplyItem-goodsMaterialsDetails-goodsMaterialsPack-num", "");
			map.put("goodsMaterialsApplyItem-goodsMaterialsDetails-code", "");
			map.put("goodsMaterialsApplyItem-goodsMaterialsDetails-name", "");
			map.put("goodsMaterialsApplyItem-applyNum", "");
			map.put("goodsMaterialsApplyItem-goodsMaterialsDetails-unit-name", "");
			map.put("sendNum", "");
			map.put("goodsMaterialsApplyItem-wayNum", "");
			map.put("goodsMaterialsApplyItem-unuseNum", "");
			map.put("goodsMaterialsApplyItem-usedNum", "");
			map.put("state", "");
			map.put("goodsMaterialsReady-name", "");
			map.put("goodsMaterialsReady-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delGoodsMaterialsReadyItem1")
	public void delGoodsMaterialsReadyItem1() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			goodsMaterialsReadyService.delGoodsMaterialsReadyItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	//========================================
	/**
	 * 根据ID删除
	 * @throws Exception
	 */
	@Action(value = "delGoodsMaterialsReadyOne")
	public void delGoodsMaterialsReadyOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
				String ids=getParameterFromRequest("ids");
				goodsMaterialsReadyService.delGoodsMaterialsReadyOne(ids);
				map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	//========================================
	//根据申请单加载子表明细
	@Action(value = "showGoodsMaterialsApplyItemList")
	public void showGoodsMaterialsApplyItemList() throws Exception {
		String code = getRequest().getParameter("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.goodsMaterialsApplyService.showGoodsMaterialsApplyItemList(code);
			result.put("success", true);
			result.put("data", dataListMap);
			//systemCodeService.getSystemCodeByNumber("321323",4);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	//设置采血管的超期状态
	@Action(value = "setOutState")
	public void setOutState() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String code = getParameterFromRequest("code");
			goodsMaterialsReadyService.setOutState(code);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public GoodsMaterialsReadyService getGoodsMaterialsReadyService() {
		return goodsMaterialsReadyService;
	}

	public void setGoodsMaterialsReadyService(GoodsMaterialsReadyService goodsMaterialsReadyService) {
		this.goodsMaterialsReadyService = goodsMaterialsReadyService;
	}

	public GoodsMaterialsReady getGoodsMaterialsReady() {
		return goodsMaterialsReady;
	}

	public void setGoodsMaterialsReady(GoodsMaterialsReady goodsMaterialsReady) {
		this.goodsMaterialsReady = goodsMaterialsReady;
	}
	
	/**
	 * 打印编码
	 * @throws Exception
	 */
	@Action(value = "makeCode", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void makeCode() throws Exception {

		String ip = getParameterFromRequest("ip");
		String id = getParameterFromRequest("id");
		//String name = getParameterFromRequest("name");
		Socket socket = null;
		OutputStream os;
		try {
			socket = new Socket();
			//向服务器端第一次发送字符串     

			//向服务器端第二次发送字符串     
			SocketAddress sa = new InetSocketAddress(ip, 9100);
			socket.connect(sa);

			os = socket.getOutputStream();

			char c = 13;
			char b = 10;
			String cb = String.valueOf(c) + String.valueOf(b);

			DicType codenamex = commonService.get(DicType.class, "codenamex");
			DicType codenamey = commonService.get(DicType.class, "codenamey");
			DicType codeidx = commonService.get(DicType.class, "codeidx");
			DicType codeidy = commonService.get(DicType.class, "codeidy");
			DicType codex = commonService.get(DicType.class, "codex");
			DicType codey = commonService.get(DicType.class, "codey");
			DicType codeidsize = commonService.get(DicType.class, "codeidsize");
			DicType codenamesize = commonService.get(DicType.class, "codenamesize");
			DicType codesize = commonService.get(DicType.class, "codesize");
			String printStr = "e PCX;*" + cb + "e IMG;*" + cb

			+ "mm" + cb + "zO" + cb + "J" + cb + "O R,P" + cb + "H75,0,T" + cb + "D 0.0,0.0" + cb
					+ "M l FNT;/iffs/gb2312" + cb + "F90;gb2312" + cb
					+ "Sl1;0.0,0.0,9.53,9.53,40.90,40.90,1"
					+ cb
					//		+ "T4.2,4.0,0,90,3.5,q70（字体大小）;" + name + cb + "T4.2,7.6,0,3,2.84,q90（字体大小）;" + id + cb
					+ "T" + codenamex.getSysCode() + "," + codenamey.getSysCode() + ",0,90,3.5,q"
					+ codenamesize.getSysCode() + ";" + cb + "T" + codeidx.getSysCode() + ","
					+ codeidy.getSysCode() + ",0,3,2.84,q" + codeidsize.getSysCode() + ";" + id + cb
					//+ "B31.6,2.5,0,DATAMATRIX,0.33(条码高度);" + id + cb + "A 1" + cb;
					+ "B" + codex.getSysCode() + "," + codey.getSysCode() + ",0,DATAMATRIX," + codesize.getSysCode()
					+ ";" + id + cb + "A 1" + cb;

			os.write(printStr.getBytes("UTF-8"));
			os.flush();

		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
				}
			}
		}
	}
}
