package com.biolims.goods.mate.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.goods.mate.service.GoodsMaterialsReadyService;

public class GoodsMaterialsReadyEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		GoodsMaterialsReadyService mbService = (GoodsMaterialsReadyService) ctx.getBean("goodsMaterialsReadyService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
