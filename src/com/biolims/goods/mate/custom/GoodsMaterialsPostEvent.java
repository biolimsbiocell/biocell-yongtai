package com.biolims.goods.mate.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.goods.mate.service.GoodsMaterialsPostService;

public class GoodsMaterialsPostEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		GoodsMaterialsPostService mbService = (GoodsMaterialsPostService) ctx.getBean("goodsMaterialsPostService");
		//mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
