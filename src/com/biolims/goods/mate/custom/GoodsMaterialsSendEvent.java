package com.biolims.goods.mate.custom;

import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.biolims.common.interfaces.ObjectEvent;
import com.biolims.goods.mate.service.GoodsMaterialsSendService;

public class GoodsMaterialsSendEvent implements ObjectEvent {
	@Override
	public String operation(String applicationTypeActionId, String contentId) throws Exception {
		WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(ServletActionContext
				.getServletContext());
		GoodsMaterialsSendService mbService = (GoodsMaterialsSendService) ctx.getBean("goodsMaterialsSendService");
		mbService.changeState(applicationTypeActionId, contentId);

		return "";
	}
}
