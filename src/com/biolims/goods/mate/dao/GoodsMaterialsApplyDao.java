package com.biolims.goods.mate.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.dic.model.DicType;
import com.biolims.goods.mate.model.GoodsMaterialsApply;
import com.biolims.goods.mate.model.GoodsMaterialsApplyItem;
import com.biolims.goods.mate.model.GoodsMaterialsApplyWay;
import com.biolims.goods.mate.model.GoodsMaterialsReadyItem;

@Repository
@SuppressWarnings("unchecked")
public class GoodsMaterialsApplyDao extends BaseHibernateDao {
	public Map<String, Object> selectGoodsMaterialsApplyList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from GoodsMaterialsApply where 1=1 ";
		if (mapForQuery.size()>0){
			key = map2where(mapForQuery);
		}else{
			key = "";
		}
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<GoodsMaterialsApply> list = new ArrayList<GoodsMaterialsApply>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}else{
				key = key + " order by id DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	//根据状态展示主数据
	public Map<String, Object> selectGoodsMaterialsApplyListByState(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from GoodsMaterialsApply where 1=1 and state='1'";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<GoodsMaterialsApply> list = new ArrayList<GoodsMaterialsApply>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}else{
				key = key + " order by priority DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
		public Map<String, Object> selectGoodsMaterialsApplyWayList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from GoodsMaterialsApplyWay where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and goodsMaterialsApply.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<GoodsMaterialsApplyWay> list = new ArrayList<GoodsMaterialsApplyWay>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}else{
				key = key + "";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectGoodsMaterialsApplyItemList(String scId,Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {

		String hql = "from GoodsMaterialsApplyItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and goodsMaterialsApply.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<GoodsMaterialsApplyItem> list = new ArrayList<GoodsMaterialsApplyItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}else{
				key = key + " order by goodsMaterialsApply.priority DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	//选择申请单加载子表信息
	public Map<String, Object> setGoodsMaterialsApplyItemList(String code) throws Exception {
		String hql = "from GoodsMaterialsApplyItem t where 1=1 ";
		String key = "";
		if (code != null)
			key = key + " and  t.goodsMaterialsApply='" + code + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<GoodsMaterialsApplyItem> list = this.getSession().createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	
	//根据主数据Id查询明细信息
	public List<GoodsMaterialsApplyItem> showGoodsMaterialsApplyItemList(String code){
		String hql = "from GoodsMaterialsApplyItem t where 1=1 and  t.goodsMaterialsApply='" + code + "'";		
		List<GoodsMaterialsApplyItem> list = this.getSession().createQuery(hql).list();
		return list;
	}
	//根据Id查询明细信息
	public List<GoodsMaterialsApplyItem> showGoodsMaterialsApplyItem(String code){
		String hql = "from GoodsMaterialsApplyItem t where 1=1 and  t.id='" + code + "'";		
		List<GoodsMaterialsApplyItem> list = this.getSession().createQuery(hql).list();
		return list;
	}
	//根据Id查主数据信息
	public List<GoodsMaterialsApply> showGoodsMaterialsApply(String code){
		String hql = "from GoodsMaterialsApply t where 1=1 and  t.id='" + code + "'";		
		List<GoodsMaterialsApply> list = this.getSession().createQuery(hql).list();
		return list;
	}
	//根据主数据Id查询物流信息
	public List<GoodsMaterialsApplyWay> showMaterialsApplyWayList(String code1) throws Exception {
		String hql = "from GoodsMaterialsApplyWay t where 1=1 and t.goodsMaterialsSendExpress='"+code1+"'";		
		List<GoodsMaterialsApplyWay> list = this.getSession().createQuery(hql).list();
		return list;
	}
	//根据主数据Id查询物流信息
	public List<GoodsMaterialsApplyWay> showMaterialsApplyWayList1(String code){
		String hql = "from GoodsMaterialsApplyWay t where 1=1 and  t.goodsMaterialsApply='" + code + "'";		
		List<GoodsMaterialsApplyWay> list = this.getSession().createQuery(hql).list();
		return list;
	}
	public List<GoodsMaterialsReadyItem> selectSumNum(String code){
		String hql = "from GoodsMaterialsReadyItem t where 1=1 and  t.goodsMaterialsApplyItem='" + code + "'";		
		List<GoodsMaterialsReadyItem> sumNum = this.getSession().createQuery(hql).list();
		return sumNum;
	}
	//根据主数据Id和物料包编号查询物料包信息
	public GoodsMaterialsApplyItem getMaterialsPack(String id,String packCode){
		String hql = "from GoodsMaterialsApplyItem t where 1=1 and  t.goodsMaterialsApply='" + id + "' and t.goodsMaterialsPack='"+packCode+"'";		
		GoodsMaterialsApplyItem list = (GoodsMaterialsApplyItem)this.getSession().createQuery(hql).uniqueResult();
		return list;
	}
	
	//查询最近一次完成的物料申请
	public GoodsMaterialsApply getGoodsMaterialsApply(){
		String hql = "from GoodsMaterialsApply t where 1=1 t.stateName='完成' and t.confirmDate = (select max(confirmDate) from GoodsMaterialsApply where 1=1)";		
		GoodsMaterialsApply ga = (GoodsMaterialsApply)this.getSession().createQuery(hql).uniqueResult();
		return ga;
	}
}