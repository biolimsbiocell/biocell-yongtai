package com.biolims.goods.mate.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.goods.mate.model.GoodsMaterialsApplyItem;
import com.biolims.goods.mate.model.GoodsMaterialsPost;
import com.biolims.goods.mate.model.GoodsMaterialsReadyItem;
import com.biolims.goods.mate.model.GoodsMaterialsReady;
import com.biolims.goods.mate.model.GoodsMaterialsReadyDetails;
import com.biolims.goods.mate.model.GoodsMaterialsSend;


@Repository
@SuppressWarnings("unchecked")
public class GoodsMaterialsReadyDao extends BaseHibernateDao {
	public Map<String, Object> selectGoodsMaterialsReadyList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from GoodsMaterialsReady where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<GoodsMaterialsReady> list = new ArrayList<GoodsMaterialsReady>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	
	//==========
	public Map<String, Object> selectGoodsMaterialsReadyDetailsList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort,String flag) {
		String key = " ";
		String hql=null;
		if(flag.equals("100")){
			 hql = " from GoodsMaterialsReadyDetails t where 1=1 ";
		}else{
			 hql = " from GoodsMaterialsReadyDetails t where 1=1 ";
		}
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<GoodsMaterialsReadyDetails> list = new ArrayList<GoodsMaterialsReadyDetails>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	//============
	//根据状态查询主数据
	public Map<String, Object> selectGoodsMaterialsReadyListByState(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from GoodsMaterialsReady where 1=1 and state='1'";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<GoodsMaterialsReady> list = new ArrayList<GoodsMaterialsReady>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
		public Map<String, Object> selectGoodsMaterialsSendDetailsList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from GoodsMaterialsReadyDetails where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and goodsMaterialsSend.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<GoodsMaterialsReadyDetails> list = new ArrayList<GoodsMaterialsReadyDetails>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectGoodsMaterialsReadyDetailsState(String scId, Integer startNum, Integer limitNum,
				String dir, String sort,String code) throws Exception {
			String hql = "from GoodsMaterialsReadyDetails where 1=1 and useState is null and packCode='"+code+"'";
			String key = "";
			if (scId != null)
				key = key + " and goodsMaterialsReady.id='" + scId + "'";
			Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
			List<GoodsMaterialsReadyDetails> list = new ArrayList<GoodsMaterialsReadyDetails>();
			if (total > 0) {
				if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
					if (sort.indexOf("-") != -1) {
						sort = sort.substring(0, sort.indexOf("-"));
					}
					key = key + " order by " + sort + " " + dir;
				} 
				if (startNum == null || limitNum == null) {
					list = this.getSession().createQuery(hql + key).list();
				} else {
					list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
				}
			}
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("total", total);
			result.put("list", list);
			return result;
		}
		public Map<String, Object> selectGoodsMaterialsReadyDetailsOut(String scId, Integer startNum, Integer limitNum,
				String dir, String sort,String code) throws Exception {
			String hql = "from GoodsMaterialsReadyDetails where 1=1 and outState='1' and packCode='"+code+"'";
			String key = "";
			if (scId != null)
				key = key + " and goodsMaterialsReady.id='" + scId + "'";
			Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
			List<GoodsMaterialsReadyDetails> list = new ArrayList<GoodsMaterialsReadyDetails>();
			if (total > 0) {
				if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
					if (sort.indexOf("-") != -1) {
						sort = sort.substring(0, sort.indexOf("-"));
					}
					key = key + " order by " + sort + " " + dir;
				} 
				if (startNum == null || limitNum == null) {
					list = this.getSession().createQuery(hql + key).list();
				} else {
					list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
				}
			}
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("total", total);
			result.put("list", list);
			return result;
		}
		public Map<String, Object> selectGoodsMaterialsReadyItemList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from GoodsMaterialsReadyItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and goodsMaterialsReady.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<GoodsMaterialsReadyItem> list = new ArrayList<GoodsMaterialsReadyItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectGoodsMaterialsReadyItemList1(String scId, Integer startNum, Integer limitNum,
				String dir, String sort) throws Exception {
			String hql = "from GoodsMaterialsReadyItem t where 1=1 and t.sendNum!=(select r.applyNum from GoodsMaterialsApplyItem r where r.id=t.goodsMaterialsApplyItem)";
			String key = "";
			if (scId != null)
				key = key + " and goodsMaterialsReady.id='" + scId + "'";
			Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
			List<GoodsMaterialsReadyItem> list = new ArrayList<GoodsMaterialsReadyItem>();
			if (total > 0) {
				if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
					if (sort.indexOf("-") != -1) {
						sort = sort.substring(0, sort.indexOf("-"));
					}
					key = key + " order by " + sort + " " + dir;
				} 
				if (startNum == null || limitNum == null) {
					list = this.getSession().createQuery(hql + key).list();
				} else {
					list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
				}
			}
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("total", total);
			result.put("list", list);
			return result;
		}
	//选择准备加载子表信息
	public Map<String, Object> setGoodsMaterialsReadyItemList(String code) throws Exception {
		String hql = "from GoodsMaterialsReadyItem t where 1=1 ";
		String key = "";
		if (code != null)
			key = key + " and  t.goodsMaterialsReady='" + code + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<GoodsMaterialsReadyItem> list = this.getSession().createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	//根据主数据Id明细信息
	public List<GoodsMaterialsReadyItem> showGoodsMaterialsReadyItemList(String code){
		String hql = "from GoodsMaterialsReadyItem t where 1=1 and  t.goodsMaterialsReady='" + code + "'";		
		List<GoodsMaterialsReadyItem> list = this.getSession().createQuery(hql).list();
		return list;
	}
	//根据申请查找准备
	public GoodsMaterialsReady showGoodsMaterialsReadyByApply(String code){
		String hql = "from GoodsMaterialsReady t where 1=1 and  t.goodsMaterialsApply.id='" + code + "'";		
		List<GoodsMaterialsReady> list = this.getSession().createQuery(hql).list();
		if(list.size()>0)
		return list.get(0);
		return null;
	}
	//根据准备查询发放
	public GoodsMaterialsSend showGoodsMaterialsSendByReady(String code){
		String hql = "from GoodsMaterialsSend t where 1=1 and  t.goodsMaterialsReady.id='" + code + "'";		
		List<GoodsMaterialsSend> list = this.getSession().createQuery(hql).list();
		if(list.size()>0)
		return list.get(0);
		return null;
	}
	//根据发放查询寄送
	public GoodsMaterialsPost showGoodsMaterialsPostBySend(String code){
		String hql = "from GoodsMaterialsPost t where 1=1 and  t.goodsMaterialsSend.id='" + code + "'";		
		List<GoodsMaterialsPost> list = this.getSession().createQuery(hql).list();
		if(list.size()>0)
			return list.get(0);
			return null;
	}
	//根据主表id查询明细
	public List<GoodsMaterialsReadyDetails> showGoodsMaterialsReadyItem(String code){
		String hql = "from GoodsMaterialsReadyDetails t where 1=1 and  t.goodsMaterialsReady.id='" + code + "'";		
		List<GoodsMaterialsReadyDetails> list = this.getSession().createQuery(hql).list();
		return list;
	}
	//根据Id明细信息
	public List<GoodsMaterialsReadyItem> showGoodsMaterialsReadyItemListById(String code){
		String hql = "from GoodsMaterialsReadyItem t where 1=1 and  t.id='" + code + "'";		
		List<GoodsMaterialsReadyItem> list = this.getSession().createQuery(hql).list();
		return list;
	}
	//根据主数据code明细信息    
	public List<GoodsMaterialsReadyDetails> getReadDetailsCode(String code){
		String hql = "from GoodsMaterialsReadyDetails t where 1=1 and  t.code='" + code + "'";		
		List<GoodsMaterialsReadyDetails> list = this.getSession().createQuery(hql).list();
		return list;
	}
	//根据编码前两位查询明细    
	public List<GoodsMaterialsReadyDetails> selectReadDetailsByCode(String code){
		String hql = "from GoodsMaterialsReadyDetails t where 1=1 and  t.code like'____" + code + "%'";		
		List<GoodsMaterialsReadyDetails> list = this.getSession().createQuery(hql).list();
		return list;
	}
	//根据编码前两位和物料包编码查询明细    
	public List<GoodsMaterialsReadyDetails> selectReadDetails(String code,String pCode){
		String hql = "from GoodsMaterialsReadyDetails t where 1=1 and  t.code like'____" + code + "%' and t.packCode='"+pCode+"'";		
		List<GoodsMaterialsReadyDetails> list = this.getSession().createQuery(hql).list();
		return list;
	}
	//根据code明细对象    
	public GoodsMaterialsReadyDetails getReadDetailsByCode(String code){
		String hql = "from GoodsMaterialsReadyDetails t where 1=1 and  t.code='" + code + "'";		
		GoodsMaterialsReadyDetails list = (GoodsMaterialsReadyDetails)this.getSession().createQuery(hql).uniqueResult();
		return list;
	}
	//查询编码前缀
	public String findIdPrefix(String modelName,String pcode) {
		String sql = "select t.id from t_application_type_table t where 1=1 and t.id = '" + modelName + "'";
		List<Map<String, Object>> list = this.findForJdbc(sql);
		String result = pcode;
		if (list != null && list.size() > 0 && list.get(0) != null && list.get(0).get("code") != null
				&& !list.get(0).get("code").equals("")) {
			result = list.get(0).get("code").toString();
		}
		return result;
	}
	public String codeParse(String val, Integer codeLength) {
		String result = null;
		if (codeLength != null && val.length() < codeLength) {
			int endVal = codeLength - val.length();
			String tem = "";
			for (int i = 0; i < endVal; i++) {
				tem = tem + 0;
			}
			result = tem + val;
		} else {
			result = val;
		}
		return result;
	}
	//查询编码的最大值
	public String selectModelTotalByType(String modelName, Map<String, String> mapForQuery) {
		String key = "";
		String hql = "select max(code) from " + modelName + " where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		return (String) this.getSession().createQuery(hql + key).uniqueResult();
	}
	//根据明细表中的申请明细ID查询发货的总数量。
	public Long selectSumNum(String code){
		String hql = "from GoodsMaterialsReadyItem t where 1=1 and  t.goodsMaterialsApplyItem='" + code + "'";		
		Long sumNum = (Long)this.getSession().createQuery("select sum(t.sendNum)"+hql).uniqueResult();
		return sumNum;
	}
	//根据物料包编码和未使用的状态查采血管数量   
	public Long getReadDetailsNum(String code){
		String hql = "from GoodsMaterialsReadyDetails t where 1=1 and  t.packCode='" + code + "' and t.useState is null";		
		Long list = (Long)this.getSession().createQuery("select count(t.code)"+hql).uniqueResult();
		return list;
	}
	//根据物料包编码和未使用的状态查采血管数量   
	public Long getReadDetailsOutNum(String code){
		String hql = "from GoodsMaterialsReadyDetails t where 1=1 and  t.packCode='" + code + "' and t.outState='1'";		
		Long list = (Long)this.getSession().createQuery("select count(t.code)"+hql).uniqueResult();
		return list;
	}
}