package com.biolims.goods.mate.dao;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.dic.model.DicType;
import com.biolims.goods.mate.model.GoodsMaterialsApply;
import com.biolims.goods.mate.model.GoodsMaterialsApplyItem;
import com.biolims.goods.mate.model.GoodsMaterialsReadyItem;
import com.biolims.goods.mate.model.GoodsMaterialsSendExpress;
import com.biolims.goods.mate.model.GoodsMaterialsSend;
import com.biolims.goods.mate.model.GoodsMaterialsSendBox;
import com.biolims.goods.mate.model.GoodsMaterialsSendItem;
import com.biolims.goods.mate.model.GoodsMaterialsSendItemTwo;
import com.biolims.sample.model.SampleCancerTemp;
import com.biolims.sample.model.SampleOrder;
import com.biolims.util.BeanUtils;

@Repository
@SuppressWarnings("unchecked")
public class GoodsMaterialsSendDao extends BaseHibernateDao {
	public Map<String, Object> selectGoodsMaterialsSendList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from GoodsMaterialsSend where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<GoodsMaterialsSend> list = new ArrayList<GoodsMaterialsSend>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}else{
				
				key = key + " order by id DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	//根据状态显示主数据
	public Map<String, Object> selectGoodsMaterialsSendListByState(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from GoodsMaterialsSend where 1=1 and state='1'";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<GoodsMaterialsSend> list = new ArrayList<GoodsMaterialsSend>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	public void copytwo(Object two, Object one,GoodsMaterialsSend send) throws Exception {
		Map<String, Object> inputMap = new HashMap<String, Object>();
		Field[] inputFields = two.getClass().getDeclaredFields();
		for (Field fd : inputFields) {
			if(fd.getName().equals("id")){
				continue;
			}
			try {
				inputMap.put(fd.getName(),
						BeanUtils.getFieldValue(one, fd.getName()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		Field[] inputTempFields = two.getClass().getDeclaredFields();
		for (Field fd : inputTempFields) {
			if (inputMap.get(fd.getName()) == null) {
				continue;
			}
			try {
				// 给属性赋值
				BeanUtils.setFieldValue(two, fd.getName(),
						inputMap.get(fd.getName()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		//两条记录基本一致，直接修改订单纪录
		GoodsMaterialsSendItemTwo twos = (GoodsMaterialsSendItemTwo) two;
		twos.setGoodsMaterialsSend(send);
		merge(twos);
		
	}
	//查询物料申请明细
	public void selectAndsaveApply(String scId,String appId) throws Exception{
		//查询物料申请明细
		String hqlapply = "from GoodsMaterialsApplyItem where 1=1 ";
		String keyapply = "";
		if (appId != null)
			keyapply = keyapply + " and goodsMaterialsApply.id='" + appId + "'";	
		List<GoodsMaterialsApplyItem> listapply = new ArrayList<GoodsMaterialsApplyItem>();
		listapply = this.getSession().createQuery(hqlapply + keyapply).list();
		//查询物料发放明细
		GoodsMaterialsSend gs  = get(GoodsMaterialsSend.class, scId);
		
		//把物料申请明细赋值到物料发放明细中
		for (GoodsMaterialsApplyItem app : listapply) {
			GoodsMaterialsSendItemTwo item = new GoodsMaterialsSendItemTwo();
			copytwo(item,app,gs);
		}
		
	}
	public Map<String, Object> selectGoodsMaterialsSendItemList(String scId,String appId,Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		//查询物料发放明细	
		String hqlq = "from GoodsMaterialsSendItemTwo where 1=1 ";
		String keyq = "";
		if (scId != null)
			keyq = keyq + " and goodsMaterialsSend.id='" + scId + "'";
		Long totalq = (Long) this.getSession().createQuery("select count(*) " + hqlq + keyq ).uniqueResult();
		if(totalq<=0){
			selectAndsaveApply(scId,appId);
		}

		//查询物料发放明细	
		String hql = "from GoodsMaterialsSendItemTwo where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and goodsMaterialsSend.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<GoodsMaterialsSendItemTwo> list = new ArrayList<GoodsMaterialsSendItemTwo>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		public Map<String, Object> selectGoodsMaterialsSendBoxList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from GoodsMaterialsSendBox where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and goodsMaterialsSend.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<GoodsMaterialsSendBox> list = new ArrayList<GoodsMaterialsSendBox>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
		
		
		public Map<String, Object> selectGoodsMaterialsSendExpressList(String scId,String appId,Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		
		String hql = "from GoodsMaterialsSendExpress where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and goodsMaterialsSend.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<GoodsMaterialsSendExpress> list = new ArrayList<GoodsMaterialsSendExpress>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	//选择发放单加载快递单信息
	public Map<String, Object> setGoodsMaterialsSendExpressList(String code) throws Exception {
		String hql = "from GoodsMaterialsSendExpress t where 1=1 ";
		String key = "";
		if (code != null)
			key = key + " and  t.goodsMaterialsSend='" + code + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<GoodsMaterialsSendExpress> list = this.getSession().createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	//生成快递单
	public Map<String, Object> editExpress(String code) throws Exception {
		String hql = "from GoodsMaterialsSendItem t where 1=1 ";
		String key = "";
		if (code != null)
			key = key + " and  t.goodsMaterialsSend='" + code + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<GoodsMaterialsSendItem> list = this.getSession().createQuery(hql + key).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	//将快递信息返回到申请页面
	public List<GoodsMaterialsSendExpress> setExpressListToApply(String code){
		String hql = "from GoodsMaterialsSendExpress t where 1=1 and t.goodsMaterialsSend='"+code+"'";
		List<GoodsMaterialsSendExpress> list = this.getSession().createQuery(hql).list();
		return list;
	}
	
		//根据Id查询明细信息
		public List<GoodsMaterialsReadyItem> showGoodsMaterialsReadyItem(String code){
			String hql = "from GoodsMaterialsReadyItem t where 1=1 and  t.id='" + code + "'";		
			List<GoodsMaterialsReadyItem> list = this.getSession().createQuery(hql).list();
			return list;
		}
		
		//根据Id查询明细信息
		public List<GoodsMaterialsSendItem> showGoodsMaterialsSendItem(String code){
			String hql = "from GoodsMaterialsSendItem t where 1=1 and  t.goodsMaterialsSend='" + code + "'";		
			List<GoodsMaterialsSendItem> list = this.getSession().createQuery(hql).list();
			return list;
		}
	//查询快递公司名称
//	public String showCompanyList(String code) throws Exception {
//		String hql = "from DicType t where 1=1 and  t.id='" + code + "'";		
//		String list = this.getSession().createQuery("select name "+hql).;
//		return list;
//	}
	/**
	 * 根据主数据Id查询运输箱数量
	 * @param code
	 * @return
	 */
	public Long selectBoxNum(String code) {
		String hql = "from GoodsMaterialsSendBox t where 1=1 and t.goodsMaterialsSend='"+code+"'";
		String totals =  (String) this.getSession().createQuery("select sum(t.boxNum) " + hql).uniqueResult();
		Long total = Long.valueOf(totals);
		return total;
	}
	/**
	 * 查询申请
	 */
	public Map<String, Object> selectGoodsMaterialsApplyList(String appId, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from GoodsMaterialsApply where 1=1 ";
		if (appId != null)
			key = key + " and  id='" + appId + "'";
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<GoodsMaterialsApply> list = new ArrayList<GoodsMaterialsApply>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}else{
				key = key + " order by priority DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
}