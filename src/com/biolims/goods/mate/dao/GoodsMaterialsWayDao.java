package com.biolims.goods.mate.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.biolims.common.dao.CommonDAO;
import com.biolims.dao.BaseHibernateDao;
import com.biolims.goods.mate.model.GoodsMaterialsWay;
import com.biolims.goods.sample.model.GoodsSampleWay;
import com.biolims.system.organize.model.ApplyOrganizeAddress;

@Repository
@SuppressWarnings("unchecked")
public class GoodsMaterialsWayDao extends BaseHibernateDao {
	public Map<String, Object> selectGoodsMaterialsWayList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from GoodsMaterialsWay where 1=1 ";
		if (mapForQuery.size()>0){
			key = map2where(mapForQuery);
		}else{
			key=" and (isSend is null or isSend='0')";
		}
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<GoodsMaterialsWay> list = new ArrayList<GoodsMaterialsWay>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}else{
				
				key = key + " order by id DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	//
	public List<GoodsMaterialsWay> setOrderList(String id) {
		String hql = "from GoodsMaterialsWay t where t.code='" + id + "'";
		List<GoodsMaterialsWay> list = this.getSession().createQuery(hql).list();
		return list;
	}
	public GoodsMaterialsWay selectway(String id) {
		String hql = "from GoodsMaterialsWay t where t.code='" + id + "'";
		GoodsMaterialsWay list = (GoodsMaterialsWay)this.getSession().createQuery(hql).uniqueResult();
		return list;
	}
	//根据收货地址查询收货地址对象
	public ApplyOrganizeAddress selectAddress(String name) {
		String hql = "from ApplyOrganizeAddress t where t.name='" + name + "'";
		ApplyOrganizeAddress list = (ApplyOrganizeAddress)this.getSession().createQuery(hql).uniqueResult();
		return list;
	}
}