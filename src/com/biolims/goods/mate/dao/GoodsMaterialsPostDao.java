package com.biolims.goods.mate.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.goods.mate.model.GoodsMaterialsApplyItem;
import com.biolims.goods.mate.model.GoodsMaterialsPost;
import com.biolims.goods.mate.model.GoodsMaterialsPostItem;

@Repository
@SuppressWarnings("unchecked")
public class GoodsMaterialsPostDao extends BaseHibernateDao {
	public Map<String, Object> selectGoodsMaterialsPostList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from GoodsMaterialsPost where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<GoodsMaterialsPost> list = new ArrayList<GoodsMaterialsPost>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}else{
				
				key = key + " order by id DESC";
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
	public Map<String, Object> selectGoodsMaterialsPostItemList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from GoodsMaterialsPostItem where 1=1 ";
		if (mapForQuery.size()>0){
			key = map2where(mapForQuery);
		}else{
			key=" and (submit='0' or submit is null)";
		}
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<GoodsMaterialsPostItem> list = new ArrayList<GoodsMaterialsPostItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
//		public Map<String, Object> selectGoodsMaterialsPostItemList(String scId, Integer startNum, Integer limitNum,
//			String dir, String sort) throws Exception {
//		String hql = "from GoodsMaterialsPostItem where 1=1 ";
//		String key = "";
//		if (scId != null)
//			key = key + " and goodsMaterialsPost.id='" + scId + "'";
//		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
//		List<GoodsMaterialsPostItem> list = new ArrayList<GoodsMaterialsPostItem>();
//		if (total > 0) {
//			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
//				if (sort.indexOf("-") != -1) {
//					sort = sort.substring(0, sort.indexOf("-"));
//				}
//				key = key + " order by " + sort + " " + dir;
//			} 
//			if (startNum == null || limitNum == null) {
//				list = this.getSession().createQuery(hql + key).list();
//			} else {
//				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
//			}
//		}
//		Map<String, Object> result = new HashMap<String, Object>();
//		result.put("total", total);
//		result.put("list", list);
//		return result;
//	}
	//根据主数据ID查询快递信息
	public List<GoodsMaterialsPostItem> saveToWay(String code){
		String hql = "from GoodsMaterialsPostItem t where 1=1 and  t.goodsMaterialsPost='" + code + "'";		
		List<GoodsMaterialsPostItem> list = this.getSession().createQuery(hql).list();
		return list; 
	}
}