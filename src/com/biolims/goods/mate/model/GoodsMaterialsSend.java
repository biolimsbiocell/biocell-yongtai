package com.biolims.goods.mate.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.common.model.user.User;
import com.biolims.dic.model.DicType;
import com.biolims.dic.model.DicCurrencyType;
import com.biolims.common.model.user.User;
/**   
 * @Title: Model
 * @Description: 物料发放
 * @author lims-platform
 * @date 2015-11-03 16:21:11
 * @version V1.0   
 *
 */
@Entity
@Table(name = "GOODS_MATERIALS_SEND")
@SuppressWarnings("serial")
public class GoodsMaterialsSend extends EntityDao<GoodsMaterialsSend> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
	/**创建人*/
	private User createUser;
	/**创建日期*/
	private Date createDate;
	/**物料申请*/
	private GoodsMaterialsApply goodsMaterialsApply;
//	/**申请组织*/
//	private String goodsMaterialsApply;
//	/**收件人*/
//	private String receiveUser;
//	/**收件人电话*/
//	private String phone;
	/**快递公司ID*/
	private String companyId;
	/**快递公司*/
	private DicType company;
	/**运输箱合计*/
	private Integer num;
	/**审核人*/
	private User confirmUser;
	/**审核日期*/
	private Date confirmDate;
	/**工作流状态*/
	private String state;
	/**工作流状态*/
	private String stateName;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@Column(name ="ID", length = 36)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 120)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得User
	 *@return: User  创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser(){
		return this.createUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  创建人
	 */
	public void setCreateUser(User createUser){
		this.createUser = createUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  创建日期
	 */
	@Column(name ="CREATE_DATE", length = 255)
	public Date getCreateDate(){
		return this.createDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  创建日期
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 *方法: 取得GoodsMaterialsReady
	 *@return: GoodsMaterialsReady  物料申请
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GOODS_MATERIALS_APPLY")
	public GoodsMaterialsApply getGoodsMaterialsApply() {
		return goodsMaterialsApply;
	}
	/**
	 *方法: 设置GoodsMaterialsReady
	 *@param: GoodsMaterialsReady  物料申请
	 */
 	public void setGoodsMaterialsApply(GoodsMaterialsApply goodsMaterialsApply) {
		this.goodsMaterialsApply = goodsMaterialsApply;
	}
	//	/**
//	 *方法: 取得String
//	 *@return: String  申请组织
//	 */
//	@Column(name ="GOODS_MATERIALS_APPLY", length = 60)
//	public String getGoodsMaterialsApply(){
//		return this.goodsMaterialsApply;
//	}
//	/**
//	 *方法: 设置String
//	 *@param: String  申请组织
//	 */
//	public void setGoodsMaterialsApply(String goodsMaterialsApply){
//		this.goodsMaterialsApply = goodsMaterialsApply;
//	}
//	/**
//	 *方法: 取得String
//	 *@return: String  收件人
//	 */
//	@Column(name ="RECEIVE_USER", length = 60)
//	public String getReceiveUser(){
//		return this.receiveUser;
//	}
//	/**
//	 *方法: 设置String
//	 *@param: String  收件人
//	 */
//	public void setReceiveUser(String receiveUser){
//		this.receiveUser = receiveUser;
//	}
//	/**
//	 *方法: 取得String
//	 *@return: String  收件人电话
//	 */
//	@Column(name ="PHONE", length = 60)
//	public String getPhone(){
//		return this.phone;
//	}
//	/**
//	 *方法: 设置String
//	 *@param: String  收件人电话
//	 */
//	public void setPhone(String phone){
//		this.phone = phone;
//	}
//	/**
//	 *方法: 取得String
//	 *@return: String  收货地址
//	 */
//	@Column(name ="ADDRESS", length = 60)
//	public String getAddress(){
//		return this.address;
//	}
//	/**
//	 *方法: 设置String
//	 *@param: String  收货地址
//	 */
//	public void setAddress(String address){
//		this.address = address;
//	}
	/**
	 *方法: 取得DicType
	 *@return: DicType  快递公司
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "COMPANY")
	public DicType getCompany(){
		return this.company;
	}
	/**
	 *方法: 设置DicType
	 *@param: DicType  快递公司
	 */
	public void setCompany(DicType company){
		this.company = company;
	}
	/**
	 *方法: 取得Integer
	 *@return: Integer  运输箱合计
	 */
	@Column(name ="NUM", length = 20)
	public Integer getNum(){
		return this.num;
	}
	/**
	 *方法: 设置Integer
	 *@param: Integer  运输箱合计
	 */
	public void setNum(Integer num){
		this.num = num;
	}
	/**
	 *方法: 取得User
	 *@return: User  审核人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CONFIRM_USER")
	public User getConfirmUser(){
		return this.confirmUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  审核人
	 */
	public void setConfirmUser(User confirmUser){
		this.confirmUser = confirmUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  审核日期
	 */
	@Column(name ="CONFIRM_DATE", length = 255)
	public Date getConfirmDate(){
		return this.confirmDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  审核日期
	 */
	public void setConfirmDate(Date confirmDate){
		this.confirmDate = confirmDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  工作流状态
	 */
	@Column(name ="STATE", length = 60)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  工作流状态
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  工作流状态
	 */
	@Column(name ="STATE_NAME", length = 60)
	public String getStateName(){
		return this.stateName;
	}
	/**
	 *方法: 设置String
	 *@param: String  工作流状态
	 */
	public void setStateName(String stateName){
		this.stateName = stateName;
	}
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	
}