package com.biolims.goods.mate.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.system.box.model.TransBox;

import com.biolims.dao.EntityDao;

/**   
 * @Title: Model
 * @Description: 物料运输箱明细
 * @author lims-platform
 * @date 2015-11-03 16:20:51
 * @version V1.0   
 *
 */
@Entity
@Table(name = "GOODS_MATERIALS_SEND_BOX")
@SuppressWarnings("serial")
public class GoodsMaterialsSendBox extends EntityDao<GoodsMaterialsSendBox> implements java.io.Serializable {
	/**编码*/
	private String id;
	/**运输箱种类*/
	private TransBox transBox;
	/**规格*/
	private String boxSize;
	/**数量*/
	private Integer num;
	/**相关主表*/
	private GoodsMaterialsSend goodsMaterialsSend;
	
	
	

	//运输巷种类
	private String boxType;
	//运输箱名称
	private String boxName;
	//运输箱规格
	private String boxSpec;
	//运输箱数量
	private String boxNum;
	
	
	
	
	public String getBoxType() {
		return boxType;
	}
	public void setBoxType(String boxType) {
		this.boxType = boxType;
	}
	public String getBoxName() {
		return boxName;
	}
	public void setBoxName(String boxName) {
		this.boxName = boxName;
	}
	public String getBoxSpec() {
		return boxSpec;
	}
	public void setBoxSpec(String boxSpec) {
		this.boxSpec = boxSpec;
	}
	public String getBoxNum() {
		return boxNum;
	}
	public void setBoxNum(String boxNum) {
		this.boxNum = boxNum;
	}
	
	
	/**
	 *方法: 取得String
	 *@return: String  编码
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 60)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编码
	 */
	public void setId(String id){
		this.id = id;
	}


	public String getBoxSize() {
		return boxSize;
	}
	public void setBoxSize(String boxSize) {
		this.boxSize = boxSize;
	}
	/**
	 *方法: 取得
	 *@return:   运输箱种类
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TRANS_BOX")
	public TransBox getTransBox() {
		return transBox;
	}
	public void setTransBox(TransBox transBox) {
		this.transBox = transBox;
	}

	/**
	 *方法: 取得Integer
	 *@return: Integer  数量
	 */
	@Column(name ="NUM", length = 60)
	public Integer getNum(){
		return this.num;
	}
	/**
	 *方法: 设置Integer
	 *@param: Integer  数量
	 */
	public void setNum(Integer num){
		this.num = num;
	}
	/**
	 *方法: 取得GoodsMaterialsSend
	 *@return: GoodsMaterialsSend  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GOODS_MATERIALS_SEND")
	public GoodsMaterialsSend getGoodsMaterialsSend(){
		return this.goodsMaterialsSend;
	}
	/**
	 *方法: 设置GoodsMaterialsSend
	 *@param: GoodsMaterialsSend  相关主表
	 */
	public void setGoodsMaterialsSend(GoodsMaterialsSend goodsMaterialsSend){
		this.goodsMaterialsSend = goodsMaterialsSend;
	}
}