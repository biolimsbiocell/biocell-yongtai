package com.biolims.goods.mate.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
/**   
 * @Title: Model
 * @Description: 物料准备明细
 * @author lims-platform
 * @date 2015-11-03 16:17:41
 * @version V1.0   
 *
 */
@Entity
@Table(name = "GOODS_MATERIALS_READY_ITEM")
@SuppressWarnings("serial")
public class GoodsMaterialsReadyItem extends EntityDao<GoodsMaterialsReadyItem> implements java.io.Serializable {
	/**编码*/
	private String id;
	/**发货数量*/
	private Integer sendNum;
	
	public Integer getSendNum() {
		return sendNum;
	}
	public void setSendNum(Integer sendNum) {
		this.sendNum = sendNum;
	}
	//=======================
	/**
	 * 备注
	 */
	private String note;
	
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	//========================
	//状态
	private String state;
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	//private GoodsMaterialsApply goodsMaterialsApply;
	
	private GoodsMaterialsApplyItem goodsMaterialsApplyItem;
	/**相关主表*/
	private GoodsMaterialsReady goodsMaterialsReady;
//	/**物料*/
//	private DicType materials;
//	private String materialsId;
//	private String materialsName;
//	
//	public String getMaterialsId() {
//		return materialsId;
//	}
//	public void setMaterialsId(String materialsId) {
//		this.materialsId = materialsId;
//	}
//	public String getMaterialsName() {
//		return materialsName;
//	}
//	public void setMaterialsName(String materialsName) {
//		this.materialsName = materialsName;
//	}
//	/**申请数量*/
//	private String applyNum;
//	/**单位*/
//	private String applyUnit;
//	/**在途数量*/
//	private String wayNum;
//	/**未使用数量*/
//	private String unuseNum;
//	/**过期数量*/
//	private String usedNum;
	//状态

	/**
	 *方法: 取得String
	 *@return: String  编码
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 60)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编码
	 */
	public void setId(String id){
		this.id = id;
	}
	
//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "MATERIALS")
//	public DicType getMaterials() {
//		return materials;
//	}
//	public void setMaterials(DicType materials) {
//		this.materials = materials;
//	}
//	/**
//	 *方法: 取得String
//	 *@return: String  申请数量
//	 */
//	@Column(name ="APPLY_NUM", length = 60)
//	public String getApplyNum(){
//		return this.applyNum;
//	}
//	/**
//	 *方法: 设置String
//	 *@param: String  申请数量
//	 */
//	public void setApplyNum(String applyNum){
//		this.applyNum = applyNum;
//	}
//	/**
//	 *方法: 取得String
//	 *@return: String  单位
//	 */
//	@Column(name ="APPLY_UNIT", length = 60)
//	public String getApplyUnit(){
//		return this.applyUnit;
//	}
//	/**
//	 *方法: 设置String
//	 *@param: String  单位
//	 */
//	public void setApplyUnit(String applyUnit){
//		this.applyUnit = applyUnit;
//	}
	/**
	 *方法: 取得String
	 *@return: String  发货数量
	 */
//	/**
//	 *方法: 取得String
//	 *@return: String  在途数量
//	 */
//	@Column(name ="WAY_NUM", length = 60)
//	public String getWayNum(){
//		return this.wayNum;
//	}
//	/**
//	 *方法: 设置String
//	 *@param: String  在途数量
//	 */
//	public void setWayNum(String wayNum){
//		this.wayNum = wayNum;
//	}
//	/**
//	 *方法: 取得String
//	 *@return: String  未使用数量
//	 */
//	@Column(name ="UNUSE_NUM", length = 60)
//	public String getUnuseNum(){
//		return this.unuseNum;
//	}
//	/**
//	 *方法: 设置String
//	 *@param: String  未使用数量
//	 */
//	public void setUnuseNum(String unuseNum){
//		this.unuseNum = unuseNum;
//	}
//	/**
//	 *方法: 取得String
//	 *@return: String  过期数量
//	 */
//	@Column(name ="USED_NUM", length = 60)
//	public String getUsedNum(){
//		return this.usedNum;
//	}
//	/**
//	 *方法: 设置String
//	 *@param: String  过期数量
//	 */
//	public void setUsedNum(String usedNum){
//		this.usedNum = usedNum;
//	}
	/**
	 *方法: 取得GoodsMaterialsReady
	 *@return: GoodsMaterialsReady  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GOODS_MATERIALS_READY")
	public GoodsMaterialsReady getGoodsMaterialsReady(){
		return this.goodsMaterialsReady;
	}
	/**
	 *方法: 设置GoodsMaterialsReady
	 *@param: GoodsMaterialsReady  相关主表
	 */
	public void setGoodsMaterialsReady(GoodsMaterialsReady goodsMaterialsReady){
		this.goodsMaterialsReady = goodsMaterialsReady;
	}
//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "GOODS_MATERIALS_APPLY")
//	public GoodsMaterialsApply getGoodsMaterialsApply() {
//		return goodsMaterialsApply;
//	}
//	public void setGoodsMaterialsApply(GoodsMaterialsApply goodsMaterialsApply) {
//		this.goodsMaterialsApply = goodsMaterialsApply;
//	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GOODS_MATERIALS_APPLY_ITEM")
	public GoodsMaterialsApplyItem getGoodsMaterialsApplyItem() {
		return goodsMaterialsApplyItem;
	}
	public void setGoodsMaterialsApplyItem(
			GoodsMaterialsApplyItem goodsMaterialsApplyItem) {
		this.goodsMaterialsApplyItem = goodsMaterialsApplyItem;
	}
	
}