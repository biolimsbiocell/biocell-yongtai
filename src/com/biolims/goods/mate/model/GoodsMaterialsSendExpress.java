package com.biolims.goods.mate.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
/**   
 * @Title: Model
 * @Description: 物料发放快递明细
 * @author lims-platform
 * @date 2015-11-03 16:20:54
 * @version V1.0   
 *
 */
@Entity
@Table(name = "GOODS_MATERIALS_SEND_EXPRESS")
@SuppressWarnings("serial")
public class GoodsMaterialsSendExpress extends EntityDao<GoodsMaterialsSendExpress> implements java.io.Serializable {
	/**编码*/
	private String id;
	/**快递单号*/
	private String code;
	/**联系人*/
	private String linkUser;
	/**联系电话*/
	private String phone;
	/**取件人*/
	private String receiveUser;
	/**取件日期*/
	private Date receiveDate;
	/**预计到达日期*/
	private Date planDate;
	/**实际到达日期*/
	private Date realDate;
	/**备注*/
	private String note;
	/**快递接口*/
	private String port;
	/**快递地址*/
	private String address;
	/**快递公司*/
	private String company;
	/**相关主表*/
	private GoodsMaterialsSend goodsMaterialsSend;
	/**
	 *方法: 取得String
	 *@return: String  编码
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 60)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编码
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  快递单号
	 */
	@Column(name ="CODE", length = 60)
	public String getCode(){
		return this.code;
	}
	/**
	 *方法: 设置String
	 *@param: String  快递单号
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 *方法: 取得GoodsMaterialsSend
	 *@return: GoodsMaterialsSend  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GOODS_MATERIALS_SEND")
	public GoodsMaterialsSend getGoodsMaterialsSend(){
		return this.goodsMaterialsSend;
	}
	/**
	 *方法: 设置GoodsMaterialsSend
	 *@param: GoodsMaterialsSend  相关主表
	 */
	public void setGoodsMaterialsSend(GoodsMaterialsSend goodsMaterialsSend){
		this.goodsMaterialsSend = goodsMaterialsSend;
	}
	public String getLinkUser() {
		return linkUser;
	}
	public void setLinkUser(String linkUser) {
		this.linkUser = linkUser;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getReceiveUser() {
		return receiveUser;
	}
	public void setReceiveUser(String receiveUser) {
		this.receiveUser = receiveUser;
	}
	public Date getReceiveDate() {
		return receiveDate;
	}
	public void setReceiveDate(Date receiveDate) {
		this.receiveDate = receiveDate;
	}
	public Date getPlanDate() {
		return planDate;
	}
	public void setPlanDate(Date planDate) {
		this.planDate = planDate;
	}
	public Date getRealDate() {
		return realDate;
	}
	public void setRealDate(Date realDate) {
		this.realDate = realDate;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	
//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "COMPANY")
//	public DicType getCompany() {
//		return company;
//	}
//	public void setCompany(DicType company) {
//		this.company = company;
//	}
	
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	
}