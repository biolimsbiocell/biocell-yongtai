package com.biolims.goods.mate.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 快递寄送明细
 * @author lims-platform
 * @date 2015-11-03 16:18:01
 * @version V1.0   
 *
 */
@Entity
@Table(name = "GOODS_MATERIALS_POST_ITEM")
@SuppressWarnings("serial")
public class GoodsMaterialsPostItem extends EntityDao<GoodsMaterialsPostItem> implements java.io.Serializable {
	/**编码*/
	private String id;
	/**快递单*/
	private String expressOrder;
	/**核对*/
	private String checked;
	
	/**收件人*/
	private String receiver;
	/**签收日期*/
	private Date dateOfLodgment;
	
	public String getChecked() {
		return checked;
	}
	public void setChecked(String checked) {
		this.checked = checked;
	}
	/**收货地址*/
	private String address;
	/**快递公司*/
	private String company;
	/**预计到达日期*/
	private Date expectDate;
	/**实际到达日期*/
	private Date realDate;
	/**轨迹*/
	private String way;
	/**是否提交*/
	private String submit;
	/**备注*/
	private String note;
	/**相关主表*/
	private GoodsMaterialsSend goodsMaterialsSend;
	/**相关主表*/
	private GoodsMaterialsSendExpress goodsMaterialsSendExpress;
	/**
	 *方法: 取得String
	 *@return: String  编码
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 60)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编码
	 */
	public void setId(String id){
		this.id = id;
	}
	
	public String getExpressOrder() {
		return expressOrder;
	}
	public void setExpressOrder(String expressOrder) {
		this.expressOrder = expressOrder;
	}

	/**
	 *方法: 取得String
	 *@return: String  收货地址
	 */
	@Column(name ="ADDRESS", length = 60)
	public String getAddress(){
		return this.address;
	}
	/**
	 *方法: 设置String
	 *@param: String  收货地址
	 */
	public void setAddress(String address){
		this.address = address;
	}
	/**
	 *方法: 取得String
	 *@return: String  快递公司
	 */
	@Column(name ="COMPANY", length = 60)
	public String getCompany(){
		return this.company;
	}
	/**
	 *方法: 设置String
	 *@param: String  快递公司
	 */
	public void setCompany(String company){
		this.company = company;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  预计到达日期
	 */
	@Column(name ="EXPECT_DATE", length = 255)
	public Date getExpectDate(){
		return this.expectDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  预计到达日期
	 */
	public void setExpectDate(Date expectDate){
		this.expectDate = expectDate;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  实际到达日期
	 */
	@Column(name ="REAL_DATE", length = 255)
	public Date getRealDate(){
		return this.realDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  实际到达日期
	 */
	public void setRealDate(Date realDate){
		this.realDate = realDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  轨迹
	 */
	@Column(name ="WAY", length = 60)
	public String getWay(){
		return this.way;
	}
	/**
	 *方法: 设置String
	 *@param: String  轨迹
	 */
	public void setWay(String way){
		this.way = way;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 60)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得GoodsMaterialsSend
	 *@return: GoodsMaterialsSend  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GOODS_MATERIALS_SEND")
	public GoodsMaterialsSend getGoodsMaterialsSend(){
		return this.goodsMaterialsSend;
	}
	/**
	 *方法: 设置GoodsMaterialsSend
	 *@param: GoodsMaterialsSend  相关主表
	 */
	public void setGoodsMaterialsSend(GoodsMaterialsSend goodsMaterialsSend){
		this.goodsMaterialsSend = goodsMaterialsSend;
	}
	
	/**
	 * 方法：获取Receiver
	 * @return String 收件人
	 */
	public String getReceiver() {
		return receiver;
	}
	
	/**
	 * 方法：设置Receiver
	 * @param receiver String 收件人
	 */
	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}
	
	/**
	 * 方法：获取DateOfLodgment
	 * @return Date 签收日期
	 */
	public Date getDateOfLodgment() {
		return dateOfLodgment;
	}
	
	/**
	 * 方法：设置DateOfLodgment
	 * @param dateOfLodgment Date 签收日期
	 */
	public void setDateOfLodgment(Date dateOfLodgment) {
		this.dateOfLodgment = dateOfLodgment;
	}
	public String getSubmit() {
		return submit;
	}
	public void setSubmit(String submit) {
		this.submit = submit;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GOODS_MATERIALS_SEND_EXPRESS")
	public GoodsMaterialsSendExpress getGoodsMaterialsSendExpress() {
		return goodsMaterialsSendExpress;
	}
	public void setGoodsMaterialsSendExpress(
			GoodsMaterialsSendExpress goodsMaterialsSendExpress) {
		this.goodsMaterialsSendExpress = goodsMaterialsSendExpress;
	}
	
}