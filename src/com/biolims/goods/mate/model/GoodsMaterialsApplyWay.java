package com.biolims.goods.mate.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 物料申请快递跟踪
 * @author lims-platform
 * @date 2015-11-03 16:20:26
 * @version V1.0   
 *
 */
@Entity
@Table(name = "GOODS_MATERIALS_APPLY_WAY")
@SuppressWarnings("serial")
public class GoodsMaterialsApplyWay extends EntityDao<GoodsMaterialsApplyWay> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**快递单*/
//	private String expressOrder;
//	/**快递公司*/
//	private String expressCompany;
	/**快递状态*/
	private String state;
	/**快递轨迹*/
	private String way;
	/**签收确认*/
	private String isReceive;
	/**预计到达日期*/
	private Date predictDate;
	/**相关主表*/
	private GoodsMaterialsApply goodsMaterialsApply;
	
	private GoodsMaterialsWay goodsMaterialsSendExpress;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GOODS_MATERIALS_SEND_EXPRESS")
	public GoodsMaterialsWay getGoodsMaterialsSendExpress() {
		return goodsMaterialsSendExpress;
	}
	public void setGoodsMaterialsSendExpress(
			GoodsMaterialsWay goodsMaterialsSendExpress) {
		this.goodsMaterialsSendExpress = goodsMaterialsSendExpress;
	}
	
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 60)
	public String getId(){
		return this.id;
	}

	/**
	 *方法: 设置String
	 *@param: String  编码
	 */
	public void setId(String id){
		this.id = id;
	}
	//快递单号
//	public String getExpressOrder() {
//		return expressOrder;
//	}
//	public void setExpressOrder(String expressOrder) {
//		this.expressOrder = expressOrder;
//	}
//	
//	
//	public String getExpressCompany() {
//		return expressCompany;
//	}
//	public void setExpressCompany(String expressCompany) {
//		this.expressCompany = expressCompany;
//	}
	/**
	 *方法: 取得String
	 *@return: String  快递状态
	 */
	@Column(name ="STATE", length = 60)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  快递状态
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  快递轨迹
	 */
	@Column(name ="WAY", length = 60)
	public String getWay(){
		return this.way;
	}
	/**
	 *方法: 设置String
	 *@param: String  快递轨迹
	 */
	public void setWay(String way){
		this.way = way;
	}
	/**
	 *方法: 取得String
	 *@return: String  签收确认
	 */
	@Column(name ="IS_RECEIVE", length = 60)
	public String getIsReceive(){
		return this.isReceive;
	}
	/**
	 *方法: 设置String
	 *@param: String  签收确认
	 */
	public void setIsReceive(String isReceive){
		this.isReceive = isReceive;
	}
	/**
	 *方法: 取得GoodsMaterialsApply
	 *@return: GoodsMaterialsApply  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GOODS_MATERIALS_APPLY")
	public GoodsMaterialsApply getGoodsMaterialsApply(){
		return this.goodsMaterialsApply;
	}
	/**
	 *方法: 设置GoodsMaterialsApply
	 *@param: GoodsMaterialsApply  相关主表
	 */
	public void setGoodsMaterialsApply(GoodsMaterialsApply goodsMaterialsApply){
		this.goodsMaterialsApply = goodsMaterialsApply;
	}
	public Date getPredictDate() {
		return predictDate;
	}
	public void setPredictDate(Date predictDate) {
		this.predictDate = predictDate;
	}
	
}