package com.biolims.goods.mate.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.dic.model.DicCurrencyType;
import com.biolims.common.model.user.User;
/**   
 * @Title: Model
 * @Description: 快递寄送
 * @author lims-platform
 * @date 2015-11-03 16:18:13
 * @version V1.0   
 *
 */
@Entity
@Table(name = "GOODS_MATERIALS_POST")
@SuppressWarnings("serial")
public class GoodsMaterialsPost extends EntityDao<GoodsMaterialsPost> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
	/**寄送类型*/
	private DicType type;
	/**物料发放*/
	private GoodsMaterialsSend goodsMaterialsSend;
	/**创建人*/
	private User createUser;
	/**创建日期*/
	private Date createDate;
	/**状态*/
	private String state;
	/**状态*/
	private String stateName;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@Column(name ="ID", length = 36)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 120)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得DicType
	 *@return: DicType  寄送类型
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "TYPE")
	public DicType getType(){
		return this.type;
	}
	/**
	 *方法: 设置DicType
	 *@param: DicType  寄送类型
	 */
	public void setType(DicType type){
		this.type = type;
	}
	/**
	 *方法: 取得GoodsMaterialsSend
	 *@return: GoodsMaterialsSend  物料发放
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GOODS_MATERIALS_SEND")
	public GoodsMaterialsSend getGoodsMaterialsSend(){
		return this.goodsMaterialsSend;
	}
	/**
	 *方法: 设置GoodsMaterialsSend
	 *@param: GoodsMaterialsSend  物料发放
	 */
	public void setGoodsMaterialsSend(GoodsMaterialsSend goodsMaterialsSend){
		this.goodsMaterialsSend = goodsMaterialsSend;
	}
	/**
	 *方法: 取得User
	 *@return: User  创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser(){
		return this.createUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  创建人
	 */
	public void setCreateUser(User createUser){
		this.createUser = createUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  创建日期
	 */
	@Column(name ="CREATE_DATE", length = 255)
	public Date getCreateDate(){
		return this.createDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  创建日期
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE", length = 60)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE_NAME", length = 60)
	public String getStateName(){
		return this.stateName;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setStateName(String stateName){
		this.stateName = stateName;
	}
}