package com.biolims.goods.mate.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.dic.model.DicCurrencyType;
/**   
 * @Title: Model
 * @Description: 物料发放明细
 * @author lims-platform
 * @date 2015-11-03 16:20:51
 * @version V1.0   
 *
 */
@Entity
@Table(name = "GOODS_MATERIALS_SEND_ITEM")
@SuppressWarnings("serial")
public class GoodsMaterialsSendItem extends EntityDao<GoodsMaterialsSendItem> implements java.io.Serializable {
	/**编码*/
	private String id;
	/**物料准备*/
	private GoodsMaterialsApply goodsMaterialsApply;
	private GoodsMaterialsApplyItem goodsMaterialsApplyItem;
	/**相关主表*/
	private GoodsMaterialsSend goodsMaterialsSend;
	/**有效期*/
	private Date goodsMaterialsValidity;
	
	private String state;
	/**
	 *方法: 取得String
	 *@return: String  编码
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 60)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编码
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 * 方法：获取Date
	 * @return Date 有效期
	 */
	@Column(name="GOODS_MATERIALS_VALIDITY", length = 255)
	public Date getGoodsMaterialsValidity() {
		return goodsMaterialsValidity;
	}
	
	/**
	 * 方法：设置Date
	 * @param goodsMaterialsValidity 有效期
	 */
	public void setGoodsMaterialsValidity(Date goodsMaterialsValidity) {
		this.goodsMaterialsValidity = goodsMaterialsValidity;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GOODS_MATERIALS_APPLY_ITEM")
	public void setGoodsMaterialsApplyItem(
			GoodsMaterialsApplyItem goodsMaterialsApplyItem) {
		this.goodsMaterialsApplyItem = goodsMaterialsApplyItem;
	}
	public void setGoodsMaterialsApply(GoodsMaterialsApply goodsMaterialsApply) {
		this.goodsMaterialsApply = goodsMaterialsApply;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GOODS_MATERIALS_APPLY")
	public GoodsMaterialsApply getGoodsMaterialsApply() {
		return goodsMaterialsApply;
	}
	public GoodsMaterialsApplyItem getGoodsMaterialsApplyItem() {
		return goodsMaterialsApplyItem;
	}
	
	/**
	 *方法: 取得GoodsMaterialsSend
	 *@return: GoodsMaterialsSend  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GOODS_MATERIALS_SEND")
	public GoodsMaterialsSend getGoodsMaterialsSend(){
		return this.goodsMaterialsSend;
	}
	/**
	 *方法: 设置GoodsMaterialsSend
	 *@param: GoodsMaterialsSend  相关主表
	 */
	public void setGoodsMaterialsSend(GoodsMaterialsSend goodsMaterialsSend){
		this.goodsMaterialsSend = goodsMaterialsSend;
	}
	
	@Column(name="STATE",length=10)
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
}