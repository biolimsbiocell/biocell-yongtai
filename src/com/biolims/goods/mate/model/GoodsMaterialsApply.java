package com.biolims.goods.mate.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.system.dept.model.ApplyDept;
import com.biolims.system.express.model.ExpressCompany;
import com.biolims.system.organize.model.ApplyOrganize;
import com.biolims.system.organize.model.ApplyOrganizeAddress;
/**   
 * @Title: Model
 * @Description: 物料申请
 * @author lims-platform
 * @date 2015-11-03 16:20:43
 * @version V1.0   
 *
 */
@Entity
@Table(name = "GOODS_MATERIALS_APPLY")
@SuppressWarnings("serial")
public class GoodsMaterialsApply extends EntityDao<GoodsMaterialsApply> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**联系方式*/
	private String name;
	/**申请组织*/
	private ApplyOrganize applyOrganize;
	/**单位名称*/
	private String applyDept;
	/**收货地址*/
	private ApplyOrganizeAddress address;
	/**申请类型*/
	private String type;
	/**收件人*/
	private String receiveUser;
	/**收件人电话*/
	private String phone;
	/**创建人*/
	private User createUser;
	/**创建日期*/
	private Date createDate;
	/**优先级*/
	private String priority;
	/**工作流状态*/
	private String state;
	/**工作流状态*/
	private String stateName;
	/**快递公司*/
	private ExpressCompany company;
	/**完成日期*/
	private Date confirmDate;
	/**发货日期*/
	private Date sendDate;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@Column(name ="ID", length = 36)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	/**
	 *方法: 取得ApplyOrganize
	 *@return: ApplyOrganize  申请组织
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "APPLY_ORGANIZE")
	public ApplyOrganize getApplyOrganize(){
		return this.applyOrganize;
	}
	/**
	 *方法: 设置ApplyOrganize
	 *@param: ApplyOrganize  申请组织
	 */
	public void setApplyOrganize(ApplyOrganize applyOrganize){
		this.applyOrganize = applyOrganize;
	}
//	/**
//	 *方法: 取得ApplyDept
//	 *@return: ApplyDept  单位名称
//	 */
//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "APPLY_DEPT")
//	public ApplyDept getApplyDept(){
//		return this.applyDept;
//	}
//	/**
//	 *方法: 设置ApplyDept
//	 *@param: ApplyDept  单位名称
//	 */
//	public void setApplyDept(ApplyDept applyDept){
//		this.applyDept = applyDept;
//	}

	/**
	 *方法: 取得User
	 *@return: User  收件人
	 */
//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "RECEIVE_USER")
//	public User getReceiveUser(){
//		return this.receiveUser;
//	}
//	/**
//	 *方法: 设置User
//	 *@param: User  收件人
//	 */
//	public void setReceiveUser(User receiveUser){
//		this.receiveUser = receiveUser;
//	}
	/**
	 *方法: 取得String
	 *@return: String  收件人电话
	 */
	@Column(name ="PHONE", length = 60)
	public String getPhone(){
		return this.phone;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "APPLY_ORGANIZE_ADDRESS")
	public ApplyOrganizeAddress getAddress() {
		return address;
	}
	public void setAddress(ApplyOrganizeAddress address) {
		this.address = address;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getReceiveUser() {
		return receiveUser;
	}
	public void setReceiveUser(String receiveUser) {
		this.receiveUser = receiveUser;
	}
	/**
	 *方法: 设置String
	 *@param: String  收件人电话
	 */
	public void setPhone(String phone){
		this.phone = phone;
	}
	/**
	 *方法: 取得User
	 *@return: User  创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser(){
		return this.createUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  创建人
	 */
	public void setCreateUser(User createUser){
		this.createUser = createUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  创建日期
	 */
	@Column(name ="CREATE_DATE", length = 255)
	public Date getCreateDate(){
		return this.createDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  创建日期
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  工作流状态
	 */
	@Column(name ="STATE", length = 120)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  工作流状态
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  工作流状态
	 */
	@Column(name ="STATE_NAME", length = 120)
	public String getStateName(){
		return this.stateName;
	}
	/**
	 *方法: 设置String
	 *@param: String  工作流状态
	 */
	public void setStateName(String stateName){
		this.stateName = stateName;
	}
	
	/**
	 * 方法：获取String
	 * @return String 优先级
	 */
	@Column(name="PRIORITY",length = 120)
	public String getPriority() {
		return priority;
	}
	
	/**
	 * 方法：设置String 
	 * @param priority String 优先级
	 */
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getApplyDept() {
		return applyDept;
	}
	public void setApplyDept(String applyDept) {
		this.applyDept = applyDept;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "COMPANY")
	public ExpressCompany getCompany(){
		return this.company;
	}
	/**
	 *方法: 设置DicType
	 *@param: DicType  快递公司
	 */
	public void setCompany(ExpressCompany company){
		this.company = company;
	}
	public Date getConfirmDate() {
		return confirmDate;
	}
	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}
	public Date getSendDate() {
		return sendDate;
	}
	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}
	
}