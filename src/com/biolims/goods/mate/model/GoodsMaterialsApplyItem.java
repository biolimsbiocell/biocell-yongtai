package com.biolims.goods.mate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.goods.pack.model.GoodsMaterialsDetails;
import com.biolims.goods.pack.model.GoodsMaterialsPack;
/**   
 * @Title: Model
 * @Description: 物料申请明细
 * @author lims-platform
 * @date 2015-11-03 16:20:29
 * @version V1.0   
 *
 */
@Entity
@Table(name = "GOODS_MATERIALS_APPLY_ITEM")
@SuppressWarnings("serial")
public class GoodsMaterialsApplyItem extends EntityDao<GoodsMaterialsApplyItem> implements java.io.Serializable {
	/**编码*/
	private String id;
	/**物料包*/
	private DicType materials;
	///
	private String materialsId;
	/**物料描述*/
	private String materialsName;
	private String unitName;
	
	/**关联物料包*/
	private GoodsMaterialsPack goodsMaterialsPack;
	/**关联物料包子表*/
	private GoodsMaterialsDetails goodsMaterialsDetails;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GOODS_MATERIALS_PACK")
	public GoodsMaterialsPack getGoodsMaterialsPack() {
		return goodsMaterialsPack;
	}
	public void setGoodsMaterialsPack(GoodsMaterialsPack goodsMaterialsPack) {
		this.goodsMaterialsPack = goodsMaterialsPack;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GOODS_MATERIALS_DETAILS")
	public GoodsMaterialsDetails getGoodsMaterialsDetails() {
		return goodsMaterialsDetails;
	}
	public void setGoodsMaterialsDetails(GoodsMaterialsDetails goodsMaterialsDetails) {
		this.goodsMaterialsDetails = goodsMaterialsDetails;
	}
	public String getMaterialsId() {
		return materialsId;
	}
	public void setMaterialsId(String materialsId) {
		this.materialsId = materialsId;
	}
	public String getMaterialsName() {
		return materialsName;
	}
	public void setMaterialsName(String materialsName) {
		this.materialsName = materialsName;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	///
	/**申请数量*/
	private Integer applyNum;
	/**剩余准备数量*/
	private Integer lackNum;
	
	public Integer getLackNum() {
		return lackNum;
	}
	public void setLackNum(Integer lackNum) {
		this.lackNum = lackNum;
	}
	/**单位*/
	private DicType unit;
	/**在途数量*/
	private Integer wayNum;
	/**未使用数量*/
	private Integer unuseNum;
	/**过期数量*/
	private Integer usedNum;
	/**状态*/
	private String state;
	/**相关主表*/
	private GoodsMaterialsApply goodsMaterialsApply;
	/**
	 *方法: 取得String
	 *@return: String  编码
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 60)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编码
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
//	 *方法: 取得String
//	 *@return: String  物料包编号
//	 */
//	@Column(name ="CODE", length = 60)
//	public String getCode(){
//		return this.code;
//	}
//	/**
//	 *方法: 设置String
//	 *@param: String  物料包编号
//	 */
//	public void setCode(String code){
//		this.code = code;
//	}
//	/**
//	 *方法: 取得String
//	 *@return: String  物料描述
//	 */
//	@Column(name ="NAME", length = 120)
//	public String getName(){
//		return this.name;
//	}
//	/**
//	 *方法: 设置String
//	 *@param: String  物料描述
//	 */
//	public void setName(String name){
//		this.name = name;
//	}
	/**
	 *方法: 取得Integer
	 *@return: Integer  申请数量
	 */
	@Column(name ="APPLY_NUM", length = 36)
	public Integer getApplyNum(){
		return this.applyNum;
	}
	/**
	 *方法: 设置Integer
	 *@param: Integer  申请数量
	 */
	public void setApplyNum(Integer applyNum){
		this.applyNum = applyNum;
	}
	/**
	 *方法: 取得DicType
	 *@return: DicType  单位
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "UNIT")
	public DicType getUnit(){
		return this.unit;
	}
	/**
	 *方法: 设置DicType
	 *@param: DicType  单位
	 */
	public void setUnit(DicType unit){
		this.unit = unit;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "Materials")
	public DicType getMaterials() {
		return materials;
	}
	public void setMaterials(DicType materials) {
		this.materials = materials;
	}
	/**
	 *方法: 取得Integer
	 *@return: Integer  在途数量
	 */
	@Column(name ="WAY_NUM", length = 36)
	public Integer getWayNum(){
		return this.wayNum;
	}
	/**
	 *方法: 设置Integer
	 *@param: Integer  在途数量
	 */
	public void setWayNum(Integer wayNum){
		this.wayNum = wayNum;
	}
	/**
	 *方法: 取得Integer
	 *@return: Integer  未使用数量
	 */
	@Column(name ="UNUSE_NUM", length = 36)
	public Integer getUnuseNum(){
		return this.unuseNum;
	}
	/**
	 *方法: 设置Integer
	 *@param: Integer  未使用数量
	 */
	public void setUnuseNum(Integer unuseNum){
		this.unuseNum = unuseNum;
	}
	/**
	 *方法: 取得Integer
	 *@return: Integer  过期数量
	 */
	@Column(name ="USED_NUM", length = 36)
	public Integer getUsedNum(){
		return this.usedNum;
	}
	/**
	 *方法: 设置Integer
	 *@param: Integer  过期数量
	 */
	public void setUsedNum(Integer usedNum){
		this.usedNum = usedNum;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATE", length = 20)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得GoodsMaterialsApply
	 *@return: GoodsMaterialsApply  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GOODS_MATERIALS_APPLY")
	public GoodsMaterialsApply getGoodsMaterialsApply(){
		return this.goodsMaterialsApply;
	}
	/**
	 *方法: 设置GoodsMaterialsApply
	 *@param: GoodsMaterialsApply  相关主表
	 */
	public void setGoodsMaterialsApply(GoodsMaterialsApply goodsMaterialsApply){
		this.goodsMaterialsApply = goodsMaterialsApply;
	}
}