package com.biolims.goods.mate.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 物料包明细
 * @author lims-platform
 * @date 2015-11-03 16:17:37
 * @version V1.0   
 *
 */
@Entity
@Table(name = "GOODS_MATERIALS_READY_DETAILS")
@SuppressWarnings("serial")
public class GoodsMaterialsReadyDetails extends EntityDao<GoodsMaterialsReadyDetails> implements java.io.Serializable {
	/**编码*/
	private String id;
	/**物料包编码*/
	private String code;
	//
	private String packCode;
	//状态
	private String state;
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * 有效期
	 * @return
	 */
	private Date validity;
	
	public Date getValidity() {
		return validity;
	}
	public void setValidity(Date validity) {
		this.validity = validity;
	}
	public String getPackCode() {
		return packCode;
	}
	public void setPackCode(String packCode) {
		this.packCode = packCode;
	}
	/**打印数量*/
	private Integer num;
	/**已使用状态*/
	private String useState;
	/**过期的状态*/
	private String outState;
	/**相关主表*/
	private GoodsMaterialsSend goodsMaterialsSend;
	/**
	 *方法: 取得String
	 *@return: String  编码
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 60)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编码
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  物料包编码
	 */
	@Column(name ="CODE", length = 60)
	public String getCode(){
		return this.code;
	}
	/**
	 *方法: 设置String
	 *@param: String  物料包编码
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 *方法: 取得String
	 *@return: String  打印数量
	 */

	/**
	 *方法: 取得GoodsMaterialsReady
	 *@return: GoodsMaterialsReady  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GOODS_MATERIALS_SEND")
	public GoodsMaterialsSend getGoodsMaterialsSend() {
		return goodsMaterialsSend;
	}
	/**
	 *方法: 设置GoodsMaterialsReady
	 *@param: GoodsMaterialsReady  相关主表
	 */

	public void setGoodsMaterialsSend(GoodsMaterialsSend goodsMaterialsSend) {
		this.goodsMaterialsSend = goodsMaterialsSend;
	}
	
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	
	public String getUseState() {
		return useState;
	}
	public void setUseState(String useState) {
		this.useState = useState;
	}
	public String getOutState() {
		return outState;
	}
	public void setOutState(String outState) {
		this.outState = outState;
	}
	
}