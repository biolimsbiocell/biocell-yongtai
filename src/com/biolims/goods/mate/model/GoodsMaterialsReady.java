package com.biolims.goods.mate.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.common.model.user.User;
import com.biolims.common.model.user.User;
/**   
 * @Title: Model
 * @Description: 物料准备
 * @author lims-platform
 * @date 2015-11-03 16:17:55
 * @version V1.0   
 *
 */
@Entity
@Table(name = "GOODS_MATERIALS_READY")
@SuppressWarnings("serial")
public class GoodsMaterialsReady extends EntityDao<GoodsMaterialsReady> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
	/**申请单*/
	private GoodsMaterialsApply goodsMaterialsApply;
//	/**申请组织*/
//	private String applyOrganize;
//	/**收件人*/
//	private String receiveUser;
//	/**收件人电话*/
//	private String phone;
//	/**收货地址*/
//	private String address;
	/**创建人*/
	private User createUser;
	/**创建日期*/
	private Date createDate;
	/**审核人*/
	private User confirmUser;
	/**审核时间*/
	private Date confirmDate;
	/**工作流状态*/
	private String state;
	/**工作流状态*/
	private String stateName;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@Column(name ="ID", length = 36)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 120)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得GoodsMaterialsApply
	 *@return: GoodsMaterialsApply  申请单
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "GOODS_MATERIALS_APPLY")
	public GoodsMaterialsApply getGoodsMaterialsApply(){
		return this.goodsMaterialsApply;
	}
	/**
	 *方法: 设置GoodsMaterialsApply
	 *@param: GoodsMaterialsApply  申请单
	 */
	public void setGoodsMaterialsApply(GoodsMaterialsApply goodsMaterialsApply){
		this.goodsMaterialsApply = goodsMaterialsApply;
	}
//	/**
//	 *方法: 取得String
//	 *@return: String  申请组织
//	 */
//	@Column(name ="APPLY_ORGANIZE", length = 60)
//	public String getApplyOrganize(){
//		return this.applyOrganize;
//	}
//	/**
//	 *方法: 设置String
//	 *@param: String  申请组织
//	 */
//	public void setApplyOrganize(String applyOrganize){
//		this.applyOrganize = applyOrganize;
//	}
//	/**
//	 *方法: 取得String
//	 *@return: String  收件人
//	 */
//	@Column(name ="RECEIVE_USER", length = 60)
//	public String getReceiveUser(){
//		return this.receiveUser;
//	}
//	/**
//	 *方法: 设置String
//	 *@param: String  收件人
//	 */
//	public void setReceiveUser(String receiveUser){
//		this.receiveUser = receiveUser;
//	}
//	/**
//	 *方法: 取得String
//	 *@return: String  收件人电话
//	 */
//	@Column(name ="PHONE", length = 60)
//	public String getPhone(){
//		return this.phone;
//	}
//	/**
//	 *方法: 设置String
//	 *@param: String  收件人电话
//	 */
//	public void setPhone(String phone){
//		this.phone = phone;
//	}
//	/**
//	 *方法: 取得String
//	 *@return: String  收货地址
//	 */
//	@Column(name ="ADDRESS", length = 60)
//	public String getAddress(){
//		return this.address;
//	}
//	/**
//	 *方法: 设置String
//	 *@param: String  收货地址
//	 */
//	public void setAddress(String address){
//		this.address = address;
//	}
	/**
	 *方法: 取得User
	 *@return: User  创建人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser(){
		return this.createUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  创建人
	 */
	public void setCreateUser(User createUser){
		this.createUser = createUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  创建日期
	 */
	@Column(name ="CREATE_DATE", length = 255)
	public Date getCreateDate(){
		return this.createDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  创建日期
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 *方法: 取得User
	 *@return: User  审核人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CONFIRM_USER")
	public User getConfirmUser(){
		return this.confirmUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  审核人
	 */
	public void setConfirmUser(User confirmUser){
		this.confirmUser = confirmUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  审核时间
	 */
	@Column(name ="CONFIRM_DATE", length = 255)
	public Date getConfirmDate(){
		return this.confirmDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  审核时间
	 */
	public void setConfirmDate(Date confirmDate){
		this.confirmDate = confirmDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  工作流状态
	 */
	@Column(name ="STATE", length = 60)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  工作流状态
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  工作流状态
	 */
	@Column(name ="STATE_NAME", length = 60)
	public String getStateName(){
		return this.stateName;
	}
	/**
	 *方法: 设置String
	 *@param: String  工作流状态
	 */
	public void setStateName(String stateName){
		this.stateName = stateName;
	}
}