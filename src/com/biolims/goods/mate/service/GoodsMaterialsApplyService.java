package com.biolims.goods.mate.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.SystemCodeService;
import com.biolims.goods.mate.dao.GoodsMaterialsApplyDao;
import com.biolims.goods.mate.dao.GoodsMaterialsPostDao;
import com.biolims.goods.mate.dao.GoodsMaterialsReadyDao;
import com.biolims.goods.mate.dao.GoodsMaterialsSendDao;
import com.biolims.goods.mate.dao.GoodsMaterialsWayDao;
import com.biolims.goods.mate.model.GoodsMaterialsApply;
import com.biolims.goods.mate.model.GoodsMaterialsApplyItem;
import com.biolims.goods.mate.model.GoodsMaterialsApplyWay;
import com.biolims.goods.mate.model.GoodsMaterialsPost;
import com.biolims.goods.mate.model.GoodsMaterialsPostItem;
import com.biolims.goods.mate.model.GoodsMaterialsReady;
import com.biolims.goods.mate.model.GoodsMaterialsReadyDetails;
import com.biolims.goods.mate.model.GoodsMaterialsReadyItem;
import com.biolims.goods.mate.model.GoodsMaterialsSend;
import com.biolims.goods.mate.model.GoodsMaterialsSendExpress;
import com.biolims.goods.mate.model.GoodsMaterialsWay;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.system.organize.model.ApplyOrganizeAddress;
import com.biolims.util.DateUtil;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class GoodsMaterialsApplyService {
	@Resource
	private GoodsMaterialsApplyDao goodsMaterialsApplyDao;
	@Resource
	private GoodsMaterialsReadyDao goodsMaterialsReadyDao;
	@Resource
	private GoodsMaterialsSendDao goodsMaterialsSendDao;
	@Resource
	private GoodsMaterialsPostDao goodsMaterialsPostDao;
	@Resource
	private GoodsMaterialsWayDao goodsMaterialsWayDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SystemCodeService systemCodeService;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findGoodsMaterialsApplyList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return goodsMaterialsApplyDao.selectGoodsMaterialsApplyList(mapForQuery, startNum, limitNum, dir, sort);
	}
	//根据状态查询主数据
	public Map<String, Object> findGoodsMaterialsApplyListByState(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return goodsMaterialsApplyDao.selectGoodsMaterialsApplyListByState(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(GoodsMaterialsApply i) throws Exception {

		goodsMaterialsApplyDao.saveOrUpdate(i);

	}
	public GoodsMaterialsApply get(String id) {
		GoodsMaterialsApply goodsMaterialsApply = commonDAO.get(GoodsMaterialsApply.class, id);
		return goodsMaterialsApply;
	}
	public Map<String, Object> findGoodsMaterialsApplyWayList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = goodsMaterialsApplyDao.selectGoodsMaterialsApplyWayList(scId, startNum, limitNum, dir, sort);
		List<GoodsMaterialsApplyWay> list = (List<GoodsMaterialsApplyWay>) result.get("list");
		return result;
	}
	public Map<String, Object> findGoodsMaterialsApplyItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = goodsMaterialsApplyDao.selectGoodsMaterialsApplyItemList(scId, startNum, limitNum, dir, sort);
		List<GoodsMaterialsApplyItem> list = (List<GoodsMaterialsApplyItem>) result.get("list");
		return result;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveGoodsMaterialsApplyWay(GoodsMaterialsApply sc, String itemDataJson) throws Exception {
		List<GoodsMaterialsApplyWay> saveItems = new ArrayList<GoodsMaterialsApplyWay>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			GoodsMaterialsApplyWay scp = new GoodsMaterialsApplyWay();
			// 将map信息读入实体类
			scp = (GoodsMaterialsApplyWay) goodsMaterialsApplyDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals("")){
				scp.setId(null);
			}
			scp.setGoodsMaterialsApply(sc);
			
			saveItems.add(scp);
		}
		goodsMaterialsApplyDao.saveOrUpdateAll(saveItems);
	}
	//保存快递信息
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveToWay(String itemDataJson) throws Exception {
		List<GoodsMaterialsApplyWay> saveItems = new ArrayList<GoodsMaterialsApplyWay>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		boolean flag=true;
		List<GoodsMaterialsApplyItem> list2=new ArrayList<GoodsMaterialsApplyItem>();
		for (Map<String, Object> map : list) {
			GoodsMaterialsApplyWay sbi = new GoodsMaterialsApplyWay();
			sbi = (GoodsMaterialsApplyWay) goodsMaterialsApplyDao.Map2Bean(map, sbi);
			/*if (sbi.getId() != null && sbi.getId().equals(""))
				sbi.setId(null);*/
			GoodsMaterialsWay gmw = goodsMaterialsApplyDao.get(GoodsMaterialsWay.class,sbi.getGoodsMaterialsSendExpress().getId());
			gmw.setRealDate(new Date());
			saveItems.add(sbi);
			//申请主数据ID
			String code=sbi.getGoodsMaterialsApply().getId();
			list2=goodsMaterialsApplyDao.showGoodsMaterialsApplyItemList(code);
			if(sbi.getIsReceive().equals("0")){
				flag=false;
			}else if(sbi.getIsReceive()==null){
				flag=false;
			}
		}
		goodsMaterialsApplyDao.saveOrUpdateAll(saveItems);

	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delGoodsMaterialsApplyWay(String[] ids) throws Exception {
		for (String id : ids) {
			GoodsMaterialsApplyWay scp =  goodsMaterialsApplyDao.get(GoodsMaterialsApplyWay.class, id);
			 goodsMaterialsApplyDao.delete(scp);
		}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveGoodsMaterialsApplyItem(GoodsMaterialsApply sc, String itemDataJson) throws Exception {
		List<GoodsMaterialsApplyItem> saveItems = new ArrayList<GoodsMaterialsApplyItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			GoodsMaterialsApplyItem scp = new GoodsMaterialsApplyItem();
			// 将map信息读入实体类
			scp = (GoodsMaterialsApplyItem) goodsMaterialsApplyDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setGoodsMaterialsApply(sc);
			scp.setState("申请中");
			saveItems.add(scp);
			
			
		}
		goodsMaterialsApplyDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delGoodsMaterialsApplyItem(String[] ids) throws Exception {
		for (String id : ids) {
			GoodsMaterialsApplyItem scp =  goodsMaterialsApplyDao.get(GoodsMaterialsApplyItem.class, id);
			 goodsMaterialsApplyDao.delete(scp);
		}
	}
	//根据申请单加载子表
	public List<Map<String, String>> showGoodsMaterialsApplyItemList(String code) throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = goodsMaterialsApplyDao.setGoodsMaterialsApplyItemList(code);
		List<GoodsMaterialsApplyItem> list = (List<GoodsMaterialsApplyItem>) result.get("list");

		if (list != null && list.size() > 0) {
			for (GoodsMaterialsApplyItem srai : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("mid", srai.getGoodsMaterialsPack().getId());
				map.put("mname",srai.getGoodsMaterialsPack().getName());
				if(srai.getGoodsMaterialsPack().getNum()!=null){
					map.put("mnum",srai.getGoodsMaterialsPack().getNum().toString());
				}else{
					map.put("mnum","");
				}
				map.put("aid", srai.getId());
				map.put("aname",srai.getGoodsMaterialsApply().getName());
				if(srai.getApplyNum()!=null){
					map.put("applyNum", srai.getApplyNum().toString());
				}else{
					map.put("applyNum", "");
				}
				if(srai.getLackNum()!=null){
					Integer n=null;
					List<GoodsMaterialsReadyItem> num=goodsMaterialsApplyDao.selectSumNum(srai.getId());
					if(num.size()>0){
						for(GoodsMaterialsReadyItem gd:num){
							n=gd.getSendNum();
						}
					}
					if(n==srai.getApplyNum()){
						map.put("lackNum", "0");
					}else{
						Integer s=srai.getApplyNum()-n;
						map.put("lackNum", s.toString());
					}

				}else{
					map.put("lackNum", "");
				}		
				
//				if(srai.getGoodsMaterialsDetails().getUnit()!=null){
//					map.put("unitName", srai.getGoodsMaterialsDetails().getUnit().getName());
//				}else{
//					map.put("unitName", "");
//				}
				if(srai.getUnuseNum()!=null){
					map.put("unuseNum", srai.getUnuseNum().toString());
				}else{
					map.put("unuseNum", "");
				}
				if(srai.getUsedNum()!=null){
					map.put("usedNum", srai.getUsedNum().toString());
				}else{
					map.put("usedNum", "");
				}
				if(srai.getWayNum()!=null){
					map.put("wayNum", srai.getWayNum().toString());
				}else{
					map.put("wayNum", "");
				}
				//map.put("unuseNum", srai.getUnuseNum().toString());
				//map.put("usedNum", srai.getUsedNum().toString());
				//map.put("wayNum", srai.getWayNum().toString());
				map.put("state", srai.getState());
				mapList.add(map);
			}
		}
		return mapList;
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(GoodsMaterialsApply sc, Map jsonMap,String aId,String aName,String phone ,String receiver) throws Exception {
		if (sc != null) {

			goodsMaterialsApplyDao.saveOrUpdate(sc);

			if(aId.equals("")&&!aName.equals("")){
				ApplyOrganizeAddress ao=new ApplyOrganizeAddress();
				ao.setName(aName);
				ao.setPhone(phone);
				ao.setReceiver(receiver);
				ao.setApplyOrganize(sc.getApplyOrganize());
				
				commonDAO.saveOrUpdate(ao);
				sc.setAddress(ao);
			}
			
			String jsonStr = "";
			jsonStr = (String)jsonMap.get("goodsMaterialsApplyWay");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveGoodsMaterialsApplyWay(sc, jsonStr);
			}
			jsonStr = (String)jsonMap.get("goodsMaterialsApplyItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveGoodsMaterialsApplyItem(sc, jsonStr);
			}
		}
   }
	//审批完成
	public void changeState(String applicationTypeActionId, String id) {
		GoodsMaterialsApply sct = goodsMaterialsApplyDao.get(GoodsMaterialsApply.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		sct.setConfirmDate(new Date());
		goodsMaterialsApplyDao.update(sct);
		
		String code=sct.getId();
		List<GoodsMaterialsApplyItem> list=goodsMaterialsApplyDao.showGoodsMaterialsApplyItemList(code);
		for(GoodsMaterialsApplyItem gai:list){
			gai.setState("申请完成");
		}
		
	}
}
