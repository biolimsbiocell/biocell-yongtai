package com.biolims.goods.mate.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.goods.mate.dao.GoodsMaterialsApplyDao;
import com.biolims.goods.mate.dao.GoodsMaterialsReadyDao;
import com.biolims.goods.mate.dao.GoodsMaterialsSendDao;
import com.biolims.goods.mate.model.GoodsMaterialsApply;
import com.biolims.goods.mate.model.GoodsMaterialsApplyItem;
import com.biolims.goods.mate.model.GoodsMaterialsPostItem;
import com.biolims.goods.mate.model.GoodsMaterialsReady;
import com.biolims.goods.mate.model.GoodsMaterialsReadyItem;
import com.biolims.goods.mate.model.GoodsMaterialsSend;
import com.biolims.goods.mate.model.GoodsMaterialsSendBox;
import com.biolims.goods.mate.model.GoodsMaterialsSendExpress;
import com.biolims.goods.mate.model.GoodsMaterialsSendItem;
import com.biolims.goods.mate.model.GoodsMaterialsSendItemTwo;
import com.biolims.goods.mate.model.GoodsMaterialsWay;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.system.box.model.TransBox;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class GoodsMaterialsSendService {
	@Resource
	private GoodsMaterialsSendDao goodsMaterialsSendDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private GoodsMaterialsApplyDao goodsMaterialsApplyDao;
	@Resource
	private GoodsMaterialsReadyDao goodsMaterialsReadyDao;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findGoodsMaterialsSendList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return goodsMaterialsSendDao.selectGoodsMaterialsSendList(mapForQuery, startNum, limitNum, dir, sort);
	}
	//根据状态显示主数据
	public Map<String, Object> findGoodsMaterialsSendListByState(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return goodsMaterialsSendDao.selectGoodsMaterialsSendListByState(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(GoodsMaterialsSend i) throws Exception {

		goodsMaterialsSendDao.saveOrUpdate(i);

	}
	public GoodsMaterialsSend get(String id) {
		GoodsMaterialsSend goodsMaterialsSend = commonDAO.get(GoodsMaterialsSend.class, id);
		return goodsMaterialsSend;
	}
	public GoodsMaterialsApply getApp(String id) {
		GoodsMaterialsApply goodsMaterialsApply = commonDAO.get(GoodsMaterialsApply.class, id);
		return goodsMaterialsApply;
	}
	public Map<String, Object> findGoodsMaterialsSendItemList(String scId,String appId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = goodsMaterialsSendDao.selectGoodsMaterialsSendItemList(scId,appId, startNum, limitNum, dir, sort);
		List<GoodsMaterialsSendItemTwo> list = (List<GoodsMaterialsSendItemTwo>) result.get("list");
		return result;
	}
	public Map<String, Object> findGoodsMaterialsSendBoxList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = goodsMaterialsSendDao.selectGoodsMaterialsSendBoxList(scId, startNum, limitNum, dir, sort);
		List<GoodsMaterialsSendBox> list = (List<GoodsMaterialsSendBox>) result.get("list");
		return result;
	}
	public Map<String, Object> findGoodsMaterialsSendExpressList(String scId, String appId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = goodsMaterialsSendDao.selectGoodsMaterialsSendExpressList(scId, appId, startNum, limitNum, dir, sort);
		List<GoodsMaterialsSendExpress> list = (List<GoodsMaterialsSendExpress>) result.get("list");
		return result;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveGoodsMaterialsSendTwoItem(GoodsMaterialsSend sc, String itemDataJson) throws Exception {
		List<GoodsMaterialsSendItemTwo> saveItems = new ArrayList<GoodsMaterialsSendItemTwo>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			GoodsMaterialsSendItemTwo scp = new GoodsMaterialsSendItemTwo();
			// 将map信息读入实体类
			scp = (GoodsMaterialsSendItemTwo) goodsMaterialsSendDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setGoodsMaterialsSend(sc);
			scp.setState("发放中");
			saveItems.add(scp);
			
			String code = scp.getGoodsMaterialsSend().getGoodsMaterialsApply().getId();
			List<GoodsMaterialsApplyItem> l = this.goodsMaterialsApplyDao.showGoodsMaterialsApplyItem(code);
			for (GoodsMaterialsApplyItem aaa : l) {
				aaa.setState("发放中");
				this.goodsMaterialsApplyDao.saveOrUpdate(aaa);
			}
		}
		goodsMaterialsSendDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delGoodsMaterialsSendItem(String[] ids) throws Exception {
		for (String id : ids) {
			GoodsMaterialsSendItem scp =  goodsMaterialsSendDao.get(GoodsMaterialsSendItem.class, id);
			 goodsMaterialsSendDao.delete(scp);
		}
	}
	/**
	 * 根据ID删除
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delGoodsMaterialsSendOne(String ids) throws Exception {
			GoodsMaterialsSendItem scp =  goodsMaterialsSendDao.get(GoodsMaterialsSendItem.class, ids);
			goodsMaterialsSendDao.delete(scp);
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveGoodsMaterialsSendBox(GoodsMaterialsSend sc, String itemDataJson) throws Exception {
		List<GoodsMaterialsSendBox> saveItems = new ArrayList<GoodsMaterialsSendBox>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		
		for (Map<String, Object> map : list) {
			GoodsMaterialsSendBox scp = new GoodsMaterialsSendBox();
			scp.setId((String)map.get("id"));
			scp.setBoxType((String)map.get("boxType"));
			scp.setBoxName((String)map.get("boxName"));
			scp.setBoxSpec((String)map.get("boxSpec"));
			scp.setBoxNum((String)map.get("boxNum"));
//			scp.setNum(Integer.parseInt((String) map.get("num")));
//			TransBox tb = new TransBox();
//			tb.setId((String)map.get("transBox-id"));
//			tb.setName((String)map.get("transBox-names"));
//			scp.setTransBox(tb);
			/*// 将map信息读入实体类
			scp = (GoodsMaterialsSendBox) goodsMaterialsSendDao.Map2Bean(map, scp);*/
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setGoodsMaterialsSend(sc);
			saveItems.add(scp);				
		}
		goodsMaterialsSendDao.saveOrUpdateAll(saveItems);
		String code=sc.getId();		
		if(goodsMaterialsSendDao.selectBoxNum(code)!=null){
			int num=Integer.valueOf(goodsMaterialsSendDao.selectBoxNum(code).toString());
			sc.setNum(num);	
		}
		
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delGoodsMaterialsSendBox(String[] ids) throws Exception {
		for (String id : ids) {
			GoodsMaterialsSendBox scp =  goodsMaterialsSendDao.get(GoodsMaterialsSendBox.class, id);
			 goodsMaterialsSendDao.delete(scp);
		}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveGoodsMaterialsSendExpress(GoodsMaterialsSend sc, String itemDataJson) throws Exception {
		List<GoodsMaterialsSendExpress> saveItems = new ArrayList<GoodsMaterialsSendExpress>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			GoodsMaterialsSendExpress scp = new GoodsMaterialsSendExpress();
			// 将map信息读入实体类
			scp = (GoodsMaterialsSendExpress) goodsMaterialsSendDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setGoodsMaterialsSend(sc);
			
			saveItems.add(scp);
			//将物料准备中数据保存到物流中
//			String id1=sc.getGoodsMaterialsApply().getId();
//			GoodsMaterialsReady ready=goodsMaterialsReadyDao.get(GoodsMaterialsReady.class,id1);
			String id2=sc.getGoodsMaterialsApply().getId();
			GoodsMaterialsApply apply=goodsMaterialsApplyDao.get(GoodsMaterialsApply.class,id2);
			scp.setAddress(apply.getAddress().getName());
			scp.setReceiveUser(apply.getReceiveUser());
			if(apply.getCompany()!=null)
			sc.setCompanyId(apply.getCompany().getId());
//			sc.setCompanyId("");
			if(apply.getCompany()!=null)
			scp.setCompany(apply.getCompany().getName());
//			scp.setCompany("");
			scp.setLinkUser(apply.getReceiveUser());
			scp.setPhone(apply.getPhone());
			//反馈快递信息到申请页面
			//setExpressToApply(sc.getId());
//			String code=scp.getCode();
//			String eid=scp.getId();
//			List<GoodsMaterialsApplyWay> wayList=goodsMaterialsApplyDao.showMaterialsApplyWayList(eid);
//			if(code!=null){
//				if(wayList.size()==0){
//					GoodsMaterialsApplyWay way=new GoodsMaterialsApplyWay();
//					way.setGoodsMaterialsApply(apply);
//					way.setGoodsMaterialsSendExpress(scp);
//					goodsMaterialsApplyDao.saveOrUpdate(way);
//				}
//			}
		}
		goodsMaterialsSendDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delGoodsMaterialsSendExpress(String[] ids) throws Exception {
		for (String id : ids) {
			GoodsMaterialsSendExpress scp =  goodsMaterialsSendDao.get(GoodsMaterialsSendExpress.class, id);
			 goodsMaterialsSendDao.delete(scp);
		}
	}
	//将快递信息返回到申请页面
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void setExpressToApply(String id) throws Exception{
//		List<GoodsMaterialsSendExpress> list=goodsMaterialsSendDao.setExpressListToApply(id);
//		if(list.size()>0){
//			for(GoodsMaterialsSendExpress gs:list){			
//			String code=gs.getGoodsMaterialsSend().getId();
//			GoodsMaterialsSend send=goodsMaterialsSendDao.get(GoodsMaterialsSend.class,code);
//			String code1=send.getGoodsMaterialsReady().getId();
//			GoodsMaterialsReady ready=goodsMaterialsReadyDao.get(GoodsMaterialsReady.class,code1);
//			String code2=ready.getGoodsMaterialsApply().getId();
//			GoodsMaterialsApply apply=goodsMaterialsApplyDao.get(GoodsMaterialsApply.class,code2);
//			List<GoodsMaterialsApplyWay> wayList=goodsMaterialsApplyDao.showMaterialsApplyWayList(id);
//			//发放的快递单
//			String eid=gs.getId();
////			String companyName=(String)goodsMaterialsSendDao.find("select t.name from DicType t where t.id='"+companyId+"'").get(0);
//			//DicType company = goodsMaterialsSendDao.get(DicType.class, companyId);
//			if(wayList.size()==0){
//				for (GoodsMaterialsApplyWay ga : wayList) {
//					String wid=ga.getId();
//					if(!eid.equals(wid)){
//						GoodsMaterialsApplyWay way=new GoodsMaterialsApplyWay();
//						way.setGoodsMaterialsApply(apply);
//						way.setGoodsMaterialsSendExpress(gs);
//
//						goodsMaterialsApplyDao.saveOrUpdate(way);			
//					}				
//				}
//			}
//		}
//	}
//}
//	//将快递信息返回到申请页面
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public List<Map<String, String>> setExpressToApply(String code) throws Exception {
//		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
//		Map<String, Object> result = goodsMaterialsSendDao.setGoodsMaterialsSendExpressList(code);
//		List<GoodsMaterialsSendExpress> list = (List<GoodsMaterialsSendExpress>) result.get("list");
//
//		if (list != null && list.size() > 0) {
//			for (GoodsMaterialsSendExpress srai : list) {
//				//Map<String, String> map = new HashMap<String, String>();				
////				List<GoodsMaterialsApplyWay> wayList=goodsMaterialsApplyDao.showMaterialsApplyItemList(srai.getGoodsMaterialsSend().getGoodsMaterialsReady().getGoodsMaterialsApply().getId());
////				String wid="";
////				for (GoodsMaterialsApplyWay ga : wayList) {
////					wid=ga.getId();
////				}
////				if(srai.getId()!=wid){
//					GoodsMaterialsApplyWay way=new GoodsMaterialsApplyWay();
//					//way.setId(srai.getId());
//					way.setExpressOrder(srai.getCode());
//					way.setExpressCompany(srai.getGoodsMaterialsSend().getCompany().getName());
//					way.setGoodsMaterialsApply(srai.getGoodsMaterialsSend().getGoodsMaterialsReady().getGoodsMaterialsApply());
//					goodsMaterialsApplyDao.saveOrUpdate(way);
////				}				
//				
//			}
//		}
//		return mapList;
//	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(GoodsMaterialsSend sc, Map jsonMap) throws Exception {
		if (sc != null) {
			goodsMaterialsSendDao.saveOrUpdate(sc);
			String jsonStr = "";
			jsonStr = (String)jsonMap.get("goodsMaterialsSendTwoItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveGoodsMaterialsSendTwoItem(sc, jsonStr);
			}
			jsonStr = (String)jsonMap.get("goodsMaterialsSendBox");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveGoodsMaterialsSendBox(sc, jsonStr);
			}
			jsonStr = (String)jsonMap.get("goodsMaterialsSendExpress");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveGoodsMaterialsSendExpress(sc, jsonStr);	
			}
			//setExpressToApply(sc.getId());
		}
   }
	//审批完成
	public void changeState(String applicationTypeActionId, String id) {
		GoodsMaterialsSend sct = goodsMaterialsSendDao.get(GoodsMaterialsSend.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		sct.setConfirmUser(user);
		sct.setConfirmDate(new Date());

		goodsMaterialsSendDao.update(sct);
		
		String code=sct.getId();
		sct.getGoodsMaterialsApply().setState("2");
		List<GoodsMaterialsSendItem> list=this.goodsMaterialsSendDao.showGoodsMaterialsSendItem(code);
		for(GoodsMaterialsSendItem gai:list){
			gai.setState("发放完成");
//			Integer num1=gai.getGoodsMaterialsReadyItem().getSendNum();
//			Integer num2=gai.getGoodsMaterialsReadyItem().getGoodsMaterialsApplyItem().getApplyNum();
//			if(num1==num2){
//				sct.getGoodsMaterialsReady().setState("2");
//			}else{
//				gai.getGoodsMaterialsReadyItem().setSendNum(num2-num1);
//			}
			String code1=gai.getGoodsMaterialsApplyItem().getId();
			List<GoodsMaterialsReadyItem> list1=this.goodsMaterialsReadyDao.showGoodsMaterialsReadyItemListById(code1);
			for(GoodsMaterialsReadyItem gai1:list1){
				gai1.setState("发放完成");
				String code2 = gai1.getGoodsMaterialsApplyItem().getId();
				List<GoodsMaterialsApplyItem> listApplyItem = this.goodsMaterialsApplyDao.showGoodsMaterialsApplyItem(code2);
				for (GoodsMaterialsApplyItem item : listApplyItem) {
					item.setState("发放完成");
				}
			}
		}
		//String id1=sct.getGoodsMaterialsReady().getId();
		//GoodsMaterialsReady ready=goodsMaterialsReadyDao.get(GoodsMaterialsReady.class,id1);
		//String id2=ready.getGoodsMaterialsApply().getId();
		//GoodsMaterialsApply apply=goodsMaterialsApplyDao.get(GoodsMaterialsApply.class,id2);
		List<GoodsMaterialsSendExpress> list3=goodsMaterialsSendDao.setExpressListToApply(id);
		if(list3.size()>0){
			for(GoodsMaterialsSendExpress se:list3){
				//GoodsMaterialsApplyWay way=new GoodsMaterialsApplyWay();
				//GoodsMaterialsSendExpress gse=goodsMaterialsSendDao.get(GoodsMaterialsSendExpress.class, se.getId());
				//way.setGoodsMaterialsApply(apply);
				//way.setGoodsMaterialsSendExpress(gse);
				//goodsMaterialsApplyDao.saveOrUpdate(way);
				
//				GoodsMaterialsPostItem gp=new GoodsMaterialsPostItem();
//				gp.setGoodsMaterialsSend(sct);
//				gp.setGoodsMaterialsSendExpress(gse);
//				gp.setExpressOrder(se.getCode());
//				gp.setReceiver(se.getReceiveUser());
//				gp.setAddress(se.getAddress());
//				gp.setCompany(se.getCompany());
				GoodsMaterialsWay gw = new GoodsMaterialsWay();
				gw.setAddress(se.getAddress());
				gw.setCode(se.getId());
				gw.setReceiver(se.getReceiveUser());
				gw.setExpressOrder(se.getCode());
				gw.setCompany(se.getCompany());
				gw.setExpectDate(se.getPlanDate());
				gw.setRealDate(se.getRealDate());
				
				commonDAO.saveOrUpdate(gw);
			}
		}
		//判断在途数量
//		List<GoodsMaterialsApplyWay> list4=goodsMaterialsApplyDao.showMaterialsApplyWayList1(apply.getId());
//		List<GoodsMaterialsApplyItem> list5=goodsMaterialsApplyDao.showGoodsMaterialsApplyItemList(apply.getId());
//		if(list4.size()>0){
//			for(GoodsMaterialsApplyWay ga:list4){
//				if(ga.getIsReceive()==null || ga.getIsReceive().equals("0")){
//					
//				} 
//			}
//		}
	}
	//根据准备单加载子表
	public List<Map<String, String>> showGoodsMaterialsSendExpressList(String code) throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = goodsMaterialsSendDao.setGoodsMaterialsSendExpressList(code);
		List<GoodsMaterialsSendExpress> list = (List<GoodsMaterialsSendExpress>) result.get("list");

		if (list != null && list.size() > 0) {
			for (GoodsMaterialsSendExpress srai : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", srai.getId());
				
				map.put("code", srai.getCode());
				map.put("address", srai.getGoodsMaterialsSend().getGoodsMaterialsApply().getAddress().getName());
				map.put("company", srai.getCompany());
				map.put("receiver", srai.getGoodsMaterialsSend().getGoodsMaterialsApply().getReceiveUser());
				if(srai.getPlanDate()!=null){
					DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					String stime=format.format(srai.getPlanDate());
					map.put("expectDate", stime);
				}else{
					map.put("expectDate", "");
				}
				mapList.add(map);
			}
		}
		return mapList;
	}
	
	//查询物料申请
	public Map<String, Object> findGoodsMaterialsApplyList(String appId, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return this.goodsMaterialsSendDao.selectGoodsMaterialsApplyList(appId, startNum, limitNum, dir, sort);
	}
	
	
}
