package com.biolims.goods.mate.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.goods.mate.dao.GoodsMaterialsApplyDao;
import com.biolims.goods.mate.dao.GoodsMaterialsReadyDao;
import com.biolims.goods.mate.dao.GoodsMaterialsWayDao;
import com.biolims.goods.mate.model.GoodsMaterialsApply;
import com.biolims.goods.mate.model.GoodsMaterialsApplyWay;
import com.biolims.goods.mate.model.GoodsMaterialsReady;
import com.biolims.goods.mate.model.GoodsMaterialsReadyItem;
import com.biolims.goods.mate.model.GoodsMaterialsSend;
import com.biolims.goods.mate.model.GoodsMaterialsSendExpress;
import com.biolims.goods.mate.model.GoodsMaterialsWay;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.system.organize.model.ApplyOrganizeAddress;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class GoodsMaterialsWayService {
	@Resource
	private GoodsMaterialsWayDao goodsMaterialsWayDao;
	@Resource
	private GoodsMaterialsReadyDao goodsMaterialsReadyDao;
	@Resource
	private GoodsMaterialsApplyDao goodsMaterialsApplyDao;
	@Resource
	private CommonDAO commonDAO;
	
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findGoodsMaterialsWayList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return goodsMaterialsWayDao.selectGoodsMaterialsWayList(mapForQuery, startNum, limitNum, dir, sort);
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveToWay(String itemDataJson) throws Exception {
		List<GoodsMaterialsWay> saveItems = new ArrayList<GoodsMaterialsWay>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			GoodsMaterialsWay sbi = new GoodsMaterialsWay();
			sbi = (GoodsMaterialsWay) goodsMaterialsWayDao.Map2Bean(map, sbi);
			if (sbi.getId() != null && sbi.getId().equals(""))
				sbi.setId(null);
			
			saveItems.add(sbi);
			
			//确认发出后，申请中显示在途数量
			String id1=sbi.getCode();
			String address=sbi.getAddress();
			ApplyOrganizeAddress ao=goodsMaterialsWayDao.selectAddress(address);
			//GoodsMaterialsPost post=commonDAO.get(GoodsMaterialsPost.class, id1);
			GoodsMaterialsSendExpress postItem=commonDAO.get(GoodsMaterialsSendExpress.class, id1);
			String sendId=postItem.getGoodsMaterialsSend().getId();
			GoodsMaterialsSend send=commonDAO.get(GoodsMaterialsSend.class, sendId);
			String id2=send.getGoodsMaterialsApply().getId();
			GoodsMaterialsApply apply=commonDAO.get(GoodsMaterialsApply.class, id2);
			if(sbi.getIsSend().equals("1")){
				if(sbi.getSendDate()!=null){
					sbi.setSendDate(new Date());
				}
				//计算预计到达时间(确认发出的时间+收货地址的运输时间)
				Date date = new Date();
				Calendar cal = Calendar.getInstance();
				cal.setTime(date);
				if(ao.getTransTime()!=null){
					cal.add(Calendar.DATE,ao.getTransTime());
				}
				String s=(new SimpleDateFormat("yyyy-MM-dd")).format(cal.getTime());
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Date sd = sdf.parse(s);
				sbi.setExpectDate(sd);
				//postItem.setExpectDate(sd);
				
//				if(gaList.size()>0){
//					for(GoodsMaterialsApplyWay gaw:gaList){
//						gaw.setPredictDate(sd);
//					}
//				}
				
				GoodsMaterialsApplyWay way=new GoodsMaterialsApplyWay();
				way.setGoodsMaterialsApply(apply);
				way.setGoodsMaterialsSendExpress(sbi);
				way.setPredictDate(sd);
				commonDAO.saveOrUpdate(way);
			}
		}
		goodsMaterialsWayDao.saveOrUpdateAll(saveItems);
	}
	
	public GoodsMaterialsWay get(String id) {
		GoodsMaterialsWay goodsMaterialsWay = commonDAO.get(GoodsMaterialsWay.class, id);
		return goodsMaterialsWay;
	}
}
