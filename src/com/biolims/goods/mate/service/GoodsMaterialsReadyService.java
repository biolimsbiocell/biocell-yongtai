package com.biolims.goods.mate.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.common.service.SystemCodeService;
import com.biolims.dic.model.DicType;
import com.biolims.goods.mate.dao.GoodsMaterialsApplyDao;
import com.biolims.goods.mate.dao.GoodsMaterialsReadyDao;
import com.biolims.goods.mate.model.GoodsMaterialsApply;
import com.biolims.goods.mate.model.GoodsMaterialsApplyItem;
import com.biolims.goods.mate.model.GoodsMaterialsReady;
import com.biolims.goods.mate.model.GoodsMaterialsReadyDetails;
import com.biolims.goods.mate.model.GoodsMaterialsReadyItem;
import com.biolims.goods.mate.model.GoodsMaterialsSend;
import com.biolims.goods.pack.model.GoodsMaterialsPack;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.system.organize.dao.OfficeApplyDao;
import com.biolims.system.organize.model.ApplyOrganize;
import com.biolims.system.organize.model.OfficeApplyItem;
import com.biolims.system.product.model.Product;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class GoodsMaterialsReadyService {
	@Resource
	private OfficeApplyDao officeApplyDao;
	@Resource
	private GoodsMaterialsReadyDao goodsMaterialsReadyDao;
	@Resource
	private GoodsMaterialsApplyDao goodsMaterialsApplyDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SystemCodeService systemCodeService;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findGoodsMaterialsReadyList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return goodsMaterialsReadyDao.selectGoodsMaterialsReadyList(mapForQuery, startNum, limitNum, dir, sort);
	}
	//==
	public Map<String, Object> findGoodsMaterialsReadyDetailList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort,String flag) {
		return goodsMaterialsReadyDao.selectGoodsMaterialsReadyDetailsList(mapForQuery, startNum, limitNum, dir, sort,flag);
	}
	//====
	//根据状态查询主数据
	public Map<String, Object> findGoodsMaterialsReadyListByState(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return goodsMaterialsReadyDao.selectGoodsMaterialsReadyListByState(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(GoodsMaterialsReady i) throws Exception {

		goodsMaterialsReadyDao.saveOrUpdate(i);

	}
	public GoodsMaterialsReady get(String id) {
		GoodsMaterialsReady goodsMaterialsReady = commonDAO.get(GoodsMaterialsReady.class, id);
		return goodsMaterialsReady;
	}
	public Map<String, Object> findGoodsMaterialsSendDetailsList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = goodsMaterialsReadyDao.selectGoodsMaterialsSendDetailsList(scId, startNum, limitNum, dir, sort);
		List<GoodsMaterialsReadyDetails> list = (List<GoodsMaterialsReadyDetails>) result.get("list");
		return result;
	}
	//未使用的采血管
	public Map<String, Object> findGoodsMaterialsReadyDetailsState(String scId, Integer startNum, Integer limitNum, String dir,
			String sort,String code) throws Exception {
		Map<String, Object> result = goodsMaterialsReadyDao.selectGoodsMaterialsReadyDetailsState(scId, startNum, limitNum, dir, sort,code);
		List<GoodsMaterialsReadyDetails> list = (List<GoodsMaterialsReadyDetails>) result.get("list");
		return result;
	}
	//过期的采血管
	public Map<String, Object> findGoodsMaterialsReadyDetailsOut(String scId, Integer startNum, Integer limitNum, String dir,
			String sort,String code) throws Exception {
		Map<String, Object> result = goodsMaterialsReadyDao.selectGoodsMaterialsReadyDetailsOut(scId, startNum, limitNum, dir, sort,code);
		List<GoodsMaterialsReadyDetails> list = (List<GoodsMaterialsReadyDetails>) result.get("list");
		return result;
	}
	public Map<String, Object> findGoodsMaterialsReadyItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = goodsMaterialsReadyDao.selectGoodsMaterialsReadyItemList(scId, startNum, limitNum, dir, sort);
		List<GoodsMaterialsReadyItem> list = (List<GoodsMaterialsReadyItem>) result.get("list");
		return result;
	}
	public Map<String, Object> findGoodsMaterialsReadyItemList1(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = goodsMaterialsReadyDao.selectGoodsMaterialsReadyItemList1(scId, startNum, limitNum, dir, sort);
		List<GoodsMaterialsReadyItem> list = (List<GoodsMaterialsReadyItem>) result.get("list");
		return result;
	}
	
	//设置采血管的超期状态
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void setOutState(String code) throws Exception {
		GoodsMaterialsReadyDetails g=goodsMaterialsReadyDao.getReadDetailsByCode(code);
		g.setOutState("1");
	}
	//
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveGoodsMaterialsReadyDetails(GoodsMaterialsSend sc, String itemDataJson) throws Exception {
		List<GoodsMaterialsReadyDetails> saveItems = new ArrayList<GoodsMaterialsReadyDetails>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		GoodsMaterialsApply gma = goodsMaterialsReadyDao.get(GoodsMaterialsApply.class, sc.getGoodsMaterialsApply().getId());
		ApplyOrganize ao = goodsMaterialsReadyDao.get(ApplyOrganize.class, gma.getApplyOrganize().getId());
		String markCode = ao.getId();
		for (Map<String, Object> map : list) {
			GoodsMaterialsReadyDetails scp = new GoodsMaterialsReadyDetails();
			// 将map信息读入实体类
			scp = (GoodsMaterialsReadyDetails) goodsMaterialsReadyDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals("")){
				scp.setId(null);
			}
			scp.setGoodsMaterialsSend(sc);
			GoodsMaterialsPack gmp = goodsMaterialsReadyDao.get(GoodsMaterialsPack.class, scp.getPackCode());
			Product product = goodsMaterialsReadyDao.get(Product.class, gmp.getProduct().getId());
			DicType sampleType = goodsMaterialsReadyDao.get(DicType.class, gmp.getSampleType().getId());
//			DicType bloodTube = goodsMaterialsReadyDao.get(DicType.class, gmp.getBloodTube().getId());
			if(scp.getCode()==null){
				scp.setCode(genTransID(product.getMark()+sampleType.getSysCode()+gmp.getBloodTube().getId(),markCode));
				//scp.setCode(genTransID("QQ"+"WW"+"EE",markCode));
			}
			saveItems.add(scp);
			
			goodsMaterialsReadyDao.saveOrUpdateAll(saveItems);
		}
//		goodsMaterialsReadyDao.saveOrUpdateAll(saveItems);
	}
	
	//生成编码
	public String genTransID(String pcode,String markCode) throws Exception {
		String modelName = "GoodsMaterialsReadyDetails";
		String autoID = getCodeByPrefix(modelName, findIdPrefix(modelName,pcode.substring(0,4)+markCode), 00000, 5,null);
		/*Map<String, Object> result = new HashMap<String, Object>();
		try {
			result.put("success", true);
			result.put("data", autoID);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));*/
		return autoID;
	}

	public String getCodeByPrefix(String modelName, String prefix, long code, Integer flowLength,Map<String, String> mapForQuery) throws Exception {
		Integer flowNum = 0;

		String val = prefix;
		if (mapForQuery == null)
			mapForQuery = new HashMap<String, String>();
		mapForQuery.put("code", "like##@@##'" + val + "%'");

		String maxId = goodsMaterialsReadyDao.selectModelTotalByType(modelName,mapForQuery);
		String _maxId = flowNum.toString();
		if (maxId != null)
			_maxId = maxId.substring(maxId.length() - flowLength);
		else {
			_maxId = String.valueOf(code);
		}

		String parseId = "";

		parseId = goodsMaterialsReadyDao.codeParse(Integer.valueOf(_maxId) + 1 + "", flowLength);

		return val + parseId;
	}

	//查询编码的前缀
	public String findIdPrefix(String modelName,String pcode) {
		return goodsMaterialsReadyDao.findIdPrefix(modelName,pcode);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delGoodsMaterialsReadyDetails(String[] ids) throws Exception {
		for (String id : ids) {
			GoodsMaterialsReadyDetails scp =  goodsMaterialsReadyDao.get(GoodsMaterialsReadyDetails.class, id);
			 goodsMaterialsReadyDao.delete(scp);
		}
	}
	//==========================
	/**
	 * 根据ID删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delGoodsMaterialsReadyDetailsOne(String ids) throws Exception {
			GoodsMaterialsReadyDetails scp =  goodsMaterialsReadyDao.get(GoodsMaterialsReadyDetails.class, ids);
			 goodsMaterialsReadyDao.delete(scp);
	}
	//==========================
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveGoodsMaterialsReadyItem(GoodsMaterialsReady sc, String itemDataJson) throws Exception {
		List<GoodsMaterialsReadyItem> saveItems = new ArrayList<GoodsMaterialsReadyItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			GoodsMaterialsReadyItem scp = new GoodsMaterialsReadyItem();
			// 将map信息读入实体类
			scp = (GoodsMaterialsReadyItem) goodsMaterialsReadyDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setGoodsMaterialsReady(sc);
			scp.setState("准备中");
			saveItems.add(scp);
			String code=scp.getGoodsMaterialsApplyItem().getId();
			List<GoodsMaterialsApplyItem> list1=goodsMaterialsApplyDao.showGoodsMaterialsApplyItem(code);
			for(GoodsMaterialsApplyItem gai:list1){
				gai.setState("准备中");
			}
		}
		goodsMaterialsReadyDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delGoodsMaterialsReadyItem(String[] ids) throws Exception {
		for (String id : ids) {
			GoodsMaterialsReadyItem scp =  goodsMaterialsReadyDao.get(GoodsMaterialsReadyItem.class, id);
			 goodsMaterialsReadyDao.delete(scp);
		}
	}
	//根据ID删除
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delGoodsMaterialsReadyOne(String ids) throws Exception {
			GoodsMaterialsReadyItem scp =  goodsMaterialsReadyDao.get(GoodsMaterialsReadyItem.class, ids);
			goodsMaterialsReadyDao.delete(scp);
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(GoodsMaterialsSend sc, Map jsonMap) throws Exception {
		if (sc != null) {
			goodsMaterialsReadyDao.saveOrUpdate(sc);
		
			String jsonStr = "";

			jsonStr = (String)jsonMap.get("goodsMaterialsReadyItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveGoodsMaterialsReadyDetails(sc, jsonStr);
			}
			jsonStr = (String)jsonMap.get("goodsMaterialsReadyDetails");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveGoodsMaterialsReadyDetails(sc, jsonStr);
			}
		}
   }
	//根据准备单加载子表
	public List<Map<String, String>> showGoodsMaterialsReadyItemList(String code) throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = goodsMaterialsReadyDao.setGoodsMaterialsReadyItemList(code);
		List<GoodsMaterialsReadyItem> list = (List<GoodsMaterialsReadyItem>) result.get("list");

		if (list != null && list.size() > 0) {
			for (GoodsMaterialsReadyItem srai : list) {
				Map<String, String> map = new HashMap<String, String>();
//				map.put("materialsId", srai.getGoodsMaterialsApplyItem().getGoodsMaterialsDetails().getCode());
//				map.put("materialsName", srai.getGoodsMaterialsApplyItem().getGoodsMaterialsDetails().getName());
				map.put("pid", srai.getGoodsMaterialsApplyItem().getGoodsMaterialsPack().getId());
				map.put("pname", srai.getGoodsMaterialsApplyItem().getGoodsMaterialsPack().getName());
				map.put("linkMan", srai.getGoodsMaterialsApplyItem().getGoodsMaterialsApply().getReceiveUser());
				if(srai.getGoodsMaterialsApplyItem().getGoodsMaterialsApply().getAddress()!=null){
					map.put("address", srai.getGoodsMaterialsApplyItem().getGoodsMaterialsApply().getAddress().getName());
				}else{
					map.put("address", "");
				}
				if(srai.getGoodsMaterialsApplyItem().getGoodsMaterialsApply().getCompany()!=null){
					map.put("company", srai.getGoodsMaterialsApplyItem().getGoodsMaterialsApply().getCompany().getName());
				}else{
					map.put("company", "");
				}
					
				
				if(srai.getGoodsMaterialsApplyItem().getApplyNum()!=null){
					map.put("applyNum", srai.getGoodsMaterialsApplyItem().getApplyNum().toString());
				}else{
					map.put("applyNum", "");
				}
				
//				map.put("unitName", srai.getGoodsMaterialsApplyItem().getGoodsMaterialsDetails().getUnit().getName());
				if(srai.getSendNum()!=null){
					map.put("sendName", srai.getSendNum().toString());
				}else{
					map.put("sendName", "");
				}
				//map.put("sendName", srai.getSendNum());
				if(srai.getGoodsMaterialsApplyItem().getUnuseNum()!=null){
					map.put("unuseNum", srai.getGoodsMaterialsApplyItem().getUnuseNum().toString());
				}else{
					map.put("unuseNum", "");
				}
				if(srai.getGoodsMaterialsApplyItem().getUsedNum()!=null){
					map.put("usedNum", srai.getGoodsMaterialsApplyItem().getUsedNum().toString());
				}else{
					map.put("usedNum", "");
				}
				if(srai.getGoodsMaterialsApplyItem().getWayNum()!=null){
					map.put("wayNum", srai.getGoodsMaterialsApplyItem().getWayNum().toString());
				}else{
					map.put("wayNum", "");
				}
				//map.put("unuseNum", srai.getGoodsMaterialsApplyItem().getUnuseNum().toString());
				//map.put("usedNum", srai.getGoodsMaterialsApplyItem().getUsedNum().toString());
				//map.put("wayNum", srai.getGoodsMaterialsApplyItem().getWayNum().toString());
				//map.put("state", srai.getState());
				map.put("mid", srai.getId());
				map.put("id", srai.getGoodsMaterialsReady().getId());
				map.put("name", srai.getGoodsMaterialsReady().getName());
				mapList.add(map);
			}
		}
		return mapList;
	}
	//审批完成
	public void changeState(String applicationTypeActionId, String id) {
		GoodsMaterialsReady sct = goodsMaterialsReadyDao.get(GoodsMaterialsReady.class, id);
		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);

		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		sct.setConfirmUser(user);
		sct.setConfirmDate(new Date());

		goodsMaterialsReadyDao.update(sct);
		
		List<OfficeApplyItem> oaItem=officeApplyDao.getOfficeApplyItem(id);
		String aa=null;
		for (OfficeApplyItem officeApplyItem : oaItem) {
			officeApplyItem.setState("56");
			aa=officeApplyItem.getCode();
		}
		
		List<GoodsMaterialsReadyDetails> rd=goodsMaterialsReadyDao.getReadDetailsCode(aa);
		for (GoodsMaterialsReadyDetails item : rd) {
			item.setState("55");
		}
		
		String code=sct.getId();
		boolean flag=true;
		List<GoodsMaterialsReadyItem> list=goodsMaterialsReadyDao.showGoodsMaterialsReadyItemList(code);
		if(list.size()>0){
			for(GoodsMaterialsReadyItem gai:list){
				gai.setState("准备完成");
				gai.getGoodsMaterialsApplyItem().setState("准备完成");
				String code1=gai.getGoodsMaterialsApplyItem().getId();
				//List<GoodsMaterialsApplyItem> list1=goodsMaterialsApplyDao.showGoodsMaterialsApplyItem(code1);
				//当发货总数量等于申请数量的时候，申请主数据状态变成2
				Integer num1=gai.getGoodsMaterialsApplyItem().getApplyNum();
				Long num=goodsMaterialsReadyDao.selectSumNum(code1);
				if(num!=null){
					int num2=Integer.valueOf(String.valueOf(num));
					if(num2!=num1){
						gai.getGoodsMaterialsApplyItem().setLackNum(num1-num2);
						flag=false;
					}
				}				
			}
			if(flag){
				sct.getGoodsMaterialsApply().setState("2");
			}
		}
		
//		String code1=sct.getGoodsMaterialsApply().getId();
//		List<GoodsMaterialsApply> listApply=goodsMaterialsApplyDao.showGoodsMaterialsApply(code1);
//		for(GoodsMaterialsApply ga:listApply){
//			ga.setState("2");
//		}
//		String code1=scp.getGoodsMaterialsApplyItem().getId();
//		List<GoodsMaterialsApplyItem> list1=goodsMaterialsApplyDao.showGoodsMaterialsApplyItem(code);
//		for(GoodsMaterialsApplyItem gai:list1){
//			gai.setState("准备中");
//		}
	}
	
}
