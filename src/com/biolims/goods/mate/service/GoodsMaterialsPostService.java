package com.biolims.goods.mate.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.common.model.user.User;
import com.biolims.goods.mate.dao.GoodsMaterialsPostDao;
import com.biolims.goods.mate.model.GoodsMaterialsApply;
import com.biolims.goods.mate.model.GoodsMaterialsApplyWay;
import com.biolims.goods.mate.model.GoodsMaterialsPost;
import com.biolims.goods.mate.model.GoodsMaterialsPostItem;
import com.biolims.goods.mate.model.GoodsMaterialsReady;
import com.biolims.goods.mate.model.GoodsMaterialsReadyItem;
import com.biolims.goods.mate.model.GoodsMaterialsSend;
import com.biolims.goods.mate.model.GoodsMaterialsWay;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.system.organize.model.ApplyOrganizeAddress;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class GoodsMaterialsPostService {
	@Resource
	private GoodsMaterialsPostDao goodsMaterialsPostDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findGoodsMaterialsPostList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return goodsMaterialsPostDao.selectGoodsMaterialsPostList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(GoodsMaterialsPost i) throws Exception {

		goodsMaterialsPostDao.saveOrUpdate(i);

	}
	public GoodsMaterialsPost get(String id) {
		GoodsMaterialsPost goodsMaterialsPost = commonDAO.get(GoodsMaterialsPost.class, id);
		return goodsMaterialsPost;
	}
//	public Map<String, Object> findGoodsMaterialsPostItemList(String scId, Integer startNum, Integer limitNum, String dir,
//			String sort) throws Exception {
//		Map<String, Object> result = goodsMaterialsPostDao.selectGoodsMaterialsPostItemList(scId, startNum, limitNum, dir, sort);
//		List<GoodsMaterialsPostItem> list = (List<GoodsMaterialsPostItem>) result.get("list");
//		return result;
//	}
	public Map<String, Object> findGoodsMaterialsPostItemList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return goodsMaterialsPostDao.selectGoodsMaterialsPostItemList(mapForQuery, startNum, limitNum, dir, sort);
	}
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void saveGoodsMaterialsPostItem(GoodsMaterialsPost sc, String itemDataJson) throws Exception {
//		List<GoodsMaterialsPostItem> saveItems = new ArrayList<GoodsMaterialsPostItem>();
//		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
//		for (Map<String, Object> map : list) {
//			GoodsMaterialsPostItem scp = new GoodsMaterialsPostItem();
//			// 将map信息读入实体类
//			scp = (GoodsMaterialsPostItem) goodsMaterialsPostDao.Map2Bean(map, scp);
//			if (scp.getId() != null && scp.getId().equals(""))
//				scp.setId(null);
//			scp.setGoodsMaterialsPost(sc);
//
//			saveItems.add(scp);
//		}
//		goodsMaterialsPostDao.saveOrUpdateAll(saveItems);
//	}
//	/**
//	 * 删除明细
//	 * @param ids
//	 * @throws Exception
//	 */
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void delGoodsMaterialsPostItem(String[] ids) throws Exception {
//		for (String id : ids) {
//			GoodsMaterialsPostItem scp =  goodsMaterialsPostDao.get(GoodsMaterialsPostItem.class, id);
//			 goodsMaterialsPostDao.delete(scp);
//		}
//	}
//	//=============2015-12-01 ly===============
//	/**
//	 * 根据ID删除
//	 * @param ids
//	 * @throws Exception
//	 */
//	@WriteOperLog
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void delGoodsMaterialsPostOne(String ids) throws Exception {
//			GoodsMaterialsPostItem scp =  goodsMaterialsPostDao.get(GoodsMaterialsPostItem.class, ids);
//			goodsMaterialsPostDao.delete(scp);
//	}
	//==================================
//	@WriteOperLogTable
//	@WriteExOperLog
//	@Transactional(rollbackFor = Exception.class)
//	public void save(GoodsMaterialsPost sc, Map jsonMap) throws Exception {
//		if (sc != null) {
//			goodsMaterialsPostDao.saveOrUpdate(sc);
//		
//			String jsonStr = "";
//			jsonStr = (String)jsonMap.get("goodsMaterialsPostItem");
//			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
//				saveGoodsMaterialsPostItem(sc, jsonStr);
//			}
//		}
//   }
//	//审批完成
//	public void changeState(String applicationTypeActionId, String id) {
//		GoodsMaterialsPost sct = goodsMaterialsPostDao.get(GoodsMaterialsPost.class, id);
//		sct.setState(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE);
//		sct.setStateName(com.biolims.workflow.WorkflowConstants.WORK_FLOW_COMPLETE_NAME);
//
//		User user = (User) ServletActionContext.getRequest().getSession()
//				.getAttribute(SystemConstants.USER_SESSION_KEY);
//		//sct.setConfirmUser(user);
//		//sct.setConfirmDate(new Date());
//		
//		goodsMaterialsPostDao.update(sct);
//		
//		sct.getGoodsMaterialsSend().setState("2");
//		
//		List<GoodsMaterialsPostItem> list=goodsMaterialsPostDao.saveToWay(sct.getId());
//		for(GoodsMaterialsPostItem gp:list){
//			GoodsMaterialsWay gm=new GoodsMaterialsWay();
//			gm.setGoodsMaterialsPost(sct);
//			gm.setCode(gp.getId());
//			gm.setChecked(gp.getChecked());
//			gm.setExpressOrder(gp.getExpressOrder());
//			gm.setAddress(gp.getAddress());
//			gm.setCompany(gp.getCompany());
//			gm.setReceiver(gp.getReceiver());
//			//gm.setExpectDate(gp.getExpectDate());
//			//gm.setRealDate(gp.getRealDate());
//			gm.setDateOfLodgment(gp.getDateOfLodgment());
//			gm.setWay(gp.getWay());
//			
//			goodsMaterialsPostDao.saveOrUpdate(gm);
//		}
//	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveGoodsMaterialsPostItemGrid(String itemDataJson) throws Exception {
		List<GoodsMaterialsPostItem> saveItems = new ArrayList<GoodsMaterialsPostItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			GoodsMaterialsPostItem sbi = new GoodsMaterialsPostItem();
			sbi = (GoodsMaterialsPostItem) goodsMaterialsPostDao.Map2Bean(map, sbi);
			if (sbi.getId() != null && sbi.getId().equals(""))
				sbi.setId(null);
			
			saveItems.add(sbi);
			if(sbi!=null && sbi.getChecked()!=null && sbi.getSubmit()!=null){
				if(sbi.getSubmit().equals("1")){
					GoodsMaterialsWay gm=new GoodsMaterialsWay();
					
					gm.setCode(sbi.getId());
					gm.setChecked(sbi.getChecked());
					gm.setExpressOrder(sbi.getExpressOrder());
					gm.setAddress(sbi.getAddress());
					gm.setCompany(sbi.getCompany());
					gm.setReceiver(sbi.getReceiver());
					gm.setDateOfLodgment(sbi.getDateOfLodgment());
					gm.setWay(sbi.getWay());
					gm.setGoodsMaterialsPostItem(sbi);
					
					goodsMaterialsPostDao.saveOrUpdate(gm);
				}
			}
		}
		goodsMaterialsPostDao.saveOrUpdateAll(saveItems);
	}
}
