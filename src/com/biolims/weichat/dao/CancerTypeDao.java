package com.biolims.weichat.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.weichat.model.WeiChatCancerType;
import com.biolims.weichat.model.WeiChatCancerTypeSeedOne;
import com.biolims.weichat.model.WeiChatCancerTypeSeedTwo;

@Repository
@SuppressWarnings("unchecked")
public class CancerTypeDao extends BaseHibernateDao {
	public String getCancerType(){
		String hql = "from WeiChatCancerType where 1=1 ";
		List<WeiChatCancerType> weiChatCancerTypes = new ArrayList<WeiChatCancerType>();
		weiChatCancerTypes = this.getSession().createQuery(hql).list();
		StringBuffer sb = new StringBuffer();
		sb.append("[");
		for(int i=0;i<weiChatCancerTypes.size();i++){
			if(weiChatCancerTypes.size()-1>i){
				sb.append("{\"id"+"\"" +":"+"\"" + weiChatCancerTypes.get(i).getId()+ "\",\"cancerTypeName"+"\"" +":"+"\"" + weiChatCancerTypes.get(i).getCancerTypeName()+"\"},");
			}else{
				sb.append("{\"id"+"\"" +":"+"\"" + weiChatCancerTypes.get(i).getId()+ "\",\"cancerTypeName"+"\"" +":"+"\"" + weiChatCancerTypes.get(i).getCancerTypeName()+"\"}");
			}
		}
		sb.append("]");
		return sb.toString();
	}
	public String getCancerTypeSeedOne(String cancerTypeId){
		String hql = "from WeiChatCancerTypeSeedOne where 1=1 and weiChatCancerType.id='" +cancerTypeId+"'" ;
		List<WeiChatCancerTypeSeedOne> weiChatCancerTypeSeedOne = new ArrayList<WeiChatCancerTypeSeedOne>();
		weiChatCancerTypeSeedOne = this.getSession().createQuery(hql).list();
		StringBuffer sb = new StringBuffer();
		for(int i=0;i<weiChatCancerTypeSeedOne.size();i++){
			if(weiChatCancerTypeSeedOne.size()-1>i){
				sb.append(weiChatCancerTypeSeedOne.get(i).getCancerTypeName()+",");
			}else{
				sb.append(weiChatCancerTypeSeedOne.get(i).getCancerTypeName());
			}
		}
		return sb.toString();
	}
	public String getCancerTypeSeedTwo(String cancerTypeId){
		String hql = "from WeiChatCancerTypeSeedTwo where 1=1 and weiChatCancerType.id='" +cancerTypeId+"'" ;
		List<WeiChatCancerTypeSeedTwo> weiChatCancerTypeSeedTwo = new ArrayList<WeiChatCancerTypeSeedTwo>();
		weiChatCancerTypeSeedTwo = this.getSession().createQuery(hql).list();
		StringBuffer sb = new StringBuffer();
		for(int i=0;i<weiChatCancerTypeSeedTwo.size();i++){
			if(weiChatCancerTypeSeedTwo.size()-1>i){
				sb.append(weiChatCancerTypeSeedTwo.get(i).getCancerTypeName()+",");
			}else{
				sb.append(weiChatCancerTypeSeedTwo.get(i).getCancerTypeName());
			}
		}
		return sb.toString();
	}
	
	public WeiChatCancerType getCancerTypeByName(String cancerTypeName){
		String hql = "from WeiChatCancerType where 1=1 and cancerTypeName='" +cancerTypeName+"'" ;
		List<WeiChatCancerType> weiChatCancerTypes = new ArrayList<WeiChatCancerType>();
		weiChatCancerTypes = this.getSession().createQuery(hql).list();
		if(!weiChatCancerTypes.isEmpty()){
			return weiChatCancerTypes.get(0);
		}else{
			return null;
		}
	}
	public WeiChatCancerTypeSeedOne getCancerTypeSeedOneByName(String cancerTypeSeedOneName){
		String hql = "from WeiChatCancerTypeSeedOne where 1=1 and cancerTypeName='" +cancerTypeSeedOneName+"'" ;
		List<WeiChatCancerTypeSeedOne> weiChatCancerTypeSeedOnes = new ArrayList<WeiChatCancerTypeSeedOne>();
		weiChatCancerTypeSeedOnes = this.getSession().createQuery(hql).list();
		if(!weiChatCancerTypeSeedOnes.isEmpty()){
			return weiChatCancerTypeSeedOnes.get(0);
		}else{
			return null;
		}
	}
	public WeiChatCancerTypeSeedTwo getCancerTypeSeedTwoByName(String cancerTypeSeedTwoName){
		String hql = "from WeiChatCancerTypeSeedTwo where 1=1 and cancerTypeName='" +cancerTypeSeedTwoName+"'" ;
		List<WeiChatCancerTypeSeedTwo> weiChatCancerTypeSeedTwos = new ArrayList<WeiChatCancerTypeSeedTwo>();
		weiChatCancerTypeSeedTwos = this.getSession().createQuery(hql).list();
		if(!weiChatCancerTypeSeedTwos.isEmpty()){
			return weiChatCancerTypeSeedTwos.get(0);
		}else{
			return null;
		}
	}
}