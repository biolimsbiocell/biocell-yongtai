package com.biolims.weichat;



import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import com.biolims.weichat.model.SampleReportFile;

@WebService
public interface IService {   
	@WebMethod
	public  String getGasData(String name);
	@WebMethod
	public  String saveInfo();
	@WebMethod
	public void getOffineOrderData(String offlineOrder);
	@WebMethod
	public void getOnlineOrderData(String offlineOrder);
	@WebMethod
	public String getOrderListData(String userId);
	@WebMethod
	public String getOrderDetailData(String id);
	@WebMethod
	public String getCancerType();
	@WebMethod
	public String saveSamplePackage(String listPackage)throws Exception;
	@WebMethod
	public String getCancerTypeSeedOne(String cancerTypeId);
	@WebMethod
	public String getCancerTypeSeedTwo(String cancerTypeId);
	@WebMethod
	public void getCourierNumber(String cancerTypeId);
	@WebMethod
	public String getExpressCompany();
	@WebMethod
	public List<SampleReportFile> getSampleReport(String id);
}
