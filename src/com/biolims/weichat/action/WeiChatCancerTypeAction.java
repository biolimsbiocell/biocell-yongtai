package com.biolims.weichat.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.weichat.model.WeiChatCancerType;
import com.biolims.weichat.model.WeiChatCancerTypeSeedOne;
import com.biolims.weichat.model.WeiChatCancerTypeSeedTwo;
import com.biolims.weichat.service.WeiChatCancerTypeService;

@Namespace("/weichat/weiChatCancerType")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class WeiChatCancerTypeAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "260109";
	@Autowired
	private WeiChatCancerTypeService weiChatCancerTypeService;
	private WeiChatCancerType weiChatCancerType = new WeiChatCancerType();
	@Resource
	private FileInfoService fileInfoService;

	@Action(value = "showWeiChatCancerTypeList")
	public String showWeiChatCancerTypeList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/weichat/weiChatCancerType.jsp");
	}

	@Action(value = "showWeiChatCancerTypeListJson")
	public void showWeiChatCancerTypeListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = weiChatCancerTypeService
				.findWeiChatCancerTypeList(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<WeiChatCancerType> list = (List<WeiChatCancerType>) result
				.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("cancerTypeName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "weiChatCancerTypeSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogWeiChatCancerTypeList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/weichat/weiChatCancerTypeDialog.jsp");
	}

	@Action(value = "showDialogWeiChatCancerTypeListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogWeiChatCancerTypeListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = weiChatCancerTypeService
				.findWeiChatCancerTypeList(map2Query, startNum, limitNum, dir,
						sort);
		Long count = (Long) result.get("total");
		List<WeiChatCancerType> list = (List<WeiChatCancerType>) result
				.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("cancerTypeName", "");
		new SendData().sendDateJson(map, list, count,
				ServletActionContext.getResponse());
	}

	@Action(value = "editWeiChatCancerType")
	public String editWeiChatCancerType() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			weiChatCancerType = weiChatCancerTypeService.get(id);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "",
					SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "weiChatCancerType");
		} else {
			User user = (User) this
					.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			// weiChatCancerType.setCreateUser(user);
			// weiChatCancerType.setCreateDate(new Date());
			// weiChatCancerType.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			// weiChatCancerType.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			putObjToContext("handlemethod",
					SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/weichat/weiChatCancerTypeEdit.jsp");
	}

	@Action(value = "copyWeiChatCancerType")
	public String copyWeiChatCancerType() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		weiChatCancerType = weiChatCancerTypeService.get(id);
		weiChatCancerType.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/weichat/weiChatCancerTypeEdit.jsp");
	}

	@Action(value = "save")
	public String save() throws Exception {
		String id = weiChatCancerType.getId();
		if (id != null && id.equals("")) {
			weiChatCancerType.setId(null);
		}
		Map aMap = new HashMap();
		aMap.put("weiChatCancerTypeSeedOne",
				getParameterFromRequest("weiChatCancerTypeSeedOneJson"));

		aMap.put("weiChatCancerTypeSeedTwo",
				getParameterFromRequest("weiChatCancerTypeSeedTwoJson"));

		weiChatCancerTypeService.save(weiChatCancerType, aMap);
		return redirect("/weichat/weiChatCancerType/editWeiChatCancerType.action?id="
				+ weiChatCancerType.getId());

	}

	@Action(value = "viewWeiChatCancerType")
	public String toViewWeiChatCancerType() throws Exception {
		String id = getParameterFromRequest("id");
		weiChatCancerType = weiChatCancerTypeService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/weichat/weiChatCancerTypeEdit.jsp");
	}

	@Action(value = "showWeiChatCancerTypeSeedOneList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWeiChatCancerTypeSeedOneList() throws Exception {
		return dispatcher("/WEB-INF/page/weichat/weiChatCancerTypeSeedOne.jsp");
	}

	@Action(value = "showWeiChatCancerTypeSeedOneListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWeiChatCancerTypeSeedOneListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = weiChatCancerTypeService
					.findWeiChatCancerTypeSeedOneList(scId, startNum, limitNum,
							dir, sort);
			Long total = (Long) result.get("total");
			List<WeiChatCancerTypeSeedOne> list = (List<WeiChatCancerTypeSeedOne>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("cancerTypeName", "");
			map.put("weiChatCancerType-id", "");
			map.put("weiChatCancerType-cancerTypeName", "");

			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWeiChatCancerTypeSeedOne")
	public void delWeiChatCancerTypeSeedOne() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			weiChatCancerTypeService.delWeiChatCancerTypeSeedOne(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	@Action(value = "showWeiChatCancerTypeSeedTwoList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showWeiChatCancerTypeSeedTwoList() throws Exception {
		return dispatcher("/WEB-INF/page/weichat/weiChatCancerTypeSeedTwo.jsp");
	}

	@Action(value = "showWeiChatCancerTypeSeedTwoListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showWeiChatCancerTypeSeedTwoListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = weiChatCancerTypeService
					.findWeiChatCancerTypeSeedTwoList(scId, startNum, limitNum,
							dir, sort);
			Long total = (Long) result.get("total");
			List<WeiChatCancerTypeSeedTwo> list = (List<WeiChatCancerTypeSeedTwo>) result
					.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("cancerTypeName", "");
			map.put("weiChatCancerType-cancerTypeName", "");
			map.put("weiChatCancerType-id", "");
			new SendData().sendDateJson(map, list, total,
					ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除明细信息
	 * 
	 * @throws Exception
	 */
	@Action(value = "delWeiChatCancerTypeSeedTwo")
	public void delWeiChatCancerTypeSeedTwo() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			weiChatCancerTypeService.delWeiChatCancerTypeSeedTwo(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public WeiChatCancerTypeService getWeiChatCancerTypeService() {
		return weiChatCancerTypeService;
	}

	public void setWeiChatCancerTypeService(
			WeiChatCancerTypeService weiChatCancerTypeService) {
		this.weiChatCancerTypeService = weiChatCancerTypeService;
	}

	public WeiChatCancerType getWeiChatCancerType() {
		return weiChatCancerType;
	}

	public void setWeiChatCancerType(WeiChatCancerType weiChatCancerType) {
		this.weiChatCancerType = weiChatCancerType;
	}

}
