package com.biolims.weichat;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.jws.WebService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;

import com.biolims.common.code.service.CodingRuleService;
import com.biolims.contract.collectionContract.action.CollectionContractAction;
import com.biolims.file.model.FileInfo;
import com.biolims.goods.sample.model.SampleInfoMain;
import com.biolims.goods.sample.service.SampleInfoMainService;
import com.biolims.sample.model.SampleOrder;
import com.biolims.sample.service.SampleOrderService;
import com.biolims.system.express.model.ExpressCompany;
import com.biolims.system.express.service.ExpressCompanyService;
import com.biolims.weichat.model.SampleReportFile;
import com.biolims.weichat.service.CancerTypeService;

@WebService
public class ServiceImpl implements IService {
	@Autowired
	private SampleOrderService sampleOrderService;
	@Autowired
	private CancerTypeService cancerTypeService;
	@Autowired
	private CodingRuleService codingRuleServices;
	@Autowired
	private SampleInfoMainService sampleInfoMainService;
	@Autowired
	private ExpressCompanyService expressCompanyService;

	@Override
	public String getGasData(String name) {
		JSONArray array = JSONArray.fromObject("[" + name + "]");
		JSONObject object = JSONObject.fromObject(array.get(0));
		System.out.println(name);
		System.out.println("INFO:Server已经启动！");
		return "Service";
	}

	@Override
	public String saveInfo() {
		CollectionContractAction coo = new CollectionContractAction();
		coo.test();
		return null;
	}

	// 保存离线订单
	@Override
	public void getOffineOrderData(String offlineOrder) {
		System.out.println("接收到的离线订单数据:" + offlineOrder);
		/*
		 * JSONArray array = JSONArray.fromObject("["+offlineOrder+"]");
		 * JSONObject object = JSONObject.fromObject(array.get(0));
		 */
		try {
			this.sampleOrderService.saveSampleOrderByJson(offlineOrder);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// 保存在线订单
	@Override
	public void getOnlineOrderData(String onlineOrder) {
		System.out.println("接收到的在线订单数据:" + onlineOrder);
		try {
			this.sampleOrderService.saveSampleOrderByJson(onlineOrder);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 订单列表
	@Override
	public String getOrderListData(String userId) {
		String sampleList = "";
		try {
			sampleList = this.sampleOrderService.outputJsonByString(
					userId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sampleList;
	}

	// 订单详情
	@Override
	public String getOrderDetailData(String id) {
		System.out.println("*********" + id);
		String sampleList = "";
		try {
			sampleList = this.sampleOrderService
					.selectSampleOrderDetail(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sampleList.toString();
	}

	// 癌种下拉列表
	@Override
	public String getCancerType() {
		return cancerTypeService.getCancerType();
	}

	// 打包
	@Override
	public String saveSamplePackage(String listPackage) throws Exception {
		// 创建一个包
		System.out.println("++++++++++++++++=" + listPackage);
		SampleInfoMain sampleInfoMain = new SampleInfoMain();
		String modelName = "SampleInfoMain";
		String markCode = "QYDB";
		String autoID = codingRuleServices.genTransID(modelName, markCode);
		sampleInfoMain.setId(autoID);
		sampleInfoMain.setState("1");
		sampleInfoMain.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_COMPLETE_NAME);
		this.sampleInfoMainService.savePackage(sampleInfoMain);
		// 循环添加订单数据
		JSONArray array = JSONArray.fromObject(listPackage);
		for (Object i : array) {
			JSONObject object = JSONObject.fromObject(i);
			System.out.println("=========================" + object + autoID);
			SampleOrder order = new SampleOrder();
			String orderID = object.getString("id");
			order = this.sampleOrderService.get(orderID);
			order.setSampleInfoMain(autoID);
			order.setReceivedDate(new Date());
			order.setState("3");
			order.setStateName(com.biolims.common.constants.SystemConstants.DIC_STATE_NEW_NAME);

			this.sampleOrderService.save(order);
		}
		return sampleInfoMain.getId();

	}

	// 快递单号
	@Override
	public void getCourierNumber(String obj) {
		JSONArray array = JSONArray.fromObject("[" + obj + "]");
		JSONObject object = JSONObject.fromObject(array.get(0));
		SampleInfoMain sampleInfoMain = new SampleInfoMain();
		sampleInfoMain = this.sampleInfoMainService.get(object
				.getString("packageId"));
		sampleInfoMain.setExpressCode(object.getString("courierNumber"));
		sampleInfoMain.setName(object.getString("descripe"));
		/*
		 * if(!("").equals(object.getString("companyId")) &&
		 * object.getString("companyId")!=null){ ExpressCompany expressCompany =
		 * new ExpressCompany(); String companyId =
		 * object.getString("companyId"); expressCompany =
		 * this.expressCompanyService.get(companyId);
		 * sampleInfoMain.setCompany(expressCompany); }
		 */
		try {
			this.sampleInfoMainService.save(sampleInfoMain);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 获取癌种子类1
	@Override
	public String getCancerTypeSeedOne(String cancerTypeId) {
		return cancerTypeService.getCancerTypeSeedOne(cancerTypeId);
	}

	// 获取癌种子类2
	@Override
	public String getCancerTypeSeedTwo(String cancerTypeId) {
		return cancerTypeService.getCancerTypeSeedTwo(cancerTypeId);
	}

	// 快递公司
	@Override
	public String getExpressCompany() {
		Map<String, Object> result = expressCompanyService
				.findExpressCompanyList(null, null, null, null, null);
		List<ExpressCompany> list = (List<ExpressCompany>) result.get("list");
		StringBuffer sb = new StringBuffer();
		sb.append("[");
		for (int i = 0; i < list.size(); i++) {
			if (list.size() - 1 > i) {
				sb.append("{\"id" + "\"" + ":" + "\"" + list.get(i).getId()
						+ "\",\"name" + "\"" + ":" + "\""
						+ list.get(i).getName() + "\"},");
			} else {
				sb.append("{\"id" + "\"" + ":" + "\"" + list.get(i).getId()
						+ "\",\"name" + "\"" + ":" + "\""
						+ list.get(i).getName() + "\"}");
			}
		}
		sb.append("]");
		return sb.toString();
	}
	//读取样本报告
	@Override
	public List<SampleReportFile> getSampleReport(String id) {
		List<SampleReportFile> list=new ArrayList<SampleReportFile>();
		List<FileInfo> fileInfo = sampleOrderService.findSampleReport(id);
		FileInputStream in = null;
		if (fileInfo != null) {
			try {
				for(int i=0;i<fileInfo.size();i++){
					SampleReportFile sampleReportFile = new SampleReportFile();
					File file = new File(fileInfo.get(i).getFilePath());
					in = new FileInputStream(file);
					byte[] b = new byte[in.available()];
					in.read(b);
					sampleReportFile.setReportId(fileInfo.get(i).getFileNote());
					sampleReportFile.setFileName(fileInfo.get(i).getFileName());
					sampleReportFile.setFileType(fileInfo.get(i).getFileType());
					sampleReportFile.setFile(b);
					sampleReportFile.setFilePath(fileInfo.get(i).getFilePath());
					list.add(sampleReportFile);
				}
			} catch (Exception e) {
			} finally {
				try {
					if (in != null) {
						in.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}
}
