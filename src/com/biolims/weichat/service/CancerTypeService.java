package com.biolims.weichat.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.weichat.dao.CancerTypeDao;
import com.biolims.weichat.model.WeiChatCancerType;
import com.biolims.weichat.model.WeiChatCancerTypeSeedOne;
import com.biolims.weichat.model.WeiChatCancerTypeSeedTwo;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class CancerTypeService {
	@Resource
	private CancerTypeDao cancerTypeDao;
	public String getCancerType(){
		return cancerTypeDao.getCancerType();
	}
	public String getCancerTypeSeedOne(String cancerTypeId){
		return cancerTypeDao.getCancerTypeSeedOne(cancerTypeId);
	}
	public String getCancerTypeSeedTwo(String cancerTypeId){
		return cancerTypeDao.getCancerTypeSeedTwo(cancerTypeId);
	}
	public WeiChatCancerType getCancerTypeByName(String cancerTypeName){
		return cancerTypeDao.getCancerTypeByName(cancerTypeName);
	}
	public WeiChatCancerTypeSeedOne getCancerTypeSeedOneByName(String cancerTypeSeedOneName){
		return cancerTypeDao.getCancerTypeSeedOneByName(cancerTypeSeedOneName);
	}
	public WeiChatCancerTypeSeedTwo getCancerTypeSeedTwoByName(String cancerTypeSeedTwoName){
		return cancerTypeDao.getCancerTypeSeedTwoByName(cancerTypeSeedTwoName);
	}
}
