package com.biolims.weichat.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.util.JsonUtils;
import com.biolims.weichat.dao.WeiChatCancerTypeDao;
import com.biolims.weichat.model.WeiChatCancerType;
import com.biolims.weichat.model.WeiChatCancerTypeSeedOne;
import com.biolims.weichat.model.WeiChatCancerTypeSeedTwo;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class WeiChatCancerTypeService {
	@Resource
	private WeiChatCancerTypeDao weiChatCancerTypeDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findWeiChatCancerTypeList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return weiChatCancerTypeDao.selectWeiChatCancerTypeList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(WeiChatCancerType i) throws Exception {

		weiChatCancerTypeDao.saveOrUpdate(i);

	}
	public WeiChatCancerType get(String id) {
		WeiChatCancerType weiChatCancerType = commonDAO.get(WeiChatCancerType.class, id);
		return weiChatCancerType;
	}
	public Map<String, Object> findWeiChatCancerTypeSeedOneList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = weiChatCancerTypeDao.selectWeiChatCancerTypeSeedOneList(scId, startNum, limitNum, dir, sort);
		List<WeiChatCancerTypeSeedOne> list = (List<WeiChatCancerTypeSeedOne>) result.get("list");
		return result;
	}
	public Map<String, Object> findWeiChatCancerTypeSeedTwoList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = weiChatCancerTypeDao.selectWeiChatCancerTypeSeedTwoList(scId, startNum, limitNum, dir, sort);
		List<WeiChatCancerTypeSeedTwo> list = (List<WeiChatCancerTypeSeedTwo>) result.get("list");
		return result;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWeiChatCancerTypeSeedOne(WeiChatCancerType sc, String itemDataJson) throws Exception {
		List<WeiChatCancerTypeSeedOne> saveItems = new ArrayList<WeiChatCancerTypeSeedOne>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			WeiChatCancerTypeSeedOne scp = new WeiChatCancerTypeSeedOne();
			// 将map信息读入实体类
			scp = (WeiChatCancerTypeSeedOne) weiChatCancerTypeDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setWeiChatCancerType(sc);

			saveItems.add(scp);
		}
		weiChatCancerTypeDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWeiChatCancerTypeSeedOne(String[] ids) throws Exception {
		for (String id : ids) {
			WeiChatCancerTypeSeedOne scp =  weiChatCancerTypeDao.get(WeiChatCancerTypeSeedOne.class, id);
			 weiChatCancerTypeDao.delete(scp);
		}
	}
	
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveWeiChatCancerTypeSeedTwo(WeiChatCancerType sc, String itemDataJson) throws Exception {
		List<WeiChatCancerTypeSeedTwo> saveItems = new ArrayList<WeiChatCancerTypeSeedTwo>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			WeiChatCancerTypeSeedTwo scp = new WeiChatCancerTypeSeedTwo();
			// 将map信息读入实体类
			scp = (WeiChatCancerTypeSeedTwo) weiChatCancerTypeDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setWeiChatCancerType(sc);

			saveItems.add(scp);
		}
		weiChatCancerTypeDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delWeiChatCancerTypeSeedTwo(String[] ids) throws Exception {
		for (String id : ids) {
			WeiChatCancerTypeSeedTwo scp =  weiChatCancerTypeDao.get(WeiChatCancerTypeSeedTwo.class, id);
			 weiChatCancerTypeDao.delete(scp);
		}
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(WeiChatCancerType sc, Map jsonMap) throws Exception {
		if (sc != null) {
			weiChatCancerTypeDao.saveOrUpdate(sc);
		
			String jsonStr = "";
			jsonStr = (String)jsonMap.get("weiChatCancerTypeSeedOne");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveWeiChatCancerTypeSeedOne(sc, jsonStr);
			}
			jsonStr = (String)jsonMap.get("weiChatCancerTypeSeedTwo");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveWeiChatCancerTypeSeedTwo(sc, jsonStr);
			}
	}
   }
}
