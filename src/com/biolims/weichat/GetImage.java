package com.biolims.weichat;

import java.io.ByteArrayOutputStream;  
import java.io.File;  
import java.io.FileOutputStream;  
import java.io.IOException;
import java.io.InputStream;  
import java.net.HttpURLConnection;  
import java.net.URL;  
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.util.ConfigFileUtil;
import com.opensymphony.xwork2.ActionContext;

public class GetImage {  
    public Map<String,String> getImage(String downloadUrl) {
        byte[] btImg = getImageFromNetByUrl(downloadUrl);  
        if(null != btImg && btImg.length > 0){  
            System.out.println("读取到：" + btImg.length + " 字节");  
            SimpleDateFormat simpleDateFormat;  
            simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");  
            Date date = new Date();  
            String fileName = simpleDateFormat.format(date)+".jpg"; 
            Map<String,String> map = new HashMap<String, String>();
            map.put("fileName", fileName);
            map.put("path", writeImageToDisk(btImg, fileName));
            return map;  
        }else{  
            System.out.println("没有从该连接获得内容");  
            return null;
        }  
    }  
    public String writeImageToDisk(byte[] img, String fileName){ 
    	Date date = new Date(); 
    	//格式化并转换String类型 
    	String path=ConfigFileUtil.getRootPath()+"\\"+new SimpleDateFormat("yyyyMMdd").format(date); 
    	//创建文件夹 
    	File f = new File(path); 
    	if(!f.exists()) 
    	f.mkdirs();
    	/*File directory = new File("");// 参数为空
        String courseFile = "";
		try {
			courseFile = directory.getCanonicalPath();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        System.out.println("=========mFilePath========="+GetImage.class.getClassLoader().getResource(""));*/
    	/*ActionContext ac = ActionContext.getContext();
    	HttpServletRequest request =(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);*/
    	/*String mFilePath = this.getRequest().getSession().getServletContext().getRealPath("");       //取得服务器路径
        System.out.println("=========mFilePath========="+mFilePath);*/
    	try {  
            File file = new File(path+"\\"+fileName);  
            FileOutputStream fops = new FileOutputStream(file);  
            fops.write(img);   
            fops.flush();  
            fops.close();  
            System.out.println("图片已经写入到磁盘");  
            return file.getPath();
        } catch (Exception e) {  
            e.printStackTrace();  
            return null;
        }  
    }  
    /** 
     * 根据地址获得数据的字节流 
     * @param strUrl 网络连接地址 
     * @return 
     */  
    public static byte[] getImageFromNetByUrl(String strUrl){  
        try {  
            URL url = new URL(strUrl);  
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();  
            conn.setRequestMethod("GET");  
            conn.setConnectTimeout(5 * 1000);  
            InputStream inStream = conn.getInputStream();//通过输入流获取图片数据  
            byte[] btImg = readInputStream(inStream);//得到图片的二进制数据  
            return btImg;  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
        return null;  
    }  
    /** 
     * 从输入流中获取数据 
     * @param inStream 输入流 
     * @return 
     * @throws Exception 
     */  
    public static byte[] readInputStream(InputStream inStream) throws Exception{  
    	ByteArrayOutputStream outStream = new ByteArrayOutputStream();  
        byte[] buffer = new byte[1024];  
        int len = 0;  
        while( (len=inStream.read(buffer)) != -1 ){  
            outStream.write(buffer, 0, len);  
        }  
        inStream.close();  
        return outStream.toByteArray();  
    }  
    public static void main(String[] args){
    	System.out.println(GetImage.class.getClassLoader().getResource(""));
    }
}   