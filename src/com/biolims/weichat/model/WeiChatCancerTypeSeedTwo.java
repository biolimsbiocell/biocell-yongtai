package com.biolims.weichat.model;

import java.lang.String;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.dao.EntityDao;

import javax.persistence.GeneratedValue;
/**   
 * @Title: Model
 * @Description: 微信-癌种-子类2
 * @author lims-platform
 * @date 2016-02-18 11:37:19
 * @version V1.0   
 *
 */
@Entity
@Table(name = "WEICHAT_CANCER_TYPE_SEED_TWO")
@SuppressWarnings("serial")
public class WeiChatCancerTypeSeedTwo extends EntityDao<WeiChatCancerTypeSeedTwo> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**癌种名称*/
	private String cancerTypeName;
	/**主表*/
	private WeiChatCancerType weiChatCancerType;
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	@Column(name = "CANCER_TYPE_SEED_TWO_NAME", length = 100)
	public String getCancerTypeName() {
		return cancerTypeName;
	}
	public void setCancerTypeName(String cancerTypeName) {
		this.cancerTypeName = cancerTypeName;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CANCER_TYPE_ID")
	public WeiChatCancerType getWeiChatCancerType() {
		return weiChatCancerType;
	}
	public void setWeiChatCancerType(WeiChatCancerType weiChatCancerType) {
		this.weiChatCancerType = weiChatCancerType;
	}
	
}