package com.biolims.weichat.model;

import java.lang.String;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

import com.biolims.dao.EntityDao;
import javax.persistence.GeneratedValue;

/**
 * @Title: Model
 * @Description: 肿瘤类型
 * @author lims-platform
 * @date 2016-02-18 11:37:19
 * @version V1.0
 * 
 */
@Entity
@Table(name = "WEICHAT_CANCER_TYPE")
@SuppressWarnings("serial")
public class WeiChatCancerType extends EntityDao<WeiChatCancerType> implements
		java.io.Serializable {
	/** 编号 */
	private String id;
	/** 癌种名称 */
	private String cancerTypeName;

	@Id
	@Column(name = "ID", length = 50)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "CANCER_TYPE_NAME", length = 100)
	public String getCancerTypeName() {
		return cancerTypeName;
	}

	public void setCancerTypeName(String cancerTypeName) {
		this.cancerTypeName = cancerTypeName;
	}

}