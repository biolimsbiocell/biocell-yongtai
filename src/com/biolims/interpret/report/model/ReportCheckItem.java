package com.biolims.interpret.report.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 报告明细
 * @author lims-platform
 * @date 2015-12-07 09:51:44
 * @version V1.0   
 *
 */
@Entity
@Table(name = "REPORTCHECK_ITEM")
@SuppressWarnings("serial")
public class ReportCheckItem extends EntityDao<ReportCheckItem> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
	/**备注*/
	private String note;
	/**样本编号*/
	private String sampleCode;
	/**关联主表*/
	private ReportCheckTask reportcheckTask;
	/**检测项目*/
	private String product;
	/**状态*/
	private String status;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本编号
	 */
	@Column(name ="SAMPLE_CODE", length = 50)
	public String getSampleCode(){
		return this.sampleCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本编号
	 */
	public void setSampleCode(String sampleCode){
		this.sampleCode = sampleCode;
	}
	/**
	 *方法: 取得ReportCheckTask
	 *@return: ReportCheckTask  关联主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "REPORTCHECK_TASK")
	public ReportCheckTask getReportcheckTask(){
		return this.reportcheckTask;
	}
	/**
	 *方法: 设置ReportCheckTask
	 *@param: ReportCheckTask  关联主表
	 */
	public void setReportcheckTask(ReportCheckTask reportcheckTask){
		this.reportcheckTask = reportcheckTask;
	}
	/**
	 *方法: 取得String
	 *@return: String  检测项目
	 */
	@Column(name ="PRODUCT", length = 50)
	public String getProduct(){
		return this.product;
	}
	/**
	 *方法: 设置String
	 *@param: String  检测项目
	 */
	public void setProduct(String product){
		this.product = product;
	}
	/**
	 *方法: 取得String
	 *@return: String  状态
	 */
	@Column(name ="STATUS", length = 50)
	public String getStatus(){
		return this.status;
	}
	/**
	 *方法: 设置String
	 *@param: String  状态
	 */
	public void setStatus(String status){
		this.status = status;
	}
}