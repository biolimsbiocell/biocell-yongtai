package com.biolims.interpret.report.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.interpret.report.dao.ReportCheckTaskDao;
import com.biolims.interpret.report.model.ReportCheckItem;
import com.biolims.interpret.report.model.ReportCheckTask;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class ReportCheckTaskService {
	@Resource
	private ReportCheckTaskDao reportCheckTaskDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findReportCheckTaskList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return reportCheckTaskDao.selectReportCheckTaskList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(ReportCheckTask i) throws Exception {

		reportCheckTaskDao.saveOrUpdate(i);

	}
	public ReportCheckTask get(String id) {
		ReportCheckTask reportCheckTask = commonDAO.get(ReportCheckTask.class, id);
		return reportCheckTask;
	}
	public Map<String, Object> findReportCheckItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = reportCheckTaskDao.selectReportCheckItemList(scId, startNum, limitNum, dir, sort);
		List<ReportCheckItem> list = (List<ReportCheckItem>) result.get("list");
		return result;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveReportCheckItem(ReportCheckTask sc, String itemDataJson) throws Exception {
		List<ReportCheckItem> saveItems = new ArrayList<ReportCheckItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			ReportCheckItem scp = new ReportCheckItem();
			// 将map信息读入实体类
			scp = (ReportCheckItem) reportCheckTaskDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setReportcheckTask(sc);

			saveItems.add(scp);
		}
		reportCheckTaskDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delReportCheckItem(String[] ids) throws Exception {
		for (String id : ids) {
			ReportCheckItem scp =  reportCheckTaskDao.get(ReportCheckItem.class, id);
			 reportCheckTaskDao.delete(scp);
		}
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(ReportCheckTask sc, Map jsonMap) throws Exception {
		if (sc != null) {
			reportCheckTaskDao.saveOrUpdate(sc);
		
			String jsonStr = "";
			jsonStr = (String)jsonMap.get("reportCheckItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveReportCheckItem(sc, jsonStr);
			}
	}
   }
}
