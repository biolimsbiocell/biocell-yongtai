package com.biolims.interpret.interpretation.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.interpret.interpretation.model.InterpretationBack;
import com.biolims.interpret.interpretation.model.InterpretationCourse;
import com.biolims.interpret.interpretation.model.InterpretationInfo;
import com.biolims.interpret.interpretation.model.InterpretationItem;
import com.biolims.interpret.interpretation.model.InterpretationTemp;
import com.biolims.interpret.interpretation.model.IpTumorTask;
import com.biolims.sample.model.SampleInfo;

@Repository
@SuppressWarnings("unchecked")
public class IpTumorTaskDao extends BaseHibernateDao {
	public Map<String, Object> selectInterpretationTumorList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		String key = " ";
		String hql = " from IpTumor where 1=1 ";
		if (mapForQuery != null)
			key = map2where(mapForQuery);
		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
		List<IpTumorTask> list = new ArrayList<IpTumorTask>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			}
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;

	}
		public Map<String, Object> selectInterpretationItemList(String scId, Integer startNum, Integer limitNum,
			String dir, String sort) throws Exception {
		String hql = "from IpTumorItem where 1=1 ";
		String key = "";
		if (scId != null)
			key = key + " and ipTumorTask.id='" + scId + "'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
		List<InterpretationItem> list = new ArrayList<InterpretationItem>();
		if (total > 0) {
			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
				if (sort.indexOf("-") != -1) {
					sort = sort.substring(0, sort.indexOf("-"));
				}
				key = key + " order by " + sort + " " + dir;
			} 
			if (startNum == null || limitNum == null) {
				list = this.getSession().createQuery(hql + key).list();
			} else {
				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
			}
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
//		public Map<String, Object> selectInterpretationCourseList(String scId, Integer startNum, Integer limitNum,
//			String dir, String sort) throws Exception {
//		String hql = "from IpTumorCourse where 1=1 ";
//		String key = "";
//		if (scId != null)
//			key = key + " and ipTumorTask.id='" + scId + "'";
//		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
//		List<IpTumorCourse> list = new ArrayList<IpTumorCourse>();
//		if (total > 0) {
//			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
//				if (sort.indexOf("-") != -1) {
//					sort = sort.substring(0, sort.indexOf("-"));
//				}
//				key = key + " order by " + sort + " " + dir;
//			} 
//			if (startNum == null || limitNum == null) {
//				list = this.getSession().createQuery(hql + key).list();
//			} else {
//				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
//			}
//		}
//		Map<String, Object> result = new HashMap<String, Object>();
//		result.put("total", total);
//		result.put("list", list);
//		return result;
//	}
//		public Map<String, Object> selectInterpretationBackList(String scId, Integer startNum, Integer limitNum,
//			String dir, String sort) throws Exception {
//		String hql = "from IpTumorBack where 1=1 ";
//		String key = "";
//		if (scId != null)
//			key = key + " and ipTumorTask.id='" + scId + "'";
//		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
//		List<IpTumorBack> list = new ArrayList<IpTumorBack>();
//		if (total > 0) {
//			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
//				if (sort.indexOf("-") != -1) {
//					sort = sort.substring(0, sort.indexOf("-"));
//				}
//				key = key + " order by " + sort + " " + dir;
//			} 
//			if (startNum == null || limitNum == null) {
//				list = this.getSession().createQuery(hql + key).list();
//			} else {
//				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
//			}
//		}
//		Map<String, Object> result = new HashMap<String, Object>();
//		result.put("total", total);
//		result.put("list", list);
//		return result;
//	}
//		public Map<String, Object> selectInterpretationInfoList(String scId, Integer startNum, Integer limitNum,
//			String dir, String sort) throws Exception {
//		String hql = "from IpTumorInfo where 1=1 ";
//		String key = "";
//		if (scId != null)
//			key = key + " and ipTumorTask.id='" + scId + "'";
//		Long total = (Long) this.getSession().createQuery("select count(*) " + hql + key).uniqueResult();
//		List<IpTumorInfo> list = new ArrayList<IpTumorInfo>();
//		if (total > 0) {
//			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
//				if (sort.indexOf("-") != -1) {
//					sort = sort.substring(0, sort.indexOf("-"));
//				}
//				key = key + " order by " + sort + " " + dir;
//			} 
//			if (startNum == null || limitNum == null) {
//				list = this.getSession().createQuery(hql + key).list();
//			} else {
//				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
//			}
//		}
//		Map<String, Object> result = new HashMap<String, Object>();
//		result.put("total", total);
//		result.put("list", list);
//		return result;
//	}
		//查询中间表
//		public Map<String, Object> selectInterpretationTempList(Map<String, String> mapForQuery, Integer startNum,
//				Integer limitNum, String dir, String sort) {
//			String key = " ";
//			String hql = " from IpTumorTaskTemp where 1=1";
//			if (mapForQuery != null)
//				key = map2where(mapForQuery);
//			Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
//			List<IpTumorTaskTemp> list = new ArrayList<IpTumorTaskTemp>();
//			if (total > 0) {
//				if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
//					if (sort.indexOf("-") != -1) {
//						sort = sort.substring(0, sort.indexOf("-"));
//					}
//					key = key + " order by " + sort + " " + dir;
//				}
//				if (startNum == null || limitNum == null) {
//					list = this.getSession().createQuery(hql + key).list();
//				} else {
//					list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
//				}
//			}
//			Map<String, Object> result = new HashMap<String, Object>();
//			result.put("total", total);
//			result.put("list", list);
//			return result;
//
//		}
	//根据样本编号查询样本详细信息
	public Map<String, Object> setSampleInfoList(String code) throws Exception {
		String hql = "from SampleInfo t where 1=1 and code='"+code+"'";
		Long total = (Long) this.getSession().createQuery("select count(*) " + hql).uniqueResult();
		List<SampleInfo> list = this.getSession().createQuery(hql).list();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", total);
		result.put("list", list);
		return result;
	}
	//查询结果表
//	public Map<String, Object> selectInterpretationInfo(Map<String, String> mapForQuery, Integer startNum,
//			Integer limitNum, String dir, String sort) {
//		String key = " ";
//		String hql = " from IpTumorInfo where 1=1 ";
//		if (mapForQuery != null)
//			key = map2where(mapForQuery);
//		Long total = (Long) this.getSession().createQuery(" select count(*) " + hql + key).uniqueResult();
//		List<IpTumorInfo> list = new ArrayList<IpTumorInfo>();
//		if (total > 0) {
//			if (dir != null && dir.length() > 0 && sort != null && sort.length() > 0) {
//				if (sort.indexOf("-") != -1) {
//					sort = sort.substring(0, sort.indexOf("-"));
//				}
//				key = key + " order by " + sort + " " + dir;
//			}
//			if (startNum == null || limitNum == null) {
//				list = this.getSession().createQuery(hql + key).list();
//			} else {
//				list = this.getSession().createQuery(hql + key).setFirstResult(startNum).setMaxResults(limitNum).list();
//			}
//		}
//		Map<String, Object> result = new HashMap<String, Object>();
//		result.put("total", total);
//		result.put("list", list);
//		return result;
//
//	}
}