﻿
package com.biolims.interpret.interpretation.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.dic.model.DicType;
import com.biolims.file.model.FileInfo;
import com.biolims.file.service.FileInfoService;
import com.biolims.interpret.interpretation.model.InterpretationBack;
import com.biolims.interpret.interpretation.model.InterpretationCourse;
import com.biolims.interpret.interpretation.model.InterpretationInfo;
import com.biolims.interpret.interpretation.model.InterpretationItem;
import com.biolims.interpret.interpretation.model.InterpretationTask;
import com.biolims.interpret.interpretation.model.InterpretationTemp;
import com.biolims.interpret.interpretation.service.InterpretationTaskService;
import com.biolims.system.work.model.WorkType;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/interpret/interpretation/interpretationTask")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class InterpretationTaskAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "270201";
	@Autowired
	private InterpretationTaskService interpretationTaskService;
	private InterpretationTask interpretationTask = new InterpretationTask();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showInterpretationTaskList")
	public String showInterpretationTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/interpret/interpretation/interpretationTask.jsp");
	}

	@Action(value = "showInterpretationTaskListJson")
	public void showInterpretationTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = interpretationTaskService.findInterpretationTaskList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<InterpretationTask> list = (List<InterpretationTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("acceptDate", "yyyy-MM-dd");
		map.put("analysisTask-id", "");
		map.put("analysisTask-name", "");
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}
	//中间表
	@Action(value = "showInterpretationTempList")
	public String showInterpretationTempList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/interpret/interpretation/interpretationTemp.jsp");
	}

	@Action(value = "showInterpretationTempListJson")
	public void showInterpretationTempListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = interpretationTaskService.findInterpretationTempList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<InterpretationTemp> list = (List<InterpretationTemp>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("sampleCode", "");
		map.put("resultTwo", "");
		map.put("cnv", "");
		map.put("readsMb", "");
		map.put("gcContent", "");
		map.put("q30Ratio", "");
		map.put("alignRatio", "");
		map.put("urRatio", "");
		map.put("result", "");
		map.put("method", "");
		map.put("note", "");
		map.put("state", "");
		//map.put("analysisInfo-name", "");
		map.put("analysisInfoId", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}
	@Action(value = "interpretationTaskSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogInterpretationTaskList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/interpret/interpretation/interpretationTaskDialog.jsp");
	}

	@Action(value = "showDialogInterpretationTaskListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogInterpretationTaskListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = interpretationTaskService.findInterpretationTaskList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<InterpretationTask> list = (List<InterpretationTask>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("acceptUser-id", "");
		map.put("acceptUser-name", "");
		map.put("acceptDate", "yyyy-MM-dd");
		map.put("analysisTask-id", "");
		map.put("analysisTask-name", "");  
		map.put("state", "");
		map.put("stateName", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editInterpretationTask")
	public String editInterpretationTask() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			interpretationTask = interpretationTaskService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "interpretationTask");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			interpretationTask.setCreateUser(user);
			interpretationTask.setState(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW);
			interpretationTask.setStateName(com.biolims.workflow.WorkflowConstants.WORKFLOW_NEW_NAME);
			interpretationTask.setCreateDate(new Date());
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/interpret/interpretation/interpretationTaskEdit.jsp");
	}

	@Action(value = "copyInterpretationTask")
	public String copyInterpretationTask() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		interpretationTask = interpretationTaskService.get(id);
		interpretationTask.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/interpret/interpretation/interpretationTaskEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = interpretationTask.getId();
		if(id!=null&&id.equals("")){
			interpretationTask.setId(null);
		}
		Map aMap = new HashMap();
			aMap.put("interpretationItem",getParameterFromRequest("interpretationItemJson"));
		
			aMap.put("interpretationCourse",getParameterFromRequest("interpretationCourseJson"));
		
			aMap.put("interpretationBack",getParameterFromRequest("interpretationBackJson"));
		
			//aMap.put("interpretationInfo",getParameterFromRequest("interpretationInfoJson"));
		
		interpretationTaskService.save(interpretationTask,aMap);
		return redirect("/interpret/interpretation/interpretationTask/editInterpretationTask.action?id=" + interpretationTask.getId());

	}

	@Action(value = "viewInterpretationTask")
	public String toViewInterpretationTask() throws Exception {
		String id = getParameterFromRequest("id");
		interpretationTask = interpretationTaskService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/interpret/interpretation/interpretationTaskEdit.jsp");
	}
	

	@Action(value = "showInterpretationItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showInterpretationItemList() throws Exception {
		return dispatcher("/WEB-INF/page/interpret/interpretation/interpretationItem.jsp");
	}

	@Action(value = "showInterpretationItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showInterpretationItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = interpretationTaskService.findInterpretationItemList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<InterpretationItem> list = (List<InterpretationItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("sampleCode", "");
			map.put("isgood", "");
			map.put("method-id", "");
			map.put("method-name", "");
			map.put("note", "");
			map.put("testNum", "");
			map.put("sampleType", "");
			map.put("wkCode", "");
			map.put("sampleReceiveDate", "yyyy-MM-dd");
			map.put("reportStopDate", "yyyy-MM-dd");
			map.put("onTime", "yyyy-MM-dd");
			map.put("perOffTime", "yyyy-MM-dd");
			map.put("acceptCnvDate", "yyyy-MM-dd");
			map.put("readEndDate", "yyyy-MM-dd");
			map.put("acceptImgDate", "yyyy-MM-dd");
			map.put("reportSendDate", "yyyy-MM-dd");
			map.put("abnormalSample", "");
			map.put("reportState", "");
			map.put("reportPostDate", "yyyy-MM-dd");
			map.put("patientName", "");
			map.put("idCard", "");
			map.put("businessType-id", "");
			map.put("businessType-name", "");
			map.put("hospital", "");
			map.put("payType-id", "");
			map.put("payType-name", "");
			map.put("productId", "");
			map.put("productName", "");
			map.put("sampleTime", "");
			map.put("spellName", "");
			map.put("gender", "");
			map.put("age", "");
			map.put("birthAddress", "");
			map.put("nation", "");
			map.put("hight", "");
			map.put("weight", "");
			map.put("marriage", "");
			map.put("cardType", "");
			map.put("cardNumber", "");
			map.put("tele", "");
			map.put("email", "");
			map.put("familyAddress", "");
			map.put("isFee", "");
			map.put("privilege", "");
			map.put("referUser", "");
			map.put("billNumber", "");
			map.put("billTitle", "");
			map.put("inputUser", "");
			map.put("confirmUser1", "");
			map.put("confirmUser2", "");
			map.put("receiveDate", "");
			map.put("area", "");
			map.put("fee", "");
			map.put("medicalCard", "");
			map.put("inspectDate", "");
			map.put("reportDate", "");
			map.put("files-id", "");
			//map.put("files-name", "");
			map.put("interpretationTask-name", "");
			map.put("interpretationTask-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delInterpretationItem")
	public void delInterpretationItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			interpretationTaskService.delInterpretationItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	

	@Action(value = "showInterpretationCourseList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showInterpretationCourseList() throws Exception {
		return dispatcher("/WEB-INF/page/interpret/interpretation/interpretationCourse.jsp");
	}

	@Action(value = "showInterpretationCourseListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showInterpretationCourseListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = interpretationTaskService.findInterpretationCourseList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<InterpretationCourse> list = (List<InterpretationCourse>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("name", "");
			map.put("sampleCode", "");
			map.put("karyotype", "");
			map.put("chrSite", "");
			map.put("area", "");
//			map.put("size", "");
			//////////////////////////////////
			map.put("resultOne", "");
			map.put("resultTwo", "");
			map.put("cnv", "");
			map.put("readsMb", "");
			map.put("gcContent", "");
			map.put("q30Ratio", "");
			map.put("alignRatio", "");
			map.put("urRatio", "");
			map.put("result", "");
			//////////////////////////////////
			map.put("tempId", "");
			map.put("gainLoss", "");
			map.put("syndrome", "");
			map.put("gene", "");
			map.put("dgv", "");
			map.put("omim", "");
			map.put("combined", "");
			map.put("isgood", "");
			map.put("method", "");
			map.put("readingResult", "");
			map.put("interpretationTask-name", "");
			map.put("interpretationTask-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delInterpretationCourse")
	public void delInterpretationCourse() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			interpretationTaskService.delInterpretationCourse(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	

//	@Action(value = "showInterpretationBackList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showInterpretationBackList() throws Exception {
//		return dispatcher("/WEB-INF/page/interpret/interpretation/interpretationBack.jsp");
//	}
//
//	@Action(value = "showInterpretationBackListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showInterpretationBackListJson() throws Exception {
//		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//		try {
//			String scId = getRequest().getParameter("id");
//			Map<String, Object> result = interpretationTaskService.findInterpretationBackList(scId, startNum, limitNum, dir,
//					sort);
//			Long total = (Long) result.get("total");
//			List<InterpretationBack> list = (List<InterpretationBack>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//
//			map.put("id", "");
//			map.put("name", "");
//			map.put("sampleCode", "");
//			map.put("cnv", "");
//			map.put("result1", "");
//			map.put("result2", "");
//			map.put("readingResult", "");
//			map.put("upload", "");
//			map.put("isgood", "");
//			map.put("method", "");
//			map.put("upLoadAccessory-fileName", "");
//			map.put("upLoadAccessory-id", "");
//			map.put("interpretationTask-name", "");
//			map.put("interpretationTask-id", "");
//			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//		/**
//	 * 删除明细信息
//	 * @throws Exception
//	 */
//	@Action(value = "delInterpretationBack")
//	public void delInterpretationBack() throws Exception {
//		Map<String, Object> map = new HashMap<String, Object>();
//		try {
//			String[] ids = getRequest().getParameterValues("ids[]");
//			interpretationTaskService.delInterpretationBack(ids);
//			map.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			map.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
//	

//	@Action(value = "showInterpretationInfoList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showInterpretationInfoList() throws Exception {
//		return dispatcher("/WEB-INF/page/interpret/interpretation/interpretationInfo.jsp");
//	}
//
//	@Action(value = "showInterpretationInfoListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showInterpretationInfoListJson() throws Exception {
//		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//		try {
//			String scId = getRequest().getParameter("id");
//			Map<String, Object> result = interpretationTaskService.findInterpretationInfoList(scId, startNum, limitNum, dir,
//					sort);
//			Long total = (Long) result.get("total");
//			List<InterpretationInfo> list = (List<InterpretationInfo>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("cnv", "");
//			map.put("readingResult", "");
//			map.put("result", "");
//			map.put("name", "");
//			map.put("code", "");
//			map.put("method", "");
//			map.put("note", "");
//			map.put("sampleCode", "");
//			map.put("interpretationTask-name", "");
//			map.put("interpretationTask-id", "");
//			map.put("result1", "");
//			map.put("result2", "");
//			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//		/**
//	 * 删除明细信息
//	 * @throws Exception
//	 */
//	@Action(value = "delInterpretationInfo")
//	public void delInterpretationInfo() throws Exception {
//		Map<String, Object> map = new HashMap<String, Object>();
//		try {
//			String[] ids = getRequest().getParameterValues("ids[]");
//			interpretationTaskService.delInterpretationInfo(ids);
//			map.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			map.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public InterpretationTaskService getInterpretationTaskService() {
		return interpretationTaskService;
	}

	public void setInterpretationTaskService(InterpretationTaskService interpretationTaskService) {
		this.interpretationTaskService = interpretationTaskService;
	}

	public InterpretationTask getInterpretationTask() {
		return interpretationTask;
	}

	public void setInterpretationTask(InterpretationTask interpretationTask) {
		this.interpretationTask = interpretationTask;
	}

	@Action(value = "setSampleInfoList")
	public void setSampleInfoList() throws Exception {
		String code = getParameterFromRequest("code");
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			List<Map<String, String>> dataListMap = this.interpretationTaskService.showSampleInfoList(code);
			result.put("success", true);
			result.put("data", dataListMap);
		} catch (Exception e) {
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	//保存样本页面
	@Action(value = "saveInterpretationItem")
	public void saveInterpretationItem() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getParameterFromRequest("itemDataJson");
			interpretationTaskService.saveItem(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	//保存解读过程
	@Action(value = "saveInterpretationCourse")
	public void saveInterpretationCourse() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getParameterFromRequest("itemDataJson");
			interpretationTaskService.saveCourse(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
	//保存作图反馈
	@Action(value = "saveInterpretationBack")
	public void saveInterpretationBack() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getParameterFromRequest("itemDataJson");
			interpretationTaskService.saveBack(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
//	//保存解读结果
//	@Action(value = "saveInterpretationInfo")
//	public void saveInterpretationInfo() throws Exception {
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			String itemDataJson = getParameterFromRequest("itemDataJson");
//			interpretationTaskService.saveInfo(itemDataJson);
//			result.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}
}
