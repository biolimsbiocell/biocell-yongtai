﻿
package com.biolims.interpret.interpretation.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.dic.model.DicType;
import com.biolims.file.model.FileInfo;
import com.biolims.file.service.FileInfoService;
import com.biolims.interpret.interpretation.model.InterpretationBack;
import com.biolims.interpret.interpretation.model.InterpretationCourse;
import com.biolims.interpret.interpretation.model.InterpretationInfo;
import com.biolims.interpret.interpretation.model.InterpretationItem;
import com.biolims.interpret.interpretation.model.InterpretationTask;
import com.biolims.interpret.interpretation.model.InterpretationTemp;
import com.biolims.interpret.interpretation.service.InterpretationTaskService;
import com.biolims.system.work.model.WorkType;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
@Namespace("/interpret/interpretation/interpretationInfo")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class InterpretationInfoAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "270202";
	@Autowired
	private InterpretationTaskService interpretationTaskService;
	private InterpretationTask interpretationTask = new InterpretationTask();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showInterpretationInfoList")
	public String showInterpretationInfoList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/interpret/interpretation/interpretationInfo.jsp");
	}

	@Action(value = "showInterpretationInfoListJson")
	public void showInterpretationInfoListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = interpretationTaskService.findInterpretationInfo(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<InterpretationInfo> list = (List<InterpretationInfo>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("cnv", "");
		map.put("readingResult", "");
		map.put("result", "");
		map.put("name", "");
		map.put("code", "");
		map.put("method", "");
		map.put("note", "");
		map.put("sampleCode", "");
		map.put("interpretationTask-name", "");
		map.put("interpretationTask-id", "");
		map.put("result1", "");
		map.put("result2", "");
		map.put("upLoadAccessory-fileName", "");
		map.put("upLoadAccessory-id", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}


//	@Action(value = "showInterpretationInfoList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public String showInterpretationInfoList() throws Exception {
//		return dispatcher("/WEB-INF/page/interpret/interpretation/interpretationInfo.jsp");
//	}
//
//	@Action(value = "showInterpretationInfoListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
//	public void showInterpretationInfoListJson() throws Exception {
//		// 开始记录数
//		int startNum = Integer.parseInt(getParameterFromRequest("start"));
//		// limit
//		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
//		// 字段
//		String dir = getParameterFromRequest("dir");
//		// 排序方式
//		String sort = getParameterFromRequest("sort");
//		try {
//			String scId = getRequest().getParameter("id");
//			Map<String, Object> result = interpretationTaskService.findInterpretationInfoList(scId, startNum, limitNum, dir,
//					sort);
//			Long total = (Long) result.get("total");
//			List<InterpretationInfo> list = (List<InterpretationInfo>) result.get("list");
//			Map<String, String> map = new HashMap<String, String>();
//			map.put("id", "");
//			map.put("cnv", "");
//			map.put("readingResult", "");
//			map.put("result", "");
//			map.put("name", "");
//			map.put("code", "");
//			map.put("method", "");
//			map.put("note", "");
//			map.put("sampleCode", "");
//			map.put("interpretationTask-name", "");
//			map.put("interpretationTask-id", "");
//			map.put("result1", "");
//			map.put("result2", "");
//			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//		/**
//	 * 删除明细信息
//	 * @throws Exception
//	 */
//	@Action(value = "delInterpretationInfo")
//	public void delInterpretationInfo() throws Exception {
//		Map<String, Object> map = new HashMap<String, Object>();
//		try {
//			String[] ids = getRequest().getParameterValues("ids[]");
//			interpretationTaskService.delInterpretationInfo(ids);
//			map.put("success", true);
//		} catch (Exception e) {
//			e.printStackTrace();
//			map.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(map));
//	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public InterpretationTaskService getInterpretationTaskService() {
		return interpretationTaskService;
	}

	public void setInterpretationTaskService(InterpretationTaskService interpretationTaskService) {
		this.interpretationTaskService = interpretationTaskService;
	}

	public InterpretationTask getInterpretationTask() {
		return interpretationTask;
	}

	public void setInterpretationTask(InterpretationTask interpretationTask) {
		this.interpretationTask = interpretationTask;
	}

//	@Action(value = "setSampleInfoList")
//	public void setSampleInfoList() throws Exception {
//		String code = getParameterFromRequest("code");
//		Map<String, Object> result = new HashMap<String, Object>();
//		try {
//			List<Map<String, String>> dataListMap = this.interpretationTaskService.showSampleInfoList(code);
//			result.put("success", true);
//			result.put("data", dataListMap);
//		} catch (Exception e) {
//			result.put("success", false);
//		}
//		HttpUtils.write(JsonUtils.toJsonString(result));
//	}
	
	//保存解读结果
	@Action(value = "saveInterpretationInfo")
	public void saveInterpretationInfo() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String itemDataJson = getParameterFromRequest("itemDataJson");
			interpretationTaskService.saveInfo(itemDataJson);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}
}
