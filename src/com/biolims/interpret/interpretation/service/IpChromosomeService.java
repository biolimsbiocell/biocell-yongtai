package com.biolims.interpret.interpretation.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.analysis.analy.model.AnalysisManager;
import com.biolims.common.SystemConstants;
import com.biolims.common.dao.CommonDAO;
import com.biolims.goods.sample.dao.SampleInfoMainDao;
import com.biolims.interpret.interpretation.dao.IpChromosomeDao;
import com.biolims.interpret.interpretation.model.InterpretationBack;
import com.biolims.interpret.interpretation.model.IpChromosome;
import com.biolims.interpret.interpretation.model.InterpretationCourse;
import com.biolims.interpret.interpretation.model.InterpretationInfo;
import com.biolims.interpret.interpretation.model.InterpretationItem;
import com.biolims.interpret.interpretation.model.InterpretationTemp;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.sample.model.SampleInfo;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class IpChromosomeService {
	@Resource
	private IpChromosomeDao interpretationChromosomeDao;
	@Resource
	private CommonDAO commonDAO;
	@Resource
	private SampleInfoMainDao sampleInfoMainDao;

	StringBuffer json = new StringBuffer();

	public Map<String, Object> findInterpretationChromosomeList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return interpretationChromosomeDao.selectInterpretationChromosomeList(
				mapForQuery, startNum, limitNum, dir, sort);
	}

	// 查询中间表
	public Map<String, Object> findInterpretationTempList(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return interpretationChromosomeDao.selectInterpretationTempList(
				mapForQuery, startNum, limitNum, dir, sort);
	}

	// 查询info
	public Map<String, Object> findInterpretationInfo(
			Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return interpretationChromosomeDao.selectInterpretationInfo(
				mapForQuery, startNum, limitNum, dir, sort);
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(IpChromosome i) throws Exception {

		interpretationChromosomeDao.saveOrUpdate(i);

	}

	public IpChromosome get(String id) {
		IpChromosome interpretationChromosome = commonDAO.get(
				IpChromosome.class, id);
		return interpretationChromosome;
	}

	public Map<String, Object> findInterpretationItemList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = interpretationChromosomeDao
				.selectInterpretationItemList(scId, startNum, limitNum, dir,
						sort);
		List<InterpretationItem> list = (List<InterpretationItem>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findInterpretationCourseList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = interpretationChromosomeDao
				.selectInterpretationCourseList(scId, startNum, limitNum, dir,
						sort);
		List<InterpretationCourse> list = (List<InterpretationCourse>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findInterpretationBackList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = interpretationChromosomeDao
				.selectInterpretationBackList(scId, startNum, limitNum, dir,
						sort);
		List<InterpretationBack> list = (List<InterpretationBack>) result
				.get("list");
		return result;
	}

	public Map<String, Object> findInterpretationInfoList(String scId,
			Integer startNum, Integer limitNum, String dir, String sort)
			throws Exception {
		Map<String, Object> result = interpretationChromosomeDao
				.selectInterpretationInfoList(scId, startNum, limitNum, dir,
						sort);
		List<InterpretationInfo> list = (List<InterpretationInfo>) result
				.get("list");
		return result;
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveInterpretationItem(IpChromosome sc, String itemDataJson)
			throws Exception {
		List<InterpretationItem> saveItems = new ArrayList<InterpretationItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			InterpretationItem scp = new InterpretationItem();
			// 将map信息读入实体类
			scp = (InterpretationItem) interpretationChromosomeDao.Map2Bean(
					map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			// scp.setInterpretationChromosome(sc);

			saveItems.add(scp);
		}
		interpretationChromosomeDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delInterpretationItem(String[] ids) throws Exception {
		for (String id : ids) {
			InterpretationItem scp = interpretationChromosomeDao.get(
					InterpretationItem.class, id);
			interpretationChromosomeDao.delete(scp);
		}
	}

	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveInterpretationCourse(IpChromosome sc, String itemDataJson)
			throws Exception {
		List<InterpretationCourse> saveItems = new ArrayList<InterpretationCourse>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			InterpretationCourse scp = new InterpretationCourse();
			// 将map信息读入实体类
			scp = (InterpretationCourse) interpretationChromosomeDao.Map2Bean(
					map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			// scp.setInterpretationChromosome(sc);

			saveItems.add(scp);
			// 保存解读过程，合格的到解读结果表，并改变中间表数据的状态
			if (scp != null && scp.getIsgood() != null) {
				if (scp.getIsgood().equals("1")) {
					// 改变中间表数据的状态
					InterpretationTemp it = commonDAO.get(
							InterpretationTemp.class, scp.getTempId());
					it.setState("1");
					// 合格的到解读结果表
					// InterpretationInfo tf=new InterpretationInfo();
					// tf.setCode(scp.getId());
					// tf.setSampleCode(scp.getSampleCode());
					// tf.setInterpretationChromosome(sc);
					// tf.setCnv(scp.getCnv());
					// tf.setResult1(scp.getResultOne());
					// tf.setResult2(scp.getResultTwo());
					// tf.setReadingResult(scp.getReadingResult());
					// commonDAO.saveOrUpdate(tf);
					AnalysisManager am = new AnalysisManager();
					am.setCnv(scp.getCnv());
					am.setResult1(scp.getResultOne());
					am.setResult2(scp.getResultTwo());
					am.setSampleCode(scp.getSampleCode());
					am.setReadingResult(scp.getReadingResult());
					// am.setInterpretationChromosome(sc);

					commonDAO.saveOrUpdate(am);

					// {解读过程合格，改变SampleInfo中原始样本的状态为“待数据解读”
					SampleInfo sf = sampleInfoMainDao.findSampleInfo(scp
							.getSampleCode());
					sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_GDP_NEW);
					sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_GDP_NEW_NAME);
					// ---------------------------------}
				}
			}
		}
		interpretationChromosomeDao.saveOrUpdateAll(saveItems);
	}

	/**
	 * 删除明细
	 * 
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delInterpretationCourse(String[] ids) throws Exception {
		for (String id : ids) {
			InterpretationCourse scp = interpretationChromosomeDao.get(
					InterpretationCourse.class, id);
			interpretationChromosomeDao.delete(scp);
		}
	}

	// @WriteOperLog
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void saveInterpretationBack(InterpretationChromosome sc, String
	// itemDataJson) throws Exception {
	// List<InterpretationBack> saveItems = new ArrayList<InterpretationBack>();
	// List<Map<String, Object>> list =
	// JsonUtils.toListByJsonArray(itemDataJson, List.class);
	// for (Map<String, Object> map : list) {
	// InterpretationBack scp = new InterpretationBack();
	// // 将map信息读入实体类
	// scp = (InterpretationBack) interpretationChromosomeDao.Map2Bean(map,
	// scp);
	// if (scp.getId() != null && scp.getId().equals(""))
	// scp.setId(null);
	// scp.setInterpretationChromosome(sc);
	//
	// saveItems.add(scp);
	// }
	// interpretationChromosomeDao.saveOrUpdateAll(saveItems);
	// }
	// /**
	// * 删除明细
	// * @param ids
	// * @throws Exception
	// */
	// @WriteOperLog
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void delInterpretationBack(String[] ids) throws Exception {
	// for (String id : ids) {
	// InterpretationBack scp =
	// interpretationChromosomeDao.get(InterpretationBack.class, id);
	// interpretationChromosomeDao.delete(scp);
	// }
	// }
	//
	// @WriteOperLog
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void saveInterpretationInfo(InterpretationChromosome sc, String
	// itemDataJson) throws Exception {
	// List<InterpretationInfo> saveItems = new ArrayList<InterpretationInfo>();
	// List<Map<String, Object>> list =
	// JsonUtils.toListByJsonArray(itemDataJson, List.class);
	// for (Map<String, Object> map : list) {
	// InterpretationInfo scp = new InterpretationInfo();
	// // 将map信息读入实体类
	// scp = (InterpretationInfo) interpretationChromosomeDao.Map2Bean(map,
	// scp);
	// if (scp.getId() != null && scp.getId().equals(""))
	// scp.setId(null);
	// scp.setInterpretationChromosome(sc);
	//
	// saveItems.add(scp);
	// }
	// interpretationChromosomeDao.saveOrUpdateAll(saveItems);
	// }
	// /**
	// * 删除明细
	// * @param ids
	// * @throws Exception
	// */
	// @WriteOperLog
	// @WriteExOperLog
	// @Transactional(rollbackFor = Exception.class)
	// public void delInterpretationInfo(String[] ids) throws Exception {
	// for (String id : ids) {
	// InterpretationInfo scp =
	// interpretationChromosomeDao.get(InterpretationInfo.class, id);
	// interpretationChromosomeDao.delete(scp);
	// }
	// }

	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(IpChromosome sc, Map jsonMap) throws Exception {
		if (sc != null) {
			interpretationChromosomeDao.saveOrUpdate(sc);

			String jsonStr = "";
			jsonStr = (String) jsonMap.get("interpretationItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveInterpretationItem(sc, jsonStr);
			}
			jsonStr = (String) jsonMap.get("interpretationCourse");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveInterpretationCourse(sc, jsonStr);
			}
			// jsonStr = (String)jsonMap.get("interpretationBack");
			// if (jsonStr != null && !jsonStr.equals("{}") &&
			// !jsonStr.equals("")) {
			// saveInterpretationBack(sc, jsonStr);
			// }
			// jsonStr = (String)jsonMap.get("interpretationInfo");
			// if (jsonStr != null && !jsonStr.equals("{}") &&
			// !jsonStr.equals("")) {
			// saveInterpretationInfo(sc, jsonStr);
			// }
		}
	}

	public List<Map<String, String>> showSampleInfoList(String code)
			throws Exception {
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		Map<String, Object> result = interpretationChromosomeDao
				.setSampleInfoList(code);
		List<SampleInfo> list = (List<SampleInfo>) result.get("list");

		if (list != null && list.size() > 0) {
			for (SampleInfo ai : list) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("id", ai.getId());
				map.put("sAge", ai.getAge());
				map.put("sArea", ai.getArea());
				map.put("sBillNumber", ai.getBillNumber());
				map.put("sBillTitle", ai.getBillTitle());
				map.put("sBirthAddress", ai.getBirthAddress());
				map.put("sCardNumber", ai.getCardNumber());
				map.put("sCardType", ai.getCardType());
				map.put("sCode", ai.getCode());
				map.put("sConfirmUser1", ai.getConfirmUser1());
				map.put("sConfirmUser2", ai.getConfirmUser2());
				map.put("sEmail", ai.getEmail());
				map.put("sFamilyAddress", ai.getFamilyAddress());
				map.put("sFileNum", ai.getFileNum());
				map.put("sGender", ai.getGender());
				map.put("sHospital", ai.getHospital());
				map.put("sIdCard", ai.getIdCard());
				map.put("sInputUser", ai.getInputUser());
				map.put("sInspectDate", ai.getInspectDate());
				map.put("sIsFee", ai.getIsFee());
				map.put("sMarriage", ai.getMarriage());
				map.put("sMedicalCard", ai.getMedicalCard());
				map.put("sName", ai.getName());
				map.put("sNation", ai.getNation());
				map.put("sPatientName", ai.getPatientName());
				map.put("sPhone", ai.getPhone());
				map.put("sPrivilege", ai.getPrivilege());
				map.put("sProductId", ai.getProductId());
				map.put("sProductName", ai.getProductName());
				if (ai.getPrice() != null) {
					map.put("sPrice", ai.getPrice().toString());
				} else {
					map.put("sPrice", "");
				}

				// map.put("sReceiveDate",ai.getReceiveDate());
				map.put("sReferUser", ai.getReferUser());
				map.put("sSampleTime", ai.getSampleTime());
				map.put("sSpellName", ai.getSpellName());
				if (ai.getSampleType() != null) {
					map.put("sampleTypeId", ai.getSampleType().getId());
					map.put("sampleType", ai.getSampleType().getName());
				} else {
					map.put("sampleTypeId", "");
					map.put("sampleType", "");
				}

				map.put("sReportDate", ai.getReportDate());
				map.put("sTele", ai.getTele());
				if (ai.getBusinessType() != null) {
					map.put("businessTypeId", ai.getBusinessType().getId());
					map.put("businessType", ai.getBusinessType().getName());
				} else {
					map.put("businessTypeId", "");
					map.put("businessType", "");
				}
				if (ai.getFee() != null) {
					map.put("sFee", ai.getFee().toString());
				} else {
					map.put("sFee", "");
				}
				if (ai.getHight() != null) {
					map.put("sHight", ai.getHight().toString());
				} else {
					map.put("sHight", "");
				}
				if (ai.getType() != null) {
					map.put("typeId", ai.getType().getId());
					map.put("typeName", ai.getType().getName());
				} else {
					map.put("typeId", "");
					map.put("typeName", "");
				}
				if (ai.getWeight() != null) {
					map.put("sWeight", ai.getWeight().toString());
				} else {
					map.put("sWeight", "");
				}
				if (ai.getUpLoadAccessory() != null) {
					map.put("upId", ai.getUpLoadAccessory().getId());
					map.put("upName", ai.getUpLoadAccessory().getFileName());
				} else {
					map.put("upId", "");
					map.put("upName", "");
				}

				mapList.add(map);
			}
		}
		return mapList;
	}

	// 保存样本明细
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveItem(String itemDataJson) throws Exception {
		List<InterpretationItem> saveItems = new ArrayList<InterpretationItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			InterpretationItem sbi = new InterpretationItem();
			sbi = (InterpretationItem) interpretationChromosomeDao.Map2Bean(
					map, sbi);
			if (sbi.getId() != null && sbi.getId().equals(""))
				sbi.setId(null);

			saveItems.add(sbi);
		}
		interpretationChromosomeDao.saveOrUpdateAll(saveItems);
	}

	// 保存解读过程
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveCourse(String itemDataJson) throws Exception {
		List<InterpretationCourse> saveItems = new ArrayList<InterpretationCourse>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			InterpretationCourse sbi = new InterpretationCourse();
			sbi = (InterpretationCourse) interpretationChromosomeDao.Map2Bean(
					map, sbi);
			if (sbi.getId() != null && sbi.getId().equals(""))
				sbi.setId(null);

			saveItems.add(sbi);
		}
		interpretationChromosomeDao.saveOrUpdateAll(saveItems);
	}

	// 保存反馈作图
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveBack(String itemDataJson) throws Exception {
		List<InterpretationBack> saveItems = new ArrayList<InterpretationBack>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			InterpretationBack sbi = new InterpretationBack();
			sbi = (InterpretationBack) interpretationChromosomeDao.Map2Bean(
					map, sbi);
			if (sbi.getId() != null && sbi.getId().equals(""))
				sbi.setId(null);

			saveItems.add(sbi);
		}
		interpretationChromosomeDao.saveOrUpdateAll(saveItems);
	}

	// 保存解读结果
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveInfo(String itemDataJson) throws Exception {
		List<InterpretationInfo> saveItems = new ArrayList<InterpretationInfo>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(
				itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			InterpretationInfo sbi = new InterpretationInfo();
			sbi = (InterpretationInfo) interpretationChromosomeDao.Map2Bean(
					map, sbi);
			if (sbi.getId() != null && sbi.getId().equals(""))
				sbi.setId(null);

			saveItems.add(sbi);
			// 根据处理方式，决定数据流向
			if (sbi != null && sbi.getMethod() != null) {
				if (sbi.getMethod().equals("0")) {

					// {样本解读通过，改变SampleInfo中原始样本的状态为“完成数据解读”
					SampleInfo sf = sampleInfoMainDao.findSampleInfo(sbi
							.getSampleCode());
					sf.setState(com.biolims.workflow.WorkflowConstants.SAMPLE_GDP_COMPLETE);
					sf.setStateName(com.biolims.workflow.WorkflowConstants.SAMPLE_GDP_COMPLETE_NAME);
					// ---------------------------------}
				} else if (sbi.getMethod().equals("1")) {
					// AnalysisManager am=new AnalysisManager();
					// am.setCnv(sbi.getCnv());
					// am.setResult1(sbi.getResult1());
					// am.setResult2(sbi.getResult2());
					// am.setSampleCode(sbi.getSampleCode());
					// am.setReadingResult(sbi.getReadingResult());
					// am.setInterpretationChromosome(sbi.getInterpretationChromosome());
					//
					// interpretationChromosomeDao.saveOrUpdate(am);
				}
			}
		}
		interpretationChromosomeDao.saveOrUpdateAll(saveItems);
	}
}
