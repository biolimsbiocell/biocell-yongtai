package com.biolims.interpret.interpretation.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 数据解读左侧temp表
 * @author lims-platform
 * @date 2015-12-07 09:38:03
 * @version V1.0   
 *
 */
@Entity
@Table(name = "INTERPRETATION_TEMP")
@SuppressWarnings("serial")
public class InterpretationTemp extends EntityDao<InterpretationTemp> implements java.io.Serializable {
	/**编码*/
	private String id;
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	/**result1*/
	private String resultOne;
	/**result2*/
	private String resultTwo;
	/**cnv*/
	private String cnv;
	/**reads_mb*/
	private String readsMb;
	/**gc_content*/
	private String gcContent;
	/**q30_ratio*/
	private String q30Ratio;
	/**align_ratio*/
	private String alignRatio;
	/**ur_ratio*/
	private String urRatio;
	/**结果*/
	private String result;
	/**处理方式*/
	private String method;
	/**备注*/
	private String note;
	/**状态*/
	private String state;
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	/**关联表Id*/
	private String  analysisInfoId;
	//样本编号
	private String sampleCode;
	
	public String getSampleCode() {
		return sampleCode;
	}
	public void setSampleCode(String sampleCode) {
		this.sampleCode = sampleCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  result1
	 */
	public String getResultOne(){
		return this.resultOne;
	}
	/**
	 *方法: 设置String
	 *@param: String  result1
	 */
	public void setResultOne(String resultOne){
		this.resultOne = resultOne;
	}
	/**
	 *方法: 取得String
	 *@return: String  result2
	 */
	@Column(name ="RESULT_TWO", length = 50)
	public String getResultTwo(){
		return this.resultTwo;
	}
	/**
	 *方法: 设置String
	 *@param: String  result2
	 */
	public void setResultTwo(String resultTwo){
		this.resultTwo = resultTwo;
	}
	/**
	 *方法: 取得String
	 *@return: String  cnv
	 */
	@Column(name ="CNV", length = 50)
	public String getCnv(){
		return this.cnv;
	}
	/**
	 *方法: 设置String
	 *@param: String  cnv
	 */
	public void setCnv(String cnv){
		this.cnv = cnv;
	}
	/**
	 *方法: 取得String
	 *@return: String  reads_mb
	 */
	@Column(name ="READS_MB", length = 50)
	public String getReadsMb(){
		return this.readsMb;
	}
	/**
	 *方法: 设置String
	 *@param: String  reads_mb
	 */
	public void setReadsMb(String readsMb){
		this.readsMb = readsMb;
	}
	/**
	 *方法: 取得String
	 *@return: String  gc_content
	 */
	@Column(name ="GC_CONTENT", length = 50)
	public String getGcContent(){
		return this.gcContent;
	}
	/**
	 *方法: 设置String
	 *@param: String  gc_content
	 */
	public void setGcContent(String gcContent){
		this.gcContent = gcContent;
	}
	/**
	 *方法: 取得String
	 *@return: String  q30_ratio
	 */
	@Column(name ="Q30_RATIO", length = 50)
	public String getQ30Ratio(){
		return this.q30Ratio;
	}
	/**
	 *方法: 设置String
	 *@param: String  q30_ratio
	 */
	public void setQ30Ratio(String q30Ratio){
		this.q30Ratio = q30Ratio;
	}
	/**
	 *方法: 取得String
	 *@return: String  align_ratio
	 */
	@Column(name ="ALIGN_RATIO", length = 50)
	public String getAlignRatio(){
		return this.alignRatio;
	}
	/**
	 *方法: 设置String
	 *@param: String  align_ratio
	 */
	public void setAlignRatio(String alignRatio){
		this.alignRatio = alignRatio;
	}
	/**
	 *方法: 取得String
	 *@return: String  ur_ratio
	 */
	@Column(name ="UR_RATIO", length = 50)
	public String getUrRatio(){
		return this.urRatio;
	}
	/**
	 *方法: 设置String
	 *@param: String  ur_ratio
	 */
	public void setUrRatio(String urRatio){
		this.urRatio = urRatio;
	}
	/**
	 *方法: 取得String
	 *@return: String  结果
	 */
	@Column(name ="RESULT", length = 50)
	public String getResult(){
		return this.result;
	}
	/**
	 *方法: 设置String
	 *@param: String  结果
	 */
	public void setResult(String result){
		this.result = result;
	}
	/**
	 *方法: 取得String
	 *@return: String  处理方式
	 */
	@Column(name ="METHOD", length = 50)
	public String getMethod(){
		return this.method;
	}
	/**
	 *方法: 设置String
	 *@param: String  处理方式
	 */
	public void setMethod(String method){
		this.method = method;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	public String getAnalysisInfoId() {
		return analysisInfoId;
	}
	public void setAnalysisInfoId(String analysisInfoId) {
		this.analysisInfoId = analysisInfoId;
	}
	
	/**
	 *方法: 取得AnalysisTask
	 *@return: AnalysisTask  关联主表
	 */
//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "ANALYSIS_INFO")
//	public AnalysisInfo getAnalysisInfo() {
//		return analysisInfo;
//	}
//	public void setAnalysisInfo(AnalysisInfo analysisInfo) {
//		this.analysisInfo = analysisInfo;
//	}
	
}