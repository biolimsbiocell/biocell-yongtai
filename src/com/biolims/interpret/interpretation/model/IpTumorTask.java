package com.biolims.interpret.interpretation.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.analysis.analy.model.AnalysisTask;
import com.biolims.common.model.user.User;
import com.biolims.common.model.user.User;
/**   
 * @Title: Model
 * @Description: 遗传性肿瘤数据解读
 * @author lims-platform
 * @date 2015-12-07 09:45:00
 * @version V1.0   
 *
 */
@Entity
@Table(name = "IP_TUMOR_TASK")
@SuppressWarnings("serial")
public class IpTumorTask extends EntityDao<IpTumorTask> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
	/**下达人*/
	private User createUser;
	/**下达时间*/
	private Date createDate;
	/**解读员*/
	private User acceptUser;
	/**解读时间*/
	private Date acceptDate;
	/**分析方案*/
	//private String analysisCase;
	/**工作流id*/
	private String state;
	/**工作流状态*/
	private String stateName;
	//信息分析
	private AnalysisTask analysisTask;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "ANALYSIS_TASK")
	public AnalysisTask getAnalysisTask() {
		return analysisTask;
	}
	public void setAnalysisTask(AnalysisTask analysisTask) {
		this.analysisTask = analysisTask;
	}
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得User
	 *@return: User  下达人
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "CREATE_USER")
	public User getCreateUser(){
		return this.createUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  下达人
	 */
	public void setCreateUser(User createUser){
		this.createUser = createUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  下达时间
	 */
	@Column(name ="CREATE_DATE", length = 50)
	public Date getCreateDate(){
		return this.createDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  下达时间
	 */
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	/**
	 *方法: 取得User
	 *@return: User  解读员
	 */

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "ACCEPT_USER")
	public User getAcceptUser(){
		return this.acceptUser;
	}
	/**
	 *方法: 设置User
	 *@param: User  解读员
	 */
	public void setAcceptUser(User acceptUser){
		this.acceptUser = acceptUser;
	}
	/**
	 *方法: 取得Date
	 *@return: Date  解读时间
	 */
	@Column(name ="ACCEPT_DATE", length = 50)
	public Date getAcceptDate(){
		return this.acceptDate;
	}
	/**
	 *方法: 设置Date
	 *@param: Date  解读时间
	 */
	public void setAcceptDate(Date acceptDate){
		this.acceptDate = acceptDate;
	}
	/**
	 *方法: 取得String
	 *@return: String  分析方案
	 */
//	@Column(name ="ANALYSIS_CASE", length = 50)
//	public String getAnalysisCase(){
//		return this.analysisCase;
//	}
//	/**
//	 *方法: 设置String
//	 *@param: String  分析方案
//	 */
//	public void setAnalysisCase(String analysisCase){
//		this.analysisCase = analysisCase;
//	}
	/**
	 *方法: 取得String
	 *@return: String  工作流id
	 */
	@Column(name ="STATE", length = 50)
	public String getState(){
		return this.state;
	}
	/**
	 *方法: 设置String
	 *@param: String  工作流id
	 */
	public void setState(String state){
		this.state = state;
	}
	/**
	 *方法: 取得String
	 *@return: String  工作流状态
	 */
	@Column(name ="STATE_NAME", length = 50)
	public String getStateName(){
		return this.stateName;
	}
	/**
	 *方法: 设置String
	 *@param: String  工作流状态
	 */
	public void setStateName(String stateName){
		this.stateName = stateName;
	}
}