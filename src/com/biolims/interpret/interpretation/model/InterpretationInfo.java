package com.biolims.interpret.interpretation.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
import com.biolims.file.model.FileInfo;
/**   
 * @Title: Model
 * @Description: 解读结果
 * @author lims-platform
 * @date 2015-12-07 09:44:40
 * @version V1.0   
 *
 */
@Entity
@Table(name = "INTERPRETATION_INFO")
@SuppressWarnings("serial")
public class InterpretationInfo extends EntityDao<InterpretationInfo> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**cnv*/
	private String cnv;
	/**解读结果*/
	private String readingResult;
	/**结果*/
	private String result;
	/**描述*/
	private String name;
	/**数据解读编号*/
	private String code;
	/**处理方式*/
	private String method;
	/**备注*/
	private String note;
	/**样本号*/
	private String sampleCode;
	/**关联主表*/
	private InterpretationTask interpretationTask;
	/**result1*/
	private String result1;
	/**result2*/
	private String result2;
	//上传图片
	private FileInfo upLoadAccessory;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  cnv
	 */
	@Column(name ="CNV", length = 50)
	public String getCnv(){
		return this.cnv;
	}
	/**
	 *方法: 设置String
	 *@param: String  cnv
	 */
	public void setCnv(String cnv){
		this.cnv = cnv;
	}
	/**
	 *方法: 取得String
	 *@return: String  解读结果
	 */
	@Column(name ="READING_RESULT", length = 50)
	public String getReadingResult(){
		return this.readingResult;
	}
	/**
	 *方法: 设置String
	 *@param: String  解读结果
	 */
	public void setReadingResult(String readingResult){
		this.readingResult = readingResult;
	}
	/**
	 *方法: 取得String
	 *@return: String  结果
	 */
	@Column(name ="RESULT", length = 50)
	public String getResult(){
		return this.result;
	}
	/**
	 *方法: 设置String
	 *@param: String  结果
	 */
	public void setResult(String result){
		this.result = result;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  数据解读编号
	 */
	@Column(name ="CODE", length = 50)
	public String getCode(){
		return this.code;
	}
	/**
	 *方法: 设置String
	 *@param: String  数据解读编号
	 */
	public void setCode(String code){
		this.code = code;
	}
	/**
	 *方法: 取得String
	 *@return: String  处理方式
	 */
	@Column(name ="METHOD", length = 50)
	public String getMethod(){
		return this.method;
	}
	/**
	 *方法: 设置String
	 *@param: String  处理方式
	 */
	public void setMethod(String method){
		this.method = method;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本号
	 */
	@Column(name ="SAMPLE_CODE", length = 60)
	public String getSampleCode(){
		return this.sampleCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本号
	 */
	public void setSampleCode(String sampleCode){
		this.sampleCode = sampleCode;
	}
	/**
	 *方法: 取得InterpretationTask
	 *@return: InterpretationTask  关联主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "INTERPRETATION_TASK")
	public InterpretationTask getInterpretationTask(){
		return this.interpretationTask;
	}
	/**
	 *方法: 设置InterpretationTask
	 *@param: InterpretationTask  关联主表
	 */
	public void setInterpretationTask(InterpretationTask interpretationTask){
		this.interpretationTask = interpretationTask;
	}
	/**
	 *方法: 取得String
	 *@return: String  result1
	 */
	@Column(name ="RESULT1", length = 50)
	public String getResult1(){
		return this.result1;
	}
	/**
	 *方法: 设置String
	 *@param: String  result1
	 */
	public void setResult1(String result1){
		this.result1 = result1;
	}
	/**
	 *方法: 取得String
	 *@return: String  result2
	 */
	@Column(name ="RESULT2", length = 50)
	public String getResult2(){
		return this.result2;
	}
	/**
	 *方法: 设置String
	 *@param: String  result2
	 */
	public void setResult2(String result2){
		this.result2 = result2;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "UPLOAD_ACCESSORY")
	public FileInfo getUpLoadAccessory() {
		return upLoadAccessory;
	}
	public void setUpLoadAccessory(FileInfo upLoadAccessory) {
		this.upLoadAccessory = upLoadAccessory;
	}
}