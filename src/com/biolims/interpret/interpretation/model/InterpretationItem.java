package com.biolims.interpret.interpretation.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.biolims.common.model.user.User;
import com.biolims.dao.EntityDao;
import com.biolims.dic.model.DicType;
import com.biolims.file.model.FileInfo;
import com.biolims.system.work.model.WorkType;
/**   
 * @Title: Model
 * @Description: 样本明细
 * @author lims-platform
 * @date 2015-12-07 09:44:29
 * @version V1.0   
 *
 */
@Entity
@Table(name = "INTERPRETATION_ITEM")
@SuppressWarnings("serial")
public class InterpretationItem extends EntityDao<InterpretationItem> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
	/**样本号*/
	private String sampleCode;
	/**结果判定*/
	private String isgood;
	/**处理方式*/
	private DicType method;
	/**备注*/
	private String note;
	/**关联主表*/
	private InterpretationTask interpretationTask;
	//实验期次
	private Integer testNum;
	//样本类型
	private String sampleType;
	//性别
	private String gender;
	//文库编号
	private String wkCode;
	//样本接收日期
	private Date sampleReceiveDate;
	//报告截止日期
	private Date reportStopDate;
	//上机时间
	private Date onTime;
	//预下机时间
	private Date perOffTime;
	//收到CNV数据日期
	private Date acceptCnvDate;
	//解读完成日期
	private Date readEndDate;
	//收到图片日期
	private Date acceptImgDate;
	//报告发客服日期
	private Date reportSendDate;
	//异常样本
	private String abnormalSample;
	//报告状态
	private String reportState;
	//报告发出日期
	private Date reportPostDate;
	/**病人姓名*/
	private String patientName;
	/**身份证*/
	private String idCard;
	/**手机号*/
	private String phone;
	/**检测方法*/
	private WorkType businessType;
	/**送检医院*/
	private String hospital;
	/**价格*/
	private Double price;
	/**付款方式*/
	private DicType payType;
	/**项目编号*/
	private String productId;
	/**项目名称*/
	private String productName;
	//取样时间
	private String sampleTime;
	//姓名拼音
	private String spellName;
	//年龄
	private String age;
	//籍贯
	private String birthAddress;
	//民族
	private String nation;
	//身高
	private Double hight;
	//体重
	private Double weight;
	//婚姻情况
	private String marriage;
	//证件类型
	private String cardType;
	//证件号码
	private String cardNumber;
	//联系方式
	private String tele;
	//邮箱
	private String email;
	//家庭住址
	private String familyAddress;
	//是否收费
	private String isFee;
	//优惠类型
	private String privilege;
	//推荐人
	private String referUser;
	//发票号
	private String billNumber;
	//发票抬头
	private String billTitle;
	//录入人
	private String inputUser;
	//审核人1
	private String confirmUser1;
	//审核人2
	private String confirmUser2;
	//接收日期
	private String receiveDate;
	//上传图片
	private FileInfo files;
	//地区
	private String area;
	//金额
	private Double fee;
	//病历号
	private String medicalCard;
	//送检日期
	private String inspectDate;
	//应出报告日期
	private String reportDate;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本号
	 */
	@Column(name ="SAMPLE_CODE", length = 50)
	public String getSampleCode(){
		return this.sampleCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本号
	 */
	public void setSampleCode(String sampleCode){
		this.sampleCode = sampleCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  结果判定
	 */
	@Column(name ="ISGOOD", length = 50)
	public String getIsgood(){
		return this.isgood;
	}
	/**
	 *方法: 设置String
	 *@param: String  结果判定
	 */
	public void setIsgood(String isgood){
		this.isgood = isgood;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "METHOD")
	public DicType getMethod() {
		return method;
	}
	public void setMethod(DicType method) {
		this.method = method;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 50)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得InterpretationTask
	 *@return: InterpretationTask  关联主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "INTERPRETATION_TASK")
	public InterpretationTask getInterpretationTask(){
		return this.interpretationTask;
	}
	/**
	 *方法: 设置InterpretationTask
	 *@param: InterpretationTask  关联主表
	 */
	public void setInterpretationTask(InterpretationTask interpretationTask){
		this.interpretationTask = interpretationTask;
	}
	public Integer getTestNum() {
		return testNum;
	}
	public void setTestNum(Integer testNum) {
		this.testNum = testNum;
	}
	public String getSampleType() {
		return sampleType;
	}
	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getWkCode() {
		return wkCode;
	}
	public void setWkCode(String wkCode) {
		this.wkCode = wkCode;
	}
	public Date getSampleReceiveDate() {
		return sampleReceiveDate;
	}
	public void setSampleReceiveDate(Date sampleReceiveDate) {
		this.sampleReceiveDate = sampleReceiveDate;
	}
	public Date getReportStopDate() {
		return reportStopDate;
	}
	public void setReportStopDate(Date reportStopDate) {
		this.reportStopDate = reportStopDate;
	}
	public Date getOnTime() {
		return onTime;
	}
	public void setOnTime(Date onTime) {
		this.onTime = onTime;
	}
	public Date getPerOffTime() {
		return perOffTime;
	}
	public void setPerOffTime(Date perOffTime) {
		this.perOffTime = perOffTime;
	}
	public Date getAcceptCnvDate() {
		return acceptCnvDate;
	}
	public void setAcceptCnvDate(Date acceptCnvDate) {
		this.acceptCnvDate = acceptCnvDate;
	}
	public Date getReadEndDate() {
		return readEndDate;
	}
	public void setReadEndDate(Date readEndDate) {
		this.readEndDate = readEndDate;
	}
	public Date getAcceptImgDate() {
		return acceptImgDate;
	}
	public void setAcceptImgDate(Date acceptImgDate) {
		this.acceptImgDate = acceptImgDate;
	}
	public Date getReportSendDate() {
		return reportSendDate;
	}
	public void setReportSendDate(Date reportSendDate) {
		this.reportSendDate = reportSendDate;
	}
	public String getAbnormalSample() {
		return abnormalSample;
	}
	public void setAbnormalSample(String abnormalSample) {
		this.abnormalSample = abnormalSample;
	}
	public String getReportState() {
		return reportState;
	}
	public void setReportState(String reportState) {
		this.reportState = reportState;
	}
	public Date getReportPostDate() {
		return reportPostDate;
	}
	public void setReportPostDate(Date reportPostDate) {
		this.reportPostDate = reportPostDate;
	}
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getIdCard() {
		return idCard;
	}
	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "BUSINESS_TYPE")
	public WorkType getBusinessType() {
		return businessType;
	}
	public void setBusinessType(WorkType businessType) {
		this.businessType = businessType;
	}
	public String getHospital() {
		return hospital;
	}
	public void setHospital(String hospital) {
		this.hospital = hospital;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "PAY_TYPE")
	public DicType getPayType() {
		return payType;
	}
	public void setPayType(DicType payType) {
		this.payType = payType;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getSampleTime() {
		return sampleTime;
	}
	public void setSampleTime(String sampleTime) {
		this.sampleTime = sampleTime;
	}
	public String getSpellName() {
		return spellName;
	}
	public void setSpellName(String spellName) {
		this.spellName = spellName;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getBirthAddress() {
		return birthAddress;
	}
	public void setBirthAddress(String birthAddress) {
		this.birthAddress = birthAddress;
	}
	public String getNation() {
		return nation;
	}
	public void setNation(String nation) {
		this.nation = nation;
	}
	public Double getHight() {
		return hight;
	}
	public void setHight(Double hight) {
		this.hight = hight;
	}
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	public String getMarriage() {
		return marriage;
	}
	public void setMarriage(String marriage) {
		this.marriage = marriage;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getTele() {
		return tele;
	}
	public void setTele(String tele) {
		this.tele = tele;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFamilyAddress() {
		return familyAddress;
	}
	public void setFamilyAddress(String familyAddress) {
		this.familyAddress = familyAddress;
	}
	public String getIsFee() {
		return isFee;
	}
	public void setIsFee(String isFee) {
		this.isFee = isFee;
	}
	public String getPrivilege() {
		return privilege;
	}
	public void setPrivilege(String privilege) {
		this.privilege = privilege;
	}
	public String getReferUser() {
		return referUser;
	}
	public void setReferUser(String referUser) {
		this.referUser = referUser;
	}
	public String getBillNumber() {
		return billNumber;
	}
	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}
	public String getBillTitle() {
		return billTitle;
	}
	public void setBillTitle(String billTitle) {
		this.billTitle = billTitle;
	}
	public String getInputUser() {
		return inputUser;
	}
	public void setInputUser(String inputUser) {
		this.inputUser = inputUser;
	}
	public String getConfirmUser1() {
		return confirmUser1;
	}
	public void setConfirmUser1(String confirmUser1) {
		this.confirmUser1 = confirmUser1;
	}
	public String getConfirmUser2() {
		return confirmUser2;
	}
	public void setConfirmUser2(String confirmUser2) {
		this.confirmUser2 = confirmUser2;
	}
	public String getReceiveDate() {
		return receiveDate;
	}
	public void setReceiveDate(String receiveDate) {
		this.receiveDate = receiveDate;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "FILES")
	public FileInfo getFiles() {
		return files;
	}
	public void setFiles(FileInfo files) {
		this.files = files;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public Double getFee() {
		return fee;
	}
	public void setFee(Double fee) {
		this.fee = fee;
	}
	public String getMedicalCard() {
		return medicalCard;
	}
	public void setMedicalCard(String medicalCard) {
		this.medicalCard = medicalCard;
	}
	public String getInspectDate() {
		return inspectDate;
	}
	public void setInspectDate(String inspectDate) {
		this.inspectDate = inspectDate;
	}
	public String getReportDate() {
		return reportDate;
	}
	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}
//	@ManyToOne(fetch = FetchType.LAZY)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "CREATE_USER")
//	public User getCreateUser() {
//		return createUser;
//	}
//	public void setCreateUser(User createUser) {
//		this.createUser = createUser;
//	}
	
}