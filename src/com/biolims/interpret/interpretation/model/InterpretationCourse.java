package com.biolims.interpret.interpretation.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 解读过程
 * @author lims-platform
 * @date 2015-12-07 09:44:33
 * @version V1.0   
 *
 */
@Entity
@Table(name = "INTERPRETATION_COURSE")
@SuppressWarnings("serial")
public class InterpretationCourse extends EntityDao<InterpretationCourse> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**描述*/
	private String name;
	/**样本号*/
	private String sampleCode;
	/**karyotype*/
	private String karyotype;
	/**chr:site*/
	private String chrSite;
	/**area*/
	private String area;
	///////////////////////////////////////////
	/**result1*/
	private String resultOne;
	/**result2*/
	private String resultTwo;
	/**cnv*/
	private String cnv;
	/**reads_mb*/
	private String readsMb;
	/**gc_content*/
	private String gcContent;
	/**q30_ratio*/
	private String q30Ratio;
	/**align_ratio*/
	private String alignRatio;
	/**ur_ratio*/
	private String urRatio;
	/**信息分析结果*/
	private String result;
	///////////////////////////////////////////
//	/**size*/
//	private String size;
	/**gain/loss*/
	private String gainLoss;
	/**syndrome*/
	private String syndrome;
	/**gene*/
	private String gene;
	/**dgv*/
	private String dgv;
	/**omim*/
	private String omim;
	/**combined*/
	private String combined;
	/**结果判定*/
	private String isgood;
	/**处理方式*/
	private String method;
	/**关联中间表ID*/
	private String tempId;
	public String getTempId() {
		return tempId;
	}
	public void setTempId(String tempId) {
		this.tempId = tempId;
	}
	/**解读结果*/
	private String readingResult;
	/**关联主表*/
	private InterpretationTask interpretationTask;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  描述
	 */
	@Column(name ="NAME", length = 50)
	public String getName(){
		return this.name;
	}
	/**
	 *方法: 设置String
	 *@param: String  描述
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 *方法: 取得String
	 *@return: String  样本号
	 */
	@Column(name ="SAMPLE_CODE", length = 50)
	public String getSampleCode(){
		return this.sampleCode;
	}
	/**
	 *方法: 设置String
	 *@param: String  样本号
	 */
	public void setSampleCode(String sampleCode){
		this.sampleCode = sampleCode;
	}
	/**
	 *方法: 取得String
	 *@return: String  karyotype
	 */
	@Column(name ="KARYOTYPE", length = 50)
	public String getKaryotype(){
		return this.karyotype;
	}
	/**
	 *方法: 设置String
	 *@param: String  karyotype
	 */
	public void setKaryotype(String karyotype){
		this.karyotype = karyotype;
	}
	/**
	 *方法: 取得String
	 *@return: String  chr:site
	 */
	@Column(name ="CHR_SITE", length = 50)
	public String getChrSite(){
		return this.chrSite;
	}
	/**
	 *方法: 设置String
	 *@param: String  chr:site
	 */
	public void setChrSite(String chrSite){
		this.chrSite = chrSite;
	}
	/**
	 *方法: 取得String
	 *@return: String  area
	 */
	@Column(name ="AREA", length = 50)
	public String getArea(){
		return this.area;
	}
	/**
	 *方法: 设置String
	 *@param: String  area
	 */
	public void setArea(String area){
		this.area = area;
	}
//	/**
//	 *方法: 取得String
//	 *@return: String  size
//	 */
//	@Column(name ="SIZE", length = 50)
//	public String getSize(){
//		return this.size;
//	}
//	/**
//	 *方法: 设置String
//	 *@param: String  size
//	 */
//	public void setSize(String size){
//		this.size = size;
//	}
	/**
	 *方法: 取得String
	 *@return: String  gain/loss
	 */
	@Column(name ="GAIN_LOSS", length = 50)
	public String getGainLoss(){
		return this.gainLoss;
	}
	/**
	 *方法: 设置String
	 *@param: String  gain/loss
	 */
	public void setGainLoss(String gainLoss){
		this.gainLoss = gainLoss;
	}
	/**
	 *方法: 取得String
	 *@return: String  syndrome
	 */
	@Column(name ="SYNDROME", length = 50)
	public String getSyndrome(){
		return this.syndrome;
	}
	/**
	 *方法: 设置String
	 *@param: String  syndrome
	 */
	public void setSyndrome(String syndrome){
		this.syndrome = syndrome;
	}
	/**
	 *方法: 取得String
	 *@return: String  gene
	 */
	@Column(name ="GENE", length = 50)
	public String getGene(){
		return this.gene;
	}
	/**
	 *方法: 设置String
	 *@param: String  gene
	 */
	public void setGene(String gene){
		this.gene = gene;
	}
	/**
	 *方法: 取得String
	 *@return: String  dgv
	 */
	@Column(name ="DGV", length = 50)
	public String getDgv(){
		return this.dgv;
	}
	/**
	 *方法: 设置String
	 *@param: String  dgv
	 */
	public void setDgv(String dgv){
		this.dgv = dgv;
	}
	/**
	 *方法: 取得String
	 *@return: String  omim
	 */
	@Column(name ="OMIM", length = 50)
	public String getOmim(){
		return this.omim;
	}
	/**
	 *方法: 设置String
	 *@param: String  omim
	 */
	public void setOmim(String omim){
		this.omim = omim;
	}
	/**
	 *方法: 取得String
	 *@return: String  combined
	 */
	@Column(name ="COMBINED", length = 50)
	public String getCombined(){
		return this.combined;
	}
	/**
	 *方法: 设置String
	 *@param: String  combined
	 */
	public void setCombined(String combined){
		this.combined = combined;
	}
	/**
	 *方法: 取得String
	 *@return: String  结果判定
	 */
	@Column(name ="ISGOOD", length = 50)
	public String getIsgood(){
		return this.isgood;
	}
	/**
	 *方法: 设置String
	 *@param: String  结果判定
	 */
	public void setIsgood(String isgood){
		this.isgood = isgood;
	}
	/**
	 *方法: 取得String
	 *@return: String  处理方式
	 */
	@Column(name ="METHOD", length = 50)
	public String getMethod(){
		return this.method;
	}
	/**
	 *方法: 设置String
	 *@param: String  处理方式
	 */
	public void setMethod(String method){
		this.method = method;
	}
	/**
	 *方法: 取得String
	 *@return: String  解读结果
	 */
	@Column(name ="READING_RESULT", length = 50)
	public String getReadingResult(){
		return this.readingResult;
	}
	/**
	 *方法: 设置String
	 *@param: String  解读结果
	 */
	public void setReadingResult(String readingResult){
		this.readingResult = readingResult;
	}
	/**
	 *方法: 取得InterpretationTask
	 *@return: InterpretationTask  关联主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "INTERPRETATION_TASK")
	public InterpretationTask getInterpretationTask(){
		return this.interpretationTask;
	}
	/**
	 *方法: 设置InterpretationTask
	 *@param: InterpretationTask  关联主表
	 */
	public void setInterpretationTask(InterpretationTask interpretationTask){
		this.interpretationTask = interpretationTask;
	}
	public String getResultOne() {
		return resultOne;
	}
	public void setResultOne(String resultOne) {
		this.resultOne = resultOne;
	}
	public String getResultTwo() {
		return resultTwo;
	}
	public void setResultTwo(String resultTwo) {
		this.resultTwo = resultTwo;
	}
	public String getCnv() {
		return cnv;
	}
	public void setCnv(String cnv) {
		this.cnv = cnv;
	}
	public String getReadsMb() {
		return readsMb;
	}
	public void setReadsMb(String readsMb) {
		this.readsMb = readsMb;
	}
	public String getGcContent() {
		return gcContent;
	}
	public void setGcContent(String gcContent) {
		this.gcContent = gcContent;
	}
	public String getQ30Ratio() {
		return q30Ratio;
	}
	public void setQ30Ratio(String q30Ratio) {
		this.q30Ratio = q30Ratio;
	}
	public String getAlignRatio() {
		return alignRatio;
	}
	public void setAlignRatio(String alignRatio) {
		this.alignRatio = alignRatio;
	}
	public String getUrRatio() {
		return urRatio;
	}
	public void setUrRatio(String urRatio) {
		this.urRatio = urRatio;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	
}