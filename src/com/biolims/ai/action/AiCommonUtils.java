package com.biolims.ai.action;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.HashMap;

import javax.imageio.ImageIO;

import jp.sourceforge.qrcode.QRCodeDecoder;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.json.JSONObject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.baidu.aip.ocr.AipOcr;
import com.baidu.aip.util.Base64Util;
import com.biolims.ai.TwoDimensionCodeImage;
import com.biolims.ai.utils.AuthService;
import com.biolims.ai.utils.FileUtil;
import com.biolims.ai.utils.HttpUtil;
import com.biolims.common.action.BaseActionSupport;
import com.biolims.util.HttpUtils;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;

/**
 * AI文字识别
 * @author cong
 *
 */
@Namespace("/common/aiUtils")
@Controller
@Scope("prototype")
@ParentPackage("default")
public class AiCommonUtils extends BaseActionSupport {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8688619817464554332L;
	//设置APPID/AK/SK
    public static final String APP_ID = "10593779";
    public static final String API_KEY = "5RodqLLpIrNnIdDGbBEGMPAL";
    public static final String SECRET_KEY = "TMOyylwAHNI8SUKc4DGpR717b4IB0oqk";
    public static final String TEMPLATE_SIGN  = "2107d44375758f98102a44f3da57db53";
    private File AiPicture;//动作类上传的属性必须是file类型,upload为表单的name值  
    private String AiPictureFileName; //上传的文件名称,固定写法:name+FileName;  
    private String AiPictureContentType; //上传文件的mime类型,固定写法: name+ContextType; 
    
    @Action(value = "AiTextRead", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void isIdAreThere() throws Exception {
    	HashMap<String, String> options = new HashMap<String, String>();
//        options.put("language_type", "CHN_ENG");
//        options.put("detect_direction", "true");
//        options.put("detect_language", "true");
//        options.put("probability", "true");
    	 // 初始化一个AipOcr
        AipOcr client = new AipOcr(APP_ID, API_KEY, SECRET_KEY);
        // 可选：设置网络连接参数
        client.setConnectionTimeoutInMillis(2000);
        client.setSocketTimeoutInMillis(60000);
        // 可选：设置代理服务器地址, http和socket二选一，或者均不设置
        //client.setHttpProxy("proxy_host", proxy_port);  // 设置http代理
        //client.setSocketProxy("proxy_host", proxy_port);  // 设置socket代理
        // 可选：设置log4j日志输出格式，若不设置，则使用默认配置
        // 也可以直接通过jvm启动参数设置此环境变量
        //System.setProperty("aip.log4j.conf", "path/to/your/log4j.properties");

        // 调用接口
        // String path = AiPicture.getPath();
        byte[] by = new byte[(int) AiPicture.length()]; 
        try { 
	        InputStream is = new FileInputStream(AiPicture); 
	        ByteArrayOutputStream bytestream = new ByteArrayOutputStream(); 
	        byte[] bb = new byte[2048]; 
	        int ch; 
	        ch = is.read(bb); 
	        while (ch != -1) { 
	        bytestream.write(bb, 0, ch); 
	        ch = is.read(bb); 
        } 
	        by = bytestream.toByteArray(); 
	        is.close();
	        bytestream.close();
        } catch (Exception ex) { 
        	ex.printStackTrace(); 
        } 

//        String path = "C:/Users/fzb/Desktop/test.jpg";
        JSONObject res = client.basicGeneral(by, options);
        System.out.println(res.toString(2));
        HttpUtils.write(res.toString(2));
    }
  
    @Action(value = "AiHanderReader", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
    public void AiHanderReader() throws Exception {
    	// 手写识别url
    	String bankcardIdentificate = "https://aip.baidubce.com/rest/2.0/ocr/v1/handwriting";
    	String result = "";
    	try {
    		String imgStr = getParameterFromRequest("imgStr");
//    		byte[] imgData = FileUtil.readFileByBytes(AiPicture.getPath());
//    		String imgStr = Base64Util.encode(imgData);
    		String params = URLEncoder.encode("image", "UTF-8") + "=" + URLEncoder.encode(imgStr, "UTF-8");
    		/**
    		 * 线上环境access_token有过期时间， 客户端可自行缓存，过期后重新获取。
    		 */
//    		String accessToken = "24.9112dafa49ff6dd84e9760e5ae074503.2592000.1530355913.282335-10593779";
    		String accessToken = AuthService.getAuth();
    		result = HttpUtil.post(bankcardIdentificate, accessToken, params);
    		System.out.println(result);
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	HttpUtils.write(result);
    }
    
    
    
    @Action(value = "AiTemplateReader", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
    public void AiTemplateRead() throws Exception {
    	HashMap<String, String> options = new HashMap<String, String>();
//        options.put("language_type", "CHN_ENG");
//        options.put("detect_direction", "true");
//        options.put("detect_language", "true");
//        options.put("probability", "true");
    	// 初始化一个AipOcr
    	AipOcr client = new AipOcr(APP_ID, API_KEY, SECRET_KEY);
    	// 可选：设置网络连接参数
    	client.setConnectionTimeoutInMillis(2000);
    	client.setSocketTimeoutInMillis(60000);
    	// 可选：设置代理服务器地址, http和socket二选一，或者均不设置
    	//client.setHttpProxy("proxy_host", proxy_port);  // 设置http代理
    	//client.setSocketProxy("proxy_host", proxy_port);  // 设置socket代理
    	// 可选：设置log4j日志输出格式，若不设置，则使用默认配置
    	// 也可以直接通过jvm启动参数设置此环境变量
    	//System.setProperty("aip.log4j.conf", "path/to/your/log4j.properties");
    	
    	// 调用接口
    	// String path = AiPicture.getPath();
    	byte[] by = new byte[(int) AiPicture.length()]; 
    	try { 
    		InputStream is = new FileInputStream(AiPicture); 
    		ByteArrayOutputStream bytestream = new ByteArrayOutputStream(); 
    		byte[] bb = new byte[2048]; 
    		int ch; 
    		ch = is.read(bb); 
    		while (ch != -1) { 
    			bytestream.write(bb, 0, ch); 
    			ch = is.read(bb); 
    		} 
    		by = bytestream.toByteArray(); 
    		is.close();
    		bytestream.close();
    	} catch (Exception ex) { 
    		ex.printStackTrace(); 
    	} 
    	
//        String path = "C:/Users/fzb/Desktop/test.jpg";
    	JSONObject res = client.custom(by,TEMPLATE_SIGN,options);
    	System.out.println(res.toString(2));
    	HttpUtils.write(res.toString(2));
    }
    @Action(value="scanCode")
    public void scanCode() throws Exception{
        MultiFormatReader formatReader=new MultiFormatReader();
        FileInputStream file=new FileInputStream(AiPicture); 
        BufferedImage image;
        try {
            image = ImageIO.read(file);
            BinaryBitmap binaryBitmap=new BinaryBitmap(new HybridBinarizer
                                    (new BufferedImageLuminanceSource(image)));

            HashMap hints=new HashMap();
            hints.put(EncodeHintType.CHARACTER_SET,"utf-8");    //指定字符编码为“utf-8”

            Result result=formatReader.decode(binaryBitmap,hints);
            HttpUtils.write(result.getText());
            System.out.println("解析结果："+result.toString());
            System.out.println("二维码格式："+result.getBarcodeFormat());
            System.out.println("二维码文本内容："+result.getText());
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
        	file.close();
        }
    }
//    @Action(value="scanCode")
//    public void decoderQRCode() throws Exception {  
//    	FileInputStream input=new FileInputStream(AiPicture); 
//        BufferedImage bufImg = null;  
//        String content = null;  
//        try {  
//            bufImg = ImageIO.read(input);  
//            QRCodeDecoder decoder = new QRCodeDecoder();  
//            content = new String(decoder.decode(new TwoDimensionCodeImage(bufImg)), "utf-8"); 
//            System.err.println(content);
//        } catch (Exception e) {  
//            System.out.println("Error: " + e.getMessage());  
//            e.printStackTrace();  
//        }finally{
//        	input.close();
//        }
//        HttpUtils.write(content); 
//    }
    
    
	public File getAiPicture() {
		return AiPicture;
	}

	public void setAiPicture(File aiPicture) {
		AiPicture = aiPicture;
	}

	public String getAiPictureFileName() {
		return AiPictureFileName;
	}

	public void setAiPictureFileName(String aiPictureFileName) {
		AiPictureFileName = aiPictureFileName;
	}

	public String getAiPictureContentType() {
		return AiPictureContentType;
	}

	public void setAiPictureContentType(String aiPictureContentType) {
		AiPictureContentType = aiPictureContentType;
	}
    
}
