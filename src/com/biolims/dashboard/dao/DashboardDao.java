package com.biolims.dashboard.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.biolims.dao.BaseHibernateDao;
import com.biolims.dashboard.model.DashboardLayout;
import com.biolims.dashboard.model.DashboardModel;
import com.biolims.dashboard.model.DashboardUserLayout;
import com.biolims.dashboard.model.RoleDashboardModel;

@Repository
@SuppressWarnings("unchecked")
public class DashboardDao extends BaseHibernateDao {

	public List<Object> selectDashboard(String userId) throws Exception {
		String hql = "select distinct layoutColumn,userId from Dashboard where userId='" + userId
				+ "' ORDER BY layoutColumn";
		return find(hql);
	}

	public List<Object> selectDashboardByLayoutColumn(String userId, int layoutColumn) throws Exception {

		String hql = "select d.id,d.model.id,d.squenceNum,d.config ,dm.moduleName,dm.moduleConfUrl,dm.moduleUrl,dm.isFrame from Dashboard d,DashboardModel dm where d.model.id=dm.id and  d.userId='"
				+ userId + "' and d.layoutColumn=" + layoutColumn + " order by d.squenceNum asc";
		return find(hql);
	}

	public DashboardLayout selectDashboardLayout(String userId) throws Exception {
		String hql = "from DashboardLayout where id=(select layout.id from DashboardUserLayout where userId='" + userId
				+ "')";
		return this.queryUniqueResult(hql);
	}

	public List<DashboardLayout> selectLayoutAll() {
		String hql = "from DashboardLayout ORDER BY id ASC";
		return find(hql);
	}

	public DashboardUserLayout selectDashboardUserLayoutByUserId(String userId) throws Exception {
		String hql = "from DashboardUserLayout where userId='" + userId + "'";
		return queryUniqueResult(hql);
	}

	public Object selectMaxSequenceNumber(String userId, int column) throws Exception {
		String hql = "select max(squenceNum)+1 from Dashboard where userId='" + userId + "' and layoutColumn=" + column;
		return queryUniqueResult(hql);
	}

	public void updateLayoutData(int num, String userId, int column) throws Exception {
		String hql = "update Dashboard set squenceNum=" + num + ",layoutColumn=0 where userId='" + userId
				+ "' and layoutColumn>=" + column;
		this.getSession().createQuery(hql).executeUpdate();
	}

	public void updateLayoutSortNum(String modelId, Integer column, Integer num) {
		String hql = "update Dashboard set squenceNum=" + num + ",layoutColumn=" + column + " where id = '" + modelId
				+ "'";
		this.getSession().createQuery(hql).executeUpdate();
	}

	public List<DashboardModel> selectDashboardModel(String type, String userId) throws Exception {
		String hql = null;

		if (type == null || type.length() <= 0) {
			hql = "from DashboardModel where level=1";
		} else {
			hql = "from DashboardModel where level=2 and moduleType='" + type + "'";
		}

		hql = hql
				+ " and id in(select dbm.id from RoleDashboardModel where role.id in(select role.id from UserRole where user.id='"
				+ userId + "'))";
		return find(hql);
	}

	public List<DashboardModel> selectDashboardModel() throws Exception {
		String hql = null;
		String key = "";
			hql = "from DashboardModel where 1=1";
		return find(hql + key);
	}

	public List<RoleDashboardModel> selectRoleDashboardModelByRoleId(String roleId) throws Exception {
		String hql = " from RoleDashboardModel where role.id='" + roleId + "'";
		return find(hql);
	}

	public void deleteRoleDashboardModelByRoleId(String roleId) throws Exception {
		String hql = "delete from RoleDashboardModel where role.id='" + roleId + "'";
		this.getSession().createQuery(hql).executeUpdate();
	}
}
