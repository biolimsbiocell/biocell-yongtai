package com.biolims.dashboard.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.core.model.user.Role;
import com.biolims.dashboard.dao.DashboardDao;
import com.biolims.dashboard.model.Dashboard;
import com.biolims.dashboard.model.DashboardLayout;
import com.biolims.dashboard.model.DashboardModel;
import com.biolims.dashboard.model.DashboardUserLayout;
import com.biolims.dashboard.model.RoleDashboardModel;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.util.JsonUtils;

@Service
public class DashboardService {

	@Resource
	private DashboardDao dashboardDao;
	@Resource
	private CommonDAO commonDAO;

	/**
	 * 查询用户工作台
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, Object>> findDashboard(String userId)
			throws Exception {
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		List<Object> dbList = dashboardDao.selectDashboard(userId);
		if (dbList != null && dbList.size() > 0) {
			for (Object obj : dbList) {
				Map<String, Object> map = new HashMap<String, Object>();
				Object[] objs = (Object[]) obj;
				map.put("layoutColumn", objs[0]);
				map.put("fkId", objs[1]);
				List<Object> childrenList = dashboardDao
						.selectDashboardByLayoutColumn(objs[1].toString(),
								Integer.valueOf(objs[0].toString()));
				map.put("children", listObjectToMap(childrenList));
				result.add(map);
			}
		}
		return result;
	}

	private List<Map<String, Object>> listObjectToMap(List<Object> objList)
			throws Exception {
		List<Map<String, Object>> listMap = new ArrayList<Map<String, Object>>();
		if (objList != null && objList.size() > 0) {
			for (Object obj : objList) {
				Object[] objs = (Object[]) obj;
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("id", objs[0]);
				map.put("modileId", objs[1]);
				map.put("sequenceNumber", objs[2]);
				map.put("config", objs[3]);
				map.put("moduleName", objs[4]);
				map.put("moduleConfUrl", objs[5]);
				map.put("moduleUrl", objs[6]);
				map.put("isFrame", objs[7]);
				listMap.add(map);
			}
		}
		return listMap;
	}

	/**
	 * 查询用户页面布局
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findDashboardLayout(String userId)
			throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		List<Object> list = new ArrayList<Object>();
		DashboardLayout dl = dashboardDao.selectDashboardLayout(userId);
		Object id = 2;
		if (dl == null) {
			// 默认两列布局
			Integer column = 0;

			Map<String, Object> _map = new HashMap<String, Object>();
			_map.put("column", column);
			_map.put("colWidth", "50");
			list.add(_map);

			_map = new HashMap<String, Object>();
			_map.put("column", ++column);
			_map.put("colWidth", "50");
			list.add(_map);

			result.put("css", "mwrap-col-3");
		} else {
			id = dl.getId();
			Integer columnNum = dl.getColumnNum();
			String css = dl.getCss();

			String[] colWidthArray = css.split(",");
			for (int i = 0; i < columnNum; i++) {
				Map<String, Object> _map = new HashMap<String, Object>();
				_map.put("column", i);
				_map.put("colWidth", colWidthArray[i]);
				list.add(_map);
			}
		}
		result.put("id", id);
		result.put("list", list);
		return result;
	}

	/**
	 * 查询所有布局
	 * 
	 * @return
	 */
	public List<DashboardLayout> findLayoutAll() {
		return dashboardDao.selectLayoutAll();
	}

	/**
	 * 修改用户布局
	 * 
	 * @param layoutId
	 * @param userId
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void eidtLayout(String layoutId, String userId) throws Exception {

		// 得到当前用户的布局
		DashboardLayout userLayout = dashboardDao.selectDashboardLayout(userId);

		DashboardLayout layout = dashboardDao.get(DashboardLayout.class,
				layoutId);
		Integer columnNum = layout.getColumnNum();

		Integer maxColumn = 0;
		if (userLayout == null) {
			maxColumn = 2;
			DashboardUserLayout dul = new DashboardUserLayout();
			userLayout = new DashboardLayout();
			userLayout.setId(layoutId);
			dul.setLayout(userLayout);
			dul.setUserId(userId);
			dashboardDao.save(dul);
		} else {
			maxColumn = userLayout.getColumnNum();
			DashboardUserLayout dul = dashboardDao
					.selectDashboardUserLayoutByUserId(userId);
			userLayout = new DashboardLayout();
			userLayout.setId(layoutId);
			dul.setLayout(userLayout);
			dashboardDao.save(dul);
		}
		if (columnNum < maxColumn) {
			// 如果设定布局小于用户当前布局.将大于的部分全部移到最前列的最后面
			Integer _maxColumn = (Integer) dashboardDao
					.selectMaxSequenceNumber(userId, 0);
			if (_maxColumn == null || new Integer(0).equals(_maxColumn)) {
				_maxColumn = 1;
			}
			dashboardDao.updateLayoutData(_maxColumn, userId, columnNum);
		}
	}

	/**
	 * 更新用户调整后的工作台上模块列位置
	 * 
	 * @param info
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void modifyLayoutNum(String info) throws Exception {

		String[] columnArray = info.split("\\|");
		for (int i = 0; i < columnArray.length; i++) {
			// 0Sort:1@2@3
			String[] temp = columnArray[i].split(":");
			if (temp.length == 2) {
				String columnStr = temp[0].replace("Column", "");
				Integer column = Integer.valueOf(columnStr);
				String[] models = temp[1].split("@");
				for (int j = 1; j <= models.length; j++) {
					String modelId = models[j - 1];
					dashboardDao.updateLayoutSortNum(modelId, column, j);
				}
			}
		}
	}

	StringBuffer json = new StringBuffer();
	List<DashboardModel> dashList = new ArrayList<DashboardModel>();
	Role allrole = new Role();

	/**
	 * 查询指定类型的数据
	 * 
	 * @return
	 * @throws Exception
	 */
	public String findDashboardModel(String type, String userId)
			throws Exception {
		List<DashboardModel> list = dashboardDao.selectDashboardModel(type,
				userId);
		if (list != null) {
			for (DashboardModel dm : list) {
				if (type == null || type.length() <= 0) {
					dm.setLeaf(false);
				} else {
					dm.setLeaf(true);
				}
				dm.setText(dm.getModuleName());
			}
		}
		return list != null ? JsonUtils.toJsonString(list) : "";
	}

	/**
	 * 查询指定角色指定类型的模块数据
	 * 
	 * @return
	 * @throws Exception
	 */
	public String findRoleDashboardModel(String roleId) throws Exception {
		List<DashboardModel> list = dashboardDao.selectDashboardModel();
		String json = getJson(list, roleId);
		return json;
	}

	private String getJson(List<DashboardModel> list, String roleId) {

		json = new StringBuffer();
		allrole.setId(roleId);
		dashList = commonDAO
				.find("select a.dbm from RoleDashboardModel a where a.role.id='"
						+ roleId + "'");
		List<DashboardModel> nodeList0 = new ArrayList<DashboardModel>();
		// commonDAO.find("from Department where level='0'");
		Iterator<DashboardModel> it1 = list.iterator();
		while (it1.hasNext()) {
			DashboardModel node = (DashboardModel) it1.next();
			if (node.getLevel() == 1) {
				nodeList0.add(node);
			}

		}
		Iterator<DashboardModel> it = nodeList0.iterator();
		while (it.hasNext()) {
			DashboardModel node = (DashboardModel) it.next();
			constructorJson(list, node);

		}

		String jsonDate = json.toString();
		return ("[" + jsonDate + "]").replaceAll(",]", "]");
	}

	private void constructorJson(List<DashboardModel> list,
			DashboardModel treeNode) {
		String lan = (String)ServletActionContext.getRequest().getSession().getAttribute("lan");
		if (hasChild(list, treeNode)) {
			json.append("{\"val\":\"");
			json.append(treeNode.getId() + "\"");
			json.append(",\"title\":\"");
			if(lan.equals("en"))
				json.append(treeNode.getEnName()+"\"");
			else{
				json.append(treeNode.getModuleName() + "\"");
				
			}
			json.append(",\"data\":[");
			List<DashboardModel> childList = getChildList(list, treeNode);
			Iterator<DashboardModel> iterator = childList.iterator();
			while (iterator.hasNext()) {
				DashboardModel node = (DashboardModel) iterator.next();
				constructorJson(list, node);
			}
			json.append("]},");
		} else {
			json.append("{\"val\":\"");
			json.append(treeNode.getId() + "\"");
			json.append(",\"title\":\"");
			if(lan.equals("en"))
				json.append(treeNode.getEnName()+"\"");
			else{
				json.append(treeNode.getModuleName() + "\"");
				
			}
			if (isHasDashs(treeNode, dashList) == false) {
				json.append(",\"checked\":false");
			} else {
				json.append(",\"checked\":true");
			}
			json.append(",\"data\":[]");
			json.append("},");
		}
	}

	public boolean isHasDashs(DashboardModel treeNode, List<DashboardModel> list) {
		if (list.contains(treeNode)) {

			return true;
		}
		return false;

	}

	/**
	 * 判断是否有子节点
	 * 
	 * @param list
	 * @param treeNode
	 * @return
	 */
	public boolean hasChild(List<DashboardModel> list, DashboardModel treeNode) {
		return getChildList(list, treeNode).size() > 0 ? true : false;
	}

	/**
	 * 获得子节点列表信息
	 * 
	 * @param list
	 * @param treeNode
	 * @return
	 */
	public List<DashboardModel> getChildList(List<DashboardModel> list,
			DashboardModel treeNode) { // 得到子节点列表
		List<DashboardModel> li = new ArrayList<DashboardModel>();
		Iterator<DashboardModel> it = list.iterator();
		while (it.hasNext()) {
			DashboardModel n = (DashboardModel) it.next();
			if (n.getModuleType() != null
					&& n.getModuleType().equals(treeNode.getModuleName())) {
				li.add(n);
			}
		}
		return li;
	}

	private Map<String, String> objList2Map(List<RoleDashboardModel> roleModel)
			throws Exception {
		Map<String, String> result = new HashMap<String, String>();
		if (roleModel != null && roleModel.size() > 0) {
			for (RoleDashboardModel rm : roleModel) {
				result.put(rm.getDbm().getId(), "");
			}
		}
		return result;
	}

	/**
	 * 保存用户工作台组件
	 * 
	 * @param userId
	 * @param modelIds
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveDashboardModel(String userId, String[] modelIds)
			throws Exception {

		if (modelIds.length > 0) {
			Integer maxColumn = (Integer) dashboardDao.selectMaxSequenceNumber(
					userId, 0);
			if (maxColumn == null || new Integer(0).equals(maxColumn)) {
				maxColumn = 1;
			}
			for (String modelId : modelIds) {
				Dashboard d = new Dashboard();
				d.setUserId(userId);
				DashboardModel dm = new DashboardModel();
				dm.setId(modelId);
				d.setModel(dm);
				d.setSquenceNum(maxColumn++);
				d.setLayoutColumn(0);
				dashboardDao.save(d);
			}
		}
	}

	/**
	 * 移除指定的组件
	 * 
	 * @param dashboardId
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void deleteDashboard(String dashboardId) throws Exception {
		Dashboard d = new Dashboard();
		d.setId(dashboardId);
		dashboardDao.delete(d);
	}

	/**
	 * 保存指定角色可选用的组件
	 * 
	 * @param dashboardId
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveRoleDashboardModel(String roleId, String[] models)
			throws Exception {
		if (models != null && models.length > 0) {
			dashboardDao.deleteRoleDashboardModelByRoleId(roleId);
			for (String model : models) {
				DashboardModel dm = new DashboardModel();
				dm.setId(model);
				Role role = new Role();
				role.setId(roleId);
				RoleDashboardModel rdm = new RoleDashboardModel();
				rdm.setDbm(dm);
				rdm.setRole(role);
				dashboardDao.save(rdm);
			}
		}
	}

	public List<RoleDashboardModel> getRoleDashboardModel(String roleId) {
	List<RoleDashboardModel> list=	commonDAO.find(" from RoleDashboardModel a where a.role.id='"
						+ roleId + "'");
		return list;
	}
}
