package com.biolims.dashboard.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

/**
 * 工作台用户所选布局
 *
 */
@Entity
@Table(name = "T_DASHBOARD_USER_LAYOUT")
public class DashboardUserLayout implements Serializable {

	private static final long serialVersionUID = -8025674386838973790L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@Column(name = "USER_ID", length = 40)
	private String userId;//用户ID

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")
	@JoinColumn(name = "DASHBOARD_LAYOUT_ID")
	private DashboardLayout layout;//布局

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public DashboardLayout getLayout() {
		return layout;
	}

	public void setLayout(DashboardLayout layout) {
		this.layout = layout;
	}
}
