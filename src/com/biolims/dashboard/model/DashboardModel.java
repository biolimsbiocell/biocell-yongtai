package com.biolims.dashboard.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

/**
 * 工作台相关模块
 *
 */
@Entity
@Table(name = "T_DASHBOARD_MODEL")
public class DashboardModel implements Serializable {

	private static final long serialVersionUID = 4417182566389485037L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@Column(name = "MODULE_NAME", length = 50)
	private String moduleName;

	@Column(name = "MODULE_CONF_URL", length = 500)
	private String moduleConfUrl;

	@Column(name = "MODULE_URL", length = 500)
	private String moduleUrl;

	@Column(name = "MODULE_TYPE", length = 20)
	private String moduleType;
	@Column(name = "EN_NAME", length = 255)
	private String enName;

	@Column(name = "MODEL_LEVEL")
	private int level;//区别一二级菜

	@Column(name = "IS_FRAME")
	private int isFrame = 0;//是否是用iframe直接显示

	//非持久化属性
	@Transient
	private boolean leaf;//是否有子级
	@Transient
	private String text;
	@Transient
	private boolean checked;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getModuleConfUrl() {
		return moduleConfUrl;
	}

	public void setModuleConfUrl(String moduleConfUrl) {
		this.moduleConfUrl = moduleConfUrl;
	}

	public String getModuleUrl() {
		return moduleUrl;
	}

	public void setModuleUrl(String moduleUrl) {
		this.moduleUrl = moduleUrl;
	}

	public String getModuleType() {
		return moduleType;
	}

	public void setModuleType(String moduleType) {
		this.moduleType = moduleType;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public boolean isLeaf() {
		return leaf;
	}

	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public int getIsFrame() {
		return isFrame;
	}

	public void setIsFrame(int isFrame) {
		this.isFrame = isFrame;
	}

	/**
	 * @return the enName
	 */
	public String getEnName() {
		return enName;
	}

	/**
	 * @param enName the enName to set
	 */
	public void setEnName(String enName) {
		this.enName = enName;
	}

}
