package com.biolims.dashboard.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import org.hibernate.annotations.ForeignKey;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

/**
 * 工作台
 *
 */
@Entity
@Table(name = "T_DASHBOARD")
public class Dashboard implements Serializable {

	private static final long serialVersionUID = -8732165319173553213L;

	@Id
	@GenericGenerator(name = "idGenerator", strategy = "uuid")
	@GeneratedValue(generator = "idGenerator")
	@Column(name = "ID", length = 32)
	private String id;

	@Column(name = "USER_ID", length = 40)
	private String userId;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@ForeignKey(name = "null")
	@JoinColumn(name = "DASHBOARD_LAYOUT_ID")
	private DashboardModel model;//工作台模块

	@Column(name = "SQUENCE_NUM")
	private int squenceNum;//排序号

	@Column(name = "LAYOUT_COLUMN")
	private int layoutColumn;//所在布局列

	@Column(name = "CONFIG", length = 100)
	private String config;//用户配置信息

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public DashboardModel getModel() {
		return model;
	}

	public void setModel(DashboardModel model) {
		this.model = model;
	}

	public int getSquenceNum() {
		return squenceNum;
	}

	public void setSquenceNum(int squenceNum) {
		this.squenceNum = squenceNum;
	}

	public int getLayoutColumn() {
		return layoutColumn;
	}

	public void setLayoutColumn(int layoutColumn) {
		this.layoutColumn = layoutColumn;
	}

	public String getConfig() {
		return config;
	}

	public void setConfig(String config) {
		this.config = config;
	}

}
