package com.biolims.dashboard.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.common.model.user.User;
import com.biolims.dashboard.model.DashboardLayout;
import com.biolims.dashboard.service.DashboardService;
import com.biolims.util.HttpUtils;
import com.biolims.util.JsonUtils;

/**
 * 工作台管理
 *
 */
@Controller
@Scope("prototype")
@ParentPackage("default")
@Namespace("/dashboard")
public class DashboardAction extends BaseActionSupport {

	private static final long serialVersionUID = -3415778179659993505L;

	@Resource
	private DashboardService dashboardService;

	/**
	 * 查询指定用户工作台
	 * @return
	 * @throws Exception
	 */
	@Action(value = "toDashboard", interceptorRefs = @InterceptorRef("biolimsLoginStack"))
	public String toDashboard() throws Exception {
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);

		List<Map<String, Object>> dashboardList = dashboardService.findDashboard(user.getId());

		Map<String, Object> dashboardMap = dashboardService.findDashboardLayout(user.getId());

		getRequest().setAttribute("dashboardList", dashboardList);
		getRequest().setAttribute("dashboardMap", dashboardMap);
		return dispatcher("/WEB-INF/page/dashboard/toDashboard.jsp");
	}

	/**
	 * 修改布局页面
	 * @return
	 * @throws Exception
	 */
	@Action(value = "editLayout", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String editLayout() throws Exception {
		//获得系统内所有布局实现
		List<DashboardLayout> layOutList = dashboardService.findLayoutAll();
		getRequest().setAttribute("layOutList", layOutList);
		//查询指定用户/值班台的布局设置实现
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		Map<String, Object> dashboardMap = dashboardService.findDashboardLayout(user.getId());
		getRequest().setAttribute("userLayoutId", dashboardMap.get("id"));
		return dispatcher("/WEB-INF/page/dashboard/editLayout.jsp");
	}

	/**
	 * 保存布局
	 */
	@Action(value = "saveLayout", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveLayout() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			User user = (User) ServletActionContext.getRequest().getSession().getAttribute(
					SystemConstants.USER_SESSION_KEY);
			String layoutId = getRequest().getParameter("layoutId");
			dashboardService.eidtLayout(layoutId, user.getId());
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 修改用户布局排序
	 */
	@Action(value = "modifyLayoutNum", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void modifyLayoutNum() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String info = getRequest().getParameter("info");
			dashboardService.modifyLayoutNum(info);
			result.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(result));
	}

	/**
	 * 工作台可用模块页面
	 * @return
	 * @throws Exception
	 */
	@Action(value = "toDashboardModel", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String toDashboardModel() throws Exception {
		return dispatcher("/WEB-INF/page/dashboard/toDashboardModel.jsp");
	}

	/**
	 * 查询工作台模块数据 
	 * @throws Exception
	 */
	@Action(value = "queryDashboardModel", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void queryDashboardModel() throws Exception {
		User user = (User) ServletActionContext.getRequest().getSession()
				.getAttribute(SystemConstants.USER_SESSION_KEY);
		String type = getRequest().getParameter("type");
		String treeNodeData = dashboardService.findDashboardModel(type, user.getId());
		HttpUtils.write(treeNodeData);
	}

	/**
	 * 为指导定角色设置可用角色组件
	 * @return
	 * @throws Exception
	 */
	@Action(value = "toRoleDashboardModel", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String toRoleDashboardModel() throws Exception {
		String roleId = getRequest().getParameter("roleId");
		getRequest().setAttribute("roleId", roleId);
		return dispatcher("/WEB-INF/page/dashboard/toRoleDashboardModel.jsp");
	}

	/**
	 * 
	 * @Title: queryRoleDashboardModel  
	 * @Description: 角色配置工作台组件  
	 * @author : shengwei.wang
	 * @date 2018年4月9日上午9:36:16
	 * @throws Exception
	 * void
	 * @throws
	 */
	@Action(value = "queryRoleDashboardModel", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void queryRoleDashboardModel() throws Exception {
		String roleId = getRequest().getParameter("roleId");
		String treeNodeData = "";
			treeNodeData = dashboardService.findRoleDashboardModel(roleId);
		HttpUtils.write(treeNodeData);
	}

	/**
	 * 保存用户添加的工作台组件
	 * @throws Exception
	 */
	@Action(value = "saveDashboard", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveDashboard() throws Exception {
		//保存用户值班台实现
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			User user = (User) ServletActionContext.getRequest().getSession().getAttribute(
					SystemConstants.USER_SESSION_KEY);
			String[] models = getRequest().getParameterValues("models[]");
			dashboardService.saveDashboardModel(user.getId(), models);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 移除组件
	 * @throws Exception
	 */
	@Action(value = "deleteDashboard", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void deleteDashboard() throws Exception {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String dashboardId = getRequest().getParameter("dashboardId");
			dashboardService.deleteDashboard(dashboardId);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}

	/**
	 * 保存指定角色可选用的工作台组件
	 * @throws Exception
	 */
	@Action(value = "saveRoleDashboardModel", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void saveRoleDashboardModel() throws Exception {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		try {
			String roleId = getRequest().getParameter("roleId");
			String[] models = getRequest().getParameterValues("models[]");
			dashboardService.saveRoleDashboardModel(roleId, models);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
}
