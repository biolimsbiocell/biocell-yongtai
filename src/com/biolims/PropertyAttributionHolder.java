package com.biolims;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyAttributionHolder {
	public  static String getAttribute(String attributeName){
		Properties prop = new Properties();
		String attributeValue = "";
		try {
			InputStream in = PropertyAttributionHolder.class.getClassLoader().getResourceAsStream("system.properties");
			prop.load(in);
			attributeValue =  prop.getProperty(attributeName);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return attributeValue;
		
	}
	public static void main(String[] args) {
		System.out.println(PropertyAttributionHolder.getAttribute("file.save.path"));
	}
}
