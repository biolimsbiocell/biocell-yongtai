package com.biolims.doc.updateplan.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.doc.updateplan.dao.DocUpdatePlanDao;
import com.biolims.doc.updateplan.model.DocUpdatePlan;
import com.biolims.util.BeanUtils;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class DocUpdatePlanService {
	@Resource
	private DocUpdatePlanDao docUpdatePlanDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findDocUpdatePlanList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return docUpdatePlanDao.selectDocUpdatePlanList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(DocUpdatePlan i) throws Exception {

		docUpdatePlanDao.saveOrUpdate(i);

	}
	public DocUpdatePlan get(String id) {
		DocUpdatePlan docUpdatePlan = commonDAO.get(DocUpdatePlan.class, id);
		return docUpdatePlan;
	}
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(DocUpdatePlan sc, Map jsonMap) throws Exception {
		if (sc != null) {
			docUpdatePlanDao.saveOrUpdate(sc);
		
			String jsonStr = "";
	}
   }
}
