package com.biolims.doc.updateplan.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.doc.updateplan.model.DocUpdatePlan;
import com.biolims.doc.updateplan.service.DocUpdatePlanService;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
@Namespace("/doc/docUpdatePlan")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class DocUpdatePlanAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "wdgl002";
	@Autowired
	private DocUpdatePlanService docUpdatePlanService;
	private DocUpdatePlan docUpdatePlan = new DocUpdatePlan();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showDocUpdatePlanList")
	public String showDocUpdatePlanList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/doc/updateplan/docUpdatePlan.jsp");
	}

	@Action(value = "showDocUpdatePlanListJson")
	public void showDocUpdatePlanListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = docUpdatePlanService.findDocUpdatePlanList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<DocUpdatePlan> list = (List<DocUpdatePlan>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("updateRate", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "docUpdatePlanSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogDocUpdatePlanList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/doc/updateplan/docUpdatePlanDialog.jsp");
	}

	@Action(value = "showDialogDocUpdatePlanListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogDocUpdatePlanListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = docUpdatePlanService.findDocUpdatePlanList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<DocUpdatePlan> list = (List<DocUpdatePlan>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("updateRate", "");
		map.put("state", "");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editDocUpdatePlan")
	public String editDocUpdatePlan() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			docUpdatePlan = docUpdatePlanService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "docUpdatePlan");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			docUpdatePlan.setCreateUser(user);
			docUpdatePlan.setCreateDate(new Date());
			docUpdatePlan.setState(SystemConstants.DIC_STATE_NEW);
			docUpdatePlan.setStateName(SystemConstants.DIC_STATE_NEW_NAME);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/doc/updateplan/docUpdatePlanEdit.jsp");
	}

	@Action(value = "copyDocUpdatePlan")
	public String copyDocUpdatePlan() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		docUpdatePlan = docUpdatePlanService.get(id);
		docUpdatePlan.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/doc/updateplan/docUpdatePlanEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = docUpdatePlan.getId();
		if(id!=null&&id.equals("")){
			docUpdatePlan.setId(null);
		}
		Map aMap = new HashMap();
		docUpdatePlanService.save(docUpdatePlan,aMap);
		return redirect("/doc/docUpdatePlan/editDocUpdatePlan.action?id=" + docUpdatePlan.getId());

	}

	@Action(value = "viewDocUpdatePlan")
	public String toViewDocUpdatePlan() throws Exception {
		String id = getParameterFromRequest("id");
		docUpdatePlan = docUpdatePlanService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/doc/updateplan/docUpdatePlanEdit.jsp");
	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public DocUpdatePlanService getDocUpdatePlanService() {
		return docUpdatePlanService;
	}

	public void setDocUpdatePlanService(DocUpdatePlanService docUpdatePlanService) {
		this.docUpdatePlanService = docUpdatePlanService;
	}

	public DocUpdatePlan getDocUpdatePlan() {
		return docUpdatePlan;
	}

	public void setDocUpdatePlan(DocUpdatePlan docUpdatePlan) {
		this.docUpdatePlan = docUpdatePlan;
	}


}
