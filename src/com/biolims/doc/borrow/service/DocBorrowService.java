package com.biolims.doc.borrow.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biolims.common.dao.CommonDAO;
import com.biolims.doc.borrow.dao.DocBorrowDao;
import com.biolims.doc.borrow.model.DocBorrow;
import com.biolims.doc.borrow.model.DocBorrowItem;
import com.biolims.log.annotation.WriteExOperLog;
import com.biolims.log.annotation.WriteOperLog;
import com.biolims.log.annotation.WriteOperLogTable;
import com.biolims.util.JsonUtils;

@Service
@SuppressWarnings("unchecked")
@Transactional
public class DocBorrowService {
	@Resource
	private DocBorrowDao docBorrowDao;
	@Resource
	private CommonDAO commonDAO;
	StringBuffer json = new StringBuffer();
	public Map<String, Object> findDocBorrowList(Map<String, String> mapForQuery, Integer startNum,
			Integer limitNum, String dir, String sort) {
		return docBorrowDao.selectDocBorrowList(mapForQuery, startNum, limitNum, dir, sort);
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(DocBorrow i) throws Exception {

		docBorrowDao.saveOrUpdate(i);

	}
	public DocBorrow get(String id) {
		DocBorrow docBorrow = commonDAO.get(DocBorrow.class, id);
		return docBorrow;
	}
	public Map<String, Object> findDocBorrowItemList(String scId, Integer startNum, Integer limitNum, String dir,
			String sort) throws Exception {
		Map<String, Object> result = docBorrowDao.selectDocBorrowItemList(scId, startNum, limitNum, dir, sort);
		List<DocBorrowItem> list = (List<DocBorrowItem>) result.get("list");
		return result;
	}
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void saveDocBorrowItem(DocBorrow sc, String itemDataJson) throws Exception {
		List<DocBorrowItem> saveItems = new ArrayList<DocBorrowItem>();
		List<Map<String, Object>> list = JsonUtils.toListByJsonArray(itemDataJson, List.class);
		for (Map<String, Object> map : list) {
			DocBorrowItem scp = new DocBorrowItem();
			// 将map信息读入实体类
			scp = (DocBorrowItem) docBorrowDao.Map2Bean(map, scp);
			if (scp.getId() != null && scp.getId().equals(""))
				scp.setId(null);
			scp.setDocBorrow(sc);

			saveItems.add(scp);
		}
		docBorrowDao.saveOrUpdateAll(saveItems);
	}
	/**
	 * 删除明细
	 * @param ids
	 * @throws Exception
	 */
	@WriteOperLog
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void delDocBorrowItem(String[] ids) throws Exception {
		for (String id : ids) {
			DocBorrowItem scp =  docBorrowDao.get(DocBorrowItem.class, id);
			 docBorrowDao.delete(scp);
		}
	}
	
	@WriteOperLogTable
	@WriteExOperLog
	@Transactional(rollbackFor = Exception.class)
	public void save(DocBorrow sc, Map jsonMap) throws Exception {
		if (sc != null) {
			docBorrowDao.saveOrUpdate(sc);
		
			String jsonStr = "";
			jsonStr = (String)jsonMap.get("docBorrowItem");
			if (jsonStr != null && !jsonStr.equals("{}") && !jsonStr.equals("")) {
				saveDocBorrowItem(sc, jsonStr);
			}
	}
   }
}
