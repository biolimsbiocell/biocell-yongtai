package com.biolims.doc.borrow.action;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.biolims.common.action.BaseActionSupport;
import com.biolims.common.constants.SystemConstants;
import com.biolims.doc.borrow.model.DocBorrow;
import com.biolims.doc.borrow.model.DocBorrowItem;
import com.biolims.doc.borrow.service.DocBorrowService;
import com.biolims.util.DateHelper;
import com.biolims.util.JsonUtils;
import com.biolims.util.SendData;
import com.biolims.util.HttpUtils;
import com.biolims.common.model.user.User;
import com.biolims.file.service.FileInfoService;
@Namespace("/doc/borrow/docBorrow")
@Controller
@Scope("prototype")
@ParentPackage("default")
@SuppressWarnings("unchecked")
public final class DocBorrowAction extends BaseActionSupport {

	private static final long serialVersionUID = 3488450258677393696L;
	private String rightsId = "wdgl003";
	@Autowired
	private DocBorrowService docBorrowService;
	private DocBorrow docBorrow = new DocBorrow();
	@Resource
	private FileInfoService fileInfoService;
	@Action(value = "showDocBorrowList")
	public String showDocBorrowList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/doc/borrow/docBorrow.jsp");
	}

	@Action(value = "showDocBorrowListJson")
	public void showDocBorrowListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = docBorrowService.findDocBorrowList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<DocBorrow> list = (List<DocBorrow>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}

	@Action(value = "docBorrowSelect", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDialogDocBorrowList() throws Exception {
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_LIST);
		return dispatcher("/WEB-INF/page/doc/borrow/docBorrowDialog.jsp");
	}

	@Action(value = "showDialogDocBorrowListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDialogDocBorrowListJson() throws Exception {
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		String dir = getParameterFromRequest("dir");
		String sort = getParameterFromRequest("sort");
		String data = getParameterFromRequest("data");
		Map<String, String> map2Query = new HashMap<String, String>();
		if (data != null && data.length() > 0)
			map2Query = JsonUtils.toObjectByJson(data, Map.class);
		Map<String, Object> result = docBorrowService.findDocBorrowList(map2Query, startNum, limitNum, dir, sort);
		Long count = (Long) result.get("total");
		List<DocBorrow> list = (List<DocBorrow>) result.get("list");

		Map<String, String> map = new HashMap<String, String>();
		map.put("id", "");
		map.put("name", "");
		map.put("createUser-id", "");
		map.put("createUser-name", "");
		map.put("createDate", "yyyy-MM-dd");
		map.put("state", "");
		map.put("confirmUser-id", "");
		map.put("confirmUser-name", "");
		map.put("confirmDate", "yyyy-MM-dd");
		map.put("stateName", "");
		map.put("note", "");
		new SendData().sendDateJson(map, list, count, ServletActionContext.getResponse());
	}



	@Action(value = "editDocBorrow")
	public String editDocBorrow() throws Exception {
		String id = getParameterFromRequest("id");
		long num = 0;
		if (id != null && !id.equals("")) {
			docBorrow = docBorrowService.get(id);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_MODIFY);
			num = fileInfoService.findFileInfoCount(id, "docBorrow");
		} else {
			User user = (User) this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
			docBorrow.setCreateUser(user);
			docBorrow.setCreateDate(new Date());
			docBorrow.setState(SystemConstants.DIC_STATE_NEW);
			docBorrow.setStateName(SystemConstants.DIC_STATE_NEW_NAME);
			putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_ADD);
			toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_ADD);
		}
		putObjToContext("fileNum", num);
		return dispatcher("/WEB-INF/page/doc/borrow/docBorrowEdit.jsp");
	}

	@Action(value = "copyDocBorrow")
	public String copyDocBorrow() throws Exception {
		String id = getParameterFromRequest("id");
		String handlemethod = getParameterFromRequest("handlemethod");
		docBorrow = docBorrowService.get(id);
		docBorrow.setId("");
		handlemethod = SystemConstants.PAGE_HANDLE_METHOD_ADD;
		toToolBar(rightsId, "", "", handlemethod);
		toSetStateCopy();
		return dispatcher("/WEB-INF/page/doc/borrow/docBorrowEdit.jsp");
	}


	@Action(value = "save")
	public String save() throws Exception {
		String id = docBorrow.getId();
		if(id!=null&&id.equals("")){
			docBorrow.setId(null);
		}
		Map aMap = new HashMap();
			aMap.put("docBorrowItem",getParameterFromRequest("docBorrowItemJson"));
		
		docBorrowService.save(docBorrow,aMap);
		return redirect("/doc/borrow/docBorrow/editDocBorrow.action?id=" + docBorrow.getId());

	}

	@Action(value = "viewDocBorrow")
	public String toViewDocBorrow() throws Exception {
		String id = getParameterFromRequest("id");
		docBorrow = docBorrowService.get(id);
		putObjToContext("handlemethod", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		toToolBar(rightsId, "", "", SystemConstants.PAGE_HANDLE_METHOD_VIEW);
		return dispatcher("/WEB-INF/page/doc/borrow/docBorrowEdit.jsp");
	}
	

	@Action(value = "showDocBorrowItemList", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public String showDocBorrowItemList() throws Exception {
		return dispatcher("/WEB-INF/page/doc/borrow/docBorrowItem.jsp");
	}

	@Action(value = "showDocBorrowItemListJson", interceptorRefs = @InterceptorRef("biolimsDefaultStack"))
	public void showDocBorrowItemListJson() throws Exception {
		// 开始记录数
		int startNum = Integer.parseInt(getParameterFromRequest("start"));
		// limit
		int limitNum = Integer.parseInt(getParameterFromRequest("limit"));
		// 字段
		String dir = getParameterFromRequest("dir");
		// 排序方式
		String sort = getParameterFromRequest("sort");
		try {
			String scId = getRequest().getParameter("id");
			Map<String, Object> result = docBorrowService.findDocBorrowItemList(scId, startNum, limitNum, dir,
					sort);
			Long total = (Long) result.get("total");
			List<DocBorrowItem> list = (List<DocBorrowItem>) result.get("list");
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", "");
			map.put("docId", "");
			map.put("num", "");
			map.put("note", "");
			map.put("docBorrow-name", "");
			map.put("docBorrow-id", "");
			new SendData().sendDateJson(map, list, total, ServletActionContext.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		/**
	 * 删除明细信息
	 * @throws Exception
	 */
	@Action(value = "delDocBorrowItem")
	public void delDocBorrowItem() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String[] ids = getRequest().getParameterValues("ids[]");
			docBorrowService.delDocBorrowItem(ids);
			map.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("success", false);
		}
		HttpUtils.write(JsonUtils.toJsonString(map));
	}
	



	public String getRightsId() {
		return rightsId;
	}

	public void setRightsId(String rightsId) {
		this.rightsId = rightsId;
	}

	public DocBorrowService getDocBorrowService() {
		return docBorrowService;
	}

	public void setDocBorrowService(DocBorrowService docBorrowService) {
		this.docBorrowService = docBorrowService;
	}

	public DocBorrow getDocBorrow() {
		return docBorrow;
	}

	public void setDocBorrow(DocBorrow docBorrow) {
		this.docBorrow = docBorrow;
	}


}
