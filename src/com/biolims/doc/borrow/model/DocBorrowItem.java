package com.biolims.doc.borrow.model;

import java.io.Serializable;
import java.util.Date;
import java.lang.Integer;
import java.lang.String;
import java.lang.Double;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import com.biolims.dao.EntityDao;
/**   
 * @Title: Model
 * @Description: 文档借阅申请明细
 * @author lims-platform
 * @date 2017-11-30 11:08:40
 * @version V1.0   
 *
 */
@Entity
@Table(name = "DOC_BORROW_ITEM")
@SuppressWarnings("serial")
public class DocBorrowItem extends EntityDao<DocBorrowItem> implements java.io.Serializable {
	/**编号*/
	private String id;
	/**文档编号*/
	private String docId;
	/**数量*/
	private String num;
	/**备注*/
	private String note;
	/**相关主表*/
	private DocBorrow docBorrow;
	/**
	 *方法: 取得String
	 *@return: String  编号
	 */
	
	@Id
	@Column(name ="ID", length = 50)
	public String getId(){
		return this.id;
	}
	/**
	 *方法: 设置String
	 *@param: String  编号
	 */
	public void setId(String id){
		this.id = id;
	}
	/**
	 *方法: 取得String
	 *@return: String  文档编号
	 */
	@Column(name ="DOC_ID", length = 100)
	public String getDocId(){
		return this.docId;
	}
	/**
	 *方法: 设置String
	 *@param: String  文档编号
	 */
	public void setDocId(String docId){
		this.docId = docId;
	}
	/**
	 *方法: 取得String
	 *@return: String  数量
	 */
	@Column(name ="NUM", length = 50)
	public String getNum(){
		return this.num;
	}
	/**
	 *方法: 设置String
	 *@param: String  数量
	 */
	public void setNum(String num){
		this.num = num;
	}
	/**
	 *方法: 取得String
	 *@return: String  备注
	 */
	@Column(name ="NOTE", length = 51)
	public String getNote(){
		return this.note;
	}
	/**
	 *方法: 设置String
	 *@param: String  备注
	 */
	public void setNote(String note){
		this.note = note;
	}
	/**
	 *方法: 取得DocBorrow
	 *@return: DocBorrow  相关主表
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "DOC_BORROW")
	public DocBorrow getDocBorrow(){
		return this.docBorrow;
	}
	/**
	 *方法: 设置DocBorrow
	 *@param: DocBorrow  相关主表
	 */
	public void setDocBorrow(DocBorrow docBorrow){
		this.docBorrow = docBorrow;
	}
}