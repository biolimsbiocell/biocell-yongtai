﻿ 			Ext.MessageBox.buttonText.ok = biolims.common.confirm;
            Ext.MessageBox.buttonText.yes = biolims.common.yes;
            Ext.MessageBox.buttonText.no = biolims.common.no;
            Ext.MessageBox.buttonText.cancel = biolims.common.cancel;

window.onerror = function() {
	if (typeof console == "object" && typeof console.log == "function") {
		//console.log(arguments);
	} else {
		return true;
	}

};
window.log = function() {
	if (typeof console == "object" && typeof console.log == "function") {
		//console.log(arguments);
	} else {
		return true;
	}
};

/*
 * 用于表单文本框效果的js
 * 
 * 
 */

function replaceAll(strOrg, strFind, strReplace) {
	var index = 0;
	while (strOrg.indexOf(strFind, index) != -1) {
		strOrg = strOrg.replace(strFind, strReplace);
		index = strOrg.indexOf(strFind, index);
	}
	return strOrg;
}

function lpLsp(value) {
	return value.replace(/\\n\\r/g, '<br>');
}
// 倪毅 2011-12-20注释
// function rpLsp(value){return value.replace(/\\r\\n/g,'<br>');}
function rpLsp(value) {
	return value.replace(/\\r\\n/g, ' ');
}
// replace line separator
function rpHtmlLsp(value) {
	return value.replace(/\<br\/\>/gi, '\n');
}

function operReturn(value) {

	return value.replace(/\r\n/g, '<BR>');
}

function shared_onfocus(evt, ctrlInst) {
	el = evt.srcElement ? evt.srcElement : evt.target;

	if (el.readOnly == true) {
		ctrlInst.select();
		return false;
	}
	// alert("ddd");
	// setfocusField(el);
	// el.style.backgroundImage="url()" ;
	// el.style.backgroundColor=FIELD_FOCUS_COLOR;

}

function textbox_ondeactivate(evt) {

	el = evt.srcElement ? evt.srcElement : evt.target;
	if (el.readOnly == true) {
		if (!undef(evt)) {
			evt.returnValue = false;
			evt.cancelBubble = true;
		}
		el.style.backgroundColor = "";
		return false;
	}
	el.style.backgroundColor = "";
	if (el.numeric == "numeric")
		el.style.textAlign = "right";

}

function undef(p) {
	var bUndef = false;
	switch (typeof (p)) {
	case "undefined":
		bUndef = true;
		break;
	case "null":
		bUndef = true;
		break;
	case "object":
		if (p == null)
			bUndef = true;
		break;
	case "number":
		if (null == p)
			bUndef = true;
		break;
	case "string":
		if ("" == p)
			bUndef = true;
		if ("null" == p)
			bUndef = true;
		if ("undefined" == p)
			bUndef = true;
		break;
	}
	return bUndef;
}

var READ_ONLY_TAB_INDEX = "-1";
var READ_ONLY_DEFAULT_FOCUS = "false";
var RECORD_INSERT_COLOR = "#F5F0D5";
var FIELD_FOCUS_COLOR = "#F5F0D5";
var FIELD_FOCUS_READ_ONLY_COLOR = "#CCC18E";

function settextreadonly(name) {

	document.getElementById(name).className = "input_parts text input default readonlytrue false";
	document.getElementById(name).readOnly = true;
}
function settextread(name) {

	document.getElementById(name).className = "input_parts text input default  readonlyfalse false";
	document.getElementById(name).readOnly = false;
}

function settextreadonlyJquery(el) {
	el.addClass("readonlytrue");
	el.attr("readonly", "readonly");
}

function settextreadonlyByAllJquery() {
	jQuery(":text, textarea").each(function() {
		var _vId = jQuery(this).attr('id');
		if (!$(this).attr("class") || $(this).attr("class").indexOf("x-tbar-page-number") == -1) {
			jQuery(this).addClass("readonlytrue");
			jQuery(this).attr("readonly", "readonly");

			if (_vId == 'actiondropdown_textbox')
				settextread(_vId);
		}
	});

}
function settextreadonlyByAll() {
	jQuery(":text, textarea").each(function() {
		var _vId = jQuery(this).attr('id');
		jQuery(this).removeClass();
		jQuery(this).addClass("input_parts text input default readonlytrue false");
		jQuery(this).attr("readonly", "readOnly");

		if (_vId == 'actiondropdown_textbox')
			settextread(_vId);
	});

}

function settextreadonlyById(t) {

	jQuery(t).removeClass();
	jQuery(t).addClass("input_parts text input default readonlytrue false");
	jQuery(t).attr("readonly", "readOnly");

}

function undisplayview(divName, maxName, minName) {

	document.getElementById(divName).style.display = 'none';
	document.getElementById(maxName).style.display = 'block';
	document.getElementById(minName).style.display = 'none';
}
function displayview(divName, maxName, minName) {

	document.getElementById(divName).style.display = 'block';
	document.getElementById(maxName).style.display = 'none';
	document.getElementById(minName).style.display = 'block';
}

function jstjview() {

	document.getElementById('closesearch').style.display = 'block';
	document.getElementById('opensearch').style.display = 'none';
	document.getElementById('jstj').style.display = 'block';

}
function jstjnoview() {

	document.getElementById('closesearch').style.display = 'none';
	document.getElementById('opensearch').style.display = 'block';
	document.getElementById('jstj').style.display = 'none';

}

function firstCommonValue(gridName) {
	var gridCount = gridName.store.getCount();
	for ( var ij = 0; ij < gridCount; ij++) {
		var record = gridName.store.getAt(ij);
		record.set("id", "");
	}
	return true;
}

function commonModifyData(params, paramsValue, url, gridName) {

	if (paramsValue != '') {
		params = params + ',' + paramsValue;
	}
	params = '({' + params + '})';
	params = eval(params);
	var myMask = new Ext.LoadMask(Ext.getBody(), {
		msg : biolims.common.pleaseWait
	});
	myMask.show();
	Ext.Ajax.request({
		url : url,
		method : 'POST',
		params : params,
		success : function(response) {
			myMask.hide();
			gridName.store.reload();
		},
		failure : function(response) {
			myMask.hide();
			Ext.MessageBox.show({
				title : biolims.common.message,
				msg : biolims.common.failed,
				width : 300,
				buttons : Ext.MessageBox.OK,
				icon : Ext.MessageBox.WARNING
			});
			gridName.store.reload();
		}
	});
}

function commonDelData(params, url, gridName) {
	params = '({' + params + '})';
	params = eval(params);
	var myMask = new Ext.LoadMask(Ext.getBody(), {
		msg : biolims.common.pleaseWait
	});
	myMask.show();
	Ext.Ajax.request({
		url : url,
		method : 'POST',
		params : params,
		success : function(response) {
			myMask.hide();
			gridName.store.reload();
		},
		failure : function(response) {
			myMask.hide();
			Ext.MessageBox.show({
				title : biolims.common.message,
				msg : biolims.common.failed,
				width : 300,
				buttons : Ext.MessageBox.OK,
				icon : Ext.MessageBox.WARNING
			});
			gridName.store.reload();
		}
	});
}



function commonSave(gridName) {
	var record = gridName.store.getModifiedRecords();
	if (!record || record == '' || record.length == 0) {
		return true;
	} else {
		var selData = '';
		for ( var ij = 0; ij < record.length; ij++) {

			if (record[ij].get('id') != '-1000') {

				if (validateFun(record[ij]) == false) {
					return false;
				}
				var array = record[ij].data;

				selData = selData + Ext.util.JSON.encode(array) + ',';
			}
		}
		if (selData.length > 0) {
			selData = selData.substring(0, selData.length - 1);
		}
		var data = '[' + selData + ']';
		data = rpLsp(data);
		var params = "data:'" + data + "'";
		modifyData(params);
	}
	gridName.store.commitChanges();
	return true;
}

function openDialogExt(url) {
	url = url + "&noButton=true";
	var localExtWindow= new Ext.Window({
		id:'localExtWindowId',modal:true,title:biolims.common.detail,layout:'fit',width:document.body.clientWidth/1.2,height:document.body.clientHeight/1.2,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
		html:"<iframe scrolling='no' name='maincontentframe' src='"+url+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
		buttons: [
		          { text: biolims.common.close,
		        	  handler: function(){
		        	 localExtWindow.close(); }  }]  });     localExtWindow.show();
}

function openDialog(url) {

	url = url + "&noButton=true";
	if (window.ActiveXObject) {
		window.showModalDialog(url, biolims.common.detail, 'dialogWidth:1000px,dialogHeight:600px,resizable=yes,status=no');
	}else{
		window.open(url, '', '');
	}
}
function openDialogWork(url) {
	
		window.open(url, '', '');
	
}
window.loadDialogPage = function(el, title, url, buttons, modal, options) {

	var dom;

	if (el) {
		dom = $(el);
	} else {
		dom = $("<div style='display:none;'></div").appendTo(document.body);
	}

	dom.attr("title", title);
	buttons = buttons || [];
	if (typeof buttons[biolims.common.cancel] == "undefined") {
		buttons[biolims.common.cancel] = function() {
			dom.dialog("close");
		};
	}

	options = options || {};
	options.closeText = "";
	options.modal = !!modal;
	options.buttons = buttons;
	
	if (typeof options.height != 'undefined') {
		if (window.ActiveXObject) {
			// IE浏览器
			var s = options.height+"";
			//if(s.indexOf("px")==-1){
			//	options.height = options.height + "px";
			//}	
		}
	}

	if (typeof options.height != 'undefined'){
		options.minHeight = options.height;
		
		
	}
		

	options.resizable = true;// DIALOG是否允许改变大小
	if (typeof options.close == "undefined") {
		options.close = function(event, ui) {
			dom.dialog("destroy");
			if (!el || el == null) {
				dom.remove();
			}
		};
	}

	if (url == null) {
		dom.dialog(options);
	} else {
		dom.load(ctx + url, options.data, function(res, status, xhr) {
			dom.dialog(options);
		});
	}

	return dom;
};



window.loadDialogPageNo = function(el, title, url, buttons, modal, options) {

	var dom;

	if (el) {
		dom = $(el);
	} else {
		dom = $("<div style='display:none;'></div").appendTo(document.body);
	}

	dom.attr("title", title);
	buttons = buttons || [];
	if (typeof buttons[biolims.common.cancel] == "undefined") {
		buttons[biolims.common.cancel] = function() {
			dom.dialog("close");
		};
	}

	options = options || {};
	options.closeText = "";
	options.modal = !!modal;
	options.buttons = buttons;
	
	
	if (typeof options.height != 'undefined'){
		options.minHeight = options.height;
		
		
	}
		

	options.resizable = true;// DIALOG是否允许改变大小
	if (typeof options.close == "undefined") {
		options.close = function(event, ui) {
			dom.dialog("destroy");
			if (!el || el == null) {
				dom.remove();
			}
		};
	}

	if (url == null) {
		dom.dialog(options);
	} else {
		dom.load(ctx + url, options.data, function(res, status, xhr) {
			dom.dialog(options);
		});
	}

	return dom;
};





window.ajax = function(method, url, data, callback, options) {

	options = options || {};

	options.type = method.toUpperCase();
	options.url = ctx + url;
	options.data = data;
	options.contentType = "application/x-www-form-urlencoded; charset=utf-8";
	options.async = false;
	options.dataType = "json";

	options.success = callback;
	$.ajax(options);
};
window.load = function(url, data, toEl, callback) {

	if (!toEl) {
		toEl = $("<div></div>");
		$(document.body).append(toEl);
	}

	$(toEl).load(ctx + url, data, function(resp, textStatus, xhr) {
		if (textStatus == 'success') {
			if ((typeof callback) == "function") {
				callback();
			}
		} else {
			$(toEl).html(resp);
		}
	});
};
/*
 * toEl:要显示GRID的DIV
 * cols:对象{fields:[],cm:[],sm:true/false}fields:store中字段名称，cm:columnModel对字段的定义,sm:是否添加复选框
 * loadParam:定义store HttpProxy对象的属性.最基本的要有请求URL opts:定义GRID所须属性
 */
window.gridTable = function(toEl, cols, loadParam, opts) {
	Ext.QuickTips.init();
	Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
	Ext.grid.RowNumberer = Ext.extend(Ext.grid.RowNumberer, {
		width : 30,
		renderer : function(value, cellmeta, record, rowIndex, columnIndex, store) {
			if (!store.lastOptions.params)
				return rowIndex + 1;
			if (!store.lastOptions.params.start)
				return rowIndex + 1;
			return store.lastOptions.params.start + rowIndex + 1;
		}
	});

	var storeOpts = {};
	storeOpts.root = 'results';
	storeOpts.totalProperty = 'total';
	storeOpts.remoteSort = true;
	storeOpts.fields = cols.fields;

	var proxyOpt = {};
	proxyOpt.url = loadParam.url;
	var params = loadParam.params;
	if (params != 'undefined' && typeof data === 'object') {
		proxyOpt.extraParams = params;
	}
	proxyOpt.method = 'POST';
	storeOpts.proxy = new Ext.data.HttpProxy(proxyOpt);
	var store = new Ext.data.JsonStore(storeOpts);

	if (opts === null)
		opts = {};
	var options = {};
	options.bodyStyle = "width:100%";
	if (typeof opts.width == 'undefined')
		options.autoWidth = true;
	if (opts.id == 'undefined') {
		options.id = 'girdGrid';
	} else {
		options.id = opts.id;
	}
	options.titleCollapse = true;
	options.autoScroll = true;
	// options.autoExpandColumn = true;
	if (typeof opts.height == 'undefined') {
		var bodyHeight = document.documentElement.clientHeight - 35;
		if (bodyHeight == 0)
			bodyHeight = document.body.clientHeight - 35;
		opts.height = bodyHeight;
	}
	options.store = store;
	options.columnLines = true;
	options.trackMouseOver = true;
	options.loadMask = true;
	var cm = cols.cm;
	if (cols.sm) {
		var sm = new Ext.grid.CheckboxSelectionModel();
		cm.unshift(sm);
		cm.unshift(new Ext.grid.RowNumberer());
		options.selModel = sm;
		options.columns = cm;
	} else {
		cm.unshift(new Ext.grid.RowNumberer());
		options.colModel = new Ext.ux.grid.LockingColumnModel(cm);
		options.view = new Ext.ux.grid.LockingGridView();
	}

	options.stripeRows = true;
	options.viewConfig = {};
	// options.viewConfig.forceFit = true;
	options.viewConfig.enableRowBody = false;
	options.viewConfig.showPreview = false;
	var bbarOpts = {};
	if (typeof loadParam.limit != 'undefined') {
		bbarOpts.pageSize = loadParam.limit;
	} else {
		var bodyHeight = document.documentElement.clientHeight;
		if (bodyHeight == 0)
			bodyHeight = document.body.clientHeight;
		bbarOpts.pageSize = parseInt((bodyHeight - 50) > 0 ? (bodyHeight - 50) / 24 : bodyHeight);

	}
	bbarOpts.store = store;
	bbarOpts.displayInfo = true;
	bbarOpts.displayMsg = biolims.common.displayMsg;
	bbarOpts.beforePageText = biolims.common.page;
	bbarOpts.afterPageText = biolims.common.afterPageText;
	bbarOpts.emptyMsg = biolims.common.noData;
	bbarOpts.plugins = [new Ext.ui.plugins.ComboPageSize({ addToItem: false, prefixText: biolims.common.show, postfixText:biolims.common.entris})];
	var bbar = new Ext.PagingToolbar(bbarOpts);

	options.bbar = bbar;
	options.renderTo = toEl;
	if (opts != null) {
		for ( var key in opts) {
			if (typeof opts[key] != "function") {
				options[key] = opts[key];
			}
		}
	}
	var grid = new Ext.grid.GridPanel(options);
	var loadParamOpt = {};
	loadParamOpt.start = 0;
	if (typeof loadParam.limit != 'undefined') {
		loadParamOpt.limit = loadParam.limit;
	} else {
		loadParamOpt.limit = bbarOpts.pageSize;
	}
	
	if(document.getElementById('extJsonDataString')){
		
		store.on('beforeload', function () { store.baseParams = {data: document.getElementById('extJsonDataString').value};  });
	}

	store.load({
		params : loadParamOpt
	});

	if (typeof opts.rowselect != 'undefined' && typeof opts.rowselect == 'function') {
		grid.getSelectionModel().on('rowselect', function(sm, rowIdx, r) {
			opts.rowselect(r.data.id, r.data);
		});
	}

	if (typeof opts.rowdblclick != 'undefined' && typeof opts.rowdblclick == 'function') {
		grid.on('rowdblclick', function() {
			var record = grid.getSelectionModel().getSelections();
			opts.rowdblclick(record[0].data.id, record[0]);
		});
	}
	grid.getSelectRecord = function() {
		return this.getSelectionModel().getSelections();
	};
	grid.getLimitInfo = function() {
		return loadParamOpt;
	};
	grid.getTotal = function() {
		return grid.store.totalLength;
	};
	return grid;
};

/*
 * toEl:要显示GRID的DIV
 * cols:对象{fields:[],cm:[],sm:true/false}fields:store中字段名称，cm:columnModel对字段的定义,sm:是否添加复选框
 * loadParam:定义store HttpProxy对象的属性.最基本的要有请求URL --- opts:定义GRID所须属性-
 * operParams:自定义属性
 */
window.gridEditTable = function(toEl, cols, loadParam, opts, operParams) {
	var grid;
	Ext.QuickTips.init();
	Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
	Ext.grid.RowNumberer = Ext.extend(Ext.grid.RowNumberer, {
		width : 30,
		renderer : function(value, cellmeta, record, rowIndex, columnIndex, store) {
			if (!store.lastOptions.params)
				return rowIndex + 1;
			if (!store.lastOptions.params.start)
				return rowIndex + 1;
			return store.lastOptions.params.start + rowIndex + 1;
		}
	});

	var storeOpts = {};
	storeOpts.root = 'results';
	storeOpts.totalProperty = 'total';
	storeOpts.remoteSort = true;
	storeOpts.fields = cols.fields;

	var proxyOpt = {};
	proxyOpt.url = loadParam.url;
	var params = loadParam.params;
	if (params != 'undefined' && typeof data === 'object') {
		proxyOpt.extraParams = params;
	}
	proxyOpt.method = 'POST';

	storeOpts.proxy = new Ext.data.HttpProxy(proxyOpt);
	var store = new Ext.data.JsonStore(storeOpts);
	store.pruneModifiedRecords = true;
	if (opts === null)
		opts = {};
	var options = {};
	options.bodyStyle = "width:100%";
	if (typeof opts.width == 'undefined')
		options.autoWidth = true;
	if (opts.id == 'undefined') {
		options.id = 'girdGrid';
	} else {
		options.id = opts.id;
	}
	options.titleCollapse = true;
	options.autoScroll = true;
	// options.autoExpandColumn = true;
	options.clicksToEdit = 1;
	if (typeof opts.height == 'undefined') {
		var bodyHeight = document.documentElement.clientHeight-35;
		if (bodyHeight == 0)
			bodyHeight = document.body.clientHeight-35;
		opts.height = bodyHeight;
	}
	options.store = store;
	options.columnLines = true;
	options.trackMouseOver = true;
	options.loadMask = true;
	var cm = cols.cm;
	if (cols.sm) {
		var sm = new Ext.grid.CheckboxSelectionModel({
			singleSelect : false
		});
		cm.unshift(sm);
		cm.unshift(new Ext.grid.RowNumberer());
		options.selModel = sm;
		if(cols.locksm){
			options.colModel = new Ext.ux.grid.LockingColumnModel(cm);
			options.view = new Ext.ux.grid.LockingGridView();	
		}else{
			options.columns = cm;
			
		}
			
		
		
		
	} else {
		cm.unshift(new Ext.grid.RowNumberer());
		options.colModel = new Ext.ux.grid.LockingColumnModel(cm);
		options.view = new Ext.ux.grid.LockingGridView();
	}
	
	options.stripeRows = true;
	options.viewConfig = {};
	// options.viewConfig.forceFit = true;
	// options.viewConfig.enableRowBody = false;
	// options.viewConfig.showPreview = false;
	var bbarOpts = {};

	if (typeof loadParam.limit != 'undefined') {
		bbarOpts.pageSize = loadParam.limit;
	} else {
		var bodyHeight = document.documentElement.clientHeight;
		if (bodyHeight == 0)
			bodyHeight = document.body.clientHeight;
		bbarOpts.pageSize = parseInt((bodyHeight - 35) > 0 ? (bodyHeight - 35) / 24 : bodyHeight);
	}
	bbarOpts.store = store;
	bbarOpts.displayInfo = true;
	bbarOpts.displayMsg = biolims.common.displayMsg;
	bbarOpts.beforePageText = biolims.common.page;
	bbarOpts.afterPageText = biolims.common.afterPageText;
	bbarOpts.emptyMsg = biolims.common.noData;
	bbarOpts.plugins = [new Ext.ui.plugins.ComboPageSize({ addToItem: false, prefixText: biolims.common.show, postfixText:biolims.common.entris})];
	var bbar = new Ext.PagingToolbar(bbarOpts);
	options.bbar = bbar;

	var tbarOpts = [];
	if (typeof operParams == 'undefined' || operParams.isShowDefaultTbar) {
		tbarOpts.push({
			text : biolims.common.fillDetail,
			iconCls : 'add',
			handler : function() {
				var ob = grid.getStore().recordType;
				var p = new ob({});
				p.isNew = true;
				grid.stopEditing();
				store.insert(0, p);
				grid.startEditing(0, 0);

			}
		});

		tbarOpts.push({
			text : biolims.common.uncheck,
			handler : function() {
				grid.getSelectionModel().clearSelections();
			}
		});

		tbarOpts.push({
			text : biolims.common.delSelected,
			handler : function() {
				grid.stopEditing();
				var record = grid.getSelectionModel().getSelections();
				if (record.length <= 0) {
					Ext.MessageBox.alert(biolims.common.prompt, biolims.common.pleaseSelectRecord, '');
				} else {
					Ext.MessageBox.show({
						title : biolims.common.prompt,
						msg : biolims.common.confirm2Del + record.length + biolims.common.record,
						buttons : Ext.MessageBox.OKCANCEL,
						closable : false,
						fn : function(btn) {
							if (btn == 'ok') {
								var delIds = [];
								var records = [];
								for ( var i = 0; i < record.length; i++) {
									store.remove(record[i]);
									if (!record[i].isNew) {
										delIds.push(record[i].get("id"));
										records.push(record[i]);
									}
								}
								if (typeof opts.delSelect != 'undefined' && typeof opts.delSelect == 'function') {
									if (delIds.length > 0) {
										opts.delSelect(delIds, records);
									}
								}
							}
						}
					});
				}
			}
		});

		tbarOpts.push({
			text : biolims.common.editableColAppear,
			handler : function() {
				setEditShow();
			}
		});
	}

	options.renderTo = toEl;
	if (opts != null) {
		for ( var key in opts) {
			if (key == 'tbar') {
				if (opts[key].length > 0) {
					for ( var i = 0; i < opts[key].length; i++) {
						var obj = opts[key][i];
						var oper = true;
						for ( var j = 0; j < tbarOpts.length; j++) {
							var objm = tbarOpts[j];
							if (obj.text && objm && objm.text) {
								if (obj.text === objm.text && obj.handler == null) {
									tbarOpts.splice(j, 1);
									if (tbarOpts[j - 1] == '-')
										tbarOpts.splice(j - 1, 1);
									opts[key].splice(i, 1);
									oper = false;
									i = i - 1;
									j = j - 1;
								} else if (obj.text === objm.text) {
									tbarOpts[j] = obj;
									oper = false;
								}
							}
						}
						if (oper) {
							tbarOpts.push(obj);
						}
					}

				} else {
					tbarOpts = [];
				}
			} else if (typeof opts[key] != "function") {
				options[key] = opts[key];
			}
		}
	}
	if (tbarOpts.length > 0) {

		var tempTbar = [];
		$.each(tbarOpts, function(i, obj) {
			tempTbar.push("-");
			tempTbar.push(obj);
		});
		options.tbar = tempTbar;
	}

	grid = new Ext.grid.EditorGridPanel(options);
	var loadParamOpt = {};
	loadParamOpt.start = 0;
	if (typeof loadParam.limit != 'undefined') {
		loadParamOpt.limit = loadParam.limit;
	} else {
		loadParamOpt.limit = bbarOpts.pageSize;
	}

	store.load({
		params : loadParamOpt
	});

	if (typeof opts.rowselect != 'undefined' && typeof opts.rowselect == 'function') {
		grid.getSelectionModel().on('rowselect', function(sm, rowIdx, r) {
			opts.rowselect(r.data.id);
		});
	}

	if (typeof opts.rowdblclick != 'undefined' && typeof opts.rowdblclick == 'function') {
		grid.on('rowdblclick', function() {
			// console.log(grid.getSelectionModel().selection.record);
			var record = grid.getSelectionModel().selection.record;
			opts.rowdblclick(record.data.id);
		});
	}

	function setEditShow() {
		for ( var i = 0; i < grid.getColumnModel().getColumnCount(); i++) {
			if (grid.getColumnModel().isCellEditable(i)) {
				grid.getView().getHeaderCell(i).style.backgroundColor = RECORD_INSERT_COLOR;
			}
		}
	}

	setTimeout(function() {
		setEditShow();
	}, 1000);

	grid.copyRecord = function(id) {
		var record = grid.getSelectionModel().selection.record;
		return record.copy(id);
	};
	grid.getLimitInfo = function() {
		return loadParamOpt;
	};
	grid.getModifyRecord = function() {
		return grid.store.getModifiedRecords();
	};
	grid.getSelectOneRecord = function() {
		return grid.getSelectionModel().getSelected();
	};
	grid.getSelectRecord = function() {
		return grid.getSelectionModel().getSelections();
	};
	grid.getAllRecord = function() {
		return grid.store.getRange();
	};
	grid.getTotal = function() {
		return grid.store.totalLength;
	};
	return grid;
};
$(function() {
	$("#jstj td").attr("nowrap", "nowrap");
});

function commonPrint(pa) {
	var url = '/../report/frameset?__format=html&__svg=false&__locale=zh_CN&__designer=true&__masterpage=true&__rtl=false&__showtitle=false&';
	url = url + pa;
	openDialogWork(url);

}

/*
 * 批量黏贴格式化数据去掉前后空格及空行
 */
function formatData(dataArray) {
	var result = [];
	$.each(dataArray, function(i, obj) {
		if (obj.length > 0) {
			result.push($.trim(obj));
		}
	});
	return result;
}

function formatDataBarcode(dataArray) {
	var result = {};
	$.each(dataArray, function(i, obj) {
		if (obj.length > 0) {
			result[$.trim(obj)] = $.trim(obj);
		}
	});
	result.length = dataArray.length;
	return result;
}

function gridToObj(grid, column) {
	var records = grid.getAllRecord();
	var length = 0;
	var data = {};
	if (records.length > 0) {
		if (typeof column == "string") {
			$.each(records, function(i, obj) {
				if (obj.get(column)) {
					data[obj.get(column)] = 1;
					length++;
				}
			});
		} else {
			$.each(records, function(i, obj) {
				$.each(column, function(j, col) {
					if (obj.get(col))
						data[obj.get(col)] = 1;
				});
				length++;
			});
		}

	}
	return length > 0 ? data : "";
}





/**
 * 消息提示
 * 
 * @return
 */
function message(messg) {
	if (messg.length > 0) {
		Ext.MessageBox.show({
			title : biolims.common.message,
			msg : messg,
			buttons : Ext.MessageBox.OK,
			icon : biolims.common.info
		});
		return;
	}
}
function confirmMsg(msg, title, callback) {
	if (title)
		title = biolims.common.makeSure;
	var result = false;
	Ext.MessageBox.confirm(title, msg, function(button, text) {
		if (button == "yes") {
			callback();
		}
	});
}