function commonChangeState(paraStr) {
	var win = Ext.getCmp('changeState');
	if (win) {
		win.close();
	}
	var changeState = new Ext.Window({
		id : 'changeState',
		modal : true,
		title : biolims.common.changeState,
		layout : 'fit',
		width : 500,
		height : 400,
		closeAction : 'close',
		plain : true,
		bodyStyle : 'padding:5px;',
		buttonAlign : 'center',
		collapsible : true,
		maximizable : true,
		items : new Ext.BoxComponent({
			id : 'maincontent',
			region : 'center',
			html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='" + window.ctx
					+ "/applicationTypeAction/applicationTypeActionLook.action?" + paraStr
					+ "&flag=changeState' frameborder='0' width='100%' height='100%' ></iframe>"
		}),
		buttons : [ {
			text : 'Confirm',
			handler : function() {
				window.frames['maincontentframe'].agree();
			}
		}, {
			text : biolims.common.close,
			handler : function() {
				changeState.close();
			}
		} ]
	});
	changeState.show();
}
