var sysRemindGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'title',
		type:"string"
	});
	    fields.push({
		name:'remindUser',
		type:"string"
	});
	    fields.push({
		name:'readUser-id',
		type:"string"
	});
	    fields.push({
		name:'readUser-name',
		type:"string"
	});
	    fields.push({
		name:'startDate',
		type:"string"
	});
	    fields.push({
		name:'readDate',
		type:"string"
	});
	    fields.push({
		name:'content',
		type:"string"
	});
	    fields.push({
		name:'handleUser-id',
		type:"string"
	});
	    fields.push({
		name:'handleUser-name',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'contentId',
		type:"string"
	});
	    fields.push({
		name:'tableId',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编码',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'title',
		header:'提示标题',
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'remindUser',
		header:'提示人',
		width:10*10,
		sortable:true
	});
		cm.push({
		dataIndex:'readUser-id',
		hidden:true,
		header:'提示阅读人ID',
		width:10*10,
		sortable:true
		});
		cm.push({
		dataIndex:'readUser-name',
		header:'提示阅读人',
		width:10*10,
		sortable:true
		});
	cm.push({
		dataIndex:'startDate',
		header:'提示日期',
		width:12*10,
		sortable:true
	});
	cm.push({
		dataIndex:'readDate',
		header:'阅读日期',
		width:12*10,
		sortable:true
	});
	cm.push({
		dataIndex:'content',
		header:'内容',
		width:15*10,
		sortable:true
	});
		cm.push({
		dataIndex:'handleUser-id',
		hidden:true,
		header:'处理人ID',
		width:10*10,
		sortable:true
		});
		cm.push({
		dataIndex:'handleUser-name',
		header:'处理人',
		width:10*10,
		sortable:true
		});
	cm.push({
		dataIndex:'state',
		header:'状态',
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'contentId',
		header:'内容id',
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'tableId',
		header:'表单id',
		width:15*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/sysRemind/showSysRemindListJson.action";
	var opts={};
	opts.title="信息提醒";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	sysRemindGrid=gridTable("show_sysRemind_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/sysRemind/editSysRemind.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location=window.ctx+'/sysRemind/editSysRemind.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/sysRemind/viewSysRemind.action?id=' + id;
}
function exportexcel() {
	sysRemindGrid.title = '导出列表';
	var vExportContent = sysRemindGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
				commonSearchAction(sysRemindGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	});
});