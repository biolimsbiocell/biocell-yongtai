﻿$(function() {
	var taskgrid;
	var cols = {};
	var fields = [];

	fields.push({
		name : 'id',
		type : "string"
	});

	fields.push({
		name : 'title',
		type : "string"
	});
	
	 fields.push({
			name:'startDate',
			type : "string"
		});
		fields.push({
			name : 'applicationName',
			type : "string"
		});
	 
		fields.push({
			name : 'whmobile',
			type : "string"
		});
	 
	fields.push({
		name : 'wh',
		type : "string"
	});

	cols.fields = fields;
	var cm = [];
	cm.push({
		xtype : 'actioncolumn',
		width : 30,
		items : [ {
			icon : window.ctx + '/javascript/lib/ext-3.4.0/examples/shared/icons/fam/accept.png',
			tooltip : '是否已处理',
			handler : function(grid, rowIndex, colIndex) {
				var rec = grid.getStore().getAt(rowIndex);
				hasLookReload(rec.get('id'), grid);

				
			}
		} ]
	});
	cm.push({
		xtype : 'actioncolumn',
		width : 30,
		items : [ {
			icon : window.ctx + '/javascript/lib/ext-3.4.0/examples/shared/icons/fam/application_go.png',
			tooltip : '办理',
			handler : function(grid, rowIndex, colIndex) {
				var rec = grid.getStore().getAt(rowIndex);
				if(rec.get('wh')){
			
					
						location.href = window.ctx + rec.get('wh');
					

					
					
				}
			}
		} ]
	});
	cm.push({
		dataIndex : 'id',
		header : 'ID',
		width : 240,
		hidden : true
	});
	cm.push({
		dataIndex : 'title',
		header : '事件标题',
		width : 540,
		sortable : true
	});
	
	cm.push({
		dataIndex : 'applicationName',
		header : '模块名称',
		width : 100,
		sortable : true
	});
	cm.push({
		dataIndex:'startDate',
		header:'提醒日期',
		width:12*10
	});
	cols.cm = cm;
	
	var loadParam = {};
	loadParam.url = ctx + "/remind/getPreRemindList.action";
	// loadParam.limit = 10
	var opts = {};
	opts.id = "workflow_pre_remind" + new Date().getTime();
	opts.height = 400;
	
	opts.tbar = [];
	opts.tbar.push({
		text : "全部已处理",
		handler : function() {
			
			Ext.MessageBox.confirm("请确认", '确认已经处理了吗？', function(button, text) {
				if (button == "yes") {
			ajax("post", "/remind/handleAllMessage.action", null, function(data) {
				if (data.success) {
					message("处理成功！");
					taskgrid.getStore().commitChanges();
					taskgrid.getStore().reload();
				} else {
					message("处理失败！");
				}
			}, null);
				} else {
					return false;
				}
			});
		}
	});
	
	// opts.title = "重订购及预维护";
	taskgrid = gridTable("workflow_pre_remind_grid_div", cols, loadParam, opts);
	function hasLookReload(vId, gridName) {

		Ext.MessageBox.confirm("请确认", '确认已经处理了吗？', function(button, text) {
			if (button == "yes") {

				var myMask = new Ext.LoadMask(Ext.getBody(), {
					msg : "请等待..."
				});
				myMask.show();
				Ext.Ajax.request({
					url : window.ctx + '/remind/handleMessage.action',
					method : 'POST',
					params : {
						id : vId
					},
					success : function(response) {
						var respText = Ext.util.JSON.decode(response.responseText);
						myMask.hide();

						if (respText.message == '') {
							myMask.hide();
							gridName.store.reload();

						} else {
							myMask.hide();

							message(respText.message);

						}

					},
					failure : function(response) {
						myMask.hide();
					}
				});

				myMask.hide();

			} else {
				return false;
			}
		});

	}
});