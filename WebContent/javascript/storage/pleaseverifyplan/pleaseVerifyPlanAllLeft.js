//待入库样本-即库存主数据(树状带批次)
var storageOutAllTab;
hideLeftDiv();
var pleaseVerifyPlan_id = $("#pleaseVerifyPlan_id").text();
$("#btn_submit").hide();
$(function() {
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : biolims.tStorage.id,
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push({
		"data" : "name",
		"title" : biolims.tStorage.name,
		"createdCell" : function(td) {
			$(td).attr("saveName", "name");
		}
	});
	colOpts.push({
		"data" : "barCode",
		"title" : biolims.tStorage.barCode,
		"visible":false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "barCode");
		}
	});
	colOpts.push({
		"data": "jdeCode",
		"title": 'JDE Code',
		"createdCell": function(td, data) {
			$(td).attr("saveName", "jdeCode");
		},
	})
	
	colOpts.push({
		"data" : "spec",
		"title" : biolims.tStorage.spec,
		"createdCell" : function(td) {
			$(td).attr("saveName", "spec");
		}
	})
	var tbarOpts = [];
	
	//选择批次
	tbarOpts.push({
		text : biolims.common.batch,
		action : function() {
			var id= "";
			$(".selected").each(function(i,v){
				id += $(v).find("input").val()+",";
			})
			top.layer.open({
				title: biolims.common.batch,
				type: 2,
				area: ["650px", "400px"],
				btn: biolims.common.selected,
				content: [window.ctx + "/storage/pleaseVerifyPlan/selStorageBatch1.action?id="+id, ''],
				yes: function(index, layer) {
					
					var rows=$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addStorageBatchTable .selected");
					var batchIds="";
					$.each(rows, function(j, k) {
						batchIds+=($(k).children("td").eq(2).text())+",";
					});
//					console.log(batchIds);
					top.layer.close(index);
					// 先保存主表信息
					var note = $("#pleaseVerifyPlan_note").val();
					var outUser = $("#pleaseVerifyPlan_outUser").attr("userId");
					var outDate = $("#pleaseVerifyPlan_outDate").text();
//					var useUser = $("#pleaseVerifyPlan_outUser_id").val();
					var id = $("#pleaseVerifyPlan_id").text();
					
					//添加数据到出库明细表
								$.ajax({
								type : 'post',
								url : '/storage/pleaseVerifyPlan/addPleaseVerifyPlanItem.action',
								data : {
									batchIds : batchIds,
									note : note,
//									useUser : useUser,
									outDate : outDate,
									outUser : outUser,
									id : id,
									
								},
								success : function(data) {
									var data = JSON.parse(data)
									if (data.success) {
										$("#pleaseVerifyPlan_id").text(data.data);
										var param = {
												id: data.data
											};	
										var pleaseVerifyPlanItemTabs = $("#pleaseVerifyPlanItemdiv")
											.DataTable();
										pleaseVerifyPlanItemTabs.settings()[0].ajax.data = param;
										pleaseVerifyPlanItemTabs.ajax.reload();
									

									} else {

									}
									;
								}
							});
					
				},
			})
			
			
			

		}
	});
	tbarOpts.push({
		text: '<i class="fa fa-ioxhost"></i>'+biolims.common.search,
		action:function(){
			search()
		}
	});
	
	

	var storageOutAllOps = table(true, pleaseVerifyPlan_id,
			"/storage/showStorageTableJson.action?p_type=", colOpts, tbarOpts);
	storageOutAllTab = renderData($("#storageOutAlldiv"), storageOutAllOps);

	// 选择数据并提示
	storageOutAllTab.on('draw', function() {
		var index = 0;
		$("#storageOutAlldiv .icheck").on('ifChanged', function(event) {
			if ($(this).is(':checked')) {
				index++;
			} else {
				index--;
			}
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
	});
	bpmTask($("#bpmTaskId").val());
});



//弹框模糊查询参数
function searchOptions() {
	return [{
		"txt" : biolims.tStorage.id,
		"type" : "input",
		"searchName" : "id",
	},{
		"txt" : biolims.tStorage.name,
		"type" : "input",
		"searchName" : "name",
	}, {
		"type" : "table",
		"table" : storageOutAllTab
	} ];
}

//大保存
function save() {
		var changeLog = "库存出库";
		$('input[class="form-control"]').each(function(i, v) {
			var valnew = $(v).val();
			var val = $(v).attr("changelog");
			if(val !== valnew) {
				changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
			}
		});
		console.log($("#form1"));
		var jsonStr = JSON.stringify($("#form1").serializeObject());

		var dataItemJson = saveItemjson($("#pleaseVerifyPlanItemdiv"));
		var ele = $("#pleaseVerifyPlanItemdiv");
		var changeLogItem = "请验计划明细：";
		changeLogItem = getChangeLog(dataItemJson, ele, changeLogItem);
		var changeLogItems="";
		if(changeLogItem !="请验计划-样本异常："){
			changeLogItems=changeLogItem;
		}
		
		var id = $("#pleaseVerifyPlan_id").text();
//		var useUser = $("#storageOut_useUser_id").val();	
		var note = $("#pleaseVerifyPlan_note").val();
		//必填验证
		var requiredField=requiredFilter();
			if(!requiredField){
				return false;
			}
		top.layer.load(4, {shade:0.3}); 
		$.ajax({
			url: ctx + '/storage/pleaseVerifyPlan/savePleaseVerifyPlanAndItem.action',
			dataType: 'json',
			type: 'post',
			data: {
				dataValue: jsonStr,
				changeLog: changeLog,
				ImteJson: dataItemJson,
				bpmTaskId: $("#bpmTaskId").val(),
				changeLogItem: changeLogItems,
				id:id,
//				useUser:useUser,
				note:note
			},
			success: function(data) {
				if(data.success) {
					top.layer.closeAll();
					var url = "/storage/pleaseVerifyPlan/toEditPleaseVerifyPlan.action?id=" + data.id;

					url = url + "&bpmTaskId=" + $("#bpmTaskId").val() + "&p_type=" + $("#p_type").val();

					window.location.href = url;
				} else {
					top.layer.closeAll();
					top.layer.msg(data.msg);

				}
			}

		});

}

$.fn.serializeObject = function()
{
   var o = {};
   var a = this.serializeArray();
   $.each(a, function() {
       if (o[this.name]) {
           if (!o[this.name].push) {
               o[this.name] = [o[this.name]];
           }
           o[this.name].push(this.value || '');
       } else {
           o[this.name] = this.value || '';
       }
   });
   return o;
};
function tjsp() {
	top.layer
			.confirm(
					biolims.common.pleaseConfirmSaveBeforeSubmit,
					{
						icon : 3,
						title : biolims.common.prompt
					},
					function(index) {
						top.layer
								.open({
									title : biolims.common.approvalTask,
									type : 2,
									anim : 2,
									area : [ '800px', '500px' ],
									btn : biolims.common.selected,
									content : window.ctx
											+ "/workflow/processinstance/toStartView.action?formName=DnaTask",
									yes : function(index, layer) {
										var datas = {
											userId : userId,
											userName : userName,
											formId : pleaseVerifyPlan_id,
											title : $("#pleaseVerifyPlan_name").val(),
											formName : "PleaseVerifyPlan"
										}
										ajax(
												"post",
												"/workflow/processinstance/start.action",
												datas,
												function(data) {
													if (data.success) {
														top.layer
																.msg(biolims.common.submitSuccess);
														if (typeof callback == 'function') {
															callback(data);
														}
														// dialogWin.dialog("close");
													} else {
														top.layer
																.msg(biolims.common.submitFail);
													}
												}, null);
										top.layer.close(index);
									},
									cancel : function(index, layer) {
										top.layer.close(index)
									}

								});
						top.layer.close(index);
					});

}
function sp() {
	var taskId = $("#bpmTaskId").val();
	var formId = pleaseVerifyPlan_id;

	top.layer.open({
		title : biolims.common.detailedExaminationAndApproval,
		type : 2,
		anim : 2,
		area : [ '800px', '500px' ],
		btn : biolims.common.selected,
		content : window.ctx
				+ "/workflow/processinstance/toCompleteTaskView.action?taskId="
				+ taskId + "&formId=" + formId,
		yes : function(index, layer) {
			var operVal = $('.layui-layer-iframe', parent.document).find("iframe").contents()
					.find("#oper").val();
			var opinionVal = $('.layui-layer-iframe', parent.document).find("iframe").contents()
					.find("#opinionVal").val();

			if (!operVal) {
				top.layer.msg(biolims.common.pleaseSelectOper);
				return false;
			}
			if (operVal == "2") {
				_trunTodoTask(taskId, callback, dialogWin);
			} else {
				var paramData = {};
				paramData.oper = operVal;
				paramData.info = opinionVal;

				var reqData = {
					data : JSON.stringify(paramData),
					formId : formId,
					taskId : taskId,
					userId : window.userId
				}
				ajax("post", "/workflow/processinstance/completeTask.action",
						reqData, function(data) {
							if (data.success) {
								top.layer.msg(biolims.common.submitSuccess);
								if (typeof callback == 'function') {
								}
							} else {
								top.layer.msg(biolims.common.submitFail);
							}
						}, null);
			}
			top.layer.closeAll();
			location.href = window.ctx + "/lims/pages/dashboard/dashboard.jsp";
		}
	});
}


//function showUseUser(){
//	top.layer.open({
//		title: biolims.user.selectionLeader,
//		type: 2,
//		area: ["650px", "400px"],
//		btn: biolims.common.selected,
//		content: [window.ctx + "/core/user/selectUserTable.action?groupId=admin", ''],
//		yes: function(index, layer) {
//			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
//			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(0).text();
//			$("#pleaseVerifyPlan_useUser_name").val(name);
//			$("#pleaseVerifyPlan_useUser_id").val(id);
//			top.layer.close(index);
//		},
//	})
//}



function changeState() {
	var rows = $("#pleaseVerifyPlanItemdiv");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	var ids=[];
	var shuju = true;
	var zhijian = true;
	$.each(rows, function(j, k) {
		if($(k).hasClass("editagain")){
			top.layer.msg("请先保存数据！");
			shuju = false;
			return false;
		}else{
			
		}
	});
	if(shuju){
		$.each(rows, function(j, k) {
			ids.push($(k).find("input[type=checkbox]").val());
			if($(k).find("td[savename=sampleDeteyionType]").text()==""
				||$(k).find("td[savename=sampleDeteyionId]").text()==""){
				top.layer.msg("请填写质检类型和质检项！");
				zhijian = false;
				return false;
			}
		});
		if(zhijian){
		    var	id=$("#pleaseVerifyPlan_id").text()
			var paraStr = "formId=" + id +
				"&tableId=PleaseVerifyPlan";
			console.log(paraStr)
			top.layer.open({
				title: biolims.common.approvalProcess ,
				type: 2,
				anim: 2,
				area: ['400px', '400px'],
				btn: biolims.common.selected,
				content: window.ctx +
					"/applicationTypeAction/applicationTypeActionLook.action?" + paraStr +
					"&flag=changeState'",
				yes: function(index, layer) {
					top.layer.confirm(biolims.common.approve, {
						icon: 3,
						title: biolims.common.prompt,
						btn:biolims.common.selected
					}, function(index) {
						ajax("post", "/applicationTypeAction/exeFun.action", {
							applicationTypeActionId: $('.layui-layer-iframe', parent.document).find("iframe").contents().find("input:checked").val(),
							formId: id
						}, function(response) {
							var respText = response.message;
							if(respText == '') {
								window.location.reload();
							} else {
								top.layer.msg(respText);
							}
						}, null)
						top.layer.closeAll();
					})
				}
			});
		}
	}


	
}
function list() {
	window.location = window.ctx +
		'/storage/pleaseVerifyPlan/showPleaseVerifyPlanTable.action';
}