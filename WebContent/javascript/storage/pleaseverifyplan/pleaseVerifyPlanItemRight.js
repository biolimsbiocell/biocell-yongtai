var pleaseVerifyPlanItemTab, oldChangeLog;
//出库明细表
$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "storage-id",
		"title":  biolims.common.reagentNo,
		"createdCell": function(td) {
			$(td).attr("saveName", "storage-id");
		}
	})
	colOpts.push({
		"data": "storage-name",
		"title": biolims.common.designation,
		"createdCell": function(td) {
			$(td).attr("saveName", "storage-name");
		}
	})
	colOpts.push({
		"data": "storage-barCode",
		"title": biolims.tStorage.barCode,
		"visible":false,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "storage-barCode");
		},
	})
	colOpts.push({
		"data": "serial",
		"title": biolims.common.batchId,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "serial");
		},
	})
	
	colOpts.push({
		"data": "code",
		"title": biolims.storage.batchNo,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "code");
		},
	})
	
	
	colOpts.push({
		"data": "batchNum",
		"title":  biolims.storage.outPrice,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "batchNum");
		},
	})
	
//	colOpts.push({
//		"data": "note",
//		"title":'sn',
//		"createdCell": function(td, data) {
//			$(td).attr("saveName", "note");
//		},
//	})
	colOpts.push({
		"data": "note2",
		"title": biolims.common.note,
		"className": "edit",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "note2");
		},
	});
	
	colOpts.push({
		"data": "sampleDeteyionType",
		"title": "质检类型",
		"className":"select",
		"name":"|自主检测|第三方检测",
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleDeteyionType");
			$(td).attr("selectOpt", "|自主检测|第三方检测");
		},
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return "自主检测";
			}
			if(data == "2") {
				return "第三方检测";
			}else{
				return "";
			}
		}
	});
	colOpts.push({
		"data": "sampleDeteyionName",
		"title": "质检项",
		"className": "edit",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleDeteyionName");
		},
	});
	colOpts.push({
		"data": "sampleDeteyionId",
		"title": "质检项id",
		"className": "edit",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleDeteyionId");
		},
	});
	colOpts.push({
		"data": "submit",
		"title": "是否提交",
//		"className":"select",
		"name":"|是|否",
		"createdCell": function(td) {
			$(td).attr("saveName", "submit");
			$(td).attr("selectOpt", "|是|否");
		},
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return "是";
			}
			if(data == "0") {
				return "否";
			}else{
				return "";
			}
		}
	});
	
	
	var tbarOpts = [];
	tbarOpts.push({
		text: biolims.common.delSelected,
		action: function() {
			removeChecked($("#pleaseVerifyPlanItemdiv"),
				"/storage/pleaseVerifyPlan/delPleaseVerifyPlanItem.action","删除出库明细数据：",$("#pleaseVerifyPlan_id").text());
		
		}
	});
	tbarOpts.push({
		text: biolims.common.Editplay,
		action: function() {
			editItemLayer($("#pleaseVerifyPlanItemdiv"));
		}
	})
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			savePleaseVerifyPlanItemTab();
		}
	});
	tbarOpts.push({
		text: "选择质检项",
		action: function() {
			chosezhijian();
		}
	});
	tbarOpts.push({
		text: "提交质检",
		action: function() {
			tijiaozhijian();
		}
	});
	tbarOpts.push({
		text: "打印请验单",
		action: function() {
			dayinQinyanD();
		}
	});
//	tbarOpts.push({
//		text : "产物数量",
//		action : function() {
//			var rows = $("#pleaseVerifyPlanItemdiv .selected");
//			var length = rows.length;
//			if (!length) {
//				top.layer.msg("请选择数据");
//				return false;
//			}
//			top.layer.open({
//				type : 1,
//				title : "产物数量",
//				content : $('#batch_data'),
//				area:[document.body.clientWidth-600,document.body.clientHeight-200],
//				btn: biolims.common.selected,
//				yes : function(index, layer) {
//					var productNum = $("#productNum").val();
//					rows.addClass("editagain");
//					rows.find("td[savename='productNum']").text(productNum);
//					top.layer.close(index);
//				}
//			});
//		}
//	})
	var pleaseVerifyPlanItemOps = table(true, $("#pleaseVerifyPlan_id").text(), "/storage/pleaseVerifyPlan/showPleaseVerifyPlanItemTableJson.action", colOpts, tbarOpts);
	pleaseVerifyPlanItemTab = renderData($("#pleaseVerifyPlanItemdiv"), pleaseVerifyPlanItemOps);
	//选择数据并提示
	pleaseVerifyPlanItemTab.on('draw', function() {
		var index = 0;
		$("#pleaseVerifyPlanItemdiv .icheck").on('ifChanged', function(event) {
			if($(this).is(':checked')) {
				index++;
				$("#plateModal").find(".mysample[sid='" + this.value + "']").css("border","2px solid #000");
			} else {
				var tt=$("#plateModal").find(".mysample[sid='" + this.value + "']").css("border","1px solid gainsboro");
				index--;
			}
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
		oldChangeLog = pleaseVerifyPlanItemTab.ajax.json();
		
	});
});
var lgx = true;
//提交质检
function tijiaozhijian() {
	var rows = $("#pleaseVerifyPlanItemdiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	var ids=[];
	var shuju = true;
	var zhijian = true;
	$.each(rows, function(j, k) {
		if($(k).hasClass("editagain")){
			top.layer.msg("请先保存数据！");
			shuju = false;
			return false;
		}else{
			
		}
	});
	if(shuju){
		$.each(rows, function(j, k) {
			ids.push($(k).find("input[type=checkbox]").val());
			if($(k).find("td[savename=sampleDeteyionType]").text()==""
				||$(k).find("td[savename=sampleDeteyionId]").text()==""){
				top.layer.msg("请填写质检类型和质检项！");
				zhijian = false;
				return false;
			}
			if($(k).find("td[savename=submit]").text()=="是"){
				top.layer.msg("该数据已提交质检！");
				zhijian = false;
				return false;
			}
		});
		if(zhijian){
			if(lgx){
				lgx = false;
				ajax("post", "/storage/pleaseVerifyPlan/submitSample.action", {
					ids: ids,
					id: $("#pleaseVerifyPlan_id").text()
				}, function(data) {
					if(data.success) {
						lgx = true;
						top.layer.msg(biolims.common.submitSuccess);
						pleaseVerifyPlanItemTab.ajax.reload();
					} else {
						lgx = true;
						top.layer.msg(biolims.common.submitFail);
					}
				}, null);
			}
		}
	}
}


//选择检测项
function chosezhijian() {
	var rows = $("#pleaseVerifyPlanItemdiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/system/detecyion/sampleDeteyion/showSampledetecyion.action", ''],
		yes: function(index, layer) {
			var id =[];
			var name =[];
//			var reali = '';
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addSampleDeteyionTable .selected").each(function(i, v) {
//				reali += '<li class="zhijianli"><span><i class="glyphicon glyphicon-th-list"></i></span><small class="label label-primary"><span zhijianid=\'' + $(v).children("td").eq(1).text() + '\' class="zhijianName">' + $(v).children("td").eq(2).text() + '</span></small><i class="fa fa-trash pull-right"></i></li>';
				//id = $(v).children("td").eq(1).text();
				//name = $(v).children("td").eq(2).text();
				id.push($(v).children("td").eq(1).text());
				name.push($(v).children("td").eq(2).text());
			});
//			$("#zhijianBody").append(reali);
//			$("#zhijianBody .nozhijian").slideUp();
			if(id.length>1||name.length>1){
				top.layer.msg("请选择一条数据！");
				return false;
			}else{
				rows.addClass("editagain");
				rows.find("td[savename='sampleDeteyionId']").text(id);
				rows.find("td[savename='sampleDeteyionName']").text(name);
				top.layer.close(index);
			}
//			rows.find("td[savename='dicSampleTypeName']").attr("dicSampleType-id", id).text(id);
//			choseType();
//			reagentAndCosRemove();
		},
	})

}

// 保存
function savePleaseVerifyPlanItemTab() {
    var ele=$("#pleaseVerifyPlanItemdiv");
	var changeLog = "库存主数据出库:";
	var data = saveItemjson(ele);
	changeLog = getChangeLog(data, ele, changeLog);
	var changeLogs="";
	if(changeLog !="库存主数据出库:"){
		changeLogs=changeLog;
	}
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		type: 'post',
		url: '/storage/pleaseVerifyPlan/savePleaseVerifyPlanItemTable.action',
		data: {
			id: $("#pleaseVerifyPlan_id").text(),
			dataJson: data,
			logInfo: changeLogs
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 添加样本类型ID
//			if(k == "dicSampleTypeName") {
//				json["dicSampleTypeId"] = $(tds[j]).attr("dicSampleTypeId");
//				continue;
//			}
			if(k == "sampleDeteyionType") {
				var sampleDeteyionType = $(tds[j]).text();
				if(sampleDeteyionType=="自主检测"){
					json[k] = "1";
				}else if(sampleDeteyionType=="第三方检测"){
					json[k] = "2";
				}else{
					json[k] = "";
				}
				continue;
			}
			if(k == "submit") {
				var submit = $(tds[j]).text();
				if(submit=="是"){
					json[k] = "1";
				}else if(submit=="否"){
					json[k] = "0";
				}else{
					json[k] = "";
				}
				continue;
			}
			
			
			json[k] = $(tds[j]).text();
		}
		json.color = $(val).attr("background");
		data.push(json);
	});
	return JSON.stringify(data);
}

/*
 * 
 * 创建者 : 郭恒开
 * 创建日期: 2018/03/08
 * 文件描述: 修改日志
 * 
 */
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		if (!id) {
			changeLog += '新增记录:';
			for ( var k in v) {
				var title = ele.find("th[savename=" + k + "]").text();
				changeLog += '"' + title + '"为"' + v[k] + '";';
			}
			return true;
		}
		changeLog += '编号为"' + v.id + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k] && k != "color") {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
function dayinQinyanD(){
	var rows = $("#pleaseVerifyPlanItemdiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg("请选择数据！");
		return false;
	}else{
		var result="";
		$("#pleaseVerifyPlanItemdiv .selected").each(function(i,o){
			if(result==""){
				result+=$(this).find("input[type=checkbox]").val();
			}else{
				result+=","+$(this).find("type=checkbox").val();
			}
		})
		var url = '__report=pleaseVerifyPlan.rptdesign&id=' + result;
		commonPrint(url);
	}
}
function commonPrint(pa) {
	var url = '/../report/frameset?__format=html&__svg=false&__locale=zh_CN&__designer=true&__masterpage=true&__rtl=false&__showtitle=false&';
	url = url + pa;
	window.open(url, '_blank', '');
}