var pleaseVerifyPlanTab;
$(function() {
	var options = table(true, "",
			"/storage/pleaseVerifyPlan/showPleaseVerifyPlanTableJson.action", [ {
				"data" : "id",
				"title" : "请验计划编号",
			}, {
				"data" : "outUser-id",
				"title" : "请验人编号",
			}, {
				"data" : "outUser-name",
				"title" : "请验人姓名",
			}, {
				"data" : "outDate",
				"title" : "请验时间",
			}, {
				"data" : "note",
				"title" :"描述",
			} , {
				"data" : "stateName",
				"title" : biolims.common.stateName,
			} ], null)
	pleaseVerifyPlanTab = renderRememberData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(pleaseVerifyPlanTab);
	})
});

function add() {
	window.location = window.ctx
			+ '/storage/pleaseVerifyPlan/toEditPleaseVerifyPlan.action';
}
function edit() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/storage/pleaseVerifyPlan/toEditPleaseVerifyPlan.action?id=' + id;
}
function view() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	$("#maincontentframe", window.parent.document)[0].src = window.ctx
			+ "/storage/pleaseVerifyPlan/toViewPleaseVerifyPlan.action?id="
			+ id;
}
// 弹框模糊查询参数
function searchOptions() {
	return [ {
		"txt" : "请验计划编号",
		"type" : "input",
		"searchName" : "id",
	}, {
		"txt" : "请验人姓名",
		"type" : "input",
		"searchName" : "outUser-name",
	}, {
		"txt" :"请验时间"+"(Start)",
		"type" : "dataTime",
		"searchName" : "outDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : "请验时间"+"(End)",
		"type" : "dataTime",
		"searchName" : "outDate##@@##2",
		"mark" : "e##@@##",
	}, {
		"type" : "table",
		"table" : pleaseVerifyPlanTab
	} ];
}
