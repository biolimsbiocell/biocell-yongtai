/* 
 * 文件描述: 单位组的弹框
 * 
 */
var addSampleOrder;
$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": "编码",
	});
	colOpts.push({
		"data": "batch",
		"title": "批次号",
	});
	var tbarOpts = [];

	tbarOpts.push({
		text:"搜索",
		action: search,
	});
	
	var options = table(false, null,
				'/storage/out/showAllSampleOrderDialogListJson.action', colOpts, tbarOpts)
		addSampleOrder = renderData($("#addSampleOrder"), options);
	$("#addSampleOrder").on(
			'init.dt',
			function(e, settings) {
				// 清除操作按钮
				//$("#addSampleOrder_wrapper .dt-buttons").empty();
				$('#addSampleOrder_wrapper').css({
					"padding": "0 16px"
				});
				var trs = $("#addSampleOrder tbody tr");
				
				addSampleOrder.ajax.reload();
				addSampleOrder.on('draw', function() {
					trs = $("#addSampleOrder tbody tr");
					trs.click(function() {
						$(this).addClass("chosed").siblings("tr")
							.removeClass("chosed");
					});
				});
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			});

})
//弹框模糊查询参数
function searchOptions() {
	return [{
			"txt": "编码",
			"type": "input",
			"searchName": "id"
		},
		{
			"txt": "批次号",
			"type": "input",
			"searchName": "batch"
		},
		{
			"type": "table",
			"table": addSampleOrder
		}
	];
}