var storageOutItemTab, oldChangeLog;
//出库明细表
$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "storage-id",
		"title":  biolims.common.reagentNo,
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "storage-id");
			$(td).attr("expireDate", rowData['expireDate']);
		}
	})
	colOpts.push({
		"data": "storage-name",
		"title": biolims.common.designation,
		"createdCell": function(td) {
			$(td).attr("saveName", "storage-name");
		}
	})
//	colOpts.push({
//		"data": "storage-barCode",
//		"title": biolims.tStorage.barCode,
//		"visible": false,
//		"createdCell": function(td, data) {
//			$(td).attr("saveName", "storage-barCode");
//		},
//	})
/*	colOpts.push({
		"data": "storage-kit-name",
		"title": biolims.common.kitName,
		"createdCell": function(td) {
			$(td).attr("saveName", "storage-kit-name");
		}
	})*/
		colOpts.push({
		"data": "storage-jdeCode",
		"title": '货号',
		"createdCell": function(td, data) {
			$(td).attr("saveName", "storage-jdeCode");
		},
	})
		colOpts.push({
		"data": "storage-spec",
		"title": biolims.storage.spec,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "storage-spec");
		},
	})
	colOpts.push({
		"data": "serial",
		"title": "产品批号ID",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "serial");
		},
	})
		colOpts.push({
		"data": "code",
		"title": "产品批号",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "code");
		},
	})
	
	colOpts.push({
		"data": "storageNum",
		"title":  biolims.sample.inventory,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "storageNum");
		},
	})
	
		colOpts.push({
		"data": "batchNum",
		"title":  biolims.storage.outPrice,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "batchNum");
		},
	})
	
	colOpts.push({
		"data": "unitGroupNew-name",
		"title": "单位组"+ '<img src="/images/required.gif"/>',
		"createdCell": function(td, data,rowData) {
			$(td).attr("saveName", "unitGroupNew-name");
			$(td).attr("unitGroupNew-id", rowData['unitGroupNew-id']);
			$(td).attr("unitGroupNew-cycle", rowData['unitGroupNew-cycle']);
		},
	});
	colOpts.push({
		"data": "packingNum",
		"title": "数量"+ '<img src="/images/required.gif"/>',
		"className": "edit",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "packingNum");
		},
	});
	colOpts.push({
		"data": "unitGroupNew-mark-name",
		"title": "主单位",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "unitGroupNew-mark-name");
		},
	});
	colOpts.push({
		"data": "num",
		"title": biolims.storage.outputNum,
//		"className": "edit",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "num");
		},
	})
	colOpts.push({
		"data": "unitGroupNew-mark2-name",
		"title": "副单位",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "unitGroupNew-mark2-name");
		},
	});

	colOpts.push({
		"data": "storage-unit-name",
		"title": biolims.common.unit,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "storage-unit-name");
		},
	})
	
	/*colOpts.push({
		"data": "storage-reactionNum",
		"title": biolims.common.reactionNumber,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "storage-reactionNum");
		},
	})*/
	
	colOpts.push({
		"data": "storage-position-id",
		"title":biolims.common.storageId,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "storage-position-id");
		},
	})
	colOpts.push({
		"data": "storage-position-name",
		"title":biolims.common.storageName,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "storage-position-name");
		},
	})
	colOpts.push({
		"data": "note2",
		"title": biolims.common.note,
		"className": "edit",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "note2");
		},
	})
	colOpts.push({
		"data": "productMark",
		"title": "产品批号",
//		"className": "edit",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "productMark");
		},
	})
		colOpts.push({
		"data": "note",
		"title":'sn',
		"createdCell": function(td, data) {
			$(td).attr("saveName", "note");
		},
	})
	
//		colOpts.push({
//		"data": "batch",
//		"title":'产品批号',
//		"createdCell": function(td, data) {
//			$(td).attr("saveName", "batch");
//		},
//	})
	
	var tbarOpts = [];
	tbarOpts.push({
		text: biolims.common.delSelected,
		action: function() {
			removeChecked($("#storageOutItemdiv"),
				"/storage/out/delOutStorageItem.action","删除出库明细数据：",$("#storageOut_id").text());
		
		}
	});
	tbarOpts.push({
		text: biolims.common.Editplay,
		action: function() {
			editItemLayer($("#storageOutItemdiv"))
		}
	})
	
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			savestorageOutItemTab();
		}
	})
//	tbarOpts.push({
//		text: "选择单位组",
//		action: function() {
//			unitBatch();
//		}
//	});
	tbarOpts.push({
		text: "选择实验",
		action: function() {
			chooseExperiment();
		}
	});
//	选择实验点击事件
//	tbarOpts.push({
//		text: "选择实验",
//		action: function() {
//			var rows = $("#storageOutItemdiv .selected");
//			if (!rows.length){
//				top.layer.msg("请选择数据");
//				return false;
//			}
//			top.layer.open({
//				title: "实验",
//				type: 2,
//				area: ["650px", "400px"],
//				btn: biolims.common.selected,
//				content: [window.ctx + "/storage/out/showSampleOrderDialogList.action", ''],
//				yes: function(index, layer) {
//					
//					rows.addClass("editagain");
//					var batch = $('.layui-layer-iframe', parent.document).find("iframe").contents().
//						find("#addSampleOrder .chosed").children("td").eq(1).text();
//					rows.find("td[savename='batch']").text(batch);
//					top.layer.close(index);
//					
//				},
//			})	
//		}
//	})
//	tbarOpts.push({
//		text : "产物数量",
//		action : function() {
//			var rows = $("#storageOutItemdiv .selected");
//			var length = rows.length;
//			if (!length) {
//				top.layer.msg("请选择数据");
//				return false;
//			}
//			top.layer.open({
//				type : 1,
//				title : "产物数量",
//				content : $('#batch_data'),
//				area:[document.body.clientWidth-600,document.body.clientHeight-200],
//				btn: biolims.common.selected,
//				yes : function(index, layer) {
//					var productNum = $("#productNum").val();
//					rows.addClass("editagain");
//					rows.find("td[savename='productNum']").text(productNum);
//					top.layer.close(index);
//				}
//			});
//		}
//	})
	var storageOutItemOps = table(true, $("#storageOut_id").text(), "/storage/out/showStorageOutItemTableJson.action", colOpts, tbarOpts);
	storageOutItemTab = renderData($("#storageOutItemdiv"), storageOutItemOps);
	//选择数据并提示
	storageOutItemTab.on('draw', function() {
		var index = 0;
		$("#storageOutItemdiv .icheck").on('ifChanged', function(event) {
			if($(this).is(':checked')) {
				index++;
				$("#plateModal").find(".mysample[sid='" + this.value + "']").css("border","2px solid #000");
			} else {
				var tt=$("#plateModal").find(".mysample[sid='" + this.value + "']").css("border","1px solid gainsboro");
				index--;
			}
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
		oldChangeLog = storageOutItemTab.ajax.json();
		
	});
});


//选择实验
function chooseExperiment(){
	var arr=[];
	var rows = $("#storageOutItemdiv .selected");
	var length = rows.length;
	if (!length) {
		top.layer.msg(biolims.common.pleaseSelectData);
		return false;
	}
	
	rows.each(function(i,j,k){
		debugger
		var storageId=$(this).find("td[savename='storage-id']").text();
		arr.push(storageId);
	    })
	    
	 for(var i=0; i<arr.length; i++){
         for(var j=i+1; j<arr.length; j++){
             if(arr[i]==arr[j]){         //第一个等同于第二个，splice方法删除第二个
                 arr.splice(j,1);
                 j--;
             }
         }
     }
	if(arr.length!=1){
		top.layer.msg("请选择相同类型的数据");
		return false;
		
	}
	$.ajax({
		type: 'post',
		url: '/storage/findMedium.action',
		data: {
			id: arr[0],
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer
				.open({
					title : "请选择",
					type : 2,
					area : [ "650px", "400px" ],
					btn : biolims.common.selected,
					content : [
							window.ctx
									+ "/experiment/cell/passage/cellPassageNow/showCellPassage.action",
							'' ],
					yes : function(index, layer) {
						var id = $('.layui-layer-iframe', parent.document).find(
								"iframe").contents().find("#showCellPassage .chosed")
								.children("td").eq(1).text();
						rows.addClass("editagain");
						
						
						
						rows.find("td[savename='productMark']").text(id);
						top.layer.close(index);
					},
				});
			} else {
				top.layer.msg("原辅料不是培养基不需要选择实验！")
			};
		}
	})
	
	
	
}
//批量单位组
function unitBatch() {
	var arr=[];
	var units=[];
	var rows = $("#storageOutItemdiv .selected");
	var length = rows.length;
	if (!length) {
		top.layer.msg(biolims.common.pleaseSelectData);
		return false;
	}
	rows.each(function(i,j,k){
		debugger
		var storageId=$(this).find("td[savename='storage-id']").text();
		var unit=$(this).find("td[savename='storage-unit-name']").text();
		arr.push(storageId);
		units.push(unit);
	    })
	    
	 for(var i=0; i<arr.length; i++){
         for(var j=i+1; j<arr.length; j++){
             if(arr[i]==arr[j]){         //第一个等同于第二个，splice方法删除第二个
                 arr.splice(j,1);
                 j--;
             }
         }
     }
	if(arr.length!=1){
		top.layer.msg("请选择相同类型的数据");
		return false;
		
	}
	top.layer
			.open({
				title : biolims.common.batchUnit,
				type : 2,
				area : [ "650px", "400px" ],
				btn : biolims.common.selected,
				content : [
						window.ctx
								+ "/com/biolims/system/work/unitGroupNew/showWorkTypeDialogList.action?state=state&arr="+units[0],
						'' ],
				yes : function(index, layer) {
					var id = $('.layui-layer-iframe', parent.document).find(
							"iframe").contents().find("#addUnitGroup .chosed")
							.children("td").eq(0).text();
					var name = $('.layui-layer-iframe', parent.document).find(
							"iframe").contents().find("#addUnitGroup .chosed")
							.children("td").eq(1).text();
					var mark1Name = $('.layui-layer-iframe', parent.document)
					.find("iframe").contents().find(
							"#addUnitGroup .chosed").children("td").eq(
							3).text();
					var mark2Name = $('.layui-layer-iframe', parent.document)
							.find("iframe").contents().find(
									"#addUnitGroup .chosed").children("td").eq(
									4).text();
					var cycle = $('.layui-layer-iframe', parent.document)
					.find("iframe").contents().find(
							"#addUnitGroup .chosed").children("td").eq(
							2).text();
					rows.addClass("editagain");
					
					
					
					rows.find("td[savename='unitGroupNew-name']").attr("unitGroupNew-id", id).attr("unitGroupNew-cycle", cycle).text(name);
					rows.find("td[savename='unitGroupNew-mark-name']").text(mark1Name);
					rows.find("td[savename='unitGroupNew-mark2-name']").text(mark2Name);
					top.layer.close(index);
				},
			});
}

// 保存
function savestorageOutItemTab() {
    var ele=$("#storageOutItemdiv");
	var changeLog = "库存主数据出库:";
	var data = saveItemjson(ele);
	changeLog = getChangeLog(data, ele, changeLog);
	var changeLogs="";
	if(changeLog !="库存主数据出库:"){
		changeLogs=changeLog;
	}
	
	state=true;
	$("#storageOutItemdiv tbody tr").each(function(i,j,k){
		var packingNum=$(this).find("td[savename='packingNum']").text();
		debugger
		var unitGroupNew=$(this).find("td[savename='unitGroupNew-name']").text();
		var batchNum=$(this).find("td[savename='batchNum']").text();
		if(unitGroupNew==""){
			top.layer.msg("请选择单位组！")
			state=false;
			return false;
		}
		if(packingNum==""){
			top.layer.msg("请填写数量！")
			state=false;
			return false;
		}
		if(!/^(?!0$|0\.00|0\.0|0\d+$)([1-9]?\d+(\.\d*)|(\\s&&[^\\f\\n\\r\\t\\v])|([1-9]*[1-9][0-9]*)?)$/.test(packingNum)){
			top.layer.msg("请填写正确的数量！")
			state=false;
			return false;
		}
		if(packingNum>batchNum){
			top.layer.msg("数量不能大于本批次存量！")
			state=false;
			return false;
		}
		
	})
	if(state){
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		type: 'post',
		url: '/storage/out/saveStorageOutItemTable.action',
		data: {
			id: $("#storageOut_id").text(),
			dataJson: data,
			logInfo: changeLogs
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
				storageOutItemTab.ajax.reload();
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
	}
}
// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 添加样本类型ID
//			if(k == "dicSampleTypeName") {
//				json["dicSampleTypeId"] = $(tds[j]).attr("dicSampleTypeId");
//				continue;
//			}
			//单位组
			if(k == "unitGroupNew-name") {
				json["unitGroupNew-id"] = $(tds[j]).attr("unitGroupNew-id");
//				json["unitgroupnew-cycle"] = $(tds[j]).attr("unitgroupnew-cycle");
			}
			if(k == "storage-id") {
			json["expireDate"] = $(tds[j]).attr("expireDate");
			}
			json[k] = $(tds[j]).text();
		}
		json.color = $(val).attr("background");
		data.push(json);
	});
	return JSON.stringify(data);
}


//必填字段验证
//function mustAddField() {
//	
//	var trs = $("#storageOutItemdiv").find("tbody").children("tr");
//	var flag = true;
//	trs.each(function(i, val) {
//
//		var json = {};
//		var tds = $(val).children("td");
//		json["id"] = $(tds[0]).find("input").val();
//		for (var j = 1; j < tds.length; j++) {
//			var k = $(tds[j]).attr("savename");
//			json[k] = $(tds[j]).text();
//			if(k == "packingNum") {
//				if(json[k]==""){
//					top.layer.msg("请填写数量！")
//					flag = false;
//				}
//			}
//		}
//	});
//	return flag;
//}

/*
 * 
 * 创建者 : 郭恒开
 * 创建日期: 2018/03/08
 * 文件描述: 修改日志
 * 
 */
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		if (!id) {
			changeLog += '新增记录:';
			for ( var k in v) {
				var title = ele.find("th[savename=" + k + "]").text();
				changeLog += '"' + title + '"为"' + v[k] + '";';
			}
			return true;
		}
		changeLog += '编号为"' + v.id + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k] && k != "color") {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
