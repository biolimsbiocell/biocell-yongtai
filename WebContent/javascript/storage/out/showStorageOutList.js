var gridGrid;
Ext
		.onReady(function() {
			Ext.QuickTips.init();
			Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
			Ext.grid.RowNumberer = Ext.extend(Ext.grid.RowNumberer, {
				width : 30,
				renderer : function(value, cellmeta, record, rowIndex,
						columnIndex, store) {
					return store.lastOptions.params.start + rowIndex + 1;
				}
			});
			var store = new Ext.data.JsonStore({
				root : 'results',
				totalProperty : 'total',
				remoteSort : true,
				fields : [ {
					name : 'id',
					type : 'string'
				}, {
					name : 'note',
					type : 'string'
				}, {
					name : 'useUser-name',
					type : 'string'
				}, {
					name : 'outUser-name',
					type : 'string'
				}, {
					name : 'outDate',
					type : 'string'
				}, {
					name : 'type-name',
					type : 'string'
				}, {
					name : 'confirmUser-name',
					type : 'string'
				}, {
					name : 'stateName',
					type : 'string'
				}],
proxy: new Ext.data.HttpProxy({url: ctx+'/storage/out/getStorageOutList.action?queryMethod=',method: 'POST'})
});
gridGrid = new Ext.grid.GridPanel({
										autoWidth : true,
						id : gridGrid,
						titleCollapse : true,
						autoScroll : true,
						height : document.body.clientHeight - 35,
						iconCls : 'icon-grid',
						title : biolims.storage.outPutManagement,
						collapsible : true,
						store : store,
						columnLines : true,
						trackMouseOver : true,
						loadMask : true,
						colModel : new Ext.ux.grid.LockingColumnModel([
								new Ext.grid.RowNumberer(), {
									dataIndex : 'id',
									header : biolims.storage.outPutId,
									width : 120,
									sortable : true
								}, {
									dataIndex : 'note',
									header : biolims.common.name,
									width : 175,
									sortable : true
								}, {
									dataIndex : 'useUser-name',
									header : biolims.storage.useUserName,
									width : 175,
									sortable : true
								}, {
									dataIndex : 'outUser-name',
									header : biolims.storage.outUserName,
									width : 175,
									sortable : true
								}, {
									dataIndex : 'outDate',
									header : biolims.storage.outDate,
									width : 80,
									sortable : true
								}, {
									dataIndex : 'type-name',
									header : biolims.storage.outType,
									width : 180,
									sortable : true,
									hidden : true
								}, {
									dataIndex : 'confirmUser-name',
									header : biolims.wk.approver,
									width : 100,
									sortable : true,
									hidden : true
								}, {
									dataIndex : 'stateName',
									header : biolims.common.workFlowStateName,
									width : 75,
									sortable : true
								} ]),
						stripeRows : true,
						view : new Ext.ux.grid.LockingGridView(),
						viewConfig : {
							forceFit : true,
							enableRowBody : false,
showPreview:false
},
bbar: new Ext.PagingToolbar({
	id : 'bbarId',
	pageSize : parseInt((document.body.clientHeight - 35) > 0 ? (document.body.clientHeight - 35) / 24
			: 1),
			store : store,
			displayInfo : true,
			displayMsg : biolims.common.displayMsg,
			beforePageText : biolims.common.page,
			afterPageText : biolims.common.afterPageText,
			emptyMsg : biolims.common.noData,
	plugins : new Ext.ui.plugins.ComboPageSize(
			{
				addToItem : false,
				prefixText : biolims.common.show,
				postfixText : biolims.common.entris
											})
								})
					});
			gridGrid.render('grid');
			gridGrid.getSelectionModel().on('rowselect',
					function(sm, rowIdx, r) {
						document.getElementById("id").value = r.data.id;
					});
			gridGrid.on('rowdblclick', function() {
				var record = gridGrid.getSelectionModel().getSelections();
				document.getElementById("id").value = record[0].data.id;
				edit();
			});
			store.on('beforeload', function() {
				store.baseParams = {
					data : document.getElementById('extJsonDataString').value
				};
			});
			store
					.load({
						params : {
							start : 0,
							limit : parseInt((document.body.clientHeight - 35) > 0 ? (document.body.clientHeight - 35) / 24
									: 1)
						}
					});
		});
function exportexcel() {
	gridGrid.title = biolims.common.exportList;
	var vExportContent = gridGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}


function add() {
	window.location = window.ctx + '/storage/out/toEditStorageOut.action';
}

function list() {
	window.location = window.ctx + '/storage/out/showStorageOutList.action';
}

function edit() {
	var id = "";
	id = document.getElementById("id").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/storage/out/toEditStorageOut.action?id=' + id;
}

function view() {
	var id = "";
	id = document.getElementById("id").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/storage/out/toViewStorageOut.action?id=' + id;
}
$(function() {
	searchworkflow("stateName", "StorageOut", null);
});
Ext.onReady(function() {
	Ext.QuickTips.init();
	new Ext.form.DateField({

		applyTo : 'queryStartDate'
	});
	$("#queryStartDate").css("width", "80px");
});
Ext.onReady(function() {
	Ext.QuickTips.init();
	new Ext.form.DateField({

		applyTo : 'queryEndDate'
	});
	$("#queryEndDate").css("width", "80px");
});
$(function() {

	$("#opensearch").click(function() {
		var option = {};
		option.width = 440;
		option.height = 217;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(gridGrid);
				$(this).dialog("close");
			},
			"清空(Empty)" : function() {
				form_reset();
			}
		}, true, option);

	});

});