var storageOutTab;
$(function() {
	var options = table(true, "",
			"/storage/out/showStorageOutTableJson.action", [ {
				"data" : "id",
				"title" : biolims.storage.outPutId,
			}, {
				"data" : "useUser-name",
				"title" : "领用人",
			}, {
				"data" : "outUser-name",
				"title" : biolims.storage.outUserName,
			}, {
				"data" : "outDate",
				"title" : biolims.storage.outDate,
			}, {
				"data" : "note",
				"title" :biolims.tInstrumentRepair.name,
			} , {
				"data" : "state",
				"title" :biolims.common.state,
			}, {
				"data" : "stateName",
				"title" : biolims.common.stateName,
			} ], null)
	storageOutTab = renderRememberData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(storageOutTab);
	})
});

function add() {
	window.location = window.ctx
			+ '/storage/out/toEditStorageOut.action?sampleStyle='
			+ $("#sampleStyle").val();
}
function edit() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/storage/out/toEditStorageOut.action?id=' + id
			+ "&sampleStyle=" + $("#sampleStyle").val();
}
function view() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ "/storage/out/toViewStorageOut.action?id="
			+ id;
}
// 弹框模糊查询参数
function searchOptions() {
	return [ {
		"txt" : biolims.storage.outPutId,
		"type" : "input",
		"searchName" : "id",
	}, {
		"txt" : biolims.storage.outUserName+"ID",
		"type" : "input",
		"searchName" : "useUser-id",
	}, {
		"txt" : biolims.storage.outUserName,
		"type" : "input",
		"searchName" : "useUser-name",
	}, {
		"txt" :biolims.storage.outDate+"(Start)",
		"type" : "dataTime",
		"searchName" : "outDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : biolims.storage.outDate+"(End)",
		"type" : "dataTime",
		"searchName" : "outDate##@@##2",
		"mark" : "e##@@##",
	}, {
		"type" : "table",
		"table" : storageOutTab
	} ];
}
