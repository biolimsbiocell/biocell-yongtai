var gridGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : 'string'
	});
	fields.push({
		name : 'storage-name',
		type : 'string'
	});
	fields.push({
		name : 'storage-id',
		type : 'string'
	});
	fields.push({
		name : 'storage-type-id',
		type : 'string'
	});
	fields.push({
		name : 'storage-type-name',
		type : 'string'
	});
	fields.push({
		name : 'storage-studyType-name',
		type : 'string'
	});
	fields.push({
		name : 'storage-unit-name',
		type : 'string'
	});
	fields.push({
		name : 'storage-jdeCode',
		type : 'string'
	});
	fields.push({
		name : 'serial',
		type : 'string'
	});
	fields.push({
		name : 'code',
		type : 'string'
	});
	fields.push({
		name : 'price',
		type : 'string'
	});
	fields.push({
		name : 'storageNum',
		type : 'float'
	});
	fields.push({
		name : 'num',
		type : 'float'
	});
	fields.push({
		name : 'position-id',
		type : 'string'
	});
	fields.push({
		name : 'position-name',
		type : 'string'
	});
	fields.push({
		name : 'note',
		type : 'string'
	});
	fields.push({
		name : 'expireDate',
		type : 'date',
		dateFormat : 'Y-m-d'
	});
	fields.push({
		name : 'qcState',
		type : 'string'
	});
	fields.push({
		name : 'note2',
		type : 'string'
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : 'ID',
		width : 150,
		sortable : true,
		hidden : true
	});
	cm.push({
		dataIndex : 'storage-name',
		header : biolims.common.componentName,
		width : 200,
		sortable : false,
		hidden : false,
	});
	cm.push({
		dataIndex : 'storage-id',
		header : biolims.common.componentID,
		width : 200,
		sortable : false,
		hidden : true,
	});
	cm.push({
		dataIndex : 'storage-type-id',
		header : biolims.storage.storageTypeId,
		width : 150,
		sortable : false,
		hidden : true,
	});
	cm.push({
		dataIndex : 'storage-type-name',
		header : biolims.storage.storageType,
		width : 100,
		sortable : false,
		hidden : true,
	});
	cm.push({
		dataIndex : 'storage-studyType-name',
		header : biolims.storage.storageStudyType,
		width : 100,
		sortable : false,
		hidden : false,
	});
	cm.push({
		dataIndex : 'storage-jdeCode',
		header : "JDE Code",
		width : 150,
		sortable : false,
		hidden : false,
	});
	cm.push({
		dataIndex : 'serial',
		header : biolims.common.batchId,
		width : 100,
		sortable : false,
		hidden : true,
		editor : serial
	});
	cm.push({
		dataIndex : 'code',
		header : biolims.storage.batchNo,
		width : 150,
		sortable : false,
		hidden : false,
		editor : serial
	});
	cm.push({
		dataIndex : 'price',
		header : biolims.storage.batchPrice,
		width : 150,
		sortable : false,
		hidden : true,
		editor : price
	});
	cm.push({
		dataIndex : 'storageNum',
		header : biolims.storage.num,
		width : 100,
		sortable : false,
		hidden : false,
	});
	cm.push({
		dataIndex : 'num',
		header : biolims.storage.outputNum,
		width : 100,
		sortable : false,
		hidden : false,
		renderer : formatCss,
		editor : num
	});
	cm.push({
		dataIndex : 'storage-unit-name',
		header : biolims.common.unit,
		width : 75,
		sortable : false,
		hidden : false,
	});
	cm.push({
		dataIndex : 'position-id',
		header : biolims.common.storageId,
		width : 150,
		sortable : false,
		hidden : true,
	});
	cm.push({
		dataIndex : 'position-name',
		header : biolims.common.storageName,
		width : 150,
		sortable : false,
		hidden : false,
	});
	cm.push({
		dataIndex : 'note',
		header : "sn",
		width : 150,
		sortable : false,
		hidden : false,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var expireDate = new Ext.form.DateField({
		format : 'Y-m-d'
	});
	cm.push({
		dataIndex : 'expireDate',
		header : biolims.common.expirationDate,
		width : 100,
		sortable : false,
		editable : true,
		renderer : formatDate,
		tooltip : biolims.common.editable,
		editor : expireDate
	});
	cm.push({
		dataIndex : 'qcState',
		header : biolims.common.qcState,
		width : 150,
		sortable : false,
		hidden : false,
	});
	cm.push({
		dataIndex : 'note2',
		header : biolims.common.note,
		width : 150,
		sortable : false,
		hidden : false,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx
			+ '/storage/out/getStorageOutItemList.action?outId='+$("#so_id").val();
	var opts = {};
	opts.tbar = [];
	opts.title = biolims.sample.outBoundDetail;
	opts.height = document.body.clientHeight * 0.95;
	opts.rowselect = function(id) {
		$("#selectId").val(id);
	};
	opts.rowdblclick = function(id) {
		$('#selectId').val(id);
	};
//	opts.tbar.push({
//		text : biolims.common.fillDetail,
//		handler : function(){
//			 var ob = gridGrid.getStore().recordType;
//				var p = new ob({});
//				p.isNew = true;
//				gridGrid.getStore().add(p);
//			    gridGrid.startEditing(0, 0);
//		}
//	});
	if($("#so_stateName").val()!="完成"){
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.selectionComponent,
		handler : function (){
			window.showMainStorageAllSelectList();
		}
	});
	 opts.delSelect = function(ids) {
			ajax("post", "/storage/out/delOutStorageItem.action", {
				ids : ids
			}, function(data) {
				if (data.success) {
					gridGrid.getStore().commitChanges();
					gridGrid.getStore().reload();
					message(biolims.common.deleteSuccess);
				} else {
					message(biolims.common.deleteFailed);
				}
			}, null);
		};
		if($("#so_id").val()&&$("#so_id").val()!="NEW"){
			opts.tbar.push({
				iconCls : 'save',
				text : biolims.common.save,
				handler : saveItem
			});
		}
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.scanSnAdd,
		handler : function() {
//			document.getElementById("many_bat_text").removeAttribute("readonly");
//			document.getElementById("many_bat_text").removeAttribute("class");
			$(".jquery-ui-warning").html(biolims.common.scanSnAddRemind);
			$("#many_bat_text").val("");
			var options = {};
			options.width = 474;
			options.height = 480;
			loadDialogPage(
					$("#many_bat_div"),
					biolims.common.scanSnAdd,
					null,
					{
						"Confirm" : function() {
							var positions = $("#many_bat_text").val();
							if (!positions) {
								message(biolims.common.pleaseScanSN);
								return;
							}
							var array = positions.split("\n");
							$.each(array,function(i, obj) {		
								ajax("post", "/storage/out/addStorageItembyNote.action", {
									note : obj
								}, function(data) {
										var ob = gridGrid.getStore().recordType;
										var p = new ob({});
										p.isNew = true;
										if (data.success) {
											p.set("storage-id",data.storageId);
											p.set("storage-name",data.storageName);
											p.set("storage-unit-name",data.storageUnitName);
											p.set("price",data.price);
											p.set("storage-type-id",data.storageTypeId);
											p.set("storage-type-name",data.storageTypeName);
											p.set("storageNum",data.storageNum);
											p.set("serial",data.serial);
											p.set("code",data.code);
											p.set("qcState",data.qcState);
											p.set("position-id",data.positionId);
											p.set("position-name",data.positionName);
											p.set("price",data.price);
											p.set("storage-jdeCode",data.storageJdeCode);
											p.set("note",obj);
										} else {
											p.set("note",obj);
										}
										gridGrid.getStore().add(p);
								}, null);
								
								gridGrid.startEditing(0, 0);
							});
							$(this).dialog("close");
						}
					}, true, options);
		}
	});	
	}
	gridGrid = gridEditTable("grid", cols, loadParam, opts);
	$("#grid").data("gridGrid", gridGrid);
	gridGrid.store.addListener('load', handleGridLoadEvent);
});
function handleGridLoadEvent(store, records) {
	var girdcount = 0;
	store.each(function(r) {
				if (r.get('qcState') == "未QC") {
					gridGrid.getView().getRow(girdcount).style.backgroundColor = 'red';
				}else{
				}
				
				girdcount = girdcount + 1;
			});
}
function setStorageItem(selRecord) {
	var record = gridGrid.getSelectionModel().getSelected();

	var oldNeedNum = record.get("num");
	var allPcNum = 0.0;

	for ( var ij = 0; ij < selRecord.length; ij++) {

		allPcNum = allPcNum + parseFloat(selRecord[ij].get('num'));
	}

	if (oldNeedNum > allPcNum) {

		alert(biolims.storage.selectTotalNumberOfBatch);
		return false;

	}

	record.set("serial", '');
	record.set("code", '');
	record.set('storageNum', '');

	var yyNeedNum0 = 0.0;

	var store10 = gridGrid.store;
	var gridCount10 = store10.getCount();
	for ( var ij1 = 0; ij1 < gridCount10; ij1++) {
		var record10 = store10.getAt(ij1);
		if (selRecord[0].get("id") && record10.get("serial")
				&& (record10.get("serial") == selRecord[0].get("id"))) {
			yyNeedNum0 = yyNeedNum0 + parseFloat(record10.get("num"));
			// alert("该批号已经被选择过，请谨慎处理！");

		}

	}

	record.set("serial", selRecord[0].get("id"));
	record.set("code", selRecord[0].get("serial"));
	record.set("qcState", selRecord[0].get("qcState"));
	record.set("position-id", selRecord[0].get("position-id"));
	record.set("position-name", selRecord[0].get("position-name"));
	record.set("price", selRecord[0].get("purchasePrice"));

	// alert(yyNeedNum1);
	var nowPcNum0 = parseFloat(selRecord[0].get('num')) - yyNeedNum0;

	record.set('storageNum', nowPcNum0);

	if ($("#mainType").val() == 'materials') {
		record.set('price', selRecord[0].get('purchasePrice'));
	}

	if (oldNeedNum > parseFloat(selRecord[0].get('num'))) {

		record.set("num", selRecord[0].get("num"));

	}
	oldNeedNum = oldNeedNum - parseFloat(selRecord[0].get('num'));
	if (oldNeedNum > 0) {

		for ( var ij = 1; ij < selRecord.length; ij++) {

			if (oldNeedNum > 0) {
				var array = selRecord[ij].data;

				var Ob = gridGrid.getStore().recordType;

				var p = new Ob({

				});

				p.set("storage-name", record.get("storage-name"));
				p.set("storage-id", record.get("storage-id"));
				p.set("storage-type-id", record.get("storage-type-id"));
				p.set("storage-type-name", record.get("storage-type-name"));
				p.set("storage-studyType-name", record
						.get("storage-studyType-name"));
				p.set("qcState",record.get("qcState"));
				p.set("storage-unit-name", record.get("storage-unit-name"));
				p.set("storage-jdeCode", record.get("storage-jdeCode"));
				p.set("storage-position-name", record
						.get("storage-position-name"));
				var yyNeedNum = 0.0;
				var yyNeedNum1 = 0.0;
				var store1 = gridGrid.store;
				var gridCount1 = store1.getCount();
				for ( var ij1 = 0; ij1 < gridCount1; ij1++) {
					var record1 = store1.getAt(ij1);
					if (selRecord[ij].get("id")
							&& record1.get("serial")
							&& (record1.get("serial") == selRecord[ij]
									.get("id"))) {

						yyNeedNum1 = yyNeedNum1
								+ parseFloat(record1.get("num"));
						// alert("该批号已经被选择过，请谨慎处理！");

					}

				}

				p.set("serial", selRecord[ij].get("id"));
				p.set("code", selRecord[ij].get("serial"));
				p.set('price', selRecord[ij].get('purchasePrice'));

				var nowPcNum = parseFloat(selRecord[ij].get('num'))
						- yyNeedNum1;
				p.set('storageNum', nowPcNum);
				if (oldNeedNum > nowPcNum) {
					p.set("num", nowPcNum);
				} else {
					p.set("num", oldNeedNum);
				}
				oldNeedNum = oldNeedNum - parseFloat(selRecord[ij].get('num'));

				gridGrid.stopEditing();
				gridGrid.store.insert(0, p);

				gridGrid.startEditing(0, 0);
				gridGrid.getView().getRow(0).style.backgroundColor = RECORD_INSERT_COLOR;

			}
		}
	}

	var win = Ext.getCmp('showMainStorageItemAllSelectList');
	if (win) {
		win.close();
	}
}

function newSave() {
	save();
}
function save() {
	var selRecord = gridGrid.store.getModifiedRecords();
	if (selRecord && selRecord.length > 0) {
		var itemIds = [];
		$.each(selRecord, function(i, obj) {
			itemIds.push(obj.get("storage-id"));
		});
		var ids = "'" + itemIds.join("','") + "'";

		ajax("post", "/storage/common/showStroageId.action", {
			data : ids
		}, function(data) {
			if (data.message) {
				message(biolims.storage.objectNumber + data.message
						+ biolims.storage.isInStock);
				return false;
			} else {
				submitForm();
			}
		}, null);
	} else {
		submitForm();
	}
}
function saveItem(){
	var id=$("#so_id").val();
	var gridGrid = $("#grid").data("gridGrid");
	var data = commonGetModifyRecords(gridGrid);
	if (data && data.length > 0) {
		ajax("post", "/storage/out/saveAjax.action", {
			id:id,
			data : data
		}, function(data) {
			if(data.success){
				gridGrid.getStore().reload();
			}
		}, null);
	} 
}
function submitForm() {
	if (checkSubmit() == false) {
		return false;
	}
	var data = makeFormJsonData();
	var params = "({data:'" + data + "'})";
	params = eval(params);
	params.soId = eval("(" + data + ")")[0]["so.id"];
	var myMask = new Ext.LoadMask(Ext.getBody(), {
		msg : biolims.common.pleaseWait
	});
	myMask.show();
	document.getElementById('jsonDataStr').value = commonGetModifyRecords(gridGrid);
	storageOutForm.action = window.ctx + "/storage/out/save.action";
	storageOutForm.submit();
}

function modifyData(params) {
	var url = window.ctx + "/storage/out/saveStorageOutItem.action";
	if (trim(document.getElementById("so_id").value) != '') {
		var paramsValue = "outId:'" + document.getElementById("so_id").value
				+ "'";
		commonModifyData(params, paramsValue, url, gridGrid);
	} else {
		message(biolims.common.IdEmpty);
	}
}

function delData(params) {
	var url = window.ctx + "/storage/out/delOutStorageItem.action";
	commonDelData(params, url, gridGrid);
}

function checkSubmit() {
	var mess = "";
	var fs = [ "so_id","so_useUser_id" ];
	var nsc = [ biolims.common.IdEmpty,biolims.common.useArtificialNumberRequired];
	mess = commonFieldsNotNullVerifyById(fs, nsc);
	if (mess != "") {
		message(mess);
		return false;
	}
	if (validateAllFun() == false) {
		return false;
	}
	var id=$("#so_useUser_id").val();
	var boo=true;
	ajax("post", "/storage/out/searchUser.action", {
		id : id
	}, function(data) {
		if (data.message) {
			message(biolims.common.theLeaderDoesNotExist);
			boo=false;
		} else {
			$("#toolbarbutton_save").hide();
		}
	}, null);
	return boo;
}
function addStorageGrid(record) {

	var fields = [ 'storage-name', 'storage-id', 'storage-searchCode',
			'storage-unit-name', 'storage-jdeCode','storage-type-id', 'storage-type-name',
			'storageNum' ];
	var fieldsfrom = [ 'name', 'id', 'searchCode', 'unit-name','jdeCode', 'type-id',
			'type-name', 'num' ];

	addgridNotCheckUnique(gridGrid, record, fields, fieldsfrom);

}

var num = new Ext.form.NumberField({
	allowBlank : true,
	allowNegative : false,

	validator : validateRecordFun
});
var serial = new Ext.form.TextField({
	allowBlank : true,
	maxLength : 32
});
var code = new Ext.form.TextField({
	allowBlank : true,
	maxLength : 32
});
var price = new Ext.form.NumberField({
	allowBlank : true,
	allowNegative : false
});
function validateRecordFun(num) {
	var record = gridGrid.getSelectionModel().getSelected();

	return true;
}

function validateRecordFun(num) {
	var record = gridGrid.getSelectionModel().getSelected();

	if (parseFloat(num) > record.get('storageNum')) {
		alertMessage(biolims.storage.deliveryGreaterInventoryQuantity);
		return biolims.storage.deliveryGreaterInventoryQuantity;
	}
	return true;
}

serial.on('focus', function() {
	showStorageItem();
});

function showStorageItem() {
	var record = gridGrid.getSelectionModel().getSelected();
	if (record.get('storage-type-id').substring(0, 2) == '12') {
		showMainStorageItemAllSelectList(record.get('storage-id'));
	} else {
		alertMessage(biolims.storage.notChooseBatch);
	}
}

function formatCss(value, cellmeta, record, rowIndex, columnIndex, store) {
	if (value == 0) {
		cellmeta.css = "redFont";
	}

	var orderNum = parseFloat(record.get('storageNum'));
	if (value > orderNum) {
		cellmeta.css = "redFont";
		// value = orderNum;
	}

	return value;
}
function validateFun(record) {

	return true;
}
var item = menu.add({
	text : biolims.common.modify
});
item.on('click', changeMessage);
function changeMessage(){
	var id=$("#so_id").val();
		ajax("post", "/storage/out/alertStorageOut.action", {
			id:id
		}, function(data) {
			if(data.success){
				window.location.reload();
			}
		}, null);
}
function validateAllFun() {
	var flag = true;
	var store1 = gridGrid.store;
	// store1.filter('storage-id',rec.get('storage-id'));
	var gridCount = store1.getCount();
	for ( var ij = 0; ij < gridCount; ij++) {
		var record = store1.getAt(ij);
		// if (record.get('num') == '' || record.get('num') == null ) {
		// alertMessage("出库数量不能为空。");
		// return false;
		// }
		if (record.get('storage-type-id').substring(0, 2) == '12') {
			if (!record.get('serial')) {
				if (confirm(biolims.storage.continueOper)) {
					flag = true;
				} else {
					flag = false;
				}
			}
		}
		if (flag == false) {
			return false;
		}
		if (record.get('num') > record.get('storageNum')) {
			if (confirm(biolims.storage.outboundQuantityContinueOper)) {
				break;
			} else {
				return false;
			}
		}
	}
	return true;
}

function showMainStorageItemAllSelectList(id) {
	var win = Ext.getCmp('showMainStorageItemAllSelectList');
	if (win) {
		win.close();
	}
	var showMainStorageItemAllSelectList = new Ext.Window(
			{
				id : 'showMainStorageItemAllSelectList',
				modal : true,
				title : biolims.storage.chooseStockBatch,
				layout : 'fit',
				width : 1000,
				height : 500,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"
									+ window.ctx
									+ "/storage/common/showMainStorageItemAllSelectListByCheck.action?tableId=storageOut&storageId="
									+ id
									+ "' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						showMainStorageItemAllSelectList.close();
					}
				} ]
			});
	showMainStorageItemAllSelectList.show();
}
var store1;
function insertStorageItem() {
	var gridCount = store1.getCount();
	for ( var ij = 0; ij < gridCount; ij++) {
		var record = store1.getAt(ij);
		var Ob = gridGrid.getStore().recordType;
		var p = new Ob({});
		p.set("storage-id", record.get("storage-id"));
		p.set("storage-name", record.get("storage-name"));
		p.set("storage-unit-name", record.get("storage-unit-name"));
		p.set("price", record.get("storage-outPrice"));
		p.set("storage-currencyType-name", record
				.get("storage-currencyType-name"));
		p.set("storage-position-name", record.get("storage-position-name"));
		p.set("storage-type-id", record.get("storage-type-id"));
		p.set("storage-type-name", record.get("storage-type-name"));
		p.set("storageNum", record.get("storage-num"));
		gridGrid.stopEditing();
		gridGrid.store.insert(0, p);
		gridGrid.startEditing(0, 0);
		gridGrid.getView().getRow(0).style.backgroundColor = RECORD_INSERT_COLOR;
	}
}
function printReport() {
	var url = '__report=storageOut.rptdesign&storageId=' + $("#so_id").val();
	commonPrint(url);
}

function formatCssYj(value, cellmeta, record, rowIndex, columnIndex, store) {
	if (value == 0) {
		cellmeta.css = "redFont";
	}
	return value;
}

function formatCss(value, cellmeta, record, rowIndex, columnIndex, store) {
	if (value == 0) {
		cellmeta.css = "redFont";
	}
	return value;
}
function setGridId() {
	var gridCount = gridGrid.store.getCount();
	for ( var ij = 0; ij < gridCount; ij++) {
		var record = gridGrid.store.getAt(ij);
		record.set("id", "");
	}
	Ext.getCmp('showEditColumn').handler();
}
Ext.onReady(function() {
	var outId = document.getElementById("so_id").value;
	var item1 = {
		title : biolims.storage.deliveryInformation,
		contentEl : 'markup'
	};
	var tabs = new Ext.TabPanel({
		renderTo : 'maintab',
		height : document.body.clientHeight - 30,
		autoWidth : true,
		activeTab : 0,
		margins : '0 0 0 0',
		items : [ item1 ]
	});  	
});
function showMainStorageAllSelectList() {
	var htmlUrl;
	htmlUrl = "<iframe scrolling='no' name='maincontentframe' src='"
			+ window.ctx
			+ "/storage/common/showMainStorageAllSelectList.action?type=1&dataType=apply&flag=showMainStorageAllSelectList' frameborder='0' width='100%' height='100%' ></iframe>";
	var win = Ext.getCmp('showMainStorageAllSelectList');
	if (win) {
		win.close();
	}
	var showMainStorageAllSelectList = new Ext.Window({
		id : 'showMainStorageAllSelectList',
		modal : true,
		title : biolims.storage.selectInventoryItems,
		layout : 'fit',
		width : document.body.clientWidth / 1.3,
		height : document.body.clientHeight / 1.5,
		closeAction : 'close',
		plain : true,
		bodyStyle : 'padding:5px;',
		buttonAlign : 'center',
		collapsible : true,
		maximizable : true,
		items : new Ext.BoxComponent({
			id : 'maincontent',
			region : 'center',
			html : htmlUrl
		}),
		buttons : [ {
			text : biolims.common.close,
			handler : function() {
				showMainStorageAllSelectList.close();
			}
		} ]
	});
	showMainStorageAllSelectList.show();
}
function viewProject() {
	if (trim(document.getElementById('so.project.id').value) == '') {
		message(biolims.common.selectRecord);
	} else {
		openDialog(window.ctx + '/project/project/toViewProject.action?id='
				+ document.getElementById('so.project.id').value);
	}
}
function viewStorageApply() {
	if (trim(document.getElementById('so.storageApply.id').value) == '') {
		message(biolims.common.selectRecord);
	} else {
		openDialog(window.ctx + '/storage/apply/toViewStorageApply.action?id='
				+ document.getElementById('so.storageApply.id').value);
	}
}
function viewExperient() {
	if (trim(document.getElementById('so.experiment.id').value) == '') {
		message(biolims.common.selectRecord);
	} else {
		openDialog(window.ctx
				+ '/experiment/experiment/toViewExperiment.action?id='
				+ document.getElementById('so.experiment.id').value);
	}
}
function add() {
	window.location = window.ctx + '/storage/out/toEditStorageOut.action';
}
function list() {
	window.location = window.ctx
			+ '/storage/out/showStorageOutList.action?queryMethod=' + "1";
}

function workflowSubmit() {

	submitWorkflow("StorageOut", {
		userId : userId,
		userName : userName,
		formId : $("#so_id").val(),
		title : $("#so_id").val()
	}, function() {
		window.location.reload();
	});

	// commonWorkflowSubmit("applicationTypeTableId=storageOut&formId=" +
	// $("#so_id").val() + "&title="
	// + encodeURI(encodeURI($("#so_id").val())));
}
function workflowLook(obj) {

	completeTask($("#so_id").val(), $(obj).attr("taskId"), function() {
		document.getElementById('toolbarSaveButtonFlag').value = 'save';
		location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
	});

}
function workflowView() {
	commonWorkflowView("applicationTypeTableId=storageOut&formId="
			+ $("#so_id").val());
}
function changeState() {
//	var selStore=gridGrid.store;
//	for(var i=0;i<selStore.getCount();i++){
////		if(selStore.getAt(i).get("expireDate")==null){
////			message("请填写过期日期！");
////			return;
////		}
//	}
	commonChangeState("tableId=StorageOut&formId="
			+ $("#so_id").val());
}

$(function() {
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "modify") {
		var t = "#so_id";
		settextreadonlyById(t);
	}
	if (handlemethod == "view") {
		settextreadonlyByAll();
	}

});