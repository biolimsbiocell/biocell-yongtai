//待入库样本-即库存主数据(树状带批次)
var storageOutAllTab;
hideLeftDiv();
var storageOut_id = $("#storageOut_id").text();
$(function() {
	
	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "view") {
		settextreadonly();
	}
	
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : biolims.tStorage.id,
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push({
		"data" : "name",
		"title" : biolims.tStorage.name,
		"createdCell" : function(td) {
			$(td).attr("saveName", "name");
		}
	});
//	colOpts.push({
//		"data" : "barCode",
//		"title" : biolims.tStorage.barCode,
//		"createdCell" : function(td) {
//			$(td).attr("saveName", "barCode");
//		}
//	});
	colOpts.push({
		"data": "jdeCode",
//		"title": 'JDE Code',
		"title": '货号',
		"createdCell": function(td, data) {
			$(td).attr("saveName", "jdeCode");
		},
	})
	
	colOpts.push({
		"data" : "spec",
		"title" : biolims.tStorage.spec,
		"createdCell" : function(td) {
			$(td).attr("saveName", "spec");
		}
	})
	var tbarOpts = [];
	
	//选择批次
	tbarOpts.push({
		text : biolims.common.batch,
		action : function() {
			var id= "";
			$(".selected").each(function(i,v){
				id += $(v).find("input").val()+",";
			})
			top.layer.open({
				title: biolims.common.batch,
				type: 2,
				area: ["650px", "400px"],
				btn: biolims.common.selected,
				content: [window.ctx + "/storage/selStorageBatch.action?id="+id, ''],
				yes: function(index, layer) {
					
					var rows=$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addStorageBatchTable .selected");
					var batchIds="";
					$.each(rows, function(j, k) {
						batchIds+=($(k).children("td").eq(2).text())+",";
					});
//					console.log(batchIds);
					top.layer.close(index);
					// 先保存主表信息
					var note = $("#storageOut_note").val();
					var outUser = $("#storageOut_outUser").attr("userId");
					var outDate = $("#storageOut_outDate").text();
					var useUser = $("#storageOut_useUser_id").val();
					var id = $("#storageOut_id").text();
					
					//添加数据到出库明细表
								$.ajax({
								type : 'post',
								url : '/storage/out/addStorageOutItem.action',
								data : {
									batchIds : batchIds,
									note : note,
									useUser : useUser,
									outDate : outDate,
									outUser : outUser,
									id : id,
									
								},
								success : function(data) {
									var data = JSON.parse(data)
									if (data.success) {
										$("#storageOut_id").text(data.data);
										var param = {
												id: data.data
											};	
										var storageOutItemTabs = $("#storageOutItemdiv")
											.DataTable();
										storageOutItemTabs.settings()[0].ajax.data = param;
										storageOutItemTabs.ajax.reload();
									

									} else {

									}
									;
								}
							});
					
				},
			})
				

		}
	});
	tbarOpts.push({
		text: '<i class="fa fa-ioxhost"></i>'+biolims.common.search,
		action:function(){
			search()
		}
	});
	
	

	var storageOutAllOps = table(true, storageOut_id,
			"/storage/showStorageTableJson.action?p_type=", colOpts, tbarOpts);
	storageOutAllTab = renderData($("#storageOutAlldiv"), storageOutAllOps);

	// 选择数据并提示
	storageOutAllTab.on('draw', function() {
		var index = 0;
		$("#storageOutAlldiv .icheck").on('ifChanged', function(event) {
			if ($(this).is(':checked')) {
				index++;
			} else {
				index--;
			}
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
	});
	bpmTask($("#bpmTaskId").val());
	
	$("#form1 :input").change(function(){ 
	     $("#form1").data("changed",true);   
	  });
});

function list() {
	window.location = window.ctx
			+ '/storage/out/showStorageOutTable.action';
}

//弹框模糊查询参数
function searchOptions() {
	return [{
		"txt" : biolims.tStorage.id,
		"type" : "input",
		"searchName" : "id",
	},{
		"txt" : biolims.tStorage.name,
		"type" : "input",
		"searchName" : "name",
	}, {
		"type" : "table",
		"table" : storageOutAllTab
	} ];
}

//大保存
function save() {
	state=true;
	$("#storageOutItemdiv tbody tr").each(function(i,j,k){
		var packingNum=$(this).find("td[savename='packingNum']").text();
		debugger
		var unitGroupNew=$(this).find("td[savename='unitGroupNew-name']").text();
		var batchNum=$(this).find("td[savename='batchNum']").text();
		if(unitGroupNew==""){
			top.layer.msg("请选择单位组！")
			state=false;
			return false;
		}
		if(packingNum==""){
			top.layer.msg("请填写数量！")
			state=false;
			return false;
		}
		if(!/^(?!0$|0\.00|0\.0|0\d+$)([1-9]?\d+(\.\d*)|(\\s&&[^\\f\\n\\r\\t\\v])|([1-9]*[1-9][0-9]*)?)$/.test(packingNum)){
			top.layer.msg("请填写正确的数量！")
			state=false;
			return false;
		}
		if(parseInt(packingNum)>parseInt(batchNum)){
			top.layer.msg("数量不能大于本批次库存量！")
			state=false;
			return false;
		}
	})
	if(state){
	
		var changeLog = "库存出库";
		$('input[class="form-control"]').each(function(i, v) {
			var valnew = $(v).val();
			var val = $(v).attr("changelog");
			if(val !== valnew) {
				changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
			}
		});
		console.log($("#form1"));
		var jsonStr = JSON.stringify($("#form1").serializeObject());

		var dataItemJson = saveItemjson($("#storageOutItemdiv"));
		var ele = $("#storageOutItemdiv");
		var changeLogItem = "主数据出库明细：";
		changeLogItem = getChangeLog(dataItemJson, ele, changeLogItem);
		var changeLogItems="";
		if(changeLogItem !="核酸提取-样本异常："){
			changeLogItems=changeLogItem;
		}
		
		var id = $("#storageOut_id").text();
		var useUser = $("#storageOut_useUser_id").val();	
		var note = $("#storageOut_note").val();
		var ctype = $("#ctype").val();
		//必填验证
		var requiredField=requiredFilter();
			if(!requiredField){
				return false;
			}
		top.layer.load(4, {shade:0.3}); 
		$.ajax({
			url: ctx + '/storage/out/saveStorageOutAndItem.action',
			dataType: 'json',
			type: 'post',
			data: {
				dataValue: jsonStr,
				changeLog: changeLog,
				ImteJson: dataItemJson,
				bpmTaskId: $("#bpmTaskId").val(),
				changeLogItem: changeLogItems,
				id:id,
				useUser:useUser,
				note:note,
				ctype:ctype
			},
			success: function(data) {
				if(data.success) {
					top.layer.closeAll();
					var url = "/storage/out/toEditStorageOut.action?id=" + data.id;

					url = url + "&bpmTaskId=" + $("#bpmTaskId").val() + "&p_type=" + $("#p_type").val();

					window.location.href = url;
				} else {
					top.layer.closeAll();
					top.layer.msg(data.msg);

				}
			}

		});
	}

}

$.fn.serializeObject = function()
{
   var o = {};
   var a = this.serializeArray();
   $.each(a, function() {
       if (o[this.name]) {
           if (!o[this.name].push) {
               o[this.name] = [o[this.name]];
           }
           o[this.name].push(this.value || '');
       } else {
           o[this.name] = this.value || '';
       }
   });
   return o;
};
function tjsp() {
	if($("#form1").data("changed") ||$("#storageOut_id").text()=="NEW"){
	      top.layer.msg("请保存后提交!");
	      return false;
	     }
	top.layer
			.confirm(
					biolims.common.pleaseConfirmSaveBeforeSubmit,
					{
						icon : 3,
						title : biolims.common.prompt
					},
					function(index) {
						top.layer
								.open({
									title : biolims.common.approvalTask,
									type : 2,
									anim : 2,
									area : [ '800px', '500px' ],
									btn : biolims.common.selected,
									content : window.ctx
											+ "/workflow/processinstance/toStartView.action?formName=DnaTask",
									yes : function(index, layer) {
										var datas = {
											userId : userId,
											userName : userName,
											formId : storageOut_id,
											title : $("#storageOut_name").val(),
											formName : "StorageOut"
										}
										ajax(
												"post",
												"/workflow/processinstance/start.action",
												datas,
												function(data) {
													if (data.success) {
														top.layer
																.msg(biolims.common.submitSuccess);
														if (typeof callback == 'function') {
															callback(data);
														}
														// dialogWin.dialog("close");
													} else {
														top.layer
																.msg(biolims.common.submitFail);
													}
												}, null);
										top.layer.close(index);
									},
									cancel : function(index, layer) {
										top.layer.close(index)
									}

								});
						top.layer.close(index);
					});

}
function sp() {
	var taskId = $("#bpmTaskId").val();
	var formId = storageOut_id;

	top.layer.open({
		title : biolims.common.detailedExaminationAndApproval,
		type : 2,
		anim : 2,
		area : [ '800px', '500px' ],
		btn : biolims.common.selected,
		content : window.ctx
				+ "/workflow/processinstance/toCompleteTaskView.action?taskId="
				+ taskId + "&formId=" + formId,
		yes : function(index, layer) {
			var operVal = $('.layui-layer-iframe', parent.document).find("iframe").contents()
					.find("#oper").val();
			var opinionVal = $('.layui-layer-iframe', parent.document).find("iframe").contents()
					.find("#opinionVal").val();

			if (!operVal) {
				top.layer.msg(biolims.common.pleaseSelectOper);
				return false;
			}
			if (operVal == "2") {
				_trunTodoTask(taskId, callback, dialogWin);
			} else {
				var paramData = {};
				paramData.oper = operVal;
				paramData.info = opinionVal;

				var reqData = {
					data : JSON.stringify(paramData),
					formId : formId,
					taskId : taskId,
					userId : window.userId
				}
				ajax("post", "/workflow/processinstance/completeTask.action",
						reqData, function(data) {
							if (data.success) {
								top.layer.msg(biolims.common.submitSuccess);
								if (typeof callback == 'function') {
								}
							} else {
								top.layer.msg(biolims.common.submitFail);
							}
						}, null);
			}
			top.layer.closeAll();
			location.href = window.ctx + "/lims/pages/dashboard/dashboard.jsp";
		}
	});
}


function showUseUser(){
	$("#form1").data("changed",true);  
	top.layer.open({
		title: biolims.user.selectionLeader,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/core/user/selectUserTable.action?groupId=admin", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(0).text();
			$("#storageOut_useUser_name").val(name);
			$("#storageOut_useUser_id").val(id);
			top.layer.close(index);
		},
	})
}



function changeState() {
	if($("#form1").data("changed") ||$("#storageOut_id").text()=="NEW"){
	      top.layer.msg("请保存后提交!");
	      return false;
	     }
    var	id=$("#storageOut_id").text()
	var paraStr = "formId=" + id +
		"&tableId=StorageOut";
	console.log(paraStr)
	top.layer.open({
		title: biolims.common.approvalProcess ,
		type: 2,
		anim: 2,
		area: ['400px', '400px'],
		btn: biolims.common.selected,
		content: window.ctx +
			"/applicationTypeAction/applicationTypeActionLook.action?" + paraStr +
			"&flag=changeState'",
		yes: function(index, layer) {
			top.layer.confirm(biolims.common.approve, {
				icon: 3,
				title: biolims.common.prompt,
				btn:biolims.common.selected
			}, function(index) {
				ajax("post", "/applicationTypeAction/exeFun.action", {
					applicationTypeActionId: $('.layui-layer-iframe', parent.document).find("iframe").contents().find("input:checked").val(),
					formId: id
				}, function(response) {
					var respText = response.message;
					if(respText == '') {
						window.location.reload();
					} else {
						top.layer.msg(respText);
					}
				}, null)
				top.layer.closeAll();
			})
		}
	});
	
	
}
