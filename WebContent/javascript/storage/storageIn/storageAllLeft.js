//待入库样本-即库存主数据
var storageAllTab;
hideLeftDiv();
var storageIn_id = $("#storageIn_id").text();
$(function() {
	$("#storageIn_handelDate").datepicker({
		format: 'yyyy-mm-dd',
		autoclose: true,
		language: 'zh-CN',
	}); 
	
	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "view") {
		settextreadonly();
	}
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : biolims.tStorage.id,
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push({
		"data" : "name",
		"title" : biolims.tStorage.name,
		"createdCell" : function(td) {
			$(td).attr("saveName", "name");
		}
	});
//	colOpts.push({
//		"data" : "barCode",
//		"title" : biolims.tStorage.barCode,
//		"createdCell" : function(td) {
//			$(td).attr("saveName", "barCode");
//		}
//	});
	colOpts.push({
		"data": "jdeCode",
		"title": '货号',
		"createdCell": function(td, data) {
			$(td).attr("saveName", "jdeCode");
		},
	})
	
	colOpts.push({
		"data" : "spec",
		"title" : biolims.tStorage.spec,
		"createdCell" : function(td) {
			$(td).attr("saveName", "spec");
		}
	})
	var tbarOpts = [];
	tbarOpts.push({
		text :biolims.common.addToDetail,
		action : function() {
			var sampleId = [];
			$("#storageAlldiv .selected").each(function(i, val) {
				sampleId.push($(val).children("td").eq(0).find("input").val());
			});

			// 先保存主表信息
			var note = $("#storageIn_note").text();
			var createUser = $("#storageIn_handelUser").attr("userId");;
			var handelDate = $("#storageIn_handelDate").val();
			var createDate = $("#storageIn_createDate").text();
			var id = $("#storageIn_id").text();
			$.ajax({
				type : 'post',
				url : '/storage/storageIn/addStorageInItem.action',
				data : {
					ids : sampleId,
					note : note,
					handelDate : handelDate,
					id : id,
					createDate : createDate,
					createUser : createUser
				},
				success : function(data) {
					var data = JSON.parse(data)
					if (data.success) {
						$("#storageIn_id").text(data.data);
						var param = {
								id: data.data
							};	
						var storageInItemTabs = $("#storageInItemdiv")
							.DataTable();
						storageInItemTabs.settings()[0].ajax.data = param;
						storageInItemTabs.ajax.reload();
					} else {
						top.layer.msg('添加失败');
					}
					;
				}
			})
		}
	});
	
	tbarOpts.push({
		text: '<i class="fa fa-ioxhost"></i>'+biolims.common.search,
		action:function(){
			search()
		}
	});
	

	var storageAllOps = table(true, storageIn_id,
			"/storage/showStorageTableJson.action?p_type=", colOpts, tbarOpts);
	storageAllTab = renderData($("#storageAlldiv"), storageAllOps);

	// 选择数据并提示
	storageAllTab.on('draw', function() {
		var index = 0;
		$("#storageAlldiv .icheck").on('ifChanged', function(event) {
			if ($(this).is(':checked')) {
				index++;
			} else {
				index--;
			}
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
	});
	bpmTask($("#bpmTaskId").val());
	$("#form1 :input").change(function(){ 
	     $("#form1").data("changed",true);   
	  });
});



//弹框模糊查询参数
function searchOptions() {
	return [{
		"txt" : biolims.tStorage.id,
		"type" : "input",
		"searchName" : "id",
	},{
		"txt" : biolims.tStorage.name,
		"type" : "input",
		"searchName" : "name",
	}, {
		"type" : "table",
		"table" : storageAllTab
	} ];
}


//大保存
function save() {
	
	var scTime="";
	$("#storageInItemdiv tbody tr").each(function(i,v){
		scTime = $(v).find("[savename='productDate']").text();
		
	})
	if(scTime==""){
		top.layer.msg("生产日期不能为空!");
		return false;
	}
	var sxTime
	$("#storageInItemdiv tbody tr").each(function(i,v){
		sxTime = $(v).find("[savename='expireDate']").text();
		
	})
	if(sxTime==""){
		top.layer.msg("失效日期不能为空!");
		return false;
	}
	
		var changeLog = "库存入库:";
		$('input[class="form-control"]').each(function(i, v) {
			var valnew = $(v).val();
			var val = $(v).attr("changelog");
			if(val !== valnew) {
				changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
			}
		});
		var changeLogs="";
		if(changeLog !="库存入库:"){
			changeLogs=changeLog;
		}
		
		console.log($("#form1"));
		var jsonStr = JSON.stringify($("#form1").serializeObject());

		var dataItemJson = saveItemjson($("#storageInItemdiv"));
		if(saveItemjsonpd($("#storageInItemdiv"))){
			var ele = $("#storageInItemdiv");
			var changeLogItem = "主数据入库明细：";
			changeLogItem = getChangeLog(dataItemJson, ele, changeLogItem);
			var changeLogItems="";
			if(changeLogItem !="主数据入库明细："){
				changeLogItems=changeLogItem;
			}
			var id = $("#storageIn_id").text();
			var createUser = $("#storageIn_handelUser").attr("userId");;
			var createDate = $("#storageIn_createDate").text();
			//必填验证
			var requiredField=requiredFilter();
				if(!requiredField){
					return false;
				}
			top.layer.load(4, {shade:0.3}); 
			$.ajax({
				url: ctx + '/storage/storageIn/saveStorageInAndItem.action',
				dataType: 'json',
				type: 'post',
				data: {
					dataValue: jsonStr,
					changeLog: changeLogs,
					ImteJson: dataItemJson,
					bpmTaskId: $("#bpmTaskId").val(),
					changeLogItem: changeLogItems,
					id:id,
					createUser:createUser,
					createDate:createDate
				},
				success: function(data) {
					if(data.success) {
						top.layer.closeAll();
						var url = "/storage/storageIn/toEditStorageIn.action?id=" + data.id;

						url = url + "&bpmTaskId=" + $("#bpmTaskId").val() + "&p_type=" + $("#p_type").val();

						window.location.href = url;
					} else {
						top.layer.closeAll();
						top.layer.msg("请选择部门!");

					}
				}

			});
		}else{
			
		}
		

}

$.fn.serializeObject = function()
{
   var o = {};
   var a = this.serializeArray();
   $.each(a, function() {
       if (o[this.name]) {
           if (!o[this.name].push) {
               o[this.name] = [o[this.name]];
           }
           o[this.name].push(this.value || '');
       } else {
           o[this.name] = this.value || '';
       }
   });
   return o;
};
function tjsp() {
	if($("#form1").data("changed") ||$("#storageIn_id").text()=="NEW"){
	      top.layer.msg("请保存后提交!");
	      return false;
	     }
	top.layer
			.confirm(
					biolims.common.pleaseConfirmSaveBeforeSubmit,
					{
						icon : 3,
						title : biolims.common.prompt
					},
					function(index) {
						top.layer
								.open({
									title : biolims.common.approvalTask,
									type : 2,
									anim : 2,
									area : [ '800px', '500px' ],
									btn : biolims.common.selected,
									content : window.ctx
											+ "/workflow/processinstance/toStartView.action?formName=DnaTask",
									yes : function(index, layer) {
										var datas = {
											userId : userId,
											userName : userName,
											formId : storageIn_id,
											title : $("#storageIn_name").val(),
											formName : "StorageIn"
										}
										ajax(
												"post",
												"/workflow/processinstance/start.action",
												datas,
												function(data) {
													if (data.success) {
														top.layer
																.msg(biolims.common.submitSuccess);
														if (typeof callback == 'function') {
															callback(data);
														}
														// dialogWin.dialog("close");
													} else {
														top.layer
																.msg(biolims.common.submitFail);
													}
												}, null);
										top.layer.close(index);
									},
									cancel : function(index, layer) {
										top.layer.close(index)
									}

								});
						top.layer.close(index);
					});

}
function sp() {
	var taskId = $("#bpmTaskId").val();
	var formId = storageIn_id;

	top.layer.open({
		title : biolims.common.detailedExaminationAndApproval,
		type : 2,
		anim : 2,
		area : [ '800px', '500px' ],
		btn : biolims.common.selected,
		content : window.ctx
				+ "/workflow/processinstance/toCompleteTaskView.action?taskId="
				+ taskId + "&formId=" + formId,
		yes : function(index, layer) {
			var operVal = $('.layui-layer-iframe', parent.document).find("iframe").contents()
					.find("#oper").val();
			var opinionVal = $('.layui-layer-iframe', parent.document).find("iframe").contents()
					.find("#opinionVal").val();

			if (!operVal) {
				top.layer.msg(biolims.common.pleaseSelectOper);
				return false;
			}
			if (operVal == "2") {
				_trunTodoTask(taskId, callback, dialogWin);
			} else {
				var paramData = {};
				paramData.oper = operVal;
				paramData.info = opinionVal;

				var reqData = {
					data : JSON.stringify(paramData),
					formId : formId,
					taskId : taskId,
					userId : window.userId
				}
				ajax("post", "/workflow/processinstance/completeTask.action",
						reqData, function(data) {
							if (data.success) {
								top.layer.msg(biolims.common.submitSuccess);
								if (typeof callback == 'function') {
								}
							} else {
								top.layer.msg(biolims.common.submitFail);
							}
						}, null);
			}
			location.href = window.ctx + "/lims/pages/dashboard/dashboard.jsp";
		},
		cancel : function(index, layer) {
			top.layer.close(index)
		}

	});
}

function changeState() {
	
    var	id=$("#storageIn_id").text()
	var paraStr = "formId=" + id +
		"&tableId=StorageIn";
    var panduan = false;
    var cunzaineirong = "";
    ajax("post", window.ctx + "/storage/storageIn/findwhile.action",
			{id:id}, function(data) {
		if (data.success) {
			if(data.cunzai){
				cunzaineirong = data.cunzaineirong;
				panduan = true;
			}else{
				
			}
		} else {
			
		}
	}, null);
    if(panduan){
    	top.layer.confirm(cunzaineirong, {
			icon: 3,
			title: "请确认",
			btn:biolims.common.selected
		}, function(indexa) {
			top.layer.open({
				title: biolims.common.approvalProcess,
				type: 2,
				anim: 2,
				area: ['400px', '400px'],
				btn: biolims.common.selected,
				content: window.ctx +
					"/applicationTypeAction/applicationTypeActionLook.action?" + paraStr +
					"&flag=changeState'",
				yes: function(index, layer) {
					top.layer.confirm(biolims.common.approve, {
						icon: 3,
						title: biolims.common.prompt,
						btn:biolims.common.selected
					}, function(index) {
						ajax("post", "/applicationTypeAction/exeFun.action", {
							applicationTypeActionId: $('.layui-layer-iframe', parent.document).find("iframe").contents().find("input:checked").val(),
							formId: id
						}, function(response) {
							var respText = response.message;
							if(respText == '') {
								window.location.reload();
							} else {
								top.layer.msg(respText);
							}
						}, null)
						top.layer.closeAll();
					})
				},
				cancel: function(index, layer) {
					top.layer.close(index)
				}

			});
		});
    }else{
    	top.layer.open({
    		title: biolims.common.approvalProcess,
    		type: 2,
    		anim: 2,
    		area: ['400px', '400px'],
    		btn: biolims.common.selected,
    		content: window.ctx +
    			"/applicationTypeAction/applicationTypeActionLook.action?" + paraStr +
    			"&flag=changeState'",
    		yes: function(index, layer) {
    			top.layer.confirm(biolims.common.approve, {
    				icon: 3,
    				title: biolims.common.prompt,
    				btn:biolims.common.selected
    			}, function(index) {
    				ajax("post", "/applicationTypeAction/exeFun.action", {
    					applicationTypeActionId: $('.layui-layer-iframe', parent.document).find("iframe").contents().find("input:checked").val(),
    					formId: id
    				}, function(response) {
    					var respText = response.message;
    					if(respText == '') {
    						window.location.reload();
    					} else {
    						top.layer.msg(respText);
    					}
    				}, null)
    				top.layer.closeAll();
    			})
    		},
    		cancel: function(index, layer) {
    			top.layer.close(index)
    		}

    	});
    }


	
}


function list() {
	window.location = window.ctx
			+ '/storage/storageIn/showStorageInTable.action';
}
//选择部门
function choseParent() {
	top.layer.open({
		title: "选择部门",
		type: 2,
		area: ["60%","65%"],
		btn: biolims.common.selected,
		content: window.ctx +
			"/core/department/departmentSelect.action",
		yes: function(index, layer) {
			var name=[],id=[];
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#mytreeGrid .chosed").each(function (i,v) {
				name.push($(v).children("td").eq(2).text());
				id.push($(v).children("td").eq(1).text());
			});
			if(name.length!=1||id.length!=1){
				top.layer.msg(biolims.common.pleaseSelectAPieceOfData);
			}else{
				$("#storageIn_departmnet_name").val(name.join(","));
				$("#storageIn_departmnet_id").val(id.join(","));
				top.layer.close(index)
			}
		}
	})
}