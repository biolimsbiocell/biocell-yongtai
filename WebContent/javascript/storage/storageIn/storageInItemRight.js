var storageInItemTab, oldChangeLog;
//入库明细表
$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "storage-id",
		"title":  biolims.common.reagentNo,
		"createdCell": function(td) {
			$(td).attr("saveName", "storage-id");
		}
	})
	colOpts.push({
		"data": "storage-name",
		"title":  biolims.common.designation,
		"createdCell": function(td) {
			$(td).attr("saveName", "storage-name");
		}
	})
//	colOpts.push({
//		"data": "storage-barCode",
//		"title": biolims.tStorage.barCode,
//		"createdCell": function(td, data) {
//			$(td).attr("saveName", "storage-barCode");
//		},
//	})
	colOpts.push({
		"data": "storage-jdeCode",
		"title": '货号',
		"createdCell": function(td, data) {
			$(td).attr("saveName", "storage-jdeCode");
		},
	})
	colOpts.push({
		"data": "storage-spec",
		"title": biolims.storage.spec,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "storage-spec");
		},
	})
	colOpts.push({
		"data" : "productDate",
		"title" : "生产日期 "+ '<img src="/images/required.gif"/>',
		"className": "date",
		width : "150px",
		"createdCell" : function(td) {
			$(td).attr("saveName", "productDate");
		}
	})
	colOpts.push({
		"data": "serial",
		"title": "产品批号",
		"className": "edit",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "serial");
		},
	})
	colOpts.push({
		"data" : "expireDate",
		"title" : "失效日期 "+ '<img src="/images/required.gif"/>',
		"className": "date",
		width : "150px",
		"createdCell" : function(td) {
			$(td).attr("saveName", "expireDate");
		}
	});
	colOpts.push({
		"data": "unitGroupNew-name",
		"title": "单位组",
		"createdCell": function(td, data,rowdata) {
			$(td).attr("saveName", "unitGroupNew-name");
			$(td).attr("unitGroupNew-id",  rowdata["unitGroupNew-id"]);
			$(td).attr("unitGroupNew-cycle",  rowdata["unitGroupNew-cycle"]);//转换系数
		},
		"visible":false,
	});
	colOpts.push({
		"data": "packingNum",
		"title": "数量",
		"className": "edit",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "packingNum");
		},
		"visible":false,
	});
	colOpts.push({
		"data": "unitGroupNew-mark-name",
		"title": "主单位",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "unitGroupNew-mark-name");
		},
		"visible":false,
	});

	colOpts.push({
		"data": "num",
		"title": biolims.storage.inputNum,
		"className": "edit",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "num");
		},
	});
	colOpts.push({
		"data": "unitGroupNew-mark2-name",
		"title": "副单位",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "unitGroupNew-mark2-name");
		},
	});
	/*colOpts.push({
		"data": "storage-unit-name",
		"title": biolims.common.unit,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "storage-unit-name");
		},
	})*/
	
	/*colOpts.push({
		"data": "storage-reactionNum",
		"title": biolims.common.reactionNumber,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "storage-reactionNum");
		},
	})*/
	
//	colOpts.push({
//		"data": "position-id",
//		"title":biolims.common.storageId,
//		"createdCell": function(td, data) {
//			$(td).attr("saveName", "position-id");
//		},
//	})
//	colOpts.push({
//		"data": "position-name",
//		"title":biolims.common.storageName,
//		"createdCell": function(td, data) {
//			$(td).attr("saveName", "position-name");
//		},
//	})
	colOpts.push({
		"data": "storage-position-id",
		"title":  biolims.common.storageId,
		"createdCell": function(td) {
			$(td).attr("saveName", "storage-position-id");
		}
	})
	colOpts.push({
		"data": "storage-position-name",
		"title": biolims.common.storageName,
		"createdCell": function(td) {
			$(td).attr("saveName", "storage-position-name");
		}
	})
	colOpts.push({
		"data": "note2",
		"title": biolims.common.note,
		"className": "edit",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "note2");
		},
	})
		colOpts.push({
		"data": "note",
		"title":'sn',
		"createdCell": function(td, data) {
			$(td).attr("saveName", "note");
		},
	})
	
	var tbarOpts = [];
	tbarOpts.push({
		text: biolims.common.delSelected,
		action: function() {
			removeChecked($("#storageInItemdiv"),
				"/storage/storageIn/delStorageInItems.action","删除入库明细数据：",$("#storageIn_id").text());
		}
	});
	tbarOpts.push({
		text: biolims.common.Editplay,
		action: function() {
			editItemLayer($("#storageInItemdiv"));
		}
	});
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			savestorageInItemTab();
		}
	});
	
//	tbarOpts.push({
//		text: "选择单位组",
//		action: function() {
//			unitBatch();
//		}
//	});
	
//	tbarOpts.push({
//		text : "产物数量",
//		action : function() {
//			var rows = $("#storageInItemdiv .selected");
//			var length = rows.length;
//			if (!length) {
//				top.layer.msg("请选择数据");
//				return false;
//			}
//			top.layer.open({
//				type : 1,
//				title : "产物数量",
//				content : $('#batch_data'),
//				area:[document.body.clientWidth-600,document.body.clientHeight-200],
//				btn: biolims.common.selected,
//				yes : function(index, layer) {
//					var productNum = $("#productNum").val();
//					rows.addClass("editagain");
//					rows.find("td[savename='productNum']").text(productNum);
//					top.layer.close(index);
//				}
//			});
//		}
//	})
	var storageInItemOps = table(true, $("#storageIn_id").text(), "/storage/storageIn/showStorageInItemTableJson.action", colOpts, tbarOpts);
	storageInItemTab = renderData($("#storageInItemdiv"), storageInItemOps);
	//选择数据并提示
	storageInItemTab.on('draw', function() {
		var index = 0;
		$("#storageInItemdiv .icheck").on('ifChanged', function(event) {
			if($(this).is(':checked')) {
				index++;
				$("#plateModal").find(".mysample[sid='" + this.value + "']").css("border","2px solid #000");
			} else {
				var tt=$("#plateModal").find(".mysample[sid='" + this.value + "']").css("border","1px solid gainsboro");
				index--;
			}
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
		oldChangeLog = storageInItemTab.ajax.json();
		
	});
});
//批量单位组
function unitBatch() {
	var rows = $("#storageInItemdiv .selected");
	var length = rows.length;
	if (!length) {
		top.layer.msg(biolims.common.pleaseSelectData);
		return false;
	}
	top.layer
			.open({
				title : biolims.common.batchUnit,
				type : 2,
				area : [ "650px", "400px" ],
				btn : biolims.common.selected,
				content : [
						window.ctx
								+ "/com/biolims/system/work/unitGroupNew/showWorkTypeDialogList.action",
						'' ],
				yes : function(index, layer) {
					var id = $('.layui-layer-iframe', parent.document).find(
							"iframe").contents().find("#addUnitGroup .chosed")
							.children("td").eq(0).text();
					var name = $('.layui-layer-iframe', parent.document).find(
							"iframe").contents().find("#addUnitGroup .chosed")
							.children("td").eq(1).text();
					var mark1Name = $('.layui-layer-iframe', parent.document)
					.find("iframe").contents().find(
							"#addUnitGroup .chosed").children("td").eq(
							3).text();
					var mark2Name = $('.layui-layer-iframe', parent.document)
							.find("iframe").contents().find(
									"#addUnitGroup .chosed").children("td").eq(
									4).text();
					var cycle = $('.layui-layer-iframe', parent.document)
					.find("iframe").contents().find(
							"#addUnitGroup .chosed").children("td").eq(
							2).text();
					rows.addClass("editagain");
					
					
					
					rows.find("td[savename='unitGroupNew-name']").attr("unitGroupNew-id", id).attr("unitGroupNew-cycle", cycle).text(name);
					rows.find("td[savename='unitGroupNew-mark-name']").text(mark1Name);
					rows.find("td[savename='unitGroupNew-mark2-name']").text(mark2Name);
					top.layer.close(index);
				},
			});
}
//行内input编辑
/*function edittable() {
	document.addEventListener('click', function(event) {
		var evt = window.event || event;
		if(evt.target.nodeName.toLowerCase() == "td") {
			var ele = event.srcElement || evt.target;
			if($(ele).hasClass("date")) {
				changeDateInput(ele);
			} else if($(ele).hasClass("select")) {
				changeSelectInput(ele);
			} else if($(ele).hasClass("edit")) {
				changeTextInput(ele);
				//console.log("123456")
			} else if($(ele).hasClass("textarea")) {
				changeTextTextarea(ele);
			}
		}
	}, false)
	document.addEventListener('touchstart', function(event) {
		var evt = window.event || event;
		if(evt.target.nodeName.toLowerCase() == "td") {
			var ele = event.srcElement || evt.target;
			if($(ele).hasClass("date")) {
				changeDateInput(ele);
			} else if($(ele).hasClass("select")) {
				changeSelectInput(ele);
			} else if($(ele).hasClass("edit")) {
				changeTextInput(ele);
				
			} else if($(ele).hasClass("textarea")) {
				changeTextTextarea(ele);
			}
		}

	}, false)
	//变普通input输入框
	function changeTextInput(ele) {
		$(ele).css({
			"padding": "0px"
		});
		var width = $(ele).css("width");
		var value=ele.innerText;
		//console.log(value);
		var ipt = $('<input type="text" id="edit">');
		ipt.css({
			"width": width,
			"height": "32px",
		});
		$(ele).html(ipt.val(value));
		ipt.focus();
		ipt.select();
		$(ipt).click();
		document.onkeydown = function(event) {
			var e = event || window.event || arguments.callee.caller.arguments[0];
			if(e.keyCode === 9) {
				var width = $("#edit").width();
				$("#edit").parent("td").css({
					"padding": "5px 0px 5px 5px",
					"overflow": "hidden",
					"text-overflow": "ellipsis",
					"max-width": width + "px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#edit").parents("tr").addClass("editagain");
				$("#edit").parent("td").html($("#edit").val());
				var next = $(ele).next("td");
				while(next.hasClass("undefined")) {
					next = next.next("td")
				}
				if(next.length) {
					next.click();
				} else {
					return false;
				}

			}

		};
	}
	//变普通Textarea输入框
	function changeTextTextarea(ele) {
		if($(ele).hasClass("edited")) {
			var width = $(ele).width() + "px";
		} else {
			var width = $(ele).width() + 10 + "px";
		}
		ele.style.padding = "0";
		$(ele).css({
			"min-width": width
		});
		var ipt = $('<textarea www=' + width + ' style="position: absolute;height:80px;width:200px" id="textarea">' + ele.innerText + '</textarea>');
		$(ele).html(ipt);
		$(ele).find("textarea").click();
		document.onkeydown = function(event) {
			var e = event || window.event || arguments.callee.caller.arguments[0];
			if(e.keyCode === 9) {
				var td = $("#textarea").parents("td");
				var width = $("#textarea").attr("www");
				td.css({
					"padding": "5px 0px",
					"max-width": width,
					"overflow": "hidden",
					"text-overflow": "ellipsis",
					"box-shadow": "0px 0px 1px #17C697",
				});
				td.addClass("edited");
				td.parent("tr").addClass("editagain");
				//td[0].title=$("#edit").val();
				td.html($("#textarea").val());

				var next = $(ele).next("td");
				while(next.hasClass("undefined")) {
					next = next.next("td")
				}
				if(next.length) {
					next.click();
				} else {
					return false;
				}
			}

		};
	}
	//变下拉框
	function changeSelectInput(ele) {
		$(ele).css("padding", "0px");
		var width = $(ele).width();
		var selectOpt = $(ele).attr("selectopt");
		var selectOptArr = selectOpt.split("|");
		var ipt = $('<select id="select"></select>');
		var value=ele.innerText;
		selectOptArr.forEach(function(val, i) {
			if(value==val){
				ipt.append("<option selected>" + val + "</option>");
			}else{
				ipt.append("<option>" + val + "</option>");
			}
		});
		ipt.css({
			"width": width + "px",
			"height": "32px",
		});
		$(ele).html(ipt);
		//$(ele).find("select").click();
		document.onkeydown = function(event) {
			var e = event || window.event || arguments.callee.caller.arguments[0];
			if(e.keyCode === 9) {
				$("#select").parent("td").css({
					"padding": "5px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#select").parents("tr").addClass("editagain");
				$("#select").parent("td").html($("#select option:selected").val());

				var next = $(ele).next("td");
				while(next.hasClass("undefined")) {
					next = next.next("td")
				}
				if(next.length) {
					next.click();
				} else {
					return false;
				}
			}

		};
	}
	//变日期选择框
	function changeDateInput(ele) {
		$(ele).css("padding", "0px");
		var ipt = $('<input type="text" id="date" autofocus value=' + $(ele).text() + '>');
		var width = $(ele).width();
		ipt.css({
			"width": width + "px",
			"height": "32px",
		});
		$(ele).html(ipt);
//		ipt.datetimepicker({
		ipt.datepicker({
			language: "zh-TW",
			autoclose: true, //选中之后自动隐藏日期选择框
//			format: "yyyy-mm-dd hh:ii" //日期格式，详见 
			format: "yyyy-mm-dd" //日期格式，详见 
		});
		$(ele).find("input").click();
		document.onkeydown = function(event) {
			var e = event || window.event || arguments.callee.caller.arguments[0];
			if(e.keyCode === 9) {
				var width = $("#edit").width();
				if(!$(".datepicker").length) {
					$("#date").parent("td").css({
						"padding": "5px",
						"box-shadow": "0px 0px 1px #17C697"
					});
					$("#date").parents("tr").addClass("editagain");
					$("#date").parent("td").html($("#date").val());
				}
				var next = $(ele).next("td");
				while(next.hasClass("undefined")) {
					next = next.next("td")
				}
				if(next.length) {
					next.click();
				} else {
					return false;
				}
			}

		};
	}
	document.onmouseup = function(event) {
		var evt = window.event || event;
		if(document.getElementById("textarea") && evt.target.id != "textarea") {
			var td = $("#textarea").parents("td");
			var width = $("#textarea").attr("www");
			td.css({
				"padding": "5px 0px",
				"max-width": width,
				"overflow": "hidden",
				"text-overflow": "ellipsis",
				"box-shadow": "0px 0px 1px #17C697",
			});
			td.addClass("edited");
			td.parent("tr").addClass("editagain");
			//td[0].title=$("#edit").val();
			td.html($("#textarea").val());
		}
		if(document.getElementById("edit") && evt.target.id != "edit") {
			var width = $("#edit").width();
			$("#edit").parent("td").css({
				"padding": "5px 0px 5px 5px",
				"overflow": "hidden",
				"text-overflow": "ellipsis",
				"max-width": width + "px",
				"box-shadow": "0px 0px 1px #17C697"
			});
			$("#edit").parents("tr").addClass("editagain");
			$("#edit").parent("td").html($("#edit").val());
		}
		if(document.getElementById("select") && evt.target.id != "select") {
			$("#select").parent("td").css({
				"padding": "5px",
				"box-shadow": "0px 0px 1px #17C697"
			});
			$("#select").parents("tr").addClass("editagain");
			$("#select").parent("td").html($("#select option:selected").val());
		}
		if(document.getElementById("date") && evt.target.id != "date") {
			if($(".datepicker").is(':hidden')) {
				$("#date").parent("td").css({
					"padding": "5px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#date").parents("tr").addClass("editagain");
				$("#date").parent("td").html($("#date").val());
				$(".datepicker").remove();
			}
		}
	}
}*/

// 保存
function savestorageInItemTab() {
	var scTime="";
	$("#storageInItemdiv tbody tr").each(function(i,v){
		scTime = $(v).find("[savename='productDate']").text();
		
	})
	if(scTime==""){
		top.layer.msg("生产日期不能为空!");
		return false;
	}
	var sxTime
	$("#storageInItemdiv tbody tr").each(function(i,v){
		sxTime = $(v).find("[savename='expireDate']").text();
		
	})
	if(sxTime==""){
		top.layer.msg("失效日期不能为空!");
		return false;
	}
	
	
	
	
    var ele=$("#storageInItemdiv");
	var changeLog = "库存主数据入库:";
	var data = saveItemjson(ele);
	changeLog = getChangeLog(data, ele, changeLog);
	var changeLogs="";
	if(changeLog !="库存主数据入库:"){
		changeLogs=changeLog;
	}
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		type: 'post',
		url: '/storage/storageIn/saveStorageInItemTable.action',
		data: {
			id: $("#storageIn_id").text(),
			dataJson: data,
			logInfo: changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	var flag = true;
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 添加样本类型ID
//			if(k == "dicSampleTypeName") {
//				json["dicSampleTypeId"] = $(tds[j]).attr("dicSampleTypeId");
//				continue;
//			}
			//单位组
			if(k == "unitGroupNew-name") {
				json["unitGroupNew-id"] = $(tds[j]).attr("unitGroupNew-id");
				continue;
			}
			//生产日期
			if(k == "productDate") {
				if(!$(tds[j]).text()){
					top.layer.msg("生产日期为空！");
					flag = false;
					return false;
				}
			}
			//有效期
			if(k == "expireDate") {
				if(!$(tds[j]).text()){
					top.layer.msg("失效日期为空！");
					flag = false;
					return false;
				}
			}
			
			json[k] = $(tds[j]).text();
		}
		json.color = $(val).attr("background");
		data.push(json);
	});
	return JSON.stringify(data);
}

//获得保存时的json数据
function saveItemjsonpd(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var flag = true;
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			//生产日期
			if(k == "productDate") {
				if(!$(tds[j]).text()){
					top.layer.msg("生产日期为空！");
					flag = false;
					return false;
				}
			}
			//有效期
			if(k == "expireDate") {
				if(!$(tds[j]).text()){
					top.layer.msg("失效日期为空！");
					flag = false;
					return false;
				}
			}
			
		}
	});
	return flag;
}

/*
 * 
 * 创建者 : 郭恒开
 * 创建日期: 2018/03/08
 * 文件描述: 修改日志
 * 
 */
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '数量为"' + v.num + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k] && k != "color") {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
