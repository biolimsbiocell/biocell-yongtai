var gridGrid;
var stateName = $("#storageIn_stateName").val();
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : 'string'
	});
	fields.push({
		name : 'storage-name',
		type : 'string'
	});
	fields.push({
		name : 'storage-id',
		type : 'string'
	});
	fields.push({
		name : 'storage-searchCode',
		type : 'string'
	});
	fields.push({
		name : 'storage-kit-id',
		type : 'string'
	});
	fields.push({
		name : 'storage-kit-name',
		type : 'string'
	});
	fields.push({
		name : 'storage-spec',
		type : 'string'
	});
	fields.push({
		name : 'serial',
		type : 'string'
	});
	fields.push({
		name : 'qcPass',
		type : 'string'
	});
	fields.push({
		name : 'storage-productAddress',
		type : 'string'
	});
	fields.push({
		name : 'productDate',
		type : 'date',
		dateFormat : 'Y-m-d'
	});
	fields.push({
		name : 'expireDate',
		type : 'date',
		dateFormat : 'Y-m-d'
	});
	fields.push({
		name : 'num',
		type : 'float'
	});
	fields.push({
		name : 'storage-unit-name',
		type : 'string'
	});
	fields.push({
		name : 'storage-reactionNum',
		type : 'float'
	});
	fields.push({
		name : 'price',
		type : 'float'
	});
	fields.push({
		name : 'position-id',
		type : 'string'
	});
	fields.push({
		name : 'position-name',
		type : 'string'
	});
	fields.push({
		name : 'inScale',
		type : 'float'
	});
	fields.push({
		name : 'inNum',
		type : 'float'
	});
	fields.push({
		name : 'cancelNum',
		type : 'float'
	});
	fields.push({
		name : 'inCondition',
		type : 'string'
	});
	fields.push({
		name : 'storage-jdeCode',
		type : 'string'
	});
	fields.push({
		name : 'note',
		type : 'string'
	});
	fields.push({
		name : 'note2',
		type : 'string'
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : 'ID',
		width : 150,
		sortable : true,
		hidden : true
	});
	cm.push({
		dataIndex : 'storage-id',
		header : biolims.purchase.applicationObject,
		width : 120,
		sortable : false,
		hidden : true
	});
	cm.push({
		dataIndex : 'storage-name',
		header : biolims.common.componentName,
		width : 120,
		sortable : false,
		hidden : false
	});
	cm.push({
		dataIndex : 'storage-kit-id',
		header : biolims.common.kitID,
		width : 120,
		sortable : false,
		hidden : true
	});
	cm.push({
		dataIndex : 'storage-kit-name',
		header : biolims.common.kitName,
		width : 120,
		sortable : false,
		hidden : false
	});
	cm.push({
		dataIndex : 'storage-searchCode',
		header : biolims.storage.searchCode,
		width : 100,
		sortable : false,
		hidden : true
	});
	cm.push({
		dataIndex : 'storage-spec',
		header : biolims.equipment.spec,
		width : 100,
		sortable : false,
		hidden : false
	});
	cm.push({
		dataIndex : 'storage-jdeCode',
		header : "JDE Code",
		width : 100,
		sortable : false,
		hidden : false
	});
	cm.push({
		dataIndex : 'serial',
		header : biolims.storage.batchNo,
		width : 100,
		sortable : false,
		hidden : false,
		editable : true,
		tooltip : biolims.common.editable,
		editor : code
	});
	cm.push({
		dataIndex : 'storage-productAddress',
		header : biolims.storage.producingArea,
		width : 100,
		sortable : false,
		hidden : true
	});
	cm.push({
		dataIndex : 'productDate',
		header : biolims.storage.productDate,
		width : 100,
		hidden : true,
		sortable : false,
		editable : true,
		renderer : formatDate,
		tooltip : biolims.common.editable,
		editor : productDate
	});
	cm.push({
		dataIndex : 'expireDate',
		header : biolims.common.expirationDate,
		width : 100,
		sortable : false,
		editable : true,
		renderer : formatDate,
		tooltip : biolims.common.editable,
		editor : expireDate
	});
	cm.push({
		dataIndex : 'num',
		header : biolims.storage.inputNum,
		width : 100,
		sortable : false,
		hidden : false,
		editable : true,
		renderer : formatCss,
		tooltip : biolims.common.editable,
		editor : inNum
	});
	cm.push({
		dataIndex : 'storage-unit-name',
		header : biolims.common.unit,
		width : 60,
		sortable : false,
		hidden : false
	});
	cm.push({
		dataIndex : 'storage-reactionNum',
		header : biolims.common.reactionNumber,
		width : 60,
		sortable : false,
		hidden : false
	});
	cm.push({
		dataIndex : 'price',
		header : biolims.storage.inputPrice,
		width : 100,
		sortable : false,
		hidden : true,
		editable : true,
		tooltip : biolims.common.editable,
		editor : inPrice
	});
	cm.push({
		dataIndex : 'position-id',
		header : biolims.common.storageId,
		width : 120,
		sortable : false,
		hidden : false,
		editable : true,
		tooltip : biolims.common.editable,
		editor : position
	});
	cm.push({
		dataIndex : 'position-name',
		header : biolims.common.storageName,
		width : 120,
		sortable : false,
		hidden : false
//		editable : true,
//		tooltip : biolims.common.editable,
//		editor : position
	});
	cm.push({
		dataIndex : 'note2',
		header : biolims.common.note,
		width : 20*6,
		hidden : false,
		editable : true,
		tooltip : biolims.common.editable,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'note',
		header : "sn",
		width : 800,
		hidden : false
//		editable : true
//		tooltip : biolims.common.editable
	});
	cm.push({
		dataIndex : 'inScale',
		header : biolims.storage.inScale,
		width : 100,
		sortable : false,
		hidden : true
	});
	cm.push({
		dataIndex : 'inNum',
		header : biolims.storage.inNum,
		width : 100,
		sortable : false,
		hidden : true
	});
	cm.push({
		dataIndex : 'cancelNum',
		header : biolims.storage.cancelNum,
		width : 100,
		sortable : false,
		hidden : true
	});
	cm.push({
		dataIndex : 'inCondition',
		header : biolims.storage.inCondition,
		width : 100,
		sortable : false,
		hidden : true
	});

	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx
			+ '/storage/storageIn/showStorageInItemListJson.action?storageInId='
			+ $("#storageIn_id").val();
	var opts = {};
	opts.tbar = [];
	opts.title = biolims.sample.warehousingDetail;
	opts.height = document.body.clientHeight * 0.95;
	opts.rowselect = function(id) {
		$("#selectId").val(id);
	};
	opts.rowdblclick = function(id) {
		$('#selectId').val(id);
	};
	if ($("#storageIn_stateName").val() != "完成") {
		opts.delSelect = function(ids) {
			ajax("post", "/storage/storageIn/delStorageInItems.action", {
				ids : ids
			}, function(data) {
				if (data.success) {
					gridGrid.getStore().commitChanges();
					gridGrid.getStore().reload();
					message(biolims.common.deleteSuccess);
				} else {
					message(biolims.common.deleteFailed);
				}
			}, null);
		};
		opts.tbar.push({
			text : biolims.common.fillDetail,
			handler : null
		});
		opts.tbar.push({
			text : biolims.common.uncheck,
			handler : null
		});
		opts.tbar.push({
			text : biolims.common.editableColAppear,
			handler : null
		});
		opts.tbar.push({
			text : biolims.common.selectionComponent,
			handler : function() {
				window.showMainStorageAllSelectList();
			}
		});
		opts.tbar.push({
			text : biolims.common.componentInformation,
			handler : function() {
				if (gridGrid.getModifyRecord().length > 0) {
					 message(biolims.common.pleaseSaveRecord);
					 return;
				 }
				var selectRecord = gridGrid.getSelectionModel()
						.getSelections();
				var id = "";
				var sid = "";
				var serial = "";
				if (selectRecord.length == 1) {
					id = selectRecord[0].get("id");
					sid = selectRecord[0].get("storage-id");
					serial=selectRecord[0].get("serial");
					if (selectRecord[0].get("serial") == ""
							|| selectRecord[0].get("serial") == null) {
						message(biolims.common.pleaseFillInTheBatchNumber);
						return;
					}
					if (selectRecord[0].get("num") == "0"
							|| selectRecord[0].get("num") == null) {
						message(biolims.common.pleaseFillInTheQuantity);
						return;
					}
					window.open(
							'/storage/storageIn/item/storageInInfo/newStorageInInfo.action?id='
									+ id + '&sid=' + sid+'&serial='+serial, '', '');
				} else {
					message(biolims.common.pleaseSelectAPieceOfData);
				}
			}
		});
		opts.tbar.push({
			text : biolims.common.printLabel,
			handler : function() {
				var selRecords = gridGrid.getSelectionModel().getSelections();
				if (selRecords.length > 0) {
				var options = {};
				options.width = document.body.clientWidth - 400;
				options.height = document.body.clientHeight - 40;
				loadDialogPage(null,biolims.common.selectThePrinter,"/system/syscode/codeMain/codeMainSelect.action",
				{"Confirm" : function() {
					var operGrid = $("#show_dialog_codeMain_div").data("codeMainDialogGrid");
					var selectRecord = operGrid.getSelectionModel().getSelections();
						if (selectRecord.length > 0) {$.each(selectRecord,
								function(i, obj) {
								id = obj.get("id");
								var ids = [];
						for (var i = 0; i < selRecords.length; i++) {
							ids.push(selRecords[i].get("id"));
									}
						ajax("post","/storage/storageIn/makeCode.action",{
							ids : ids,
							id : id},
						function() {},null);});} else {
							message(biolims.common.selectYouWant);
							return;
							}
						$(this).dialog("close");}
					}, true, options);
				} else {
						message(biolims.common.selectYouWant);
						}
					}
				});
		opts.tbar.push({
			text : biolims.common.uploadCSV,
			handler : function() {
				var options = {};
				options.width = 350;
				options.height = 200;
				loadDialogPage($("#bat_svnuploadcsv_div"), biolims.common.batchUpload, null, {
					"Confirm" : function() {
						goInExcelcsvSvn();
						$(this).dialog("close");
					}
				}, true, options);
			}
		});
//		opts.delSelect = function(ids) {
//			gridGrid.stopEditing();
//			var record = gridGrid.getSelectionModel().getSelections();
//			if (!record || record == '' || record.length == 0) {
//				Ext.MessageBox.alert(biolims.common.prompt,
//						biolims.common.pleaseSelectRecord, '');
//			} else {
//				Ext.MessageBox.show({
//					title : biolims.common.prompt,
//					msg : biolims.common.confirm2Del + record.length
//							+ biolims.common.record,
//					buttons : Ext.MessageBox.OKCANCEL,
//					closable : false,
//					fn : function(btn) {
//						if (btn == 'ok') {
//							for (var ij = 0; ij < record.length; ij++) {
//								var id = record[ij].get('id');
//								if (id == undefined) {
//									record[ij].set('id', '-1000');
//									store.remove(record[ij]);
//								} else {
//									store.remove(record[ij]);
//									var params = "id:'" + id + "'";
//									delData(params);
//								}
//							}
//						}
//					}
//				});
//			}
//		};
	}
	gridGrid = gridEditTable("grid", cols, loadParam, opts);
	$("#grid").data("gridGrid", gridGrid);
});
function goInExcelcsvSvn() {
	var file = document.getElementById("file-svnuploadcsv").files[0];
	var n = 0;
	var ob = gridGrid.getStore().recordType;
	var reader = new FileReader();
	reader.readAsText(file, 'GB2312');
	reader.onload = function(f) {
		var csv_data = $.simple_csv(this.result);
		$(csv_data).each(function() {
			if (n > 0) {
				if (this[0]) {
					var p = new ob({});
					p.isNew = true;
					p.set("storage-name", this[0]);// 组分名称
					p.set("serial", this[1]);// 批号
					p.set("expireDate", getDate(this[2]));// 过期日期
					p.set("num", this[3]);// 入库数量
					p.set("qcPass", this[4]);// QC状态
					p.set("position-id", this[5]);// 储位编码
					p.set("note2", this[6]);// 备注
					gridGrid.getStore().insert(0, p);
				}
			}
			n = n + 1;
		});
	};
}
function getDate(strDate) {
	var date = eval('new Date('
			+ strDate.replace(/\d+(?=-[^-]+$)/, function(a) {
				return parseInt(a, 10) - 1;
			}).match(/\d+/g) + ')');
	return date;
}
function list() {
	window.location = window.ctx
			+ '/storage/storageIn/showStorageInList.action?queryMethod=' + "1";
}

function add() {
	window.location = window.ctx + "/storage/storageIn/toEditStorageIn.action";
}
function edit() {
	var id = "";
	id = document.getElementById("id").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/storage/storageIn/toEditStorageIn.action?id=' + id;
}
function newSave() {
	save();
}
function save() {
	var selRecord = gridGrid.store.getModifiedRecords();
	if (selRecord && selRecord.length > 0) {
		var itemIds = [];
		$.each(selRecord, function(i, obj) {
			itemIds.push(obj.get("storage-id"));
		});
		var ids = "'" + itemIds.join("','") + "'";
		ajax("post", "/storage/common/showStroageId.action", {
			data : ids
		}, function(data) {
			if (data.message) {
				message(biolims.storage.objectNumber + data.message
						+ biolims.storage.isInStock);
				return false;
			} else {
				$("#toolbarbutton_save").hide();
				submitForm();
			}
		}, null);
	} else {
		$("#toolbarbutton_save").hide();
		submitForm();
	}
}
function submitForm() {

	if (checkSubmit() == true) {
		Ext.MessageBox.show({
			msg : biolims.common.savingData,
			progressText : biolims.common.saving,
			width : 300,
			wait : true,
			icon : 'ext-mb-download'
		});
		document.getElementById('jsonDataStr').value = commonGetModifyRecords(gridGrid);
		orderForm.action = window.ctx + "/storage/storageIn/save.action";
		orderForm.submit();
	} else {
		return false;
	}
}
function checkSubmit() {
	var mess = "";
	// var fs = [ "storageIn_id", "storageIn_purchaseApply_id" ];
	// var nsc = [ "编号不能为空！", "采购申请单号不能为空！" ];
	var fs = [ "storageIn_id" ];
	var nsc = [ biolims.common.IdEmpty ];

	mess = commonFieldsNotNullVerifyById(fs, nsc);
	if (mess != "") {
		message(mess);
		return false;
	}

	return true;
}

function addStorageGrid(record) {
	var fields = [ 'storage-id', 'storage-name', 'storage-spec',
			'storage-jdeCode', 'storage-unit-name', 'price',
			'storage-supplierName', 'spec', 'positoin-id', 'positoin-name',
			'storage-kit-id', 'storage-kit-name' ];
	var fieldsfrom = [ 'id', 'name', 'spec', 'jdeCode', 'unit-name', 'price',
			'storage-supplierName', 'storage-spec', 'positoin-id',
			'positoin-name', 'kit-id', 'kit-name' ];
	var fields1 = [];
	var fields1from = [];
	if (!record || record == '') {
		Ext.MessageBox.alert(biolims.common.prompt,
				biolims.common.pleaseSelectRecord2Save, '');
	} else {
		var selData = '';
		for (var ij = 0; ij < record.length; ij++) {
			var array = record[ij].data;

			var Ob = gridGrid.getStore().recordType;

			var p = new Ob({

			});

			for (var i = 0; i < fields.length; i++) {

				p.set(fields[i], record[ij].get(fieldsfrom[i]));

			}

			gridGrid.stopEditing();
			gridGrid.store.insert(0, p);

			gridGrid.startEditing(0, 0);
			gridGrid.getView().getRow(0).style.backgroundColor = RECORD_INSERT_COLOR;

		}

	}

}
var item3 = menu.add({
	text : biolims.common.modify
});
item3.on('click', changeMessage);
var storeisQcCob = new Ext.data.ArrayStore({
	fields : [ 'name' ],
	data : [ [ '未 QC' ], [ 'QC Pass' ], [ 'QC' ] ]
});
var qcCob = new Ext.form.ComboBox({
	store : storeisQcCob,
	displayField : 'name',
	valueField : 'name',
	mode : 'local'
});
var inPrice = new Ext.form.NumberField({
	allowBlank : true,
	allowNegative : false,

	decimalPrecision : 6
});
var productDate = new Ext.form.DateField({
	format : 'Y-m-d'
});
var expireDate = new Ext.form.DateField({
	format : 'Y-m-d'
});
var inNum = new Ext.form.NumberField({
	// allowBlank : true,
	// allowNegative : false
	allowDecimals : true,
	decimalPrecision : 0
});
var serial = new Ext.form.TextField({
	allowBlank : true
});
var code = new Ext.form.TextField({
	allowBlank : true
});
var position = new Ext.form.TextField({
	allowBlank : true
});
position.on('focus', function() {
	showStoragePosition();
});
function showStoragePosition() {
	var win = Ext.getCmp('showStoragePosition');
	if (win) {
		win.close();
	}
	var showStoragePosition = new Ext.Window(
			{
				id : 'showStoragePosition',
				modal : true,
				title : biolims.sample.selectLocation,
				layout : 'fit',
				width : document.body.clientWidth / 2,
				height : document.body.clientHeight / 1.5,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
									+ window.ctx
									+ "/storage/position/showStoragePositionTreeDialog2.action?setStoragePostion=true&flag=showStoragePosition' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						showStoragePosition.close();
					}
				} ]
			});
	showStoragePosition.show();
}
function setshowStoragePosition(id, name, node) {
	var record = gridGrid.getSelectionModel().getSelected();
	record.set('position-id', id);
	record.set('position-name', name);

	var win = Ext.getCmp('showStoragePosition');
	if (win) {
		win.close();
	}

}
function validateRecordFun(num) {

	var record = gridGrid.getSelectionModel().getSelected();
	if (record.get('purchaseApplyItem-id')) {

		var orderNum = parseFloat(record.get('purchaseApplyItem-num'));

		if (num > orderNum) {
			alertMessage(biolims.storage.InventoryQuantityGreaterApplication);
			return false;
		}
	}

	return true;
}
function formatCss(value, cellmeta, record, rowIndex, columnIndex, store) {
	var cancelNum = 0.0;
	var inNum = 0.0;

	if (record.get('cancelNum') != null) {

		cancelNum = record.get('cancelNum');

	}
	if (record.get('inNum') != null) {

		inNum = record.get('inNum');

	}
	var allNum = orderNum - (inNum + cancelNum);

	if (value > allNum) {
		cellmeta.css = "redFont";
	}

	var orderNum = parseFloat(record.get('purchaseOrderNum'));

	if (value > orderNum) {
		cellmeta.css = "redFont";
	}
	return value;
}

function validateFun(record) {

	var num = record.get('num');

	if (num == null || num == 0) {
		alertMessage(biolims.storage.numberIsEmpty);
		return false;
	}
	return true;
}
function validateAllFun() {

	var store1 = gridGrid.store;
	var gridCount = store1.getCount();

	var allNum = 0.0;
	for (var ij = 0; ij < gridCount; ij++) {
		var record = store1.getAt(ij);
		var num = record.get('num');

		if (num == null || num == 0) {
			alertMessage(biolims.storage.numberIsEmpty);
			return false;
		}
	}

	return true;
}

function hasValidateValue() {
	/*
	 * var gridCount = gridGrid.store.getCount(); for ( var ij = 0; ij <
	 * gridCount; ij++) { var record = gridGrid.store.getAt(ij);
	 * 
	 * var orderNum = parseFloat(record.get('purchaseApplyItem-num')); var num =
	 * parseFloat(record.get('num'));
	 * 
	 * 
	 * if (num > orderNum) { alertMessage("入库数量不能大于申请数量。"); return false; } }
	 */
	return true;
}

function viewSupplier() {
	if (trim(document.getElementById('storageIn_supplier_id').value) == '') {
		message(biolims.common.selectRecord);
	} else {
		openDialog(window.ctx + '/supplier/toViewSupplier.action?id='
				+ document.getElementById('storageIn_supplier_id').value);

	}
}
function viewPurchaseApply() {
	if (trim(document.getElementById('storageIn_purchaseApply_id').value) == '') {
		message(biolims.common.selectRecord);
	} else {
		openDialog(window.ctx
				+ '/purchase/apply/toViewPurchaseApply.action?id='
				+ document.getElementById('storageIn_purchaseApply_id').value);

	}
}
function printReport() {
	var url = '__report=storageIn.rptdesign&storageIn='
			+ $("#storageIn_id").val();
	commonPrint(url);

}
var store1;
function setGridId() {

	var gridCount = gridGrid.store.getCount();
	for (var ij = 0; ij < gridCount; ij++) {
		var record = gridGrid.store.getAt(ij);
		record.set("id", "");
	}
	Ext.getCmp('showEditColumn').handler();
}
function insertStorageItem() {

	var gridCount = store1.getCount();

	for (var ij = 0; ij < gridCount; ij++) {
		var record = store1.getAt(ij);

		var Ob = gridGrid.getStore().recordType;
		var p = new Ob({});
		p.set("storage-id", record.get("storage-id"));
		p.set("storage-name", record.get("storage-name"));
		p.set("storage-searchCode", record.get("storage-searchCode"));
		p.set("storage-spec", record.get("storage-spec"));
		p.set("storage-breed", record.get("storage-breed"));
		p.set("storage-unit-name", record.get("storage-unit-name"));
		p.set("storage-supplierName", record.get("storage-supplierName"));
		p.set("storage-productAddress", record.get("storage-productAddress"));
		p.set("purchaseOrderItem-id", record.get("id"));
		p.set("purchaseOrderItem-num", record.get("purchaseOrderNum"));
		p.set("inNum", record.get("inNum"));
		p.set("cancelNum", record.get("cancelNum"));
		var scale = record.get("inNum") / record.get("purchaseOrderNum");

		p.set("inScale", scale);
		if (scale >= 1) {

			p.set("inCondition", biolims.storage.completeInput);

		} else {
			if (scale)
				p.set("inCondition", biolims.storage.incompleteInput);
		}

		gridGrid.stopEditing();
		gridGrid.store.insert(0, p);

		gridGrid.startEditing(0, 0);
		gridGrid.getView().getRow(0).style.backgroundColor = RECORD_INSERT_COLOR;

	}

}
function setsurchApplyFun(record) {

	document.getElementById('storageIn_purchaseApply_id').value = record
			.get('id');
	document.getElementById('storageIn_purchaseApply_note').value = record
			.get('note');
	document.getElementById('storageIn_purchaseApply_createUser_name').value = record
			.get('createUser-name');
	gridGrid.store.removeAll();
	gridGrid.store.commitChanges();
	store1 = new Ext.data.JsonStore(
			{
				root : 'results',
				totalProperty : 'total',
				remoteSort : true,
				fields : [ {
					name : 'id',
					type : 'string'
				}, {
					name : 'costCenter-id',
					type : 'string'
				}, {
					name : 'costCenter-name',
					type : 'string'
				}, {
					name : 'storage-id',
					type : 'string'
				}, {
					name : 'storage-unit-name',
					type : 'string'
				}, {
					name : 'storage-name',
					type : 'string'
				}, {
					name : 'storage-searchCode',
					type : 'string'
				}, {
					name : 'storage-spec',
					type : 'string'
				}, {
					name : 'storage-breed',
					type : 'string'
				}, {
					name : 'storage-supplierName',
					type : 'string'
				}, {
					name : 'storage-productAddress',
					type : 'string'
				}, {
					name : 'productDate',
					type : 'date',
					dateFormat : 'Y-m-d'
				}, {
					name : 'expireDate',
					type : 'date',
					dateFormat : 'Y-m-d'
				}, {
					name : 'num',
					type : 'float'
				}, {
					name : 'price',
					type : 'float'
				}, {
					name : 'purchaseOrderNum',
					type : 'float'
				}, {
					name : 'inNum',
					type : 'float'
				}, {
					name : 'cancelNum',
					type : 'float'
				} ],
				proxy : new Ext.data.HttpProxy(
						{
							url : window.ctx
									+ '/purchase/order/showPurchaseApplyItemListJson.action?purchaseApplyInStr='
									+ document
											.getElementById('storageIn_purchaseApply_id').value,
							method : 'POST'
						})
			});
	var o1 = {
		start : 0,
		limit : 1000
	};
	store1.load({
		params : o1
	});
	setTimeout(insertStorageItem, 1000);

	var win = Ext.getCmp('surchApplyFun');
	if (win) {
		win.close();
	}

}
function changeState() {
	if (validateAllFun() == false) {

		return false;
	}
	commonChangeState("formId=" + $("#storageIn_id").val()
			+ "&tableId=StorageIn");
}
function workflowSubmit() {
	commonWorkflowSubmit("applicationTypeTableId=storageIn&formId="
			+ $("#storageIn_id").val() + "&title="
			+ encodeURI(encodeURI($("#storageIn_id").val())));
}
function workflowLook() {
	commonWorkflowLook("applicationTypeTableId=storageIn&formId="
			+ $("#storageIn_id").val());
}
function workflowView() {
	commonWorkflowView("applicationTypeTableId=storageIn&formId="
			+ $("#storageIn_id").val());
}
Ext.onReady(function() {
	var tabs = new Ext.TabPanel({
		renderTo : 'maintab',
		height : document.body.clientHeight - 30,
		autoWidth : true,
		activeTab : 0,
		margins : '0 0 0 0',
		items : [ {
			title : biolims.storage.inputInfo,
			contentEl : 'markup'

		} ]
	});
});

$(function() {
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "modify") {

		var t = "#storageIn_id";
		settextreadonlyById(t);

	}
	if (handlemethod == "view") {
		settextreadonlyByAll();

	}

	var copyMode = $("#copyMode").val();
	if (copyMode && copyMode == "true") {
		settextread("storageIn_id");
		document.getElementById("storageIn_id").value = "";
		document.getElementById("storageIn_state").value = "3";
		document.getElementById("storageIn_stateName").value = biolims.common.create;
		setTimeout("firstCommonValue(gridGrid)", 1000);

	}
});
function changeMessage(){
	var id = "";
	$("#storageIn_state").val("3");
	$("#storageIn_stateName").val(biolims.common.modify);
	submitForm();
}