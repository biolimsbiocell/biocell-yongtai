var gridGrid;
Ext
		.onReady(function() {
			Ext.QuickTips.init();
			Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
			Ext.grid.RowNumberer = Ext.extend(Ext.grid.RowNumberer, {
				width : 30,
				renderer : function(value, cellmeta, record, rowIndex,
						columnIndex, store) {
					return store.lastOptions.params.start + rowIndex + 1;
				}
			});
var store = new Ext.data.JsonStore({
										root : 'results',
						totalProperty : 'total',
						remoteSort : true,
						fields : [ {
							name : 'id',
							type : 'string'
						}, {
							name : 'purchaseOrder-id',
							type : 'string'
						}, {
							name : 'handelUser-name',
							type : 'string'
						}, {
							name : 'handelDate',
							type : 'date',
							dateFormat : 'Y-m-d'
						}, {
							name : 'stateName',
							type : 'string'
						} ],
proxy: new Ext.data.HttpProxy({url: ctx+'/storage/storageIn/showStorageInListJson.action?queryMethod=',method: 'POST'})
});
gridGrid = new Ext.grid.GridPanel({
										autoWidth : true,
						id : gridGrid,
						titleCollapse : true,
						autoScroll : true,
						height : document.body.clientHeight - 35,
						iconCls : 'icon-grid',
						title : biolims.storage.inputManagement,
						collapsible : true,
						store : store,
						columnLines : true,
						trackMouseOver : true,
						loadMask : true,
						colModel : new Ext.ux.grid.LockingColumnModel([
								new Ext.grid.RowNumberer(), {
									dataIndex : 'id',
									header : biolims.sample.id,
									width : 150,
									sortable : true
								}, {
									dataIndex : 'purchaseOrder-id',
									header : biolims.storage.purchaseOrderId,
									width : 150,
									sortable : true,
									hidden : true
								}, {
									dataIndex : 'handelUser-name',
									header : biolims.storage.handelUser,
									width : 150,
									sortable : true
								}, {
									dataIndex : 'handelDate',
									header : biolims.storage.inDate,
									width : 150,
									sortable : true,
									renderer : formatDate
								}, {
									dataIndex : 'stateName',
									header : biolims.common.state,
									width : 100,
									sortable : true,
									hidden : false
								} ]),
						stripeRows : true,
						view : new Ext.ux.grid.LockingGridView(),
						viewConfig : {
							forceFit : true,
							enableRowBody : false,
showPreview:false
},
bbar: new Ext.PagingToolbar({
	id : 'bbarId',
	pageSize : parseInt((document.body.clientHeight - 35) > 0 ? (document.body.clientHeight - 35) / 24
			: 1),
			store : store,
			displayInfo : true,
			displayMsg : biolims.common.displayMsg,
			beforePageText : biolims.common.page,
			afterPageText : biolims.common.afterPageText,
			emptyMsg : biolims.common.noData,
	plugins : new Ext.ui.plugins.ComboPageSize(
			{
				addToItem : false,
				prefixText : biolims.common.show,
				postfixText : biolims.common.entris
											})
								})
					});
			gridGrid.render('grid');
			gridGrid.getSelectionModel().on('rowselect',
					function(sm, rowIdx, r) {
						document.getElementById("id").value = r.data.id;
					});
			gridGrid.on('rowdblclick', function() {
				var record = gridGrid.getSelectionModel().getSelections();
				document.getElementById("id").value = record[0].data.id;
				edit();
			});
			store.on('beforeload', function() {
				store.baseParams = {
					data : document.getElementById('extJsonDataString').value
				};
			});
			store
					.load({
						params : {
							start : 0,
							limit : parseInt((document.body.clientHeight - 35) > 0 ? (document.body.clientHeight - 35) / 24
									: 1)
						}
					});
		});
function exportexcel() {
	gridGrid.title = biolims.common.exportList
	var vExportContent = gridGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}




function list() {
	window.location = window.ctx + '/storage/storageIn/showStorageInList.action';
}

function add() {
	window.location = window.ctx + "/storage/storageIn/toEditStorageIn.action";
}

function edit() {
	var id = "";
	id = document.getElementById("id").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/storage/storageIn/toEditStorageIn.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("id").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/storage/storageIn/toViewStorageIn.action?id=' + id;
}
Ext.onReady(function() {
	Ext.QuickTips.init();
	new Ext.form.DateField({

		applyTo : 'handelDate'
	});
	$("#handelDate").css("width", "80px");
});
Ext.onReady(function() {
	Ext.QuickTips.init();
	new Ext.form.DateField({

		applyTo : 'queryStartDate'
	});
	$("#queryStartDate").css("width", "80px");
});
Ext.onReady(function() {
	Ext.QuickTips.init();
	new Ext.form.DateField({

		applyTo : 'queryEndDate'
	});
	$("#queryEndDate").css("width", "80px");
});
$(function() {

	$("#opensearch").click(function() {
		var option = {};
		option.width = 450;
		option.height = 217;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(gridGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);

	});

});