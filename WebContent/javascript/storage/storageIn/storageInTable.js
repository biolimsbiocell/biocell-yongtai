var storageInTab;
$(function() {
	var options = table(true, "",
			"/storage/storageIn/showStorageInTableJson.action", [ {
				"data" : "id",
				"title" : biolims.storage.inputInfo,
			}, {
				"data" : "handelUser-id",
				"title" : biolims.storage.handelUser+"ID",
			}, {
				"data" : "handelUser-name",
				"title" : biolims.storage.handelUser,
			}, {
				"data" : "createDate",
				"title" : biolims.tStorage.createDate,
			}, {
				"data" : "note",
				"title" : biolims.tInstrumentRepair.name,
			} , {
				"data" : "handelDate",
				"title" : biolims.storage.inDate,
			},{
				"data" : "financeCode-name",
				"title" : biolims.common.confirmDate,
				"visible" : false
			},{
				"data" : "stateName",
				"title" : biolims.common.stateName,
			} ], null)
	storageInTab = renderRememberData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(storageInTab);
	})
});

function add() {
	window.location = window.ctx
			+ '/storage/storageIn/toEditStorageIn.action?sampleStyle='
			+ $("#sampleStyle").val();
}
function edit() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/storage/storageIn/toEditStorageIn.action?id=' + id
			+ "&sampleStyle=" + $("#sampleStyle").val();
}
function view() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ "/storage/storageIn/toViewStorageIn.action?id="+ id;
}
// 弹框模糊查询参数
function searchOptions() {
	return [ {
		"txt" : biolims.storage.inputInfo,
		"type" : "input",
		"searchName" : "id",
	}, {
		"txt" : biolims.storage.handelUser+"ID",
		"type" : "input",
		"searchName" : "handelUser-id",
	}, {
		"txt" : biolims.storage.handelUser,
		"type" : "input",
		"searchName" : "handelUser-name",
	}, {
		"txt" : biolims.sample.createDateStart,
		"type" : "dataTime",
		"searchName" : "createDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : biolims.sample.createDateEnd,
		"type" : "dataTime",
		"searchName" : "createDate##@@##2",
		"mark" : "e##@@##",
	}, {
		"type" : "table",
		"table" : storageInTab
	} ];
}

function printTempalte(){
	$("#sampleReceiveModal").modal("show");
	// 选择临床样本或者科研样本
	$("#modal-body .col-xs-12").hide();
	$("#modal-body #scancode").show();
	scanCode();
}
// 扫码
function scanCode() {
	$("#scanInput").keypress(function(e) {
		var e = e || window.event;
		if (e.keyCode == "13") {
			var sampleCode = this.value;
			if (sampleCode) {
				$.ajax({
					type : "post",
					async : false,
					data : {
						sampleCode : sampleCode,
					},
					url : ctx + "/storage/storageIn/findU8StorageIn.action",
					success : function(data2) {
						var data3 = JSON.parse(data2);
						if(data3.success){
							if(data3.cunzai){
								window.location = window.ctx
								+ '/storage/storageIn/toEditStorageIn.action?id=' + sampleCode;
							}else{
								$.ajax({
									type : "post",
									async : false,
									data : {
										sampleCode : sampleCode,
									},
									url : ctx + "/storage/storageIn/saveU8StorageIn.action",
									success : function(data4) {
										var data5 = JSON.parse(data4);
										if(data5.success){
											if(data5.cunzai){
												window.location = window.ctx
												+ '/storage/storageIn/toEditStorageIn.action?id=' + sampleCode;
											}else{
												top.layer.msg("查询U8的出库单失败！");
											};
										}else{
											top.layer.msg("程序查询数据失败，请联系管理员！");
										}
									}
								});
							};
						}else{
							top.layer.msg("程序查询数据失败，请联系管理员！");
						}
					}
				});
			};
		};
	});
}
