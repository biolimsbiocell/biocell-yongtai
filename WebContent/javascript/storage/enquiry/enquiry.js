var enquiryListTable;
$(function(){
	
	var colData = [{
			"data": "id",
			"title": "询价编号"
		}, {
			"data": "name",
			"title": "描述"
		}, {
			"data": "createUser-id",
			"title": "创建人ID",
			"visible":false
		}, {
			"data": "createUser-name",
			"title": "创建人"
		}, {
			"data": "createDate",
			"title": biolims.purchase.createDate
		}, {
			"data": "confirmUser-id",
			"title": "审核人ID",
			"visible":false
		}, {
			"data": "confirmUser-name",
			"title": "审核人"
		}, {
			"data": "confirmDate",
			"title": "审批时间"
		}, {
			"data": "state",
			"title": "状态",
			"visible":false
		}, {
			"data": "stateName",
			"title": "状态"
		}, {
			"data": "note",
			"title": "备注"
		}];
	
	$.ajax({
		type:"post",
		url:window.ctx + "/system/customfields/findFieldByModuleValue.action",
		async:false,
		data:{
			moduleValue : "Enquiry"
		},
		success:function(data){
			var objData = JSON.parse(data);
			if(objData.success){
				$.each(objData.data, function(i,n) {
					var str = {
						"data" : n.fieldName,
						"title" : n.label
					}
					colData.push(str);
				});
				
			}else{
				top.layer.msg(biolims.tInstrumentBorrowDetail.tableError);
			}
		}
	});
	
	var options = table(true, "","/storage/enquiry/showEnquiryListJson.action",colData , null)
		enquiryListTable = renderRememberData($("#main"), options);
		//恢复之前查询的状态
		$('#main').on('init.dt', function() {
			recoverSearchContent(enquiryListTable);
		})
})

// 新建
function add() {
	window.location = window.ctx + "/storage/enquiry/editEnquiry.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/storage/enquiry/editEnquiry.action?id=' + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/storage/enquiry/toViewEnquiry.action?id=' + id;
}
//弹框模糊查询参数
function searchOptions() {
	return [{
			"txt": "询价编号",
			"type": "input",
			"searchName": "id"
		},
		{
			"txt": biolims.sample.createUserName,
			"type": "input",
			"searchName": "createUser"
		},
		{
			"txt": biolims.sample.createDateStart,
			"type": "dataTime",
			"searchName": "createDate##@@##",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.common.state,
			"type": "input",
			"searchName": "stateName",
		},
		{
			"type": "table",
			"table": enquiryListTable
		}
	];
}