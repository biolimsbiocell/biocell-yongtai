$(function(){
	
	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "view"||$("#enquiry_state").val()=="1") {
		settextreadonly();
	}
	
})

function fileUp() {
	if($("#enquiry_id").val() == "NEW") {
		top.layer.msg("请先保存数据再上传附件！")
		return false;
	}
	$("#uploadFile").modal("show");
}

function fileView() {
	top.layer.open({
		title: biolims.common.attachment,
		type: 2,
		skin: 'layui-layer-lan',
		area: ["650px", "400px"],
		content: window.ctx + "/operfile/initFileList.action?flag=1&modelType=enquiry&id=" + $("#enquiry_id").val(),
		cancel: function(index, layer) {
			top.layer.close(index)
		}
	})
}

//保存
function save(){
	//loading
	top.layer.load(4, {
		shade: 0.3
	});
	//必填验证
	var requiredField=requiredFilter();
	if(!requiredField){
		return false;
	}
	//页面数据
	var jsonn = {};
	$("#form1 input").each(function(i, v) {
		var k = v.name;
		jsonn[k] = v.value;
	});
	$("#form1 select").each(function(i, v) {
		var k = v.name;
		jsonn[k] = v.value;
	});
	
	//日志
	var changeLog = "";
	$('input[class="form-control"]').each(
			function(i, v) {
				var valnew = $(v).val();
				var val = $(v).attr("changelog");
				if (val !== valnew) {
					changeLog += $(v).prev("span").text() + ':由"' + val
							+ '"变为"' + valnew + '";';
				}
			});
	
	
	var enquiryJson = saveEnquiryItemjson($("#enquiryItemTable"));
	var supplierEnquiryJson = saveSupplierEnquiryItemjson($("#supplierEnquiryItemTable"));
	
	var enquiryChangeLogItem = "询价管理明细"+":";
	enquiryChangeLogItem = getEnquiryItemChangeLog(enquiryJson, $("#enquiryItemTable"), enquiryChangeLogItem);
	
	var supplierEnquiryChangeLogItem = "供应商报价明细"+":";
	supplierEnquiryChangeLogItem = getSupplierEnquiryItemChangeLog(supplierEnquiryJson, $("#supplierEnquiryItemTable"), supplierEnquiryChangeLogItem);
	
	var data = {
		main : JSON.stringify(jsonn),
		changeLog : changeLog,
		enquiryJson : enquiryJson,
		enquiryChangeLogItem : enquiryChangeLogItem,
		supplierEnquiryJson : supplierEnquiryJson,
		supplierEnquiryChangeLogItem : supplierEnquiryChangeLogItem
	};
	
	$.ajax({
		url : ctx + '/storage/enquiry/save.action',
		type : 'post',
		data : data,
		success : function(data){
			var data = JSON.parse(data);
			if(data.success){//成功
				top.layer.closeAll();
				var url = "/storage/enquiry/editEnquiry.action?id="
					+ data.id;
				window.location.href = url;
				top.layer.msg(biolims.common.saveSuccess);
			}else{//失败
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed);
			}
		}
	})
}
//审核人
function selectConfirmUser(){
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/core/user/selectUserTable.action?groupId=admin", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index);
			$("#enquiry_confirmUser").val(id);
			$("#enquiry_confirmUser_name").val(name);
		},
	})
}

//列表
function list(){
	window.location = window.ctx + '/storage/enquiry/showEnquiryList.action';
}

//改变状态
function changeState() {
    var	id=$("#enquiry_id").val()
	var paraStr = "formId=" + id +
		"&tableId=Enquiry";
	console.log(paraStr)
	top.layer.open({
		title:biolims.common.approvalProcess,
		type: 2,
		anim: 2,
		area: ['400px', '400px'],
		btn: biolims.common.selected,
		content: window.ctx +
			"/applicationTypeAction/applicationTypeActionLook.action?" + paraStr +
			"&flag=changeState'",
		yes: function(indexx, layer) {
			top.layer.confirm(biolims.common.approve , {
				icon: 3,
				title: biolims.common.prompt,
				btn:biolims.common.selected
			}, function(index) {
				ajax("post", "/applicationTypeAction/exeFun.action", {
					applicationTypeActionId: $('.layui-layer-iframe', parent.document).find("iframe").contents().find("input:checked").val(),
					formId: id
				}, function(response) {
					var respText = response.message;
					if(respText == '') {
						window.location.reload();
					} else {
						top.layer.msg(respText);
					}
				}, null)
				top.layer.closeAll();
			})
		},
		

	});
}