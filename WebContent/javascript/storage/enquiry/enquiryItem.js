var enquiryItemListTable;
var oldEnquiryListChangeLog;
$(function(){
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : "ID",
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		},
		"visible" : false
	});
	colOpts.push({
		"data" : "storageId",
		"title" : "物品编号",
		"createdCell" : function(td,data,rowdata) {
			$(td).attr("saveName", "storageId");
			$(td).attr("typeId", rowdata["type"]);
		}
	});
	colOpts.push({
		"data" : "storageName",
		"title" : "物品描述",
		"createdCell" : function(td) {
			$(td).attr("saveName", "storageName");
		}
	});
	colOpts.push({
		"data" : "num",
		"title" : "采购数量",
		"createdCell" : function(td) {
			$(td).attr("saveName", "num");
		}
	});
	colOpts.push({
		"data" : "type",
		"title" : "类型",
		"createdCell" : function(td) {
			$(td).attr("saveName", "type");
		},
		"visible" : false
	});
	colOpts.push({
		"data" : "note",
		"title" : "备注",
		"createdCell" : function(td) {
			$(td).attr("saveName", "note");
		},
		"className":"edit"
	});
	colOpts.push({
		"data" : "enquiry-id",
		"title" : "相关主表ID",
		"createdCell" : function(td) {
			$(td).attr("saveName", "enquiry-id");
		},
		"visible" : false
	});
	colOpts.push({
		"data" : "enquiry-name",
		"title" : "相关主表",
		"createdCell" : function(td) {
			$(td).attr("saveName", "enquiry-name");
		},
		"visible" : false
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
		
		tbarOpts.push({
			text: "选择采购申请单",
			action: function() {
				selectPurchaseApply();
			}
		});

		tbarOpts.push({
			text : biolims.common.delSelected,
			action : function() {
				removeChecked($("#enquiryItemTable"),
						"/storage/enquiry/delEnquiryItem.action");
			}
		});
		
	}	
	
	var options = table(true, $("#enquiry_id").val(), "/storage/enquiry/showEnquiryItemListJson.action", colOpts, tbarOpts);

	enquiryItemListTable = renderData($("#enquiryItemTable"), options);
	enquiryItemListTable.on('draw', function() {
		oldEnquiryListChangeLog = enquiryItemListTable.ajax.json();
	});
		
})
//选择采购申请单
function selectPurchaseApply(){
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/purchase/apply/showPurchaseApplyDialogList.action", ''],
		yes: function(index, layer) {
			
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addPurchaseApply .chosed").
				children("td").eq(0).text();
			var typeId = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addPurchaseApply .chosed").
			children("td").eq(3).text();
			var typeName = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addPurchaseApply .chosed").
			children("td").eq(4).text();
			top.layer.closeAll();
			if(typeId=="2"){//原辅料
				$.ajax({
					url : ctx + '/purchase/apply/findItemListJson.action',
					type : 'post',
					data : {
						id:id,
						typeId:typeId
					},
					success : function(data){
						var data = JSON.parse(data);
						if(data.success){//成功
							var list = data.list;
							for(var i=0;i<list.length;i++){
								$("#enquiryItemTable").find(".dataTables_empty").parent("tr").remove();
								var tr = $("<tr class='editagain'><td><input type='checkbox' class='icheck' value=''/></td></tr>");
								var tds="<td savename='storageId' typeId="+typeId+">"+list[i].storage.id+"</td><td savename='storageName'>"+list[i].storage.name+"</td>" +
								"<td savename='num'>"+list[i].storage.num+"<td class='edit' savename='note'></td>";
								tr.height(32);
								$("#enquiryItemTable").find("tbody").append(tr.append(tds));
							}
							checkall($("#enquiryItemTable"));
						}
					}
				})
			}else if(typeId=="77"){//设备
				$.ajax({
					url : ctx + '/purchase/apply/findItemListJson.action',
					type : 'post',
					data : {
						id:id,
						typeId:typeId
					},
					success : function(data){
						var data = JSON.parse(data);
						if(data.success){//成功
							var list = data.list;
							for(var i=0;i<list.length;i++){
								$("#enquiryItemTable").find(".dataTables_empty").parent("tr").remove();
								var tr = $("<tr class='editagain'><td><input type='checkbox' class='icheck' value=''/></td></tr>");
								var tds="<td savename='storageId' typeId="+typeId+">"+list[i].equipment.id+"</td><td savename='storageName'>"+list[i].equipment.name+"</td>" +
								"<td savename='num'>"+list[i].equipment.num+"<td class='edit' savename='note'></td>";
								tr.height(32);
								$("#enquiryItemTable").find("tbody").append(tr.append(tds));
							}
							checkall($("#enquiryItemTable"));
						}
					}
				})
			}else if(typeId=="88"){//服务
				$.ajax({
					url : ctx + '/purchase/apply/findItemListJson.action',
					type : 'post',
					data : {
						id:id,
						typeId:typeId
					},
					success : function(data){
						var data = JSON.parse(data);
						if(data.success){//成功
							var list = data.list;
							for(var i=0;i<list.length;i++){
								$("#enquiryItemTable").find(".dataTables_empty").parent("tr").remove();
								var tr = $("<tr class='editagain'><td><input type='checkbox' class='icheck' value=''/></td></tr>");
								var tds="<td savename='storageId' typeId="+typeId+">"+list[i].serviceCode+"</td><td savename='storageName'>"+list[i].serviceName+"</td>" +
								"<td savename='num'>"+list[i].num+"<td class='edit' savename='note'></td>";
								tr.height(32);
								$("#enquiryItemTable").find("tbody").append(tr.append(tds));
							}
							checkall($("#enquiryItemTable"));
						}
					}
				})
			}
		},
	})
}

// 获得保存时的json数据
function saveEnquiryItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for (var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			json[k] = $(tds[j]).text();
			
			if(k == "storageId") {
				json["type"] = $(tds[j]).attr("typeId");
				continue;
			}
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
//日志
function getEnquiryItemChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '询价明细编号为"' + v.code + '":';
		oldEnquiryListChangeLog.data.forEach(function(vv, ii) {
			if (vv.id == id) {
				for ( var k in v) {
					if (v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"'
								+ v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}