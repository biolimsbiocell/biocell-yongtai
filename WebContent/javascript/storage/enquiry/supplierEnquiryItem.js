var supplierEnquiryItemListTable;
var oldSupplierEnquiryItemListChangeLog;
var supplierEnquiryId;
$(function(){
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : "ID",
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		},
		"visible" : false
	});
	colOpts.push({
		"data" : "supplierId",
		"title" : "供应商编号",
		"createdCell" : function(td) {
			$(td).attr("saveName", "supplierId");
		}
	});
	colOpts.push({
		"data" : "supplierName",
		"title" : "供应商名称",
		"createdCell" : function(td,data,rowdata) {
			$(td).attr("saveName", "supplierName");
		}
	});
	colOpts.push({
		"data" : "num",
		"title" : "采购数量",
		"createdCell" : function(td) {
			$(td).attr("saveName", "num");
		},
		"className":"edit"
	});
	colOpts.push({
		"data" : "price",
		"title" : "单价",
		"createdCell" : function(td) {
			$(td).attr("saveName", "price");
		},
		"className":"edit"
	});
	colOpts.push({
		"data" : "fee",
		"title" : "总价",
		"createdCell" : function(td) {
			$(td).attr("saveName", "fee");
		}
	});
	colOpts.push({
		"data" : "type",
		"title" : "类型",
		"createdCell" : function(td) {
			$(td).attr("saveName", "type");
		},
		"visible" : false
	});
	colOpts.push({
		"data" : "note",
		"title" : "备注",
		"createdCell" : function(td) {
			$(td).attr("saveName", "note");
		},
		"className":"edit"
	});
	colOpts.push({
		"data" : "enquiryItemId",
		"title" : "询价明细关联",
		"createdCell" : function(td) {
			$(td).attr("saveName", "enquiryItemId");
		},
		"visible" : false
	});
	colOpts.push({
		"data" : "enquiry-id",
		"title" : "相关主表ID",
		"createdCell" : function(td) {
			$(td).attr("saveName", "enquiry-id");
		},
		"visible" : false
	});
	colOpts.push({
		"data" : "enquiry-name",
		"title" : "相关主表",
		"createdCell" : function(td) {
			$(td).attr("saveName", "enquiry-name");
		},
		"visible" : false
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
		
		tbarOpts.push({
			text: "选择供应商",
			action: function() {
				selectSupplier();
			}
		});

		tbarOpts.push({
			text : biolims.common.delSelected,
			action : function() {
				removeChecked($("#supplierEnquiryItemTable"),
						"/storage/enquiry/delSupplierEnquiryItem.action");
			}
		});
		
	}	
	
	var itemId = $('.layui-layer-iframe', parent.document).find("iframe").contents().
		find("#enquiryItemTable").children("td").eq(1).text();
	
	var options = table(true, $("#enquiry_id").val(), "/storage/enquiry/showSupplierEnquiryItemListJson.action?itemId="+itemId, colOpts, tbarOpts);

	supplierEnquiryItemListTable = renderData($("#supplierEnquiryItemTable"), options);
	supplierEnquiryItemListTable.on('draw', function() {
		oldSupplierEnquiryItemListChangeLog = supplierEnquiryItemListTable.ajax.json();
	});
		
})
//选择供应商
function selectSupplier(){
	if($("#enquiry_id").val()=="NEW"){
		top.layer.msg("请先保存");
		return;
	}
	/*$.ajax({
		url : ctx + '/purchase/apply/findItemListJson.action',
		type : 'post',
		data : {
			id:$("#enquiry_id").val()
		},
		success : function(data){
			var data = JSON.parse(data);
			if(data.msg){//子表明细没有数据
				top.layer.msg("请先添加询价管理明细信息,保存之后在执行此操作");
			}
		}
	})*/
	var row = $("#enquiryItemTable .selected");
	if(row.length<1){
		top.layer.msg("请选择询价管理明细");
		return;
	}else if(row.length>1){
		top.layer.msg("只能选择一条询价管理");
		return;
	}else if(!row.children("td").eq(0).find("input").val()){
		top.layer.msg("保存之后在执行此操作");
		return;
	}
	
	supplierEnquiryId = row.children("td").eq(0).find("input").val();
	var typeId = row.children("td").eq(1).attr("typeId");
	alert(typeId);
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/supplier/showSupplierDialogList.action", ''],
		yes: function(index, layer) {
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addSupplier .selected").each(function(i,v){
				var id = $(v).children("td").eq(1).text();
				var name = $(v).children("td").eq(2).text();
				
				top.layer.close(index);
				$("#supplierEnquiryItemTable").find(".dataTables_empty").parent("tr").remove();
				var tr = $("<tr class='editagain'><td><input type='checkbox' class='icheck' value=''/></td></tr>");
				var tds="<td savename='supplierId' typeId="+typeId+">"+name+"</td><td savename='supplierName' enquiryItemId="+supplierEnquiryId+">"+name+"</td>" +
				"<td class='edit' savename='num'>"+"<td class='edit' savename='price'></td><td savename='fee'></td>"+
				"<td class='edit' savename='note'></td>";
				tr.height(32);
				$("#supplierEnquiryItemTable").find("tbody").append(tr.append(tds));
			})
			checkall($("#supplierEnquiryItemTable"));
		},
	})
}

// 获得保存时的json数据
function saveSupplierEnquiryItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for (var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			json[k] = $(tds[j]).text();
			
			if(k == "supplierName") {
				json["enquiryItemId"] = $(tds[j]).attr("enquiryItemId");
				continue;
			}
			
			if(k == "supplierId") {
				json["type"] = $(tds[j]).attr("typeId");
				continue;
			}
			
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
//日志
function getSupplierEnquiryItemChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '供应商明细编号为"' + v.code + '":';
		oldSupplierEnquiryItemListChangeLog.data.forEach(function(vv, ii) {
			if (vv.id == id) {
				for ( var k in v) {
					if (v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"'
								+ v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}