var thisTd = '';
$("#cont_table td .add_data").click(function() {
	var num = $(this).attr("num").split(",");
	thisTd = $("#cont_table tr").eq(num[0]).find("td").eq(num[1]);
	showItem();
});

var itemIds = {};

var _info = $("#contInfo").val();
if (_info) {
	var info = eval("(" + _info + ")");
	var storageId = $("#storageId").val();

	var trTable = $("#cont_table tr");

	$.each(
					info,
					function(k, obj) {
						var num = obj.num.split(",");// 位置
						var itemId = obj.itemId;// 实验材料主数据明细ID
						var ldDate = obj.ldDate;// 冻存时间
						var itemName = obj.itemName;// 
						var objName = obj.objName;// 
						var cellNum = obj.cellNum;// 
						var mainDataId = obj.mainDataId;// 
						$("#cont_table tr").eq(num[0]).find("td").eq(num[1])
								.append("<span class='showSpan'></span>");
						var showSpan = $("#cont_table tr").eq(num[0])
								.find("td").eq(num[1]).find(".showSpan");
						showSpan
								.append(
										"<br> <a href='#' mainDataId='"
												+ mainDataId + "'>"
												+ mainDataId + "</a><br>")
								.find("a[mainDataId]")
								.click(
										function() {
											var _mainDataId = $(this).attr(
													"mainDataId");
											openDialog(window.ctx
													+ '/materials/materials/toViewMaterials.action?id='
													+ _mainDataId);

										});
						showSpan.append(cellNum + "<br>"
								+ $.format.date(ldDate, 'yyyy-MM-dd'));

						$("#cont_table tr").eq(num[0]).find("td").eq(num[1])
								.attr("itemId", itemId);
						$("#cont_table tr").eq(num[0]).find("td").eq(num[1])
								.attr("ldDate", ldDate);
						$("#cont_table tr").eq(num[0]).find("td").eq(num[1])
								.attr("itemName", itemName);
						$("#cont_table tr").eq(num[0]).find("td").eq(num[1])
								.attr("objName", objName);
						$("#cont_table tr").eq(num[0]).find("td").eq(num[1])
								.attr("cellNum", cellNum);
						$("#cont_table tr").eq(num[0]).find("td").eq(num[1])
								.attr("mainDataId", mainDataId);
						itemIds[itemId] = true;
					});
}

var showChar = [ '', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
		'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y',
		'Z' ];
var tds = $("#cont_table tr td");
var delItemsIds = [];
$.each(tds, function(i, td) {
	var num = $(this).attr("num");
	if (num != undefined) {
		var rowNum = $("#cont_table tr").length;
		var rowcol = num.split(",");
		$(td).find(".showIndexSpan").html(
				showChar[parseInt(rowcol[1])]
						+ (rowNum - parseInt(rowcol[0]) - 1));
	}

	var cls = $(td).find(".remove_data");
	cls.click(function() {
		var itemId = $(td).attr("itemId");
		if (itemId) {
			$(td).attr("itemId", '');
			$(td).attr("ldDate", '');
			$(td).attr("objName", '');
			$(td).attr("itemName", '');
			$(td).attr("cellNum", '');
			$(td).attr("mainDataId", '');
			$(td).find(".showSpan").html("");
			delItemsIds.push(itemId);
			$("#delItemIds").val(delItemsIds.join("','"));
			itemIds[itemId] = false;
		}

	});
});

var lasttr = $("#cont_table tr:last td");
$.each(lasttr, function(i, obj) {
	if ($(obj).find(".show_row")) {
		$(obj).html(showChar[parseInt($(obj).text())]);
	}
});

function showItem() {
	Ext.QuickTips.init();
	Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
	var posiId = $("#showId").val();
	var showMaterialsItem = new Ext.Window(
			{
				id : 'showMaterialsItem',
				modal : true,
				title : biolims.common.selectedDetail,
				layout : 'fit',
				width : document.body.clientWidth / 2,
				height : document.body.clientHeight / 1.5,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"+window.ctx+"/sysmanage/container/showMaterialsItemList.action?posiId="
									+ posiId
									+ "' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						showMaterialsItem.close();
					}
				} ]
			});
	showMaterialsItem.show();
}

function setValue(data) {
	var win = Ext.getCmp('showMaterialsItem');
	if (win) {
		win.close();
	}
	var cellNum = data.cellNum;
	var name = data.name;
	var ldDate = data.ldDate;
	var id = data.id;
	var mainDataId = data["sc-id"];
	// var positionId=data.positionId;
	if (itemIds[id] == true) {
		message("此材料在容器中已存在！");
		return;
	}
	// if(positionId){
	// message("此材料在其它位置的容器中已存在！");
	// return ;
	// }

	$(thisTd).attr("itemId", id);
	$(thisTd).attr("ldDate", $.format.date(ldDate, 'yyyy-MM-dd'));
	$(thisTd).attr("objName", 'MaterialsCellItem');
	$(thisTd).attr("itemName", name);
	$(thisTd).attr("cellNum", cellNum);
	$(thisTd).attr("mainDataId", mainDataId);

	if ($(thisTd).find(".showSpan").length == 0) {
		$(thisTd).append("<span class='showSpan'></span>");
	} else {
		$(thisTd).find(".showSpan").html("");
	}

	$(thisTd).find(".showSpan").append(
			"<br><a href='#' mainDataId='" + mainDataId + "'>" + mainDataId
					+ "</a>").find("a").click(
			function() {
				var _mainDataId = $(this).attr("mainDataId");
				openDialog(window.ctx
						+ '/materials/materials/toViewMaterials.action?id='
						+ _mainDataId);

			});
	$(thisTd).find(".showSpan").append("<br>" + cellNum);
	$(thisTd).find(".showSpan").append(
			"<br>" + $.format.date(ldDate, 'yyyy-MM-dd'));
	itemIds[id] = true;
}
