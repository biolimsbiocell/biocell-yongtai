	//显示二维的位置信息
	var gridContainerGrid;
	var colNum = $("#hid_3d_sub_col_num").val();
	var rowNum = $("#hid_3d_sub_row_num").val();
	var idStr = $("#tree_Id").val();
	var subIdStr = $("#sub_tree_Id").val();
	var type=$("#type").val();
	$(function(){
		var cols={};
		cols.sm = false;
	    var fields=[];
	    fields.push({
			name: "key",
			type:"string"
		});
	    for(var i=0;i<colNum;i++){
	    	fields.push({
	    		name: i+1,
	    		type:"string"
	    	});
	    }
		    
		cols.fields=fields;
		var cm=[];
		cm.push({
			dataIndex: "key",
			hidden : false,
			header:"",
			width:60
		});
		for(var i=0;i<colNum;i++){
			cm.push({
				dataIndex: i+1,
				hidden : false,
				header:""+(i+1)+"",
				width:162,
				height:120,
				renderer:function(value){
				    return '<div align="center">'+value+'</div>';
				}
			});
		}
		cols.cm=cm;
		var loadParam={};
		loadParam.url=ctx+"/storage/container/showGridDataDialogJson.action?id="+ idStr+"&subId="+subIdStr;
		var opts={};
		opts.tbar = [];
		
		opts.title =  subIdStr;
		opts.height=400;
		opts.tbar.push({
			text : biolims.common.fillDetail,
			handler : null
		});
		opts.tbar.push({
			text : biolims.common.uncheck,
			handler : null
		});
		opts.tbar.push({
			text : biolims.common.delSelected,
			handler : null
		});
		opts.tbar.push({
			text : biolims.common.editableColAppear,
			handler : null
		});
		
		
		
		
		gridContainerGrid=gridEditTable("gridContainerDialogdiv",cols,loadParam,opts);
		$("#gridContainerDialogdiv").data("gridContainerGrid", gridContainerGrid);
		$(".x-panel-bbar,.x-toolbar").css("width", "100%");
		$(".x-panel-tbar").css("width", "100%");
		
		gridContainerGrid.addListener('cellclick', cellclick);   
		   
		function cellclick(grid, rowindex, columnIndex, e){
			if(columnIndex>1){
				var adr = "";
				if((columnIndex-1)>=10){
					adr = subIdStr + "-" + String.fromCharCode(65+rowindex) + (columnIndex-1);
				}else{
					adr = subIdStr + "-" + String.fromCharCode(65+rowindex) + "0"+(columnIndex-1);
				}
				window.parent.setFrozenLocationFun(adr,subIdStr,type);
			}
			
		}   
	    
	});