var showStorageContainerListTables,oldChangeLog;
$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": "ID",
		"width":"10px",
//		"className": "edit",
		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		}
	});
	
	//容器名称
	colOpts.push({
		"data": "name",
		"title": biolims.common.containerName,
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
		}
	});
	//行数
	colOpts.push({
		"data": "rowNum",
		"title": biolims.storage.rowNum,
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "rowNum");
		}
	});
	//列数
	colOpts.push({
		"data": "colNum",
		"title": biolims.storage.colNum,
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "colNum");
		}
	});
	
	var tbarOpts = [];
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#showStorageDiv"));
		}

	});
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveItem($("#showStorageDiv"));
		}
	});
	tbarOpts.push({
		text : biolims.common.editwindow,
		action : function() {
			editItemLayer($("#showStorageDiv"))
		}
	});
	var showStorageListOps= table(true, null, "/storage/container/storageContainerListJson.action", colOpts, tbarOpts);
	showStorageListTab = renderData($("#showStorageDiv"), showStorageListOps);
	//日志 dwb 2018-05-12 14:07:21
	showStorageListTab.on('draw', function() {
		oldChangeLog = showStorageListTab.ajax.json();
	});
});

/*var type_id = "";
var type_name = "";
$.ajax({
		type: "post",
		url: ctx + "/dic/type/selectDicMainTypes.action",
		success: function(data) {
			var data = JSON.parse(data);
			console.log(data);
			if(data.success) {
				for(var key in data.data){
					console.log("属性：" + key + ",值：" + data.data[key]);
					type_id += key;
					type_name += data.data[key];
				}
			}
		}
	});*/


// 保存
function saveItem() {
	var ele=$("#showStorageDiv");
	var changeLog = biolims.common.containerMent+"：";
	var data = saveItemjson(ele);
	changeLog = getChangeLog(data, ele, changeLog);
	var changeLogs="";
	if(changeLog != biolims.common.containerMent+"："){
		changeLogs=changeLog;
	}
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		type: 'post',
		url: '/storage/container/changeLocation.action',
		data: {
            id: "",
			dataJson: data,
			logInfo: changeLogs
		},
		success: function(data) {
			var data = JSON.parse(data);
			if(data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed);
			};
		}
	});
}
// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断是否有效并转换为数字
			/*if(k == "state") {
				var state = $(tds[j]).text();
				if(state == biolims.master.invalid) {
					json[k] = "0";
				} else if(state == biolims.master.valid) {
					json[k] = "1";
				}
				continue;
			}*/
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		if(!id){
			changeLog += '新增记录:';
			for(var k in v) {
					var title = ele.find("th[savename=" + k + "]").text();
					changeLog += '"' + title + '"为"' + v[k] + '";';
				}
			return true;
		}
		changeLog += 'ID为"' + v.id + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}