$(function() {
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : biolims.common.id,
	});
	colOpts.push({
		"data" : "name",
		"title" : biolims.user.eduName1,
	});
	colOpts.push({
		"data" : "rowNum",
		"title" : biolims.common.rowCode,
	});
	colOpts.push({
		"data" : "colNum",
		"title" : biolims.common.colCode,
	});
	var tbarOpts = [];
	var addContainerOptions = table(false, null,
			'/storage/container/showContainerTableJson.action?flag=' + $("#flag").val(),
			colOpts, tbarOpts)
	var ContainerTable = renderData($("#addcontainerTable"), addContainerOptions);
	$("#addcontainerTable").on(
			'init.dt',
			function(e, settings) {
				// 清除操作按钮
				$("#addcontainerTable_wrapper .dt-buttons").empty();
				$('#addcontainerTable_wrapper').css({
					"padding": "0 16px"
				});
			var trs = $("#addcontainerTable tbody tr");
			ContainerTable.ajax.reload();
			ContainerTable.on('draw', function() {
				trs = $("#addcontainerTable tbody tr");
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			});
			trs.click(function() {
				$(this).addClass("chosed").siblings("tr")
					.removeClass("chosed");
			});
			});
})

