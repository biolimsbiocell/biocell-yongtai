	//显示二维的位置信息
	var gridContainerGrid;
	var colNum = $("#hid_3d_col_num").val();
	var rowNum = $("#hid_3d_row_num").val();
	var idStr = $("#tree_Id").val();
	var type = $("#type").val();
	$(function(){
		var cols={};
		cols.sm = false;
	    var fields=[];
	    fields.push({
			name: "key",
			type:"string"
		});
	    for(var i=0;i<colNum;i++){
	    	fields.push({
	    		name: i+1,
	    		type:"string"
	    	});
	    }
		    
		cols.fields=fields;
		var cm=[];
		cm.push({
			dataIndex: "key",
			hidden : false,
			header:"",
			width:60
		});
		for(var i=0;i<colNum;i++){
			cm.push({
				dataIndex: i+1,
				hidden : false,
				header:""+(i+1)+"",
				width:120,
				height:120,
				renderer:function(value){
				    return '<div align="center">'+value+'</div>';
				}
			});
		}
		cols.cm=cm;
		var loadParam={};
		loadParam.url=ctx+"/storage/container/showGridDataMidDialogJson.action?id="+ idStr;
		var opts={};
		opts.tbar = [];
		opts.title =  idStr;
		opts.height = 400;
		opts.tbar.push({
			text : biolims.common.fillDetail,
			handler : null
		});
		opts.tbar.push({
			text : biolims.common.uncheck,
			handler : null
		});
		opts.tbar.push({
			text : biolims.common.delSelected,
			handler : null
		});
		opts.tbar.push({
			text : biolims.common.editableColAppear,
			handler : null
		});
		
		
		
		
		gridContainerGrid=gridEditTable("gridContainerdiv",cols,loadParam,opts);
		$("#gridContainerdiv").data("gridContainerGrid", gridContainerGrid);
		$(".x-panel-bbar,.x-toolbar").css("width", "100%");
		$(".x-panel-tbar").css("width", "100%");
		
		gridContainerGrid.addListener('cellclick', cellclick);   
		   
		function cellclick(grid, rowindex, columnIndex, e){
			if(columnIndex>1){
				var adr = idStr + "-" + String.fromCharCode(65+rowindex) + (columnIndex-1);
				for(var i=rowindex;i<rowNum;i++){
					if(i==rowindex){
						for(var j=(columnIndex+1);j<=(parseInt(colNum)+1);j++){
							var contr = gridContainerGrid.store.getAt(i).get(j-1);
							if(contr.length<=52){
								var adre = idStr + "-" + String.fromCharCode(65+i) + (j-1); 
								adr = adr + "," + adre;
							}
						}
					}else{
						for(var j=2;j<=(parseInt(colNum)+1);j++){
							var contr = gridContainerGrid.store.getAt(i).get(j-1);
							if(contr.length<=52){
								var adre = idStr + "-" + String.fromCharCode(65+i) + (j-1); 
								adr = adr + "," + adre;
							}
						}
					}
					
				}
				window.parent.setFrozenLocationFun(adr,type);
			}
			
		}   
	    
	});
	var nw;
	 function showDialog(n,cType,type) {
		
//			nw=		load("/storage/container/showGridDataDialog.action", {
//						idStr : n,
//						cType : cType
//					}, "#gridContainerMidDiolagdiv");
		 
		 var url = "/storage/container/showGridDataDialog.action?idStr="+n+"&cType="+cType+"&type="+type;
		 var options = {};
			options.width = 900;
			options.height = 500;
		 loadDialogPage(null,"盒子",url,{
				"Confirm" : function() {}
			}, false, options);
		
		 
			
		}
	 function showBox(n){
		 
		 
		 
			var url = "/system/storage/storageBox/storageBoxSelect.action?flag=StorageBoxFun&spId="+n;
			var options = {};
			options.width = 500;
			options.height = 500;
			loadDialogPage(null, "选择盒子", url, {
				"Confirm" : function() {
					
					var record = storageBoxDialogGrid.getSelectionModel().getSelections();
					
					ajax("post", "/system/storage/storageBox/setStorageBox.action", {
						spId : n,
						id : record[0].get("id")
					}, function(data) {
						if (data.success) {
							
							message("设置成功！");
						}
					});
					gridContainerGrid.getStore().reload();
					$(this).dialog("close");
				}
			}, false, options);
		
		 
		 
		 
//			var win = Ext.getCmp('StorageBoxFun');
//			if (win) {win.close();}
//			var StorageBoxFun= new Ext.Window({
//			id:'StorageBoxFun',modal:true,title:'选择盒子',layout:'fit',width:500,height:500,closeAction:'close',
//			plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
//			collapsible: true,maximizable: true,
//			items: new Ext.BoxComponent({id:'maincontent', region: 'center',
//			html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/system/storage/storageBox/storageBoxSelect.action?flag=StorageBoxFun&spId="+n+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
//			buttons: [
//			{ text: '关闭',
//			 handler: function(){
//				 StorageBoxFun.close(); }  }]  });     StorageBoxFun.show(); 
	 }
	 
	 function setStorageBoxFun(rec){
		 document.getElementById('sp_storageBox_name').value=rec.get('name');
			var win = Ext.getCmp('StorageBoxFun');
			if(win){
				win.close();
			}
		}	