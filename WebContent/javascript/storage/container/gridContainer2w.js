var gridContainer2wGrid;
var colNum = $("#hid_3d_col_num").val();
$(function(){
	var cols={};
	cols.sm = false;
    var fields=[];
    fields.push({
		name: "key",
		type:"string"
	});
    for(var i=0;i<colNum;i++){
    	fields.push({
    		name: biolims.common.beforePageText+(i+1)+"排",
    		type:"string"
    	});
    }
	    
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex: "key",
		hidden : false,
		header:"",
		width:10*4
	});
	for(var i=0;i<colNum;i++){
		cm.push({
			dataIndex: biolims.common.beforePageText+(i+1)+"排",
			hidden : false,
			header:"<table><tr><td align=center>"+biolims.common.beforePageText+(i+1)+"排</td></tr></table>",
			width:20*10,
			renderer:function (value, metadata, record, rowIndex, columnIndex, store) {  
				if(value!=null&&value!=""){
					var title = 'Details for&nbsp;'+String.fromCharCode(65+rowIndex)+(columnIndex-1);  
					var numStr = "数量（入库单/主数据/总数目）";
					var num = value;
					
					var id = $("#sp_id").val() + "-" + (rowIndex + 1) + "-" + (columnIndex-1);
					var aid = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:loadChildPage(\''+id+'\')">进入</a>';
				    
				    var table = '<table  style="height:100%;width:200px;background:red;align:center;">';
//				    var tr_1 = ' <tr height=15px><td><input type="checkbox" name="box" value="'+id+'" />&nbsp;&nbsp;<a href=javascript:FrozenLocationFun("'+id+'")>转存</a></td></tr>';
				    var tr_1 = ' <tr height=30px><td style="align:center">'+id + aid +'</td></tr>';
				    var tr_2 = ' <tr height=30px><td style="align:center">'+numStr+'</td></tr>';
				    var tr_3 = ' <tr height=30px><td style="align:center">'+num+'</td></tr>';
				    var tableEnd='</table>';
				    var font = "<div class=tip style=height:100%;width:100%;>"
				    var fontEnd = "</div>";
//				    metadata.attr = 'ext:qtitle="' + title + '"' + ' ext:qtip="' + qtip1 +'"';  
				   
				    var tr_4 = "";
				    var displayText = "";
				    	displayText =  table + tr_1 + tr_2 + tr_3 + tableEnd;
				    return displayText;  
				}else{
					var table = '<table  style="height:100%;width:200px;">';
				    var tr_1 = ' <tr height=30px><td></td></tr>';
				    var tr_2 = ' <tr height=30px><td></td></tr>';
				    var tr_3 = ' <tr height=30px><td></td></tr>';
				    var tableEnd='</table>';
				    var displayText =  table + tr_1 + tr_2 + tr_3 + tableEnd;
				    return displayText; 
				}
			    
			}
		});
	}
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/storage/container/showGridData2wJson.action?id="+ $("#sp_id").val();
//	loadParam = null;
	var opts={};
	opts.tbar = [];
	
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	
	gridContainer2wGrid=gridEditTable("gridContainer2wdiv",cols,loadParam,opts);
	$("#gridContainer2wdiv").data("gridContainer2wGrid", gridContainer2wGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
	
})

//加载存储位置上详细信息的页面
function loadChildPage(id){
	window.parent.location = window.ctx + '/storage/position/toEditStoragePosition.action?id=' + id;
}
	   