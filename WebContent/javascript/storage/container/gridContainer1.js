var gridContainerGrid1;
var colNum = $("#hid_3d_col_num").val();
var maxNum=$("#hid_3d_maxNum").val();
	$(function(){
		var cols={};
		cols.sm = false;
		var fields=[];
		fields.push({
			name: "key",
			type:"string"
		});
		for(var i=0;i<colNum;i++){
			fields.push({
				name: i+1,
				type:"string"
			});
		}
		
		cols.fields=fields;
		var cm=[];
		cm.push({
			dataIndex: "key",
			hidden : false,
			header:"",
			width:5*4
		});
		for(var i=0;i<colNum;i++){
			cm.push({
				dataIndex: i+1,
				hidden : false,
				header:""+(i+1)+"",
				width:20*6,
				renderer:function (value, metadata, record, rowIndex, columnIndex, store) {
					var table = '<table  style="height:100%;width:200px;">';
					var tr_1 = ' <tr height=15px><td>'+value+'</td></tr>';
					var tableEnd='</table>';
					var displayText =  table + tr_1+ tableEnd;
					return displayText; 
				}
			
			});
		}
		
		cols.cm=cm;
		var loadParam={};
		if($("#type").val()=='plasma'){
			loadParam.url=ctx+"/storage/container/showGridTestDataJson.action?id="+ $("#bloodSplit_id").val()+"&type="+$("#type").val()+"&maxNum=1";
		}else if($("#type").val()=='dna'){
			loadParam.url=ctx+"/storage/container/showGridTestDataJson.action?id="+ $("#experimentDnaGet_id").val()+"&type="+$("#type").val()+"&maxNum=1";
		}else if($("#type").val()=='wk'){
			loadParam.url=ctx+"/storage/container/showGridTestDataJson.action?id="+ $("#wk_id").val()+"&type="+$("#type").val()+"&maxNum=1";
		}else if($("#type").val()=='pooling'){
			loadParam.url=ctx+"/storage/container/showGridTestDataJson.action?id="+ $("#pooling_id").val()+"&type="+$("#type").val()+"&maxNum=1";
		}else if($("#type").val()=='qc2100'){
			loadParam.url=ctx+"/storage/container/showGridTestDataJson.action?id="+ $("#wKQualitySampleTask_id").val()+"&type="+$("#type").val()+"&maxNum=1";
		}else if($("#type").val()=='qcQpcr'){
			loadParam.url=ctx+"/storage/container/showGridTestDataJson.action?id="+ $("#wKQpcrSampleTask_id").val()+"&type="+$("#type").val()+"&maxNum=1";
		}
//	loadParam = null;
		var opts={};
		opts.tbar = [];
		
		opts.tbar.push({
			text : biolims.common.fillDetail,
			handler : null
		});
		opts.tbar.push({
			text : biolims.common.uncheck,
			handler : null
		});
		opts.tbar.push({
			text : biolims.common.delSelected,
			handler : null
		});
		opts.tbar.push({
			text : biolims.common.editableColAppear,
			handler : null
		});
		
		gridContainerGrid1=gridEditTable("gridContainerdiv1",cols,loadParam,opts);
		$("#gridContainerdiv1").data("gridContainerGrid1", gridContainerGrid1);
		$(".x-panel-bbar,.x-toolbar").css("width", "100%");
		$(".x-panel-tbar").css("width", "100%");
	});



//显示转存输入框
function FrozenLocationFun(stri){
	$("#oldLoc").val(stri);
	 var win = Ext.getCmp('FrozenLocationFun');
	 if (win) {win.close();}
	 var ProjectFun= new Ext.Window({
	 id:'FrozenLocationFun',modal:true,title:biolims.sample.selectLocation,layout:'fit',width:600,height:580,closeAction:'close',
	 plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	 collapsible: true,maximizable: true,
	 items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
	 html:"<iframe scrolling='no' name='maincontentframe' src='/storage/position/showStoragePositionTreeDialog.action?typeId="+$("#storage_storageType").val()+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
	 buttons: [
	 { text: biolims.common.close,
	  handler: function(){
	  ProjectFun.close(); }  }]  });     ProjectFun.show(); }

function setFrozenLocationFun(str){
	$("#newAdr").show();
	var ads = str.split(",");
	$("#newLoc").val(ads[0]);
	 var win = Ext.getCmp('FrozenLocationFun')
	 if(win){win.close();}
	 }		

/*function unloading(){
	var obj = document.getElementsByName("box");
	var str = [];
	for(k in obj){
		if(obj[k].checked){
			str.push(obj[k].value);
		}
			
	}
	$("#newAdr").show();
	
}*/
//导出
function exportexcel() {
	gridContainerGrid1.title = biolims.common.exportList;
	var vExportContent = gridContainerGrid1.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm1.submit();
}
//执行转存操作
function chengeAdr(){
	var newAdr = $("#newLoc").val();
	var oldArr = $("#oldLoc").val();
	var classType = $("#storage_storageType").val();
	if(oldArr&&oldArr!=""){
		ajax("post", "/storage/container/changeLocation.action", {
			id : oldArr,
			type : "1",      //操作类型：材料入库（0）、材料主数据(1)
			classType : classType,
			newAdr : newAdr
			}, function(data) {
				if (data.success) {
					if(data.data){
						gridContainerGrid1.getStore().commitChanges();
						gridContainerGrid1.getStore().reload();
						message("转存成功。");
					}else{
						message("转存失败，请重试！");
					}
				} else {
					message(biolims.common.anErrorOccurred);
				}
			}, null);
	}
}

//行序
function putSingle(){
	var obj = document.getElementsByName("box"); 
	if(document.getElementById("selAll").checked == false){ 
		  for(var i=0; i<obj.length; i++){ 
		    obj[i].checked=false; 
		  } 
	  }else{ 
		  for(var i=0; i<obj.length; i++){	  
			    obj[i].checked=true; 
		  }	
	 } 
}

//列序
function putDouble(){
	var checkboxs=document.getElementsByName("box"); 
	for (var i=0;i<checkboxs.length;i++) { 
	  var e=checkboxs[i]; 
	  e.checked=!e.checked; 
	  setSelectAll(); 
	} 
}

//当选中所有的时候，全选按钮会勾上 
function setSelectAll(){ 
	var obj=document.getElementsByName("box"); 
	var count = obj.length; 
	var selectCount = 0; 
	
	for(var i = 0; i < count; i++){ 
		if(obj[i].checked == true){ 
			selectCount++;	
		} 
	} 
	if(count == selectCount){	
		document.all.selAll.checked = true; 
	}else{ 
		document.all.selAll.checked = false; 
	} 
} 
	   