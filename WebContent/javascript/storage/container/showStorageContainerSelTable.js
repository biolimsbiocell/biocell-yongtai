$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.storage.containerId,
	});
	colOpts.push({
		"data": "name",
		"title": biolims.storage.container,
	});
	colOpts.push({
		"data": "rowNum",
		"title": biolims.storage.rowNum,
	});
	colOpts.push({
		"data": "colNum",
		"title": biolims.storage.colNum,
	});
	var tbarOpts = [];
	var options = table(false, null,
			'/storage/container/showContainerTableJson.action',colOpts , tbarOpts)
		var containerTable = renderData($("#addContainer"), options);
	$("#addContainer").on(
	'init.dt',
	function(e, settings) {
		// 清除操作按钮
		$("#addContainer_wrapper .dt-buttons").empty();
		$('#addContainer_wrapper').css({
			"padding": "0 16px"
		});
	var trs = $("#addContainer tbody tr");
	containerTable.ajax.reload();
	containerTable.on('draw', function() {
		trs = $("#addContainer tbody tr");
		trs.click(function() {
			$(this).addClass("chosed").siblings("tr")
				.removeClass("chosed");
		});
	});
	trs.click(function() {
		$(this).addClass("chosed").siblings("tr")
			.removeClass("chosed");
	});
	});

})

