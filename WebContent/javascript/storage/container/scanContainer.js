$(function() {
	var showChar = [ '', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
			'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
			'Y', 'Z' ];
	var tds = $("#cont_table tr td");
	$.each(tds, function(i, td) {
		var num = $(this).attr("num");
		if (num != undefined) {
			var rowNum = $("#cont_table tr").length;
			var rowcol = num.split(",");
			$(td).find(".showIndexSpan").html(
					showChar[parseInt(rowcol[1])]
							+ (rowNum - parseInt(rowcol[0]) - 1));
		}
	});
	 
	var lasttr = $("#cont_table tr:last td");
	$.each(lasttr, function(i, obj) {
		if ($(obj).find(".show_row")) {
			$(obj).html(showChar[parseInt($(obj).text())]);
		}
	});
});