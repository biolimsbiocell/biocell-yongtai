var ctGrid;
$(function(){
	var cols={};
//	cols.sm = true;
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    
	    fields.push({
		name:'name',
		type:"string"
	});

	    fields.push({
		name:'rowNum',
		type:"string"
	});

	    fields.push({
		name:'colNum',
		type:"string"
	});
	   
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*6,
		hidden:true,
		sortable:true
	});

	cm.push({
		dataIndex:'name',
		header:biolims.common.containerName,
		width:40*6,
		hidden:false,
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'rowNum',
		header:biolims.storage.rowNum,
		width:40*6,
		hidden:false,
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'colNum',
		header:biolims.storage.colNum,
		width:40*6,
		hidden:false,
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/storage/container/showContainerJson.action";
	var opts={};
	opts.tbar = [];
	opts.title=biolims.common.containerMent;
	opts.height=document.body.clientHeight;

	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.save,
		handler : save
	});
	ctGrid=gridEditTable("show_ct_div",cols,loadParam,opts);
	$("#show_ct_div").data("ctGrid", ctGrid);
});


//保存
function save(){	
	//var selectRecord = ctGrid.getSelectionModel();
	//var inItemGrid = $("#show_ct_div").data("ctGrid");
	var itemJson = commonGetModifyRecords(ctGrid);
	if(itemJson.length>0){
		//$.each(selectRecord.getSelections(), function(i, obj) {
			ajax("post", "/storage/container/saveContainer.action", {
				data : itemJson
			}, function(data) {
				message(biolims.common.saveSuccess);
				ctGrid.getStore().commitChanges();
				ctGrid.getStore().reload();
				
			}, null);			
		//});
	}else{
		message(biolims.common.noData2Save);
	}
	
}
function add(){
		window.location=window.ctx+'/goods/sample/ct/editct.action';
	}

function exportexcel() {
	ctGrid.title = biolims.common.exportList;
	var vExportContent = ctGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
function search() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				
				commonSearchAction(ctGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
}
