	//显示二维的位置信息
	var gridContainerBoxGrid;
	var colNum = $("#hid_3d_col_num").val();
	var rowNum = $("#hid_3d_row_num").val();
	var idStr = $("#tree_Id").val();
	var boxId = $("#boxId").val();
	if(!boxId){
		boxId = $("#storageBox_id").val();
	}
		
	var type = $("#type").val();
	$(function(){
		var cols={};
		cols.sm = false;
	    var fields=[];
	    fields.push({
			name: "key",
			type:"string"
		});
	    for(var i=0;i<colNum;i++){
	    	fields.push({
	    		name: i+1,
	    		type:"string"
	    	});
	    }
		    
		cols.fields=fields;
		var cm=[];
		cm.push({
			dataIndex: "key",
			hidden : false,
			header:"",
			width:60
		});
		for(var i=0;i<colNum;i++){
			cm.push({
				dataIndex: i+1,
				hidden : false,
				header:""+(i+1)+"",
				width:120,
				height:120,
				renderer:function(value){
				    return '<div align="center">'+value+'</div>';
				}
			});
		}
		cols.cm=cm;
		var loadParam={};
		loadParam.url=ctx+"/storage/container/showGridDataBoxDialogJson.action?id="+ boxId;
		var opts={};
		opts.tbar = [];
		opts.title =  idStr;
	
		opts.tbar.push({
			text : biolims.common.fillDetail,
			handler : null
		});
		opts.tbar.push({
			text : biolims.common.uncheck,
			handler : null
		});
		opts.tbar.push({
			text : biolims.common.delSelected,
			handler : null
		});
		opts.tbar.push({
			text : biolims.common.editableColAppear,
			handler : null
		});
		
		
		
		
		gridContainerBoxGrid=gridEditTable("gridContainerBoxDiolagdiv",cols,loadParam,opts);
		$("#gridContainerBoxDiolagdiv").data("gridContainerBoxGrid", gridContainerBoxGrid);
		$(".x-panel-bbar,.x-toolbar").css("width", "100%");
		$(".x-panel-tbar").css("width", "100%");
		
		gridContainerBoxGrid.addListener('cellclick', cellclick);   
		   
		function cellclick(grid, rowindex, columnIndex, e){
			if(columnIndex>1){
				var adr = idStr + "-" + String.fromCharCode(65+rowindex) + (columnIndex-1);
				for(var i=rowindex;i<rowNum;i++){
					if(i==rowindex){
						for(var j=(columnIndex+1);j<=(parseInt(colNum)+1);j++){
							var contr = gridContainerBoxGrid.store.getAt(i).get(j-1);
							if(contr.length<=52){
								var adre = idStr + "-" + String.fromCharCode(65+i) + (j-1); 
								adr = adr + "," + adre;
							}
						}
					}else{
						for(var j=2;j<=(parseInt(colNum)+1);j++){
							var contr = gridContainerBoxGrid.store.getAt(i).get(j-1);
							if(contr.length<=52){
								var adre = idStr + "-" + String.fromCharCode(65+i) + (j-1); 
								adr = adr + "," + adre;
							}
						}
					}
					
				}
				setStorageBoxFun(adr,type);
			}
			
		}   
	    
	});
	var nw;
	 
	function setStorageBoxFun(str,type){
	var ids = str.split(",");
	var str1="";
	var strId="";
	if(ids.length>0){
		str1 =ids[0];
		var str2 = str1.split("-");
		if(str2.length>1){
			if(str2[1].length==2){
				strId = str2[1].substr(0,1)+"0"+str2[1].substr(1);
			}else{
				strId = str2[1];
			}
		}
		var gridGrid = sampleInItemGrid;
		var selRecords = gridGrid.getSelectOneRecord(); 
		if(selRecords){
			//selRecords.set('location',strId);
			message("请不要勾选样本！");
		}else{
			$("#sampleIn_location").val(str2[0]+"-"+strId);
		}
	}
//	else if(ids.length==3){
//		if(ids[2].length==2){
//			strId = ids[2].substr(0,1)+"0"+ids[2].substr(1);
//		}else{
//			strId = ids[2];
//		}
//		str1 = ids[0]+"-"+ids[1]+"-"+strId;
//	}else if(ids.length==4){
//		if(ids[3].length==2){
//			strId = ids[3].substr(0,1)+"0"+ids[3].substr(1);
//		}else{
//			strId = ids[3];
//		}
//		str1 = ids[0]+"-"+ids[1]+"-"+ids[2]+"-"+strId;
//	}else if(ids.length==5){
//		if(ids[4].length==2){
//			strId = ids[4].substr(0,1)+"0"+ids[4].substr(1);
//		}else{
//			strId = ids[4];
//		}
//		str1 = ids[0]+"-"+ids[1]+"-"+ids[2]+"-"+ids[3]+"-"+strId;
//	}
	 	
		$(this).dialog("close");
//		 var win = Ext.getCmp('StorageBoxFun')
//		 if(win){win.close();}
 }