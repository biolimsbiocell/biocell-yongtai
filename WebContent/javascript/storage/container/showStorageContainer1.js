var gdsGrid;
$(function(){
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/storage/container.action";
	var opts={};
	opts.tbar = [];
	opts.title="容器管理";
	opts.height=document.body.clientHeight;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	var button = new Ext.Button({   
        text: biolims.common.save,   
        iconCls: "save"   
}); 





var name = new Ext.form.TextField({
	allowBlank : true
});
var rowNum = new Ext.form.NumberField({
	allowBlank : true,
	maxValue : 100
});
var colNum = new Ext.form.NumberField({
	allowBlank : true,
	maxValue : 100
});
 
var store = new Ext.data.JsonStore({
	root : 'results',
	remoteSort : true,
	fields : [ {
		name : 'id'
	}, {
		name : 'name'
	} ],
	proxy : new Ext.data.HttpProxy({
		url : window.ctx + '/storage/container/storageContainerList.action',
		method : 'POST'
	})
});

store.load();
var cob = new Ext.form.ComboBox({

	store : store,
	displayField : 'name',
	valueField : 'id',
	typeAhead : true,
	mode : 'remote',
	forceSelection : true,
	triggerAction : 'all',
	selectOnFocus : true
});

function validateFun(record) {
	var name = record.get('name');
	var rowNum = record.get('rowNum');
	var colNum = record.get('colNum');

	if (name == null || name == 0) {
		alertMessage("名称不能为空。");
		return false;
	}
	if (rowNum == null || rowNum == 0) {
		alertMessage("行数不能为空。");
		return false;
	}

	if (colNum == null || colNum == 0) {
		alertMessage("列数不能为空。");
		return false;
	}

	return true;
}
function views() {
	var record = gridGrid.getSelectionModel().getSelected();
	if (!record) {
		alertMessage("请选择您要查看的容器!");
		return;
	}
	var rowNum = record.get("rowNum");
	var colNum = record.get("colNum");
	if (!rowNum) {
		alertMessage("请填写行数!");
		return;
	}
	if (!colNum) {
		alertMessage("请填写列数!");
		return;
	}

	var win = Ext.getCmp('showMainStorageItemAllSelectList');
	if (win) {
		win.close();
	}

	var ctx = $("#ctx").val();
	var showMainStorageItemAllSelectList = new Ext.Window({
		id : 'showMainStorageItemAllSelectList',
		modal : true,
		title : '查看容器',
		layout : 'fit',
		width : 780,
		height : 500,
		closeAction : 'close',
		plain : true,
		bodyStyle : 'padding:5px;',
		buttonAlign : 'center',
		collapsible : true,
		maximizable : true,
		items : new Ext.BoxComponent({
			id : 'maincontent',
			region : 'center',
			html : "<iframe scrolling='no' name='maincontentframe' src='" + ctx
					+ "/sysmanage/container/scanContainer.action?rowNum=" + rowNum + "&colNum=" + colNum
					+ "' frameborder='0' width='780' height='500' ></iframe>"
		}),
		buttons : [ {
			text : biolims.common.close,
			handler : function() {
				showMainStorageItemAllSelectList.close();
			}
		} ]
	});
	showMainStorageItemAllSelectList.show();
}
});
