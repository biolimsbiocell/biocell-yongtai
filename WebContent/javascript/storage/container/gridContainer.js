var gridContainerGrid;
var colNum = $("#hid_3d_col_num").val();
$(function(){
	var cols={};
	cols.sm = false;
    var fields=[];
    fields.push({
		name: "key",
		type:"string"
	});
    for(var i=0;i<colNum;i++){
    	fields.push({
    		name: i+1,
    		type:"string"
    	});
    }
	    
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex: "key",
		hidden : false,
		header:"",
		width:5*4
	});
	for(var i=0;i<colNum;i++){
		cm.push({
			dataIndex: i+1,
			hidden : false,
			header:""+(i+1)+"",
			width:20*6,
			renderer:function (value, metadata, record, rowIndex, columnIndex, store) {
				var table = '<table  style="height:100%;width:200px;">';
				var tr_1 = ' <tr height=15px><td>'+value+'</td></tr>';
				var tableEnd='</table>';
				var displayText =  table + tr_1+ tableEnd;
				return displayText; 
			}
		
		});
	}
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/storage/container/showGridDataJson.action?id="+ $("#sp_id").val();
//	loadParam = null;
	var opts={};
	opts.tbar = [];
	opts.tbar.push({
		text : '导出条码',
		handler : exportexcel
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	
	gridContainerGrid=gridEditTable("gridContainerdiv",cols,loadParam,opts);
	$("#gridContainerdiv").data("gridContainerGrid", gridContainerGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
/*	var grid = gridContainerGrid;  
	gridContainerGrid.on('render', function(e) {   
	    var store = grid.getStore();  // Capture the Store.   
	  
	    var view = grid.getView();    // Capture the GridView.   
	  
	    gridContainerGrid.tip = new Ext.ToolTip({   
	        target: view.mainBody,    // The overall target element.   
	  
	        delegate: '.x-grid3-row', // Each grid row causes its own seperate show and hide.   
	  
	        trackMouse: true,         // Moving within the row should not hide the tip.   
	  
	        renderTo: document.body,  // Render immediately so that tip.body can be referenced prior to the first show.   
	  
	        listeners: {              // Change content dynamically depending on which element triggered the show.   
	  
	            beforeshow: function updateTipBody(tip) {   
	                var rowIndex = view.findRowIndex(tip.triggerElement);   
	                alert(rowIndex);
	                tip.body.dom.innerHTML = "Over Record ID " + store.getAt(rowIndex).get('2');   
	            }   
	        }   
	    });   
	}); */ 
	
	
//	var _grid = gridContainerGrid;
//    var _store = _grid.getStore();     
//    var view = _grid.getView();
//    _grid.on('mouseover',function(e){//添加mouseover事件
//		  var index = view.findRowIndex(e.getTarget());//根据mouse所在的target可以取到列的位置
//		  var cell= view.findCellIndex(e.getTarget());
//		  var str = _store.getAt(index).get(cell-1);
////		  alert(str);
//		  if(index!=false&&cell>1){//当取到了正确的列时，（因为如果传入的target列没有取到的时候会返回false）
//		   
//		    _grid.tip = new Ext.ToolTip ({     
//	            target:view.mainBody,     
//	            delegate:'.x-grid3-row',     
//	            trackMouse :true,     
//	            width:230,     
//	            autoHide:true,      
//	            title:String.fromCharCode(65+index)+(cell-1)+"位置信息",    //显示框标题   
//	            dismissDelay:5000,  //默认5秒后提示框消失     
//	            frame:true,     
//	            renderTo:document.body,     
//	            bodyStyle:"color:red",//字体颜色     
//	            listeners:{  
////	            	index = view.findRowIndex(tip.triggerElement);
//	               /* beforeshow:function updateTipBody(tip){     
//	                        tip.body.dom.innerHTML=str;     //显示内容   
//	                }*/
//	            	
//	            	 beforeshow:function updateTipBody(tip){
//					    if(str&&(str!="<table><tr height=30px><td></td></tr><tr><td></td></tr></table>"||str=="")){     
//					    	tip.body.dom.innerHTML=str;     //显示内容    
//			            }else{  
//			            	index = false; 
//			            	tip.destroy();
//			            	e.stopEvent();
////			            	message("Out:"+String.fromCharCode(65+index)+(cell-1));
//			            	/*_grid.on('mouseout',function(e){
//			            		tip.dismissDelay=1;
//			            		tip=null;
//			                	_grid.unbind("mouseover"); 
//			                });*/
//			            	return ; 
//			            }   
//	            	 }
//	            }     
//	        });
//		    _grid.on('mouseout',function(e){
//		    	_grid.tip.destory();
//		    });
//
//		  }/*else{
//			  _grid.tip = null;
//		  }*/
//	}); 
   /* _grid.on('mouseout',function(e){
    	_grid.tip.destory();
    });*/
//    $(".TreeView1_0").mouseout(function(){
//        $(this).removeClass("moveInNode");
//    }); 
    /*var e1 = window.event || e; 

    if ( e1.stopPropagation ){ //如果提供了事件对象，则这是一个非IE浏览器 
    	e1.stopPropagation(); 
    }else{ 
    //兼容IE的方式来取消事件冒泡 
    window.event.cancelBubble = true; 
    } */
})

//鼠标划过时，grid表格中内容加亮加粗显示
/*function showGjToolTip(_grid){     
    var _store = _grid.getStore();     
    var view = _grid.getView();     
    _grid.tip = new Ext.ToolTip ({     
            target:view.mainBody,     
            delegate:'.x-grid3-row',     
            trackMouse :true,     
            width:230,     
            autoHide:true,      
            title:"详细信息",     
            dismissDelay:5000,  //默认5秒后提示框消失     
            frame:true,     
            renderTo:document.body,     
            bodyStyle:"color:red",//字体颜色     
            listeners:{     
                beforeshow:function updateTipBody(tip){     
                    var rowIndex = view.findRowIndex(tip.triggerElement); 
                    var colIndex = view.findCellIndex(tip.getTarget());
//                    var cell= grid.getView().findCellIndex(e.getTarget());
                    if(_store.getAt(rowIndex).get('1')==0){     
                        return false;     
                        tip.destroy();      
                    }else{  
                    	alert(colIndex);
                        tip.body.dom.innerHTML=_store.getAt(rowIndex).get("'"+colIndex+"'");     
                    }     
                }     
            }     
        })     
}   */



//显示转存输入框
function FrozenLocationFun(stri){
	$("#oldLoc").val(stri);
	 var win = Ext.getCmp('FrozenLocationFun');
	 if (win) {win.close();}
	 var ProjectFun= new Ext.Window({
	 id:'FrozenLocationFun',modal:true,title:biolims.sample.selectLocation,layout:'fit',width:600,height:580,closeAction:'close',
	 plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	 collapsible: true,maximizable: true,
	 items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
	 html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/storage/position/showStoragePositionTreeDialog.action?typeId="+$("#storage_storageType").val()+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
	 buttons: [
	 { text: biolims.common.close,
	  handler: function(){
	  ProjectFun.close(); }  }]  });     ProjectFun.show(); }

function setFrozenLocationFun(str){
	$("#newAdr").show();
	var ads = str.split(",");
	$("#newLoc").val(ads[0]);
	 var win = Ext.getCmp('FrozenLocationFun')
	 if(win){win.close();}
	 }		

/*function unloading(){
	var obj = document.getElementsByName("box");
	var str = [];
	for(k in obj){
		if(obj[k].checked){
			str.push(obj[k].value);
		}
			
	}
	$("#newAdr").show();
	
}*/
//执行转存操作
function chengeAdr(){
	var newAdr = $("#newLoc").val();
	var oldArr = $("#oldLoc").val();
	var classType = $("#storage_storageType").val();
	if(oldArr&&oldArr!=""){
		ajax("post", "/storage/container/changeLocation.action", {
			id : oldArr,
			type : "1",      //操作类型：材料入库（0）、材料主数据(1)
			classType : classType,
			newAdr : newAdr
			}, function(data) {
				if (data.success) {
					if(data.data){
						gridContainerGrid.getStore().commitChanges();
						gridContainerGrid.getStore().reload();
						message("转存成功。");
					}else{
						message("转存失败，请重试！");
					}
				} else {
					message(biolims.common.anErrorOccurred);
				}
			}, null);
	}
}
//导出
function exportexcel() {
	gridContainerGrid.title = biolims.common.exportList;
	var vExportContent = gridContainerGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
//全选
function selectAll(){
	var obj = document.getElementsByName("box"); 
	if(document.getElementById("selAll").checked == false){ 
		  for(var i=0; i<obj.length; i++){ 
		    obj[i].checked=false; 
		  } 
	  }else{ 
		  for(var i=0; i<obj.length; i++){	  
			    obj[i].checked=true; 
		  }	
	 } 
}

//反选
function selectInverse(){
	var checkboxs=document.getElementsByName("box"); 
	for (var i=0;i<checkboxs.length;i++) { 
	  var e=checkboxs[i]; 
	  e.checked=!e.checked; 
	  setSelectAll(); 
	} 
}

//当选中所有的时候，全选按钮会勾上 
function setSelectAll(){ 
	var obj=document.getElementsByName("box"); 
	var count = obj.length; 
	var selectCount = 0; 
	
	for(var i = 0; i < count; i++){ 
		if(obj[i].checked == true){ 
			selectCount++;	
		} 
	} 
	if(count == selectCount){	
		document.all.selAll.checked = true; 
	}else{ 
		document.all.selAll.checked = false; 
	} 
} 
	   