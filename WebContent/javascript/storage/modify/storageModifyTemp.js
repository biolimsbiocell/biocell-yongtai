﻿//待维修设备
var storageModifyTempTab;

var instrumentRepair_id = $("#storageModify_id").text();
$(function() {
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : biolims.common.id,
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		},
	"visible":	false,
	});
	colOpts.push({
		"data" : "name",
		"title" : biolims.common.designation,
		"createdCell" : function(td) {
			$(td).attr("saveName", "name");
		}
	});
	colOpts.push({
		"data" : "kit-id",
		"title" : biolims.tStorage.kitId+"Id",
		"createdCell" : function(td) {
			$(td).attr("saveName", "kit-id");
		}
	});
	colOpts.push({
		"data": "kit-name",
		"title": biolims.tStorage.kitId,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "instrument-spec");
		},
	})
	
	colOpts.push({
		"data" : "spec",
		"title" : biolims.equipment.spec,
		"createdCell" : function(td) {
			$(td).attr("saveName", "spec");
		}
	})
	colOpts.push({
		"data" : "type-name",
		"title" : biolims.common.type,
		"createdCell" : function(td) {
			$(td).attr("saveName", "type-name");
		}
	})
	colOpts.push({
		"data" : "studyType-name",
		"title" : biolims.storage.subtypes,
		"createdCell" : function(td) {
			$(td).attr("saveName", "studyType-name");
		}
	})
	colOpts.push({
		"data" : "position-name",
		"title" : biolims.common.storageName,
		"createdCell" : function(td) {
			$(td).attr("saveName", "position-name");
		}
	})
	colOpts.push({
		"data" : "num",
		"title" : biolims.storage.num,
		"createdCell" : function(td) {
			$(td).attr("saveName", "num");
		}
	})
	colOpts.push({
		"data" : "unit-name",
		"title" : biolims.common.unit,
		"createdCell" : function(td) {
			$(td).attr("saveName", "unit-name");
		}
	})
	colOpts.push({
		"data" : "note",
		"title" : biolims.common.note,
		"createdCell" : function(td) {
			$(td).attr("saveName", "note");
		}
	})
	colOpts.push({
		"data" : "state-name",
		"title" : biolims.common.state,
		"createdCell" : function(td) {
			$(td).attr("saveName", "state-name");
		}
	})
	colOpts.push({
		"data" : "dutyUser-name",
		"title" : biolims.master.personName,
		"createdCell" : function(td) {
			$(td).attr("saveName", "dutyUser-name");
		}
	})
	colOpts.push({
		"data" : "useDesc",
		"title" : biolims.storage.useDesc,
		"createdCell" : function(td) {
			$(td).attr("saveName", "useDesc");
		}
	})
	colOpts.push({
		"data" : "searchCode",
		"title" : biolims.storage.searchCode,
		"createdCell" : function(td) {
			$(td).attr("saveName", "searchCode");
		}
	})
	colOpts.push({
		"data" : "source-name",
		"title" : biolims.sample.sourceName,
		"createdCell" : function(td) {
			$(td).attr("saveName", "source-name");
		}
	})
	colOpts.push({
		"data" : "barCode",
		"title" : biolims.common.barCode,
		"createdCell" : function(td) {
			$(td).attr("saveName", "barCode");
		}
	})
	colOpts.push({
		"data" : "ifCall",
		"title" : biolims.storage.ifCall,
		"createdCell" : function(td) {
			$(td).attr("saveName", "ifCall");
		}
	})
	colOpts.push({
		"data" : "i5",
		"title" : "i5",
		"createdCell" : function(td) {
			$(td).attr("saveName", "i5");
		}
	})
	colOpts.push({
		"data" : "i7",
		"title" : "i7",
		"createdCell" : function(td) {
			$(td).attr("saveName", "i7");
		}
	})
	colOpts.push({
		"data" : "currentIndex",
		"title" : biolims.tStorage.currentIndex,
		"createdCell" : function(td) {
			$(td).attr("saveName", "currentIndex");
		}
	})
	
	var tbarOpts = [];
	tbarOpts.push({
		text: '<i class="fa fa-ioxhost"></i>'+biolims.common.search,
		action:function(){
			search();
		}
	});
	if(1!=$("#storageModifyState").val()){
	tbarOpts.push({
		text : biolims.common.batch,
		action : function() {
			var id= "";
			$(".selected").each(function(i,v){
				id += $(v).find("input").val()+",";
			})
			top.layer.open({
				title: biolims.common.batch,
				type: 2,
				area: ["650px", "400px"],
				btn: biolims.common.selected,
				content: [window.ctx + "/storage/selStorageBatch.action?id="+id, ''],
				yes: function(index, layer) {
					var rows=$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addStorageBatchTable .selected");
					var batchIds="";
					$.each(rows, function(j, k) {
						batchIds+=($(k).children("td").eq(2).text())+",";
					});
					var sampleId = [];
					$("#storageModifyTempdiv .selected").each(function(i, val) {
						sampleId.push($(val).children("td").eq(0).find("input").val());
					});

					if(0>=sampleId.length){
						top.layer.msg(biolims.common.pleaseSelect);
						return;
					}
//					console.log(batchIds);
					top.layer.close(index);
					// 先保存主表信息
					var id = $("#storageModify_id").text();
					var state = $("#storageModify_state").text();
					var createDate = $("#storageModify_handleDate").text();
					var createUser = $("#storageModify_createUser").text();
					var type = $("#storageModify_type_id").val();
					var note = $("#storageModify_note").val();
					var confirmUser = $("#storageModify_confirmUser_id").val();
					var endDate = $("#storageModify_endDate").val();
					
					//添加数据到出库明细表
								$.ajax({
								type : 'post',
								url : '/storage/modify/addStorageModifyItem.action',
								data : {
									batchIds : batchIds,
									ids : sampleId,
									name : name,
									state : state,
									id : id,
									createDate : createDate,
									createUser : createUser,
									type : type,
									note : note,
									confirmUser : confirmUser,
									endDate : endDate
									
								},
								success : function(data) {
									var data = JSON.parse(data)
									if (data.success) {
										$("#storageModify_id").text(data.data);
										$("#storageModifyId").val(data.data);
										var param = {
												id: data.data
											};	
										var storageModifyItemTabs = $("#storageModifyItemdiv")
											.DataTable();
										storageModifyItemTabs.settings()[0].ajax.data = param;
										storageModifyItemTabs.ajax.reload();
									} else {
										top.layer.msg(biolims.common.addToDetailFailed);
									}
									;
								}
							});
					
				},
			})
			
			
			

		}
	})
}	

	var storageModifyTempOps = table(true, instrumentRepair_id,
			"/storage/modify/showStorageModifyTempTableJson.action", colOpts, tbarOpts);
	storageModifyTempTab = renderData($("#storageModifyTempdiv"), storageModifyTempOps);

	// 选择数据并提示
	storageModifyTempTab.on('draw', function() {
		var index = 0;
		$("#storageModifyTempdiv .icheck").on('ifChanged', function(event) {
			if ($(this).is(':checked')) {
				index++;
			} else {
				index--;
			}
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
	});
	bpmTask($("#bpmTaskId").val());
});



//大保存
function saveinstrumentRepairAndItem() {
		var changeLog = "";
		$('input[class="form-control"]').each(function(i, v) {
			var valnew = $(v).val();
			var val = $(v).attr("changelog");
			if(val !== valnew) {
				changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
			}
		});
		console.log($("#form1"));
		var jsonStr = JSON.stringify($("#form1").serializeObject());

		var dataItemJson = saveItemjson($("#storageModifyItemdiv"));
		var ele = $("#storageModifyTempdiv");
		var changeLogItem = "主数据入库明细：";
		changeLogItem = getChangeLog(dataItemJson, ele, changeLogItem);
		var id = $("#storageModify_id").text();
		$.ajax({
			url: ctx + '/storage/storageIn/saveStorageInAndItem.action',
			dataType: 'json',
			type: 'post',
			data: {
				dataValue: jsonStr,
				changeLog: changeLog,
				ImteJson: dataItemJson,
				bpmTaskId: $("#bpmTaskId").val(),
				changeLogItem: changeLogItem,
				id:id,
			},
			success: function(data) {
				if(data.success) {
					var url = "/equipment/repair/editInstrumentRepairNew.action?id=" + data.id;

					url = url + "&bpmTaskId=" + $("#bpmTaskId").val() + "&p_type=" + $("#p_type").val();

					window.location.href = url;
				} else {
					top.layer.msg(data.msg);

				}
			}

		});

}

$.fn.serializeObject = function()
{
   var o = {};
   var a = this.serializeArray();
   $.each(a, function() {
       if (o[this.name]) {
           if (!o[this.name].push) {
               o[this.name] = [o[this.name]];
           }
           o[this.name].push(this.value || '');
       } else {
           o[this.name] = this.value || '';
       }
   });
   return o;
};
function tjsp() {
	top.layer
			.confirm(
					biolims.common.pleaseConfirmSaveBeforeSubmit,
					{
						icon : 3,
						title : biolims.common.prompt
					},
					function(index) {
						top.layer
								.open({
									title : biolims.common.approvalTask,
									type : 2,
									anim : 2,
									area : [ '800px', '500px' ],
									btn : biolims.common.selected,
									content : window.ctx
											+ "/workflow/processinstance/toStartView.action?formName=DnaTask",
									yes : function(index, layer) {
										var datas = {
											userId : userId,
											userName : userName,
											formId : instrumentRepair_id,
											title : $("#storageModify_name").val(),
											formName : "storageModify"
										}
										ajax(
												"post",
												"/workflow/processinstance/start.action",
												datas,
												function(data) {
													if (data.success) {
														top.layer
																.msg(biolims.common.submitSuccess);
														if (typeof callback == 'function') {
															callback(data);
														}
														// dialogWin.dialog("close");
													} else {
														top.layer
																.msg(biolims.common.submitFail);
													}
												}, null);
										top.layer.close(index);
									},
									cancel : function(index, layer) {
										top.layer.close(index)
									}

								});
						top.layer.close(index);
					});

}
function sp() {
	var taskId = $("#bpmTaskId").val();
	var formId = instrumentRepair_id;

	top.layer.open({
		title : biolims.common.detailedExaminationAndApproval,
		type : 2,
		anim : 2,
		area : [ '800px', '500px' ],
		btn : biolims.common.selected,
		content : window.ctx
				+ "/workflow/processinstance/toCompleteTaskView.action?taskId="
				+ taskId + "&formId=" + formId,
		yes : function(index, layer) {
			var operVal = $('.layui-layer-iframe', parent.document).find("iframe").contents()
					.find("#oper").val();
			var opinionVal = $('.layui-layer-iframe', parent.document).find("iframe").contents()
					.find("#opinionVal").val();

			if (!operVal) {
				top.layer.msg(biolims.common.pleaseSelectOper);
				return false;
			}
			if (operVal == "2") {
				_trunTodoTask(taskId, callback, dialogWin);
			} else {
				var paramData = {};
				paramData.oper = operVal;
				paramData.info = opinionVal;

				var reqData = {
					data : JSON.stringify(paramData),
					formId : formId,
					taskId : taskId,
					userId : window.userId
				}
				ajax("post", "/workflow/processinstance/completeTask.action",
						reqData, function(data) {
							if (data.success) {
								top.layer.msg(biolims.common.submitSuccess);
								if (typeof callback == 'function') {
								}
							} else {
								top.layer.msg(biolims.common.submitFail);
							}
						}, null);
			}
			location.href = window.ctx + "/lims/pages/dashboard/dashboard.jsp";
		},
		cancel : function(index, layer) {
			top.layer.close(index)
		}

	});
}
//弹框模糊查询参数
function searchOptions() {
	return [{
		"txt" : biolims.common.id,
		"type" : "input",
		"searchName" : "id",
	},{
		"txt" : biolims.common.designation,
		"type" : "input",
		"searchName" : "name",
	}, {
		"type" : "table",
		"table" : storageModifyTempTab
	} ];
}