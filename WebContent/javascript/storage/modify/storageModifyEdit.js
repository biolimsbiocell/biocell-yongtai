$(function() {
	$("#storageModify_endDate").datepicker({
		language : "zh-TW",
		autoclose : true, // 选中之后自动隐藏日期选择框
		format : "yyyy-mm-dd" // 日期格式，详见
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "view") {
		settextreadonly();
	}
	var mainFileInput = fileInput('1', 'storageModify', $("#storageModify_id")
			.val());
	fieldCustomFun();
	
	$("#form1 :input").change(function(){ 
	     $("#form1").data("changed",true);   
	  });

})
function change(id) {
	$("#" + id).css({
		"background-color" : "white",
		"color" : "black"
	});
}
function add() {
	window.location = window.ctx + "/storage/modify/editStorageModify.action";
}

function list() {
	window.location = window.ctx
			+ '/storage/modify/showStorageModifyList.action';
}
function newSave() {
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});
function tjsp() {
	if($("#form1").data("changed") ||$("#storageModify_id").text()=="NEW"){
	      top.layer.msg("请保存后提交!");
	      return false;
	     }
	top.layer
			.confirm(
					biolims.common.pleaseConfirmSaveBeforeSubmit,
					{
						icon : 3,
						title : biolims.common.prompt,
						btn : biolims.common.selected
					},
					function(index) {
						top.layer
								.open({
									title : biolims.common.submit,
									type : 2,
									anim : 2,
									area : [ '800px', '500px' ],
									btn : biolims.common.selected,
									content : window.ctx
											+ "/workflow/processinstance/toStartView.action?formName=StorageModify",
									yes : function(index, layer) {
										var datas = {
											userId : userId,
											userName : userName,
											formId : $("#storageModify_id")
													.val(),
											title : $("#storageModify_name")
													.val(),
											formName : 'StorageModify'
										}
										ajax(
												"post",
												"/workflow/processinstance/start.action",
												datas,
												function(data) {
													if (data.success) {
														top.layer
																.msg(biolims.common.submitSuccess);
														if (typeof callback == 'function') {
															callback(data);
														}
														dialogWin
																.dialog("close");
													} else {
														top.layer
																.msg(biolims.common.submitFail);
													}
												}, null);
										top.layer.close(index);
									},
									cancel : function(index, layer) {
										top.layer.close(index)
									}

								});
						top.layer.close(index);
					});
}
function sp() {

	var taskId = $("#bpmTaskId").val();
	var formId = $("#storageModify_id").val();

	top.layer.open({
		title : biolims.common.handle,
		type : 2,
		anim : 2,
		area : [ '800px', '500px' ],
		btn : biolims.common.selected,
		content : window.ctx
				+ "/workflow/processinstance/toCompleteTaskView.action?taskId="
				+ taskId + "&formId=" + formId,
		yes : function(index, layer) {
			var operVal = $('.layui-layer-iframe', parent.document).find(
					"iframe").contents().find("#oper").val();
			var opinionVal = $('.layui-layer-iframe', parent.document).find(
					"iframe").contents().find("#opinionVal").val();
			var opinion = $('.layui-layer-iframe', parent.document).find(
					"iframe").contents().find("#opinion").val();
			if (!operVal) {
				top.layer.msg(biolims.common.pleaseSelectOper);
				return false;
			}
			if (operVal == "2") {
				_trunTodoTask(taskId, callback, dialogWin);
			} else {
				var paramData = {};
				paramData.oper = operVal;
				paramData.info = opinion;

				var reqData = {
					data : JSON.stringify(paramData),
					formId : formId,
					taskId : taskId,
					userId : window.userId
				}
				ajax("post", "/workflow/processinstance/completeTask.action",
						reqData, function(data) {
							if (data.success) {
								top.layer.msg(biolims.common.submitSuccess);
								if (typeof callback == 'function') {
								}
							} else {
								top.layer.msg(biolims.common.submitFail);
							}
						}, null);
			}
			window.open(window.ctx + "/main/toPortal.action", '_parent');
		},
		cancel : function(index, layer) {
			top.layer.close(index)
		}

	});
}

function ck() {
	top.layer
			.open({
				title : biolims.common.checkFlowChart,
				type : 2,
				anim : 2,
				area : [ '800px', '500px' ],
				btn : biolims.common.selected,
				content : window.ctx
						+ "/workflow/processinstance/toTraceProcessInstanceView.action?formId="
						+ $("#storageModify_id").val(),
				yes : function(index, layer) {
					top.layer.close(index)
				},
				cancel : function(index, layer) {
					top.layer.close(index)
				}
			});
}

/*
 * function save() { if(checkSubmit()==true){
 * document.getElementById('storageModifyItemJson').value =
 * saveStorageModifyItemjson($("#storageModifyItemTable"));
 * console.log($("#storageModifyItemJson").val()); form1.action = window.ctx +
 * "/storage/modify/save.action?loadNumber=1"; form1.submit(); } }
 */

// 必填字段验证
function mustAddField1() {
	
	var trs = $("#storageModifyItemdiv").find("tbody").children("tr");
	var flag = true;
	trs.each(function(i, val) {

		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for (var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			json[k] = $(tds[j]).text();
			if (k == "num") {

				var a = $(tds[j]).text();
				var b = Number(a);
				var z = /^[0-9]*$/;
				var x = /^\-[1-9][0-9]*$/;
//				var c = /^\+?[1-9][0-9]*$/;
				if (z.test(a)||x.test(a)) {
					if (a == null || a == "") {
						top.layer.msg("请确认是否输入数量");
						flag = false;
					} else{
						
						if (b < 0) {
							
							// return true;
							top.layer.msg("请输入正数");
							flag = false;
						} 
					}
				}

			}

		}
	});
	return flag;
}

function save() {
	if (checkSubmit() == true) {

		// 自定义字段
		// 拼自定义字段（实验记录）
		var inputs = $("#fieldItemDiv input");
		var options = $("#fieldItemDiv option");
		var contentData = {};
		var checkboxArr = [];
		// 必填验证
		var requiredField = requiredFilter();
		if (!requiredField) {
			return false;
		}

		if (!mustAddField1()) {
			return false;
		}

		$("#fieldItemDiv .checkboxs").each(function(i, v) {
			$(v).find("input").each(function(ii, inp) {
				var k = inp.name;
				if (inp.checked == true) {
					checkboxArr.push(inp.value);
					contentData[k] = checkboxArr;
				}
			});
		});
		inputs.each(function(i, inp) {
			var k = inp.name;
			if (inp.type != "checkbox") {
				contentData[k] = inp.value;
			}
		});
		options.each(function(i, opt) {
			if (opt.selected == true) {
				var k = opt.getAttribute("name");
				contentData[k] = opt.value;
			}
		});
		document.getElementById("fieldContent").value = JSON
				.stringify(contentData);
		var ele = $("#storageModifyItemdiv");
		var data = saveItemjson(ele);
		var changeLog = "仓库调整:";
		$('input[class="form-control"]').each(
				function(i, v) {
					var valnew = $(v).val();
					var val = $(v).attr("changelog");
					if (val !== valnew) {
						changeLog += $(v).prev("span").text() + ':由"' + val
								+ '"变为"' + valnew + '";';
					}
				});
		var changeLogs = "";
		if (changeLog != "仓库调整") {
			changeLogs = changeLog;
		}

		var dataItemJson = saveItemjson($("#storageModifyItemdiv"));
		var ele = $("#storageModifyItemdiv");
		var changeLogItem = "仓库调整明细:";
		changeLogItem = getChangeLog(dataItemJson, ele, changeLogItem);
		var changeLogItems = "";
		if (changeLogItem != "仓库调整明细:") {
			changeLogItems = changeLogItem;
		}

		var jsonStr = JSON.stringify($("#form1").serializeObject());
		top.layer.load(4, {
			shade : 0.3
		});
		$.ajax({
			url : ctx + '/storage/modify/save.action',
			dataType : 'json',
			type : 'post',
			data : {
				dataValue : jsonStr,
				changeLog : changeLogs,
				changeLogItem : changeLogItems,
				storageModifyItemJson : data,

				bpmTaskId : $("#bpmTaskId").val()
			},
			success : function(data) {
				// 关闭加载层
				// top.layer.close(index);

				if (data.success) {
					top.layer.closeAll();
					var url = "/storage/modify/editStorageModify.action?id="
							+ data.id;
					if (data.bpmTaskId) {
						url = url + "&bpmTaskId=" + data.bpmTaskId;
					}
					window.location.href = url;
				} else {
					top.layer.closeAll();
					top.layer.msg(biolims.common.saveFailed);
				}
			}
		});
	}

}

$.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name]) {
			if (!o[this.name].push) {
				o[this.name] = [ o[this.name] ];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};

function editCopy() {
	window.location = window.ctx
			+ '/storage/modify/copyStorageModify.action?id='
			+ $("#storageModify_id").val();
}
function changeState() {
	var paraStr = "formId=" + $("#storageModify_id").text()
			+ "&tableId=StorageModify";
	top.layer.open({
		title : biolims.common.changeState,
		type : 2,
		anim : 2,
		area : [ '400px', '400px' ],
		btn : biolims.common.selected,
		content : window.ctx
				+ "/applicationTypeAction/applicationTypeActionLook.action?"
				+ paraStr + "&flag=changeState'",
		yes : function(index, layer) {
			top.layer.confirm(biolims.common.toSubmit, {
				icon : 3,
				title : biolims.common.prompt,
				btn : biolims.common.selected
			}, function(index) {
				ajax("post", "/applicationTypeAction/exeFun.action", {
					applicationTypeActionId : $('.layui-layer-iframe',
							parent.document).find("iframe").contents().find(
							"input:checked").val(),
					formId : $("#storageModify_id").text()
				}, function(response) {
					var respText = response.message;
					if (respText == '') {
						window.location.reload();
					} else {
						top.layer.msg(respText);
					}
				}, null)
				top.layer.closeAll();
			})
		}

	});
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
		top.layer.msg(mess);
		return false;
	}
	return true;
}
function fileUp() {
	$("#uploadFile").modal("show");
}
function fileView() {
	top.layer
			.open({
				title : biolims.common.attachment,
				type : 2,
				skin : 'layui-top.layer-lan',
				area : [ document.body.clientWidth - 300,
						document.body.clientHeight - 100 ],
				content : window.ctx
						+ "/operfile/initFileList.action?flag=1&modelType=storageModify&id="
						+ $("#storageModify_id").val(),
				cancel : function(index, layer) {
					top.layer.close(index)
				}
			})
}
function settextreadonly() {
	jQuery(":text, textarea").each(
			function() {
				var _vId = jQuery(this).attr('id');
				jQuery(this).css("background-color", "#B4BAB5").attr(
						"readonly", "readOnly");
				if (_vId == 'actiondropdown_textbox')
					settextread(_vId);
			});
}
function settextread(name) {
	document.getElementById(name).className = "input_parts text input default  readonlyfalse false";
	document.getElementById(name).readOnly = false;
}

function showtype() {
	top.layer.open({
		title : biolims.common.pleaseChoose,
		type : 2,
		area : [ document.body.clientWidth - 300,
				document.body.clientHeight - 100 ],
		btn : biolims.common.selected,
		content : [
				window.ctx + "/dic/type/dicTypeSelectTable.action?flag=whlx",
				'' ],
		yes : function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#storageTypeTable .chosed")
					.children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#storageTypeTable .chosed")
					.children("td").eq(0).text();
			top.layer.close(index)
			$("#storageModify_type_id").val(id)
			$("#storageModify_type_name").val(name)
		},
	})
}

function showconfirmUser() {
	$("#form1").data("changed",true);  
	top.layer.open({
		title : biolims.common.pleaseChoose,
		type : 2,
		area : [ document.body.clientWidth - 300,
				document.body.clientHeight - 100 ],
		btn : biolims.common.selected,
		content : [
				window.ctx + "/core/user/selectUserTable.action?groupId=admin",
				'' ],
		yes : function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#addUserTable .chosed").children("td")
					.eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#addUserTable .chosed").children("td")
					.eq(0).text();
			top.layer.close(index)
			$("#storageModify_confirmUser_id").val(id)
			$("#storageModify_confirmUser_name").val(name)
		},
	})
}
// 自定义模块
function fieldCustomFun() {

	// 获取自定义字段值的相关内容
	var fieldContent = $("#fieldContent").val();

	// 查找自定义列表的订单模块的此检测项目的相关内容
	$
			.ajax({
				type : "post",
				url : window.ctx
						+ "/system/customfields/findFieldByModuleValue.action",
				data : {
					moduleValue : "StorageModify"
				},
				async : false,
				success : function(data) {
					var objValue = JSON.parse(data);
					if (objValue.success) {
						$
								.each(
										objValue.data,
										function(i, n) {
											var inputs = '';
											var disabled = n.readOnly ? ' '
													: "disabled";
											var defaultValue = n.defaultValue ? n.defaultValue
													: ' ';
											if (n.fieldType == "checkbox") {
												var checkboxs = '';
												var singleOptionIdArry = n.singleOptionId
														.split("/");
												var singleOptionNameArry = n.singleOptionName
														.split("/");
												singleOptionIdArry
														.forEach(function(vv,
																jj) {
															checkboxs += '<input type="checkbox" lay-ignore name='
																	+ n.fieldName
																	+ ' value='
																	+ jj
																	+ ' title='
																	+ singleOptionNameArry[vv]
																	+ '>'
																	+ singleOptionNameArry[vv]
																	+ '';
														});
												inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group checkboxs " changelog=""><span class="input-group-addon">'
														+ n.label
														+ '</span>'
														+ checkboxs
														+ '</div></div>';
											} else if (n.fieldType == "radio") {
												var options = '';
												var singleOptionIdArry = n.singleOptionId
														.split("/");
												var singleOptionNameArry = n.singleOptionName
														.split("/");
												singleOptionIdArry
														.forEach(function(vv,
																jj) {
															options += '<option name='
																	+ n.fieldName
																	+ ' value='
																	+ jj
																	+ '>'
																	+ singleOptionNameArry[vv]
																	+ '</option>';
														});
												inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">'
														+ n.label
														+ '</span><select class="form-control">'
														+ options
														+ '</select></div></div>';
											} else if (n.fieldType == "date") {
												inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">'
														+ n.label
														+ '</span><input type="text" name='
														+ n.fieldName
														+ ' required='
														+ n.isRequired
														+ ' '
														+ disabled
														+ ' class="form-control datepick" value='
														+ defaultValue
														+ '></div></div>';
											} else {
												inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">'
														+ n.label
														+ '</span><input type='
														+ n.fieldType
														+ ' name='
														+ n.fieldName
														+ ' required='
														+ n.isRequired
														+ '   class="form-control" '
														+ disabled
														+ ' value='
														+ defaultValue
														+ '></div></div>';
											}

											$("#fieldItemDiv").append(inputs);
										});

					} else {
						top.layer.msg(biolims.customList.updateFailed);
					}
				}
			});

	// 显示自定义字段的数据
	if (fieldContent != null && fieldContent != undefined && fieldContent != "") {
		if (fieldContent && fieldContent != "null") {
			var contentData = JSON.parse(fieldContent);
			for ( var k in contentData) {
				$("#fieldItemDiv input").each(function(i, inp) {
					if (inp.name == k) {
						if (inp.type == "checkbox") {
							if (contentData[k].indexOf(inp.value) != -1) {
								inp.setAttribute("checked", true);
							}
						} else {
							inp.value = contentData[k];
						}
					}
				});
				$("#fieldItemDiv option").each(function(i, val) {
					if (k == val.getAttribute("name")) {
						if (val.value == contentData[k]) {
							val.setAttribute("selected", true);
						}
					}
				});
			}
			;
		}
	}

	// 日期格式化
	$("#fieldItemDiv").find('.datepick').datepicker({
		language : "zh-TW",
		autoclose : true, // 选中之后自动隐藏日期选择框
		format : "yyyy-mm-dd" // 日期格式，详见
	});
	// 多选框 格式化
	$("#fieldItemDiv").find('input[type="checkbox"]').iCheck({
		checkboxClass : 'icheckbox_square-blue',
		increaseArea : '20%' // optional
	});

}
