var storageModifyTable;
var oldstorageModifyChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表

	var tbarOpts = [];
	var colOpts = [];
	   colOpts.push({
		"data":"id",
		"title": biolims.tStorageModify.id,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
		"className": "edit"
	});
	colOpts.push( {
		"data": "type-id",
		"title": biolims.storage.storageTypeId,
		"createdCell": function(td) {
			$(td).attr("saveName", "type-id");
		}
	});
	colOpts.push( {
		"data": "type-name",
		"title": biolims.tStorageModify.type,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "type-name");
			$(td).attr("type-id", rowData['type-id']);
		}
	});
	colOpts.push( {
		"data": "handleUser-id",
		"title": biolims.tStorageModify.handleUser+"ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "handleUser-id");
		}
	});
	colOpts.push( {
		"data": "handleUser-name",
		"title": biolims.tStorageModify.handleUser,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "handleUser-name");
			$(td).attr("handleUser-id", rowData['handleUser-id']);
		}
	});
	   colOpts.push({
		"data":"handleDate",
		"title": biolims.tStorageModify.handleDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "handleDate");
	    },
		"className": "date"
	});
	   colOpts.push({
		"data":"endDate",
		"title": biolims.tStorageModify.endDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "endDate");
	    },
		"className": "date"
	});
	   colOpts.push({
		"data":"stateName",
		"title": biolims.tStorageModify.stateName,
		"createdCell": function(td) {
			$(td).attr("saveName", "stateName");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"state",
		"title": biolims.tStorageModify.state,
		"createdCell": function(td) {
			$(td).attr("saveName", "state");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"note",
		"title": biolims.tStorageModify.note,
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
	    },
		"visible": false,	
		"className": "textarea"
	});
	colOpts.push( {
		"data": "confirmUser-id",
		"title": biolims.sampleOut.acceptUserId,
		"createdCell": function(td) {
			$(td).attr("saveName", "confirmUser-id");
		}
	});
	colOpts.push( {
		"data": "confirmUser-name",
		"title": biolims.tStorageModify.confirmUser,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "confirmUser-name");
			$(td).attr("confirmUser-id", rowData['confirmUser-id']);
		}
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#storageModifyTable"))
	}
	});
	tbarOpts.push({
		text: biolims.common.addwindow,
		action: function() {
			addItemLayer($("#storageModifyTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.editwindow,
		action: function() {
			editItemLayer($("#storageModifyTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.batchUpload,
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#storageModify_id").val(),
						fileId: data.response.fileId
					},
					url: ctx + "/storage/modify/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							 storageModifyTable.ajax.reload();
						} else {
							top.layer.msg(biolims.common.uploadFailed)
						}
		
					}
				});
			});
			
		}
		});
	
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveStorageModify($("#storageModifyTable"));
		}
	});
	}
	
	var storageModifyOptions = 
	table(true, "","/storage/modify/showStorageModifyTableJson.action",
	 colOpts, tbarOpts)
	
	
	
	storageModifyTable = renderData($("#storageModifyTable"), storageModifyOptions);
	storageModifyTable.on('draw', function() {
		oldstorageModifyChangeLog = storageModifyTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});

// 保存
function saveStorageModify(ele) {
	var data = saveStorageModifyjson(ele);
	var ele=$("#storageModifyTable");
	var changeLog = "实验结果：";
	changeLog = getChangeLog(data, ele, changeLog);
	$.ajax({
		type: 'post',
		url: '/storage/modify/saveStorageModifyTable.action',
		data: {
			dataJson: data,
			changeLog:changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveStorageModifyjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			
			if(k == "type-name") {
				json["type-id"] = $(tds[j]).attr("type-id");
				continue;
			}
			
			if(k == "handleUser-name") {
				json["handleUser-id"] = $(tds[j]).attr("handleUser-id");
				continue;
			}
			
			if(k == "confirmUser-name") {
				json["confirmUser-id"] = $(tds[j]).attr("confirmUser-id");
				continue;
			}
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		oldstorageModifyChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
