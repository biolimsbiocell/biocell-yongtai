	var storageModifyItemTab, oldChangeLog;
hideLeftDiv();
//设备维修明细表
$(function() {
	var colOpts = [];
	  colOpts.push({
			"data":"id",
			"visible":false,
			"title": biolims.tStorageModifyItem.id,
			"createdCell": function(td) {
				$(td).attr("saveName", "id");
		    },
		});
		colOpts.push( {
			"data": "storageModify-id",
			"title": biolims.tStorageModifyItem.storageModify+"ID",
			"createdCell": function(td) {
				$(td).attr("saveName", "storageModify-id");
			}
		});
		colOpts.push( {
			"data": "storage-id",
			"title": biolims.common.adjustFor,
			"createdCell": function(td) {
				$(td).attr("saveName", "storage-id");
			}
		});
		colOpts.push( {
			"data": "storage-name",
			"title": biolims.tStorageModifyItem.storage,
			"createdCell": function(td) {
				$(td).attr("saveName", "storage-name");
			}
		});
		colOpts.push( {
			"data": "storage-searchCode",
			"title": 'Component Cat No.',
			"createdCell": function(td) {
				$(td).attr("saveName", "storage-searchCode");
			},
			visible:false
		});
		
		colOpts.push( {
			"data": "storage-type-id",
			"title": biolims.common.typeID,
			"createdCell": function(td) {
				$(td).attr("saveName", "storage-type-id");
			}
		});
		colOpts.push( {
			"data": "storage-type-name",
			"title": biolims.common.type,
			"createdCell": function(td) {
				$(td).attr("saveName", "storage-type-name");
			}
		});
		colOpts.push( {
			"data": "storage-kit-id",
			"title": biolims.tStorage.kitId+"Id",
			"createdCell": function(td) {
				$(td).attr("saveName", "storage-kit-id");
			}
		});
		colOpts.push( {
			"data": "storage-kit-name",
			"title": biolims.common.kitName,
			"createdCell": function(td) {
				$(td).attr("saveName", "storage-kit-name");
			}
		});
		colOpts.push( {
			"data": "storage-unit-name",
			"title": biolims.tStorage.dicUnitId,
			"createdCell": function(td) {
				$(td).attr("saveName", "storage-unit-name");
			}
		});
		
		   colOpts.push({
			"data":"serial",
			"title": "产品批号",
			"createdCell": function(td) {
				$(td).attr("saveName", "serial");
		    },
		});
		   colOpts.push({
			"data":"code",
			"title": biolims.tStorageModifyItem.code,
			"createdCell": function(td) {
				$(td).attr("saveName", "code");
		    },
		});
		   colOpts.push({
			"data":"outPrice",
			"title": biolims.tStorageModifyItem.outPrice,
			"createdCell": function(td) {
				$(td).attr("saveName", "outPrice");
		    },
			"className": "edit"
		});
		   colOpts.push({
			"data":"num",
			"title": biolims.tStorageModifyItem.num,
			"createdCell": function(td) {
				$(td).attr("saveName", "num");
		    },
			"className": "edit"
		});
	var tbarOpts = [];
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
/*	tbarOpts.push({
		text: biolims.common.addwindow,
		action: function() {
			addItemLayer($("#storageModifyItemTable"))
		}
	});*/
/*	tbarOpts.push({
		text: biolims.common.editwindow,
		action: function() {
			editItemLayer($("#storageModifyItemdiv"))
		}
	});*/
	if(1!=$("#storageModifyState").val()){
		tbarOpts.push({
			text: biolims.common.delSelected,
			action: function() {
				removeChecked($("#storageModifyItemdiv"),
					"/storage/modify/delStorageModifyItem.action","删除入库明细数据：",$("#storageModify_id").text());
			}
		});
		
		tbarOpts.push({
			text: biolims.common.save,
			action: function() {
				saveinstrumentRepairTaskTab($("#storageModifyItemdiv"));
			}
		});
		tbarOpts.push({
			text : biolims.common.editwindow,
			action : function() {
				editItemLayer($("#storageModifyItemdiv"))
			}
		});
		
	}
	}

	var storageModifyItemOps = table(true, $("#storageModify_id").text(), "/storage/modify/showStorageModifyItemTableJson.action", colOpts, tbarOpts);
	storageModifyItemTab = renderData($("#storageModifyItemdiv"), storageModifyItemOps);
	//选择数据并提示
	storageModifyItemTab.on('draw', function() {
		var index = 0;
		$("#storageModifyItemdiv .icheck").on('ifChanged', function(event) {
			if($(this).is(':checked')) {
				index++;
				$("#plateModal").find(".mysample[sid='" + this.value + "']").css("border","2px solid #000");
			} else {
				var tt=$("#plateModal").find(".mysample[sid='" + this.value + "']").css("border","1px solid gainsboro");
				index--;
			}
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
		oldChangeLog = storageModifyItemTab.ajax.json();
		
	});
});

// 保存
function saveinstrumentRepairTaskTab() {
    var ele=$("#storageModifyItemdiv");
	var changeLog = "仓库调整明细:";
	var data = saveItemjson(ele);
	
	changeLog = getChangeLog(data, ele, changeLog);
	var changeLogs="";
	if(changeLog !="仓库调整明细:"){
		changeLogs=changeLog;
	}
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		type: 'post',
		url: '/storage/modify/saveStorageModifyItemTable.action',
		data: {
			id: $("#storageModify_id").text(),
			dataJson: data,
			logInfo: changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
	
	
}
// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 添加样本类型ID
//			if(k == "dicSampleTypeName") {
//				json["dicSampleTypeId"] = $(tds[j]).attr("dicSampleTypeId");
//				continue;
//			}
//			if(k == "num") {
//				$(tds[j]).text();
//				return false;
//			}
			json[k] = $(tds[j]).text();
		}
		json.color = $(val).attr("background");
		data.push(json);
	});
	return JSON.stringify(data);
}

/*
 * 
 * 创建者 : 郭恒开
 * 创建日期: 2018/03/08
 * 文件描述: 修改日志
 * 
 */
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		if (!id) {
			changeLog += '新增记录:';
			for ( var k in v) {
				var title = ele.find("th[savename=" + k + "]").text();
				changeLog += '"' + title + '"为"' + v[k] + '";';
			}
			return true;
		}
		changeLog += '编号为"' + v.id + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k] && k != "color") {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
