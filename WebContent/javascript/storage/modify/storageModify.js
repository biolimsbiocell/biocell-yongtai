var storageModifyTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":biolims.tStorageModify.id,
	});
	
//	    fields.push({
//		"data":"type-id",
//		"visible":false,
//		"title":biolims.storage.storageTypeId
//	});
//	    fields.push({
//		"data":"type-name",
//		"visible":false,
//		"title":biolims.tStorageModify.type
//	});
	
	    fields.push({
		"data":"handleUser-id",
		"title":biolims.tStorageModify.handleUser+"ID"
	});
	    fields.push({
		"data":"handleUser-name",
		"title":biolims.tStorageModify.handleUser
	});
	
	    fields.push({
		"data":"handleDate",
		"title":biolims.tStorageModify.handleDate,
		"render": function(data) {
				if(data) {
					return parent.moment(data).format('YYYY-MM-DD');
				} else {
					return "";
				}
			}
	});
	
	    fields.push({
		"data":"endDate",
		"title":biolims.tStorageModify.endDate,
		"render": function(data) {
				if(data) {
					return parent.moment(data).format('YYYY-MM-DD');
				} else {
					return "";
				}
			}
	});
	
	    fields.push({
		"data":"stateName",
		"title":biolims.tStorageModify.stateName,
	});
	
	    fields.push({
		"data":"state",
		"title":biolims.tStorageModify.state,
	});
	
	    fields.push({
		"data":"note",
		"title":biolims.tStorageModify.note,
	});
	
	    fields.push({
		"data":"confirmUser-id",
		"title":biolims.sampleOut.acceptUserId
	});
	    fields.push({
		"data":"confirmUser-name",
		"title":biolims.tStorageModify.confirmUser
	});
	

		$.ajax({
			type:"post",
			url:window.ctx + "/system/customfields/findFieldByModuleValue.action",
			async:false,
			data:{
				moduleValue : "StorageModify"
			},
			success:function(data){
				var objData = JSON.parse(data);
				if(objData.success){
					$.each(objData.data, function(i,n) {
						var str = {
							"data" : n.fieldName,
							"title" : n.label
						}
						colData.push(str);
					});
					
				}else{
					top.layer.msg(biolims.tInstrumentBorrowDetail.tableError);
				}
			}
		});

	var options = table(true, "","/storage/modify/showStorageModifyTableJson.action",
	 fields, null)
	storageModifyTable = renderRememberData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(storageModifyTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
		"/storage/modify/editStorageModify.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/storage/modify/editStorageModify.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/storage/modify/viewStorageModify.action?id=" + id;
}
//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":biolims.tStorageModify.id
		});
	
	fields.push({
	    "type":"input",
		"searchName":"type.name",
		"txt":biolims.tStorageModify.type
	});
	fields.push({
	    "type":"input",
		"searchName":"handleUser.id",
		"txt":biolims.tStorageModify.handleUser+"ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"handleUser.name",
		"txt":biolims.tStorageModify.handleUser
	});
	   fields.push({
		    "searchName":"handleDate",
			"type":"input",
			"txt":biolims.tStorageModify.handleDate
		});
	fields.push({
			"txt": biolims.tStorageModify.handleDate+"(Start)",
			"type": "dataTime",
			"searchName": "handleDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.tStorageModify.handleDate+"(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "handleDate##@@##2"
		});
	   fields.push({
		    "searchName":"endDate",
			"type":"input",
			"txt":biolims.tStorageModify.endDate
		});
	fields.push({
			"txt": biolims.tStorageModify.endDate+"(Start)",
			"type": "dataTime",
			"searchName": "endDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.tStorageModify.endDate+"(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "endDate##@@##2"
		});
	   fields.push({
		    "searchName":"stateName",
			"type":"input",
			"txt":biolims.tStorageModify.stateName
		});
	   fields.push({
		    "searchName":"state",
			"type":"input",
			"txt":biolims.tStorageModify.state
		});
	   fields.push({
		    "searchName":"note",
			"type":"input",
			"txt":biolims.tStorageModify.note
		});
	fields.push({
	    "type":"input",
		"searchName":"confirmUser.id",
		"txt":biolims.sampleOut.acceptUserId
	});
	fields.push({
	    "type":"input",
		"searchName":"confirmUser.name",
		"txt":biolims.tStorageModify.confirmUser
	});
	
	fields.push({
		"type":"table",
		"table":storageModifyTable
	});
	return fields;
}
