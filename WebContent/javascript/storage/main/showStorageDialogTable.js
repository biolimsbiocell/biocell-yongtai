$(function() {
	var searchModel = '<div class="input-group"><input type="text" class="form-control" placeholder="Search..." id="searchvalue" onkeydown="entersearch()"><span class="input-group-btn"> <button class="btn btn-info" type="button" onclick="searchModel()">按名称搜索</button></span> </div>';
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": "原辅料编号",
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "id");
			$(td).attr("unit", rowData["outUnitGroup-mark2-name"]);
		},
	});
	colOpts.push({
		"data": "outUnitGroup-mark2-name",
		"title": "单位",
		"visible":false,
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "outUnitGroup-mark2-name");
		},
	});
	colOpts.push({
		"data": "name",
		"title": "原辅料名称",
	});
	colOpts.push({
		"data": "spec",
		"title": "规格",
	});
	colOpts.push({
		"data": "note",
		"title": "备注",
	});
	var tbarOpts = [];
//	tbarOpts.push({
//		text:"搜索",
//		action: search,
//	});
	var options = table(false, null,
			'/storage/getStorageJson.action?type='+$("#studyType").val(),colOpts , tbarOpts)
		 storageTable = renderData($("#addStorage"), options);
	$("#addStorage").on(
	'init.dt',
	function(e, settings) {
		// 清除操作按钮
		$("#addStorage_wrapper .dt-buttons").html(searchModel)
//		$("#addStorage_wrapper .dt-buttons").empty();
		$('#addStorage_wrapper').css({
			"padding": "0 16px"
		});
	var trs = $("#addStorage tbody tr");
	storageTable.ajax.reload();
	storageTable.on('draw', function() {
		trs = $("#addStorage tbody tr");
		trs.click(function() {
			$(this).addClass("chosed").siblings("tr")
				.removeClass("chosed");
		});
	});
	trs.click(function() {
		$(this).addClass("chosed").siblings("tr")
			.removeClass("chosed");
	});
	});

})
//回车搜索
function entersearch(){  
    var event = window.event || arguments.callee.caller.arguments[0];  
    if (event.keyCode == 13)  
    {  
        searchModel();
    }  
} 
//搜索方法
function searchModel() {
	storageTable.settings()[0].ajax.data = {
		query: JSON.stringify({
			name:"%"+$.trim($("#searchvalue").val())+"%"
		})
	};
	storageTable.ajax.reload();
}

