var storageReagentGrid;
$(function() {
	var cols = {};
	var loadParam = {};
	var opts = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name: 'id',
		type: "string"
	});
	fields.push({
		name: 'code',
		type: 'string'
	});
	fields.push({
		name: 'serial',
		type: 'string'
	});
	fields.push({
		name: 'note',
		type: 'string'
	});
	fields.push({
		name: 'note2',
		type: 'string'
	});
	fields.push({
		name: 'productDate',
		type: 'date',
		dateFormat: 'Y-m-d'
	});
	fields.push({
		name: 'expireDate',
		type: 'date',
		dateFormat: 'Y-m-d'
	});
	fields.push({
		name: 'remindDate',
		type: 'date',
		dateFormat: 'Y-m-d'
	});
	fields.push({
		name: 'inDate',
		type: 'date',
		dateFormat: 'Y-m-d'
	});
	fields.push({
		name: 'position-id',
		type: 'string'
	});
	fields.push({
		name: 'position-name',
		type: 'string'
	});
	fields.push({
		name: 'purchasePrice',
		type: 'string'
	});
	fields.push({
		name: 'num',
		type: 'string'
	});
	fields.push({
		name: 'recationSum',
		type: 'string'
	});
	fields.push({
		name: 'qcState',
		type: 'string'
	});
	fields.push({
		name: 'qcPassDate',
		type: 'string'
	});
	fields.push({
		name: 'storage-unit-name',
		type: 'string'
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex: 'id',
		hidden: true,
		header: biolims.user.itemNo,
		width: 20 * 6,
	});
	cm.push({
		dataIndex: 'serial',
		hidden: false,
		header: biolims.storage.batchNo,
		width: 20 * 6,
	});
	cm.push({
		dataIndex: 'expireDate',
		header: biolims.common.expirationDate,
		width: 120,
		sortable: true,
		hidden: false,
		renderer: formatDate
	});
	cm.push({
		dataIndex: 'inDate',
		header: biolims.common.acceptDate,
		width: 120,
		sortable: true,
		hidden: false,
		editable: false,
		renderer: formatDate
	});
	cm.push({
		dataIndex: 'num',
		header: biolims.common.count,
		width: 60,
		sortable: true,
		hidden: false
	});
	cm.push({
		dataIndex: 'storage-unit-name',
		header: biolims.common.unit,
		width: 50,
		sortable: true,
		hidden: false
	});
	cm.push({
		dataIndex: 'position-name',
		header: biolims.common.storageName,
		width: 150,
		sortable: true,
		hidden: false,
	});
	cm.push({
		dataIndex: 'qcState',
		header: biolims.common.qcState,
		width: 60,
		sortable: true,
		hidden: false,
		editable: true,
		editor: qcCob
	});
	cm.push({
		dataIndex: 'productDate',
		header: biolims.storage.productDate,
		width: 120,
		sortable: true,
		hidden: true,
		renderer: formatDate
	});
	cm.push({
		dataIndex: 'remindDate',
		header: biolims.storage.remindDate,
		width: 120,
		sortable: true,
		hidden: false,
		editable: true,
		renderer: formatDate,
		editor: new Ext.form.DateField({
			format: 'Y-m-d'
		})
	});
	cm.push({
		dataIndex: 'position-id',
		header: '存储位置ID',
		width: 150,
		sortable: true,
		hidden: true,
	});
	cm.push({
		dataIndex: 'purchasePrice',
		header: '批次价格',
		width: 150,
		sortable: true,
		hidden: true
	});
	cm.push({
		dataIndex: 'recationSum',
		header: biolims.common.reactionNumber,
		width: 100,
		sortable: true,
		hidden: false
	});
	cm.push({
		dataIndex: 'note',
		hidden: false,
		header: 'sn',
		width: 200,
		editor: new Ext.form.TextField({
			allowBlank: true
		})
	});
	cm.push({
		dataIndex: 'note2',
		hidden: false,
		header: biolims.common.note,
		width: 200,
		editor: new Ext.form.TextField({
			allowBlank: true
		})
	});
	cols.cm = cm;
	loadParam.url = ctx + '/storage/storageReagentSerial.action?storageId=' + $("#storageId").val();
	opts.title = biolims.purchase.purchaseDetail;
	opts.height = document.body.clientHeight * 0.95;
	opts.tbar = [];
	opts.delSelect = function(ids) {
		ajax("post", "/storage/delStorageReagentBuySerial.action", {
			ids: ids
		}, function(data) {
			if(data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	opts.tbar.push({
		text: biolims.common.save,
		handler: function() {
			var record = storageReagentGrid.store.getModifiedRecords();
			if(!record || record == '' || record.length == 0) {
				return true;
			} else {
				var selData = '';
				for(var ij = 0; ij < record.length; ij++) {
					if(record[ij].get('id') != '-1000') {
						var array = record[ij].data;
						selData = selData + Ext.util.JSON.encode(array) + ',';
					}
				}
				if(selData.length > 0) {
					selData = selData.substring(0, selData.length - 1);
				}
				var data = '[' + selData + ']';
				data = rpLsp(data);
				var params = "data:'" + data + "'";
				modifyData(params);
			}
			storageReagentGrid.store.commitChanges();
			return true;
		}
	})
	opts.tbar.push({
		text: biolims.common.fillDetail,
		handler: null
	});
	opts.tbar.push({
		text: biolims.common.editableColAppear,
		handler: null
	});
	opts.tbar.push({
		text: biolims.common.uncheck,
		handler: null
	});
	storageReagentGrid = gridEditTable("storageReagentdiv", cols, loadParam, opts);
	$("#storageReagentdiv").data("storageReagentGrid", storageReagentGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
var comboboxStore = new Ext.data.JsonStore({
	id: 0,
	url: window.ctx + '/dic/type/dicTypeCobJson.action',
	root: 'results',
	baseParams: {
		type: 'jcjb'
	},
	autoLoad: true,
	fields: ['id', 'name']
});
comboboxStore.load();

var cob = new Ext.form.ComboBox({
	typeAhead: true,
	triggerAction: 'all',

	id: "nodeBandFormId",
	mode: 'remote',
	editable: false,
	valueField: 'id',
	displayField: 'name',

	store: comboboxStore

});
var storeisQcCob = new Ext.data.ArrayStore({
	fields: ['name'],
	data: [
		['Not QC'],
		['QC Pass'],
		['QC']
	]
});
var qcCob = new Ext.form.ComboBox({
	store: storeisQcCob,
	displayField: 'name',
	valueField: 'name',
	mode: 'local'
});
//var position = new Ext.form.TextField({
//	allowBlank : true
//});
//position.on('focus', function() {
//	showStoragePosition();
//});
//function showStoragePosition() {
//	var win = Ext.getCmp('showStoragePosition');
//	if (win) {
//		win.close();
//	}
//	var showStoragePosition = new Ext.Window(
//			{
//				id : 'showStoragePosition',
//				modal : true,
//				title : biolims.sample.selectLocation,
//				layout : 'fit',
//				width : document.body.clientWidth / 2,
//				height : document.body.clientHeight / 1.5,
//				closeAction : 'close',
//				plain : true,
//				bodyStyle : 'padding:5px;',
//				buttonAlign : 'center',
//				collapsible : true,
//				maximizable : true,
//				items : new Ext.BoxComponent(
//						{
//							id : 'maincontent',
//							region : 'center',
//							html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
//									+ window.ctx
//									+ "/storage/common/showStoragePositionTree.action?setStoragePostion=true&flag=showStoragePosition' frameborder='0' width='100%' height='100%' ></iframe>"
//						}),
//				buttons : [ {
//					text : biolims.common.close,
//					handler : function() {
//						showStoragePosition.close();
//					}
//				} ]
//			});
//	showStoragePosition.show();
//}
//function setshowStoragePosition(id, name, node) {
//
//	var record = gridGrid.getSelectionModel().getSelected();
//	record.set('position-id', id);
//	record.set('position-name', name);
//
//	var win = Ext.getCmp('showStoragePosition');
//	if (win) {
//		win.close();
//	}
//
//}