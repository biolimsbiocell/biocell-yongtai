$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "storage-id",
		"title": '原辅料编号',
		"createdCell": function(td) {
			$(td).attr("saveName", "storage-id");
		},
	});
	colOpts.push({
		"data": "id",
		"title": '该批次编号',
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		},
	});
	colOpts.push({
		"data": "serial",
		"title": biolims.tStorageReagentBuySerial.serial,
		"createdCell": function(td) {
			$(td).attr("saveName", "serial");
		},
	});
	colOpts.push({
		"data": "productDate",
		"title": biolims.tStorageReagentBuySerial.productDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "productDate");
		},
	});
	colOpts.push({
		"data": "expireDate",
		"title": biolims.tStorageReagentBuySerial.expireDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "expireDate");
		},
	});
	colOpts.push({
		"data": "remindDate",
		"title": biolims.tStorageReagentBuySerial.remindDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "remindDate");
		},
	});
	colOpts.push({
		"data": "inDate",
		"title": biolims.tStorageReagentBuySerial.inDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "inDate");
		},
	});
	colOpts.push({
		"data": "num",
		"title": biolims.tStorageReagentBuySerial.num,
		"createdCell": function(td) {
			$(td).attr("saveName", "num");
		},
	});
	
	colOpts.push({
		"data": "position-name",
		"title": biolims.tStorageReagentBuySerial.position,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "position-name");
			$(td).attr("position-id", rowData['positionr-id']);
		}
	});
	colOpts.push({
		"data": "note",
		"title": biolims.tStorageReagentBuySerial.note,
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
		},
	});
	colOpts.push({
		"data": "note2",
		"title": biolims.tStorageReagentBuySerial.note2,
		"createdCell": function(td) {
			$(td).attr("saveName", "note2");
		},
	});
	
	colOpts.push({
		"data": "recationSum",
		"title": biolims.tStorageReagentBuySerial.recationSum,
		"createdCell": function(td) {
			$(td).attr("saveName", "recationSum");
		},
	});
	
	var tbarOpts = [];
	var addStorageBatchOptions = table(true, null,
			'/storage/pleaseVerifyPlan/storageBatchJson1.action?id=' + $("#id").val(),
			colOpts, tbarOpts)
	var StorageBatchTable = renderData($("#addStorageBatchTable"), addStorageBatchOptions);
	$("#addStorageBatchTable").on(
			'init.dt',
			function(e, settings) {
				// 清除操作按钮
				$("#addStorageBatchTable_wrapper .dt-buttons").empty();
				$('#addStorageBatchTable_wrapper').css({
					"padding": "0 16px"
				});
			var trs = $("#addStorageBatchTable tbody tr");
			StorageBatchTable.ajax.reload();
			StorageBatchTable.on('draw', function() {
				trs = $("#addStorageBatchTable tbody tr");
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			});
			trs.click(function() {
				$(this).addClass("chosed").siblings("tr")
					.removeClass("chosed");
			});
			});
})

