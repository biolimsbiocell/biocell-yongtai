var storageReagentBuySerialTable;
var oldstorageReagentBuySerialChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表
	var id = $("#storage_id").val();
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.tStorageReagentBuySerial.id,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		},
		"visible": false,
		"className": "edit"
	});
	colOpts.push({
		"data": "serial",
		"title": biolims.tStorageReagentBuySerial.serial,
		"createdCell": function(td) {
			$(td).attr("saveName", "serial");
		},
		"className": "edit"
	});
	colOpts.push({
		"data": "productDate",
		"title": biolims.tStorageReagentBuySerial.productDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "productDate");
		},
		"visible": true,
		"className": "date"
	});
	colOpts.push({
		"data": "expireDate",
		"title": biolims.tStorageReagentBuySerial.expireDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "expireDate");
		},
		"visible": true,
		"className": "date"
	});
	colOpts.push({
		"data": "remindDate",
		"title": biolims.tStorageReagentBuySerial.remindDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "remindDate");
		},
		"visible": true,
		"className": "date"
	});
	colOpts.push({
		"data": "inDate",
		"title": biolims.tStorageReagentBuySerial.inDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "inDate");
		},
		"visible": true,
		"className": "date"
	});
	colOpts.push({
		"data": "num",
		"title": biolims.tStorageReagentBuySerial.num,
		"createdCell": function(td) {
			$(td).attr("saveName", "num");
		},
		"visible": true,
		"className": "edit"
	});
	
	colOpts.push({
		"data": "position-name",
		"visible":false,
		"title": biolims.tStorageReagentBuySerial.position,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "position-name");
			$(td).attr("position-id", rowData['positionr-id']);
		}
	});
	

	colOpts.push({
		"data": "storage-unitGroup-mark2-name",
		"visible":false,
		"title": biolims.tStorageReagentBuySerial.unit,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "storage-unitGroup-mark2-name");
			$(td).attr("storage-unitGroup-mark2-id", rowData['storage-unitGroup-mark2-id']);
		}
	});
	colOpts.push({
		"data": "qcState",
		"title": biolims.tStorageReagentBuySerial.qcState,
		"createdCell": function(td) {
			$(td).attr("saveName", "qcState");
			$(td).attr("selectOpt", "|NotQC|QCPass|QC");
		},
		"visible": true,
		"className": "select",
		"name": "|NotQC|QCPass|QC",
		"render": function(data, type, full, meta) {
			if(data == "0") {
				return 'NotQC';
			}
			if(data == "1") {
				return 'QCPass';
			}
			if(data == "2") {
				return 'QC';
			} else {
				return '';
			}
		}
	});
	colOpts.push({
		"data": "note",
		"title": biolims.tStorageReagentBuySerial.note,
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
		},
		"visible": true,
		"className": "textarea"
	});
	colOpts.push({
		"data": "scopeId",
		"title": "scopeId",
		"createdCell": function(td) {
			$(td).attr("saveName", "scopeId");
		},
		"visible": false
	});
	colOpts.push({
		"data": "scopeName",
		"title": "scopeName",
		"createdCell": function(td) {
			$(td).attr("saveName", "scopeName");
		},
		"visible": false
	});	
	colOpts.push({
		"data": "note2",
		"title": biolims.tStorageReagentBuySerial.note2,
		"createdCell": function(td) {
			$(td).attr("saveName", "note2");
		},
		"visible": true,
		"className": "edit"
	});
	
	colOpts.push({
		"data": "recationSum",
		"visible":false,
		"title": biolims.tStorageReagentBuySerial.recationSum,
		"createdCell": function(td) {
			$(td).attr("saveName", "recationSum");
		},
		"className": "edit"
	});
	
	var handlemethod = $("#handlemethod").val();
	if(handlemethod != "view") {
		tbarOpts.push({
			text: biolims.common.fillDetail,
			action: function() {
				addItem($("#storageReagentBuySerialTable"))
			}
		});
		tbarOpts.push({
			text: biolims.common.addwindow,
			action: function() {
				addItemLayer($("#storageReagentBuySerialTable"))
			}
		});
		tbarOpts.push({
			text: biolims.common.Editplay,
			action: function() {
				editItemLayer($("#storageReagentBuySerialTable"))
			}
		});
		tbarOpts.push({
			text: biolims.common.save,
			action: function() {
				saveStorageReagentBuySerial($("#storageReagentBuySerialTable"));
			}
		});
	}

	var storageReagentBuySerialOptions = table(true,
		id,
		'/storage/showStorageReagentBuySerialTableJson.action', colOpts, tbarOpts)
	storageReagentBuySerialTable = renderData($("#storageReagentBuySerialTable"), storageReagentBuySerialOptions);
	storageReagentBuySerialTable.on('draw', function() {
		oldstorageReagentBuySerialChangeLog = storageReagentBuySerialTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	stepViewChange();
});

//行内input编辑
function edittable() {
	document.addEventListener('click', function(event) {
		var evt = window.event || event;
		if(evt.target.nodeName.toLowerCase() == "td") {
			var ele = event.srcElement || evt.target;
			if($(ele).hasClass("date")) {
				changeDateInput(ele);
			} else if($(ele).hasClass("select")) {
				changeSelectInput(ele);
			} else if($(ele).hasClass("edit")) {
				changeTextInput(ele);
				//console.log("123456")
			} else if($(ele).hasClass("textarea")) {
				changeTextTextarea(ele);
			}
		}
	}, false)
	document.addEventListener('touchstart', function(event) {
		var evt = window.event || event;
		if(evt.target.nodeName.toLowerCase() == "td") {
			var ele = event.srcElement || evt.target;
			if($(ele).hasClass("date")) {
				changeDateInput(ele);
			} else if($(ele).hasClass("select")) {
				changeSelectInput(ele);
			} else if($(ele).hasClass("edit")) {
				changeTextInput(ele);
				
			} else if($(ele).hasClass("textarea")) {
				changeTextTextarea(ele);
			}
		}

	}, false)
	//变普通input输入框
	function changeTextInput(ele) {
		$(ele).css({
			"padding": "0px"
		});
		var width = $(ele).css("width");
		var value=ele.innerText;
		//console.log(value);
		var ipt = $('<input type="text" id="edit">');
		ipt.css({
			"width": width,
			"height": "32px",
		});
		$(ele).html(ipt.val(value));
		ipt.focus();
		ipt.select();
		$(ipt).click();
		document.onkeydown = function(event) {
			var e = event || window.event || arguments.callee.caller.arguments[0];
			if(e.keyCode === 9) {
				var width = $("#edit").width();
				$("#edit").parent("td").css({
					"padding": "5px 0px 5px 5px",
					"overflow": "hidden",
					"text-overflow": "ellipsis",
					"max-width": width + "px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#edit").parents("tr").addClass("editagain");
				$("#edit").parent("td").html($("#edit").val());
				var next = $(ele).next("td");
				while(next.hasClass("undefined")) {
					next = next.next("td")
				}
				if(next.length) {
					next.click();
				} else {
					return false;
				}

			}

		};
	}
	//变普通Textarea输入框
	function changeTextTextarea(ele) {
		if($(ele).hasClass("edited")) {
			var width = $(ele).width() + "px";
		} else {
			var width = $(ele).width() + 10 + "px";
		}
		ele.style.padding = "0";
		$(ele).css({
			"min-width": width
		});
		var ipt = $('<textarea www=' + width + ' style="position: absolute;height:80px;width:200px" id="textarea">' + ele.innerText + '</textarea>');
		$(ele).html(ipt);
		$(ele).find("textarea").click();
		document.onkeydown = function(event) {
			var e = event || window.event || arguments.callee.caller.arguments[0];
			if(e.keyCode === 9) {
				var td = $("#textarea").parents("td");
				var width = $("#textarea").attr("www");
				td.css({
					"padding": "5px 0px",
					"max-width": width,
					"overflow": "hidden",
					"text-overflow": "ellipsis",
					"box-shadow": "0px 0px 1px #17C697",
				});
				td.addClass("edited");
				td.parent("tr").addClass("editagain");
				//td[0].title=$("#edit").val();
				td.html($("#textarea").val());

				var next = $(ele).next("td");
				while(next.hasClass("undefined")) {
					next = next.next("td")
				}
				if(next.length) {
					next.click();
				} else {
					return false;
				}
			}

		};
	}
	//变下拉框
	function changeSelectInput(ele) {
		$(ele).css("padding", "0px");
		var width = $(ele).width();
		var selectOpt = $(ele).attr("selectopt");
		var selectOptArr = selectOpt.split("|");
		var ipt = $('<select id="select"></select>');
		var value=ele.innerText;
		selectOptArr.forEach(function(val, i) {
			if(value==val){
				ipt.append("<option selected>" + val + "</option>");
			}else{
				ipt.append("<option>" + val + "</option>");
			}
		});
		ipt.css({
			"width": width + "px",
			"height": "32px",
		});
		$(ele).html(ipt);
		//$(ele).find("select").click();
		document.onkeydown = function(event) {
			var e = event || window.event || arguments.callee.caller.arguments[0];
			if(e.keyCode === 9) {
				$("#select").parent("td").css({
					"padding": "5px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#select").parents("tr").addClass("editagain");
				$("#select").parent("td").html($("#select option:selected").val());

				var next = $(ele).next("td");
				while(next.hasClass("undefined")) {
					next = next.next("td")
				}
				if(next.length) {
					next.click();
				} else {
					return false;
				}
			}

		};
	}
	//变日期选择框
	function changeDateInput(ele) {
		$(ele).css("padding", "0px");
		var ipt = $('<input type="text" id="date" autofocus value=' + $(ele).text() + '>');
		var width = $(ele).width();
		ipt.css({
			"width": width + "px",
			"height": "32px",
		});
		$(ele).html(ipt);
//		ipt.datetimepicker({
		ipt.datepicker({
			language: "zh-TW",
			autoclose: true, //选中之后自动隐藏日期选择框
//			format: "yyyy-mm-dd hh:ii" //日期格式，详见 
			format: "yyyy-mm-dd" //日期格式，详见 
		});
		$(ele).find("input").click();
		document.onkeydown = function(event) {
			var e = event || window.event || arguments.callee.caller.arguments[0];
			if(e.keyCode === 9) {
				var width = $("#edit").width();
				if(!$(".datepicker").length) {
					$("#date").parent("td").css({
						"padding": "5px",
						"box-shadow": "0px 0px 1px #17C697"
					});
					$("#date").parents("tr").addClass("editagain");
					$("#date").parent("td").html($("#date").val());
				}
				var next = $(ele).next("td");
				while(next.hasClass("undefined")) {
					next = next.next("td")
				}
				if(next.length) {
					next.click();
				} else {
					return false;
				}
			}

		};
	}
	document.onmouseup = function(event) {
		var evt = window.event || event;
		if(document.getElementById("textarea") && evt.target.id != "textarea") {
			var td = $("#textarea").parents("td");
			var width = $("#textarea").attr("www");
			td.css({
				"padding": "5px 0px",
				"max-width": width,
				"overflow": "hidden",
				"text-overflow": "ellipsis",
				"box-shadow": "0px 0px 1px #17C697",
			});
			td.addClass("edited");
			td.parent("tr").addClass("editagain");
			//td[0].title=$("#edit").val();
			td.html($("#textarea").val());
		}
		if(document.getElementById("edit") && evt.target.id != "edit") {
			var width = $("#edit").width();
			$("#edit").parent("td").css({
				"padding": "5px 0px 5px 5px",
				"overflow": "hidden",
				"text-overflow": "ellipsis",
				"max-width": width + "px",
				"box-shadow": "0px 0px 1px #17C697"
			});
			$("#edit").parents("tr").addClass("editagain");
			$("#edit").parent("td").html($("#edit").val());
		}
		if(document.getElementById("select") && evt.target.id != "select") {
			$("#select").parent("td").css({
				"padding": "5px",
				"box-shadow": "0px 0px 1px #17C697"
			});
			$("#select").parents("tr").addClass("editagain");
			$("#select").parent("td").html($("#select option:selected").val());
		}
		if(document.getElementById("date") && evt.target.id != "date") {
			if(!$(".datepicker").length) {
				$("#date").parent("td").css({
					"padding": "5px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#date").parents("tr").addClass("editagain");
				$("#date").parent("td").html($("#date").val());
			}
		}
	}
}

// 保存
function saveStorageReagentBuySerial(ele) {
	
	/**
	 * 判断日期
	 */
	var arr = [];
	var arr1 = [];
	$("#storageReagentBuySerialTable tbody tr").each(function(i,v){
		arr.push($(v).find("td[savename='productDate']").text());
		arr1.push($(v).find("td[savename='inDate']").text());
		
	})
	var scTime = "";
	var rkTime = "";
	for(var i=0;i<arr.length;i++){
		 for(var j=0;j<arr.length;j++){
			 scTime = new Date(arr[i]).getTime();
			 rkTime = new Date(arr1[j]).getTime();
			 if(scTime>rkTime){
				  top.layer.msg("生产日期不能小于入库日期！");
				  return false;
			  }
		 }
		 
	}
	
	var data = saveStorageReagentBuySerialjson(ele);
	var ele = $("#storageReagentBuySerialTable");
	var changeLog = "实验结果：";
	changeLog = getChangeLog(data, ele, changeLog);

	$.ajax({
		type: 'post',
		url: '/storage/saveStorageReagentBuySerialTable.action',
		data: {
			id: $("#storage_id").val(),
			dataJson: data,
			changeLog: changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveStorageReagentBuySerialjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字

			if(k == "storage-name") {
				json["storage-id"] = $(tds[j]).attr("storage-id");
				continue;
			}

			if(k == "costCenter-name") {
				json["costCenter-id"] = $(tds[j]).attr("costCenter-id");
				continue;
			}

			if(k == "position-name") {
				json["position-id"] = $(tds[j]).attr("position-id");
				continue;
			}

			if(k == "storageInItem-name") {
				json["storageInItem-id"] = $(tds[j]).attr("storageInItem-id");
				continue;
			}

			if(k == "regionType-name") {
				json["regionType-id"] = $(tds[j]).attr("regionType-id");
				continue;
			}

			if(k == "linkStorageItem-name") {
				json["linkStorageItem-id"] = $(tds[j]).attr("linkStorageItem-id");
				continue;
			}

			if(k == "rankType-name") {
				json["rankType-id"] = $(tds[j]).attr("rankType-id");
				continue;
			}

			if(k == "currencyType-name") {
				json["currencyType-id"] = $(tds[j]).attr("currencyType-id");
				continue;
			}

			if(k == "timeUnit-name") {
				json["timeUnit-id"] = $(tds[j]).attr("timeUnit-id");
				continue;
			}

			if(k == "storage-unitGroup-mark2-name") {
				json["storage-unitGroup-mark2-id"] = $(tds[j]).attr("storage-unitGroup-mark2-id");
				continue;
			}
			
			if(k == "qcState") {
				var qcState = $(tds[j]).text();
				if(qcState=="NotQC"){
					json[k] = "0";
				}else if(qcState=="QCPass"){
					json[k] = "1";
				}else if(qcState=="QC"){
					json[k] = "2";
				}else{
					json[k] = "";
				}
				continue;
			}
			
			

			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}

function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '编号为"' + v.id + '":';
		oldstorageReagentBuySerialChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}