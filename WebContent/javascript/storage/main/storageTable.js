var storageTable;
$(function() {
	var fields = [];
	fields.push({
		"data": "id",
		"title": biolims.tStorage.id,
	});

//	fields.push({
//		"data": "barCode",
//		"title": biolims.tStorage.barCode,
//	});

//	fields.push({
//		"data": "source-name",
//		"title": biolims.tStorage.dicSourceTypeId
//	});

	fields.push({
		"data": "spec",
		"title": biolims.tStorage.spec,
	});

	fields.push({
		"data": "state-name",
		"title": biolims.tStorage.dicStateId
	});

	fields.push({
		"data": "createUser-name",
		"title": biolims.tStorage.userCreateUserId
	});

	fields.push({
		"data": "createDate",
		"title": biolims.tStorage.createDate,
		"render": function(data) {
			if(data) {
				return parent.moment(data).format('YYYY-MM-DD');
			} else {
				return "";
			}
		}
	});

	fields.push({
		"data": "ifCall",
		"title": biolims.tStorage.ifCall,
		"render" : function(data, type, full, meta) {
			if (data == "1") {
				return "是";
			}
			if (data == "0") {
				return "否";
			}
			 else {
				return '';
			}
		}
	});


	fields.push({
		"data": "num",
		"title": biolims.tStorage.num,
	});


	fields.push({
		"data": "name",
		"title": biolims.tStorage.name,
	});

	fields.push({
		"data": "unitGroup-id",
		"visible": false,
		"title": biolims.common.unitGroupId,
	});
	
	fields.push({
		"data": "unitGroup-name",
		"visible": false,
		"title": biolims.common.unitGroupName,
	});
	
	fields.push({
		"data": "unitGroup-mark2-name",
		"visible": false,
		"title": biolims.common.unit,
	});
	
	fields.push({
		"data": "producer-name",
		"title": biolims.equipment.producerName,
	});

	fields.push({
		"data": "position-name",
		"title": biolims.tStorage.storagePositionId
	});

	fields.push({
		"data": "breed",
		"title": biolims.tStorage.breed,
	});


	fields.push({
		"data": "note",
		"title": biolims.tStorage.note,
	});

//	fields.push({
//		"data": "kit-name",
//		"title": biolims.tStorage.kitId
//	});

//	fields.push({
//		"data": "jdeCode",
//		"title": biolims.tStorage.jdeCode,
//	});

	$.ajax({
		type: "post",
		url: window.ctx + "/system/customfields/findFieldByModuleValue.action",
		async: false,
		data: {
			moduleValue: "Storage"
		},
		success: function(data) {
			var objData = JSON.parse(data);
			if(objData.success) {
				$.each(objData.data, function(i, n) {
					var str = {
						"data": n.fieldName,
						"title": n.label
					}
					fields.push(str);
				});

			} else {
				top.layer.msg(biolims.tInstrumentBorrowDetail.tableError);
			}
		}
	});

	var options = table(true, "", ctx + '/storage/showStorageTableJson.action?p_type=' +
		$("#p_type").val(),
		fields, null)
	storageTable = renderRememberData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(storageTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
		"/storage/toEditStorage.action?p_type=" + $("#p_type").val();
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/storage/toEditStorage.action?id=" + id+"&p_type=" + $("#p_type").val();
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/storage/toViewStorage.action?id=" + id+"&p_type=" + $("#p_type").val();
}
//弹框模糊查询参数
function searchOptions() {
	var fields = [];
	
	fields.push({
		"searchName": "id",
		"type": "input",
		"txt": biolims.tStorage.id
	});
	/*fields.push({
		"searchName": "barCode",
		"type": "input",
		"txt": biolims.tStorage.barCode
	});
	fields.push({
		"type": "input",
		"searchName": "dicSourceTypeId.id",
		"txt": biolims.tStorage.dicSourceTypeId
	});
	fields.push({
		"type": "input",
		"searchName": "dicSourceTypeId.name",
		"txt": biolims.tStorage.dicSourceTypeId
	});*/
	fields.push({
		"searchName": "spec",
		"type": "input",
		"txt": biolims.tStorage.spec
	});
/*	fields.push({
		"type": "input",
		"searchName": "dicStateId.id",
		"txt": biolims.common.state+"ID"
	});*/
	fields.push({
		"type": "input",
		"searchName": "state.name",
		"txt": biolims.tStorage.dicStateId
	});
/*	fields.push({
		"type": "input",
		"searchName": "userCreateUserId.id",
		"txt": biolims.sample.createUserId
	});*/
	fields.push({
		"type": "input",
		"searchName": "createUser.name",
		"txt": biolims.sample.createUserName
	});
/*	fields.push({
		"searchName": "createDate",
		"type": "input",
		"txt": biolims.tStorage.createDate
	});*/
	fields.push({
		"txt": biolims.tStorage.createDate+"(Start)",
		"type": "dataTime",
		"searchName": "createDate##@@##1",
		"mark": "s##@@##",
	}, {
		"txt": biolims.tStorage.createDate+"(End)",
		"type": "dataTime",
		"mark": "e##@@##",
		"searchName": "createDate##@@##2"
	});
/*	fields.push({
		"searchName": "ifCall",
		"type": "input",
		"txt": biolims.tStorage.ifCall
	});
	fields.push({
		"searchName": "picPath",
		"type": "input",
		"txt": biolims.tStorage.picPath
	});*/
	fields.push({
		"searchName": "num",
		"type": "input",
		"txt": biolims.tStorage.num
	});
/*	fields.push({
		"searchName": "purchaseNum",
		"type": "input",
		"txt": biolims.tStorage.purchaseNum
	});*/
	fields.push({
		"searchName": "name",
		"type": "input",
		"txt": biolims.tStorage.name
	});
	fields.push({
		"searchName": "breed",
		"type": "input",
		"txt": biolims.tStorage.breed
	});
	fields.push({
		"txt": "是否失效提醒",
		"type": "select",
		"options": "请选择|是|否",
		"changeOpt": "''|1|0",
		"searchName": "ifCall"
	});
	fields.push({
		"type": "table",
		"table": storageTable
	});
	return fields;
}