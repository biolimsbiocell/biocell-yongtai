﻿$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.reagentNo,
	});
	colOpts.push({
		"data": "name",
		"title": "名称",
	});
	colOpts.push({
		"data": "jdeCode",
		"title": "JDE编码",
	});
	colOpts.push({
		"data": "producer-id",
		"title": "生产商ID",
	});
	colOpts.push({
		"data": "producer-name",
		"title": "生产商",
	});
	var tbarOpts = [];
	var options = table(true, null,
		'/storage/showStorageDialogListForGoodsJson.action', colOpts, null)
	var addToGoods = renderData($("#addToGoods"), options);
	$("#addToGoods").on(
		'init.dt',
		function(e, settings) {
			// 清除操作按钮
			$("#addToGoods_wrapper .dt-buttons").empty();
			$('#addToGoods_wrapper').css({
				"padding": "0 16px"
			});
//			var trs = $("#addToGoods tbody tr");
			addToGoods.ajax.reload();
//			addToGoods.on('draw', function() {
//				trs = $("#addToGoods tbody tr");
//				trs.click(function() {
//					$(this).addClass("chosed").siblings("tr")
//						.removeClass("chosed");
//				});
//			});
//			trs.click(function() {
//				$(this).addClass("chosed").siblings("tr")
//					.removeClass("chosed");
//			});
		});
})