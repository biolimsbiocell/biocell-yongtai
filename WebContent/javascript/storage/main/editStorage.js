function exportexcel() {
	gridGrid.title = biolims.common.exportList
	var vExportContent = gridGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}


function storageifSecondBuy(){
	var storageifSecondBuy = $("#storage_ifSecondBuy").val();
	if(storageifSecondBuy==0){
		$("#storage_safeNum").removeClass().addClass("input_parts text input default  readonlytrue false");
		$("#storage_safeNum").attr({readonly:'true'});
	}
	if(storageifSecondBuy==1){
		$("#storage_safeNum").removeClass().addClass("input_parts text input default  readonlytfalse false");
		$("#storage_safeNum").removeAttr("readonly"); 
	}
}
function storageremindLeadTime(){
	var storageifSecondBuy = $("#storage_ifCall").val();
	if(storageifSecondBuy==0){
		$("#storage_durableDays").removeClass().addClass("input_parts text input default  readonlytrue false");
		$("#storage_durableDays").attr({readonly:'true'});
		$("#storage_remindLeadTime").removeClass().addClass("input_parts text input default  readonlytrue false");
		$("#storage_remindLeadTime").attr({readonly:'true'});
	}
	if(storageifSecondBuy==1){
		$("#storage_durableDays").removeClass().addClass("input_parts text input default  readonlytfalse false");
		$("#storage_durableDays").removeAttr("readonly"); 
		$("#storage_remindLeadTime").removeClass().addClass("input_parts text input default  readonlytfalse false");
		$("#storage_remindLeadTime").removeAttr("readonly"); 
	}
}

function viewCCSupplier() {
	if (trim(document.getElementById('instrument_producer_id').value) == '') {
		message(biolims.common.selectRecord);
	} else {
		openDialog(window.ctx + '/supplier/toViewSupplier.action?id='
				+ document.getElementById('instrument_producer_id').value);

	}
}

function add() {
	window.location = window.ctx + "/storage/toEditStorage.action?p_type="+$("#p_type").val();
}
function list() {
	window.location = window.ctx + '/storage/showStorageList.action?queryMethod='
	+ "1"+"&p_type="+$("#p_type").val() ;
}

function newSave() {
	if (checkSubmit() == false) {
		return false;

	}
	var myMask = new Ext.LoadMask(Ext.getBody(), {
		msg : biolims.common.pleaseWait
	});
	myMask.show();
	Ext.Ajax.request({
		url : window.ctx + '/common/hasId.action',
		method : 'POST',
		params : {
			id : $("#storage_id").val(),
			obj : 'Storage'
		},
		success : function(response) {
			var respText = Ext.util.JSON.decode(response.responseText);
			myMask.hide();
			if (respText.message == '') {
				save();
			} else {
				message(respText.message);
			}

		},
		failure : function(response) {
		}
	});

}
function save() {if (checkSubmit() == true) {
	Ext.MessageBox.show({
		msg : biolims.common.savingData,
		progressText : biolims.common.saving,
		width : 300,
		wait : true,
		icon : 'ext-mb-download'
	});
	 $("#toolbarbutton_save").hide();
//	 window.tab11frame.saveitem();
	top.layer.load(4, {shade:0.3}); 
	form1.action = window.ctx + "/storage/save.action?p_type="+$("#p_type").val()+"&id="+$("#storage_id").val();
	form1.submit();
	top.layer.closeAll();
	//item 保存
} else {
	return false;
}}
function checkSubmit() {
	var mess = "";
	var fs = [ document.getElementById("storage_id").value, document.getElementById("storage_type_id").value,
			 document.getElementById("storage_state_id").value ];
	var nsc = [ biolims.storage.materialCodeEmpty, biolims.storage.batchTypeEmpty, biolims.common.stateIsEmpty ];
	mess = commonFieldsNotNullVerify(fs, nsc);

	if (mess != "") {
		message(mess);
		return false;
	}
	return true;
}

function modifyData(params) {
	var url = window.ctx + "/storage/saveStoragePostion.action";
	if (trim(document.getElementById("storage_id").value) != '') {
		var paramsValue = "storageId:'" + document.getElementById("storage_id").value + "'";
		commonModifyData(params, paramsValue, url, gridGrid);
	} else {
		message(biolims.common.IdEmpty);
	}
}

function delData(params) {
	var url = window.ctx + "/storage/delStoragePostion.action";
	commonDelData(params, url, gridGrid);

}

function viewSupplier() {
	if (trim(document.getElementById('storage.producer.id').value) == '') {
		message(biolims.common.selectRecord);
	} else {
		openDialog(window.ctx + '/supplier/toViewSupplier.action?id='
				+ document.getElementById('storage.producer.id').value);
	}
}
function editCopy() {
	window.location = window.ctx + '/storage/toCopyStorage.action?id=' + $("#storage_id").val();
}
function setGridId() {
	var gridCount = gridGrid.store.getCount();
	for ( var ij = 0; ij < gridCount; ij++) {
		var record = gridGrid.store.getAt(ij);
		record.set("id", "");
	}
}

$(function() {
	if($("#storage_studyType_id").val()=="index"){
		$("#tri1").removeClass("tri");
		$("#tri2").removeClass("tri");
	}else{
		$("#tri1").addClass("tri");
		$("#tri2").addClass("tri");
	}
	
	$("#showDicYjly").click(function(){
		var win = Ext.getCmp('showyjlxList');
		if (win) {
			win.close();
		}
		var showyjlxList = new Ext.Window(
				{
					id : 'showyjlxList',
					modal : true,
					title : biolims.sample.selectType,
					layout : 'fit',
					width : 600,
					height : 600,
					closeAction : 'close',
					plain : true,
					bodyStyle : 'padding:5px;',
					buttonAlign : 'center',
					collapsible : true,
					maximizable : true,
					items : new Ext.BoxComponent(
							{
								id : 'maincontent',
								region : 'center',
								html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/dic/type/dicTypeSelect.action?flag=yjlx' frameborder='0' width='780' height='500' ></iframe>"
							}),
					buttons : [ {
						text : biolims.common.close,
						handler : function() {
							showyjlxList.close();
						}
					} ]
				});
		showyjlxList.show();
	});
	
	
	
	var storageIfCall = new Ext.form.ComboBox({
		transform : "storage_ifCall",
		width : 80,
		hiddenId : "storage_ifCall",
		hiddenName : "storage.ifCall",
		listeners :{
			 'select': function(){
					var storageifSecondBuy = $("#storage_ifCall").val();
					if(storageifSecondBuy==0){
						$("#storage_durableDays").removeClass().addClass("input_parts text input default  readonlytrue false");
						$("#storage_durableDays").attr({readonly:'true'});
						$("#storage_remindLeadTime").removeClass().addClass("input_parts text input default  readonlytrue false");
						$("#storage_remindLeadTime").attr({readonly:'true'});
					}
					if(storageifSecondBuy==1){
						$("#storage_durableDays").removeClass().addClass("input_parts text input default  readonlytfalse false");
						$("#storage_durableDays").removeAttr("readonly"); 
						$("#storage_remindLeadTime").removeClass().addClass("input_parts text input default  readonlytfalse false");
						$("#storage_remindLeadTime").removeAttr("readonly"); 
					}
			 }
		}
	});
	var storageIfSecondBuy = new Ext.form.ComboBox({
		transform : "storage_ifSecondBuy",
		width : 80,
		hiddenId : "storage_ifSecondBuy",
		hiddenName : "storage.ifSecondBuy",
		listeners :{
			 'select': function(){
					var storageifSecondBuy = $("#storage_ifSecondBuy").val();
					if(storageifSecondBuy==0){
						$("#storage_safeNum").removeClass().addClass("input_parts text input default  readonlytrue false");
						$("#storage_safeNum").attr({readonly:'true'});
					}
					if(storageifSecondBuy==1){
						$("#storage_safeNum").removeClass().addClass("input_parts text input default  readonlytfalse false");
						$("#storage_safeNum").removeAttr("readonly"); 
					}
			 }
		}
	});
	var copyBl = $("#copyBl").val();
	if(copyBl){
		setTimeout("setGridId()",1000); 	
	}
	storageifSecondBuy();
	storageremindLeadTime();
	
	
});
function setyjlx(id,name) {
	if(id=="index"){
		$("#tri1").removeClass("tri");
		$("#tri2").removeClass("tri");
		ajax("post", "/storage/maxCurrentIndex.action", {
		}, function(data) {
			if(data.success){
				$("#storage_currentIndex").val(data.data);
			}
		}, null);
	}else{
		$("#tri1").addClass("tri");
		$("#tri2").addClass("tri");
	}
	$("#storage_studyType_id").val(id);
	$("#storage_studyType_name").val(name);
	var win = Ext.getCmp('showyjlxList');
	if (win) {
		win.close();
	}
}	
Ext.onReady(function() {
	var tabs = new Ext.TabPanel({
		renderTo : 'maintab',
		height : document.body.clientHeight-30,
		autoWidth : true,
		activeTab : 0,
		margins : '0 0 0 0',
		items : [ {
			title : biolims.storage.basicInformation,
			contentEl : 'markup'

		} ]
	});
});
Ext.onReady(function(){
	if($("#p_type").val()==2){
		$("#tri1").addClass("tri");
		$("#tri2").addClass("tri");
		$("#tri3").addClass("tri");
		$("#tri4").addClass("tri");
	}
	var item = menu.add({
	    	text: biolims.storage.checkInOut
		});
	item.on('click', ckcrk);
	});
function ckcrk(){
	
	var url = '__report=storageOutIn.rptdesign&storageId=' + $("#storage_id").val();
	commonPrint(url);
	
}
function ckcrk1(){
	
	var url = '__report=zjpz43.rptdesign&storageId=' + $("#storage_id").val();
	commonPrint(url);
	
}
function makeCode(){
	
	
	
	ajax("post", "/sample/receive/makeCode.action", {
		
		id : $("#storage_id").val(),
		ip : '10.0.0.213'
	}, function() {
	
	}, null);
}


