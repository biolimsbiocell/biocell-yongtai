﻿$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.reagentNo,
	});
	colOpts.push({
		"data": "storage-name",
		"title": biolims.common.reagentName,
	});
	colOpts.push({
		"data": "serial",
		"title": biolims.storage.batchNo,
	});
	/*colOpts.push({
		"data": "expireDate",
		"title": biolims.common.expirationDate,
	});*/
	colOpts.push({
		"data": "num",
		"title": biolims.tStorageModifyItem.num,
	});
	colOpts.push({
		"data": "unit-name",
		"title": biolims.common.unit,
	});
	var tbarOpts = [];
	var options = table(false, null,
		'/storage/showStorageDialogListJson.action', colOpts, null)
	var addStorage = renderData($("#addStorage"), options);
	$("#addStorage").on(
		'init.dt',
		function(e, settings) {
			// 清除操作按钮
			$("#addStorage_wrapper .dt-buttons").empty();
			$('#addStorage_wrapper').css({
				"padding": "0 16px"
			});
			var trs = $("#addStorage tbody tr");
			addStorage.ajax.reload();
			addStorage.on('draw', function() {
				trs = $("#addStorage tbody tr");
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			});
			trs.click(function() {
				$(this).addClass("chosed").siblings("tr")
					.removeClass("chosed");
			});
		});
})