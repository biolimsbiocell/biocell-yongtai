$(function() {
	$("#tabs").tabs({
		select: function(event, ui) {}
	});
});

function add() {
	window.location = window.ctx + "/storage/editStorage.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});

function list() {
	window.location = window.ctx + '/storage/showStorageList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});

function newSave() {
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});
$("#toolbarbutton_tjsp").click(function() {
	submitWorkflow("#storage", {
		userId: userId,
		userName: userName,
		formId: $("#storage_id").val(),
		title: $("#storage_name").val()
	}, function() {
		window.location.reload();
	});

});
$("#toolbarbutton_sp").click(function() {
	completeTask($("#storage_id").val(), $(this).attr("taskId"), function() {
		document.getElementById('toolbarSaveButtonFlag').value = 'save';
		location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
	});
});

function save() {
	if(checkSubmit() == true) {
		Ext.MessageBox.show({
			msg: biolims.common.savingData,
			progressText: biolims.common.saving,
			width: 300,
			wait: true,
			icon: 'ext-mb-download'
		});
		document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/storage/save.action";
		form1.submit();

	}
}

function editCopy() {
	window.location = window.ctx + '/storage/copyStorage.action?id=' + $("#storage_id").val();
}
/*function changeState() {
	commonChangeState("formId=" + $("#storage_id").val() + "&tableId=storage");
}*/
$("#toolbarbutton_status").click(function() {
	commonChangeState("formId=" + $("#storage_id").val() + "&tableId=Storage");
});

function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#storage_id").val());
	nsc.push(biolims.storage.REFisEmpty);
	fs.push($("#storage_name").val());
	nsc.push(biolims.storage.nameIsEmpty);
	fs.push($("#storage_spec_name").val());
	nsc.push(biolims.storage.specNameIsEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if(mess != "") {
		message(mess);
		return false;
	}
	return true;
}
$(function() {
	Ext.onReady(function() {
		var tabs = new Ext.TabPanel({
			id: 'tabs11',
			renderTo: 'maintab',
			height: document.body.clientHeight - 30,
			autoWidth: true,
			activeTab: 0,
			margins: '0 0 0 0',
			items: [{
				title: biolims.common.theInventoryMasterData,
				contentEl: 'markup'
			}]
		});
	});

	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "view") {
		settextreadonlyByAll();
	}
});

var item = menu.add({
	text: biolims.common.copy
});
item.on('click', editCopy);