var storageReagentComponentsTable;
var oldstorageReagentComponentsChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表
	var id = $("#storage_id").val();
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": "编码",
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		},
		"visible": false,
		"className": "edit"
	});
	colOpts.push({
		"data": "compoundName",
		"title": "化合物名称",
		"createdCell": function(td) {
			$(td).attr("saveName", "compoundName");
		},
		"className": "edit"
	});
	colOpts.push({
		"data": "content",
		"title": "含量（mg/L）",
		"createdCell": function(td) {
			$(td).attr("saveName", "content");
		},
		"className": "edit"
	});
	colOpts.push({
		"data": "accreditation",
		"title": "是否认证",
		"createdCell": function(td) {
			$(td).attr("saveName", "accreditation");
			$(td).attr("selectOpt", "|是|否");
		},
		"className": "select",
		"name": "|是|否",
		"render": function(data, type, full, meta) {
			if(data == "0") {
				return '否';
			}
			if(data == "1") {
				return '是';
			}else {
				return '';
			}
		}
	});
	colOpts.push({
		"data": "certificationBody-name",
		"title": "认证机构",
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "certificationBody-name");
			$(td).attr("certificationBody-id", rowData['certificationBody-id']);
		}
	});
	
	var handlemethod = $("#handlemethod").val();
	if(handlemethod != "view") {
		tbarOpts.push({
			text: biolims.common.fillDetail,
			action: function() {
				addItem($("#storageReagentComponentsTable"))
			}
		});
		tbarOpts.push({
			text: biolims.common.addwindow,
			action: function() {
				addItemLayer($("#storageReagentComponentsTable"))
			}
		});
		tbarOpts.push({
			text: biolims.common.Editplay,
			action: function() {
				editItemLayer($("#storageReagentComponentsTable"))
			}
		});
		tbarOpts.push({
			text: biolims.common.save,
			action: function() {
				saveStorageReagentComponents($("#storageReagentComponentsTable"));
			}
		});
		tbarOpts.push({
			text: "选择认证机构",
			action: function() {
				selectComponents();
			}
		});
	}

	var storageReagentComponentsOptions = table(true,
		id,
		'/storage/showStorageReagentComponentsTableJson.action', colOpts, tbarOpts)
	storageReagentComponentsTable = renderData($("#storageReagentComponentsTable"), storageReagentComponentsOptions);
	storageReagentComponentsTable.on('draw', function() {
		oldstorageReagentComponentsChangeLog = storageReagentComponentsTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	stepViewChange();
});

//选择认证机构
function selectComponents() {
	var rows = $("#storageReagentComponentsTable .selected");
	var length = rows.length;
	if (!length) {
		top.layer.msg(biolims.common.pleaseSelectData);
		return false;
	}
	top.layer.open({
		title : "选择认证机构",
		type : 2,
		area : [ "650px", "400px" ],
		btn : biolims.common.selected,
		content : [
				window.ctx + "/dic/type/dicTypeSelectTable.action?flag=Components",
				'' ],
		yes : function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(
				0).text();
			
			top.layer.close(index);
			rows.addClass("editagain");
			rows.find("td[savename='certificationBody-name']").text(name);
			rows.find("td[savename='certificationBody-name']").attr("certificationBody-id",id);
			top.layer.close(index)
		},
	})
}

//行内input编辑
//function edittable() {
//	document.addEventListener('click', function(event) {
//		var evt = window.event || event;
//		if(evt.target.nodeName.toLowerCase() == "td") {
//			var ele = event.srcElement || evt.target;
//			if($(ele).hasClass("date")) {
//				changeDateInput(ele);
//			} else if($(ele).hasClass("select")) {
//				changeSelectInput(ele);
//			} else if($(ele).hasClass("edit")) {
//				changeTextInput(ele);
//				//console.log("123456")
//			} else if($(ele).hasClass("textarea")) {
//				changeTextTextarea(ele);
//			}
//		}
//	}, false)
//	document.addEventListener('touchstart', function(event) {
//		var evt = window.event || event;
//		if(evt.target.nodeName.toLowerCase() == "td") {
//			var ele = event.srcElement || evt.target;
//			if($(ele).hasClass("date")) {
//				changeDateInput(ele);
//			} else if($(ele).hasClass("select")) {
//				changeSelectInput(ele);
//			} else if($(ele).hasClass("edit")) {
//				changeTextInput(ele);
//				
//			} else if($(ele).hasClass("textarea")) {
//				changeTextTextarea(ele);
//			}
//		}
//
//	}, false)
//	//变普通input输入框
//	function changeTextInput(ele) {
//		$(ele).css({
//			"padding": "0px"
//		});
//		var width = $(ele).css("width");
//		var value=ele.innerText;
//		//console.log(value);
//		var ipt = $('<input type="text" id="edit">');
//		ipt.css({
//			"width": width,
//			"height": "32px",
//		});
//		$(ele).html(ipt.val(value));
//		ipt.focus();
//		ipt.select();
//		$(ipt).click();
//		document.onkeydown = function(event) {
//			var e = event || window.event || arguments.callee.caller.arguments[0];
//			if(e.keyCode === 9) {
//				var width = $("#edit").width();
//				$("#edit").parent("td").css({
//					"padding": "5px 0px 5px 5px",
//					"overflow": "hidden",
//					"text-overflow": "ellipsis",
//					"max-width": width + "px",
//					"box-shadow": "0px 0px 1px #17C697"
//				});
//				$("#edit").parents("tr").addClass("editagain");
//				$("#edit").parent("td").html($("#edit").val());
//				var next = $(ele).next("td");
//				while(next.hasClass("undefined")) {
//					next = next.next("td")
//				}
//				if(next.length) {
//					next.click();
//				} else {
//					return false;
//				}
//
//			}
//
//		};
//	}
//	//变普通Textarea输入框
//	function changeTextTextarea(ele) {
//		if($(ele).hasClass("edited")) {
//			var width = $(ele).width() + "px";
//		} else {
//			var width = $(ele).width() + 10 + "px";
//		}
//		ele.style.padding = "0";
//		$(ele).css({
//			"min-width": width
//		});
//		var ipt = $('<textarea www=' + width + ' style="position: absolute;height:80px;width:200px" id="textarea">' + ele.innerText + '</textarea>');
//		$(ele).html(ipt);
//		$(ele).find("textarea").click();
//		document.onkeydown = function(event) {
//			var e = event || window.event || arguments.callee.caller.arguments[0];
//			if(e.keyCode === 9) {
//				var td = $("#textarea").parents("td");
//				var width = $("#textarea").attr("www");
//				td.css({
//					"padding": "5px 0px",
//					"max-width": width,
//					"overflow": "hidden",
//					"text-overflow": "ellipsis",
//					"box-shadow": "0px 0px 1px #17C697",
//				});
//				td.addClass("edited");
//				td.parent("tr").addClass("editagain");
//				//td[0].title=$("#edit").val();
//				td.html($("#textarea").val());
//
//				var next = $(ele).next("td");
//				while(next.hasClass("undefined")) {
//					next = next.next("td")
//				}
//				if(next.length) {
//					next.click();
//				} else {
//					return false;
//				}
//			}
//
//		};
//	}
//	//变下拉框
//	function changeSelectInput(ele) {
//		$(ele).css("padding", "0px");
//		var width = $(ele).width();
//		var selectOpt = $(ele).attr("selectopt");
//		var selectOptArr = selectOpt.split("|");
//		var ipt = $('<select id="select"></select>');
//		var value=ele.innerText;
//		selectOptArr.forEach(function(val, i) {
//			if(value==val){
//				ipt.append("<option selected>" + val + "</option>");
//			}else{
//				ipt.append("<option>" + val + "</option>");
//			}
//		});
//		ipt.css({
//			"width": width + "px",
//			"height": "32px",
//		});
//		$(ele).html(ipt);
//		//$(ele).find("select").click();
//		document.onkeydown = function(event) {
//			var e = event || window.event || arguments.callee.caller.arguments[0];
//			if(e.keyCode === 9) {
//				$("#select").parent("td").css({
//					"padding": "5px",
//					"box-shadow": "0px 0px 1px #17C697"
//				});
//				$("#select").parents("tr").addClass("editagain");
//				$("#select").parent("td").html($("#select option:selected").val());
//
//				var next = $(ele).next("td");
//				while(next.hasClass("undefined")) {
//					next = next.next("td")
//				}
//				if(next.length) {
//					next.click();
//				} else {
//					return false;
//				}
//			}
//
//		};
//	}
//	//变日期选择框
//	function changeDateInput(ele) {
//		$(ele).css("padding", "0px");
//		var ipt = $('<input type="text" id="date" autofocus value=' + $(ele).text() + '>');
//		var width = $(ele).width();
//		ipt.css({
//			"width": width + "px",
//			"height": "32px",
//		});
//		$(ele).html(ipt);
////		ipt.datetimepicker({
//		ipt.datepicker({
//			language: "zh-TW",
//			autoclose: true, //选中之后自动隐藏日期选择框
////			format: "yyyy-mm-dd hh:ii" //日期格式，详见 
//			format: "yyyy-mm-dd" //日期格式，详见 
//		});
//		$(ele).find("input").click();
//		document.onkeydown = function(event) {
//			var e = event || window.event || arguments.callee.caller.arguments[0];
//			if(e.keyCode === 9) {
//				var width = $("#edit").width();
//				if(!$(".datepicker").length) {
//					$("#date").parent("td").css({
//						"padding": "5px",
//						"box-shadow": "0px 0px 1px #17C697"
//					});
//					$("#date").parents("tr").addClass("editagain");
//					$("#date").parent("td").html($("#date").val());
//				}
//				var next = $(ele).next("td");
//				while(next.hasClass("undefined")) {
//					next = next.next("td")
//				}
//				if(next.length) {
//					next.click();
//				} else {
//					return false;
//				}
//			}
//
//		};
//	}
//	document.onmouseup = function(event) {
//		var evt = window.event || event;
//		if(document.getElementById("textarea") && evt.target.id != "textarea") {
//			var td = $("#textarea").parents("td");
//			var width = $("#textarea").attr("www");
//			td.css({
//				"padding": "5px 0px",
//				"max-width": width,
//				"overflow": "hidden",
//				"text-overflow": "ellipsis",
//				"box-shadow": "0px 0px 1px #17C697",
//			});
//			td.addClass("edited");
//			td.parent("tr").addClass("editagain");
//			//td[0].title=$("#edit").val();
//			td.html($("#textarea").val());
//		}
//		if(document.getElementById("edit") && evt.target.id != "edit") {
//			var width = $("#edit").width();
//			$("#edit").parent("td").css({
//				"padding": "5px 0px 5px 5px",
//				"overflow": "hidden",
//				"text-overflow": "ellipsis",
//				"max-width": width + "px",
//				"box-shadow": "0px 0px 1px #17C697"
//			});
//			$("#edit").parents("tr").addClass("editagain");
//			$("#edit").parent("td").html($("#edit").val());
//		}
//		if(document.getElementById("select") && evt.target.id != "select") {
//			$("#select").parent("td").css({
//				"padding": "5px",
//				"box-shadow": "0px 0px 1px #17C697"
//			});
//			$("#select").parents("tr").addClass("editagain");
//			$("#select").parent("td").html($("#select option:selected").val());
//		}
//		if(document.getElementById("date") && evt.target.id != "date") {
//			if(!$(".datepicker").length) {
//				$("#date").parent("td").css({
//					"padding": "5px",
//					"box-shadow": "0px 0px 1px #17C697"
//				});
//				$("#date").parents("tr").addClass("editagain");
//				$("#date").parent("td").html($("#date").val());
//			}
//		}
//	}
//}

// 保存
function saveStorageReagentComponents(ele) {
	var data = saveStorageReagentComponentsjson(ele);
	var ele = $("#storageReagentComponentsTable");
	var changeLog = "实验结果：";
	changeLog = getComponentsChangeLog(data, ele, changeLog);

	$.ajax({
		type: 'post',
		url: '/storage/saveStorageReagentComponentsTable.action',
		data: {
			id: $("#storage_id").val(),
			dataJson: data,
			changeLog: changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveStorageReagentComponentsjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字

			if(k == "certificationBody-name") {
				json["certificationBody-id"] = $(tds[j]).attr("certificationBody-id");
				continue;
			}
			
			if(k == "accreditation") {
				var qcState = $(tds[j]).text();
				if(qcState=="否"){
					json[k] = "0";
				}else if(qcState=="是"){
					json[k] = "1";
				}else{
					json[k] = "";
				}
				continue;
			}
			
			

			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}

function getComponentsChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '编号为"' + v.id + '":';
		oldstorageReagentComponentsChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}