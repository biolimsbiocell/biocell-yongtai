var showSpecSelectGrid;
$(function(){
	var cols={};
    var fields=[];
    fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'spec',
		type:"string"
	});
	    fields.push({
		name:'type-name',
		type:"string"
	});
	    fields.push({
		name:'studyType-name',
		type:"string"
	});
	   fields.push({
		name:'position-name',
		type:"string"
	});
//	    fields.push({
//		name:'producer-name',
//		type:"string"
//	});
	    fields.push({
		name:'num',
		type:"string"
	});
	   fields.push({
		name:'unit-name',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'state-name',
		type:"string"
	});
	   fields.push({
		name:'dutyUser-name',
		type:"string"
	});
	    fields.push({
		name:'useDesc',
		type:"string"
	});
	    fields.push({
		name:'searchCode',
		type:"string"
	});
	    fields.push({
		name:'source-name',
		type:"string"
	});
	   fields.push({
		name:'barCode',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'ifCall',
		type:"string"
	});
    fields.push({
		name:'i5',
		type:"string"
	});
	    fields.push({
		name:'i7',
		type:"string"
	});
    fields.push({
		name:'kit-id',
		type:"string"
	});
    fields.push({
		name:'kit-name',
		type:"string"
	});
    fields.push({
		name:'currentIndex',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : false,
		header:biolims.common.id,
		width:20*6,
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:biolims.common.designation,
		width:20*6
	});
	cm.push({
		dataIndex:'kit-id',
		hidden : true,
		header:"原辅料盒Id",
		width:20*13
	});
	cm.push({
		dataIndex:'kit-name',
		hidden : false,
		header:"原辅料盒",
		width:20*13
	});
	cm.push({
		dataIndex:'spec',
		hidden : true,
		header:biolims.equipment.spec,
		width:20*6
	});
	cm.push({
		dataIndex:'type-name',
		hidden : true,
		header:biolims.common.type,
		width:20*6,
	});
	cm.push({
		dataIndex:'studyType-name',
		hidden : true,
		header:biolims.storage.subtypes,
		width:15*10,
	});
	cm.push({
		dataIndex:'position-name',
		hidden : true,
		header : biolims.common.storageName,
		width:15*10
	});
	cm.push({
		dataIndex:'num',
		hidden : true,
		header:biolims.storage.num,
		width:10*6,
	});
	cm.push({
		dataIndex:'unit-name',
		hidden : true,
		header:biolims.common.unit,
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : true,
		header : biolims.common.note,
		width:40*6
	});
	cm.push({
		dataIndex:'state-name',
		hidden : true,
		header:biolims.common.state,
		width:20*6,
	});
	cm.push({
		dataIndex:'dutyUser-name',
		hidden : true,
		header:biolims.master.personName,
		width:15*10,
	});
	cm.push({
		dataIndex:'useDesc',
		hidden : true,
		header:biolims.storage.useDesc,
		width:15*10
	});
	cm.push({
		dataIndex:'searchCode',
		hidden : true,
		header:biolims.storage.searchCode,
		width:10*6,
	});
	cm.push({
		dataIndex:'source-name',
		hidden : true,
		header:biolims.sample.sourceName,
		width:20*6
	});
	cm.push({
		dataIndex:'barCode',
		hidden : true,
		header:biolims.common.barCode,
		width:40*6
	});
	var ifCall = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '0',
				name : biolims.common.no
			},{
				id : '1',
				name : biolims.common.yes
			} ]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'ifCall',
		hidden : true,
		header:biolims.storage.ifCall,
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(ifCall),editor: ifCall
	});
	cm.push({
		dataIndex:'i5',
		hidden : false,
		header:'i5',
		width:20*6
	});
	cm.push({
		dataIndex:'i7',
		hidden : false,
		header:'i7',
		width:20*6
	});
	cm.push({
		dataIndex:'currentIndex',
		hidden : true,
		header:'排序号',
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/storage/showSpecSelectListJson.action?type="+$("#showType").val()+"&p_type="+$("#p_type").val();
	var opts={};
	opts.title=biolims.common.theInventoryMasterData;
	opts.height=document.body.clientHeight-150;
//	opts.rowselect=function(id){
//		$("#selectId").val(id);
//	};
	opts.rowdblclick=function(id,rec){
//		$('#selectId').val(id);
//		window.parent.setSpecFun(rec);
		setspecFun();
	};
	showSpecSelectGrid=gridTable("showSpecSelectdiv",cols,loadParam,opts);
	$("#showSpecSelectdiv").data("showSpecSelectGrid", showSpecSelectGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});


function disableEnter(event){
    var e = event || window.event || arguments.callee.caller.arguments[0];
     if(e && e.keyCode==13){ // enter 键
    	 commonSearchAction(showSpecSelectGrid);
    }
//	var keyCode = event.keyCode?event.keyCode:event.which?event.which:event.charCode;
//	if (keyCode ==13){
//		var batch=$("#code").val();
//		commonSearchAction(showStorageReagentBuyGrid);
//	}
}
function selkit(){
	commonSearchAction(showSpecSelectGrid);
}
