var gridGrid;
$(function() {
	var hid = false;
	var title = null;
	if($("#p_type").val() == 1) {
		title = biolims.common.mainDataOfReagents;
	} else {
		title = biolims.common.consumablesMasterData;
		hid = true
	}
	var cols = {};
	var fields = [];
	fields.push({
		name: 'id',
		type: 'string'
	});
	fields.push({
		name: 'name',
		type: 'string'
	});
	fields.push({
		name: 'spec',
		type: 'string'
	});
	fields.push({
		name: 'type-id',
		type: 'string'
	});
	fields.push({
		name: 'type-name',
		type: 'string'
	});
	fields.push({
		name: 'studyType-id',
		type: 'string'
	});
	fields.push({
		name: 'studyType-name',
		type: 'string'
	});
	fields.push({
		name: 'position-name',
		type: 'string'
	});
	fields.push({
		name: 'producer-name',
		type: 'string'
	});
	fields.push({
		name: 'num',
		type: 'string'
	});
	fields.push({
		name: 'unit-name',
		type: 'string'
	});
	fields.push({
		name: 'note',
		type: 'string'
	});
	fields.push({
		name: 'state-id',
		type: 'string'
	});
	fields.push({
		name: 'state-name',
		type: 'string'
	});
	fields.push({
		name: 'dutyUser-name',
		type: 'string'
	});
	fields.push({
		name: 'useDesc',
		type: 'string'
	});
	fields.push({
		name: 'searchCode',
		type: 'string'
	});
	fields.push({
		name: 'source-name',
		type: 'string'
	});
	fields.push({
		name: 'barCode',
		type: 'string'
	});
	fields.push({
		name: 'createUser-name',
		type: 'string'
	});
	fields.push({
		name: 'createDate',
		type: 'string'
	});
	fields.push({
		name: 'ifCall',
		type: 'string'
	});
	fields.push({
		name: 'i5',
		type: 'string'
	});
	fields.push({
		name: 'i7',
		type: 'string'
	});
	fields.push({
		name: 'i5Index',
		type: 'string'
	});
	fields.push({
		name: 'i7Index',
		type: 'string'
	});
	fields.push({
		name: 'kit-name',
		type: 'string'
	});
	fields.push({
		name: 'breed',
		type: 'string'
	});
	fields.push({
		name: 'purchaseState',
		type: 'string'
	});
	fields.push({
		name: 'jdeCode',
		type: 'string'
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex: 'id',
		header: biolims.common.id,
		width: 150,
		sortable: true,
		hidden: true
	});
	cm.push({
		dataIndex: 'jdeCode',
		header: "JDE Code",
		width: 100,
		sortable: true
	});
	cm.push({
		dataIndex: 'producer-name',
		header: biolims.equipment.producerName,
		width: 200,
		sortable: true,
		hidden: false
	});
	cm.push({
		dataIndex: 'barCode',
		header: "Catalog No.",
		width: 150,
		sortable: true,
		hidden: false
	});
	cm.push({
		dataIndex: 'kit-name',
		header: biolims.common.kitName,
		width: 150,
		hidden: hid,
		sortable: true
	});
	cm.push({
		dataIndex: 'name',
		header: biolims.common.groupName,
		width: 150,
		sortable: true
	});
	cm.push({
		dataIndex: 'searchCode',
		header: "Component Cat No.",
		width: 150,
		sortable: true,
	});
	cm.push({
		dataIndex: 'source-name',
		header: biolims.sample.sourceName,
		width: 150,
		sortable: true,
		hidden: true
	});
	cm.push({
		dataIndex: 'spec',
		header: biolims.equipment.spec,
		width: 150,
		sortable: true,
		hidden: true
	});
	cm.push({
		dataIndex: 'breed',
		header: biolims.common.brand,
		width: 150,
		sortable: true,
		hidden: true
	});
	cm.push({
		dataIndex: 'type-id',
		header: biolims.common.type,
		width: 100,
		sortable: true,
		hidden: true
	});
	cm.push({
		dataIndex: 'type-name',
		header: biolims.common.type,
		width: 100,
		sortable: true,
		hidden: true
	});
	cm.push({
		dataIndex: 'studyType-id',
		header: biolims.storage.studyType,
		width: 100,
		sortable: true,
		hidden: true
	});
	cm.push({
		dataIndex: 'studyType-name',
		header: biolims.storage.studyType,
		width: 100,
		sortable: true,
		hidden: true
	});
	cm.push({
		dataIndex: 'position-name',
		header: biolims.equipment.positionId,
		width: 100,
		sortable: true,
		hidden: true
	});
	cm.push({
		dataIndex: 'num',
		header: biolims.storage.num,
		width: 100,
		sortable: true,
	});
	cm.push({
		dataIndex: 'unit-name',
		header: biolims.common.unit,
		width: 80,
		sortable: false
	});
	cm.push({
		dataIndex: 'state-id',
		header: biolims.common.state,
		width: 75,
		sortable: true,
		hidden: true
	});
	cm.push({
		dataIndex: 'state-name',
		header: biolims.common.state,
		width: 75,
		sortable: true,
		hidden: true
	});
	cm.push({
		dataIndex: 'dutyUser-name',
		header: biolims.master.personName,
		width: 150,
		sortable: true,
		hidden: true
	});
	cm.push({
		dataIndex: 'useDesc',
		header: biolims.storage.useDesc,
		width: 150,
		sortable: true,
		hidden: true
	});
	cm.push({
		dataIndex: 'createUser-name',
		header: biolims.sample.createUserName,
		width: 75,
		sortable: true,
		hidden: true
	});
	cm.push({
		dataIndex: 'createDate',
		header: biolims.sample.createDate,
		width: 75,
		sortable: true,
		hidden: true
	});
	cm.push({
		dataIndex: 'ifCall',
		header: biolims.storage.ifCall,
		width: 150,
		sortable: true,
		hidden: true
	});
	cm.push({
		dataIndex: 'i5',
		header: 'i5',
		width: 75,
		sortable: true,
		hidden: true
	});
	cm.push({
		dataIndex: 'i7',
		header: 'i7',
		width: 150,
		sortable: true,
		hidden: true
	});
	cm.push({
		dataIndex: 'i5Index',
		header: biolims.storage.i5Index,
		width: 150,
		sortable: true,
		hidden: true
	});
	cm.push({
		dataIndex: 'i7Index',
		header: biolims.storage.i7Index,
		width: 150,
		sortable: true,
		hidden: true
	});
	cm.push({
		dataIndex: 'note',
		header: biolims.common.note,
		width: 200,
		sortable: false,
		id: 'note'
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx +
		'/storage/getStorageList.action?p_type=' +
		$("#p_type").val();
	var opts = {};
	opts.tbar = [];
	opts.title = title;
	opts.height = document.body.clientHeight * 0.95;
	opts.rowselect = function(id) {
		$("#selectId").val(id);
	};
	opts.rowdblclick = function(id) {
		$('#selectId').val(id);
		edit();
	};
	if($("#p_type").val() == 1) {

		opts.tbar.push({
			text: "<font color='red'>" + biolims.common.save + "</font>",
			handler: function() {
				var result = commonGetModifyRecords(gridGrid);
				if(result.length > 0) {
					ajax("post", "/storage/saveStorageItemList.action", {
						itemDataJson: result,
						p_type: $("#p_type").val()
					}, function(data) {
						if(data.success) {
							message(biolims.common.saveSuccess);
							gridGrid.getStore().commitChanges();
							gridGrid.getStore().reload();
						} else {
							message(biolims.common.saveFailed);
						}
					}, null);
				} else {
					message(biolims.common.noData2Save);
				}
			}
		});
		opts.tbar.push({
			text: biolims.common.uploadCSV,
			handler: function() {
				var options = {};
				options.width = 350;
				options.height = 200;
				loadDialogPage($("#bat_uploadcsv_div"), biolims.common.batchUpload, null, {
					"Confirm": function() {
						goInExcelcsvSvn();
						$(this).dialog("close");
					}
				}, true, options);
			}
		});
	}

	gridGrid = gridTable("grid", cols, loadParam, opts);
	$("#grid").data("gridGrid", gridGrid);
});

function exportexcel() {
	gridGrid.title = biolims.common.exportList;
	var vExportContent = gridGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}

function goInExcelcsvSvn() {
	var file = document.getElementById("file-uploadcsv").files[0];
	var n = 0;
	var ob = gridGrid.getStore().recordType;
	var reader = new FileReader();
	reader.readAsText(file, 'GB2312');
	reader.onload = function(f) {
		var csv_data = $.simple_csv(this.result);
		$(csv_data).each(function() {
			if(n > 0) {
				if(this[0]) {
					var p = new ob({});
					p.isNew = true;
					p.set("producer-name", this[0]); // 生产商名称
					p.set("barCode", this[1]); // Catalog No.
					p.set("jdeCode", this[2]); // JDE code
					p.set("kit-name", this[3]); // Kit Name
					p.set("name", this[4]); // Component Name
					p.set("searchCode", this[5]); // Component Cat No.
					p.set("i5", this[6]); // i5
					p.set("i7", this[7]); // i7
					p.set("state-id", this[8]); // 状态
					p.set("type-id", this[9]); // 类型
					p.set("studyType-id", this[10]); // 类别
					gridGrid.getStore().insert(0, p);
				}
			}
			n = n + 1;
		});
	};
}

function add() {
	window.location = window.ctx + "/storage/toEditStorage.action?p_type=" + $("#p_type").val();
}

function edit() {
	var id = "";
	id = document.getElementById("selectId").value;
	if(id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/storage/toEditStorage.action?id=' + id + '&p_type=' + $("#p_type").val();
}

function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if(id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/storage/toViewStorage.action?id=' + id + '&p_type=' + $("#p_type").val();
}

Ext.onReady(function() {
	Ext.QuickTips.init();

	var store = new Ext.data.JsonStore({
		id: 0,
		url: window.ctx + '/storage/findSearchCode.action',
		root: 'results',
		autoLoad: true,
		fields: ['code']
	});
	var combo = new Ext.form.ComboBox({
		store: store,
		displayField: 'code',
		typeAhead: false,
		mode: 'local',
		editable: true,
		triggerAction: 'all',
		selectOnFocus: true,
		width: 50,
		height: 21,
		onSelect: function(record) {
			document.getElementById("searchCode").value = record.data.code;
			commonSearchAction(gridGrid);
			this.collapse();
			combo.setValue(record.data.code);
		},
		renderTo: 'autosuggest'
	});

});
$(function() {

	$("#name").autocomplete({
		source: function(request, response) {
			var mmRsName = encodeURIComponent($("#name").val());
			$.ajax({
				url: "/storage/findStorageByNames.action",
				dataType: "json",
				data: {
					name: mmRsName
				},
				success: function(data) {

					if(data.length > 0) {
						response($.map(data, function(item) {
							return {
								label: item.name,
								value: item.name

							}
						}));
					}

					//response(data);
				}
			});
		},
		minLength: 1,
		select: function(event, ui) {
			$("#name").val(ui.item);
		},
		open: function() {
			$(this).removeClass("ui-corner-all").addClass("ui-corner-top");
		},
		close: function() {
			$(this).removeClass("ui-corner-top").addClass("ui-corner-all");
		}
	});

	$("#opensearch").click(function() {
		var option = {};
		option.width = 600;
		option.height = 442;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)": function() {
				commonSearchAction(gridGrid);
				$(this).dialog("close");
			},
			"清空(Empty)": function() {
				form_reset();
			}
		}, true, option);

	});

});