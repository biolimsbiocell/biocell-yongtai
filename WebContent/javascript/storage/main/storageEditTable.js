$(function() {

	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "view") {
		settextreadonly();
	}
	if(handlemethod=="modify"){
		$("#step_no").removeClass("disabled").addClass("done");
		$("#step_no2").removeClass("disabled").addClass("done");
	}
	var mainFileInput = fileInput('1', 'storage', $("#storage_id").val());
	fieldCustomFun();

})

function change(id) {
	$("#" + id).css({
		"background-color": "white",
		"color": "black"
	});
}

function add() {
	window.location = window.ctx + "/storage/toEditStorage.action";
}

function list() {
	window.location = window.ctx + '/storage/showStorageList.action?p_type=' + $("#p_type").val();
}

function newSave() {
	save();
}

function tjsp() {
	top.layer.confirm(biolims.common.pleaseConfirmSaveBeforeSubmit, {
		icon: 3,
		title: biolims.common.prompt,
		btn:biolims.common.selected
	}, function(index) {
		top.layer.open({
			title: biolims.common.submit,
			type: 2,
			anim: 2,
			area: ['800px', '500px'],
			btn: biolims.common.selected,
			content: window.ctx + "/workflow/processinstance/toStartView.action?formName=Storage",
			yes: function(index, layer) {
				var datas = {
					userId: userId,
					userName: userName,
					formId: $("#storage_id").val(),
					title: $("#storage_name").val(),
					formName: 'Storage'
				}
				ajax("post", "/workflow/processinstance/start.action", datas, function(data) {
					if(data.success) {
						top.layer.msg(biolims.common.submitSuccess);
						if(typeof callback == 'function') {
							callback(data);
						}
						dialogWin.dialog("close");
					} else {
						top.layer.msg(biolims.common.submitFail);
					}
				}, null);
				top.layer.close(index);
			},
			cancel: function(index, layer) {
				top.layer.close(index)
			}

		});
		top.layer.close(index);
	});
}

function sp() {

	var taskId = $("#bpmTaskId").val();
	var formId = $("#storage_id").val();

	top.layer.open({
		title: biolims.common.handle,
		type: 2,
		anim: 2,
		area: ['800px', '500px'],
		btn: biolims.common.selected,
		content: window.ctx + "/workflow/processinstance/toCompleteTaskView.action?taskId=" + taskId + "&formId=" + formId,
		yes: function(index, layer) {
			var operVal = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#oper").val();
			var opinionVal = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#opinionVal").val();
			var opinion = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#opinion").val();
			if(!operVal) {
				top.layer.msg(biolims.common.pleaseSelectOper);
				return false;
			}
			if(operVal == "2") {
				_trunTodoTask(taskId, callback, dialogWin);
			} else {
				var paramData = {};
				paramData.oper = operVal;
				paramData.info = opinion;

				var reqData = {
					data: JSON.stringify(paramData),
					formId: formId,
					taskId: taskId,
					userId: window.userId
				}
				ajax("post", "/workflow/processinstance/completeTask.action", reqData, function(data) {
					if(data.success) {
						top.layer.msg(biolims.common.submitSuccess);
						if(typeof callback == 'function') {}
					} else {
						top.layer.msg(biolims.common.submitFail);
					}
				}, null);
			}
			location.href = window.ctx + "/lims/pages/dashboard/dashboard.jsp";
		},
		cancel: function(index, layer) {
			top.layer.close(index)
		}

	});
}

function ck() {
	top.layer.open({
		title: biolims.common.checkFlowChart,
		type: 2,
		anim: 2,
		area: ['800px', '500px'],
		btn: biolims.common.selected,
		content: window.ctx + "/workflow/processinstance/toTraceProcessInstanceView.action?formId=" + $("#storage_id").val(),
		yes: function(index, layer) {
			top.layer.close(index)
		},
		cancel: function(index, layer) {
			top.layer.close(index)
		}
	});
}

/*function save() {
if(checkSubmit()==true){
		document.getElementById('storageReagentBuySerialJson').value = saveStorageReagentBuySerialjson($("#storageReagentBuySerialTable"));
		console.log($("#storageReagentBuySerialJson").val());
		form1.action = window.ctx
				+ "/storage/storage/save.action?loadNumber=1";
		form1.submit();
	}
}*/

function save() {
	
	/**
	 * 判断日期
	 */
	var arr = [];
	var arr1 = [];
	$("#storageReagentBuySerialTable tbody tr").each(function(i,v){
		arr.push($(v).find("td[savename='productDate']").text());
		arr1.push($(v).find("td[savename='inDate']").text());
		
	})
	var scTime = "";
	var rkTime = "";
	for(var i=0;i<arr.length;i++){
		 for(var j=0;j<arr.length;j++){
			 scTime = new Date(arr[i]).getTime();
			 rkTime = new Date(arr1[j]).getTime();
			 if(scTime>rkTime){
				  top.layer.msg("生产日期不能小于入库日期！");
				  return false;
			  }
		 }
		 
	}
	if(checkSubmit() == true) {
		//自定义字段
		//拼自定义字段儿（实验记录）
		var inputs = $("#fieldItemDiv input");
		var options = $("#fieldItemDiv option");
		var contentData = {};
		var checkboxArr = [];
		//必填验证
		var requiredField=requiredFilter();
			if(!requiredField){
				return false;
			}
		$("#fieldItemDiv .checkboxs").each(function(i, v) {
			$(v).find("input").each(function(ii, inp) {
				var k = inp.name;
				if(inp.checked == true) {
					checkboxArr.push(inp.value);
					contentData[k] = checkboxArr;
				}
			});
		});
		inputs.each(function(i, inp) {
			var k = inp.name;
			if(inp.type != "checkbox") {
				contentData[k] = inp.value;
			}
		});
		options.each(function(i, opt) {
			if(opt.selected == true) {
				var k = opt.getAttribute("name");
				contentData[k] = opt.value;
			}
		});
		document.getElementById("fieldContent").value = JSON.stringify(contentData);

		var changeLog = "";
		$('input[class="form-control"]').each(function(i, v) {
			var valnew = $(v).val();
			var val = $(v).attr("changelog");
			if(val !== valnew) {
				changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
			}
		});
		var jsonStr = JSON.stringify($("#form1").serializeObject());

		var dataItemJson = saveStorageReagentBuySerialjson($("#storageReagentBuySerialTable"));
		var dataItemComponentsJson = saveStorageReagentComponentsjson($("#storageReagentComponentsTable"));
		var ele = $("#storageReagentBuySerialTable");
		var eleComponents = $("#storageReagentComponentsTable");
		var changeLogItem = "物资采购明细：";
		changeLogItem = getChangeLog(dataItemJson, ele, changeLogItem);
		var changeLogComponentsItem = "组成成分：";
		changeLogComponentsItem = getComponentsChangeLog(dataItemComponentsJson, eleComponents, changeLogItem);


		$.ajax({
			url: ctx + '/storage/saveStorageTable.action',
			dataType: 'json',
			type: 'post',
			data: {
				dataValue: jsonStr,
				changeLog: changeLog,
				p_type: $("#p_type").val(),
				storageReagentBuySerialJson: dataItemJson,
				storageReagentComponentsJson: dataItemComponentsJson,
				bpmTaskId: $("#bpmTaskId").val(),
				changeLogItem: changeLogItem,
				changeLogComponentsItem: changeLogComponentsItem
			},
			success: function(data) {
				if(data.success) {
					var url = "/storage/toEditStorage.action?id=" + data.id;

					url = url + "&bpmTaskId=" + $("#bpmTaskId").val() + "&p_type=" + $("#p_type").val();

					window.location.href = url;
				} else {
					top.layer.msg(data.msg);

				}
			}

		});

	}
}


//使用单位
function unitBatch() {
	top.layer.open({
		title: biolims.common.batchUnit,
		type: 2,
		area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/unit/showUnitSelectTable.action", ''],
		yes: function(index, layero) {
			var type = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUnitTable .chosed").children("td")
				.eq(0).attr("id");
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUnitTable .chosed").children("td")
			.eq(0).text();
			$("#storage_unit_id").val(type);
			$("#storage_unit_name").val(name);
			top.layer.close(index)
		},
	})

}

$.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if(o[this.name]) {
			if(!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};

function editCopy() {
	window.location = window.ctx + '/storage/toCopyStorage.action?id=' + $("#storage_id").val();
}

function changeState() {
	var paraStr = "formId=" + $("#storage_id").val() +
		"&tableId=Storage";
	top.layer.open({
		title: biolims.common.changeState,
		type: 2,
		anim: 2,
		area: ['400px', '400px'],
		btn: biolims.common.selected,
		content: window.ctx +
			"/applicationTypeAction/applicationTypeActionLook.action?" + paraStr +
			"&flag=changeState'",
		yes: function(index, layer) {
			top.layer.confirm(biolims.common.toSubmit, {
				icon: 3,
				title: biolims.common.prompt,
				btn:biolims.common.selected
			}, function(index) {
				ajax("post", "/applicationTypeAction/exeFun.action", {
					applicationTypeActionId: $('.layui-layer-iframe', parent.document).find("iframe").contents().find("input:checked").val(),
					formId: $("#storage_id").val()
				}, function(response) {
					var respText = response.message;
					if(respText == '') {
						window.location.reload();
					} else {
						top.layer.msg(respText);
					}
				}, null)
				top.layer.close(index);
			})
		},
		cancel: function(index, layer) {
			top.layer.close(index)
		}

	});
}

function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#storage_id").val());
	nsc.push("编号" + biolims.common.describeEmpty);
	fs.push($("#storage_state_name").val());
	nsc.push("状态" + biolims.common.describeEmpty);
	fs.push($("#storage_name").val());
	nsc.push("名称" + biolims.common.describeEmpty);
	fs.push($("#storage_type_name").val());
	nsc.push("类型" + biolims.common.describeEmpty);
	fs.push($("#storage_studyType_name").val());
	nsc.push("类别" + biolims.common.describeEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if(mess != "") {
		top.layer.msg(mess);
		return false;
	}
	return true;
}

function fileUp() {
	$("#uploadFile").modal("show");
}

function fileView() {
	top.layer.open({
		title: biolims.common.attachment,
		type: 2,
		skin: 'layui-top.layer-lan',
		area: ["650px", "400px"],
		content: window.ctx + "/operfile/initFileList.action?flag=1&modelType=storage&id=" + $("#storage_id").val(),
		cancel: function(index, layer) {
			top.layer.close(index)
		}
	})
}

function settextreadonly() {
	jQuery(":text, textarea").each(function() {
		var _vId = jQuery(this).attr('id');
		jQuery(this).css("background-color", "#B4BAB5").attr("readonly", "readOnly");
		if(_vId == 'actiondropdown_textbox')
			settextread(_vId);
	});
}

function settextread(name) {
	document.getElementById(name).className = "input_parts text input default  readonlyfalse false";
	document.getElementById(name).readOnly = false;
}

function showstate() {
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/state/dicStateSelectTable.action?flag=storage", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicStateTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicStateTable .chosed").children("td").eq(0).text();
			top.layer.close(index)
			$("#storage_state_id").val(id)
			$("#storage_state_name").val(name)
		},
	})
}

function showunitGroup() {
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/com/biolims/system/work/unitGroupNew/showWorkTypeDialogList.action", ''],
		yes: function(index, layer) {
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUnitGroup .chosed").children("td")
			.eq(0).text();
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUnitGroup .chosed").children("td")
				.eq(1).text();
			var mark2Name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUnitGroup .chosed").children("td")
			.eq(4).text();
//			rows.addClass("editagain");
			/*rows.find("td[savename='unitGroup-name']").attr("unitGroup-id",id);
			rows.find("td[savename='unitGroup-name']").text(name);
			rows.find("td[savename='unit']").text(mark2Name);*/
			top.layer.close(index);
			
			$("#storage_unitGroup_id").val(id)
			$("#storage_unitGroup_name").val(name)
			$("#storage_unitGroup_mark2").val(mark2Name)
			
			/*var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUnitTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUnitTable .chosed").children("td").eq(0).text();
			top.layer.close(index);
			$("#storage_unit_id").val(id)
			$("#storage_unit_name").val(name)*/
		},
	})
}
function showOutUnitGroup() {
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/com/biolims/system/work/unitGroupNew/showWorkTypeDialogList.action", ''],
		yes: function(index, layer) {
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUnitGroup .chosed").children("td")
			.eq(0).text();
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUnitGroup .chosed").children("td")
			.eq(1).text();
			var mark2Name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUnitGroup .chosed").children("td")
			.eq(4).text();
//			rows.addClass("editagain");
			/*rows.find("td[savename='unitGroup-name']").attr("unitGroup-id",id);
			rows.find("td[savename='unitGroup-name']").text(name);
			rows.find("td[savename='unit']").text(mark2Name);*/
			top.layer.close(index);
			
			$("#storage_outUnitGroup_id").val(id)
			$("#storage_outUnitGroup_name").val(name)
			$("#storage_outUnitGroup_mark2").val(mark2Name)
			
			/*var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUnitTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUnitTable .chosed").children("td").eq(0).text();
			top.layer.close(index);
			$("#storage_unit_id").val(id)
			$("#storage_unit_name").val(name)*/
		},
	})
}

/*function showunit() {
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/unit/showUnitSelectTable.action", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUnitTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUnitTable .chosed").children("td").eq(0).text();
			top.layer.close(index);
			$("#storage_unit_id").val(id)
			$("#storage_unit_name").val(name)
		},
	})
}*/

function showcreateUser() {
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/core/user/selectUserTable.action?groupId=admin", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(0).text();
			top.layer.close(index)
			$("#storage_createUser").val(id)
			$("#storage_createUser_name").val(name)
		},
	})
}

function showposition() {
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/storage/newPosition/showStoragePositionDialogList.action", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addStoragePasition .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addStoragePasition .chosed").children("td").eq(0).text();
			$("#storage_position_id").val(id);
			$("#storage_position_name").val(name);
			top.layer.closeAll();
		},
	})
}

function showtype() {
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/storage/common/storageTypeSelectTable.action?type=1", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#storageTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#storageTypeTable .chosed").children("td").eq(0).text();
			top.layer.close(index);
			$("#storage_type_id").val(id)
			$("#storage_type_name").val(name)
		},
	})
}

function showdutyUser() {
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/core/user/selectUserTable.action?groupId=admin", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(0).text();
			top.layer.close(index)
			$("#storage_dutyUser_id").val(id)
			$("#storage_dutyUser_name").val(name)
		},
	})
}

function showproducer() {
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/supplier/common/supplierSelectTable.action?flag=customer", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#supplierSelectTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#supplierSelectTable .chosed").children("td").eq(0).text();
			top.layer.close(index);
			$("#storage_producer_id").val(id)
			$("#storage_producer_name").val(name)
		},
	})
}

function showkit() {
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=kit", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(0).text();
			top.layer.close(index)
			$("#storage_kit_id").val(id)
			$("#storage_kit_name").val(name)
		},
	})
}

function showstudyType() {
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=yjlx", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(0).text();
			top.layer.close(index)
			$("#storage_studyType_id").val(id)
			$("#storage_studyType_name").val(name)
		},
	})
}
//自定义模块
function fieldCustomFun() {

	//获取自定义字段值的相关内容
	var fieldContent = $("#fieldContent").val();

	//查找自定义列表的订单模块的此检测项目的相关内容
	$.ajax({
		type: "post",
		url: window.ctx + "/system/customfields/findFieldByModuleValue.action",
		data: {
			moduleValue: "Storage"
		},
		async: false,
		success: function(data) {
			var objValue = JSON.parse(data);
			if(objValue.success) {
				$.each(objValue.data, function(i, n) {
					var inputs = '';
					var disabled = n.readOnly ? ' ' : "disabled";
					var defaultValue = n.defaultValue ? n.defaultValue : ' ';
					if(n.fieldType == "checkbox") {
						var checkboxs = '';
						var singleOptionIdArry = n.singleOptionId.split("/");
						var singleOptionNameArry = n.singleOptionName.split("/");
						singleOptionIdArry.forEach(function(vv, jj) {
							checkboxs += '<input type="checkbox" lay-ignore name=' + n.fieldName + ' value=' + jj + ' title=' + singleOptionNameArry[vv] + '>' + singleOptionNameArry[vv] + '';
						});
						inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group checkboxs " changelog=""><span class="input-group-addon">' + n.label + '</span>' + checkboxs + '</div></div>';
					} else if(n.fieldType == "radio") {
						var options = '';
						var singleOptionIdArry = n.singleOptionId.split("/");
						var singleOptionNameArry = n.singleOptionName.split("/");
						singleOptionIdArry.forEach(function(vv, jj) {
							options += '<option name=' + n.fieldName + ' value=' + jj + '>' + singleOptionNameArry[vv] + '</option>';
						});
						inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><select class="form-control">' + options + '</select></div></div>';
					} else if(n.fieldType == "date") {
						inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><input type="text" name=' + n.fieldName + ' required=' + n.isRequired + ' ' + disabled + ' class="form-control datepick" value=' + defaultValue + '></div></div>';
					} else {
						if(n.isRequired!="false"){
							inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '<img class="requiredimage" src="/images/required.gif" /></span><input type=' + n.fieldType + ' name=' + n.fieldName + '   class="form-control" ' + disabled + ' value=' + defaultValue + '></div></div>';
						}else{
							inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><input type=' + n.fieldType + ' name=' + n.fieldName + '   class="form-control" ' + disabled + ' value=' + defaultValue + '></div></div>';
						}
					}
					$("#fieldItemDiv").append(inputs);
				});

			} else {
				top.layer.msg(biolims.customList.updateFailed);
			}
		}
	});

	//显示自定义字段的数据
	if(fieldContent != null && fieldContent != undefined && fieldContent != "") {
		if(fieldContent && fieldContent != "null") {
			var contentData = JSON.parse(fieldContent);
			for(var k in contentData) {
				$("#fieldItemDiv input").each(function(i, inp) {
					if(inp.name == k) {
						if(inp.type == "checkbox") {
							if(contentData[k].indexOf(inp.value) != -1) {
								inp.setAttribute("checked", true);
							}
						} else {
							inp.value = contentData[k];
						}
						inp.setAttribute("changelog", inp.value);
					}
				});
				$("#fieldItemDiv option").each(function(i, val) {
					if(k == val.getAttribute("name")) {
						if(val.value == contentData[k]) {
							val.setAttribute("selected", true);
							//inp.setAttribute("changelog", inp.value);
						}
					}
				});
			};
		}
	}

	//日期格式化
	$("#fieldItemDiv").find('.datepick').datepicker({
		language: "zh-TW",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});
	//多选框 格式化
	$("#fieldItemDiv").find('input[type="checkbox"]').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		increaseArea: '20%' // optional
	});

}