var storageGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'spec-id',
		type:"string"
	});
	    fields.push({
		name:'spec-name',
		type:"string"
	});
	    fields.push({
		name:'properNum',
		type:"string"
	});
	    fields.push({
		name:'factNum',
		type:"string"
	});
	    fields.push({
		name:'inOrOut',
		type:"string"
    });
	    fields.push({
		name:'payNum',
		type:"string"
	});
	    fields.push({
		name:'giveNum',
		type:"string"
	});
	    fields.push({
		name:'lot',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
//	    fields.push({
//		name:'note',
//		type:"string"
//	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.sequencing.refCode,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.designation,
		width:20*15,
		
		sortable:true
	});
	cm.push({
		dataIndex:'spec-id',
		header:biolims.storage.specId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'spec-name',
		header:biolims.storage.specName,
		width:20*10,
		
		sortable:true
	});
	cm.push({
		dataIndex:'properNum',
		header:biolims.storage.properNum,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'factNum',
		header:biolims.storage.factNum,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'inOrOut',
		header:biolims.storage.inOrOut,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'payNum',
		header:biolims.storage.payNum,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'giveNum',
		header:biolims.storage.giveNum,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'lot',
		header:'Lot',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'createUser-id',
		header : biolims.sample.createUserId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'createDate',
		header:biolims.common.startDate,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*6,
		hidden:true,
		sortable:true
	});
	var stateName = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '1',
				name : biolims.master.valid
			}, {
				id : '0',
				name : biolims.master.invalid
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'stateName',
		hidden : false,
		header : biolims.common.state,
		width:15*6,
		renderer: Ext.util.Format.comboRenderer(stateName),
//		editor: stateName
	});
//	cm.push({
//		dataIndex:'stateName',
//		header:'状态',
//		width:20*6,
//		
//		sortable:true
//	});
//	cm.push({
//		dataIndex:'note',
//		header:'备注',
//		width:20*10,
//		
//		sortable:true
//	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/storage/showStorageListJson.action";
	var opts={};
	opts.title=biolims.common.theInventoryMasterData;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	storageGrid=gridTable("storagediv",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/storage/editStorage.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/storage/editStorage.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/storage/viewStorage.action?id=' + id;
}
function exportexcel() {
	storageGrid.title = biolims.common.exportList;
	var vExportContent = storageGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				if (($("#startreciveDate").val() != undefined) && ($("#startreciveDate").val() != '')) {
					var startreciveDatestr = ">=##@@##" + $("#startreciveDate").val();
					$("#reciveDate1").val(startreciveDatestr);
				}
				if (($("#endreciveDate").val() != undefined) && ($("#endreciveDate").val() != '')) {
					var endreciveDatestr = "<=##@@##" + $("#endreciveDate").val();

					$("#reciveDate2").val(endreciveDatestr);

				}
				
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}
				
				
				commonSearchAction(storageGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
