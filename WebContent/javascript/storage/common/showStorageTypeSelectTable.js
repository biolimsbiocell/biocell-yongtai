$(function() {
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : biolims.common.id,
	});
	colOpts.push({
		"data" : "name",
		"title" : biolims.common.sname,
	});
	colOpts.push({
		"data" : "state",
		"title" : biolims.common.state,
	});
	var tbarOpts = [];
	var storageTypeSelectTableOptions = table(false, null,
			'/storage/common/storageTypeSelectTableJson.action?type=' + $("#type").val(),
			colOpts, tbarOpts)
	var StorageTypeSelectTable = renderData($("#storageTypeTable"), storageTypeSelectTableOptions);
	$("#storageTypeTable").on(
			'init.dt',
			function(e, settings) {
				// 清除操作按钮
				$("#storageTypeTable_wrapper .dt-buttons").empty();
				$('#storageTypeTable_wrapper').css({
					"padding": "0 16px"
				});
			var trs = $("#storageTypeTable tbody tr");
			StorageTypeSelectTable.ajax.reload();
			StorageTypeSelectTable.on('draw', function() {
				trs = $("#storageTypeTable tbody tr");
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			});
			trs.click(function() {
				$(this).addClass("chosed").siblings("tr")
					.removeClass("chosed");
			});
			});
})

