var fromGrid;
Ext.onReady(function() {
			Ext.QuickTips.init();
			Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
			var sm = new Ext.grid.CheckboxSelectionModel();
			Ext.grid.RowNumberer = Ext.extend(Ext.grid.RowNumberer, {
				width : 30,
				renderer : function(value, cellmeta, record, rowIndex,
						columnIndex, store) {
					return store.lastOptions.params.start + rowIndex + 1;
				}
			});
var store = new Ext.data.JsonStore({
										root : 'results',
						totalProperty : 'total',
						remoteSort : true,
						fields : [ {
							name : 'id',
							type : 'string'
						}, {
							name : 'name',
							type : 'string'
						}, {
							name : 'kit-name',
							type : 'string'
						}, {
							name : 'breed',
							type : 'string'
						}, {
							name : 'type-id',
							type : 'string'
						}, {
							name : 'type-name',
							type : 'string'
						}, {
							name : 'department-id',
							type : 'string'
						}, {
							name : 'department-name',
							type : 'string'
						}, {
							name : 'spec',
							type : 'string'
						}, {
							name : 'num',
							type : 'float'
						}, {
							name : 'prepareNum',
							type : 'float'
						}, {
							name : 'outPrice',
							type : 'float'
						}, {
							name : 'unit-name',
							type : 'string'
						}, {
							name : 'producer-name',
							type : 'string'
						}, {
							name : 'source-name',
							type : 'string'
						}, {
							name : 'searchCode',
							type : 'string'
						}, {
							name : 'jdeCode',
							type : 'string'
						}, {
							name : 'currencyType-name',
							type : 'string'
						}, {
							name : 'position-id',
							type : 'string'
						}, {
							name : 'position-name',
							type : 'string'
						} ],
						proxy : new Ext.data.HttpProxy({url: ctx+'/storage/common/showMainStorageAllSelectListJson.action?type=1',method: 'POST'})
});

fromGrid = new Ext.grid.GridPanel({
										autoWidth : true,
						id : fromGrid,
						titleCollapse : true,
						autoScroll : true,
						height : parent.document.body.clientHeight - 120,
						title : biolims.common.theInventoryMasterData,
						store : store,
						columnLines : true,
						trackMouseOver : true,
						loadMask : true,
						columns : [ new Ext.grid.RowNumberer(), sm, {
							dataIndex : 'id',
							header : biolims.common.id,
							width : 280,
							sortable : true,
							hidden : true
						}, {
							dataIndex : 'name',
							header : biolims.common.componentName,
							width : 280,
							sortable : true
						}, {
							dataIndex : 'kit-name',
							header : biolims.common.kitName,
							width : 280,
							sortable : true
						}, {
							dataIndex : 'breed',
							header : biolims.common.brand,
							width : 200,
							sortable : true,
							hidden : true
						}, {
							dataIndex : 'type-id',
							header : biolims.common.typeID,
							width : 150,
							sortable : true,
							hidden : true
						}, {
							dataIndex : 'type-name',
							header : biolims.common.type,
							width : 150,
							sortable : true,
							hidden : true
						}, {
							dataIndex : 'department-id',
							header : biolims.equipment.departmentId,
							width : 150,
							sortable : true,
							hidden : true
						}, {
							dataIndex : 'department-name',
							header : biolims.equipment.departmentName,
							width : 150,
							sortable : true,
							hidden : true
						}, {
							dataIndex : 'spec',
							header : biolims.storage.spec,
							width : 250,
							sortable : true,
							hidden : true
						}, {
							dataIndex : 'num',
							header : biolims.storage.num,
							width : 150,
							sortable : true
						}, {
							dataIndex : 'unit-name',
							header : biolims.common.unit,
							width : 150,
							sortable : false
						}, {
							dataIndex : 'prepareNum',
							header : biolims.common.reservedNumber,
							width : 150,
							sortable : true,
							hidden : true
						}, {
							dataIndex : 'outPrice',
							header : biolims.sample.price,
							width : 150,
							sortable : true,
							hidden : true
						}, {
							dataIndex : 'producer-name',
							header : biolims.equipment.producerName,
							width : 150,
							sortable : true
						}, {
							dataIndex : 'source-name',
							header : biolims.sample.sourceName,
							width : 150,
							sortable : true,
							hidden : true
						}, {
							dataIndex : 'searchCode',
							header : 'Component Cat No.',
							width : 150,
							sortable : true,
							hidden : false
						}, {
							dataIndex : 'jdeCode',
							header : 'JDE Code',
							width : 150,
							sortable : true
						}, {
							dataIndex : 'currencyType-name',
							header : biolims.storage.rankType,
							width : 100,
							sortable : true,
							hidden : true
						}, {
							dataIndex : 'position-id',
							header : biolims.common.storageId,
							width : 100,
							sortable : true,
							hidden : true
						}, {
							dataIndex : 'position-name',
							header : biolims.common.storageName,
							width : 100,
							sortable : true,
							hidden : true
						} ],
						stripeRows : true,
						selModel : sm,
						viewConfig : {
							forceFit : true,
							enableRowBody : false,
showPreview:false
},
bbar: new Ext.PagingToolbar({id : 'bbarId',pageSize : parseInt((document.body.clientHeight - 70) > 25 ? (document.body.clientHeight - 70) / 25
		: 0),
									store : store,
									displayInfo : true,
									displayMsg : biolims.common.displayMsg,
									beforePageText : biolims.common.page,
									afterPageText : biolims.common.afterPageText,
									emptyMsg : biolims.common.noData,
									plugins : new Ext.ui.plugins.ComboPageSize(
											{
												addToItem : false,
												prefixText : biolims.common.show,
												postfixText : biolims.common.entris
											})
								})
					});
			fromGrid.render('from');
			fromGrid.getSelectionModel().on('rowselect',
					function(sm, rowIdx, r) {
						document.getElementById("id").value = r.data.id;
					});
			fromGrid.on('rowdblclick', function() {
				var record = fromGrid.getSelectionModel().getSelections();
				document.getElementById("id").value = record[0].data.id;
				edit();
			});
			store.on('beforeload', function() {
				store.baseParams = {
					data : document.getElementById('extJsonDataString').value
				};
			});
			store.load({
						params : {
							start : 0,
							limit : parseInt((document.body.clientHeight - 70) > 25 ? (document.body.clientHeight - 70) / 25
									: 0)
						}
					});
		});
$(function() {
	$("#name").autocomplete({
		source : function(request, response) {
			var mmRsName = encodeURIComponent($("#name").val());
			$.ajax({
				url : "/storage/findStorageByName.action",
				dataType : "json",
				data : {
					name : mmRsName
				},
				success : function(data) {

					if (data.length > 0) {
						response($.map(data, function(item) {
							return {
								label : item.name + " " + item.spec,
								value : item.name,
								spec : item.spec
							}
						}));
					}

					 response(data);
				}
			});
		},
		minLength : 1,
		select : function(event, ui) {
			$("#name").val(ui.item.spec);
			setTimeout("search()", 1000);

		},
		open : function() {
			$(this).removeClass("ui-corner-all").addClass("ui-corner-top");
		},
		close : function() {
			$(this).removeClass("ui-corner-top").addClass("ui-corner-all");
		}
	});

});
function formatCss(value, cellmeta, record, rowIndex, columnIndex, store) {

	cellmeta.css = "redFont";

	return value;

}
function setMainStorageValue() {
	var record = fromGrid.getSelectionModel().getSelections();
	var bool = true;
	var dataType = $("#dataType").val();
	if (dataType === 'apply') {
		$.each(record, function(i, obj) {
			if (obj.get("num") <= 0) {
				message(biolims.common.goodsNotUser);
				bool = false;
				return false;
			}
		});
	}
	if (bool) {
		window.parent.addStorageGrid(record);
	}
}
function query() {

	window.parent.showMainStorage();
}

function gotoValue() {
	var model = fromGrid.getSelectionModel();

	var record = model.getSelections();

	var store1 = fromGrid.store;
	var gridCount = store1.getCount();

	if (gridCount == 1) {
		model.selectAll();

		// model.selectRows(0);
		setMainStorageValue();
	}

}
function view() {
	if (trim(document.getElementById('id').value) == '') {

		message(biolims.common.selectRecord);
	} else {
		openDialog(window.ctx
				+ '/storage/toViewStorage.action?viewMode=true&noButton=true&id='
				+ document.getElementById('id').value);

	}
}

function searchComponent() {
	commonSearchAction(fromGrid);
}

function checkKey() {

	if (event.keyCode == 13) {
		search();
	}
}
