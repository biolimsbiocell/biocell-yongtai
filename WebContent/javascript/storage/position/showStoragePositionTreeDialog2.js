	
	Ext.onReady(function() {

		Ext.BLANK_IMAGE_URL = window.ctx+'/images/s.gif';
			Ext.QuickTips.init();
			function reloadtree() {
				var node = treeGrid.getSelectionModel().getSelectedNode();
				if(node==null){
					treeGrid.getLoader().load(treeGrid.getRootNode(), function(treeNode) {
						treeGrid.getRootNode().expand(true);
					}, this);
				}else{
					var path = treeGrid.getSelectionModel().getSelectedNode().getPath('id');
					treeGrid.getLoader().load(treeGrid.getRootNode(), function(treeNode) {
						treeGrid.expandPath(path, 'id', function(bSucess, oLastNode) {
							treeGrid.getSelectionModel().select(oLastNode);
						});
					}, this);
				}
			}
			col = [ {
				header : biolims.storage.positionId,
				dataIndex : 'id',
				width : 200
			}, {
				header : biolims.storage.positionName,
				width : 200,
				dataIndex : 'name'
			}, {
				header : biolims.storage.sampleNum,
				width : 200,
				dataIndex : 'storageObj'
			}, {
				header : biolims.common.type,
				width : 160,
				dataIndex : 'type-name'
			}, {
				header : biolims.common.state,
				width : 60,
				dataIndex : 'state-name'
			}, {
				header : biolims.storage.upPositionId,
				width : 160,
				dataIndex : 'upId'
			}, {
				header : biolims.storage.colNum,
				width : 60,
				dataIndex : 'colNum',
				hidden :true	
			}, {
				header : biolims.storage.rowNum,
				width : 60,
				dataIndex : 'rowNum',
				hidden :true	
			}];
			var tbl = [];
			var treeGrid = new Ext.ux.tree.TreeGrid({
				id:'treeGrid',
				width:parent.document.body.clientWidth-50,
				height: parent.document.body.clientHeight-80,
				renderTo: 'markup',
				enableDD: true,
				autoExpandColumn: 'common',
				columnLines:true,
				columns:col,
				root:new Ext.tree.AsyncTreeNode({  
		            id:'0',  
		            loader:new Ext.tree.TreeLoader({  
		                 dataUrl: $("#positionPath").val(),  
		                 listeners:{  
		                     "beforeload":function(treeloader,node)  
		                     {  
		                        treeloader.baseParams={  
		                        treegrid_id:node.id,  
		                        method:'POST'  
		                        };  
		                     }  
		                 }    
		            })  
		        }),  
		       
				listeners: {
				click: function(node,event)  
	            {  
					document.getElementById("id").value=node.attributes.id;
					document.getElementById("leaf").value=node.attributes.leaf;
	                if (node.isLeaf()) {  
	                    event.stopEvent();  
	                } else {  
	                    event.stopEvent();  
	                    node.toggle();  
	                }  
	            }  ,
	            dblclick: function(n) {
	            	window.parent.setBatchLocationFun(n.attributes.id);
				}
			}
		 });
	});


	   