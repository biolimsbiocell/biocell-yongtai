var storageTransferItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'cellId',
		type:"string"
	});
	   fields.push({
		name:'cellName',
		type:"string"
	});
	   fields.push({
		name:'saveUser',
		type:"string"
	});
	   fields.push({
		name:'project',
		type:"string"
	});
	   fields.push({
		name:'oldLocation',
		type:"string"
	});
	   fields.push({
		name:'newLocation',
		type:"string"
	});
	    fields.push({
		name:'storageTransfer-id',
		type:"string"
	});
	    fields.push({
		name:'storageTransfer-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'cellId',
		hidden : false,
		header:biolims.common.code,
		width:30*6
	});
	cm.push({
		dataIndex:'cellName',
		hidden : false,
		header:biolims.storage.cellName,
		width:30*6
	});
	cm.push({
		dataIndex:'saveUser',
		hidden : false,
		header:biolims.storage.saveUser,
		width:20*6
	});
	cm.push({
		dataIndex:'project',
		hidden : false,
		header:biolims.common.projectId,
		width:30*6
	});
	cm.push({
		dataIndex:'oldLocation',
		hidden : false,
		header:biolims.storage.oldLocation,
		width:20*6
	});
	cm.push({
		dataIndex:'newLocation',
		hidden : false,
		header:biolims.storage.newLocation,
		width:20*6
	});
	cm.push({
		dataIndex:'storageTransfer-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'storageTransfer-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:15*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/storage/position/storageTransfer/showStorageTransferItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.storage.materialTransferDetail;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/storage/position/storageTransfer/delStorageTransferItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				storageTransferItemGrid.getStore().commitChanges();
				storageTransferItemGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
    
	if($("#storageTransfer_state").val()==1){
//		   alert("工作流状态已完成，页面不可编辑。");
		 } else{
			 opts.tbar.push({
					text : biolims.storage.selectMaterial,
					handler : selectmaterailsMainFun
				});
			 
			
			opts.tbar.push({
				text : biolims.storage.selectNewLocation,
				handler : FrozenLocationFun
			});
			
			opts.tbar.push({
				text : biolims.storage.batchLocation,
				handler : BatchLocationFun
			});
	
		 }
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	 opts.tbar.push({
			text : biolims.common.editableColAppear,
			handler : null
		});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	
	storageTransferItemGrid=gridEditTable("storageTransferItemdiv",cols,loadParam,opts);
	$("#storageTransferItemdiv").data("storageTransferItemGrid", storageTransferItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

//选择材料
function selectmaterailsMainFun(){
			var options = {};
			options.width = 800;
			options.height = 660;
			var type = $("#storageTransfer_type").val();
			var url="/tra/materials/materailsMain/materailsMainSelect.action?typeId="+type;
			loadDialogPage(null, biolims.storage.selectMaterial, url, {
				 "Confirm": function() {
					 if(type=="0")
					      addCell();
					 else if(type=="1")
						 addPlasmid();
					 else if(type==2)
						 addBac();
					 options.close();
				}
			}, true, options);
		}
var d="";
var name="";
var projectName = "";
function setMaterailsMainFun(rec){
	var type = $("#storageTransfer_type").val(); 
	d = rec.get('id');
	name = rec.get('name');
	projectName = rec.get('project-name');
	 if(type=="0"){
		 var selectRecord = materailsMainDialogGrid.getSelectionModel();
			var selRecord = materailsMainCellDialogGrid.store;
			if (selectRecord.getSelections().length > 0) {
				if (selRecord && selRecord.getCount() > 0) {
					for(var i=0;i<selectRecord.getSelections().length;i++){
						var newv = selectRecord.getSelections()[i].get("id");
						for(var j=0;j<selRecord.getCount();j++){
							var oldv = selRecord.getAt(j).get("materailsMainId");
							if( oldv == newv ){
								message(biolims.common.haveDuplicate);
								return ;
							}
						}
					}
				}
			ajax("post", "/tra/materials/materailsOut/showMainCellList.action", {
				mainId : d
				}, function(data) {
					if (data.success) {
						var ob = materailsMainCellDialogGrid.getStore().recordType;
						materailsMainCellDialogGrid.stopEditing();
						$.each(data.data, function(i, obj) {	
							var p = new ob({});
							p.isNew = true;
							p.set("materailsMainId", d);
							p.set("materailsMainName", name);
							p.set("cellName", obj.name);
							p.set("cellId", obj.id);
							p.set("isMy", obj.isMy);
							p.set("generation", obj.generation);
							p.set("size", obj.size);
							p.set("num", obj.num);
							p.set("note", obj.note);
							p.set("frozenUser-name", obj.frozenUserName);
							p.set("strain-name", obj.strain);
							p.set("frozenLocation", obj.frozenLocation);
							p.set("frozenCellsNum", obj.frozenCellsNum);
							p.set("saveUser", obj.saveUser);
							materailsMainCellDialogGrid.getStore().add(p);
							
						});
						materailsMainCellDialogGrid.startEditing(0, 0);
					} else {
						message(biolims.common.anErrorOccurred);
					}
				}, null);
			}
	 }else if(type=="1"){
		 var selectRecord = materailsMainDialogGrid.getSelectionModel();
		 var selRecord = materailsMainPlasmidDialogGrid.store;
			if (selectRecord.getSelections().length > 0) {
				if (selRecord && selRecord.getCount() > 0) {
					for(var i=0;i<selectRecord.getSelections().length;i++){
						var newv = selectRecord.getSelections()[i].get("id");
						for(var j=0;j<selRecord.getCount();j++){
							var oldv = selRecord.getAt(j).get("materailsMainId");
							if( oldv == newv ){
								message(biolims.common.haveDuplicate);
								return ;
							}
						}
					}
				}
			ajax("post", "/tra/materials/materailsOut/showMainPlasmidList.action", {
				mainId : d
				}, function(data) {
					if (data.success) {
						var ob = materailsMainPlasmidDialogGrid.getStore().recordType;
						materailsMainPlasmidDialogGrid.stopEditing();
						$.each(data.data, function(i, obj) {
					
							var p = new ob({});
							p.isNew = true;
							p.set("materailsMainId", d);
							p.set("materailsMainName", name);
							p.set("plasmidId", obj.id);
							p.set("plasmidName", obj.name);
							p.set("typeName", obj.typeName);
							p.set("shortName", obj.shortName);
							p.set("property", obj.property);
							p.set("resistance", obj.resistance);
							p.set("reformUser-name", obj.reformUserName);
							p.set("reformDate", obj.reformDate);
							p.set("concentration", obj.concentration);
							p.set("num", obj.num);
							p.set("volume", obj.volume);
							p.set("frozenLocation", obj.frozenLocation);
							p.set("saveUser", obj.saveUser);
							materailsMainPlasmidDialogGrid.getStore().add(p);
						});
						materailsMainPlasmidDialogGrid.startEditing(0, 0);
					} else {
						message(biolims.common.anErrorOccurred);
					}
				}, null);
			}
	 }else if(type==2){
		 var selectRecord = materailsMainDialogGrid.getSelectionModel();
			var selRecord = materailsMainBACDialogGrid.store;
			if (selectRecord.getSelections().length > 0) {
				if (selRecord && selRecord.getCount() > 0) {
					for(var i=0;i<selectRecord.getSelections().length;i++){
						var newv = selectRecord.getSelections()[i].get("id");
						for(var j=0;j<selRecord.getCount();j++){
							var oldv = selRecord.getAt(j).get("materailsMainId");
							if( oldv == newv ){
								message(biolims.common.haveDuplicate);
								return ;
							}
						}
					}
				}
			ajax("post", "/tra/materials/materailsOut/showMainBacList.action", {
				mainId : d
				}, function(data) {
					if (data.success) {
						var ob = materailsMainBACDialogGrid.getStore().recordType;
						materailsMainBACDialogGrid.stopEditing();
						$.each(data.data, function(i, obj) {

							var p = new ob({});
							p.isNew = true;
							
							p.set("materailsMainId", d);
							p.set("materailsMainName", name);
							p.set("bacId", obj.id);
							p.set("bacName", obj.name);
							//p.set("materailsMainBac-type-id", obj.typeId);
							p.set("typeName", obj.typeName);
							p.set("englishName", obj.englishName);
							p.set("property", obj.property);
							p.set("resistance", obj.resistance);
							//p.set("materailsMainBac-hostType-id", obj.hostTypeId);
							p.set("hostTypeName", obj.hostTypeName);
							p.set("saveCondition", obj.saveCondition);
							p.set("specoesName", obj.specoesName);
							p.set("unitName", obj.unitName);
							p.set("num", obj.num);
							p.set("volume", obj.volume);
							p.set("frozenLocation", obj.frozenLocation);
							p.set("saveUser", obj.saveUser);
							materailsMainBACDialogGrid.getStore().add(p);
						});
						materailsMainBACDialogGrid.startEditing(0, 0);
					} else {
						message(biolims.common.anErrorOccurred);
					}
				}, null);
			}
	 }
		 
}

function addCell(){
	var selectRecord = materailsMainCellDialogGrid.getSelectionModel();
	var selRecord = storageTransferItemGrid.store;
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			var isRepeat = false;
			for(var j=0;j<selRecord.getCount();j++){
				var oldv = selRecord.getAt(j).get("cellId");
				if(oldv == obj.get("cellId")){
					isRepeat = true;
					break;
				}
			}
			if(!isRepeat){
			var ob = storageTransferItemGrid.getStore().recordType;
			storageTransferItemGrid.stopEditing();
			var p = new ob({});
			p.isNew = true;
			p.set("cellId", obj.get("cellId"));
			p.set("cellName", obj.get("materailsMainName"));
			p.set("saveUser", obj.get("saveUser"));
			p.set("project", projectName);
			p.set("oldLocation", obj.get("frozenLocation"));
			
			storageTransferItemGrid.getStore().add(p);
			}
			
		});
		storageTransferItemGrid.startEditing(0, 0);			
	}
}

function addPlasmid(){
	var selectRecord = materailsMainPlasmidDialogGrid.getSelectionModel();
	var selRecord = storageTransferItemGrid.store;
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			var isRepeat = false;
			for(var j=0;j<selRecord.getCount();j++){
				var oldv = selRecord.getAt(j).get("plasmidId");
				if(oldv == obj.get("plasmidId")){
					isRepeat = true;
					break;
				}
			}
			if(!isRepeat){
			var ob = storageTransferItemGrid.getStore().recordType;
			storageTransferItemGrid.stopEditing();
			var p = new ob({});
			p.isNew = true;
			p.set("cellId", obj.get("plasmidId"));
			p.set("cellName", obj.get("materailsMainName"));
			p.set("saveUser", obj.get("saveUser"));
			p.set("project", projectName);
			p.set("oldLocation", obj.get("frozenLocation"));
			
			storageTransferItemGrid.getStore().add(p);
			}
			
		});
		storageTransferItemGrid.startEditing(0, 0);			
	}
}

function addBac(){
	var selectRecord = materailsMainBACDialogGrid.getSelectionModel();
	var selRecord = storageTransferItemGrid.store;
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			var isRepeat = false;
			for(var j=0;j<selRecord.getCount();j++){
				var oldv = selRecord.getAt(j).get("bacId");
				if(oldv == obj.get("bacId")){
					isRepeat = true;
					break;
				}
			}
			if(!isRepeat){
			var ob = storageTransferItemGrid.getStore().recordType;
			storageTransferItemGrid.stopEditing();
			var p = new ob({});
			p.isNew = true;
			p.set("cellId", obj.get("bacId"));
			p.set("cellName", obj.get("materailsMainName"));
			p.set("saveUser", obj.get("saveUser"));
			p.set("project", projectName);
			p.set("oldLocation", obj.get("frozenLocation"));
			
			storageTransferItemGrid.getStore().add(p);
			}
			
		});
		storageTransferItemGrid.startEditing(0, 0);			
	}
}

//选择新位置
function FrozenLocationFun(){
		var selRecords = storageTransferItemGrid.getSelectionModel().getSelections(); 
		if(!selRecords||selRecords.length<1){
			message(biolims.common.pleaseSelect);
			return ;
		}
		for(var i=0;i<selRecords.length;i++){
			if(selRecords[i].get("id")==null){
				message(biolims.storage.pleaseSave2Setup);
				return ;
			}
		}
		 var win = Ext.getCmp('FrozenLocationFun');
		 if (win) {win.close();}
		 var ProjectFun= new Ext.Window({
		 id:'FrozenLocationFun',modal:true,title:biolims.storage.selectNewLocation,layout:'fit',width:600,height:580,closeAction:'close',
		 plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		 collapsible: true,maximizable: true,
		 items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
		 html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/storage/position/showStoragePositionTreeDialog.action?typeId="+$("#storageTransfer_type").val()+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
		 buttons: [
		 { text: biolims.common.close,
		  handler: function(){
		  ProjectFun.close(); }  }]  });     ProjectFun.show(); }

	 function setFrozenLocationFun(str){
		 	var gridGrid = $("#storageTransferItemdiv").data("storageTransferItemGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			var ads = str.split(",");
			$.each(selRecords, function(i, obj) {
				obj.set('newLocation',ads[i]);
			});
			
			 var win = Ext.getCmp('FrozenLocationFun')
			 if(win){win.close();}
			 }
	 
	//实验材料批量入库
		function BatchLocationFun(){
			var selRecords = storageTransferItemGrid.getSelectionModel().getSelections(); 
			
			if(selRecords.length>0){
				var win = Ext.getCmp('BatchLocationFun');
				 if (win) {win.close();}
				 var ProjectFun= new Ext.Window({
				 id:'BatchLocationFun',modal:true,title:biolims.storage.selectBatchLocation,layout:'fit',width:600,height:580,closeAction:'close',
				 plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
				 collapsible: true,maximizable: true,
				 items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
				 html:"<iframe scrolling='no' name='maincontentframe' src='/storage/position/showStoragePositionTreeDialog2.action?typeId="+$("#storageTransfer_type").val()+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
				 buttons: [
				 { text: biolims.common.close,
				  handler: function(){
				  ProjectFun.close(); }  }]  });     ProjectFun.show(); 
			}else{
				message(biolims.plasma.pleaseSelectWarehousing);
			}
		}
		
		 function setBatchLocationFun(str){
			var selRecords = storageTransferItemGrid.getSelectionModel().getSelections(); 
			var type = $("#storageTransfer_type").val();
			var ids = "";
			for(var i=0;i<selRecords.length;i++){
				if(selRecords[i].get("id")!=null){
					ids = ids + selRecords[i].get("id") + ",";
				}else{
					message(biolims.storage.pleaseSave2BatchLocation);
					return ;
				}
			}
			if(str!=null&&str!=""&&ids!=""){
				ajax("post", "/storage/position/storageTransfer/batchLocationFun.action", {
					idstr : ids,
					type : "0",
					classType: type,
					pid : str
					}, function(data) {
						if (data.success) {
							if(data){
								window.location = window.ctx + "/storage/position/storageTransfer/editStorageTransfer.action?id="+$("#storageTransfer_id").val();
//								message("位置分配成功。");
							}else{
								message(biolims.storage.locationAllocationFailure);
							}
						} else {
							message(biolims.common.anErrorOccurred);
						}
					}, null);
			}
			
			 var win = Ext.getCmp('BatchLocationFun')
			 if(win){win.close();}
		 }	