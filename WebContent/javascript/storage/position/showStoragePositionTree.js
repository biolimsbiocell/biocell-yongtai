function add(){
	var id="";
	id = document.getElementById("id").value;
	window.location= window.ctx +'/storage/position/toEditStoragePosition.action?upId='+id;

}
function edit(){
	var id="";
	id = document.getElementById("id").value;
	if(id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location= window.ctx +'/storage/position/toEditStoragePosition.action?id='+id;
}
function view(){
	var id="";
	id = document.getElementById("id").value;
	if(id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location= window.ctx +'/storage/position/toViewStoragePosition.action?id='+id;
}

	Ext.onReady(function() {

		Ext.BLANK_IMAGE_URL = window.ctx+'/images/s.gif';
			Ext.QuickTips.init();
			function reloadtree() {
				var node = treeGrid.getSelectionModel().getSelectedNode();
				if(node==null){
					treeGrid.getLoader().load(treeGrid.getRootNode(), function(treeNode) {
						treeGrid.getRootNode().expand(true);
					}, this);
				}else{
					var path = treeGrid.getSelectionModel().getSelectedNode().getPath('id');
					treeGrid.getLoader().load(treeGrid.getRootNode(), function(treeNode) {
						treeGrid.expandPath(path, 'id', function(bSucess, oLastNode) {
							treeGrid.getSelectionModel().select(oLastNode);
						});
					}, this);
				}
			}
			col = [ {
				header : biolims.storage.positionId,
				dataIndex : 'id',
				width : 200
			}, {
				header : biolims.storage.positionName,
				width : 200,
				dataIndex : 'name'
			}, {
				header : biolims.common.type,
				width : 160,
				dataIndex : 'type-name'
			}, {
				header : biolims.common.state,
				width : 60,
				dataIndex : 'state-name'
			}, {
				header : biolims.storage.upPositionId,
				width : 160,
				dataIndex : 'upId'
			}];
			var tbl = [];
			var treeGrid = new Ext.ux.tree.TreeGrid({
				id:'treeGrid',
				width:parent.document.body.clientWidth-50,
				height: parent.document.body.clientHeight-80,
				renderTo: 'markup',
				enableDD: true,
				autoExpandColumn: 'common',
				columnLines:true,
				columns:col,
				root:new Ext.tree.AsyncTreeNode({  
		            id:'0',  
		            loader:new Ext.tree.TreeLoader({  
		                 dataUrl: $("#positionPath").val(),  
		                 listeners:{  
		                     "beforeload":function(treeloader,node)  
		                     {  
		                        treeloader.baseParams={  
		                        treegrid_id:node.id,  
		                        method:'POST'  
		                        };  
		                     }  
		                 }    
		            })  
		        }),  
		       
				listeners: {
				click: function(node,event)  
	            {  
					document.getElementById("id").value=node.attributes.id;
					document.getElementById("leaf").value=node.attributes.leaf;
	                if (node.isLeaf()) {  
	                    event.stopEvent();  
	                } else {  
	                    event.stopEvent();  
	                    node.toggle();  
	                }  
	            }  ,
	            dblclick: function(n) {
//					document.getElementById("id").value=n.attributes.id;
//					document.getElementById("leaf").value=n.attributes.leaf;
					if(n.attributes.upId == null || n.attributes.upId==""){
						window.location= window.ctx +'/storage/position/editStoragePosition2w.action?id='+n.attributes.id;
					}
					//view(n.attributes.id);
					/*var isLeaf = document.getElementById("leaf").value;
					var str = "";
					while(isLeaf){
						str = document.getElementById("upId").value+str;
//						isLeaf = 
					}
					window.parent.setFrozenLocationFun(str);*/
				}
			}
		 });
//			treeGrid.getRootNode().expand(true);
	});
	$(function() {

		/*$("#opensearch").click(function() {
			var option = {};
			option.width = 650;
			option.height = 442;
			loadDialogPage($("#jstj"), "搜索", null, {
				"开始检索" : function() {
					commonSearchAction(gridGrid);
					$(this).dialog("close");
				},
				"清空" : function() {
					form_reset();
				}
			}, true, option);

		});
		load("/search/toSearch.action", {
			fkMdoel : $("#rightsId").val()
		}, $("#search_show_div"), null);*/

	});
	
	Ext.onReady(function(){
		var item = menu.add({
		    	text: biolims.storage.ewt
			});
		item.on('click', ewt);
	
		});
	function ewt(){
		var url =  ctx+"/storage/position/showStoragePicture.action";
		location.href = url;
	}
	
	Ext.onReady(function(){
		var item1 = menu.add({
		    	text: biolims.storage.szt
			});
		item1.on('click', szt);
		
		});
	function szt(){
		var url = ctx+"/storage/position/showStoragePositionTree.action";
	
		location.href = url;
	}