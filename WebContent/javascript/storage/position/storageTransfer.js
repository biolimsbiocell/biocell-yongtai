var storageTransferGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'type',
		type:"string"
	});
	    fields.push({
		name:'reason-id',
		type:"string"
	});
	    fields.push({
		name:'reason-name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'confirmUser-id',
		type:"string"
	});
	    fields.push({
		name:'confirmUser-name',
		type:"string"
	});
	    fields.push({
		name:'confirmDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:50*6,
		
		sortable:true
	});
	var typestore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.yes ], [ '0', biolims.common.no ]]
	});
	
	var typeComboxFun = new Ext.form.ComboBox({
		store : typestore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'type',
		header:biolims.common.type,
		width:15*6,
		
		renderer: Ext.util.Format.comboRenderer(typeComboxFun),				
		sortable:true
	});
		cm.push({
		dataIndex:'reason-id',
		hidden:true,
		header:biolims.storage.reasonId,
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'reason-name',
		header:biolims.storage.reasonName,
		
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:biolims.sample.createUserId,
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createDate,
		width:15*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'confirmUser-id',
		hidden:true,
		header:biolims.wk.confirmUserId,
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'confirmUser-name',
		header:biolims.wk.confirmUserName,
		
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'confirmDate',
		header:biolims.wk.confirmDate,
		width:15*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.workFlowState,
		width:40*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.workFlowState,
		width:40*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:biolims.common.note,
		width:50*6,
		
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/storage/position/storageTransfer/showStorageTransferListJson.action";
	var opts={};
	opts.title=biolims.storage.materialTransfer;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	storageTransferGrid=gridTable("show_storageTransfer_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/storage/position/storageTransfer/editStorageTransfer.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/storage/position/storageTransfer/editStorageTransfer.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/storage/position/storageTransfer/viewStorageTransfer.action?id=' + id;
}
function exportexcel() {
	storageTransferGrid.title = biolims.common.exportList;
	var vExportContent = storageTransferGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}
				
				if (($("#startconfirmDate").val() != undefined) && ($("#startconfirmDate").val() != '')) {
					var startconfirmDatestr = ">=##@@##" + $("#startconfirmDate").val();
					$("#confirmDate1").val(startconfirmDatestr);
				}
				if (($("#endconfirmDate").val() != undefined) && ($("#endconfirmDate").val() != '')) {
					var endconfirmDatestr = "<=##@@##" + $("#endconfirmDate").val();

					$("#confirmDate2").val(endconfirmDatestr);

				}
				
				
				commonSearchAction(storageTransferGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
