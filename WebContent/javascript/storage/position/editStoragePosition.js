//Ext.onReady(function() {
//	var sysCode = document.getElementById("sp.type.id").getAttribute("sysCode");
//	if (sysCode == '3d') {
//		// $("#store_obj_div").hide();
//		var url = "/materials/container/scanPositionContainer.action";
//		var data = {};
//		data.posiId = $("#sp_id").val();
//		data.contId = $("#storage_container_id").val();
//		if (data.contId) {
//			load(url, data, $("#store_obj_div"), null);
//		}
//	}
//});
 
function add() {
	var id = "";
	id = document.getElementById("id").value;
	window.location = window.ctx + '/storage/position/toEditStoragePosition.action?upId=' + id;

}

function getInfo() {
	var trs = $("#cont_table tr");
	var infos = [];
	$.each(trs, function(i, tr) {
		var tds = $(tr).children();
		$.each(tds, function(i, td) {
			var info = {};
			info.itemId = $(td).attr("itemId");
			if (info.itemId) {
				info.objName = $(td).attr("objName");
				info.ldDate = $(td).attr("ldDate");
				info.itemName = $(td).attr("itemName");
				info.num = $(td).attr("num");
				info.cellNum = $(td).attr("cellNum");
				infos.push(info);
			}
		});
	});
	console.log(infos);
	return infos;
}

function newSave() {
	if (checkSubmit() == false) {
		return false;

	}
	var myMask = new Ext.LoadMask(Ext.getBody(), {
		msg : biolims.common.pleaseWait
	});
	myMask.show();
	Ext.Ajax.request({
		url : window.ctx + '/common/hasId.action',
		method : 'POST',
		params : {
			id : $("#sp_id").val(),
			obj : 'StoragePosition'
		},
		success : function(response) {
			var respText = Ext.util.JSON.decode(response.responseText);
			myMask.hide();
			if (respText.message == '') {
				save();
			} else {
				message(respText.message);

			}

		},
		failure : function(response) {
		}
	});
}
function checkSubmit() {
	var mess = "";
	var fs = [ document.getElementById("sp_id").value, document.getElementById("sp.type.name").value ];
	var nsc = [ biolims.common.IdEmpty, biolims.common.typeIsEmpty ];
	mess = commonFieldsNotNullVerify(fs, nsc);

	if (mess != "") {
		message(mess);
		return false;
	}

	return true;

}
function save() {
	if (!checkSubmit()) {
		return;
	}

	var sysCode = document.getElementById("sp.type.id").getAttribute("sysCode");
	if (sysCode == '3d') {
		$("#container_info").val(JSON.stringify(getInfo()));
		var delItemIds = $("#delItemIds").val();
		if (delItemIds) {
			$("#container_del").val("'" + delItemIds + "'");
		}
	}
	$("#toolbarbutton_save").hide();
	document.getElementById("storagePositionForm").submit();
}

function list() {
	window.location = window.ctx + '/storage/position/showStoragePositionTree.action';
}
$(function() {
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "modify") {
		settextreadonly("sp_id");
	}
	if (handlemethod == "view") {
		settextreadonlyByAll();
	}
});
Ext.onReady(function() {
	var tabs = new Ext.TabPanel({
		renderTo : 'maintab',
		height : document.body.clientHeight-30,
		autoWidth : true,
		activeTab : 0,
		margins : '0 0 0 0',
		items : [ {
			title : biolims.common.storageLocalName,
			contentEl : 'markup'

		} ]
	});

});
setTimeout(function() {
	if($("#storage_container_id").val()){
//		load("/storage/container/sampleContainer.action", {
//			id : $("#storage_container_id").val()
//		}, "#3d_image", null);
//	
		
		load("/storage/container/showGridMidDataDialog.action?=", {
		idStr : $("#sp_id").val()
	}, "#3d_image", null);
		
	}
	
}, 700);
var item = menu.add({
	text: biolims.storage.generateOutboundOrder
	});
item.on('click', batchOut);
var item = menu.add({
	text: biolims.storage.materialTransfer
	});
item.on('click', batchTansfer);

//批量操作
function batchOut(){
	var obj = document.getElementsByName("box");
	var str = "";
	for(k in obj){
		if(obj[k].checked){
			str = str + obj[k].value + ","
		}
			
	}
	if(str!=""){
		if(confirm(biolims.storage.sure2Generate)==true){
			var type = $("#storage_storageType").val();
			ajax("post", "/storage/container/generateMaterialOut.action", {
				type : type,
				ids : str
				}, function(data) {
					if (data.success) {
						if(data.data){
							message(biolims.storage.generateSuccess);
						}else{
							message(biolims.storage.generateFailed);
						}
					} else {
						message(biolims.common.anErrorOccurred);
					}
				}, null);
		}
	}else{
		message(biolims.storage.pleaseSelectMaterial);
	}
}

//材料转移
function batchTansfer(){
	var obj = document.getElementsByName("box");
	var str = "";
	for(k in obj){
		if(obj[k].checked){
			str = str + obj[k].value + ","
		}
			
	}
	if(str!=""){
		if(confirm(biolims.storage.sure2TransferMaterial)==true){
			var type = $("#storage_storageType").val();
			ajax("post", "/storage/container/batchTransfer.action", {
				type : type,
				ids : str
				}, function(data) {
					if (data.success) {
						if(data.data){
							message(biolims.storage.generateTransferSuccess);
						}else{
							message(biolims.storage.generateTransferFailed);
						}
					} else {
						message(biolims.common.anErrorOccurred);
					}
				}, null);
		}
	}else{
		message(biolims.storage.pleaseSelectMaterial);
	}
}
Ext.onReady(function() {
	 Ext.QuickTips.init();
	Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
	var dicStorageType = Ext.get('dicStorageType');
	dicStorageType.on('click', 
	 function freezeType(){
	var win = Ext.getCmp('freezeType');
	if (win) {win.close();}
	var freezeType= new Ext.Window({
	id:'freezeType',modal:true,title:biolims.storage.selectClassification,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
	html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/dic/type/dicTypeSelect.action?type=freezeType&flag=freezeType' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
	 freezeType.close(); }  }]  });     freezeType.show(); }
	);
	});
	 function setfreezeType(id,name){
	 document.getElementById("sp.type.id").value = id;
	 document.getElementById("sp.type.name").value = name;
	 if(rec.get("sysCode")){
		if(rec.get("sysCode").indexOf(",")>0) {
			
			var a = rec.get("sysCode").split(",");
			$("#storage_rowNum").val(a[0]);
			$("#storage_colNum").val(a[1]);
		}
		 
	 }
	var win = Ext.getCmp('freezeType')
	if(win){win.close();}
	}
/*setTimeout(function() {
	var showChar = {};
	for ( var i = 65; i < 91; i++) {// ascii码A-H
		showChar[String.fromCharCode(i)] = i - 64;
	}
	var trs = $("#cont_table tr");

		
			var letter = 'B';
			var colNum = '2';
			var tds = $("td", trs[showChar[letter]]);
			var showDiv = $(tds[colNum]).find(".show-index-div");
			$(showDiv).removeClass();
			$(showDiv).html("<font color='blue'>ceshi</font>");
			



var height = $("#cont_table").height();
var width = $("#right").width() - 20;
new superTable("cont_table", {
	cssSkin : "sDefault",
	height : height,
	width : width,
	headerRows : 1,
	fixedCols : 1
});
}, 2000);*/
