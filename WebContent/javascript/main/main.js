function hyperlink_onclick() {
	var option = {};
	option.width = 393;
	option.height = 322;
	if (window.ActiveXObject) {
		// IE浏览器
		//option.height = option.height + "px";
	}
	var url = "/main/userSetView.action";
	var win = loadDialogPage(null, biolims.main.userSet, url, {
		"确定" : function() {
			userSet(win);
		}
	}, true, option);

}
function userSet(win) {
	var pwdreal = document.getElementById("pwdreal").value;
	var pwd = document.getElementById("user.password").value;
	if ($.trim(pwdreal) !== $.trim(pwd)) {
		message(biolims.main.password2);
		return;
	}
	var data = {};
	data.id = document.getElementById("user.id").value;
	data.oldPwd = document.getElementById("user.oldPassword").value;
	data.pwd = document.getElementById("user.password").value;
	data.mobile = document.getElementById("user.mobile").value;
	data.address = document.getElementById("user.address").value;
	data.emailPassword = document.getElementById("user_emailPassword").value;
	var url = "/main/userSet.action";
	ajax("post", url, data, function(data, textStatus, jqXHR) {
	
		if(data){
			message("信息修改成功！");
			win.dialog("close");
		}else{
			
			message("信息修改失败，请输入正确原始密码！");
		}
		//
	});
}
function closewest() {
	var w = Ext.getCmp('navtree');
	if (document.getElementById("wcollapseid").value == "true") {
		w.collapse();
	}
	if (document.getElementById("wcollapseid").value == "false") {
		w.expand();
	}
}
function openwest() {
	var w = Ext.getCmp('navtree');
	document.getElementById("wcollapseid").value = "false";
	w.expand();
}
function setclosewest() {
	var w = Ext.getCmp('navtree');
	document.getElementById("wcollapseid").value = "true";
}
function setalwaysopen() {
	document.getElementById("alwaysopen").value = "false";
}
$(function() {
	Ext.onReady(function() {
				Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
				Ext.QuickTips.init();

				var tree = new Ext.tree.TreePanel({
					id : 'testtree',
					rootVisible : false,
					useArrows : true,
					animate : false,
					enableDD : true,
					containerScroll : true,
					border : false,
					autoWidth : true,
					root : {
						nodeType : 'async'
					},
					sorters : [ {
						property : 'orderNumber',
						direction : 'ASC'
					}, {
						property : 'orderNumber',
						direction : 'ASC'
					} ],
					dataUrl : window.ctx + '/main/treeShowJson.action',
					listeners : {
						'click' : function(node) {
							if (node.leaf == true) {
								if (node.attributes.contenthref != '#') {
									
									if (window.document.getElementById("alwaysopen").value == 'false') {
										window.closewest();
									}
									
									if(node.attributes.contenthref.indexOf("report/frameset") >= 0 )   
			            			{ 
										
										window.open( window.ctx
												+ node.attributes.contenthref,"_blank","");
										
			            			}else{
//										window.frames["maincontentframe"].location.href = window.ctx
//										+ node.attributes.contenthref;
			            				window.frames[0].location = window.ctx + node.attributes.contenthref;
			            				
			            			}  
			            			    
									
									
									
									
								}
							} else {
								tree.expandPath(tree.getNodeById(node.id).getPath());// 展开
							}
						}
					}
				});
				var viewport = new Ext.Viewport(
						{
							id : 'border-example',
							layout : 'border',
							items : [
									new Ext.BoxComponent({
										region : 'north',
										contentEl : 'north'
									}),
									{
										id : 'navtree',
										region : 'west',
										split : true,
										width : 150,
										collapsible : true,
										collapsed : false,
										layout : 'accordion',
										minSize : 155,
										maxSize : 400,
										collapsible : true,
										margins : '0 0 0 0',
										cmargins : '0 0 0 0',
										layout : 'accordion',
										layoutConfig : {
											animate : true
										},
										items : [ {
											title : biolims.main.menu,
											items : tree,
											border : false,
											autoScroll : true,
											iconCls : 'settings'
										}, {
											title : biolims.main.other,
											html : '',
											border : false,
											autoScroll : true,
											iconCls : 'settings'
										} ]
									},
									new Ext.BoxComponent(
											{
												id : 'maincontent',
												region : 'center', 
												html : '<iframe scrolling="auto" name="maincontentframe" src='
														+ window.ctx + '/dashboard/toDashboard.action frameborder="0" width="100%" height="100%" ></iframe>'
											}) ]
						});
			});

});
