$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];

	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({
		name : 'searchCode',
		type : "string"
	});
	fields.push({
		name : 'spec',
		type : "string"
	});
	fields.push({
		name : 'department-name',
		type : "string"
	});
	fields.push({
		name : 'unit-name',
		type : "string"
	});
	fields.push({
		name : 'unit-id',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : biolims.common.instrumentNo,
		width : 100,
		sortable : true
	});
	cm.push({
		dataIndex : 'note',
		header : biolims.equipment.note,
		width : 250,
		sortable : true
	});
	cm.push({
		dataIndex : 'searchCode',
		header : biolims.storage.searchCode,
		width : 100,
		sortable : true
	});
	cm.push({
		dataIndex : 'spec',
		header : biolims.equipment.spec,
		width : 150,
		sortable : true
	});
	cm.push({
		dataIndex : 'department-name',
		header : biolims.equipment.departmentName,
		width : 100,
		sortable : true
	});
	cm.push({
		dataIndex : 'unit-id',
		header : biolims.common.unitId,
		hidden : 100,
		sortable : true
	});
	cm.push({
		dataIndex : 'unit-name',
		header : biolims.common.unit,
		width : 100,
		hidden : true
	});
	cols.cm = cm;
	var loadParam = {};
		
	loadParam.url = ctx + "/equipment/common/selEquipmentListJosn.action?eType="+$("#eType").val();
	
	var opts = {};
	//opts.title = "选择设备列表";
	opts.height = document.body.clientHeight-25;
	opts.tbar = [];
	opts.tbar.push({
		text : biolims.common.confirmSelected,
		handler : function(){
			
		
			var scRecord = selEquipmentGrid.getSelectRecord();
			window.parent.setScpInsGrid(scRecord);
		}
	});
	selEquipmentGrid = gridTable("sel_equipment_gird_div", cols, loadParam, opts);
	
	$("#sel_equipment_gird_div").data("selEquipmentGrid",selEquipmentGrid);
	
	/* 修改页签切换 */
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
	
	//$("#sel_search").click(function() {
	//	var data = {};
	//	data.id = $("#sel_search_id").val();
	//	data.note = $("#sel_search_note").val();
	//	selEquipmentGrid.getStore().baseParams.data = JSON.stringify(data);
	//	selEquipmentGrid.getStore().load();
	//});
	
});

function search(){
	var dialogsctGrid = $("#sel_equipment_gird_div").data("selEquipmentGrid");
	commonSearchAction(dialogsctGrid);
	}

function checkKey() {

	if (event.keyCode == 13) {
		search();
	}
}
