var gridGrid;
Ext
		.onReady(function() {
			Ext.QuickTips.init();
			Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
			Ext.grid.RowNumberer = Ext.extend(Ext.grid.RowNumberer, {
				width : 30,
				renderer : function(value, cellmeta, record, rowIndex,
						columnIndex, store) {
					return store.lastOptions.params.start + rowIndex + 1;
				}
			});
var store = new Ext.data.JsonStore({
										root : 'results',
						totalProperty : 'total',
						remoteSort : true,
						fields : [ {
							name : 'id',
							type : 'string'
						}, {
							name : 'appleUser-name',
							type : 'string'
						}, {
							name : 'stateName',
							type : 'string'
						}, {
							name : 'dutyUser-name',
							type : 'string'
						}, {
							name : 'newState-name',
							type : 'string'
						}, {
							name : 'note',
							type : 'string'
						} ],
proxy: new Ext.data.HttpProxy({url:ctx+ '/equipment/borrow/showInstrumentBorrowListJson.action?queryMethod=',method: 'POST'})
});
gridGrid = new Ext.grid.GridPanel({
										autoWidth : true,
						id : gridGrid,
						titleCollapse : true,
						autoScroll : true,
						height : document.body.clientHeight - 35,
						iconCls : 'icon-grid',
						title : biolims.equipment.instrumentAdjustment,
						collapsible : true,
						store : store,
						columnLines : true,
						trackMouseOver : true,
						loadMask : true,
						colModel : new Ext.ux.grid.LockingColumnModel([
								new Ext.grid.RowNumberer(), {
									dataIndex : 'id',
									header : biolims.equipment.instrumentAdjustmentId,
									width : 120,
									sortable : true
								}, {
									dataIndex : 'appleUser-name',
									header : biolims.sample.applyUserName,
									width : 150,
									sortable : true
								}, {
									dataIndex : 'stateName',
									header : biolims.common.workFlowStateName,
									width : 120,
									sortable : true
								}, {
									dataIndex : 'dutyUser-name',
									header : biolims.master.personName,
									width : 150,
									sortable : true,
									hidden : true
								}, {
									dataIndex : 'newState-name',
									header : biolims.equipment.newState,
									width : 150,
									sortable : true,
									hidden : false
								}, {
									dataIndex : 'note',
									header : biolims.equipment.adjustReason,
									width : 200,
									sortable : true,
									hidden : false
								} ]),
						stripeRows : true,
						view : new Ext.ux.grid.LockingGridView(),
						viewConfig : {
							forceFit : true,
							enableRowBody : false,
showPreview:false
},
bbar: new Ext.PagingToolbar({
									id : 'bbarId',
									pageSize : parseInt((document.body.clientHeight - 35) > 0 ? (document.body.clientHeight - 35) / 24
											: 1),
									store : store,
									displayInfo : true,
									displayMsg : biolims.common.displayMsg,
									beforePageText : biolims.common.page,
									afterPageText : biolims.common.afterPageText,
									emptyMsg : biolims.common.noData,
									plugins : new Ext.ui.plugins.ComboPageSize(
											{
												addToItem : false,
												prefixText : biolims.common.show,
												postfixText : biolims.common.entris
											})
								})
					});
			gridGrid.render('grid');
			gridGrid.getSelectionModel().on('rowselect',
					function(sm, rowIdx, r) {
						document.getElementById("id").value = r.data.id;
					});
			gridGrid.on('rowdblclick', function() {
				var record = gridGrid.getSelectionModel().getSelections();
				document.getElementById("id").value = record[0].data.id;
				edit();
			});
			store.on('beforeload', function() {
				store.baseParams = {
					data : document.getElementById('extJsonDataString').value
				};
			});
			store
					.load({
						params : {
							start : 0,
							limit : parseInt((document.body.clientHeight - 35) > 0 ? (document.body.clientHeight - 35) / 24
									: 1)
						}
					});
		});
function exportexcel() {
	gridGrid.title = biolims.common.exportList;
	var vExportContent = gridGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}

function list() {
	window.location = window.ctx
			+ '/equipment/borrow/showInstrumentBorrowList.action';
}

function add() {
	window.location = window.ctx
			+ "/equipment/borrow/toEditInstrumentBorrow.action";
}

function edit() {
	var id = "";
	id = document.getElementById("id").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/equipment/borrow/toEditInstrumentBorrow.action?id=' + id;
}

function view() {
	var id = "";
	id = document.getElementById("id").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/equipment/borrow/toViewInstrumentBorrow.action?id=' + id;
}
$(function() {

	$("#opensearch").click(function() {
		var option = {};
		option.width = 500;
		option.height = 180;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(gridGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);

	});

});