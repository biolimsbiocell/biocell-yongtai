
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
var instrumentBorrowDetailTab, oldChangeLog;
hideLeftDiv();
//设备维修明细表
$(function() {
	var colOpts = [];
	 colOpts.push({
			"data":"id",
			"title": biolims.tInstrumentBorrowDetail.id,
			"createdCell": function(td) {
				$(td).attr("saveName", "id");
		    },
			"className": "edit",
			"visible":	false,
		});
		colOpts.push( {
			"data": "instrument-id",
			"title": "EQL",
			"createdCell": function(td) {
				$(td).attr("saveName", "instrument-id");
			},
		"visible": false,
		});
		colOpts.push( {
			"data": "instrument-name",
			"title": biolims.tInstrumentState.instrumentName,
			"createdCell": function(td) {
				$(td).attr("saveName", "instrument-name");
			},
		});
		colOpts.push( {
			"data": "instrument-effectiveDate",
			"title": biolims.equipment.effectiveDate,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "instrument-effectiveDate");
			},
			"visible": false,
		});
		colOpts.push( {
			"data": "instrument-expiryDate",
			"title": biolims.equipment.expiryDate,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "instrument-expiryDate");
			},
			"visible": false,
		});
		colOpts.push( {
			"data": "instrument-nextDate",
			"title": biolims.equipment.nextDate,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "instrument-nextDate");
			},
		"visible": false,
		});
		colOpts.push( {
			"data": "instrument-spec",
			"title": biolims.equipment.spec,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "instrument-spec");
			}
		});
		colOpts.push( {
			"data": "roomManagement-id",
			"title": "房间编号",
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "roomManagement-id");
			}
		});
		colOpts.push( {
			"data": "roomManagement-roomName",
			"title": "房间名称 ",
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "roomManagement-roomName");
			}
		});
		colOpts.push({
			"data":"note",
			"title": biolims.tInstrumentBorrowDetail.note,
			"createdCell": function(td) {
				$(td).attr("saveName", "note");
		    },
			"className": "textarea",
		});
	var tbarOpts = [];
	/*	tbarOpts.push({
			text: biolims.common.fillDetail,
			action: function() {
				addItem($("#instrumentBorrowDetaildiv"))
		}
		});*/
	/*	tbarOpts.push({
			text: biolims.common.addwindow,
			action: function() {
				addItemLayer($("#instrumentBorrowDetaildiv"))
			}
		});*/
		tbarOpts.push({
			text: biolims.common.delSelected,
			action: function() {
				removeChecked($("#instrumentBorrowDetaildiv"),
					"/equipment/borrow/delInstrumentBorrowDetail.action");
			}
		});
	/*	tbarOpts.push({
			text: biolims.common.editwindow,
			action: function() {
				editItemLayer($("#instrumentBorrowDetaildiv"))
			}
		});*/
		tbarOpts.push({
			text: "选择房间",
			action: function() {
				showRoomManagement();
			}
		});
		tbarOpts.push({
			text : biolims.common.editwindow,
			action : function() {
				editItemLayer($("#instrumentBorrowDetaildiv"))
			}
		});
		tbarOpts.push({
			text: biolims.common.save,
			action: function() {
				saveinstrumentBorrowDetail($("#instrumentBorrowDetaildiv"));
			}
		});
	
	
	var instrumentBorrowDetailOps = table(true, $("#instrumentBorrow_id").text(), "/equipment/borrow/showInstrumentBorrowDetailTableJson.action", colOpts, tbarOpts);
	instrumentBorrowDetailTab = renderData($("#instrumentBorrowDetaildiv"), instrumentBorrowDetailOps);
	//选择数据并提示
	instrumentBorrowDetailTab.on('draw', function() {
		var index = 0;
		$("#instrumentBorrowDetaildiv .icheck").on('ifChanged', function(event) {
			if($(this).is(':checked')) {
				index++;
				$("#plateModal").find(".mysample[sid='" + this.value + "']").css("border","2px solid #000");
			} else {
				var tt=$("#plateModal").find(".mysample[sid='" + this.value + "']").css("border","1px solid gainsboro");
				index--;
			}
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
		oldChangeLog = instrumentBorrowDetailTab.ajax.json();
	});
});

// 保存
function saveinstrumentBorrowDetail() {
    var ele=$("#instrumentBorrowDetaildiv");
	var changeLog = biolims.equipment.instrumentAdjustment;
	var data = saveItemjson(ele);
	changeLog = getChangeLog(data, ele, changeLog);
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		type: 'post',
		url: '/equipment/borrow/saveInstrumentBorrowDetailTable.action',
		data: {
			id: $("#instrumentBorrow_id").text(),
			dataJson: data,
			logInfo: changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 添加样本类型ID
//			if(k == "dicSampleTypeName") {
//				json["dicSampleTypeId"] = $(tds[j]).attr("dicSampleTypeId");
//				continue;
//			}
			json[k] = $(tds[j]).text();
		}
		json.color = $(val).attr("background");
		data.push(json);
	});
	return JSON.stringify(data);
}

/*
 * 
 * 创建者 : 郭恒开
 * 创建日期: 2018/03/08
 * 文件描述: 修改日志
 * 
 */
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '数量为"' + v.num + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k] && k != "color") {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
function showRoomManagement(){
	var rows=$("#instrumentBorrowDetaildiv .selected");
	if(!rows.length){
		top.layer.msg("请选择数据！");
		return false;
	}
	top.layer.open({
		title: "选择房间",
		type: 2,
		offset: ['10%', '10%'],
		area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
		btn: biolims.common.selected,
		content: [ window.ctx + "/experiment/cell/passage/cellPassage/selectRoomTable.action", ""],
		yes: function(index, layero) {
			var name = [];
			var id=[];
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addRoomTable .selected").each(function(i, v) {
				id.push($(v).children("td").eq(1).text());
				name.push($(v).children("td").eq(2).text());
			})
			if(name.length>1||id.length>1){
				top.layer.msg("请选择一条数据！");
				return false;
			}else{
				rows.addClass("editagain");
				rows.find("td[savename='roomManagement-id']").text(id);
				rows.find("td[savename='roomManagement-roomName']").text(name);
				top.layer.close(index)	
			}
		},
	});
}