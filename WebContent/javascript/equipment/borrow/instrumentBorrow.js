var instrumentBorrowTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":biolims.tInstrumentBorrow.id,
	});
	
	    fields.push({
		"data":"appleUser-id",
		"title":biolims.sample.applyUserId,
	});
	    fields.push({
		"data":"appleUser-name",
		"title":biolims.tInstrumentBorrow.appleUser
	});
	
	    fields.push({
		"data":"stateName",
		"title":biolims.tInstrumentBorrow.stateName,
	});
	
	    fields.push({
		"data":"newState-id",
		"title":biolims.tInstrumentBorrow.newStateId
	});
	    fields.push({
		"data":"newState-name",
		"title":biolims.tInstrumentBorrow.newState
	});
	
	    fields.push({
		"data":"note",
		"title":biolims.tInstrumentBorrow.note,
	});
	
	    fields.push({
		"data":"handleDate",
		"title":biolims.tInstrumentBorrow.handleDate,
		"render": function(data) {
				if(data) {
					return parent.moment(data).format('YYYY-MM-DD');
				} else {
					return "";
				}
			}
	});
	

		$.ajax({
			type:"post",
			url:window.ctx + "/system/customfields/findFieldByModuleValue.action",
			async:false,
			data:{
				moduleValue : "InstrumentBorrow"
			},
			success:function(data){
				var objData = JSON.parse(data);
				if(objData.success){
					$.each(objData.data, function(i,n) {
						var str = {
							"data" : n.fieldName,
							"title" : n.label
						}
						colData.push(str);
					});
					
				}else{
					top.layer.msg("biolims.common.error");
				}
			}
		});

	var options = table(true, "","/equipment/borrow/showInstrumentBorrowTableJson.action",
	 fields, null)
	instrumentBorrowTable = renderRememberData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(instrumentBorrowTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
		"/equipment/borrow/editInstrumentBorrow.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/equipment/borrow/editInstrumentBorrow.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/equipment/borrow/viewInstrumentBorrow.action?id=" + id;
}
//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":biolims.tInstrumentBorrow.id
		});
	fields.push({
	    "type":"input",
		"searchName":"appleUser.id",
		"txt":biolims.sample.applyUserId
	});
	fields.push({
	    "type":"input",
		"searchName":"appleUser.name",
		"txt":biolims.tInstrumentBorrow.appleUser
	});
	   fields.push({
		    "searchName":"stateName",
			"type":"input",
			"txt":biolims.tInstrumentBorrow.stateName
		});
	fields.push({
	    "type":"input",
		"searchName":"newState.id",
		"txt":biolims.tInstrumentBorrow.newStateId
	});
	fields.push({
	    "type":"input",
		"searchName":"newState.name",
		"txt":biolims.tInstrumentBorrow.newState
	});
	   fields.push({
		    "searchName":"note",
			"type":"input",
			"txt":biolims.tInstrumentBorrow.note
		});
	   fields.push({
		    "searchName":"handleDate",
			"type":"input",
			"txt":biolims.tInstrumentBorrow.handleDate
		});
	fields.push({
			"txt": biolims.tInstrumentBorrow.handleDate+"(Start)",
			"type": "dataTime",
			"searchName": "handleDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.tInstrumentBorrow.handleDate+"(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "handleDate##@@##2"
		});
	
	fields.push({
		"type":"table",
		"table":instrumentBorrowTable
	});
	return fields;
}
