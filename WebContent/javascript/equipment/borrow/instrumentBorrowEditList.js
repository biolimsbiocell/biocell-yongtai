var instrumentBorrowTable;
var oldinstrumentBorrowChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表

	var tbarOpts = [];
	var colOpts = [];
	   colOpts.push({
		"data":"id",
		"title": biolims.tInstrumentBorrow.id,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
		"className": "edit"
	});
	colOpts.push( {
		"data": "appleUser-id",
		"title": biolims.sample.applyUserId,
		"createdCell": function(td) {
			$(td).attr("saveName", "appleUser-id");
		}
	});
	colOpts.push( {
		"data": "appleUser-name",
		"title": biolims.tInstrumentBorrow.appleUser,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "appleUser-name");
			$(td).attr("appleUser-id", rowData['appleUser-id']);
		}
	});
	   colOpts.push({
		"data":"stateName",
		"title": biolims.tInstrumentBorrow.stateName,
		"createdCell": function(td) {
			$(td).attr("saveName", "stateName");
	    },
		"className": "edit"
	});
	colOpts.push( {
		"data": "newState-id",
		"title": biolims.tInstrumentBorrow.newStateId,
		"createdCell": function(td) {
			$(td).attr("saveName", "newState-id");
		}
	});
	colOpts.push( {
		"data": "newState-name",
		"title": biolims.tInstrumentBorrow.newState,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "newState-name");
			$(td).attr("newState-id", rowData['newState-id']);
		}
	});
	   colOpts.push({
		"data":"note",
		"title": biolims.tInstrumentBorrow.note,
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
	    },
		"visible": false,	
		"className": "textarea"
	});
	   colOpts.push({
		"data":"handleDate",
		"title": biolims.tInstrumentBorrow.handleDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "handleDate");
	    },
		"className": "date"
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#instrumentBorrowTable"))
	}
	});
	tbarOpts.push({
		text: biolims.common.addwindow,
		action: function() {
			addItemLayer($("#instrumentBorrowTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.editwindow,
		action: function() {
			editItemLayer($("#instrumentBorrowTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.batchUpload,
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#instrumentBorrow_id").val(),
						fileId: data.response.fileId
					},
					url: ctx + "/equipment/borrow/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							 instrumentBorrowTable.ajax.reload();
						} else {
							top.layer.msg(biolims.common.uploadFailed)
						}
		
					}
				});
			});
			
		}
		});
	
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveInstrumentBorrow($("#instrumentBorrowTable"));
		}
	});
	}
	
	var instrumentBorrowOptions = 
	table(true, "","/equipment/borrow/showInstrumentBorrowTableJson.action",
	 colOpts, tbarOpts)
	
	
	
	instrumentBorrowTable = renderData($("#instrumentBorrowTable"), instrumentBorrowOptions);
	instrumentBorrowTable.on('draw', function() {
		oldinstrumentBorrowChangeLog = instrumentBorrowTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});

// 保存
function saveInstrumentBorrow(ele) {
	var data = saveInstrumentBorrowjson(ele);
	var ele=$("#instrumentBorrowTable");
	var changeLog = "biolims.equipment.instrumentAdjustment";
	changeLog = getChangeLog(data, ele, changeLog);
	$.ajax({
		type: 'post',
		url: '/equipment/borrow/saveInstrumentBorrowTable.action',
		data: {
			dataJson: data,
			changeLog:changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveInstrumentBorrowjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			
			if(k == "appleUser-name") {
				json["appleUser-id"] = $(tds[j]).attr("appleUser-id");
				continue;
			}
			
			if(k == "newState-name") {
				json["newState-id"] = $(tds[j]).attr("newState-id");
				continue;
			}
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		oldinstrumentBorrowChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
