var instrumentBorrowTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":biolims.tInstrumentBorrow.id,
	});
	
	    fields.push({
		"data":"appleUser-id",
		"title":biolims.sample.applyUserId
	});
	    fields.push({
		"data":"appleUser-name",
		"title":biolims.tInstrumentBorrow.appleUser
	});
	
	    fields.push({
		"data":"stateName",
		"title":biolims.tInstrumentBorrow.stateName,
	});
	
	    fields.push({
		"data":"newState-id",
		"title":biolims.tInstrumentBorrow.newStateId
	});
	    fields.push({
		"data":"newState-name",
		"title":biolims.tInstrumentBorrow.newState
	});
	
	    fields.push({
		"data":"note",
		"title":biolims.tInstrumentBorrow.note,
	});
	
	    fields.push({
		"data":"handleDate",
		"title":biolims.tInstrumentBorrow.handleDate,
	});
	
	var options = table(true, "","/equipment/borrow/showInstrumentBorrowTableJson.action",
	 fields, null)
	instrumentBorrowTable = renderData($("#addInstrumentBorrowTable"), options);
	$('#addInstrumentBorrowTable').on('init.dt', function() {
		recoverSearchContent(instrumentBorrowTable);
	})
});

//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":biolims.tInstrumentBorrow.id
		});
	fields.push({
	    "type":"input",
		"searchName":"appleUser.id",
		"txt":biolims.sample.applyUserId
	});
	fields.push({
	    "type":"input",
		"searchName":"appleUser.name",
		"txt":biolims.tInstrumentBorrow.appleUser
	});
	   fields.push({
		    "searchName":"stateName",
			"type":"input",
			"txt":biolims.tInstrumentBorrow.stateName
		});
	fields.push({
	    "type":"input",
		"searchName":"newState.id",
		"txt":biolims.tInstrumentBorrow.newStateId
	});
	fields.push({
	    "type":"input",
		"searchName":"newState.name",
		"txt":biolims.tInstrumentBorrow.newState
	});
	   fields.push({
		    "searchName":"note",
			"type":"input",
			"txt":biolims.tInstrumentBorrow.note
		});
	   fields.push({
		    "searchName":"handleDate",
			"type":"input",
			"txt":biolims.tInstrumentBorrow.handleDate
		});
	fields.push({
			"txt": biolims.tInstrumentBorrow.handleDate+"(Start)",
			"type": "dataTime",
			"searchName": "handleDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.tInstrumentBorrow.handleDate+"(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "handleDate##@@##2"
		});
	
	fields.push({
		"type":"table",
		"table":instrumentBorrowTable
	});
	return fields;
}
