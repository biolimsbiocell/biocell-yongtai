//待维修设备
var instrumentMaintainTempTab;
var instrumentMaintain_type_id = $("#instrumentMaintain_type_id").val();
var instrumentMaintainState = $("#instrumentMaintainState").val();
$(function() {
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : biolims.common.id,
		"visible":false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		},
	"visible":	false,
	});
	/*colOpts.push({
		"data" : "instrumentRepairPlanTask-id",
		"title" : "检定计划ID",
		"createdCell" : function(td) {
			$(td).attr("saveName", "instrumentRepairPlanTask-id");
		},
	"visible":	false,
	});*/
	colOpts.push({
		"data" : "instrumentRepairPlanTask-type-id",
		"title" :biolims.common.maintenanceTypeID,
		"visible":false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "instrumentRepairPlanTask-type-id");
		}
	});
	colOpts.push({
		"data": "instrumentRepairPlanTask-instrument-id",
		"title": "编号",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "instrumentRepairPlanTask-instrument-id");
		},
	})
	
	colOpts.push({
		"data" : "instrumentRepairPlanTask-instrument-name",
		"title" : biolims.tInstrumentState.instrumentName,
		"createdCell" : function(td) {
			$(td).attr("saveName", "instrumentRepairPlanTask-instrument-name");
		}
	})
	colOpts.push({
		"data" : "instrumentRepairPlanTask-type-name",
		"title" : biolims.tInstrumentRepair.type,
		"createdCell" : function(td) {
			$(td).attr("saveName", "instrumentRepairPlanTask-type-name");
		}
	})
	colOpts.push({
		"data" : "instrumentRepairPlanTask-period",
		"title" : biolims.tInstrumentMaintainItem.period,
		"createdCell" : function(td) {
			$(td).attr("saveName", "instrumentRepairPlanTask-period");
		}
	})
	colOpts.push({
		"data" : "instrumentRepairPlanTask-unit-name",
		"title" : biolims.tInstrumentMaintainItem.unit,
		"createdCell" : function(td) {
			$(td).attr("saveName", "instrumentRepairPlanTask-unit-name");
		}
	})
	  colOpts.push({
		"data":"instrumentRepairPlanTask-instrument-nextDate",
		"title": biolims.tInstrumentMaintainItem.nextDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "instrumentRepairPlanTask-instrument-nextDate");
	    }
	});
	colOpts.push({
		"data" : "instrumentRepairPlanTask-note",
		"title" : biolims.tInstrumentMaintainItem.note,
		"createdCell" : function(td) {
			$(td).attr("saveName", "instrumentRepairPlanTask-note");
		}
	})
	var tbarOpts = [];
	tbarOpts.push({
		text: '<i class="fa fa-ioxhost"></i>'+biolims.common.search,
		action:function(){
			search();
		}
	});
	
	  if("1"!=instrumentMaintainState){
		  tbarOpts.push({
				text : biolims.common.addToDetail,
				action : function() {
					var sampleId = [];
					$("#instrumentMaintainTempdiv .selected").each(function(i, val) {
						sampleId.push($(val).children("td").eq(0).find("input").val());
					});

					// 先保存主表信息
					var createUser = $("#instrumentMaintain_creatUser_id").text();
					var createDate = $("#instrumentMaintain_createDate").text();
					var state = $("#instrumentMaintain_state").text();
					var name = $("#instrumentMaintain_name").val();
					var note = $("#instrumentMaintain_note").val();
					var typeId = $("#instrumentMaintain_type_id").val();
					var id = $("#instrumentMaintain_id").text();
					var maintainDate = $("#instrumentMaintain_maintainDate").val();
					if(0>=sampleId.length){
						top.layer.msg(biolims.common.pleaseSelect);
						return;
					}
					$.ajax({
						type : 'post',
						url : '/equipment/maintain/addInstrumentMaintainItem.action',
						data : {
							ids : sampleId,
							name : name,
							state : state,
							id : id,
							createDate : createDate,
							createUser : createUser,
							maintainDate : maintainDate,
							typeId : typeId,
							note : note,
						},
						success : function(data) {
							var data = JSON.parse(data)
							if (data.success) {
								$("#instrumentMaintain_id").text(data.data);
								$("#instrumentMaintainId").val(data.data);
								var param = {
										id: data.data
									};	
								var instrumentMaintainItemTabs = $("#instrumentMaintainItemdiv")
									.DataTable();
								instrumentMaintainItemTabs.settings()[0].ajax.data = param;
								instrumentMaintainItemTabs.ajax.reload();
								var instrumentMaintainItemTabsss = $("#instrumentMaintainTempdiv").DataTable()
								instrumentMaintainItemTabsss.ajax.reload();
							} else {
								top.layer.msg(biolims.common.addToDetailFailed);
							}
							;
						}
					})
				}
			});

	  }

	var instrumentRepairTempOps = table(true, instrumentMaintain_type_id,
			"/equipment/maintain/showInstrumentMaintainTempJson.action", colOpts, tbarOpts);
	instrumentMaintainTempTab = renderData($("#instrumentMaintainTempdiv"), instrumentRepairTempOps);

	// 选择数据并提示
	instrumentMaintainTempTab.on('draw', function() {
		var index = 0;
		$("#instrumentMaintainTempdiv .icheck").on('ifChanged', function(event) {
			if ($(this).is(':checked')) {
				index++;
			} else {
				index--;
			}
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
	});
	bpmTask($("#bpmTaskId").val());
});






//大保存
function saveinstrumentRepairAndItem() {
		var changeLog = "";
		$('input[class="form-control"]').each(function(i, v) {
			var valnew = $(v).val();
			var val = $(v).attr("changelog");
			if(val !== valnew) {
				changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
			}
		});
		console.log($("#form1"));
		var jsonStr = JSON.stringify($("#form1").serializeObject());

		var dataItemJson = saveItemjson($("#instrumentRepairTaskdiv"));
		var ele = $("#instrumentMaintainTempdiv");
		var changeLogItem = "主数据入库明细：";
		changeLogItem = getChangeLog(dataItemJson, ele, changeLogItem);
		var id = $("#instrumentRepair_id").text();
		var createUser = $("#storageIn_handelUser").attr("userId");;
		var createDate = $("#storageIn_createDate").text();
		$.ajax({
			url: ctx + '/storage/storageIn/saveStorageInAndItem.action',
			dataType: 'json',
			type: 'post',
			data: {
				dataValue: jsonStr,
				changeLog: changeLog,
				ImteJson: dataItemJson,
				bpmTaskId: $("#bpmTaskId").val(),
				changeLogItem: changeLogItem,
				id:id,
				createUser:createUser,
				createDate:createDate
			},
			success: function(data) {
				if(data.success) {
					var url = "/equipment/repair/editInstrumentRepairNew.action?id=" + data.id;

					url = url + "&bpmTaskId=" + $("#bpmTaskId").val() + "&p_type=" + $("#p_type").val();

					window.location.href = url;
				} else {
					top.layer.msg(data.msg);

				}
			}

		});

}

$.fn.serializeObject = function()
{
   var o = {};
   var a = this.serializeArray();
   $.each(a, function() {
       if (o[this.name]) {
           if (!o[this.name].push) {
               o[this.name] = [o[this.name]];
           }
           o[this.name].push(this.value || '');
       } else {
           o[this.name] = this.value || '';
       }
   });
   return o;
};
function tjsp() {
	top.layer
			.confirm(
					biolims.common.pleaseConfirmSaveBeforeSubmit,
					{
						icon : 3,
						title : biolims.common.prompt
					},
					function(index) {
						top.layer
								.open({
									title : biolims.common.approvalTask,
									type : 2,
									anim : 2,
									area : [ '800px', '500px' ],
									btn : biolims.common.selected,
									content : window.ctx
											+ "/workflow/processinstance/toStartView.action?formName=DnaTask",
									yes : function(index, layer) {
										var datas = {
											userId : userId,
											userName : userName,
											formId : instrumentRepair_id,
											title : $("#instrumentRepair_name").val(),
											formName : "instrumentRepair"
										}
										ajax(
												"post",
												"/workflow/processinstance/start.action",
												datas,
												function(data) {
													if (data.success) {
														top.layer
																.msg(biolims.common.submitSuccess);
														if (typeof callback == 'function') {
															callback(data);
														}
														// dialogWin.dialog("close");
													} else {
														top.layer
																.msg(biolims.common.submitFail);
													}
												}, null);
										top.layer.close(index);
									},
									cancel : function(index, layer) {
										top.layer.close(index)
									}

								});
						top.layer.close(index);
					});

}
function sp() {
	var taskId = $("#bpmTaskId").val();
	var formId = instrumentRepair_id;

	top.layer.open({
		title : biolims.common.detailedExaminationAndApproval,
		type : 2,
		anim : 2,
		area : [ '800px', '500px' ],
		btn : biolims.common.selected,
		content : window.ctx
				+ "/workflow/processinstance/toCompleteTaskView.action?taskId="
				+ taskId + "&formId=" + formId,
		yes : function(index, layer) {
			var operVal = $('.layui-layer-iframe', parent.document).find("iframe").contents()
					.find("#oper").val();
			var opinionVal = $('.layui-layer-iframe', parent.document).find("iframe").contents()
					.find("#opinionVal").val();

			if (!operVal) {
				top.layer.msg(biolims.common.pleaseSelectOper);
				return false;
			}
			if (operVal == "2") {
				_trunTodoTask(taskId, callback, dialogWin);
			} else {
				var paramData = {};
				paramData.oper = operVal;
				paramData.info = opinionVal;

				var reqData = {
					data : JSON.stringify(paramData),
					formId : formId,
					taskId : taskId,
					userId : window.userId
				}
				ajax("post", "/workflow/processinstance/completeTask.action",
						reqData, function(data) {
							if (data.success) {
								top.layer.msg(biolims.common.submitSuccess);
								if (typeof callback == 'function') {
								}
							} else {
								top.layer.msg(biolims.common.submitFail);
							}
						}, null);
			}
			location.href = window.ctx + "/lims/pages/dashboard/dashboard.jsp";
		},
		cancel : function(index, layer) {
			top.layer.close(index)
		}

	});
}
function showtype(){
	top.layer.open({
	title:biolims.common.pleaseChoose,
	type:2,
	area:[document.body.clientWidth-300,document.body.clientHeight-100],
	btn: biolims.common.selected,
	content:[window.ctx+"/dic/type/dicTypeSelectTable.action?flag=whlx",''],
	yes: function(index, layer) {
	var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
	var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(0).text();
	top.layer.close(index)
		$("#instrumentMaintain_type_id").val(id)
		$("#instrumentMaintain_type_name").val(name)
				instrumentMaintainTempTab.settings()[0].ajax.data = {
				id: id,
			};			instrumentMaintainTempTab.ajax.reload();
	},
})
}
function searchOptions() { 
	return [{
		"txt" : biolims.tInstrumentState.instrumentName,
		"type" : "input",
		"searchName" : "instrumentRepairPlanTask-instrument-name",
	},{
		"txt" : biolims.common.instrumentNo,
		"type" : "input",
		"searchName" : "instrumentRepairPlanTask-instrument-id",
	}, 
	{
		"type" : "table",
		"table" : instrumentMaintainTempTab
	} ];
}