var instrumentMaintainTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":biolims.tInstrumentMaintain.id,
	});
	
	    fields.push({
		"data":"name",
		"title":biolims.tInstrumentMaintain.name,
	});
	
	    fields.push({
		"data":"createUser-id",
		"title":biolims.tInstrumentMaintain.createUser+"ID"
	});
	    fields.push({
		"data":"createUser-name",
		"title":biolims.tInstrumentMaintain.createUser
	});
	
	    fields.push({
		"data":"createDate",
		"title":biolims.tInstrumentMaintain.createDate,
	});
	
	    fields.push({
		"data":"state",
		"title":biolims.tInstrumentMaintain.state,
	});
	
	    fields.push({
		"data":"stateName",
		"title":biolims.tInstrumentMaintain.stateName,
	});
	
	    fields.push({
		"data":"maintainDate",
		"title":biolims.tInstrumentMaintain.maintainDate,
	});
	
	    fields.push({
		"data":"note",
		"title":biolims.tInstrumentMaintain.note,
	});
	
	var options = table(true, "","/equipment/maintain/showInstrumentMaintainTableJson.action",
	 fields, null)
	instrumentMaintainTable = renderData($("#addInstrumentMaintainTable"), options);
	$('#addInstrumentMaintainTable').on('init.dt', function() {
		recoverSearchContent(instrumentMaintainTable);
	})
});

//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":biolims.tInstrumentMaintain.id
		});
	   fields.push({
		    "searchName":"name",
			"type":"input",
			"txt":biolims.tInstrumentMaintain.name
		});
	fields.push({
	    "type":"input",
		"searchName":"createUser.id",
		"txt":biolims.tInstrumentMaintain.createUser+"ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"createUser.name",
		"txt":biolims.tInstrumentMaintain.createUser
	});
	   fields.push({
		    "searchName":"createDate",
			"type":"input",
			"txt":biolims.tInstrumentMaintain.createDate
		});
	fields.push({
			"txt": biolims.tInstrumentMaintain.createDate+"(Start)",
			"type": "dataTime",
			"searchName": "createDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.tInstrumentMaintain.createDate+"(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "createDate##@@##2"
		});
	   fields.push({
		    "searchName":"state",
			"type":"input",
			"txt":biolims.tInstrumentMaintain.state
		});
	   fields.push({
		    "searchName":"stateName",
			"type":"input",
			"txt":biolims.tInstrumentMaintain.stateName
		});
	   fields.push({
		    "searchName":"maintainDate",
			"type":"input",
			"txt":biolims.tInstrumentMaintain.maintainDate
		});
	fields.push({
			"txt": biolims.tInstrumentMaintain.maintainDate+"(Start)",
			"type": "dataTime",
			"searchName": "maintainDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.tInstrumentMaintain.maintainDate+"(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "maintainDate##@@##2"
		});
	   fields.push({
		    "searchName":"note",
			"type":"input",
			"txt":biolims.tInstrumentMaintain.note
		});
	
	fields.push({
		"type":"table",
		"table":instrumentMaintainTable
	});
	return fields;
}
