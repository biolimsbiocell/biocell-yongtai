var instrumentMaintainItemTab, oldChangeLog;
hideLeftDiv();
var instrumentMaintainState = $("#instrumentMaintainState").val();
//设备维护明细表
$(function() {
	var colOpts = [];
	
	
	colOpts.push( {
		"data": "id",
		"title": biolims.common.id,
		"visible":false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push( {
		"data": "instrumentMaintain-id",
		"title": biolims.crmDoctorItem.crmDoctorId,
		"visible":false,
		"createdCell": function(td) {
			$(td).attr("saveName", "instrumentMaintain-id");
		}
	});

	colOpts.push( {
		"data": "instrument-id",
		"title": "编号",
		"createdCell": function(td) {
			$(td).attr("saveName", "instrument-id");
		}
	});
	colOpts.push( {
		"data": "instrument-name",
		"title": "设备名称",
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "instrument-name");
			$(td).attr("instrument-id", rowData['instrumentr-id']);
		}
	});
	   colOpts.push({
		"data":"useDate",
		"title": biolims.tInstrumentMaintainItem.useDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "useDate");
	    }
	});
	   colOpts.push({
		"data":"nextDate",
		"title": "下次检测日期",
		"createdCell": function(td) {
			$(td).attr("saveName", "nextDate");
	    },
	});
	   colOpts.push({
		"data":"tsDate",
		"title": biolims.tInstrumentMaintainItem.tsDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "tsDate");
	    },
	});
	   colOpts.push({
		"data":"period",
		"title": biolims.tInstrumentMaintainItem.period,
		"createdCell": function(td) {
			$(td).attr("saveName", "period");
	    },
	});
	colOpts.push( {
		"data": "unit-id",
		"title": biolims.common.unitId,
		"visible":false,
		"createdCell": function(td) {
			$(td).attr("saveName", "unit-id");
		}
	});
	colOpts.push( {
		"data": "unit-name",
		"title": biolims.tInstrumentMaintainItem.unit,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "unit-name");
			$(td).attr("unit-id", rowData['unit-id']);
		}
	});
	   colOpts.push({
		"data":"tempId",
		"title": biolims.tInstrumentMaintainItem.tempId,
		"createdCell": function(td) {
			$(td).attr("saveName", "tempId");
	    },
		"visible": false,	
	});
	   var tbarOpts = [];
	   if("1"!=instrumentMaintainState){
		   tbarOpts.push({
				text: biolims.common.save,
				action: function() {
					saveInstrumentMaintainItem($("#instrumentMaintainItemdiv"));
				}
			});
		   tbarOpts.push({
				text: biolims.common.delSelected,
				action: function() {
					//刷新左侧表
					var instrumentMaintainItemss = $("#instrumentMaintainTempdiv").DataTable();
					removeChecked($("#instrumentMaintainItemdiv"),
						"/equipment/maintain/delInstrumentMaintainItem.action","删除入库明细数据：",$("#instrumentRepair_id").text());
				}
			});
	   }
//	tbarOpts.push({
//		text: biolims.common.fillDetail,
//		action: function() {
//			addItem($("#instrumentMaintainItemTable"))
//	}
//	});
/*	tbarOpts.push({
		text: biolims.common.addwindow,
		action: function() {
			addItemLayer($("#instrumentMaintainItemTable"))
		}
	});*/
	/*tbarOpts.push({
		text: biolims.common.editwindow,
		action: function() {
			editItemLayer($("#instrumentMaintainItemTable"))
		}
	});
	/*tbarOpts.push({
		text: biolims.common.batchUpload,
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#instrumentMaintainItem_id").val(),
						fileId: data.response.fileId
					},
					url: ctx + "/equipment/maintain/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							 instrumentMaintainItemTable.ajax.reload();
						} else {
							top.layer.msg(biolims.common.uploadFailed)
						}
		
					}
				});
			});
			
		}
		});*/

	
	var instrumentMaintainItemOps = table(true, $("#instrumentMaintain_id").text(), "/equipment/maintain/showInstrumentMaintainItemTableJson.action", colOpts, tbarOpts);
	instrumentMaintainItemTab = renderData($("#instrumentMaintainItemdiv"), instrumentMaintainItemOps);
	//选择数据并提示
	instrumentMaintainItemTab.on('draw', function() {
		var index = 0;
		$("#instrumentMaintainItemdiv .icheck").on('ifChanged', function(event) {
			if($(this).is(':checked')) {
				index++;
				$("#plateModal").find(".mysample[sid='" + this.value + "']").css("border","2px solid #000");
			} else {
				var tt=$("#plateModal").find(".mysample[sid='" + this.value + "']").css("border","1px solid gainsboro");
				index--;
			}
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
		oldChangeLog = instrumentMaintainItemTab.ajax.json();
		
	});
});

//保存
function saveInstrumentMaintainItem(ele) {
	var data = saveInstrumentMaintainItemjson(ele);
	var ele=$("#instrumentMaintainItemdiv");
	var changeLog = "设备维护明细:";
	changeLog = getChangeLog(data, ele, changeLog);
	var changeLogs="";
	if(changeLog !="设备维护明细:"){
		changeLogs=changeLog;
	}
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		type: 'post',
		url: '/equipment/maintain/saveInstrumentMaintainItemTable.action',
		data: {
			id: $("#instrumentMaintain_id").taxt(),
			dataJson: data,
			changeLog: changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveInstrumentMaintainItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			
			if(k == "instrumentMaintain-name") {
				json["instrumentMaintain-id"] = $(tds[j]).attr("instrumentMaintain-id");
				continue;
			}
			
			if(k == "instrument-name") {
				json["instrument-id"] = $(tds[j]).attr("instrument-id");
				continue;
			}
			
			if(k == "unit-name") {
				json["unit-id"] = $(tds[j]).attr("unit-id");
				continue;
			}
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
//获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 添加样本类型ID
//			if(k == "dicSampleTypeName") {
//				json["dicSampleTypeId"] = $(tds[j]).attr("dicSampleTypeId");
//				continue;
//			}
			json[k] = $(tds[j]).text();
		}
		json.color = $(val).attr("background");
		data.push(json);
	});
	return JSON.stringify(data);
}
/*
 * 
 * 创建者 : 郭恒开
 * 创建日期: 2018/03/08
 * 文件描述: 修改日志
 * 
 */
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '数量为"' + v.num + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k] && k != "color") {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
