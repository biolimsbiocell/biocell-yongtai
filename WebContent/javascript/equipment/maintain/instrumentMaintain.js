var instrumentMaintainTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":biolims.tInstrumentMaintain.id,
	});
	
	    fields.push({
		"data":"name",
		"title":biolims.tInstrumentMaintain.name,
	});
	
	    fields.push({
		"data":"createUser-id",
		"title":biolims.tInstrumentMaintain.createUser+"ID"
	});
	    fields.push({
		"data":"createUser-name",
		"title":biolims.tInstrumentMaintain.createUser
	});
	
	    fields.push({
		"data":"createDate",
		"title":biolims.tInstrumentMaintain.createDate,
		"render": function(data) {
				if(data) {
					return parent.moment(data).format('YYYY-MM-DD');
				} else {
					return "";
				}
			}
	});
	
	    fields.push({
		"data":"state",
		"title":biolims.tInstrumentMaintain.state,
	});
	
	    fields.push({
		"data":"stateName",
		"title":biolims.tInstrumentMaintain.stateName,
	});
	
	    fields.push({
		"data":"maintainDate",
		"title":biolims.tInstrumentMaintain.maintainDate,
		"render": function(data) {
				if(data) {
					return parent.moment(data).format('YYYY-MM-DD');
				} else {
					return "";
				}
			}
	});
	
	    fields.push({
		"data":"note",
		"title":biolims.tInstrumentMaintain.note,
	});
	

		$.ajax({
			type:"post",
			url:window.ctx + "/system/customfields/findFieldByModuleValue.action",
			async:false,
			data:{
				moduleValue : "InstrumentMaintain"
			},
			success:function(data){
				var objData = JSON.parse(data);
				if(objData.success){
					$.each(objData.data, function(i,n) {
						var str = {
							"data" : n.fieldName,
							"title" : n.label
						}
						colData.push(str);
					});
					
				}else{
					top.layer.msg(biolims.tInstrumentBorrowDetail.tableError);
				}
			}
		});

	var options = table(true, "","/equipment/maintain/showInstrumentMaintainTableJson.action",
	 fields, null)
	instrumentMaintainTable = renderRememberData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(instrumentMaintainTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
		"/equipment/maintain/editInstrumentMaintain.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/equipment/maintain/editInstrumentMaintain.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/equipment/maintain/viewInstrumentMaintain.action?id=" + id;
}
//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":biolims.tInstrumentMaintain.id
		});
	   fields.push({
		    "searchName":"name",
			"type":"input",
			"txt":biolims.tInstrumentMaintain.name
		});
	fields.push({
	    "type":"input",
		"searchName":"createUser.id",
		"txt":biolims.tInstrumentMaintain.createUser+"ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"createUser.name",
		"txt":biolims.tInstrumentMaintain.createUser
	});
	   fields.push({
		    "searchName":"createDate",
			"type":"input",
			"txt":biolims.tInstrumentMaintain.createDate
		});
	fields.push({
			"txt": biolims.tInstrumentMaintain.createDate+"(Start)",
			"type": "dataTime",
			"searchName": "createDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.tInstrumentMaintain.createDate+"(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "createDate##@@##2"
		});
	   fields.push({
		    "searchName":"state",
			"type":"input",
			"txt":biolims.tInstrumentMaintain.state
		});
	   fields.push({
		    "searchName":"stateName",
			"type":"input",
			"txt":biolims.tInstrumentMaintain.stateName
		});
	   fields.push({
		    "searchName":"maintainDate",
			"type":"input",
			"txt":biolims.tInstrumentMaintain.maintainDate
		});
	fields.push({
			"txt": biolims.tInstrumentMaintain.maintainDate+"(Start)",
			"type": "dataTime",
			"searchName": "maintainDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.tInstrumentMaintain.maintainDate+"(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "maintainDate##@@##2"
		});
	   fields.push({
		    "searchName":"note",
			"type":"input",
			"txt":biolims.tInstrumentMaintain.note
		});
	
	fields.push({
		"type":"table",
		"table":instrumentMaintainTable
	});
	return fields;
}
