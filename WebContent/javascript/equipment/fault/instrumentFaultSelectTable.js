var instrumentFaultTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":biolims.tInstrumentFault.id,
	});
	
	    fields.push({
		"data":"note",
		"title":biolims.tInstrumentFault.note,
	});
	
	    fields.push({
		"data":"type-id",
		"title":biolims.common.typeID
	});
	    fields.push({
		"data":"type-name",
		"title":biolims.tInstrumentFault.type
	});
	
	    fields.push({
		"data":"stateName",
		"title":biolims.tInstrumentFault.stateName,
	});
	
	    fields.push({
		"data":"handleUser-id",
		"title":biolims.tInstrumentFault.handleUserId
	});
	    fields.push({
		"data":"handleUser-name",
		"title":biolims.tInstrumentFault.handleUser
	});
	
	    fields.push({
		"data":"handleDate",
		"title":biolims.tInstrumentFault.handleDate,
	});
	
	var options = table(true, "","/equipment/fault/showInstrumentFaultTableJson.action",
	 fields, null)
	instrumentFaultTable = renderData($("#addInstrumentFaultTable"), options);
	$('#addInstrumentFaultTable').on('init.dt', function() {
		recoverSearchContent(instrumentFaultTable);
	})
});

//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":biolims.tInstrumentFault.id
		});
	   fields.push({
		    "searchName":"note",
			"type":"input",
			"txt":biolims.tInstrumentFault.note
		});
	fields.push({
	    "type":"input",
		"searchName":"type.id",
		"txt":biolims.common.typeID
	});
	fields.push({
	    "type":"input",
		"searchName":"type.name",
		"txt":biolims.tInstrumentFault.type
	});
	   fields.push({
		    "searchName":"stateName",
			"type":"input",
			"txt":biolims.tInstrumentFault.stateName
		});
	fields.push({
	    "type":"input",
		"searchName":"handleUser.id",
		"txt":biolims.tInstrumentFault.handleUserId
	});
	fields.push({
	    "type":"input",
		"searchName":"handleUser.name",
		"txt":biolims.tInstrumentFault.handleUser
	});
	   fields.push({
		    "searchName":"handleDate",
			"type":"input",
			"txt":biolims.tInstrumentFault.handleDate
		});
	fields.push({
			"txt": biolims.tInstrumentFault.handleDate+"(Start)",
			"type": "dataTime",
			"searchName": "handleDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.tInstrumentFault.handleDate+"(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "handleDate##@@##2"
		});
	
	fields.push({
		"type":"table",
		"table":instrumentFaultTable
	});
	return fields;
}
