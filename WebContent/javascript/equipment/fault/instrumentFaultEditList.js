var instrumentFaultTable;
var oldinstrumentFaultChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表

	var tbarOpts = [];
	var colOpts = [];
	   colOpts.push({
		"data":"id",
		"title": biolims.tInstrumentFault.id,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"note",
		"title": biolims.tInstrumentFault.note,
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
	    },
		"className": "textarea"
	});
	colOpts.push( {
		"data": "type-id",
		"title": biolims.common.typeID,
		"createdCell": function(td) {
			$(td).attr("saveName", "type-id");
		}
	});
	colOpts.push( {
		"data": "type-name",
		"title": biolims.tInstrumentFault.type,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "type-name");
			$(td).attr("type-id", rowData['type-id']);
		}
	});
	   colOpts.push({
		"data":"stateName",
		"title": biolims.tInstrumentFault.stateName,
		"createdCell": function(td) {
			$(td).attr("saveName", "stateName");
	    },
		"className": "edit"
	});
	colOpts.push( {
		"data": "handleUser-id",
		"title": biolims.tInstrumentFault.handleUserId,
		"createdCell": function(td) {
			$(td).attr("saveName", "handleUser-id");
		}
	});
	colOpts.push( {
		"data": "handleUser-name",
		"title": biolims.tInstrumentFault.handleUser,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "handleUser-name");
			$(td).attr("handleUser-id", rowData['handleUser-id']);
		}
	});
	   colOpts.push({
		"data":"handleDate",
		"title": biolims.tInstrumentFault.handleDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "handleDate");
	    },
		"className": "date"
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#instrumentFaultTable"))
	}
	});
	tbarOpts.push({
		text: biolims.common.addwindow,
		action: function() {
			addItemLayer($("#instrumentFaultTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.editwindow,
		action: function() {
			editItemLayer($("#instrumentFaultTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.batchUpload,
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#instrumentFault_id").val(),
						fileId: data.response.fileId
					},
					url: ctx + "/equipment/fault/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							 instrumentFaultTable.ajax.reload();
						} else {
							top.layer.msg(biolims.common.uploadFailed)
						}
		
					}
				});
			});
			
		}
		});
	
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveInstrumentFault($("#instrumentFaultTable"));
		}
	});
	}
	
	var instrumentFaultOptions = 
	table(true, "","/equipment/fault/showInstrumentFaultTableJson.action",
	 colOpts, tbarOpts)
	
	
	
	instrumentFaultTable = renderData($("#instrumentFaultTable"), instrumentFaultOptions);
	instrumentFaultTable.on('draw', function() {
		oldinstrumentFaultChangeLog = instrumentFaultTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});

// 保存
function saveInstrumentFault(ele) {
	var data = saveInstrumentFaultjson(ele);
	var ele=$("#instrumentFaultTable");
	var changeLog = "实验结果：";
	changeLog = getChangeLog(data, ele, changeLog);
	$.ajax({
		type: 'post',
		url: '/equipment/fault/saveInstrumentFaultTable.action',
		data: {
			dataJson: data,
			changeLog:changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveInstrumentFaultjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			
			if(k == "type-name") {
				json["type-id"] = $(tds[j]).attr("type-id");
				continue;
			}
			
			if(k == "handleUser-name") {
				json["handleUser-id"] = $(tds[j]).attr("handleUser-id");
				continue;
			}
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		oldinstrumentFaultChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
