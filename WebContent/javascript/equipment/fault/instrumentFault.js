var instrumentFaultTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":biolims.tInstrumentFault.id,
	});
	
	    fields.push({
		"data":"note",
		"title":biolims.tInstrumentFault.note,
	});
	
	  
	    fields.push({
		"data":"type-name",
		"title":biolims.tInstrumentFault.type
	});
	
	    fields.push({
		"data":"stateName",
		"title":biolims.tInstrumentFault.stateName,
	});
	
	    fields.push({
		"data":"handleUser-id",
		"title":biolims.tInstrumentFault.handleUserId
	});
	    fields.push({
		"data":"handleUser-name",
		"title":biolims.tInstrumentFault.handleUser
	});
	
	    fields.push({
		"data":"handleDate",
		"title":biolims.tInstrumentFault.handleDate,
		"render": function(data) {
				if(data) {
					return parent.moment(data).format('YYYY-MM-DD');
				} else {
					return "";
				}
			}
	});
	

		$.ajax({
			type:"post",
			url:window.ctx + "/system/customfields/findFieldByModuleValue.action",
			async:false,
			data:{
				moduleValue : "InstrumentFault"
			},
			success:function(data){
				var objData = JSON.parse(data);
				if(objData.success){
					$.each(objData.data, function(i,n) {
						var str = {
							"data" : n.fieldName,
							"title" : n.label
						}
						colData.push(str);
					});
					
				}else{
					top.layer.msg(biolims.common.error);
				}
			}
		});

	var options = table(true, "","/equipment/fault/showInstrumentFaultTableJson.action",
	 fields, null)
	instrumentFaultTable = renderRememberData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(instrumentFaultTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
		"/equipment/fault/editInstrumentFault.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/equipment/fault/editInstrumentFault.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/equipment/fault/viewInstrumentFault.action?id=" + id;
}
//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":biolims.tInstrumentFault.id
		});
	   fields.push({
		    "searchName":"note",
			"type":"input",
			"txt":biolims.tInstrumentFault.note
		});
/*	fields.push({
	    "type":"input",
		"searchName":"type.id",
		"txt":biolims.common.typeID
	});*/
	fields.push({
	    "type":"input",
		"searchName":"type.name",
		"txt":biolims.tInstrumentFault.type
	});
	   fields.push({
		    "searchName":"stateName",
			"type":"input",
			"txt":biolims.tInstrumentFault.stateName
		});
	fields.push({
	    "type":"input",
		"searchName":"handleUser.id",
		"txt":biolims.tInstrumentFault.handleUserId
	});
	fields.push({
	    "type":"input",
		"searchName":"handleUser.name",
		"txt":biolims.tInstrumentFault.handleUser
	});
	   fields.push({
		    "searchName":"handleDate",
			"type":"input",
			"txt":biolims.tInstrumentFault.handleDate
		});
	fields.push({
			"txt": biolims.tInstrumentFault.handleDate+"(Start)",
			"type": "dataTime",
			"searchName": "handleDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.tInstrumentFault.handleDate+"(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "handleDate##@@##2"
		});
	
	fields.push({
		"type":"table",
		"table":instrumentFaultTable
	});
	return fields;
}
