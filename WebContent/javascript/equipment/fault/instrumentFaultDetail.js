var instrumentFaultDetailTab, oldChangeLog;
//设备维修明细表
$(function() {
	var colOpts = [];
	 colOpts.push({
			"data":"id",
			"title": biolims.tInstrumentFaultDetail.id,
			"createdCell": function(td) {
				$(td).attr("saveName", "id");
		    },
			"className": "edit",
			"visible":	false,
		});
		   colOpts.push({
				"data":"note",
				"title": biolims.tInstrumentFaultDetail.note,
				"createdCell": function(td) {
					$(td).attr("saveName", "note");
			    },
				"className": "textarea",
					"visible":	true,
			});
		colOpts.push( {
			"data": "instrument-id",
			"title": biolims.tInstrumentFaultDetail.instrument,
			"createdCell": function(td,data) {
				$(td).attr("saveName", "instrument-id");
			},
//		"visible": false,
		});
		colOpts.push( {
			"data": "instrument-name",
			"title": biolims.tInstrumentState.instrumentName,
			"createdCell": function(td, data) {
				$(td).attr("saveName", "instrument-name");
//				$(td).attr("instrument-id", 'instrument-id');
			},
		});
		colOpts.push( {
			"data": "instrument-effectiveDate",
			"title": biolims.tInstrumentBorrowDetail.effectiveDate,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "instrument-effectiveDate");
			},
			"visible": false,
		});
		colOpts.push( {
			"data": "instrument-expiryDate",
			"title": biolims.common.discardedDate,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "instrument-expiryDate");
			},
			"visible": false,
		});
		colOpts.push( {
			"data": "instrument-nextDate",
			"title": biolims.common.nextDetectionDate,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "instrument-nextDate");
			},
		"visible": false,
		});
		colOpts.push( {
			"data": "instrument-spec",
			"title": biolims.equipment.spec,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "instrument-spec");
			}
		});
		colOpts.push( {
			"data": "instrument-serialNumber",
			"title": biolims.equipment.serialNumber,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "instrument-serialNumber");
			}
		});
		colOpts.push( {
			"data": "instrument-producer-name",
			"title": biolims.tStorage.supplierId,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "instrument-producer-name");
			}
		});
		colOpts.push( {
			"data": "instrument-state-name",
			"title": biolims.tInstrument.state,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "instrument-state-name");
			}
		});
	
	var tbarOpts = [];
	if("1"!=$("#instrumentFaultState").val()){
		tbarOpts.push({
			text: biolims.common.delSelected,
			action: function() {
				removeChecked($("#instrumentFaultDetaildiv"),
					"/equipment/fault/delInstrumentFaultDetail.action");
			}
		});
		tbarOpts.push({
			text: biolims.common.editwindow,
			action: function() {
				editItemLayer($("#instrumentFaultDetaildiv"))
			}
		});
		tbarOpts.push({
			text: biolims.common.save,
			action: function() {
				saveInstrumentFaultDetail($("#instrumentFaultDetaildiv"));
			}
		});
	}
	
	var instrumentFaultDetailOps = table(true, $("#instrumentFault_id").text(), "/equipment/fault/showInstrumentFaultDetailTableJson.action", colOpts, tbarOpts);
	instrumentFaultDetailTab = renderData($("#instrumentFaultDetaildiv"), instrumentFaultDetailOps);
	//选择数据并提示
	instrumentFaultDetailTab.on('draw', function() {
		var index = 0;
		$("#instrumentFaultDetaildiv .icheck").on('ifChanged', function(event) {
			if($(this).is(':checked')) {
				index++;
				$("#plateModal").find(".mysample[sid='" + this.value + "']").css("border","2px solid #000");
			} else {
				var tt=$("#plateModal").find(".mysample[sid='" + this.value + "']").css("border","1px solid gainsboro");
				index--;
			}
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
		oldChangeLog = instrumentFaultDetailTab.ajax.json();
	});
});

// 保存
function saveInstrumentFaultDetail() {
    var ele=$("#instrumentFaultDetaildiv");
	var changeLog = "设备报修:";
	var data = saveItemjson(ele);
	changeLog = getChangeLog(data, ele, changeLog);
	
	$.ajax({
		type: 'post',
		url: '/equipment/fault/saveInstrumentFaultDetailTable.action',
		data: {
			id: $("#instrumentFault_id").text(),
			dataJson: data,
			logInfo: changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
			} else {
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 添加样本类型ID
//			if(k == "dicSampleTypeName") {
//				json["dicSampleTypeId"] = $(tds[j]).attr("dicSampleTypeId");
//				continue;
//			}
			
//			if(k == "instrument-name") {
//				json["instrument-id"] = $(tds[j]).attr("instrument-id");
//				continue;
//			}
			
			
			
			json[k] = $(tds[j]).text();
//			if(k == "instrument-name") {
//				json["instrument-id"] = $(tds[j]).attr("instrument-id");
//				continue;
//			}
		}
		json.color = $(val).attr("background");
		data.push(json);
	});
	return JSON.stringify(data);
}

/*
 * 
 * 创建者 : 郭恒开
 * 创建日期: 2018/03/08
 * 文件描述: 修改日志
 * 
 */
function getChangeLog(data, ele, changeLog) {
	debugger
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '数量为"' + v.num + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k] && k != "color") {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
