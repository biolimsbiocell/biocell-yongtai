var incubatorSampleInfoTable;
var oldincubatorSampleInfoChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表
	var id = $("#instrument_id").val();
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push({
		"data":"id",
		"title": "库存编号",
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
		"visible": false,	
		
	});
	colOpts.push({
		"data":"batch",
		"title": "批次号",
		"createdCell": function(td) {
			$(td).attr("saveName", "batch");
	    },
	});
	colOpts.push({
		"data":"location",
		"title": "储位",
		"createdCell": function(td) {
			$(td).attr("saveName", "location");
	    },
	});
	var handlemethod = $("#handlemethod").val();
	
	var incubatorSampleInfoOptions = table(true,
		id,
		'/equipment/main/showIncubatorSampleInfoTableJson.action', colOpts, tbarOpts)
	incubatorSampleInfoTable = renderData($("#incubatorSampleInfoTable"), incubatorSampleInfoOptions);
	incubatorSampleInfoTable.on('draw', function() {
		oldincubatorSampleInfoChangeLog = incubatorSampleInfoTable.ajax.json();
	});
});

