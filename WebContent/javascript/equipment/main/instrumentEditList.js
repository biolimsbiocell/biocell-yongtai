var instrumentTable;
var oldinstrumentChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表

	var tbarOpts = [];
	var colOpts = [];
	   colOpts.push({
		"data":"id",
		"title": biolims.tInstrument.id,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"name",
		"title": biolims.tInstrument.name,
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
	    },
		"className": "textarea"
	});
	   colOpts.push({
		"data":"rank",
		"title": biolims.tInstrument.rank,
		"createdCell": function(td) {
			$(td).attr("saveName", "rank");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"serialNumber",
		"title": biolims.tInstrument.serialNumber,
		"createdCell": function(td) {
			$(td).attr("saveName", "serialNumber");
	    },
		"className": "edit"
	});
	colOpts.push( {
		"data": "mainType-id",
		"title": "分类ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "mainType-id");
		}
	});
	colOpts.push( {
		"data": "mainType-name",
		"title": biolims.tInstrument.mainType,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "mainType-name");
			$(td).attr("mainType-id", rowData['mainType-id']);
		}
	});
	   colOpts.push({
		"data":"spec",
		"title": biolims.tInstrument.spec,
		"createdCell": function(td) {
			$(td).attr("saveName", "spec");
	    },
		"className": "edit"
	});
	colOpts.push( {
		"data": "state-id",
		"title": "状态ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "state-id");
		}
	});
	colOpts.push( {
		"data": "state-name",
		"title": biolims.tInstrument.state,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "state-name");
			$(td).attr("state-id", rowData['state-id']);
		}
	});
	   colOpts.push({
		"data":"createUser",
		"title": biolims.tInstrument.createUser,
		"createdCell": function(td) {
			$(td).attr("saveName", "createUser");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"createDate",
		"title": biolims.tInstrument.createDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "createDate");
	    },
		"className": "date"
	});
	colOpts.push( {
		"data": "department-id",
		"title": "部门ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "department-id");
		}
	});
	colOpts.push( {
		"data": "department-name",
		"title": biolims.tInstrument.department,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "department-name");
			$(td).attr("department-id", rowData['department-id']);
		}
	});
	   colOpts.push({
		"data":"price",
		"title": biolims.tInstrument.price,
		"createdCell": function(td) {
			$(td).attr("saveName", "price");
	    },
		"className": "edit"
	});
	colOpts.push( {
		"data": "currencyType-id",
		"title": "币种ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "currencyType-id");
		}
	});
	colOpts.push( {
		"data": "currencyType-name",
		"title": biolims.tInstrument.currencyType,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "currencyType-name");
			$(td).attr("currencyType-id", rowData['currencyType-id']);
		}
	});
	   colOpts.push({
		"data":"note",
		"title": biolims.tInstrument.note,
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
	    },
		"className": "textarea"
	});
	   colOpts.push({
		"data":"capitalCode",
		"title": biolims.tInstrument.capitalCode,
		"createdCell": function(td) {
			$(td).attr("saveName", "capitalCode");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"effectiveDate",
		"title": biolims.tInstrument.effectiveDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "effectiveDate");
	    },
		"className": "date"
	});
	colOpts.push( {
		"data": "products-id",
		"title": "生产商ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "products-id");
		}
	});
	colOpts.push( {
		"data": "products-name",
		"title": biolims.tInstrument.products,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "products-name");
			$(td).attr("products-id", rowData['products-id']);
		}
	});
	   colOpts.push({
		"data":"showstate",
		"title": biolims.tInstrument.showstate,
		"createdCell": function(td) {
			$(td).attr("saveName", "showstate");
	    },
		"className": "edit"
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#instrumentTable"))
	}
	});
	tbarOpts.push({
		text: biolims.common.addwindow,
		action: function() {
			addItemLayer($("#instrumentTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.editwindow,
		action: function() {
			editItemLayer($("#instrumentTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.batchUpload,
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#instrument_id").val(),
						fileId: data.response.fileId
					},
					url: ctx + "/equipment/main/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							 instrumentTable.ajax.reload();
						} else {
							top.layer.msg(biolims.common.uploadFailed)
						}
		
					}
				});
			});
			
		}
		});
	
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveInstrument($("#instrumentTable"));
		}
	});
	}
	
	var instrumentOptions = 
	table(true, "","/equipment/main/showInstrumentTableJson.action",
	 colOpts, tbarOpts)
	
	
	
	instrumentTable = renderData($("#instrumentTable"), instrumentOptions);
	instrumentTable.on('draw', function() {
		oldinstrumentChangeLog = instrumentTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});

// 保存
function saveInstrument(ele) {
	var data = saveInstrumentjson(ele);
	var ele=$("#instrumentTable");
	var changeLog = "实验结果：";
	changeLog = getChangeLog(data, ele, changeLog);
	$.ajax({
		type: 'post',
		url: '/equipment/main/saveInstrumentTable.action',
		data: {
			dataJson: data,
			changeLog:changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveInstrumentjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			
			if(k == "mainType-name") {
				json["mainType-id"] = $(tds[j]).attr("mainType-id");
				continue;
			}
			
			if(k == "state-name") {
				json["state-id"] = $(tds[j]).attr("state-id");
				continue;
			}
			
			if(k == "department-name") {
				json["department-id"] = $(tds[j]).attr("department-id");
				continue;
			}
			
			if(k == "currencyType-name") {
				json["currencyType-id"] = $(tds[j]).attr("currencyType-id");
				continue;
			}
			
			if(k == "products-name") {
				json["products-id"] = $(tds[j]).attr("products-id");
				continue;
			}
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		oldinstrumentChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
