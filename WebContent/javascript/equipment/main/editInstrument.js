function list() {
	window.location = window.ctx + '/equipment/main/showInstrumentList.action?queryMethod='
	+ "1"+"&p_type="+$("#p_type").val() ;

}
function lockList() {
	window.location = window.ctx + '/equipment/main/showInstrumentList.action?lock=true';
}
function add() {
	window.location = window.ctx + "/equipment/main/toEditInstrument.action?p_type="+$("#p_type").val();
}
function printReport() {
	var url = '__report=instrumentRecord.rptdesign&id=' + $("#instrument_id").val();
	commonPrint(url);
}

function edit() {
	var id = "";
	id = document.getElementById("instrument_id").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/equipment/main/toEditInstrument.action?id=' + id+"&p_type="+$("#p_type").val();
}

function normalsearch(gridName) {
	var limit = parseInt((parent.document.body.clientHeight - 250) > 0 ? (parent.document.body.clientHeight - 250) / 25
			: 1);
	var fields = [ 'id', 'note' ];
	var fieldsValue = [ document.getElementById('id').value, document.getElementById('note').value ];
	searchAllGrid(gridName, fields, fieldsValue, 0, limit);

}

function newSave() {
	if (checkSubmit() == false) {
		return false;
	}
	var myMask = new Ext.LoadMask(Ext.getBody(), {
		msg : biolims.common.pleaseWait
	});
	myMask.show();
	Ext.Ajax.request({
		url : window.ctx + '/common/hasId.action',
		method : 'POST',
		params : {
			id : $("#instrument_id").val(),
			obj : 'Instrument'
		},
		success : function(response) {
			var respText = Ext.util.JSON.decode(response.responseText);
			myMask.hide();
			if (respText.message == '') {
				save();
			} else {
				message(respText.message);
			}
		},
		failure : function(response) {
		}
	});

}
function save() {

	if (checkSubmit() == true) {
		Ext.MessageBox.show({
			msg : biolims.common.savingData,
			progressText : biolims.common.saving,
			width : 300,
			wait : true,
			icon : 'ext-mb-download'
		});
		 $("#toolbarbutton_save").hide();
		form1.action = window.ctx + "/equipment/main/save.action?p_type="+$("#p_type").val();
		form1.submit();
	} else {
		return false;
	}
}
function checkSubmit() {
	var mess = "";
	var fs = [ "instrument_id" ];
	var nsc = [ biolims.common.IdEmpty ];

	mess = commonFieldsNotNullVerifyById(fs, nsc);
	if (mess != "") {
		message(mess);
		return false;
	}
	return true;
}



function viewSupplier() {
	if (trim(document.getElementById('instrument_supplier_id').value) == '') {
		message(biolims.common.selectRecord);
	} else {
		openDialog(window.ctx + '/supplier/toViewSupplier.action?id='
				+ document.getElementById('instrument_supplier_id').value);

	}
}
function viewCCSupplier() {
	if (trim(document.getElementById('instrument_producer_id').value) == '') {
		message(biolims.common.selectRecord);
	} else {
		openDialog(window.ctx + '/supplier/toViewSupplier.action?id='
				+ document.getElementById('instrument_producer_id').value);

	}
}
function editCopy() {
	window.location = window.ctx + '/equipment/main/toCopyInstrument.action?id=' + $("#instrument_id").val();
}
Ext.onReady(function() {
	var tabs = new Ext.TabPanel({
		renderTo : 'maintab',
		height : document.body.clientHeight-30,
		autoWidth : true,
		activeTab : 0,
		margins : '0 0 0 0',
		items : [ {
			title : biolims.common.instrumentInfo,
			contentEl : 'markup'
		} ]
	});
	if($("#p_type").val()==4){
		$("#show1").removeClass("tri");
		$("#show2").removeClass("tri");
	}else{
		$("#show1").addClass("tri");
		$("#show2").addClass("tri");
	}
});
$(function() {
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "modify") {
		settextreadonly("instrument_id");
	}
	if (handlemethod == "view") {
		settextreadonlyByAll();
	}
	var copyMode = $("#copyMode").val();
	if (copyMode && copyMode == "true") {
		settextread("instrument_id");
		$("#instrument_id").val("");
		lue = "3";
	}
	load("/equipment/main/showInstrumentTest.action", {
		id : $("#instrument_id").val()
	}, "#testGrid11");

	
});

function makeCode(){
	
	
	
	ajax("post", "/sample/receive/makeCode.action", {
		
		id : $("#instrument_id").val(),
		ip : '10.0.0.213'
	}, function() {
	
	}, null);
}

