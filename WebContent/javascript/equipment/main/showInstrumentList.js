var gridGrid;
Ext.onReady(function() {
			Ext.QuickTips.init();
			Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
			Ext.grid.RowNumberer = Ext.extend(Ext.grid.RowNumberer, {
				width : 30,
				renderer : function(value, cellmeta, record, rowIndex,
						columnIndex, store) {
					return store.lastOptions.params.start + rowIndex + 1;
				}
			});
var store = new Ext.data.JsonStore({
										root : 'results',
						totalProperty : 'total',
						remoteSort : true,
						fields : [ {
							name : 'id',
							type : 'string'
						}, {
							name : 'name',
							type : 'string'
						}, {
							name : 'spec',
							type : 'string'
						}, {
							name : 'type-id',
							type : 'string'
						}, {
							name : 'type-name',
							type : 'string'
						}, {
							name : 'producer-name',
							type : 'string'
						}, {
							name : 'position',
							type : 'string'
						}, {
							name : 'dutyUser-name',
							type : 'string'
						}, {
							name : 'dutyUserTwo-name',
							type : 'string'
						}, {
							name : 'rank',
							type : 'string'
						}, {
							name : 'serialNumber',
							type : 'string'
						}, {
							name : 'enquiryOdd-number',
							type : 'string'
						}, {
							name : 'state-name',
							type : 'string'
						}, {
							name : 'studyType-name',
							type : 'string'
						}, {
							name : 'createUser-name',
							type : 'string'
						}, {
							name : 'createDate',
							type : 'string'
						}, {
							name : 'outCode',
							type : 'string'
						}, {
							name : 'accuracy',
							type : 'string'
						}, {
							name : 'purchaseDate',
							type : 'string'
						}, {
							name : 'surveyScope',
							type : 'string'
						}, {
							name : 'checkItem',
							type : 'string'
						}, {
							name : 'producer-linkMan',
							type : 'string'
						}, {
							name : 'producer-linkTel',
							type : 'string'
						}, {
							name : 'supplier-name',
							type : 'string'
						}, {
							name : 'supplier-linkMan',
							type : 'string'
						}, {
							name : 'supplier-linkTel',
							type : 'string'
						}, {
							name : 'effectiveDate',
							type : 'string'
						}, {
							name : 'expiryDate',
							type : 'string'
						}, {
							name : 'nextDate',
							type : 'string'
						}, {
							name : 'parent-name',
							type : 'string'
						}, {
							name : 'parent2-name',
							type : 'string'
						}, {
							name : 'capitalCode',
							type : 'string'
						}, {
							name : 'note',
							type : 'string'
						} ],
						proxy : new Ext.data.HttpProxy({url: ctx+'/equipment/main/showInstrumentListJson.action?p_type='+$("#p_type").val(),method: 'POST'})
});
var title=null;
var hid=true;
var spec=biolims.equipment.spec;
var type=$("#p_type").val();
if(type==1){
	title=biolims.common.commonInstrument;
}else if(type==2){
	title=biolims.common.refrigerator;
}else if(type==3){
	title=biolims.common.pipette;
	spec=biolims.common.range;
}else{
	title=biolims.common.temperatureAndHumidityProbe;
	hid=false;
}
gridGrid = new Ext.grid.GridPanel({
										autoWidth : true,
						id : gridGrid,
						titleCollapse : true,
						autoScroll : true,
						height : document.body.clientHeight - 35,
						iconCls : 'icon-grid',
						title : title,
						collapsible : true,
						store : store,
						columnLines : true,
						trackMouseOver : true,
						loadMask : true,
						
						colModel : new Ext.ux.grid.LockingColumnModel([
								new Ext.grid.RowNumberer(),{
									dataIndex : 'name',
									header : biolims.common.instrumentName,
									width : 150,
									sortable : true
								}, {
									dataIndex : 'id',
									header : "EQL",
									width : 120,
									sortable : true
								},{
									dataIndex : 'rank',
									header : biolims.equipment.level,
									width : 60,
									sortable : true
								},{
									dataIndex : 'spec',
									header : spec,
									width : 150,
									sortable : true
								},{
									dataIndex : 'type-id',
									header : biolims.common.typeID,
									width : 150,
									sortable : true,
									hidden : true
								},{
									dataIndex : 'type-name',
									header : biolims.storage.studyType,
									width : 150,
									sortable : true
								}, {
									dataIndex : 'producer-name',
									header : biolims.equipment.producerName,
									width : 100,
									sortable : true
								},{
									dataIndex : 'effectiveDate',
									header :biolims.common.installDate,
									width : 100,
									sortable : true,
									hidden : false
								},{
									dataIndex : 'serialNumber',
									header : biolims.equipment.serialNumber,
									width : 180,
									sortable : true
								},{
									dataIndex : 'capitalCode',
									header : biolims.common.fixedAssetNumber,
									width : 180,
									sortable : true
								}, {
									dataIndex : 'position',
									header : biolims.equipment.positionId,
									width : 120,
									sortable : true
								}, {
									dataIndex : 'dutyUser-name',
									header : biolims.master.personName,
									width : 100,
									sortable : true,
									hidden : true
								}, {
									dataIndex : 'dutyUserTwo-name',
									header : biolims.master.person2Name,
									width : 100,
									sortable : true,
									hidden : true
								},{
									dataIndex : 'enquiryOddNumber',
									header : biolims.equipment.enquiryOddNumber,
									width : 150,
									hidden : true,
									sortable : true
								},{
									dataIndex : 'state-name',
									header : biolims.common.state,
									width : 75,
									sortable : true
								}, {
									dataIndex : 'studyType-name',
									header : biolims.equipment.studyTypeName,
									width : 125,
									hidden : true,
									sortable : true
								}, {
									dataIndex : 'createUser-name',
									header : biolims.sample.createUserName,
									width : 150,
									sortable : true,
									hidden : true
								}, {
									dataIndex : 'createDate',
									header : biolims.sample.createDate,
									width : 150,
									sortable : true,
									hidden : true
								}, {
									dataIndex : 'outCode',
									header : biolims.equipment.outCode,
									width : 200,
									sortable : true,
									hidden : true
								}, {
									dataIndex : 'accuracy',
									header : biolims.equipment.accuracy,
									width : 200,
									sortable : true,
									hidden : true
								}, {
									dataIndex : 'purchaseDate',
									header : biolims.equipment.purchaseDate,
									width : 200,
									sortable : true,
									hidden : true
								}, {
									dataIndex : 'surveyScope',
									header : biolims.equipment.surveyScope,
									width : 200,
									sortable : true,
									hidden : true
								}, {
									dataIndex : 'checkItem',
									header : biolims.equipment.checkItem,
									width : 200,
									sortable : true,
									hidden : true
								}, {
									dataIndex : 'producer-linkMan',
									header : biolims.equipment.producerLinkMan,
									width : 200,
									sortable : true,
									hidden : true
								}, {
									dataIndex : 'producer-linkTel',
									header : biolims.equipment.producerLinkTel,
									width : 200,
									sortable : true,
									hidden : true
								}, {
									dataIndex : 'supplier-name',
									header : biolims.equipment.supplierName,
									width : 200,
									sortable : true,
									hidden : true
								}, {
									dataIndex : 'supplier-linkMan',
									header : biolims.equipment.supplierLinkMan,
									width : 200,
									sortable : true,
									hidden : true
								}, {
									dataIndex : 'supplier-linkTel',
									header : biolims.equipment.supplierLinkTel,
									width : 200,
									sortable : true,
									hidden : true
								},{
									dataIndex : 'expiryDate',
									header : biolims.equipment.factEndDate,
									width : 100,
									sortable : true,
									hidden : true
								},{
									dataIndex : 'nextDate',
									header : biolims.common.nextDetectionDate,
									width : 100,
									sortable : true,
									hidden : true
								},{
									dataIndex : 'parent-name',
									header : biolims.common.fridge,
									width : 100,
									sortable : true,
									hidden : hid
								},{
									dataIndex : 'parent-name',
									header : biolims.common.monitorHost,
									width : 100,
									sortable : true,
									hidden : hid
								},{
									dataIndex : 'note',
									header : biolims.common.note,
									width : 100,
									sortable : true,
								}
								]),
						stripeRows : true,
						view : new Ext.ux.grid.LockingGridView(),
						viewConfig : {
							forceFit : true,
							enableRowBody : false,
showPreview:false
},
bbar: new Ext.PagingToolbar({
									id : 'bbarId',
									pageSize : parseInt((document.body.clientHeight - 35) > 0 ? (document.body.clientHeight - 35) / 24
											: 1),
									store : store,
									displayInfo : true,
									displayMsg : biolims.common.displayMsg,
									beforePageText : biolims.common.page,
									afterPageText : biolims.common.afterPageText,
									emptyMsg : biolims.common.noData,
									plugins : new Ext.ui.plugins.ComboPageSize(
											{
												addToItem : false,
												prefixText : biolims.common.show,
												postfixText : biolims.common.entris
											})
								})
					});
			gridGrid.render('grid');
			gridGrid.getSelectionModel().on('rowselect',
					function(sm, rowIdx, r) {
						document.getElementById("id").value = r.data.id;
					});
			gridGrid.on('rowdblclick', function() {
				var record = gridGrid.getSelectionModel().getSelections();
				document.getElementById("id").value = record[0].data.id;
				edit();
			});
			store.on('beforeload', function() {
				store.baseParams = {
					data : document.getElementById('extJsonDataString').value
				};
			});
			store.load({
						params : {
							start : 0,
							limit : parseInt((document.body.clientHeight - 35) > 0 ? (document.body.clientHeight - 35) / 24
									: 1)
						}
					});
			store.addListener('load',handleGridLoadEvent);
			
		});
function handleGridLoadEvent(store,records) {
	var girdcount=0;
	store.each(function(r){
	if(r.get('state-name')=="维修中"){
	gridGrid.getView().getRow(girdcount).style.backgroundColor='#FF0000';
	}
	if(r.get('state-name')=="报废"){
		gridGrid.getView().getRow(girdcount).style.backgroundColor='#888888';
		}
	if(r.get('state-name')=="校准"){
		gridGrid.getView().getRow(girdcount).style.backgroundColor='#DFDF31';
		}
	girdcount=girdcount+1;
	});
	}
function exportexcel() {
	gridGrid.title = biolims.common.exportList
	var vExportContent = gridGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
function list() {
	window.location = window.ctx + '/equipment/main/showInstrumentList.action?p_type='+$("#p_type").val();
}
function lockList() {
	window.location = window.ctx
			+ '/equipment/main/showInstrumentList.action?lock=true';
}
function add() {
	window.location = window.ctx + "/equipment/main/toEditInstrument.action?p_type="+$("#p_type").val();
}
function edit() {
	var id = "";
	id = document.getElementById("id").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/equipment/main/toEditInstrument.action?id=' + id+"&p_type="+$("#p_type").val();
}
function view() {
	var id = "";
	id = document.getElementById("id").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/equipment/main/toViewInstrument.action?id=' + id+"&p_type="+$("#p_type").val();
}

function editCopy() {
	var id = "";
	id = document.getElementById("id").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/equipment/main/toEditInstrument.action?copy=true&id=' + id;
}

function normalsearch(gridName) {
	var limit = parseInt((parent.document.body.clientHeight - 250) > 0 ? (parent.document.body.clientHeight - 250) / 25
			: 1);
	if (trim(document.getElementById("limitNum").value) != '') {
		limit = document.getElementById("limitNum").value;

	}
	var fields = [ 'id', 'name','p_type' ];
	var fieldsValue = [ document.getElementById('id').value,
			document.getElementById('name').value,
			document.getElementById('p_type')];
	searchAllGrid(gridName, fields, fieldsValue, 0, limit);

}
function workflowSubmit() {
	Ext.onReady(function() {
		Ext.QuickTips.init();
		Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";

		var id = "";
		id = document.getElementById("id").value;
		if (id == "" || id == undefined) {
			message(biolims.common.selectRecord);
			return false;
		}

		var params = "formId : '" + id + "',applicationTypeTableId:'2'";
		params = '({' + params + '})';
		params = eval(params);

		var myMask = new Ext.LoadMask(Ext.getBody(), {
			msg : "Please wait..."
		});
		myMask.show();
		Ext.Ajax.request({
			url : window.ctx + '/workflowEngine/workflowSubmit.action',
			method : 'POST',
			params : params,
			success : function(response) {
				myMask.hide();
				// var respText = Ext.util.JSON.decode(response.responseText);
				// alert(respText);

			},
			failure : function() {
			}

		});

	});
}

$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 450;
		option.height = 300;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(gridGrid);
				$(this).dialog("close");
			},
			"清空(Empty)" : function() {
				form_reset();
			}
		}, false, option);
	});

});