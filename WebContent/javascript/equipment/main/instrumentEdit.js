$(function() {
	$("#instrument_effectiveDate").datepicker({
		language: "zh-TW",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});
	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "view") {
		settextreadonly();
	}
	if(handlemethod == "modify") {
		$("#instrument_id").prop("readonly", "readonly");
	}
	var mainFileInput = fileInput('1', 'instrument', $("#instrument_id").val());
	fieldCustomFun();
	
	var yqlx = $("#instrument_mainType_id").val();
	if(yqlx == "281616366a9b6ff9016a9bf060f70019"){
		$("#rq").show();
		$("#rqyb").show();
	}else{
		$("#rq").hide();
		$("#rqyb").hide();
	}

})

function change(id) {
	$("#" + id).css({
		"background-color": "white",
		"color": "black"
	});
}

function add() {
	window.location = window.ctx + "/equipment/main/editInstrument.action";
}

function list() {
	window.location = window.ctx + '/equipment/main/showInstrumentList.action';
}

function newSave() {
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});

function tjsp() {
	top.layer.confirm(biolims.common.pleaseConfirmSaveBeforeSubmit, {
		icon: 3,
		title: biolims.common.prompt,
		btn:biolims.common.selected
	}, function(index) {
		top.layer.open({
			title: biolims.common.submit,
			type: 2,
			anim: 2,
			area: ['800px', '500px'],
			btn: biolims.common.selected,
			content: window.ctx + "/workflow/processinstance/toStartView.action?formName=Instrument",
			yes: function(index, layer) {
				var datas = {
					userId: userId,
					userName: userName,
					formId: $("#instrument_id").val(),
					title: $("#instrument_name").val(),
					formName: 'Instrument'
				}
				ajax("post", "/workflow/processinstance/start.action", datas, function(data) {
					if(data.success) {
						top.layer.msg(biolims.common.submitSuccess);
						if(typeof callback == 'function') {
							callback(data);
						}
						dialogWin.dialog("close");
					} else {
						top.layer.msg(biolims.common.submitFail);
					}
				}, null);
				top.layer.close(index);
			},
			cancel: function(index, layer) {
				top.layer.close(index)
			}

		});
		top.layer.close(index);
	});
}
function sp(){
	
		var taskId = $("#bpmTaskId").val();
		var formId = $("#instrument_id").val();
		
					top.layer.open({
						  title: biolims.common.handle,
						  type:2,
						  anim: 2,
						  area: ['800px','500px']
						  ,btn: biolims.common.selected,
						  content: window.ctx+"/workflow/processinstance/toCompleteTaskView.action?taskId="+taskId+"&formId="+formId,
						  yes: function(index, layer) {
							  var operVal =  $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#oper").val();
							  var opinionVal =  $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#opinionVal").val();
							  var opinion =  $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#opinion").val();
								if(!operVal){
									top.layer.msg(biolims.common.pleaseSelectOper);
									return false;
								}
								if (operVal == "2") {
									_trunTodoTask(taskId, callback, dialogWin);
								} else {
									var paramData = {};
									paramData.oper = operVal;
									paramData.info = opinion;
					
									var reqData = {
										data : JSON.stringify(paramData),
										formId : formId,
										taskId : taskId,
										userId : window.userId
									}
									ajax("post", "/workflow/processinstance/completeTask.action", reqData, function(data) {
										if (data.success) {
											top.layer.msg(biolims.common.submitSuccess);
											if (typeof callback == 'function') {
											}
										} else {
											top.layer.msg(biolims.common.submitFail);
										}
									}, null);
								}
								window.open(window.ctx+"/main/toPortal.action",'_parent');
							},
							cancel: function(index, layer) {
								top.layer.close(index)
							}
					
					});     
}

function ck() {
	top.layer.open({
		title: biolims.common.checkFlowChart,
		type: 2,
		anim: 2,
		area: ['800px', '500px'],
		btn: biolims.common.selected,
		content: window.ctx + "/workflow/processinstance/toTraceProcessInstanceView.action?formId=" + $("#instrument_id").val(),
		yes: function(index, layer) {
			top.layer.close(index)
		},
		cancel: function(index, layer) {
			top.layer.close(index)
		}
	});
}

/*function save() {
if(checkSubmit()==true){
		document.getElementById('instrumentStateJson').value = saveInstrumentStatejson($("#instrumentStateTable"));
		console.log($("#instrumentStateJson").val());
		form1.action = window.ctx
				+ "/equipment/main/save.action?loadNumber=1";
		form1.submit();
	}
}*/

function save() {
	//if(checkSubmit()==true){

	//自定义字段
	//拼自定义字段（实验记录）
	var inputs = $("#fieldItemDiv input");
	var options = $("#fieldItemDiv option");
	var contentData = {};
	var checkboxArr = [];
	//必填验证
	var requiredField = requiredFilter();
	if(!requiredField) {
		return false;
	}
	$("#fieldItemDiv .checkboxs").each(function(i, v) {
		$(v).find("input").each(function(ii, inp) {
			var k = inp.name;
			if(inp.checked == true) {
				checkboxArr.push(inp.value);
				contentData[k] = checkboxArr;
			}
		});
	});
	inputs.each(function(i, inp) {
		var k = inp.name;
		if(inp.type != "checkbox") {
			contentData[k] = inp.value;
		}
	});
	options.each(function(i, opt) {
		if(opt.selected == true) {
			var k = opt.getAttribute("name");
			contentData[k] = opt.value;
		}
	});
	document.getElementById("fieldContent").value = JSON.stringify(contentData);

	var changeLog = "常用设备：";
	$('input[class="form-control"]').each(function(i, v) {
		var valnew = $(v).val();
		var val = $(v).attr("changelog");
		if(val !== valnew) {
			changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
		}
	});
	var changeLogs = "";
	if(changeLog != "常用设备：") {
		changeLogs = changeLog;
		$("#changeLog").val(changeLogs);
	}
	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "modify" && checkSubmit() == true) {
//		document.getElementById('instrumentStateJson').value = saveInstrumentStatejson($("#instrumentStateTable"));
		console.log($("#instrumentStateJson").val());
		top.layer.load(4, {
			shade: 0.3
		});
		$("#form1").attr("action", "/equipment/main/save.action");
		$("#form1").submit();
		top.layer.closeAll();
	} else {
		$.ajax({
			type: "post",
			url: ctx + '/common/hasId.action',
			data: {
				id: $("#instrument_id").val(),
				obj: 'Instrument'
			},
			success: function(data) {
				var data = JSON.parse(data);
				if(data.message) {
					top.layer.msg(data.message);
				} else {
					top.layer.load(4, {
						shade: 0.3
					});
					$("#form1").attr("action", "/equipment/main/save.action");
					$("#form1").submit();
					top.layer.closeAll();
				}
			}
		});
	}
	/*var jsonStr = JSON.stringify($("#form1").serializeObject());  
	$.ajax({
		url: ctx + '/equipment/main/save.action',
		dataType: 'json',
		type: 'post',
		data: {
			dataValue: jsonStr,
			changeLog: changeLog,
			instrumentStateJson : saveInstrumentStatejson($("#instrumentStateTable")),
			
			bpmTaskId : $("#bpmTaskId").val()
		},
		success: function(data) {
			//关闭加载层
			//top.layer.close(index);
			
			
			if(data.success) {
				var url = "/equipment/main/editInstrument.action?id="+data.id;
				
				if(data.bpmTaskId){
					url = url +"&bpmTaskId="+data.bpmTaskId;
				}
				
				window.location.href=url;
				
				
			}else{
				top.layer.msg(biolims.common.saveFailed);
			
			}
			}
		
	});*/

}

$.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if(o[this.name]) {
			if(!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};

function editCopy() {
	window.location = window.ctx + '/equipment/main/copyInstrument.action?id=' + $("#instrument_id").val();
}

function changeState() {
	var paraStr = "formId=" + $("#instrument_id").val() +
		"&tableId=Instrument";
	top.layer.open({
		title: biolims.common.changeState,
		type: 2,
		anim: 2,
		area: ['400px', '400px'],
		btn: biolims.common.selected,
		content: window.ctx +
			"/applicationTypeAction/applicationTypeActionLook.action?" + paraStr +
			"&flag=changeState'",
		yes: function(index, layer) {
			top.layer.confirm(biolims.common.toSubmit, {
				icon: 3,
				title: biolims.common.prompt,
				btn:biolims.common.selected
			}, function(index) {
				ajax("post", "/applicationTypeAction/exeFun.action", {
					applicationTypeActionId: $('.layui-layer-iframe', parent.document).find("iframe").contents().find("input:checked").val(),
					formId: $("#instrument_id").val()
				}, function(response) {
					var respText = response.message;
					if(respText == '') {
						window.location.reload();
					} else {
						top.layer.msg(respText);
					}
				}, null)
				top.layer.close(index);
			})
		},
		cancel: function(index, layer) {
			top.layer.close(index)
		}

	});
}

function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#instrument_id").val());
	nsc.push("eql" + biolims.common.describeEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if(mess != "") {
		top.layer.msg(mess);
		return false;
	}
	return true;
}

function fileUp() {
	if(!$("#instrument_id").val()) {
		top.layer.msg("请先保存数据再上传附件！")
		return false;
	}
	$("#uploadFile").modal("show");
}

function fileView() {
	top.layer.open({
		title: biolims.common.attachment,
		type: 2,
		skin: 'layui-top.layer-lan',
		area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
		content: window.ctx + "/operfile/initFileList.action?flag=1&modelType=instrument&id=" + $("#instrument_id").val(),
		cancel: function(index, layer) {
			top.layer.close(index)
		}
	})
}

function settextreadonly() {
	jQuery(":text, textarea").each(function() {
		var _vId = jQuery(this).attr('id');
		jQuery(this).css("background-color", "#B4BAB5").attr("readonly", "readOnly");
		if(_vId == 'actiondropdown_textbox')
			settextread(_vId);
	});
}

function settextread(name) {
	document.getElementById(name).className = "input_parts text input default  readonlyfalse false";
	document.getElementById(name).readOnly = false;
}

function showmainType() {
	top.layer.open({
		title: biolims.common.instrumentType,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=device", ''],//instrument
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$("#instrument_mainType_id").val(id)
			$("#instrument_mainType_name").val(name)
		},
	})

}
//状态
function showstate() {
	top.layer.open({
		title: biolims.common.instrumentState,
		type: 2,
		area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/state/dicStateSelectTable.action?flag=instrument", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicStateTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicStateTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$("#instrument_state_id").val(id)
			$("#instrument_state_name").val(name)
		},
	})
}

function showdepartment() {

	top.layer.open({
		title: biolims.user.selectTheDepartment,
		type: 2,
		area: ["60%", "65%"],
		btn: biolims.common.selected,
		content: window.ctx +
			"/core/department/departmentSelect.action",
		yes: function(index, layer) {
			var name = [],
				id = [];
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#mytreeGrid .chosed").each(function(i, v) {
				name.push($(v).children("td").eq(2).text());
				id.push($(v).children("td").eq(1).text());
			});
			if(name.length != 1 || id.length != 1) {
				top.layer.msg(biolims.common.pleaseSelectAPieceOfData);
			} else {
				$("#instrument_department_name").val(name.join(","));
				$("#instrument_department_id").val(id.join(","));
				top.layer.close(index)
			}
		},
		cancel: function(index, layer) {
			top.layer.close(index)
		}
	})
}
//币种
function showcurrencyType() {
	top.layer.open({
		title: biolims.storage.rankType,
		type: 2,
		area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=bz", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$("#instrument_currencyType_id").val(id)
			$("#instrument_currencyType_name").val(name)
		},
	})
}




function showproducts() {
	top.layer.open({
		title: biolims.purchase.selectManufacturer,
		type: 2,
		area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
		btn: biolims.common.selected,
		content: [window.ctx + "/supplier/common/supplierSelectTable.action?flag=customer", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#supplierSelectTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#supplierSelectTable .chosed").children("td").eq(0).text();
		
			top.layer.close(index)
			$("#instrument_supplier_id").val(id)
			$("#instrument_supplier_name").val(name)
		},
	})
}
//自定义模块
function fieldCustomFun() {

	//获取自定义字段值的相关内容
	var fieldContent = $("#fieldContent").val();

	//查找自定义列表的订单模块的此检测项目的相关内容
	$.ajax({
		type: "post",
		url: window.ctx + "/system/customfields/findFieldByModuleValue.action",
		data: {
			moduleValue: "Instrument"
		},
		async: false,
		success: function(data) {
			var objValue = JSON.parse(data);
			if(objValue.success) {
				$.each(objValue.data, function(i, n) {
					var inputs = '';
					var disabled = n.readOnly ? ' ' : "disabled";
					var defaultValue = n.defaultValue ? n.defaultValue : ' ';
					if(n.fieldType == "checkbox") {
						var checkboxs = '';
						var singleOptionIdArry = n.singleOptionId.split("/");
						var singleOptionNameArry = n.singleOptionName.split("/");
						singleOptionIdArry.forEach(function(vv, jj) {
							checkboxs += '<input type="checkbox" lay-ignore name=' + n.fieldName + ' value=' + jj + ' title=' + singleOptionNameArry[vv] + '>' + singleOptionNameArry[vv] + '';
						});
						inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group checkboxs " changelog=""><span class="input-group-addon">' + n.label + '</span>' + checkboxs + '</div></div>';
					} else if(n.fieldType == "radio") {
						var options = '';
						var singleOptionIdArry = n.singleOptionId.split("/");
						var singleOptionNameArry = n.singleOptionName.split("/");
						singleOptionIdArry.forEach(function(vv, jj) {
							options += '<option name=' + n.fieldName + ' value=' + jj + '>' + singleOptionNameArry[vv] + '</option>';
						});
						inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><select class="form-control">' + options + '</select></div></div>';
					} else if(n.fieldType == "date") {
						inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><input type="text" name=' + n.fieldName + ' required=' + n.isRequired + ' ' + disabled + ' class="form-control datepick" value=' + defaultValue + '></div></div>';
					} else {
						if(n.isRequired != "false") {
							inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '<img class="requiredimage" src="/images/required.gif" /></span><input type=' + n.fieldType + ' name=' + n.fieldName + '   class="form-control" ' + disabled + ' value=' + defaultValue + '></div></div>';
						} else {
							inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><input type=' + n.fieldType + ' name=' + n.fieldName + '   class="form-control" ' + disabled + ' value=' + defaultValue + '></div></div>';
						}
					}

					$("#fieldItemDiv").append(inputs);
				});

			} else {
				top.layer.msg(biolims.customList.updateFailed);
			}
		}
	});

	//显示自定义字段的数据
	if(fieldContent != null && fieldContent != undefined && fieldContent != "") {
		if(fieldContent && fieldContent != "null") {
			var contentData = JSON.parse(fieldContent);
			for(var k in contentData) {
				$("#fieldItemDiv input").each(function(i, inp) {
					if(inp.name == k) {
						if(inp.type == "checkbox") {
							if(contentData[k].indexOf(inp.value) != -1) {
								inp.setAttribute("checked", true);
							}
						} else {
							inp.value = contentData[k];
						}
					}
				});
				$("#fieldItemDiv option").each(function(i, val) {
					if(k == val.getAttribute("name")) {
						if(val.value == contentData[k]) {
							val.setAttribute("selected", true);
						}
					}
				});
			};
		}
	}

	//日期格式化
	$("#fieldItemDiv").find('.datepick').datepicker({
		language: "zh-TW",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});
	//多选框 格式化
	$("#fieldItemDiv").find('input[type="checkbox"]').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		increaseArea: '20%' // optional
	});

}
function showRoomManagements(){
	top.layer.open({
		title: "选择房间",
		type: 2,
		offset: ['10%', '10%'],
		area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
		btn: biolims.common.selected,
		content: [ window.ctx + "/experiment/cell/passage/cellPassage/selectRoomTable.action", ""],
		yes: function(index, layero) {
			var name = [];
			var id=[];
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addRoomTable .selected").each(function(i, v) {
				id.push($(v).children("td").eq(1).text());
				name.push($(v).children("td").eq(2).text());
			})
			if(name.length>1||id.length>1){
				top.layer.msg("请选择一条数据！");
				return false;
			}else{
				top.layer.close(index);
				$("#instrument_roomManagement_name").val(name);
				$("#instrument_roomManagement_id").val(id);
			}
		},
	});
}

//容器
function showStorageContainer(){
		top.layer.open({
		title:'选择容器 ',
		type:2,
		area:[document.body.clientWidth-300,document.body.clientHeight-100],
		btn: biolims.common.selected,
		content:[window.ctx+"/storage/container/showContainerTable.action",''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addContainer .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addContainer .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
		$("#instrument_storageContainer_id").val(id)
		$("#instrument_storageContainer_name").val(name)
		},
	})
}