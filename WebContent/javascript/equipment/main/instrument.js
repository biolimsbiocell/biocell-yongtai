var instrumentTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":biolims.tInstrument.id,
	});
	
	    fields.push({
		"data":"name",
		"title":"设备名称",
	});
	
	    fields.push({
		"data":"rank",
		"title":biolims.tInstrument.rank,
	});
	
	    fields.push({
		"data":"serialNumber",
		"title":biolims.tInstrument.serialNumber,
	});
	
	    fields.push({
		"data":"mainType-name",
		"title":biolims.tInstrument.mainType
	});
	
	    fields.push({
		"data":"spec",
		"title":biolims.tInstrument.spec,
	});
	
	    fields.push({
		"data":"state-name",
		"title":biolims.tInstrument.state
	});
	    fields.push({
			"data":"isFull",
			"title":"占用状态",
			"render": function(data, type, full, meta) {
				if(data == "0") {
					return "未占用";
				}
				if(data == "1") {
					return "占用";
				}
				else {
					return data;
				}
			}
		});
    fields.push({
		"data":"totalLocationsNumber",
		"title":"总位置数"
	});
    fields.push({
		"data":"surplusLocationsNumber",
		"title":"剩余位置数"
	});
	    fields.push({
		"data":"createUser-name",
		"title":biolims.tInstrument.createUser,
	});
	
	    fields.push({
		"data":"createDate",
		"title":biolims.tInstrument.createDate,
		"render": function(data) {
				if(data) {
					return parent.moment(data).format('YYYY-MM-DD');
				} else {
					return "";
				}
			}
	});
	
	    fields.push({
		"data":"department-name",
		"title":biolims.tInstrument.department
	});
	
	    fields.push({
		"data":"price",
		"title":biolims.tInstrument.price,
	});
	
	    fields.push({
		"data":"currencyType-name",
		"title":biolims.tInstrument.currencyType
	});
	
	    fields.push({
		"data":"note",
		"title":biolims.tInstrument.note,
	});
	
	    fields.push({
		"data":"capitalCode",
		"title":biolims.tInstrument.capitalCode,
	});
	
	    fields.push({
		"data":"effectiveDate",
		"title":biolims.tInstrument.effectiveDate,
		"render": function(data) {
				if(data) {
					return parent.moment(data).format('YYYY-MM-DD');
				} else {
					return "";
				}
			}
	});
	
	    fields.push({
		"data":"supplier-name",
		"title":biolims.tInstrument.products
	});
	
	

		$.ajax({
			type:"post",
			url:window.ctx + "/system/customfields/findFieldByModuleValue.action",
			async:false,
			data:{
				moduleValue : "Instrument"
			},
			success:function(data){
				var objData = JSON.parse(data);
				if(objData.success){
					$.each(objData.data, function(i,n) {
						var str = {
							"data" : n.fieldName,
							"title" : n.label
						}
						colData.push(str);
					});
					
				}else{
					top.layer.msg(biolims.tInstrumentBorrowDetail.tableError);
				}
			}
		});

	var options = table(true, "","/equipment/main/showInstrumentTableJson.action",
	 fields, null)
	instrumentTable = renderRememberData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(instrumentTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
		"/equipment/main/editInstrument.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/equipment/main/editInstrument.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/equipment/main/viewInstrument.action?id=" + id;
}
//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":biolims.tInstrument.id
		});
	   fields.push({
		    "searchName":"name",
			"type":"input",
			"txt":"设备名称"
		});
	   fields.push({
		    "searchName":"rank",
			"type":"input",
			"txt":biolims.tInstrument.rank
		});
	   fields.push({
		    "searchName":"serialNumber",
			"type":"input",
			"txt":biolims.tInstrument.serialNumber
		});
/*	fields.push({
	    "type":"input",
		"searchName":"mainType.id",
		"txt":biolims.purchase.typeId
	});*/
	fields.push({
	    "type":"input",
		"searchName":"mainType.name",
		"txt":biolims.tInstrument.mainType
	});
	   fields.push({
		    "searchName":"spec",
			"type":"input",
			"txt":biolims.tInstrument.spec
		});
/*	fields.push({
	    "type":"input",
		"searchName":"state.id",
		"txt":biolims.tInstrumentMaintain.state
	});*/
	fields.push({
	    "type":"input",
		"searchName":"state.name",
		"txt":biolims.tInstrument.state
	});
	   fields.push({
		    "searchName":"createUser",
			"type":"input",
			"txt":biolims.tInstrument.createUser
		});
	   fields.push({
		    "searchName":"createDate",
			"type":"input",
			"txt":biolims.tInstrument.createDate
		});
	fields.push({
			"txt": biolims.sample.createDateStart,
			"type": "dataTime",
			"searchName": "createDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt":biolims.sample.createDateEnd,
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "createDate##@@##2"
		});
/*	fields.push({
	    "type":"input",
		"searchName":"department.id",
		"txt":biolims.equipment.departmentId
	});*/
	fields.push({
	    "type":"input",
		"searchName":"department.name",
		"txt":biolims.tInstrument.department
	});
	   fields.push({
		    "searchName":"price",
			"type":"input",
			"txt":biolims.tInstrument.price
		});
/*	fields.push({
	    "type":"input",
		"searchName":"currencyType.id",
		"txt":biolims.storage.rankType
	});*/
	fields.push({
	    "type":"input",
		"searchName":"currencyType.name",
		"txt":biolims.tInstrument.currencyType
	});
	   fields.push({
		    "searchName":"note",
			"type":"input",
			"txt":biolims.tInstrument.note
		});
	   fields.push({
		    "searchName":"capitalCode",
			"type":"input",
			"txt":biolims.tInstrument.capitalCode
		});
	   fields.push({
		    "searchName":"effectiveDate",
			"type":"input",
			"txt":biolims.tInstrument.effectiveDate
		});
	fields.push({
			"txt": "投用日期(Start)",
			"type": "dataTime",
			"searchName": "effectiveDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "投用日期(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "effectiveDate##@@##2"
		});
/*	fields.push({
	    "type":"input",
		"searchName":"products.id",
		"txt":"生产商ID"
	});*/
	fields.push({
	    "type":"input",
		"searchName":"supplier.name",
		"txt":biolims.tInstrument.products
	});
	
	fields.push({
		"type":"table",
		"table":instrumentTable
	});
	return fields;
}
