var testGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'instrumentCode',
		type : "string"
	});
	fields.push({
		name : 'productId',
		type : 'string'
	});
	fields.push({
		name : 'productName',
		type : 'string'
	});
	fields.push({
		name : 'endDate',
		type : 'string'
	});
	fields.push({
		name : 'tableTypeId',
		type : 'string'
	});
	fields.push({
		name : 'stageName',
		type : 'string'
	});
	fields.push({
		name : 'acceptUser',
		type : 'string'
	});
	fields.push({
		name : 'taskId',
		type : "string"
	});
	fields.push({
		name : 'taskMethod',
		type : 'string'
	});
	fields.push({
		name : 'taskResult',
		type : "string"
	});
	fields.push({
		name : 'techTaskId',
		type : "string"
	});
	fields.push({
		name : 'startDate',
		type : "string"
	});
	fields.push({
		name : 'userGroup',
		type : 'string'
	});
	fields.push({
		name : 'countData',
		type : 'string'
	});
	fields.push({
		name : 'note',
		type : 'string'
	});
	fields.push({
		name : 'note1',
		type : 'string'
	});
	fields.push({
		name : 'mode',
		type : 'string'
	});
	fields.push({
		name : 'userGroup',
		type : 'string'
	});
	fields.push({
		name : 'countData',
		type : 'string'
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'instrumentCode',
		header : biolims.common.id,
		width : 120,
		sortable : true,
		hidden : true
	});
	cm.push({
		dataIndex : 'productId',
		header : biolims.common.productId,
		width : 120,
		sortable : true
	});
	cm.push({
		dataIndex : 'productName',
		header : biolims.common.productName,
		width : 150,
		sortable : true,
	});
	cm.push({
		dataIndex : 'mode',
		header : biolims.common.experimentalTemplate,
		width : 150,
		sortable : true,
	});
	cm.push({
		dataIndex : 'userGroup',
		header : biolims.common.userGroup,
		width : 150,
		sortable : true,
	});
	
	cm.push({
		dataIndex : 'stageTime',
		header : biolims.sample.stageTime,
		width : 150,
		sortable : true,
		hidden : true
	});
	cm.push({
		dataIndex : 'startDate',
		header : biolims.common.startTime,
		width : 150,
		sortable : true
	});
	cm.push({
		dataIndex : 'endDate',
		header : biolims.common.endTime,
		width : 150,
		sortable : true
	});
	cm.push({
		dataIndex : 'countData',
		header : biolims.common.dose,
		width : 150,
		sortable : true,
	});
	cm.push({
		dataIndex : 'tableTypeId',
		header : biolims.sample.stageId,
		width : 150,
		sortable : true,
		hidden:true
	});
	cm.push({
		dataIndex : 'stageName',
		header : biolims.common.testName,
		width : 150,
		sortable : true
	});
	cm.push({
		dataIndex : 'acceptUser',
		header : biolims.master.operUserName,
		width : 150,
		sortable : true
	});
	cm.push({
		dataIndex : 'taskId',
		header : biolims.sample.taskId,
		width : 150,
		sortable : true
	});
	cm.push({
		dataIndex : 'taskMethod',
		header : biolims.sample.taskMethod,
		width : 75,
		sortable : true
	});
	cm.push({
		dataIndex : 'taskResult',
		header : biolims.sample.taskResult,
		width : 75,
		hidden:true
	});
	cm.push({
		dataIndex : 'techTaskId',
		header : biolims.common.scienceServiceId,
		hidden:true,
		width : 75,
		sortable : true
	});
	cm.push({
		dataIndex : 'mode',
		header : biolims.common.experimentalTemplate,
		hidden:true,
		width : 75,
		sortable : true
	});
	cm.push({
		dataIndex : 'userGroup',
		header : biolims.common.userGroup,
		hidden:true,
		width : 75,
		sortable : true
	});
	cm.push({
		dataIndex : 'note',
		header : biolims.common.note,
		width : 75,
		sortable : true
	});
	cm.push({
		dataIndex : 'note1',
		header : biolims.common.noteOne,
		hidden:true,
		width : 75,
		sortable : true
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx
			+ '/equipment/main/showInstrumentTestListJson.action?instrumentId='
			+ $("#instrument_id").val();
	var opts = {};
	opts.tbar = [];
	opts.title = biolims.common.instrumentExperimentRecord;
	opts.height = document.body.clientHeight * 0.72;
	testGrid = gridEditTable("testGrid111", cols, loadParam, opts);
	$("#testGrid111").data("testGrid", testGrid);
});	



Ext.onReady(function() {
	Ext.QuickTips.init();

	// stateTextField('instrumentFault_state',document.getElementById('instrumentFault.state').value);

});


//Ext.onReady(function() {
//	var tabs = new Ext.TabPanel({
//		renderTo : 'maintab',
//		height : document.body.clientHeight-30,
//		autoWidth : true,
//		activeTab : 0,
//		margins : '0 0 0 0',
//		items : [ {
//			title : biolims.equipment.equipmentMaintenanceApplication,
//			contentEl : 'markup'
//		} ]
//	});
//});
