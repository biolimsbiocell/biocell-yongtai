var addInstrument;
$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
	});
	colOpts.push({
		"data": "name",
		"title": "设备名称",
	});
	colOpts.push({
		"data": "serialNumber",
		"title": "序列号",
	});
	colOpts.push({
		"data": "spec",
		"title": "型号",
	});
	colOpts.push({
		"data": "supplier-id",
		"title": "生产商ID",
	});
	colOpts.push({
		"data": "supplier-name",
		"title": "生产商名称",
	});
	var tbarOpts = [];
	var options = table(true, null,
		'/equipment/main/showInstrumentDialogListJson.action', colOpts, tbarOpts)
	addInstrument = renderData($("#addInstrument"), options);
	$("#addInstrument").on(
		'init.dt',
		function(e, settings) {
			// 清除操作按钮
			$("#addInstrument_wrapper .dt-buttons").empty();
			$('#addInstrument_wrapper').css({
				"padding": "0 16px"
			});
//			var trs = $("#addInstrument tbody tr");
			addInstrument.ajax.reload();
//			addInstrument.on('draw', function() {
//				trs = $("#addInstrument tbody tr");
//				trs.click(function() {
//					$(this).addClass("chosed").siblings("tr")
//						.removeClass("chosed");
//				});
//			});
//			trs.click(function() {
//				$(this).addClass("chosed").siblings("tr")
//					.removeClass("chosed");
//			});
		});

})
function query(){
	var name = $.trim($("#name").val());
	var searchItemLayerValue = {};
	var k1 = $("#name").attr("searchname");
	if(name != null && name != "") {
		searchItemLayerValue[k1] = "%" + name + "%";
	} else {
		searchItemLayerValue[k1] = name;
	}
	var sid = $.trim($("#id").val());
	var k2 = $("#id").attr("searchname");
	if(sid != null && sid != "") {
		searchItemLayerValue[k2] = "%" + sid + "%";
	} else {
		searchItemLayerValue[k2] = sid;
	}
	var query = JSON.stringify(searchItemLayerValue);
	var param = {
		query: query
	};
	sessionStorage.setItem("searchContent", query);
	addInstrument.settings()[0].ajax.data = param;
	addInstrument.ajax.reload();
}