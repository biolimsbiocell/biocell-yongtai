var instrumentTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":biolims.tInstrument.id,
	});
	
	    fields.push({
		"data":"name",
		"title":biolims.tInstrument.name,
	});
	
	    fields.push({
		"data":"rank",
		"title":biolims.tInstrument.rank,
	});
	
	    fields.push({
		"data":"serialNumber",
		"title":biolims.tInstrument.serialNumber,
	});
	
	    fields.push({
		"data":"mainType-id",
		"title":biolims.purchase.typeId
	});
	    fields.push({
		"data":"mainType-name",
		"title":biolims.tInstrument.mainType
	});
	
	    fields.push({
		"data":"spec",
		"title":biolims.tInstrument.spec,
	});
	
	    fields.push({
		"data":"state-id",
		"title":biolims.tInstrumentMaintain.state
	});
	    fields.push({
		"data":"state-name",
		"title":biolims.tInstrument.state
	});
	
	    fields.push({
		"data":"createUser",
		"title":biolims.tInstrument.createUser,
	});
	
	    fields.push({
		"data":"createDate",
		"title":biolims.tInstrument.createDate,
	});
	
	    fields.push({
		"data":"department-id",
		"title":biolims.equipment.departmentId
	});
	    fields.push({
		"data":"department-name",
		"title":biolims.tInstrument.department
	});
	
	    fields.push({
		"data":"price",
		"title":biolims.tInstrument.price,
	});
	
	    fields.push({
		"data":"currencyType-id",
		"title":biolims.storage.rankType
	});
	    fields.push({
		"data":"currencyType-name",
		"title":biolims.tInstrument.currencyType
	});
	
	    fields.push({
		"data":"note",
		"title":biolims.tInstrument.note,
	});
	
	    fields.push({
		"data":"capitalCode",
		"title":biolims.tInstrument.capitalCode,
	});
	
	    fields.push({
		"data":"effectiveDate",
		"title":biolims.tInstrument.effectiveDate,
	});
	
	    fields.push({
		"data":"supplier-id",
		"title":biolims.tStorage.supplierId
	});
	    fields.push({
		"data":"supplier-name",
		"title":biolims.tInstrument.products
	});
	
	    fields.push({
		"data":"showstate",
		"title":biolims.tInstrument.showstate,
	});
	
	var options = table(true, "","/equipment/main/showInstrumentTableJson.action",
	 fields, null)
	instrumentTable = renderData($("#addInstrumentTable"), options);
	$('#addInstrumentTable').on('init.dt', function() {
		recoverSearchContent(instrumentTable);
	})
});

//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":biolims.tInstrument.id
		});
	   fields.push({
		    "searchName":"name",
			"type":"input",
			"txt":biolims.tInstrument.name
		});
	   fields.push({
		    "searchName":"rank",
			"type":"input",
			"txt":biolims.tInstrument.rank
		});
	   fields.push({
		    "searchName":"serialNumber",
			"type":"input",
			"txt":biolims.tInstrument.serialNumber
		});
	fields.push({
	    "type":"input",
		"searchName":"mainType.id",
		"txt":"分类ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"mainType.name",
		"txt":biolims.tInstrument.mainType
	});
	   fields.push({
		    "searchName":"spec",
			"type":"input",
			"txt":biolims.tInstrument.spec
		});
	fields.push({
	    "type":"input",
		"searchName":"state.id",
		"txt":"状态ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"state.name",
		"txt":biolims.tInstrument.state
	});
	   fields.push({
		    "searchName":"createUser",
			"type":"input",
			"txt":biolims.tInstrument.createUser
		});
	   fields.push({
		    "searchName":"createDate",
			"type":"input",
			"txt":biolims.tInstrument.createDate
		});
	fields.push({
			"txt": "创建时间(Start)",
			"type": "dataTime",
			"searchName": "createDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "创建时间(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "createDate##@@##2"
		});
	fields.push({
	    "type":"input",
		"searchName":"department.id",
		"txt":"部门ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"department.name",
		"txt":biolims.tInstrument.department
	});
	   fields.push({
		    "searchName":"price",
			"type":"input",
			"txt":biolims.tInstrument.price
		});
	fields.push({
	    "type":"input",
		"searchName":"currencyType.id",
		"txt":"币种ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"currencyType.name",
		"txt":biolims.tInstrument.currencyType
	});
	   fields.push({
		    "searchName":"note",
			"type":"input",
			"txt":biolims.tInstrument.note
		});
	   fields.push({
		    "searchName":"capitalCode",
			"type":"input",
			"txt":biolims.tInstrument.capitalCode
		});
	   fields.push({
		    "searchName":"effectiveDate",
			"type":"input",
			"txt":biolims.tInstrument.effectiveDate
		});
	fields.push({
			"txt": "投用日期(Start)",
			"type": "dataTime",
			"searchName": "effectiveDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "投用日期(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "effectiveDate##@@##2"
		});
	fields.push({
	    "type":"input",
		"searchName":"supplier.id",
		"txt":"生产商ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"supplier.name",
		"txt":biolims.tInstrument.products
	});
	   fields.push({
		    "searchName":"showstate",
			"type":"input",
			"txt":biolims.tInstrument.showstate
		});
	
	fields.push({
		"type":"table",
		"table":instrumentTable
	});
	return fields;
}
