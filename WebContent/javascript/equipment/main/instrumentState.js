var instrumentStateTable;
var oldinstrumentStateChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表
	var id = $("#instrument_id").val();
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push({
		"data":"instrumentCode",
		"title": biolims.tInstrumentState.instrumentCode,
		"createdCell": function(td) {
			$(td).attr("saveName", "instrumentCode");
	    },
		"visible": false,	
		
	});
	colOpts.push({
		"data":"stageName",
		"title": "占用实验名称",
		"createdCell": function(td) {
			$(td).attr("saveName", "stageName");
	    },
	    "visible": false,	
	});
	colOpts.push({
		"data":"tableTypeId",
		"title": "占用实验单号",
		"createdCell": function(td) {
			$(td).attr("saveName", "tableTypeId");
	    },
	    "visible": false,	
	});
	colOpts.push({
		"data":"batch",
		"title": "批次号",
		"createdCell": function(td) {
			$(td).attr("saveName", "batch");
	    },
	});
	colOpts.push({
		"data":"name",
		"title": "患者姓名",
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
	    },
	});
	colOpts.push({
		"data":"note",
		"title": "占用实验阶段",
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
	    },
	});
	colOpts.push({
		"data":"note2",
		"title": "占用实验阶段名称",
		"createdCell": function(td) {
			$(td).attr("saveName", "note2");
	    },
	});
	colOpts.push({
		"data":"acceptUser",
		"title": "操作人",
		"createdCell": function(td) {
			$(td).attr("saveName", "acceptUser");
	    },
	});
	colOpts.push({
		"data":"startDate",
		"title": "开始时间",
		"createdCell": function(td) {
			$(td).attr("saveName", "startDate");
	    },
	});
	colOpts.push({
		"data":"endDate",
		"title": "结束时间",
		"createdCell": function(td) {
			$(td).attr("saveName", "endDate");
	    },
	});
	colOpts.push({
		"data":"instrumentState",
		"title": "设备状态",
		"createdCell": function(td) {
			$(td).attr("saveName", "instrumentState");
	    },
	});
//	   colOpts.push({
//		"data":"stageName",
//		"title": biolims.tInstrumentState.stageName,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "stageName");
//	    },
//		
//	});
//	   colOpts.push({
//		"data":"acceptUser",
//		"title": biolims.tInstrumentState.acceptUser,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "acceptUser");
//	    },
//		
//	});
//	   colOpts.push({
//		"data":"taskId",
//		"title": biolims.tInstrumentState.taskId,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "taskId");
//	    },
//		
//	});
//	   colOpts.push({
//		"data":"taskMethod",
//		"title": biolims.tInstrumentState.taskMethod,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "taskMethod");
//	    },
//		
//	});
//	   colOpts.push({
//		"data":"taskResult",
//		"title": biolims.tInstrumentState.taskResult,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "taskResult");
//	    },
//		
//	});
//	   colOpts.push({
//		"data":"techTaskId",
//		"title": biolims.tInstrumentState.techTaskId,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "techTaskId");
//	    },
//		
//	});
//	   colOpts.push({
//		"data":"mode",
//		"title": biolims.tInstrumentState.mode,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "mode");
//	    },
//		
//	});
//	   colOpts.push({
//		"data":"userGroup",
//		"title": biolims.tInstrumentState.userGroup,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "userGroup");
//	    },
//		
//	});
//	   colOpts.push({
//		"data":"countData",
//		"title": biolims.tInstrumentState.countData,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "countData");
//	    },
//		
//	});
//	   colOpts.push({
//		"data":"note",
//		"title": biolims.tInstrumentState.note,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "note");
//	    },
////		"className": "textarea"
//	});
//	   colOpts.push({
//		"data":"instrumentName",
//		"title": biolims.tInstrumentState.instrumentName,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "instrumentName");
//	    },
//		"visible": false,	
//		
//	});
//	colOpts.push( {
//		"data": "instrument-id",
//		"title": biolims.common.relatedMainTableId,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "instrument-id");
//		}
//	});
//	colOpts.push( {
//		"data": "instrument-name",
//		"title": biolims.tInstrumentState.instrument,
//		"createdCell": function(td, data, rowData) {
//			$(td).attr("saveName", "instrument-name");
//			$(td).attr("instrument-id", rowData['instrumentr-id']);
//		}
//	});
//	   colOpts.push({
//		"data":"instrumentState",
//		"title": biolims.tInstrumentState.instrumentState,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "instrumentState");
//	    },
//		
//	});
//	   colOpts.push({
//		"data":"productId",
//		"title": biolims.tInstrumentState.productId,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "productId");
//	    },
//		
//	});
//	   colOpts.push({
//		"data":"productName",
//		"title": biolims.tInstrumentState.productName,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "productName");
//	    },
//		
//	});
//	   colOpts.push({
//		"data":"stageTime",
//		"title": biolims.tInstrumentState.stageTime,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "stageTime");
//	    },
//		
//	});
//	   colOpts.push({
//		"data":"startDate",
//		"title": biolims.tInstrumentState.startDate,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "startDate");
//	    },
//		
//	});
//	   colOpts.push({
//		"data":"endDate",
//		"title": biolims.tInstrumentState.endDate,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "endDate");
//	    },
//		
//	});
//	   colOpts.push({
//		"data":"tableTypeId",
//		"title": biolims.tInstrumentState.tableTypeId,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "tableTypeId");
//	    },
//		
//	});
	var handlemethod = $("#handlemethod").val();
/*	if (handlemethod != "view") {
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#instrumentStateTable"))
	}
	});
	tbarOpts.push({
		text: biolims.common.addwindow,
		action: function() {
			addItemLayer($("#instrumentStateTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.editwindow,
		action: function() {
			editItemLayer($("#instrumentStateTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.batchUpload,
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#instrumentState_id").val(),
						fileId: data.response.fileId
					},
					url: ctx + "/equipment/main/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							 instrumentStateTable.ajax.reload();
						} else {
							top.layer.msg(biolims.common.uploadFailed)
						}
		
					}
				});
			});
			
		}
		});
	
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveInstrumentState($("#instrumentStateTable"));
		}
	});
	}*/
	
	var instrumentStateOptions = table(true,
		id,
		'/equipment/main/showInstrumentStateTableJson.action', colOpts, tbarOpts)
	instrumentStateTable = renderData($("#instrumentStateTable"), instrumentStateOptions);
	instrumentStateTable.on('draw', function() {
		oldinstrumentStateChangeLog = instrumentStateTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});

// 保存
//function saveInstrumentState(ele) {
//	var data = saveInstrumentStatejson(ele);
//	var ele=$("#instrumentStateTable");
//	var changeLog = "设备报修明细：";
//	changeLog = getChangeLog(data, ele, changeLog);
//	var changeLogs="";
//	if(changeLog !="设备报修明细："){
//		changeLogs=changeLog;
//	}
//	top.layer.load(4, {shade:0.3}); 
//	$.ajax({
//		type: 'post',
//		url: '/equipment/main/saveInstrumentStateTable.action',
//		data: {
//			id: $("#instrument_id").val(),
//			dataJson: data,
//			changeLog: changeLogs
//		},
//		success: function(data) {
//			var data = JSON.parse(data)
//			if(data.success) {
//				top.layer.closeAll();
//				top.layer.msg(biolims.common.saveSuccess);
//				tableRefresh();
//			} else {
//				top.layer.closeAll();
//				top.layer.msg(biolims.common.saveFailed)
//			};
//		}
//	})
//}
// 获得保存时的json数据
//function saveInstrumentStatejson(ele) {
//	var trs = ele.find("tbody").children(".editagain");
//	var data = [];
//	trs.each(function(i, val) {
//		var json = {};
//		var tds = $(val).children("td");
//		json["id"] = $(tds[0]).find("input").val();
//		for(var j = 1; j < tds.length; j++) {
//			var k = $(tds[j]).attr("savename");
//			// 判断并转换为数字
//			
//			if(k == "instrument-name") {
//				json["instrument-id"] = $(tds[j]).attr("instrument-id");
//				continue;
//			}
//		
//			json[k] = $(tds[j]).text();
//		}
//		data.push(json);
//	});
//	return JSON.stringify(data);
//}

//function getChangeLog(data, ele, changeLog) {
//	var saveJson = JSON.parse(data);
//	saveJson.forEach(function(v, i) {
//		var id = v.id;
//		if (!id) {
//			changeLog += '新增记录:';
//			for ( var k in v) {
//				var title = ele.find("th[savename=" + k + "]").text();
//				changeLog += '"' + title + '"为"' + v[k] + '";';
//			}
//			return true;
//		}
//		changeLog += 'ID为"' + v.id + '":';
//		oldinstrumentStateChangeLog.data.forEach(function(vv, ii) {
//			if(vv.id == id) {
//				for(var k in v) {
//					if(v[k] != vv[k]) {
//						var title = ele.find("th[savename=" + k + "]").text();
//						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
//					}
//				}
//				return false;
//			}
//		});
//	});
//	return changeLog;
//}
