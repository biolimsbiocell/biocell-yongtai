var showInstrumentGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
			name:'name',
			type:"string"
		});
	    fields.push({
	    	name : 'spec',
			type : 'string'
		});
	    fields.push({
	    	name : 'producer-name',
			type : 'string'
		});
	    fields.push({
	    	name : 'position-id',
			type : 'string'
		});
	    fields.push({
	    	name : 'dutyUser-name',
			type : 'string'
		});
	    fields.push({
	    	name : 'dutyUserTwo-name',
			type : 'string'
		});
	    fields.push({
	    	name : 'rank',
			type : 'string'
		});
	    fields.push({
	    	name : 'serialNumber',
			type : 'string'
		});
	    fields.push({
			name:'note',
			type:"string"
		});
	    fields.push({
	    	name : 'enquiryOdd-number',
			type : 'string'
		});
	    fields.push({
	    	name : 'state-name',
			type : 'string'
		});
	    fields.push({
	    	name : 'studyType-name',
			type : 'string'
		});
	    fields.push({
	    	name : 'createUser-name',
			type : 'string'
		});
	    fields.push({
	    	name : 'createDate',
	    	type : "date",
			dateFormat : "Y-m-d"
		});
	    fields.push({
	    	name : 'outCode',
			type : 'string'
		});
	    fields.push({
	    	name : 'accuracy',
			type : 'string'
		});
	    fields.push({
	    	name : 'purchaseDate',
	    	type : "date",
			dateFormat : "Y-m-d"
		});
	    fields.push({
	    	name : 'surveyScope',
			type : 'string'
		});
	    fields.push({
	    	name : 'checkItem',
			type : 'string'
		});
	    fields.push({
	    	name : 'type-id',
			type : 'string'
		});
	    fields.push({
	    	name : 'type-name',
			type : 'string'
		});
	    fields.push({
	    	name : 'supplier-name',
			type : 'string'
		});
	    fields.push({
	    	name : 'effectiveDate',
	    	type : "date",
			dateFormat : "Y-m-d"
		});
	    fields.push({
	    	name : 'expiryDate',
	    	type : "date",
			dateFormat : "Y-m-d"
		});
	    fields.push({
	    	name : 'nextDate',
	    	type : "date",
			dateFormat : "Y-m-d"
		});
	cols.fields=fields;
	var cm=[];
	 cm.push({
			dataIndex : 'id',
			header : "EQL",
			width : 120,
			sortable : true
		}); cm.push({
			dataIndex : 'name',
			header : biolims.common.instrumentName,
			width : 200,
			sortable : true,
		}); cm.push({
			dataIndex : 'spec',
			header : biolims.equipment.spec,
			width : 150,
			sortable : true
		}); cm.push({
			dataIndex : 'producer-name',
			header : biolims.equipment.producerName,
			width : 100,
			sortable : true
		});cm.push({
			dataIndex : 'type-name',
			header : biolims.storage.studyType,
			width : 100,
			sortable : true
		}); cm.push({
			dataIndex : 'position-id',
			header : biolims.equipment.positionId,
			width : 120,
			sortable : true,
			hidden : true
		}); cm.push({
			dataIndex : 'dutyUser-name',
			header : biolims.master.personName,
			width : 150,
			sortable : true,
			hidden : true
		}); cm.push({
			dataIndex : 'dutyUserTwo-name',
			header : biolims.master.person2Name,
			width : 150,
			sortable : true,
			hidden : true
		});cm.push({
			dataIndex : 'rank',
			header : biolims.equipment.level,
			width : 150,
			sortable : true
		});cm.push({
			dataIndex : 'serialNumber',
			header : biolims.equipment.serialNumber,
			width : 150,
			sortable : true
		});cm.push({
			dataIndex : 'enquiryOddNumber',
			header : biolims.equipment.enquiryOddNumber,
			width : 150,
			sortable : true,
			hidden : true
		});cm.push({
			dataIndex : 'state-name',
			header : biolims.common.stateName,
			width : 75,
			sortable : true,
			hidden : false
		}); cm.push({
			dataIndex : 'studyType-name',
			header : biolims.equipment.studyTypeName,
			width : 125,
			sortable : true,
			hidden : true
		}); cm.push({
			dataIndex : 'createUser-name',
			header : biolims.sample.createUserName,
			width : 150,
			sortable : true,
			hidden : true
		}); cm.push({
			dataIndex : 'createDate',
			header : biolims.sample.createDate,
			width : 150,
			sortable : true,
			hidden : true
		}); cm.push({
			dataIndex : 'outCode',
			header : biolims.equipment.outCode,
			width : 200,
			sortable : true,
			hidden : true
		}); cm.push({
			dataIndex : 'accuracy',
			header : biolims.equipment.accuracy,
			width : 200,
			sortable : true,
			hidden : true
		}); cm.push({
			dataIndex : 'purchaseDate',
			header : biolims.equipment.purchaseDate,
			width : 200,
			sortable : true,
			hidden : true
		}); cm.push({
			dataIndex : 'surveyScope',
			header : biolims.equipment.surveyScope,
			width : 200,
			sortable : true,
			hidden : true
		}); cm.push({
			dataIndex : 'checkItem',
			header : biolims.equipment.checkItem,
			width : 200,
			sortable : true,
			hidden : true
		}); cm.push({
			dataIndex : 'producerLinkMan',
			header : biolims.equipment.producerLinkMan,
			width : 200,
			sortable : true,
			hidden : true
		}); cm.push({
			dataIndex : 'producerLinkTel',
			header : biolims.equipment.producerLinkTel,
			width : 200,
			sortable : true,
			hidden : true
		}); cm.push({
			dataIndex : 'supplierName',
			header : biolims.equipment.supplierName,
			width : 200,
			sortable : true,
			hidden : true
		}); cm.push({
			dataIndex : 'supplierLinkMan',
			header : biolims.equipment.supplierLinkMan,
			width : 200,
			sortable : true,
			hidden : true
		}); cm.push({
			dataIndex : 'supplierLinkTel',
			header : biolims.equipment.supplierLinkTel,
			width : 200,
			sortable : true,
			hidden : true
		});cm.push({
			dataIndex : 'effectiveDate',
			header :"启用日期",
			width : 100,
			sortable : true,
			hidden : true
		});cm.push({
			dataIndex : 'expiryDate',
			header : "结束日期",
			width : 100,
			sortable : true,
			hidden : true
		});cm.push({
			dataIndex : 'nextDate',
			header : "下个检测日期",
			width : 100,
			sortable : true,
			hidden : true
		});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/equipment/repair/showInstrumentJson.action";
	var opts={};
	opts.title=biolims.common.selectInstrument;
	opts.tbar = [];
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.height=document.body.clientHeight*0.65;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
	};
//	opts.tbar.push({
//		text : biolims.common.retrieve,
//		handler : function() {
//			var options = {};
//			options.width = 500;
//			options.height = 300;
//			loadDialogPage($("#jstj"), biolims.common.retrieve, null, {
//				"Confirm": function() {
//					commonSearchAction(showInstrumentGrid);
//					$(this).dialog("close");
//				},
//				"清空(Empty)" : function() {
//					form_reset();
//				}
//			}, true, options);
//		}
//	});
	showInstrumentGrid=gridEditTable("show_instrument_div",cols,loadParam,opts);
	$("#show_instrument_div").data("showInstrumentGrid", showInstrumentGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
function searchInstrument(){
	var grid=$("#show_instrument_div").data("showInstrumentGrid");
	commonSearchAction(grid);
}