var instrumentRepairTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":biolims.tInstrumentRepair.id,
	});
	
	    fields.push({
		"data":"note",
		"title":biolims.tInstrumentRepair.note,
	});
	
	    fields.push({
		"data":"startDate",
		"title":biolims.tInstrumentRepair.startDate,
	});
	
	    fields.push({
		"data":"endDate",
		"title":biolims.tInstrumentRepair.endDate,
	});
	
	    fields.push({
		"data":"factEndDate",
		"title":biolims.tInstrumentRepair.factEndDate,
	});
	
	    fields.push({
		"data":"name",
		"title":biolims.tInstrumentRepair.name,
	});
	
	  
	    fields.push({
		"data":"creatDate",
		"title":biolims.tInstrumentRepair.creatDate
	});
	
	    fields.push({
		"data":"creatUser-id",
		"title":biolims.tInstrumentRepair.creatUser+"ID"
	});
	    fields.push({
		"data":"creatUser-name",
		"title":biolims.tInstrumentRepair.creatUser
	});
	
	    fields.push({
		"data":"verify",
		"title":biolims.tInstrumentRepair.verify,
	});
	
	    fields.push({
		"data":"factStartDate",
		"title":biolims.tInstrumentRepair.factStartDate,
	});
	
	    fields.push({
		"data":"type-id",
		"title":biolims.tInstrumentRepair.type+"ID"
	});
	    fields.push({
		"data":"type-name",
		"title":biolims.tInstrumentRepair.type
	});
	
	    fields.push({
		"data":"stateName",
		"title":biolims.tInstrumentRepair.stateName,
	});
	
	    fields.push({
		"data":"tInstrumentRepairPlan-id",
		"title":biolims.tInstrumentRepair.tInstrumentRepairPlan+"ID"
	});
	    fields.push({
		"data":"tInstrumentRepairPlan-name",
		"title":biolims.tInstrumentRepair.tInstrumentRepairPlan
	});
	
	var options = table(true, "","/equipment/instrumentRepair/showInstrumentRepairTableJson.action",
	 fields, null)
	instrumentRepairTable = renderData($("#addInstrumentRepairTable"), options);
	$('#addInstrumentRepairTable').on('init.dt', function() {
		recoverSearchContent(instrumentRepairTable);
	})
});

//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":biolims.tInstrumentRepair.id
		});
	   fields.push({
		    "searchName":"note",
			"type":"input",
			"txt":biolims.tInstrumentRepair.note
		});
	   fields.push({
		    "searchName":"startDate",
			"type":"input",
			"txt":biolims.tInstrumentRepair.startDate
		});
	fields.push({
			"txt": biolims.tInstrumentRepair.startDate+"(Start)",
			"type": "dataTime",
			"searchName": "startDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.tInstrumentRepair.startDate+"(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "startDate##@@##2"
		});
	   fields.push({
		    "searchName":"endDate",
			"type":"input",
			"txt":biolims.tInstrumentRepair.endDate
		});
	fields.push({
			"txt": biolims.tInstrumentRepair.endDate+"(Start)",
			"type": "dataTime",
			"searchName": "endDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.tInstrumentRepair.endDate+"(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "endDate##@@##2"
		});
	   fields.push({
		    "searchName":"factEndDate",
			"type":"input",
			"txt":biolims.tInstrumentRepair.factEndDate
		});
	fields.push({
			"txt": biolims.tInstrumentRepair.factEndDate+"(Start)",
			"type": "dataTime",
			"searchName": "factEndDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.tInstrumentRepair.factEndDate+"(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "factEndDate##@@##2"
		});
	   fields.push({
		    "searchName":"name",
			"type":"input",
			"txt":biolims.tInstrumentRepair.name
		});
	
	fields.push({
	    "type":"input",
		"searchName":"creatDate",
		"txt":biolims.tInstrumentRepair.creatDate
	});
	fields.push({
			"txt": biolims.tInstrumentRepair.creatDate+"(Start)",
			"type": "dataTime",
			"searchName": "creatDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.tInstrumentRepair.creatDate+"(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "creatDate##@@##2"
		});
	fields.push({
	    "type":"input",
		"searchName":"creatUser.id",
		"txt":biolims.tInstrumentRepair.creatUser+"ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"creatUser.name",
		"txt":biolims.tInstrumentRepair.creatUser
	});
	   fields.push({
		    "searchName":"verify",
			"type":"input",
			"txt":biolims.tInstrumentRepair.verify
		});
	   fields.push({
		    "searchName":"factStartDate",
			"type":"input",
			"txt":biolims.tInstrumentRepair.factStartDate
		});
	fields.push({
			"txt": biolims.tInstrumentRepair.factStartDate+"(Start)",
			"type": "dataTime",
			"searchName": "factStartDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.tInstrumentRepair.factStartDate+"(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "factStartDate##@@##2"
		});
	fields.push({
	    "type":"input",
		"searchName":"type.id",
		"txt":biolims.tInstrumentRepair.type+"ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"type.name",
		"txt":biolims.tInstrumentRepair.type
	});
	   fields.push({
		    "searchName":"stateName",
			"type":"input",
			"txt":biolims.tInstrumentRepair.stateName
		});
	fields.push({
	    "type":"input",
		"searchName":"tInstrumentRepairPlan.id",
		"txt":biolims.tInstrumentRepair.tInstrumentRepairPlan+"ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"tInstrumentRepairPlan.name",
		"txt":biolims.tInstrumentRepair.tInstrumentRepairPlan
	});
	
	fields.push({
		"type":"table",
		"table":instrumentRepairTable
	});
	return fields;
}
