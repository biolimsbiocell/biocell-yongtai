var instrumentRepairTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":biolims.tInstrumentRepair.id,
	});
	
	    fields.push({
		"data":"note",
		"title":biolims.tInstrumentRepair.note,
		
	});
	
//	    fields.push({
//		"data":"startDate",
//		"title":biolims.tInstrumentRepair.startDate,
//		"render": function(data) {
//				if(data) {
//					return parent.moment(data).format('YYYY-MM-DD');
//				} else {
//					return "";
//				}
//			},
//	});
//	
//	    fields.push({
//		"data":"endDate",
//		"title":biolims.tInstrumentRepair.endDate,
//		"render": function(data) {
//				if(data) {
//					return parent.moment(data).format('YYYY-MM-DD');
//				} else {
//					return "";
//				}
//			},
//	});
	
	    fields.push({
		"data":"factEndDate",
		"title":biolims.tInstrumentRepair.factEndDate,
		"render": function(data) {
				if(data) {
					return parent.moment(data).format('YYYY-MM-DD');
				} else {
					return "";
				}
			}
	});
	
	    fields.push({
		"data":"name",
		"title":biolims.tInstrumentRepair.name,
	});
	
//	    fields.push({
//		"data":"createDate",
//		"title":biolims.common.createDate,
//	});
	    fields.push({
		"data":"createUser-id",
		"title":biolims.common.createUserId
	});
	    fields.push({
		"data":"createUser-name",
		"title":biolims.tInstrumentRepair.creatUser
	});
	
	    fields.push({
		"data":"verify",
		"title":biolims.tInstrumentRepair.verify,
	});
	
//	    fields.push({
//		"data":"factStartDate",
//		"title":biolims.tInstrumentRepair.factStartDate,
//		"render": function(data) {
//				if(data) {
//					return parent.moment(data).format('YYYY-MM-DD');
//				} else {
//					return "";
//				}
//			}
//	});
	
//	    fields.push({
//		"data":"type-id",
//		"title":biolims.common.maintenanceTypeID
//	});
//	    fields.push({
//		"data":"type-name",
//		"title":biolims.tInstrumentRepair.type
//	});
	
	    fields.push({
		"data":"stateName",
		"title":biolims.tInstrumentRepair.stateName,
	});
	
//	    fields.push({
//		"data":"instrumentRepairPlan-id",
//		"title":biolims.tInstrumentRepair.tInstrumentRepairPlan+"ID"
//	});
//	    fields.push({
//		"data":"instrumentRepairPlan-name",
//		"title":biolims.tInstrumentRepair.tInstrumentRepairPlan
//	});
	

		$.ajax({
			type:"post",
			url:window.ctx + "/system/customfields/findFieldByModuleValue.action",
			async:false,
			data:{
				moduleValue : "InstrumentRepair"
			},
			success:function(data){
				var objData = JSON.parse(data);
				if(objData.success){
					$.each(objData.data, function(i,n) {
						var str = {
							"data" : n.fieldName,
							"title" : n.label
						}
						colData.push(str);
					});
					
				}else{
					top.layer.msg(biolims.tInstrumentBorrowDetail.tableError);
				}
			}
		});

	var options = table(true, "","/equipment/repair/showInstrumentRepairTableJsonNew.action",
	 fields, null)
	instrumentRepairTable = renderRememberData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(instrumentRepairTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
		"/equipment/repair/editInstrumentRepairNew.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/equipment/repair/editInstrumentRepairNew.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/equipment/repair/viewInstrumentRepairNew.action?id=" + id;
}
//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":biolims.tInstrumentRepair.id
		});
	   fields.push({
		    "searchName":"note",
			"type":"input",
			"txt":biolims.tInstrumentRepair.note
		});
	  /* fields.push({
		    "searchName":"startDate",
			"type":"input",
			"txt":biolims.tInstrumentRepair.startDate
		});
	fields.push({
			"txt": biolims.tInstrumentRepair.startDate+"(Start)",
			"type": "dataTime",
			"searchName": "startDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.tInstrumentRepair.startDate+"(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "startDate##@@##2"
		});
	   fields.push({
		    "searchName":"endDate",
			"type":"input",
			"txt":biolims.tInstrumentRepair.endDate
		});
	fields.push({
			"txt": biolims.tInstrumentRepair.endDate+"(Start)",
			"type": "dataTime",
			"searchName": "endDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.tInstrumentRepair.endDate+"(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "endDate##@@##2"
		});*/
	   fields.push({
		    "searchName":"factEndDate",
			"type":"input",
			"txt":biolims.tInstrumentRepair.factEndDate
		});
	fields.push({
			"txt": biolims.tInstrumentRepair.factEndDate+"(Start)",
			"type": "dataTime",
			"searchName": "factEndDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.tInstrumentRepair.factEndDate+"(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "factEndDate##@@##2"
		});
	   fields.push({
		    "searchName":"name",
			"type":"input",
			"txt":biolims.tInstrumentRepair.name
		});

	/*fields.push({
	    "type":"input",
		"searchName":"creatDate",
		"txt":biolims.tInstrumentRepair.creatDate
	});
	fields.push({
			"txt": biolims.tInstrumentRepair.creatDate+"(Start)",
			"type": "dataTime",
			"searchName": "creatDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.tInstrumentRepair.creatDate+"(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "creatDate##@@##2"
		});*/
	fields.push({
	    "type":"input",
		"searchName":"creatUser.id",
		"txt":biolims.common.createUserId
	});
	fields.push({
	    "type":"input",
		"searchName":"creatUser.name",
		"txt":biolims.tInstrumentRepair.creatUser
	});
	   fields.push({
		    "searchName":"verify",
			"type":"input",
			"txt":biolims.tInstrumentRepair.verify
		});
	 /*  fields.push({
		    "searchName":"factStartDate",
			"type":"input",
			"txt":biolims.tInstrumentRepair.factStartDate
		});
	fields.push({
			"txt": biolims.tInstrumentRepair.factStartDate+"(Start)",
			"type": "dataTime",
			"searchName": "factStartDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.tInstrumentRepair.factStartDate+"(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "factStartDate##@@##2"
		});
	fields.push({
	    "type":"input",
		"searchName":"type.id",
		"txt":biolims.common.maintenanceTypeID
	});
	fields.push({
	    "type":"input",
		"searchName":"type.name",
		"txt":biolims.tInstrumentRepair.type
	});*/
	   fields.push({
		    "searchName":"stateName",
			"type":"input",
			"txt":biolims.tInstrumentRepair.stateName
		});/*
	fields.push({
	    "type":"input",
		"searchName":"tInstrumentRepairPlan.id",
		"txt":biolims.tInstrumentRepair.tInstrumentRepairPlan+"ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"tInstrumentRepairPlan.name",
		"txt":biolims.tInstrumentRepair.tInstrumentRepairPlan
	});*/
	
	fields.push({
		"type":"table",
		"table":instrumentRepairTable
	});
	return fields;
}
