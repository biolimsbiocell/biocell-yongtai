var gridGrid;
Ext
		.onReady(function() {
			Ext.QuickTips.init();
			Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
			Ext.grid.RowNumberer = Ext.extend(Ext.grid.RowNumberer, {
				width : 30,
				renderer : function(value, cellmeta, record, rowIndex,
						columnIndex, store) {
					return store.lastOptions.params.start + rowIndex + 1;
				}
			});
var store = new Ext.data.JsonStore({
										root : 'results',
						totalProperty : 'total',
						remoteSort : true,
						fields : [ {
							name : 'id',
							type : 'string'
						},{
							name : 'name',
							type : 'string'
						}, {
							name : 'type-name',
							type : 'string'
						}, {
							name : 'stateName',
							type : 'string'
						}, {
							name : 'instrumentRepairPlan-id',
							type : 'string'
						}, {
							name : 'note',
							type : 'string'
						}, {
							name : 'createUser-name',
							type : 'string'
						}, {
							name : 'createDate',
							type : 'string'
						}, {
							name : 'verify',
							type : 'string'
						}, {
							name : 'startDate',
							type : 'string'
						}, {
							name : 'endDate',
							type : 'string'
						}, {
							name : 'factStartDate',
							type : 'string'
						}, {
							name : 'factEndDate',
							type : 'string'
						} ],
proxy: new Ext.data.HttpProxy({url: ctx+'/equipment/repair/showInstrumentRepairListJson.action?p_type='+$("#p_type").val(),method: 'POST'})
});
gridGrid = new Ext.grid.GridPanel({
										autoWidth : true,
						id : gridGrid,
						titleCollapse : true,
						autoScroll : true,
						height : document.body.clientHeight - 35,
						iconCls : 'icon-grid',
						title :biolims.common.instrumentMaintenance,
						collapsible : true,
						store : store,
						columnLines : true,
						trackMouseOver : true,
						loadMask : true,
						colModel : new Ext.ux.grid.LockingColumnModel([
								new Ext.grid.RowNumberer(), {
									dataIndex : 'id',
									header : biolims.common.id,
									width : 100,
									sortable : true
								}, {
									dataIndex : 'name',
									header : biolims.common.name,
									width : 100,
									sortable : true
								}, {
									dataIndex : 'createDate',
									header : biolims.common.createDate,
									width : 150,
									sortable : true,
									hidden : false
								}, {
									dataIndex : 'createUser-name',
									header : biolims.common.createUserName,
									width : 150,
									sortable : true
								}, {
									dataIndex : 'verify',
									header : biolims.common.maintenanceUnit,
									width : 150,
									sortable : true
								}, {
									dataIndex : 'factStartDate',
									header : biolims.common.maintenanceDate,
									width : 150,
									sortable : true,
									hidden : false
								},{
									dataIndex : 'type-name',
									header : biolims.equipment.maintenanceType,
									width : 80,
									sortable : true,
									hidden: true
								}, {
									dataIndex : 'stateName',
									header : biolims.common.workFlowStateName,
									width : 80,
									sortable : true
								}, {
									dataIndex : 'instrumentRepairPlan-id',
									header : biolims.equipment.instrumentRepairPlanId,
									width : 150,
									sortable : true,
									hidden: true
								}, {
									dataIndex : 'note',
									header : biolims.common.note,
									width : 200,
									sortable : true,
									hidden : true
								}, {
									dataIndex : 'startDate',
									header : biolims.equipment.startDate,
									width : 150,
									sortable : true,
									hidden : true
								}, {
									dataIndex : 'endDate',
									header : biolims.equipment.endDate,
									width : 150,
									sortable : true,
									hidden : true
								}, {
									dataIndex : 'factEndDate',
									header : biolims.equipment.factEndDate,
									width : 150,
									sortable : true,
									hidden : true
								} ]),
						stripeRows : true,
						view : new Ext.ux.grid.LockingGridView(),
						viewConfig : {
							forceFit : true,
							enableRowBody : false,
showPreview:false
},
bbar: new Ext.PagingToolbar({
	id : 'bbarId',
	pageSize : parseInt((document.body.clientHeight - 35) > 0 ? (document.body.clientHeight - 35) / 24
			: 1),
			store : store,
			displayInfo : true,
			displayMsg : biolims.common.displayMsg,
			beforePageText : biolims.common.page,
			afterPageText : biolims.common.afterPageText,
			emptyMsg : biolims.common.noData,
	plugins : new Ext.ui.plugins.ComboPageSize(
			{
				addToItem : false,
				prefixText : biolims.common.show,
				postfixText : biolims.common.entris
											})
								})
					});
			gridGrid.render('grid');
			gridGrid.getSelectionModel().on('rowselect',
					function(sm, rowIdx, r) {
						document.getElementById("id").value = r.data.id;
					});
			gridGrid.on('rowdblclick', function() {
				var record = gridGrid.getSelectionModel().getSelections();
				document.getElementById("id").value = record[0].data.id;
				edit();
			});
			store.on('beforeload', function() {
				store.baseParams = {
					data : document.getElementById('extJsonDataString').value
				};
			});
			store
					.load({
						params : {
							start : 0,
							limit : parseInt((document.body.clientHeight - 35) > 0 ? (document.body.clientHeight - 35) / 24
									: 1)
						}
					});
		});
function exportexcel() {
	gridGrid.title = biolims.common.exportList;
	var vExportContent = gridGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}


function add() {
	window.location = window.ctx + "/equipment/repair/toEditInstrumentRepair.action?p_type="+$("#p_type").val();
}

function edit() {
	var id = "";
	id = document.getElementById("id").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/equipment/repair/toEditInstrumentRepair.action?id=' + id+'&p_type='+$("#p_type").val();
}

function view() {
	var id = "";
	id = document.getElementById("id").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/equipment/repair/toViewInstrumentRepair.action?id=' + id+'&p_type='+$("#p_type").val();
}
function newSave() {
	save();
}
function save() {

	if (checkSubmit() == true) {
		Ext.MessageBox.show({
			msg : biolims.common.savingData,
			progressText : biolims.common.saving,
			width : 300,
			wait : true,
			icon : 'ext-mb-download'
		});
		form1.action = window.ctx + "/equipment/repair/save.action?p_type="+$("#p_type").val();
		form1.submit();
	} else {
		return false;
	}
}
function checkSubmit() {
	return true;

}



$(function() {

	$("#opensearch").click(function() {
		var option = {};
		option.width = 580;
		option.height = 180;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(gridGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, false, option);

	});

});