﻿var instrumentRepairTaskItemGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'instrument-id',
		type : 'string'
	});
	fields.push({
		name : 'tempId',
		type : 'string'
	});
	fields.push({
		name : 'instrument-name',
		type : 'string'
	});
	fields.push({
		name : 'instrument-effectiveDate',
		type : 'string'
	});
	fields.push({
		name : 'instrument-expiryDate',
		type : 'string'
	});
	fields.push({
		name : 'instrument-nextDate',
		type : 'string'
	});
	fields.push({
		name : 'instrument-spec',
		type : "string"
	});
	fields.push({
		name : 'instrument-serialNumber',
		type : 'string'
	});
	fields.push({
		name : 'instrument-state-name',
		type : "string"
	});
	fields.push({
		name : 'instrument-producer-name',
		type : "string"
	});
	fields.push({
		name : 'reason',
		type : 'string'
	});
	fields.push({
		name : 'note',
		type : 'string'
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : biolims.common.id,
		width : 120,
		sortable : true,
		hidden : true
	});
	cm.push({
		dataIndex : 'instrument-id',
		header : "EQL",
		width : 120,
		sortable : true
	});
	cm.push({
		dataIndex : 'tempId',
		header : biolims.common.tempId,
		width : 120,
		sortable : true,
		hidden : true
	});
	cm.push({
		dataIndex : 'instrument-name',
		header : biolims.common.instrumentName,
		width : 150,
		sortable : true,
	});
	cm.push({
		dataIndex : 'instrument-effectiveDate',
		header : biolims.common.enableDate,
		width : 150,
		sortable : true,
		hidden : true
	});
	cm.push({
		dataIndex : 'instrument-expiryDate',
		header : biolims.common.discardedDate,
		width : 150,
		sortable : true,
		hidden : true
	});
	cm.push({
		dataIndex : 'instrument-nextDate',
		header : biolims.common.nextDetectionDate,
		width : 150,
		sortable : true,
		hidden : true
	});
	cm.push({
		dataIndex : 'instrument-spec',
		header : biolims.equipment.spec,
		width : 150,
		sortable : true
	});
	cm.push({
		dataIndex : 'instrument-serialNumber',
		header : biolims.equipment.serialNumber,
		width : 150,
		sortable : true
	});
	cm.push({
		dataIndex : 'instrument-producer-name',
		header : biolims.equipment.producerName,
		width : 150,
		sortable : true
	});
	cm.push({
		dataIndex : 'instrument-state-name',
		header : biolims.common.stateName,
		width : 150,
		sortable : true,
		hidden : true
	});
	cm.push({
		dataIndex : 'reason',
		header : biolims.common.causeOfFailure,
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'note',
		header : biolims.common.note,
		width : 150,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx
			+ '/equipment/repair/showInstrumentRepairTaskListJson.action?instrumentRepairId='
			+ $("#instrumentRepairId").val();
	var opts = {};
	opts.tbar = [];
	opts.title = biolims.common.listOfInstruments;
	opts.height = document.body.clientHeight * 0.80;
	opts.rowselect = function(id) {
		$("#selectId").val(id);
	};
	opts.rowdblclick = function(id) {
		$('#selectId').val(id);
		edit();
	};
	if($("#instrumentRepair_stateName").val()!="完成"){
		
//		 opts.delSelect = function(ids) {
//				ajax("post", "/equipment/repair/delInstrumentRepairTask.action", {
//					ids : ids
//				}, function(data) {
//					if (data.success) {
//						deSequencingItemGrid.getStore().commitChanges();
//						deSequencingItemGrid.getStore().reload();
//						message(biolims.common.deleteSuccess);
//					} else {
//						message(biolims.common.deleteFailed);
//					}
//				}, null);
//			};
		
		 opts.delSelect = function(ids) {
				ajax("post", "/equipment/repair/delInstrumentRepairTask.action", {
					ids : ids
				}, function(data) {
					if (data.success) {
						instrumentRepairTaskItemGrid.getStore().commitChanges();
						instrumentRepairTaskItemGrid.getStore().reload();
						instrumentRepairLeftPageGrid.getStore().commitChanges();
						instrumentRepairLeftPageGrid.getStore().reload();
						message(biolims.common.deleteSuccess);
						
					} else {
						message(biolims.common.deleteFailed);
					}
				}, null);
			};
		
		
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
//	opts.tbar.push({
//		text : '删除选中',
//		handler : null
//	});
//	opts.tbar.push({
//		text : "故障原因",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_reason_div"), "故障原因", null, {
//				"Confirm" : function() {
//					var records = instrumentRepairTaskItemGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var reason = $("#instrumentRepairItem_reason").val();
//						instrumentRepairTaskItemGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("reason", reason);
//						});
//						instrumentRepairTaskItemGrid.startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
//	opts.tbar.push({
//		text : "维修方式",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_note_div"), "维修方式及备注", null, {
//				"Confirm" : function() {
//					var records = instrumentRepairTaskItemGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var note = $("#instrumentRepairItem_note").val();
//						instrumentRepairTaskItemGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("note", note);
//						});
//						instrumentRepairTaskItemGrid.startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	}
	instrumentRepairTaskItemGrid = gridEditTable("instrumentRepairItemdiv", cols, loadParam, opts);
	$("#instrumentRepairItemdiv").data("instrumentRepairTaskItemGrid", instrumentRepairTaskItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});	
