﻿//待维修设备
var instrumentRepairTempTab;
var instrumentRepair_id = $("#instrumentRepair_id").text();
$(function() {
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : biolims.tInstrumentRepairPlan.id,
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		},
	"visible":	false,
	});
	colOpts.push({
		"data" : "instrument-id",
		"title" : '编号',
		"createdCell" : function(td) {
			$(td).attr("saveName", "instrument-id");
		}
	});
	colOpts.push({
		"data" : "instrument-name",
		"title" : biolims.tInstrumentState.instrumentName,
		"createdCell" : function(td) {
			$(td).attr("saveName", "instrument-name");
		}
	});
	colOpts.push({
		"data": "instrument-spec",
		"title": biolims.equipment.spec,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "instrument-spec");
		},
	})
	
	colOpts.push({
		"data" : "instrument-serialNumber",
		"title" : biolims.equipment.serialNumber,
		"createdCell" : function(td) {
			$(td).attr("saveName", "instrument-serialNumber");
		}
	})
//	colOpts.push({
//		"data" : "instrument-supplier-name",
//		"title" : biolims.tStorage.supplierId,
//		"createdCell" : function(td) {
//			$(td).attr("saveName", "instrument-supplier-name");
//		}
//	})
	colOpts.push({
		"data" : "instrument-state-name",
		"title" : biolims.common.instrumentState,
		"createdCell" : function(td) {
			$(td).attr("saveName", "instrument-state-name");
		}
	})
	colOpts.push({
		"data" : "note",
		"title" : biolims.tInstrumentRepair.note,
		"createdCell" : function(td) {
			$(td).attr("saveName", "note");
		}
	})
	var tbarOpts = [];
	tbarOpts.push({
		text: '<i class="fa fa-ioxhost"></i>'+biolims.common.search,
		action:function(){
			search();
		}
	});
	tbarOpts.push({
		text : biolims.common.addToDetail,
		action : function() {
			var sampleId = [];
			$("#instrumentRepairTempdiv .selected").each(function(i, val) {
				sampleId.push($(val).children("td").eq(0).find("input").val());
			});

			// 先保存主表信息
			var name = $("#instrumentRepair_name").val();
			var factEndDate = $("#instrumentRepair_factEndDate").val();
			var verify = $("#instrumentRepair_verify").val();
			var createUser = $("#instrumentRepair_creatUser_id").text();
			var createDate = $("#instrumentRepair_createDate").text();
			var id = $("#instrumentRepair_id").text();
			if(0>=sampleId.length){
				top.layer.msg(biolims.common.pleaseSelect);
				return;
			}
			$.ajax({
				type : 'post',
				url : '/equipment/repair/addInstrumentRepairTask.action',
				data : {
					ids : sampleId,
					name : name,
					factEndDate : factEndDate,
					verify : verify,
					id : id,
					createDate : createDate,
					createUser : createUser
				},
				success : function(data) {
					var data = JSON.parse(data)
					if (data.success) {
						$("#instrumentRepair_id").text(data.data);
						$("#instrumentRepairId").val(data.data);
						var param = {
								id: data.data
							};	
						var instrumentRepairTaskTabs = $("#instrumentRepairTaskdiv")
							.DataTable();
						instrumentRepairTaskTabs.settings()[0].ajax.data = param;
						instrumentRepairTaskTabs.ajax.reload();
						var instrumentRepairTaskTabss = $("#instrumentRepairTempdiv").DataTable();
						instrumentRepairTaskTabss.ajax.reload();
					} else {
						top.layer.msg(biolims.common.addToDetailFailed);
					}
					;
				}
			})
		}
	});

	var instrumentRepairTempOps = table(true, instrumentRepair_id,
			"/equipment/repair/showInstrumentRepairTempTableJsonNew.action?p_type=", colOpts, tbarOpts);
	instrumentRepairTempTab = renderData($("#instrumentRepairTempdiv"), instrumentRepairTempOps);

	// 选择数据并提示
	instrumentRepairTempTab.on('draw', function() {
		var index = 0;
		$("#instrumentRepairTempdiv .icheck").on('ifChanged', function(event) {
			if ($(this).is(':checked')) {
				index++;
			} else {
				index--;
			}
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
	});
	bpmTask($("#bpmTaskId").val());
});



//大保存
function saveinstrumentRepairAndItem() {
		var changeLog = "";
		$('input[class="form-control"]').each(function(i, v) {
			var valnew = $(v).val();
			var val = $(v).attr("changelog");
			if(val !== valnew) {
				changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
			}
		});
		console.log($("#form1"));
		var jsonStr = JSON.stringify($("#form1").serializeObject());

		var dataItemJson = saveItemjson($("#instrumentRepairTaskdiv"));
		var ele = $("#instrumentRepairTempdiv");
		var changeLogItem = "主数据入库明细：";
		changeLogItem = getChangeLog(dataItemJson, ele, changeLogItem);
		var id = $("#instrumentRepair_id").text();
		var createUser = $("#storageIn_handelUser").attr("userId");;
		var createDate = $("#storageIn_createDate").text();
		$.ajax({
			url: ctx + '/storage/storageIn/saveStorageInAndItem.action',
			dataType: 'json',
			type: 'post',
			data: {
				dataValue: jsonStr,
				changeLog: changeLog,
				ImteJson: dataItemJson,
				bpmTaskId: $("#bpmTaskId").val(),
				changeLogItem: changeLogItem,
				id:id,
				createUser:createUser,
				createDate:createDate
			},
			success: function(data) {
				if(data.success) {
					var url = "/equipment/repair/editInstrumentRepairNew.action?id=" + data.id;

					url = url + "&bpmTaskId=" + $("#bpmTaskId").val() + "&p_type=" + $("#p_type").val();

					window.location.href = url;
				} else {
					top.layer.msg(data.msg);

				}
			}

		});

}

$.fn.serializeObject = function()
{
   var o = {};
   var a = this.serializeArray();
   $.each(a, function() {
       if (o[this.name]) {
           if (!o[this.name].push) {
               o[this.name] = [o[this.name]];
           }
           o[this.name].push(this.value || '');
       } else {
           o[this.name] = this.value || '';
       }
   });
   return o;
};
function tjsp() {
	top.layer
			.confirm(
					biolims.common.pleaseConfirmSaveBeforeSubmit,
					{
						icon : 3,
						title : biolims.common.prompt
					},
					function(index) {
						top.layer
								.open({
									title : biolims.common.approvalTask,
									type : 2,
									anim : 2,
									area : [ '800px', '500px' ],
									btn : biolims.common.selected,
									content : window.ctx
											+ "/workflow/processinstance/toStartView.action?formName=DnaTask",
									yes : function(index, layer) {
										var datas = {
											userId : userId,
											userName : userName,
											formId : instrumentRepair_id,
											title : $("#instrumentRepair_name").val(),
											formName : "instrumentRepair"
										}
										ajax(
												"post",
												"/workflow/processinstance/start.action",
												datas,
												function(data) {
													if (data.success) {
														top.layer
																.msg(biolims.common.submitSuccess);
														if (typeof callback == 'function') {
															callback(data);
														}
														// dialogWin.dialog("close");
													} else {
														top.layer
																.msg(biolims.common.submitFail);
													}
												}, null);
										top.layer.close(index);
									},
									cancel : function(index, layer) {
										top.layer.close(index)
									}

								});
						top.layer.close(index);
					});

}
function sp() {
	var taskId = $("#bpmTaskId").val();
	var formId = instrumentRepair_id;

	top.layer.open({
		title : biolims.common.detailedExaminationAndApproval,
		type : 2,
		anim : 2,
		area : [ '800px', '500px' ],
		btn : biolims.common.selected,
		content : window.ctx
				+ "/workflow/processinstance/toCompleteTaskView.action?taskId="
				+ taskId + "&formId=" + formId,
		yes : function(index, layer) {
			var operVal = $('.layui-layer-iframe', parent.document).find("iframe").contents()
					.find("#oper").val();
			var opinionVal = $('.layui-layer-iframe', parent.document).find("iframe").contents()
					.find("#opinionVal").val();

			if (!operVal) {
				top.layer.msg(biolims.common.pleaseSelectOper);
				return false;
			}
			if (operVal == "2") {
				_trunTodoTask(taskId, callback, dialogWin);
			} else {
				var paramData = {};
				paramData.oper = operVal;
				paramData.info = opinionVal;

				var reqData = {
					data : JSON.stringify(paramData),
					formId : formId,
					taskId : taskId,
					userId : window.userId
				}
				ajax("post", "/workflow/processinstance/completeTask.action",
						reqData, function(data) {
							if (data.success) {
								top.layer.msg(biolims.common.submitSuccess);
								if (typeof callback == 'function') {
								}
							} else {
								top.layer.msg(biolims.common.submitFail);
							}
						}, null);
			}
			location.href = window.ctx + "/lims/pages/dashboard/dashboard.jsp";
		},
		cancel : function(index, layer) {
			top.layer.close(index)
		}

	});
}
function searchOptions() { 
	return [{
		"txt" : biolims.tInstrumentState.instrumentName,
		"type" : "input",
		"searchName" : "instrument-name",
	},{
		"txt" : biolims.common.instrumentNo,
		"type" : "input",
		"searchName" : "instrument-id",
	},{
		"txt" : biolims.equipment.spec,
		"type" : "input",
		"searchName" : "instrument-spec",
	},{
		"txt" : biolims.tStorage.supplierId,
		"type" : "input",
		"searchName" : "instrument-producer-name",
	},{
		"txt" : biolims.tInstrument.serialNumber,
		"type" : "input",
		"searchName" : "instrument-serialNumber",
	}, {
		"type" : "table",
		"table" : instrumentRepairTempTab
	} ];
}