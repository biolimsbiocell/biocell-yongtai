$(document).ready(function() {


var fields = [];
	   
	fields.push({
		title : biolims.tInstrumentRepair.id,
		field : "id"
	});
	
	   
	fields.push({
		title : biolims.tInstrumentRepair.note,
		field : "note"
	});
	
	   
	fields.push({
		title : biolims.tInstrumentRepair.startDate,
		field : "startDate"
	});
	
	   
	fields.push({
		title : biolims.tInstrumentRepair.endDate,
		field : "endDate"
	});
	
	   
	fields.push({
		title : biolims.tInstrumentRepair.factEndDate,
		field : "factEndDate"
	});
	
	   
	fields.push({
		title : biolims.tInstrumentRepair.name,
		field : "name"
	});
	
	  fields.push({
		title : biolims.tInstrumentRepair.creatDate,
		field : "creatDate.name"
	});
	  fields.push({
		title : biolims.tInstrumentRepair.creatUser,
		field : "creatUser.name"
	});
	   
	fields.push({
		title : biolims.tInstrumentRepair.verify,
		field : "verify"
	});
	
	   
	fields.push({
		title : biolims.tInstrumentRepair.factStartDate,
		field : "factStartDate"
	});
	
	  fields.push({
		title : biolims.tInstrumentRepair.type,
		field : "type.name"
	});
	   
	fields.push({
		title : biolims.tInstrumentRepair.stateName,
		field : "stateName"
	});
	
	  fields.push({
		title : biolims.tInstrumentRepair.tInstrumentRepairPlan,
		field : "tInstrumentRepairPlan.name"
	});
	//生成树
	$('#mytreeGrid').treegridData({
		id: 'id',
		parentColumn: 'parent',
		type: "GET", //请求数据的ajax类型
		url: '/equipment/instrumentRepair/showInstrumentRepairListJson.action', //请求数据的ajax的url
		ajaxParams: {}, //请求数据的ajax的data属性
		expandColumn: null, //在哪一列上面显示展开按钮
		striped: false, //是否各行渐变色
		bordered: true, //是否显示边框
		expandAll: false, //是否全部展开
		columns: fields
	});

});
// 新建
function add() {
	window.location = window.ctx +
		"/equipment/instrumentRepair/editInstrumentRepair.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/equipment/instrumentRepair/editInstrumentRepair.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/equipment/instrumentRepair/viewInstrumentRepair.action?id=" + id;
}

// 弹框模糊查询参数
function searchOptions() {

var fields = [];
	   
	fields.push({
		"txt" : biolims.tInstrumentRepair.id,
		"searchName" : 'id',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.tInstrumentRepair.note,
		"searchName" : 'note',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.tInstrumentRepair.startDate,
		"searchName" : 'startDate',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.tInstrumentRepair.endDate,
		"searchName" : 'endDate',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.tInstrumentRepair.factEndDate,
		"searchName" : 'factEndDate',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.tInstrumentRepair.name,
		"searchName" : 'name',
		"type" : "input"
	});
	
	   fields.push({
		header : biolims.tInstrumentRepair.creatDate,
		"searchName" : 'creatDate.name',
		"type" : "input"
	});
	   fields.push({
		header : '下达人',
		"searchName" : 'creatUser.name',
		"type" : "input"
	});
	   
	fields.push({
		"txt" : biolims.tInstrumentRepair.verify,
		"searchName" : 'verify',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.tInstrumentRepair.factStartDate,
		"searchName" : 'factStartDate',
		"type" : "input"
	});
	
	   fields.push({
		header : biolims.tInstrumentRepair.type,
		"searchName" : 'type.name',
		"type" : "input"
	});
	   
	fields.push({
		"txt" : biolims.tInstrumentRepair.stateName,
		"searchName" : 'stateName',
		"type" : "input"
	});
	
	   fields.push({
		header : biolims.tInstrumentRepair.tInstrumentRepairPlan,
		"searchName" : 'tInstrumentRepairPlan.name',
		"type" : "input"
	});

	fields.push({
			"type" : "table",
			"table" : $('#mytreeGrid'),
			"reloadUrl":ctx+"/com/biolims/system/product/showProductListJson.action"
		});





	return fields;
}

