var instrumentRepairTaskTab, oldChangeLog;
//设备维修明细表
$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"visible":false,
		"title":  biolims.tInstrumentRepair.id,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		},
	})
	colOpts.push({
		"data": "tempId",
		"visible":false,
		"title":  biolims.common.tempId,
		"createdCell": function(td) {
			$(td).attr("saveName", "tempId");
		},
	})
	colOpts.push({
		"data": "instrument-id",
		"title": '编号',
		"createdCell": function(td, data) {
			$(td).attr("saveName", "instrument-id");
		},
	})
/*	colOpts.push({
		"data": "storage-kit-name",
		"title": biolims.common.kitName,
		"createdCell": function(td) {
			$(td).attr("saveName", "storage-kit-name");
		}
	})*/
		colOpts.push({
		"data": "instrument-name",
		"title": biolims.tInstrumentState.instrumentName,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "instrument-name");
		},
	})
		colOpts.push({
		"data": "instrument-effectiveDate",
		"title": biolims.tInstrumentRepairPlan.effectiveDate,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "instrument-effectiveDate");
		},
	})
	//报废时间
		colOpts.push({
		"data": "instrument-expiryDate",
		"title": biolims.tInstrumentRepairPlan.expiryDate,
		"visible" :false,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "instrument-expiryDate");
		},
	})
	//下次检测日期
		colOpts.push({
		"data": "instrument-nextDate",
		"title": biolims.tInstrumentRepairPlan.nextDate,
		"visible" :false,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "instrument-nextDate");
		},
	})
		colOpts.push({
		"data": "instrument-spec",
		"title": biolims.equipment.spec,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "instrument-spec");
		},
	})
		colOpts.push({
		"data": "instrument-serialNumber",
		"title":  biolims.equipment.serialNumber,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "instrument-serialNumber");
		},
	})
	
	colOpts.push({
		"data": "instrument-producer-name",
		"title": biolims.tStorage.supplierId,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "instrument-producer-name");
		},
	})

	
	colOpts.push({
		"data": "instrument-state-name",
		"title":biolims.tInstrument.state,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "instrument-state-name");
		},
	})
	colOpts.push({
		"data": "reason",
		"title":biolims.common.causeOfFailure,
		"className": "edit",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "reason");
		},
	})
	colOpts.push({
		"data": "note",
		"title": biolims.tInstrumentFaultDetail.note,
		"className": "edit",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "note");
		},
	})
	
	var tbarOpts = [];
	if($("#instrumentRepairState").val()!="1"){
		tbarOpts.push({
			text: biolims.common.delSelected,
			action: function() {
				//刷新左侧表
				var instrumentRepairTaskTabs = $("#instrumentRepairTempdiv").DataTable();
				removeChecked($("#instrumentRepairTaskdiv"),
						"/equipment/repair/delInstrumentRepairTaskNew.action","删除入库明细数据：",$("#instrumentRepair_id").text());
			}
		});
		tbarOpts.push({
			text: biolims.common.Editplay,
			action: function() {
				editItemLayer($("#instrumentRepairTaskdiv"))
			}
		})
	}
/*	tbarOpts.push({
		text : "产物数量",
		action : function() {
			var rows = $("#storageInItemdiv .selected");
			var length = rows.length;
			if (!length) {
				top.layer.msg("请选择数据");
				return false;
			}
			top.layer.open({
				type : 1,
				title : "产物数量",
				content : $('#batch_data'),
				area:[document.body.clientWidth-600,document.body.clientHeight-200],
				btn: biolims.common.selected,
				yes : function(index, layer) {
					var productNum = $("#productNum").val();
					rows.addClass("editagain");
					rows.find("td[savename='productNum']").text(productNum);
					top.layer.close(index);
				}
			});
		}
	})*/
	var instrumentRepairTaskOps = table(true, $("#instrumentRepair_id").text(), "/equipment/repair/showInstrumentRepairTaskTableJsonNew.action", colOpts, tbarOpts);
	instrumentRepairTaskTab = renderData($("#instrumentRepairTaskdiv"), instrumentRepairTaskOps);
	//选择数据并提示
	instrumentRepairTaskTab.on('draw', function() {
		var index = 0;
		$("#instrumentRepairTaskdiv .icheck").on('ifChanged', function(event) {
			if($(this).is(':checked')) {
				index++;
				$("#plateModal").find(".mysample[sid='" + this.value + "']").css("border","2px solid #000");
			} else {
				var tt=$("#plateModal").find(".mysample[sid='" + this.value + "']").css("border","1px solid gainsboro");
				index--;
			}
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
		oldChangeLog = instrumentRepairTaskTab.ajax.json();
		
	});
});

// 保存
function saveinstrumentRepairTaskTab() {
    var ele=$("#instrumentRepairTaskdiv");
	var changeLog = "设备维修明细:";
	var data = saveItemjson(ele);
	changeLog = getChangeLog(data, ele, changeLog);
	var changeLogs="";
	if(changeLog !="设备维修明细:"){
		changeLogs=changeLog;
	}
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		type: 'post',
		url: '/equipment/repair/saveInstrumentRepairTaskTableNew.action',
		data: {
			id: $("#instrumentRepair_id").text(),
			dataJson: data,
			logInfo: changeLogs
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 添加样本类型ID
//			if(k == "dicSampleTypeName") {
//				json["dicSampleTypeId"] = $(tds[j]).attr("dicSampleTypeId");
//				continue;
//			}
			json[k] = $(tds[j]).text();
		}
		json.color = $(val).attr("background");
		data.push(json);
	});
	return JSON.stringify(data);
}

/*
 * 
 * 创建者 : 郭恒开
 * 创建日期: 2018/03/08
 * 文件描述: 修改日志
 * 
 */
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		if (!id) {
			changeLog += '新增记录:';
			for ( var k in v) {
				var title = ele.find("th[savename=" + k + "]").text();
				changeLog += '"' + title + '"为"' + v[k] + '";';
			}
			return true;
		}
		changeLog += 'ID为"' + v.id + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k] && k != "color") {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
