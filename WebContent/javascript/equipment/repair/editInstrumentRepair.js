
$("#toolbarbutton_add").click(function() {
	add();
});

$("#toolbarbutton_list").click(function() {
	list();
});
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_status").click(function() {
	changeState()
});
function list() {
	window.location = window.ctx
			+ '/equipment/repair/showInstrumentRepairList.action?queryMethod=1'
			+ "&p_type="+$("#p_type").val();
}

function add() {
	window.location = window.ctx
			+ "/equipment/repair/toEditInstrumentRepair.action?p_type="+$("#p_type").val();
}

function edit() {
	var id = "";
	id = document.getElementById("id").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/equipment/repair/toEditInstrumentRepair.action?id=' + id+ "&p_type="+$("#p_type").val();
}

function newSave() {
	save();
}
function save() {
	if (checkSubmit() == true) {

		Ext.MessageBox.show({
			msg : biolims.common.savingData,
			progressText : biolims.common.saving,
			width : 300,
			wait : true,
			icon : 'ext-mb-download'
		});

		document.getElementById('jsonDataStr').value = commonGetModifyRecords(instrumentRepairTaskItemGrid);
		// document.getElementById('jsonDataStr1').value =
		// commonGetModifyRecords(storageGrid);
		// document.getElementById('jsonDataStr2').value =
		// commonGetModifyRecords(personGrid);

		form1.action = window.ctx + "/equipment/repair/save.action?p_type="+$("#p_type").val();
		form1.submit();
	} else {
		return false;
	}
}
function checkSubmit() {
	var mess = "";
	var fs = [ "instrumentRepair_id"];
	var nsc = [ biolims.common.IdEmpty];

	mess = commonFieldsNotNullVerifyById(fs, nsc);
	if (mess != "") {
		message(mess);
		return false;
	}
	return true;

}
function changeState() {
	commonChangeState("formId=" + $("#instrumentRepair_id").val()
			+ "&tableId=InstrumentRepair");
}
Ext.onReady(function() {

});
var id = new Ext.form.TextField({
	allowBlank : true
});
var type = new Ext.form.TextField({
	allowBlank : true
});
type.on('focus', function() {
	wxrwlx();
});

var storage = new Ext.form.TextField({
	allowBlank : true
});
storage.on('focus', function() {
	duixiangbianma1();
});
var fee = new Ext.form.NumberField({
	allowBlank : true,
	allowNegative : false
});

var note = new Ext.form.TextArea({
	allowBlank : true,
	maxLength : 2000,
	maxLengthText : biolims.master.isNotGreater2000
});
var instrumentRepairTask = new Ext.form.TextField({
	allowBlank : true
});
function validateFun(rec) {
	var mess = "";

	var fs = [ rec.get('id') ];
	var nsc = [ biolims.common.IdEmpty ];
	mess = commonFieldsNotNullVerify(fs, nsc);

	if (mess != "") {
		message(mess);
		return false;
	}
	return true;
}

function viewInstruent() {
	if (trim(document.getElementById('instrumentRepair_storage_id').value) == '') {
		message(biolims.common.selectRecord);
	} else {
		openDialog(window.ctx + '/equipment/main/toViewInstrument.action?id='
				+ document.getElementById('instrumentRepair_storage_id').value);

	}
}
function viewInstruentFault() {
	if (trim(document.getElementById('instrumentRepair_instrumentFault_id').value) == '') {
		message(biolims.common.selectRecord);
	} else {
		openDialog(window.ctx
				+ '/equipment/fault/toViewInstrumentFault.action?id='
				+ document
						.getElementById('instrumentRepair_instrumentFault_id').value);

	}
}
function viewInstruentFaultPlan() {
	if (trim(document
			.getElementById('instrumentRepair_instrumentRepairPlan_id').value) == '') {
		message(biolims.common.selectRecord);
	} else {
		openDialog(window.ctx
				+ '/equipment/plan/toViewInstrumentRepairPlan.action?id='
				+ document
						.getElementById('instrumentRepair_instrumentRepairPlan_id').value);

	}
}

function workflowSubmit() {
	commonWorkflowSubmit("applicationTypeTableId=instrumentRepair&formId="
			+ $("#instrumentRepair_id").val() + "&title="
			+ encodeURI(encodeURI($("#instrumentRepair_note").val())));
}
function workflowLook() {
	commonWorkflowLook("applicationTypeTableId=instrumentRepair&formId="
			+ $("#instrumentRepair_id").val());
}
function workflowView() {
	commonWorkflowView("applicationTypeTableId=instrumentRepair&formId="
			+ $("#instrumentRepair_id").val());
}
function editCopy() {
	window.location = window.ctx
			+ '/equipment/repair/toCopyInstrumentRepair.action?id='
			+ $("#instrumentRepair_id").val();
}
Ext.onReady(function() {
	var tabs = new Ext.TabPanel({
		id : 'tabs11',
		renderTo : 'maintab',
		height : document.body.clientHeight - 30,
		autoWidth : true,
		activeTab : 0,
		margins : '0 0 0 0',
		items : [ {
			title : biolims.common.instrumentMaintenance,
			contentEl : 'markup'
		}
		/*
		 * , { title : '组件列表', contentEl : 'item1' }, { // contentEl:'markup',
		 * title : '人员组成', contentEl : 'item2' }
		 */]
	});
	load("/equipment/repair/showInstrumentRepairTaskList.action", {
		id : $("#instrumentRepair_id").val()
	}, "#instrumentRepairTaskItempage");
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "view") {
		settextreadonlyByAll();}
});
function chmx(instrumentRepairTaskId) {
	Ext.onReady(function() {
		Ext.getCmp('tabs11').setActiveTab(2);
	});
}
function zjlb(instrumentRepairTaskId) {
	Ext.onReady(function() {
		Ext.getCmp('tabs11').setActiveTab(1);

	});
}
function setGridId() {
	var gridCount = gridGrid.store.getCount();
	for ( var ij = 0; ij < gridCount; ij++) {
		var record = gridGrid.store.getAt(ij);
		record.set("id", "");
	}
}
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	if($("#instrumentRepair_state").val()=="3"){
		load("/equipment/repair/showInstrumentRepairFromFaultList.action", { }, "#instrumentRepairTaskTempPage");
		//$("#markup").css("width","75%");
	}
});

var workTime = new Ext.form.NumberField({
	allowBlank : true,
	allowNegative : false
});
var scale = new Ext.form.NumberField({
	allowBlank : true,
	allowNegative : false
});
var workType = new Ext.form.TextField({
	allowBlank : true
});

var user = new Ext.form.TextField({
	allowBlank : true
});
var instrumentRepairTask = new Ext.form.TextField({
	allowBlank : true
});
var num = new Ext.form.NumberField({
	allowBlank : true,
	allowNegative : false
});
var factNum = new Ext.form.NumberField({
	allowBlank : true,
	allowNegative : false
});
var price = new Ext.form.NumberField({
	allowBlank : true,
	allowNegative : false
});
Ext.onReady(function() {
	var item = menu.add({
		text : biolims.equipment.printingEquipmentForm
	});
	item.on('click', ckcrk);

});
function ckcrk() {

	var url = '__report=instrument.rptdesign&id='
			+ $("#instrumentRepair_id").val();
	commonPrint(url);

}
function goInExcelcsvSvn(){
	var file = document.getElementById("file-svnuploadcsv").files[0];  
	var n = 0;
	var ob = gridGrid.getStore().recordType;
	var reader = new FileReader();  
	reader.readAsText(file,'GB2312');  
	reader.onload=function(f){  
		var csv_data = $.simple_csv(this.result);
		$(csv_data).each(function() {
            	if(n>0){
            		if(this[0]){
            			var p = new ob({});
            			p.isNew = true;	
            			p.set("instrument-id",this[0]);//设备编号
            			p.set("instrument-name",this[1]);//设备名称
            			p.set("instrument-effectiveDate",this[2]);//启用日期
            			p.set("instrument-expiryDate",this[3]);//报废日期
            			p.set("instrument-spec",this[4]);//规格型号
            			p.set("instrument-state-name",this[5]);//规格型号
            			p.set("note",this[6]);//备注
            			gridGrid.getStore().insert(0, p);
            		}
            	}
                 n = n +1;
            	
            });
	};
}
