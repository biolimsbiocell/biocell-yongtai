var instrumentRepairPlanTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":biolims.tInstrumentRepairPlan.id,
	});
	
/*	    fields.push({
		"data":"unit-id",
		"title":"单位ID",
		"visible": false,	
	});
	    fields.push({
		"data":"unit-name",
		"title":biolims.tInstrumentRepairPlan.unit,
		"visible": false,
	});*/
	    fields.push({
			"data":"unitName",
			"title":biolims.common.unit,
			"render": function(data, type, full, meta) {
				if(data == "1") {
					return biolims.user.day;
				}if(data == "2") {
					return biolims.user.week;
				}if(data == "3") {
					return biolims.user.month;
				}else {
					return "";
				}
			}	
		});
	    fields.push({
		"data":"dutyUser-name",
		"title":biolims.tInstrumentRepairPlan.dutyUser,
	});
	
	    fields.push({
		"data":"nextDate",
		"title":biolims.tInstrumentRepairPlan.nextDate,
		"render": function(data) {
				if(data) {
					return parent.moment(data).format('YYYY-MM-DD');
				} else {
					return "";
				}
			},
	    "visible": false,	
	});
	
	    fields.push({
		"data":"note",
		"title":biolims.tInstrumentRepairPlan.note,
	});
	
	    fields.push({
		"data":"createUser-id",
		"title":biolims.sample.createUserId,
		"visible": false,	
	});
	    fields.push({
		"data":"createUser-name",
		"title":biolims.tInstrumentRepairPlan.createUser
	});
	
	    fields.push({
		"data":"type-id",
		"title":biolims.common.maintenanceTypeID,
		"visible": false,	
	});
	    fields.push({
		"data":"type-name",
		"title":biolims.tInstrumentRepairPlan.type
	});
	


	
	    fields.push({
		"data":"stateName",
		"title":biolims.tInstrumentRepairPlan.stateName,
	});
	
	    fields.push({
		"data":"effectiveDate",
		"title":biolims.tInstrumentRepairPlan.effectiveDate,
		"render": function(data) {
				if(data) {
					return parent.moment(data).format('YYYY-MM-DD');
				} else {
					return "";
				}
			},
	    "visible": false,	
	});
	
	    fields.push({
		"data":"expiryDate",
		"title":biolims.tInstrumentRepairPlan.expiryDate,
		"render": function(data) {
				if(data) {
					return parent.moment(data).format('YYYY-MM-DD');
				} else {
					return "";
				}
			},
			"visible": false,	
	});
	
	    fields.push({
		"data":"period",
		"title":biolims.tInstrumentRepairPlan.period,
	});
	

		$.ajax({
			type:"post",
			url:window.ctx + "/system/customfields/findFieldByModuleValue.action",
			async:false,
			data:{
				moduleValue : "InstrumentRepairPlan"
			},
			success:function(data){
				var objData = JSON.parse(data);
				if(objData.success){
					$.each(objData.data, function(i,n) {
						var str = {
							"data" : n.fieldName,
							"title" : n.label
						}
						colData.push(str);
					});
					
				}else{
					top.layer.msg(biolims.tInstrumentBorrowDetail.tableError);
				}
			}
		});

	var options = table(true, "","/equipment/plan/showInstrumentRepairPlanTableJson.action",
	 fields, null)
	instrumentRepairPlanTable = renderRememberData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(instrumentRepairPlanTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
		"/equipment/plan/editInstrumentRepairPlan.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/equipment/plan/editInstrumentRepairPlan.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/equipment/plan/viewInstrumentRepairPlan.action?id=" + id;
}
//弹框模糊查询参数
function searchOptions() {
var fields=[];
	  
/*	fields.push({
	    "type":"input",
		"searchName":"unit.id",
		"txt":"单位ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"unit.name",
		"txt":biolims.tInstrumentRepairPlan.unit
	});*/
		fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":"编码"
		});
	   fields.push({
		    "searchName":"dutyUser-name",
			"type":"input",
			"txt":biolims.tInstrumentRepairPlan.dutyUser
		});
	   /*fields.push({
		    "searchName":"nextDate",
			"type":"input",
			"txt":biolims.tInstrumentRepairPlan.nextDate
		});
	fields.push({
			"txt": biolims.tInstrumentRepairPlan.nextDate+"(Start)",
			"type": "dataTime",
			"searchName": "nextDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.tInstrumentRepairPlan.nextDate+"(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "nextDate##@@##2"
		});*/
	 fields.push({
		    "searchName":"note",
			"type":"input",
			"txt":biolims.tInstrumentRepairPlan.note
		});
	   fields.push({
		    "searchName":"unitName",
		    "type": "select",
			"options": "请选择"+"|"+biolims.user.day+"|"+biolims.user.week+"|"+biolims.user.month,
			"changeOpt": "''|1|2|3",
			"txt":biolims.common.unit
		});
/*	fields.push({
	    "type":"input",
		"searchName":"createUser.id",
		"txt":biolims.sample.createUserId
	});*/
	fields.push({
	    "type":"input",
		"searchName":"createUser.name",
		"txt":biolims.tInstrumentRepairPlan.createUser
	});
/*	fields.push({
	    "type":"input",
		"searchName":"type.id",
		"txt":biolims.common.maintenanceTypeID
	});*/
	fields.push({
	    "type":"input",
		"searchName":"type.name",
		"txt":biolims.tInstrumentRepairPlan.type
	});
	   fields.push({
		    "searchName":"stateName",
			"type":"input",
			"txt":biolims.tInstrumentRepairPlan.stateName
		});/*
	   fields.push({
		    "searchName":"effectiveDate",
			"type":"input",
			"txt":biolims.tInstrumentRepairPlan.effectiveDate
		});
	fields.push({
			"txt": biolims.tInstrumentRepairPlan.effectiveDate+"(Start)",
			"type": "dataTime",
			"searchName": "effectiveDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.tInstrumentRepairPlan.effectiveDate+"(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "effectiveDate##@@##2"
		});
	   fields.push({
		    "searchName":"expiryDate",
			"type":"input",
			"txt":biolims.tInstrumentRepairPlan.expiryDate
		});
	fields.push({
			"txt": biolims.tInstrumentRepairPlan.expiryDate+"(Start)",
			"type": "dataTime",
			"searchName": "expiryDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.tInstrumentRepairPlan.expiryDate+"(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "expiryDate##@@##2"
		});*/
	   fields.push({
		    "searchName":"period",
			"type":"input",
			"txt":biolims.tInstrumentRepairPlan.period
		});
	
	fields.push({
		"type":"table",
		"table":instrumentRepairPlanTable
	});
	return fields;
}
