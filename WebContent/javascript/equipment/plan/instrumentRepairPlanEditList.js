var instrumentRepairPlanTable;
var oldinstrumentRepairPlanChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表

	var tbarOpts = [];
	var colOpts = [];
	   colOpts.push({
		"data":"id",
		"visible":false,
		"title": biolims.tInstrumentRepairPlan.id,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
	});
	colOpts.push( {
		"data": "unit-id",
		"title": biolims.common.unitId,
		"createdCell": function(td) {
			$(td).attr("saveName", "unit-id");
		}
	});
	colOpts.push( {
		"data": "unit-name",
		"title": biolims.tInstrumentRepairPlan.unit,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "unit-name");
			$(td).attr("unit-id", rowData['unit-id']);
		}
	});
	   colOpts.push({
		"data":"dutyUser",
		"title": biolims.tInstrumentRepairPlan.dutyUser,
		"createdCell": function(td) {
			$(td).attr("saveName", "dutyUser");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"nextDate",
		"title": biolims.tInstrumentRepairPlan.nextDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "nextDate");
	    },
		"visible": false,	
		"className": "date"
	});
	   colOpts.push({
		"data":"note",
		"title": biolims.tInstrumentRepairPlan.note,
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
	    },
		"className": "textarea"
	});
	colOpts.push( {
		"data": "createUser-id",
		"title": biolims.sample.createUserId,
		"createdCell": function(td) {
			$(td).attr("saveName", "createUser-id");
		}
	});
	colOpts.push( {
		"data": "createUser-name",
		"title": biolims.tInstrumentRepairPlan.createUser,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "createUser-name");
			$(td).attr("createUser-id", rowData['createUser-id']);
		}
	});
	colOpts.push( {
		"data": "type-id",
		"title": biolims.common.maintenanceTypeID,
		"createdCell": function(td) {
			$(td).attr("saveName", "type-id");
		}
	});
	colOpts.push( {
		"data": "type-name",
		"title": biolims.tInstrumentRepairPlan.type,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "type-name");
			$(td).attr("type-id", rowData['type-id']);
		}
	});
	   colOpts.push({
		"data":"stateName",
		"title": biolims.tInstrumentRepairPlan.stateName,
		"createdCell": function(td) {
			$(td).attr("saveName", "stateName");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"effectiveDate",
		"title": biolims.tInstrumentRepairPlan.effectiveDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "effectiveDate");
	    },
		"visible": false,	
		"className": "date"
	});
	   colOpts.push({
		"data":"expiryDate",
		"title": biolims.tInstrumentRepairPlan.expiryDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "expiryDate");
	    },
		"visible": false,	
		"className": "date"
	});
	   colOpts.push({
		"data":"period",
		"title": biolims.tInstrumentRepairPlan.period,
		"createdCell": function(td) {
			$(td).attr("saveName", "period");
	    },
		"visible": false,	
		"className": "edit"
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#instrumentRepairPlanTable"))
	}
	});
	tbarOpts.push({
		text: biolims.common.addwindow,
		action: function() {
			addItemLayer($("#instrumentRepairPlanTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.editwindow,
		action: function() {
			editItemLayer($("#instrumentRepairPlanTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.batchUpload,
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#instrumentRepairPlan_id").val(),
						fileId: data.response.fileId
					},
					url: ctx + "/equipment/plan/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							 instrumentRepairPlanTable.ajax.reload();
						} else {
							top.layer.msg(biolims.common.uploadFailed)
						}
		
					}
				});
			});
			
		}
		});
	
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveInstrumentRepairPlan($("#instrumentRepairPlanTable"));
		}
	});
	}
	
	var instrumentRepairPlanOptions = 
	table(true, "","/equipment/plan/showInstrumentRepairPlanTableJson.action",
	 colOpts, tbarOpts)
	
	
	
	instrumentRepairPlanTable = renderData($("#instrumentRepairPlanTable"), instrumentRepairPlanOptions);
	instrumentRepairPlanTable.on('draw', function() {
		oldinstrumentRepairPlanChangeLog = instrumentRepairPlanTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});

// 保存
function saveInstrumentRepairPlan(ele) {
	var data = saveInstrumentRepairPlanjson(ele);
	var ele=$("#instrumentRepairPlanTable");
	var changeLog = "实验结果：";
	changeLog = getChangeLog(data, ele, changeLog);
	$.ajax({
		type: 'post',
		url: '/equipment/plan/saveInstrumentRepairPlanTable.action',
		data: {
			dataJson: data,
			changeLog:changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveInstrumentRepairPlanjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			
			if(k == "unit-name") {
				json["unit-id"] = $(tds[j]).attr("unit-id");
				continue;
			}
			
			if(k == "createUser-name") {
				json["createUser-id"] = $(tds[j]).attr("createUser-id");
				continue;
			}
			
			if(k == "type-name") {
				json["type-id"] = $(tds[j]).attr("type-id");
				continue;
			}
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		oldinstrumentRepairPlanChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
