$(document).ready(function() {


var fields = [];
	   
	fields.push({
		title : biolims.tInstrumentBorrowDetail.id,
		field : "id"
	});
	
	  fields.push({
		title : biolims.common.unit,
		field : "unit.name"
	});
	   
	fields.push({
		title : biolims.master.personName,
		field : "dutyUser"
	});
	
	   
	fields.push({
		title : biolims.tInstrumentRepairPlan.nextDate,
		field : "nextDate"
	});
	
	   
	fields.push({
		title : biolims.tInstrumentRepairPlan.note,
		field : "note"
	});
	
	  fields.push({
		title : biolims.tInstrumentRepairPlan.createUser,
		field : "createUser.name"
	});
	  fields.push({
		title : biolims.tInstrumentRepairPlan.type,
		field : "type.name"
	});
	   
	fields.push({
		title : biolims.tInstrumentRepairPlan.state,
		field : "state"
	});
	
	   
	fields.push({
		title : biolims.tInstrumentRepairPlan.stateName,
		field : "stateName"
	});
	
	   
	fields.push({
		title : biolims.tInstrumentRepairPlan.effectiveDate,
		field : "effectiveDate"
	});
	
	   
	fields.push({
		title : biolims.tInstrumentRepairPlan.expiryDate,
		field : "expiryDate"
	});
	
	   
	fields.push({
		title : biolims.tInstrumentRepairPlan.period,
		field : "period"
	});
	
	//生成树
	$('#mytreeGrid').treegridData({
		id: 'id',
		parentColumn: 'parent',
		type: "GET", //请求数据的ajax类型
		url: '/equipment/plan/instrumentRepairPlan/showInstrumentRepairPlanListJson.action', //请求数据的ajax的url
		ajaxParams: {}, //请求数据的ajax的data属性
		expandColumn: null, //在哪一列上面显示展开按钮
		striped: false, //是否各行渐变色
		bordered: true, //是否显示边框
		expandAll: false, //是否全部展开
		columns: fields
	});

});
// 新建
function add() {
	window.location = window.ctx +
		"/equipment/plan/instrumentRepairPlan/editInstrumentRepairPlan.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/equipment/plan/instrumentRepairPlan/editInstrumentRepairPlan.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/equipment/plan/instrumentRepairPlan/viewInstrumentRepairPlan.action?id=" + id;
}

// 弹框模糊查询参数
function searchOptions() {

var fields = [];
	   
	fields.push({
		"txt" : biolims.tInstrumentRepairPlan.id,
		"searchName" : 'id',
		"type" : "input"
	});
	
	   fields.push({
		header : biolims.tInstrumentRepairPlan.unit,
		"searchName" : 'unit.name',
		"type" : "input"
	});
	   
	fields.push({
		"txt" : biolims.tInstrumentRepairPlan.dutyUser,
		"searchName" : 'dutyUser',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.tInstrumentRepairPlan.nextDate,
		"searchName" : 'nextDate',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.tInstrumentRepairPlan.note,
		"searchName" : 'note',
		"type" : "input"
	});
	
	   fields.push({
		header : biolims.tInstrumentRepairPlan.createUser,
		"searchName" : 'createUser.name',
		"type" : "input"
	});
	   fields.push({
		header : biolims.tInstrumentRepairPlan.type,
		"searchName" : 'type.name',
		"type" : "input"
	});
	   
	fields.push({
		"txt" : biolims.tInstrumentRepairPlan.state,
		"searchName" : 'state',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.tInstrumentRepairPlan.stateName,
		"searchName" : 'stateName',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.tInstrumentRepairPlan.effectiveDate,
		"searchName" : 'effectiveDate',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.tInstrumentRepairPlan.expiryDate,
		"searchName" : 'expiryDate',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.tInstrumentRepairPlan.period,
		"searchName" : 'period',
		"type" : "input"
	});
	

	fields.push({
			"type" : "table",
			"table" : $('#mytreeGrid'),
			"reloadUrl":ctx+"/com/biolims/system/product/showProductListJson.action"
		});





	return fields;
}

