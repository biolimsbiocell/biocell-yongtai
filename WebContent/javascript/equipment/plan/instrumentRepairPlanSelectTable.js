var instrumentRepairPlanTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":biolims.tInstrumentRepairPlan.id,
	});
	
	    fields.push({
		"data":"unit-id",
		"title":biolims.common.unitId
	});
	    fields.push({
		"data":"unit-name",
		"title":biolims.tInstrumentRepairPlan.unit
	});
	
	    fields.push({
		"data":"dutyUser",
		"title":biolims.tInstrumentRepairPlan.dutyUser,
	});
	
	    fields.push({
		"data":"nextDate",
		"title":biolims.tInstrumentRepairPlan.nextDate,
	});
	
	    fields.push({
		"data":"note",
		"title":biolims.tInstrumentRepairPlan.note,
	});
	
	    fields.push({
		"data":"createUser-id",
		"title":biolims.sample.createUserId
	});
	    fields.push({
		"data":"createUser-name",
		"title":biolims.tInstrumentRepairPlan.createUser
	});
	
	    fields.push({
		"data":"type-id",
		"title":biolims.common.maintenanceTypeID
	});
	    fields.push({
		"data":"type-name",
		"title":biolims.tInstrumentRepairPlan.type
	});
	
	
	    fields.push({
		"data":"stateName",
		"title":biolims.tInstrumentRepairPlan.stateName,
	});
	
	    fields.push({
		"data":"effectiveDate",
		"title":biolims.tInstrumentRepairPlan.effectiveDate,
	});
	
	    fields.push({
		"data":"expiryDate",
		"title":biolims.tInstrumentRepairPlan.expiryDate,
	});
	
	    fields.push({
		"data":"period",
		"title":biolims.tInstrumentRepairPlan.period,
	});
	
	var options = table(true, "","/equipment/plan/showInstrumentRepairPlanTableJson.action",
	 fields, null)
	instrumentRepairPlanTable = renderData($("#addInstrumentRepairPlanTable"), options);
	$('#addInstrumentRepairPlanTable').on('init.dt', function() {
		recoverSearchContent(instrumentRepairPlanTable);
	})
});

//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":biolims.tInstrumentRepairPlan.id
		});
	fields.push({
	    "type":"input",
		"searchName":"unit.id",
		"txt":biolims.common.unitId
	});
	fields.push({
	    "type":"input",
		"searchName":"unit.name",
		"txt":biolims.tInstrumentRepairPlan.unit
	});
	   fields.push({
		    "searchName":"dutyUser",
			"type":"input",
			"txt":biolims.tInstrumentRepairPlan.dutyUser
		});
	   fields.push({
		    "searchName":"nextDate",
			"type":"input",
			"txt":biolims.tInstrumentRepairPlan.nextDate
		});
	fields.push({
			"txt": biolims.tInstrumentRepairPlan.nextDate+"(Start)",
			"type": "dataTime",
			"searchName": "nextDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.tInstrumentRepairPlan.nextDate+"(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "nextDate##@@##2"
		});
	   fields.push({
		    "searchName":"note",
			"type":"input",
			"txt":biolims.tInstrumentRepairPlan.note
		});
	fields.push({
	    "type":"input",
		"searchName":"createUser.id",
		"txt":biolims.sample.createUserId
	});
	fields.push({
	    "type":"input",
		"searchName":"createUser.name",
		"txt":biolims.tInstrumentRepairPlan.createUser
	});
	fields.push({
	    "type":"input",
		"searchName":"type.id",
		"txt":biolims.common.maintenanceTypeID
	});
	fields.push({
	    "type":"input",
		"searchName":"type.name",
		"txt":biolims.tInstrumentRepairPlan.type
	});
	   fields.push({
		    "searchName":"stateName",
			"type":"input",
			"txt":biolims.tInstrumentRepairPlan.stateName
		});
	   fields.push({
		    "searchName":"effectiveDate",
			"type":"input",
			"txt":biolims.tInstrumentRepairPlan.effectiveDate
		});
	fields.push({
			"txt": biolims.tInstrumentRepairPlan.effectiveDate+"(Start)",
			"type": "dataTime",
			"searchName": "effectiveDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.tInstrumentRepairPlan.effectiveDate+"(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "effectiveDate##@@##2"
		});
	   fields.push({
		    "searchName":"expiryDate",
			"type":"input",
			"txt":biolims.tInstrumentRepairPlan.expiryDate
		});
	fields.push({
			"txt": biolims.tInstrumentRepairPlan.expiryDate+"(Start)",
			"type": "dataTime",
			"searchName": "expiryDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.tInstrumentRepairPlan.expiryDate+"(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "expiryDate##@@##2"
		});
	   fields.push({
		    "searchName":"period",
			"type":"input",
			"txt":biolims.tInstrumentRepairPlan.period
		});
	
	fields.push({
		"type":"table",
		"table":instrumentRepairPlanTable
	});
	return fields;
}
