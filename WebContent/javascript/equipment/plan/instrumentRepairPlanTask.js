
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
var instrumentRepairPlanTaskTab, oldChangeLog;
hideLeftDiv();
//设备维修明细表
$(function() {
	var colOpts = [];
	 colOpts.push({
			"data":"id",
			"title": biolims.tInstrumentRepairPlanTask.id,
			"visible":false,
			"createdCell": function(td) {
				$(td).attr("saveName", "id");
		    },
			"className": "edit"
		});
		colOpts.push( {
			"data": "type-id",
			"title": biolims.common.maintenanceTypeID,
			"createdCell": function(td) {
				$(td).attr("saveName", "type-id");
			},
		"visible": false,	
		});
		colOpts.push( {
			"data": "type-name",
			"title": biolims.tInstrumentRepairPlanTask.type,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "type-name");
				$(td).attr("type-id", rowData['typer-id']);
			}
		});
		colOpts.push( {
			"data": "instrument-id",
			"title": "编号",
			"createdCell": function(td) {
				$(td).attr("saveName", "instrument-id");
			},
		});
		colOpts.push( {
			"data": "instrument-name",
			"title": biolims.tInstrumentState.instrumentName,
			"createdCell": function(td) {
				$(td).attr("saveName", "instrument-name");
			},
		
		});
		colOpts.push( {
			"data": "instrumentRepairPlan-id",
			"title": biolims.crmDoctorItem.crmDoctorId,
			"createdCell": function(td) {
				$(td).attr("saveName", "instrumentRepairPlan-id");
			},
		"visible": false,	
		});
		colOpts.push( {
			"data": "instrumentRepairPlan-name",
			"title": biolims.tInstrumentRepairPlanTask.tInstrumentRepairPlan,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "instrumentRepairPlan-name");
				$(td).attr("instrumentRepairPlan-id", rowData['instrumentRepairPlanr-id']);
			},
			"visible": false,
		});

			colOpts.push({
			"data": "instrument-effectiveDate",
			"title": biolims.tInstrumentRepairPlan.effectiveDate,
			"createdCell": function(td, data) {
				$(td).attr("saveName", "instrument-effectiveDate");
			},
		})
			colOpts.push({
			"data": "instrument-expiryDate",
			"title": biolims.tInstrumentRepairPlan.expiryDate,
			"createdCell": function(td, data) {
				$(td).attr("saveName", "instrument-expiryDate");
			},
		})
			colOpts.push({
			"data": "instrument-nextDate",
			"className": "date",
			"title": "验证有效期至",
			"createdCell": function(td, data) {
				$(td).attr("saveName", "instrument-nextDate");
			},
		})
			colOpts.push({
			"data": "instrument-spec",
			"title": biolims.equipment.spec,
			"className": "edit",
			"createdCell": function(td, data) {
				$(td).attr("saveName", "instrument-spec");
			},
			"visible": false,	
		})
			colOpts.push({
			"data": "instrument-tsDate",
			"title":  biolims.common.maintenanceRemindingDate,
			"createdCell": function(td, data) {
				$(td).attr("saveName", "instrument-tsDate");
			},
		})
		colOpts.push({
			"data": "instrument-state-name",
			"title":biolims.tInstrument.state,
			"createdCell": function(td, data) {
				$(td).attr("saveName", "instrument-state-name");
			},
			
		})
		   colOpts.push({
			"data":"tsTime",
			"title": biolims.tInstrumentRepairPlanTask.tsTime,
			"createdCell": function(td) {
				$(td).attr("saveName", "tsTime");
		    },
		});
		   colOpts.push({
			"data":"fee",
			"title": biolims.tInstrumentRepairPlanTask.fee,
			"createdCell": function(td) {
				$(td).attr("saveName", "fee");
		    },
			"visible": false,	
			"className": "edit"
		});
		   colOpts.push({
			"data":"note",
			"title": biolims.tInstrumentRepairPlanTask.note,
			"createdCell": function(td) {
				$(td).attr("saveName", "note");
		    },
			"className": "textarea"
		});
	
	var tbarOpts = [];
	tbarOpts.push({
		text: biolims.common.delSelected,
		action: function() {
			removeChecked($("#instrumentRepairPlanTaskdiv"),
				"/equipment/plan/delInstrumentRepairPlanTask.action","删除入库明细数据：",$("#instrumentRepairPlan_id").text());
		}
	});
	tbarOpts.push({
		text: biolims.common.Editplay,
		action: function() {
			editItemLayer($("#instrumentRepairPlanTaskdiv"))
		}
	})
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveinstrumentRepairPlanTaskTab();
		}
	})
/*	tbarOpts.push({
		text : "产物数量",
		action : function() {
			var rows = $("#storageInItemdiv .selected");
			var length = rows.length;
			if (!length) {
				top.layer.msg("请选择数据");
				return false;
			}
			top.layer.open({
				type : 1,
				title : "产物数量",
				content : $('#batch_data'),
				area:[document.body.clientWidth-600,document.body.clientHeight-200],
				btn: biolims.common.selected,
				yes : function(index, layer) {
					var productNum = $("#productNum").val();
					rows.addClass("editagain");
					rows.find("td[savename='productNum']").text(productNum);
					top.layer.close(index);
				}
			});
		}
	})*/
	var instrumentRepairPlanTaskOps = table(true, $("#instrumentRepairPlan_id").text(), "/equipment/plan/showInstrumentRepairPlanTaskTableJson.action", colOpts, tbarOpts);
	instrumentRepairPlanTaskTab = renderData($("#instrumentRepairPlanTaskdiv"), instrumentRepairPlanTaskOps);
	//选择数据并提示
	instrumentRepairPlanTaskTab.on('draw', function() {
		var index = 0;
		$("#instrumentRepairPlanTaskdiv .icheck").on('ifChanged', function(event) {
			if($(this).is(':checked')) {
				index++;
				$("#plateModal").find(".mysample[sid='" + this.value + "']").css("border","2px solid #000");
			} else {
				var tt=$("#plateModal").find(".mysample[sid='" + this.value + "']").css("border","1px solid gainsboro");
				index--;
			}
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
		oldChangeLog = instrumentRepairPlanTaskTab.ajax.json();
		
	});
});

// 保存
function saveinstrumentRepairPlanTaskTab() {
    var ele=$("#instrumentRepairPlanTaskdiv");
	var changeLog = "设备维护设备周期检定计划明细:";
	var data = saveItemjson(ele);
	changeLog = getChangeLog(data, ele, changeLog);
	var changeLogs="";
	if(changeLog !="设备维护设备周期检定计划明细:"){
		changeLogs=changeLog;
	}
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		type: 'post',
		url: '/equipment/plan/saveInstrumentRepairPlanTaskTable.action',
		data: {
			id: $("#instrumentRepairPlan_id").text(),
			dataJson: data,
			logInfo: changeLogs
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children();
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 添加样本类型ID
//			if(k == "dicSampleTypeName") {
//				json["dicSampleTypeId"] = $(tds[j]).attr("dicSampleTypeId");
//				continue;
//			}
			json[k] = $(tds[j]).text();
		}
		json.color = $(val).attr("background");
		data.push(json);
	});
	return JSON.stringify(data);
}

/*
 * 
 * 创建者 : 郭恒开
 * 创建日期: 2018/03/08
 * 文件描述: 修改日志
 * 
 */
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		if (!id) {
			changeLog += '新增记录:';
			for ( var k in v) {
				var title = ele.find("th[savename=" + k + "]").text();
				changeLog += '"' + title + '"为"' + v[k] + '";';
			}
			return true;
		}
		changeLog += 'ID为"' + v.id + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k] && k != "color") {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
//如果状态名称不是NEW,隐藏左侧的DIV
function hideLeftDiv(){
	var stateName=$("#headStateName").attr("state");
	if(stateName!="1q"){
		$("#leftDiv").hide();
		$("#rightDiv").attr("class","col-md-12 col-xs-12");
	}
}