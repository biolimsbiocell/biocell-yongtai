var gridGrid;

Ext.onReady(function() {
			Ext.QuickTips.init();
			Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
			Ext.grid.RowNumberer = Ext.extend(Ext.grid.RowNumberer, {
				width : 30,
				renderer : function(value, cellmeta, record, rowIndex,
						columnIndex, store) {
					return store.lastOptions.params.start + rowIndex + 1;
				}
			});
			var store = new Ext.data.JsonStore(
					{
						root : 'results',
						totalProperty : 'total',
						remoteSort : true,
						fields : [ {
							name : 'id',
							type : 'string'
						}, {
							name : 'note',
							type : 'string'
						},{
							name : 'createUser-name',
							type : 'string'
						}, {
							name : 'state-name',
							type : 'string'
						}, {
							name : 'effectiveDate',
							type : 'string'
						}, {
							name : 'expiryDate',
							type : 'string'
						}, {
							name : 'period',
							type : 'string'
						}, {
							name : 'dutyUser-name',
							type : 'string'
						}, {
							name : 'unit-name',
							type : 'string'
						}, {
							name : 'nextDate',
							type : 'string'
						} , {
							name : 'type-id',
							type : 'string'
						}, {
							name : 'type-name',
							type : 'string'
						}],
						proxy : new Ext.data.HttpProxy(
								{
									url : ctx
											+ '/equipment/plan/showInstrumentRepairPlanListJson.action?queryMethod=',
									method : 'POST'
								})
					});
			gridGrid = new Ext.grid.GridPanel(
					{
						autoWidth : true,
						id : gridGrid,
						titleCollapse : true,
						autoScroll : true,
						height : document.body.clientHeight - 35,
						iconCls : 'icon-grid',
						title : biolims.common.verificationSchedule,
						collapsible : true,
						store : store,
						columnLines : true,
						trackMouseOver : true,
						loadMask : true,
						colModel : new Ext.ux.grid.LockingColumnModel([
								new Ext.grid.RowNumberer(), {
									dataIndex : 'id',
									header : biolims.common.id,
									width : 100,
									sortable : true
								}, {
									dataIndex : 'note',
									header : biolims.common.name,
									width : 240,
									sortable : true
								},{
									dataIndex : 'createUser-name',
									header : biolims.common.createUserName,
									width : 80,
									sortable : true
								},{
									dataIndex : 'type-id',
									header : biolims.common.maintenanceTypeID,
									width : 80,
									sortable : true,
									hidden : true
								},{
									dataIndex : 'type-name',
									header : biolims.common.maintenanceType,
									width : 80,
									sortable : true
								}, {
									dataIndex : 'state-name',
									header : biolims.common.state,
									width : 80,
									sortable : true,
								}, {
									dataIndex : 'effectiveDate',
									header : biolims.equipment.effectiveDate,
									width : 150,
									sortable : true,
									hidden : true
								}, {
									dataIndex : 'expiryDate',
									header : biolims.equipment.expiryDate,
									width : 150,
									sortable : true,
									hidden : true
								}, {
									dataIndex : 'period',
									header : biolims.equipment.period,
									width : 150,
									sortable : true,
									hidden : true
								}, {
									dataIndex : 'unit-name',
									header : biolims.common.unit,
									width : 150,
									sortable : true,
									hidden : true
								},{
									dataIndex : 'dutyUser-name',
									header : biolims.master.personName,
									width : 150,
									sortable : true,
								}, {
									dataIndex : 'nextDate',
									header : biolims.equipment.nextDate,
									width : 150,
									sortable : true,
									hidden : true
								} ]),
						stripeRows : true,
						view : new Ext.ux.grid.LockingGridView(),
						viewConfig : {
							forceFit : true,
							enableRowBody : false,
							showPreview : false
						},
						bbar : new Ext.PagingToolbar(
								{
									id : 'bbarId',
									pageSize : parseInt((document.body.clientHeight - 35) > 0 ? (document.body.clientHeight - 35) / 24
											: 1),
									store : store,
									displayInfo : true,
									displayMsg : biolims.common.displayMsg,
									beforePageText : biolims.common.page,
									afterPageText : biolims.common.afterPageText,
									emptyMsg : biolims.common.noData,
									plugins : new Ext.ui.plugins.ComboPageSize(
											{
												addToItem : false,
												prefixText : biolims.common.show,
												postfixText : biolims.common.entris
											})
								})
					});
			gridGrid.render('grid');
			gridGrid.getSelectionModel().on('rowselect',
					function(sm, rowIdx, r) {
						document.getElementById("id").value = r.data.id;
					});
			gridGrid.on('rowdblclick', function() {
				var record = gridGrid.getSelectionModel().getSelections();
				document.getElementById("id").value = record[0].data.id;
				edit();
			});
			store.on('beforeload', function() {
				store.baseParams = {
					data : document.getElementById('extJsonDataString').value
				};
			});
			store
					.load({
						params : {
							start : 0,
							limit : parseInt((document.body.clientHeight - 35) > 0 ? (document.body.clientHeight - 35) / 24
									: 1)
						}
					});
		});
function exportexcel() {
	gridGrid.title = biolims.common.exportList;
	var vExportContent = gridGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}

function add() {
	window.location = window.ctx
			+ "/equipment/plan/toEditInstrumentRepairPlan.action";
}

function edit() {
	var id = "";
	id = document.getElementById("id").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/equipment/plan/toEditInstrumentRepairPlan.action?id=' + id;
}

function view() {
	var id = "";
	id = document.getElementById("id").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/equipment/plan/toViewInstrumentRepairPlan.action?id=' + id;
}
function newSave() {
	save();
}
function save() {

	if (checkSubmit() == true) {
		Ext.MessageBox.show({
			msg : biolims.common.savingData,
			progressText : biolims.common.saving,
			width : 300,
			wait : true,
			icon : 'ext-mb-download'
		});
		form1.action = window.ctx + "/equipment/plan/save.action";
		form1.submit();
		// Ext.MessageBox.hide();
		// Ext.MessageBox.alert('Status', 'Changes saved successfully.', '');
	} else {
		return false;
	}
}
function checkSubmit() {
	return true;
}

$(function() {

	$("#opensearch").click(function() {
		var option = {};
		option.width = 500;
		option.height = 170;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(gridGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, false, option);

	});

});