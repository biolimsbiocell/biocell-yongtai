﻿//待维修设备
var instrumentFault_id = $("#instrumentRepairPlan_id").text();
$(function() {
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : biolims.tInstrumentFaultDetail.instrument,
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push({
		"data" : "name",
		"title" : biolims.tInstrumentState.instrumentName,
		"createdCell" : function(td) {
			$(td).attr("saveName", "name");
		}
	});
	colOpts.push({
		"data": "spec",
		"title": biolims.equipment.spec,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "spec");
		},
	})
	
	colOpts.push({
		"data" : "serialNumber",
		"title" : biolims.equipment.serialNumber,
		"createdCell" : function(td) {
			$(td).attr("saveName", "serialNumber");
		}
	})
	colOpts.push({
		"data" : "supplier-name",
		"title" : biolims.tStorage.supplierId,
		"createdCell" : function(td) {
			$(td).attr("saveName", "supplier-name");
		}
	})
	colOpts.push({
		"data" : "type-name",
		"title" : biolims.tInstrument.mainType,
		"createdCell" : function(td) {
			$(td).attr("saveName", "type-name");
		}
	})
	colOpts.push({
		"data" : "state-name",
		"title" : biolims.tInstrument.state,
		"createdCell" : function(td) {
			$(td).attr("saveName", "state-name");
		}
	})
	var tbarOpts = [];
	tbarOpts.push({
		text: '<i class="fa fa-ioxhost"></i>'+biolims.common.search,
		action:function(){
			search();
		}
	});
		tbarOpts.push({
			text : biolims.common.addToDetail,
			action : function() {
				var sampleId = [];
				$("#instrumentTempdiv .selected").each(function(i, val) {
					sampleId.push($(val).children("td").eq(0).find("input").val());
				});
				// 先保存主表信息
				var createUser = $("#instrumentRepairPlan_createUser").text();
				var state = $("#instrumentRepairPlan_state").text();
				var id = $("#instrumentRepairPlan_id").text();
				var   unitId    = $("#instrumentRepairPlan_unit_id").val();
				var typeId = $("#instrumentRepairPlan_type_id").val();
				var note = $("#instrumentRepairPlan_note").val();
				var period = $("#instrumentRepairPlan_period").val();
				var tsTime = $("#instrumentRepairPlan_tsTime").val();

				if(0>=sampleId.length){
					top.layer.msg(biolims.common.pleaseSelect);
					return;
				}
				$.ajax({
					type : 'post',
					url : '/equipment/plan/addInstrument.action',
					data : {
						ids : sampleId,
						note : note,
						id : id,
						state : state,
						typeId : typeId,
						createUser : createUser,
						unitId : unitId,
						period :period,
						tsTime : tsTime
					},
					success : function(data) {
						var data = JSON.parse(data)
						if (data.success) {
							if(data.zy=="1"){
								top.layer.msg("已有设备被添加,请勿重复添加设备");
								return false;
							}else{
								$("#instrumentRepairPlan_id").text(data.data);
								$("#instrumentRepairPlanId").val(data.data);
								var param = {
										id: data.data
									};	
								var instrumentRepairPlanTaskTabs = $("#instrumentRepairPlanTaskdiv")
									.DataTable();
								instrumentRepairPlanTaskTabs.settings()[0].ajax.data = param;
								instrumentRepairPlanTaskTabs.ajax.reload();
							}
						} else {
							top.layer.msg(biolims.common.addToDetailFailed);
						}
						;
					}
				});
			}
		});

	
	var instrumentTempOps = table(true, instrumentFault_id,
			"/equipment/plan/showInstrumentTempTableJson.action?p_type=", colOpts, tbarOpts);
		instrumentTempTab = renderData($("#instrumentTempdiv"), instrumentTempOps);

	// 选择数据并提示
	instrumentTempTab.on('draw', function() {
		var index = 0;
		$("#instrumentTempdiv .icheck").on('ifChanged', function(event) {
			if ($(this).is(':checked')) {
				index++;
			} else {
				index--;
			}
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
	});
	bpmTask($("#bpmTaskId").val());
});



//大保存
function saveinstrumentRepairAndItem() {
		var changeLog = "";
		$('input[class="form-control"]').each(function(i, v) {
			var valnew = $(v).val();
			var val = $(v).attr("changelog");
			if(val !== valnew) {
				changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
			}
		});
		console.log($("#form1"));
		var jsonStr = JSON.stringify($("#form1").serializeObject());

		var dataItemJson = saveItemjson($("#instrumentTaskdiv"));
		var ele = $("#instrumentTempdiv");
		var changeLogItem = "主数据入库明细：";
		changeLogItem = getChangeLog(dataItemJson, ele, changeLogItem);
		var id = $("#instrumentFault_id").text();
		var createUser = $("#instrumentFault_handelUser").attr("userId");;
		var createDate = $("#instrumentFault_createDate").text();
		var typeId = $("#instrumentFault_type_id").val();
		var note = $("#instrumentFault_note").val();
		var state = $("#instrumentFault_state").text();
		top.layer.load(4, {shade:0.3}); 
		$.ajax({
			url: ctx + '/equipment/fault/addInstrument.action',
			dataType: 'json',
			type: 'post',
			data: {
				dataValue: jsonStr,
				changeLog: changeLog,
				ImteJson: dataItemJson,
				bpmTaskId: $("#bpmTaskId").val(),
				changeLogItem: changeLogItem,
				typeId : typeId,
				id:id,
				note : note,
				createUser:createUser,
				createDate:createDate
			},
			success: function(data) {
				if(data.success) {
					top.layer.closeAll();
					var url = "/equipment/fault/editInstrumentFault.action?id=" + data.id;

					url = url + "&bpmTaskId=" + $("#bpmTaskId").val() + "&p_type=" + $("#p_type").val();

					window.location.href = url;
				} else {
					top.layer.closeAll();
					top.layer.msg(data.msg);

				}
			}

		});

}

$.fn.serializeObject = function()
{
   var o = {};
   var a = this.serializeArray();
   $.each(a, function() {
       if (o[this.name]) {
           if (!o[this.name].push) {
               o[this.name] = [o[this.name]];
           }
           o[this.name].push(this.value || '');
       } else {
           o[this.name] = this.value || '';
       }
   });
   return o;
};
function tjsp() {
	top.layer
			.confirm(
					biolims.common.pleaseConfirmSaveBeforeSubmit,
					{
						icon : 3,
						title : biolims.common.prompt
					},
					function(index) {
						top.layer
								.open({
									title : biolims.common.approvalTask,
									type : 2,
									anim : 2,
									area : [ '800px', '500px' ],
									btn : biolims.common.selected,
									content : window.ctx
											+ "/workflow/processinstance/toStartView.action?formName=DnaTask",
									yes : function(index, layer) {
										var datas = {
											userId : userId,
											userName : userName,
											formId : instrumentFault_id,
											title : $("#instrumentRepair_name").val(),
											formName : "instrumentRepair"
										}
										ajax(
												"post",
												"/workflow/processinstance/start.action",
												datas,
												function(data) {
													if (data.success) {
														top.layer
																.msg(biolims.common.submitSuccess);
														if (typeof callback == 'function') {
															callback(data);
														}
														// dialogWin.dialog("close");
													} else {
														top.layer
																.msg(biolims.common.submitFail);
													}
												}, null);
										top.layer.close(index);
									},
									cancel : function(index, layer) {
										top.layer.close(index)
									}

								});
						top.layer.close(index);
					});

}
function sp() {
	var taskId = $("#bpmTaskId").val();
	var formId = instrumentFault_id;

	top.layer.open({
		title : biolims.common.detailedExaminationAndApproval,
		type : 2,
		anim : 2,
		area : [ '800px', '500px' ],
		btn : biolims.common.selected,
		content : window.ctx
				+ "/workflow/processinstance/toCompleteTaskView.action?taskId="
				+ taskId + "&formId=" + formId,
		yes : function(index, layer) {
			var operVal = $('.layui-layer-iframe', parent.document).find("iframe").contents()
					.find("#oper").val();
			var opinionVal = $('.layui-layer-iframe', parent.document).find("iframe").contents()
					.find("#opinionVal").val();

			if (!operVal) {
				top.layer.msg(biolims.common.pleaseSelectOper);
				return false;
			}
			if (operVal == "2") {
				_trunTodoTask(taskId, callback, dialogWin);
			} else {
				var paramData = {};
				paramData.oper = operVal;
				paramData.info = opinionVal;

				var reqData = {
					data : JSON.stringify(paramData),
					formId : formId,
					taskId : taskId,
					userId : window.userId
				}
				ajax("post", "/workflow/processinstance/completeTask.action",
						reqData, function(data) {
							if (data.success) {
								top.layer.msg(biolims.common.submitSuccess);
								if (typeof callback == 'function') {
								}
							} else {
								top.layer.msg(biolims.common.submitFail);
							}
						}, null);
			}
			location.href = window.ctx + "/lims/pages/dashboard/dashboard.jsp";
		},
		cancel : function(index, layer) {
			top.layer.close(index)
		}

	});
}
function searchOptions() { 
	return [{
		"txt" : biolims.tInstrumentState.instrumentName,
		"type" : "input",
		"searchName" : "name",
	},{
		"txt" : biolims.common.instrumentNo,
		"type" : "input",
		"searchName" : "id",
	},{
		"txt" : biolims.equipment.spec,
		"type" : "input",
		"searchName" : "spec",
	},{
		"txt" : biolims.equipment.producerName,
		"type" : "input",
		"searchName" : "producer-name",
	},{
		"txt" : biolims.tStorage.dicStorageMainTypeId,
		"type" : "input",
		"searchName" : "type-name",
	},{
		"txt" : biolims.tInstrument.serialNumber,
		"type" : "input",
		"searchName" : "serialNumber",
	}, {
		"type" : "table",
		"table" : instrumentTempTab
	} ];
}