/* 
 * 文件名称 :productBootstraptree.js
 * 创建者 : 郭恒开
 * 创建日期: 2018/04/12
 * 文件描述: 组织机构的treeGrid操作
 * 
 */

$(function() {
	$('#mytreeGrid').treegridData({
		id: 'id',
		parentColumn: 'parent',
		type: "GET", //请求数据的ajax类型
		url: '/core/department/showProductNewListJson.action', //请求数据的ajax的url
		ajaxParams: {}, //请求数据的ajax的data属性
		expandColumn: null, //在哪一列上面显示展开按钮
		striped: false, //是否各行渐变色
		bordered: true, //是否显示边框
		expandAll: false, //是否全部展开
		bSort: false,



		columns: [{
				title: 'ID',
				field: 'id'
			},
			{
				title: biolims.user.abbreviation,
				field: 'briefName'
			},
			{
				title: biolims.user.organizationName,
				field: 'name'
			},
			{
				title: biolims.common.state, //状态
				field: 'state'
			},
			{
				title: biolims.common.addr, //地址
				field: 'address'
			},
			{
				title: biolims.sample.note, //说明
				field: 'note'
			},
		]
	});
});
//新建
function add() {
	window.location = window.ctx + '/core/department/toAddDepartment.action'
}
//编辑
function edit() {
	var id = $(".chosed").children("td").eq(1).text();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/core/department/toModifyDepartment.action?id=' + id;
}
//查看
function view() {
	var id = $(".chosed").children("td").eq(1).text();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/core/department/toViewDepartments.action?id=' + id;
}

function query(){
	var instrumentDepartmentId = $("#instrument_department_id").val();
	var searchItemLayerValue = {};
	var k1 = $("#instrumentDepartmentId").attr("searchname");
	if(instrumentDepartmentId != null && instrumentDepartmentId != "") {
		searchItemLayerValue[k1] = "%" + instrumentDepartmentId + "%";
	} else {
		searchItemLayerValue[k1] = instrumentDepartmentId;
	}
	var query = JSON.stringify(searchItemLayerValue);
	var param = {
		query: query
	};
	sessionStorage.setItem("searchContent", query);
	
}

// 弹框模糊查询参数
function searchOptions() {
	return [{
		"txt": "ID",
		"type": "input",
		"searchName": "id",
	}, {
		"txt": biolims.user.abbreviation,
		"type": "input",
		"searchName": "briefName",
	}, {
		"txt": biolims.user.organizationName,
		"type": "input",
		"searchName": "name",
	}, {
		"txt":biolims.common.state, //状态
		"type": "select",
		"options": "有效|无效",
		"changeOpt": "1|0",
		"searchName": "state"
	},{
		"txt": biolims.common.addr, //地址
		"type": "input",
		"searchName": "address",
	}, {
		"txt": biolims.sample.note, //说明
		"type": "input",
		"searchName": "note",
	}, {
		"type": "table",
		"table": $('#mytreeGrid'),
		"reloadUrl": ctx + "/core/department/showProductNewListJson.action"
	}];
}
