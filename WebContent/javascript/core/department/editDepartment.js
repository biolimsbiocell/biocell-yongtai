/* 
 * 文件名称 :
 * 创建者 : 郭恒开
 * 创建日期: 2018/04/13
 * 文件描述: 组织机构管理子表展示
 * 
 */
$(function() {
	var id = $("#id").val();
	if($("#handlemethod").val() == "modify") {
		$("#id").prop("readonly", "readonly");
	}

	//机构用户
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.user.id,
	});
	colOpts.push({
		"data": "name",
		"title": biolims.common.sname,
	});
	colOpts.push({
		"data": "department-name",
		"title": biolims.equipment.departmentName,
	});

	colOpts.push({
		"data": "dicJob-name",
		"title": biolims.user.post,
	});
	var handlemethod = $("#handlemethod").val();
	var sampleInfoOptions = table(true,
		id,
		'/core/department/showDepartmentUserListJson.action?id='+$("#id").val(), colOpts, tbarOpts)
	myTable = renderData($("#departmentTable"), sampleInfoOptions);

})
//保存
function save() {
	//必填验证
	var requiredField=requiredFilter();
		if(!requiredField){
			return false;
		}
	if(checkSubmit() == true) {
		if($("#handlemethod").val() == "modify") {
			var changeLog = "";
			$('input[class="form-control"]').each(function(i, v) {
				var valnew = $(v).val();
				var val = $(v).attr("changelog");
				if(val !== valnew) {
					changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
				}
			});
			document.getElementById("changeLog").value = changeLog;
			top.layer.load(4, {shade:0.3}); 
			$("#form1").attr("action",ctx+"/core/department/save.action");
			$("#form1").submit();
			top.layer.closeAll();
		} else {
			//先查组织结构ID是否存在了
			$.ajax({
				type: "post",
				url: ctx + '/common/hasId.action',
				data: {
					id: $("#id").val(),
					obj: 'Department'
				},
				success: function(data) {
					var data = JSON.parse(data);
					if(data.message) {
						top.layer.msg(data.message);
					} else {
						var changeLog = "";
						$('input[class="form-control"]').each(function(i, v) {
							var valnew = $(v).val();
							var val = $(v).attr("changelog");
							if(val !== valnew) {
								changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
							}
						});
						document.getElementById("changeLog").value = changeLog;
						top.layer.load(4, {shade:0.3}); 
						$("#form1").attr("action",ctx+"/core/department/save.action");
						$("#form1").submit();
						top.layer.closeAll();
					}
				}
			});
		}
	}
	
}
function checkSubmit() {
	if($("#id").val() == null || $("#id").val() == "") {
		top.layer.msg(biolims.user.idIsEmpty);
		return false;
	};
	return true;
}
//选择上机组织
function choseParent() {
	top.layer.open({
		title: biolims.user.selectedUperiorOrganization,
		type: 2,
		area: ["60%","65%"],
		btn: biolims.common.selected,
		content: window.ctx +
			"/core/department/departmentSelect.action",
		yes: function(index, layer) {
			var name=[],id=[];
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#mytreeGrid .chosed").each(function (i,v) {
				name.push($(v).children("td").eq(2).text());
				id.push($(v).children("td").eq(1).text());
			});
			if(name.length!=1||id.length!=1){
				top.layer.msg(biolims.common.pleaseSelectAPieceOfData);
			}else{
				if(id.join(",")==$("#id").val()){
					top.layer.msg(biolims.common.organizationRechoose);
				}else{
					$("#department_upDepartment_name").val(name.join(","));
					$("#department_upDepartment_id").val(id.join(","));
					$("#department_parent").val(id.join(","));
					top.layer.close(index)
				}
			}
		}
	})
}


//新建
function add() {
	window.location = window.ctx+'/core/department/toAddDepartment.action';
}
//列表
function list() {
	window.location= window.ctx +'/core/department/showDepartmentList.action';
}