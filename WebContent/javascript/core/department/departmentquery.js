
function query(){
	var instrumentDepartmentId = $.trim($("#instrumentDepartmentId").val());
	var searchItemLayerValue = {};
	var k1 = "briefName";
	if(instrumentDepartmentId != null && instrumentDepartmentId != "") {
		searchItemLayerValue[k1] = "%" + instrumentDepartmentId + "%";
	} else {
		searchItemLayerValue[k1] = instrumentDepartmentId;
	}
	var query = JSON.stringify(searchItemLayerValue);
	var param = {
		query: query
	};
	$('#mytreeGrid').html("");
	$('#mytreeGrid').treegridData({
		id: 'id',
		parentColumn: 'parent',
		type: "post", //请求数据的ajax类型
		url: '/core/department/showProductNewListJson.action', //请求数据的ajax的url
		ajaxParams: param, //请求数据的ajax的data属性
		expandColumn: null, //在哪一列上面显示展开按钮
		striped: false, //是否各行渐变色
		bordered: true, //是否显示边框
		expandAll: false, //是否全部展开
		columns: [{
				title: 'ID',
				field: 'id'
			},
			{
				title: biolims.user.abbreviation,
				field: 'briefName'
			},
			{
				title: biolims.user.organizationName,
				field: 'name'
			}
		]
	});

}