/* 
 * 文件名称 :editRole.js
 * 创建者 : 郭恒开
 * 创建日期: 2018/04/10
 * 文件描述: 
 * 
 */
$(function() {
	var id = $("#id").val();
	if($("#handlemethod").val() == "modify") {
		$("#id").prop("readonly", "readonly");
	}
	//人员配置
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.user.id ,
	});
	colOpts.push({
		"data": "name",
		"title": biolims.common.sname,
	});
	colOpts.push({
		"data": "department-name",
		"title": biolims.tInstrument.department,
	});

	colOpts.push({
		"data": "dicJob-name",
		"title": biolims.user.post ,
	});
	var handlemethod = $("#handlemethod").val();

	if(handlemethod != "view") {
		tbarOpts.push({
			text: '<i class="fa fa-yelp"></i>'+biolims.common.fillDetail,
			action: addUser
		});

		tbarOpts.push({
			text: biolims.common.delSelected,
			action: function() {
				removeChecked($("#userGroupTable"),
					"/core/userGroup/delUserGroupUser.action","人员组管理：",id);
			}
		});	
	}
	var sampleInfoOptions = table(true,
		id,
		'/core/userGroup/showUserGroupUserListJson.action', colOpts, tbarOpts)
	myTable = renderData($("#userGroupTable"), sampleInfoOptions);

})
//保存
function save() {
	var changeLog = "";
	$('input[class="form-control"]').each(
			function(i, v) {
				var valnew = $(v).val();
				var val = $(v).attr("changelog");
				if (val !== valnew) {
					changeLog += $(v).prev("span").text() + ':由"' + val
							+ '"变为"' + valnew + '";';
				}
			});
	//必填验证
	var requiredField=requiredFilter();
		if(!requiredField){
			return false;
		}
	if($("#handlemethod").val() == "modify") {
		top.layer.load(4, {shade:0.3}); 
		$("#form1").attr("action",ctx+"/core/userGroup/save.action?changeLog="+changeLog);
		$("#form1").submit();
		top.layer.closeAll();
	} else {
		//先查角色编号是否存在了
		$.ajax({
			type: "post",
			url: ctx + '/common/hasId.action',
			data: {
				id: $("#id").val(),
				obj: 'UserGroup',
				changeLog :changeLog
			},
			success: function(data) {
				var data = JSON.parse(data);
				if(data.message) {
					top.layer.msg(data.message);
				} else {
					top.layer.load(4, {shade:0.3});
					$("#form1").attr("action",ctx+"/core/userGroup/save.action");
					$("#form1").submit();
					top.layer.closeAll();
				}
			}
		});
	}
}
//添加明细
function addUser () {
		top.layer.open({
		title: "选择人员",
		type: 2,
		area: ["65%", "70%"],
		btn: biolims.common.selected,
		content: [window.ctx + "/core/user/selectUsersTable.action", ''],
		yes: function(index, layer) {
			var chosed = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .selected");
			var user=[];
			chosed.each(function (i,tr) {
				user.push($(tr).children("td").eq(1).text());
			});
			$.ajax({
				type:"post",
				data:{
					userId:user,
					groupId:$("#id").val()
				},
				url:ctx+"/core/userGroup/saveUserGroupUser.action",
				success:function(data){
					var data=JSON.parse(data);
					if(data.success){
						tableRefresh();
					}
				}
			});
			top.layer.close(index)
		},
	})
}
//新建
function add() {
	window.location = window.ctx+'/core/userGroup/toEditUserGroup.action';
}
//列表
function list() {
	window.location= window.ctx +'/core/userGroup/showUserGroupList.action';
}