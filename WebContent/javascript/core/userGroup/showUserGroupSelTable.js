/* 
 * 文件名称 :showUserGroupSelTable.js
 * 创建者 : 郭恒开
 * 创建日期: 2018/02/06
 * 文件描述: 负责组的弹框
 * 
 */

$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
	});
	colOpts.push({
		"data": "name",
		"title": biolims.user.eduName1,
	});
	colOpts.push({
		"data": "note",
		"title": biolims.common.note,
	});
	var tbarOpts = [];
	var options = table(false, null,
		'/core/userGroup/userGroupSelTableJson.action', colOpts, tbarOpts)
	var addUserGroup = renderData($("#addUserGroup"), options);
	$("#addUserGroup").on(
		'init.dt',
		function(e, settings) {
			// 清除操作按钮
			$("#addUserGroup_wrapper .dt-buttons").empty();
			$('#addUserGroup_wrapper').css({
				"padding": "0 16px"
			});
			var trs = $("#addUserGroup tbody tr");
			addUserGroup.ajax.reload();
			addUserGroup.on('draw', function() {
				trs = $("#addUserGroup tbody tr");
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			});
			trs.click(function() {
				$(this).addClass("chosed").siblings("tr")
					.removeClass("chosed");
			});
		});

})