var UserTable;
$(function() {
	var searchModel = '<div class="input-group"><input type="text" class="form-control" placeholder="Search..." id="searchvalue" onkeydown="entersearch()"><span class="input-group-btn"> <button class="btn btn-info" type="button" onclick="searchModel()">按姓名搜索</button></span> </div>';	

	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": "用户名",
	});
	colOpts.push({
		"data": "name",
		"title": "姓名",
	});
	colOpts.push({
		"data": "email",
		"title": "Email",
	});
	colOpts.push({
		"data": "address",
		"title": "地址",
	});
	var tbarOpts = [];
	var addUserOptions = table(true, null,
		'/core/user/selectUsersTableJson.action',
		colOpts, tbarOpts)
	UserTable = renderData($("#addUserTable"), addUserOptions);
	$("#addUserTable").on('init.dt',function(e, settings) {
			// 清除操作按钮
			$("#addUserTable_wrapper .dt-buttons").empty();
			$("#addUserTable_wrapper .dt-buttons").html(searchModel);

			$('#addUserTable_wrapper').css({
				"padding": "0 16px"
			});
			var trs = $("#addUserTable tbody tr");
			UserTable.ajax.reload();
		});
})

//回车搜索
function entersearch(){  
    var event = window.event || arguments.callee.caller.arguments[0];  
    if (event.keyCode == 13)  
    {  
        searchModel();
    }  
} 
//搜索方法
function searchModel() {
	UserTable.settings()[0].ajax.data = {
		query: JSON.stringify({
			name:"%"+$("#searchvalue").val().trim()+"%"
		})
	};
	UserTable.ajax.reload();
}