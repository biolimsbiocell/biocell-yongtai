$(function() {

	if($("#groupId").val()=="XXGL"){
		var colOpts = [];
		colOpts.push({
			"data" : "id",
			"title" : biolims.common.id,
		});
		colOpts.push({
			"data" : "name",
			"title" : biolims.common.sname,
		});

		var tbarOpts = [];
		tbarOpts.push({
			text:"搜索",
			action: search,
		});
		var addUserOptions = table(false, null,
				'/core/user/selectUserTableNewJson.action?groupId=' + $("#groupId").val(),
				colOpts, tbarOpts)
		UserTable = renderData($("#addUserTable"), addUserOptions);
		$("#addUserTable").on(
				'init.dt',
				function(e, settings) {
					// 清除操作按钮
					$("#addUserTable_wrapper .dt-buttons").empty();
					$('#addUserTable_wrapper').css({
						"padding": "0 16px"
					});
				var trs = $("#addUserTable tbody tr");
				UserTable.ajax.reload();
				UserTable.on('draw', function() {
					trs = $("#addUserTable tbody tr");
					trs.click(function() {
						$(this).addClass("chosed").siblings("tr")
							.removeClass("chosed");
					});
				});
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
				});
		
	}else{
		var colOpts = [];
		colOpts.push({
			"data" : "user-id",
			"title" : biolims.common.id,
		});
		colOpts.push({
			"data" : "user-name",
			"title" : biolims.common.sname,
		});

		var tbarOpts = [];
		
		tbarOpts.push({
			text:"搜索",
			action: search,
		});
		var addUserOptions = table(false, null,
				'/core/user/selectUserTableJson.action?groupId=' + $("#groupId").val(),
				colOpts, tbarOpts)
		 UserTable = renderData($("#addUserTable"), addUserOptions);
		$("#addUserTable").on(
				'init.dt',
				function(e, settings) {
					// 清除操作按钮
//					$("#addUserTable_wrapper .dt-buttons").empty();
//					$('#addUserTable_wrapper').css({
//						"padding": "0 16px"
//					});
				var trs = $("#addUserTable tbody tr");
				UserTable.ajax.reload();
				UserTable.on('draw', function() {
					trs = $("#addUserTable tbody tr");
					trs.click(function() {
						$(this).addClass("chosed").siblings("tr")
							.removeClass("chosed");
					});
				});
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
				});
		
	}
	
	
})
function searchOptions() {
			return [{
					"txt": "编码",
					"type": "input",
					"searchName": "user-id"
				},
				{
					"txt": "姓名",
					"type": "input",
					"searchName": "user-name"
				},
				{
					"type" : "table",
					"table" : UserTable
				}
				
			];
		}


