var userHealthItemTable;
var olduserHealthItemChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
/**
 * 用户健康状况
 * @returns
 */
$(function() {
	// 加载子表
	var id = $("#user_id").val();
	var tbarOpts = [];
	var colOpts = [];
	   colOpts.push({
		"data":"id",
		"title": "ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
		"visible": false
	});
	   colOpts.push({
		"data":"testedDate",
		"title": "体检时间",
		"createdCell": function(td) {
			$(td).attr("saveName", "testedDate");
	    },
	    "className":"date"
	});
	   colOpts.push({
		"data":"expireDate",
		"title": biolims.common.expirationDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "expireDate");
	    },
	    "className":"date"
	});
	   colOpts.push({
		"data":"checkResult",
		"title":biolims.common.checkResult,
		"className":"select",
		"name":biolims.common.health+"|"+biolims.common.good+"|"+biolims.common.notHealth,
		"createdCell": function(td) {
			$(td).attr("saveName", "checkResult");
			$(td).attr("selectOpt", biolims.common.health+"|"+biolims.common.good+"|"+biolims.common.notHealth);
		},
		"render": function(data, type, full, meta) {
			if(data == "0") {
				return biolims.common.notHealth;
			}
			if(data == "1") {
				return biolims.common.health;
			}
			if(data == "2") {
				return biolims.common.good;
			} else {
				return '';
			}
		}
	});
	   colOpts.push({
			"data":"name",
			"title":biolims.common.name,
			"createdCell": function(td) {
				$(td).attr("saveName", "name");
		    },
			"className": "textarea"
		});
	   colOpts.push({
		"data":"note",
		"title":biolims.ufTask.note,
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
	    },
		"className": "edit"
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
		
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#userJobTable"))
	}
	});
	
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveUserHealthItem($("#userJobTable"));
		}
	});
	tbarOpts.push({
		text: biolims.common.delSelected,
		action: function() {
			removeChecked($("#userJobTable"),
				"/core/user/delUserHealthItem.action");
		}
	});
	tbarOpts.push({
		text : biolims.common.editwindow,
		action : function() {
			editItemLayer($("#userJobTable"))
		}
	});
	}
	
	var userHealthItemOptions = table(true,id,
		'/core/user/showUserHealthTableListJson.action', colOpts, tbarOpts)
	userHealthItemTable = renderData($("#userJobTable"), userHealthItemOptions);
	userHealthItemTable.on('draw', function() {
		olduserHealthItemChangeLog = userHealthItemTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});
// 保存
function saveUserHealthItem(ele) {
	var data = saveUserHealthItemjson(ele);
	var ele=$("#userJobTable");
	var changeLog = "用户健康情况：";
	changeLog = getChangeLog(data, ele, changeLog);
	top.layer.load(4, {shade:0.3});
	$.ajax({
		type: 'post',
		url: '/core/user/saveUserHealthItemTable.action',
		data: {
			id: $("#user_id").val(),
			dataJson: data,
			changeLog: changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveUserHealthItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			json[k] = $(tds[j]).text();
			
			if(k == "checkResult") {
				var checkResult = $(tds[j]).text();
				if(checkResult == biolims.common.notHealth) {
					json[k] = "0";
				} else if(checkResult == biolims.common.health) {
					json[k] = "1";
				} else if(checkResult == biolims.common.good) {
					json[k] = "2";
				} else {
					json[k] = "";
				}
				continue;
			}
		}
		data.push(json);
	});
	return JSON.stringify(data);
}

function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '健康情况编号为"' + v.code + '":';
		olduserHealthItemChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
