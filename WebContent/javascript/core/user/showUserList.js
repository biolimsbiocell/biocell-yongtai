var userTab;
$(function() {
	var options = table(true, "",
			"/core/user/getUserList.action", [ {
				"data" : "id",
				"title" :biolims.user.id,
			}, {
				"data" : "name",
				"title" : biolims.common.sname,
			}, {
				"data" : "sex",
				"title" : biolims.common.gender,
				
				"createdCell" : function(td) {
					$(td).attr("saveName", "sex");
					$(td).attr(
							"selectOpt",
							biolims.common.male + "|" + biolims.common.female + "|"
									+ biolims.common.unknown);
				},
				"name" : biolims.common.male + "|" + biolims.common.female + "|"
						+ biolims.common.unknown,
				"render" : function(data, type, full, meta) {
					if (data == "0") {
						return biolims.common.female;
					}
					if (data == "1") {
						return biolims.common.male;
					}
					if (data == "3") {
						return biolims.common.unknown;
					} else {
						return '';
					}
				}
			}, {
				"data" : "birthday",
				"title" : biolims.common.birthDay,
			}, {
				"data" : "mobile",
				"title" : biolims.user.mobile,
			}, {
				"data" : "inDate",
				"title" : biolims.user.inDate,
			}, {
				"data" : "department-id",
				"title" : biolims.equipment.departmentId,
			}, {
				"data" : "department-name",
				"title" : biolims.equipment.departmentName,
			},{
				"data" : "area",
				"title" : biolims.crm.districtName,
			},{
				"data" : "city",
				"title" : biolims.user.city,
			},{
				"data" : "scopeName",
				"title" : biolims.purchase.costCenter,
			},{
				"data" : "state-name",
				"title" : biolims.common.state,
			} ], null)
	userTab = renderRememberData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(userTab);
	})
});
function edit() {
	var id = "";
	id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/core/user/toEditUser.action?id=' + id;
}

function add() {
	window.location = window.ctx + '/core/user/toEditUser.action';

}

function view() {

	var id = "";

	id = $(".selected").find("input").val();

	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}

	window.location = window.ctx + '/core/user/toViewUser.action?id=' + id;
}

//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt": "用户名"
		});
	   fields.push({
		    "searchName":"name",
			"type":"input",
			"txt": "姓名"
		});
	   fields.push({
		    "searchName":"sex",
			"type":"input",
			"txt": "性别"
		});
	   fields.push({
		    "searchName":"mobile",
			"type":"input",
			"txt": "手机号"
		});
	   fields.push({
		    "searchName":"inDate##@@##1",
			"type":"dataTime",
			"mark":"z##@@##",
			"txt": "入职时间"
		});
	   fields.push({
		    "searchName":"department.id",
			"type":"input",
			"txt": "部门ID"
		});
	   fields.push({
		    "searchName":"department.name",
			"type":"input",
			"txt": "部门"
		});
	   fields.push({
		    "searchName":"state.name",
			"type":"input",
			"txt": "状态"
		});
	   fields.push({
		    "searchName":"area",
			"type":"input",
			"txt": "所属片区"
		});
	   fields.push({
		    "searchName":"city",
			"type":"input",
			"txt": "所属城市"
		});
	fields.push({
		"type":"table",
		"table":userTab
	});
	return fields;
}
