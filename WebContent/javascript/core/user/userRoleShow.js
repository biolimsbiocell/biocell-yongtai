var oldChangeLog;
$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.user.roleID,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push({
		"data": "name",
		"title": biolims.user.roleName,
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
		}
	});
	colOpts.push({
		"data": "note",
		"title": biolims.sample.note,
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
		}
	})
	colOpts.push({
		"data": "isCheck",
		"title": biolims.common.select,
		"createdCell": function(td) {
			$(td).attr("saveName", "isCheck");
		},
		"render": function(data, type, full, meta) {
			if(data == "0") {
				return "<input type='checkbox' class='icheck' >";
			}
			if(data == "1") {
				return "<input type='checkbox' checked='checked' class='icheck'>";
			}
		}
	})
	var tbarOpts = [];
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveRole();
		}
	});
	var options = table(false, $("#user_id").val(), "/core/user/userRoleShowJson.action", colOpts, tbarOpts);
	userRoleTab = renderData($("#userRoleTable"), options);
	userRoleTab.on('draw', function() {
		oldChangeLog = userRoleTab.ajax.json();
	});
	bpmTask($("#bpmTaskId").val());
});
//保存
function saveRole() {
	var ele=$("#userRoleTable");
	var changeLog = "用户权限：";
	var data = saveRolejson(ele);
	changeLog = getChangeLog(data, ele, changeLog);
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		type: 'post',
		url: '/core/user/setRole.action',
		data: {
			id: $("#user_id").val(),
			dataJson: data,
			logInfo: changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
//获得保存时的json数据
function saveRolejson(ele) {
	var trs = ele.find("tbody").children();
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		for(var j = 0; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断男女并转换为数字
			if(k == "isCheck") {
				var isCheck = $(tds[j]).find(".checked");
				console.log(isCheck)
				if(isCheck.length==1) {
					json[k] = "1";
				} else{
					json[k] = "0";
				}
				continue;
			}
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '角色编号为"' + v.code + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}