var userCertShowItemTable;
var oldcertShowItemChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
/**
 * 用户健康状况
 * 
 * @returns
 */
$(function() {
	// 加载子表
	var id = $("#user_id").val();
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : "ID",
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		},
		"visible" : false
	});
	colOpts.push({
		"data" : "name",
		"title" : biolims.user.certificateName,
		"createdCell" : function(td) {
			$(td).attr("saveName", "name");
		},
		"className" : "edit"
	});
	colOpts.push({
		"data" : "no",
		"title" : biolims.user.certificateNo,
		"createdCell" : function(td) {
			$(td).attr("saveName", "no");
		},
		"className" : "edit"
	});
	colOpts.push({
		"data" : "sendOrg",
		"title" : "培训机关",
		"createdCell" : function(td) {
			$(td).attr("saveName", "sendOrg");
		},
		"className" : "edit"
	});
	colOpts.push({
		"data" : "sendDate",
		"title" : biolims.user.issueDate,
		"createdCell" : function(td) {
			$(td).attr("saveName", "sendDate");
		},
		"className" : "date"
	});
	colOpts.push({
		"data" : "overDate",
		"title" : biolims.user.overDate,
		"createdCell" : function(td) {
			$(td).attr("saveName", "overDate");
		},
		"className" : "date"
	});
	colOpts.push({
		"data" : "remark",
		"title" : biolims.ufTask.name,
		"createdCell" : function(td) {
			$(td).attr("saveName", "remark");
		},
		"className" : "textarea"
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {

		tbarOpts.push({
			text : biolims.common.fillDetail,
			action : function() {
				addItem($("#userCertTable"))
			}
		});

		tbarOpts.push({
			text : biolims.common.save,
			action : function() {
				saveUserCertItem($("#userCertTable"));
			}
		});
		tbarOpts.push({
			text : biolims.common.delSelected,
			action : function() {
				removeChecked($("#userCertTable"),
						"/core/user/delUserCertItem.action");
			}
		});
		tbarOpts.push({
			text : biolims.common.editwindow,
			action : function() {
				editItemLayer($("#userCertTable"))
			}
		});
	}

	var certShowOptions = table(true, id,
			'/core/user/showUserCertTableListJson.action', colOpts, tbarOpts)
	userCertShowItemTable = renderData($("#userCertTable"), certShowOptions);
	userCertShowItemTable.on('draw', function() {
		oldcertShowItemChangeLog = userCertShowItemTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	// stepViewChange();
});
// 保存
function saveUserCertItem(ele) {
	var data = saveUserCertItemjson(ele);
	var ele = $("#userCertTable");
	var changeLog = "用户证书情况：";
	changeLog = getChangeLog(data, ele, changeLog);

	$.ajax({
		type : 'post',
		url : '/core/user/saveUserCertItemTable.action',
		data : {
			id : $("#user_id").val(),
			dataJson : data,
			changeLog : changeLog
		},
		success : function(data) {
			var data = JSON.parse(data)
			if (data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.msg(biolims.common.saveFailed)
			}
			;
		}
	})
}
// 获得保存时的json数据
function saveUserCertItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for (var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			json[k] = $(tds[j]).text();

		}
		data.push(json);
	});
	return JSON.stringify(data);
}

function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '证书情况编号为"' + v.code + '":';
		oldcertShowItemChangeLog.data.forEach(function(vv, ii) {
			if (vv.id == id) {
				for ( var k in v) {
					if (v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"'
								+ v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
