	$("#btn_submit").hide();
	$("#btn_changeState").hide();
$(function() {
	var id = $("#user_id").val();
	if($("#handlemethod").val() == "modify") {
		$("#user_id").prop("readonly", "readonly");
	}
	
	$(".step").click(
		function() {
			if($("#user_id").attr("changelog")) {
				var index = $(".wizard_steps .step").index($(this));
				$(".wizard_steps .selected").removeClass("selected")
					.addClass("done");
				$(this).removeClass("done").addClass("selected");
				$(".HideShowPanel").eq(index).slideDown().siblings(
					'.HideShowPanel').slideUp();
				//table刷新和操作隐藏
				console.log(index);
				if(index == 0) {
					$(".box-tools").hide();
				} else {
					$(".box-tools").hide();
				}
				$('body,html').animate({
					scrollTop: 0
				}, 500, function() {
					$(".HideShowPanel").each(function() {
						if($(this).css("display") == "block") {
							var table = $(this).find(".dataTables_scrollBody").children("table").attr("id");
							$("#" + table).DataTable().ajax.reload();
						}
					});
				});
			} else {
				top.layer.msg("请先填写用户名、密码并保存");
			}
		});
	$("#user_birthday").datepicker({
		language: "zh-TW",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});
	$("#user_inDate").datepicker({
		language: "zh-TW",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});
	layui.use('form', function() {
		var form = layui.form;
		// 控制性别选择
		form.on('checkbox', function(data) {
			$(data.othis[0]).siblings(".layui-form-checkbox").removeClass(
				"layui-form-checked");
			if(data.elem.checked) {
				$("#user_sex").val(data.elem.value);
			}
		});
	});
});

function list() {
	window.location = window.ctx + '/core/user/showUserList.action';

}

function add() {
	window.location = window.ctx + '/core/user/toEditUser.action';
}

function save() {
	
	
	//身份证
	var reg1 =/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/;
	//电话
	var reg3 = /^[1][3,4,5,7,8][0-9]{9}$/;
	//邮箱
	var reg4 = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
	debugger
	if(!reg1.test($("#user_cardNo").val())&&$("#user_cardNo").val()!=""){
		top.layer.msg(("您输入的身份证号码有误,请重新输入!"));
		return false;
	}
	
	if(!reg3.test($("#user_contactMethod").val())&&$("#user_contactMethod").val()!=""){
		top.layer.msg("联系电话不是完整的11位手机号或者正确的手机号!")
		return false;
	}
	if(!reg3.test($("#user_phone").val())&&$("#user_phone").val()!=""){
		top.layer.msg("电话不是完整的11位手机号或者正确的手机号!")
		return false;
	}
	if(!reg3.test($("#user_mobile").val())&&$("#user_mobile").val()!=""){
		top.layer.msg("手机号不是完整的11位手机号或者正确的手机号!")
		return false;
	}
	if(!reg4.test($("#user_email").val())&&$("#user_email").val()!=""){
		top.layer.msg("email格式有误,请重新输入!")
		return false;
	}
	
	//必填验证
	var requiredField=requiredFilter();
		if(!requiredField){
			return false;
		}
		var handlemethod = $("#handlemethod").val();
		if(handlemethod == "modify"&&checkSubmit() == true) {
			
			var changeLog = "用户列表-";
			$('input[class="form-control"]').each(function(i, v) {
				var valnew = $(v).val();
				var val = $(v).attr("changelog");
				if(val !== valnew) {
					changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
				}
			});
			
			var changeLogs = "";
			if(changeLog != "用户列表-") {
				changeLogs = changeLog;
				$("#changeLog").val(changeLogs);
			}
			document.getElementById('userRoleJson').value = saveRolejson($("#userRoleTable"));
			document.getElementById('userGroupJson').value = saveItemjson($("#userGroupUserTable"));
			top.layer.load(4, {shade:0.3}); 
			
			$("#form1").attr("action", "/core/user/save.action?changeLog="+encodeURIComponent(encodeURIComponent(changeLog)));
			$("#form1").submit();
			top.layer.closeAll();
		} else {
			$.ajax({
				type: "post",
				url: ctx + '/common/hasId.action',
				data: {
					id: $("#user_id").val(),
					obj: 'User',
					changeLog:changeLog
				},
				success: function(data) {
					var data = JSON.parse(data);
					if(data.message) {
						top.layer.msg("用户已存在!");
					} else {
						$("#form1").attr("action", "/core/user/save.action");
						$("#form1").submit();
					}
				}
			});
		}
	/*if(checkSubmit() == true) {
		document.getElementById('userRoleJson').value = saveRolejson($("#userRoleTable"));
		document.getElementById('userGroupJson').value = saveItemjson($("#userGroupUserTable"));
		top.layer.load(4, {shade:0.3}); 
		form1.action = window.ctx +
			"/core/user/save.action";
		form1.submit();
		top.layer.closeAll();
	}*/

}

function checkSubmit() {
	if($("#user_id").val() == null || $("#user_id").val() == "") {
		top.layer.msg(biolims.main.usernull);
		return false;
	};
	return true;
}

//选择所属部门
function showDepartment() {
	top.layer.open({
		title: biolims.user.selectTheDepartment,
		type: 2,
		area: ["60%", "65%"],
		btn: biolims.common.selected,
		content: window.ctx +
			"/core/department/departmentSelect.action",
		yes: function(index, layer) {
			var name = [],
				id = [];
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#mytreeGrid .chosed").each(function(i, v) {
				name.push($(v).children("td").eq(2).text());
				id.push($(v).children("td").eq(1).text());
			});
			if(name.length != 1 || id.length != 1) {
				top.layer.msg(biolims.common.pleaseSelectAPieceOfData);
			} else {
				$("#user_department_name").val(name.join(","));
				$("#user_department_id").val(id.join(","));
				top.layer.close(index)
			}
		}
	})
}
//成本中心
function showScope() {
	top.layer.open({
		title: biolims.user.selectCostCentre,
		type: 2,
		area: ["60%", "65%"],
		btn: biolims.common.selected,
		content: window.ctx + "/core/user/showCostCenter.action",
		yes: function(index, layer) {
			var id = [],
				name = [];
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#costCenter input").each(function(i, v) {
				if($(v).is(':checked')) {
					id.push(v.value);
					name.push(v.title);
				}
			});
			$("#user_scopeName").val(name.join(","));
			$("#user_scopeId").val(id.join(","));
			top.layer.close(index)
		}
	})
}
function showState(){
	layer.open({
		title: biolims.user.selectedState,
		type: 2,
		area: ["60%", "65%"],
		btn: biolims.common.selected,
		content: window.ctx + "/dic/state/dicStateSelectTable.action?flag=user",
		yes: function(index, layero) {
			var name = $(".layui-layer-iframe").find("iframe").contents().find("#addDicStateTable .chosed").children("td")
			.eq(1).text();
		var id = $(".layui-layer-iframe").find("iframe").contents().find("#addDicStateTable .chosed").children("td").eq(
			0).text();
			$("#user_state_name").val(name);
			$("#user_state_id").val(id);
			layer.close(index)
		},
		cancel: function(index, layero) {
			layer.close(index)
		}
	})
}