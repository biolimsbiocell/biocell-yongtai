/* 
* 文件名称 :showRoleList.js
* 创建者 : 郭恒开
* 创建日期: 2018/04/08
* 文件描述: 角色管理主表
* 
*/
$(function() {
	var options = table(true, "",
			"/core/role/showRoleListJson.action", [ {
				"data" : "id",
				"title" : biolims.user.roleID,
			}, {
				"data" : "name",
				"title" : biolims.user.roleName,
			}, {
				"data" : "note",
				"title" : biolims.common.name,
			}, {
				"data" : "state",
				"title" :biolims.common.state,
				"render":function (data) {
					if(data=="1"){
						return biolims.master.valid;
					}else{
						return biolims.master.invalid;
					}
				}
			}], null)
	roleList = renderRememberData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(roleList);
	})
});

function add() {
	window.location = window.ctx+'/core/role/toEditRole.action';
}
function edit() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx+'/core/role/toEditRole.action?id='+id;
}
function view() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	$("#maincontentframe", window.parent.document)[0].src = window.ctx+'/core/role/toView.action?id='+id;
}
// 弹框模糊查询参数
function searchOptions() {
	return [ {
		"txt" : biolims.user.roleID,
		"type" : "input",
		"searchName" : "id",
	}, {
		"txt" : biolims.user.roleName,
		"type" : "input",
		"searchName" : "name",
	}, {
		"txt" : biolims.common.name,
		"type" : "input",
		"searchName" : "note",
	},{
		"txt" : biolims.common.state,
		"type" : "select",
		"options":"请选择"+"|"+biolims.master.valid+"|"+biolims.master.invalid,
		"changeOpt":"''|1|0",
		"searchName" : "state",
	},{
		"type" : "table",
		"table" : roleList
	} ];
}
