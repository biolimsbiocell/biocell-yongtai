/* 
 * 文件名称 :editRole.js
 * 创建者 : 郭恒开
 * 创建日期: 2018/01/10
 * 文件描述: 角色管理及权限配置
 * 
 */
$(function() {
	var id = $("#id").val();
	if($("#handlemethod").val() == "modify") {
		$("#id").prop("readonly", "readonly");
	}
	// 点击上一步下一步切换
	stepViewChange();
	//layui 的 form 模块是必须的
	layui.use(['form'], function() {
		var form = layui.form;
		//获取权限配置的数据
		$.ajax({
			type: "post",
			url: ctx + "/core/role/getRights.action",
			data: {
				roleId: id
			},
			success: function(data) {
				//console.log(data);
				var right = JSON.parse(data);
				xtree1 = new layuiXtree({
					elem: 'xtree1',
					form: form,
					data: right,
					isopen: false, //加载完毕后的展开状态，默认值：true
					ckall: false, //启用全选功能，默认值：false	
					ckallback: function() {}, //全选框状态改变后执行的回调函数
					icon: { //三种图标样式，更改几个都可以，用的是layui的图标
						open: "&#xe7a0;", //节点打开的图标
						close: "&#xe622;", //节点关闭的图标
						end: "&#xe621;" //末尾节点的图标
					},
					color: { //三种图标颜色，独立配色，更改几个都可以
						open: "#EE9A00", //节点图标打开的颜色
						close: "#EEC591", //节点图标关闭的颜色
						end: "#828282" //末级节点图标的颜色
					},
					click: function(data) { //节点选中状态改变事件监听，全选框有自己的监听事件
						var elem = data.elem;
						if(elem.getAttribute("rightsFunction") != "undefined" && elem.checked) {
							top.layer.open({
								title: "编辑动作权限",
								type: 1,
								area: ["40%", "50%"],
								btn: biolims.common.selected,
								content: $("#editPower").html(),
								success: function() {
									var allPower = elem.getAttribute("rightsFunction");
									var activePower = elem.getAttribute("rightsAction");
									var inputs = $("#editrights", parent.document).find("input");
									inputs.each(function (i,v) {
										if(allPower.indexOf(v.value)==-1){
											$(v).parents(".col-xs-6").hide();
										}
										if(activePower.indexOf(v.value)!=-1){
											$(v).prop("checked",true);
										}
									});
									inputs.iCheck({
										checkboxClass: 'icheckbox_square-blue',
										increaseArea: '20%' // optional
									});
								},
								yes: function(index, layer) {
									var inputs=$("#editrights", parent.document).find("input");
									var rightsAction='';
									inputs.each(function (i,v) {
										if($(v).is(':checked')){
											rightsAction+=v.value;
										}
									});
									elem.setAttribute("rightsAction",rightsAction);
									top.layer.close(index);
								},
							})
						}
					}
				});
				$("#xtree1").children(".animal").remove();
			}
		});
		//工作台配置的数据
		$.ajax({
			type: "post",
			url: ctx + "/dashboard/queryRoleDashboardModel.action",
			data: {
				roleId: id
			},
			success: function(data) {
				var right = JSON.parse(data);
				xtree2 = new layuiXtree({
					elem: 'xtree2',
					form: form,
					data: right,
					isopen: true, //加载完毕后的展开状态，默认值：true
					ckall: false, //启用全选功能，默认值：false	
					ckallback: function() {}, //全选框状态改变后执行的回调函数
					icon: { //三种图标样式，更改几个都可以，用的是layui的图标
						open: "&#xe7a0;", //节点打开的图标
						close: "&#xe622;", //节点关闭的图标
						end: "&#xe621;" //末尾节点的图标
					},
					color: { //三种图标颜色，独立配色，更改几个都可以
						open: "#EE9A00", //节点图标打开的颜色
						close: "#EEC591", //节点图标关闭的颜色
						end: "#828282" //末级节点图标的颜色
					},
				});
				$("#xtree2").children(".animal").remove();
			}
		});

	});

	//人员配置
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.user.id,
	});
	colOpts.push({
		"data": "name",
		"title": biolims.common.sname,
	});
	colOpts.push({
		"data": "department-name",
		"title": biolims.equipment.departmentName,
	});

	colOpts.push({
		"data": "dicJob-name",
		"title": biolims.user.post,
	});
	var handlemethod = $("#handlemethod").val();

	if(handlemethod != "view") {
		tbarOpts.push({
			text: '<i class="fa fa-yelp"></i>添加明细',
			action: addUser
		});

		tbarOpts.push({
			text: '删除选中',
			action: function() {
				removeChecked($("#rolesTable"),
					"/core/role/delRoleUser.action", "角色管理人员配置删除：", id);
			}
		});

	}
	var sampleInfoOptions = table(true,
		"",
		'/core/role/showRoleUserListJson.action?roleId=' + id, colOpts, tbarOpts)
	myTable = renderData($("#rolesTable"), sampleInfoOptions);

})

// 点击上一步下一步切换
function stepViewChange() {
	$(".step").click(function() {
		var index = $(".wizard_steps .step").index($(this));
		$(".wizard_steps .selected").removeClass("selected")
			.addClass("done");
		$(this).removeClass("done").addClass("selected");
		$(".HideShowPanel").eq(index).slideDown().siblings(
			'.HideShowPanel').slideUp();
		//table刷新和操作隐藏
		if(index == 0) {
			$(".box-tools").hide();
		} else {
			tableRefresh();
			$(".box-tools").show();
		}
	});
}
//保存
function save() {
	//必填验证
	var requiredField=requiredFilter();
		if(!requiredField){
			return false;
		}
	if($("#handlemethod").val() == "modify") {
		var load = top.layer.load(4, {
			shade: 0.3
		});
		saveMain(load)
	} else {
		//先查角色编号是否存在了
		$.ajax({
			type: "post",
			url: ctx + '/common/hasId.action',
			data: {
				id: $("#id").val(),
				obj: 'Role'
			},
			success: function(data) {
				var data = JSON.parse(data);
				if(data.message) {
					top.layer.msg(data.message);
				} else {
					var load = top.layer.load(4, {
						shade: 0.3
					});
					saveMain(load)
					$("#handlemethod").val("modify");
					$("#id").attr('readonly','true')
				}
			}
		});
	}
}
//大保存
function saveMain(load) {
	//执行保存操作
	var mainform = $("#form1").serializeArray();
	var main = {}
	mainform.forEach(function(v, i) {
		main[v.name] = v.value;
	});

	var oCks1 = xtree1.GetAllCheckBox(); //这是方法
	var rights = []
	for(var i = 0; i < oCks1.length; i++) {
		if(oCks1[i].checked) {
			var obj = {}
			obj.val = oCks1[i].value;
			var rightsAction = oCks1[i].getAttribute("rightsAction");
			if(rightsAction != "undefined") {
				obj.rightsAction = rightsAction;
			}
			rights.push(obj);
		}
	}
	var oCks2 = xtree2.GetAllCheckBox(); //这是方法
	var dashboard = []
	for(var i = 0; i < oCks2.length; i++) {
		if(oCks2[i].checked) {
			var objj = {}
			objj.val = oCks2[i].value;
			dashboard.push(objj);
		}
	}
	
	var changeLog = "角色列表-";
	$('input[class="form-control"]').each(function(i, v) {
		var valnew = $(v).val();
		var val = $(v).attr("changelog");
		if(val !== valnew) {
			changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
		}
	});
	$.ajax({
		type: "post",
		url: ctx + "/core/role/save.action",
		data: {
			id: $("#id").val(),
			main: JSON.stringify(main),
			rights: JSON.stringify(rights),
			dashboard: JSON.stringify(dashboard),
			changeLog:changeLog
		},
		success: function(data) {
			var data = JSON.parse(data);
			if(data.success) {
				top.layer.close(load);
				top.layer.msg("保存成功！");
			}
		}
	});

}
//添加明细
function addUser() {
	top.layer.open({
		title: "选择人员",
		type: 2,
		area: ["65%", "70%"],
		btn: biolims.common.selected,
		content: [window.ctx + "/core/user/selectUsersTable.action", ''],
		yes: function(index, layer) {
			var chosed = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .selected");
			var user = [];
			chosed.each(function(i, tr) {
				user.push($(tr).children("td").eq(1).text());
			});
			$.ajax({
				type: "post",
				data: {
					userId: user,
					roleId: $("#id").val()
				},
				url: ctx + "/core/role/saveRoleUser.action",
				success: function(data) {
					var data = JSON.parse(data);
					if(data.success) {
						tableRefresh();
					}
				}
			});
			top.layer.close(index)
		},
	})
}

//待办事项模块
function dbsy() {
	top.layer.open({
		title: biolims.order.dbsx,
		type: 2,
		area: ["60%", "65%"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/type/dicTypeSelectTables.action?flag=dbsx", ''],
		yes: function(index, layer) {
			var name = [],
				id = [];
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .selected").each(function(i, v) {
				name.push($(v).children("td").eq(2).text());
				id.push($(v).children("td").eq(1).text());
			});
			top.layer.close(index)
			$("#role_modelId").val(id.join(","));
			$("#role_modelName").val(name.join(","));
		},
	})
}
//异常处理模块
function yccl() {
	top.layer.open({
		title: biolims.order.ycxx,
		type: 2,
		area: ["60%", "65%"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/type/dicTypeSelectTables.action?flag=ycxx", ''],
		yes: function(index, layer) {
			var name = [],
				id = [];
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .selected").each(function(i, v) {
				name.push($(v).children("td").eq(2).text());
				id.push($(v).children("td").eq(4).text());
			});
			top.layer.close(index)
			$("#role_abnormalId").val(id.join(","));
			$("#role_abnormalName").val(name.join(","));
		},
	})
}

//新建
function add() {
	window.location = window.ctx + '/core/role/toEditRole.action';
}
//列表
function list() {
	window.location = window.ctx + '/core/role/showRoleList.action';
}