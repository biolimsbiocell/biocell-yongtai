var showBacklogListGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.ModuleId,
		width:20*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.applicationName,
		width:20*10,
		hidden:false,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/core/role/showBacklogListJson.action?type="+$("#type").val();
	var opts={};
	opts.title=biolims.common.selectModule;
	opts.width = 450;
	opts.height = 400;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
//		$('#selectId').val(id);
//		window.parent.setTechJkServiceTaskFun(rec);
//		setUserGroupUser();
	};
	showBacklogListGrid=gridTable("showBacklogListDiv",cols,loadParam,opts);
	$("#showBacklogListDiv").data("showBacklogListGrid", showBacklogListGrid);
});