/* 
 * 文件名称 :
 * 创建者 : 郭恒开
 * 创建日期: 2018/04/17
 * 文件描述: 文档管理子表
 * 
 */
$(function() {
	var id = $("#id").val();
	//机构用户
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push({
		"data": "documentInfo-id",
		"title": biolims.documentManagement.documentId,
		"createdCell": function(td) {
			$(td).attr("saveName", "documentInfo-id");
		}
	});
	colOpts.push({
		"data": 'fileName',
		"title": biolims.documentManagement.documentName,
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "fileName");
		}
	});
	colOpts.push({
		"data": "fileType",
		"title": biolims.documentManagement.documentType,
		"className": "select",
		"createdCell": function(td) {
			$(td).attr("saveName", "fileType");
			$(td).attr("selectOpt", "word|zip|txt|excel");
		},
		"name": "word|zip|txt|excel",
	});
	colOpts.push({
		"data": 'uploadTime',
		"title": biolims.crm.fileUploadDate,
		"className": "date",
		"createdCell": function(td) {
			$(td).attr("saveName", "uploadTime");
		}
	});
	colOpts.push({
		"data": 'versionNo',
		"title": biolims.documentManagement.versionNumber,
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "versionNo");
		}
	});
	colOpts.push({
		"data": 'attach-fileName',
		"title": biolims.common.attachment,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "attach-fileName");
			$(td).attr("attach-id", rowData['attach-id']);
		}
	});
	if(handlemethod != "view") {

		tbarOpts.push({
			text: biolims.common.fillDetail,
			action: function() {
				addItem($("#documentTable"))

			}
		});
		tbarOpts.push({
			text: biolims.common.delSelected,
			action: function() {
				removeChecked($("#documentTable"),
					"/system/sample/sampleOrder/delSampleOrderItemTable.action", "删除文档：", id);
			}
		});
		tbarOpts.push({
			text: biolims.common.uploadAttachment,
			action: function() {
				var rows = $("#documentTable").find("tbody .selected");
				var length = rows.length;
				if(!length || length > 1) {
					top.layer.msg(biolims.common.pleaseSelectAPieceOfData);
					return false;
				}
				$("#uploadCsv").modal("show");
				//上传附件
				var fileInput = $("#uploadCsvVal").fileinput({
					uploadUrl: ctx + "/attachmentUpload", //上传的地址
					uploadAsync: true,
					language: 'zh',
					//showPreview: false,
					enctype: 'multipart/form-data',
					elErrorContainer: '#kartik-file-errors',
					//allowedFileExtensions: ["csv", "xls", "xlsx"],
					//uploadExtraData: data
				});
				fileInput.on("fileuploaded", function(event, data, previewId, index) {
					$("#uploadCsv").modal("hide");
					var fileId = data.response.fileId;
					var fileName = data.files[0].name;
					rows.children("td[savename='attach-fileName']").attr("attach-id",fileId).text(fileName);
				});
			}
		});
		tbarOpts.push({
			text: biolims.documentManagement.downloadAttachment,
			action: function() {
				var rows = $("#documentTable").find("tbody .selected");
				var length = rows.length;
				if(!length || length > 1) {
					top.layer.msg(biolims.common.pleaseSelectAPieceOfData);
					return false;
				}
				var fileId=rows.children("td[savename='attach-fileName']").attr("attach-id");
				window.open(ctx+"/operfile/downloadById.action?id="+fileId);
			}
		});
		tbarOpts.push({
			text: biolims.common.save,
			action: function() {
				saveItem($("#documentTable"));
			}
		});
	}
	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "modify") {
		$("#dif_fileName").prop("readonly", "readonly");
	}
	var sampleInfoOptions = table(true, id,
		'/document/documentInfo/showDocumentInfoItemListJson.action', colOpts, tbarOpts)
	myTable = renderData($("#documentTable"), sampleInfoOptions);

})
//保存
function save() {
	var changeLog = "";
	$('input[class="form-control"]').each(function(i, v) {
		var valnew = $(v).val();
		var val = $(v).attr("changelog");
		if(val !== valnew) {
			changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
		}
	});
	//必填验证
	var requiredField=requiredFilter();
		if(!requiredField){
			return false;
		}
	document.getElementById("changeLog").value = changeLog;
	var index=top.layer.load(4, {
		shade: 0.3
	});
	$("#form1").attr("action", ctx + "/document/documentInfo/save.action");
	$("#documentInfoItemJson").val(saveItemjson($("#documentTable")));
	$("#form1").submit();
	top.layer.close(index);
}
// 保存
function saveItem(ele) {
	var id=$("#id").val();
	if(!id){
		top.layer.msg("请先保存主表信息！");
		return false;
	}
	var data = saveItemjson(ele);
	var changeLog = biolims.documentManagement.documentManagementItem+"：";
	changeLog = getChangeLogCrmLinkManItem(data, ele, changeLog);
	$.ajax({
		type: 'post',
		url: '/document/documentInfo/saveItem.action',
		data: {
			id: id,
			dataJson: data,
			changeLog:changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			if(k == "attach-fileName") {
				json["attach-id"] = $(tds[j]).attr("attach-id");
				continue;
			}
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
//选择上机组织
function choseParent() {
	top.layer.open({
		title: biolims.documentManagement.chooseSuperior,
		type: 2,
		area: ["60%", "65%"],
		btn: biolims.common.selected,
		content: window.ctx +
			"/document/documentInfo/showDocumentInfoItemTree.action",
		yes: function(index, layer) {
			var name = [],
				id = [];
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#itemTreeGrid .chosed").each(function(i, v) {
				name.push($(v).children("td").eq(1).text());
				id.push($(v).find("input").val());
			});
			if(name.length != 1 || id.length != 1) {
				top.layer.msg(biolims.common.pleaseSelectAPieceOfData);
			} else {
				if(id.join(",") == $("#id").val()) {
					top.layer.msg(biolims.common.organizationRechoose);
				} else {
					$("#dif_parent_name").val(name.join(","));
					$("#dif_parent_id").val(id.join(","));
					top.layer.close(index)
				}
			}
		}
	})
}
//选择上传人
function choseUpPeople() {
	top.layer.open({
		title: biolims.documentManagement.chooseUploadThePerson,
		type: 2,
		area: ["60%", "65%"],
		btn: biolims.common.selected,
		content: [window.ctx + "/core/user/selectUserTable.action?groupId=XXGL", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$("#dif_uploadUser_id").val(id)
			$("#dif_uploadUser_name").val(name)
		},
	})
}
//选择研究方向
function choseYJ() {
	top.layer.open({
		title: biolims.documentManagement.choiceResearchDirection,
		type: 2,
		area: ["60%", "65%"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=yjfx", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$("#dif_studyDirection_id").val(id);
			$("#dif_studyDirection_name").val(name);
		},
	})
}

//新建
function add() {
	window.location = window.ctx + '/document/documentInfo/editDocumentInfo.action';
}
//列表
function list() {
	window.location = window.ctx + '/document/documentInfo/showDocumentInfoTree.action';
}

function getChangeLogCrmLinkManItem(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '编号为"' + v.id + '":';
		crmLinkManItemChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}