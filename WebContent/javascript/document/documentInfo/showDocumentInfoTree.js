/* 
 * 文件名称 :
 * 创建者 : 郭恒开
 * 创建日期: 2018/04/16
 * 文件描述: 组织机构的treeGrid操作
 * 
 */

var options = {
	target: $("#mytreeGrid"),
	url: ctx + "/document/documentInfo/showDocumentInfoTreeJson.action",
	data: {
		upId: ""
	},
	columns: [{
			title: biolims.documentManagement.documentName,
			field: 'fileName',
		},

		{
			title: biolims.documentManagement.documentDescription,
			field: 'contentNote',
		},

		{
			title: biolims.documentManagement.documentType,
			field: 'fileType',
		},
		{
			title: biolims.common.uploadPerson,
			field: 'uploadUser-name',
		},
		//
		//		{
		//			title: '上传时间',
		//			field: 'uploadTime',
		//		}
	]
}
$(document).ready(function() {
	var treegridIndex = 0;
	var target = options.target;
	$.ajax({
		type: "post",
		url: options.url,
		data: options.data,
		success: function(data) {
			var data = JSON.parse(data);
			//构造表头
			var thr = $('<tr><th style="padding:10px;width: 180px;"></th></tr>');
			$.each(options.columns, function(i, item) {
				var th = $('<th style="padding:10px;"></th>');
				th.text(item.title);
				thr.append(th);
			});
			var thead = $('<thead></thead>');
			thead.append(thr);
			target.append(thead);
			//构造表体
			var tbody = $('<tbody></tbody>');
			$.each(data, function(i, item) {
				treegridIndex++;
				if(item.subNode == "yes") {
					var tr = $('<tr class="treegrid-' + treegridIndex + '" parent="parent-0"><td><span class="glyphicon glyphicon-chevron-right" style="padding-left:0;"></span>&nbsp;&nbsp;&nbsp;<input type="checkbox" class="icheck"  value="' + item.id + '"/></td></tr>');
				} else {
					var tr = $('<tr class="treegrid-' + treegridIndex + '" parent="parent-0"><td>&nbsp;&nbsp;&nbsp;<input type="checkbox" class="icheck"  value="' + item.id + '" style="margin-left:18" /></td></tr>');
				}
				$.each(options.columns, function(index, column) {
					var td = $('<td></td>');
					td.text(item[column.field]);
					tr.append(td);
				});
				tbody.append(tr);
			});

			target.append(tbody);
			//多选框格式化
			var ichecks = target.find('.icheck');
			ichecks.iCheck({
				checkboxClass: 'icheckbox_square-blue',
				increaseArea: '20%' // optional
			});
			ichecks.on('ifChanged', function() {
				if($(this).is(':checked')) {
					$(this).parents("tr").addClass("chosed");
				} else {
					$(this).parents("tr").removeClass("chosed");
				}
			});
			openItem(treegridIndex);
		}
	});

})

function openItem(treegridIndex) {
	console.log(treegridIndex);
	$(".glyphicon-chevron-right").unbind("click").click(function() {
		var paddLeft = $(this).css("padding-left");
		var parentIndex = $(this).parent("td").parent("tr").attr("class").split("-")[1];
		$(this).removeClass("glyphicon-chevron-right").addClass("glyphicon-chevron-down");
		if($("tr[parent='parent-" + parentIndex + "']").length) { //有数据，展开就行
			$("tr[parent='parent-" + parentIndex + "']").show();
			openSon($("tr[parent='parent-" + parentIndex + "']"));
		} else { //没数据，异步加载
			var upId = $(this).parent("td").find("input").val();
			var nodetr = $(this).parent("td").parent("tr");
			var load = top.layer.load(4);
			for(k in options.data) {
				options.data[k] = upId;
			}
			$.ajax({
				type: "post",
				data: options.data,
				url: options.url,
				success: function(data) {
					var data = JSON.parse(data);
					for(var i = 0; i < data.length; i++) {
						treegridIndex++;
						var tr = $("<tr class='treegrid-" + treegridIndex + "' parent='parent-" + parentIndex + "'></tr>");
						var tds = "";
						var paddMore = parseInt(paddLeft) + 15;
						var paddMmore = parseInt(paddLeft) + 30;
						if(data[i].subNode == "yes") {
							tds += "<td><span class='glyphicon glyphicon-chevron-right' style='padding-left:" + paddMore + "'></span>&nbsp;&nbsp;&nbsp;<input type='checkbox' class='icheck' value='" + data[i].id + "'/></td>";
						} else {
							tds += "<td><span style='padding-left:" + paddMmore + "'></span>&nbsp;&nbsp;&nbsp;<input type='checkbox' class='icheck' value='" + data[i].id + "'/></td>";
						}
						$.each(options.columns, function(index, column) {
							tds += "<td>" + data[i][column.field] + "</td>";
						});
						nodetr.after(tr.append(tds));
						nodetr = tr;
						//多选框格式化
						var ichecks = tr.find('.icheck');
						ichecks.iCheck({
							checkboxClass: 'icheckbox_square-blue',
							increaseArea: '20%' // optional
						});
						ichecks.on('ifChanged', function() {
							if($(this).is(':checked')) {
								$(this).parents("tr").addClass("chosed");
							} else {
								$(this).parents("tr").removeClass("chosed");
							}
						});
					}
					top.layer.close(load);
					openItem(treegridIndex);
				}
			});

		}
		$(".glyphicon-chevron-down").unbind("click").click(function() {
			console.log("xxxxx");
			$(this).removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-right");
			var parentIndex = $(this).parent("td").parent("tr").attr("class").split("-")[1];
			$("tr[parent='parent-" + parentIndex + "']").hide();
			closeSon($("tr[parent='parent-" + parentIndex + "']"))
			openItem(treegridIndex);
		});

	});
}

function closeSon(ele) {
	ele.each(function(i, v) {
		if($(v).find(".glyphicon").length) {
			var sonIndex = $(this).attr("class").split("-")[1];
			console.log(sonIndex);
			$("tr[parent='parent-" + sonIndex + "']").hide();
			closeSon($("tr[parent='parent-" + sonIndex + "']"))
		}
	})
}

function openSon(ele) {
	ele.each(function(i, v) {
		var sonIndex = $(this).attr("class").split("-")[1];
		$("tr[parent='parent-" + sonIndex + "']").show();
		openSon($("tr[parent='parent-" + sonIndex + "']"));
	})
}

//新建
function add() {
	window.location = window.ctx+'/document/documentInfo/editDocumentInfo.action?reqMethodType=add';
}
//编辑
function edit() {
	var id = $(".chosed").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/document/documentInfo/editDocumentInfo.action?reqMethodType=modify&id=' + id;
}
//查看
function view() {
	var id = $(".chosed").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/document/documentInfo/editDocumentInfo.action?reqMethodType=view&id=' + id;
}