var documentInfoItemGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'fileName',//文件名称
		type : "string"
	});
	fields.push({
		name : 'fileType',//文件类型 word，excel
		type : "string"
	});
	fields.push({
		name : 'uploadTime',//上传时间
		type : "date",
		dateFormat : "Y-m-d"
	});
	fields.push({
		name : 'uploadUser-id',//上传人
		type : "String"
	});
	fields.push({
		name : 'uploadUser-name',//上传人
		type : "String"
	});
	
	fields.push({
		name : 'versionNo',//版本号
		type : "String"
	});
	
	//文件上传
	fields.push({
		name : 'attach-fileName',
		type : "string"
	});
	fields.push({
		name : 'attach-id',
		type : "string"
	});
	
	//文档
	fields.push({
		name : 'documentInfo-id',
		type : "string"
	});
	
	

	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		hidden : true,
		header : '文件编号',
		width : 15 * 10
	});
	
	cm.push({
		dataIndex : 'documentInfo-id',
		hidden : true,
		header : '文档编号',
		width : 15 * 10
	});
	
	cm.push({
		dataIndex : 'fileName',
		hidden : false,
		header : '文件名称',
		width : 15 * 10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	var storeGoodCob = new Ext.data.ArrayStore({
		fields : ['value','name'],
		data : [ ['word','word'], ['zip','zip'],['txt','txt'],['excel','excel']]
	});
	var goodCob = new Ext.form.ComboBox({
		store : storeGoodCob,
		valueField : 'value',
		displayField : 'name',
		mode : 'local'
	});
	cm.push({
		dataIndex : 'fileType',
		hidden : false,
		header : '文件类型',
		width : 15 * 10,
		editor : goodCob,
		renderer : Ext.util.Format.comboRenderer(goodCob)
	});
	var testItemloadUser =new Ext.form.TextField({
        allowBlank: true
	});
	testItemloadUser.on('focus', function() {
		showUserSelectList();
	});
	cm.push({
		dataIndex : 'uploadUser-id',
		hidden : true,
		header : '上传人ID',
		width : 15 * 10
	});
	cm.push({
		dataIndex : 'uploadUser-name',
		hidden : false,
		header : '上传人姓名',
		width : 15 * 10,
		editor :testItemloadUser
	});
	
	
	cm.push({
		dataIndex : 'uploadTime',
		hidden : false,
		header : '上传时间',
		width : 15 * 10,
		renderer : formatDate,
		editor : new Ext.form.DateField({
			format : 'Y-m-d',
			value: new Date()
		})
	});
	
	
	cm.push({
		dataIndex : 'versionNo',
		hidden : false,
		header : '版本号',
		width : 15 * 10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

		
	cm.push({
		dataIndex : 'attach-fileName',
		header : biolims.common.attachment,
		width : 200,
		hidden : false
	});
	
	cm.push({
		dataIndex : 'attach-id',
		header : biolims.report.reportFileId,
		width : 200,
		hidden : true
	});
	
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx
			+ "/document/documentInfoItem/showDocumentInfoItemListJson.action?id="+$("#dif_id").val();
	var opts = {};
	opts.title = "上传文件";
	opts.height = document.body.clientHeight + 60;
	opts.tbar = [];
//	opts.delSelect = function(ids) {
//		ajax("post", "/crm/customer/patient/delCrmPatientDiagnosis.action", {
//			ids : ids
//		}, function(data) {
//			if (data.success) {
//				crmPatientRestsGrid.getStore().commitChanges();
//				crmPatientRestsGrid.getStore().reload();
//				message("删除成功！");
//			} else {
//				message("删除失败！");
//			}
//		}, null);
//	};
/*	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : function() {
			
			var selectRecord = fileInfoUpGrid.getSelectRecord();
			if (selectRecord && selectRecord.length > 0) {
				var vals = "";
				$.each(selectRecord, function(i, obj) {
					vals = vals + "'" + obj.get("id") + "',";
				});
				vals = vals.substring(0, vals.length - 1);
				ajax("post", "/crm/customer/patient/delModelId.action", {
					objName : "CrmPatientDiagnosis",
					objProperty : "id",
					vals : vals
				}, function(data) {
					if (data.success) {
						fileInfoUpGrid.getStore().commitChanges();
						fileInfoUpGrid.getStore().reload();
						message(biolims.common.deleteSuccess);
					} else {
						message(biolims.common.deleteFailed);
					}
				}, null);
			} else {
				message(biolims.common.pleaseChoose);
			}
		}
	});*/

	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	
	opts.tbar.push({
		text : biolims.report.uploadReportTemplate,
		handler : function() {
			var isUpload = true;
			var record = documentInfoItemGrid.getSelectOneRecord();
			load("/system/template/template/toSampeUpload.action", { // 是否修改
				fileId : record.get("attach-id"),
				isUpload : isUpload
			}, null, function() {
				$("#upload_file_div").data("callback", function(data) {
					record.set("attach-fileName", data.fileName);
					record.set("attach-id", data.fileId);
				});
			});
		}
	});
	
	opts.tbar.push({
		text : biolims.common.save,
		handler : function() {
			var result = commonGetModifyRecords(documentInfoItemGrid);
			if (result.length > 0) {
				ajax("post", "/document/documentInfo/saveItem.action", {
					itemDataJson : result,
					id : $("#dif_id").val()
				}, function(data) {
					if (data.success=="true") {
						message(biolims.common.saveSuccess);
						documentInfoItemGrid.getStore().commitChanges();
						documentInfoItemGrid.getStore().reload();
					}else if(data.success=="error"){
						message('请先保存文档信息，再保存上传附件信息！');
					}else {
						message(biolims.common.saveFailed);
					}
				}, null);
			}
		}
	});
	
	opts.tbar.push({
		text : '下载附件',
		handler : function() {
			var record = documentInfoItemGrid.getSelectOneRecord();
			downFile(record.get("attach-id"));
		}
	});
	documentInfoItemGrid = gridEditTable("documentInfoItemdiv", cols,
			loadParam, opts);
	$("#documentInfoItemdiv").data("documentInfoItemGrid",
			documentInfoItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//上传人
function showUserSelectList(){
	var win = Ext.getCmp('showUserSelectListFun');
	if (win) {
		win.close();
	}
	var showUserSelectListFun = new Ext.Window(
			{
				id : 'showUserSelectListFun',
				modal : true,
				title : '选择人员',
				layout : 'fit',
				width : document.body.clientWidth / 1.3,
				height : document.body.clientHeight / 1.5,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"
									+ ctx
									+ "/core/user/showUserListAllSelect.action' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text: biolims.common.close,
					handler : function() {
						showUserSelectListFun.close();
					}
				} ]
			});
	showUserSelectListFun.show();
}

var testItemdocumentInfo;	
//上级位置
function testItemdocumentInfo(){
	 var options = {};
		options.width = document.body.clientWidth-800;
		options.height = document.body.clientHeight-40;
		testItemdocumentInfo=loadDialogPage(null, biolims.common.sampleType, "/sample/dicSampleType/dicSampleTypeSelect.action?a=2", {
			"Confirm" : function() {
				var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = sangerTaskResultGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("dicSampleType-id", b.get("id"));
							obj.set("dicSampleType-name", b.get("name"));
						});
					});
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}
function downFile(id){
	window.open(window.ctx + '/operfile/downloadById.action?id='+id,'','');
}

function addUserGrid(record) {
	var expPersToGrid = $("#documentInfoItemdiv").data("documentInfoItemGrid");
	var selRecords = expPersToGrid.getSelectionModel().getSelections(); 
	var selRecord=selRecords[0];
	
	
    var userIds = "";
    var userNames = "";
	$.each(record, function(i, obj) {
		userIds += obj.get("id")+",";
	    userNames += obj.get("name")+",";
	});
	userIds = userIds.substring(0,userIds.length-1);
	userNames = userNames.substring(0,userNames.length-1);
	selRecord.set("uploadUser-id",userIds);
	selRecord.set("uploadUser-name",userNames);
	var win = Ext.getCmp('showUserSelectListFun');
	if(win){win.close();}
}