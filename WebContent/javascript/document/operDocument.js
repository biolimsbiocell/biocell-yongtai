var classData= [];
		var swfu ;
		function getSwfl(){
			var settings_object = {
			  upload_url : ctx+"/operfile/upload.action?modelType="+document.getElementById("modelType").value+"&picUserId="+document.getElementById("picUserId").value+"&useType="+document.getElementById("useType").value,
			  flash_url : ctx+"/javascript/lib/swfupload.swf",
			  file_post_name: "file",
			  file_size_limit : "0" ,
			  //post_params :reqParams,
			  button_image_url : ctx+"/images/sel_file_button.jpg",
			  button_placeholder_id : "uploadButton",
			  button_width: 75,
			  button_height: 23,
			 
			  file_dialog_start_handler : Application.uploadDialog.fileDialogStart,
			  file_queued_handler : fileQueued,		
			  file_queue_error_handler : Application.uploadDialog.uploadError,
			  file_dialog_complete_handler : Application.uploadDialog.fileDialogComplete,
		      upload_start_handler : Application.uploadDialog.uploadFileStar,
			  upload_progress_handler : Application.uploadDialog.uploadProgress,
		      upload_error_handler : Application.uploadDialog.uploadError,
		     
			  file_complete_handler : Application.uploadDialog.uploadFileComplete,
			  upload_success_handler : Application.uploadDialog.uploadQueueComplete,
			 
			  ui_container_id : "SWFUploadTarget",
			  degraded_container_id : "divDegraded",
			  debug: false
			};
		 swfu = new SWFUpload(settings_object);
		}
		
		function fileQueued(file){
			Application.uploadDialog.show(classData,swfu);
			obj=Application.uploadDialog;
			var filetype=(file.type.substr(1)).toUpperCase();
			//if(filetype=='JPG' | filetype=='GIF'){
				var data=[];
				data.push([file.id,'未上传',file.name,file.size,filetype]);
				obj.uploadGrid.store.loadData(data,true);
				obj.uploadAction[1].enable();
				obj.uploadAction[2].enable();
				obj.uploadAction[3].enable();
				//obj.stateInfo.getEl().innerHTML=String.format(obj.statuBarText,obj.uploadGrid.store.getCount(),Ext.util.Format.fileSize(obj.uploadGrid.store.sum('size')));
			//}
			
		}

		function getAllSel(){
			var names = document.getElementsByName("sel");
			var ids='';
			if(names){
				for(i=0;i<names.length;i++){
					if(names[i].checked){
						ids = ids+"'"+names[i].value+"',";
					}
				}
				if(ids.length>0){
					ids = ids.substring(0,ids.length-1);
				}
			}
			return ids;
		}
		
		//删除文件
		function delFile(){
			var ids=getAllSel();
			if(ids.length<=0){
				alertMessage("请选择您要删除的文件!");
			}else{
				Ext.MessageBox.confirm("请确认","您确定要删除您所选中的文件吗？",
							function (button,text){
								if(button=='yes'){
									document.getElementById("ids").value=ids;
									document.operForm.action=ctx+"/operfile/del.action";
									document.operForm.submit();
								}
							}

						);
			}
		}

		//下载文件
		function download(){
			var ids=getAllSel();
			
			if(ids.length<=0){
				alertMessage("请选择您要下载的文件!");
			}else{
				document.getElementById("ids").value=ids;
				document.operForm.action=ctx+"/operfile/download.action";
				document.operForm.submit();
			}
		}

		//输出消息
		function alertMessage(msg){
			Ext.MessageBox.show( {
				title : '消息',
				msg : msg,
				width : 300,
				buttons : Ext.MessageBox.OK,
				icon : Ext.MessageBox.WARNING
			});
		}

		function init(){
			//if(userId){
				document.getElementById("operId_selFile").style.display="inline";
				document.getElementById("operId_fileOper").style.display="inline";
				getSwfl();
			///}else{
			///	document.getElementById("operId_selFile").style.display="none";
			///	document.getElementById("operId_fileOper").style.display="none";
			//}
		}
	
		//全选 
		function checkAll(str){
			var all = document.getElementById("all");
			if(all.checked){
				var a= document.getElementsByName(str);
				 var n=a.length;
				 for(var i=0;i<n;i++)
				 a[i].checked = "checked";
			}else{
				var a= document.getElementsByName(str);
				 var n=a.length;
				 for(var i=0;i<n;i++)
				 a[i].checked = "";
			}
		}