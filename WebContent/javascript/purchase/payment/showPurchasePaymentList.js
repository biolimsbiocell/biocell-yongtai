

function add() {
	window.location = window.ctx
			+ "/purchase/payment/toEditPurchasePayment.action";
}
function edit() {
	var id = "";
	id = document.getElementById("id").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx
			+ '/purchase/payment/toEditPurchasePayment.action?id=' + id;
}
function view(){
	var id="";
	id = document.getElementById("id").value;
	if(id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location= window.ctx +'/purchase/payment/toViewPurchasePayment.action?id='+id;
}
$(function() {

	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
				commonSearchAction(gridGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, false, option);

	});

});