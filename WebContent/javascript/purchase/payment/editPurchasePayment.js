function list() {
	window.location = window.ctx + '/purchase/payment/showPurchasePaymentList.action';
}

function add() {
	window.location = window.ctx + "/purchase/payment/toEditPurchasePayment.action";
}

function newSave() {
	save();
}
function save() {

	if (checkSubmit() == true) {
		Ext.MessageBox.show({
			msg : '正在保存数据,请等待...',
			progressText : '保存中...',
			width : 300,
			wait : true,
			icon : 'ext-mb-download'
		});

		document.getElementById('jsonDataStr').value = commonGetModifyRecords(gridGrid);
		document.getElementById('jsonDataStr1').value = commonGetModifyRecords(orderGrid);
		document.getElementById('jsonDataStr2').value = commonGetModifyRecords(invoiceGrid);
		document.getElementById('paymentForm').action = window.ctx + "/purchase/payment/save.action";
		document.getElementById('paymentForm').submit();
	} else {
		return false;
	}
}
function checkSubmit() {

	var mess = "";
	var fs = [ "purchasePayment_id" ];
	var nsc = [ "编号不能为空！"];

	mess = commonFieldsNotNullVerifyById(fs, nsc);
	if (mess != "") {
		message(mess);
		return false;
	}
	return true;
}
var scale = new Ext.form.NumberField({
	allowBlank : true,
	allowNegative : false,
	maxValue : 100000
});
var purchaseOrder = new Ext.form.TextField({
	allowBlank : true,
	maxLength : 32
});
purchaseOrder.on('focus', function() {
	searchOrderFun();
});
var fee = new Ext.form.NumberField({
	allowBlank : true,
	allowNegative : false,
	maxValue : 100000
});
var payFee = new Ext.form.NumberField({
	allowBlank : true,
	allowNegative : false
});

var payDate = new Ext.form.DateField({

});

var invoiceCode = new Ext.form.TextField({
	allowBlank : true,
	maxLength : 50
});
var payCondition = new Ext.form.TextArea();

function viewSupplier() {
	if (trim(document.getElementById('purchasePayment_purchaseOrder_supplier_id').value) == '') {
		message("请选择一条记录!");
	} else {
		openDialog(window.ctx + '/supplier/toViewSupplier.action?id='
				+ document.getElementById('purchasePayment_purchaseOrder_supplier_id').value);

	}
}
function workflowSubmit() {
	commonWorkflowSubmit("applicationTypeTableId=purchasePayment&formId=" + $("#purchasePayment_id").val() + "&title="
			+ encodeURI(encodeURI('${purchasePayment.note}')));
}
function workflowLook() {
	commonWorkflowLook("applicationTypeTableId=purchasePayment&formId=" + $("#purchasePayment_id").val());
}
function workflowView() {
	commonWorkflowView("applicationTypeTableId=purchasePayment&formId=" + $("#purchasePayment_id").val());
}
function changeState() {
	commonChangeState("formId=" + $("#purchasePayment_id").val() + "&tableId=purchasePayment");
}
function editCopy() {
	editCommonCopy('${ctx}/purchase/payment/toEditPurchasePayment.action?id=' + $("#purchasePayment_id").val());
}
Ext.onReady(function() {
	var tabs = new Ext.TabPanel({
		renderTo : 'maintab',
		height : document.body.clientHeight-30,
		autoWidth : true,
		activeTab : 0,
		margins : '0 0 0 0',
		items : [ {
			title : '付款信息',
			contentEl : 'markup'

		} ]
	});
});
$(function() {
	var handlemethod = $("#handlemethod").val();
	var copyMode = $("#copyMode").val();
	if ('modify' == handlemethod) {

		var t = "#purchasePayment_id";
		settextreadonlyById(t);

	}
	if ('view' == handlemethod) {
		settextreadonlyByAll();

	}
	if ('true' == copyMode) {
		settextread("purchasePayment_id");
		document.getElementById("purchasePayment_id").value = "";
		document.getElementById("purchasePayment_state").value = "3";
		document.getElementById("purchasePayment_stateName").value = "新建";
		setTimeout("firstCommonValue(gridGrid)", 1000);

	}
});