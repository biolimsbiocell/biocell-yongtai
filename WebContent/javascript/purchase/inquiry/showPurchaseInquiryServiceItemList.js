$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];

	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'supplier1Price',
		type : "string"
	});
	fields.push({
		name : 'supplier2Price',
		type : "string"
	});
	fields.push({
		name : 'supplier3Price',
		type : "string"
	});
	fields.push({
		name : 'unit',
		type : "string"
	});
	fields.push({
		name : 'serviceCode',
		type : "string"
	});
	fields.push({
		name : 'serviceName',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];

	cm.push({
		dataIndex : 'id',
		header : '编号',
		width : 240,
		hidden : true
	});
	cm.push({
		dataIndex : 'serviceCode',
		header : '服务编码',
		width : 180,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})

	});
	cm.push({
		dataIndex : 'serviceName',
		header : '服务描述',
		width : 200,
		sortable : true,
		editor : new Ext.form.TextArea({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'unit',
		header : '单位',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'supplier1Price',
		header : '供应商1价格',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'supplier2Price',
		header : '供应商2价格',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'supplier3Price',
		header : '供应商3价格',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'note',
		header : '备注',
		width : 240,
		sortable : true,
		editor : new Ext.form.TextArea({
			allowBlank : true
		})
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/purchase/inquiry/showPurchaseInquiryServiceItemListJosn.action?id=" + $("#task_id").val();
	// loadParam.limit = 10
	var opts = {};
	opts.title = "服务询价明细";
	opts.height = document.documentElement.clientHeight + 425;
	opts.tbar = [];
	opts.delSelect = function(ids) {
		var id;
		if (ids) {
			id = "'" + ids.join("','") + "'";
		}
		ajax("post", "/purchase/inquiry/delPurchaseInquiryServiceItemList.action", {
			ids : id
		}, function(data) {
			if (data.success) {
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : "显示可编辑列",
		handler : null
	});
	opts.tbar.push({
		text : "取消选中",
		handler : null
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != 'view') {
		/* 以下是删除程序 */

	} else {
		opts.tbar.push({
			text : "删除选中",
			handler : null
		});
		opts.tbar.push({
			text : "填加明细",
			handler : null
		});
	}

	var setPurchaseInquiryServiceItem = function() {
		ajax("post", "/purchase/inquiry/createPurchaseInquiryServiceItem.action", {
			id : $("#purchaseInquiry_purchaseApply_id").val(),
			sysCode : $("#purchaseInquiry_purchaseApply_type_sysCode").val()
		}, function(data) {
			if (data.success) {
				var result = data.result;
				var ob = purchaseInquiryServiceItemGrid.getStore().recordType;
				purchaseInquiryServiceItemGrid.stopEditing();
				$.each(result, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("serviceCode", obj[0]);
					p.set("serviceName", obj[1]);
					p.set("unit", obj[2]);
					purchaseInquiryServiceItemGrid.getStore().add(p);
				});
				purchaseInquiryServiceItemGrid.startEditing(0, 0);
			} else {
				message("生成服务明细失败！");
			}
		}, null);
	}

	var getPurchaseInquiryServiceItem = function() {
		var planRecord = purchaseInquiryServiceItemGrid.store.getModifiedRecords();
		var itemDataJson = [];
		if (planRecord && planRecord.length > 0) {
			$.each(planRecord, function(i, obj) {
				var data = {};
				data.id = obj.get("id");
				data.supplier1Price = obj.get("supplier1Price");
				data.supplier2Price = obj.get("supplier2Price");
				data.supplier3Price = obj.get("supplier3Price");
				data.note = obj.get("note");
				data["serviceCode"] = obj.get("serviceCode");
				data["serviceName"] = obj.get("serviceName");
				data["unit"] = obj.get("unit");
				itemDataJson.push(data);
			});
		}
		return itemDataJson.length > 0 ? JSON.stringify(itemDataJson) : "";
	}
	$("#purchase_inquiry_service_item_grid_div").data("setPurchaseInquiryServiceItem", setPurchaseInquiryServiceItem);
	
	$("#purchase_inquiry_service_item_grid_div").data("getPurchaseInquiryServiceItem", getPurchaseInquiryServiceItem);

	purchaseInquiryServiceItemGrid = gridEditTable("purchase_inquiry_service_item_grid_div", cols, loadParam, opts);

	$("#purchase_inquiry_service_item_grid_div").data("purchaseInquiryServiceItemGrid", purchaseInquiryServiceItemGrid);

});
