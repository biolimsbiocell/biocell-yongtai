$(function() {
	var plasmaApplyList;
	var cols = {};
	// cols.sm = true;
	var fields = [];

	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'remark',
		type : "string"
	});
	fields.push({
		name : 'type-name',
		type : "string"
	});

	fields.push({
		name : 'applyUser-name',
		type : "string"
	});
	fields.push({
		name : 'applyDate',
		type : "string"
	});

	fields.push({
		name : 'stateName',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];

	cm.push({
		dataIndex : 'id',
		header : '编号',
		width : 140,
		sortable : true
	});
	cm.push({
		dataIndex : 'remark',
		header : '描述',
		width : 200,
		sortable : true
	});
	cm.push({
		dataIndex : 'type-name',
		header : '描述',
		width : 140,
		sortable : true
	});
	cm.push({
		dataIndex : 'applyUser-name',
		header : '申请人',
		width : 120,
		sortable : true
	});
	cm.push({
		dataIndex : 'applyDate',
		header : '申请时间',
		width : 140,
		sortable : true
	});
	cm.push({
		dataIndex : 'stateName',
		header : '状态',
		width : 120,
		sortable : true
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/purchase/inquiry/showPurchaseInquiryListJosn.action";
	// loadParam.limit = 10
	var opts = {};
	opts.title = "询价列表";
	opts.height = document.documentElement.clientHeight+600;
	opts.rowselect = function(id) {
		$("#id").val(id);
	};
	opts.rowdblclick = function(id) {
		$("#id").val(id);
		view();
	};
	purchaseInquiryGird = gridTable("purchase_inquiry_gird_div", cols, loadParam, opts);
	
	var view = function() {
		window.location = window.ctx + '/purchase/inquiry/toEditPurchaseInquiry.action?handlemethod=view&id='
				+ $("#id").val();
	};
	
	$("#toolbarbutton_add").click(function() {
		// alert("新建事件");
		window.location = window.ctx + '/purchase/inquiry/toEditPurchaseInquiry.action?handlemethod=add';
	});
	
	$("#toolbarbutton_modify").click(
			function() {
				// alert("编辑事件");
				var id = $("#id").val();
				if (id) {
					window.location = window.ctx
							+ '/purchase/inquiry/toEditPurchaseInquiry.action?handlemethod=modify&id=' + id;
				} else {
					message("请选择您要修改的记录!");
				}
			});
	
	
	$("#toolbarbutton_scan").click(function() {
		// alert("查看事件");
		if ($("#id").val()) {
			view();
		} else {
			message("请选择您要查看的记录！");
		}

	});
	
	$("#toolbarbutton_export_excel").click(function() {
		showPoolingTaskList.title = '导出列表';
		var vExportContent = showPoolingTaskList.getExcelXml();
		var x = document.getElementById('gridhtm');
		x.value = vExportContent;
		document.excelfrm.submit();
	});
});