$(function() {
	$("#toolbarbutton_add").click(function() {
		window.location = window.ctx + '/purchase/inquiry/toEditPurchaseInquiry.action?reqMethodType=add';
	});

	$("#toolbarbutton_list").click(function() {
		window.location = window.ctx + '/purchase/inquiry/showPurchaseInquiryList.action';
	});

	$("#toolbarbutton_save").click(
			function() {
				var id = $("#purchaseInquiry_id").val();
				if (!id) {
					message("询价编号不能为空！");
					return;
				}
				var id = $("#purchaseInquiry_type_id").val();
				if (!id) {
					message("询价类型不能为空！");
					return;
				}
				var supplierJsonFun = $("#purchase_inquiry_supplier_gird_div").data("getPurchaseInquirySupplier");
				if (typeof supplierJsonFun == 'function') {
					var resultData = supplierJsonFun();
					if (resultData) {
						$("#supplierDataJson").val(resultData);
					}
				}

				var itemJsonFun = $("#purchase_inquiry_item_grid_div").data("getPurchaseInquiryItem");
				if (typeof itemJsonFun == 'function') {
					var resultData = itemJsonFun();
					if (resultData) {
						$("#itemDataJson").val(resultData);
					}
				}

				var equipmentItemJsonFun = $("#purchase_inquiry_equipment_item_grid_div").data(
						"getPurchaseInquiryEquipmentItem");
				if (typeof equipmentItemJsonFun == 'function') {
					var resultData = equipmentItemJsonFun();
					if (resultData) {
						$("#equipmentItemDataJson").val(resultData);
					}
				}

				var serviceItemJsonFun = $("#purchase_inquiry_service_item_grid_div").data(
						"getPurchaseInquiryServiceItem");
				if (typeof serviceItemJsonFun == 'function') {
					var resultData = serviceItemJsonFun();
					if (resultData) {
						$("#serviceItemDataJson").val(resultData);
					}
				}
				$("#purchase_inquiry").attr("action", ctx + "/purchase/inquiry/save.action").submit();
			});

	var sysCode = $("#purchaseInquiry_type_sysCode").val();

	var handlemethod = $("#handlemethod").val();

	if ('view' === handlemethod) {
		setTimeout(function() {
			settextreadonlyByAllJquery();
		}, 500);
	} else {
		if ($("#purchaseInquiry_id").val()) {
			setTimeout(function() {
				settextreadonlyJquery($("#purchaseInquiry_id"));
			}, 500);
		}
		if (sysCode) {
			setTimeout(function() {
				settextreadonlyJquery($("#purchaseInquiry_type_name"));
			}, 500);
		}
	}

	load("/purchase/inquiry/showPurchaseInquirySupplierList.action", null, $("#show_purchase_inquiry_supplier_div"),
			null);
	if ('storage' === sysCode) {
		$("#show_purchase_inquiry_item_div").css("display", "");
		load("/purchase/inquiry/showPurchaseInquiryItemList.action", null, $("#show_purchase_inquiry_item_div"), null);
	} else if ('equipment' === sysCode) {
		$("#show_spurchase_inquiry_equipment_item_div").css("display", "");
		load("/purchase/inquiry/showPurchaseInquiryEquipmentItemList.action", null,
				$("#show_spurchase_inquiry_equipment_item_div"), null);
	} else if ('service' === sysCode) {
		$("#show_purchase_inquiry_service_div").css("display", "");
		load("/purchase/inquiry/showPurchaseInquiryServiceItemList.action", null,
				$("#show_purchase_inquiry_service_div"), null);
	}
	new Ext.TabPanel({
		renderTo : 'maintab',
		height : document.body.clientHeight,
		autoWidth : true,
		activeTab : 0,
		margins : '0 0 0 0',
		items : [ {
			title : '询价管理',
			autoWidth : true,
			contentEl : 'markup'

		} ]
	});
	$("#searchOrder").click(
			function() {
				var title = "选择采购申请单";
				var url = "/purchase/apply/selPurchaseApplyList.action";
				var option = {};
				option.width = 685;
				option.height = 525;
				loadDialogPage(null, title, url, {}, true, option);
				setTimeout(function() {
					$("#sel_purchase_apply_btn").click(
							function() {
								var selPurchaseApplyGrid = $("#sel_purchase_apply_list_gird_div").data(
										"selPurchaseApplyGrid");
								var scRecord = selPurchaseApplyGrid.getSelectRecord();
								if (scRecord && scRecord.length > 0) {
									var record = scRecord[0];
									$("#purchaseInquiry_purchaseApply_id").val(record.get('id'));
									$("#purchaseInquiry_purchaseApply_note").val(record.get('note'));
									$("#purchaseInquiry_purchaseApply_type_sysCode").val(record.get('type-sysCode'));
									if ('storage' === sysCode) {
										ajax("post", "/purchase/inquiry/createPurchaseInquiryItem.action", {
											id : $("#purchaseInquiry_purchaseApply_id").val(),
											sysCode : $("#purchaseInquiry_purchaseApply_type_sysCode").val()
										}, function(data) {
											if (data.success) {
												var result = data.result;
												var purchaseInquiryItemGrid = $("#purchase_inquiry_item_grid_div")
														.data("purchaseInquiryItemGrid");
												var ob = purchaseInquiryItemGrid.getStore().recordType;
												purchaseInquiryItemGrid.stopEditing();
												$.each(result, function(i, obj) {
													var p = new ob({});
													p.isNew = true;
													p.set("storage-id", obj[0]);
													p.set("storage-name", obj[1]);
													p.set("storage-currencyType-name", obj[2]);
													purchaseInquiryItemGrid.getStore().add(p);
												});
												purchaseInquiryItemGrid.startEditing(0, 0);
											} else {
												message("生成库存明细失败！");
											}
										}, null);

									} else if ('equipment' === sysCode) {
										ajax("post", "/purchase/inquiry/createPurchaseInquiryEquipmentItem.action", {
											id : $("#purchaseInquiry_purchaseApply_id").val(),
											sysCode : $("#purchaseInquiry_purchaseApply_type_sysCode").val()
										}, function(data) {
											if (data.success) {
												var result = data.result;
												var purchaseInquiryEquipmentItemGrid = $(
														"#purchase_inquiry_equipment_item_grid_div").data(
														"purchaseInquiryEquipmentItemGrid");
												var ob = purchaseInquiryEquipmentItemGrid.getStore().recordType;
												purchaseInquiryEquipmentItemGrid.stopEditing();
												$.each(result, function(i, obj) {
													var p = new ob({});
													p.isNew = true;
													p.set("equipment-id", obj[0]);
													p.set("equipment-name", obj[1]);
													p.set("equipment-currencyType-name", obj[2]);
													purchaseInquiryEquipmentItemGrid.getStore().add(p);
												});
												purchaseInquiryEquipmentItemGrid.startEditing(0, 0);
											} else {
												message("生成设备明细失败！");
											}
										}, null);

									} else if ('service' === sysCode) {
										ajax("post", "/purchase/inquiry/createPurchaseInquiryServiceItem.action", {
											id : $("#purchaseInquiry_purchaseApply_id").val(),
											sysCode : $("#purchaseInquiry_purchaseApply_type_sysCode").val()
										}, function(data) {
											if (data.success) {
												var result = data.result;
												var purchaseInquiryServiceItemGrid = $(
														"#purchase_inquiry_service_item_grid_div").data(
														"purchaseInquiryServiceItemGrid");
												var ob = purchaseInquiryServiceItemGrid.getStore().recordType;
												purchaseInquiryServiceItemGrid.stopEditing();
												$.each(result, function(i, obj) {
													var p = new ob({});
													p.isNew = true;
													p.set("serviceCode", obj[0]);
													p.set("serviceName", obj[1]);
													p.set("unit", obj[2]);
													purchaseInquiryServiceItemGrid.getStore().add(p);
												});
												purchaseInquiryServiceItemGrid.startEditing(0, 0);
											} else {
												message("生成服务明细失败！");
											}
										}, null);
									}
								} else {
									message("请选择您要添加的样本数据！");
								}
							});
				}, 100);
			});
});
