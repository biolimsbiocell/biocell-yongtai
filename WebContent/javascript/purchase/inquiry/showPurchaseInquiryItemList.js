$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];

	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'supplier1Price',
		type : "string"
	});
	fields.push({
		name : 'supplier2Price',
		type : "string"
	});
	fields.push({
		name : 'supplier3Price',
		type : "string"
	});
	fields.push({
		name : 'storage-currencyType-name',
		type : "string"
	});
	fields.push({
		name : 'storage-id',
		type : "string"
	});
	fields.push({
		name : 'storage-name',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];

	cm.push({
		dataIndex : 'id',
		header : '编号',
		width : 240,
		hidden : true
	});
	cm.push({
		dataIndex : 'storage-id',
		header : '对象编码',
		width : 180,
		sortable : true
	});
	cm.push({
		dataIndex : 'storage-name',
		header : '对象名称',
		width : 200,
		sortable : true
	});
	cm.push({
		dataIndex : 'storage-currencyType-name',
		header : '单位',
		width : 120,
		sortable : true
	});
	cm.push({
		dataIndex : 'supplier1Price',
		header : '供应商1价格',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'supplier2Price',
		header : '供应商2价格',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'supplier3Price',
		header : '供应商3价格',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'note',
		header : '备注',
		width : 240,
		sortable : true,
		editor : new Ext.form.TextArea({
			allowBlank : true
		})
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/purchase/inquiry/showPurchaseInquiryItemListJosn.action?id=" + $("#task_id").val();
	// loadParam.limit = 10
	var opts = {};
	opts.title = "库存询价明细";
	opts.height = document.documentElement.clientHeight + 425;
	opts.tbar = [];
	/* 以下是删除程序 */
	opts.delSelect = function(ids) {
		var id;
		if (ids) {
			id = "'" + ids.join("','") + "'";
		}
		ajax("post", "/purchase/inquiry/delPurchaseInquiryItemList.action", {
			ids : id
		}, function(data) {
			if (data.success) {
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : "显示可编辑列",
		handler : null
	});
	opts.tbar.push({
		text : "取消选中",
		handler : null
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != 'view') {
		opts.tbar.push({
			text : "填加明细",
			handler : function() {
				showMainStorageAllSelectList();
			}
		});
	} else {
		opts.tbar.push({
			text : "删除选中",
			handler : null
		});
		opts.tbar.push({
			text : "填加明细",
			handler : null
		});
	}
	var setPurchaseInquiryItem = function() {
		ajax("post", "/purchase/inquiry/createPurchaseInquiryItem.action", {
			id : $("#purchaseInquiry_purchaseApply_id").val(),
			sysCode : $("#purchaseInquiry_purchaseApply_type_sysCode").val()
		}, function(data) {
			if (data.success) {
				var result = data.result;
				var ob = purchaseInquiryItemGrid.getStore().recordType;
				purchaseInquiryItemGrid.stopEditing();
				$.each(result, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("storage-id", obj[0]);
					p.set("storage-name", obj[1]);
					p.set("storage-currencyType-name", obj[2]);
					purchaseInquiryItemGrid.getStore().add(p);
				});
				purchaseInquiryItemGrid.startEditing(0, 0);
			} else {
				message("生成库存明细失败！");
			}
		}, null);
	}
	var getPurchaseInquiryItem = function() {
		var planRecord = purchaseInquiryItemGrid.store.getModifiedRecords();
		var itemDataJson = [];
		if (planRecord && planRecord.length > 0) {
			$.each(planRecord, function(i, obj) {
				var data = {};
				data.id = obj.get("id");
				data.supplier1Price = obj.get("supplier1Price");
				data.supplier2Price = obj.get("supplier2Price");
				data.supplier3Price = obj.get("supplier3Price");
				data.note = obj.get("note");
				data["storage-id"] = obj.get("storage-id");
				itemDataJson.push(data);
			});
		}
		return itemDataJson.length > 0 ? JSON.stringify(itemDataJson) : "";
	}
	$("#purchase_inquiry_item_grid_div").data("setPurchaseInquiryItem", setPurchaseInquiryItem);
	
	$("#purchase_inquiry_item_grid_div").data("getPurchaseInquiryItem", getPurchaseInquiryItem);

	purchaseInquiryItemGrid = gridEditTable("purchase_inquiry_item_grid_div", cols, loadParam, opts);

	$("#purchase_inquiry_item_grid_div").data("purchaseInquiryItemGrid", purchaseInquiryItemGrid);
	
});

function addStorageGrid(record) {
	var fields = [ 'storage-name', 'storage-id' ,'storage-currencyType-name'];
	var fieldsfrom = [ 'name', 'id','currencyType-name' ];
	addgridfrom(purchaseInquiryItemGrid, record, fields, fieldsfrom);
}
