$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];

	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'supplier1Price',
		type : "string"
	});
	fields.push({
		name : 'supplier2Price',
		type : "string"
	});
	fields.push({
		name : 'supplier3Price',
		type : "string"
	});
	fields.push({
		name : 'equipment-currencyType-name',
		type : "string"
	});
	fields.push({
		name : 'equipment-id',
		type : "string"
	});
	fields.push({
		name : 'equipment-name',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];

	cm.push({
		dataIndex : 'id',
		header : '编号',
		width : 240,
		hidden : true
	});
	cm.push({
		dataIndex : 'equipment-id',
		header : '设备编码',
		width : 180,
		sortable : true
	});
	cm.push({
		dataIndex : 'equipment-name',
		header : '设备名称',
		width : 200,
		sortable : true
	});
	cm.push({
		dataIndex : 'equipment-currencyType-name',
		header : '单位',
		width : 120,
		sortable : true
	});
	cm.push({
		dataIndex : 'supplier1Price',
		header : '供应商1价格',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'supplier2Price',
		header : '供应商2价格',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'supplier3Price',
		header : '供应商3价格',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'note',
		header : '备注',
		width : 240,
		sortable : true,
		editor : new Ext.form.TextArea({
			allowBlank : true
		})
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/purchase/inquiry/showPurchaseInquiryEquipmentItemListJosn.action?id=" + $("#task_id").val();
	// loadParam.limit = 10
	var opts = {};
	opts.title = "设备询价明细";
	opts.height = document.documentElement.clientHeight + 425;
	opts.tbar = [];
	/* 以下是删除程序 */
	opts.delSelect = function(ids) {
		var id;
		if (ids) {
			id = "'" + ids.join("','") + "'";
		}
		ajax("post", "/purchase/inquiry/delPurchaseInquiryEquipmentItemList.action", {
			ids : id
		}, function(data) {
			if (data.success) {
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : "显示可编辑列",
		handler : null
	});
	opts.tbar.push({
		text : "取消选中",
		handler : null
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != 'view') {
		opts.tbar.push({
			text : "填加明细",
			handler : function() {
				var title = "选择设备";
				var url = "/equipment/common/selEquipmentList.action";
				var option = {};
				option.width = 685;
				option.height = 525;
				loadDialogPage(null, title, url, {}, true, option);
				setTimeout(function() {
					$("#sel_equipment_btn").click(function() {
						var selEquipmentGird = $("#sel_equipment_gird_div").data("selEquipmentGrid");
						var scEquipmentRecord = selEquipmentGird.getSelectRecord();
						if (scEquipmentRecord && scEquipmentRecord.length > 0) {
							var ob = purchaseInquiryEquipmentItemGrid.getStore().recordType;
							purchaseInquiryEquipmentItemGrid.stopEditing();
							$.each(scEquipmentRecord, function(i, obj) {
								/* 以下是验证重复代码 */
								var p = new ob({});
								p.isNew = true;
								p.set("equipment-id", obj.get("id"));
								p.set("equipment-name", obj.get("name"));
								p.set("equipment-currencyType-name", obj.get("currencyType-name"));
								purchaseInquiryEquipmentItemGrid.getStore().add(p);
							});
							purchaseInquiryEquipmentItemGrid.startEditing(0, 0);
						} else {
							message("请选择您要添加的样本数据！");
						}
					});
				}, 100)
			}
		});
	} else {
		opts.tbar.push({
			text : "删除选中",
			handler : null
		});
		opts.tbar.push({
			text : "填加明细",
			handler : null
		});
	}
	var setPurchaseInquiryEquipmentItem = function() {
		ajax("post", "/purchase/inquiry/createPurchaseInquiryEquipmentItem.action", {
			id : $("#purchaseInquiry_purchaseApply_id").val(),
			sysCode : $("#purchaseInquiry_purchaseApply_type_sysCode").val()
		}, function(data) {
			if (data.success) {
				var result = data.result;
				var ob = purchaseInquiryEquipmentItemGrid.getStore().recordType;
				purchaseInquiryEquipmentItemGrid.stopEditing();
				$.each(result, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("equipment-id", obj[0]);
					p.set("equipment-name", obj[1]);
					p.set("equipment-currencyType-name", obj[3]);
					purchaseInquiryEquipmentItemGrid.getStore().add(p);
				});
				purchaseInquiryEquipmentItemGrid.startEditing(0, 0);
			} else {
				message("生成设备明细失败！");
			}
		}, null);
	}
	var getPurchaseInquiryEquipmentItem = function() {
		var planRecord = purchaseInquiryEquipmentItemGrid.store.getModifiedRecords();
		var itemDataJson = [];
		if (planRecord && planRecord.length > 0) {
			$.each(planRecord, function(i, obj) {
				var data = {};
				data.id = obj.get("id");
				data.supplier1Price = obj.get("supplier1Price");
				data.supplier2Price = obj.get("supplier2Price");
				data.supplier3Price = obj.get("supplier3Price");
				data.note = obj.get("note");
				data["equipment-id"] = obj.get("equipment-id");
				itemDataJson.push(data);
			});
		}
		return itemDataJson.length > 0 ? JSON.stringify(itemDataJson) : "";
	}
	$("#purchase_inquiry_equipment_item_grid_div").data("setPurchaseInquiryEquipmentItem",
			setPurchaseInquiryEquipmentItem);
	
	$("#purchase_inquiry_equipment_item_grid_div").data("getPurchaseInquiryEquipmentItem",
			getPurchaseInquiryEquipmentItem);

	purchaseInquiryEquipmentItemGrid = gridEditTable("purchase_inquiry_equipment_item_grid_div", cols, loadParam, opts);

	$("#purchase_inquiry_equipment_item_grid_div").data("purchaseInquiryEquipmentItemGrid",
			purchaseInquiryEquipmentItemGrid);

});
