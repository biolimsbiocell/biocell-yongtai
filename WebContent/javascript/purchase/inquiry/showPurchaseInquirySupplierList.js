$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'supplier-id',
		type : "string"
	});
	fields.push({
		name : 'supplier-name',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];

	cm.push({
		dataIndex : 'id',
		header : '编号',
		width : 240,
		hidden : true
	});
	cm.push({
		dataIndex : 'supplier-id',
		header : '客户编号',
		width : 180,
		sortable : true
	});
	cm.push({
		dataIndex : 'supplier-name',
		header : '客户名称',
		width : 200,
		sortable : true
	});
	cm.push({
		dataIndex : 'note',
		header : '备注',
		width : 240,
		sortable : true,
		editor : new Ext.form.TextArea({
			allowBlank : true
		})
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/purchase/inquiry/showPurchaseInquirySupplierListJosn.action?id=" + $("#task_id").val();
	loadParam.limit = 3;
	var opts = {};
	opts.title = "客户明细";
	opts.height = 200;
	opts.tbar = [];
	/* 以下是删除程序 */
	opts.delSelect = function(ids) {
		var id;
		if (ids) {
			id = "'" + ids.join("','") + "'";
		}
		ajax("post", "/purchase/inquiry/delPurchaseInquirySupplierList.action", {
			ids : id
		}, function(data) {
			if (data.success) {
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : "显示可编辑列",
		handler : null
	});
	opts.tbar.push({
		text : "取消选中",
		handler : null
	});
	var handlemethod = $("#handlemethod").val();

	if (handlemethod != 'view') {
		opts.tbar.push({
			text : "填加明细",
			handler : function() {
				searchSupplierFun();
			}
		});
	} else {
		opts.tbar.push({
			text : "删除选中",
			handler : null
		});
		opts.tbar.push({
			text : "填加明细",
			handler : null
		});
	}

	var getPurchaseInquirySupplier = function() {
		var planRecord = purchaseInquirySupplierGrid.store.getModifiedRecords();
		var itemDataJson = [];
		if (planRecord && planRecord.length > 0) {
			$.each(planRecord, function(i, obj) {
				var data = {};
				data.id = obj.get("id");
				data.note = obj.get("note");
				data["supplier-id"] = obj.get("supplier-id");
				itemDataJson.push(data);
			});
		}
		return itemDataJson.length > 0 ? JSON.stringify(itemDataJson) : "";
	}
	$("#purchase_inquiry_supplier_gird_div").data("getPurchaseInquirySupplier", getPurchaseInquirySupplier);

	purchaseInquirySupplierGrid = gridEditTable("purchase_inquiry_supplier_gird_div", cols, loadParam, opts);

	$("#purchase_inquiry_supplier_gird_div").data("purchaseInquirySupplierGrid", purchaseInquirySupplierGrid);

});
function insertPurchaseInquirySupplier(id, name) {
	var ob = purchaseInquirySupplierGrid.getStore().recordType;
	purchaseInquirySupplierGrid.stopEditing();
	var p = new ob({});
	p.isNew = true;
	p.set("supplier-id", id);
	p.set("supplier-name", name);
	purchaseInquirySupplierGrid.getStore().add(p);
	purchaseInquirySupplierGrid.startEditing(0, 0);
}
