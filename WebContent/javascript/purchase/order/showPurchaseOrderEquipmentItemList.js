var instrumentListTable;
var oldInstrumentListChangeLog;
$(function(){
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : "ID",
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		},
		"visible" : false
	});
	colOpts.push({
		"data" : "equipment-id",
		"title" : biolims.common.instrumentNo,
		"createdCell" : function(td) {
			$(td).attr("saveName", "equipment-id");
		}
	});
	colOpts.push({
		"data" : "equipment-name",
		"title" : biolims.tInstrumentState.instrumentName,
		"createdCell" : function(td) {
			$(td).attr("saveName", "equipment-name");
		}
	});
	colOpts.push({
		"data" : "equipment-searchCode",
		"title" : biolims.storage.searchCode,
		"createdCell" : function(td) {
			$(td).attr("saveName", "equipment-searchCode");
		}
	});
	colOpts.push({
		"data" : "equipment-spec",
		"title" : biolims.equipment.spec,
		"createdCell" : function(td) {
			$(td).attr("saveName", "equipment-spec");
		}
	});
	colOpts.push({
		"data" : "num",
		"title" : biolims.common.count,
		"createdCell" : function(td) {
			$(td).attr("saveName", "num");
		},
		"className" : "edit"
	});
	colOpts.push({
		"data" : "unit",
		"title" : biolims.common.unit,
		"createdCell" : function(td) {
			$(td).attr("saveName", "unit");
		},
		"className" : "edit"
	});
	colOpts.push({
		"data" : "supplier-id",
		"title" : "供应商ID",
		"createdCell" : function(td) {
			$(td).attr("saveName", "supplier-id");
		},
		"visible" : false
	});
	colOpts.push({
		"data" : "supplier-name",
		"title" : "供应商",
		"createdCell" : function(td,data,rowdata) {
			$(td).attr("saveName", "supplier-name");
			$(td).attr("supplierId", rowdata["supplier-name"]);
		}
	});
	colOpts.push({
		"data" : "price",
		"title" : "单价",
		"createdCell" : function(td) {
			$(td).attr("saveName", "price");
		},
		"className" : "edit"
	});
	colOpts.push({
		"data" : "fee",
		"title" : "总价",
		"createdCell" : function(td) {
			$(td).attr("saveName", "fee");
		}
	});
	colOpts.push({
		"data" : "note",
		"title" : biolims.common.note,
		"createdCell" : function(td) {
			$(td).attr("saveName", "note");
		},
		"className" : "edit"
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {

		tbarOpts.push({
			text : biolims.common.delSelected,
			action : function() {
				removeChecked($("#instrumentTable"),
						"/purchase/order/delPurchaseOrderEquipmentItem.action");
			}
		});
		
		//选择申请单
		tbarOpts.push({
			text : biolims.common.selectInstrument,
			action : function() {
				selectPurchaseOrderForInstrument();
			}
		});
	}	
	
	var options = table(true, $("#purchaseOrder_id").val(), 
			"/purchase/order/showPurchaseOrderEquipmentItemListJson.action", colOpts, tbarOpts);

	instrumentListTable = renderData($("#instrumentTable"), options);
	instrumentListTable.on('draw', function() {
		oldInstrumentListChangeLog = instrumentListTable.ajax.json();
	});
		
})
//选择申请单
function selectPurchaseOrderForInstrument(){
	if($("#purchaseOrder_type_id").val=="77"){
		top.layer.open({
			title: biolims.common.pleaseChoose,
			type: 2,
			area: ["650px", "400px"],
			btn: biolims.common.selected,
			content: [window.ctx + "/purchase/apply/showPurchaseApplyDialogList.action?type=77", ''],
			yes: function(index, layer) {
				
				var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addPurchaseApply .chosed").
				children("td").eq(0).text();
				var typeId = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addPurchaseApply .chosed").
				children("td").eq(3).text();
				top.layer.closeAll();
				$.ajax({
					url : ctx + '/purchase/apply/findItemListJsonByType.action',
					type : 'post',
					data : {
						id:id,
						typeId:typeId
					},
					success : function(data){
						var data = JSON.parse(data);
						if(data.success){//成功
							var list = data.list;
							for(var i=0;i<list.length;i++){
								$("#instrumentTable").find(".dataTables_empty").parent("tr").remove();
								var tr = $("<tr class='editagain'><td><input type='checkbox' class='icheck' value=''/></td></tr>");
								var tds="<td savename='equipment-id'>"+list[i].equipment.id+"</td><td savename='equipment-name'>"+list[i].equipment.name+"</td>" +
								"<td savename='equipment-searchCode'>"+list[i].equipment.searchCode+"</td><td savename='equipment-spec'>"+list[i].equipment.spec+"</td>"+
								"<td class='edit' savename='num'></td><td savename='unit'></td>"+
								"<td savename='supplier-name' supplierId="+list[i].equipment.supplier.id+">"+list[i].equipment.supplier.name+"</td><td class='edit' savename='price'>"+list[i].equipment.price+"</td>"+
								"<td savename='fee'></td><td class='edit' savename='unit'>"+list[i].equipment.note+"</td>";
								tr.height(32);
								$("#instrumentTable").find("tbody").append(tr.append(tds));
							}
							checkall($("#instrumentTable"));
						}
					}
				})
			},
		})
	}
}

// 获得保存时的json数据
function saveInstrumentItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for (var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			json[k] = $(tds[j]).text();
			
			if(k == "supplier-name") {
				json["supplier-id"] = $(tds[j]).attr("supplierId");
				continue;
			}

		}
		data.push(json);
	});
	return JSON.stringify(data);
}
//日志
function getInstrumentChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '设备明细编号为"' + v.code + '":';
		oldInstrumentListChangeLog.data.forEach(function(vv, ii) {
			if (vv.id == id) {
				for ( var k in v) {
					if (v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"'
								+ v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}