$(function(){
	
	//日期格式化
	$("#purchaseOrder_reachDate").datepicker({
		language: "zh-TW",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});
	
	//显示那个明细的div
	if($("#purchaseOrder_type_id").val()=="2"){
		$("#goods").show();
		$("#instrument").hide();
		$("#service").hide();
	}else if($("#purchaseOrder_type_id").val()=="77"){
		$("#instrument").show();
		$("#goods").hide();
		$("#service").hide();
	}else if($("#purchaseOrder_type_id").val()=="88"){
		$("#service").show();
		$("#goods").hide();
		$("#instrument").hide();
	}else{
		$("#goods").show();
		$("#instrument").hide();
		$("#service").hide();
	}
	
	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "view"||$("#purchaseOrder_state").val()=="1") {
		settextreadonly();
	}
})

function fileUp() {
	if($("#purchaseOrder_id").val() == "NEW") {
		top.layer.msg("请先保存数据再上传附件！")
		return false;
	}
	$("#uploadFile").modal("show");
}

function fileView() {
	top.layer.open({
		title: biolims.common.attachment,
		type: 2,
		skin: 'layui-layer-lan',
		area: ["650px", "400px"],
		content: window.ctx + "/operfile/initFileList.action?flag=1&modelType=purchaseOrder&id=" + $("#purchaseOrder_id").val(),
		cancel: function(index, layer) {
			top.layer.close(index)
		}
	})
}

//分类
function selectType(){
	if($("#purchaseOrder_type_id").val()=="2"){
		$("#goods").show();
		goodsListTable.ajax.reload();
		$("#instrument").hide();
		$("#service").hide();
	}else if($("#purchaseOrder_type_id").val()=="77"){
		$("#instrument").show();
		instrumentListTable.ajax.reload();
		$("#goods").hide();
		$("#service").hide();
	}else if($("#purchaseOrder_type_id").val()=="88"){
		$("#service").show();
		serviceListTable.ajax.reload();
		$("#goods").hide();
		$("#instrument").hide();
	}
}

//付款联系人
function selectPayer(){
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/core/user/selectUserTable.action?groupId=admin", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index);
			$("#purchaseOrder_fukuanUser").val(id);
			$("#purchaseOrder_fukuanUser_name").val(name);
		},
	})
}

//采购联系人
function selectBuyer(){
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/core/user/selectUserTable.action?groupId=admin", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(
					0).text();
			top.layer.close(index);
			$("#purchaseOrder_caigouUser").val(id);
			$("#purchaseOrder_caigouUser_name").val(name);
		},
	})
}

//币种
function selectCurrency (){
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=bz", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$("#purchaseOrder_currencyType").val(id);
			$("#purchaseOrder_currencyType_name").val(name);
		},
	})
}

//保存
function save(){
	//loading
	top.layer.load(4, {
		shade: 0.3
	});
	//必填验证
	var requiredField=requiredFilter();
	if(!requiredField){
		return false;
	}
	//页面数据
	var jsonn = {};
	$("#form1 input").each(function(i, v) {
		var k = v.name;
		jsonn[k] = v.value;
	});
	$("#form1 select").each(function(i, v) {
		var k = v.name;
		jsonn[k] = v.value;
	});
	
	//日志
	var changeLog = "";
	$('input[class="form-control"]').each(
			function(i, v) {
				var valnew = $(v).val();
				var val = $(v).attr("changelog");
				if (val !== valnew) {
					changeLog += $(v).prev("span").text() + ':由"' + val
							+ '"变为"' + valnew + '";';
				}
			});
	
	
	var goodsJson = saveGoodsItemjson($("#goodsTable"));
	var instrumentJson = saveInstrumentItemjson($("#instrumentTable"));
	var serviceJson =  saveServiceItemjson($("#serviceTable"));
	
	var goodsChangeLogItem = "物资采购明细"+":";
	goodsChangeLogItem = getGoodsChangeLog(goodsJson, $("#goodsTable"), goodsChangeLogItem);
	
	var instrumentChangeLogItem = "设备采购明细"+":";
	instrumentChangeLogItem = getInstrumentChangeLog(instrumentJson, $("#instrumentTable"), instrumentChangeLogItem);
	
	var serviceChangeLogItem = "服务采购明细"+":";
	serviceChangeLogItem = getServiceChangeLog(serviceJson, $("#serviceTable"), serviceChangeLogItem);
	
	var data = {
		main : JSON.stringify(jsonn),
		changeLog : changeLog,
		goodsJson : goodsJson,
		goodsChangeLogItem : goodsChangeLogItem,
		instrumentJson : instrumentJson,
		instrumentChangeLogItem : instrumentChangeLogItem,
		serviceJson : serviceJson,
		serviceChangeLogItem : serviceChangeLogItem
	};
	
	$.ajax({
		url : ctx + '/purchase/order/save.action',
		type : 'post',
		data : data,
		success : function(data){
			var data = JSON.parse(data);
			if(data.success){//成功
				top.layer.closeAll();
				var url = "/purchase/order/toEditPurchaseOrder.action?id="
					+ data.id;
				window.location.href = url;
				top.layer.msg(biolims.common.saveSuccess);
			}else{//失败
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed);
			}
		}
	})
}
//批准人
function selectConfirmUser(){
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/core/user/selectUserTable.action?groupId=admin", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index);
			$("#purchaseOrder_confirmUser").val(id);
			$("#purchaseOrder_confirmUser_name").val(name);
		},
	})
}

//列表
function list(){
	window.location = window.ctx + '/purchase/order/showPurchaseOrderListTable.action';
}

function changeState() {
    var	id=$("#purchaseOrder_id").val()
	var paraStr = "formId=" + id +
		"&tableId=PurchaseOrder";
	console.log(paraStr)
	top.layer.open({
		title:biolims.common.approvalProcess,
		type: 2,
		anim: 2,
		area: ['400px', '400px'],
		btn: biolims.common.selected,
		content: window.ctx +
			"/applicationTypeAction/applicationTypeActionLook.action?" + paraStr +
			"&flag=changeState'",
		yes: function(indexx, layer) {
			top.layer.confirm(biolims.common.approve , {
				icon: 3,
				title: biolims.common.prompt,
				btn:biolims.common.selected
			}, function(index) {
				ajax("post", "/applicationTypeAction/exeFun.action", {
					applicationTypeActionId: $('.layui-layer-iframe', parent.document).find("iframe").contents().find("input:checked").val(),
					formId: id
				}, function(response) {
					var respText = response.message;
					if(respText == '') {
						window.location.reload();
					} else {
						top.layer.msg(respText);
					}
				}, null)
				top.layer.closeAll();
			})
		},
		

	});
}

//提交
function tjsp() {
	top.layer.confirm(biolims.common.pleaseConfirmSaveBeforeSubmit, {
		icon: 3,
		title: biolims.common.prompt,
		btn:biolims.common.selected
	}, function(index) {
		top.layer.open({
			title: biolims.common.submit,
			type: 2,
			anim: 2,
			area: ['800px', '500px'],
			btn: biolims.common.selected,
			content: window.ctx + "/workflow/processinstance/toStartView.action?formName=PurchaseApply",
			yes: function(index, layer) {
				var datas = {
					userId: userId,
					userName: userName,
					formId: $("#purchaseApply_id").val(),
					title: $("#purchaseApply_name").val(),
					formName: 'PurchaseApply'
				}
				ajax("post", "/workflow/processinstance/start.action", datas, function(data) {
					if(data.success) {
						top.layer.msg(biolims.common.submitSuccess);
						if(typeof callback == 'function') {
							callback(data);
						}
						dialogWin.dialog("close");
					} else {
						top.layer.msg(biolims.common.submitFail);
					}
				}, null);
				top.layer.close(index);
			},
			cancel: function(index, layer) {
				top.layer.close(index)
			}

		});
		top.layer.close(index);
	});
}

//审批
function sp(){
	var taskId = $("#bpmTaskId").val();
	var formId = $("#purchaseApply_id").val();
	
				top.layer.open({
					  title: biolims.common.handle,
					  type:2,
					  anim: 2,
					  area: ['800px','500px']
					  ,btn: biolims.common.selected,
					  content: window.ctx+"/workflow/processinstance/toCompleteTaskView.action?taskId="+taskId+"&formId="+formId,
					  yes: function(index, layer) {
						  var operVal =  $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#oper").val();
						  var opinionVal =  $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#opinionVal").val();
						  var opinion =  $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#opinion").val();
							if(!operVal){
								top.layer.msg(biolims.common.pleaseSelectOper);
								return false;
							}
							if (operVal == "2") {
								_trunTodoTask(taskId, callback, dialogWin);
							} else {
								var paramData = {};
								paramData.oper = operVal;
								paramData.info = opinion;
				
								var reqData = {
									data : JSON.stringify(paramData),
									formId : formId,
									taskId : taskId,
									userId : window.userId
								}
								ajax("post", "/workflow/processinstance/completeTask.action", reqData, function(data) {
									if (data.success) {
										top.layer.msg(biolims.common.submitSuccess);
										if (typeof callback == 'function') {
										}
									} else {
										top.layer.msg(biolims.common.submitFail);
									}
								}, null);
							}
							window.open(window.ctx+"/main/toPortal.action",'_parent');
						},
						cancel: function(index, layer) {
							top.layer.close(index)
						}
				
				});     
}