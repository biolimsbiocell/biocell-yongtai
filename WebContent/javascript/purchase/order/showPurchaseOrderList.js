var purchaseOrderListTable;
$(function(){
	
	var colData = [{
			"data": "id",
			"title": "采购订单编号"
		}, {
			"data": "note",
			"title": biolims.common.name
		}, {
			"data": "type-name",
			"title": biolims.sample.orderType
		}, {
			"data": "createUser-name",
			"title": biolims.purchase.buyer
		}, {
			"data": "createDate",
			"title": biolims.purchase.orderDate
		}, {
			"data": "confirmUser-name",
			"title": biolims.wk.approver,
			"visible":false
		}, {
			"data": "stateName",
			"title": biolims.common.stateName
		}];
	
	$.ajax({
		type:"post",
		url:window.ctx + "/system/customfields/findFieldByModuleValue.action",
		async:false,
		data:{
			moduleValue : "PurchaseOrder"
		},
		success:function(data){
			var objData = JSON.parse(data);
			if(objData.success){
				$.each(objData.data, function(i,n) {
					var str = {
						"data" : n.fieldName,
						"title" : n.label
					}
					colData.push(str);
				});
				
			}else{
				top.layer.msg(biolims.tInstrumentBorrowDetail.tableError);
			}
		}
	});
	
	var options = table(true, "","/purchase/order/showPurchaseOrderListTableJson.action",colData , null)
		purchaseOrderListTable = renderRememberData($("#main"), options);
		//恢复之前查询的状态
		$('#main').on('init.dt', function() {
			recoverSearchContent(purchaseOrderListTable);
		})
})

// 新建
function add() {
	window.location = window.ctx + "/purchase/order/toEditPurchaseOrder.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/purchase/order/toEditPurchaseOrder.action?id=' + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/purchase/order/toViewPurchaseOrder.action?id=' + id;
}
//弹框模糊查询参数
function searchOptions() {
	return [{
			"txt": "采购订单编号",
			"type": "input",
			"searchName": "id"
		},
		{
			"txt": "采购人",
			"type": "input",
			"searchName": "createUser.name"
		},
		{
			"txt": "订单日期",
			"type": "dataTime",
			"searchName": "createDate##@@##",
			"mark": "c##@@##",
		},
		{
			"txt": biolims.common.state,
			"type": "input",
			"searchName": "stateName",
		},
		{
			"type": "table",
			"table": purchaseOrderListTable
		}
	];
}