var goodsListTable;
var oldGoodsListChangeLog;
$(function(){
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : "ID",
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		},
		"visible" : false
	});
	colOpts.push({
		"data" : "storage-id",
		"title" : "物资编号",
		"createdCell" : function(td) {
			$(td).attr("saveName", "storage-id");
		}
	});
	colOpts.push({
		"data" : "storage-name",
		"title" : "物资名称",
		"createdCell" : function(td,data,rowdata) {
			$(td).attr("saveName", "storage-name");
		}
	});
	colOpts.push({
		"data" : "storage-spec",
		"title" : "规格",
		"createdCell" : function(td,data,rowdata) {
			$(td).attr("saveName", "storage-spec");
		}
	});
	colOpts.push({
		"data" : "num",
		"title" : "数量",
		"createdCell" : function(td) {
			$(td).attr("saveName", "num");
		},
		"className" : "edit"
	});
	colOpts.push({
		"data" : "unit",
		"title" : biolims.common.unit,
		"createdCell" : function(td) {
			$(td).attr("saveName", "unit");
		},
		"className" : "edit"
	});
	colOpts.push({
		"data" : "supplier-id",
		"title" : "供应商ID",
		"createdCell" : function(td) {
			$(td).attr("saveName", "supplier-id");
		},
		"visible" : false
	});
	colOpts.push({
		"data" : "supplier-name",
		"title" : "供应商",
		"createdCell" : function(td,data,rowdata) {
			$(td).attr("saveName", "supplier-name");
			$(td).attr("supplierId", rowdata["supplier-name"]);
		}
	});
	colOpts.push({
		"data" : "price",
		"title" : "单价",
		"createdCell" : function(td) {
			$(td).attr("saveName", "price");
		},
		"className" : "edit"
	});
	colOpts.push({
		"data" : "fee",
		"title" : "总价",
		"createdCell" : function(td) {
			$(td).attr("saveName", "fee");
		}
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
		
		tbarOpts.push({
			text: "选择申请单",
			action: function() {
				selectPurchaseOrderForGoods();
			}
		});

		tbarOpts.push({
			text : biolims.common.delSelected,
			action : function() {
				removeChecked($("#goodsTable"),
						"/purchase/order/delPurchaseOrderItem.action");
			}
		});
		tbarOpts.push({
			text : biolims.common.editwindow,
			action : function() {
				editItemLayer($("#goodsTable"))
			}
		});
		
	}	
	
	var options = table(true, $("#purchaseOrder_id").val(), "/purchase/order/showPurchaseOrderItemListJson.action", colOpts, tbarOpts);

	goodsListTable = renderData($("#goodsTable"), options);
	goodsListTable.on('draw', function() {
		oldGoodsListChangeLog = goodsListTable.ajax.json();
	});
		
})
//选择申请单
function selectPurchaseOrderForGoods(){
	var id = $("#purchaseOrder_type_id").val();
	if($("#purchaseOrder_type_id").val()=="2"){
		top.layer.open({
			title: biolims.common.pleaseChoose,
			type: 2,
			area: ["650px", "400px"],
			btn: biolims.common.selected,
			content: [window.ctx + "/purchase/apply/showPurchaseApplyDialogList.action?type=2", ''],
			yes: function(index, layer) {
				
				var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addPurchaseApply .chosed").
				children("td").eq(0).text();
				var typeId = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addPurchaseApply .chosed").
				children("td").eq(3).text();
				top.layer.closeAll();
				$.ajax({
					url : ctx + '/purchase/apply/findItemListJsonByType.action',
					type : 'post',
					data : {
						id:id,
						typeId:typeId
					},
					success : function(data){
						var data = JSON.parse(data);
						if(data.success){//成功
							var list = data.list;
							for(var i=0;i<list.length;i++){
								$("#goodsTable").find(".dataTables_empty").parent("tr").remove();
								var tr = $("<tr class='editagain'><td><input type='checkbox' class='icheck' value=''/></td></tr>");
								var tds="<td savename='storage-id'>"+list[i].storage.id+"</td><td savename='storage-name'>"+list[i].storage.name+"</td>" +
								"<td savename='storage-spec'>"+list[i].storage.spec+"</td><td class='edit' savename='num'></td>"+
								"<td class='edit' savename='unit'></td><td savename='supplier-name' supplierId="+list[i].storage.producer.id+">"+list[i].storage.producer.name+"</td>"+
								"<td class='edit' savename='price'></td><td savename='fee'></td>";
								tr.height(32);
								$("#goodsTable").find("tbody").append(tr.append(tds));
							}
							checkall($("#goodsTable"));
						}
					}
				})
			},
		})
	}
}

// 获得保存时的json数据
function saveGoodsItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for (var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			json[k] = $(tds[j]).text();
			
			
			if(k == "supplier-name") {
				json["supplier-id"] = $(tds[j]).attr("supplierId");
				continue;
			}

		}
		data.push(json);
	});
	return JSON.stringify(data);
}
//日志
function getGoodsChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '物资明细编号为"' + v.code + '":';
		oldGoodsListChangeLog.data.forEach(function(vv, ii) {
			if (vv.id == id) {
				for ( var k in v) {
					if (v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"'
								+ v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}