var serviceListTable;
var oldserviceListChangeLog;
$(function(){
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : "ID",
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		},
		"visible" : false
	});
	colOpts.push({
		"data" : "serviceCode",
		"title" : biolims.purchase.serviceCode,
		"createdCell" : function(td) {
			$(td).attr("saveName", "serviceCode");
		}
	});
	colOpts.push({
		"data" : "serviceName",
		"title" : biolims.purchase.serviceName,
		"createdCell" : function(td) {
			$(td).attr("saveName", "serviceName");
		}
	});
	colOpts.push({
		"data" : "supplier-id",
		"title" : "供应商ID",
		"createdCell" : function(td) {
			$(td).attr("saveName", "supplier-id");
		},
		"visible" : false
	});
	colOpts.push({
		"data" : "supplier-name",
		"title" : "供应商",
		"createdCell" : function(td) {
			$(td).attr("saveName", "supplier-name");
		}
	});
	colOpts.push({
		"data" : "num",
		"title" : biolims.common.count,
		"createdCell" : function(td) {
			$(td).attr("saveName", "num");
		},
		"className" : "edit"
	});
	colOpts.push({
		"data" : "unit",
		"title" : biolims.common.unit,
		"createdCell" : function(td) {
			$(td).attr("saveName", "unit");
		},
		"className" : "edit"
	});
	colOpts.push({
		"data" : "price",
		"title" : "单价",
		"createdCell" : function(td) {
			$(td).attr("saveName", "price");
		},
		"className" : "edit"
	});
	colOpts.push({
		"data" : "fee",
		"title" : "合计",
		"createdCell" : function(td) {
			$(td).attr("saveName", "fee");
		}
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
		
		tbarOpts.push({
			text: "选择申请单",
			action: function() {
				selectPurchaseOrderForService($("#serviceTable"));
			}
		});

		tbarOpts.push({
			text : biolims.common.delSelected,
			action : function() {
				removeChecked($("#serviceTable"),
						"/purchase/order/delPurchaseOrderServiceItem.action");
			}
		});
		
	}	
	
	var options = table(true, $("#purchaseOrder_id").val(), 
			"/purchase/order/showPurchaseOrderServiceItemListJson.action", colOpts, tbarOpts);

	serviceListTable = renderData($("#serviceTable"), options);
	serviceListTable.on('draw', function() {
		oldserviceListChangeLog = serviceListTable.ajax.json();
	});
		
})

function selectPurchaseOrderForService(){
	if($("#purchaseOrder_type_id").val=="88"){
		top.layer.open({
			title: biolims.common.pleaseChoose,
			type: 2,
			area: ["650px", "400px"],
			btn: biolims.common.selected,
			content: [window.ctx + "/purchase/apply/showPurchaseApplyDialogList.action?type=88", ''],
			yes: function(index, layer) {
				
				var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addPurchaseApply .chosed").
				children("td").eq(0).text();
				var typeId = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addPurchaseApply .chosed").
				children("td").eq(3).text();
				top.layer.closeAll();
				$.ajax({
					url : ctx + '/purchase/apply/findItemListJsonByType.action',
					type : 'post',
					data : {
						id:id,
						typeId:typeId
					},
					success : function(data){
						var data = JSON.parse(data);
						if(data.success){//成功
							var list = data.list;
							for(var i=0;i<list.length;i++){
								$("#serviceTable").find(".dataTables_empty").parent("tr").remove();
								var tr = $("<tr class='editagain'><td><input type='checkbox' class='icheck' value=''/></td></tr>");
								var tds="<td savename='serviceCode'>"+list[i].serviceCode+"</td><td savename='serviceName'>"+list[i].serviceName+"</td>" +
								"<td savename='supplier-name'><td class='edit' savename='num'></td>"+
								"<td class='edit' savename='unit'></td><td savename='price'></td>"+
								"<td savename='fee'></td>";
								tr.height(32);
								$("#serviceTable").find("tbody").append(tr.append(tds));
							}
							checkall($("#serviceTable"));
						}
					}
				})
			},
		})
	}
}

// 获得保存时的json数据
function saveServiceItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for (var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			json[k] = $(tds[j]).text();

		}
		data.push(json);
	});
	return JSON.stringify(data);
}
//日志
function getServiceChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '服务明细编号为"' + v.code + '":';
		oldserviceListChangeLog.data.forEach(function(vv, ii) {
			if (vv.id == id) {
				for ( var k in v) {
					if (v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"'
								+ v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}