function setStorageItem(rec) {
	
	var record = gridGrid.getSelectionModel().getSelected();
	record.set('serial', rec.get('id'));
	
	record.set('storageNum', rec.get('num'));
	
	var win = Ext.getCmp('showMainStorageItemAllSelectList');
	if (win) {
		win.close();
	}
}
function list() {
	window.location = window.ctx + '/purchase/cancel/showPurchaseCancelList.action';
}

function add() {
	window.location = window.ctx + "/purchase/cancel/toEditPurchaseCancel.action";
}

function newSave() {
	save();
}
function save() {

	if (checkSubmit() == true) {
		Ext.MessageBox.show({
			msg : '正在保存数据,请等待...',
			progressText : '保存中...',
			width : 300,
			wait : true,
			icon : 'ext-mb-download'
		});

		document.getElementById('jsonDataStr').value = commonGetModifyRecords(gridGrid);

		cancelForm.action = window.ctx + "/purchase/cancel/save.action";
		cancelForm.submit();
	} else {
		return false;
	}
}
function checkSubmit() {

	var mess = "";
	var fs = [ "purchaseCancel_id" ];
	var nsc = [ "编号不能为空！" ];
	mess = commonFieldsNotNullVerifyById(fs, nsc);
	if (mess != "") {
		message(mess);
		return false;
	}

	if (validateAllFun() == false) {

		return false;
	}

	return true;
}
var num = new Ext.form.NumberField({
	allowBlank : true,
	allowNegative : false,

	validator : validateRecordFun
});

function validateRecordFun(num) {
	var record = gridGrid.getSelectionModel().getSelected();
	var inNum = 0.0;
	var num1 = 0.0;
	var storageItemNum = 0.0;
	if (record.get('storageNum') != null) {
		storageItemNum = record.get('storageNum');
	}

	

	var s = record.get('storage-type-id');

	if (s.substring(0, 2) == '12') {
		if (parseFloat(num) > storageItemNum) {
			alertMessage("退货数量不能大于库存明细数量。");
			return '退货数量不能大于库存明细数量。';
		}
	}
	return true;
}

function validateFun(record) {

	var num = record.get('num');

	if (num == null || num == 0) {
		alertMessage("数量不能为空。");
		return false;
	}
	return true;
}
function validateAllFun() {

	var store1 = gridGrid.store;
	// store1.filter('storage-id',rec.get('storage-id'));
	var gridCount = store1.getCount();

	var allNum = 0.0;
	for ( var ij = 0; ij < gridCount; ij++) {
		var record = store1.getAt(ij);
		var num = record.get('num');

		if (num == null || num == 0) {
			alertMessage("数量不能为空。");
			return false;
		}
	}

	return true;
}

function addStorageGrid(record) {
	mess = commonFieldsNotNullVerifyById(fs, nsc);
	if (mess != "") {
		message(mess);
		return false;
	}
	return true;
}

function addStorageGrid(record) {

	var fields = [ 'storage-id', 'storage-name',  'storage-unit-name','storage-type-id'];
	var fieldsfrom = [ 'id', 'name', 'unit-name', 'type-id' ];
	
	if (!record || record == '') {
		Ext.MessageBox.alert('提示', '请选择记录保存', '');
	} else {
		var selData = '';
		for ( var ij = 0; ij < record.length; ij++) {
			var array = record[ij].data;

			var Ob = gridGrid.getStore().recordType;

			var p = new Ob({

			});

			for ( var i = 0; i < fields.length; i++) {
				

				p.set(fields[i], record[ij].get(fieldsfrom[i]));

			}

			gridGrid.stopEditing();
			gridGrid.store.insert(0, p);

			gridGrid.startEditing(0, 0);
			gridGrid.getView().getRow(0).style.backgroundColor = RECORD_INSERT_COLOR;

		}

	}
}

function viewPurchaseOrder() {
	if (trim(document.getElementById('purchaseCancel_purchaseOrder_id').value) == '') {
		message("请选择一条记录!");
	} else {
		openDialog(window.ctx + '/purchase/order/toViewPurchaseOrder.action?id='
				+ document.getElementById('purchaseCancel_purchaseOrder_id').value);

	}
}

function workflowSubmit() {
	commonWorkflowSubmit("applicationTypeTableId=purchaseCancel&formId=" + $("#purchaseCancel_id").val() + "&title="
			+ encodeURI(encodeURI('${purchaseCancel.reason}')));
}
function workflowLook() {
	commonWorkflowLook("applicationTypeTableId=purchaseCancel&formId=" + $("#purchaseCancel_id").val());
}
function workflowView() {
	commonWorkflowView("applicationTypeTableId=purchaseCancel&formId=" + $("#purchaseCancel_id").val());
}
function changeState() {
	commonChangeState("formId=" + $("#purchaseCancel_id").val() + "&tableId=purchaseCancel");
}
var store2;
function setGridId() {
	var gridCount = gridGrid.store.getCount();
	for ( var ij = 0; ij < gridCount; ij++) {
		var record = gridGrid.store.getAt(ij);
		record.set("id", "");
	}
	Ext.getCmp('showEditColumn').handler();
}

function insertStorageInItem() {

	var gridCount = store2.getCount();
	for ( var ij = 0; ij < gridCount; ij++) {

		var record = store2.getAt(ij);
		var Ob = gridGrid.getStore().recordType;
		var p = new Ob({});
		p.set("storageInItem-id", record.get("id"));
		p.set("storage-id", record.get("storage-id"));
		p.set("storage-name", record.get("storage-name"));
		p.set("storage-unit-name", record.get("storage-unit-name"));
		p.set("storage-searchCode", record.get("storage-searchCode"));
		p.set("storage-spec", record.get("storage-spec"));
		
		p.set("storage-type-id", record.get("storage-type-id"));
		
		p.set("price", record.get("price"));
		p.set("storageNum", record.get("storageNum"));
		p.set("storage-num", record.get("storage-num"));
		p.set("storageInItem-num", record.get("inNum"));
		p.set("costCenter-id", record.get("costCenter-id"));
		p.set("costCenter-name", record.get("costCenter-name"));
		
		p.set("supplierName", record.get("supplierName"));
		gridGrid.stopEditing();
		gridGrid.store.insert(0, p);
		gridGrid.startEditing(0, 0);
		gridGrid.getView().getRow(0).style.backgroundColor = RECORD_INSERT_COLOR;
	}
}
function setGridId() {
	var gridCount = gridGrid.store.getCount();
	for ( var ij = 0; ij < gridCount; ij++) {
		var record = gridGrid.store.getAt(ij);
		record.set("id", "");
	}
	Ext.getCmp('showEditColumn').handler();
}
function editCopy() {
	editCommonCopy(window.ctx + '/purchase/cancel/toEditPurchaseCancel.action?id=' + $("#purchaseCancel_id").val());
}
Ext.onReady(function() {
	var tabs = new Ext.TabPanel({
		renderTo : 'maintab',
		height : document.body.clientHeight-30,
		autoWidth : true,
		activeTab : 0,
		margins : '0 0 0 0',
		items : [ {
			title : '退货信息',
			contentEl : 'markup'
		} ]
	});
});
$(function() {
	var handlemethod = $("#handlemethod").val();
	var copyMode = $("#copyMode").val();
	if ('modify' == handlemethod) {
		var t = "#purchaseApply_id";
		settextreadonlyById(t);
	}
	if ('view' == handlemethod) {
		settextreadonlyByAll();
	}
	if ('true' == copyMode) {
		settextread("purchaseCancel_id");
		document.getElementById("purchaseCancel_id").value = "";
		document.getElementById("purchaseCancel_state").value = "3";
		document.getElementById("purchaseCancel_stateName").value = "新建";
		setTimeout("firstCommonValue(gridGrid)", 1000);

	}
});
var serial = new Ext.form.TextField({
	allowBlank : true,
	maxLength : 32
});

serial.on('focus', function() {
	showStorageItem();
});

function showStorageItem() {
	var record = gridGrid.getSelectionModel().getSelected();

	if (record.get('storage-type-id').substring(0, 2) == '12') {
		showMainStorageItemAllSelectList(record.get('storage-id'));
	} else {
		alertMessage("不能选择批次。");
	}
}
function showMainStorageItemAllSelectList(id) {
	var win = Ext.getCmp('showMainStorageItemAllSelectList');
	if (win) {
		win.close();
	}
var showMainStorageItemAllSelectList = new Ext.Window({
	id : 'showMainStorageItemAllSelectList',
	modal : true,
	title : '选择库存批次',
	layout : 'fit',
	width : 750,
	height : 500,
	closeAction : 'close',
	plain : true,
	bodyStyle : 'padding:5px;',
	buttonAlign : 'center',
	collapsible : true,
	maximizable : true,
	items : new Ext.BoxComponent({
		id : 'maincontent',
		region : 'center',
		html : "<iframe scrolling='no' name='maincontentframe' src='" + window.ctx
				+ "/storage/common/showMainStorageItemAllSelectList.action?tableId=storageOut&storageId=" + id
				+ "' frameborder='0' width='100%' height='100%' ></iframe>"
	}),
	buttons : [ {
		text : '关闭',
		handler : function() {
			showMainStorageItemAllSelectList.close();
		}
	} ]
});
showMainStorageItemAllSelectList.show();
}
