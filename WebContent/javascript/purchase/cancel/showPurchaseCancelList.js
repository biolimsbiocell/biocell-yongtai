
function list() {
	window.location = window.ctx + '/purchase/apply/showPurchaseCancelList.action';
}

function add() {
	window.location = window.ctx
			+ "/purchase/cancel/toEditPurchaseCancel.action";
}

function edit() {
	var id = "";
	id = document.getElementById("id").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx
			+ '/purchase/cancel/toEditPurchaseCancel.action?id=' + id;
}
function view(){
	var id="";
	id = document.getElementById("id").value;
	if(id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location= window.ctx +'/purchase/cancel/toViewPurchaseCancel.action?id='+id;
}
