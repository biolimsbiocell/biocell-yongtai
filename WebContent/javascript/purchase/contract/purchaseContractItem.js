var purchaseContractItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'storageId',
		type:"string"
	});
	   fields.push({
		name:'storageName',
		type:"string"
	});
	   fields.push({
		name:'num',
		type:"string"
	});
	   fields.push({
			name:'price',
			type:"string"
		});
	   fields.push({
			name:'fee',
			type:"string"
		});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'purchaseContract-id',
		type:"string"
	});
	    fields.push({
		name:'purchaseContract-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
	});
	cm.push({
		dataIndex:'storageId',
		hidden : true,
		header:'物品编号',
		width:50*6,
	});
	cm.push({
		dataIndex:'storageName',
		hidden : false,
		header:'物品描述',
		width:20*6,
	});
	cm.push({
		dataIndex:'num',
		hidden : false,
		header:'数量',
		width:20*6,
	});
	cm.push({
		dataIndex:'price',
		hidden : false,
		header:'单价',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'fee',
		hidden : false,
		header:'总价',
		width:20*6,
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'purchaseContract-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
	});
	cm.push({
		dataIndex:'purchaseContract-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/purchase/contract/purchaseContract/showPurchaseContractItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="合同明细";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/purchase/contract/purchaseContract/delPurchaseContractItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				purchaseContractItemGrid.getStore().commitChanges();
				purchaseContractItemGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
			text : '填加明细',
			handler : null
		});
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = purchaseContractItemGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
							purchaseContractItemGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	purchaseContractItemGrid=gridEditTable("purchaseContractItemdiv",cols,loadParam,opts);
	$("#purchaseContractItemdiv").data("purchaseContractItemGrid", purchaseContractItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})
function selecttPurchaseContractFun(){
	var win = Ext.getCmp('selecttPurchaseContract');
	if (win) {win.close();}
	var selecttPurchaseContract= new Ext.Window({
	id:'selecttPurchaseContract',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selecttPurchaseContract.close(); }  }]  }) });  
    selecttPurchaseContract.show(); }
	function settPurchaseContract(rec){
		var gridGrid = $("#purchaseContractItemdiv").data("purchaseContractItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('purchaseContract-id',rec.get('id'));
			obj.set('purchaseContract-name',rec.get('name'));
		});
		var win = Ext.getCmp('selecttPurchaseContract')
		if(win){
			win.close();
		}
	}
	function selecttPurchaseContractDialogFun(){
			var title = '';
			var url = '';
			title = "选择相关主表";
			url = ctx + "/PurchaseContractSelect.action?flag=tPurchaseContract";
			var option = {};
			option.width = document.body.clientWidth-30;
			option.height = document.body.clientHeight-160;
			loadDialogPage(null, title, url, {
				"确定" : function() {
						seltPurchaseContractVal(this);
				}
			}, true, option);
		}
	var seltPurchaseContractVal = function(win) {
		var operGrid = purchaseContractDialogGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			var gridGrid = $("#purchaseContractItemdiv").data("purchaseContractItemGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('purchaseContract-id',rec.get('id'));
				obj.set('purchaseContract-name',rec.get('name'));
			});
			$(win).dialog("close");
			$(win).dialog("remove");
		} else {
			message("请选择您要选择的数据");
			return;
		}
	};
