$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
})	
function add() {
	window.location = window.ctx + "/purchase/contract/editPurchaseContract.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/purchase/contract/showPurchaseContractList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_status").click(function() {
	changeState();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#purchaseContract", {
					userId : userId,
					userName : userName,
					formId : $("#purchaseContract_id").val(),
					title : $("#purchaseContract_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#purchaseContract_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/dashboard/toDashboard.action';
		});
});






function save() {
if(checkSubmit()==true){
	    var purchaseContractItemDivData = $("#purchaseContractItemdiv").data("purchaseContractItemGrid");
		document.getElementById('purchaseContractItemJson').value = commonGetModifyRecords(purchaseContractItemDivData);
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/purchase/contract/purchaseContract/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : '正在处理，请稍候。。。。。。',
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/purchase/contract/purchaseContract/copyPurchaseContract.action?id=' + $("#purchaseContract_id").val();
}
function changeState() {
//	commonChangeState("formId=" + $("#purchaseContract_id").val() + "&tableId=PurchaseContract");
	var paraStr = "formId=" + $("#purchaseContract_id").val() +
	"&tableId=PurchaseContract";
top.layer.open({
	title: biolims.common.approvalTask,
	type: 2,
	anim: 2,
	area: ['400px', '400px'],
	btn: biolims.common.selected,
	content: window.ctx +
		"/applicationTypeAction/applicationTypeActionLook.action?" + paraStr +
		"&flag=changeState'",
	yes: function(index, layer) {
		top.layer.confirm(biolims.common.approve, {
			icon: 3,
			title: biolims.common.prompt,
			btn:biolims.common.selected
		}, function(index) {
			ajax("post", "/applicationTypeAction/exeFun.action", {
				applicationTypeActionId: $('.layui-layer-iframe', parent.document).find("iframe").contents().find("input:checked").val(),
				formId: $("#purchaseContract_id").val()
			}, function(response) {
				var respText = response.message;
				if(respText == '') {
					window.open(window.location,'_self'); 
				} else {
					top.layer.msg(respText);
				}
			}, null)
			top.layer.closeAll();
		})
	}
});
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#purchaseContract_id").val());
	nsc.push("合同编码不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'采购合同',
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/purchase/contract/purchaseContract/showPurchaseContractItemList.action", {
				id : $("#purchaseContract_id").val()
			}, "#purchaseContractItempage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);