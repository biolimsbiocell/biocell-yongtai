var purchaseApplyListTable;
$(function(){
	
	var colData = [{
		//家系编号
			"data": "id",
			"title": biolims.purchase.purchasingApplicationNo
		}, {
			"data": "note",
			"title": biolims.common.name
		}, {
			"data": "type-name",
			"title": biolims.storage.studyType,
		}, {
			"data": "createUser-name",
			"title": biolims.sample.applyUserName
		}, {
			"data": "createDate",
			"title": biolims.purchase.createDate
		}, {
			"data": "confirmUser-name",
			"title": biolims.wk.approver
		}, {
			"data": "stateName",
			"title": biolims.common.stateName
		}];
	
	$.ajax({
		type:"post",
		url:window.ctx + "/system/customfields/findFieldByModuleValue.action",
		async:false,
		data:{
			moduleValue : "PurchaseApply"
		},
		success:function(data){
			var objData = JSON.parse(data);
			if(objData.success){
				$.each(objData.data, function(i,n) {
					var str = {
						"data" : n.fieldName,
						"title" : n.label
					}
					colData.push(str);
				});
				
			}else{
				top.layer.msg(biolims.tInstrumentBorrowDetail.tableError);
			}
		}
	});
	
	var options = table(true, "","/purchase/apply/showPurchaseApplyListJson.action",colData , null)
		purchaseApplyListTable = renderRememberData($("#main"), options);
		//恢复之前查询的状态
		$('#main').on('init.dt', function() {
			recoverSearchContent(purchaseApplyListTable);
		})
})

// 新建
function add() {
	window.location = window.ctx + "/purchase/apply/toEditPurchaseApply.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/purchase/apply/toEditPurchaseApply.action?id=' + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/purchase/apply/toViewPurchaseApply.action?id=' + id;
}
//弹框模糊查询参数
function searchOptions() {
	return [{
			"txt": biolims.purchase.purchasingApplicationNo,
			"type": "input",
			"searchName": "id"
		},
		{
			"txt": "申请人",
			"type": "input",
			"searchName": "createUser.name"
		},
		{
			"txt": "申请日期",
			"type": "dataTime",
			"searchName": "createDate##@@##",
			"mark": "c##@@##",
		},
		{
			"txt": biolims.common.state,
			"type": "input",
			"searchName": "stateName",
		},
		{
			"type": "table",
			"table": purchaseApplyListTable
		}
	];
}