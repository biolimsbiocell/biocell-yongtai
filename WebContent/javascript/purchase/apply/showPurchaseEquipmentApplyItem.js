var instrumentListTable;
var oldInstrumentListChangeLog;
$(function(){
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : "ID",
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		},
		"visible" : false
	});
	colOpts.push({
		"data" : "equipment-id",
		"title" : "EQL",
		"createdCell" : function(td) {
			$(td).attr("saveName", "equipment-id");
		}
	});
	colOpts.push({
		"data" : "equipment-name",
		"title" : biolims.tInstrumentState.instrumentName,
		"createdCell" : function(td) {
			$(td).attr("saveName", "equipment-name");
		}
	});
	colOpts.push({
		"data" : "equipment-serialNumber",
		"title" : biolims.equipment.serialNumber,
		"createdCell" : function(td) {
			$(td).attr("saveName", "equipment-serialNumber");
		}
	});
	colOpts.push({
		"data" : "equipment-spec",
		"title" : biolims.equipment.spec,
		"createdCell" : function(td) {
			$(td).attr("saveName", "equipment-spec");
		}
	});
	/*colOpts.push({
		"data" : "equipment-producer-id",
		"title" : biolims.equipment.spec,
		"createdCell" : function(td) {
			$(td).attr("saveName", "equipment-producer-id");
		},
		"visible":false
	});*/
	colOpts.push({
		"data" : "equipment-producer-name",
		"title" : biolims.equipment.producerName,
		"createdCell" : function(td,data,rowdata) {
			$(td).attr("saveName", "equipment-producer-name");
			$(td).attr("equipmentId",rowdata["equipment-producer-id"]);
		}
	});
	colOpts.push({
		"data" : "num",
		"title" : biolims.common.count,
		"createdCell" : function(td) {
			$(td).attr("saveName", "num");
		},
		"className" : "edit"
	});
	colOpts.push({
		"data" : "unit",
		"title" : biolims.common.unit,
		"createdCell" : function(td) {
			$(td).attr("saveName", "unit");
		},
		"className" : "edit"
	});
	colOpts.push({
		"data" : "approveNum",
		"title" : biolims.equipment.approveNum,
		"createdCell" : function(td) {
			$(td).attr("saveName", "approveNum");
		},
		"className" : "edit"
	});
	colOpts.push({
		"data" : "note",
		"title" : biolims.common.note,
		"createdCell" : function(td) {
			$(td).attr("saveName", "note");
		},
		"className" : "edit"
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {

		tbarOpts.push({
			text : biolims.common.delSelected,
			action : function() {
				removeChecked($("#instrumentTable"),
						"/purchase/apply/delPurchaseEquipmentApplyItem.action");
			}
		});
		
		//选择设备
		tbarOpts.push({
			text : biolims.common.selectInstrument,
			action : function() {
				selectInstrument();
			}
		});
	}	
	
	var options = table(true, $("#purchaseApply_id").val(), "/purchase/apply/showInstrumentItemListJson.action", colOpts, tbarOpts);

	instrumentListTable = renderData($("#instrumentTable"), options);
	instrumentListTable.on('draw', function() {
		oldInstrumentListChangeLog = instrumentListTable.ajax.json();
	});
		
})
//选择设备
function selectInstrument(){
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/equipment/main/showInstrumentDialogList.action", ''],
		yes: function(index, layer) {
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addInstrument .selected").each(function(i,v){

			var id=$(v).children("td").eq(1).text();
			var name = $(v).children("td").eq(2).text();
			var serialNumber = $(v).children("td").eq(3).text();
			var spec = $(v).children("td").eq(4).text();
			var producerId = $(v).children("td").eq(5).text();
			var producerName = $(v).children("td").eq(6).text();
			top.layer.close(index);
			$("#instrumentTable").find(".dataTables_empty").parent("tr").remove();
			var tr = $("<tr><td><input type='checkbox' class='icheck' value=''/></td></tr>");
			var tds="<td savename='equipment-id'>"+id+"</td><td savename='equipment-name'>"+name+"</td><td savename='equipment-serialNumber'>"+serialNumber+"</td>" +
				"<td savename='equipment-spec'>"+spec+"</td><td savename='equipment-producer-name' producerId="+producerId+">"+producerName+"</td>" +
				"<td class='edit' savename='num'></td><td class='edit' savename='unit'></td>"+
				"<td class='edit' savename='approveNum'></td><td class='edit' savename='note'></td>";
			tr.height(32);
			$("#instrumentTable").find("tbody").append(tr.append(tds));
			})
			
			checkall($("#instrumentTable"));
		},
	})
}

// 获得保存时的json数据
function saveInstrumentItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for (var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			json[k] = $(tds[j]).text();
			
			if(k == "equipment-producer-name") {
				json["equipment-producer-id"] = $(tds[j]).attr("producerId");
				continue;
			}

		}
		data.push(json);
	});
	return JSON.stringify(data);
}
//日志
function getInstrumentChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '设备明细编号为"' + v.code + '":';
		oldInstrumentListChangeLog.data.forEach(function(vv, ii) {
			if (vv.id == id) {
				for ( var k in v) {
					if (v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"'
								+ v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}