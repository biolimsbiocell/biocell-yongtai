var selPurchaseApplyGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];

	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
	fields.push({
		name : 'department-id',
		type : "string"
	});
	fields.push({
		name : 'department-name',
		type : "string"
	});
	fields.push({
		name : 'costCenter-id',
		type : "string"
	});
	fields.push({
		name : 'costCenter-name',
		type : "string"
	});
	fields.push({
		name : 'type-id',
		type : "string"
	});
	fields.push({
		name : 'type-name',
		type : "string"
	});
	fields.push({
		name : 'type-sysCode',
		type : "string"
	});
	fields.push({
		name : 'createDate',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : biolims.purchase.purchaseRequisition,
		width : 100,
		sortable : true
	});
	cm.push({
		dataIndex : 'createDate',
		header : biolims.purchase.createDate,
		width : 90,
		sortable : true
	});
	cm.push({
		dataIndex : 'note',
		header : biolims.common.name,
		width : 200,
		sortable : true
	});
	cm.push({
		dataIndex : 'type-id',
		header : biolims.purchase.typeId,
		width : 140,
		hidden : true
	});
	cm.push({
		dataIndex : 'type-sysCode',
		header : 'sysCode',
		width : 140,
		hidden : true
	});
	cm.push({
		dataIndex : 'type-name',
		header : biolims.storage.studyType,
		width : 140,
		sortable : true
	});

	cm.push({
		dataIndex : 'department-id',
		header : biolims.equipment.departmentId,
		width : 140,
		hidden : true
	});
	cm.push({
		dataIndex : 'department-name',
		header : biolims.equipment.departmentName,
		width : 140,
		sortable : true
	});
	cm.push({
		dataIndex : 'costCenter-id',
		header : biolims.purchase.costCenterId,
		width : 140,
		hidden : true
	});
	cm.push({
		dataIndex : 'costCenter-name',
		header : biolims.purchase.costCenter,
		width : 140,
		sortable : true
	});
	
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/purchase/apply/selPurchaseApplyListJosn.action?sysCode="+$("#applyType").val();
	loadParam.limit = 15;
	var opts = {};
	opts.height = document.body.clientHeight-100;
	opts.tbar = [];
	opts.tbar.push({
		text:"填加明细",
		handler:null
	})
	opts.tbar.push({
		text:"删除选中",
		handler:null
	})
	//opts.title = "选择设备列表";
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	selPurchaseApplyGrid = gridEditTable("sel_purchase_apply_list_gird_div", cols, loadParam, opts);
	
	$("#sel_purchase_apply_list_gird_div").data("selPurchaseApplyGrid",selPurchaseApplyGrid);
	
	/* 修改页签切换 */
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
	
	$("#sel_search").click(function() {
		var data = {};
		data.id = $("#sel_search_id").val();
		data.note = $("#sel_search_note").val();
		selPurchaseApplyGrid.getStore().baseParams.data = JSON.stringify(data);
		selPurchaseApplyGrid.getStore().load();
	});
});
