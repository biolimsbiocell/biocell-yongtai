$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": "采购申请单",
	});
	colOpts.push({
		"data": "createDate",
		"title": "申请时间",
	});
	colOpts.push({
		"data": "note",
		"title": "描述",
	});
	colOpts.push({
		"data": "type-id",
		"title": "类型",
	});
	colOpts.push({
		"data": "type-name",
		"title": "类型",
	});
	colOpts.push({
		"data": "type-sysCode",
		"title": "类型",
		"visible":false
	});
	colOpts.push({
		"data": "department-id",
		"title": "部门",
		"visible":false
	});
	colOpts.push({
		"data": "department-name",
		"title": "部门",
	});
	colOpts.push({
		"data": "costCenter-id",
		"title": "成本中心",
		"visible":false
	});
	colOpts.push({
		"data": "costCenter-name",
		"title": "成本中心",
	});
	var tbarOpts = [];
	var type = $("#type").val();
	if(type!=null){
		var options = table(false, null,
				'/purchase/apply/showPurchaseApplyDialogListByTypeJson.action?type='+type, colOpts, null)
		var addPurchaseApply = renderData($("#addPurchaseApply"), options);
	}else{
		var options = table(false, null,
				'/purchase/apply/showPurchaseApplyDialogListJson.action', colOpts, null)
		var addPurchaseApply = renderData($("#addPurchaseApply"), options);
	}
	$("#addPurchaseApply").on(
		'init.dt',
		function(e, settings) {
			// 清除操作按钮
			$("#addPurchaseApply_wrapper .dt-buttons").empty();
			$('#addPurchaseApply_wrapper').css({
				"padding": "0 16px"
			});
			var trs = $("#addPurchaseApply tbody tr");
			addPurchaseApply.ajax.reload();
			addPurchaseApply.on('draw', function() {
				trs = $("#addPurchaseApply tbody tr");
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			});
			trs.click(function() {
				$(this).addClass("chosed").siblings("tr")
					.removeClass("chosed");
			});
		});

})