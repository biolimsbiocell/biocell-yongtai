var goodsListTable;
var oldGoodsListChangeLog;
$(function(){
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : "ID",
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		},
		"visible" : false
	});
	colOpts.push({
		"data" : "storage-id",
		"title" : biolims.purchase.applicationObject,
		"createdCell" : function(td) {
			$(td).attr("saveName", "storage-id");
		},
		"visible" : false
	});
	colOpts.push({
		"data" : "storage-name",
		"title" : biolims.common.designation,
		"createdCell" : function(td,data,rowdata) {
			$(td).attr("saveName", "storage-name");
			$(td).attr("storage-id", rowdata["storage-id"]);
		}
	});
	colOpts.push({
		"data" : "storage-jdeCode",
		"title" : "JDE编码",
		"createdCell" : function(td) {
			$(td).attr("saveName", "storage-jdeCode");
		}
	});
	colOpts.push({
		"data" : "storage-producer-id",
		"title" : biolims.purchase.applicationObject,
		"createdCell" : function(td) {
			$(td).attr("saveName", "storage-producer-id");
		},
		"visible" : false
	});
	colOpts.push({
		"data" : "storage-producer-name",
		"title" : biolims.purchase.applicationObject,
		"createdCell" : function(td,data,rowdata) {
			$(td).attr("saveName", "storage-producer-name");
			$(td).attr("producerId", rowdata["storage-producer-id"]);
		}
	});
	colOpts.push({
		"data" : "num",
		"title" : "申请数量",
		"createdCell" : function(td) {
			$(td).attr("saveName", "num");
		},
		"className" : "edit"
	});
	colOpts.push({
		"data" : "approveNum",
		"title" : "审批数量",
		"createdCell" : function(td) {
			$(td).attr("saveName", "approveNum");
		},
		"className" : "edit"
	});
	colOpts.push({
		"data" : "unit",
		"title" : biolims.common.unit,
		"createdCell" : function(td) {
			$(td).attr("saveName", "unit");
		},
		"className" : "edit"
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
		
		tbarOpts.push({
			text: "选择物资",
			action: function() {
				selectGoods();
			}
		});

		tbarOpts.push({
			text : biolims.common.delSelected,
			action : function() {
				removeChecked($("#goodsTable"),
						"/purchase/apply/delPurchaseGoodsItem.action");
			}
		});
		tbarOpts.push({
			text : biolims.common.editwindow,
			action : function() {
				editItemLayer($("#goodsTable"))
			}
		});
		
	}	
	
	var options = table(true, $("#purchaseApply_id").val(), "/purchase/apply/showGoodsListJson.action", colOpts, tbarOpts);

	goodsListTable = renderData($("#goodsTable"), options);
	goodsListTable.on('draw', function() {
		oldGoodsListChangeLog = goodsListTable.ajax.json();
	});
		
})
//选择物资
function selectGoods(){
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/storage/showStorageDialogListForGoods.action", ''],
		yes: function(index, layer) {
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addToGoods .selected").each(function(i,v){
				var id = $(v).children("td").eq(1).text();
				var name = $(v).children("td").eq(2).text();
				var jde = $(v).children("td").eq(3).text();
				var producerId = $(v).children("td").eq(4).text();
				var producerName = $(v).children("td").eq(5).text();
				
				top.layer.close(index);
				$("#goodsTable").find(".dataTables_empty").parent("tr").remove();
				var tr = $("<tr><td><input type='checkbox' class='icheck' value=''/></td></tr>");
				var tds="<td savename='storage-name' storageId="+id+">"+name+"</td><td savename='storage-jdeCode'>"+jde+"</td>" +
				"<td savename='storage-producer-name' producerId="+producerId+">"+producerName+
				"<td class='edit' savename='num'></td><td class='edit' savename='approveNum'></td>"+
				"<td class='edit' savename='unit'></td>";
				tr.height(32);
				$("#goodsTable").find("tbody").append(tr.append(tds));
			})
			checkall($("#goodsTable"));
		},
	})
}

// 获得保存时的json数据
function saveGoodsItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for (var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			json[k] = $(tds[j]).text();
			
			if(k == "storage-producer-name") {
				json["storage-producer-id"] = $(tds[j]).attr("producerId");
				continue;
			}
			
			if(k == "storage-name") {
				json["storage-id"] = $(tds[j]).attr("storage-id");
				continue;
			}

		}
		data.push(json);
	});
	return JSON.stringify(data);
}
//日志
function getGoodsChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '物资明细编号为"' + v.code + '":';
		oldGoodsListChangeLog.data.forEach(function(vv, ii) {
			if (vv.id == id) {
				for ( var k in v) {
					if (v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"'
								+ v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}