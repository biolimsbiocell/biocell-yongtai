var serviceListTable;
var oldserviceListChangeLog;
$(function(){
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : "ID",
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		},
		"visible" : false
	});
	colOpts.push({
		"data" : "serviceCode",
		"title" : biolims.purchase.serviceCode,
		"createdCell" : function(td) {
			$(td).attr("saveName", "serviceCode");
		},
		"className" : "edit"
	});
	colOpts.push({
		"data" : "serviceName",
		"title" : biolims.purchase.serviceName,
		"createdCell" : function(td) {
			$(td).attr("saveName", "serviceName");
		},
		"className" : "edit"
	});
	colOpts.push({
		"data" : "num",
		"title" : biolims.common.count,
		"createdCell" : function(td) {
			$(td).attr("saveName", "num");
		},
		"className" : "edit"
	});
	colOpts.push({
		"data" : "unit",
		"title" : biolims.common.unit,
		"createdCell" : function(td) {
			$(td).attr("saveName", "unit");
		},
		"className" : "edit"
	});
	colOpts.push({
		"data" : "approveNum",
		"title" : "批准数量",
		"createdCell" : function(td) {
			$(td).attr("saveName", "approveNum");
		},
		"className" : "edit"
	});
	colOpts.push({
		"data" : "note",
		"title" : biolims.common.note,
		"createdCell" : function(td) {
			$(td).attr("saveName", "note");
		},
		"className" : "edit"
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
		
		tbarOpts.push({
			text: biolims.common.fillDetail,
			action: function() {
				addItem($("#serviceTable"));
			}
		});

		tbarOpts.push({
			text : biolims.common.delSelected,
			action : function() {
				removeChecked($("#serviceTable"),
						"/purchase/apply/delPurchaseServiceApplyItem.action");
			}
		});
		
	}	
	
	var options = table(true, $("#purchaseApply_id").val(), "/purchase/apply/showServiceItemListJson.action", colOpts, tbarOpts);

	serviceListTable = renderData($("#serviceTable"), options);
	serviceListTable.on('draw', function() {
		oldserviceListChangeLog = serviceListTable.ajax.json();
	});
		
})

// 获得保存时的json数据
function saveServiceItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for (var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			json[k] = $(tds[j]).text();

		}
		data.push(json);
	});
	return JSON.stringify(data);
}
//日志
function getServiceChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '服务明细编号为"' + v.code + '":';
		oldserviceListChangeLog.data.forEach(function(vv, ii) {
			if (vv.id == id) {
				for ( var k in v) {
					if (v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"'
								+ v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}