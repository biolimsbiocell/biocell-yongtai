var purchasePayableGrid;
$(function(){
	var cols={};
	cols.sm=true;
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'contractId',
		type:"string"
	});
	    fields.push({
		name:'contractName',
		type:"string"
	});
	    fields.push({
		name:'totalPrice',
		type:"string"
	});
	    fields.push({
		name:'paid',
		type:"string"
	});
	    fields.push({
		name:'unpaid',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编码',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:50*3,
		hidden:true,
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:'创建人ID',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:'创建人',
		width:10*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:'创建时间',
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'contractId',
		header:'采购合同编码',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'contractName',
		header:'采购合同描述',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'totalPrice',
		header:'总价',
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'paid',
		header:'已付',
		width:20*6,
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'unpaid',
		header:'未付',
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:'状态',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:'状态名称',
		width:20*6,
		sortable:true,
	});
	cm.push({
		dataIndex:'note',
		header:'备注',
		width:50*6,
		hidden:true,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/purchase/payable/purchasePayable/showPurchasePayableListJson.action";
	var opts={};
	opts.tbar=[];
	opts.title="应付账款";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.tbar.push({
		text : '填加明细',
		handler : null
		})
		opts.tbar.push({
		text : '取消选中',
		handler : null
		})
		opts.tbar.push({
		text : '删除选中',
		handler : null
		})
		opts.tbar.push({
		text : '显示可编辑列',
		handler : null
		})
	opts.tbar.push({
		text : '保存',
		handler : function(){
			operRecord=commonGetModifyRecords(purchasePayableGrid);
			allRecord=purchasePayableGrid;
			for(var i=0;i<allRecord.length;i++){
				alert(allRecord[i]);
				alert(allRecord[i]);
				if(allRecord[i].paid>allRecord[i].totalPrice){
					message("已付不能大于总价！")
					return
				}
			}
			ajax("post","/purchase/payable/purchasePayable/save.action",{
				record:operRecord
			},function(data){
				if(data.success){
					purchasePayableGrid.getStore().commitChanges();
					purchasePayableGrid.getStore().reload();
					message("保存成功！");
				}
			},null)
		}
	})
	
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	purchasePayableGrid=gridEditTable("show_purchasePayable_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/purchase/payable/purchasePayable/editPurchasePayable.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location=window.ctx+'/purchase/payable/purchasePayable/editPurchasePayable.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/purchase/payable/purchasePayable/viewPurchasePayable.action?id=' + id;
}
function exportexcel() {
	purchasePayableGrid.title = '导出列表';
	var vExportContent = purchasePayableGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
			
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}
				
				
				commonSearchAction(purchasePayableGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	});
});
