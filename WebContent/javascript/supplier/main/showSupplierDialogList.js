$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": "编号",
	});
	colOpts.push({
		"data": "name",
		"title": "供应商名称",
	});
	var tbarOpts = [];
	var options = table(true, null,
		'/supplier/showSupplierDialogListJson.action', colOpts, null)
	var addSupplier = renderData($("#addSupplier"), options);
	$("#addSupplier").on(
		'init.dt',
		function(e, settings) {
			// 清除操作按钮
			$("#addSupplier_wrapper .dt-buttons").empty();
			$('#addSupplier_wrapper').css({
				"padding": "0 16px"
			});
			var trs = $("#addSupplier tbody tr");
			addSupplier.ajax.reload();
			addSupplier.on('draw', function() {
				trs = $("#addSupplier tbody tr");
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			});
			trs.click(function() {
				$(this).addClass("chosed").siblings("tr")
					.removeClass("chosed");
			});
		});

})