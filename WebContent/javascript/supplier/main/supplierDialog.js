var supplierDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
			name:'linkMan',
			type:"string"
		});
	    fields.push({
			name:'linkTel',
			type:"string"
		});
	    fields.push({
			name:'email',
			type:"string"
		});
	    fields.push({
			name:'fax',
			type:"string"
		});
	    fields.push({
			name:'address5',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:"供应商编号",
		width:100,
		sortable:true
	});	
	cm.push({
		dataIndex:'name',
		header:"供应商名称",
		width:100,
		sortable:true
	});
	cm.push({
		dataIndex:'linkMan',
		header:"联系人",
		width:100,
		sortable:true
	});
	cm.push({
		dataIndex:'linkTel',
		header:"联系电话",
		width:100,
		sortable:true
	});
	cm.push({
		dataIndex:'email',
		header:"邮箱",
		width:100,
		sortable:true
	});
	cm.push({
		dataIndex:'fax',
		header:"传真",
		width:100,
		sortable:true
	});
	cm.push({
		dataIndex:'address5',
		header:"地址",
		width:100,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/supplier/showDialogSupplierListJson.action";
	var opts={};
	opts.title="供应商";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){

		$('#selectId').val(id);
		setPValue(rec);
	};
	supplierDialogGrid=gridTable("show_dialog_supplier_div",cols,loadParam,opts);
});
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(crmCustomerDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
