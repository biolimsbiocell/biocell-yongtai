var showSupplierList;
$(function() {
	
	var colData = [{
			"data": "id",
			"title": biolims.purchase.supplierId,
		}, {
			"data": "name",
			"title": biolims.common.designation,
		}, {
			"data": "way",
			"title": biolims.purchase.industry,
		}, {
			"data": "supplierType-name",
			"title": biolims.common.type,
		}, {
			"data": "address5",
			"title": biolims.common.addr,
		}, {
			"data": "state-name",
			"title": biolims.common.stateName,
		/*}, {
			"data": "supplierType-id",
			"title": biolims.common.typeID,
		}, {
			"data": "website",
			"title": biolims.purchase.website,
		}, {
			"data": "linkMan",
			"title": biolims.common.linkmanName,
		}, {
			"data": "linkTel",
			"title": biolims.common.phone,
		}, {
			"data": "fax",
			"title": biolims.purchase.fax,
		}, {
			"data": "postCode",
			"title": biolims.purchase.postCode,
		}, {
			"data": "address2",
			"title": biolims.purchase.address2,
		}, {
			"data": "address3",
			"title": biolims.purchase.address3,
		}, {
			"data": "address4",
			"title": biolims.purchase.address4,
		}, {
			"data": "capitalNum",
			"title": biolims.purchase.capitalNum,
		}, {
			"data": "currencyType-name",
			"title": biolims.storage.rankType,
		}, {
			"data": "orgCode",
			"title": biolims.purchase.orgCode,
		}, {
			"data": "accountBank",
			"title": biolims.purchase.accountBank,
		}, {
			"data": "account",
			"title": biolims.purchase.account,
		}, {
			"data": "bankCode",
			"title": biolims.purchase.bankCode,*/
		
		}];
		
		$.ajax({
			type:"post",
			url:window.ctx + "/system/customfields/findFieldByModuleValue.action",
			/*/biolim-dna-v7.0-BIT/src/com/biolims/supplier/main/action/SupplierAction.java*/
			/*url:window.ctx + "/supplier/main/action/showSupplierListJson.action",*/
			async:false,
			data:{
				moduleValue : "Supplier"
			},
			success:function(data){
				var objData = JSON.parse(data);
				if(objData.success){
					$.each(objData.data, function(i,n) {
						var str = {
							"data" : n.fieldName,
							"title" : n.label
						}
//						colData.push(str);
					});
					
				}else{
					top.layer.msg("表格解析错误，请重新刷新界面！");
				}
			}
		});
		
		
	var options = table(true, "",
		/*"/system/sample/sampleOrder/showSampleOrderTableJson.action",colData , null)*/
			/*"/supplier/main/action/showSupplierListJson.action",colData , null)*/
			"/supplier/showSupplierListJson.action",colData , null)
	sampleOrderTable = renderRememberData($("#main"), options);
	//恢复之前查询的状态
	$('#main').on('init.dt', function() {
		recoverSearchContent(sampleOrderTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
		/*'/system/sample/sampleOrder/editSampleOrder.action';*/
	'/supplier/toEditSupplier.action';
	/*'/WEB-INF/page/supplier/main/editSupplier.jsp';*/
	
	
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		/*'/system/sample/sampleOrder/editSampleOrder.action?id=' + id;*/
	/*'/supplier/toEditCustomer.action?id=' + id;*/
	'/supplier/toEditSupplier.action?id=' + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/supplier/toViewSupplier.action?id=' + id;
}
//弹框模糊查询参数
function searchOptions() {
	return [{
			"txt": "编码",
			"type": "input",
			"searchName": "id"
		},
		{
			"txt": "名称",
			"type": "input",
			"searchName": "name"
		},
		{
			"txt": "状态",
			"type": "input",
			"searchName": "state.name",
		},
		{
			"type": "table",
			"table": sampleOrderTable
		}
	];
}

function confirmUser() {
	top.layer.msg("这个是选的");
}
