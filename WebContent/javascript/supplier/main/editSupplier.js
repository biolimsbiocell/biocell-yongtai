/* 
 * 文件名称 :productNew.js
 * 创建者 : 郭恒开
 * 创建日期: 2018/02/06
 * 文件描述: 新建产品数据操作函数
 * 
 */
$(function() {
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "view") {
		settextreadonly();
	}
	if (handlemethod == "modify") {
		$("#supplier_id").prop("readonly", "readonly");
	}
	// 上传附件(1.useType,2.modelType,3.id)
	var mainFileInput = fileInput('1', 'supplier', $("#supplier_id").val());
	
	fieldCustomFun();
})

function settextreadonly() {
	jQuery(":text, textarea").each(function() {
		var _vId = jQuery(this).attr('id');
		jQuery(this).css("background-color", "#B4BAB5").attr("readonly", "readOnly");
		if(_vId == 'actiondropdown_textbox')
			settextread(_vId);
	});
}

function settextread(name) {
	document.getElementById(name).className = "input_parts text input default  readonlyfalse false";
	document.getElementById(name).readOnly = false;
}

//自定义模块
function fieldCustomFun() {

	//获取自定义字段值的相关内容
	var fieldContent = $("#fieldContent").val();

	//查找自定义列表的订单模块的此检测项目的相关内容
	$.ajax({
		type: "post",
		url: window.ctx + "/system/customfields/findFieldByModuleValue.action",
		data: {
			moduleValue: "Supplier"
		},
		async: false,
		success: function(data) {
			var objValue = JSON.parse(data);
			if(objValue.success) {
				$.each(objValue.data, function(i, n) {
					var inputs = '';
					var disabled = n.readOnly ? ' ' : "disabled";
					var defaultValue = n.defaultValue ? n.defaultValue : ' ';
					if(n.fieldType == "checkbox") {
						var checkboxs = '';
						var singleOptionIdArry = n.singleOptionId.split("/");
						var singleOptionNameArry = n.singleOptionName.split("/");
						singleOptionIdArry.forEach(function(vv, jj) {
							checkboxs += '<input type="checkbox" lay-ignore name=' + n.fieldName + ' value=' + jj + ' title=' + singleOptionNameArry[vv] + '>' + singleOptionNameArry[vv] + '';
						});
						inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group checkboxs " changelog=""><span class="input-group-addon">' + n.label + '</span>' + checkboxs + '</div></div>';
					} else if(n.fieldType == "radio") {
						var options = '';
						var singleOptionIdArry = n.singleOptionId.split("/");
						var singleOptionNameArry = n.singleOptionName.split("/");
						singleOptionIdArry.forEach(function(vv, jj) {
							options += '<option name=' + n.fieldName + ' value=' + jj + '>' + singleOptionNameArry[vv] + '</option>';
						});
						inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><select class="form-control">' + options + '</select></div></div>';
					} else if(n.fieldType == "date") {
						inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><input type="text" name=' + n.fieldName + ' required=' + n.isRequired + ' ' + disabled + ' class="form-control datepick" value=' + defaultValue + '></div></div>';
					} else {
						if(n.isRequired != "false") {
							inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '<img class="requiredimage" src="/images/required.gif" /></span><input type=' + n.fieldType + ' name=' + n.fieldName + '   class="form-control" ' + disabled + ' value=' + defaultValue + '></div></div>';
						} else {
							inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><input type=' + n.fieldType + ' name=' + n.fieldName + '   class="form-control" ' + disabled + ' value=' + defaultValue + '></div></div>';
						}
					}

					$("#fieldItemDiv").append(inputs);
				});

			} else {
				top.layer.msg(biolims.customList.updateFailed);
			}
		}
	});

	//显示自定义字段的数据
	if(fieldContent != null && fieldContent != undefined && fieldContent != "") {
		if(fieldContent && fieldContent != "null") {
			var contentData = JSON.parse(fieldContent);
			for(var k in contentData) {
				$("#fieldItemDiv input").each(function(i, inp) {
					if(inp.name == k) {
						if(inp.type == "checkbox") {
							if(contentData[k].indexOf(inp.value) != -1) {
								inp.setAttribute("checked", true);
							}
						} else {
							inp.value = contentData[k];
						}
						inp.setAttribute("changelog", inp.value);
					}
				});
				$("#fieldItemDiv option").each(function(i, val) {
					if(k == val.getAttribute("name")) {
						if(val.value == contentData[k]) {
							val.setAttribute("selected", true);
							inp.setAttribute("changelog", inp.value);
						}
					}
				});
			};
		}
	}

	//日期格式化
	$("#fieldItemDiv").find('.datepick').datepicker({
		language: "zh-TW",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});
	//多选框 格式化
	$("#fieldItemDiv").find('input[type="checkbox"]').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		increaseArea: '20%' // optional
	});
}

// 保存
function save() {
	// 将form表单数据转换数组
	/*var mainform = $("#form1").serializeArray();
	var main = {}
	// 将main数组转换为json
	mainform.forEach(function(v, i) {
		main[v.name] = v.value;
	});
	var mainValue = JSON.stringify(main);
	var changeLog = "";
	changeLog = getChangeLog(userId, changeLog);*/
	
	
	//自定义字段
	//拼自定义字段儿（实验记录）
	var inputs = $("#fieldItemDiv input");
	var options = $("#fieldItemDiv option");
	var contentData = {};
	var checkboxArr = [];
	//必填验证
	var requiredField=requiredFilter();
		if(!requiredField){
			return false;
		}
	$("#fieldItemDiv .checkboxs").each(function(i, v) {
		$(v).find("input").each(function(ii, inp) {
			var k = inp.name;
			if(inp.checked == true) {
				checkboxArr.push(inp.value);
				contentData[k] = checkboxArr;
			}
		});
	});
	inputs.each(function(i, inp) {
		var k = inp.name;
		if(inp.type != "checkbox") {
			contentData[k] = inp.value;
		}
	});
	options.each(function(i, opt) {
		if(opt.selected == true) {
			var k = opt.getAttribute("name");
			contentData[k] = opt.value;
		}
	});
	document.getElementById("fieldContent").value = JSON.stringify(contentData);

	var changeLog = "供应商信息:";
	$('input[class="form-control"]').each(function(i, v) {
		var valnew = $(v).val();
		var val = $(v).attr("changelog");
		if(val !== valnew) {
			changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
		}
	});
	var changeLogs="";
	if(changeLog !="供应商信息:"){
		changeLogs=changeLog;
		$("#changeLog").val(changeLogs);
	}
	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "modify") {
		top.layer.load(4, {shade:0.3}); 
			$("#form1").attr("action", "/supplier/saveSupplier.action?changeLog="+changeLogs);
			$("#form1").submit();
			top.layer.closeAll();
	} else {
		$.ajax({
			type: "post",
			url: ctx + '/common/hasId.action',
			data: {
				id: $("#supplier_id").val(),
				obj: 'Supplier'
			},
			success: function(data) {
				var data = JSON.parse(data);
				if(data.message) {
					top.layer.msg(data.message);
				} else {
					top.layer.load(4, {shade:0.3}); 
					$("#form1").attr("action", "/supplier/saveSupplier.action?changeLog="+changeLogs);
					$("#form1").submit();
					top.layer.closeAll();
				}
			}
		});
	}
	/*var jsonStr = JSON.stringify($("#form1").serializeObject());


	var info = {
		id : $("#supplier_id").val(),
		obj : 'Supplier',
		main : jsonStr,
		logInfo : changeLog
	};
	$.ajax({
		type : 'post',
		url : ctx + "/supplier/saveSupplier.action",
		data : info,
		success : function(data) {
			var da =JSON.parse(data);
			if (da.success) {
				top.layer.msg(biolims.common.saveSuccess);
				window.location = window.ctx
				+ '/supplier/toEditSupplier.action?id=' + da.id;
//				tableRefresh();
			} 
		}
	});*/
}


$.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if(o[this.name]) {
			if(!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};

function list() {
	window.location = window.ctx + '/supplier/showSupplierList.action';
}
// 选择父级
function choseParent() {
	top.layer.open({
		title : "选择父级",
		type : 2,
		area : [ document.body.clientWidth - 300,
				document.body.clientHeight - 100 ],
		btn : biolims.common.selected,
		content : window.ctx
				+ "/com/biolims/system/product/showProductSelTree.action",
		yes : function(index, layer) {
			var name = [], id = [];
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find(
					"#mytreeGrid .chosed").each(function(i, v) {
				name.push($(v).children("td").eq(2).text());
				id.push($(v).children("td").eq(1).text());
			});
			$("#product_parent_name").val(name.join(","));
			$("#product_parent_id").val(id.join(","));
			top.layer.close(index)
		},
		cancel : function(index, layer) {
			top.layer.close(index)
		}
	})
}
// 选择区块
function choseQK() {
	top.layer
			.open({
				title : "选择区块",
				type : 2,
				area : [ document.body.clientWidth - 300,
						document.body.clientHeight - 100 ],
				btn : biolims.common.selected,
				content : [
						window.ctx
								+ "/dic/type/dicTypeSelectTable.action?flag=orderBlock",
						'' ],
				yes : function(index, layer) {
					var name = $('.layui-layer-iframe', parent.document).find("iframe")
							.contents().find("#addDicTypeTable .chosed")
							.children("td").eq(1).text();
					var id = $('.layui-layer-iframe', parent.document).find("iframe").contents()
							.find("#addDicTypeTable .chosed").children("td")
							.eq(0).text();
					top.layer.close(index)
					$("#orderBlockId").val(id);
					$("#orderBlock").val(name);
				},
			})
}
// 选择检测方法
function choseJCFF() {
	top.layer.open({
		title : "选择检测方法",
		type : 2,
		area : [ document.body.clientWidth - 300,
				document.body.clientHeight - 100 ],
		btn : biolims.common.selected,
		content : [
				window.ctx + "/dic/type/dicTypeSelectTable.action?flag=test",
				'' ],
		yes : function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find(
					"#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find(
					"#addDicTypeTable .chosed").children("td").eq(0).text();
			top.layer.close(index)
			$("#testMethodId").val(id);
			$("#testMethodName").val(name);
		},
	})
}
// 选择负责组
function choseUserGroup() {
	top.layer.open({
		title : "选择负责组",
		type : 2,
		area : [ document.body.clientWidth - 300,
				document.body.clientHeight - 100 ],
		btn : biolims.common.selected,
		content : [ window.ctx + "/core/userGroup/userGroupSelTable.action" ],
		yes : function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find(
					"#addUserGroup .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find(
					"#addUserGroup .chosed").children("td").eq(0).text();
			top.layer.close(index)
			$("#userGroupId").val(id);
			$("#userGroupName").val(name);
		},
	})
}
// 上传附件模态框出现
function fileUp() {
	if($("#supplier_id").val()=="NEW"){
		top.layer.msg("请先保存数据再上传附件！")
		return false;
	}
	$("#uploadFile").modal("show");
}
// 查看附件
function fileView() {
	top.layer.open({
		title : "附件",
		type : 2,
		skin : 'layui-top.layer-lan',
		area : [ document.body.clientWidth - 300,
				document.body.clientHeight - 100 ],
		content : window.ctx
				+ "/operfile/initFileList.action?flag=1&modelType=supplier&id="
				+ $("#supplier_id").val(),
		cancel : function(index, layer) {
			top.layer.close(index)
		}
	})
}
// 改变状态
/*
 * function changeState(that) { var state = $(that).val(); if (state == "0e") {
 * $("#stateName").val("无效"); } else { $("#stateName").val("有效"); } }
 */
function changeState(that) {
	var state = $(that).val();
	if (state == "0e") {
		$("#supplier_state_id").val("不可用");
	} else {
		$("#supplier_state_id").val("可用");
	}
}
// 选择流程
function choseLC() {
	top.layer
			.open({
				title : "选择流程",
				type : 2,
				area : [ document.body.clientWidth - 300,
						document.body.clientHeight - 100 ],
				btn : biolims.common.selected,
				content : [ window.ctx
						+ "/com/biolims/system/work/workOrder/workOrderSelect.action" ],
				yes : function(index, layer) {
					var name = $('.layui-layer-iframe', parent.document).find("iframe")
							.contents().find("#addWorkOrder .selected")
							.children("td").eq(2).text();
					var id = $('.layui-layer-iframe', parent.document).find("iframe").contents()
							.find("#addWorkOrder .selected").children("td").eq(
									1).text();
					top.layer.close(index)
					$("#workOrderId").val(id);
					$("#workOrderName").val(name);
				},
			})
}
// 查看流程图
function viewLC() {
	var workOrderId = $("#workOrderId").val();
	if (!workOrderId) {
		top.layer.msg("请先选择实验流程");
		return false;
	}
	top.layer
			.open({
				title : "查看流程",
				type : 2,
				area : [ document.body.clientWidth - 300,
						document.body.clientHeight - 100 ],
				btn : biolims.common.selected,
				content : [ window.ctx
						+ "/com/biolims/system/product/viewWorkOrder.action?workOrderId="
						+ workOrderId ],
				yes : function(index, layer) {
					top.layer.close(index)
				},
			})
}


// 获取描述和容器数量he模板sop的修改日志
function getChangeLog(userId, changeLog) { // 获取待处理样本的修改日志
	/*
	 * var sampleNew = []; $(".selected").each(function(i, val) {
	 * sampleNew.push($(val).children("td").eq(1).text()); }); changeLog +=
	 * '"待处理样本"新增有：' + sampleNew + ";";
	 */
	// 获取描述和容器数量的修改日志
	$('input[class="form-control"]').each(
			function(i, v) {
				var valnew = $(v).val();
				var val = $(v).attr("changelog");
				if (val !== valnew) {
					changeLog += $(v).prev("span").text() + ':由"' + val
							+ '"变为"' + valnew + '";';
				}
			});
	return changeLog;
}
//成本中心
function showScope() {
	top.layer.open({
		title: biolims.user.selectCostCentre,
		type: 2,
		area: ["60%", "65%"],
		btn: biolims.common.selected,
		content: window.ctx + "/core/user/showCostCenter.action",
		yes: function(index, layer) {
			var id = [],
				name = [];
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#costCenter input").each(function(i, v) {
				if($(v).is(':checked')) {
					id.push(v.value);
					name.push(v.title);
				}
			});
			$("#supplier_scopeName").val(name.join(","));
			$("#supplier_scopeId").val(id.join(","));
			top.layer.close(index);
		},
		cancel: function(index, layer) {
			top.layer.close(index);
		}
	});
}