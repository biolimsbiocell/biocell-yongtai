var supplierDialogGrid;
$(function(){
	var cols={};
	cols.sm=true;
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:"供应商编码",
		width:100,
		sortable:true,
		hidden:true
	});	
	cm.push({
		dataIndex:'name',
		header:"供应商名称",
		width:100,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/supplier/showDialogSupplierListByStoragNameJson.action?name="+$("#commodityName").val();
	var opts={};
	opts.tbar = [];
	opts.title="供应商";
	opts.height=document.body.clientHeight-100;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
	};
	opts.tbar.push({
		text:"填加明细",
		handler:null
	})
	opts.tbar.push({
		text:"取消选中",
		handler:null
	})
	opts.tbar.push({
		text:"显示可编辑列",
		handler:null
	})
	supplierDialogGrid=gridEditTable("show_dialog_supplier_div",cols,loadParam,opts);
});
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(crmCustomerDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
