$(function() {
	$.ajax({
		type: "post",
		url: ctx + "/core/user/showCostCenterJson.action",
		success: function(data) {
			var data=JSON.parse(data);
			var str = '';
			if(data.length) {
				for(var i = 0; i < data.length; i++) {
					str += '<div class="col-xs-6">' +
						'<label>' +
						'<input type="checkbox" title="'+ data[i].name+'" value="' + data[i].id + '">' + data[i].name +
						'</label>' +
						'</div>';
				}
				$("#costCenter").append(str);
				$("#costCenter input").iCheck({
					checkboxClass: 'icheckbox_square-blue',
					increaseArea: '20%' // optional
				});
			} else {
				top.layer.msg("没有数据，请联系管理员！");
			}
		}
	});
});