Ext.onReady(function() {
	var costCenterJsonPath = $("#costCenterJsonPath").val();
	Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
	var treeGrid = new Ext.ux.tree.TreeGrid({
		id : 'treeGrid',
		width : document.body.clientWidth,
		height : document.body.clientHeight,
		renderTo : 'markup',
		enableDD : true,
		autoExpandColumn : 'common',
		columnLines : true,

		columns : [ {
			header : 'ID',
			dataIndex : 'id',
			width : 150
		}, {
			header : '名称',
			width : 200,
			dataIndex : 'name'

		}, {
			id : 'common',
			header : '说明',
			width : 300,
			dataIndex : 'note'
		} ],
		dataUrl : costCenterJsonPath,
		listeners : {
			click : function(n) {
				document.getElementById("id").value = n.attributes.id;
				document.getElementById("leaf").value = n.attributes.leaf;
				setvalue(n.attributes.id, n.attributes.name);
			}
		}

	});

});