Ext.onReady(function() {
		Ext.QuickTips.init();
		var costCenterJsonPath = $("#costCenterJsonPath").val();
		var treeGrid = new Ext.ux.tree.TreeGrid({
			id : 'treeGrid',
			width : parent.document.body.clientWidth,
			height : parent.document.body.clientHeight,
			renderTo : 'markup',
			enableDD : true,
			autoExpandColumn : 'common',
			columnLines : true,
			autoScroll : true,
			sorters : [ {
				property : 'orderNumber',
				direction : 'ASC'
			} ],
			columns : [ {
				header : 'ID',
				dataIndex : 'id',
				width : 300
			}, {
				header : '明细项',
				dataIndex : 'name',
				width : 300
			}, {
				id : 'common',
				header : '说明',
				width : 500,

				dataIndex : 'note'
			}, {
				id : 'upCostCenter',
				header : '上级科目',
				width : 100,
				hidden : true,
				dataIndex : 'upCostCenter'
			}, {
				id : 'department',
				header : '部门',
				width : 100,
				hidden : true,
				dataIndex : 'department'
			}, {
				header : '排序号',
				dataIndex : 'orderNumber',
				width : 300
			} ],
			tbar : [
					{
						text : '填加',
						handler : function() {
							var form = new Ext.FormPanel({
								// title : '基本信息',
								labelWidth : 85,
								frame : true,
								defaultType : 'textfield',
								bodyStyle : 'padding:5px 5px 0',

								width : 400,
								height : 280,
								defaults : {
									width : 230
								},

								items : [ {
									fieldLabel : 'ID',
									name : 'id',
									id : 'id'

								}, {

									fieldLabel : '明细项',
									name : 'name',
									id : 'name',
									allowBlank : false

								}, {

									fieldLabel : '上级ID',
									name : 'upCostCenter',
									id : 'upCostCenter',
									value : document.getElementById("id").value

								}, {

									fieldLabel : '上级名称',
									name : 'upCostCenterName',
									id : 'upCostCenterName',
									value : document.getElementById("name").value,
									readOnly : true,
									hidden : false

								}, {

									fieldLabel : '部门',
									name : 'department',
									id : 'department',

									readOnly : true,
									hidden : false,
							          listeners:{
			 								"focus" : function(){
			 	            				showDepartment();
			 							}
			 						}

								}, {
									xtype : 'textarea',
									fieldLabel : '说明',
									name : 'note',
									id : 'note'
								}, {
									fieldLabel : '排序号',
									name : 'orderNumber',
									id : 'orderNumber'
								} ]
							});
							var w = new Ext.Window({
								title : '填加明细',
								modal : true,
								collapsible : true,
								maximizable : true,
								width : 450,
								height : 450,
								minWidth : 300,
								minHeight : 200,
								layout : 'fit',
								plain : true,
								bodyStyle : 'padding:5px;',
								buttonAlign : 'center',
								items : form,
								buttons : [
										{
											text : '保存',
											handler : function() {
												if (form.getForm().isValid()) {
													var data = "{\"note\":\"" + Ext.getCmp("note").getValue()
															+ "\",\"department-id\":\""
															+ Ext.getCmp("department").getValue() + "\",\"name\":\""
															+ Ext.getCmp("name").getValue() + "\",\"id\":\""
															+ Ext.getCmp("id").getValue() + "\",\"orderNumber\":\""
															+ Ext.getCmp("orderNumber").getValue()
															+ "\",\"upCostCenter-id\":\""
															+ Ext.getCmp("upCostCenter").getValue() + "\",\"note\":\""
															+ Ext.getCmp("note").getValue() + "\"}";
													modifyData("data:'" + data + "'");

												}
												form.getForm().reset();
											}
										}, {
											text : '关闭',
											handler : function() {
												w.close();
											}
										} ]
							});
							w.show();

						}
					},
					{
						text : '编辑',
						handler : function() {
							var n = treeGrid.getSelectionModel().getSelectedNode();

							var form = new Ext.FormPanel({
								labelWidth : 85,
								frame : true,
								defaultType : 'textfield',
								bodyStyle : 'padding:5px 5px 0',
								width : 400,
								height : 280,
								defaults : {
									width : 230
								},

								items : [ {
									fieldLabel : 'ID',
									name : 'id',
									id : 'id',
									hidden : true,
									value : n.attributes.id
								}, {

									fieldLabel : '明细项',
									name : 'name',
									id : 'name',
									allowBlank : false,
									value : n.attributes.name
								}, {

									fieldLabel : '上级ID',
									name : 'upCostCenter',
									id : 'upCostCenter',
									value : n.attributes.upCostCenter

								}, {

									fieldLabel : '部门',
									name : 'department',
									id : 'department',
									value : n.attributes.department,
									  listeners:{
			 								"focus" : function(){
			 	            				showDepartment();
			 							}
			 						}

								}, {
									xtype : 'textarea',
									fieldLabel : '说明',
									name : 'note',
									id : 'note',
									value : n.attributes.note
								}, {
									fieldLabel : '排序号',
									name : 'orderNumber',
									id : 'orderNumber',
									value : n.attributes.orderNumber
								} ]
							});
							var w = new Ext.Window({
								title : '明细',
								modal : true,
								collapsible : true,
								maximizable : true,
								width : 450,
								height : 450,
								minWidth : 300,
								minHeight : 200,
								layout : 'fit',
								plain : true,
								bodyStyle : 'padding:5px;',
								buttonAlign : 'center',
								items : form,
								buttons : [
										{
											text : '保存',
											handler : function() {
												if (form.getForm().isValid()) {
													var data = "{\"note\":\"" + Ext.getCmp("note").getValue()
															+ "\",\"department-id\":\""
															+ Ext.getCmp("department").getValue() + "\",\"name\":\""
															+ Ext.getCmp("name").getValue() + "\",\"id\":\""
															+ Ext.getCmp("id").getValue() + "\",\"orderNumber\":\""
															+ Ext.getCmp("orderNumber").getValue()
															+ "\",\"upCostCenter-id\":\""
															+ Ext.getCmp("upCostCenter").getValue() + "\",\"note\":\""
															+ Ext.getCmp("note").getValue() + "\"}";
													modifyData("data:'" + data + "'");

												}

											}
										}, {
											text : '关闭',
											handler : function() {
												w.close();
											}
										} ]
							});
							w.show();
						}
					}, {
						text : '删除',
						handler : function() {
							var str = "id:'" + document.getElementById("id").value + "'";
							delData(str);
						}
					}, {
						text : '重载',
						id : 'reloadtree',
						tooltip : '重载数据并选择上一次的节点',
						scope : this,
						handler : function(b, e) {

							var path = treeGrid.getSelectionModel().getSelectedNode().getPath('id');
							//重载数据,并在回调函数里面选择上一次的节点  
							treeGrid.getLoader().load(treeGrid.getRootNode(), function(treeNode) {
								//展开路径,并在回调函数里面选择该节点  
								treeGrid.expandPath(path, 'id', function(bSucess, oLastNode) {
									treeGrid.getSelectionModel().select(oLastNode);
								});
							}, this);
						}
					}, {
						text : '重载全部',
						id : 'reloadtreeall',
						tooltip : '重载数据',
						scope : this,
						handler : function(b, e) {

							treeGrid.getLoader().load(treeGrid.getRootNode(), this);
						}
					}

			],

			dataUrl : costCenterJsonPath,
			listeners : {
				click : function(n) {

					document.getElementById("id").value = n.attributes.id;
					document.getElementById("name").value = n.attributes.name;
					document.getElementById("leaf").value = n.attributes.leaf;
				}
			}

		});

		//Ext.getCmp('treeGrid').getRootNode().expand(true);

	});