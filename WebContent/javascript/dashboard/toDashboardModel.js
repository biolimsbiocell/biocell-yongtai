$(function() {
	var root = new Ext.tree.AsyncTreeNode({
		id : "00000000000000000000000000000000",
		text : "root",
		singleClickExpand : true
	});

	var loader = new Ext.tree.TreeLoader({
		dataUrl : ctx + "/dashboard/queryDashboardModel.action"
	});

	loader.on("beforeload", function(treeLoader, node) {
		treeLoader.baseParams.type = node.attributes.moduleType;
	}, this);
	modelTree = new Ext.tree.TreePanel({
		renderTo : "dashboard_model_div",
		root : root,
		width : 310,
		height : 300,
		loader : loader,
		bbar : [ {
			// pressed:true,
			text : biolims.common.expandAll,
			handler : function() {
				modelTree.expandAll();
			}
		}, {
			xtype : "tbseparator"
		}, {
			// pressed:true,
			text : biolims.common.collapseAll,
			handler : function() {
				modelTree.collapseAll();
			}
		} ],
		containerScrol : true,
		autoScroll : true,
		rootVisible : false
	});

	$("#dashboard_model_div").data("modelTree", modelTree);
});