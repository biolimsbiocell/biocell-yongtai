$(function() {

	$("#add-widget-btn").button().click(function() {
		var url = "/dashboard/toDashboardModel.action";
		var option = {};
		option.width = 334;
		option.height = 394;
		if (window.ActiveXObject) {
			// IE浏览器
			//option.height = option.height + "px";
		}
		loadDialogPage(null, biolims.dashboard.selectComponent, url, {
			"Confirm" : function() {
				var modelTree = $("#dashboard_model_div").data("modelTree");
				var notes = modelTree.getChecked();
				if (notes.length <= 0) {
					message(biolims.dashboard.selectComponent);
					return;
				}

				var oper = true;
				var models = [];
				$.each(notes, function(i, obj) {
					if (obj.attributes.level == 1) {
						oper = false;
						return false;
					}
					models.push(obj.attributes.id);
				});

				if (!oper) {
					message(biolims.dashboard.selectOneComponent);
					return;
				}

				var setting = {};
				setting.dataType = "json";
				setting.type = "POST";
				setting.data = {};
				setting.data.models = models;
				setting.url = "/dashboard/saveDashboard.action";
				ajax("post", setting.url, setting.data, function(data) {
					var result = data;
					if (result.success) {
						//$(obj).dialog("close");
						var url = window.ctx+"/dashboard/toDashboard.action";
						window.location = url;
					} else {
						message(biolims.dashboard.selectComponentError);
					}
				});

			}
		}, true, option);
	});

	$("#change-layout-btn").button().click(function() {
		var url = "/dashboard/editLayout.action";
		var option = {};
		option.width = 742;
		option.height = 312;
		if (window.ActiveXObject) {
			// IE浏览器
			//option.height = option.height + "px";
		}
		loadDialogPage(null, biolims.dashboard.modifyModel, url, {
			"Confirm" : function() {
				modifyLayout(this);
			}
		}, true, option);
	});

	function modifyLayout(obj) {
		var layoutNameChk = document.getElementsByName("layoutNameChk");
		var layoutId = '';
		for ( var i = 0; i < layoutNameChk.length; i++) {
			if (layoutNameChk[i].checked) {
				layoutId = layoutNameChk[i].value;
				break;
			}
		}
		if (layoutId.length == 0) {
			message(biolims.dashboard.modifyModelPlease);
			return;
		}
		var setting = {};
		setting.dataType = "json";
		setting.type = "POST";
		setting.data = {};
		setting.data.layoutId = layoutId;
		setting.url = "/dashboard/saveLayout.action";
		ajax("post", setting.url, setting.data, function(data) {
			var result = data;
			if (result.success) {
				$(obj).dialog("close");
				var url = window.ctx+"/dashboard/toDashboard.action";
				window.location = url;
			} else {
				message(biolims.dashboard.modifyModelError);
			}
		});
	}

	$(".widget-content").each(function(i, obj) {
		var dashboardId = $(obj).attr("dashboardId");
		var isFrame = $(obj).attr("is-frame");
		if (isFrame == 0) {
			var str = $("#" + dashboardId + "Config").val();
			if (str.length <= 0) {
				var confURL = $(obj).attr("config-url");
				if (confURL == '#') {
					confURL = $(obj).attr("module-url");
				}
				load(confURL, {
					widgetId : dashboardId
				}, obj);
			} else {
				var url = $(obj).attr("module-url");
				var config = $.parseJSON(str);
				load(url, config, obj, null);
			}
		} else {
			$(obj).css("height", "400px");
			var frame = $("iframe", $(obj));
			$(frame).attr("width", $(obj).width() - 3);
			var murl;
			if($(obj).attr("module-url").indexOf("?")==-1){
				murl = $(obj).attr("module-url") + "?userId="+window.userId+"&groupId="+window.groupIds;;
			}else{
				murl = $(obj).attr("module-url") + "&userId="+window.userId+"&groupId="+window.groupIds;;
			}
			$(frame).attr("src", murl);
			$(frame).attr("height", "400px");
		}

	});

	$(".widget-header a.setting").click(function() {
		var widget = $(this).parents(".widget");
		// 从widget的属性上读取参数值，例如var dashboardId = widget.attr("dashboardId");
		// 加载设置页面到当前组件的窗口内，例如load(URL, 参数, widget.find(".widget-content"));
		var dashboardId = widget.attr("dashboardId");
		var confURL = widget.attr("config-url");
		var str = $("#" + dashboardId + "Config").val();
		var config = {};
		if (str.length > 0) {
			config = $.parseJSON(str);
		}
		if (confURL != '#') {
			// load(confURL, config, widget.find(".widget-content"));
		}
	});

	$(".widget-header a.close").click(function() {

		if (!confirm(biolims.dashboard.removeModelConfirm)) {
			return;
		}

		var widget = $(this).parents(".widget");
		// 从widget的属性上读取参数值，例如var dashboardId = widget.attr("dashboardId");
		// 根据参数发送ajax请求删除组件，例如ajax(...);ajax的回调函数中判断结果如果成功，则需要调用widget.remove();
		var dashboardId = widget.attr("dashboardId");
		var setting = {};
		setting.data = {};
		setting.data.dashboardId = dashboardId;
		setting.url = "/dashboard/deleteDashboard.action";
		ajax("post", setting.url, setting.data, function(data) {
			var result = data;
			if (result.success) {
				widget.remove();
			} else {
				message(biolims.dashboard.removeModelError);
			}

		});
	});

	$("#dashboard-widgets-content .col").disableSelection().sortable({
		connectWith : "#dashboard-widgets-content .col",
		stop : function(event, ui) {
			ui.item.css("zoom", "0.9").css("zoom", "1");
			var list = "";
			$.each($(".col"), function(m) {
				list += $(this).attr('id') + ":";
				$.each($(this).children(".widget-model"), function(d) {
					list += $(this).attr('dashboardId') + "@";
				});
				if (/@$/.test(list)) {
					list = list.substring(0, list.length - 1);
				}
				list += "|";
			});
			if (/\|$/.test) {
				list = list.substring(0, list.length - 1);
			}
			// alert(list)
			var setting = {};
			setting.data = {};
			setting.data.info = list;
			setting.url = "/dashboard/modifyLayoutNum.action";
			ajax("post", setting.url, setting.data, function(data) {
				if (!data.success) {
					message(biolims.dashboard.modifyModelError);
				}
			});
		}
	});
});