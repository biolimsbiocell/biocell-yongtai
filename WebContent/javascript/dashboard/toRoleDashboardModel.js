$(function() {
	var root = new Ext.tree.AsyncTreeNode({
		id : "00000000000000000000000000000000",
		text : "root",
		singleClickExpand : true
	});

	var loader = new Ext.tree.TreeLoader({
		dataUrl : ctx + "/dashboard/queryRoleDashboardModel.action"
	});

	loader.on("beforeload", function(treeLoader, node) {
		treeLoader.baseParams.type = node.attributes.moduleType;
		treeLoader.baseParams.roleId = $("#role_id").val();
	}, this);
	modelTree = new Ext.tree.TreePanel({
		renderTo : "dashboard_model_div",
		root : root,
		width : 400,
		height : document.body.clientHeight - 130,
		loader : loader,
		title : biolims.dashboard.component,
		bbar : [ {
			// pressed:true,
			text : biolims.common.expandAll,
			handler : function() {
				modelTree.expandAll();
			}
		}, {
			xtype : "tbseparator"
		}, {
			// pressed:true,
			text : biolims.common.collapseAll,
			handler : function() {
				modelTree.collapseAll();
			}
		}, {
			xtype : "tbseparator"
		}, {
			// pressed : true,
			id : 'saveAllModify',
			text : biolims.common.save,
			handler : function() {
				var notes = modelTree.getChecked();
				if (notes.length <= 0) {
					message(biolims.dashboard.setComponent);
					return;
				}

				var models = [];
				$.each(notes, function(i, obj) {
					models.push(obj.attributes.id);
				});

				data = {};
				data.models = models;
				data.roleId = $("#role_id").val();
				url = "/dashboard/saveRoleDashboardModel.action";
				ajax("post", url, data, function(data) {
					var result = data;
					if (result.success) {
						message(biolims.common.saveSuccess);
					} else {
						message(biolims.common.saveFailed);
					}
				});
			}
		} ],
		containerScrol : true,
		autoScroll : true,
		rootVisible : false
	});
	modelTree.expandAll();
	$("#dashboard_model_div").data("modelTree", modelTree);
});