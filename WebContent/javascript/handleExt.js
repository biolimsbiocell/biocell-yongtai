
/*
 *
 * Bio-LIMS Software Confidential
 *
 * (C) COPYRIGHT Bio-LIMS Software, Inc.
 *
 * The source code for this program is not published or otherwise
 * divested of its trade secrets, irrespective of what has been
 * deposited with.
 *
 */

Ext.UpdateManager.defaults.indicatorText = '<div class="loading-indicator">'+biolims.common.loading+'</div>';


Date.monthNames =
   [biolims.common.jan,
    biolims.common.feb,
    biolims.common.mar,
    biolims.common.apr,
    biolims.common.may,
    biolims.common.jun,
    biolims.common.jul,
    biolims.common.aug,
    biolims.common.sep,
    biolims.common.oct,
    biolims.common.nov,
    biolims.common.dec];

Date.dayNames =
   [biolims.common.sun,
    biolims.common.mon,
    biolims.common.tues,
    biolims.common.wed,
    biolims.common.thur,
    biolims.common.fri,
    biolims.common.sat];
    
Ext.apply(Ext.grid.GridView.prototype, {
sortAscText: biolims.common.ascend,
sortDescText: biolims.common.descend,
lockText: biolims.common.lock,
unlockText: biolims.common.unlock,
columnsText: biolims.common.column
});

Ext.apply(Ext.DatePicker.prototype, {
    todayText : biolims.common.todayText,
    todayTip : biolims.common.todayTip,
    minText : biolims.common.minText,
    maxText : biolims.common.maxText,
    format : "y-m-d",
    disabledDaysText : "",
    disabledDatesText : "",
    monthNames : Date.monthNames,
    dayNames : Date.dayNames,
    nextText: biolims.common.nextText,
    prevText: biolims.common.prevText,
    monthYearText: biolims.common.monthYearText,
    startDay: 1 // Week start on Monday
});

Ext.apply(Ext.form.DateField.prototype, {
	
	
	selectOnFocus:true,
	showToday : true,
	invalidText :biolims.common.invalidText , 
	editable:false,
    format: "Y-m-d"
});

Ext.MessageBox.buttonText = 
{
    ok : biolims.common.confirm,
    cancel : biolims.common.cancel,
    yes : biolims.common.yes,
    no : biolims.common.no
};

Ext.apply(Ext.PagingToolbar.prototype, {
    beforePageText : biolims.common.beforePageText,
    afterPageText : biolims.common.afterPageText,
    firstText : biolims.common.firstText,
    prevText : biolims.common.prevText,
    nextText : biolims.common.nextText,
    lastText : biolims.common.lastText,
    refreshText : biolims.common.rearranging
});

//Ext.TabPanelItem.prototype.closeText = "关闭此标签";

Ext.apply(Ext.form.ComboBox.prototype, {
	editable:false,
    loadingText: biolims.common.loading,
    typeAhead: true,
    
    mode: 'local',
    forceSelection: true,
    triggerAction: 'all',

    selectOnFocus:true	
});



function GridPanel(dataStore,dataColumns,pSize)
{
    /// <summary>
    /// Ext.form.ComboBox封装
    /// </summary>
    /// <param name="dataStore">store</param>
    /// <param name="dataColumns">cm</param>
    /// <param name="pSize">pageSize 默认15</param>
    /// <returns>Ext.grid.GridPanel</returns>
    var grid = new Ext.grid.GridPanel({
        store: dataStore,
        cm: dataColumns, 
        
        autoScroll: true,
        autoHeight: true,
        border: true,
        disableSelection: true,
        frame: true,
        loadMask: { msg: biolims.common.loadingData },     //loadMask: true,
        region: 'center',
        stripeRows: true,
        width: '100%',
        enableHdMenu:false,

        //设置单行选中模式
        selModel: new Ext.grid.RowSelectionModel({ singleSelect: false }),

        //使列自动均分
        viewConfig: {
            forceFit: true
        },
        
        //底部分页工具条
        bbar: new Ext.PagingToolbar({
            pageSize: GridPanelPageSize(pSize),
            store: dataStore,
            displayInfo: true,
            emptyMsg: biolims.common.noRecord
        })
    });
    //GridPanelPageSize(pSize)
   
    return grid;
}

function GridPanelPageSize(pSize)
{
    if(pSize!=null && !isNaN(pSize))
        return pSize;
    else
        return 15;
}

function NumberField(fLable,_name)
{
    var nf = new Ext.form.NumberField({
        fieldLabel:fLable,
        name:_name,
        allowDecimals: false, // 允许小数点
        allowNegative: false // 允许负数
    });
    
    
    return nf;
}



function DateField(fLable,nowValue,aBlank,bText,eText)
{
    /// <summary>
    /// Ext.form.DateField封装
    /// </summary>
    /// <param name="fLable">fieldLabel</param>
    /// <param name="nowValue">默认为当前时间</param>
    /// <param name="aBlank">allowBlank</param>
    /// <param name="bText">blankText</param>
    /// <param name="eText">emptyText</param>
    /// <returns>Ext.form.DateField</returns>
    var df = new Ext.form.DateField({
        fieldLabel:fLable,
        format:"Y-m-d",
        anchor:"95%",
        editable : false   // 不允许输入
    });
    if(nowValue!=null)
    {
        if(nowValue == true)
            df.setValue(new Date().dateFormat('Y-m-d'));
        else
        {
            var date = nowValue.split("-");
            if(date.length==3)
                df.setValue(new Date(date[0],parseInt(date[1])-1,date[2]).dateFormat('Y-m-d'));
        }
    }
    if(aBlank!=null)
        df.allowBlank = aBlank;
    if(bText!=null)
        df.blankText = bText;
    if(eText!=null)
        df.emptyText = eText;
        
    return df;
}

function ComboBox(hName,dataStore,applyToName)
{
    /// <summary>
    /// Ext.form.ComboBox封装
    /// </summary>
    /// <param name="hName">hiddenName</param>
    /// <param name="fLabel">fieldLabel</param>
    /// <param name="dataStore">store</param>
    /// <param name="defaultValue">value</param>
    /// <returns>Ext.form.ComboBox</returns>
    var combo = new Ext.form.ComboBox({
        hiddenName: hName,
        displayField: "name",        
        valueField: "id",
        //lazyRender: true,
        mode: 'local',
        editable : false,
        
        typeAhead: true,
        forceSelection: true,   // 必须选择一项
        triggerAction: 'all',
        selectOnFocus:true,
        applyTo:applyToName
        
    });
    
    //设置数据源
    if(Ext.isArray(dataStore))
    {
        combo.store = new Ext.data.SimpleStore({
            fields: ['id', 'name'],
            data:dataStore
        });
        //设置默认值
        if(defaultValue!=null)
        {
            combo.setValue(defaultValue);   
        }
    }
    else
    {
        combo.store = dataStore;
        combo.mode = 'remote';
    }
    
    return combo;
}


function ProcessBar(_title,_msg,_width,_wait,_interval)
{
    var msg_pro = Ext.MessageBox.show({
        title: biolims.common.pleaseWait,
        msg: biolims.common.submittingData,
        width:300,
        wait:true,
        waitConfig: {interval:100}
    });
       
    if(_title!=null)
        msg_pro.title = _title;
        
    if(_msg!=null)
        msg_pro.msg = _msg;
        
    if(_width!=null)
        msg_pro.width = _width;
        
    if(_wait!=null)
        msg_pro.wait = _wait;
        
    if(_interval!=null)
        msg_pro.waitConfig.interval = _interval;
                       
      return msg_pro;
}

function TextField(fName,fLabel,defaultValue)
{
    /// <summary>
    /// Ext.form.TextField封装
    /// </summary>
    /// <param name="fName">name</param>
    /// <param name="fLabel">fieldLabel</param>
    /// <param name="defaultValue">默认值</param>
    /// <returns>Ext.form.TextField</returns>
    var text = new Ext.form.TextField();
    
    if(fName!=null)
        text.name = fName;
    
    if(fLabel!=null)
        text.fieldLabel = fLabel;
    
    //设置默认值
    if(defaultValue != null)
        text.setValue(defaultValue);
    
    return text;
}
//#endregion

//#region Ext.form.TextField
function NumberField(fName,fLabel,defaultValue,allowDecimals,allowNegative,maxValue,minValue)
{
    /// <summary>
    /// Ext.form.NumberField封装
    /// </summary>
    /// <param name="fName">name</param>
    /// <param name="fLabel">fieldLabel</param>
    /// <param name="defaultValue">默认值</param>
    /// <param name="allowDecimals">是否允许小数点</param>
    /// <param name="allowNegative">是否允许负数</param>
    /// <param name="maxValue">最大值</param>
    /// <param name="minValue">最小值</param>
    /// <returns>Ext.form.NumberField</returns>
    var number = new Ext.form.NumberField();
    
    if(fName!=null)
        number.name = fName;
    
    if(fLabel!=null)
        number.fieldLabel = fLabel;
    
    //设置默认值
    if(defaultValue != null && typeof(defaultValue) == "number")
        number.setValue(defaultValue);
    
    //设置是否允许小数点，默认(不设置)为不允许
    if(allowDecimals != null && typeof(allowDecimals) == "boolean")
        number.allowDecimals = Boolean(allowDecimals);
        
    //设置是否允许负数，默认(不设置)为不允许
    if(allowNegative != null && typeof(allowNegative) == "boolean")
        number.allowNegative = Boolean(allowNegative);     
    
    //设置最大值
    if(maxValue != null && typeof(maxValue) == "number")
        number.maxValue = Number(maxValue);     
        
    //设置最小值
    if(minValue != null && typeof(minValue) == "number")
        number.minValue = Number(minValue);
    
    return number;
}
//#endregion

//#region Ext.form.DateField
function DateField(fName,fLabel,defaultValue,format,editable)
{
    /// <summary>
    /// Ext.form.DateField封装
    /// </summary>
    /// <param name="fName">name</param>
    /// <param name="fLabel">fieldLabel</param>
    /// <param name="defaultValue">Boolean:true为当前日期/String: 按'Y-m-d'格式，如2009-1-1</param>
    /// <param name="format">设置日期格式化字符串</param>
    /// <param name="editable">是否允许输入，还是只能选择日期</param>
    /// <returns>Ext.form.DateField</returns>
    var date = new Ext.form.DateField();
    
    if(fName!=null)
        date.name = fName;
    
    if(fLabel!=null)
        date.fieldLabel = fLabel;
    
    //设置日期格式化字符串
    if(format == null)
        format = 'Y-m-d';
    date.format = format;
    
    //设置默认日期
    if(defaultValue != null)
    {
        if(typeof(defaultValue) == "boolean" && Boolean(defaultValue) == true)
        {
            date.setValue(new Date().dateFormat(format));
        }
        else if(typeof(defaultValue) == "string")
        {
            var strDate =String(defaultValue).split("-");
            if(strDate.length==3)
                date.setValue(new Date(strDate[0],parseInt(strDate[1])-1,strDate[2]).dateFormat(format));
        }
    }
    
    //是否允许编辑
    if(editable==null)
        editable = false;
    else if(typeof(editable) == "boolean")
        editable = Boolean(editable);
    
    date.editable = editable;
    
    return date;
}
//#endregion

//#region Ext.form.TimeField

function TimeField(fName,fLabel,increment,format)
{
    /// <summary>
    /// Ext.form.TimeField封装
    /// </summary>
    /// <param name="fName">name</param>
    /// <param name="fLabel">fieldLabel</param>
    /// <param name="increment">设置时间间隔，单位为分钟</param>
    /// <param name="format">格式化输出，支持格式如下："g:ia|g:iA|g:i a|g:i A|h:i|g:i|H:i|ga|ha|gA|h a|g a|g A|gi|hi|gia|hia|g|H"</param>
    /// <returns>Ext.form.TimeField</returns>
    var time = new Ext.form.TimeField();
    
    if(fName!=null)
        time.name = fName;
    
    if(fLabel!=null)
        time.fieldLabel = fLabel;
        
    //设置时间间隔 默认为15分钟
    if(increment!=null && typeof(increment) == "number")
        time.increment = Number(increment);
        
    //设置格式化输出 默认为 "g:i A"
    if(format != null && typeof(format) == "string")
        time.format = String(format);
    
    return time;
}

//#endregion

//#region Ext.form.ComboBox
function ComboBox(fName,fLabel,dataStore,defaultValue,displayField,valueField,editable)
{
    /// <summary>
    /// Ext.form.ComboBox封装
    /// </summary>
    /// <param name="fName">name</param>
    /// <param name="fLabel">fieldLabel</param>
    /// <param name="dataStore">数据源。本地模式，如[[1,'男'],[0,'女']]；远程模式，传入Ext.data.Store</param>
    /// <param name="defaultValue">默认值</param>
    /// <param name="displayField">选项的文本对应的字段</param>
    /// <param name="valueField">选项的值对应的字段</param>
    /// <param name="editable">是否可以输入,还是只能选择下拉框中的选项</param>
    /// <returns>Ext.form.ComboBox</returns>
    var combo = new Ext.form.ComboBox({
        mode: 'local',
        editable : false,
        typeAhead: true,
        triggerAction: 'all',
        selectOnFocus:true
    });
    
    if(fName!=null)
        combo.name = fName;
    
    if(fLabel!=null)
        combo.fieldLabel = fLabel;
        
    if(displayField==null || typeof(displayField) != "string")
        displayField = "Name";
    combo.displayField = String(displayField);
        
    if(valueField==null || typeof(valueField) != "string")
        valueField = "Id";
    combo.valueField = String(valueField);    
    
    //设置数据源
    if(Ext.isArray(dataStore))
    {
        combo.store = new Ext.data.SimpleStore({
            fields: [valueField, displayField],
            data:dataStore
        });
    }
    else
    {
        combo.store = dataStore;
        combo.mode = 'remote';
    }
    
    //设置默认值
    if(defaultValue!=null)
        combo.setValue(defaultValue);
    
    //是否允许编辑
    if(editable!=null && typeof(editable) == "boolean")
        combo.editable = Boolean(editable);  
    
    return combo;
}
//#endregion

//#region Ext.form.TextArea

function TextArea(fName,fLabel,width,height,value)
{
    /// <summary>
    /// Ext.form.TextArea封装
    /// </summary>
    /// <param name="fName">name</param>
    /// <param name="fLabel">fieldLabel</param>
    /// <param name="width">width</param>
    /// <param name="height">height</param>
    /// <param name="value">value</param>
    /// <returns>Ext.form.TextArea</returns>
    var area = new Ext.form.TextArea();

    if(fName!=null)
        area.name = fName;
    
    if(fLabel!=null)
        area.fieldLabel = fLabel;
        
    if(width!=null && typeof(width) == "number")
        area.width = Number(width);
        
    if(height!=null && typeof(height) == "number")
        area.height = Number(height);
        
    if(value!=null)
        area.value = value;
        
    return area;
}

//#endregion

//#region Ext.form.RadioGroup

function RadioGroup(fName,fLabel,items,columns)
{
    /// <summary>
    /// Ext.form.RadioGroup封装
    ///     例子：new RadioGroup('Gender','性别',[['男','男',true],['女','女']])
    /// <param name="fName">name</param>
    /// <param name="fLabel">fieldLabel</param>
    /// <param name="items">数据，格式如下:[['男','男',true],['女','女']]</param>
    /// <param name="columns">设置几列自动排列，如果是1的话会按一行来显示</param>
    /// </summary>
    /// <returns>Ext.form.RadioGroup</returns>
    var rg = new Ext.form.RadioGroup();
    
    if(fName!=null)
        rg.name = fName;
    
    if(fLabel!=null)
        rg.fieldLabel = fLabel;
    
    if(columns!=null && typeof(columns) == "number")
        rg.columns = Number(columns);
        
    var rebuildiItems = new Array();
        
    if(items !=null){
        for(var i = 0 ;i<items.length; i++)
        {
            rebuildiItems[i] = {};
            rebuildiItems[i].name = fName;
            rebuildiItems[i].boxLabel = items[i][0];
            rebuildiItems[i].inputValue = items[i][1];
            if(items[i].length > 2 && typeof(items[i][2]) == "boolean")
                rebuildiItems[i].checked = Boolean(items[i][2]);
        }
        
        //Ext.form.RadioGroup扩展
        Ext.override(Ext.form.CheckboxGroup, {
            setItems:function(data){
                this.items = data;
            }
        });

        if(rg.setItems){
            rg.setItems(rebuildiItems);
        }        
    }
    
    return rg;
}

//#endregion

//#region Ext.form.CheckboxGroup

function CheckboxGroup(fName,fLabel,items,columns)
{
    /// <summary>
    /// Ext.form.CheckboxGroup封装
    ///     例子：new CheckboxGroup('Gender','性别',[['男','男',true],['女','女']])
    /// <param name="fName">name</param>
    /// <param name="fLabel">fieldLabel</param>
    /// <param name="items">数据，格式如下:[['男','男',true],['女','女']]</param>
    /// <param name="columns">设置几列自动排列，如果是1的话会按一行来显示</param>
    /// </summary>
    /// <returns>Ext.form.CheckboxGroup</returns>
    var cg = new Ext.form.CheckboxGroup();
    
    if(fName!=null)
        cg.name = fName;
    
    if(fLabel!=null)
        cg.fieldLabel = fLabel;
    
    if(columns!=null && typeof(columns) == "number")
        cg.columns = Number(columns);
        
    var rebuildiItems = new Array();
        
    if(items !=null){
        for(var i = 0 ;i<items.length; i++)
        {
            rebuildiItems[i] = {};
            rebuildiItems[i].name = fName;
            rebuildiItems[i].boxLabel = items[i][0];
            rebuildiItems[i].inputValue = items[i][1];
            if(items[i].length > 2 && typeof(items[i][2]) == "boolean")
                rebuildiItems[i].checked = Boolean(items[i][2]);
        }
        
        //Ext.form.RadioGroup扩展
        Ext.override(Ext.form.CheckboxGroup, {
            setItems:function(data){
                this.items = data;
            }
        });

        if(cg.setItems){
            cg.setItems(rebuildiItems);
        }        
    }
    
    return cg;
}

//#endregion

//#region Ext.form.HtmlEditor

function HtmlEditor(fName,fLabel,width,height,value)
{
    /// <summary>
    /// Ext.form.HtmlEditor封装
    /// </summary>
    /// <param name="fName">name</param>
    /// <param name="fLabel">fieldLabel</param>
    /// <param name="width">width</param>
    /// <param name="height">height</param>
    /// <param name="value">value</param>
    /// <returns>Ext.form.HtmlEditor</returns>
    var editor = new Ext.form.HtmlEditor();

    if(fName!=null)
        editor.name = fName;
    
    if(fLabel!=null)
        editor.fieldLabel = fLabel;
        
    if(width!=null && typeof(width) == "number")
        editor.width = Number(width);
        
    if(height!=null && typeof(height) == "number")
        editor.height = Number(height);
        
    if(value!=null)
        editor.value = value;
        
    return editor;
}

//#endregion

//#region Ext.form.Hidden

function Hidden(fName,value)
{
    /// <summary>
    /// Ext.form.Hidden封装
    /// </summary>
    /// <param name="fName">name</param>
    /// <param name="value">value</param>
    /// <returns>Ext.form.Hidden</returns>
    var hidden = new Ext.form.Hidden();

    if(fName!=null)
        hidden.name = fName;
        
    if(value!=null)
        hidden.value = value;
        
    return hidden;
}

//#endregion

//#region Ext.form.Checkbox

function Checkbox(fName,fLabel,value,boxLabel,inputValue,checked)
{
    /// <summary>
    /// Ext.form.Checkbox封装
    /// </summary>
    /// <param name="fName">name</param>
    /// <param name="fLabel">fieldLabel</param>
    /// <param name="value">value</param>
    /// <param name="boxLabel"></param>
    /// <param name="inputValue"></param>
    /// <param name="checked"></param>
    /// <returns>Ext.form.Checkbox</returns>
    var checkbox = new Ext.form.Checkbox();

    if(fName!=null)
        checkbox.name = fName;
        
    if(fLabel!=null)
        checkbox.fieldLabel = fLabel;
        
    if(value!=null)
        checkbox.value = value;
        
    if(boxLabel!=null && typeof(boxLabel) == "string")
        checkbox.boxLabel = String(boxLabel);
    
    if(inputValue!=null)
        checkbox.inputValue = inputValue;
        
    if(checked!=null && typeof(checked) == "boolean")
        checkbox.checked = Boolean(checked);
        
    return checkbox;
}

//#endregion

//#region Ext.form.FieldSet

function FieldSet(fName,title,items)
{
    /// <summary>
    /// Ext.form.FieldSet封装
    /// </summary>
    /// <param name="fName">fieldLabel</param>
    /// <param name="title">title</param>
    /// <param name="items">数据源</param>
    /// <returns>Ext.form.FieldSet</returns>
    var fieldset = new Ext.form.FieldSet();
    
    if(fName!=null)
        fieldset.fieldLabel = fName;
        
    if(title!=null  && typeof(title) == "string")
        fieldset.title = String(title);
        
    if(items!=null)
        fieldset.add(items);
    
    return fieldset;
}

//#endregion

//#region Ext.Panel

function Panel(title,width,height,frame)
{
    /// <summary>
    /// Ext.Panel封装
    /// </summary>
    /// <param name="title">name</param>
    /// <param name="width">value</param>
    /// <param name="height">height</param>
    /// <param name="frame">frame</param>
    /// <returns>Ext.Panel</returns>
    if(title ==null)
        title = '';  //默认值 如果为此值则不显示标题栏
        
    var panel = new Ext.Panel({
        title:title
    });
        
    if(width!=null && typeof(width) == "number")
        panel.width = Number(width);
        
    if(height!=null && typeof(height) == "number")
        panel.height = Number(height);
        
    if(frame!=null && typeof(frame) == "boolean")
        panel.frame = Boolean(frame);
    
    return panel;
}

//#endregion

//#region MessageBox

function MsgConfirm(msg,title,fn,width)
{
    /// <summary>
    /// Ext.Msg.show 问题确认框
    /// </summary>
    /// <param name="msg">设置要输出的信息</param>
    /// <param name="title">设置确认框标题</param>
    /// <param name="fn">设置回调fn函数</param>
    /// <param name="width">设置确认框宽</param>
    /// <returns>Ext.Msg.show</returns>
    if(msg ==null)
        msg = "";
    
    if(title == null || typeof(title) != "string")
        title = biolims.common.question;
        
    if(width == null || typeof(width) != "number")
        width = 400;
        
    if(fn == null || typeof(fn) != "function")
        fn = new function(){};
    
    return Ext.Msg.show({
        title: title,
        msg: msg,
        width: width,
        icon:Ext.MessageBox.QUESTION,
        buttons: Ext.MessageBox.YESNO,
        fn:fn
    });
}

function MsgInfo(msg,title,width)
{
    /// <summary>
    /// Ext.Msg.show 信息提示框
    /// </summary>
    /// <param name="msg">设置要输出的信息</param>
    /// <param name="title">设置标题</param>
    /// <param name="width">设置提示框宽</param>
    /// <returns>Ext.Msg.show</returns>
    if(msg ==null)
        msg = "";
    
    if(title == null || typeof(title) != "string")
        title = biolims.common.prompt;
        
    if(width == null || typeof(width) != "number")
        width = 400;
        
    return Ext.MessageBox.show({
    	id:'msg',
    	name:'msg',
        title: title,
        msg: msg,
        width: width,
        icon:Ext.MessageBox.INFO,
        buttons: Ext.MessageBox.OK
    });
}

function MsgError(msg,title,width)
{
    /// <summary>
    /// Ext.Msg.show 错误提示框
    /// </summary>
    /// <param name="msg">设置要输出的信息</param>
    /// <param name="title">设置标题</param>
    /// <param name="width">设置提示框宽</param>
    /// <returns>Ext.Msg.show</returns>
    if(msg ==null)
        msg = "";
    
    if(title == null || typeof(title) != "string")
        title = biolims.common.error;
        
    if(width == null || typeof(width) != "number")
        width = 400;

    return Ext.MessageBox.show({
        title: title,
        msg: msg,
        width: width,
        icon:Ext.MessageBox.ERROR,
        buttons: Ext.MessageBox.OK
    });
}

function MsgWarning(msg,title,width)
{
    /// <summary>
    /// Ext.Msg.show 警告提示框
    /// </summary>
    /// <param name="msg">设置要输出的信息</param>
    /// <param name="title">设置标题</param>
    /// <param name="width">设置提示框宽</param>
    /// <returns>Ext.Msg.show</returns>
    if(msg ==null)
        msg = "";
    
    if(title == null || typeof(title) != "string")
        title = biolims.common.warning;
        
    if(width == null || typeof(width) != "number")
        width = 400;

    return Ext.MessageBox.show({
        title: title,
        msg: msg,
        width: width,
        icon:Ext.MessageBox.WARNING,
        buttons: Ext.MessageBox.OK
    });
    
    
}
/*
*EXT消息框
*/
function alertMessage(msg){
	Ext.MessageBox.show( {
		title : biolims.common.message,
		msg : msg,
		width : 300,
		buttons : Ext.MessageBox.OK,
		icon : Ext.MessageBox.WARNING
	});
}
	
function alertConfirm(msg){
	Ext.MessageBox.confirm(biolims.common.makeSure,
		msg,
		function(button,text){
			if(button=="yes"){
				return true;
			}else{
				return false;
			}
		}
	  );
}
var workflowData = [['0', biolims.master.invalid],['2', biolims.common.approving],['1',biolims.master.takeEffect],['3', biolims.common.create],['4', biolims.common.noPass]];
var stateData = [['0', biolims.master.invalid],['1', biolims.master.valid]];

function workflowStateChange(val) {

    if (val == '0') {
        return biolims.common.edit;
    } else if (val == '2') {
        return biolims.common.approving;
    }else if (val == '1') {
        return biolims.common.takeEffect;
    }else if (val == '3') {
        return biolims.common.create;
    }else if (val == '4') {
        return biolims.common.noPass;
    }
    return val;
}

function applicationTypeChange(val) {

    if (val == 'animal') {
        return biolims.common.animal;
    } else if (val == 'user') {
        return biolims.common.user;
    }else if (val == 'other') {
        return biolims.common.others;
    }else if (val == 'storage') {
        return biolims.common.storage;
    }else if (val == '') {
        return '';
    }
    return val;
}

function stateComboBox(hName,appName){

var store = new Ext.data.ArrayStore({
		
    fields : ['id', 'name'],
    data : stateData
	    });
var cob =  new Ext.form.ComboBox({
	readonly:true,
	editable:false,
	 hiddenName :hName,
    store: store,
    displayField:'name',
    valueField :'id',
    applyTo: appName
});

return cob;
}



function stateTextField(appName,defaultValue)
{
    /// <summary>
    /// Ext.form.TextField封装
    /// </summary>
    /// <param name="fName">name</param>
    /// <param name="fLabel">fieldLabel</param>
    /// <param name="defaultValue">默认值</param>
    /// <returns>Ext.form.TextField</returns>
    var text = new Ext.form.TextField({
    		
    		readOnly:true,
    		applyTo:appName }
    );
    

    
   
    
    //设置默认值
    if(defaultValue != null)
        text.setValue(workflowStateChange(defaultValue));
    
 
    
    return text;
}


function formatDate(value){
	return value ? value.dateFormat('Y-m-d') : '';}



	
		Ext.util.Format.comboRenderer = function(combo){
		return function(value){
		var record = combo.findRecord(combo.valueField, value);
		return record ? record.get(combo.displayField) : combo.valueNotFoundText;}};
	
	