$(function() {

	$(".removeable-items .ui-icon").click(function() {
		delSearch($(this));
	});

	$(".removeable-items li .search-name").click(function() {
		setInfoShow($(this).parent());
	});

	function setInfoShow(obj) {
		var allInfo = $(obj).attr("allInfo");

		var data = {};
		if (allInfo.length > 0)
			data = eval("(" + allInfo + ")");

		var vals = $("input,select");
		$.each(vals, function(i, obj) {
			$("#" + $(obj).attr("id")).val(data[$(obj).attr("id")]);
		});
	}
	function getSearchInfo() {
		var result = {};
		var data = {};
		var allData = {};
		var length = 0;
		$("input:text,input:hidden,textarea,select").each(function() {
			var vName = $(this).attr('name');
			var vId = $(this).attr('id');
			var vValue = $(this).attr('value');
			var vSearchField = $(this).attr('searchField');
			if (vSearchField != undefined && vSearchField != '' && vValue != undefined && vValue != '') {
				data[vName] = vValue;
				length++;
			}
			if (vValue)
				allData[vId] = vValue;
		});
		data.length = length;
		result.data = data;
		result.allData = allData;
		return result;
	}

	$("#save_search_info").button().click(function() {
		var data = {};
		data.fkModel = $("#rightsId").val();
		data.name = $("#search_name").val();
		var searchInfo = getSearchInfo();

		if (!data.name) {
			message("请填写查询条件名称！");
			return;
		}
		if (searchInfo.data.length <= 0) {
			message("请填写查询条件！");
			return;
		}
		delete searchInfo.data.length;
		data.searchInfo = JSON.stringify(searchInfo.data);
		data.allInfo = JSON.stringify(searchInfo.allData);
		ajax("post", "/search/save.action", data, function(result) {
			if (result.success) {
				var newSearch = [];
				newSearch.push("<li searchId='");
				newSearch.push(result.data.id);
				newSearch.push("' searchInfo='");
				newSearch.push(result.data.searchInfo);
				newSearch.push("' allInfo='");
				newSearch.push(result.data.allInfo);
				newSearch.push("' style='cursor: pointer;'><span class='search-name'>");
				newSearch.push($("#search_name").val());
				newSearch.push("</span>");
				newSearch.push("<span class='ui-icon ui-icon-close'>删除查询条件</span></li>");
				var li = $(".removeable-items").append($(newSearch.join("")));
				li.find(".ui-icon").click(function() {
					delSearch($(this));
				});
				li.find(".search-name").click(function() {
					setInfoShow($(this).parent());
				});
				$("#search_name").val("");
			} else {
				message("对不起，保存查询条件时发生错误！");
			}
		});
	});

	function delSearch(delSearch) {
		confirmMsg("您确定删除吗？", function() {
			ajax("post", "/search/delete.action", {
				searchId : delSearch.parent().attr("searchId")
			}, function(result) {
				if (result.success) {
					delSearch.parent().remove();
				} else {
					message("对不起，删除查询条件时发生错误！");
				}
			});
		});
	}
});