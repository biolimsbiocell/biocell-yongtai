jQuery(document).ready(function() {
	jQuery("input:text,input:hidden,input:password,textarea").each(function() {
		jQuery(this).attr('_value', jQuery(this).val());
	});
	jQuery(':checkbox, :radio', this).each(function() {
		var _v = this.checked ? 'on' : 'off';
		jQuery(this).attr('_value', _v);
	});
	jQuery('select', this).each(function() {
		jQuery(this).attr('_value', this.options[this.selectedIndex].value);
	});

});
function form_reset() {

	jQuery("input:text,input:hidden").each(function() {
		var _v = jQuery(this).attr('_value');
		var _vId = jQuery(this).attr('id');
		var _value = jQuery(this).val();
		if (typeof (_v) == 'undefined')
			_v = '';
		if (typeof (_value) == 'undefined')
			_value = '';
		if (_v != _value) {

			document.getElementById(_vId).value = _v;
		}

	});

	jQuery("select").each(function() {
		var _v = jQuery(this).attr('_value');

		var _vId = jQuery(this).attr('id');
		if (typeof (_v) == 'undefined')
			_v = '';

		// if (_v != this.options[this.selectedIndex].value) {
		jQuery(this).attr('value', _v);
		jQuery(this).selectedIndex = 0;
		// }
	});

}

function commonSearchActionByHeight(gridName, ht) {
	var limit = parseInt(ht > 0 ? ht / 25 : 1);
	if (trim(document.getElementById("limitNum").value) != '') {
		limit = document.getElementById("limitNum").value;

	}
	makeSearchJson(gridName, 0, limit);

}
function commonSearch(gridName) {
	var limit = parseInt((document.body.clientHeight - 70) > 0 ? (document.body.clientHeight - 70) / 25 : 1);
	if (trim(document.getElementById("limitNum").value) != '') {
		limit = document.getElementById("limitNum").value;

	}

	makeSearchJsonData(gridName, 0, limit);

}

function commonSearchAction(gridName) {

	var limit = parseInt((document.body.clientHeight - 50) > 0 ? (document.body.clientHeight - 50) / 24 : 1);
	if ($.trim($("#limitNum").val()) != '') {
		limit = document.getElementById("limitNum").value;
	}
	makeSearchJson(gridName, 0, limit);
}
function commonSearchLimit(gridName,limit) {

	

	makeSearchJson(gridName, 0, limit);

}

function makeSearchJson(gridName, start, limt) {
	var data = "{";
	jQuery("input:text,input:hidden,textarea,select").each(function() {
		var vName = jQuery(this).attr('name');
		var vValue = $.trim(jQuery(this).attr('value'));
		var vSearchField = jQuery(this).attr('searchField');
		if (vSearchField != undefined && vSearchField != '' && vValue != undefined && vValue != '') {
			if (data != "{") {
				data += ",";
			}

			if((vName.indexOf("##@@##")>-1)&&(vValue.indexOf("##@@##")>-1)){
				
				data += "\"" + vName + "\": \"" + vValue + "\"";
					
			}else{
				
				data += "\"" + vName + "\": \"%" + vValue + "%\"";
			}
			
			
		}

	});

	data += "}";
	document.getElementById('extJsonDataString').value = data;
	Ext.onReady(function() {
		// var gridGrid = Ext.getCmp(gridName);

		var o = {
			start : start,
			limit : limt,
			data : data
		};

		// gridName.pageSize=limt;

		gridName.store.load({
			params : o
		});
		gridName.render();
	});
}
window.searchworkflow = function(domId, moduleName, gridName) {
	/*Ext.QuickTips.init();

	var store = new Ext.data.JsonStore({
		root : 'results',
		remoteSort : true,
		fields : [ {
			name : 'state'
		} ],
		proxy : new Ext.data.HttpProxy({
			url : window.ctx + '/common/findState.action?obj=' + moduleName,
			method : 'POST'
		})
	});

	store.load();
	var cob = new Ext.form.ComboBox({

		store : store,
		displayField : 'state',
		valueField : 'state',
		typeAhead : true,

		mode : 'remote',
		forceSelection : true,
		triggerAction : 'all',

		selectOnFocus : true,
		applyTo : domId
	});
	cob.on('select', function() {
		var grid = gridGrid;
		if (gridName)
			grid = gridName + gridGrid;
		commonSearchAction(grid);
	});

	$("#" + domId).css("width", "80px");*/
}