$(function() {
	var cols = {};
	// cols.sm = true;
	var fields = [];

	fields.push({
		name : 'id',
		type : "string"
	});

	fields.push({
		
		name : 'title',
		type : "string"
	});
	fields.push({
		name : 'createUser-name',
		type : "string"
	});
	fields.push({
		name : 'createDate',
		type : "string"
	});

	cols.fields = fields;
	var cm = [];
	cm.push({
		xtype : 'actioncolumn',
		width : 30,
		items : [ {
			icon : window.ctx + '/javascript/lib/ext-3.4.0/examples/shared/icons/fam/application_go.png',
			tooltip : '查看',
			handler : function(grid, rowIndex, colIndex) {
				var rec = grid.getStore().getAt(rowIndex);
				window.open(window.ctx + '/info/toViewInfo.action?id=' + rec.get("id"),'通知公告','height=800,width=800,resizable=yes');
			}
		} ]
	});
	cm.push({
		dataIndex : 'id',
		header : 'ID',
		width : 100,
		hidden : true
	});
	cm.push({
		id:'title',
		dataIndex : 'title',
		header : '标题',
		width : 340,
		sortable : true
	});
	cm.push({
		dataIndex : 'createUser-name',
		header : '发起人',
		width : 100,
		sortable : true,
		hidden : true
	});
	cm.push({
		dataIndex : 'createDate',
		header : '日期',
		width : 100,
		sortable : true
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/info/showDashboardInfoListJson.action";
	// loadParam.limit = 10
	var opts = {};
	opts.id = "workflow_dashboard_info" + new Date().getTime();
	opts.height = 400;
//	opts.autoExpandColumn: 'title';
	// opts.title = "通知公告";

	taskgrid = gridTable("workflow_dashboard_info_grid_div", cols, loadParam, opts);

});