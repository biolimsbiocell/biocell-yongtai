function list() {
	window.location = window.ctx + '/info/showInfoList.action';
}
function add() {
	window.location = window.ctx + '/info/editInfoView.action';
}

function edit() {
	var id = "";
	id = document.getElementById("id").value;

	if (id) {
		window.location = window.ctx + '/info/editInfoView.action?infoId=' + id;
	} else {
		message("请选择一条记录!");
		return false;
	}
}

function view(){
	var id="";
	id = document.getElementById("id").value;
	if(id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location= window.ctx +'/info/toViewInfo.action?id='+id;
}
