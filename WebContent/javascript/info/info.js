
function list() {
	window.location = window.ctx + '/info/showInfoList.action';
}
function add() {
	window.location = window.ctx + '/info/editInfoView.action';
}
function newSave() {
	save();
}
function save() {
	var title = document.getElementById("info.title").value;
	if ($.trim(title).length<=0) {
		message("请填写信息标题！");
		return;
	}
	document.infoForm.submit();
}
