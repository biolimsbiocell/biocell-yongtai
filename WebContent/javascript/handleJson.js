function addgridfrom(gridName, record, fields, fieldsfrom) {
	if (!record || record == '') {
		Ext.MessageBox.alert(biolims.common.prompt, biolims.common.pleaseSelectRecord2Save, '');
	} else {

		var selData = '';
		for ( var ij = 0; ij < record.length; ij++) {
			var array = record[ij].data;

			var Ob = gridName.getStore().recordType;

			var p = new Ob({

			});

			for ( var i = 0; i < fields.length; i++) {

				p.set(fields[i], record[ij].get(fieldsfrom[i]));

			}

			gridName.stopEditing();
			gridName.store.insert(0, p);

			gridName.startEditing(0, 0);
			gridName.getView().getRow(0).style.backgroundColor = RECORD_INSERT_COLOR;

		}

	}

}
function addgridfromCheck(gridName, record, fields, fieldsfrom, idType) {

	if (!record || record == '') {
		Ext.MessageBox.alert(biolims.common.prompt, biolims.common.pleaseSelectRecord2Save, '');
	} else {
		var selData = '';
		for ( var ij = 0; ij < record.length; ij++) {
			var array = record[ij].data;

			if (hasNotIdValue(gridName, record[ij].get('id'), idType)) {

				var Ob = gridName.getStore().recordType;

				var p = new Ob({

				});

				for ( var i = 0; i < fields.length; i++) {

					p.set(fields[i], record[ij].get(fieldsfrom[i]));

				}

				gridName.stopEditing();
				gridName.store.insert(0, p);

				gridName.startEditing(0, 0);
				gridName.getView().getRow(0).style.backgroundColor = RECORD_INSERT_COLOR;
			} else {
				alertMessage("已有该信息!");
			}
		}

	}

}

function addgridNotCheckUnique(gridName, record, fields, fieldsfrom) {

	if (!record || record == '') {
		Ext.MessageBox.alert(biolims.common.prompt, biolims.common.pleaseSelectRecord2Save, '');
	} else {
		var selData = '';
		for ( var ij = 0; ij < record.length; ij++) {
			var array = record[ij].data;

			var Ob = gridName.getStore().recordType;

			var p = new Ob({

			});

			for ( var i = 0; i < fields.length; i++) {

				p.set(fields[i], record[ij].get(fieldsfrom[i]));

			}

			gridName.stopEditing();
			gridName.store.insert(0, p);

			gridName.startEditing(0, 0);
			gridName.getView().getRow(0).style.backgroundColor = RECORD_INSERT_COLOR;

		}

	}

}
function hasNotIdValue(gridName, idValue, idName) {
	var gridCount = gridName.store.getCount();
	for ( var ij = 0; ij < gridCount; ij++) {
		var record = gridName.store.getAt(ij);
		idValue = idValue.replace(new RegExp("－", "gm"), "-");
		var _idName = record.get(idName).replace(new RegExp("－", "gm"), "-");

		if (_idName == idValue) {
			return false;

		}
	}
	return true;
}

function addgridfromByIdName(gridName, record, fields, fieldsfrom, idName) {

	if (!record || record == '') {
		Ext.MessageBox.alert(biolims.common.prompt, biolims.common.pleaseSelectRecord2Save, '');
	} else {
		var selData = '';
		for ( var ij = 0; ij < record.length; ij++) {
			var array = record[ij].data;

			if (hasNotIdValueByIdName(gridName, record[ij].get('id'), idName)) {

				var Ob = gridName.getStore().recordType;

				var p = new Ob({

				});

				for ( var i = 0; i < fields.length; i++) {

					p.set(fields[i], record[ij].get(fieldsfrom[i]));

				}

				gridName.stopEditing();
				gridName.store.insert(0, p);

				gridName.startEditing(0, 0);
				gridName.getView().getRow(0).style.backgroundColor = RECORD_INSERT_COLOR;
			} else {
				alertMessage(biolims.common.alreadyHaveInformation);
			}
		}

	}

}

function hasNotIdValueByIdName(gridName, idValue, idName) {
	var gridCount = gridName.store.getCount();
	for ( var ij = 0; ij < gridCount; ij++) {
		var record = gridName.store.getAt(ij);
		idValue = idValue.replace(new RegExp("－", "gm"), "-");
		var _idName = record.get(idName).replace(new RegExp("－", "gm"), "-");

		if (_idName == idValue) {
			return false;

		}
	}
	return true;
}

function addallgrid(record, fields, fieldsfrom, gridName) {
	Ext.onReady(function() {

		if (!record || record == '') {
			Ext.MessageBox.alert(biolims.common.prompt, biolims.common.pleaseSelectRecord2Fill, '');
		} else {
			var selData = '';
			alert(record.length);

			for ( var ij = 0; ij < record.length; ij++) {
				var array = record[ij].data;

				// record[ij].set('id','');
				var Ob = gridName.getStore().recordType;
				// Ob.set('name',record[ij].get('name'));

				var p = new Ob({

				// name: record[ij].get('name')
				});
				alert(fields.length);
				for ( var i = 0; i < fields.length; i++) {

					alert(record[ij].get(fieldsfrom[i]));
					p.set(fields[i], record[ij].get(fieldsfrom[i]));

				}

				gridName.stopEditing();
				gridName.store.insert(0, p);

				gridName.startEditing(0, 0);
				gridName.getView().getRow(0).style.backgroundColor = RECORD_INSERT_COLOR;
			}

		}
	});

}
function addallgridfrom(record, fields, fieldsfrom, fields1, fieldsfrom1, gridName) {
	Ext.onReady(function() {

		if (!record || record == '') {
			Ext.MessageBox.alert(biolims.common.prompt, biolims.common.pleaseSelectRecord2Fill, '');
		} else {
			var selData = '';
			for ( var ij = 0; ij < record.length; ij++) {
				var array = record[ij].data;
				if (hasNotIdValue(gridName, record[ij].get('id'))) {
					// record[ij].set('id','');
					var Ob = gridName.getStore().recordType;
					// Ob.set('name',record[ij].get('name'));

					var p = new Ob({

					// name: record[ij].get('name')
					});

					for ( var i = 0; i < fields.length; i++) {

						p.set(fields[i], record[ij].get(fieldsfrom[i]));

					}
					for ( var i = 0; i < fields1.length; i++) {

						p.set(fields1[i], fieldsfrom1[i]);

					}
					gridName.stopEditing();
					gridName.store.insert(0, p);

					gridName.startEditing(0, 0);
					gridName.getView().getRow(0).style.backgroundColor = RECORD_INSERT_COLOR;

				} else {
					alertMessage(biolims.common.alreadyHaveInformation);

				}
			}
		}
	});

}
function addallgridfromNew(record, fields, fieldsfrom, fields1, fieldsfrom1, gridName) {
	Ext.onReady(function() {

		if (!record || record == '') {
			Ext.MessageBox.alert(biolims.common.prompt, biolims.common.pleaseSelectRecord2Fill, '');
		} else {
			var selData = '';
			for ( var ij = 0; ij < record.length; ij++) {
				var array = record[ij].data;

				// record[ij].set('id','');
				var Ob = gridName.getStore().recordType;
				// Ob.set('name',record[ij].get('name'));

				var p = new Ob({

				// name: record[ij].get('name')
				});

				for ( var i = 0; i < fields.length; i++) {

					p.set(fields[i], record[ij].get(fieldsfrom[i]));

				}
				for ( var i = 0; i < fields1.length; i++) {

					p.set(fields1[i], fieldsfrom1[i]);

				}
				gridName.stopEditing();
				gridName.store.insert(0, p);

				gridName.startEditing(0, 0);
				gridName.getView().getRow(0).style.backgroundColor = RECORD_INSERT_COLOR;

			}
		}
	});

}
function makeSearchJsonData(gridName, start, limt) {
	var data = "{";
	$("input:text,input:hidden,textarea,select").each(function(i, obj) {
		var vName = $(obj).attr('name');
		var vValue = $(obj).attr('value');

		if (vValue != undefined && vValue != '') {
			if (data != "{") {
				data += ",";
			}

			data += "\"" + vName + "\": \"" + vValue + "\"";
		}

	});

	data += "}";

	Ext.onReady(function() {
		// var gridGrid = Ext.getCmp(gridName);

		var o = {
			start : start,
			limit : limt,
			data : data
		};

		// gridName.pageSize=limt;

		gridName.store.load({
			params : o
		});
	});
}
function searchAllGrid(gridName, fields, fieldsvalue, start, limt) {
	Ext.onReady(function() {
		// var gridGrid = Ext.getCmp(gridName);
		var data = "{";
		for ( var i = 0; i < fields.length; i++) {
			if (data != "{") {
				data += ",";
			}
			data += "\"" + fields[i] + "\": \"" + fieldsvalue[i] + "\"";

		}
		data += "}";

		var o = {
			start : start,
			limit : limt,
			data : data
		};

		gridName.pageSize = limt;

		gridName.store.load({
			params : o
		});
	});
}

function makeFormJsonData() {
	var data = "[{";
	$("input:text,input:hidden,textarea,select").each(function(i, obj) {
		var vName = $(obj).attr('name');
		var vValue = $(obj).attr('value');

		if (data != "[{") {
			data += ",";
		}
		data += "\"" + vName + "\": \"" + vValue + "\"";

	});

	data += "}]";
	return data;
}

function makeAllDataJson(fields, fieldsvalue) {

	var data = "[{";
	for ( var i = 0; i < fields.length; i++) {
		if (document.getElementById(fieldsvalue[i]).value != null
				&& document.getElementById(fieldsvalue[i]).value != '') {
			if (data != "[{") {
				data += ",";
			}

			data += "\"" + fields[i] + "\": \"" + document.getElementById(fieldsvalue[i]).value + "\"";
		}

	}
	data += "}]";

	return data;
}
function makeAllDataStrJson(fields, fieldsvalue) {

	var data = "[{";
	for ( var i = 0; i < fields.length; i++) {
		if (data != "[{") {
			data += ",";
		}
		data += "\"" + fields[i] + "\": \"" + fieldsvalue[i] + "\"";

	}
	data += "}]";
	return data;
}
function makeTreeGrid(col, tbl, path, oldPath) {
	var treeGrid = new Ext.ux.tree.TreeGrid({

		id : 'treeGrid',
		width : parent.document.body.clientWidth - 40,
		height : document.body.clientHeight,
		renderTo : 'markup',
		enableDD : true,
		autoExpandColumn : 'common',
		columnLines : true,
		// collapsible : true,
		sorters : [ {
			property : 'id',
			direction : 'ASC'
		} ],
		autoScroll : true,
		enableSort : true,
		expanded : true,
		columns : col,

		tbar : tbl,

		dataUrl : path,
		listeners : {
			click : function(n) {
				document.getElementById("id").value = n.attributes.id;
				document.getElementById("leaf").value = n.attributes.leaf;
			}
		}

	});

	return treeGrid;

}
function commonGetModifyRecords(gridName) {
	var record = gridName.store.getModifiedRecords();
	if (!record || record == '' || record.length == 0) {
		return '';
	} else {
		var selData = '';
		for ( var ij = 0; ij < record.length; ij++) {
			if (record[ij].get('id') != '-1000') {
				var array = record[ij].data;
				selData = selData + Ext.util.JSON.encode(array) + ','
			}
		}
		if (selData.length > 0) {
			selData = selData.substring(0, selData.length - 1)
		}

		var data = '';
		if (selData != '{}' && selData != '')
			data = '[' + selData + ']';

		return data;
	}

}
