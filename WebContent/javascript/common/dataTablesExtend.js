/**
 * 列参数：data: 取值key 
 * title:表头
 * orderable：是否排序 
 * visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 
 * classname: 
 *edit:行内编辑   select：行内下拉框选择  date：行内日期选择
 * 设置下拉框选项"createdCell": function(td) {
				$(td).attr("selectOpt", "男|女|未知");
			}


	//表单时分选择不含日期  在指定js调用 ------》 $("#id").bind('click',function(event){newTimePacker($(this),event)});
	//表格里的 时分选择 是 className:"newhour"
 */

$(function() {
	edittable();
	//datatableScrollY=$("body").height()-200;

});
//如果状态名称不是NEW,隐藏左侧的DIV
function hideLeftDiv() {
	var stateName = $("#headStateName").attr("state");
	if(stateName != "3") {
		$("#leftDiv").hide();
		$("#rightDiv").attr("class", "col-md-12 col-xs-12");
	}
}

function table(cheboxvisible, id, url, col, tbar) {
	var cols = new Array();
	cols.push({
		"data": "id",
		"width": "18px",
		"visible": cheboxvisible,
		"title": '<input type="checkbox" class="icheck"  id="checkall"/>',
		"orderable": false,
		"render": function(data, type, full, meta) {
			return '<input type="checkbox" class="icheck" value="' + data + '"/>';
		}
	})
	for(var j = 0; j < col.length; j++) {
		cols.push(col[j])
	}
	if($("#workflowState").val() == "1" || $("#handlemethod").val() == "view") {
		var tbars = new Array();
	} else {
		var tbars = new Array();
		if(tbar != null) {
			for(var i = 0; i < tbar.length; i++) {
				if(tbar[i].text == biolims.common.fillDetail) {
					tbars.push({
						text: '<i class="glyphicon glyphicon-plus"></i> ' + biolims.common.fillDetail,
						className: 'btn btn-sm  btn-success addItem',
						action: tbar[i].action
					});
				} else if(tbar[i].text == biolims.common.delSelected) {
					tbars.push({
						text: '<i class="glyphicon glyphicon-trash"></i> ' + biolims.common.delSelected,
						className: 'btn btn-sm btn-success',
						action: tbar[i].action
					});
				} else if(tbar[i].text == biolims.common.batchUpload) {
					tbars.push({
						text: '<i class="glyphicon glyphicon-share-alt"></i> ' + biolims.common.batchUpload,
						className: 'btn btn-sm  btn-success',
						action: tbar[i].action
					})
				} else if(tbar[i].text == biolims.common.save) {
					if(id != "NEW") {
						tbars.push({
							text:biolims.common.save,
							className: 'btn btn-sm btn-success subSave',
							action: tbar[i].action
						})
					}
				} else if(tbar[i].text == biolims.common.find) {
					if(id != "NEW") {
						tbars.push({
							text: '<i class="glyphicon glyphicon-search"></i>' + biolims.common.find,
							className: 'btn btn-sm btn-success',
							action: tbar[i].action
						})
					}
				} else {
					var className = tbar[i].className ? tbar[i].className : 'btn btn-sm btn-success';
					tbars.push({
						text: tbar[i].text,
						className: className,
						action: tbar[i].action
					})
				}
			}
		}
		tbars.push({
			text: biolims.common.exportList,
			extend: 'excel',
			className: 'btn btn-sm btn-success hide'
		});
		tbars.push({
			text: biolims.common.copy,
			extend: 'copy',
			className: 'btn btn-sm btn-success hide',
			info: biolims.common.copySuccess,
		});
		tbars.push({
			text: biolims.common.print,
			extend: 'print',
			className: 'btn btn-sm  btn-success hide',
		});
		tbars.push({
			text: 'csv',
			extend: 'csv',
			className: 'btn btn-sm btn-success hide'
		});
//		tbars.push({
//			text: '<i class="glyphicon glyphicon-gift"></i>' + biolims.common.showHide,
//			extend: 'colvis',
//			columns: ':gt(0)',
//			className: 'btn btn-sm btn-success colvis'
//		});

	}

	var option = {
		// ajax的路径和数据
		ajax: {
			url: ctx + url,
			data: {
				id: id,
			}
		},
		// 每一列需要的数据key，对应上面thead里面的序列
		columns: cols,
		buttons: tbars

	}
	return option;
}
//datatable请求数据
function renderData(ele, options) {
	return ele.DataTable({
		serverSide: true,
		processing: true, //载入数据的时候是否显示“载入中”
		order: [
			[1, 'desc']
		],
		dom: "<'row tablebtns'<'col-sm-12'B>>" +
			"<'row'<'col-xs-2'l>>" +
			"<'row'<'col-sm-12 table-responsive'tr>>" +
			"<'row'<'col-sm-5'i><'col-sm-7'p>>",
		buttons: options.buttons,
		pageLength: 15, //首次加载的数据条数
		searching: false,
		responsive: false,
		autoWidth: false,
		scrollX: true,
		//autoFill: true,
		//fixedColumns: true,
		colReorder: {
			fixedColumnsLeft: 1
		},
		ajax: {
			type: "POST",
			url: options.ajax.url,
			data: options.ajax.data
		},
		columns: options.columns,
		language: {
			lengthMenu: '<select class="form-control input-xsmall">' + '<option value="10">10</option>' + '<option value="15">15</option>' + '<option value="20">20</option>' + '<option value="30">30</option>' + '<option value="40">40</option>' + '<option value="200">200</option>' + '</select>' + biolims.common.data,
			processing: biolims.common.loading, //处理页面数据的时候的显示
			paginate: { //分页的样式文本内容。
				previous: biolims.common.prevPage,
				next: biolims.common.nextPage,
				first: biolims.common.firstPage,
				last: biolims.common.lastPage
			},
			zeroRecords: biolims.common.notDataList, //table tbody内容为空时，tbody的内容。
			//下面三者构成了总体的左下角的内容。
			info: biolims.order.page, //左下角的信息显示，大写的词为关键字，筛选之后得到 _TOTAL_ 条。
			infoEmpty: biolims.common.noRecord, //筛选为空时左下角的显示。
		},
		headerCallback: function() {
			var columns = options.columns;
			var header = new $.fn.dataTable.Api(ele).columns().header();
			columns.forEach(function(val, i) {
				var kkey = val.className;
				var width = val.width;
				if(kkey) {
					if(kkey == "select") {
						$(header[i]).attr("selectopt", val.name);
					}
					//设置当前列是否可编辑
					$(header[i]).attr("key", kkey);
					$(header[i]).css("background-color", "#02C39A");
				}
				if(width) {
					$(header[i]).css({"max-width":width,"min-width":width});
				}
				//为添加明细设置保存的键
				$(header[i]).attr("saveName", val.data);
				
			});
		},
		drawCallback: function() {
			checkall(ele);
			$(".newhour").bind('click',function(event){newTimePacker($(this),event)});
		},
		initComplete: function() {
			
			checkall(ele);
			ele.on('column-sizing.dt', function() {
				checkall(ele);
			});
			autoFill(ele);
			$(".dataTables_scrollBody").css("padding-bottom", "10px");
			new $.fn.dataTable.Api(ele).draw();
			
			//调整按钮样式
			$(".dt-buttons").css("margin-bottom", "10px");
			$(".dt-buttons .btn").css({
				"margin-right": "3px",
				"margin-top": "10px",
				"border-radius": 0,
				"color": "#fff"
			})
		}
	});

}

//记录状态的datatables
function renderRememberData(ele, options) {
	return ele.DataTable({
		serverSide: true,
		processing: true, //载入数据的时候是否显示“载入中”
		order: [
			[1, 'desc']
		],
		dom: "<'row tablebtns'<'col-sm-12'B>>" +
			"<'row'<'col-xs-2'l>>" +
			"<'row'<'col-sm-12 table-responsive'tr>>" +
			"<'row'<'col-sm-5'i><'col-sm-7'p>>",
		buttons: options.buttons,
		pageLength: 15, //首次加载的数据条数
		searching: false,
		stateSave: true,
		responsive: false,
		autoWidth: false,
		scrollX: true,
		//fixedColumns: true,
		colReorder: {
			fixedColumnsLeft: 1
		},
		ajax: {
			type: "POST",
			url: options.ajax.url,
			data: options.ajax.data
		},
		columns: options.columns,
		language: {
			lengthMenu: '<select class="form-control input-xsmall">' + '<option value="10">10</option>' + '<option value="15">15</option>' + '<option value="20">20</option>' + '<option value="30">30</option>' + '<option value="40">40</option>' + '<option value="200">200</option>' + '</select>' + biolims.common.data,
			processing: biolims.common.loading, //处理页面数据的时候的显示
			paginate: { //分页的样式文本内容。
				previous: biolims.common.prevPage,
				next: biolims.common.nextPage,
				first: biolims.common.firstPage,
				last: biolims.common.lastPage
			},
			zeroRecords: biolims.common.notDataList, //table tbody内容为空时，tbody的内容。
			//下面三者构成了总体的左下角的内容。
			info: biolims.order.page, //左下角的信息显示，大写的词为关键字，筛选之后得到 _TOTAL_ 条。
			infoEmpty: biolims.common.noRecord, //筛选为空时左下角的显示。
		},
		headerCallback: function() {
			var columns = options.columns;
			var header = new $.fn.dataTable.Api(ele).columns().header();
			columns.forEach(function(val, i) {
				var kkey = val.className;
				var width = val.width;
				if(kkey) {
					if(kkey == "select") {
						$(header[i]).attr("selectopt", val.name);
					}
					//设置当前列是否可编辑
					$(header[i]).attr("key", kkey);
					$(header[i]).css("background-color", "#02C39A");
				}
				if(width) {
					$(header[i]).css("min-width", width);
				}
				//为添加明细设置保存的键
				$(header[i]).attr("saveName", val.data);
			});
		},
		drawCallback: function() {
			checkall(ele);
			$(" .newhour").bind('click',function(event){newTimePacker($(this),event)});
		},
		initComplete: function() {
			
			checkall(ele);
			ele.on('column-sizing.dt', function() {
				checkall(ele);
			});
			new $.fn.dataTable.Api(ele).draw();

			//调整按钮样式
			$(".dt-buttons").css("margin-bottom", "10px");
			$(".dt-buttons .btn").css({
				"margin-right": "3px",
				"margin-top": "10px",
				"border-radius": 0,
				"color": "#fff"
			})
		}
	});

}
//全选与反选

function checkall(ele) {
	
	// console.log(ele)
	ele.parent(".dataTables_scrollBody").parent(".dataTables_scroll").find('.icheck').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		increaseArea: '20%' // optional
	});
	ele.parent(".dataTables_scrollBody").parent(".dataTables_scroll").find("#checkall").on('ifChanged', function(event) {
		console.log(123)
		if($(this).is(':checked')) {
			ele.find("tbody input").iCheck('check');
		} else {
			ele.find("tbody input").iCheck('uncheck'); //移除 checked 状态 
		}
	});
	var rem = new Array();
	var ichecks = ele.find("tbody .icheck");
	ichecks.on('ifChanged', function(e) {
		if($(this).is(':checked')) {
			$(this).parent("div").parent("td").parent("tr").addClass("selected");
		} else {
			$(this).parent("div").parent("td").parent("tr").removeClass("selected");
		}
		//shift配合鼠标左键全选
		var eve = window.event || e;
		rem.push(ichecks.index($(this)));
		if(eve.shiftKey) {
			var iMin = Math.min(rem[rem.length - 2], rem[rem.length - 1]);
			var iMax = Math.max(rem[rem.length - 2], rem[rem.length - 1]);
			ichecks.each(function(i, val) {
				if(i < iMax && i > iMin) {
					$(val).iCheck('check');
				}
			});
		}
	});
}
// 固定列全选反选
function checkFixcloumn(){
	$('.DTFC_Cloned').find('.icheck').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		increaseArea: '20%' // optional
	});
	
	
	var icheckss = $('.DTFC_Cloned').find("tbody .icheck");
	icheckss.on('ifChanged', function(e) {
		if($(this).is(':checked')) {
			$(this).parent("div").parent("td").parent("tr").addClass("selected");
		} else {
			$(this).parent("div").parent("td").parent("tr").removeClass("selected");
		}
		
	});
	
}
//删除选中
function removeChecked(ele, urll, del, mainId, tabs) {
	var arr = [];
	var rows = ele.find("tbody .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	top.layer.confirm(biolims.common.confirm2Del + length + biolims.common.record,
			{
		icon: 3,
		title: biolims.common.prompt,
		btn:biolims.common.selected
	},
			function(index) {
		top.layer.close(index);
		rows.each(function(i, val) {
			var id = $(val).find("input[type=checkbox]").val();
			if(id) {
				arr.push(id);
			} else {
				$(val).remove();
			}
		});
		if(arr.length) {
			$.ajax({
				type: "post",
				data: {
					ids: arr,
					del: del,
					id: mainId
				},
				url: ctx + urll,
				success: function(data) {
					var data = JSON.parse(data);
					if(data.success) {
						top.layer.msg(biolims.common.deleteSuccess);
						rows.remove();
						tabs.ajax.reload()
					}
				}
			});
		}

	});
};

function autoFill() {
	document.addEventListener('mouseover', function(event) {
		var evt = window.event || event;
		if(evt.target.nodeName.toLowerCase() == "td") {
			var ele = evt.srcElement || evt.target;
			if($(ele).hasClass("date") || $(ele).hasClass("select") || $(ele).hasClass("edit") || $(ele).hasClass("textarea")|| $(ele).hasClass("day")|| $(ele).hasClass("hour")|| $(ele).hasClass("minute")) {
				if(!$(".dataTables_scrollBody .adon").length) {
					var adon = '<div class="fillTop"></div>' +
						'<div class="fillRight"></div>' +
						'<div class="fillBottom"></div>' +
						'<div class="fillLeft"></div>' +
						'<span class="glyphicon glyphicon-stop adon"></span>';
					$(".dataTables_scrollBody").append(adon).children(".table").css("position", "relative");
				}
				var x = $(ele).position();
				var width = $(ele).width() + 10;
				var height = $(ele).height() + 10;
				$(".dataTables_scrollBody .adon").css({
					top: x.top + height - 10,
					left: x.left + width - 10,
				}).show();
				$(".dataTables_scrollBody .adon").unbind("mousedown").mousedown(function() {
					document.onselectstart = function(event) {
						event = window.event || event;
						event.returnValue = false;
					}
					var adonY = $(this).offset().top;
					var renderNum = 0;
					var ipts = [];
					$(".dataTables_scrollBody .adon").hide();
					$(".dataTables_scrollBody .fillTop").css({
						"top": x.top,
						"left": x.left,
						"width": width
					});
					$(".dataTables_scrollBody .fillRight").css({
						"top": x.top,
						"left": x.left + width,
						"height": height
					});
					$(".dataTables_scrollBody .fillBottom").css({
						"top": x.top + height,
						"left": x.left,
						"width": width
					});
					$(".dataTables_scrollBody .fillLeft").css({
						"top": x.top,
						"left": x.left,
						"height": height
					});
					var tdIndex = $(ele).parent("tr").children("td").index($(ele));
					var eleEles = '';
					$(document).mousemove(function(e) {
						$(".dataTables_scrollBody .adon").hide();
						var e = e || window.event;
						var eles = e.srcElement || e.target;
						if($(eles).hasClass("date") || $(eles).hasClass("select") || $(eles).hasClass("edit") || $(eles).hasClass("textarea")|| $(eles).hasClass("day")|| $(eles).hasClass("hour")|| $(ele).hasClass("minute")) {
							if(ele !== eles) {
								if(eleEles != eles) {
									var tdIndexx = $(eles).parent("tr").children("td").index($(eles));
									if(tdIndexx == tdIndex) {
										ipts.push(eles);
										var xx = $(eles).position();
										var widthx = $(eles).width() + 10;
										var heightx = $(eles).height() + 10;
										$(".dataTables_scrollBody .fillRight").css({
											"height": xx.top - x.top + 33
										});
										$(".dataTables_scrollBody .fillBottom").css({
											"top": xx.top + heightx
										});
										$(".dataTables_scrollBody .fillLeft").css({
											"height": xx.top - x.top + 33
										});
										eleEles = eles;
									}
								}
							}
						}
					});
					$(document).mouseup(function(e) {
						document.onselectstart = null;
						$(document).off("mousemove");
						ipts.forEach(function(td, i) {
							$(td).css("box-shadow", "0px 0px 1px #17C697").addClass("edited").text(ele.innerText);
							$(td).parent("tr").addClass("editagain");
						});
						$(".dataTables_scrollBody .fillTop").css({
							"top": "0px",
							"left": "0px",
							"width": "2px"
						});
						$(".dataTables_scrollBody .fillRight").css({
							"top": "0px",
							"left": "0px",
							"height": "2px"
						});
						$(".dataTables_scrollBody .fillBottom").css({
							"top": "0px",
							"left": "0px",
							"width": "2px"
						});
						$(".dataTables_scrollBody .fillLeft").css({
							"top": "0px",
							"left": "0px",
							"height": "2px"
						});
						$(document).off("mouseup");
					})
				});
			}
		}
	}, false)
}
//行内input编辑
function edittable() {
//	if(window.navigator.platform =='iPad'){
//		return false;
//	}
	document.addEventListener('click', function(event) {
		var evt = window.event || event;
		if(evt.target.nodeName.toLowerCase() == "td") {
			var ele = event.srcElement || evt.target;
			if($(ele).hasClass("date")) {
				changeDateInput(ele);
			} else if($(ele).hasClass("select")) {
				changeSelectInput(ele);
			} else if($(ele).hasClass("edit")) {
				changeTextInput(ele);
				//console.log("123456")
			} else if($(ele).hasClass("textarea")) {
				changeTextTextarea(ele);
			}else if($(ele).hasClass("day")){
				changeDateInput(ele);
			}else if($(ele).hasClass("hour")){
				changeDateInput(ele);
			}else if($(ele).hasClass("minute")){
				changeDateInput(ele);
			}
		}
	}, false)
	document.addEventListener('touchstart', function(event) {
		var evt = window.event || event;
		if(evt.target.nodeName.toLowerCase() == "td") {
			var ele = event.srcElement || evt.target;
			if($(ele).hasClass("date")) {
//				changeDateInput(ele);
			} else if($(ele).hasClass("select")) {
				changeSelectInput(ele);
			} else if($(ele).hasClass("edit")) {
//				changeTextInput(ele);
				
			} else if($(ele).hasClass("textarea")) {
//				changeTextTextarea(ele);
			}else if($(ele).hasClass("day")){
//				changeDateInput(ele);
			}else if($(ele).hasClass("hour")){
//				changeDateInput(ele);
			}else if($(ele).hasClass("minute")){
//				changeDateInput(ele);
			}
		}

	}, false)
	//变普通input输入框
	function changeTextInput(ele) {
		$(ele).css({
			"padding": "0px"
		});
		var width = $(ele).css("width");
		var value=ele.innerText;
		//console.log(value);
		var ipt = $('<input type="text" id="edit">');
		ipt.css({
			"width": width,
			"height": "32px",
		});
		$(ele).html(ipt.val(value));
		ipt.focus();
		ipt.select();
		$(ipt).click();
		document.onkeydown = function(event) {
			var e = event || window.event || arguments.callee.caller.arguments[0];
			var tdIndex = $(ele).parent("tr").children("td").index($(ele));
//			if(e.keyCode === 9) {
//				var width = $("#edit").width();
//				$("#edit").parent("td").css({
//					"padding": "5px 0px 5px 5px",
//					"overflow": "hidden",
//					"text-overflow": "ellipsis",
//					"max-width": width + "px",
//					"box-shadow": "0px 0px 1px #17C697"
//				});
//				$("#edit").parents("tr").addClass("editagain");
//				$("#edit").parent("td").html($("#edit").val());
//				var next = $(ele).next("td");
//				while(next.hasClass("undefined")) {
//					next = next.next("td")
//				}
//				if(next.length) {
//					next.click();
//				} else {
//					return false;
//				}
//
//			}
			if(e.keyCode == 38) { //上：38 下：40 左：39 右：37 回车 ：13
				var shangtr = $(ele).parent("tr").prev("tr");
				var width = $("#edit").width();
				$("#edit").parent("td").css({
					"padding": "5px 0px 5px 5px",
					"overflow": "hidden",
					"text-overflow": "ellipsis",
					"max-width": width + "px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#edit").parents("tr").addClass("editagain");
				$("#edit").parent("td").html($("#edit").val());
				if(shangtr.length) {
					var ele1 = shangtr.children("td").eq(tdIndex)[0];
					ele1.click();
				}
			} else if(e.keyCode == 40||e.keyCode==13) {
				var xiatr = $(ele).parent("tr").next("tr");
				var width = $("#edit").width();
				$("#edit").parent("td").css({
					"padding": "5px 0px 5px 5px",
					"overflow": "hidden",
					"text-overflow": "ellipsis",
					"max-width": width + "px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#edit").parents("tr").addClass("editagain");
				$("#edit").parent("td").html($("#edit").val());
				if(xiatr.length) {
					var ele1 = xiatr.children("td").eq(tdIndex)[0];
					ele1.click();
				}
			} else if(e.keyCode == 39) {
				var width = $("#edit").width();
				$("#edit").parent("td").css({
					"padding": "5px 0px 5px 5px",
					"overflow": "hidden",
					"text-overflow": "ellipsis",
					"max-width": width + "px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#edit").parents("tr").addClass("editagain");
				$("#edit").parent("td").html($("#edit").val());
				var ele1 = $(ele).next("td");
				var flag = ele;
				while(ele1.attr("class") == undefined) {
					ele1 = ele1.next("td");
					if(!ele1.length) {
						ele1 = flag;
						break;
					}
				}
				if(ele1.length) {
					ele1.click();
				}

			} else if(e.keyCode == 37) {
				var width = $("#edit").width();
				$("#edit").parent("td").css({
					"padding": "5px 0px 5px 5px",
					"overflow": "hidden",
					"text-overflow": "ellipsis",
					"max-width": width + "px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#edit").parents("tr").addClass("editagain");
				$("#edit").parent("td").html($("#edit").val());
				var ele1 = $(ele).prev("td");
				var flag = ele;
				while(ele1.attr("class") == undefined) {
					ele1 = ele1.prev("td");
					if(!ele1.length) {
						ele1 = flag;
						break;
					}
				}
				if(ele1.length) {
					ele1.click();
				}
			}

		};
	}
	//变普通Textarea输入框
	function changeTextTextarea(ele) {
		if($(ele).hasClass("edited")) {
			var width = $(ele).width() + "px";
		} else {
			var width = $(ele).width() + 10 + "px";
		}
		ele.style.padding = "0";
		$(ele).css({
			"min-width": width
		});
		var ipt = $('<textarea www=' + width + ' style="position: absolute;height:80px;width:'+width+'" id="textarea">' + ele.innerText + '</textarea>');
		$(ele).html(ipt);
		$(ele).find("textarea").click();
		document.onkeydown = function(event) {
			var e = event || window.event || arguments.callee.caller.arguments[0];
			if(e.keyCode === 9) {
				var td = $("#textarea").parents("td");
				var width = $("#textarea").attr("www");
				td.css({
					"padding": "5px 0px",
					"max-width": width,
					"overflow": "hidden",
					"text-overflow": "ellipsis",
					"box-shadow": "0px 0px 1px #17C697",
				});
				td.addClass("edited");
				td.parent("tr").addClass("editagain");
				//td[0].title=$("#edit").val();
				td.html($("#textarea").val());

				var next = $(ele).next("td");
				while(next.hasClass("undefined")) {
					next = next.next("td")
				}
				if(next.length) {
					next.click();
				} else {
					return false;
				}
			}

		};
	}
	//变下拉框
	function changeSelectInput(ele) {
		$(ele).css("padding", "0px");
		var width = $(ele).width();
		var selectOpt = $(ele).attr("selectopt");
		var selectOptArr = selectOpt.split("|");
		var ipt = $('<select id="select"></select>');
		var value=ele.innerText;
		selectOptArr.forEach(function(val, i) {
			if(value==val){
				ipt.append("<option selected>" + val + "</option>");
			}else{
				ipt.append("<option>" + val + "</option>");
			}
		});
		ipt.css({
			"width": width + "px",
			"height": "32px",
		});
		$(ele).html(ipt);
		//$(ele).find("select").click();
		document.onkeydown = function(event) {
			var e = event || window.event || arguments.callee.caller.arguments[0];
			if(e.keyCode === 9) {
				$("#select").parent("td").css({
					"padding": "5px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#select").parents("tr").addClass("editagain");
				$("#select").parent("td").html($("#select option:selected").val());

				var next = $(ele).next("td");
				while(next.hasClass("undefined")) {
					next = next.next("td")
				}
				if(next.length) {
					next.click();
				} else {
					return false;
				}
			}

		};
	}
	//变日期选择框
	function changeDateInput(ele) {
		console.log(ele)
		if($(ele).hasClass('date')){
			console.log(0)
					//$(".datetimepicker").remove();
				$(ele).css("padding", "0px");
				var ipt = $('<input type="text" id="date" readonly="readonly" autofocus value="' + $(ele).text() + '">');
				var width = $(ele).width();
				ipt.css({
					"width": width + "px",
					"height": "32px",
				});
				$(ele).html(ipt);
				ipt.datetimepicker({
					language: "zh-TW",
					autoclose: true, 
					format: "yyyy-mm-dd hh:ii" 
				});
				$(ele).find("input").click();
		}else if($(ele).hasClass('day')){
//			$(".datetimepicker").remove();
			$(ele).css("padding", "0px");
			var iptt = $('<input type="text" id="day" readonly="readonly" autofocus value="' + $(ele).text() + '">');
			var width = $(ele).width();
			iptt.css({
				"width": width + "px",
				"height": "32px",
			});
			$(ele).html(iptt);
			iptt.datetimepicker({
				minView: "month",
				language: "zh-TW",
				autoclose: true, 
				format: "yyyy-mm-dd" 
			});
			$(ele).find("input").click();
		}else if($(ele).hasClass('hour')){
			console.log(7)
			//$(".datetimepicker").remove();
			$(ele).css("padding", "0px");
			var ipttt = $('<input type="text" id="hour" readonly="readonly" autofocus value="' + $(ele).text() + '">');
			var width = $(ele).width();
			ipttt.css({
				"width": width + "px",
				"height": "32px",
			});
			$(ele).html(ipttt);
			ipttt.datetimepicker({
				startView: 1,
				language: "zh-CH",
				autoclose: true, 
				format: "hh:ii" 
			});
			$('.switch').text('请选择');
			$(ele).find("input").click();
		}else if($(ele).hasClass('minute')){
			console.log(7)
			//$(".datetimepicker").remove();
			$(ele).css("padding", "0px");
			var ipttt = $('<input type="text" id="minute" readonly="readonly" autofocus value="' + $(ele).text() + '">');
			var width = $(ele).width();
			ipttt.css({
				"width": width + "px",
				"height": "32px",
			});
			$(ele).html(ipttt);
			ipttt.datetimepicker({
				startView: 0,
				language: "zh-TW",
				autoclose: true, 
				format: "hh:ii",
				startView : 1,
				keyboardNavigation : true
			});
			$(ele).find("input").click();
		}


		// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		// $(".datetimepicker").remove();
		// $(ele).css("padding", "0px");
		// var ipt = $('<input type="text" id="date" autofocus value="' + $(ele).text() + '">');
		// var width = $(ele).width();
		// ipt.css({
		// 	"width": width + "px",
		// 	"height": "32px",
		// });
		// $(ele).html(ipt);
		// ipt.datetimepicker({
		// 	language: "zh-TW",
		// 	autoclose: true, 
		// 	format: "yyyy-mm-dd hh:ii" 
		// });
		// $(ele).find("input").click();
//		document.onkeydown = function(event) {
//			var e = event || window.event || arguments.callee.caller.arguments[0];
//			if(e.keyCode === 9) {
//				var width = $("#edit").width();
//				if(!$(".datepicker").length) {
//					$("#date").parent("td").css({
//						"padding": "5px",
//						"box-shadow": "0px 0px 1px #17C697"
//					});
//					$("#date").parents("tr").addClass("editagain");
//					$("#date").parent("td").html($("#date").val());
//				}
//				var next = $(ele).next("td");
//				while(next.hasClass("undefined")) {
//					next = next.next("td")
//				}
//				if(next.length) {
//					next.click();
//				} else {
//					return false;
//				}
//			}
//
//		};
	}
	document.onmouseup = function(event) {
		var evt = window.event || event;
		if(document.getElementById("textarea") && evt.target.id != "textarea") {
			var td = $("#textarea").parents("td");
			var width = $("#textarea").attr("www");
			td.css({
				"padding": "5px 0px",
				"max-width": width,
				"overflow": "hidden",
				"text-overflow": "ellipsis",
				"box-shadow": "0px 0px 1px #17C697",
			});
			td.addClass("edited");
			td.parent("tr").addClass("editagain");
			//td[0].title=$("#edit").val();
			td.html($("#textarea").val());
		}
		if(document.getElementById("edit") && evt.target.id != "edit") {
			var width = $("#edit").width();
			$("#edit").parent("td").css({
				"padding": "5px 0px 5px 5px",
				"overflow": "hidden",
				"text-overflow": "ellipsis",
				"max-width": width + "px",
				"box-shadow": "0px 0px 1px #17C697"
			});
			$("#edit").parents("tr").addClass("editagain");
			$("#edit").parent("td").html($("#edit").val());
		}
		if(document.getElementById("select") && evt.target.id != "select") {
			$("#select").parent("td").css({
				"padding": "5px",
				"box-shadow": "0px 0px 1px #17C697"
			});
			$("#select").parents("tr").addClass("editagain");
			$("#select").parent("td").html($("#select option:selected").val());
		}
		if(document.getElementById("date") && evt.target.id != "date") {
			if($(".datetimepicker").is(':hidden')) {
				$("#date").parent("td").css({
					"padding": "5px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#date").parents("tr").addClass("editagain");
				$("#date").parent("td").html($("#date").val());
				$(".datetimepicker").remove();
			}
		}
		if(document.getElementById("day") && evt.target.id != "day") {
			if($(".datetimepicker").is(':hidden')) {
				$("#day").parent("td").css({
					"padding": "5px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#day").parents("tr").addClass("editagain");
				$("#day").parent("td").html($("#day").val());
				$(".datetimepicker").remove();
			}
		}
		if(document.getElementById("hour") && evt.target.id != "hour") {
			if($(".datetimepicker").is(':hidden')) {
				$("#hour").parent("td").css({
					"padding": "5px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#hour").parents("tr").addClass("editagain");
				$("#hour").parent("td").html($("#hour").val());
				$(".datetimepicker").remove();
			}
		}
		if(document.getElementById("minute") && evt.target.id != "minute") {
			if($(".datetimepicker").is(':hidden')) {
				$("#minute").parent("td").css({
					"padding": "5px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#minute").parents("tr").addClass("editagain");
				$("#minute").parent("td").html($("#minute").val());
				$(".datetimepicker").remove();
			}
		}
	}
}
//添加明细
function addItem(ele) {
	$(".dataTables_scrollHead th").off();
	//禁用固定列
	$("#fixdeLeft2").hide();
	//禁用列显示隐藏
	$(".colvis").hide();
	//清除没有内容选项
	ele.find(".dataTables_empty").parent("tr").remove();
	//添加明细
	var ths = ele.find("th");
	var tr = $("<tr><td><input type='checkbox' class='icheck' value=''/></td></tr>");
	tr.height(32);
	for(var i = 1; i < ths.length; i++) {
		var edit = $(ths[i]).attr("key");
		var saveName = $(ths[i]).attr("saveName");
		if(edit == "select") {
			var selectOpt = $(ths[i]).attr("selectopt");
			tr.append("<td class=" + edit + " saveName=" + saveName + " selectopt=" + selectOpt + "></td>");
		} else {
			tr.append("<td class=" + edit + " saveName=" + saveName + "></td>");
		}

	}
	ele.find("tbody").prepend(tr);
	checkall(ele);
	$(".newhour").bind('click',function(event){newTimePacker($(this),event)});
}
//弹框编辑
function editItemLayer(ele) {
	var row = ele.find(".selected");
	var length = row.length;
	if(!length || length > 1) {
		top.layer.msg(biolims.common.pleaseSelectAPieceOfData);
		return false;
	}
	var ths = ele.find("th");
	var tds = row.children("td");
	//获取能被修改的列的表头并生成输入框
	$("body").append('<div id="editItemLaye" class="hide"><div id="editItemLayer" class="row"></div></div>');
	var str = "";
	ths.each(function(i, val) {
		if(i != 0) {
			var edit = $(val).attr("key");
			var txt = $(val).text();
			var val = $(tds[i]).text();
			if(edit == "select") {
				var selectopt = $(tds[i]).attr("selectopt");
				var selectOptArr = selectopt.split("|");
				var opt = "";
				selectOptArr.forEach(function(v, i) {
					if(v == val) {
						opt += "<option selected>" + v + "</option>";
					} else {
						opt += "<option>" + v + "</option>";
					}
				});
				str += '<div class="col-sm-6" style="margin-top: 10px;"><div class="input-group"><span class="input-group-addon">' + txt + '</span><select class="form-control editItemLayerInput addItemLayerSelect">' + opt + '</select></div></div>';
			} else if(edit == "date") {
				str += '<div class="col-sm-6" style="margin-top: 10px;"><div class="input-group"><span class="input-group-addon">' + txt + '</span><input type="date" class="form-control editItemLayerInput editItemLayerDate" value=' + val + '></div></div>';
			} else if(edit == "edit") {
				str += '<div class="col-sm-6" style="margin-top: 10px;"><div class="input-group"><span class="input-group-addon">' + txt + '</span><input type="text" class="form-control editItemLayerInput" value=' + val + '></div></div>';
			} else {
				str += '<div class="col-sm-6" style="margin-top: 10px;"><div class="input-group"><span class="input-group-addon">' + txt + '</span><input type="text" class="form-control editItemLayerInput" disabled value=' + val + '></div></div>';
			}
		}
	});
	$("#editItemLayer").append(str);
	top.layer.open({
		type: 1,
		content: $("#editItemLaye").html(),
		anim: '1',
		area: ['650px', "300px"],
		shade: 0.3,
		title: biolims.common.vim,
		btn: ['<<', biolims.common.selected, '>>'],
		yes: function(index, layero) {
			row.find(".icheck").iCheck('uncheck');
			row = row.prev("tr");
			if(!row.length) {
				top.layer.msg(biolims.common.noPreData);
				top.layer.close(index);
				return false;
			}
			row.find(".icheck").iCheck('check');
			tds = row.children("td");
			var thisTextArr = [];
			tds.each(function(i, val) {
				if(i != 0) {
					thisTextArr.push($(val).text());
				}
			});
			$(".editItemLayerInput").attr('type','date')
			$(".editItemLayerInput", parent.document).each(function(i, val) {
				if($(val).hasClass("addItemLayerSelect")) {
					$(val).children("option").each(function(j, opt) {
						if($(opt).val() == thisTextArr[i]) {
							$(opt).prop("selected", true);
						} else {
							$(opt).prop("selected", false);
						}
					});
				} else {
					$(val).val(thisTextArr[i]);
				}
			});
			return false;
		},
		btn2: function(index, layero) {
			//获取添加的数据
			var addItemLayerValue = [];
			$(".editItemLayerInput", parent.document).each(function(i, val) {
				addItemLayerValue.push($(val).val());
			});
			//回填tr
			for(var i = 1; i < tds.length; i++) {
				var width = $(tds[i]).width();
				$(tds[i]).css({
					"overflow": "hidden",
					"text-overflow": "ellipsis",
					"max-width": width + "px",
				});
				$(tds[i]).text(addItemLayerValue[i - 1]);
			}
			row.addClass("editagain");
			top.layer.msg(biolims.main.success);
			top.layer.close(index);
			return false;
		},
		btn3: function(index, layero) {
			row.find(".icheck").iCheck('uncheck');
			row = row.next("tr");
			if(!row.length) {
				top.layer.msg(biolims.common.noNextData);
				top.layer.close(index);
				return false;
			}
			row.find(".icheck").iCheck('check');
			tds = row.children("td");
			var thisTextArr = [];
			tds.each(function(i, val) {
				if(i != 0) {
					thisTextArr.push($(val).text());
				}
			});
			$(".editItemLayerInput", parent.document).each(function(i, val) {
				if($(val).hasClass("addItemLayerSelect")) {
					$(val).children("option").each(function(j, opt) {
						if($(opt).val() == thisTextArr[i]) {
							$(opt).prop("selected", true);
						} else {
							$(opt).prop("selected", false);
						}
					});
				} else {
					$(val).val(thisTextArr[i]);
				}
			});
			return false;
		},
		btnAlign: 'c',
		success: function(index, layero) {
			$(".editItemLayerDate").datepicker({
				language: "zh-TW",
				autoclose: true, //选中之后自动隐藏日期选择框
				format: "yyyy-mm-dd" //日期格式，详见 
			});
		},
		end: function() {
			$("#editItemLaye").remove();
		}
	});

}
//弹框添加明细
function addItemLayer(ele) {
	//获取能被修改的列的表头并生成table
	var ths = ele.find("th");
	$("body").append('<div id="addItemLaye" class="hide"><div id="addItemLayer" class="row"></div></div>');
	var str = "";
	ths.each(function(i, val) {
		if(i != 0) {
			var edit = $(val).attr("key");
			var txt = $(val).text();
			if(edit == "select") {
				var selectopt = $(ths[i]).attr("selectopt");
				var selectOptArr = selectopt.split("|");
				var opt = "";
				selectOptArr.forEach(function(v, i) {
					opt += "<option>" + v + "</option>";
				});
				str += '<div class="col-sm-6" style="margin-top: 10px;"><div class="input-group"><span class="input-group-addon">' + txt + '</span><select class="form-control addItemLayerInput addItemLayerSelect">' + opt + '</select></div></div>';
			} else if(edit == "date") {
				str += '<div class="col-sm-6" style="margin-top: 10px;"><div class="input-group"><span class="input-group-addon">' + txt + '</span><input type="date" class="form-control addItemLayerInput addItemLayerDate"></div></div>';
			} else if(edit == "edit") {
				str += '<div class="col-sm-6" style="margin-top: 10px;"><div class="input-group"><span class="input-group-addon">' + txt + '</span><input type="text" class="form-control addItemLayerInput"></div></div>';
			} else {
				str += '<div class="col-sm-6" style="margin-top: 10px;"><div class="input-group"><span class="input-group-addon">' + txt + '</span><input type="text" class="form-control addItemLayerInput" disabled></div></div>';
			}
		}
	});
	$("#addItemLayer").append(str);
	top.layer.open({
		type: 1,
		content: $("#addItemLaye").html(),
		anim: '1',
		area: ['650px', "300px"],
		shade: 0,
		title: biolims.common.fillDetail,
		btn: biolims.common.selected,
		yes: function(index, layero) {
			//清除没有内容选项
			ele.find(".dataTables_empty").parent("tr").remove();
			//获取添加的数据
			var addItemLayerValue = [];
			$(".addItemLayerInput", parent.document).each(function(i, val) {
				addItemLayerValue.push($(val).val());
			});
			//生成tr
			var tr = $("<tr class='editagain'><td><input type='checkbox' class='icheck' value=''/></td></tr>");
			tr.height(32);
			for(var i = 1; i < ths.length; i++) {
				var edit = $(ths[i]).attr("key");
				var saveName = $(ths[i]).attr("saveName");
				var width = $(ths[i]).width();
				if(edit == "select") {
					var selectOpt = $(ths[i]).attr("selectopt");
					tr.append("<td class=" + edit + " saveName=" + saveName + " selectopt=" + selectOpt + ">" + addItemLayerValue[i - 1] + "</td>");
				} else {
					tr.append("<td class=" + edit + " saveName=" + saveName + " style='overflow:hidden;text-overflow:ellipsis;max-width: " + width + "px'>" + addItemLayerValue[i - 1] + "</td>");
				}
			}
			ele.find("tbody").prepend(tr);
			checkall(ele);
			top.layer.close(index);
		},
		btnAlign: 'c',
		//		success: function(index, layero) {
		//			$(".addItemLayerDate").datepicker({
		//				language: "zh-TW",
		//				autoclose: true, //选中之后自动隐藏日期选择框
		//				format: "yyyy-mm-dd" //日期格式，详见 
		//			});
		//		},
		end: function() {
			$("#addItemLaye").remove();
		}
	});
}
//弹框搜索
function search() {
//	top.layer.closeAll();
	searchContent = searchOptions();
	console.log(1);
	console.log(searchContent);
	var myTable;
	$("body").append('<div id="searchItemLaye" class="hide"><div id="searchItemLayer" class="row"></div></div>');
	var str = "";
	searchContent.forEach(function(val, i) {
		if(val.type == "input") {
			str += '<div class="col-sm-6" style="margin-top: 10px;"><div class="input-group"><span class="input-group-addon">' + val.txt + '</span><input type="text" class="form-control searchItemLayerInput" searchname=' + val.searchName + '></div></div>';
		}
		if(val.type == "select") {
			var selectOptArr = val.options.split("|");
			var selectOptVal = val.changeOpt.split("|");
			var opt = "";
			selectOptArr.forEach(function(v, j) {
				opt += "<option value=" + selectOptVal[j] + ">" + v + "</option>";
			});
			str += '<div class="col-sm-6" style="margin-top: 10px;"><div class="input-group"><span class="input-group-addon">' + val.txt + '</span><select class="form-control searchItemLayerInput addItemLayerSelect" searchname=' + val.searchName + '>' + opt + '</select></div></div>';
		}
		if(val.type == "dataTime") {
			str += '<div class="col-sm-6" style="margin-top: 10px;"><div class="input-group"><span class="input-group-addon">' + val.txt + '</span><input type="date" mark=' + val.mark + ' class="form-control searchItemLayerInput searchItemLayerDate" searchname=' + val.searchName + '></div></div>';
		}
		if(val.type == "layer") {
			str += '<div class="col-sm-6" style="margin-top: 10px;"><div class="input-group"><span class="input-group-addon">' + val.txt + '</span><input type="text" class="form-control searchItemLayerInput searchItemLayerTK" searchname=' + val.searchName + '><span class="input-group-btn"><button class="btn btn-info" type="button" style="height: 34px;" onclick=' + val.action + '><i class="glyphicon glyphicon-search"></i></button></span></div></div>';
		}
		if(val.type == "table") {
			myTable = val.table;
		}
	});
	$("#searchItemLayer").html(str);
	top.layer.open({
		type: 1,
		content: $("#searchItemLaye").html(),
		anim: '1',
		area: ['650px', "300px"],
		shade: 0,
		title: biolims.common.find,
		btn: biolims.common.selected,
		yes: function(index, layero) {
			//获取添加的数据
			var searchItemLayerValue = {};
			$(".searchItemLayerInput", parent.document).each(function(i, val) {
				if($(val).hasClass("addItemLayerSelect")) {
					var k = $(val).attr("searchname");
					var val = $(val).children("option:selected").val();
					searchItemLayerValue[k] = val;
				} else if($(val).hasClass("searchItemLayerDate")) {
					var k = $(val).attr("searchname");
					var val = $(val).attr("mark") + $(val).val();
					searchItemLayerValue[k] = val;
				} else if($(val).hasClass("searchItemLayerTK")) {
					var k = $(val).attr("searchname");
					searchItemLayerValue[k + "-name"] = $(val).val();
					searchItemLayerValue[k + "-id"] = $(val).attr("sid");
				} else {
					var k = $(val).attr("searchname");
					if($.trim($(val).val()) != null && $.trim($(val).val()) != "") {
						searchItemLayerValue[k] = "%" + $.trim($(val).val()) + "%";
					} else {
						searchItemLayerValue[k] = $.trim($(val).val());
					}
				}
			});
			//重新加载datatable
			var query = JSON.stringify(searchItemLayerValue);
			var param = {
				query: query
			};
			
			/*
			 * 新建进去 点击查询  由于session 中存了query 当点击列表按钮时刷新列表页面
			 * query  值还在  列表报错
			 * */
			
//			sessionStorage.setItem("searchContent", query);
			myTable.settings()[0].ajax.data = param;
			myTable.ajax.reload();
			top.layer.close(index);
		},
		btnAlign: 'c',
		end: function() {
			$("#searchItemLaye").remove();
		}
	});
}
//恢复之前查询的状态
function recoverSearchContent(table) {
	var searchContent = sessionStorage.getItem("searchContent");
	if(!searchContent) {
		return false;
	}
	var flag = false;
	for(var k in searchContent) {
		if(searchContent[k]) {
			flag = true;
		}
	}
	if(flag) {
		var param = {
			query: searchContent
		};

		table.settings()[0].ajax.data = param;
		setTimeout(function() {
			table.ajax.reload();
		}, 500);

	}
}

//复制行
function copyRow(ele) {
	var rows = ele.find(".selected");
	var length = rows.length;
	/*if(!length || length > 1) {
		top.layer.msg(biolims.common.pleaseSelectAPieceOfData);
	}*/
	if(length==1){
		var row = rows.clone(true);
		row.find("td").eq(1).text("");
		row.find(".icheck").iCheck('uncheck').val("");
		rows.after(row);
		checkall(ele);
	}else if(!length||length<1){
		top.layer.msg(biolims.common.pleaseSelectAPieceOfData);
	}else{
		top.layer.msg("只能选中一行复制");
	}
}
//时间戳转化为具体时间
function format(timestamp) {
	var time = new Date(parseInt(timestamp));
	var year = time.getFullYear();
	var month = time.getMonth() + 1;
	month = month < 10 ? '0' + month : month;
	var date = time.getDate();
	date = date < 10 ? '0' + date : date;
	var hours = time.getHours();
	hours = hours < 10 ? '0' + hours : hours;
	var minutes = time.getMinutes();
	minutes = minutes < 10 ? '0' + minutes : minutes;
	var seconds = time.getSeconds();
	seconds = seconds < 10 ? '0' + seconds : seconds;
	return year + '-' + month + '-' + date;
}
//主表刷新table
function tableRefresh() {
	// 解禁固定列
	$("#fixdeLeft2").show();
	// 重新加载数据
	var table = $(".dataTables_scrollBody").children("table").attr("id");
	$("#" + table).DataTable().ajax.reload();
}
//子表刷新table
function tableRefreshItem() {
	// 解禁列显示隐藏
	$(".colvis").show();
	// 解禁添加明细
	$(".addSampleInfo").attr("disabled", false);
	$(".addSampleInfo").show();
	// 解禁固定列
	$("#fixdeLeft2").show();
	// 重新加载数据
	$(".HideShowPanel").each(function() {
		if($(this).css("display") == "block") {
			var table = $(this).find(".dataTables_scrollBody").children("table").attr("id");
			$("#" + table).DataTable().ajax.reload();
		}
	});
}
//打印表格
function printDataTablesItem() {
	$(".HideShowPanel").each(function() {
		if($(this).css("display") == "block") {
			$(this).find(".buttons-print").click();
		}
	});
}
//复制表格
function copyDataTablesItem() {
	$(".HideShowPanel").each(function() {
		if($(this).css("display") == "block") {
			$(this).find(".buttons-copy").click();
		}
	});
}
//导出Excel
function excelDataTablesItem() {
	$(".HideShowPanel").each(function() {
		if($(this).css("display") == "block") {
			$(this).find(".buttons-excel").click();
		}
	});
}
//导出CSV
function csvDataTablesItem() {
	$(".HideShowPanel").each(function() {
		if($(this).css("display") == "block") {
			$(this).find(".buttons-csv").click();
		}
	});
}
//固定前两列(单个datatables)
function fixedCol(num) {
	var fixedNum = parseInt(num) + 1;
	// 禁用列显示隐藏
	$(".colvis").hide();
	// 禁用添加明细
	$(".addItem").hide();
	var table = $(".dataTables_scrollBody").children("table").attr("id");
	var myTable = $('#' + table).DataTable();
	
	new $.fn.dataTable.FixedColumns(myTable, {
		leftColumns: fixedNum,
		drawCallback:function(ele){
			console.log(777)
			$('.DTFC_Cloned').find('.icheck').iCheck({
				checkboxClass: 'icheckbox_square-blue',
				increaseArea: '20%' // optional
			});
			var icheckss = $('.DTFC_Cloned').find("tbody .icheck");
			// $('.DTFC_Cloned').find('.icheck').attr('id','#checkalll')
			// $("#checkalll").on('click', function(event) {
			// 	console.log(33)
			// 	if($(this).is(':checked')) {
			// 		$('.DTFC_Cloned').find("tbody input").iCheck('check');
			// 	} else {
			// 		$('.DTFC_Cloned').find("tbody input").iCheck('uncheck'); //移除 checked 状态 
			// 	}
			// });
				
			icheckss.on('ifChanged', function(e) {
				if($(this).is(':checked')) {
					$(this).parent("div").parent("td").parent("tr").addClass("selected");
					var tdindex = $(this).parent("div").parent("td").parent("tr").index();
					
					$('#main tbody tr').eq(tdindex).addClass("selected");
				} else {
					$(this).parent("div").parent("td").parent("tr").removeClass("selected");
					var tdIndexx = $(this).parent("div").parent("td").parent("tr").index();
					
					$('#main tbody tr').eq(tdIndexx).removeClass("selected");
				}
				
			});
			
			

			
		}
	});
}
//固定前两列(多个datatables)
function itemFixedCol(num) {
	var fixedNum = parseInt(num) + 1;
	$(".HideShowPanel").each(function() {
		if($(this).css("display") == "block") {
			// 禁用列显示隐藏
			$(this).find(".colvis").hide();
			// 禁用添加明细
			$(this).find(".addItem").hide();
			var table = $(this).find(".dataTables_scrollBody").children("table").attr("id");
			var myTable = $('#' + table).DataTable();
			new $.fn.dataTable.FixedColumns(myTable, {
				leftColumns: fixedNum
			});
		}
	});
}

//解除固定
function unfixde() {
	window.location.reload();
}
//上传附件
function fileInput(useType, modelType, contentId) {
	return $("#uploadFileVal").fileinput({
		uploadUrl: ctx + "/attachmentUpload?useType=" + useType + "&modelType=" + modelType + "&contentId=" + contentId, //上传的地址
		uploadAsync: true,
		language: biolims.common.language,
		//showPreview: false,
		enctype: 'multipart/form-data',
		elErrorContainer: '#kartik-file-errors',
	});
}
//上传Csv
function fileInputCsv(data) {
	return $("#uploadCsvVal").fileinput({
		uploadUrl: ctx + "/attachmentUpload", //上传的地址
		uploadAsync: true,
		language: biolims.common.language,
		textEncoding: 'GBK',
		//showPreview: false,
		enctype: 'multipart/form-data',
		elErrorContainer: '#kartik-file-errors',
		allowedFileExtensions: ["csv", "xls", "xlsx"],
		uploadExtraData: data
	});
}

// 点击上一步下一步切换
function stepViewChange() {
	var bpmTaskId = $("#bpmTaskId").val();
	var handlemethod = $("#handlemethod").val();
	if(bpmTaskId != "null" || handlemethod == "view") {
		$(".wizard_steps .step").removeClass("selected").removeClass("disabled").addClass("done");
		$("#next").hide();
		$("#pre").hide();
	}
	$("#next").unbind("click").click(
		function() {
			$("#pre").attr("disabled", false);
			var index = $(".wizard_steps .step").index(
				$(".wizard_steps .selected"));
			if(index == $(".wizard_steps .step").length - 1) {
				$(this).attr("disabled", true);
				return false;
			}
			$(".HideShowPanel").eq(index + 1).slideDown().siblings(
				'.HideShowPanel').slideUp();
			$(".wizard_steps .step").eq(index).removeClass("selected")
				.addClass("done");
			var nextStep = $(".wizard_steps .step").eq(index + 1);
			if(nextStep.hasClass("disabled")) {
				nextStep.removeClass("disabled").addClass("selected");
			} else {
				nextStep.removeClass("done").addClass("selected");
			}
			//table刷新和操作显示
			$(".box-tools").show();
			// 回到顶部
			$('body,html').animate({
				scrollTop: 0
			}, 500, function() {
				var table = $(".HideShowPanel").eq(index + 1).find(".dataTables_scrollBody").children("table").attr("id");
				$("#" + table).DataTable().ajax.reload();
			});
		});
	$("#pre").click(
		function() {
			$("#next").attr("disabled", false);
			var index = $(".wizard_steps .step").index(
				$(".wizard_steps .selected"));
			if(index == 0) {
				$(this).attr("disabled", true);
				//table刷新和操作隐藏
				$(".box-tools").hide();
				return false;
			}
			$(".HideShowPanel").eq(index - 1).slideDown().siblings(
				'.HideShowPanel').slideUp();
			$(".wizard_steps .step").eq(index).removeClass("selected")
				.addClass("done");
			$(".wizard_steps .step").eq(index - 1).removeClass("done")
				.addClass("selected");
			$('body,html').animate({
				scrollTop: 0
			}, 500);
		});
	$(".step").click(
		function() {
			if($(this).hasClass("done")) {
				var index = $(".wizard_steps .step").index($(this));
				$(".wizard_steps .selected").removeClass("selected")
					.addClass("done");
				$(this).removeClass("done").addClass("selected");
				$(".HideShowPanel").eq(index).slideDown().siblings(
					'.HideShowPanel').slideUp();
				//table刷新和操作隐藏
				if(index == 0) {
					$(".box-tools").hide();
				} else {
					$(".box-tools").show();
					var table = $(".HideShowPanel").eq(index).find(".dataTables_scrollBody").children("table").attr("id");
					$("#" + table).DataTable().ajax.reload();
				}

			} else if($(this).hasClass("disabled")) {
				top.layer.msg(biolims.order.finishPre);
			}
		});
}
window.ajax = function(method, url, data, callback, options) {

	options = options || {};

	options.type = method.toUpperCase();
	options.url = ctx + url;
	options.data = data;
	options.contentType = "application/x-www-form-urlencoded; charset=utf-8";
	options.async = false;
	options.dataType = "json";

	options.success = callback;
	$.ajax(options);
};

function bpmTask(bpmTaskId) {
	if(bpmTaskId != null && bpmTaskId != "") {
		$("#tjsp").hide()
		$("#prev").hide()
		$("#sp").show()
		$("#finish").hide()
	}
}

function btnChangeDropdown(table, btn, options, savename) {
	var lis = '';
	options.forEach(function(v, i) {
		lis += '<li><a href="###">' + v + '</a></li>';
	});
	table.on('init.dt', function() {
		btn.addClass("dropdown-toggle").attr("data-toggle", "dropdown").append('<span class="caret"></span>').wrap("<div class='btn-group'></div>").after('<ul class="dropdown-menu">' + lis + '</ul>');
		btn.next("ul").children("li").click(function() {
			var rows = table.find(".selected");
			var length = rows.length;
			if(length) {
				rows.find("td[savename='" + savename + "']").text($(this).text());
				rows.addClass("editagain");
			} else {
				top.layer.msg(biolims.common.pleaseSelectData);
			}
		});
	});
}

function settextreadonly() {
	jQuery(":text, textarea").each(function() {
		var _vId = jQuery(this).attr('id');
		jQuery(this).css("background-color", "#eee").attr("readonly", "readOnly");
		if(_vId == 'actiondropdown_textbox')
			settextread(_vId);
	});
}

function settextread(name) {
	document.getElementById(name).className = "input_parts text input default  readonlyfalse false";
	document.getElementById(name).readOnly = false;
}

//必填验证
function requiredFilter() {
	var flag=true;
	$(".requiredimage").each(function(i, v) {
		var ipt = $(v).parent(".input-group-addon").siblings("input[type='text']");
		if(ipt.length) {
			if(!ipt.val()) {
				var title = $(v).parent(".input-group-addon").text();
				var setTop = $(v).offset().top;
				$("body").animate({
					scrollTop: setTop-200
				}, 500);
				ipt.focus();
				top.layer.msg(biolims.common.pleaseFillInfo1 + title);
				flag=false;
				return false;
			}
		}
	});
	return flag;
}

//选中刷新和修改
function checkedUpdata(ele) {
	var ths = ele.find("th");
	//获取能被修改的列的表头并生成输入框
	var str = "";
	if(!$("#formItem").children("div").length){
	ths.each(function(i, val) {
		if(i != 0) {
			var edit = $(val).attr("key");
			var txt = $(val).text();
			//var val = $(tds[i]).text();
			if(edit == "select") {
				var selectopt = $(val).attr("selectopt");
				var selectOptArr = selectopt.split("|");
				var opt = "";
				selectOptArr.forEach(function(v, i) {
					if(v == val) {
						opt += "<option selected>" + v + "</option>";
					} else {
						opt += "<option>" + v + "</option>";
					}
				});
				str += '<div class="col-sm-12"><div class="input-group input-group-sm"><span class="input-group-addon">' + txt + '</span><select class="form-control editItemLayerInput addItemLayerSelect">' + opt + '</select></div></div>';
			} else if(edit == "date") {
				str += '<div class="col-sm-12"><div class="input-group input-group-sm"><span class="input-group-addon">' + txt + '</span><input type="text" class="form-control editItemLayerInput editItemLayerDate"></div></div>';
			} else if(edit == "dateBillingTime") {
				str += '<div class="col-sm-12"><div class="input-group input-group-sm"><span class="input-group-addon">' + txt + '</span><input type="text" class="form-control editItemLayerInput editItemLayerDate" disabled></div></div>';
			} else if(edit == "edit") {
				str += '<div class="col-sm-12"><div class="input-group input-group-sm"><span class="input-group-addon">' + txt + '</span><input type="text" class="form-control editItemLayerInput"></div></div>';
			} else if(edit == "method") {
				str += '<div class="col-sm-12"><div class="input-group input-group-sm"><span class="input-group-addon">' + txt + '</span><input type="text" class="form-control editItemLayerInput" disabled> <span class="input-group-btn"><button class="btn btn-info" type="button" onclick="' + $(val).attr("method") + '(this)"><span class="glyphicon glyphicon-search"></span></button></span></div></div>';
			} else if(edit == "department"){
				str += '<div class="col-sm-12"><div class="input-group input-group-sm"><span class="input-group-addon">' + txt + '</span><input type="text" id="department" class="form-control editItemLayerInput" disabled></div></div>';
			} else if(edit == "dicSampleType-id"){
				str += '<div class="col-sm-12"><div class="input-group input-group-sm"><span class="input-group-addon">' + txt + '</span><input type="text" id="dicSampleType-id" class="form-control editItemLayerInput" ></div></div>';
			} else if(edit == "state"){
				str += '<div class="col-sm-12"><div class="input-group input-group-sm"><span class="input-group-addon">' + txt + '</span><input type="text"  class="form-control editItemLayerInput" disabled></div></div>';
			} else if(edit == "isHurry"){
				str += '<div class="col-sm-12"><div class="input-group input-group-sm"><span class="input-group-addon">' + txt + '</span><input type="text"  class="form-control editItemLayerInput" disabled></div></div>';
			} else if(edit == "nextFlowId"){
				str += '<div class="col-sm-12"><div class="input-group input-group-sm"><span class="input-group-addon">' + txt + '</span><input type="text" id="nextFlowId" class="form-control editItemLayerInput" disabled></div></div>';
			} else if(edit == "productId"){
				str += '<div class="col-sm-12"><div class="input-group input-group-sm"><span class="input-group-addon">' + txt + '</span><input type="text" id="productId" class="form-control editItemLayerInput" disabled></div></div>';
			} else {
				str += '<div class="col-sm-12"><div class="input-group input-group-sm"><span class="input-group-addon">' + txt + '</span><input type="text" class="form-control editItemLayerInput" ></div></div>';
			}
		}
	});
	$("#formItem").append(str);
	$(".editItemLayerDate").datepicker({todayHighlight: true,
		language: "zh-TW",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});
	}
	/*$('#main tbody .icheck').on('ifChecked', function() {*/
		/*$('#main tbody .icheck').not(this).iCheck('uncheck');*/
	
	$('#main tbody tr').on('click', function() {
		if(typeof jiazai === "function"){
			jiazai();
		}
		
		var row = $(this)/*.parents("tr")*/;
		//加载样式
		$('#main tbody tr').removeClass("sel");
		if($(this).is('.odd')){
			$(this).css("background-color:#5AC8D8");
		}
		$(this).addClass("sel");
		var tds = row.children("td");
		var thisTextArr = [];
		tds.each(function(i, val) {
			if(i != 0) {
				thisTextArr.push($(val).text());
			}
		});
		$(".editItemLayerInput").each(function(i, val) {
			if($(val).hasClass("addItemLayerSelect")) {
				$(val).children("option").each(function(j, opt) {
					if($(opt).val() == thisTextArr[i]) {
						$(opt).prop("selected", true);
					} else {
						$(opt).prop("selected", false);
					}
				});
			} else {
				$(val).val(thisTextArr[i]);
			}
		});
		$("#preItem").unbind("click").click(function() {
			$('#main tbody tr').removeClass("sel");
			row = row.prev("tr").click();
			//$(row).addClass("sel");
			console.log(row.length);
			if(!row.length) {
				top.layer.msg(biolims.common.noPreData);
				row=$('#main tbody tr:first');
				$(row).addClass("sel");
				console.log(row.length);
				return false;
			}
			
			var tds = row.children("td");
			var thisTextArr = [];
			tds.each(function(i, val) {
				if(i != 0) {
					thisTextArr.push($(val).text());
				}
			});
			$(".editItemLayerInput").each(function(i, val) {
				if($(val).hasClass("addItemLayerSelect")) {
					$(val).children("option").each(function(j, opt) {
						if($(opt).val() == thisTextArr[i]) {
							$(opt).prop("selected", true);
						} else {
							$(opt).prop("selected", false);
						}
					});
				} else {
					$(val).val(thisTextArr[i]);
				}
			});
		})
		$("#nextItem").unbind("click").click(function() {
			$('#main tbody tr').removeClass("sel");
			row = row.next("tr").click();
			//$(row).addClass("sel");
			if(!row.length) {
				top.layer.msg("下一条数据不存在");
				row=$('#main tbody tr:last');
				$(row).addClass("sel");
				return false;
			}
			var tds = row.children("td");
			console.log(tds);
			var thisTextArr = [];
			tds.each(function(i, val) {
				if(i != 0) {
					thisTextArr.push($(val).text());
				}
			});
			console.log(thisTextArr);
			$(".editItemLayerInput").each(function(i, val) {
				if($(val).hasClass("addItemLayerSelect")) {
					$(val).children("option").each(function(j, opt) {
						if($(opt).val() == thisTextArr[i]) {
							$(opt).prop("selected", true);
						} else {
							$(opt).prop("selected", false);
						}
					});
				} else {
					$(val).val(thisTextArr[i]);
				}
			});
		});
		$("#isThis").unbind("click").click(function() {
			var addItemLayerValue = [];
			var ids;
			$(".editItemLayerInput").each(function(i, val) {
				addItemLayerValue.push($(val).val());
				if(i==0){
					ids=$(val).val();
					console.log(ids);
				}
	
			});
			if(typeof jiazai === "function"){
				jiazai();
			}
			/*$.ajax({
				type: 'post',
				url: "/experiment/plasma/sampleReceiveEx/editExReceive.action",
				
				data: {
					id: ids 
				},
				success: function(data) {
					console.log(data);
				}
				});*/
			//回填tr
			console.log(addItemLayerValue.length);
			console.log(tds.length);
			for(var i = 1; i < tds.length; i++) {
				var width = $(tds[i]).width();
				$(tds[i]).css({
					"overflow": "hidden",
					"text-overflow": "ellipsis",
					"max-width": width + "px",
				});
				$(tds[i]).text(addItemLayerValue[i - 1]);
			}
			//row.addClass("editagain");
			save();
			//top.layer.msg(biolims.main.success);
		})
	});
	//	top.layer.open({
	//		type: 1,
	//		content: $("#editItemLaye").html(),
	//		anim: '1',
	//		area: ['650px', "300px"],
	//		shade: 0,
	//		title: biolims.common.vim,
	//		btn: ['<<', biolims.common.selected, '>>'],
	//		yes: function(index, layero) {
	//			row.find(".icheck").iCheck('uncheck');
	//			row = row.prev("tr");
	//			if(!row.length) {
	//				top.layer.msg(biolims.common.noPreData);
	//				top.layer.close(index);
	//				return false;
	//			}
	//			row.find(".icheck").iCheck('check');
	//			tds = row.children("td");
	//			var thisTextArr = [];
	//			tds.each(function(i, val) {
	//				if(i != 0) {
	//					thisTextArr.push($(val).text());
	//				}
	//			});
	//			$(".editItemLayerInput", parent.document).each(function(i, val) {
	//				if($(val).hasClass("addItemLayerSelect")) {
	//					$(val).children("option").each(function(j, opt) {
	//						if($(opt).val() == thisTextArr[i]) {
	//							$(opt).prop("selected", true);
	//						} else {
	//							$(opt).prop("selected", false);
	//						}
	//					});
	//				} else {
	//					$(val).val(thisTextArr[i]);
	//				}
	//			});
	//			return false;
	//		},
	//		btn2: function(index, layero) {
	//			//获取添加的数据
	//			var addItemLayerValue = [];
	//			$(".editItemLayerInput", parent.document).each(function(i, val) {
	//				addItemLayerValue.push($(val).val());
	//			});
	//			//回填tr
	//			for(var i = 1; i < tds.length; i++) {
	//				var width = $(tds[i]).width();
	//				$(tds[i]).css({
	//					"overflow": "hidden",
	//					"text-overflow": "ellipsis",
	//					"max-width": width + "px",
	//				});
	//				$(tds[i]).text(addItemLayerValue[i - 1]);
	//			}
	//			row.addClass("editagain");
	//			top.layer.msg(biolims.main.success);
	//			return false;
	//		},
	//		btn3: function(index, layero) {
	//			row.find(".icheck").iCheck('uncheck');
	//			row = row.next("tr");
	//			if(!row.length) {
	//				top.layer.msg(biolims.common.noNextData);
	//				top.layer.close(index);
	//				return false;
	//			}
	//			row.find(".icheck").iCheck('check');
	//			tds = row.children("td");
	//			var thisTextArr = [];
	//			tds.each(function(i, val) {
	//				if(i != 0) {
	//					thisTextArr.push($(val).text());
	//				}
	//			});
	//			$(".editItemLayerInput", parent.document).each(function(i, val) {
	//				if($(val).hasClass("addItemLayerSelect")) {
	//					$(val).children("option").each(function(j, opt) {
	//						if($(opt).val() == thisTextArr[i]) {
	//							$(opt).prop("selected", true);
	//						} else {
	//							$(opt).prop("selected", false);
	//						}
	//					});
	//				} else {
	//					$(val).val(thisTextArr[i]);
	//				}
	//			});
	//			return false;
	//		},
	//		btnAlign: 'c',
	//		success: function(index, layero) {
	//			$(".editItemLayerDate").datepicker({todayHighlight: true,
	//				language: "zh-TW",
	//				autoclose: true, //选中之后自动隐藏日期选择框
	//				format: "yyyy-mm-dd" //日期格式，详见 
	//			});
	//		},
	//		end: function() {
	//			$("#editItemLaye").remove();
	//		}
	//	});

}


//不含日期 只有时分 选择 的方法
function newTimePacker(dom,e) {
	
    var hours = null;//存储 "时"
    var minutes = null;//存储 "分"
    var clientY = dom.offset().top + dom.height();//获取位置
    var clientX = dom.offset().left;
    var date = new Date();
    var nowHours = date.getHours();
    var nowMinutes = date.getMinutes();
    var time_hm=/^(0\d{1}|\d{1}|1\d{1}|2[0-3]):([0-5]\d{1})$/; //时间正则，防止手动输入的时间不符合规范
    var inputText = dom.is("input") ? dom.val():dom.text();
    //插件容器布局
    var html = '';
    html += '<div class="timePacker">';
        html += '<div class="timePacker-hours" style="display: block;">';
            html += '<div class="timePacker-title"><span>小时</span><span class="timePacker-close" title="关闭时间框"><img src="../../../images/project_close.png"/> </span></div>';
            html += '<div class="timePacker-content">';
                html += '<ul>';
                    var i = 0;
                    while (i < 24)
                    {
                        i = i < 10 ? "0" + i : i;
                        if(inputText !== "" && Number(inputText.split(":")[0]) === i){
                            html += '<li class="hoursList timePackerSelect">'+i+'</li>';
                            hours = Number(inputText.split(":")[0]);
                        }else{
                            if(i === nowHours){
                                html += '<li class="hoursList" style="color: #007BDB;">'+i+'</li>';
                            }else{
                                html += '<li class="hoursList">'+i+'</li>';
                            }
                        }
                        i++;
                    }
                html += '</ul>';
            html +=  '</div>';
        html += '</div>';
        html += '<div class="timePacker-minutes" style="display: none;">';
            html += '<div class="timePacker-title"><span>分钟</span><span class="timePacker-close" title="关闭时间框"><img src="../../../images/project_close.png"/> </span></div>';
            html += '<div class="timePacker-content">';
                html += '<ul>';
                    var m = 0;
                    while (m < 60)
                    {
                        var textM = m < 10 ? "0" + m : m;
                        if(inputText !== "" && Number(inputText.split(":")[1]) === textM){
                            html += '<li class="mList timePackerSelect">'+textM+'</li>';
                            minutes = Number(inputText.split(":")[1]);
                        }else{
                            if(m === nowMinutes){
                                html += '<li class="mList" style="color: #007BDB;">'+textM+'</li>';
                            }else{
                                html += '<li class="mList">'+textM+'</li>';
                            }
                        }
                        m++;
                    }
                html += '</ul>';
            html +=  '</div>';
        html += '</div>';
    html += '</div>';
    if($(".timePacker").length > 0){
        $(".timePacker").remove();
    }
    $("body").append(html);
    $(".timePacker").css({
        position:"absolute",
        top:clientY,
        left:clientX
    });
    var _con = $(".timePacker"); // 设置目标区域,如果当前鼠标点击非此插件区域则移除插件
    $(document).mouseup(function(e){
        if(!_con.is(e.target) && _con.has(e.target).length === 0){ // Mark 1
            _con.remove();
        }
    });
    //小时选择
    $(".hoursList").bind('click',function () {
        $(this).addClass("timePackerSelect").siblings().removeClass("timePackerSelect");
        hours = $(this).text();
        var timer = setTimeout(function () {
            $(".timePacker-hours").css("display","none");
            $(".timePacker-minutes").fadeIn();
            if(minutes !== null){
                var getTime = hours + ":" + minutes;
                if(time_hm.test(getTime)){
                    dom.removeClass("errorStyle");
                
                }
                dom.is("input") ? dom.val(getTime):dom.text(getTime);
            }
            clearTimeout(timer);
        },100);
    });
    //返回小时选择
    $(".timePacker-back-hours").bind('click',function () {
        var timer = setTimeout(function () {
            $(".timePacker-minutes").css("display","none");
            $(".timePacker-hours").fadeIn();
            clearTimeout(timer);
        },500);
    });
    //分钟选择
    $(".mList").bind('click',function () {
        $(this).addClass("timePackerSelect").siblings().removeClass("timePackerSelect");
        minutes = $(this).text();
        var timer = setTimeout(function () {
            var getTime = hours + ":" + minutes;
            if(time_hm.test(getTime)){
                dom.removeClass("errorStyle");
            }
            dom.is("input") ? dom.val(getTime):dom.text(getTime);
            clearTimeout(timer);
            _con.remove();
            dom.parent().addClass('editagain');
        },500);
    })
    $('.timePacker-close').on('click',function(){
    	
        _con.remove();
    })
}