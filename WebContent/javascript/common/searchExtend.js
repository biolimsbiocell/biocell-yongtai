/* 
 * 文件名称 :
 * 创建者 : 郭恒开
 * 创建日期: 2018/04/25
 * 
 * 
 */
(function($) {
	"use strict";
	$.fn.searchExtend = function(options) {
		var target = $(this);
		$("#currentYear").val(new Date().getFullYear());
		if(options.searchOptions) {
			options.searchOptions.forEach(function(v, i) {
				target.append('<h3 class="panel-title">' +
					'<i class="glyphicon glyphicon-bookmark"></i>' +
					v +
					'</h3>' +
					'<div style="min-height: 30px;overflow:hidden;"></div>' +
					'<button  open="N" class="btn btn-xs btn-primary center-block" style="display: none;">More...</button>');
			});
		} else {
			top.layer.msg("请正确填写参数配置！"); //只有开发人员才能看到，可不国际化。
			return false;
		}
		//画饼图充饥
		target.dateYearPie = function(data) {
			var options = {
				title: {
					text: '',
					x: 'center',
					textStyle: {
						fontSize: 15,
						fontWeight: 'normal',
						color: '#333'
					}
				},
				tooltip: {
					trigger: 'item',
					formatter: "{b}:{c}({d}%)"
				},
				series: [{
					name: '',
					type: 'pie',
					radius: '55%',
					center: ['50%', '60%'],
					data: data,
					itemStyle: {
						normal: {
							label: {
								show: true,
								formatter: '{b}:{c}({d}%)'
							},
							labelLine: {
								show: true
							}
						},
						emphasis: {
							shadowBlur: 10,
							shadowOffsetX: 0,
							shadowColor: 'rgba(0, 0, 0, 0.5)'
						}
					}
				}]
			};
			myChart11.setOption(options);
			myChart11.hideLoading();

		};
		//画柱图
		target.dateYearBar = function(month, num) {

			var options = {
				color: ['#3398DB'],
				title: {
					x: 'center',
					textStyle: {
						fontSize: 15,
						fontWeight: 'normal',
						color: '#333'
					}
				},
				tooltip: {},
				legend: {
					data: ["123"]
				},
				xAxis: {
					data: month
				},
				yAxis: {},
				series: [{
					// 根据名字对应到相应的系列
					name: "提示：",
					type: 'bar',
					data: num
				}]
			};
			myChart.setOption(options);
			myChart.hideLoading();

		};
		//发ajax去搞柱图和饼图的数据
		target.renderCharts = function(month, query) {
			var data = {};
			target.children("div").each(function(i, v) {
				if($(v).attr("saveValue")) {
					var k = $(v).attr("saveName");
					data[k] = $(v).attr("saveValue").split(",");
				}
			});
			data.year = $("#currentYear").val();
			data.month = month;
			data.query = query;
			$.ajax({
				type: "post",
				data: data,
				url: ctx + options.getChartUrl,
				success: function(data) {
					var data = JSON.parse(data);
					var bar = data.histogram;
					var month = [],
						num = [];
					for(var i = 0; i < bar.length; i++) {
						month.push(bar[i][0] + "月");
						num.push(bar[i][1]);
					}
					target.dateYearBar(month, num);
					var pie = data.pieChart;
					var pieArr = [];
					for(var i = 0; i < pie.length; i++) {
						var obj = {};
						obj.value = pie[i][1];
						obj.name = pie[i][0];
						pieArr.push(obj);
					}
					target.dateYearPie(pieArr);
				}
			});
		};
		//点击多选框拼数据
		target.clickCheckbox = function(data) {
			$("#searchOption input").on('ifChanged', function(event) {
				if($(this).is(':checked')) {
					var parentDiv = $(this).parents(".checkbox").parent("div");
					if(parentDiv.attr("saveValue")) {
						parentDiv.attr("saveValue", parentDiv.attr("saveValue") + "," + this.value);
					} else {
						parentDiv.attr("saveValue", this.value);
					}
				} else {
					//造个删除数组的方法啦
					Array.prototype.remove = function(val) {
						var index = this.indexOf(val);
						if(index > -1) {
							this.splice(index, 1);
						}
					};
					var parentDiv = $(this).parents(".checkbox").parent("div");
					var arr = parentDiv.attr("saveValue").split(",");
					arr.remove(this.value);
					parentDiv.attr("saveValue", arr);
				}
				target.renderCharts();
				target.reloadtable();
			});

		};
		//table表重新加载
		target.reloadtable = function(month) {
			var data = {};
			$("#searchOption").children("div").each(function(i, v) {
				if($(v).attr("saveValue")) {
					var k = $(v).attr("saveName");
					data[k] = $(v).attr("saveValue").split(",");
				}
			});
			data.year = $("#currentYear").val();
			data.month = month;
			var table = $(".dataTables_scrollBody").children("table").attr("id");
			var myTable = $('#' + table).DataTable();
			myTable.settings()[0].ajax.data = data;
			myTable.ajax.reload();
		}
		//搜索不常用的字段儿
		target.srarchUnusualItem = function(searchContent) {
			var str = "";
			searchContent.forEach(function(val, i) {
				if(val.type == "input") {
					str += '<div class="input-group" style="margin-top: 10px;">' +
						'<span class="input-group-addon">' + val.txt + '</span>' +
						'<input type="text" class="form-control input-sm">' +
						'<span class="input-group-btn">' +
						'<button class="btn btn-primary btn-sm" searchname="' + val.searchName + '" type="button">' +
						'go' +
						'</button>' +
						'</span>' +
						'</div>';
				}
				if(val.type == "select") {
					var selectOptArr = val.options.split("|");
					var selectOptVal = val.changeOpt.split("|");
					var opt = "";
					selectOptArr.forEach(function(v, j) {
						opt += "<option value=" + selectOptVal[j] + ">" + v + "</option>";
					});
					str += '<div class="input-group" style="margin-top: 10px;">' +
						'<span class="input-group-addon">' + val.txt + '</span>' +
						'<select name="" class="form-control input-sm">"' + opt + '"</select>' +
						'<span class="input-group-btn">' +
						'<button class="btn btn-primary btn-sm select" searchname="' + val.searchName + '" type="button">' +
						'go' +
						'</button>' +
						'</span>' +
						'</div>';
				}
				if(val.type == "dataTime") {
					str += '<div class="input-group" style="margin-top: 10px;">' +
						'<span class="input-group-addon">' + val.txt + '</span>' +
						'<input type="text" class="form-control input-sm searchItemDate" mark="' + val.mark + '">' +
						'<span class="input-group-btn">' +
						'<button class="btn btn-primary btn-sm date" searchname="' + val.searchName + '" type="button">' +
						'go' +
						'</button>' +
						'</span>' +
						'</div>';
				}
				if(val.type == "target") {
					var targetDiv = val.targetDiv;
					targetDiv.append(str);
					targetDiv.find(".searchItemDate").datepicker({
						language: "zh-TW",
						autoclose: true, //选中之后自动隐藏日期选择框
						format: "yyyy-mm-dd" //日期格式，详见 
					});
					//获取搜索的数据

					targetDiv.find(".btn-sm").click(function() {
						var searchItemlayerValue = {};
						if($(this).hasClass("select")) {
							var k = $(this).attr("searchname");
							var val = $(this).parents(".input-group").find("select").children("option:selected").val();
							searchItemlayerValue[k] = val;
						} else if($(val).hasClass("date")) {
							var k = $(this).attr("searchname");
							var inpt = $(this).parents(".input-group").find("input");
							var val = inpt.attr("mark") + inpt.val();
							searchItemlayerValue[k] = val;
						} else {
							var k = $(this).attr("searchname");
							var inpt = $(this).parents(".input-group").find("input");
							if(inpt.val() != null && inpt.val() != "") {
								searchItemlayerValue[k] = "%" + $.trim(inpt.val()) + "%";
							} else {
								searchItemlayerValue[k] = inpt.val();
							}
						}
						var query = JSON.stringify(searchItemlayerValue);
						var data = {};
						$("#searchOption").children("div").each(function(i, v) {
							if($(v).attr("saveValue")) {
								var k = $(v).attr("saveName");
								data[k] = $(v).attr("saveValue").split(",");
							}
						});
						data.year = $("#currentYear").val();
						data.query = query;
						var table = $(".dataTables_scrollBody").children("table").attr("id");
						var myTable = $('#' + table).DataTable();
						myTable.settings()[0].ajax.data = data;
						myTable.ajax.reload();
						target.renderCharts("", query)
					});
				}
			});
		}

		if(options.searchOptionsUrl) {
			$.ajax({
				type: "post",
				url: ctx + options.searchOptionsUrl,
				success: function(data, textStatus, jqXHR) {
					var data = JSON.parse(data);
					var index = 0;
					for(var k in data) {
						var item = '';
						data[k].forEach(function(v, i) {
							item += '<div class="checkbox">' +
								'<label>' +
								'<input type="checkbox" value="' + v[0] + '">' + v[1] + '</label>' +
								'<span class="label label-success">' + v[2] + '</span>' +
								'</div>';
						});
						var tatgetDiv = target.children("div").eq(index);
						tatgetDiv.append(item).attr("saveName", k);
						if(data[k].length > 8) {
							tatgetDiv.attr("h", tatgetDiv.height() + 30).height(224);
							tatgetDiv.next("button").show(function() {
								$(this).click(function() {
									if(this.getAttribute("open") == "N") { //展开
										this.setAttribute("open", "Y");
										this.innerText = "Less...";
										$(this).prev("div").animate({
											"height": $(this).prev("div").attr("h")
										}, 600, "linear");
									} else {
										this.setAttribute("open", "N");
										this.innerText = "More...";
										$(this).prev("div").animate({
											height: "224px"
										});
									}
								});
							});
						}
						index++;
					}
					//美化一下啦
					target.find("input").iCheck({
						checkboxClass: 'icheckbox_square-blue',
						increaseArea: '20%' // optional
					});
					//点击多选框拼数据
					target.clickCheckbox();
				}
			});
		}
		if(options.barChartId) {
			window.myChart = echarts.init(document.getElementById(options.barChartId));
			window.myChart11 = echarts.init(document.getElementById(options.pieChartId));
			target.renderCharts();
			$("#addYear").click(function() {
				$("#currentYear").val(parseInt($("#currentYear").val()) + 1);
				myChart.showLoading();
				myChart11.showLoading();
				target.renderCharts();
				target.reloadtable();
			})
			$("#subYear").click(function() {
				$("#currentYear").val(parseInt($("#currentYear").val()) - 1);
				var dateYear = $("#currentYear").val();
				myChart.showLoading();
				myChart11.showLoading();
				target.renderCharts();
				target.reloadtable();
			})
			myChart.on('click', function(param) {
				target.renderCharts(param.name.replace("月", ""), "");
				target.reloadtable(param.name.replace("月", ""), "");
			});
		}
		if(options.inputsearchOptions) {
			var searchContent = options.inputsearchOptions;
			target.srarchUnusualItem(searchContent);
		}

		return target;
	};

})(jQuery);