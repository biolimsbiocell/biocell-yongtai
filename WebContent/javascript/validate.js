/**
 * 说明: 校验是否是整数 参数: (1) num: 返回: true/false
 */
function isInteger(num) {
	var regexp = /^(-|\+)?\d+$/.test(num);
	if (regexp) {
		var h = num.substring(0, 1);
		if (h === '-' || h === '+') {
			if (num.length > 1) {
				var h2 = num.substring(1, 2);
				if (h2 === '0') {
					return false;
				}
			}
		} else if (h == '0' && num.length > 1) {
			return false;
		}
	}
	return regexp;
}
/**
 * 说明: 校验是否是正整数 参数: (1) num: 返回: true/false
 */
function isZInteger(num) {
	var regexp = /^(\+)?\d+$/.test(num);
	if (regexp) {
		var h = num.substring(0, 1);
		if (h === '+') {
			if (num.length > 1) {
				var h2 = num.substring(1, 2);
				if (h2 === '0') {
					return false;
				}
			}
		} else if (h == '0' && num.length > 1) {
			return false;
		}
	}
	return regexp;
}
/**
 * 说明:按范围校验年龄，即min<=age<=max 参数: (1) num: 最小值; (2)max:最大值；(3)age：年龄 返回:
 * true/false
 */
function checkAge(age, min, max) {
	if (isZInteger(age)) {
		if (min === "" && max === "") {
			return true;
		} else if (min === "" && max !== "") {
			return age <= max;
		} else if (min !== "" && max === "") {
			return age >= min;
		} else {
			return (age <= max) && (age >= min);
		}

	} else {
		return false;
	}
}
/**
 * 说明: 判断字符串的开头字母是由字母或下划线组成 参数: (1) str: 字符串 返回: 字母或下划线：true/false
 */
function IsChaAndNumberAndLineWithHead(str) {
	var flag = /^([a-zA-z_]{1})([\w]*)$/g.test(str);
	return flag;
}
/**
 * 说明: 邮箱地质校验 参数: (1) str: 字符串 返回: true/false
 */
function checkEmail(str) {
	var regexp = /^(\w)[^@]{0,16}(@)(\w){1,30}(.(\w){1,5})+$/;
	return (regexp.test(str));
}
/**
 * 说明: 邮政编码校验 参数: (1) str: 字符串 返回: true/false
 */
function isPostcode(v) {
	var regexp = /^(\d){6}$/;
	return (regexp.test(v));
}
/**
 * 说明: 身份证校验 参数: (1) str: 字符串 返回: true/false
 */
function isIDCard(str) {
	var regexp;
	if (str.length == 15) { // 15λ
		regexp = /^(\d{15})$/;
		return regexp.test(str);
	}
	if (str.length == 18) { // 18λ
		regexp = /^(\d{17})([1]{1})$/;
		if (!regexp.test(str))
			regexp = /^(\d{17})(\w{1})$/;
		return regexp.test(str);
	}
	return false;
}
/**
 * 说明: 校验小数的位数是否是1位 参数: (1) str: 返回: true/false
 */
function isFloat1(v) {
	var regexp = /^(-?)?(\d){1,11}((\.)(\d){1,1})?$/;
	return regexp.test(v);
}
/**
 * 说明: 校验小数的位数是否是2位 参数: (1) str: 返回: true/false
 */
function isFloat2(v) {
	var regexp = /^(-?)?(\d){1,11}((\.)(\d){1,2})?$/;
	return regexp.test(v);
}
/**
 * 说明: 校验小数的位数是否是3位 参数: (1) str: 返回: true/false
 */
function isFloat3(v) {
	var regexp = /^(-?)?(\d){1,11}((\.)(\d){1,3})?$/;
	return regexp.test(v);
}
/**
 * 说明: 验证表单中必须填写的属性 参数: (1) fields:字段属性 (2)names：字段中文名称 返回: string
 */
function commonFieldsNotNullVerify(fields, names) {
	var message = "";
	var count = 0;
	for ( var i = 0; i < fields.length; i++) {
		if (fields[i] == undefined || trim(fields[i]) == "") {
			count++;
			message = message + count + "." + names[i] + "<br>";
		}
	}
	return message;
	/*
	 * if(message!==""){ alert(message+"\n\n"); return false; }else{ return
	 * true; }
	 */
}
/**
 * 说明: 验证表单中必须填写的属性 参数: (1) fields:字段属性 (2)names：字段中文名称 返回: string
 */
function commonFieldsNotNullVerifyById(fields, names) {
	var message = "";
	var count = 0;
	for ( var i = 0; i < fields.length; i++) {

		if (fields[i] == undefined || trim(document.getElementById(fields[i]).value) == "") {
			count++;
			message = message + count + "." + names[i] + "<br>";
		}
	}
	return message;
	/*
	 * if(message!==""){ alert(message+"\n\n"); return false; }else{ return
	 * true; }
	 */
}
/**
 * 说明: 验证表单中textarea的字符个数 参数: 返回: string
 */
function commoncheckTextAreaLength(fields, num, names) {
	var message = "";
	var count = 0;
	for ( var i = 0; i < fields.length; i++) {
		if (checkTextAreaLength(fields[i], num[i])) {
			count++;
			message = message + count + "." + names[i] + "<br>";
		}
	}
	return message;
	/*
	 * if(message!==""){ alert(message+"\n\n"); return false; }else{ return
	 * true; }
	 */
}

/**
 * 说明: 校验textarea的字符个数 参数: (1) v :字符串 (2)length ：长度 返回: true/false
 */
function checkTextAreaLength(v, length) {
	if (len(v.value) > length) {
		alert(biolims.common.charNum + length + biolims.common.overLimit);
		v.focus();
	}
}

/**
 * 说明: 校验radio是否选中 参数: (1) 返回: 选中返回：radio的value，否则返回false。
 */
function checkRadioIsSelected(rid) {
	var x = document.getElementsByName(rid);
	var mycount = 0;
	var rvalue = "";
	for ( var i = 0; i < x.length; i++) {
		if (x[i].checked) {
			rvalue = x[i].value;
			mycount++;
		}
	}

	if (mycount == 0) {
		return false;
	} else {
		return rvalue;
	}
}

/**
 * 说明: 校验字符串是否为空 参数: (1) 字符串 返回: ture：字符串为空或" "
 */
function isEmpty(inputString) {
	if ((inputString == null) || (inputString.length == 0)) {
		return true;
	}
	for (i = 0; i < inputString.length; i++) {
		if (inputString.charAt(i) != " ") {
			return false;
		}
	}
	return true;
}

/**
 * 说明: 校验字符串是否为数字 参数: (1) 字符串 返回: ture：是数字
 */
function chknumber(checkStr) {
	var checkOK = "0123456789.";
	var allValid = true;
	var checkCode = 0;
	for (i = 0; i < checkStr.length; i++) {
		ch = checkStr.charAt(i);
		if (checkOK.indexOf(ch) == -1) {
			allValid = false;
			break;
		}
		if (ch == '.') {
			checkCode += 1;
			if (checkCode > 1) {
				allValid = false;
				break;
			}
		}
	}
	return (allValid);
}

// 格式化时间的函数
function formateTime(str) {
	timestr = Date.parse(str);
	entrytime = new Date(timestr);
	return entrytime.getFullYear() + "-" + (entrytime.getMonth() + 1) + "-" + entrytime.getDate();
}

// 去掉字符串左边的空格
function ltrim(s) {
	return s.replace(/^\s*/, "");
}

// 去掉字符串右边的空格
function rtrim(s) {
	return s.replace(/\s*$/, "");
}

// 去字符串左右两边的空格
function trim(s) {
	return rtrim(ltrim(s));
}

/**
 * 检查输入框中的中英文长度，英文字母长度为1，中文长度为2
 * 
 * @param strText
 *            要检查的字符串 验证字符串时先去掉字符串两边的空格在进行验证长度
 */
function validate_length(strText) {
	var len = 0;
	var str = trim(strText);
	for ( var i = 0; i < str.length; i++) {
		charx = str.charCodeAt(i);
		if (!(charx > 255 || charx < 0)) {
			len = len + 1;
		} else {
			len = len + 2;
		}
	}
	return len;
}
function len(strText) {
	var len = 0;
	var str = strText;
	for ( var i = 0; i < str.length; i++) {
		charx = str.charCodeAt(i);
		if (!(charx > 255 || charx < 0)) {
			len = len + 1;
		} else {
			len = len + 2;
		}
	}
	return len;
}
/**
 * 显示或隐藏div
 * 
 * @param id
 *            要显示隐藏的div的id 如果div是显示的则隐藏，如果div是隐藏的则显示
 */
function showCloseDiv(id) {
	var showid = document.getElementById(id);
	if (null != showid) {
		if (showid.style.display = "block") {
			showid.style.display = "none";
		}
		// else{
		// alert("777");
		// showid.style.display = "block"
		// alert("6666");
		// }
	}
}

/**
 * 把左边的资源加到右边的资源中
 * 
 * @param leftid
 *            左边的多选框id
 * @param rightid
 *            右边的多选框id
 */
function addOption(leftid, rightid) {
	var leftSelelct = document.getElementById(leftid);
	var rightSelect = document.getElementById(rightid);
	for ( var i = 0; i < leftSelelct.options.length; i++) {
		var flag = true;
		if (leftSelelct.options[i].selected) {
			var oOption = document.createElement("OPTION");
			oOption.value = leftSelelct.options[i].value;
			oOption.text = leftSelelct.options[i].text;
			for ( var j = 0; j < rightSelect.options.length; j++) {
				if (rightSelect.options[j].value == oOption.value) {
					flag = false;
				}
			}
			if (flag) {
				rightSelect.add(oOption);
			}
		}
	}
}

/**
 * 从右边的资源中删除一项
 * 
 * @param rightid
 *            右边的多选框的id
 */
function removeOption(rightid) {
	var rightSelect = document.getElementById(rightid);
	for ( var i = 0; i < rightSelect.options.length; i++) {
		if (rightSelect.options[i].selected) {
			var oOption = document.createElement("OPTION");
			oOption.value = rightSelect.options[i].value;
			oOption.text = rightSelect.options[i].text;
			rightSelect.remove(oOption);
		}
	}
}

/**
 * 选中select框中的所有选项
 * 
 * @param id
 *            多选框的id
 */
function selectAll(id) {
	var selectObj = document.getElementById(id);
	if (null != selectObj) {
		for ( var i = 0; i < selectObj.options.length; i++) {
			selectObj.options[i].selected = true;
		}
	}
}

/**
 * 打开一个新的窗口
 * 
 * @param url
 *            新窗口的链接
 * @param msg
 *            新窗口的窗口名
 */
function openNewWin(url, msg) {
	var win;
	win = window.open(url, msg,
			('left=100,top=100,width=480,height=600,status=no,toolbar=no,menubar=no,scrollbars,resizable=yes'));
	win.focus();
	return false;
}

/**
 * 在留言中增加"回复：某某"
 * 
 * @param id
 *            留言框
 * @param replayName
 *            要回复的人(一般为姓名)
 */
function addReply(id, replayName) {
	var content = document.getElementById(id);
	content.focus();
	var text = content.value;
	text = text + biolims.common.reply + replayName + "\n";
	content.value = text;
}

/**
 * 验证关键字(用","分隔的字符串)
 * 
 * @param tagid
 *            要验证的id
 * @param reg
 *            验证时用到的正则表达式
 * @param errorObj
 *            错误信息显示的位置的对象
 * @param msg
 *            错误消息
 */
function checkTags(tagid, reg, errorObj, msg) {
	var tags = document.getElementById(tagid).value;
	var temp_tags = tags.replace(/\，/g, ",");
	var tags_array = temp_tags.split(",");
	tags = temp_tags;
	for (loop = 0; loop < tags_array.length; loop++) {
		if (tags_array[loop].length > 0) {
			checkReg(reg, tags_array[loop], errorObj, msg)
		}
	}
	return true;
}

/**
 * copy网址
 * 
 * @param id
 *            要复制的url的输入框的id
 */
function copyUrl(id) {
	var url = document.getElementById(id);
	if (document.all) {
		url.select();
		window.clipboardData.clearData();
		window.clipboardData.setData("text", url.value);
		alert(biolims.common.urlCopySuccess);
	} else {
		alert(biolims.common.useShortcuts);
		url.select();
	}
}

/**
 * 检查字符串是否与正则表达式匹配
 * 
 * @param reg
 *            要使用的正则表达式
 * @param str
 *            要验证的字符串
 * @param errorObj
 *            错误信息显示的位置的对象
 * @param msg
 *            错误消息
 * 
 */
function checkReg(reg, str, errorObj, msg) {
	if (validate_length(str) != 0) {
		if (!reg.test(str)) {
			errorObj.innerHTML = "<p class='error_msg'>" + msg + "</p>";
			return false;
		}
	}
	return true;
}

/**
 * 验证是输入值的范围
 * 
 * @param id
 *            要验证的输入框的id
 * @param errorObj
 *            错误信息显示的位置的对象
 * @param msg
 *            错误信息
 * @param minLength
 *            最小范围（数字）
 * @param maxLength
 *            最大范围（数字）
 * 
 * 如果最小范围为1时，则相当于验证输入是否为空
 */
function validateInput(id, errorObj, msg, minLength, maxLength) {
	var obj = document.getElementById(id);
	var str = obj.value;
	if (minLength != null && validate_length(str) < minLength) {
		errorObj.innerHTML = "<ul class='error_msg'>" + msg + "<li></li></ul>";
		return false;
	}
	if (maxLength != null && validate_length(str) > maxLength) {
		errorObj.innerHTML = "<ul class='error_msg'>" + msg + "<li></li></ul>";
		return false;
	}
	return true;
}

/**
 * 判断select框是否为空
 * 
 * @param id
 *            要验证的输入框的id
 * @param errorObj
 *            错误信息显示的位置的对象
 * @param msg
 *            错误信息
 */
function validateSelect(id, errorObj, msg) {
	var obj = document.getElementById(id);
	if (obj.value == 0) {
		errorObj.innerHTML = "<ul class='error_msg'>" + msg + "<li></li></ul>";
		return false;
	}
	return true;
}

/**
 * 判断多选框是否为空
 * 
 * @param id
 *            要验证的输入框的id
 * @param errorObj
 *            错误信息显示的位置的对象
 * @param msg
 *            错误信息
 */
function validateCheckBox(id, errorObj, msg) {
	var obj = document.getElementsByName(id);
	var len = obj.length;
	var bool = false;
	for ( var i = 0; i < len; i++) {
		if (obj[i].checked) {
			bool = true;
		}
	}
	if (!bool) {
		errorObj.innerHTML = "<div id='error_div'><ul class='error_msg'>" + msg + "<li></li></ul></div>";
		return false;
	}
	return true;
}
