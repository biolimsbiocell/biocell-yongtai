/* 
 * 文件名称 :
 * 创建者 : 郭恒开
 * 创建日期: 2018/07/12
 * 文件描述: 修改日志
 * 
 */
$(function() {
	var colData = [{
			"data": 'logDate',
			"title": biolims.common.programName,
			"visible":false,
			"width" : "200px",
		},{
			"data": 'className',
			"title": biolims.common.programName,
			"width" : "200px",
		}, {
			"data": 'fileId',
			"title": biolims.common.recordID,
		}, {
			"data": 'userId',
			"title": biolims.master.operUserName,
		}, {
			"data": 'logDate',
			"title": biolims.master.operDate,
			"width" : "150px",
		}, {
			"data": 'modifyContent',
			"title":  biolims.common.recordChanges,
			"width" : "400px",
		},{
			"data": 'state',
			"title":  "状态",
			"visible":false,
			"width" : "400px",
		},{
			"data": 'stateName',
			"title":  "状态",
			"width" : "112px",
		}];
		
//		$.ajax({
//			type:"post",
//			url:window.ctx + "/system/customfields/findFieldByModuleValue.action",
//			async:false,
//			data:{
//				moduleValue : "SampleOrder"
//			},
//			success:function(data){
//				var objData = JSON.parse(data);
//				if(objData.success){
//					$.each(objData.data, function(i,n) {
//						var str = {
//							"data" : n.fieldName,
//							"title" : n.label
//						}
//						colData.push(str);
//					});
//					
//				}else{
//					top.layer.msg(biolims.tInstrumentBorrowDetail.tableError);
//				}
//			}
//		});
		
		console.log(colData);
	var options = table(true, "","/logInfo/showLogInfoListJson.action",colData , null)
	LoginfoList = renderRememberData($("#main"), options);
});
//弹框模糊查询参数
function searchOptions() {
	return [
		{
			"searchName": 'className',
			"type": "input",
			"txt":  biolims.common.programName,
		},
		{
			"searchName": 'fileId',
			"type": "input",
			"txt": biolims.common.recordID,
		},
		{
			"searchName": 'userId',
			"type": "input",
			"txt":   biolims.master.operUserName,
		},
		{
			"txt": biolims.sample.createDateStart,
			"type": "dataTime",
			"searchName": "logDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.sample.createDateEnd,
			"type": "dataTime",
			"searchName": "logDate##@@##2",
			"mark": "e##@@##",
		},
		{
			"txt": biolims.common.recordChanges,
			"type": "input",
			"searchName": 'modifyContent',
		},
		{

			"txt": "状态",
			"type": "select",
			"options": ""+"|"+"数据新增"+"|"+"数据删除"+"|"+"数据修改"+"|"+"登录"+"|"+"查询",
			"changeOpt": "''|1|2|3|4|5",
			"searchName": 'state',
		},
		{
			"type": "table",
			"table": LoginfoList
		}
	];
}
