$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'fileName',
		type : "string"
	});
	 fields.push({
		name : 'fileId',
		type : "string"
	});
	 fields.push({
			name : 'userId',
			type : "string"
		});
	 fields.push({
			name : 'logDate',
			type : "string"
		});
	 fields.push({
			name : 'logContent',
			type : "string"
		}); 
	 fields.push({
			name : 'className',
			type : "string"
		}); 
	 fields.push({
			name : 'modifyContent',
			type : "string"
		}); 
	 fields.push({
			name : 'modifyItemContent',
			type : "string"
		}); 
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : biolims.user.itemNo,
		width : 120,
		sortable : true,
		hidden:true
	});
	cm.push({
		dataIndex : 'fileName',
		header : biolims.user.eduName,
		width : 120,
		sortable : true,
		hidden:true
	});
  cm.push({
			dataIndex : 'className',
			header : biolims.common.programName,
			width : 200,
			sortable : true
		});
   cm.push({
		dataIndex : 'fileId',
		header : biolims.common.recordID,
		width : 120,
		sortable : true
	});
   cm.push({
		dataIndex : 'userId',
		header : biolims.master.operUserName,
		width : 80,
		sortable : true
	});
   cm.push({
		dataIndex : 'logDate',
		header : biolims.master.operDate,
		width : 130,
		sortable : true
	});

   cm.push({
		dataIndex : 'modifyContent',
		header : biolims.common.recordChanges,
		width : 500,
		sortable : true,
		editor : new Ext.form.TextArea({
			allowBlank : true
		})
	});
  
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/logInfo/showLogInfoListJson.action";
	var opts = {};
	opts.title = biolims.common.detail;
	opts.height = document.body.clientHeight-20;
	opts.tbar = [];

	
	dicMainTypeGrid = gridEditTable("show_dicmaintypelist_grid_id", cols, loadParam, opts);
	
});