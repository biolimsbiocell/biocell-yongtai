//window.onbeforeunload = function() {
//	if (document.getElementById('toolbarSaveButtonFlag') != null) {
//		if (document.getElementById('toolbarSaveButtonFlag').value != 'save') {
//			if (is_form_changed()) {
//
//				return "信息已改变,确定离开不保存记录吗?";
//			}
//			if (Ext.getCmp('gridGrid') != null) {
//				if (is_grid_changed(gridGrid)) {
//
//					return "信息已改变,确定离开不保存记录吗?";
//
//				}
//
//			}
//		}
//	}
//};

function is_grid_changed(gridName) {

	if (gridName != undefined) {
		if (gridName.store.getModifiedRecords().length > 0)
			return true;

	}
	if (window.frames['tab11frame'] != undefined) {
		if (window.frames['tab11frame'].gridGrid != null) {
			if (window.frames['tab11frame'].gridGrid.store.getModifiedRecords().length > 0)
				return true;
		}
	}
	if (window.frames['tab12frame'] != undefined) {
		if (window.frames['tab12frame'].gridGrid != null) {
			if (window.frames['tab12frame'].gridGrid.store.getModifiedRecords().length > 0)
				return true;
		}

	}
	if (window.frames['tab13frame'] != undefined) {
		if (window.frames['tab13frame'].gridGrid != null) {
			if (window.frames['tab13frame'].gridGrid.store.getModifiedRecords().length > 0)
				return true;
		}
	}
	if (window.frames['tab14frame'] != undefined) {
		if (window.frames['tab14frame'].gridGrid != null) {
			if (window.frames['tab14frame'].gridGrid.store.getModifiedRecords().length > 0)
				return true;
		}
	}
	if (window.frames['tab14frame'] != undefined) {
		if (window.frames['tab14frame'].gridGrid != null) {
			if (window.frames['tab14frame'].gridGrid.store.getModifiedRecords().length > 0)
				return true;
		}
	}
	if (window.frames['tab15frame'] != undefined) {
		if (window.frames['tab15frame'].gridGrid != null) {
			if (window.frames['tab15frame'].gridGrid.store.getModifiedRecords().length > 0)
				return true;
		}
	}
	if (window.frames['tab16frame'] != undefined) {
		if (window.frames['tab16frame'].gridGrid != null) {
			if (window.frames['tab16frame'].gridGrid.store.getModifiedRecords().length > 0)
				return true;
		}
	}
	if (window.frames['tab17frame'] != undefined) {
		if (window.frames['tab17frame'].gridGrid != null) {
			if (window.frames['tab17frame'].gridGrid.store.getModifiedRecords().length > 0)
				return true;
		}
	}
	if (window.frames['tab18frame'] != undefined) {
		if (window.frames['tab18frame'].gridGrid != null) {
			if (window.frames['tab18frame'].gridGrid.store.getModifiedRecords().length > 0)
				return true;
		}
	}
	return false;
}



function is_form_changed() {

	var is_changed = false;

	$("input:text,input:hidden,textarea").each(function(i, obj) {
		
		
		var validateField = $(obj).attr('validateField');
		
		if(validateField){
			
			var _v = $(obj).attr('_value');
			var _vId = $(obj).attr('id');
			if (_vId) {
				var flag = _vId.indexOf("ext-");
				var vreadonly = $(obj).attr('readonly');
				if (typeof (_v) == 'undefined')
					_v = '';
				if (flag == -1 && !vreadonly) {

					if (_v != $(obj).val()) {
						is_changed = true;
					}
				}
			}
			
		}
		

	});
//	$(":checkbox, :radio").each(function(i, obj) {
//		var _v = obj.checked ? 'on' : 'off';
//
//		if (typeof (_v) == 'undefined')
//			_v = '';
//
//		if (_v != $(obj).val()) {
//			is_changed = true;
//		}
//	});

	$("select").each(function(i, obj) {
		
		var validateField = $(obj).attr('validateField');
		
		if(validateField){
			
		
			var _v = $(obj).attr('_value');
			if (typeof (_v) == 'undefined')
				_v = '';
	
			if (_v != obj.options[obj.selectedIndex].value) {
				is_changed = true;
			}
		}
	});
	return is_changed;
}

$(function() {
	$("input:text,input:hidden,textarea").each(function(i, obj) {
	var validateField = $(obj).attr('validateField');
		
		if(validateField){
			
		$(obj).attr('_value', $(obj).val());
		}
	});

	$('select').each(function(i, obj) {
	var validateField = $(obj).attr('validateField');
		
		if(validateField){
			$(obj).attr('_value', obj.options[obj.selectedIndex].value);
		}
	});

});
