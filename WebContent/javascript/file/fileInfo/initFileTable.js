$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "fileName",
		"title": "文件名称",
	});
	colOpts.push({
		"data": "uploadTime",
		"title": "上传时间",
	});
	colOpts.push({
		"data": "uploadUser-name",
		"title": "上传人",
	});
	var tbarOpts = [{
		text: "下载",
		action: function() {
			var rows = $("#showFileInfo").find("tbody .selected");
			console.log(rows)
			var length = rows.length;
			if(length<1) {
				top.layer.msg("请选择一条数据");
				return false;
			}
			var id="";
			var fileId= []; 
			rows.each(function(i,v){
				fileId.push($(v).find(".icheck").val());
				
			})
			for (var i = 0; i < fileId.length; i++) {
			  id=fileId[i];
			  debugger;
			  window.open(ctx+"/operfile/downloadById.action?id="+id);
			}
			
		}
	},
	{
		text: "删除",
		action: function() {
			removeChecked($("#showFileInfo"),
				"/operfile/del.action", "删除附件：", $("#id").val());
		}
	}];
	var modelType =$("#modelType").val();
	var id = $("#id").val();
	var flag = $("#flag").val();
	var options = table(true, null,
			'/operfile/initFileTableJson.action?modelType='+modelType+"&id="+id+"&flag="+flag,colOpts , tbarOpts)
		var fileInfoTable = renderData($("#showFileInfo"), options);
	$("#showFileInfo").on(
	'init.dt',
	function(e, settings) {
		// 清除操作按钮
		//$("#showFileInfo_wrapper .dt-buttons").empty();
		$('#showFileInfo_wrapper').css({
			"padding": "0 16px"
		});
	var trs = $("#showFileInfo tbody tr");
	fileInfoTable.ajax.reload();
	fileInfoTable.on('draw', function() {
		trs = $("#showFileInfo tbody tr");
		trs.click(function() {
			$(this).addClass("chosed").siblings("tr")
				.removeClass("chosed");
		});
	});
	trs.click(function() {
		$(this).addClass("chosed").siblings("tr")
			.removeClass("chosed");
	});
	});

})

