$(function() {
	load("/file/fileInfo/editFileInfo.action", {
		id : $("#id_hid").val()
	}, "#tabs-0");

	new Ext.TabPanel({
		renderTo : 'maintab',
		autoWidth : true,
		activeTab : 0,
		margins : '0 0 0 0',
		items : [ {
			title : '文档信息',
			autoWidth : true

		} ]
	});
	var handlemethod = $("#handlemethod").val();
	if ('view' === handlemethod) {
		setTimeout(function() {
			settextreadonlyByAllJquery();
		}, 500);
	} else if ('modify' === handlemethod) {
		setTimeout(function() {
			settextreadonlyJquery($("#fif_id"));
		}, 500);
	}

	$("#toolbarbutton_list").click(function() {
		window.location = window.ctx + '/file/fileInfo/showFileInfoList.action';
	});
	$("#toolbarbutton_save").click(function() {
		var code = $("#fif_id").val();
		if (!code) {
			message("请填写文档编号！");
			return;
		}
		$("#fileInfo_form").attr("action", ctx+"/file/fileInfo/save.action").submit();
	});
});