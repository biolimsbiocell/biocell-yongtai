
$(function() {
	var cols = {};
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'fileName',
		type : "string"
	});
	fields.push({
		name : 'contentNote',
		type : "string"
	});
	fields.push({
		name : 'ownerModel-name',
		type : "string"
	});
	fields.push({
		name : 'modelContentId',
		type : "string"
	});
	fields.push({
		name : 'studyDirection-name',
		type : "string"
	});
	fields.push({
		name : 'keyWord',
		type : "string"
	});
	fields.push({
		name : 'project',
		type : "string"
	});
	fields.push({
		name : 'projectTask',
		type : "string"
	});
	fields.push({
		name : 'experiment',
		type : "string"
	});
	fields.push({
		name : 'function',
		type : "string"
	});
	fields.push({
		name : 'equipment',
		type : "string"
	});
	fields.push({
		name : 'stateName',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : '序号',
		width : 150,
		sortable : true
	});
	cm.push({
		dataIndex : 'fileName',
		header : '文档名称',
		width : 150,
		sortable : true
	});
	cm.push({
		dataIndex : 'contentNote',
		header : '文件说明',
		width : 150,
		sortable : true
	});
	cm.push({
		dataIndex : 'ownerModel-name',
		header : '所属模块',
		width : 150,
		sortable : true
	});
	cm.push({
		dataIndex : 'modelContentId',
		header : '内容Id',
		width : 150,
		sortable : true
	});
	cm.push({
		dataIndex : 'studyDirection-name',
		header : '研究方向',
		width : 150,
		sortable : true
	});
	cm.push({
		dataIndex : 'keyWord',
		header : '主题词',
		width : 150,
		sortable : true
	});
	cm.push({
		dataIndex : 'project',
		header : '关联项目',
		width : 150,
		sortable : true
	});
	cm.push({
		dataIndex : 'projectTask',
		header : '关联任务',
		width : 150,
		sortable : true
	});
	cm.push({
		dataIndex : 'experiment',
		header : '关联实验',
		width : 150,
		sortable : true
	});
	cm.push({
		dataIndex : 'function',
		header : '关联方法',
		width : 150,
		sortable : true
	});
	cm.push({
		dataIndex : 'equipment',
		header : '关联设备',
		width : 150,
		sortable : true
	});
	cm.push({
		dataIndex : 'stateName',
		header : '状态',
		width : 150,
		sortable : true
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/file/fileInfo/showFileInfoListJson.action?id="+$("#id_hid").val();
	// loadParam.limit = 10
	var opts = {};
	//opts.tbar = [];
	opts.title = "文档管理";
	//opts.height=document.body.clientHeight-30;
	opts.rowselect = function(id) {
		$("#id").val(id);
	};
	opts.rowdblclick = function(id) {
		$("#id").val(id);
		view();
	};
	
	var fileFinfoGridId = gridTable("file_info_grid_div", cols, loadParam, opts);

	var view = function() {
		window.location = window.ctx + '/file/fileInfo/toMainframe.action?reqMethodType=view&id='
				+ $("#id").val();
	};

	$("#toolbarbutton_add").unbind();
	$("#toolbarbutton_add").click(function() {
		window.location = window.ctx + '/file/fileInfo/toMainframe.action?reqMethodType=add';
	});
	$("#toolbarbutton_modify").unbind();
	$("#toolbarbutton_modify").click(function() {
		var id = $("#id").val();
		if (id) {
			window.location = window.ctx + '/file/fileInfo/toMainframe.action?reqMethodType=modify&id=' + id;
		} else {
			message("请选择您要修改的记录!");
		}
	});
	$("#toolbarbutton_scan").unbind();
	$("#toolbarbutton_scan").click(function() {
		if ($("#id").val()) {
			view();
		} else {
			message("请选择您要查看的记录！");
		}

	});
	$("#toolbarbutton_export_excel").unbind();
	$("#toolbarbutton_export_excel").click(function() {
		fileFinfoGridId.title = '导出列表';
		var vExportContent = fileFinfoGridId.getExcelXml();
		var x = document.getElementById('gridhtm');
		x.value = vExportContent;
		document.excelfrm.submit();
	});
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 542;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
				commonSearchAction(fileFinfoGridId);
				$(this).dialog("close");
			},
			"清空" : function() {
				form_reset();
			}
		}, true, option);
	});
});