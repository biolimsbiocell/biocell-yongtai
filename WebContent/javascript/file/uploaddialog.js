﻿	var Application={};
	Application.uploadDialog={
		
		progressBarText:'正在上传：{0}，{1}%完成',
		
		statuBarText:'文件总数：{0}个  ，大小：{1}',
		
		show:function(data,swfu){
			
			this.swfu = swfu;
			if(!this.dialog)
				this.initDialog();
			
			//this.uploadGrid.store.removeAll();
			if(data)
	 			this.classStore.loadData(data);
			//this.uploadAction[0].enable();
			this.uploadAction[1].disable();
			this.uploadAction[2].disable();
			this.uploadAction[3].disable();
			this.uploadAction[4].enable();
			//this.uploadProgressBar.updateProgress(0,'');

			this.dialog.show();

		},

		hide:function(){
			this.dialog.hide();
		},

		classStore:new Ext.data.SimpleStore({fields: ["id", "text"],data:[]}),
		
		uploadAction:[
			new Ext.Action({text:'增加',
				handler:function(){
					Application.uploadDialog.swfu.selectFiles();
				}
			}),
			new Ext.Action({text:'删除',disabled:true,handler:function(){
				var obj=Application.uploadDialog;
				var grid=obj.uploadGrid;
				var store=grid.store;
				var selection=grid.getSelectionModel().getSelections();
				if(selection.length<=0){
					Ext.Msg.alert("请注意","请选择您要删除的文件！");
				}
				for(var i=0;i<selection.length;i++){
					var rec=store.getAt(store.indexOfId(selection[i].id));
					obj.swfu.cancelUpload(rec.data.id);
					store.remove(rec);
				}
			//	obj.stateInfo.getEl().innerHTML=String.format(obj.statuBarText,obj.uploadGrid.store.getCount(),Ext.util.Format.fileSize(obj.uploadGrid.store.sum('size')));
				if(obj.uploadGrid.store.getCount()==0){
					obj.uploadGrid.store.removeAll();
					obj.uploadAction[1].disable();
					obj.uploadAction[2].disable();
					obj.uploadAction[3].disable();
				}
			}}),
			new Ext.Action({text:'清空',disabled:true,handler:function(){
				var obj=Application.uploadDialog;
				var store=obj.uploadGrid.store;
				var len=store.getCount();
				for(var i=0;i<len;i++){
					var rec=store.getAt(i);
					obj.swfu.cancelUpload(rec.data.id);
				}
				store.removeAll();
				obj.classCombo.clearValue();
				//obj.stateInfo.getEl().innerHTML=String.format(obj.statuBarText,0,Ext.util.Format.fileSize(0));
				obj.uploadProgressBar.updateProgress(0,'');
				obj.uploadProgressBar.updateText("");
				//obj.uploadAction[0].enable();
				obj.uploadAction[1].disable();
				obj.uploadAction[2].disable();
				obj.uploadAction[3].disable();
			}}),
			new Ext.Action({text:'上传',disabled:true,handler:function(){
				var obj=Application.uploadDialog;
				//obj.uploadAction[0].disable();
				var store=obj.uploadGrid.store;
//				var flag=true;
				var name="";
				
				
				for(var j=0;j<store.getCount();j++){
					var names = store.getAt(j).get("file");
					var nameStr=names.substring(0, 6);
//					if(nameStr=="Sample"){
//						falg=true;
//					}else{
//						flag=false;
//					}
					name += names+",";
				}
				
				
				
//				if(flag){
					var scode = new Array();
					scode = name.split(",");
					for(var j=0; j<scode.length; j++){
						var str=scode[j];
						if(str!=""){
							console.log();
							if(document.getElementById("modelType").value=="sampleInput"){
								saveImg(str,scode.length);	
							}
							//ajax后台查询file信息加载到前台
							
						}
					}
//				}
				
				function saveImg(str,codes){
					setTimeout(function(){
						ajax("post", "/sample/sampleInput/saveInput.action", {
							code : str
						}, function(data) {
							if (data.success) {
								message("上传成功!！");
							}else{
								message("上传失败！");
								return;
							}
						});
				    }, 1500*codes);
				} 
				obj.uploadAction[2].disable();
				obj.uploadAction[3].disable();
				obj.uploadAction[4].disable();
				//var store=obj.uploadGrid.store;
				//var len=store.getCount();
				//var classid=obj.classCombo.getValue();
				//obj.swfu.setPostParams({'classid':classid});
				obj.swfu.startUpload();
			}}),
			new Ext.Action({text:biolims.common.close,handler:function(){
				var obj=Application.uploadDialog;
				var store=obj.uploadGrid.store;
				var len=store.getCount();
				for(var i=0;i<len;i++){
					var rec=store.getAt(i);
					obj.swfu.cancelUpload(rec.data.id);
				}
				store.removeAll();
				obj.classCombo.clearValue();
				obj.uploadProgressBar.updateProgress(0,'');
				obj.uploadProgressBar.updateText("");
				//obj.uploadAction[0].enable();
				obj.uploadAction[1].disable();
				obj.uploadAction[2].disable();
				obj.uploadAction[3].disable();

				Application.uploadDialog.hide();
				//Ext.getCmp("gridGrid").store.load();
				//parent.operForm.window.location.href=ctx+'/files/fileOper_initFileList.action';//重新刷新列表
				window.location.href=window.ctx+'/operfile/initFileList.action?modelType='+document.getElementById("modelType").value+'&id='+document.getElementById("picUserId").value+'&flag='+document.getElementById("flag").value;
			}}),
		],

		initDialog:function(){
			this.classCombo=new Ext.form.ComboBox({
				hiddenName:'classid',name: 'classid_name',valueField:"id",displayField:"text",mode:'local',
				store:this.classStore,blankText:'请选择类别',emptyText:'请选择类别',editable:true,anchor:'90%'
			})
			
			
			this.dialog=new Ext.Window({

	     // layout:'fit',width:800,height:400,title:'上传文件',closeAction:'hide',border:false,modal:true,

	      layout:'fit',width:800,height:400,title:'上传文件',closeAction:'hide',border:false,modal:true,

	      plain:true,closable:false,resizable:false,
				bbar:[this.uploadProgressBar=new Ext.ProgressBar({width:786})],
				
	      items:[
	      	
	      	Application.uploadDialog.uploadGrid=new Ext.grid.GridPanel({
		      	autoExpandColumn:2,enableHdMenu:false,
						tbar:[Application.uploadDialog.uploadAction[1],'-',Application.uploadDialog.uploadAction[2],
						'-',Application.uploadDialog.uploadAction[3],"->"
						,Application.uploadDialog.uploadAction[4]],
						//bbar:[Application.uploadDialog.stateInfo=new Ext.Toolbar.TextItem(String.format(Application.uploadDialog.statuBarText,0,Ext.util.Format.fileSize(0)))],
			      store: new Ext.data.SimpleStore({fields: ["id","state", "file","size","type"],data:[]}),
			      columns:[
			       	new Ext.grid.RowNumberer(),
			        {id:'id',header: "id",hidden:true,width:150,dataIndex:'id',resizable:false,sortable:false},
			        {header: "文件名",width:Ext.grid.GridView.autoFill,dataIndex:'file',sortable:true},
			        {header: "大小", width: 80,renderer:Ext.util.Format.fileSize,dataIndex:'size',sortable:true,align:'right'},
			        {header: "类型", width: 80,dataIndex:'type',align:'center',sortable:true},
			        {header: "状态", width: 100,dataIndex:'state',align:'center',sortable:true}
			      ]
					})
				]
			})
		},

		fileQueued:function(file){
			var obj=Application.uploadDialog;
			var filetype=(file.type.substr(1)).toUpperCase();
			//if(filetype=='JPG' | filetype=='GIF'){
				var data=[];
				data.push([file.id,'未上传',file.name,file.size,filetype]);
				obj.uploadGrid.store.loadData(data,true);
				obj.uploadAction[1].enable();
				obj.uploadAction[2].enable();
				obj.uploadAction[3].enable();
				//obj.stateInfo.getEl().innerHTML=String.format(obj.statuBarText,obj.uploadGrid.store.getCount(),Ext.util.Format.fileSize(obj.uploadGrid.store.sum('size')));
			//}
		},
			
		uploadFileStar:function(file){
			var obj=Application.uploadDialog;
			var index=obj.findData(file.id);
			if(index>=0){
				obj.uploadGrid.store.getAt(index).set('state','正在上传……');
			}
			obj.uploadProgressBar.updateProgress(0,String.format(obj.progressBarText,file.name,0));
			return true;
		},
	
			
		uploadProgress:function(file,bytesloaded){
			var obj=Application.uploadDialog
			var percent = Math.ceil((bytesloaded / file.size) * 100);
			obj.uploadProgressBar.updateProgress(percent/100,String.format(obj.progressBarText,file.name,percent));
		},
			
		uploadFileComplete:function(file){
			//console.log("uploadFileComplete");
			//console.log(arguments);
			var obj=Application.uploadDialog;
			var index=obj.findData(file.id);
			if(index>=0){
				obj.uploadGrid.store.getAt(index).set('state','已上传');
			}
			if(obj.swfu.getStats().files_queued>0)
				obj.swfu.startUpload();
		},
			
		uploadFileCancelled:function(file, queuelength){
			
		},
			
		uploadQueueComplete:function(file,server_data){
			//console.log("success");
			//console.log(arguments);
			var obj=Application.uploadDialog;
			var index=obj.findData(file.id);
			if(server_data=='successed'){

				//改变状态列的状态
				if(index>=0){
					obj.uploadGrid.store.getAt(index).set('state','已上传');
				}
				
				//改变进度条的状态
				obj.uploadProgressBar.updateProgress(1,'完成上传');			
				obj.uploadAction[2].enable();
				obj.uploadAction[4].enable();

			}else if(server_data=='error'){
				//console.log(server_data);
				if(index>=0){
					obj.uploadGrid.store.getAt(index).set('state','上传失败');
					obj.uploadAction[4].enable();
				}
			}
			else{
				if(index>=0){
					obj.uploadGrid.store.getAt(index).set('state','上传失败');
					obj.uploadAction[4].enable();
				}
			}
			if(obj.swfu.getStats().files_queued>0){//判断队列内是否还有未上传的文件
					obj.swfu.startUpload();
			}else{
				//如果没有未上传的文件则重新加载文档列表
			}
		},
	
		uploadError:function(file,errcode,msg){
			//console.log("error");
			//console.log(arguments);
			var index=Application.uploadDialog.findData(file.id);
			if(index>=0)
				Application.uploadDialog.uploadGrid.store.getAt(index).set('state','上传失败');
				var obj=Application.uploadDialog;
				obj.uploadAction[1].enable();
				obj.uploadAction[2].enable();
				obj.uploadAction[4].enable();
		},
			
		uploadCancel:function(file, queuelength){
			var index=Application.uploadDialog.findData(file.id);
			if(index>=0)
				Application.uploadDialog.uploadGrid.store.getAt(index).set('state','取消上传');
		},
		
		fileDialogStart:function(){
					
					var obj=Application.uploadDialog;
					
					if(Application.uploadDialog.dialog){
						
				var store=obj.uploadGrid.store;
				var len=store.getCount();
				for(var i=0;i<len;i++){
					var rec=store.getAt(i);
					obj.swfu.cancelUpload(rec.data.id);
				}
				store.removeAll();
				obj.classCombo.clearValue();
				obj.uploadProgressBar.updateProgress(0,'');
				obj.uploadProgressBar.updateText("");
				//obj.uploadAction[0].enable();
				obj.uploadAction[1].disable();
				obj.uploadAction[2].disable();
				obj.uploadAction[3].disable();
					}
		},
		
		fileDialogComplete:function (num_files_queued){
				
		},
		
		findData:function(id){
			var rowindex=Application.uploadDialog.uploadGrid.store.find('id',id);
			return rowindex;
		}

	}//Application.uploadDialog
	
