$(function() {
	$("#file-upload")
			.fileupload(
					{
						forceIframeTransport : true,
						autoUpload : true,
						type : 'POST',
						dataType : "json",
						url : ctx
								+ "/operfile/ajaxUpload.action?modelType=infoImg",
						always : function(e, data) {
							if (data.success != "success") {
								$(
										".cke_dialog_contents table table tr:first input[class=cke_dialog_ui_input_text]")
										.val(
												ctx
														+ "/operfile/showImg.action?fileId="
														+ data.result.fileId)
								win.dialog("close");
								win = null;
							} else {
								// TODO 文件上传失败提醒
							}
						}
					});
});
