var showDicUnitListTables;
$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": "ID",
		"width": "10px",
//		"className": "edit",
		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push({
		"data": "name",
		"title": biolims.common.designation,
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
		}
	});
	colOpts.push({
		"data": "type",
		"title": biolims.common.type,
		"name": biolims.common.quantityOrWeight + "|" + biolims.common.time,
		"className": "select",
		"createdCell": function(td) {
			$(td).attr("saveName", "type");
			$(td).attr("selectOpt", biolims.common.quantityOrWeight + "|" + biolims.common.time);
		},
		"render": function(data, type, full, meta) {
			if(data == "weight") {
				return biolims.common.quantityOrWeight;
			} else if(data == "time") {
				return biolims.common.time;
			} else {
				return "";
			}
		}
	})
	colOpts.push({
		"data": "note",
		"title": biolims.common.explain,
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
		}
	});
	colOpts.push({
		"data": "orderNumber",
		"title": biolims.user.sortingNumber,
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "orderNumber");
		}
	})
	var tbarOpts = [];
	tbarOpts.push({
		text: biolims.common.find,
		action: function() {
			search();
		}
	});
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#showDicUnitDiv"));
		}
	});
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveItem($("#showDicUnitDiv"));
		}
	});
	tbarOpts.push({
		text: biolims.common.delSelected,
		action: function() {
			removeChecked($("#showDicUnitDiv"), "/dic/unit/delDicUnit.action");
		}
	});
	tbarOpts.push({
		text : biolims.common.editwindow,
		action : function() {
			editItemLayer($("#showDicUnitDiv"))
		}
	});
	var showDicUnitListOps = table(true, null, "/dic/unit/showDicUnitListJson.action", colOpts, tbarOpts);
	showDicUnitListTab = renderData($("#showDicUnitDiv"), showDicUnitListOps);
	showDicUnitListTab.on('draw', function() {
		showDicUnitListTables = showDicUnitListTab.ajax.json();
	});
});
//弹框模糊查询参数
function searchOptions() {
	return [{
		"txt": biolims.common.designation,
		"type": "input",
		"searchName": "name",
	}, {
		"type": "table",
		"table": showDicUnitListTab
	}];
}

// 保存
function saveItem() {
	var ele = $("#showDicUnitDiv");
	var trs = ele.find("tbody").children(".editagain");
	console.log(trs.eq(0).children("td[savename=name]").text())
	console.log(trs.eq(1).children("td[savename=name]").text())
	var boo = true;
	trs.each(function(j, k) {
		if(!$(k).children("td[savename=name]").text()) {
			boo = false;
			return false;
		}
	});
	if(!boo) {
		top.layer.msg(biolims.storage.nameIsEmpty);
		return false;
	}
	var changeLog = biolims.common.dicManageMent + "：";
	var data = saveItemjson(ele);
	changeLog = getChangeLog(data, ele, changeLog);
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		type: 'post',
		url: '/dic/unit/saveDicUnit.action',
		data: {
			id: "",
			dataJson: data,
			logInfo: changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})

}
// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			if(k == "type") {
				var type = $(tds[j]).text();
				if(type == biolims.common.quantityOrWeight) {
					json[k] = "weight";
				} else if(type == biolims.common.time) {
					json[k] = "time";
				}
				continue;
			}
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}

function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		if(!id){
			changeLog += '新增记录:';
			for(var k in v) {
					var title = ele.find("th[savename=" + k + "]").text();
					changeLog += '"' + title + '"为"' + v[k] + '";';
				}
			return true;
		}
		changeLog += 'ID为"' + v.id + '":';
		showDicUnitListTables.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}