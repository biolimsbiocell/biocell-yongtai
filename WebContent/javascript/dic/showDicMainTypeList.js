$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	 fields.push({
		name : 'orderNumber',
		type : "string"
	});
	 fields.push({
			name : 'state',
			type : "string"
		});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : '编号',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'name',
		header : '名称',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
   cm.push({
		dataIndex : 'orderNumber',
		header : '排序号',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
   cm.push({
		dataIndex : 'state',
		header : '状态',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/dicMainType/showDicMainTypeListJson.action";
	var opts = {};
	opts.title = "详细信息";
	opts.height = document.body.clientHeight-20;
	opts.tbar = [];
	opts.delSelect = function(ids) {
		ajax("post", "/dicMainType/delDicMainType.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				dicMainTypeGrid.getStore().commitChanges();
				/*dicMainTypeGrid.getStore().reload();*/
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : '保存',
		handler : function() {
			var result = {};
			if (result) {
				ajax("post", "/dicMainType/saveDicMainTypeList.action", {
					itemDataJson : getItemData()
				}, function(data) {
					if (data.success) {
						dicMainTypeGrid.getStore().commitChanges();
						dicMainTypeGrid.getStore().reload();
						message("保存成功！");
					} else {
						message("保存失败！");
					}
				}, null);
			}
		}
	});
	dicMainTypeGrid = gridEditTable("show_dicmaintypelist_grid_id", cols, loadParam, opts);
	var getItemData = function() {
		var itemDataJson = [];
		var modifyRecord = dicMainTypeGrid.getModifyRecord();
		if (modifyRecord && modifyRecord.length > 0) {
			$.each(modifyRecord, function(i, obj) {
				var data = {};
				data.id = obj.get("id");
				data.name=obj.get("name");
				data.orderNumber=obj.get("orderNumber");
				data.state=obj.get("state");
				itemDataJson.push(data);
			});
		}
		return itemDataJson.length > 0 ? JSON.stringify(itemDataJson) : "";
	};
});