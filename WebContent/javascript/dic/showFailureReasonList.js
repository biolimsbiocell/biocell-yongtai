$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});

	cols.fields = fields;
	var cm = [];

	cm.push({
		dataIndex : 'id',
		header : '编码',
		width : 120
	});

	cm.push({
		dataIndex : 'name',
		header : '名称',
		width : 130
	});

	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/dic/failureReason/showFailureReasonListJson.action";
	// loadParam.limit = 10
	var opts = {};
	opts.width = 500;
	opts.height = 300;

	var grid = gridTable("f_r_div", cols, loadParam, opts);
	setTimeout(function() {
		$("#f_r_div").data("frGrid", grid);
	}, 1000);

});