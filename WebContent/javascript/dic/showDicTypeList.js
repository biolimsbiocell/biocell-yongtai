var showDicTypeListTables;
var type_mainId = "";
var type_mainName = "";
$(function() {
//	myEditor = CKEDITOR.replace('checkCounts');
	
	$.ajax({
		type: "post",
		url: ctx + "/dic/type/selectDicMainTypes.action",
		success: function(data) {
			var data = JSON.parse(data);
//				console.log(data);
			if(data.success) {
				for(var key in data.data){
					//console.log("属性：" + key + ",值：" + data.data[key]);
					type_mainId += key;
					type_mainName += data.data[key];
				}
				var selectOptArr = type_mainId.split("|");
				var selectOptVal = type_mainName.split("|");
				$('#type_names').autocomplete({
				    hints: selectOptVal,
				    width: 300,
				    height: 30,
				    onSubmit: function(text){
				    	var a=0;
				    	for(var i=0;i<selectOptVal.length;i++){
				    		if(text==selectOptVal[i]){
				    			a=i;
				    		}
				    	}
				    	var  artime_vals  = selectOptArr[a];
				    	var  artime_texts  = text;
				    	var searchItemLayerValue = {};
				    	searchItemLayerValue["type-id"] = artime_vals;
				    	//重新加载datatable
				    	var query = JSON.stringify(searchItemLayerValue);
				    	var param = {
				    		query: query
				    	};
				    	sessionStorage.setItem("searchContent", query);
				    	
				    	$("#types").val(artime_vals);
				    	
				    	showDicTypeListTab.settings()[0].ajax.data = param;
				    	showDicTypeListTab.ajax.reload();
				}
				});
				var opt = "";
				selectOptArr.forEach(function(v, j) {
					opt += "<option value=" + v + ">" + selectOptVal[j] + "</option>";
				});
				$("#types").append(opt);
			
			}
		}
	});
//	$.ajax({
//		type: "post",
//		url: ctx + "/dic/type/selectDicMainTypes.action",
//		success: function(data) {
//			var data = JSON.parse(data);
////				console.log(data);
//			if(data.success) {
//				for(var key in data.data){
//					console.log("属性：" + key + ",值：" + data.data[key]);
//					type_mainId += key;
//					type_mainName += data.data[key];
//				}
//				var selectOptArr = type_mainId.split("|");
//				var selectOptVal = type_mainName.split("|");
//				var opt = "";
//				selectOptArr.forEach(function(v, j) {
//					opt += "<option value=" + v + ">" + selectOptVal[j] + "</option>";
//				});
//				$("#type_names").append(opt);
//			
//			}
//		}
//	});
	
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": "ID",
		"width":"10px",
//		"className": "edit",
		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push({
		"data": "code",
		"title": biolims.common.functionCode,
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "code");
		}
	});
	
//	colOpts.push({
//		"data": "dicPhone",
//		"title": "科室电话",
//		"className": "edit",
//		"createdCell": function(td) {
//			$(td).attr("saveName", "dicPhone");
////		"visibel":false;
//		}
//	});
	colOpts.push({
		"data": "name",
		"title": biolims.tStorage.name,
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
		}
	})
	colOpts.push({
		"data": "note",
		"title": biolims.common.explain,
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
		}
	});
	colOpts.push({
		"data": "sysCode",
		"title": biolims.common.systemFlag,
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "sysCode");
		}
	})
	colOpts.push({
		"data": "type-id",
		"title": biolims.common.typeID,
//		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "type-id");
		}
	})
	colOpts.push({
		"data": "type-name",
		"title": biolims.common.type,
		"createdCell": function(td) {
			$(td).attr("saveName", "type-name");
		}
	})
	
	colOpts.push({
		"data": "orderNumber",
		"title": biolims.user.sortingNumber,
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "orderNumber");
		}
	})
	colOpts.push({
		"data": "state",
		"title": biolims.common.state,
		"name":biolims.master.valid+"|"+biolims.master.invalid,
		"className":"select",
		"createdCell": function(td) {
			$(td).attr("saveName", "state");
			$(td).attr("selectOpt", biolims.master.valid+"|"+biolims.master.invalid);
		},
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.master.valid;
			}
			else if(data == "0") {
				return biolims.master.invalid;
			}else {
				return "";
			}
		}
	})
	var tbarOpts = [];
	tbarOpts.push({
		text: biolims.common.find,
		action: function() {
			search();
		}
	});
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			var ele=$("#showDicTypeDiv");
			var rows = ele.find(".selected");
			var length = rows.length;
			
			if(length > 1) {
				top.layer.msg(biolims.common.pleaseSelectAPieceOfData);
				return;
			}else{
				if(length == 1){
					var type_ids = $(rows[0]).find("td[savename='type-id']").text();
					var type_names = $(rows[0]).find("td[savename='type-name']").text();
						console.log(type_ids);
						//禁用固定列
						$("#fixdeLeft2").hide();
						//禁用列显示隐藏
						$(".colvis").hide();
						//添加明细
						var ths = ele.find("th"); //获取表头
						var tr = $("<tr><td><input type='checkbox' class='icheck' value=''/></td></tr>"); //添加一行
						tr.height(32);
						for(var i = 1; i < ths.length; i++) {
							var edit = $(ths[i]).attr("key");
							var saveName = $(ths[i]).attr("saveName");
							if(saveName=="type-id"){
								tr.append("<td class=" + edit + " saveName=" + saveName + ">"+type_ids+"</td>");
							}else if(saveName=="type-name"){
								tr.append("<td class=" + edit + " saveName=" + saveName + ">"+type_names+"</td>");
							}else if(saveName=="state"){
								tr.append("<td class=" + edit + " saveName=" + saveName + ">"+biolims.master.valid+"</td>");
							}else{
								tr.append("<td class=" + edit + " saveName=" + saveName + "></td>");
							}
						}
						ele.find("tbody").prepend(tr);
				}else{
					var obj = $("#types option:selected");
					var  artime_val  = obj.val();
					var  artime_text  = obj.text();
					if(artime_val !=""){
						//禁用固定列
						$("#fixdeLeft2").hide();
						//禁用列显示隐藏
						$(".colvis").hide();
						//添加明细
						var thss = ele.find("th"); //获取表头
						var trs = $("<tr><td><input type='checkbox' class='icheck' value=''/></td></tr>"); //添加一行
						trs.height(32);
						for(var k = 1; k < thss.length; k++) {
							var edits = $(thss[k]).attr("key");
							var saveNames = $(thss[k]).attr("saveName");
							if(saveNames=="type-id"){
								trs.append("<td class=" + edits + " saveName=" + saveNames + ">"+artime_val+"</td>");
							}else if(saveNames=="type-name"){
								trs.append("<td class=" + edits + " saveName=" + saveNames + ">"+artime_text+"</td>");
							}else if(saveNames=="state"){
								trs.append("<td class=" + edits + " saveName=" + saveNames + ">"+biolims.master.valid+"</td>");
							}else{
								trs.append("<td class=" + edits + " saveName=" + saveNames + "></td>");
							}
						}
						ele.find("tbody").prepend(trs);
					}else{
						top.layer.msg(biolims.user.dicMinType);
					}
				}
			}
//			copyRow($("#showDicTypeDiv"))
		}
	});
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveItem($("#showDicTypeDiv"));
		}
	});
	tbarOpts.push({
		text: biolims.common.delSelected,
		action: function() {
			removeChecked($("#showDicTypeDiv"),"/dic/type/delDicType.action");
		}
	});
	tbarOpts.push({
		text : biolims.common.editwindow,
		action : function() {
			editItemLayer($("#showDicTypeDiv"))
		}
	});
	var showDicTypeListOps= table(true, null, "/dic/type/showDicTypeListJson.action", colOpts, tbarOpts);
	showDicTypeListTab = renderData($("#showDicTypeDiv"), showDicTypeListOps);
	showDicTypeListTab.on('draw', function() {
		showDicTypeListTables = showDicTypeListTab.ajax.json();
	});
});


$("#types").bind("change",function(){
	var objs = $("#types option:selected");
	var  artime_vals  = objs.val();
	var  artime_texts  = objs.text();
	var searchItemLayerValue = {};
	searchItemLayerValue["type-id"] = artime_vals;
	//重新加载datatable
	var query = JSON.stringify(searchItemLayerValue);
	var param = {
		query: query
	};
	sessionStorage.setItem("searchContent", query);
	showDicTypeListTab.settings()[0].ajax.data = param;
	showDicTypeListTab.ajax.reload();
});

//弹框模糊查询参数
function searchOptions() {
	return [
		{
			 "type":"input",
				"searchName":"code",
				"txt":"功能码",
		},
		{
			 "type":"input",
				"searchName":"name",
				"txt":"名称",
		},
//		{
//			"txt": biolims.common.type,
//			"type": "select",
//			"options": type_mainName,
//			"changeOpt": type_mainId,
//			"searchName": "type-id"
//		},
		{
			 "type":"input",
				"searchName":"type-name",
				"txt":biolims.common.type,
		},
		{
		"type":"table",
		"table":showDicTypeListTab
	}];
}



// 保存
function saveItem() {
	var ele=$("#showDicTypeDiv");
	var changeLog = biolims.common.dicManageMent+"：";
	var data = saveItemjson(ele);
	changeLog = getChangeLog(data, ele, changeLog);
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		type: 'post',
		url: '/dic/type/saveDicType.action',
		data: {
            id: "",
			dataJson: data,
			logInfo: changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断是否有效并转换为数字
			if(k == "state") {
				var state = $(tds[j]).text();
				if(state == biolims.master.invalid) {
					json[k] = "0";
				} else if(state == biolims.master.valid) {
					json[k] = "1";
				}
				continue;
			}
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		if(!id){
			changeLog += '新增记录:';
			for(var k in v) {
					var title = ele.find("th[savename=" + k + "]").text();
					changeLog += '"' + title + '"为"' + v[k] + '";';
				}
			return true;
		}
		changeLog += 'ID为"' + v.id + '":';
		showDicTypeListTables.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}


