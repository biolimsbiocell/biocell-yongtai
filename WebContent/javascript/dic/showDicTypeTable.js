$(function() {
	Array.prototype.remove = function(val) {
		var index = this.indexOf(val);
		if(index > -1) {
			this.splice(index, 1);
		}
	};
	var searchModel = '<div class="input-group"><input type="text" class="form-control" placeholder="Search..." id="searchvalue" onkeydown="entersearch()"><span class="input-group-btn"> <button class="btn btn-info" type="button" onclick="searchModel()">按名称搜索</button></span> </div>';
	if($("#flag").val()=="chose_id"){
		$("body").prepend("<input class='hidden' id='sysCode'/>");
	}
	if($("#flag").val()=="bzcx"){
		$("body").prepend("<input class='hidden' id='sysCode'/>");
	}
	var colOpts = [];
	if($("#flag").val()!="chose_id"&&$("#flag").val()!="bzcx"){
		colOpts.push({
			"data" : "id",
			"title" : biolims.common.id,
		});
	}
	
	colOpts.push({
		"data" : "name",
		"title" : biolims.user.eduName1,
	});
	
	if($("#flag").val()!="ysfs"){
	colOpts.push({
		"data" : "sysCode",
		"title" : biolims.master.mark,
	});
	colOpts.push({
		"data" : "code",
		"title" : biolims.master.mark,
	});
	}
//	colOpts.push({
//		"data" : "dicPhone",
//		"title" : "科室电话",
//	});
	var tbarOpts = [];
	var checkbox=false;
	if($("#flag").val()=="chose_id"){
		checkbox=true;
	}
	if($("#flag").val()=="bzcx"){
		checkbox=true;
	}
	var addDicTypeOptions = table(checkbox, null,
			'/dic/type/dicTypeSelectTableJson.action?flag=' + $("#flag").val(),
			colOpts, tbarOpts)
	DicTypeTable = renderData($("#addDicTypeTable"), addDicTypeOptions);
	var id;
	if($("#flag").val()=="chose_id"){
		
		if($("#maincontentframe", parent.document).contents().find("#productHideFields").text()==null
				||$("#maincontentframe", parent.document).contents().find("#productHideFields").text()==""){
			id=[];
		}else{
			id = $("#maincontentframe", parent.document).contents().find("#productHideFields").val().split(",");
		}
	}
if($("#flag").val()=="bzcx"){
		
		if($("#maincontentframe", parent.document).contents().find("#productHideFields").text()==null
				||$("#maincontentframe", parent.document).contents().find("#productHideFields").text()==""){
			id=[];
		}else{
			id = $("#maincontentframe", parent.document).contents().find("#productHideFields").val().split(",");
		}
	}

	$("#addDicTypeTable").on(
			'init.dt',
			function(e, settings) {
				// 清除操作按钮
				$("#addDicTypeTable_wrapper .dt-buttons").html(searchModel);
				//$("#addDicTypeTable_wrapper .dt-buttons").empty();
				$('#addDicTypeTable_wrapper').css({
					"padding": "0 16px"
				});
			var trs = $("#addDicTypeTable tbody tr");
			DicTypeTable.ajax.reload();
			if($("#flag").val()=="chose_id"){
				returnChecked(id)
			}else{
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			}
			if($("#flag").val()=="bzcx"){
				returnCheckedName(id)
			}else{
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			}
			DicTypeTable.on('draw', function() {
				if($("#flag").val()=="chose_id"){
					returnChecked(id)
				}else{
					trs = $("#addDicTypeTable tbody tr");
					trs.click(function() {
						$(this).addClass("chosed").siblings("tr")
							.removeClass("chosed");
					});	
				}
				
				if($("#flag").val()=="bzcx"){
					returnChecked(id)
				}else{
					trs = $("#addDicTypeTable tbody tr");
					trs.click(function() {
						$(this).addClass("chosed").siblings("tr")
							.removeClass("chosed");
					});	
				}
				
				
			});
			
			});
})

//回选放方法
function returnChecked(id) {
	console.log(id)
	var trs = $("#addDicTypeTable tbody tr");
	id.forEach(function(vv, ii) {
		trs.each(function(i, v) {
			var samid = $(v).children("td").eq(3).text();
			if(vv == samid) {
				$(v).find('.icheck').iCheck('check');
			}
		})
	});
	
	$("#addDicTypeTable tbody .icheck").on('ifChanged', function(e) {
		if($(this).is(':checked')) {
			id.push($(this).parents("tr").children("td").eq(3).text());
			for(var i =0;i<id.length;i++){
				if(id[i]==""){
					id.remove(id[i]);
					break;
				}
			}
			$("#sysCode").val(id);
		} else {
			console.log($(this).parents("tr").children("td").eq(3).text())
			console.log(id)
			id.remove($(this).parents("tr").children("td").eq(3).text());
			for(var i =0;i<id.length;i++){
				if(id[i]==""){
					id.remove(id[i]);
					break;
				}
			}
			console.log(id)
			$("#sysCode").val(id);
		}
	})
}
//回选放方法
function returnCheckedName(id) {
	console.log(id)
	var trs = $("#addDicTypeTable tbody tr");
	id.forEach(function(vv, ii) {
		trs.each(function(i, v) {
			var samid = $(v).children("td").eq(1).text();
			if(vv == samid) {
				$(v).find('.icheck').iCheck('check');
			}
		})
	});
	
	$("#addDicTypeTable tbody .icheck").on('ifChanged', function(e) {
		if($(this).is(':checked')) {
			id.push($(this).parents("tr").children("td").eq(1).text());
			for(var i =0;i<id.length;i++){
				if(id[i]==""){
					id.remove(id[i]);
					break;
				}
			}
			$("#sysCode").val(id);
		} else {
			console.log($(this).parents("tr").children("td").eq(1).text())
			console.log(id)
			id.remove($(this).parents("tr").children("td").eq(1).text());
			for(var i =0;i<id.length;i++){
				if(id[i]==""){
					id.remove(id[i]);
					break;
				}
			}
			console.log(id)
			$("#sysCode").val(id);
		}
	})
}

//回车搜索
function entersearch(){  
    var event = window.event || arguments.callee.caller.arguments[0];  
    if (event.keyCode == 13)  
    {  
        searchModel();
    }  
} 
//搜索方法
function searchModel() {
	DicTypeTable.settings()[0].ajax.data = {
		query: JSON.stringify({
			name:"%"+$.trim($("#searchvalue").val())+"%"
		})
	};
	DicTypeTable.ajax.reload();
}
