$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
	});
	colOpts.push({
		"data": "name",
		"title": biolims.user.eduName1,
	});
	colOpts.push({
		"data": "sysCode",
		"title": biolims.master.mark,
	});
	colOpts.push({
		"data" : "code",
		"title" : biolims.master.mark,
	});
	var tbarOpts = [];
	var addDicTypeOptions = table(true, null,
		'/dic/type/dicTypeSelectTablesJson.action?flag=' + $("#flag").val(),
		colOpts, tbarOpts)
	var DicTypeTable = renderData($("#addDicTypeTable"), addDicTypeOptions);
	$("#addDicTypeTable").on(
		'init.dt',
		function(e, settings) {
			// 清除操作按钮
			$("#addDicTypeTable_wrapper .dt-buttons").empty();
			$('#addDicTypeTable_wrapper').css({
				"padding": "0 16px"
			});
			var trs = $("#addDicTypeTable tbody tr");
			DicTypeTable.ajax.reload();
		});
})