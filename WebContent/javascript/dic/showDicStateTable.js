$(function() {
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : biolims.common.id,
	});
	colOpts.push({
		"data" : "name",
		"title" : biolims.common.name,
	});
	colOpts.push({
		"data" : "note",
		"title" : biolims.tStorage.note,
	});
	var tbarOpts = [];
	var addDicStateOptions = table(false, null,
			'/dic/state/dicStateSelectTableJson.action?flag=' + $("#flag").val(),
			colOpts, tbarOpts)
	var DicStateTable = renderData($("#addDicStateTable"), addDicStateOptions);
	$("#addDicStateTable").on(
			'init.dt',
			function(e, settings) {
				// 清除操作按钮
				$("#addDicStateTable_wrapper .dt-buttons").empty();
				$('#addDicStateTable_wrapper').css({
					"padding": "0 16px"
				});
			var trs = $("#addDicStateTable tbody tr");
			DicStateTable.ajax.reload();
			DicStateTable.on('draw', function() {
				trs = $("#addDicStateTable tbody tr");
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			});
			trs.click(function() {
				$(this).addClass("chosed").siblings("tr")
					.removeClass("chosed");
			});
			});
})

