$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"visible":false,
		"title": biolims.common.id,
	});
	colOpts.push({
		"data": "name",
		"title": biolims.user.eduName1,
		"createdCell": function(td, data, rowData) {
			$(td).attr("id", rowData['id']);
		}
	});
	colOpts.push({
		"data": "note",
		"title": biolims.tInstrumentState.note,
	});
	var tbarOpts = [];
	var options = table(false, null,
			'/dic/unit/showUnitSelectTableJson.action',colOpts , tbarOpts)
		var addUnitTable = renderData($("#addUnitTable"), options);
	$("#addUnitTable").on(
	'init.dt',
	function(e, settings) {
		// 清除操作按钮
		$("#addUnitTable_wrapper .dt-buttons").empty();
		$('#addUnitTable_wrapper').css({
			"padding": "0 16px"
		});
	var trs = $("#addUnitTable tbody tr");
	addUnitTable.ajax.reload();
	addUnitTable.on('draw', function() {
		trs = $("#addUnitTable tbody tr");
		trs.click(function() {
			$(this).addClass("chosed").siblings("tr")
				.removeClass("chosed");
		});
	});
	trs.click(function() {
		$(this).addClass("chosed").siblings("tr")
			.removeClass("chosed");
	});
	});
})
