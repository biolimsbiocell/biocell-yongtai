var typeName = new Ext.form.TextField({
	allowBlank : true
});
var note = new Ext.form.TextField({
	allowBlank : true,
	maxValue : 100
});

var store = new Ext.data.ArrayStore({
	fields : [ 'id', 'name' ],

	data : [ [ '0', biolims.common.no ], [ '1', biolims.common.yes ] ]
});

var cob = new Ext.form.ComboBox({
	store : store,
	displayField : 'name',
	valueField : 'id',
	mode : 'local'
});
var store1 = new Ext.data.ArrayStore({
	fields : [ 'id', 'name' ],

	data : [ [ '0', biolims.common.no ], [ '1', biolims.common.yes ] ]
});

var cob1 = new Ext.form.ComboBox({
	store : store1,
	displayField : 'name',
	valueField : 'id',
	mode : 'local'
});


var store2 = new Ext.data.ArrayStore({
	fields : [ 'id', 'name' ],

	data : [ [ '0', biolims.common.no ], [ '1', biolims.common.yes ] ]
});

var cob2 = new Ext.form.ComboBox({
	store : store2,
	displayField : 'name',
	valueField : 'id',
	mode : 'local'
});


function validateFun(record) {
	var name = record.get('name');
	if (name == null || name == 0) {
		alertMessage(biolims.storage.nameIsEmpty);
		return false;
	}

	return true;
}

var department = new Ext.form.TextField({
	allowBlank : true,
	maxLength : 32
});
department.on('focus', function() {
	showDepartment();
});