var typeName = new Ext.form.TextField({
	allowBlank : true
});
var note = new Ext.form.TextField({
	allowBlank : true,
	maxValue : 100
});

function validateFun(record) {
	var name = record.get('name');
	if (name == null || name == 0) {
		alertMessage(biolims.storage.nameIsEmpty);
		return false;
	}
	
	return true;
}
