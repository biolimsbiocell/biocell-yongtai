function editCopy() {
	editCommonCopy('contract_id');
}
function saveCopy() {
	saveCommonCopy('contract_id');

}
function list() {
	window.location = window.ctx + '/contract/collectionContract/showContractList.action';
}
function add() {
	window.location = window.ctx + "/contract/collectionContract/toEditContract.action";
}

function edit() {
	var id = "";
	id = document.getElementById("id").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/contract/collectionContract/toEditContract.action?id=' + id;
}

function newSave() {
	if (checkSubmit() == false) {
		return false;
	}
	var myMask = new Ext.LoadMask(Ext.getBody(), {
		msg : "请等待..."
	});
	myMask.show();
	Ext.Ajax.request({
		url : window.ctx + '/common/hasId.action',
		method : 'POST',
		params : {
			id : $("#contract_id").val(),
			obj : 'Contract'
		},
		success : function(response) {
			var respText = Ext.util.JSON.decode(response.responseText);
			myMask.hide();
			if (respText.message == '') {
				save();
			} else {
				message(respText.message);

			}

		},
		failure : function(response) {
		}
	});

}
function save() {

	if (checkSubmit() == true) {
		Ext.MessageBox.show({
			msg : '正在保存数据,请等待...',
			progressText : '保存中...',
			width : 300,
			wait : true,
			icon : 'ext-mb-download'
		});

//		document.getElementById('jsonDataStr').value = commonGetModifyRecords(gridGrid);
		document.getElementById('jsonDataStr1').value = commonGetModifyRecords(payGrid);
//        alert(document.getElementById('jsonDataStr1').value);
		var crmContractItemDivData = $("#crmContractItemdiv").data(
		"crmContractItemGrid");
document.getElementById('jsonDataStr').value = commonGetModifyRecords(crmContractItemDivData);
		form1.action = window.ctx + "/contract/collectionContract/save.action";
		form1.submit();
		// Ext.MessageBox.hide();
		// Ext.MessageBox.alert('Status', 'Changes saved successfully.', '');
	} else {
		return false;
	}
}
function checkSubmit() {
	var mess = "";
	var fs = [ "contract_id" ];
	var nsc = [ "编号不能为空！" ];
    
	mess = commonFieldsNotNullVerifyById(fs, nsc);
	if (mess != "") {
		message(mess);
		return false;
	}
	return true;

}

Ext.onReady(function() {
	new Ext.form.DateField({

		applyTo : 'contract_startDate'
	});
	new Ext.form.DateField({

		applyTo : 'contract_endDate'
	});
	new Ext.form.DateField({

		applyTo : 'contract_signDate'
	});
	// stateTextField('contract_state',document.getElementById('contract_state').value);
});
function sl() {
	var record = gridGrid.getSelectionModel().getSelected();

	var fee = record.get('price') * record.get('num');
	record.set('fee', fee);
}
function jg() {
	var record = gridGrid.getSelectionModel().getSelected();

	var rebatePrice = record.get('exRebate') * record.get('price');
	record.set('rebatePrice', rebatePrice);
}

function esl() {
	var record = gridGrid.getSelectionModel().getSelected();

	var excFee = record.get('exNum') * record.get('rebatePrice');
	record.set('excFee', excFee);
}

var objId = new Ext.form.TextField({
	allowBlank : true
});
var num = new Ext.form.NumberField({
	allowBlank : true,
	allowNegative : false
});

var note = new Ext.form.TextArea({

	maxLength : 100
});
var payDate = new Ext.form.DateField({});
var scale = new Ext.form.NumberField({
	allowBlank : true,
	allowNegative : false
});
var payCondition = new Ext.form.TextArea();
var handleDate = new Ext.form.DateField();
var invoiceNum = new Ext.form.TextField({
	allowBlank : true,
	maxLength : 50
});

var price = new Ext.form.NumberField({
	allowBlank : true,
	allowNegative : false
});
var fee = new Ext.form.NumberField({
	allowBlank : true,
	allowNegative : false
});
var cfee = new Ext.form.NumberField({
	allowBlank : true,
	allowNegative : false
});
var exRebate = new Ext.form.NumberField({
	allowBlank : true,
	allowNegative : false
});
var rebatePrice = new Ext.form.NumberField({
	allowBlank : true,
	allowNegative : false
});
var exNum = new Ext.form.NumberField({
	allowBlank : true,
	allowNegative : false
});
var excFee = new Ext.form.NumberField({
	allowBlank : true,
	allowNegative : false
});
objId.on('focus', function() {
	mailStorageAll();
});
fee.on('focus', function() {
	sl();
});
rebatePrice.on('focus', function() {
	jg();
});
excFee.on('focus', function() {
	esl();
});

function viewCustomer() {
	if (trim(document.getElementById('contract_supplier_id').value) == '') {
		message("请选择一条记录!");
	} else {
		openDialog(window.ctx + '/customer/toViewCustomer.action?id='
				+ document.getElementById('contract_supplier_id').value);

	}
}
function viewContract() {
	if (trim(document.getElementById('contract.mainContract.id').value) == '') {
		message("请选择一条记录!");
	} else {
		openDialog(window.ctx + '/contract/discollectionContract/toViewContract.action?id='
				+ document.getElementById('contract.mainContract.id').value);

	}
}
function workflowSubmit() {
	commonWorkflowSubmit("applicationTypeTableId=collectionContract&formId=" + $("#contract_id").val() + "&title="
			+ encodeURI(encodeURI($("#contract_note").val())));
}
function workflowLook() {
	commonWorkflowLook("applicationTypeTableId=collectionContract&formId=" + $("#contract_id").val());
}
function workflowView() {
	commonWorkflowView("applicationTypeTableId=collectionContract&formId=" + $("#contract_id").val());
}
function changeState() {
	commonChangeState("formId=" + $("#contract_id").val() + "&tableId=collectionContract");
}
function editCopy() {
	window.location = window.ctx + '/contract/collectionContract/toCopyContract.action?id=' + $("#contract_id").val();
}
Ext.onReady(function() {
	var tabs = new Ext.TabPanel({
		renderTo : 'maintab',
		height : document.body.clientHeight-30,
		autoWidth : true,
		activeTab : 0,
		margins : '0 0 0 0',
		items : [ {
			title : '合同信息',
			contentEl : 'markup'
		} ]
	});
});
function setGridId() {
	var gridCount = gridGrid.store.getCount();
	for ( var ij = 0; ij < gridCount; ij++) {
		var record = gridGrid.store.getAt(ij);
		record.set("id", null);
	}
	var payCount = payGrid.store.getCount();
	for ( var ij = 0; ij < payCount; ij++) {
		var record = payGrid.store.getAt(ij);
		record.set("id", null);
	}
}
$(function() {
load("/contract/collectionContract/showContractItemList.action", {
	id : $("#contract_id").val()
}, "#crmContractItempage");
//	var handlemethod = $("#handlemethod").val();
//	if (handlemethod == "modify") {
//		settextreadonly("contract_id");
//	}
//	if (handlemethod == "view") {
//		settextreadonlyByAll();
//	}
//	var copyBl = $("#copyBl").val();
//	if (copyBl) {
//		setTimeout("setGridId()", 1000);
//	}
});