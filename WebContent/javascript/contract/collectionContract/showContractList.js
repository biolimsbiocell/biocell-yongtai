function add() {
	window.location = window.ctx + "/contract/collectionContract/toEditContract.action";
}

function edit() {
	var id = "";
	id = document.getElementById("id").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/contract/collectionContract/toEditContract.action?id=' + id;
}

function view() {
	var id = "";
	id = document.getElementById("id").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/contract/collectionContract/toViewContract.action?id=' + id;
}

function normalsearch(gridName) {
	var limit = parseInt((parent.document.body.clientHeight - 250) > 0 ? (parent.document.body.clientHeight - 250) / 25
			: 1);
	if (trim(document.getElementById("limitNum").value) != '') {
		limit = document.getElementById("limitNum").value;

	}

	var fields = [ 'id', 'note' ];
	var fieldsValue = [ document.getElementById('id').value, document.getElementById('note').value ];
	searchAllGrid(gridName, fields, fieldsValue, 0, limit);

}
$(function() {
	searchworkflow("stateName", "Contract", null);

	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
				commonSearchAction(gridGrid);
				$(this).dialog("close");
			},
			"清空" : function() {
				form_reset();

			}
		}, false, option);

	});

});
