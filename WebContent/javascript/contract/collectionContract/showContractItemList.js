var crmContractItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
    fields.push({
		name:'id',
		type:"string"
	});
    fields.push({
		name:'crmProduct-id',
		type:"string"
	});
    fields.push({
		name:'fee',
		type:"string"
	});
    fields.push({
		name:'price',
		type:"string"
	});
    fields.push({
		name:'num',
		type:"string"
	});
    fields.push({
		name:'contract-id',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'id',
		width:10*10,
		hidden : true
	});
	var crmProductstore = new Ext.data.JsonStore({
		root : 'results',
		remoteSort : true,
		fields : [ {
			name : 'id'
		}, {
			name : 'name'
		} ], 
		proxy : new Ext.data.HttpProxy({
			url : window.ctx + '/crm/service/product/showTableJson.action?table=CrmProduct',
			method : 'POST'
		})
	});
	crmProductstore.load();
	var crmProductcob = new Ext.form.ComboBox({
		store : crmProductstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'remote',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
		cm.push({
		dataIndex:'crmProduct-id',
		header:'产品',
		width:15*10,
		renderer : Ext.util.Format.comboRenderer(crmProductcob),
		editor : crmProductcob
		});
	
		cm.push({
			dataIndex : 'price',
			header : '单价',
			width : 150,
			editor : new Ext.form.NumberField({
				allowBlank : true,
				allowNegative : false
			})
		});
		cm.push({
			dataIndex : 'num',
			header : '数量',
			width : 150,
			editor : new Ext.form.NumberField({
				allowBlank : true,
				allowNegative : false
			})
		});
		cm.push({
			dataIndex : 'fee',
			header : '金额',
			width : 150,
			editor : new Ext.form.NumberField({
				allowBlank : true,
				allowNegative : false
			})
		});
		cm.push({
			dataIndex:'contract-id',
			header:'contract-id',
			width:10*10,
			hidden : true
		});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/contract/collectionContract/showContractItemListJson1.action?id="+ $("#contract_id").val();
	var opts={};
	opts.title="产品明细";
	opts.height =  document.body.clientHeight-300;
	opts.tbar = [];
	opts.tbar.push({
		text : '删除选中',
		handler : function() {
			
			var selectRecord = crmContractItemGrid.getSelectRecord();
			if (selectRecord && selectRecord.length > 0) {
				var vals = "";
				$.each(selectRecord, function(i, obj) {
					vals = vals + "'" + obj.get("id") + "',";
				});
				vals = vals.substring(0, vals.length - 1);
				ajax("post", "/crm/customer/patient/delModelId.action", {
					objName : "ContractItem",
					objProperty : "id",
					vals : vals
				}, function(data) {
					if (data.success) {
						crmContractItemGrid.getStore().commitChanges();
						crmContractItemGrid.getStore().reload();
						message("删除成功！");
					} else {
						message("删除失败！");
					}
				}, null);
			} else {
				message("请选择！");
			}
		}
	});

	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	crmContractItemGrid=gridEditTable("crmContractItemdiv",cols,loadParam,opts);
	$("#crmContractItemdiv").data("crmContractItemGrid", crmContractItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})


	
