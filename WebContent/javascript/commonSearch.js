﻿function commonSearchActionByMo(gridName,type) {

	var limit = parseInt((document.body.clientHeight - 50) > 0 ? (document.body.clientHeight - 50) / 24 : 1);
	if ($.trim($("#limitNum").val()) != '') {
		limit = document.getElementById("limitNum").value;

	}

	makeSearchJsonByMo(gridName, 0, limit,type);

}

function makeSearchJsonByMo(gridName, start, limt,type) {
	var data = "{";
	jQuery("input:text,input:hidden,textarea,select").each(function() {
		var vName = jQuery(this).attr('name');
		var vValue = jQuery(this).attr('value');
		var vSearchField = jQuery(this).attr('searchField');
		var vSearchType = jQuery(this).attr('searchType');
		if (vSearchField != undefined && vSearchField != '' && vValue != undefined && vValue != ''&&vSearchType==type) {
			if (data != "{") {
				data += ",";
			}

			if((vName.indexOf("##@@##")>-1)&&(vValue.indexOf("##@@##")>-1)){
				
				data += "\"" + vName + "\": \"" + vValue + "\"";
					
			}else{
				
				data += "\"" + vName + "\": \"%" + vValue + "%\"";
			}
			
			
		}

	});

	data += "}";
	document.getElementById('extJsonDataString').value = data;
	
		// var gridGrid = Ext.getCmp(gridName);

		var o = {
			start : start,
			limit : limt,
			data : data
		};

		// gridName.pageSize=limt;
		gridName.store.load({
			params : o
		});
		gridName.render();
	
}