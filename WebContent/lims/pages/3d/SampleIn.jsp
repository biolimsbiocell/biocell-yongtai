<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>
			<fmt:message key="biolims.NewStoragePosition.storageManagement" />
		</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
		<link rel="stylesheet" type="text/css" href="${ctx}/lims/css/3d.css" />
		<link rel="stylesheet" type="text/css" href="${ctx}/css/sampleIcon/iconfont.css"/>
		<script type="text/javascript" src="${ctx}/lims/plugins/layui/layui.js"></script>
	</head>

	<body class="hold-transition">
		<div class="wrapper">
			<div class="content-wrapper" id="content1" style="margin-left: 0px;">
				<section class="content">
					<div class="row">
						<div class="alert alert-info" role="alert"></div>
						<div class="col-xs-12">
							<div class="box box-info box-solid">
								<div class="box-header with-border" id="myStorageType" type="0">
									<i class="fa fa-bell-o"></i>
									<h3 class="box-title" style="margin-right: 20px;"><fmt:message key="biolims.sample.sampleStorage"/></h3>
									<button class="btn  btn-sm  btn-info myStorageType  active" id="inStorage"><fmt:message key="biolims.sample.sampleStorage"/></button>
									<%-- <button class="btn btn-sm btn-info myStorageType"><fmt:message key="biolims.NewStoragePosition.bagsPut"/></button> --%>
									<button class="btn btn-sm btn-info choseStorage"><fmt:message key="biolims.NewStoragePosition.SelectBox"/></button>
								</div>
								<div class="box-body ipadmini" style="min-height: 95%;">
									<div class="row">

										<!--待存样本-->
										<div class="col-xs-4">
											<div class="box box-primary">
												<div class="box-header with-border">
													<i class="glyphicon glyphicon-leaf"></i>
													<h3 class="box-title"><fmt:message key="biolims.common.waitSample"/></h3>
												</div>
												<!-- /.box-header -->
												<div class="box-body" id="staySaveTembody">
													<div id="mask"></div>
													<div class="input-group">
														<input type="text" class="form-control" id="searchText" placeholder=<fmt:message key="biolims.common.fileSampleCode" />>
														<span class="input-group-btn">
				        									<button class="btn btn-info" id="search">Search</button>
				     									 </span>
													</div>
													<div class="input-group">
														<input type="text" class="form-control" id="searchSampleType" placeholder="请输入样本类型">
														<span class="input-group-btn">
				        									<button class="btn btn-info" id="searchType">Search</button>
				     									 </span>
													</div>

												</div>
												<div class="box-footer table-responsive">
													<table class="table table-condensed table-hover" id="staySaveTem">

													</table>
												</div>
											</div>
										</div>
										<!--冰箱-->
										<div class="col-xs-3 ">
											<div class="box box-primary">
												<div class="box-header with-border">
													<i class="glyphicon glyphicon-leaf"></i>
													<h3 class="box-title">液氮罐</h3>
													<br/>
													<button class="btn  btn-sm  btn-info" id="oneself">自体细胞库</button>
													<button class="btn  btn-sm  btn-info" id="public">公共细胞库</button>
													<button class="btn  btn-sm  btn-info" id="sample">样本库</button>
												</div>
												<!-- /.box-header -->
												<div class="box-body icebox">
													<ul class="parentWrap">

													</ul>
												</div>
											</div>
										</div>
										<!--储位-->
										<div class="col-xs-5 chuwei">
											<!--排-->
											<div class="box box-primary chushelf">
												<div class="box-header with-border" id="animate">
													<i class="fa fa-calendar"></i>
													<h3 class="box-title">
									
									<span class="chushelfTitle">
									<fmt:message key="biolims.common.storageName"/>
									</span>
									</h3>

													<div class="box-tools pull-right">
														<fmt:message key="biolims.NewStoragePosition.sample" />
														<button class="btn btn-sm" style="background-color:#AED09E;"></button>
														<fmt:message key="biolims.NewStoragePosition.blood" />
														<button class="btn btn-sm" style="background-color:#FF7F5B;"></button>
														<!--<button type="button" class="btn btn-sm btn-info showBox">显示盒子</button>-->
													</div>

												</div>
												<!-- /.box-header -->
												<div class="box-body table-responsive">
													<!--架子-->
													<table class="table table-striped table-bordered" id="temShelf">

													</table>
												</div>

											</div>
											<!--盒子-->
											<div class="box chuBox box-primary">
												<div class="box-header with-border" id="animate2">
													<i class="fa fa-adjust"></i>
													<h3 class="box-title"></h3>
													<div class="box-tools pull-right">
														<button class="btn btn-info btn-sm" id="cleanTem"><fmt:message key="biolims.common.refresh"/></button>
														<button class="btn btn-info btn-sm" id="saveTem"><fmt:message key="biolims.common.submit"/></button>
														<button class="btn btn-info btn-sm" id="saveOutTem"><fmt:message key="biolims.common.submit"/></button>
														<button class="btn btn-info btn-sm" id="saveOutBox"><fmt:message key="biolims.common.putInStorage"/></button>
													</div>
												</div>
												<!-- /.box-header -->
												<div class="box-body table-responsive">
													<!--盒子-->
													<table class="table" id="temBox">

													</table>
												</div>

											</div>
										</div>
										<!--样本信息-->
										<div class="col-xs-3" id="temInfo">
											<div class="box">
												<div class="box-header bg-blue-active temInfoHead">
													<div id="temIcon">
														<i class="fa fa-credit-card"></i>
													</div>
													<h3 class="box-title temInfoTitle"><fmt:message key="biolims.common.sampleInfo"/></h3>
													<h5 class="temInfoSub"><span>Code:</span><i>201356</i></h5>
													<div class="box-tools pull-right">
														<button type="button" class="btn btn-xs btn-danger rmvTemInfo"><i class="fa fa-times"></i></button>
													</div>
												</div>
												<!-- /.box-header -->
												<div class="box-body table-responsive temInfo">
													<table class="table with-border table-hover">
														<tr>
															<td>
																<label><fmt:message key="biolims.common.sampleName"/>:
												<input type="text" value="1" disabled/></label>
															</td>
														</tr>
														<tr>
															<td>
																<label><fmt:message key="biolims.common.sampleCode"/>:
												<input type="text" value="1" disabled/></label>
															</td>

														</tr>
														<tr>
															<td>
																<label><fmt:message key="biolims.common.samplePosition"/>:
												<input type="text" value="1" disabled/></label>
															</td>
														</tr>
														<tr>
															<td>
																<label><fmt:message key="biolims.user.concentration"/>:
												<input type="text" value="1" disabled/></label>
															</td>
														</tr>
														<tr>
															<td>
																<label><fmt:message key="biolims.common.bulk"/>:
												<input type="text" value="1" disabled/></label>
															</td>
														</tr>
														<tr>
															<td>
																<label><fmt:message key="biolims.common.note"/>:
												<input type="text" value="1" disabled/></label>
															</td>
														</tr>
													</table>
												</div>
												<div class="box-footer temInfoBtn bg-blue-active">

												</div>
											</div>
										</div>

									</div>
								</div>
							</div>
						</div>

					</div>
				</section>
			</div>
		</div>
		<script src="${ctx}/lims/dist/js/app.min.js "></script>
		<script src="${ctx}/lims/js/3d/sampleIn.js" type="text/javascript " charset="utf-8 "></script>
		<script src="${ctx}/lims/js/3d/3dChoseBox.js" type="text/javascript " charset="utf-8 "></script>
	</body>

</html>