<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>
			<fmt:message key="biolims.NewStoragePosition.storageManagement" />
		</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
		<link rel="stylesheet" type="text/css" href="${ctx}/lims/css/3d.css" />
		<link rel="stylesheet" type="text/css" href="${ctx}/css/sampleIcon/iconfont.css"/>
			<script type="text/javascript" src="${ctx}/lims/plugins/layui/layui.js"></script>
	</head>

	<body class="hold-transition">
		<div class="wrapper">
			<div class="content-wrapper" id="content1" style="margin-left: 0px;">
				<section class="content">
					<div class="row">
						<div class="alert alert-info" role="alert"></div>
						<div class="col-xs-12">
							<div class="box box-info box-solid">
								<div class="box-header with-border" id="myStorageType" type="2">
									<i class="fa fa-bell-o"></i>
									<h3 class="box-title" style="margin-right: 20px;"><fmt:message key="biolims.tStorage.sampleManager" /></h3>
									<div class="box-tools pull-right">
										<div class="input-group" style="width: 300px;">
											<input type="text" class="form-control" id="searchPostionCode" placeholder=<fmt:message key="biolims.common.fileSampleCode1" />>
											<span class="input-group-btn">
        									<button class="btn btn-info" id="searchPositionBtn">Search!</button>
     									 </span>
										</div>
									</div>
								</div>
								<div class="box-body ipadmini" style="min-height: 95%;">
									<div class="row">
										<!--冰箱-->
										<div class="col-xs-5 ">
											<div class="box box-primary">
												<div class="box-header with-border">
													<i class="glyphicon glyphicon-leaf"></i>
													<h3 class="box-title"><fmt:message key="biolims.common.refrigerators"/></h3>
													<div class="box-tools pull-right">
														<button class="btn btn-sm btn-success" onclick='$("#maincontentframe", window.parent.document)[0].src = window.ctx + "/storage/newPosition/newIceBox.action"'><fmt:message key="biolims.common.create" /></button>
														<button class="btn btn-sm btn-success" onclick='$("#maincontentframe", window.parent.document)[0].src = window.ctx + "/storage/newPosition/editIceBox.action?id="+$(".iceChosed").attr("id")'><fmt:message key="biolims.common.edit" /></button>
													</div>
												</div>
												<!-- /.box-header -->
												<div class="box-body icebox">
													<ul class="parentWrap">

													</ul>
												</div>
											</div>
										</div>
										<!--储位-->
										<div class="col-xs-7 chuwei">
											<!--排-->
											<div class="box box-primary chushelf">
												<div class="box-header with-border" id="animate">
													<i class="fa fa-calendar"></i>
													<h3 class="box-title">
									
									<span class="chushelfTitle">
									<fmt:message key="biolims.common.storageName"/>
									</span>
									</h3>

													<div class="box-tools pull-right">
														<fmt:message key="biolims.NewStoragePosition.sample" />
														<button class="btn btn-sm" style="background-color:#AED09E;"></button>
														<fmt:message key="biolims.NewStoragePosition.blood" />
														<button class="btn btn-sm" style="background-color:#FF7F5B;"></button>
														<button type="button" class="btn btn-xs btn-info" id="moveShelfBox"  style="margin-left: 30px;"><fmt:message key="biolims.NewStoragePosition.moveLocation"/></button>
													</div>

												</div>
												<!-- /.box-header -->
												<div class="box-body table-responsive">
													<!--架子-->
													<table class="table table-striped table-bordered" id="temShelf">

													</table>
												</div>

											</div>
											<!--盒子-->
											<div class="box chuBox box-primary">
												<div class="box-header with-border" id="animate2">
													<i class="fa fa-adjust"></i>
													<h3 class="box-title"></h3>
													<div class="box-tools pull-right">
														<button class="btn btn-sm btn-info nextStorage"><fmt:message key="biolims.sample.sampleOutStorage"/></button>
														<div class="btn-group" id="move">
															<button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><fmt:message key="biolims.NewStoragePosition.moveLocation"/><span class="caret"></span>
  </button>
															<ul class="dropdown-menu">
																<li class="moveitem">
																	<a href="#">
																		<fmt:message key="biolims.NewStoragePosition.moveSamples" />
																	</a>
																</li>
																<li role="separator" class="divider"></li>
																<li class="moveitem">
																	<a href="#">
																		<fmt:message key="biolims.NewStoragePosition.moveBox" />
																	</a>
																</li>
															</ul>
														</div>
													</div>
												</div>
												<!-- /.box-header -->
												<div class="box-body table-responsive">
													<!--盒子-->
													<table class="table" id="temBox">

													</table>
												</div>

											</div>
											<!--血袋-->
											<div class="box bloodBox box-primary" style="display: none;">
												<div class="box-header with-border">
													<i class="glyphicon glyphicon-tint"></i>
													<h3 class="box-title"></h3>
													<div class="box-tools pull-right">
														<button class="btn btn-sm btn-info nextStorage"><fmt:message key="biolims.NewStoragePosition.bagsPutOut"/></button>
														<button class="btn btn-info btn-sm" id="moveBlood"><fmt:message key="biolims.NewStoragePosition.moveLocation"/></button>
													</div>
												</div>
												<!-- /.box-header -->
												<div class="box-body">
													<!--盒子-->

												</div>

											</div>
										</div>
									</div>
									<!--下一步流向的弹出层-->
									<div class="nextStor">
										<div class="box box-danger mask">
											<div class="box-header ">
												<button class="btn btn-success btn-sm pull-right rmvNextStor"><i class="fa fa-times"></i></button>
												<div class="dropdown">
													<button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    <fmt:message key="biolims.common.wetherToReturnLibrary"/>
    <span class="caret"></span>
  </button>
													<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
														<li>
															<a href="#" class="back">
																<fmt:message key="biolims.common.returnLibrary" />
															</a>
														</li>
														<li>
															<a href="#" class="noBack">
																<fmt:message key="biolims.common.notToReturnLibrary" />
															</a>
														</li>
													</ul>
												</div>
												<div class="dropdown">
													<button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
   <fmt:message key="biolims.common.flow"/>
    <span class="caret"></span>
  </button>
													<ul class="dropdown-menu" aria-labelledby="dropdownMenu2">

													</ul>
												</div>
												<div class="dropdown" style="top: 3px;">
													<select class="form-control" id="outSelect">
														<option>
															<fmt:message key="biolims.common.outboundType" />
														</option>
														<option>
															<fmt:message key="biolims.common.experiment" />
														</option>
														<option>
															<fmt:message key="biolims.common.returnToCustomer" />
														</option>
														<option>
															<fmt:message key="biolims.common.outsourceOutStorage" />
														</option>
														<option>
															<fmt:message key="biolims.common.sampleDestruction" />
														</option>
													</select>
												</div>
											</div>

											<div class="box-body">
												<table class="table table-bordered table-hover">
													<thead>
														<tr>
															<th><input type="checkbox" id="allCheck" /></th>
															<th>
																<fmt:message key="biolims.common.code" />
															</th>
															<th>
																<fmt:message key="biolims.common.wetherToReturnLibrary" />
															</th>
															<th>
																<fmt:message key="biolims.common.flow" />
															</th>
															<th>
																<fmt:message key="biolims.common.note" />
															</th>
														</tr>
													</thead>
													<tbody id="nextStorBody">

													</tbody>
												</table>
											</div>
											<div class="box-footer">

												<div class="input-group">
													<input type="text" class="form-control" id="scanText" placeholder=<fmt:message key="biolims.common.outSampleputCode"></fmt:message> >
													<span class="input-group-btn">
        									<button class="btn btn-info" id="csan">Go!</button>
     									 </span>
												</div>
												<!-- /input-group -->

												<div>
													<button class="btn btn-success confirm" style="width: 100%;margin-top: 20px;"><fmt:message key="biolims.common.identifyDelivery"/></button>
												</div>

											</div>
										</div>
									</div>

									<!--库内样本位置查询-->
									<div class="searchPositionBlock">
										<div class=" box box-danger">
											<div class="box-header ">
												<button class="btn btn-success btn-sm pull-right rmvSearchPostion"><i class="fa fa-times"></i></button>
											</div>

											<div class="box-body">
												<table class="table table-bordered table-hover">
													<thead>
														<tr>
															<th><input type="checkbox" /></th>
															<th>
																<fmt:message key="biolims.common.code" />
															</th>
															<th>
																<fmt:message key="biolims.common.samplePosition" />
															</th>
															<th>
																<fmt:message key="biolims.common.sampleType" />
															</th>
															<th>
																<fmt:message key="biolims.common.totalAmount" />
															</th>
														</tr>
													</thead>
													<tbody id="searchPositionBody">

													</tbody>
												</table>
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>

					</div>
				</section>
			</div>
		</div>
		<script src="${ctx}/lims/dist/js/app.min.js "></script>
		<script src="${ctx}/lims/plugins/template/template.js" type="text/javascript" charset="utf-8"></script>
		<script src="${ctx}/lims/js/3d/sampleManager.js" type="text/javascript " charset="utf-8 "></script>
		<script src="${ctx}/lims/js/3d/3dChoseBox.js" type="text/javascript " charset="utf-8 "></script>
		<script type="text/html" id="templatePosition">
			{{each data}}
			<tr>
				<td><input type="checkbox"></td>
				<td>{{$value.code}}</td>
				<td>{{$value.location}}</td>
				<td>{{$value.sampleType}}</td>
				<td>{{$value.sumTotal}}</td>
			</tr>
			{{/each}}
		</script>
	</body>

</html>