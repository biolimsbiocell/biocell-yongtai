<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>

<html>

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Welcome to Bio-LIMS</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		
		<!--icon-->
		<link rel="shortcut icon" href="${ctx}/lims/img/favicon.ico" />
		<!-- Bootstrap 3.3.6 -->
		<link rel="stylesheet" href="${ctx}/lims/bootstrap/css/bootstrap.min.css">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="${ctx}/lims/dist/css/font-awesome.min.css">
		<!-- Ionicons -->
		<link rel="stylesheet" href="${ctx}/lims/dist/css/ionicons.min.css">
		<!-- Theme style -->
		<link rel="stylesheet" href="${ctx}/lims/dist/css/AdminLTE.min.css">
		<!-- iCheck -->
		<link rel="stylesheet" href="${ctx}/lims/plugins/icheck/skins/square/blue.css">
		<!--login.css-->
		<link rel="stylesheet" type="text/css" href="${ctx}/lims/css/login.css" />
		<link rel="stylesheet" type="text/css" href="//at.alicdn.com/t/font_yxt5suxamj9k9.css"/>
		<style type="text/css">
		#show {
			display:none;
			position:absolute;
			top:2%;
			left:45%;
			width:53%;
			height:49%;
			padding:8px;
			border:8px solid #E8E9F7;
			background-color:white;
			z-index:1002;
			overflow:auto;
		}
		</style>
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

	</head>

	<body class="hold-transition" style="position: relative;">
		<div class="row mask">
			<div class="col-xs-12"></div>
		</div>

		<div class="login-logo">
			<a href="#"><img id="imgId"  src="${ctx}/lims/img/logo.png" width="400px"/></a>
		</div>
		<input type="hidden" id="errmsg" value="${requestScope.errmsg}">
		<div class="login-box">
			<!-- /.login-logo -->
			<div class="login-box-body">
				<p class="login-box-msg" id="msg"><font color="red">细胞生产管理系统V7.0</font></p>
				<form method="post" id="login">
					<div class="form-group has-feedback">
						<input type="text" value="" class="form-control name" placeholder="Username" name="user" required autofocus>
						<span class="glyphicon glyphicon-user form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
						<input type="password" value="" class="form-control psd" placeholder="Password" name="pad" required>
						<span class="glyphicon glyphicon-lock form-control-feedback"></span>
					</div>
					<div style="text-align: center;margin-bottom: 10px;display:none;">
						<input type="radio" name="langue" id="china" checked/><img src="${ctx}/lims/img/china.png" width="25px"/>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="langue" id="english"/><img src="${ctx}/lims/img/english.png" width="25px"></img>
					</div>
					<input type="hidden" id="language" name="language" value="">
					<div class="row">
						<div class="col-xs-12">
							<button class="btn btn-primary btn-block" id="logg">登录</button>
						</div>
						<!-- /.col -->
					</div>
				</form>
			</div>
			<div class="footer">
				<text>本试用版系统仅供用户体验使用，不得用于任何商业用途或抄袭、复制、逆向工程等侵权行为</text>
				<br>
				<img src="${ctx}/lims/img/logo.gif" width="20px" alt="" />
<text>本系统的版权归北京百奥利盟软件技术有限公司所有</text>
			</div>
			
			<!-- /.login-box-body -->
		</div>
		<!-- /.login-box <-->
		<!-- jQuery 2.2.3 -->
		<script src="${ctx}/lims/plugins/jQuery/jquery-2.2.3.min.js"></script>
		<!-- Bootstrap 3.3.6 -->
		<script src="${ctx}/biolims/layui.all.js"></script>
		<script src="${ctx}/biolims/layui.js"></script>
		<script src="${ctx}/lims/bootstrap/js/bootstrap.min.js"></script>
		<script src="${ctx}/lims/js/login.js" type="text/javascript" charset="utf-8"></script>
		
	</body>

</html>