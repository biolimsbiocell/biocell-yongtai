<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Biolims | 样本流程控制</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!--icon-->
		<link rel="shortcut icon" href="${ctx}/lims/img/favicon.ico" />
		<!-- Bootstrap 3.3.6 -->
		<link rel="stylesheet" href="${ctx}/lims/bootstrap/css/bootstrap.min.css">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="${ctx}/lims/dist/css/font-awesome.min.css">
		<!-- Ionicons -->
		<link rel="stylesheet" href="${ctx}/lims/dist/css/ionicons.min.css">

		<link rel="stylesheet" href="${ctx}/lims/plugins/datatables/dataTables.bootstrap.css">
		<!-- Theme style -->
		<link rel="stylesheet" href="${ctx}/lims/dist/css/AdminLTE.min.css">
		<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
		<link rel="stylesheet" href="${ctx}/lims/dist/css/skins/_all-skins.min.css">
		<link rel="stylesheet" type="text/css" href="${ctx}/lims/css/todolist.css" />
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
	</head>

	<body class="hold-transition">
		<div class="wrapper">

			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper" id="content">
				<!-- Content Header (Page header) -->
				<section class="content-header">
					<h1>
        				超声破碎 
      				</h1>
					<ol class="breadcrumb">
						<li>
							<a href="#"><i class="fa fa-dashboard"></i> Home</a>
						</li>
						<li>
							<a href="#">实验中心</a>
						</li>
						<li class="active">超声破碎</li>
					</ol>
				</section>
				<!-- Main content -->
				<section class="content">
					<!--页面1-->
					<div id="mask"></div>
					<div class="row" id="content1">
						<div class="col-md-12">
							<div class="box box-success">
								<div class="box-header">
									<h2 class="box-title text-center">
										<strong>样本中心</strong>
									</h2>
									<div class="input-group">
										<input type="text" class="form-control" id="searchText" placeholder="请扫码输入样本编码">
										<span class="input-group-btn">
        									<button class="btn btn-info" id="search">Search!</button>
     									 </span>
									</div>
								</div>
								<div class="box-body table-responsive" id="sampleHeight">
									<table class="table table-hover">
										<thead>
											<tr class="info">
												<th><input type="checkbox"></th>
												<th>样本编号</th>
												<th>原始样本编号</th>
												<th>实验室样本号</th>
												<th>样本类型</th>
												<th>Qubit浓度(ng/ul)</th>
												<th>Nanodrop浓度(ng/ul)</th>
												<th>检测项目</th>
											</tr>
										</thead>
										<tbody id="sample-body">

										</tbody>
									</table>
								</div>
								<div class="box-footer">
									<div class="box box-danger text-center">
										<div class="box-header">
											<h3 class="box-title "><strong>SOP</strong></h3>
										</div>
										<div class="box-body" id="sop-body">
											<button class="btn btn-primary btn-block btn-lg disabled">DNA-SOP</button>
										</div>
										<!-- /.box-body -->
										<div class="box-footer">
											<button class="btn btn-danger pull-right" id="nextbtn1">下一步</button>
										</div>
									</div>
								</div>
							</div>
							<!-- /.box -->
						</div>
					</div>
					<!--页面2-->
					<div id="mask2"></div>
					<div id="content2">
						<div class="row">
							<div class="col-xs-6" id="changeOne">
								<div class="box box-success">
									<div class="box-header">
										<h3 class="box-title"><strong>样本中心</strong></h3>
									</div>
									<div class="box-body table-responsive" id="gridHeight">
										<table id="table-self" class="table table-condensed">
											<thead>
												<tr>
													<th><input type="checkbox" class="check-all"></th>
													<th>样本编号</th>
												<th>原始样本编号</th>
												<th>实验室样本号</th>
												<th>样本类型</th>
												<th>Qubit浓度(ng/ul)</th>
												<th>Nanodrop浓度(ng/ul)</th>
												<th>检测项目</th>
												</tr>
											</thead>
											<tbody id="grid-body">

											</tbody>
										</table>
									</div>
									<!-- /.box-body -->
								</div>
								<!-- /.box -->
							</div>
							<!-- /.col (left) -->
							<div class="col-xs-6" id="changeTwo">
								<div class="box box-danger text-center">
									<div class="box-header">
										<h3 class="box-title ">
              	<strong>孔板</strong>
              </h3>
									</div>
									<div class="box-body">
										<table class="table" id="grid">

										</table>
									</div>
									<!-- /.box-body -->
								</div>
								<!-- /.box -->
							</div>
							<!-- /.col (right) -->
						</div>
						<div class="row">
							<div class="col-xs-12">
								<div class="box box-success">
									<div class="box-header">
										<h3 class="box-title"><strong>样本中心</strong></h3>
										<ul class="pull-right">
											<li class="btn btn-xs btn-danger" id="nextbtn2">保存并下一步</li>
											<li class="btn btn-xs btn-danger" id="prev2">上一步</li>
										</ul>
									</div>
									<div class="box-body table-responsive" id="resultHeight">
										<table class="table table-bordered">
											<thead>
												<tr class="success">
													<th>坐标</th>
													<th>样本编号</th>
												<th>原始样本编号</th>
												<th>实验室样本号</th>
												<th>样本类型</th>
												<th>Qubit浓度(ng/ul)</th>
												<th>Nanodrop浓度(ng/ul)</th>
												<th>检测项目</th>
												</tr>
											</thead>
											<tbody id="result-body">

											</tbody>
										</table>
									</div>
									<!-- /.box-body -->
								</div>
								<!-- /.box -->
							</div>
						</div>

					</div>
					<!--页面3-->
					<div id="content3">
						<div class="row">
							<div class="col-xs-12" id="yemian3">
								<div class="box box-success">
									<div class="box-header" style="
    padding-bottom: 0px;
">
										<ul class="pull-right">
											<li class="btn btn-xs btn-danger" id="nextbtn3">下一步</li>
											<li class="btn btn-xs btn-danger" id="prev3">上一步</li>
										</ul>
									</div>
									<div class="box-body" style="
    padding-top: 0px;
">
										<div class="row" id="stepbg">
											<div class="col-xs-2 pull-left">

												<div class="step-head">
													<table class="table">

													</table>
												</div>
											</div>
											<div class="col-xs-9 pull-right">
												<div class="step-body">
													<div id="tem" class="table-responsive">
														<h4><i class="fa fa-hand-lizard-o"></i>步骤 <button class="btn btn-sm btn-info pull-right savetem">保存</button></h4>
														<table class="table no-border">
															<tr>
																<td> <label><i class="fa fa-user"></i>实验员：<input type="text"/></label></td>
																<td><button class="btn btn-xs btn-success startDate">开始日期</button>：
																	<input type="text" /></td>
																<td><button class="btn btn-xs btn-success stopDate">结束日期</button>：
																	<input type="text" /></td>
																
															</tr>
															<tr>
																<td colspan="4" class="steptable">步骤明细：
																	<div>
																	</div>
																</td>
															</tr>
														</table>
													</div>
													<div class="fengexian"></div>
													<div id="reagent" class="table-responsive">
														<h4><i class="fa fa-eyedropper"></i>原辅料 <button class="btn btn-xs btn-info pull-right moreRet">增加原辅料</button></h4>

													</div>
													<div class="fengexian"></div>
													<div id="instrument" class="table-responsive">
														<h4><i class="fa  fa-flask"></i>设备<button class="btn btn-xs btn-info pull-right moreIst">增加设备</button></h4>

													</div>
												</div>
											</div>
										</div>
										<!-- /.row -->

									</div>
								</div>
							</div>
						</div>
						<!-- /.row -->
					</div>

					<!--页面4-->
					<div class="row" id="content4">
						<div class="col-md-12">
							<div class="box box-success">
								<div class="box-header">
									<button class="btn btn-xs btn-danger pull-right" id="prev4">上一步 </button> &nbsp;&nbsp;&nbsp;
									<button class="btn btn-xs btn-danger pull-right" id="nextbtn4">保存</button>
								</div>
								<div class="box-body table-responsive" id="saveResuleHeight">
									<table class="table table-hover">
										<thead>
											<tr class="info">

												<th><input type="checkbox"></th>
												<th>样本编号</th>
												<th>原始样本号</th>
												<th>实验室样本号 </th>
												<th>样本类型</th>
												<th>检测项目</th>
												<th>体积</th>
												<th>总量</th>
											</tr>
										</thead>
										<tbody id="save-result">

										</tbody>
									</table>
								</div>
							</div>
							<!-- /.box -->
						</div>
					</div>

				</section>
				<!-- /.content -->
			</div>

		</div>
		<!-- ./wrapper -->

		<script src="${ctx}/lims/plugins/jQuery/jquery-2.2.3.min.js"></script>
		<!-- Bootstrap 3.3.6 -->
		<script src="${ctx}/lims/bootstrap/js/bootstrap.min.js"></script>
		<!-- SlimScroll -->
		<script src="${ctx}/lims/plugins/slimScroll/jquery.slimscroll.min.js"></script>
		<!-- FastClick -->
		<script src="${ctx}/lims/plugins/fastclick/fastclick.js"></script>

		<script src="${ctx}/lims/plugins/daterangepicker/moment.js" type="text/javascript" charset="utf-8"></script>

		<!-- AdminLTE App -->
		<script src="${ctx}/lims/dist/js/app.min.js"></script>
		<!-- AdminLTE for demo purposes -->
		<script src="${ctx}/lims/plugins/template/template.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/html" id="sampleTemplate">
			{{each data}}
			<tr>
				<td><input type="checkbox"></td>
				<td>{{$value.code}}</td>
				<td>{{$value.sampleCode}}</td>
				<td>{{$value.labCode}}</td>
				<td>{{$value.sampleType}}</td>
				<td>{{$value.concentration}}</td>
				<td>{{$value.nconcentration}}</td>
				<td style="display: none;">{{$value.productId}}</td>
				<td>{{$value.productName}}</td>
			</tr>
			{{/each}}
		</script>
		<script type="text/html" id="gridTemplate">
			{{each data}}
			<tr>
				<td><input type="text" disabled value=""></td>
				<td><input type="text" disabled value={{$value.code}}></td>
				<td><input type="text" disabled value={{$value.sampleCode}}></td>
				<td><input type="text" disabled value={{$value.labCode}}></td>
				<td><input type="text" disabled value={{$value.sampleType}}></td>
				<td><input type="text" disabled value={{$value.concentration}}></td>
				<td><input type="text" disabled value={{$value.nconcentration}}></td>
				<td style="display: none;"><input type="text" value={{$value.productId}}></td>
				<td><input type="text" disabled value={{$value.productName}}></td>
			</tr>
			{{/each}}
		</script>
		<script type="text/html" id="clickGridTemplate">
			{{each gridData}}
			<tr>
				<td><input type="text" disabled value=""></td>
				<td><input type="text" disabled value={{$value.code}}></td>
				<td><input type="text" disabled value={{$value.sampleCode}}></td>
				<td><input type="text" disabled value={{$value.labCode}}></td>
				<td><input type="text" disabled value={{$value.sampleType}}></td>
				<td><input type="text" disabled value={{$value.concentration}}></td>
				<td><input type="text" disabled value={{$value.nconcentration}}></td>
				<td style="display: none;"><input type="text" value={{$value.productId}}></td>
				<td><input type="text" disabled value={{$value.productName}}></td>
			</tr>
			{{/each}}
		</script>
		<script type="text/html" id="template4">
			{{each data}}
			<tr>
				<td><input type="checkbox"></td>
				<td style="display: none;"><input type="text" value={{$value.id}}></td>
				<td><input type="text" disabled value={{$value.code}}></td>
				<td><input type="text" disabled value={{$value.sampleCode}}></td>
				<td><input type="text" disabled value={{$value.labCode}}></td>
				<td><input type="text" disabled value={{$value.sampleType}}></td>
				<td style="display: none;"><input type="text" value={{$value.productId}}></td>
				<td><input type="text" disabled value={{$value.productName}}></td>
				<td><input type="text" value={{$value.volume}}></td>
				<td><input type="text" value={{$value.dataNum}}></td>
			</tr>
			{{/each}}
		</script>
		<script src="${ctx}/lims/js/todolist/ufTask.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/JavaScript" src="${ctx}/js/language/ext-lang-<%=session.getAttribute("lan")%>.js"></script>
	</body>

</html>