<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>

<html>

<head>
<meta charset="utf-8">
<title>Welcome to Bio-LIMS</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet"
	href="${ctx}/lims/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="${ctx}/lims/dist/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="${ctx}/lims/dist/Ionicons/css/ionicons.min.css">
<link rel="stylesheet" type="text/css"
	href="${ctx}/lims/css/dashboard.css" />
<link rel="stylesheet" href="${ctx}/lims/dist/css/AdminLTE.min.css">
<link rel="stylesheet" type="text/css"
	href="${ctx}/lims/plugins/datatables/datatables.min.css" />
<link rel="stylesheet"
	href="${ctx}/lims/plugins/icheck/skins/square/blue.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
<style>

#main3 {
	width: 600px;
}
.box-body{
	min-height:60px !important;
	max-height: 520px !important;
}
@media screen and (min-width:600px) and (max-width:1000px) {
	#content1 {
		max-height: 600px;
	}
}
#allProduction_wrapper .dataTables_scrollBody{max-height:372px;}

</style>
</head>


<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<div class="content-wrapper" id="content1" style="margin-left: 0px;">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					<fmt:message key="biolims.dashboard.dashboard" />
					<small><fmt:message
							key="biolims.dashboard.dashboardPreview" /> </small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">
				<!--数量提示模块-->
				<%-- <div class="row" id="temNum">
						<div class="col-md-3 col-xs-6">
							<!-- small box -->
							<div class="small-box bg-aqua">
								<div class="inner">
									<h3 class="numone">0</h3>

									<p>
										<fmt:message key="biolims.dashboard.orderQuantity" />
									</p>
								</div>
								<div class="icon">
									<i class="fa fa-building"></i>
								</div>
								<a href="${ctx}/system/sample/sampleOrder/showSampleOrderTable.action" target="iframename" class="small-box-footer">
									<fmt:message key="biolims.dashboard.readMore" /> <i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div>
						<!-- ./col -->
						<div class="col-md-4 col-xs-6">
							<!-- small box -->
							<div class="small-box bg-green">
								<div class="inner">
									<h3 class="numtwo">0</h3>

									<p>
										<fmt:message key="biolims.common.sampleSize" />
									</p>
								</div>
								<div class="icon">
									<i class="fa fa-eyedropper"></i>
								</div>
								<a href="####" src="${ctx}/system/sample/sampleMain/showSampleMainList.action?type=3" target="iframename" class="small-box-footer">
									<fmt:message key="biolims.dashboard.readMore" /> <i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div>
						<!-- ./col -->
						<div class="col-md-4 col-xs-6">
							<!-- small box -->
							<div class="small-box bg-yellow">
								<div class="inner">
									<h3 class="numthree">0</h3>

									<p>
										<fmt:message key="biolims.dashboard.report" /> </p>
								</div>
								<div class="icon">
									<i class="fa fa-desktop"></i>
								</div>
								<a href="####" src="${ctx}/sample/report/main/showSampleMainReportList.action" target="iframename" class="small-box-footer">
									<fmt:message key="biolims.dashboard.readMore" /> <i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div>
						<!-- ./col -->
						<div class="col-md-4 col-xs-6">
							<!-- small box -->
							<div class="small-box bg-red">
								<div class="inner">
									<h3 class="numfour">0</h3>

									<p>
										<fmt:message key="biolims.dashboard.projectNum" />
									</p>
								</div>
								<div class="icon">
									<i class="fa fa-magnet"></i>
								</div>
								<a href="####" src="${ctx}/crm/project/showProjectList.action" target="iframename" class="small-box-footer">
									<fmt:message key="biolims.dashboard.readMore" /><i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div>
						<!-- ./col -->
					</div> --%>
				<!--待办事项与信息预警-->
				<div class="row">
					<!-- Left col -->
					<!-- TO DO List -->
					<div class="col-sm-12" id="scdb">
						<div class="box box-success">
							<div class="box-header">
								<i class="fa fa-envelope-o"></i>
								<h3 class="box-title">生产待办</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
									<button type="button" class="btn btn-box-tool"
										data-widget="remove">
										<i class="fa fa-times"></i>
									</button>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-4"
									style="padding-left: 26px">
									<div class="input-group">
										<span class="input-group-addon">房间编号</span> <input type="text"
											id="fjbh" name="fjbh" class="form-control" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-2">
									<div class="input-group">
										<input type="button" class="btn btn-info btn-sm"
											onClick="serchfjbh();" value='查询' />
									</div>
								</div>

								<!-- <div class="col-xs-12 col-sm-6 col-md-4"
									style="padding-left: 26px">
									<div class="input-group">
										<span class="input-group-addon">产品批号</span> <input type="text"
											id="pici" name="pici" class="form-control" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-2">
									<div class="input-group">
										<input type="button" class="btn btn-info btn-sm"
											onClick="serchpici();" value='查询' />
									</div>
								</div> -->
								<!-- <div class="col-xs-12 col-sm-6 col-md-4"
									style="padding-left: 26px">
									<div class="input-group">
										<span class="input-group-addon">样本编号</span>
										<textArea type="text" id="sampleCodes" name="sampleCodes"
											class="form-control"></textArea>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-2">
									<div class="input-group">
										<input type="button" class="btn btn-info btn-sm"
											onClick="serchsampleCodes();" value='样本查询' />
									</div>
								</div> -->
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<table
									class="table table-hover table-striped table-bordered table-condensed"
									id="allProduction" style="font-size: 12px;">
								</table>
							</div>

						</div>
					</div>
					<!-- /.box -->
				</div>
				<!--待办事项与信息预警-->
				<div class="row">
					<div class="col-sm-12 connectedSortable" id="shsj1">
						<div class="box box-warning">
							<div class="box-header">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title">收获时间</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
									<button type="button" class="btn btn-box-tool"
										data-widget="remove">
										<i class="fa fa-times"></i>
									</button>
								</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body table-responsive"
								style="max-height: 500px; overflow: auto">
								<table class="table table-hover warninglistHeight">
									<thead>
										<tr>
											<th><i class="fa fa-ellipsis-v"></i> <i
												class="fa fa-ellipsis-v"></i></th>
											<th>产品批号</th>
											<th>收获开始时间</th>
											<th>收获结束时间</th>
											<th>运输时间</th>
											<th>出细胞时间</th>
											<th>CCOI</th>
											<th>培养箱编号</th>
											<th>位置</th>
										</tr>
									</thead>
									<tbody class="shsj2">

									</tbody>
								</table>
							</div>

						</div>
					</div>
					<!-- Left col -->
					<div
						style="width: 50%; height: 520px; float: left; position: relative;">
						<div class="col-sm-12" id="toDoList" style="position: absolute;">

							<div class="box box-success">
								<div class="box-header">
									<i class="fa fa-envelope-o"></i>
									<h3 class="box-title">
										<fmt:message key="biolims.dashboard.todoList" />
									</h3>
									<div class="box-tools pull-right">
										<button type="button" class="btn btn-box-tool"
											data-widget="collapse">
											<i class="fa fa-minus"></i>
										</button>
										<button type="button" class="btn btn-box-tool"
											data-widget="remove">
											<i class="fa fa-times"></i>
										</button>
									</div>
								</div>
								<!-- /.box-header -->
								<div class="box-body table-responsive todolistHeight">
									<table class="table table-hover todo-list">
										<thead>
											<tr>
												<th><i class="fa fa-ellipsis-v"></i> <i
													class="fa fa-ellipsis-v"></i></th>
												<th><fmt:message key="biolims.common.title" /></th>
												<th><fmt:message key="biolims.common.taskName" /></th>
												<th><fmt:message key="biolims.common.projectLeader" />
												</th>
												<th><fmt:message key="biolims.common.createDate" /></th>
											</tr>
										</thead>
										<tbody class="list">

										</tbody>
									</table>
								</div>

							</div>
							<!-- /.box -->
						</div>
					</div>

					<div class="col-sm-6 connectedSortable" id="ggl">
						<div class="box box-warning">
							<div class="box-header">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title">公告栏</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
									<button type="button" class="btn btn-box-tool"
										data-widget="remove">
										<i class="fa fa-times"></i>
									</button>
								</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body table-responsive"
								style="max-height: 500px; overflow: auto">
								<table class="table table-hover warninglistHeight">
									<thead>
										<tr>
											<th><i class="fa fa-ellipsis-v"></i> <i
												class="fa fa-ellipsis-v"></i></th>
											<th>公告编号</th>
											<th>公告简述</th>
										</tr>
									</thead>
									<tbody class="ggl1">

									</tbody>
								</table>
							</div>

						</div>
					</div>
					<div class="col-sm-6 connectedSortable" id="warnInfo">
						<div class="box box-warning">
							<div class="box-header">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title">库存预警</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
									<button type="button" class="btn btn-box-tool"
										data-widget="remove">
										<i class="fa fa-times"></i>
									</button>
								</div>
							</div>
							<div class="box-body table-responsive"
								style="max-height: 200px; overflow: auto">
								<table class="table table-hover warninglistHeight">
									<thead>
										<tr>
											<th><i class="fa fa-ellipsis-v"></i> <i
												class="fa fa-ellipsis-v"></i></th>
											<th><fmt:message key="biolims.common.title" /></th>
											<th><fmt:message key="biolims.common.createDate" /></th>
											<th><fmt:message key="biolims.dashboard.warningContent" />
											</th>
										</tr>
									</thead>
									<tbody class="list1">
									</tbody>
								</table>
							</div>
						</div>
						<!-- /.box -->
						<div class="box box-warning">
							<div class="box-header">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title">仪器预警</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
									<button type="button" class="btn btn-box-tool"
										data-widget="remove">
										<i class="fa fa-times"></i>
									</button>
								</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body table-responsive"
								style="max-height: 200px; overflow: auto">
								<table class="table table-hover warninglistHeight">
									<thead>
										<tr>
											<th><i class="fa fa-ellipsis-v"></i> <i
												class="fa fa-ellipsis-v"></i></th>

											<th>仪器编号</th>
											<th>仪器名称</th>
											<th>开始预警日期</th>
											<th><fmt:message key="biolims.dashboard.warningContent" />
											</th>
										</tr>
									</thead>
									<tbody class="yiqi">

									</tbody>
								</table>
							</div>

						</div>
					</div>
					<!-- <div class="col-sm-6" id="waitSample">
							TO DO List
							<div class="box box-success">
								<div class="box-header">
									<i class="fa fa-envelope-o"></i>
									<h3 class="box-title">待接收样本</h3>
									<div class="box-tools pull-right">
										<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
										<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
									</div>
								</div>
								/.box-header
								<div class="box-body table-responsive todolistHeight">
									<table class="table table-hover todo-list">
										<thead>
											<tr>
												<th> <i class="fa fa-ellipsis-v"></i>
													<i class="fa fa-ellipsis-v"></i></th>
												<th>
													姓名
												</th>
												<th>
													性别
												</th>
												<th>
													外部样本编号
												</th>
												<th>
													条形码
												</th>
											</tr>
										</thead>
										<tbody class="listWs">
												
										</tbody>
									</table>
								</div>

							</div>
							/.box
						</div> -->
				</div>
				<div class="row">
					<!-- =================================== 订单到期 =================================================-->

					<!-- <div style="width: 50%; height: 520px; float: left; position: relative;"> -->
					<!-- <div class="col-sm-12" id="ordersDue" style="position: absolute;"> -->
					<div class="col-sm-6" id="ordersDue">

						<div class="box box-warning" style="border-top-color: #F39C93;">
							<div class="box-header">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title">订单到期提醒</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
									<button type="button" class="btn btn-box-tool"
										data-widget="remove">
										<i class="fa fa-times"></i>
									</button>
								</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body table-responsive todolistHeight">
								<table class="table table-hover todo-list">
									<thead>
										<tr>
											<th><i class="fa fa-ellipsis-v"></i> <i
												class="fa fa-ellipsis-v"></i></th>
											<th>订单编号</th>
											<th>预计采血时间</th>
											<th>产品批号</th>
											<!-- <th>CCOI</th> -->
											<th style="width: 83px">提醒状态</th>
										</tr>
									</thead>
									<tbody class="ordersDuelist">

									</tbody>
								</table>
							</div>

						</div>
						<!-- /.box -->
					</div>
					<!-- ===================================15分钟结果数据================================================== -->
					<div class="col-sm-6" id="resultAll">

						<div class="box box-warning" style="border-top-color: #F39C93;">
							<div class="box-header">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title">细胞计数</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
									<button type="button" class="btn btn-box-tool"
										data-widget="remove">
										<i class="fa fa-times"></i>
									</button>
								</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body table-responsive todolistHeight">
								<table class="table table-hover todo-list">
									<thead>
										<tr>
											<th><i class="fa fa-ellipsis-v"></i> <i
												class="fa fa-ellipsis-v"></i></th>
											<th style="width: 85px">区域</th>
											<th style="width: 85px">样本编号</th>
											<th style="width: 85px">检测项</th>
											<th style="width: 85px">提交时间</th>
											<th style="width: 85px">提醒状态</th>
										</tr>
									</thead>
									<tbody class="resultAll">

									</tbody>
								</table>
							</div>

						</div>
						<!-- /.box -->
					</div>
					<!--=================================== 30分钟传递窗 =================================================  -->
					<div class="col-sm-6" id="deliveryWindow">

						<div class="box box-warning" style="border-top-color: #F39C93;">
							<div class="box-header">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title">传递窗</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
									<button type="button" class="btn btn-box-tool"
										data-widget="remove">
										<i class="fa fa-times"></i>
									</button>
								</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body table-responsive todolistHeight">
								<table class="table table-hover todo-list">
									<thead>
										<tr>
											<th><i class="fa fa-ellipsis-v"></i> <i
												class="fa fa-ellipsis-v"></i></th>
											<th style="width: 83px">检测项样本编号</th>
											<th style="width: 83px">批次号</th>
											<th style="width: 83px">检测项</th>
											<th style="width: 83px">提交时间</th>
											<th style="width: 83px">提醒状态</th>
										</tr>
									</thead>
									<tbody class="deliveryWindow">

									</tbody>
								</table>
							</div>

						</div>
						<!-- /.box -->
					</div>
					<!--============================================检测项提醒=========================================  -->
					<div class="col-sm-6" id="testItem">

						<div class="box box-warning" style="border-top-color: #F39C93;">
							<div class="box-header">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title">检测项信息提醒</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
									<button type="button" class="btn btn-box-tool"
										data-widget="remove">
										<i class="fa fa-times"></i>
									</button>
								</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body table-responsive todolistHeight">
								<table class="table table-hover todo-list">
									<thead>
										<tr>
											<th><i class="fa fa-ellipsis-v"></i> <i
												class="fa fa-ellipsis-v"></i></th>
											<th style="width: 83px">样本编号</th>
											<th style="width: 83px">批次号</th>
											<th style="width: 83px">检测项</th>
											<th style="width: 83px">提交时间</th>
											<th style="width: 83px">提醒状态</th>
										</tr>
									</thead>
									<tbody class="testItem">

									</tbody>
								</table>
							</div>

						</div>
						<!-- /.box -->
					</div>
					<!-- </div> -->
					<!-- =================================== 血样接收未完成（是指所有已经建立的申请单没有走到样本接收环节并提交的）需提醒继续完成 =================================================-->

					<!-- <div style="width: 50%; height: 520px; float: left; position: relative;"> -->
					<!-- <div class="col-sm-12" id="notSampleReceive"
						style="position: absolute;"> -->
					<div class="col-sm-6" id="notSampleReceive">

						<div class="box box-warning" style="border-top-color: #F39C93;">
							<div class="box-header">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title">样本接收提醒</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
									<button type="button" class="btn btn-box-tool"
										data-widget="remove">
										<i class="fa fa-times"></i>
									</button>
								</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body table-responsive todolistHeight">
								<table class="table table-hover todo-list">
									<thead>
										<tr>
											<th><i class="fa fa-ellipsis-v"></i> <i
												class="fa fa-ellipsis-v"></i></th>
											<th>订单编号</th>
											<th>预计采血时间</th>
											<th>产品批号</th>
											<!-- <th>CCOI</th> -->
											<th>提醒状态</th>
										</tr>
									</thead>
									<tbody class="notSampleReceiveList">

									</tbody>
								</table>
							</div>

						</div>
						<!-- /.box -->
					</div>
					<!-- </div> -->
					<!-- =================================== 回输日期已出（BP后生产发出的时期），但未进行确认的，需要提醒日期确认 =================================================-->
					<!-- <div style="width: 50%; height: 520px; float: left; position: relative;"> -->
					<!-- <div class="col-sm-12" id="returnDateConfirm"
						style="position: absolute;"> -->
					<div class="col-sm-6" id="returnDateConfirm">

						<div class="box box-warning" style="border-top-color: #F39C93;">
							<div class="box-header">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title">回输日期确认</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
									<button type="button" class="btn btn-box-tool"
										data-widget="remove">
										<i class="fa fa-times"></i>
									</button>
								</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body table-responsive todolistHeight">
								<table class="table table-hover todo-list">
									<thead>
										<tr>
											<th><i class="fa fa-ellipsis-v"></i> <i
												class="fa fa-ellipsis-v"></i></th>
											<th>产品批号</th>
											<th>计划回输日期</th>

											<!-- <th>CCOI</th> -->
											<th>提醒状态</th>
										</tr>
									</thead>
									<tbody class="returnDateConfirmList">

									</tbody>
								</table>
							</div>

						</div>
						<!-- /.box -->
					</div>
					<!-- </div> -->
					<!-- =================================== 未进入运输计划的回输或订单，需提醒进入运输计划。（提前一天的10:00开始提醒） =================================================-->
					<!-- <div style="width: 50%;height:500px; float:left; position: relative;"> -->
					<!-- <div class="col-sm-12" id="toDoList" style="position: absolute;"> -->
					<div class="col-sm-6" id="noTransportPlan">

						<div class="box box-warning" style="border-top-color: #F39C93;">
							<div class="box-header">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title">运输计划确认</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
									<button type="button" class="btn btn-box-tool"
										data-widget="remove">
										<i class="fa fa-times"></i>
									</button>
								</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body table-responsive todolistHeight">
								<table class="table table-hover todo-list">
									<thead>
										<tr>
											<th><i class="fa fa-ellipsis-v"></i> <i
												class="fa fa-ellipsis-v"></i></th>
											<th>提醒开始日期</th>
											<th>预计日期</th>
											<th>产品批号</th>
											<th>来源</th>
										</tr>
									</thead>
									<tbody class="noTransportPlanList">

									</tbody>
								</table>
							</div>

						</div>
						<!-- /.box -->
					</div>
					
		<!-- =================================== 未进入运输计划的回输或订单，需提醒进入运输计划。（提前一天的10:00开始提醒） =================================================-->
					<div class="col-sm-6" id="transportPlanBug">

						<div class="box box-warning" style="border-top-color: #F39C93;">
							<div class="box-header">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title">运输计划待补充</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
									<button type="button" class="btn btn-box-tool"
										data-widget="remove">
										<i class="fa fa-times"></i>
									</button>
								</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body table-responsive todolistHeight">
								<table class="table table-hover todo-list">
									<thead>
										<tr>
											<th><i class="fa fa-ellipsis-v"></i> <i
												class="fa fa-ellipsis-v"></i></th>
											<th>运输计划编码</th>
											<th>产品批号</th>
											<th>提醒状态</th>
										</tr>
									</thead>
									<tbody class="transportPlanBugList">

									</tbody>
								</table>
							</div>

						</div>
						<!-- /.box -->
					</div>
					<!-- </div>  -->
					
					
					
			<!-- 根据人员组判断剩余质检项 -->
					<div class="col-sm-6" id="zhiJianSurplus">

						<div class="box box-warning" style="border-top-color: #F39C93;">
							<div class="box-header">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title">剩余样本</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
									<button type="button" class="btn btn-box-tool"
										data-widget="remove">
										<i class="fa fa-times"></i>
									</button>
								</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body table-responsive todolistHeight">
								<table class="table table-hover todo-list">
									<thead>
										<tr>
											<th><i class="fa fa-ellipsis-v"></i> <i
												class="fa fa-ellipsis-v"></i></th>
											<th>剩余样本</th>
											<th>检测项</th>
										</tr>
									</thead>
									<tbody class="zhiJianSurplusList">

									</tbody>
								</table>
							</div>

						</div>
						<!-- /.box -->
					</div>
					<!-- </div>  -->
				</div>
				<div class="row">
					<div class="col-sm-6 connectedSortable" id="pcInfo">
						<div class="box box-warning">
							<div class="box-header">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title">产品偏差</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
									<button type="button" class="btn btn-box-tool"
										data-widget="remove">
										<i class="fa fa-times"></i>
									</button>
								</div>
							</div>
							<div class="box-body table-responsive">
								<table class="table table-hover warninglistHeight">
									<thead>
										<tr>
											<th><i class="fa fa-ellipsis-v"></i> <i
												class="fa fa-ellipsis-v"></i></th>
											<th>描述</th>
											<th>产品批号</th>
											<th>备注</th>
										</tr>
									</thead>
									<tbody class="pcList1">
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="col-sm-6 connectedSortable" id="pcInfo1">
						<!-- /.box -->
						<div class="box box-warning">
							<div class="box-header">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title">物料偏差</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
									<button type="button" class="btn btn-box-tool"
										data-widget="remove">
										<i class="fa fa-times"></i>
									</button>
								</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body table-responsive">
								<table class="table table-hover warninglistHeight">
									<thead>
										<tr>
											<th><i class="fa fa-ellipsis-v"></i> <i
												class="fa fa-ellipsis-v"></i></th>

											<th>描述</th>
											<th>物料编号</th>
											<th>物料名称</th>
											<th>产品批号</th>
											<th>备注</th>
										</tr>
									</thead>
									<tbody class="pcList2">

									</tbody>
								</table>
							</div>

						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6 connectedSortable" id="pcInfo2">
						<div class="box box-warning">
							<div class="box-header">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title">设备偏差</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
									<button type="button" class="btn btn-box-tool"
										data-widget="remove">
										<i class="fa fa-times"></i>
									</button>
								</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body table-responsive">
								<table class="table table-hover warninglistHeight">
									<thead>
										<tr>
											<th><i class="fa fa-ellipsis-v"></i> <i
												class="fa fa-ellipsis-v"></i></th>

											<th>描述</th>
											<th>设备编号</th>
											<th>设备名称</th>
											<th>备注</th>
										</tr>
									</thead>
									<tbody class="pcList3">

									</tbody>
								</table>
							</div>

						</div>
					</div>
					<!-- 公告 -->
					<!-- <div class="col-sm-6 connectedSortable">
						<div class="box box-success">
							<div class="box-header">
								<i class="fa fa-envelope-o"></i>
								<h3 class="box-title">公告</h3>
										<input type="button" class="btn btn-info btn-xs"
											onClick="save();" value='保存' />
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
									<button type="button" class="btn btn-box-tool"
										data-widget="remove">
										<i class="fa fa-times"></i>
									</button>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-4"
									style="padding-left: 26px">
									<div class="input-group">
										<textarea   id="txt" name="txt" class="form-control"   
											  style="overflow: hidden; width: 500px; height: 112px;"  ></textarea> 
									</div>
								</div>
							</div>
							/.box-header
							<div class="box-body">
								<table
									class="table table-hover table-striped table-bordered table-condensed"
									id="allProduction" style="font-size: 12px;">
								</table>
							</div>

						</div>
					</div> -->
				</div>
				<div class="row">
					<div class="col-sm-6 connectedSortable" id="hsjh">
						<!-- <div class="box box-warning">
							<div class="box-header">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title">回输计划待确认</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
									<button type="button" class="btn btn-box-tool"
										data-widget="remove">
										<i class="fa fa-times"></i>
									</button>
								</div>
							</div>
							/.box-header
							<div class="box-body table-responsive"
								style="max-height: 200px; overflow: auto">
								<table class="table table-hover warninglistHeight">
									<thead>
										<tr>
											<th><i class="fa fa-ellipsis-v"></i> <i
												class="fa fa-ellipsis-v"></i></th>
											<th>产品批号</th>
											<th>计划回输日期</th>
										</tr>
									</thead>
									<tbody class="hsjh1">

									</tbody>
								</table>
							</div>

						</div> -->
						<div class="box box-warning">
							<div class="box-header">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title">回输计划未生成（预回输时间前5天）</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
									<button type="button" class="btn btn-box-tool"
										data-widget="remove">
										<i class="fa fa-times"></i>
									</button>
								</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body table-responsive"
								style="max-height: 200px; overflow: auto">
								<table class="table table-hover warninglistHeight">
									<thead>
										<tr>
											<th><i class="fa fa-ellipsis-v"></i> <i
												class="fa fa-ellipsis-v"></i></th>
											<th>产品批号</th>
											<th>订单号</th>
											<th>预计回输时间</th>
										</tr>
									</thead>
									<tbody class="hsjh2">

									</tbody>
								</table>
							</div>

						</div>
					</div>
				</div>
				<!--实验待办数量-->
				<div class="row">
					<!-- Left col -->
					<div class="col-sm-6" id="toDoNum">
						<!-- 实验待办数量 -->
						<div class="box box-success">
							<div class="box-header">
								<i class="fa fa-envelope-o"></i>
								<h3 class="box-title">
									<fmt:message key="biolims.common.testWaitNum" />
								</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
									<button type="button" class="btn btn-box-tool"
										data-widget="remove">
										<i class="fa fa-times"></i>
									</button>
								</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body table-responsive todolistHeight">
								<table class="table table-hover todo-list">
									<thead>
										<tr>
											<th><i class="fa fa-ellipsis-v"></i> <i
												class="fa fa-ellipsis-v"></i></th>
											<th><fmt:message key="biolims.common.testProject" /></th>
											<th><fmt:message key="biolims.common.waitNum" /></th>
										</tr>
									</thead>
									<tbody class="list2">

									</tbody>
								</table>
							</div>

						</div>
						<!-- /.box -->
					</div>

					<!-- Left col -->
					<%-- <div class="col-sm-6"  id="toDoPro">
							<!-- TO DO List -->
							<div class="box box-success">
								<div class="box-header">
									<i class="fa fa-envelope-o"></i>
									<h3 class="box-title"><fmt:message key="biolims.common.prjRemind"/></h3>
									<div class="box-tools pull-right">
										<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
										<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
									</div>
								</div>
								<!-- /.box-header -->
								<div class="box-body table-responsive todolistHeight">
									<table class="table table-hover todo-list">
										<thead>
											<tr>
												<th> <i class="fa fa-ellipsis-v"></i>
													<i class="fa fa-ellipsis-v"></i></th>
												<th>
													<fmt:message key="biolims.common.orderId"/>
												</th>
												<th>
													<fmt:message key="biolims.common.orderType"/>
												</th>
												<th>
													<fmt:message key="biolims.common.name"/>
												</th>
												<th>
													<fmt:message key="biolims.common.granter"/></th>
												<th>
													<fmt:message key="biolims.common.grantDate"/></th>
												<th>
													<fmt:message key="biolims.common.unfinishedQuantity"/></th>
											</tr>
										</thead>
										<tbody id="list3">

										</tbody>
									</table>
								</div>

							</div>
							<!-- /.box -->
						</div> --%>
				</div>
				<!--项目主数据   -->
				<div class="row" id="projectMain">
					<div class="col-xs-6">
						<div class="box box-info">
							<div class="box-header">
								<h3 class="box-title">
									<fmt:message key="biolims.dashboard.projectData" />
								</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
									<button type="button" class="btn btn-box-tool"
										data-widget="remove">
										<i class="fa fa-times"></i>
									</button>
								</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body proData">
								<ul class="parentWrap">

								</ul>
							</div>
						</div>
					</div>
					<div class="col-xs-5">
						<div class="box box-success">
							<div class="box-header">
								<i class="fa fa-bookmark"></i>
								<h3 class="box-title">
									<fmt:message key="biolims.dashboard.projectInfo" />
								</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
									<button type="button" class="btn btn-box-tool"
										data-widget="remove">
										<i class="fa fa-times"></i>
									</button>
								</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body" id="mainInfo">
								<div class="form-group row">
									<label class="col-sm-3 control-label"><fmt:message
											key="biolims.common.serialNumber" /></label>
									<div class="col-sm-9">
										<input type="text" class="form-control" disabled>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 control-label"><fmt:message
											key="biolims.common.leader" /></label>
									<div class="col-sm-9">
										<input type="text" class="form-control" disabled>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 control-label"><fmt:message
											key="biolims.common.projectType" /></label>
									<div class="col-sm-9">
										<input type="text" class="form-control" disabled>
									</div>
								</div>
								<div
									style="height: 4px; background-color: #3c8dbc; margin-bottom: 15px;"></div>
								<div class="form-group row">
									<label class="col-sm-3 control-label"><fmt:message
											key="biolims.common.department" /></label>
									<div class="col-sm-9">
										<input type="text" class="form-control" disabled>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 control-label"><fmt:message
											key="biolims.common.sponsor" /></label>
									<div class="col-sm-9">
										<input type="text" class="form-control" disabled>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 control-label"><fmt:message
											key="biolims.common.commandTime" /></label>
									<div class="col-sm-9">
										<input type="text" class="form-control" disabled>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 control-label"><fmt:message
											key="biolims.common.actualDeliveryDate" /></label>
									<div class="col-sm-9">
										<input type="text" class="form-control" disabled>
									</div>
								</div>
								<div>
									<button class="btn btn-sm btn-info pull-right">
										<a href="####"
											src="${ctx}/sample/sampleReceive/editSampleReceive.action?type=1"
											target="iframename" id="projectAccept"><fmt:message
												key="biolims.dashboard.projectReceipt" /></a>
									</button>
								</div>
							</div>

						</div>
						<!-- /.box -->
					</div>

				</div>
				<!--产品主数据   -->
				<div class="row" id="productMain">
					<div class="col-xs-6">
						<div class="box box-warning">
							<div class="box-header">
								<h3 class="box-title">
									<fmt:message key="biolims.dashboard.productData" />
								</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
									<button type="button" class="btn btn-box-tool"
										data-widget="remove">
										<i class="fa fa-times"></i>
									</button>
								</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body proData1">
								<ul class="parentWrap1">

								</ul>
							</div>
						</div>
					</div>
					<div class="col-xs-5">
						<div class="box box-danger">
							<div class="box-header">
								<i class="fa fa-bookmark"></i>
								<h3 class="box-title">
									<fmt:message key="biolims.dashboard.productInfo" />
								</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
									<button type="button" class="btn btn-box-tool"
										data-widget="remove">
										<i class="fa fa-times"></i>
									</button>
								</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body" id="mainInfo1">
								<div class="form-group row">
									<label class="col-sm-3 control-label"><fmt:message
											key="biolims.common.serialNumber" /></label>
									<div class="col-sm-9">
										<input type="text" class="form-control" disabled>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 control-label"><fmt:message
											key="biolims.common.leader" /></label>
									<div class="col-sm-9">
										<input type="text" class="form-control" disabled>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 control-label"><fmt:message
											key="biolims.common.describe" /></label>
									<div class="col-sm-9">
										<input type="text" class="form-control" disabled>
									</div>
								</div>
								<div
									style="height: 4px; background-color: red; margin-bottom: 15px;"></div>
								<div class="form-group row">
									<label class="col-sm-3 control-label"><fmt:message
											key="biolims.common.detectionMethod" /></label>
									<div class="col-sm-9">
										<input type="text" class="form-control" disabled>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 control-label"><fmt:message
											key="biolims.common.testingCycle" /></label>
									<div class="col-sm-9">
										<input type="text" class="form-control" disabled>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 control-label"><fmt:message
											key="biolims.common.identificationCode" /></label>
									<div class="col-sm-9">
										<input type="text" class="form-control" disabled>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 control-label"><fmt:message
											key="biolims.common.parent" /></label>
									<div class="col-sm-9">
										<input type="text" class="form-control" disabled>
									</div>
								</div>
								<div>
									<button class="btn btn-sm btn-danger pull-right">
										<a href="####"
											src="${ctx}/sample/sampleReceive/editSampleReceive.action?type=3"
											target="iframename" id="productAccept"><fmt:message
												key="biolims.dashboard.productReceipt" /></a>
									</button>
								</div>
							</div>

						</div>
						<!-- /.box -->
					</div>

				</div>
				<!--柱状图与饼图-->
				<div class="row" id="eCharts">
					<div class="col-xs-6">
						<div class="box box-warning">
							<div class="box-header">
								<h3 class="box-title">
									<fmt:message key="biolims.dashboard.orderQuantity" />
								</h3>
								<div class="input-group pull-right">
									<span class="input-group-btn">
										<button class="btn btn-info btn-xs" type="button" id="addYear">+</button>
									</span> <input type="text" class="form-control" value="2017"
										id="currentYear" disabled> <span
										class="input-group-btn">
										<button class="btn btn-info btn-xs" type="button" id="subYear">-</button>
									</span>
								</div>
								<!-- /input-group -->
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<div id="main" style="height: 400px;"></div>
							</div>
						</div>
					</div>
					<div class="col-xs-6">
						<div class="box box-warning">
							<div class="box-header">
								<h3 class="box-title">
									<fmt:message key="biolims.common.productName"></fmt:message>
								</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
									<button type="button" class="btn btn-box-tool"
										data-widget="remove">
										<i class="fa fa-times"></i>
									</button>
								</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<div id="main1" style="height: 400px;"></div>
							</div>
						</div>
					</div>
				</div>
				<!--饼图查询状态-->
				<div class="row" id="eChartsNew">
					<div class="col-xs-6">
						<div class="box box-warning">
							<div class="box-header">
								<h3 class="box-title">生产状况</h3>
								<div class="input-group pull-right">
									<span class="input-group-btn">
										<button class="btn btn-info btn-xs" type="button"
											id="addYearNew">+</button>
									</span> <input type="text" class="form-control" value="2019"
										id="currentYearNew" disabled> <span
										class="input-group-btn">
										<button class="btn btn-info btn-xs" type="button"
											id="subYearNew">-</button>
									</span>
								</div>
								<!-- <div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool"
										data-widget="collapse">
										<i class="fa fa-minus"></i>
									</button>
									<button type="button" class="btn btn-box-tool"
										data-widget="remove">
										<i class="fa fa-times"></i>
									</button>
								</div> -->
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<div id="main3" style="height: 400px;"></div>
								<div style="height: 50px;">
									<div class="col-xs-6">
										<a id="xbzsl" class="aii" href="javascript:;">细胞总数量 </a>
									</div>
									<div class="col-xs-6">
										<a id="zpysl" class="aii" href="javascript:;">在培养数量 </a>
									</div>
									<div class="col-xs-6">
										<a id="ywcxbsl" class="aii" href="javascript:;">已完成细胞数量 </a>
									</div>
									<div class="col-xs-6">
										<a id="wtxbsl" class="aii" href="javascript:;">问题细胞数量 </a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- /.content -->
		</div>
	</div>
	<!-- ./wrapper -->

	<script src="${ctx}/lims/plugins/jQuery/jquery-2.2.3.min.js "></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="${ctx}/lims/bootstrap/js/bootstrap.min.js "></script>
	<script type="text/javascript"
		src="${ctx}/lims/plugins/datatables/datatables.min.js"></script>
	<script type="text/javascript"
		src="${ctx}/lims/plugins/icheck/icheck.min.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/common/dataTablesExtend.js"></script>
	<!--<script src="${ctx}/lims/dist/js/app.min.js "></script>-->
	<!-- AdminLTE for demo purposes -->
	<script src="${ctx}/lims/plugins/template/template.js"
		type="text/javascript" charset="utf-8"></script>
	<script src="${ctx}/lims/plugins/moment/moment.js"
		type="text/javascript" charset="utf-8"></script>
	<script src="${ctx}/lims/plugins/chartjs/echarts.js"
		type="text/javascript" charset="utf-8"></script>
	<script type="text/JavaScript"
		src="${ctx}/js/language/ext-lang-<%=session.getAttribute("lan")%>.js"></script>

	<script type="text/javascript"
		src="${ctx}/js/experiment/homepage/showAllProdution.js"></script>
	<script src="${ctx}/lims/js/dashboard.js" type="text/javascript"></script>
	<script src="${ctx}/lims/dist/js/app.js "></script>
	<script type="text/html" id="wait">
			{{each data}}
			<tr>
				<td> <i class="fa fa-tv"></i></td>
				<td>{{$value.name}}</td>
				<td>{{$value.sampleCode}}</td>
				<td>{{$value.slideCode}}</td>
			</tr>
			{{/each}}
		</script>
	<script type="text/html" id="yujing1">
			{{each}}
			<tr>
				<td> <i class="fa fa-hand-rock-o"></i>
				</td>
				<td class="hide">{{$value.id}}</td>
				<td></td>
				<td></td>
				<td>{{$value.storage.name}}</td>
				<td class="startData1">{{$value.storage.createDate}}</td>
				<td>{{$value.storage.name}}到失效日期</td>
			</tr>
			{{/each}}
		</script>
	<script type="text/html" id="yujing2">
			{{each}}
			<tr>
				<td> <i class="fa fa-hand-rock-o"></i>
				</td>
				<td class="hide">{{$value.id}}</td>
				<td></td>
				<td></td>
				<td>{{$value.name}}</td>
				<td class="startData1">{{$value.createDate}}</td>
				<td>{{$value.name}}安全数量小于库存数量</td>
			</tr>
			{{/each}}
		</script>
	<script type="text/html" id="yujing3">
			{{each}}
			<tr>
				<td> <i class="fa fa-hand-rock-o"></i>
				</td>
				<td class="hide">{{$value.id}}</td>
				<td>{{$value.instrument.id}}</td>
				<td>{{$value.instrument.name}}</td>
				<td class="startData1">{{$value.instrument.tsDate}}</td>
				<td>{{$value.note}}过期</td>
			</tr>
			{{/each}}
		</script>
	<script type="text/html" id="template">
			{{each data}}
			<tr>
				<td> <i class="fa fa-tv"></i></td>
				<td>{{$value.taskId}}</td>
				<td>{{$value.formId}}</td>
				<td>{{$value.formName}}</td>
				<td>{{$value.title}}</td>
				<td>{{$value.taskName}}</td>
				<td>{{$value.applUserName}}</td>
				<td class="startData">{{$value.startDate}}</td>
			</tr>
			{{/each}}
		</script>
	<script type="text/html" id="template1">
			{{each data}}
			<tr>
				<td> <i class="fa fa-hand-rock-o"></i>
				</td>
				<td>{{$value.id}}</td>
				<td>{{$value.contentId}}</td>
				<td>{{$value.tableId}}</td>
				<td>{{$value.title }}</td>
				<td class="startData1">{{$value.startDate}}</td>
				<td>{{$value.content}}</td>
			</tr>
			{{/each}}
		</script>
	<script type="text/html" id="template2">
			{{each}}
			<tr hrf={{$value.taskUrl}}>
				<td> <i class="fa  fa-hand-rock-o"></i>
				</td>
				<td>{{$value.taskId}}</td>
				<td>{{$value.taskName}}</td>
				<td>{{$value.taskNode}}</td>
				<td>{{$value.taskUser }}</td>
				<td>{{$value.createDate}}</td>
				<td >{{$value.taskNum}}</td>
			</tr>
			{{/each}}
		</script>
	<script type="text/html" id="projectTem">
			{{each data}}
			<li class="menuGroup">
				<button class="groupTitle">{{$value.name}}<span class="label label-default"><i class="fa fa-industry"></i></span></button>
				<div class="groupMenu">
					<div class="pull-left proNum">
						<h5><span class="label label-default pNum">{{$value.sampleCodeNum}}</span></h5>
					</div>
					<div class="pull-left proMan">
						<p class="planStartDate">{{$value.planStartDate}}</p>
						{{if $value.dutyUser}}
							<p>{{$value.dutyUser.name}}</p>
						{{else}}
							<p></p>
   						{{/if}}
					</div>
				</div>
			</li>
			{{/each}}
		</script>
	<script type="text/html" id="productTem">
			{{each data}}
			<li class="menuGroup1">
				<button class="groupTitle1">{{$value.name}}<span class="label label-default"><i class="fa fa-industry"></i></span></button>
				<div class="groupMenu1">
					<div class="pull-left proNum1">
						<h5><span class="label label-default pNum1">{{$value.sampleCodeNum}}</span></h5>
					</div>
					<div class="pull-left proMan1">
						<p class="createDate">{{$value.createDate}}</p>
						{{if $value.createUser}}
							<p>{{$value.createUser.name}}</p>
						{{else}}
							<p></p>
   						{{/if}}
						
					</div>
				</div>
			</li>
			{{/each}}
		</script>



	<script type="text/html" id="pcList10">
			{{each data}}
			<tr>
				<td> <i class="fa fa-tv"></i></td>
				<td>{{$value.name}}</td>
				<td>{{$value.orderCode}}</td>
				<td>{{$value.note}}</td>
			</tr>
			{{/each}}
		</script>
	<script type="text/html" id="pcList20">
			{{each data}}
			<tr>
				<td> <i class="fa fa-tv"></i></td>
				<td>{{$value.name}}</td>
				<td>{{$value.materielId}}</td>
				<td>{{$value.materielName}}</td>
				<td>{{$value.batchNumber}}</td>
				<td>{{$value.note}}</td>
			</tr>
			{{/each}}
		</script>
	<script type="text/html" id="pcList30">
			{{each data}}
			<tr>
				<td> <i class="fa fa-tv"></i></td>
				<td>{{$value.name}}</td>
				<td>{{$value.equipmentId}}</td>
				<td>{{$value.equipmentName}}</td>
				<td>{{$value.note}}</td>
			</tr>
			{{/each}}
		</script>
</body>

</html>