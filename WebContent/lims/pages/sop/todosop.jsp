<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Biolims | 样本流程控制</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!--icon-->
		<link rel="shortcut icon" href="${ctx}/lims/img/favicon.ico" />
		<!-- Bootstrap 3.3.6 -->
		<link rel="stylesheet" href="${ctx}/lims/bootstrap/css/bootstrap.min.css">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="${ctx}/lims/bootstrap/css/bootstrap.min.css">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="${ctx}/lims/dist/css/font-awesome.min.css">
		<link rel="stylesheet" href="${ctx}/lims/plugins/datatables/dataTables.bootstrap.css">
		<!-- Theme style -->
		<link rel="stylesheet" href="${ctx}/lims/dist/css/AdminLTE.min.css">
		<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
		<link rel="stylesheet" href="${ctx}/lims/dist/css/skins/_all-skins.min.css">
		<link rel="stylesheet" type="text/css" href="${ctx}/lims/css/todosop.css" />
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
	</head>

	<body class="hold-transition">
		<div class="wrapper">

			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper" id="content1">
				<!-- Content Header (Page header) -->
				<section class="content-header">
					<h1>
        				样本控制
        				<small>已选取样本，请点击下一步</small>
      				</h1>
					<ol class="breadcrumb">
						<li>
							<a href="#"><i class="fa fa-dashboard"></i> Home</a>
						</li>
						<li>
							<a href="#">实验中心</a>
						</li>
						<li class="active">核酸提取</li>
					</ol>
				</section>
				<!-- Main content -->
				<section class="content">
					<!--//跟随鼠标的小遮罩层-->
					<div id="mask"></div>
					<div class="row">
						<div class="col-md-12">
							<div class="box box-success">
								<div class="box-header">
									<h3 class="box-title">样本中心</h3>
								</div>
								<div class="box-body table-responsive">
									<table class="table table-hover">
										<thead>
											<tr class="info">
												<th><input type="checkbox" class="check-all"></th>
												<th>样本编号</th>
												<th>原始样本编号</th>
												<th>项目名称 </th>
												<th>样本数量</th>
												<th>样本 类型</th>
												<th>产物数量</th>
											</tr>
										</thead>
										<tbody id="sample-body">

										</tbody>
									</table>
								</div>
								<div class="box-footer">
									<div class="box box-danger text-center">
										<div class="box-header">
											<h3 class="box-title "><strong>SOP</strong></h3>
										</div>
										<div class="box-body" id="sop-body">
											<button class="btn btn-primary btn-block btn-lg disabled">DNA-SOP</button>
										</div>
										<!-- /.box-body -->
										<div class="box-footer">
											<button class="btn btn-danger pull-right" id="nextbtn1">下一步</button>
										</div>
									</div>
								</div>
							</div>
							<!-- /.box -->
						</div>
					</div>
					<!-- /.row -->

				</section>
				<!-- /.content -->
			</div>
			<!-- /.content1-wrapper -->
			<div class="content-wrapper" id="content2">
				<!-- Content Header (Page header) -->
				<section class="content-header">
					<h1>
        样本控制
        <small>样本已拖拽至96孔板，请点击下一步</small>
      </h1>
					<ol class="breadcrumb">
						<li>
							<a href="#"><i class="fa fa-dashboard"></i> Home</a>
						</li>
						<li>
							<a href="#">实验中心</a>
						</li>
						<li class="active">核酸提取</li>
					</ol>
				</section>

				<!-- Main content -->
				<section class="content">
					<!--//跟随鼠标的小遮罩层-->
					<div id="mask2"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="box box-success">
								<div class="box-header">
									<h3 class="box-title"><strong>样本中心</strong></h3>
								</div>
								<div class="box-body table-responsive" id="table-box">
									<table id="table-self" class="table table-condensed">
										<thead>
											<tr class="danger">
												<th><input type="checkbox" class="check-all"></th>
												<th>样本编号</th>
												<th>原始样本编号</th>
												<th>项目名称 </th>
												<th>样本数量</th>
												<th>样本 类型</th>
												<th>产物数量</th>
											</tr>
										</thead>
										<tbody id="grid-body">

										</tbody>
									</table>
								</div>
								<!-- /.box-body -->
							</div>
							<!-- /.box -->
						</div>
						<!-- /.col (left) -->
						<div class="col-md-6">

							<div class="box box-danger text-center">
								<div class="box-header">
									<h3 class="box-title ">
              	<strong>96孔板</strong>
              </h3>
								</div>
								<div class="box-body">
									<table class="table" id="grid">
										<tr id="tr0">
											<td>

											</td>
											<td>
												1
											</td>
											<td>
												2
											</td>
											<td>
												3
											</td>
											<td>
												4
											</td>
											<td>
												5
											</td>
											<td>
												6
											</td>
											<td>
												7
											</td>
											<td>
												8
											</td>
											<td>
												9
											</td>
											<td>
												10
											</td>
											<td>
												11
											</td>
											<td>
												12
											</td>
										</tr>
										<tr id="tr1">
											<td id="td0">
												A
											</td>
											<td class="td1">
												<div z="1" x="A" y="1"></div>
											</td>
											<td class="td2">
												<div z="9" x="A" y="2"></div>
											</td>
											<td class="td3">
												<div z="17" x="A" y="3"></div>
											</td>
											<td class="td4">
												<div z="25" x="A" y="4"></div>
											</td>
											<td class="td5">
												<div z="33" x="A" y="5"></div>
											</td>
											<td class="td6">
												<div z="41" x="A" y="6"></div>
											</td>
											<td class="td7">
												<div z="49" x="A" y="7"></div>
											</td>
											<td class="td8">
												<div z="57" x="A" y="8"></div>
											</td>
											<td class="td9">
												<div z="65" x="A" y="9"></div>
											</td>
											<td class="td10">
												<div z="73" x="A" y="10"></div>
											</td>
											<td class="td11">
												<div z="81" x="A" y="11"></div>
											</td>
											<td class="td12">
												<div z="89" x="A" y="12"></div>
											</td>
										</tr>
										<tr id="tr2">
											<td id="td0">
												B
											</td>
											<td class="td1">
												<div z="2" x="B" y="1"></div>
											</td>
											<td class="td2">
												<div z="10" x="B" y="2"></div>
											</td>
											<td class="td3">
												<div z="18" x="B" y="3"></div>
											</td>
											<td class="td4">
												<div z="26" x="B" y="4"></div>
											</td>
											<td class="td5">
												<div z="34" x="B" y="5"></div>
											</td>
											<td class="td6">
												<div z="42" x="B" y="6"></div>
											</td>
											<td class="td7">
												<div z="50" x="B" y="7"></div>
											</td>
											<td class="td8">
												<div z="58" x="B" y="8"></div>
											</td>
											<td class="td9">
												<div z="66" x="B" y="9"></div>
											</td>
											<td class="td10">
												<div z="74" x="B" y="10"></div>
											</td>
											<td class="td11">
												<div z="82" x="B" y="11"></div>
											</td>
											<td class="td12">
												<div z="90" x="B" y="12"></div>
											</td>
										</tr>
										<tr id="tr3">
											<td id="td0">
												C
											</td>
											<td class="td1">
												<div z="3" x="C" y="1"></div>
											</td>
											<td class="td2">
												<div z="11" x="C" y="2"></div>
											</td>
											<td class="td3">
												<div z="19" x="C" y="3"></div>
											</td>
											<td class="td4">
												<div z="27" x="C" y="4"></div>
											</td>
											<td class="td5">
												<div z="35" x="C" y="5"></div>
											</td>
											<td class="td6">
												<div z="43" x="C" y="6"></div>
											</td>
											<td class="td7">
												<div z="51" x="C" y="7"></div>
											</td>
											<td class="td8">
												<div z="59" x="C" y="8"></div>
											</td>
											<td class="td9">
												<div z="67" x="C" y="9"></div>
											</td>
											<td class="td10">
												<div z="75" x="C" y="10"></div>
											</td>
											<td class="td11">
												<div z="83" x="C" y="11"></div>
											</td>
											<td class="td12">
												<div z="91" x="C" y="12"></div>
											</td>
										</tr>
										<tr id="tr4">
											<td id="td0">
												D
											</td>
											<td class="td1">
												<div z="4" x="D" y="1"></div>
											</td>
											<td class="td2">
												<div z="12" x="D" y="2"></div>
											</td>
											<td class="td3">
												<div z="20" x="D" y="3"></div>
											</td>
											<td class="td4">
												<div z="28" x="D" y="4"></div>
											</td>
											<td class="td5">
												<div z="36" x="D" y="5"></div>
											</td>
											<td class="td6">
												<div z="44" x="D" y="6"></div>
											</td>
											<td class="td7">
												<div z="52" x="D" y="7"></div>
											</td>
											<td class="td8">
												<div z="60" x="D" y="8"></div>
											</td>
											<td class="td9">
												<div z="68" x="D" y="9"></div>
											</td>
											<td class="td10">
												<div z="76" x="D" y="10"></div>
											</td>
											<td class="td11">
												<div z="84" x="D" y="11"></div>
											</td>
											<td class="td12">
												<div z="92" x="D" y="12"></div>
											</td>
										</tr>
										<tr id="tr5">
											<td id="td0">
												E
											</td>
											<td class="td1">
												<div z="5" x="E" y="1"></div>
											</td>
											<td class="td2">
												<div z="13" x="E" y="2"></div>
											</td>
											<td class="td3">
												<div z="21" x="E" y="3"></div>
											</td>
											<td class="td4">
												<div z="29" x="E" y="4"></div>
											</td>
											<td class="td5">
												<div z="37" x="E" y="5"></div>
											</td>
											<td class="td6">
												<div z="45" x="E" y="6"></div>
											</td>
											<td class="td7">
												<div z="53" x="E" y="7"></div>
											</td>
											<td class="td8">
												<div z="61" x="E" y="8"></div>
											</td>
											<td class="td9">
												<div z="69" x="E" y="9"></div>
											</td>
											<td class="td10">
												<div z="77" x="E" y="10"></div>
											</td>
											<td class="td11">
												<div z="85" x="E" y="11"></div>
											</td>
											<td class="td12">
												<div z="93" x="E" y="12"></div>
											</td>
										</tr>
										<tr id="tr6">
											<td id="td0">
												F
											</td>
											<td class="td1">
												<div z="6" x="F" y="1"></div>
											</td>
											<td class="td2">
												<div z="14" x="F" y="2"></div>
											</td>
											<td class="td3">
												<div z="22" x="F" y="3"></div>
											</td>
											<td class="td4">
												<div z="30" x="F" y="4"></div>
											</td>
											<td class="td5">
												<div z="38" x="F" y="5"></div>
											</td>
											<td class="td6">
												<div z="46" x="F" y="6"></div>
											</td>
											<td class="td7">
												<div z="54" x="F" y="7"></div>
											</td>
											<td class="td8">
												<div z="62" x="F" y="8"></div>
											</td>
											<td class="td9">
												<div z="70" x="F" y="9"></div>
											</td>
											<td class="td10">
												<div z="78" x="F" y="10"></div>
											</td>
											<td class="td11">
												<div z="86" x="F" y="11"></div>
											</td>
											<td class="td12">
												<div z="94" x="F" y="12"></div>
											</td>
										</tr>
										<tr id="tr7">
											<td id="td0">
												G
											</td>
											<td class="td1">
												<div z="7" x="G" y="1"></div>
											</td>
											<td class="td2">
												<div z="15" x="G" y="2"></div>
											</td>
											<td class="td3">
												<div z="23" x="G" y="3"></div>
											</td>
											<td class="td4">
												<div z="31" x="G" y="4"></div>
											</td>
											<td class="td5">
												<div z="39" x="G" y="5"></div>
											</td>
											<td class="td6">
												<div z="47" x="G" y="6"></div>
											</td>
											<td class="td7">
												<div z="55" x="G" y="7"></div>
											</td>
											<td class="td8">
												<div z="63" x="G" y="8"></div>
											</td>
											<td class="td9">
												<div z="71" x="G" y="9"></div>
											</td>
											<td class="td10">
												<div z="79" x="G" y="10"></div>
											</td>
											<td class="td11">
												<div z="87" x="G" y="11"></div>
											</td>
											<td class="td12">
												<div z="95" x="G" y="12"></div>
											</td>
										</tr>
										<tr id="tr8">
											<td id="td0">
												H
											</td>
											<td class="td1">
												<div z="8" x="H" y="1"></div>
											</td>
											<td class="td2">
												<div z="16" x="H" y="2"></div>
											</td>
											<td class="td3">
												<div z="24" x="H" y="3"></div>
											</td>
											<td class="td4">
												<div z="32" x="H" y="4"> </div>
											</td>
											<td class="td5">
												<div z="40" x="H" y="5"></div>
											</td>
											<td class="td6">
												<div z="48" x="H" y="6"></div>
											</td>
											<td class="td7">
												<div z="56" x="H" y="7"></div>
											</td>
											<td class="td8">
												<div z="64" x="H" y="8"></div>
											</td>
											<td class="td9">
												<div z="72" x="H" y="9"></div>
											</td>
											<td class="td10">
												<div z="80" x="H" y="10"></div>
											</td>
											<td class="td11">
												<div z="88" x="H" y="11"></div>
											</td>
											<td class="td12">
												<div z="96" x="H" y="12"></div>
											</td>
										</tr>
									</table>
								</div>
								<!-- /.box-body -->
							</div>
							<!-- /.box -->
						</div>
						<!-- /.col (right) -->
					</div>
					<!-- /.row -->
					<div class="row">
						<div class="col-md-12">
							<div class="box box-success">
								<div class="box-header">
									<h3 class="box-title"><strong>样本中心</strong></h3>
									<ul class="pull-right">
										<li class="btn btn-xs btn-danger" id="nextbtn2">保存并下一步</li>
										<li class="btn btn-xs btn-danger" id="prev2">上一步</li>
										<li class="btn btn-xs btn-danger" id="rmv-sam">清除</li>
									</ul>
								</div>
								<div class="box-body table-responsive">
									<table class="table table-bordered">
										<thead>
											<tr class="success">
												<th>坐标</th>
												<th>样本编号</th>
												<th>原始样本编号</th>
												<th>项目名称 </th>
												<th>样本数量</th>
												<th>样本类型</th>
												<th>产物数量</th>
											</tr>
										</thead>
										<tbody id="result-body">

										</tbody>
									</table>
								</div>
								<!-- /.box-body -->
							</div>
							<!-- /.box -->
						</div>
					</div>
				</section>
				<!-- /.content -->
			</div>
			<!-- /.content2-wrapper -->
			<div class="content-wrapper" id="content3">
				<!-- Content Header (Page header) -->
				<section class="content-header">
					<h1>
        	实验步骤操作
        <small>查看实验项目明细</small>
      </h1>
					<ol class="breadcrumb">
						<li>
							<a href="#"><i class="fa fa-dashboard"></i> Home</a>
						</li>
						<li>
							<a href="#">实验中心</a>
						</li>
						<li class="active">核酸提取</li>
					</ol>
				</section>

				<!-- Main content -->
				<section class="content">
					<div class="row">
						<div class="col-md-12">
							<div class="box box-success">
								<div class="box-header" style="
    padding-bottom: 0px;
">
									<ul class="pull-right">
										<li class="btn btn-xs btn-danger" id="nextbtn3">下一步</li>
										<li class="btn btn-xs btn-danger" id="prev3">上一步</li>
									</ul>
								</div>
								<div class="box-body" style="
    padding-top: 0px;
">
									<div class="row" id="stepbg">
										<div class="col-xs-2 pull-left">

											<div class="step-head">
												<table class="table">

												</table>
											</div>
										</div>
										<div class="col-xs-9 pull-right">
											<div class="step-body">
												<div id="tem" class="table-responsive">
													<h4><i class="fa fa-hand-lizard-o"></i>步骤 <button class="btn btn-sm btn-info pull-right savetem">保存</button></h4>
													<table class="table no-border">
														<tr>
															<td> <label><i class="fa fa-user"></i>实验员：<input type="text"/></label></td>
															<td><button class="btn btn-xs btn-success startDate">开始日期</button>：
																<input type="text" /></td>
															<td><button class="btn btn-xs btn-success stopDate">结束日期</button>：
																<input type="text" /></td>
															<td><label>状态：
															<input type="text" /></label></td>
														</tr>
														<tr>
															<td colspan="4" class="steptable" disabled>步骤明细：
																<div>
																</div>
															</td>
														</tr>
													</table>
												</div>
												<div class="fengexian"></div>
												<div id="reagent" class="table-responsive">
													<h4><i class="fa fa-eyedropper"></i>原辅料 <button class="btn btn-xs btn-info pull-right moreRet">增加原辅料</button></h4>

												</div>
												<div class="fengexian"></div>
												<div id="instrument" class="table-responsive">
													<h4><i class="fa  fa-flask"></i>设备<button class="btn btn-xs btn-info pull-right moreIst">增加设备</button></h4>

												</div>
											</div>
										</div>
									</div>
									<!-- /.row -->

								</div>
							</div>
						</div>
					</div>
					<!-- /.row -->
				</section>
				<!-- /.content -->
			</div>
			<!-- /.content3-wrapper -->
			<div class="content-wrapper" id="content4">
				<!-- Content Header (Page header) -->
				<section class="content-header">
					<h1>核酸提取结果</h1>
					<ol class="breadcrumb">
						<li>
							<a href="#"><i class="fa fa-dashboard"></i> Home</a>
						</li>
						<li>
							<a href="#">实验中心</a>
						</li>
						<li class="active">核酸提取</li>
					</ol>
				</section>

				<!-- Main content -->
				<section class="content">
					<div class="row">
						<div class="col-md-12">
							<div class="box box-success">
								<div class="box-header">
									<!-- ul class="tab-guo pull-left">
										<li class="btn btn-info btn-sm">删除选中</li>
										<li class="btn btn-info btn-sm">产物类型</li>
										<li class="btn btn-info btn-sm">批量导入</li>
										<li class="btn btn-info btn-sm">批量结果</li>
										<li class="btn btn-info btn-sm">选择等级</li>
										<li class="btn btn-info btn-sm">种系/变异</li>
										<li class="btn btn-info btn-sm">导出列表</li>
									</ul-->
									<button class="btn btn-xs btn-danger pull-right" id="prev4">上一步 </button> &nbsp;&nbsp;&nbsp;
									<button class="btn btn-xs btn-danger pull-right" id="nextbtn4">保存</button>
								</div>
								<div class="box-body ">
									<div class="table-responsive">
										<table id="table-self" class="table table-condensed">
											<thead>
												<tr class="danger">
													<th><input type="checkbox" class="check-all"></th>
													<th>样本编号</th>
													<th>样本类型</th>
													<th>检测项目</th>
													<th>产物类型</th>
													<th>浓度</th>
													<th>体积</th>
													<th>总量</th>
													<th>OD260</th>
													<th>OD280</th>
													<th>RIN</th>
													<th>样本用量</th>
													<th>总量评级</th>
													<th>质量评级</th>
													<th>结果</th>
													<th>下步流向</th>
													<th>是否提交</th>
													<th>备注</th>
												</tr>
											</thead>
											<tbody class="sample-body" id="save-result">

											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /.row -->

				</section>
				<!-- /.content -->
			</div>

		</div>
		<!-- ./wrapper -->

		<script src="${ctx}/lims/plugins/jQuery/jquery-2.2.3.min.js"></script>
		<!-- Bootstrap 3.3.6 -->
		<script src="${ctx}/lims/bootstrap/js/bootstrap.min.js"></script>
		<!-- SlimScroll -->
		<script src="${ctx}/lims/plugins/slimScroll/jquery.slimscroll.min.js"></script>
		<!-- FastClick -->
		<script src="${ctx}/lims/plugins/fastclick/fastclick.js"></script>

		<script src="${ctx}/lims/plugins/daterangepicker/moment.js" type="text/javascript" charset="utf-8"></script>

		<!-- AdminLTE App -->
		<script src="${ctx}/lims/dist/js/app.min.js"></script>
		<!-- AdminLTE for demo purposes -->
		<script src="${ctx}/lims/plugins/template/template.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/html" id="sampleTemplate">
			{{each data}}
			<tr>
				<td><input type="checkbox"></td>
				<td>{{$value.code}}</td>
				<td>{{$value.sampleCode}}</td>
				<td>{{$value.productName}}</td>
				<td>{{$value.sampleNum}}</td>
				<td>{{$value.sampleType}}</td>
				<td>{{$value.productNum}}</td>
			</tr>
			{{/each}}
		</script>
		<script type="text/html" id="gridTemplate">
			{{each data}}
			<tr>
				<td><input type="text" value="" disabled></td>
				<td><input type="text" value={{$value.code}} disabled/></td>
				<td><input type="text" value={{$value.sampleCode}} disabled/></td>
				<td><input type="text" value={{$value.productName}} disabled/></td>
				<td><input type="text" value={{$value.sampleNum}} disabled/></td>
				<td><input type="text" value={{$value.sampleType}} disabled/></td>
				<td><input type="text" value={{$value.productNum}} /></td>
			</tr>
			{{/each}}
		</script>
		<script type="text/html" id="clickGridTemplate">
			{{each gridData}}
			<tr>
				<td><input type="text" value="" disabled></td>
				<td><input type="text" value={{$value.code}} disabled/></td>
				<td><input type="text" value={{$value.sampleCode}} disabled/></td>
				<td><input type="text" value={{$value.productName}} /></td>
				<td><input type="text" value={{$value.sampleNum}} /></td>
				<td><input type="text" value={{$value.sampleType}} /></td>
				<td><input type="text" value={{$value.productNum}} /></td>
			</tr>
			{{/each}}
		</script>
		<script type="text/html" id="template4">
			{{each data}}
			<tr>
				<td><input type="checkbox"></td>
				<td style="display: none;"><input type="text" value={{$value.id}}></td>
				<td><input type="text" value={{$value.code}} disabled></td>
				<td><input type="text" value={{$value.sampleType}} disabled></td>
				<td><input type="text" value={{$value.productName}}></td>
				<td><input type="text" value={{$value.dicSampleType}}></td>
				<td><input type="text" value={{$value.contraction}}></td>
				<td><input type="text" value={{$value.volume}}></td>
				<td><input type="text" value={{$value.sampleNum}}></td>
				<td><input type="text" value={{$value.od260}}></td>
				<td><input type="text" value={{$value.od280}}></td>
				<td><input type="text" value={{$value.rin}}></td>
				<td><input type="text" value={{$value.sampleConsumeV}}></td>
				<td><input type="text" value={{$value.totalGrade}}></td>
				<td><input type="text" value={{$value.zlGrade}}></td>
				<td><input type="text" value={{if $value.result===0 }} 不合格 {{else}} 合格 {{/if}}></td>
				<td><input type="text" value={{$value.nextFlow}}></td>
				<td><input type="text" value={{$value.submit}}></td>
				<td><input type="text" value={{$value.note}}></td>
			</tr>
			{{/each}}
		</script>
		<script src="${ctx}/lims/js/todosop.js" type="text/javascript" charset="utf-8"></script>
	</body>

</html>