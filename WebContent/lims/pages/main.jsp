<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<fmt:setLocale value="${sessionScope.lan}" scope="session" />
		<fmt:setBundle basename="ResouseInternational/msg" />
		<title>Bio-LIMS</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!--icon-->
		<link rel="shortcut icon" href="${ctx}/lims/img/favicon.ico" />
		<!-- Bootstrap 3.3.6 -->
		<link rel="stylesheet" href="${ctx}/lims/bootstrap/css/bootstrap.min.css">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="${ctx}/lims/dist/font-awesome/css/font-awesome.min.css">
		<!-- Ionicons -->
		<link rel="stylesheet" href="${ctx}/lims/dist/Ionicons/css/ionicons.min.css">

		<link rel="stylesheet" href="${ctx}/lims/dist/css/AdminLTE.min.css">
		<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
		<link rel="stylesheet" href="${ctx}/lims/plugins/icheck/skins/square/blue.css">
		<link rel="stylesheet" href="${ctx}/lims/dist/css/skins/_all-skins.min.css">
		<link rel="stylesheet" type="text/css" href="${ctx}/lims/css/main.css" />
		<link rel="stylesheet" href="${ctx}/lims/plugins/layui/css/layui.css">
		<script type="text/javascript" src="${ctx}/lims/plugins/layui/layui.js"></script>
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
			<style>
				@media screen and (min-width:600px) and (max-width:1000px){
	body{
	  max-height: 750px;
  }
  				.navbar-nav>.notifications-menu>.dropdown-menu, .navbar-nav>.messages-menu>.dropdown-menu, .navbar-nav>.tasks-menu>.dropdown-menu {
	  				width:407px !important;
				}
				}
			</style>
	</head>

	<body class="hold-transition skin-blue-light sidebar-mini">
		<div class="wrapper">
			<!-- Main Header -->
			<header class="main-header">

				<!-- Logo -->
				<a href="#" class="logo">
					<!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini">
						<img alt="" src="${ctx}/lims/img/logo.gif" width="50px" height="50px">
					</span>
					<!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><b>Bio-LIMS</b><fmt:message key="biolims.dashboard.company" /></span>
				</a>

				<!-- Header Navbar -->
				<nav class="navbar navbar-static-top" role="navigation">
					<!-- Sidebar toggle button-->
					<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
						<span class="sr-only">Sidebar栏控制按钮</span>
					</a>
					<!-- Navbar Right Menu -->
					<div class="navbar-custom-menu">
						<ul class="nav navbar-nav">
							<!-- 异常提醒-->
							<li class="dropdown messages-menu" id="dangerMenu" style="display:none;">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" id="dangerNum">
									<i class="fa fa-warning"></i>
									<span class="label label-success">0</span>
								</a>
								<ul class="dropdown-menu">
									<li>
										<ul class="menu dangerlist">

										</ul>
									</li>
									<li class="footer">
										<a href="#">
											<fmt:message key="biolims.common.abnormalReminder" />
										</a>
									</li>
								</ul>
							</li>
							<!-- 待办事项提醒-->
							<li class="dropdown messages-menu" id="todoMenu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="fa fa-envelope-o"></i>
									<span class="label label-success todoNum"></span>
								</a>
								<ul class="dropdown-menu">
									<li>
										<ul class="menu mainlist">

										</ul>
									</li>
									<li class="footer">
										<a href="#">
											<fmt:message key="biolims.dashboard.todo" />
										</a>
									</li>
								</ul>
							</li>
							<!-- 信息预警 -->
							<li class="dropdown notifications-menu" id="waringMenu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="fa fa-bell-o"></i>
									<span class="label label-warning warnNum"></span>
								</a>
								<ul class="dropdown-menu" style="width:407px;">
									<li>
										<!-- 系统消息提醒 -->
										<ul class="menu mainlist1" >

										</ul>
									</li>
									<li class="footer">
										<a href="#">
											信息提示
											<%-- <fmt:message key="biolims.dashboard.warning" /> --%>
										</a>
									</li>
								</ul>
							</li>
							<!-- 管理员、注销、修改密码 -->
							<li class="dropdown user user-menu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<span class="hidden-xs mainuser1"></span>
									<input type="hidden" id="userName" value="${userName}">
									<span id="data-now"></span>
								</a>
								<ul class="dropdown-menu">
									<!-- User image -->
									<li class="user-header">
										<img src="${ctx}/lims/img/logo.gif" class="img-circle" alt="User Image">
										<p class="mainuser2"></p>
									</li>
									<li class="user-footer">
										<div class="pull-left">
											<a href="javascript:void(0)" class="btn btn-default btn-flat chagepsd">
												<fmt:message key="biolims.dashboard.changePwd" />
											</a>
										</div>
										<div class="pull-right">
											<a class="btn btn-default btn-flat" id="logout">
												<fmt:message key="biolims.dashboard.logout" />
											</a>
										</div>
									</li>
								</ul>
							</li>
							<!-- 板式设置-->
							<li>
								<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
							</li>
						</ul>
					</div>
				</nav>
			</header>
			<!-- Left side column. contains the logo and sidebar -->
			<div class="main-sidebar">
				<section id="sidebarsection" class="sidebar">
					<!-- search form (Optional) -->
					<div class="sidebar-form" style="display:none;">
						<div class="input-group">
							<input type="text" name="q" class="form-control" placeholder="Search..." id="search-txt">
							<span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
						</div>
					</div>
					<!-- /.search form -->

					<!-- Sidebar Menu -->
					<ul class="sidebar-menu">

						<li class="header">
							<fmt:message key="biolims.dashboard.function" />
						</li>
						<!-- Optionally, you can add icons to the links -->
						<!--主页 -->
						<li class="active">
							<a class='sea' href="#####" src="${ctx}/lims/pages/dashboard/dashboard.jsp"><i class="fa fa-home"></i> <span><fmt:message key="biolims.dashboard.dashboard" /></span></a>
						</li>
					</ul>
					<!-- /.sidebar-menu -->
				</section>
			</div>
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper" style="-webkit-overflow-scrolling:touch; overflow:auto;">
				<iframe src="${ctx}/lims/pages/dashboard/dashboard.jsp" scrolling="auto" width="100%" id="maincontentframe" name="iframename" frameborder="0" onload="this.height=$(document.body).height()-55"></iframe>
			</div>
			<!--修改密码的弹出层-->
			<div class="layer1">
				<div class=" box box-danger mask">
					<div class="box-header ">
						<h3 class="pull-left"><fmt:message key="biolims.dashboard.changePwd" /></h3>
						<button class="btn btn-success btn-sm pull-right rmv-btn1"><i class="fa fa-times"></i></button>
					</div>
					<div class="box-body layer-body">
						<div class="form-group row">
							<label class="col-sm-3 control-label"><fmt:message key="biolims.common.userName" /></label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="user" disabled>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-3 control-label"><fmt:message key="biolims.common.password" /></label>
							<div class="col-sm-9">
								<input type="password" class="form-control" id="psd">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-3 control-label"><fmt:message key="biolims.dashboard.newPwd" /></label>
							<div class="col-sm-9">
								<input type="password" class="form-control" id="newpsd">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-3 control-label"><fmt:message key="biolims.common.ensurePassword" /></label>
							<div class="col-sm-9">
								<input type="password" class="form-control" id="newpsdmore">
							</div>
						</div>
					</div>
					<div class="box-footer">
						<button class="btn btn-success save-btn" style="width: 100%;"><fmt:message key="biolims.common.submit" /></button>
					</div>
				</div>
			</div>

			<!-- Control Sidebar -->
			<aside class="control-sidebar control-sidebar-dark" style="max-height:620px;overflow-y:hidden;">
				<!-- Create the tabs -->
				<ul class="nav nav-tabs nav-justified control-sidebar-tabs">
					<li>
						<a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-heartbeat"></i></a>
					</li>
				</ul>
				<!-- Tab panes -->
				<div class="tab-content">
					<!-- Home tab content -->
					<div class="tab-pane" id="control-sidebar-home-tab">

					</div>

					<div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>

				</div>
			</aside>
			<!-- /.control-sidebar -->
			<!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
			<div class="control-sidebar-bg"></div>
		</div>
		<!-- ./wrapper -->

		<script src="${ctx}/lims/plugins/jQuery/jquery-2.2.3.min.js "></script>
		<!-- Bootstrap 3.3.6 -->
		<script src="${ctx}/lims/bootstrap/js/bootstrap.min.js "></script>
		<!-- SlimScroll -->
		<script src="${ctx}/lims/plugins/moment/moment.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript" src="${ctx}/lims/plugins/datepicker/bootstrap-datepicker.js"></script>
		<script type="text/javascript">
   var screeWidth = document.body.clientWidth;
   if(screeWidth > 768) {
    screeProportion = ["60%", "75%"];
   } else {
    screeProportion = ["95%", "75%"];
   }
   layui.use('layer', function() {
    layer = layui.layer;
    layer.config({
     offset: '30px'
    });
   });
  </script>
		<!-- AdminLTE App -->
		<script src="${ctx}/lims/dist/js/app.min.js "></script>
		<!-- AdminLTE for demo purposes -->
		<script src="${ctx}/lims/dist/js/demo.js "></script>
		<script type="text/JavaScript" src="${ctx}/js/language/ext-lang-<%=session.getAttribute("lan")%>.js"></script>
		<script src="${ctx}/lims/plugins/template/template.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/html" id="templatem">
			{{each data}}
			<li>
				<a class='frmhref' fom={{$value.formId}} tsk={{$value.taskId}} fname={{$value.formName}}>
					<div class="pull-left">
						<i class="fa fa-tv"></i>
					</div>
					<h4>
                       {{$value.title}}
                        <small class="date"><i class="fa fa-clock-o"></i>{{$value.startDate}}</small>
                      </h4>
					<p>{{$value.taskName}}</p>
				</a>
			</li>
			{{/each}}
		</script>
		<script type="text/html" id="templatem11">
			{{each data}}
			<li>
				<a id={{$value.id}} contentId={{$value.contentId}} contentId={{$value.contentId}} tableId={{$value.tableId}} class='frmhref1'>
					<div class="pull-left">
						<i class="fa fa-users"></i>
					</div>
					<h4>
                       {{$value.content}}
                        <small class="date1"><i class="fa fa-clock-o"></i>{{$value.startDate}}</small>
                      </h4>
				</a>
			</li>
			{{/each}}
		</script>

		<script src="${ctx}/lims/js/main.js" type="text/javascript" charset="utf-8"></script>
	</body>

</html>