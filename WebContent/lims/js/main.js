$(function() {
	
	//更新用户名
	var userName = $("#userName").val();
//	var userName = sessionStorage.getItem("userName");
	$(".mainuser1").html(biolims.newDashboard.welcome + userName + "&nbsp;&nbsp;&nbsp;&nbsp");
	$(".mainuser2").text(userName);
	//自动生成tab栏
	var userId = sessionStorage.getItem("userId");
	$.ajax({
		type: "post",
		data: {
			user: userId
		},
		url: ctx + "/main/newTreeShow.action",
		async: true,
		success: function(data) {
			var list = JSON.parse(data);
			var jsonn = list.data;
			//console.log(jsonn);
			var jss = JSON.parse(jsonn);
			updateAside(jss);
			searchTab();
			tabChosed ();
		}
	});
	//导航栏实验异常提醒
	dangerTest();
	setInterval(dangerTest, 60000);
	//导航栏的待办事项更新
	updateTodolist(userId);
	$("#todoMenu").click(updateTodolist);
	//导航栏的信息预警更新
	updateWearList(userId);
	$("#waringMenu").click(updateWearList);
	//修改密码
	changepsd(userName, userId);
	//点击注销清除缓存
	$("#logout").click(function() {
		$.ajax({
			type: "get",
			url: ctx + "/main/newLogOut.action",
			async: true,
			success: function(data) {
				var data = JSON.parse(data);
				if(data.success) {
					window.location.href = window.ctx + "/main/logOut.action";
				}
			}
		});
	});
	setTimeout(function(){
		var sUserAgent = navigator.userAgent.toLowerCase(); 
	   var isIpad = sUserAgent.match(/ipad/i) == "ipad";
	   if(isIpad){
		   $('.sidebar-toggle').click();
		  
	   }
	},1000)
});
// 导航栏试验异常提醒
function dangerTest() {
	$.ajax({
		type: "post",
		data: {
			userId: userId
		},
		url: ctx + "/main/selAbnormal.action",
		success: function(data) {
			var info = JSON.parse(data).data[0];
			var num = 0;
			$(".dangerlist").empty();
			for(var k in info) {
				num += parseInt(info[k]);
				if(parseInt(info[k]) > 0) {
					var li = $('<li text=' + k + '><a href="####"><i class="fa fa-warning text-yellow"></i>  ' + info[k] + ' ' + k + '</a></li>');
					$(".dangerlist").append(li);
					$("#dangerNum").addClass("flicker");
				}
			}
			$("#dangerNum").find("span").text(num);
			var lis = $(".dangerlist").find("li");
			lis.unbind("click").click(function() {
				var name = $(this).attr("text");
				$.ajax({
					type: "post",
					data: {
						name: name,
						type: "ycxx"
					},
					url: ctx + "/main/selUrl.action",
					success: function(data) {
						var src = JSON.parse(data).data;
						$("#maincontentframe", window.parent.document)[0].src = src;
					}
				});
			})
		}
	})
}
//导航栏待办事项更新
function updateTodolist() {
	$.ajax({
		type: "post",
		data: {
			user: userId
		},
		url: ctx + "/main/showListOne.action",
		async: true,
		success: function(data) {
			//更新待办事项的length
			var info = JSON.parse(data)
			$(".todoNum").text(info.data.length);
			//更新待办事项数据
			var result = template("templatem", info);
			$(".mainlist").html(result);
			//将时间戳转换为具体时间
			function add0(m) {
				return m < 10 ? '0' + m : m
			}

			function format(timestamp) {
				var time = new Date(parseInt(timestamp));
				var year = time.getFullYear();
				var month = time.getMonth() + 1;
				var date = time.getDate();
				return year + '-' + add0(month) + '-' + add0(date);
			}
			var infoo = $(".date");
			for(var i = 0; i < infoo.length; i++) {
				infoo[i].innerText = format(infoo[i].innerText);
			}
			//导航栏待办事项点击跳转
			dbclickToNewPage();
			clickToOldPage();
			//单击标题进入待办事项新页面 
			function dbclickToNewPage() {
				$(".frmhref").unbind("click").click(function() {
					var formId = this.getAttribute("fom");
					var taskId = this.getAttribute("tsk");
					var formName = this.getAttribute("fname");
					sessionStorage.removeItem("taskId");
					sessionStorage.removeItem("formId");
					sessionStorage.removeItem("formName");
					sessionStorage.setItem("taskId", taskId);
					sessionStorage.setItem("formId", formId);
					sessionStorage.setItem("formName", formName);
					$.ajax({

						type: "post",
						url: ctx + "/workflow/processinstance/getFormUrl.action",
						data: {
							taskId: taskId,
							formId: formId,
							formName: formName
						},
						datatype: "json",
						success: function(data) {
							info = JSON.parse(data)
							if(info.success) {

								if(info.data.indexOf("id=") > -1) {
									$("#maincontentframe", window.parent.document)[0].src = window.ctx + info.data + formId + "&bpmTaskId=" + taskId;
								} else {

									$("#maincontentframe", window.parent.document)[0].src = window.ctx + info.data;
								}

							}
						}
					
					});

				});
			}
			//单击图标进入老页面
			function clickToOldPage() {
				$(".fa-tv").unbind("click").click(function() {
					var formId = $(this).parents(".frmhref").attr("fom");
					var taskId = $(this).parents(".frmhref").attr("tsk");
					var formName = $(this).parents(".frmhref").attr("fname");
					sessionStorage.setItem("taskId", taskId);
					sessionStorage.setItem("formId", formId);
					sessionStorage.setItem("formName", formName);
					$.ajax({
						type: "post",
						url: ctx + "/workflow/processinstance/getFormUrl.action",
						data: {
							taskId: taskId,
							formId: formId,
							formName: formName
						},
						datatype: "json",
						success: function(data) {
							info = JSON.parse(data)
							if(info.success) {

								if(info.data.indexOf("id=") > -1) {
									$("#maincontentframe", window.parent.document)[0].src = window.ctx + info.data + formId + "&bpmTaskId=" + taskId;
								} else {

									$("#maincontentframe", window.parent.document)[0].src = window.ctx + info.data;
								}

							}
						}
					});
					return false;
				})
			}

		}
	});
}
//导航栏信息预警更新
function updateWearList() {
	$.ajax({
		type: "post",
		data: {
			userId: userId
		},
		url: ctx + "/main/showInformationInfo.action",
		success: function(data) {
			//更新信息预警的长度
			var info = JSON.parse(data);
			$(".warnNum").text(info.data.length);
			//更新信息预警数据
			var result = template("templatem11", info);
			$(".mainlist1").html(result);
			function add0(m) {
				return m < 10 ? '0' + m : m
			}
			function format(timestamp) {
				var time = new Date(parseInt(timestamp));
				var year = time.getFullYear();
				var month = time.getMonth() + 1;
				var date = time.getDate();
				return year + '-' + add0(month) + '-' + add0(date);
			}
			var infoo = $(".date1");
			for(var i = 0; i < infoo.length; i++) {
				infoo[i].innerText = format(infoo[i].innerText);
			}
			$(".frmhref1").unbind("click").click(function() {
				var id = this.getAttribute("id");;
				var contentId = this.getAttribute("contentId");
				var tableId = this.getAttribute("tableId");
				$.ajax({
					type: "post",
					data: {
						id: id,
						contentId: contentId,
						tableId: tableId
					},
					url: ctx + "/main/showInformationItem.action",
					async: true,
					success: function(data) {
						var href = JSON.parse(data).data;
						$("#maincontentframe", window.parent.document)[0].src = href;
					}
				});

			})
		}
	});
}
//tab栏目录更新
function updateAside(list) {
	for(var i = 0, l = list.length; i < l; i++) {
		if(list[i].leaf == false) {
			var mainli = $("<li class='treeview'></li>");
			var txt = list[i].text;
			var font = list[i].qtip;
			var a1 = "<a href='#'><i class='fa  " + font + "'></i><span>" + txt + "</span><span class='pull-right-container'><i class='fa fa-angle-left pull-right'></i></span></a>";
			var ul1 = $("<ul class='treeview-menu'></ul>");
			mainli.append(a1);
			mainli.append(ul1);
			var son = list[i].children;
			for(var j = 0, p = son.length; j < p; j++) {
				if(son[j].leaf == false) {
					var li2 = $("<li> </li>");
					ul1.append(li2);
					var a2 = "<a href='#'><i class='fa fa-folder-open'></i><span>" + son[j].text + "</span><span class='pull-right-container'><i class='fa fa-angle-left pull-right'></i> </span></a>";
					var ul2 = $("<ul class='treeview-menu'></ul>");
					li2.append(a2);
					li2.append(ul2);
					var grandson = son[j].children;
					for(var z = 0, q = grandson.length; z < q; z++) {
						if(grandson[z].leaf == false) {
							var li3 = $("<li> </li>");
							ul2.append(li3);
							var a3 = "<a href='#'><i class='fa fa-folder-open'></i><span>" + grandson[z].text + "</span><span class='pull-right-container'><i class='fa fa-angle-left pull-right'></i></span></a>";
							var ul3 = $("<ul class='treeview-menu'></ul>");
							li3.append(a3);
							li3.append(ul3);
							var grandsonMore = grandson[z].children;
							for(var x = 0; x < grandsonMore.length; x++) {
								var li4 = $("<li></li>");
								var a4 = "<a href='###' src=" + ctx + grandsonMore[x].contenthref + "  class='sea'><i class='fa fa-folder-o'></i><span>" +  grandsonMore[x].text + "</span> </a>";
								li4.append(a4);
								ul3.append(li4);
							}
						} else {
							var li3 = $("<li></li>");
							var a3 = "<a href='###' src=" + ctx + grandson[z].contenthref + "  class='sea'><i class='fa fa-folder-o'></i> <span> "+  grandson[z].text + "</span> </a>";
							li3.append(a3);
							ul2.append(li3);
						}
					}
				} else {
					var li2 = $("<li></li>");
					var a2 = "<a href='###' src=" + ctx + son[j].contenthref + "  class='sea'><i class='fa fa-folder-o'></i> <span> " + son[j].text + " </span></a>";
					li2.append(a2);
					ul1.append(li2);
				}

			}
		} else {
			var mainli = $("<li></li>");
			var a = "<a href='###' src=" + ctx + list[i].contenthref + "  class='sea'><i class='fa fa-folder-o'></i><span>" + list[i].text + "<span></a>";
			mainli.append(a);
		}
		$(".sidebar-menu").append(mainli);
	}
	if(list.length > 5) {
		document.getElementById("sidebarsection").style.maxHeight = document.body.clientHeight * 0.9;
		document.getElementById("sidebarsection").style.overflowY = "auto";
	}
	$(".sidebar-toggle").click(function() {
		if($("body").hasClass("sidebar-collapse")) {
			document.getElementById("sidebarsection").style.overflowY = "auto";
		} else {
			document.getElementById("sidebarsection").style.overflowY = "visible";
		}
	})
}
//修改密码
function changepsd(admin, userId) {
	$(".rmv-btn1").click(function() {
		$(".layer1").hide();
	});
	$(".chagepsd").click(function() {
		$(".layer1").show();
		$("#user").val(admin);
		$(".save-btn").click(function() {
			var user = $("#user").val();
			var pwd = $("#psd").val();
			var newpwd = $("#newpsd").val();
			var pwdmore = $("#newpsdmore").val();
			if(newpwd !== pwdmore) {
				$(".mask1").text(biolims.main.password2).fadeIn(1000).fadeOut(1000);
				return;
			}
			$.ajax({
				type: "post",
				data: {
					userId: userId,
					oldPwd: pwd,
					newPwd: newpwd
				},
				url: ctx + "/main/updPwd.action",
				async: true,
				success: function(data) {
					var obj = JSON.parse(data);
					if(obj.data == 0) {
						$(".mask1").text(biolims.main.diffrent).fadeIn(1000).fadeOut(1000);
					} else if(obj.data == 1) {
						$(".mask1").text(biolims.main.success).fadeIn(300).fadeOut(500);
						$(".layer1").hide(1000);
					}
				}
			});
		});
	});
}
//点击搜索tab栏项目
function searchTab() {
	$("#search-btn").click(function() {
		var txt = $("#search-txt").val();
		var sea = $(".sea");
		for(var i = 0; i < sea.length; i++) {
			var seatxt = sea[i].innerText.trim();
			if(seatxt === txt) {
				$(sea[i]).parents("li").addClass("active");
			}
		}
	});
}
function tabChosed () {
	$(".sea").click(function () {
		$(".sea").removeClass("tabChosed");
		$(this).addClass("tabChosed");
		$("#maincontentframe").attr("src",this.getAttribute("src"));
		sessionStorage.removeItem("searchContent");
	});
}