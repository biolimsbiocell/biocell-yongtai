$(function() {
//	$('#pici').bind('keydown', function (event) {
//		var event = window.event || arguments.callee.caller.arguments[0];
//		if (event.keyCode == 13){
//			setTimeout(function() {
//				//扫码回车
//				//判断该批号是否是登陆人的任务
//				var pici = $('#pici').val();
//				$.ajax({
//					type: "post",
//					url:ctx+"/main/checkProject.action",
//					data:{
//						pici:pici,
//					},
//					success: function(dataBack) {
//						var data = JSON.parse(dataBack);
//						if(data.success){
//							if(data.checkYes){
//								$("#maincontentframe", window.parent.document)[0].src = window.ctx + 
//								"/experiment/cell/passage/cellPassage/showCellPassageSteps.action?id=" + data.id + "&orderNum=" + data.ordernum;
//							}else{
//								top.layer.msg("该任务不是您的任务，请核查！");
//							}
//						}else{
//							top.layer.msg("查询任务失败！");
//						}
//					}
//				});
//				//直接进入该批号登陆人任务的第一个任务
//			},500);
//		}
//	})
	$('#fjbh').bind('keydown', function (event) {
		var event = window.event || arguments.callee.caller.arguments[0];
		if (event.keyCode == 13){
			var rows = $("#allProduction .selected");
			var length = rows.length;
			if (!length) {
				top.layer.msg("请选择一条待办数据！");
				$('#fjbh').val("");
				return false;
			}else{
				setTimeout(function() {
					var operatingRoomId = rows.find("td[savename='operatingRoomName']").attr("operatingRoomId");
					if(operatingRoomId==$('#fjbh').val()){
						//扫码回车
						//判断该批号是否是登陆人的任务
						var fjbh = $('#fjbh').val();
						$.ajax({
							type: "post",
							url:ctx+"/main/checkProjectRoom.action",
							data:{
								fjbh:fjbh,
							},
							success: function(dataBack) {
								var data = JSON.parse(dataBack);
								if(data.success){
									if(data.roomnull){
										if(data.roomstate){
											if(data.checkYes){
//												$("#maincontentframe", window.parent.document)[0].src = window.ctx + 
//												"/experiment/cell/passage/cellPassage/showCellPassageSteps.action?id=" + data.id + "&orderNum=" + data.ordernum;
												$("#maincontentframe", window.parent.document)[0].src = window.ctx + 
												"/experiment/cell/passage/cellPassage/showCellPassageSteps.action?id=" + rows.find("td[savename='cellPassage-batch']").attr("cellPassage-id") + "&orderNum=" + rows.find("td[savename='cellPassage-batch']").attr("orderNum");
											}else{
												top.layer.msg("该任务不是您的任务，请核查！");
												$('#fjbh').val("");
												return false;
											}
										}else{
											top.layer.msg($('#fjbh').val()+"房间不合格，请核查！");
											$('#fjbh').val("");
											return false;
										}
									}else{
										top.layer.msg("未查到该房间，请确认！");
										$('#fjbh').val("");
										return false;
									}
								}else{
									top.layer.msg("查询任务失败！");
									$('#fjbh').val("");
									return false;
								}
							}
						});
						//直接进入该批号登陆人任务的第一个任务
					}else{
						top.layer.msg("选择待办数据房间与扫码房间不一致，请核查！");
						$('#fjbh').val("");
						return false;
					}
					
				},500);
				
			}
		}
	})
	
	var userId = sessionStorage.getItem("userId");
	//根据权限 显示模块
	$("#toDoList").hide();
	$("#warnInfo").hide();
	$("#toDoNum").hide();
	$("#projectMain").hide();
	$("#productMain").hide();
	$("#eCharts").hide();
	$("#toDoPro").hide();
	$("#dangerMenu",window.parent.document).hide();
	$("#scdb").hide();
	$("#pcInfo").hide();
	$("#pcInfo1").hide();
	$("#pcInfo2").hide();
	$("#shsj1").hide();
	$("#hsjh").hide();
	$("#eChartsNew").hide();
	$("#ggl").hide();
	//订单到期
	$("#ordersDue").hide();
	//血样接收未完成
	$("#notSampleReceive").hide();
	//回输日期已出等待确认
	$("#returnDateConfirm").hide();
	//未进入运输计划的回输或订单
	$("#noTransportPlan").hide();
	//运 输 计 划 完 成 （运输计划后面项目不完整)
	$("#transportPlanBug").hide();
	$("#deliveryWindow").hide();
	$("#resultAll").hide();

	$("#zhiJianSurplus").hide();

	$("#testItem").hide();



	$.ajax({
		type: "post",
		data: {
			userId: userId
		},
		url: ctx + "/newDashboard/toNewDashboard.action",
		success: function(data) {
			var data = JSON.parse(data);
			var temArr = data.data;
			for(var i = 0; i < temArr.length; i++) {
				if(temArr[i] == "11") {
					$("#toDoList").show();
					//更新待办事项
					updateTodolist(userId);
				}
				if(temArr[i] == "12") {
					$("#warnInfo").show();
					//更新信息预警
					updataWarning(userId);
				}
				if(temArr[i] == "21") {
					$("#projectMain").show();
					//更新项目主数据
					projectData();
				}
				if(temArr[i] == "22") {
					$("#productMain").show();
					//更新产品主数据
					productData();
				}
				if(temArr[i] == "23"){
					$("#eChartsNew").show();
					//饼图查询生产数量
					eChartNew();
					dbclickTodolistYB();
				}
//				if(temArr[i] == "23") {
//					$("#eCharts").show();
//					//绘制饼图和柱状图
//					eChart();
//				}
				if(temArr[i] == "24") {
					$("#toDoNum").show();
					//更新实验待办数量
					updateTodoNum(userId);
				}
				if(temArr[i] == "25") {
					$("#toDoPro").show();
					//更新实验待办数量
					updataTask(userId);
				}
				
				if(temArr[i] == "26") {
					$("#dangerMenu",window.parent.document).show();
				}
				//生产代办
				if(temArr[i] == "27") {
					$("#scdb").show();
				}
				
				//偏差
				if(temArr[i] == "28") {
					$("#pcInfo").show();
					$("#pcInfo1").show();
					$("#pcInfo2").show();
					//更新实验待办数量
					findDeviation();
				}
				
				//收获时间
				if(temArr[i] == "29") {
					$("#shsj1").show();
					//更新实验待办数量
					findshsj();
				}
				//回输计划
				if(temArr[i] == "30") {
					$("#hsjh").show();
					//更新实验待办数量
					findhsjh();
				}
				
				//公告栏
				if(temArr[i] == "31") {
					$("#ggl").show();
					//更新实验待办数量
					ggl();
				}
				//订单到期提醒
				if(temArr[i] == "33") {
					$("#ordersDue").show();
					ordersDue();
				}
				//采血接收未完成提醒
				if(temArr[i] == "34") {
					$("#notSampleReceive").show();
					//更新实验待办数量
					notSampleReceive();
				}
				//回输日期未确认提醒
				if(temArr[i] == "35") {
					$("#returnDateConfirm").show();
					//更新实验待办数量
					returnDateConfirm();
				}
				//未进入运输计划的回输或订单
				if(temArr[i] == "36") {
					$("#noTransportPlan").show();
					//更新实验待办数量
					noTransportPlan();
				}
				//. 运 输 计 划 完 成 （运输计划后面项目不完
				if(temArr[i] == "37") {
					$("#transportPlanBug").show();
					//更新实验待办数量
					transportPlanBug();
				}
				
				if(temArr[i] == "38") {
					$("#deliveryWindow").show();
					//更新实验待办数量
					deliveryWindow();
				}
				
				if(temArr[i] == "39") {
					$("#resultAll").show();
					//更新实验待办数量
					resultAll();
				}
				if(temArr[i] == "41") {
					$("#zhiJianSurplus").show();
					//更新实验待办数量
					zhiJianSurplus(userId);
					
				}
				if(temArr[i] == "40") {
					$("#testItem").show();
					//更新实验待办数量
					testItem();
				}
				
			}
//			//偏差
//			if(true) {
//				$("#pcInfo").show();
//				$("#pcInfo1").show();
//				$("#pcInfo2").show();
//				//更新实验待办数量
//				findDeviation();
//			}
//			//收获时间
//			if(true) {
//				$("#shsj1").show();
//				//更新实验待办数量
//				findshsj();
//			}
//			//回输计划
//			if(true) {
//				$("#hsjh").show();
//				//更新实验待办数量
//				findhsjh();
//			}
			
			
			$.ajax({
				type: "post",
				url:ctx+"/main/selectUserId.action",
				data:{
					userId:userId,
				},
				success: function(dataBack) {
					var data = JSON.parse(dataBack);
					if(data.success){
						if(data.result){
							$.ajax({
								type: "post",
								data: {},
								url: ctx + "/system/sample/sampleOrder/selectSampleOrder.action",
								success: function(data) {
									var info = JSON.parse(data);
									var result = template("wait", info);
									$(".listWs").html(result);
									
								}
							})
						}
					}
				}
			});
			
		}
		
	});

	//更新页面顶部项目模块数据
	$.ajax({
		type: "post",
		url: ctx + "/main/showCounts.action",
		async: true,
		success: function(data) {
			var data = JSON.parse(data);
			$(".numone").text(data.counts1);
			$(".numtwo").text(data.counts2);
			$(".numthree").text(data.counts3);
			$(".numfour").text(data.counts4);
			$(".small-box-footer").click(function(){
				$("#maincontentframe",parent.document).attr("src",this.getAttribute("src"));
				
			})
		}
	});
	
});

//查询并进入实验单
function serchsampleCodes(){
//	var arr = [];
	var arr = $("#sampleCodes").val().split("\n");
	arr.pop();
//	var qkarr = $.grep(array, function(n) {
//				return $.trim(n).length > 0;
//			})
//	for (var index = 0; index < qkarr.length; index++) {
//		arr[index] = qkarr[index].split("\t");
//	}
	//是否是他的任务，判断样本是否在外周血步骤 ，并且样本是否是一个实验单，样本数量是否正确
	$.ajax({
		type: "post",
		data: {
			ids: arr
		},
		url: ctx + "/main/showCellpassageItem.action",
		success: function(data) {
			var info = JSON.parse(data);
			if(info.success){
				if(info.checkYes){
					$("#maincontentframe", window.parent.document)[0].src = window.ctx + 
					"/experiment/cell/passage/cellPassage/showCellPassageSteps.action?id=" + info.checkYesId + "&orderNum=1";
				}else{
					top.layer.msg(info.checkYesNote);
				}
			}
		}
	});
}

//更新待办事项
function updateTodolist(userId) {
	$.ajax({
		type: "post",
		data: {
			user: userId
		},
		url: ctx + "/main/showListOne.action",
		success: function(data) {
			var info = JSON.parse(data)
			var result = template("template", info);
			$(".list").html(result);

			function add0(m) {
				return m < 10 ? '0' + m : m
			}

			function format(timestamp) {
				var time = new Date(parseInt(timestamp));
				var year = time.getFullYear();
				var month = time.getMonth() + 1;
				var date = time.getDate();
				var hours = time.getHours();
				var minutes = time.getMinutes();
				var seconds = time.getSeconds();
				return year + '-' + add0(month) + '-' + add0(date) + ' ' + add0(hours) + ':' + add0(minutes) + ':' + add0(seconds);
			}
			var infoo = $(".startData");
			for(var i = 0; i < infoo.length; i++) {
				infoo[i].innerHTML = format(infoo[i].innerHTML);
			}
			clickToOldPage()
			dbclickTodolist()
		}
	})
}

//双击进入待办事项新页面 
function dbclickTodolist() {
	var touchtime = new Date().getTime();
	$(".list tr").unbind("click").click(function() {
		if(new Date().getTime() - touchtime < 500) {
			var taskId = $(this).find("td").eq(1).text();
			var formId = $(this).find("td").eq(2).text();
			var formName = $(this).find("td").eq(3).text();
			sessionStorage.removeItem("taskId");
			sessionStorage.removeItem("formId");
			sessionStorage.removeItem("formName");
			sessionStorage.setItem("taskId", taskId);
			sessionStorage.setItem("formId", formId);
			sessionStorage.setItem("formName", formName);
			$.ajax({
				type: "post",
				url: ctx + "/workflow/processinstance/getFormUrl.action",
				data: {
					taskId: taskId,
					formId: formId,
					formName: formName
				},
				datatype: "json",
				success: function(data) {
					info = JSON.parse(data)
					if(info.success) {

						if(info.data.indexOf("id=") > -1) {
							$("#maincontentframe", window.parent.document)[0].src = window.ctx + info.data + formId + "&bpmTaskId=" + taskId;
						} else {

							$("#maincontentframe", window.parent.document)[0].src = window.ctx + info.data;
						}

					}
				}
			});

		} else {
			touchtime = new Date().getTime();
		}
	});
}


//项目任务提醒
function updataTask(userName) {
	$.ajax({
		type: "post",
		data: {
			user: userName
		},
		url: ctx + "/experiment/techreport/techReportMain/showMainTaskJson.action",
		success: function(data) {
			var info = JSON.parse(data);
			var result = template("template2", info);
			$("#list3").html(result);
			var touchtime = new Date().getTime();
			$("#list3").on("click", "tr", function() {
				if(new Date().getTime() - touchtime < 500) {
					var taskId = $(this).find("td").eq(1).text();
					var href=$(this).attr("hrf");
					$("#maincontentframe", window.parent.document)[0].src = href+"?"+"taskId="+taskId;
				} else {
					touchtime = new Date().getTime();
				}
			});
		}
	})

}
//单击进入老页面
function clickToOldPage() {
	var touchtime = new Date().getTime();
	$(".fa-tv").unbind("click").click(function() {
			var taskId = $(this).parents("tr").find("td").eq(1).text();
			var formId = $(this).parents("tr").find("td").eq(2).text();
			var formName = $(this).parents("tr").find("td").eq(3).text();
			sessionStorage.removeItem("taskId");
			sessionStorage.removeItem("formId");
			sessionStorage.removeItem("formName");
			sessionStorage.setItem("taskId", taskId);
			sessionStorage.setItem("formId", formId);
			sessionStorage.setItem("formName", formName);
			$.ajax({
				type: "post",
				url: ctx + "/workflow/processinstance/getFormUrl.action",
				data: {
					taskId: taskId,
					formId: formId,
					formName: formName
				},
				datatype: "json",
				success: function(data) {
					info = JSON.parse(data)
					if(info.success) {

						if(info.data.indexOf("id=") > -1) {
							$("#maincontentframe", window.parent.document)[0].src = window.ctx + info.data + formId + "&bpmTaskId=" + taskId;
						} else {

							$("#maincontentframe", window.parent.document)[0].src = window.ctx + info.data;
						}

					}
				}
			});
	});

//	$(".fa-tv").unbind("click").click(function() {
//		var formName = $(this).parents("tr").find("td").eq(3).text();
//		var formId = $(this).parents("tr").find("td").eq(2).text();
//		var taskId = $(this).parents("tr").find("td").eq(1).text();
//		$.ajax({
//			type: "post",
//			data: {
//				formName: formName
//			},
//			url: ctx + "/main/showOldTaskItem.action",
//			async: true,
//			success: function(data) {
//				var info = JSON.parse(data);
//				var href = info.data + "" + formId + "&bpmTaskId=" + taskId + "";
//				$("#maincontentframe", window.parent.document)[0].src = window.ctx + href;
//			}
//		});
//	})
}

//查询收获时间
function findshsj(){
	$.ajax({
		type: "post",
		data: {
//			userId: userId
		},
		url: ctx + "/main/showHarvestDate.action",
		success: function(data) {
			
			var info = JSON.parse(data);
			console.log(info)
			console.log(info.pcList1)
			$(".shsj2").html("");
			for(var i=0;i < info.pcList1.length;i++) {
				var tr = $('<tr>'
					+	'<td><i class="fa fa-hand-rock-o"></i></td>'
					+	'<td>' + info.pcList1[i].code + '</td>'
					+	'<td>' + info.pcList1[i].harvestDate + '</td>'
					+	'<td>' + info.pcList1[i].endHarvestDate + '</td>'
					+	'<td>' + info.pcList1[i].transportDate + '</td>'
					+	'<td>' + info.pcList1[i].cellDate + '</td>'
					+	'<td>' + info.pcList1[i].ccoi + '</td>'
					+	'<td>' + info.pcList1[i].counts + '</td>'
					+	'<td>' + info.pcList1[i].posId + '</td>'
				+	'</tr>');
				$(".shsj2").append(tr);
			}
		}
	});
}

//公告栏查询
function ggl(){
	$.ajax({
		type: "post",
		data: {
//			userId: userId
		},
		url: ctx + "/main/showGglList.action",
		success: function(data) {
			
			var info = JSON.parse(data);
			console.log(info)
			console.log(info.ggl)
			$(".ggl1").html("");
			for(var i=0;i < info.ggl.length;i++) {
				var tr = $('<tr>'
					+	'<td><i class="fa fa-hand-rock-o"></i></td>'
					+	'<td id="gglid">' + info.ggl[i].id + '</td>'
					+	'<td>' + info.ggl[i].name + '</td>'
				+	'</tr>');
				$(".ggl1").append(tr);
			}
		}
	});
}

//查询回输计划
function findhsjh(){
	$.ajax({
		type: "post",
		data: {
//			userId: userId
		},
		url: ctx + "/main/showHsjhList.action",
		success: function(data) {
			
			var info = JSON.parse(data);
			console.log(info)
			console.log(info.hsjhList1)
			console.log(info.hsjhList2)
			$(".hsjh1").html("");
			for(var i=0;i < info.hsjhList1.length;i++) {
				var tr = $('<tr>'
					+	'<td><i class="fa fa-hand-rock-o"></i></td>'
					+	'<td>' + info.hsjhList1[i].batch + '</td>'
					+	'<td>' + info.hsjhList1[i].reinfusionPlanDate + '</td>'
				+	'</tr>');
				$(".hsjh1").append(tr);
			}
			$(".hsjh2").html("");
			for(var i=0;i < info.hsjhList2.length;i++) {
				var tr = $('<tr>'
					+	'<td><i class="fa fa-hand-rock-o"></i></td>'
					+	'<td>' + info.hsjhList2[i].barcode + '</td>'
					+	'<td>' + info.hsjhList2[i].id + '</td>'
					+	'<td>' + info.hsjhList2[i].feedBackTime + '</td>'
				+	'</tr>');
				$(".hsjh2").append(tr);
			}
		}
	});
}

//查询偏差表
function findDeviation(){
	$.ajax({
		type: "post",
		data: {
//			userId: userId
		},
		url: ctx + "/main/showDeviationInfo.action",
		success: function(data) {
//			var info = JSON.parse(data);
//			var result = template("template1", info);
//			$(".list1").html(result);
			
			
			var info = JSON.parse(data);
			console.log(info)
			console.log(info.pcList1)
			$(".pcList1").html("");
			for (var i = 0; i < info.pcList1.length; i++) {
				var tr = $('<tr>'
					+	'<td><i class="fa fa-hand-rock-o"></i></td>'
					+	'<td>' + info.pcList1[i].name + '</td>'
					+	'<td>' + info.pcList1[i].orderCode + '</td>'
					+	'<td>' + info.pcList1[i].note + '</td>'
				+	'</tr>');
				$(".pcList1").append(tr);
			}
			$(".pcList2").html("");
			for(var r=0;r < info.pcList2.length;r++) {
				var tr = $('<tr>'
					+	'<td><i class="fa fa-hand-rock-o"></i></td>'
					+	'<td>' + info.pcList2[r].name + '</td>'
					+	'<td>' + info.pcList2[r].materielId + '</td>'
					+	'<td>' + info.pcList2[r].materielName + '</td>'
					+	'<td>' + info.pcList2[r].batchNumber + '</td>'
					+	'<td>' + info.pcList2[r].note + '</td>'
				+	'</tr>');
				$(".pcList2").append(tr);
			}
			$(".pcList3").html("");
			for(var z=0;z < info.pcList3.length;z++) {
				var tr = $('<tr>'
					+	'<td><i class="fa fa-hand-rock-o"></i></td>'
					+	'<td>' + info.pcList3[z].name + '</td>'
					+	'<td>' + info.pcList3[z].equipmentId + '</td>'
					+	'<td>' + info.pcList3[z].equipmentName + '</td>'
					+	'<td>' + info.pcList3[z].note + '</td>'
				+	'</tr>');
				$(".pcList3").append(tr);
			}
//			var result1 = template("pcList10", info.pcList1);
//			var result2 = template("pcList20", info.pcList2);
//			var result3 = template("pcList30", info.pcList3);
//			//console.log(result1);
//			console.log(result1)
////			$(".pcList1").html(result1);
//			$(".pcList2").html(result2);
//			$(".pcList3").html(result3);
//			
//			
//			function add0(m) {
//				return m < 10 ? '0' + m : m
//			}
//
//			function format(timestamp) {
//				var time = new Date(parseInt(timestamp));
//				var year = time.getFullYear();
//				var month = time.getMonth() + 1;
//				var date = time.getDate();
//				var hours = time.getHours();
//				var minutes = time.getMinutes();
//				var seconds = time.getSeconds();
//				return year + '-' + add0(month) + '-' + add0(date) ;//+ ' ' + add0(hours) + ':' + add0(minutes) + ':' + add0(seconds);
//			}
//			var infoo = $(".startData1");
//			for(var i = 0; i < infoo.length; i++) {
//				console.log(infoo[i].innerHTML)
//				if(infoo[i].innerHTML==null
//						||infoo[i].innerHTML==""){
//					infoo[i].innerHTML = "";
//				}else{
//					infoo[i].innerHTML = format(infoo[i].innerHTML);
//				}
//			}
//			dbclickWarnlist();
		}
	});
}

//更新信息预警
function updataWarning(userId) {
	$.ajax({
		type: "post",
		data: {
			userId: userId
		},
		url: ctx + "/main/showInformation.action",
		success: function(data) {
//			var info = JSON.parse(data);
//			var result = template("template1", info);
//			$(".list1").html(result);
			
			
			var info = JSON.parse(data);
			var result1 = template("yujing1", info.bsList);
			var result2 = template("yujing2", info.sList);
			var result3 = template("yujing3", info.ptList);
			$(".list1").html(result1);
			$(".list1").append(result2);
			$(".yiqi").html(result3);
			
			
			function add0(m) {
				return m < 10 ? '0' + m : m
			}

			function format(timestamp) {
				var time = new Date(parseInt(timestamp));
				var year = time.getFullYear();
				var month = time.getMonth() + 1;
				var date = time.getDate();
				var hours = time.getHours();
				var minutes = time.getMinutes();
				var seconds = time.getSeconds();
				return year + '-' + add0(month) + '-' + add0(date) ;//+ ' ' + add0(hours) + ':' + add0(minutes) + ':' + add0(seconds);
			}
			var infoo = $(".startData1");
			for(var i = 0; i < infoo.length; i++) {
				console.log(infoo[i].innerHTML)
				if(infoo[i].innerHTML==null
						||infoo[i].innerHTML==""){
					infoo[i].innerHTML = "";
				}else{
					infoo[i].innerHTML = format(infoo[i].innerHTML);
				}
			}
			dbclickWarnlist();
		}
	});
}

//双击进入信息预警详细页面 
function dbclickWarnlist() {
	var touchtime = new Date().getTime();
//	$(".list1").on("click", "tr", function() {
//		if(new Date().getTime() - touchtime < 500) {
//			var id = $(this).find("td").eq(1).text();
//			var contentId = $(this).find("td").eq(2).text();
//			var tableId = $(this).find("td").eq(3).text();
//			$.ajax({
//				type: "post",
//				data: {
//					id: id,
//					contentId: contentId,
//					tableId: tableId
//				},
//				url: ctx + "/main/showInformationItem.action",
//				async: true,
//				success: function(data) {
//					var href = JSON.parse(data).data;
//					$("#maincontentframe", window.parent.document)[0].src = href;
//				}
//			});
//		} else {
//			touchtime = new Date().getTime();
//		}
//	});
	$(".yiqi").on("click", "tr", function() {
		if(new Date().getTime() - touchtime < 500) {
			window.location = window.ctx +
				'/equipment/maintain/editInstrumentMaintain.action';
		} else {
			touchtime = new Date().getTime();
		}
	});
	
	$(".ggl1").on("click", "tr", function() {
		if(new Date().getTime() - touchtime < 500) {
			window.location = window.ctx +
				'/experiment/noticeInformation/noticeInformation/editNoticeInformation.action?id='+$(this).find("#gglid").text();
		} else {
			touchtime = new Date().getTime();
		}
	});
}
//更新实验待办数量并点击跳转
function updateTodoNum(userId) {
	$.ajax({
		type: "post",
		data: {
			userId: userId
		},
		url: ctx + "/main/selBacklog.action",
		//url: ctx + "/main/selAbnormal.action",
		success: function(data) {
			var info = JSON.parse(data).data[0];
			for(var k in info) {
				var tr = $('<tr><td><i class="fa fa-ellipsis-v"></i> <i class="fa fa-ellipsis-v"></i></td><td>' + k + '</td><td>' + info[k] + '</td></tr>');
				$(".list2").append(tr);
			}
			var trs = $(".list2").find("tr");
			trs.unbind("click").click(function() {
				var name = $(this).children("td")[1].innerHTML;
				$.ajax({
					type: "post",
					data: {
						name: name,
						type: "dbsx"
					},
					url: ctx + "/main/selUrl.action",
					success: function(data) {
						var src = JSON.parse(data).data;
						$("#maincontentframe", window.parent.document)[0].src = src;
					}
				});
			})
		}
	})
}

//更新项目主数据
function projectData() {
	$.ajax({
		type: "post",
		url: ctx + "/main/showProject.action",
		async: true,
		success: function(data) {
			//console.log(data);
			var info = JSON.parse(data);
			var result = template("projectTem", info);
			var arrs = info.data;
			//更新项目名称
			$(".parentWrap").html(result);
			//调用封装的时间函数将时间戳转换为时间
			for(var i = 0; i < $(".planStartDate").length; i++) {
				$(".planStartDate")[i].innerText = formatt($(".planStartDate")[i].innerText);
			}
			//默认第一个项目样式
			$(".groupTitle").eq(0).css("background-color", "#3c8dbc");
			$(".groupMenu").eq(0).css({
				"background-color": "white",
				"color": "#000000"
			});
			$(".pNum").eq(0).removeClass("label-default").addClass("label-info");
			//默认显示第一个项目信息明细
			var inputs = $("#mainInfo").find("input");
			$(inputs[0]).val(arrs[0].id);
			if(arrs[0].dutyUser){
				$(inputs[1]).val(arrs[0].dutyUser.name);
			}else{
				$(inputs[1]).val("");
			}
			if(arrs[0].type){
				$(inputs[2]).val(arrs[0].type.name);
			}else{
				$(inputs[2]).val("");
			}
			if(arrs[0].crmDoctor){
				$(inputs[3]).val(arrs[0].crmDoctor.name);
			}else{
				$(inputs[3]).val("");
			}
			if(arrs[0].mainProject){
				$(inputs[4]).val(arrs[0].mainProject.name);
			}else{
				$(inputs[4]).val("");
			}
			
			$(inputs[5]).val(formatt(arrs[0].planStartDate));
			$(inputs[6]).val(formatt(arrs[0].factEndDate));
			//点击每一个项目更新项目信息
			var title = $(".groupTitle");
			for(var i = 0; i < title.length; i++) {
				$(title[i]).click(function() {
					var seft = this;
					$("#mainInfo").fadeToggle(300);
					var index = title.index(this);
					$(this).css("background-color", "#3c8dbc").parent("li").siblings("li").find("button").css({
						"background-color": "gray"
					});
					$(".groupMenu").css({
						"background-color": "gainsboro",
						"color": "white"
					});
					$(".groupMenu").eq(index).css({
						"background-color": "white",
						"color": "#000000"
					});
					$(".pNum").removeClass("label-info").addClass("label-default");
					$(".pNum").eq(index).removeClass("label-default").addClass("label-info");
					$(inputs[0]).val(arrs[index].id);
					$(inputs[1]).val(arrs[index].dutyUser.name);
					$(inputs[2]).val(arrs[index].type.name);
					if(arrs[index].crmDoctor) {
						var crmDoctor = arrs[index].crmDoctor.name;
					} else {
						var crmDoctor = arrs[index].crmDoctor;
					}
					$(inputs[3]).val(crmDoctor);
					$(inputs[4]).val(arrs[index].mainProject.name);
					$(inputs[5]).val(formatt(arrs[index].planStartDate));
					$(inputs[6]).val(formatt(arrs[index].factEndDate));
					$("#mainInfo").fadeToggle(300);
				})
			}
		}
	});
}
//更新产品主数据
function productData() {
	$.ajax({
		type: "post",
		url: ctx + "/main/showProduct.action",
		async: true,
		success: function(data) {
			var info = JSON.parse(data);
			var arrs = info.data;
			//更新产品主数据
			var result = template("productTem", info);
			$(".parentWrap1").html(result);
			//默认显示第一个产品样式
			for(var i = 0; i < $(".createDate").length; i++) {
				$(".createDate")[i].innerText = formatt($(".createDate")[i].innerText);
			}
			var title = $(".groupTitle1");
			title.eq(0).css("background-color", "#ECBC55");
			$(".groupMenu1").eq(0).css({
				"background-color": "white",
				"color": "#000000"
			});
			$(".pNum1").eq(0).removeClass("label-default").addClass("label-warning");
			//默认显示第一个产品信息明细			
			var inputs = $("#mainInfo1").find("input");
			$(inputs[0]).val(arrs[0].id);
			$(inputs[1]).val(arrs[0].createUser.name);
			$(inputs[2]).val(arrs[0].name);
			if(arrs[0].testMethod) {
				var testMethod = arrs[0].testMethod.name;
			} else {
				var testMethod = arrs[0].testMethod;
			}
			$(inputs[3]).val(testMethod);
			$(inputs[4]).val(arrs[0].testTime);
			$(inputs[5]).val(arrs[0].mark);
			if(arrs[0].parent) {
				var parent = arrs[0].parent.name;
			} else {
				var parent = arrs[0].parent;
			}
			$(inputs[6]).val(parent);
			//点击每一个产品，详细信息更新
			for(var i = 0; i < title.length; i++) {
				$(title[i]).click(function() {
					$("#mainInfo1").slideToggle(300);
					var index = title.index(this);
					$(this).css({
						"background-color": "#ECBC55"
					}).parent("li").siblings("li").find("button").css({
						"background-color": "gray"
					});
					$(".groupMenu1").css({
						"background-color": "gainsboro",
						"color": "white"
					});
					$(".groupMenu1").eq(index).css({
						"background-color": "white",
						"color": "#000000"
					});
					$(".pNum1").removeClass("label-warning").addClass("label-default");
					$(".pNum1").eq(index).removeClass("label-default").addClass("label-warning");
					var arrs = info.data;
					var inputs = $("#mainInfo1").find("input");
					$(inputs[0]).val(arrs[index].id);
					$(inputs[1]).val(arrs[index].createUser.name);
					$(inputs[2]).val(arrs[index].name);
					if(arrs[index].testMethod) {
						var testMethod = arrs[index].testMethod.name;
					} else {
						var testMethod = arrs[index].testMethod;
					}
					$(inputs[3]).val(testMethod);
					$(inputs[4]).val(arrs[index].testTime);
					$(inputs[5]).val(arrs[index].mark);
					if(arrs[index].parent) {
						var parent = arrs[index].parent.name;
					} else {
						var parent = arrs[index].parent;
					}
					$(inputs[6]).val(parent);
					$("#mainInfo1").slideToggle(300);
				})
			}
		}
	});

}
//饼图查询生产数据
function eChartNew() {
	//初始化
	var myChart1 = echarts.init(document.getElementById('main3'));
	var year = new Date().getFullYear();
	$("#currentYearNew").val(year);
	dateYearPieNew(year)
	$("#addYearNew").click(function() {
		$("#currentYearNew").val(parseInt($("#currentYearNew").val()) + 1);
		var dateYear = $("#currentYearNew").val();
		myChart1.showLoading();
//		dateYearBar(dateYear)
		dateYearPieNew(dateYear)
	})
	$("#subYearNew").click(function() {
		$("#currentYearNew").val(parseInt($("#currentYearNew").val()) - 1);
		var dateYear = $("#currentYearNew").val();
		myChart1.showLoading();
//		dateYearBar(dateYear)
		dateYearPieNew(dateYear)
	})
//	myChart1.on('click', function(param) {
//		var dateYear = $("#currentYearNew").val();
//		var dateYearMonth = dateYear + "-" + param.name.replace("月", "");
//		dateYearMonthPieNew(dateYearMonth);
//	});
	//加载图片
	//myChart.showLoading();


	function dateYearPieNew(dateYear) {
		$.ajax({
			type: "post",
			async: true,
			url: "/system/sample/sampleOrder/selStateByOrderByYear.action",
			data: {
				dateYear: dateYear
			},
			success: function(data) {
				console.log("------------------------"+data)
				var data = JSON.parse(data);
				var info = data.data[0];
				var arr = [];
				for(var k in info) {
					var obj = {};
					obj.value = info[k];
					obj.name = k;
					if(k=="细胞总数量"){
						$("#xbzsl").text("细胞总数量:"+info[k]);
					}
					if(k=="在培养数量"){
						$("#zpysl").text("在培养数量:"+info[k]);
						arr.push(obj);
					}
					if(k=="已完成细胞数量"){
						$("#ywcxbsl").text("已完成细胞数量:"+info[k]);
						arr.push(obj);
					}
					if(k=="问题细胞数量"){
						$("#wtxbsl").text("问题细胞数量:"+info[k]);
						arr.push(obj);
					}
				}
				option1 = {
					title: {
						text: '',
						x: 'center',
						textStyle: {
							fontSize: 15,
							fontWeight: 'normal',
							color: '#333'
						}
					},
					tooltip: {
						trigger: 'item',
						formatter: "{b}:{c}({d}%)"
					},
					series: [{
						name: '',
						type: 'pie',
						radius: '55%',
						center: ['44%', '60%'],
						data: arr,
						itemStyle: {
							normal: {
								label: {
									show: true,
									formatter: '{b}:{c}({d}%)'
								},
								labelLine: {
									show: true
								}
							},
							emphasis: {
								shadowBlur: 10,
								shadowOffsetX: 0,
								shadowColor: 'rgba(0, 0, 0, 0.5)'
							}
						}
					}]
				};
				myChart1.setOption(option1);
				myChart1.hideLoading();
			}

		});

	}

//	function dateYearMonthPieNew(dateYearMonth) {
//		$.ajax({
//			type: "post",
//			async: true,
//			url: "/system/sample/sampleOrder/selProductByOrderByMonth.action",
//			data: {
//				dateYearMonth: dateYearMonth
//			},
//			success: function(data) {
//				var data = JSON.parse(data);
//				var info = data.data[0];
//				var arr = [];
//				for(var k in info) {
//					var obj = {};
//					obj.value = info[k];
//					obj.name = k;
//					arr.push(obj);
//				}
//				option1 = {
//					title: {
//						text: '',
//						x: 'center',
//						textStyle: {
//							fontSize: 15,
//							fontWeight: 'normal',
//							color: '#333'
//						}
//					},
//					tooltip: {
//						trigger: 'item',
//						formatter: "{b}:{c}({d}%)"
//					},
//					series: [{
//						name: '',
//						type: 'pie',
//						radius: '55%',
//						center: ['50%', '60%'],
//						data: arr,
//						itemStyle: {
//							normal: {
//								label: {
//									show: true,
//									formatter: '{b}:{c}({d}%)'
//								},
//								labelLine: {
//									show: true
//								}
//							},
//							emphasis: {
//								shadowBlur: 10,
//								shadowOffsetX: 0,
//								shadowColor: 'rgba(0, 0, 0, 0.5)'
//							}
//						}
//					}]
//				};
//				myChart1.setOption(option1);
//				myChart1.hideLoading();
//			}
//
//		});
//
//	}

}
//点击进入样本查询页 
function dbclickTodolistYB() {
	$(".aii").unbind("click").click(function() {
		var style="";
		if($(this).attr("id")=="xbzsl"){
			style="1";
		}else if($(this).attr("id")=="zpysl"){
			style="2";
		}else if($(this).attr("id")=="ywcxbsl"){
			style="3";
		}else if($(this).attr("id")=="wtxbsl"){
			style="4";
		}
		$("#maincontentframe", window.parent.document)[0].src = window.ctx + "/sample/sampleSearchList/showSampleSearch.action" + "?style=" + style+"&time="+$("#currentYearNew").val();
	});
}
//柱状图与饼图
function eChart() {
	//初始化
	var myChart = echarts.init(document.getElementById('main'));
	var myChart1 = echarts.init(document.getElementById('main1'));
	var year = new Date().getFullYear();
	$("#currentYear").val(year);
	dateYearBar(year)
	dateYearPie(year)
	$("#addYear").click(function() {
		$("#currentYear").val(parseInt($("#currentYear").val()) + 1);
		var dateYear = $("#currentYear").val();
		myChart.showLoading();
		dateYearBar(dateYear)
		dateYearPie(dateYear)
	})
	$("#subYear").click(function() {
		$("#currentYear").val(parseInt($("#currentYear").val()) - 1);
		var dateYear = $("#currentYear").val();
		myChart.showLoading();
		dateYearBar(dateYear)
		dateYearPie(dateYear)
	})
	myChart.on('click', function(param) {
		var dateYear = $("#currentYear").val();
		var dateYearMonth = dateYear + "-" + param.name.replace("月", "");
		dateYearMonthPie(dateYearMonth);
	});
	//加载图片
	//myChart.showLoading();

	function dateYearBar(dateYear) {
		$.ajax({
			type: "post",
			async: true,
			url: "/system/sample/sampleOrder/selOrderNumberByYear.action",
			data: {
				dateYear: dateYear
			},
			success: function(data) {
				var data = JSON.parse(data);
				var info = data.data;
				mon = [];
				num = [];
				for(var i = 0; i < info.length; i++) {
					mon.push(info[i][0] + biolims.user.month);
					num.push(info[i][1]);
				}
				myChart.hideLoading();
				option = {
					color: ['#3398DB'],
					title: {
						x: 'center',
						textStyle: {
							fontSize: 15,
							fontWeight: 'normal',
							color: '#333'
						}
					},
					tooltip: {},
					legend: {
						data: [biolims.common.count]
					},
					xAxis: {
						data: mon
					},
					yAxis: {},
					series: [{
						// 根据名字对应到相应的系列
						name:biolims.common.count,
						type: 'bar',
						data: num
					}]
				};
				myChart.setOption(option);
			}
		});
	}

	function dateYearPie(dateYear) {
		$.ajax({
			type: "post",
			async: true,
			url: "/system/sample/sampleOrder/selProductByOrderByYear.action",
			data: {
				dateYear: dateYear
			},
			success: function(data) {
				var data = JSON.parse(data);
				var info = data.data[0];
				var arr = [];
				for(var k in info) {
					var obj = {};
					obj.value = info[k];
					obj.name = k;
					arr.push(obj);
				}
				option1 = {
					title: {
						text: '',
						x: 'center',
						textStyle: {
							fontSize: 15,
							fontWeight: 'normal',
							color: '#333'
						}
					},
					tooltip: {
						trigger: 'item',
						formatter: "{b}:{c}({d}%)"
					},
					series: [{
						name: '',
						type: 'pie',
						radius: '55%',
						center: ['50%', '60%'],
						data: arr,
						itemStyle: {
							normal: {
								label: {
									show: true,
									formatter: '{b}:{c}({d}%)'
								},
								labelLine: {
									show: true
								}
							},
							emphasis: {
								shadowBlur: 10,
								shadowOffsetX: 0,
								shadowColor: 'rgba(0, 0, 0, 0.5)'
							}
						}
					}]
				};
				myChart1.setOption(option1);
				myChart1.hideLoading();
			}

		});

	}

	function dateYearMonthPie(dateYearMonth) {
		$.ajax({
			type: "post",
			async: true,
			url: "/system/sample/sampleOrder/selProductByOrderByMonth.action",
			data: {
				dateYearMonth: dateYearMonth
			},
			success: function(data) {
				var data = JSON.parse(data);
				var info = data.data[0];
				var arr = [];
				for(var k in info) {
					var obj = {};
					obj.value = info[k];
					obj.name = k;
					arr.push(obj);
				}
				option1 = {
					title: {
						text: '',
						x: 'center',
						textStyle: {
							fontSize: 15,
							fontWeight: 'normal',
							color: '#333'
						}
					},
					tooltip: {
						trigger: 'item',
						formatter: "{b}:{c}({d}%)"
					},
					series: [{
						name: '',
						type: 'pie',
						radius: '55%',
						center: ['50%', '60%'],
						data: arr,
						itemStyle: {
							normal: {
								label: {
									show: true,
									formatter: '{b}:{c}({d}%)'
								},
								labelLine: {
									show: true
								}
							},
							emphasis: {
								shadowBlur: 10,
								shadowOffsetX: 0,
								shadowColor: 'rgba(0, 0, 0, 0.5)'
							}
						}
					}]
				};
				myChart1.setOption(option1);
				myChart1.hideLoading();
			}

		});

	}

}

//封装一个时间戳转具体时间函数
function add1(m) {
	return m < 10 ? '0' + m : m
}

function formatt(timestamp) {
	var time = new Date(parseInt(timestamp));
	var year = time.getFullYear();
	var month = time.getMonth() + 1;
	var date = time.getDate();
	return year + '-' + add1(month) + '-' + add1(date);
}

//订单到期提醒
function ordersDue(userId) {
	var tr ;
	$.ajax({
		type: "post",
		data: {
			userId: userId
		},
		url: ctx + "/main/showOrdersDue.action",
		success: function(data) {
			var data = JSON.parse(data);
			
			for(var i=0;i < data.sampleOrderDue.length;i++) {
				if(data.sampleOrderDue[i].state=="提醒"){
					tr= $('<tr style="background:#FFFF66">'
							+	'<td><i class="fa fa-hand-rock-o"></i></td>'
							+	'<td>' + data.sampleOrderDue[i].id + '</td>'
							+	'<td>' + data.sampleOrderDue[i].drawBloodTime + '</td>'
							+	'<td>' + data.sampleOrderDue[i].barcode + '</td>'
//							+	'<td>' + data.sampleOrderDue[i].ccoi + '</td>'
							+	'<td>' + data.sampleOrderDue[i].state + '</td>'
						    +	'</tr>');
				}else{
					tr= $('<tr style="background:#FF3333">'
							+	'<td><i class="fa fa-hand-rock-o"></i></td>'
							+	'<td>' + data.sampleOrderDue[i].id + '</td>'
							+	'<td>' + data.sampleOrderDue[i].drawBloodTime + '</td>'
							+	'<td>' + data.sampleOrderDue[i].barcode + '</td>'
//							+	'<td>' + data.sampleOrderDue[i].ccoi + '</td>'
							+	'<td>' + data.sampleOrderDue[i].state + '</td>'
						    +	'</tr>');
				}
				
				$(".ordersDuelist").append(tr);
			}
			
			var trs = $(".ordersDuelist").find("tr");
			   trs.unbind("click").click(function() {
			    var id = $(this).children("td")[1].innerHTML;

			    $("#maincontentframe", window.parent.document)[0].src =window.ctx + "/system/sample/sampleOrder/editSampleOrder.action?id="+id+"&type=0";
			   })
			
		}
	});
}
//血样接收未完成提醒
function notSampleReceive(userId) {
	var tr ;
	$.ajax({
		type: "post",
		data: {
			userId: userId
		},
		url: ctx + "/main/showNotSampleReceive.action",
		success: function(data) {
			var data = JSON.parse(data);
			console.log(data)
			for(var i=0;i < data.notSampleReceive.length;i++) {
				
				   tr= $('<tr>'
						+	'<td><i class="fa fa-hand-rock-o"></i></td>'
						+	'<td style="display:none;">' + data.notSampleReceive[i].sampleReceiveId + '</td>'
						+	'<td>' + data.notSampleReceive[i].id + '</td>'
						+	'<td>' + data.notSampleReceive[i].drawBloodTime + '</td>'
						+	'<td>' + data.notSampleReceive[i].barcode + '</td>'
						+	'<td>' + data.notSampleReceive[i].state + '</td>'
					    +	'</tr>');
				$(".notSampleReceiveList").append(tr);
			}
			
			var trs = $(".notSampleReceiveList").find("tr");
			trs.unbind("click").click(function() {
				var id = $(this).children("td")[1].innerHTML;
				if(id==""||id==null){
					$("#maincontentframe", window.parent.document)[0].src =window.ctx + "/sample/sampleReceive/editSampleReceive.action";	
				}else{
					$("#maincontentframe", window.parent.document)[0].src =window.ctx + "/sample/sampleReceive/editSampleReceive.action?id="+id+"&type=";

				}
			})
			
		}
	});
}
//回输日期确认提醒
function returnDateConfirm(userId) {
	var tr ;
	$.ajax({
		type: "post",
		data: {
			userId: userId
		},
		url: ctx + "/main/showReturnDateConfirm.action",
		success: function(data) {
			var data = JSON.parse(data);
			for(var i=0;i < data.returnDateConfirm.length;i++) {
				
				tr= $('<tr>'
						+	'<td><i class="fa fa-hand-rock-o"></i></td>'
						+	'<td>' + data.returnDateConfirm[i].batch + '</td>'
						+	'<td>' + data.returnDateConfirm[i].reinfusionPlanDate + '</td>'
						+	'<td>' + data.returnDateConfirm[i].state + '</td>'
						+	'</tr>');
				$(".returnDateConfirmList").append(tr);
			}
			
			var trs = $(".returnDateConfirmList").find("tr");
			trs.unbind("click").click(function() {
					$("#maincontentframe", window.parent.document)[0].src =window.ctx + "/experiment/reinfusionPlan/reinfusionPlan/showReinfusionPlanItemList.action?type=2";	
			})
			
		}
	});
}
//未进入运输计划的回输或者订单
function noTransportPlan(userId) {
	var tr ;
	$.ajax({
		type: "post",
		data: {
			userId: userId
		},
		url: ctx + "/main/showNoTransportPlan.action",
		success: function(data) {
			var data = JSON.parse(data);
			for(var i=0;i < data.noTransportPlan.length;i++) {
				
				tr= $('<tr>'
						+	'<td><i class="fa fa-hand-rock-o"></i></td>'
						+	'<td>' + data.noTransportPlan[i].tixingTime + '</td>'
						+	'<td>' + data.noTransportPlan[i].reinfusionPlanDate + '</td>'
						+	'<td>' + data.noTransportPlan[i].barcode + '</td>'
						+	'<td>' + data.noTransportPlan[i].state + '</td>'
						+	'</tr>');
				$(".noTransportPlanList").append(tr);
			}
			
			var trs = $(".noTransportPlanList").find("tr");
			trs.unbind("click").click(function() {
				$("#maincontentframe", window.parent.document)[0].src =window.ctx + "/tra/transport/transportOrder/editTransportOrder.action";	
			})
			
		}
	});
}
//. 运 输 计 划 完 成 （运输计划后面项目不完
function transportPlanBug(userId) {
	var tr ;
	$.ajax({
		type: "post",
		data: {
			userId: userId
		},
		url: ctx + "/main/showTransportPlanBug.action",
		success: function(data) {
			var data = JSON.parse(data);
			for(var i=0;i < data.transportPlanBugList.length;i++) {
				
				tr= $('<tr>'
						+	'<td><i class="fa fa-hand-rock-o"></i></td>'
						+	'<td>' + data.transportPlanBugList[i].id + '</td>'
						+	'<td>' + data.transportPlanBugList[i].samplecode + '</td>'
						+	'<td>' + data.transportPlanBugList[i].state + '</td>'
						+	'</tr>');
				$(".transportPlanBugList").append(tr);
			}
			
			var trs = $(".transportPlanBugList").find("tr");
			trs.unbind("click").click(function() {
				
				var id = $(this).children("td")[1].innerHTML;
				$("#maincontentframe", window.parent.document)[0].src =window.ctx + "/tra/transport/transportOrder/editTransportOrder.action?id="+id+"";	
			})
			
		}
	});
}
//. 检测项剩余
function zhiJianSurplus(userId) {
	var tr ;
	$.ajax({
		type: "post",
		data: {
			userId: userId
		},
		url: ctx + "/main/zhiJianSurplus.action",
		success: function(data) {
			var data = JSON.parse(data);
			for(var i=0;i < data.zhiJianSurplus.length;i++) {
				
				tr= $('<tr>'
						+	'<td><i class="fa fa-hand-rock-o"></i></td>'
						+	'<td>' + data.zhiJianSurplus[i].sum + '</td>'
						+	'<td>' + data.zhiJianSurplus[i].name + '</td>'
						+	'</tr>');
				$(".zhiJianSurplusList").append(tr);
			}
			
			var trs = $(".zhiJianSurplusList").find("tr");
			trs.unbind("click").click(function() {
				
				var id = $(this).children("td")[1].innerHTML;
				$("#maincontentframe", window.parent.document)[0].src =window.ctx + "/experiment/quality/qualityTest/editQualityTest.action?cellType=1";	
			})
			
		}
	});
}


//传递窗
function deliveryWindow(userId) {
	var tr ;
	$.ajax({
		type: "post",
		data: {
			userId: userId
		},
		url: ctx + "/main/showDeliveryWindow.action",
		success: function(data) {
			var data = JSON.parse(data);
			console.log(data)
			for(var i=0;i < data.deliveryWindow.length;i++) {
				if(data.deliveryWindow[i].okko=="超时"){
					tr= $('<tr style="background:#ff3333">'
							+	'<td><i class="fa fa-hand-rock-o"></i></td>'
							+	'<td>' + data.deliveryWindow[i].code + '</td>'
							+	'<td>' + data.deliveryWindow[i].batch + '</td>'
							+	'<td>' + data.deliveryWindow[i].jcxName + '</td>'
							+	'<td>' + data.deliveryWindow[i].tjName + '</td>'
							+	'<td>' + data.deliveryWindow[i].okko + '</td>'
						    +	'</tr>');
				}else{
					tr= $('<tr style="background:#ffffff">'
							+	'<td><i class="fa fa-hand-rock-o"></i></td>'
							+	'<td>' + data.deliveryWindow[i].code + '</td>'
							+	'<td>' + data.deliveryWindow[i].batch + '</td>'
							+	'<td>' + data.deliveryWindow[i].jcxName + '</td>'
							+	'<td>' + data.deliveryWindow[i].tjName + '</td>'
							+	'<td>' + data.deliveryWindow[i].okko + '</td>'
						    +	'</tr>');
				}
				
				$(".deliveryWindow").append(tr);
			}
			
			var trs = $(".deliveryWindow").find("tr");
			   trs.unbind("click").click(function() {
			    var id = $(this).children("td")[1].innerHTML;
			    $("#maincontentframe", window.parent.document)[0].src =window.ctx + "/experiment/quality/qualityTest/editQualityTest.action?cellType=1";
			   })
			
		}
	});
}
//结果出具信息
function resultAll(userId) {
	var tr ;
	$.ajax({
		type: "post",
		data: {
			userId: userId
		},
		url: ctx + "/main/showResultAll.action",
		success: function(data) {
			var data = JSON.parse(data);
			for(var i=0;i < data.resultAll.length;i++) {
				if(data.resultAll[i].okko=="超时"){
					tr= $('<tr style="background:#ff3333">'
							+	'<td><i class="fa fa-hand-rock-o"></i></td>'
							+	'<td>' + data.resultAll[i].qy + '</td>'
							+	'<td>' + data.resultAll[i].code + '</td>'
							+	'<td>' + data.resultAll[i].jcxName + '</td>'
							+	'<td>' + data.resultAll[i].tjName + '</td>'
							+	'<td>' + data.resultAll[i].okko + '</td>'
						    +	'</tr>');
				}else{
					tr= $('<tr style="background:#ffffff">'
							+	'<td><i class="fa fa-hand-rock-o"></i></td>'
							+	'<td>' + data.resultAll[i].qy + '</td>'
							+	'<td>' + data.resultAll[i].code + '</td>'
							+	'<td>' + data.resultAll[i].jcxName + '</td>'
							+	'<td>' + data.resultAll[i].tjName + '</td>'
							+	'<td>' + data.resultAll[i].okko + '</td>'
						    +	'</tr>');
				}
				
				$(".resultAll").append(tr);
			}
			
			var trs = $(".resultAll").find("tr");
			   trs.unbind("click").click(function() {
			    var id = $(this).children("td")[1].innerHTML;
//			    $("#maincontentframe", window.parent.document)[0].src =window.ctx + "/experiment/quality/qualityTest/editQualityTest.action?cellType=1";
			   })
			
		}
	});
}

//检测项提醒日期
function testItem(userId) {
	var tr ;
	$.ajax({
		type: "post",
		data: {
			userId: userId
		},
		url: ctx + "/main/showTestItem.action",
		success: function(data) {
			var data = JSON.parse(data);
			console.log(data)
			for(var i=0;i < data.testItem.length;i++) {
				if(data.testItem[i].okko=="超时"){
					tr= $('<tr style="background:#ff3333">'
							+	'<td><i class="fa fa-hand-rock-o"></i></td>'
							+	'<td>' + data.testItem[i].code + '</td>'
							+	'<td>' + data.testItem[i].batch + '</td>'
							+	'<td>' + data.testItem[i].jcxName + '</td>'
							+	'<td>' + data.testItem[i].tjName + '</td>'
							+	'<td>' + data.testItem[i].okko + '</td>'
						    +	'</tr>');
				}else{
					tr= $('<tr style="background:#ffffff">'
							+	'<td><i class="fa fa-hand-rock-o"></i></td>'
							+	'<td>' + data.testItem[i].code + '</td>'
							+	'<td>' + data.testItem[i].batch + '</td>'
							+	'<td>' + data.testItem[i].jcxName + '</td>'
							+	'<td>' + data.testItem[i].tjName + '</td>'
							+	'<td>' + data.testItem[i].okko + '</td>'
						    +	'</tr>');
				}
				
				$(".testItem").append(tr);
			}
			
			var trs = $(".testItem").find("tr");
			   trs.unbind("click").click(function() {
			    var id = $(this).children("td")[1].innerHTML;
//			    $("#maincontentframe", window.parent.document)[0].src =window.ctx + "/experiment/quality/qualityTest/editQualityTest.action?cellType=1";
			   })
			
		}
	});
}


