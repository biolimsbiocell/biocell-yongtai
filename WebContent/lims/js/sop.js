$(function() {
	setInterval(function  () {
		$("#data-now").text(moment().format('YYYY-MM-DD HH:mm:ss'))
	},1000);
	check_all($(".check-all"));
	shift_select($("#sample-body tr"));
	shift_select($(".sample-body tr"));
	drag();
	change();
	slide();
});

function check_all(element) {
	
	element.click(function() {
		$(this).parents("thead").siblings("tbody").find("input").prop("checked", $(this).prop("checked"));
		if($(this).parents("thead").siblings("tbody").find("input").prop("checked")){
			$(this).parents("thead").siblings("tbody").find("tr").css("background-color","#90caaf");
		}else{
			console.log("meixuanzhaong ")
			$(this).parents("thead").siblings("tbody").find("tr").css("background-color","white");
		}
	});
}

function shift_select(element) {
	var rem = new Array();
	element.click(function(e) {
		if($(this).find("input").prop("checked")) {
			$(this).find("input").prop("checked", false);
		} else {
			$(this).find("input").prop("checked", true);
		}
		//shift配合鼠标左键全选
		rem.push(element.index($(this)))
		if(e.shiftKey) {
			var iMin = Math.min(rem[rem.length - 2], rem[rem.length - 1])
			var iMax = Math.max(rem[rem.length - 2], rem[rem.length - 1])
			for(i = iMin; i <= iMax; i++) {
				element.eq(i).find("input").prop("checked", true);
				element.eq(i).css("background-color", "#90caaf");
			}

		}

		//获取选中个数
		var inputs=element.find("input");
		for(var j = 0; j < inputs.length; j++) {
			if(inputs[j].checked) {
				element.eq(j).css("background-color", "#90caaf");
			} else {
				element.eq(j).css("background-color", "white");
			}
		}

		//阻止事件冒泡
		e = e || window.event;
		if(e.stopPropagation) {
			e.stopPropagation();
		} else {
			e.cancelBubble = true;
		}

	});
}

//与程序无关，是禁止鼠标选中文字。点选的时候容易选中文字 太讨厌 。  
document.onselectstart = function(event) {
	event = window.event || event;
	event.returnValue = false;
}

function drag() {
	document.getElementById("content1").onmousedown = function() {
		document.getElementById("mask").style.display = "none";
		this.onmousemove = function(e) {
			document.getElementById("mask").style.display = "block";
			document.getElementById("mask").style.left = e.pageX + 10 + "px";
			document.getElementById("mask").style.top = e.pageY + 10 + 'px';
			var a = document.getElementById("mask").innerHTML = $("#sample-body tr input:checked").length;
			var sops = document.getElementById("sop-body").children;
			for(var i = 0; i < sops.length; i++) {
				sops[i].onclick = function() {
					document.getElementById("mask").style.display = "none";
					this.innerHTML = a + "条被选";
				}

			}

		}
	}
}

function change() {
	$("#nextbtn1").click(function(e) {
		//这里的trs直接append进目标table里顺序是反的，所以反向循环一下插入
		var trs = $("#sample-body tr input:checked").parents("tr").clone();
		for(var i = 95; i >= 0; i--) {
			if(trs[i]) {
				$("#grid-body").append(trs[i]);
			}
		}

		$("#mask").hide();
		$("#content1").fadeOut(1000);
		$("#content2").fadeIn(1000);
		shift_select($("#grid-body tr"));
		drag_more();
	})
	$("#nextbtn2").click(function  () {
		$("#content2").hide(1000);
		$("#content3").show(1000);
	})
	$("#nextbtn3").click(function  () {
		$("#content3").hide(1000);
		$("#content4").show(1000);
	})
	$("#prev2").click(function  () {
		$("#content2").hide(1000);
		$("#content1").show(1000);
	})
	$("#prev3").click(function  () {
		$("#content3").hide(1000);
		$("#content2").show(1000);
	})
	$("#prev4").click(function  () {
		$("#content4").hide(1000);
		$("#content3").show(1000);
	})
}
function drag_more() {
	document.getElementById("grid-body").onmousedown = function() {
		document.getElementById("mask2").style.display = "none";
		document.onmousemove = function(e) {
			document.getElementById("mask2").style.display = "block";
			document.getElementById("mask2").style.left = e.pageX + 10 + "px";
			document.getElementById("mask2").style.top = e.pageY + 10 + 'px';
			var c = document.getElementById("mask2").innerHTML = $("#grid-body tr input:checked").length;

			var holes = document.getElementById("grid").getElementsByTagName("div");
			for(var i = 0; i < holes.length; i++) {
				holes[i].onmouseover = function() {
					var zz = parseInt(this.getAttribute("z"));
					var yy = parseInt(this.getAttribute("y"));
					var cc = parseInt(c);
					for(var j = 0; j < holes.length; j++) {

						if(holes[j].getAttribute("y") >= yy && zz <= holes[j].getAttribute("z") && holes[j].getAttribute("z") < zz + cc) {
							holes[j].className = "myhover";
						} else {
							holes[j].className = null;
						}
					}

				}
				holes[i].onmouseout = function() {
					for(var j = 0; j < holes.length; j++) {
						holes[j].className = null;
					}
				}
				holes[i].onclick = function() {
					var zz = parseInt(this.getAttribute("z"));
					var yy = parseInt(this.getAttribute("y"));
					var cc = parseInt(c);
					var xxx = [];
					var yyy = [];
					for(var j = 0; j < holes.length; j++) {
						if(holes[j].getAttribute("y") >= yy && zz <= holes[j].getAttribute("z") && holes[j].getAttribute("z") < zz + cc) {
							holes[j].style.backgroundColor = "#007BB6";
							if(holes[j].getAttribute("y") == yy) {
								xxx.push(holes[j].getAttribute("x"));
								yyy.push(holes[j].getAttribute("y"));
							}

						} else {
							holes[j].className = null;
						}
					}
					for(var j = 0; j < holes.length; j++) {
						if(holes[j].getAttribute("y") >= yy && zz <= holes[j].getAttribute("z") && holes[j].getAttribute("z") < zz + cc) {
							holes[j].style.backgroundColor = "#007BB6";
							if(holes[j].getAttribute("y") == yy + 1) {
								xxx.push(holes[j].getAttribute("x"));
								yyy.push(holes[j].getAttribute("y"));
							}

						} else {
							holes[j].className = null;
						}
					}
					for(var j = 0; j < holes.length; j++) {
						if(holes[j].getAttribute("y") >= yy && zz <= holes[j].getAttribute("z") && holes[j].getAttribute("z") < zz + cc) {
							holes[j].style.backgroundColor = "#007BB6";
							if(holes[j].getAttribute("y") == yy + 2) {
								xxx.push(holes[j].getAttribute("x"));
								yyy.push(holes[j].getAttribute("y"));
							}

						} else {
							holes[j].className = null;
						}
					}
					for(var j = 0; j < holes.length; j++) {
						if(holes[j].getAttribute("y") >= yy && zz <= holes[j].getAttribute("z") && holes[j].getAttribute("z") < zz + cc) {
							holes[j].style.backgroundColor = "#007BB6";
							if(holes[j].getAttribute("y") == yy + 3) {
								xxx.push(holes[j].getAttribute("x"));
								yyy.push(holes[j].getAttribute("y"));
							}

						} else {
							holes[j].className = null;
						}
					}
					for(var j = 0; j < holes.length; j++) {
						if(holes[j].getAttribute("y") >= yy && zz <= holes[j].getAttribute("z") && holes[j].getAttribute("z") < zz + cc) {
							holes[j].style.backgroundColor = "#007BB6";
							if(holes[j].getAttribute("y") == yy + 4) {
								xxx.push(holes[j].getAttribute("x"));
								yyy.push(holes[j].getAttribute("y"));
							}

						} else {
							holes[j].className = null;
						}
					}

					document.onmousemove = null;
					document.getElementById("mask2").style.display = "none";	
					//因为jQuery获取的grid元素是逆序的，所以反向遍历获取
					var trs = $("#grid-body tr input:checked").parents("tr").clone();
					for (var i = 95; i >=0; i--) {
						if(trs[i]){
								trs[i].getElementsByTagName("td")[0].innerHTML = xxx[xxx.length-i-1] + yyy[xxx.length-i-1];
								$("#result-body").append(trs[i]);
						}
					}
				}
			}
		}
	}

	//点击清除按钮页96孔板和动态生成的样本中心 重绘
	$("#rmv-sam").click(function() {
		$("#result-body").empty();
		$("#grid div").css("background-color", "#fff");
	})
}

function slide() {
	$(".tab-guo>li").click(function() {
		var col = $(this).css("background-color");
		$(this).css("background-color", "#1392E9").siblings("li").css("background-color", col)
		$(".template").eq($(this).index()).slideDown().siblings("div").slideUp();
	});
}
