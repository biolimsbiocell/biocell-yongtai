$(function() {
	//默认显示架子为8*8
	createShelf(8, 8, "#temShelf");
	//查询库内样本位置
	searchPostion()
	//选择入库或出库操作
	$(".myStorageType").click(function() {
		var index = $(".myStorageType").index(this);
		$(this).parent("#myStorageType").attr("type", index);
		$(this).addClass("active").siblings(".myStorageType").removeClass("active");
	});
	layer.config({
		offset: '300px'
	});
	//接收待存样本信息
	var options = table(true, "",
		"/storage/newPosition/selSampleInTemp.action", [{
			"data": "patientName",
			"title": biolims.common.sampleName,
		}, {
			"data": "code",
			"title": biolims.common.code,
		}, {
			"data": "sampleType",
			"title": biolims.common.sampleType,
		}, {
			"data": "concentration",
			"title":biolims.NewStoragePosition.concentration,
		}, {
			"data": "volume",
			"title": biolims.common.bulk,
		}, {
			"data": "sumTotal",
			"title": biolims.common.sumNum,
		}], null)
	sampleOrderTable = renderData($("#staySaveTem"), options);
	sampleOrderTable.on('draw', function() {
		var ichecks = $("#staySaveTem").find("tbody .icheck");
		ichecks.on('ifChanged', function() {
			var staySaveTemLength = $("#staySaveTem .selected").length;
			layer.msg(biolims.common.Youselect + staySaveTemLength + biolims.common.data);
		});
	});
	$("#search").unbind("click").click(function() {
		var stayCode = $("#searchText").val().split(" ");
		var scanCode = [];
		for(var i = 0; i < stayCode.length; i++) {
			if(stayCode[i] != "") {
				scanCode.push(stayCode[i].trim());
			}
		}
		if(scanCode.length == 0) {
			layer.msg(biolims.common.inputSampleCode);
		} else {
			var str = scanCode.join(",");
			sampleOrderTable.settings()[0].ajax.data = {
				strObj: str
			};
			sampleOrderTable.ajax.reload();
		}
	})
	//接收冰箱数据
	$.ajax({
		type: "post",
		data: {
			id: 0
		},
		url: ctx + "/storage/newPosition/showNewStoragePosition.action",
		success: function(data) {
			var data1 = JSON.parse(data);
			var info1 = data1.data;
			if(info1.length) {
				for(var i = 0; i < info1.length; i++) {
					var li1 = "<li class='menuGroup'><span class='groupTitle' id=" + info1[i].id + ">" + info1[i].name + "（" + info1[i].id + "）" + "</span><section class='sidebar'><ul class='sidebar-menu'></ul></section></li>";
					$(".parentWrap").append(li1);
				}
				var spans = $(".menuGroup").children("span");
				//默认加载第一个冰箱数据展开
				$.ajax({
					type: "post",
					data: {
						id: spans[0].id
					},
					url: ctx + "/storage/newPosition/showNewStoragePosition.action",
					success: function(data) {
						var data2 = JSON.parse(data);
						var info2 = data2.data;
						for(var i = 0; i < info2.length; i++) {
							var li2 = "<li class='treeview'><a href='####' id=" + info2[i].id + " class='ceng'><i class='fa fa-calculator'></i> <span>" + info2[i].name + "</span><span class='pull-right-container'><i class='fa fa-angle-left pull-right'></i></span></a><ul class='treeview-menu'></ul></li>";
							$(spans[0]).siblings("section").children("ul").append(li2);
						}
						$(spans[0]).siblings("section").slideDown();
						cengClick();
					}
				});
				//点击每个冰箱加载层数据并展开
				spans.click(function() {
					layer.load(2, {
						time: 500
					});
					if($(this).siblings("section").css("display") == "block") {
						$(this).siblings("section").slideUp();
					} else {
						var self = this;
						$(self).siblings("section").children("ul").empty();
						$.ajax({
							type: "post",
							url: ctx + "/storage/newPosition/showNewStoragePosition.action",
							data: {
								id: this.id
							},
							success: function(data) {
								var data2 = JSON.parse(data);
								var info2 = data2.data;
								for(var i = 0; i < info2.length; i++) {
									var li2 = "<li class='treeview'><a href='####' id=" + info2[i].id + " class='ceng'><i class='fa fa-calculator'></i> <span>" + info2[i].name + "</span><span class='pull-right-container'><i class='fa fa-angle-left pull-right'></i></span></a><ul class='treeview-menu'></ul></li>";
									$(self).siblings("section").children("ul").append(li2);
								}
								$(self).siblings("section").slideToggle().parent("li").siblings("li").find("section").hide();
								cengClick();
							}
						});
					}
				});

			} else {
				layer.msg(biolims.NewStoragePosition.noIce);
			}
		}
	});
	//冰箱的层点击事件
	function cengClick() {
		$(".ceng").click(function() {
			layer.load(2, {
				time: 500
			});
			var self2 = this;
			$(self2).siblings("ul").empty();
			$.ajax({
				type: "post",
				data: {
					id: this.id
				},
				url: ctx + "/storage/newPosition/showNewStoragePosition.action",
				success: function(data) {
					var data3 = JSON.parse(data);
					var info3 = data3.data;
					for(var i = 0; i < info3.length; i++) {
						if(info3[i].storageContainer) {
							var rowNum = info3[i].storageContainer.rowNum;
							var colNum = info3[i].storageContainer.colNum;

						} else {
							var rowNum = info3[i].storageContainer;
							var colNum = info3[i].storageContainer;

						}
						var li3 = "<li class='temShelf' row=" + rowNum + " col=" + colNum + " id=" + info3[i].id + "><a href='####'><i class='fa fa-adjust'></i>" + info3[i].name + "</a></li>";
						$(self2).siblings("ul").append(li3);
					}
					//点击对应储位   架子/排 背景色变化并生成对应规格架子
					choseShelf();
				}
			});
		})

	}

	//点击对应储位   架子/排 背景色变化并生成对应规格架子
	function choseShelf() {
		var temShelfs = $(".temShelf");
		var animate = $("<i class='fa fa-refresh fa-spin ani'></i>");
		for(var i = 0; i < temShelfs.length; i++) {
			$(temShelfs[i]).click(function() {
				//每次点击保持架子出现 盒子和信息框隐藏
				$(".chuBox").hide();
				$("#temBox .out").removeClass("out");
				$(".bloodBox").hide();
				$(".chushelf").show();
				$("#temShelf").empty();
				$("#temInfo").fadeOut(500);
				//背景色排他变化
				temShelfs.removeClass("chosedtemShelf");
				$(this).addClass("chosedtemShelf");
				//架子标题变化
				$(".chushelfTitle").html("/" + $(this).text());
				var row = $(this).attr("row");
				var col = $(this).attr("col");
				var id = $(this).attr("id");
				if(row !== "null") {
					//加载动画
					$("#temShelf").css("opacity", "0.5");
					$("#animate").append(animate);
					//根据要求生成不同规格架子
					createShelf(row, col, "#temShelf");
					//渲染架子里面的数据，点击架子里面的盒子显示对应规格
					renderShelf(id)
				}

			})
		}
	}
	//渲染架子/排 中的数据
	function renderShelf(id) {
		var temShelfs = $("#temShelf div");
		$.ajax({
			type: "post",
			data: {
				id: id
			},
			url: ctx + "/storage/newPosition/showNewStoragePositionData.action",
			success: function(data) {
				var data = JSON.parse(data);
				if(data.success) {
					var dataName = data.data;
					var box = data.data1;
					var shelfName = JSON.parse(dataName)[0];
					var hasbox = JSON.parse(box)[0];
					var index = 0;
					for(var k in shelfName) {
						if(shelfName[k] && hasbox[k]) {
							temShelfs[index].innerHTML = shelfName[k];
							temShelfs[index].setAttribute("boxId", hasbox[k]);
							temShelfs[index].style.backgroundColor = "#AED09E";
							temShelfs[index].className = "mytemShelf";
						}
						if(shelfName[k] && !hasbox[k] && shelfName[k] != "0") {
							temShelfs[index].innerHTML = shelfName[k];
							temShelfs[index].style.backgroundColor = "#FF7F5B";
							temShelfs[index].className = "myBloodShelf";
						}
						index++;
					}
					//数据加载好后加载动画自杀
					$("#temShelf").css("opacity", "1");
					$(".ani").remove();
				} else {
					//没有数据加载动画也自杀
					$("#temShelf").css("opacity", "1");
					$(".ani").remove();
				}
			}
		});
		shelfsClick(id);
	}
	//点击架子(或保存盒子，或查看盒子，或血袋入库，或血袋出库)
	function shelfsClick(id) {
		$("#temShelf div").click(function() {
			var type = $("#myStorageType").attr("type");
			layer.load(2, {
				time: 500
			});
			if(type == "0") { //样本入库
				if($(this).hasClass("mytemShelf")) {
					var boxId=this.getAttribute("boxId");
					var that = this;
					renderBox(id, that,boxId);
					temHover();
					temClick();
					saveTem();
				} else {
					layer.msg(biolims.NewStoragePosition.pleaseClick);
				}
			} else if(type == "1") { //血袋入库
				var selectedBlood = $("#staySaveTem .selected");
				if(selectedBlood.length) {
					if($(this).text()) {
						layer.msg(biolims.NewStoragePosition.thisLocation);
						return false;
					}
					var coord = $(this).attr("coord");
					layer.confirm(biolims.NewStoragePosition.confirmToStore, function(index) {
						var subId = $(".chuBox").find(".box-title").attr("shelfPostion");
						//拼接需要保存的数据			
						var code = "";
						var codeArr = [];
						selectedBlood.each(function(i, v) {
							codeArr.push($(v).children("td").eq(2).text());
						});
						var data1 = {
							code: codeArr
						};
						var str1 = JSON.stringify(data1);
						//位置 {"position":"A0001-01-01-A02"
						//样本编号 {"code":["DA1706150008","DA1706150009","DA1706150010","DA1706150011"]}
						$.ajax({
							type: "post",
							data: {
								code: str1,
								position: $(".chosedtemShelf").attr("id") + "-" + coord
							},
							url: ctx + "/storage/saveSampleInByBlood.action",
							success: function(data) {
								console.log(data);
								var data = JSON.parse(data);
								if(data.success) {
									layer.open({
										title: biolims.NewStoragePosition.submitSuccess,
										content: biolims.common.inputStorageTaskNum + data.id
									});
									selectedBlood.remove();
									$(".chosedtemShelf").click();
								}
							}
						})
						layer.close(index);
					});

				} else {
					layer.msg(biolims.NewStoragePosition.pleaseChoose);
				}

			} else if(type == "2") { //样本出库或血袋出库
				if($(this).hasClass("mytemShelf")) { //样本出库
					var boxId=this.getAttribute("boxId");
					var that = this;
					renderBox(id, that,boxId);
					temOutTable(id, that);
					outStorage();
				} else if($(this).hasClass("myBloodShelf")) { //血袋出库
					var that = this;
					bloodOutTable(id, that);
					temNextFlow();
				} else {
					layer.msg(biolims.NewStoragePosition.pleaseClick);
				}
			} else if(type == "3") { //新盒子选位置
				if($(this).text()) {
					layer.msg(biolims.NewStoragePosition.thisLocation)
				} else {
					var coord = $(this).attr("coord");
					layer.confirm(biolims.NewStoragePosition.confirmToStore, function(index) {
						var codeArr = [];
						var positionArr = [];
						$("#temBox .mysample").each(function(i, v) {
							codeArr.push(v.getAttribute("code"));
							positionArr.push(v.getAttribute("coord"));
						});
						$.ajax({
							type: "post",
							data: {
								code: codeArr.join(","),
								position: positionArr.join(","),
								id: $(".chuBox").find(".box-title").attr("shelfid"),
								iceBox: $(".chosedtemShelf").attr("id") + "-" + coord
							},
							url: ctx + "/storage/newPosition/saveSampleInTask.action",
							success: function(data) {
								console.log(data);
								var data = JSON.parse(data);
								if(data.success) {
									layer.open({
										title: biolims.NewStoragePosition.storageSuccess,
										content: biolims.common.inputStorageTaskNum + data.id
									});
									$("#myStorageType .active").removeClass("active");
									$(".myStorageType").eq(0).addClass("active");
									$("#myStorageType").attr("type", "0");
									$(".chosedtemShelf").click();
								}else{
									layer.msg(biolims.NewStoragePosition.storageFailed);
								}
							}
						})
						layer.close(index);
					});

				}
			} else if(type == "4") {
				if($(this).text()) {
					layer.msg(biolims.NewStoragePosition.thisLocation)
				} else {
					var that = this;
					layer.confirm(biolims.NewStoragePosition.confirmToMove, function(indexx) {
						var coord = $(that).attr("coord");
						// 样本编号
						// {"code":["DA1706150008","DA1706150009","DA1706150010","DA1706150011"]}
						// 旧盒子位置
						// {"oldPosition":["A0001-01-01-A02"}
						// 新盒子位置
						// {"newPosition":["A0001-01-01-A05"}
						$.ajax({
							type: "post",
							data: {
								code: "",
								oldPosition: $("#move").attr("oldPosition"),
								newPosition: $(".chosedtemShelf").attr("id") + "-" + coord,
								boxId:$(".chuBox").find(".box-title").attr("boxId")
							},
							url: ctx + "/storage/moveSample.action",
							success: function(data) {
								console.log(data);
								var data = JSON.parse(data);
								if(data.success) {
									layer.msg(biolims.NewStoragePosition.sampleShiftSuccess);
									var index = $(".myStorageType").index($("#myStorageType .active")[0]);
									$("#myStorageType").attr("type", index);
									$(".chosedtemShelf").click();
								}else{
									layer.msg(biolims.NewStoragePosition.sampleShiftFailed);
								}
							}
						})
						layer.close(indexx);
					});
				}
			} else if(type == "5") {
				if($(this).text()) {
					layer.msg(biolims.NewStoragePosition.thisLocation)
				} else {
					var that = this;
					layer.confirm(biolims.NewStoragePosition.confirmToStore, function(indexx) {
						var coord = $(that).attr("coord");
						// {"position":["A0001-01-01-A02-B03","A0001-01-01-A02-B04","A0001-01-01-A02-B05","A0001-01-01-A02-B06"]}
	// {"code":["DA1706150008","DA1706150009","DA1706150010","DA1706150011"]}
						$.ajax({
							type: "post",
							data: {
								code:$(".bloodBox").attr("code"),
								oldPosition:"",
								newPosition:$(".chosedtemShelf").attr("id") + "-" + coord,
							},
							url: ctx + "/storage/moveSampleByBlood.action",
							success: function(data) {
								console.log(data);
								var data = JSON.parse(data);
								if(data.success) {
									layer.msg(biolims.NewStoragePosition.bloodBag);
									var index = $(".myStorageType").index($("#myStorageType .active")[0]);
									$("#myStorageType").attr("type", index);
									$(".chosedtemShelf").click();
								}
							}
						})
						layer.close(indexx);
					});
				}
			}
		})

	}
	//渲染盒子中的样本
	function renderBox(id, that,boxId) {
		//架子隐藏
		$(".chushelf").fadeOut(500);
		$(".chuBox").find(".box-title").removeClass("newBox")
		var coord = that.getAttribute("coord");
		var subId = id + "-" + coord;
		//盒子出现,标题改变并加载动画
		$(".chuBox").show();
		$("#saveTem").show();
		var title = $(".chushelfTitle").parent(".box-title").text();
		$(".chuBox").find(".box-title").text(title + "/" + coord + biolims.sample.box);
		$(".chuBox").find(".box-title").attr("shelfPostion", subId);
		$(".chuBox").find(".box-title").attr("boxId", boxId);
		var animate = $("<i class='fa fa-refresh fa-spin ani'></i>");
		$("#animate2").append(animate);
		$.ajax({
			type: "post",
			data: {
				id: id,
				subId: subId
			},
			async: false,
			url: ctx + "/storage/newPosition/showNewStorageBox.action",
			success: function(data) {
				var data = JSON.parse(data);
				var row = data.rowNum;
				var col = data.colNum;
				var picCode = data.picCode;
				var info = data.data;
				var iconFont = JSON.parse(picCode)[0];
				var temInfo = JSON.parse(info);
				//创建对应规格盒子	
				createShelf(row, col, "#temBox");
				//显示己经存在的样本图标
				var temBox = $("#temBox div");
				var index = 0;
				for(var k in iconFont) {
					if(iconFont[k]) {
						temBox[index].children[0].className = "iconfont" + " " + iconFont[k];
						temBox[index].children[0].setAttribute("code", temInfo[0][k][0].code);
						temBox[index].children[0].setAttribute("type", temInfo[0][k][0].sampleType);
						temBox[index].children[0].setAttribute("samId", temInfo[0][k][0].sampleTypeId);
						temBox[index].children[0].setAttribute("patientName", temInfo[0][k][0].patientName);
						temBox[index].children[0].setAttribute("sampleCode", temInfo[0][k][0].sampleTypeId);
						temBox[index].children[0].setAttribute("location", temInfo[0][k][0].sampleCode);
						temBox[index].children[0].setAttribute("concentration", temInfo[0][k][0].concentration);
						temBox[index].children[0].setAttribute("volume", temInfo[0][k][0].volume);
						temBox[index].children[0].setAttribute("note", temInfo[0][k][0].note);
					}
					index++;
				}
				//数据加载好加载动画自杀
				$(".ani").remove();
			}
		});
		if($("#move").attr("movecode")) {
			var holes = $("#temBox div");
			var code = $("#move").attr("movecode").split(",");
			holes.click(function() {
				var span = this.children[0];
				if($(span).hasClass("iconfont")) {
					layer.msg(biolims.NewStoragePosition.pleaseClickNo);
				} else {
					var that = this;
					layer.confirm(biolims.NewStoragePosition.confirmToMoveSample, function(indexx) {
						var hh = parseInt(that.getAttribute("h"));
						var cc = code.length;
						var index = 0;
						for(var j = 0; j < holes.length; j++) {
							if(hh <= holes[j].getAttribute("h") && holes[j].getAttribute("h") < hh + cc) {
								holes[j].className = "mymove";
								holes[j].style.backgroundColor = "red";
								holes[j].setAttribute("code", code[index]);
								index++;
							}
						}
						layer.close(indexx);
						sampleMoveSave();
					});
				}
			});
			holes.mouseover(function() {
				var hh = parseInt(this.getAttribute("h"));
				var cc = code.length;
				for(var j = 0; j < holes.length; j++) {
					if(hh <= holes[j].getAttribute("h") && holes[j].getAttribute("h") < hh + cc) {
						holes[j].style.boxShadow = "0 0 3px red";
					}
				}
			});
			holes.mouseout(function() {
				for(var j = 0; j < holes.length; j++) {
					holes[j].style.boxShadow = "none";
				}
			});
		} else {
			sampleMove();
		}

	}
	//渲染样本出库的datatables
	function temOutTable(id, that) {
		$("#moveBlood").hide();
		$(".chushelf").fadeOut(500);
		$(".chuBox").find(".box-title").removeClass("newBox")
		var coord = that.getAttribute("coord");
		var subId = id + "-" + coord;
		//盒子出现,标题改变并加载动画
		$(".bloodBox").show();
		var title = $(".chushelfTitle").parent(".box-title").text();
		$(".bloodBox").find(".box-title").text(title + "/" + coord);
		$(".bloodBox .box-body").html('<table class="table" id="bloodTable"></table>');
		var options = table(true, "",
			"/storage/newPosition/selSampleInItemByBox.action?location=" + subId, [{
				
				"data": "patientName",
				"title": biolims.common.sampleName,
			}, {
				"data": "code",
				"title": biolims.common.code,
			}, {
				"data": "sampleType",
				"title": biolims.common.sampleType,
			},{
				"data": "location",
				"title":biolims.tStorageReagentBuySerial.position,
			}, {
				"data": "concentration",
				"title": biolims.NewStoragePosition.concentration,
			}, {
				"data": "volume",
				"title": biolims.common.bulk,
			}, {
				"data": "sumTotal",
				"title": biolims.common.sumNum,
			}], null)
		renderData($("#bloodTable"), options);
	}
	//渲染血袋出库的datatables
	function bloodOutTable(id, that) {
		$("#moveBlood").show();
		$(".chushelf").fadeOut(500);
		$(".chuBox").find(".box-title").removeClass("newBox")
		var coord = that.getAttribute("coord");
		var subId = id + "-" + coord;
		//盒子出现,标题改变并加载动画
		$(".bloodBox").show();
		var title = $(".chushelfTitle").parent(".box-title").text();
		$(".bloodBox").find(".box-title").text(title + "/" + coord);
		$(".bloodBox .box-body").html('<table class="table" id="bloodTable"></table>');
		var options = table(true, "",
			"/storage/selSampleByLocation.action?position=" + subId, [{
				"data": "code",
				"title": biolims.common.code,
				"createdCell": function(td, data, rowdata) {
					$(td).attr("samId", rowdata.sampleTypeId);
				}
			}, {
				"data": "sampleType",
				"title": biolims.common.sampleType,
			}, {
				"data": "location",
				"title": biolims.tStorageReagentBuySerial.position,
			}, {
				"data": "concentration",
				"title": biolims.NewStoragePosition.concentration,
			}, {
				"data": "volume",
				"title": biolims.common.bulk,
			}, {
				"data": "sumTotal",
				"title": biolims.common.sumNum,
			}], null);
		renderData($("#bloodTable"), options);
		bloodMove();
	}
	//样本入库保存
	function saveTem() {
		var subId = $(".chuBox").find(".box-title").attr("shelfPostion");
		$("#saveTem").unbind("click").click(function() {
			//拼接需要保存的数据			
			var code = "";
			var codeArr = [];
			var positionArr = [];
			$("#temBox .mysample").each(function(i, v) {
				codeArr.push(v.getAttribute("code"));
				positionArr.push(subId + "-" + v.getAttribute("coord"));
			});
			var data1 = {
				code: codeArr
			};
			var str1 = JSON.stringify(data1);
			var data2 = {
				position: positionArr
			};
			var str2 = JSON.stringify(data2);
			//位置 {"position":["A0001-01-01-A02-B03","A0001-01-01-A02-B04","A0001-01-01-A02-B05","A0001-01-01-A02-B06"]}
			//样本编号 {"code":["DA1706150008","DA1706150009","DA1706150010","DA1706150011"]}
			$.ajax({
				type: "post",
				data: {
					code: str1,
					position: str2
				},
				url: ctx + "/storage/saveSampleIn.action",
				success: function(data) {
					console.log(data);
					var data = JSON.parse(data);
					if(data.success) {
						layer.open({
							title: biolims.NewStoragePosition.submitSuccess,
							content: biolims.common.inputStorageTaskNum + data.id
						});
						$("#temBox div").off("mouseover");
					}
				}
			})

		})

	}
	//样本出库造作
	function outStorage() {
		if(!$("#move").attr("movecode")) {
			var temBox = $("#temBox div");
			temBox.click(function() {
				var span = this.children[0];
				if($(span).hasClass("iconfont")) {
					if($(this).hasClass("out")) {
						$(this).removeClass("out");
					} else {
						$(this).addClass("out");
					}
				}
			});
			temNextFlow();
		}

	}

	//下一步流向
	function temNextFlow() {
		//下一步流向操作。
		$(".nextStorage").unbind("click").click(function() {
			$("#nextStorBody").empty();
			var code = [];
			var samId = [];
			//判断是血袋出库还是样本出库
			if($("#temBox .out").length) { //样本出库
				$("#temBox .out").each(function(i, val) {
					code.push(val.children[0].getAttribute("code"));
					samId.push(val.children[0].getAttribute("samId"));
				});
			} else if($("#bloodTable .selected").length) { //血袋出库
				$("#bloodTable .selected").each(function(i, val) {
					code.push($(val).children("td").eq(1).text());
					samId.push($(val).children("td").eq(1).attr("samId"));
				});
			} else {
				layer.msg(biolims.NewStoragePosition.pleaseSelect);
				return false;
			}
			console.log(code);
			console.log(samId);
			//下步流向弹出框的显示或隐藏
			$(".nextStor").fadeIn(500);
			$(".rmvNextStor").click(function() {
				$(".nextStor").fadeOut(500);
			});
			for(var i = 0; i < code.length; i++) {
				var tr = "<tr><td><input type='checkbox' samId=" + samId[i] + " class='chose'/></td><td>" + code[i] + "</td><td></td><td></td><td class='notetd'><input type='text' class='note form-control'/></td></tr>";
				$("#nextStorBody").append(tr);
			}
			//全选与反选
			$("#allCheck").click(function() {
				$(this).parents("thead").siblings("tbody").find("input").prop("checked", $(this).prop("checked"));
			})
			//是否返库选择
			$("#dropdownMenu1").click(function() {
				var chose1 = $("#nextStorBody tr input:checked");
				var menu1 = $(this).siblings(".dropdown-menu");
				if(chose1.length == 0) {
					$(".alert-info").text(biolims.common.pleaseSelectData).fadeIn(500).fadeOut(2000);
					menu1.hide();
				} else {
					menu1.show();
					$(".back").unbind("click").click(function() {
						for(var i = 0; i < chose1.length; i++) {
							$(chose1[i]).parents("tr").children("td")[2].innerHTML = $(".back").text();
							menu1.hide();
						}
					});
					$(".noBack").unbind("click").click(function() {

						for(var i = 0; i < chose1.length; i++) {
							$(chose1[i]).parents("tr").children("td")[2].innerHTML = $(".noBack").text();
							menu1.hide();
						}
					});
				}
			})
			//下步流向选择
			$("#dropdownMenu2").unbind("click").click(function() {
				var chose = $("#nextStorBody tr input:checked");
				var menu = $(this).siblings(".dropdown-menu");
				menu.empty();
				if(chose.length == 0) {
					$(".alert-info").text(biolims.common.pleaseSelectData).fadeIn(500).fadeOut(2000);
					menu.hide();
				} else {
					menu.hide();
					var samId = chose[chose.length - 1].getAttribute("samId");
					$.ajax({
						type: "post",
						data: {
							sampleTypeId: samId,
							model:"SampleOut"
						},
						url: ctx + "/storage/newPosition/selNextFlowBySampleTypeId.action",
						success: function(data) {
							console.log(data);
							var data = JSON.parse(data);
							if(data.success) {
								var nextFlow = data.data[0].name.split(",");
								var nextFlowId = data.data[0].id.split(",");
								for(var i = 0; i < nextFlowId.length; i++) {
									var li = "<li flowId=" + nextFlowId[i] + "><a href='#'>" + nextFlow[i] + "</a></li>";
									menu.append(li);
								}
								menu.show();
								var lis = menu.find("li");
								lis.click(function() {
									for(var j = 0; j < chose.length; j++) {
										$(chose[j]).parents("tr").children("td")[3].innerHTML = $(this).text();
										$(chose[j]).parents("tr").children("td")[3].setAttribute("flowid", this.getAttribute("flowid"));
									}
									menu.hide();
								})
							}
						}
					});
				}

			})
			//扫码匹配
			$("#csan").unbind("click").click(function() {
				var scanText = $("#scanText").val().split(" ");
				var match = [];
				var a = "";
				for(var i = 0; i < code.length; i++) {
					for(var j = 0; j < scanText.length; j++) {
						if(code[i].trim() == scanText[j].trim()) {
							a = 0;
							break;
						} else {
							a = 1;
						}
					}
					if(a == 1) {
						match.push(code[i]);
					}
				}
				console.log(match)
				if(match.length == 0) {
					$(".alert-info").text(biolims.common.yesPleaseOutputStorage).fadeIn(500).fadeOut(2000);
				} else {
					var noMatch = match.join(",");
					console.log(noMatch)
					$(".alert-info").text(noMatch + biolims.common.codeScavengingMismatch).fadeIn(500).fadeOut(2000);
				}
			})
			//确认并出库
			$(".confirm").unbind("click").click(function() {
				var outType = $('#outSelect option:selected').text();
				var trss = $("#nextStorBody tr");
				var arr = [];
				trss.each(function(i, val) {
					var tr = val;
					var json = {};
					json.code = $(tr).find("td").eq(1).text();
					json.isDepot = $(tr).find("td").eq(2).text();
					json.nextFlowName = $(tr).find("td").eq(3).text();
					json.nextFlowId = $(tr).find("td").eq(3)[0].getAttribute("flowid");
					json.note = $(tr).find("td").eq(4).find("input").val();
					arr.push(json);
				});
				var data = {
					strobj: arr
				};
				var str = JSON.stringify(data);
				$.ajax({
					type: "post",
					data: {
						strobj: str,
						outType: outType
					},
					url: ctx + "/storage/saveSampleOut.action",
					//发送前验证下部流向是否为空
					beforeSend: function() {
						var trss = $("#nextStorBody tr");
						var isOk = "";
						trss.each(function(i, val) {
							var tr = val;
							var isReturn = $(tr).find("td").eq(2).text();
							if(isReturn) {} else {
								$(".alert-info").text(biolims.wk.pleaseifBackLib).fadeIn(500).fadeOut(2000);
								isOk = false;

							}
							var next = $(tr).find("td").eq(3).text();
							if(next) {} else {
								$(".alert-info").text(biolims.common.pleaseSelectNextFlow + '！').fadeIn(500).fadeOut(2000);
								isOk = false;

							}
						});
						if(outType == biolims.NewStoragePosition.outboundType) {
							$(".alert-info").text(biolims.common.pleaseChooseType).fadeIn(500).fadeOut(2000);
							isOk = false;
						}
						return isOk;
					},
					success: function(data) {
						console.log(data);
						var data = JSON.parse(data);
						if(data.success) {
							layer.open({
								title: biolims.NewStoragePosition.outboundSuccess,
								content: biolims.common.outputStorageTaskNum + data.id
							});
							$("#temBox .out").removeClass("out");
							$("#bloodTable .selected").remove();
							$(".nextStor").fadeOut(500);
						}
					}
				});

			});

		})

	}
	//查询库内样本位置
	function searchPostion() {
		$("#searchPositionBtn").unbind("click").click(function() {

			var stayCode = $("#searchPostionCode").val().split(" ");
			var scanCode = [];
			for(var i = 0; i < stayCode.length; i++) {
				if(stayCode[i] != "") {
					scanCode.push(stayCode[i].trim());
				}
			}
			if(scanCode.length == 0) {
				$(".alert-info").text(biolims.common.pleaseinputSampleCode).fadeIn(500).fadeOut(2000);
			} else {
				var str = scanCode.join(",");
				console.log(str)
				$.ajax({
					type: "post",
					data: {
						strobj: str
					},
					url: ctx + "/storage/newPosition/selCodeOrLocation.action",
					success: function(data) {

						var data = JSON.parse(data);

						if(data.success) {
							if(data.data.length !== 0) {
								$("#searchPositionBody").empty();
								var result = template("templatePosition", data);
								$("#searchPositionBody").html(result);
								$(".searchPositionBlock").fadeIn();
							}
							if(data.codes.length != 0 || data.locations.length != 0) {
								layer.msg(biolims.common.sample + '：' + data.codes + data.locations + biolims.common.notFind);
							}
							$(".rmvSearchPostion").click(function() {
								$(".searchPositionBlock").fadeOut();
							});
						}
					}
				});
			}

		})
	}
})

/** 
 * 根据要求生成要求规格的架子或盒子
 * @param  m => 行
 * @param  n => 列
 */
function createShelf(m, n, element) {
	var m = parseInt(m);
	var n = parseInt(n);
	$(element).empty();
	var arr = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "k", "L"];
	var tr0 = document.createElement("tr");
	var num = "";
	for(var i = 0; i <= n; i++) {
		num += '<td>' + i + '</td>';
	}
	tr0.innerHTML = num;
	tr0.children[0].innerHTML = "X";
	$(element).append(tr0);
	var h = 0;
	for(var i = 0; i < m; i++) {
		var tr = document.createElement("tr");
		var tds = "";
		var x = i + 1;
		for(var j = 0; j <= n; j++) {
			var jj = j < 10 ? "0" + j : j;
			tds += '<td><div x=' + arr[i] + ' y=' + j + ' z=' + (x - m) + ' coord=' + arr[i] + jj + '><span></span></div></td>';
			x += m;
		}
		tr.innerHTML = tds;
		tr.children[0].innerHTML = arr[i];
		$(element).append(tr);
	}
	var divs = $(element).find("div");
	for(var i = 0; i < divs.length; i++) {
		var h = i + 1;
		divs[i].setAttribute("h", h);
	}
}
//样本悬浮提示样本数量
function temHover() {
	var holes = $("#temBox div");
	holes.mouseover(function() {
		var hh = parseInt(this.getAttribute("h"));
		var cc = $("#staySaveTem .selected").length;
		for(var j = 0; j < holes.length; j++) {
			if(hh <= holes[j].getAttribute("h") && holes[j].getAttribute("h") < hh + cc) {
				holes[j].style.boxShadow = "0 0 3px #007BB6";
			}
		}
	});
	holes.mouseout(function() {
		for(var j = 0; j < holes.length; j++) {
			holes[j].style.boxShadow = "none";
		}
	});

}