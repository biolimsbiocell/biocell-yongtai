$(function() {
	$(".choseStorage").click(function() {
		layer.open({
			title: biolims.NewStoragePosition.selectBox,
			type: 2,
			offset: '60px',
			area: ["650px", "400px"],
			//btn: biolims.common.selected,
			btn: ['批量盒子入库', '放入样本'],
			content: [window.ctx + "/storage/newPosition/showNewBox.action", ''],
			btnAlign: 'c',
			yes: function(index, layero) { //批量空盒子入库
				var chosed = $(".layui-layer-iframe").find("iframe").contents().find("#3dStorageBoxDialog .selected");
				if(chosed.length) {
					$("#myStorageType").attr("type", 2);
					var id = [];
					chosed.each(function(i, v) {
						id.push($(v).children("td").eq(1).text());
					});
					layer.msg("请立即选择冰箱位置！");
					$(".chuBox").find(".box-title").attr("shelfid", id);
					$(".chushelf").show();
					$(".chuBox").hide();
				} else {
					layer.msg("请选择数据！");
					return false;
				}
				layer.close(index);
			},
			btn2: function(index, layero) { //查看单个盒子
				var chosed = $(".layui-layer-iframe").find("iframe").contents().find("#3dStorageBoxDialog .selected");
				if(chosed.length != 1) {
					layer.msg("请选择一条数据！");
					return false;
				}
				var id = chosed.children("td").eq(1).text();
				var name = chosed.children("td").eq(2).text();
				var row = chosed.children("td").eq(4).text();
				var col = chosed.children("td").eq(5).text();
				$(".chuBox").find(".box-title").text(name);
				$(".chuBox").find(".box-title").attr("shelfid", id);
				$(".chuBox").find(".box-title").addClass("newBox");
				$(".chushelf").hide();
				$(".chuBox").show();
				$("#saveTem").hide();
				$("#saveOutTem").show();
				$("#saveOutBox").show();
				//创建对应规格盒子	
				createShelf(row, col, "#temBox");
				//渲染盒子=》选择的未入库盒子中的样本
				renderOutBox(id);
				temHover();
				temClick();
				//保存数据
				$("#saveOutTem").click(function() {
					var codeArr = [];
					var positionArr = [];
					$("#temBox .mysample").each(function(i, v) {
						codeArr.push(v.getAttribute("code"));
						positionArr.push(v.getAttribute("coord"));
					});
					$.ajax({
						type:"post",
						url:ctx+"/storage/saveSampleByBox.action",
						data:{
							code:codeArr.join(","),
							position:positionArr.join(","),
							boxId:id
						},
						success:function(data){
							console.log(data);
							var data=JSON.parse(data);
							if(data.success){
								layer.msg("保存成功！！");
								createShelf(row, col, "#temBox");
								renderOutBox(id);
							}
						}
					});
				});
				
				//将当前盒子保存到架子
				$("#saveOutBox").click(function () {
					if($("#temBox .mysample").length){
						layer.msg("请先保存当前盒子数据！");
						return false;
					}
					$("#myStorageType").attr("type",3);
					layer.msg("请立即选择冰箱位置！");
					$(".chushelf").show();
					$(".chuBox").hide();
				})
				layer.close(index);
			},
			cancel: function(index, layero) {
				$("#myStorageType .active").removeClass("active");
				$(".myStorageType").eq(0).addClass("active");
				$("#myStorageType").attr("type", "0");
				layer.close(index)
			}
		})
	});

})

function temClick() {
	if(!$("#move").attr("movecode")) {
		var holes = $("#temBox div");
		holes.unbind("click").click(function() {
			var span = this.children[0];
			if($(span).hasClass("iconfont")) {
				var top = $(this).offset().top - $(this).height() - 35;
				var left = $(this).offset().left - $("#temInfo").width() - 35;
				$("#temInfo").fadeIn(500);
				$("#temInfo").css({
					"top": top + "px",
					"left": left + "px"
				});
				var x = this.getAttribute("x");
				var y = this.getAttribute("y");
				var coord = x + y;
				$("#temInfo .temInfoTitle").text(span.getAttribute("type"));
				$("#temInfo .temInfoSub i").text(span.getAttribute("code"));
				$("#temInfo input").eq(0).val(span.getAttribute("patientName"));
				$("#temInfo input").eq(1).val(span.getAttribute("sampleCode"));
				$("#temInfo input").eq(2).val(span.getAttribute("location"));
				$("#temInfo input").eq(3).val(span.getAttribute("concentration"));
				$("#temInfo input").eq(4).val(span.getAttribute("volume"));
				$("#temInfo input").eq(5).val(span.getAttribute("note"));
				$(".rmvTemInfo").click(function() {
					$("#temInfo").fadeOut(500);
				});
			} else {
				$("#temInfo").fadeOut(500);
				var selectedTemplate = $("#staySaveTem .selected");
				var hh = parseInt(this.getAttribute("h"));
				var cc = selectedTemplate.length;
				var xxx = [];
				var index = 0;
				for(var j = 0; j < holes.length; j++) {
					if(hh <= holes[j].getAttribute("h") && holes[j].getAttribute("h") < hh + cc) {
						holes[j].className = "mysample";
						holes[j].style.backgroundColor = "#007BB6";
						var code = $(selectedTemplate[index]).children("td").eq(2).text();
						holes[j].setAttribute("code", code);
						index++;
					}
				}
				selectedTemplate.remove();
			}
		});

	}
}

//样本移位置
function sampleMove() {
	$(".moveitem").click(function() {
		var index = $(".moveitem").index(this);
		$(this).parents("#move").attr("moveType", index);
		if(index == "0") { //样本移位置
			layer.msg(biolims.NewStoragePosition.pleaseClickTheSample);
			var holes = $("#temBox div");
			var code = [];
			Array.prototype.remove = function(val) {
				var index = this.indexOf(val);
				if(index > -1) {
					this.splice(index, 1);
				}
			};
			holes.unbind("click").click(function() {
				var span = this.children[0];
				if($(span).hasClass("iconfont")) {
					if($(this).hasClass("move")) {
						$(this).removeClass("move");
						code.remove(span.getAttribute("code"));
					} else {
						$(this).addClass("move");
						code.push(span.getAttribute("code"));
					}
					$("#move").attr("movecode", code);
				} else {
					var that = this;
					if(!code.length) {
						return false;
					}
					layer.confirm(biolims.NewStoragePosition.confirmToMoveSample, function(indexx) {
						var hh = parseInt(that.getAttribute("h"));
						var cc = code.length;
						var index = 0;
						for(var j = 0; j < holes.length; j++) {
							if(hh <= holes[j].getAttribute("h") && holes[j].getAttribute("h") < hh + cc) {
								holes[j].className = "mymove";
								holes[j].style.backgroundColor = "red";
								holes[j].setAttribute("code", code[index]);
								index++;
							}
						}
						layer.close(indexx);
						sampleMoveSave();
					});
				}
			});
			holes.mouseover(function() {
				var hh = parseInt(this.getAttribute("h"));
				var cc = code.length;
				for(var j = 0; j < holes.length; j++) {
					if(hh <= holes[j].getAttribute("h") && holes[j].getAttribute("h") < hh + cc) {
						holes[j].style.boxShadow = "0 0 3px red";
					}
				}
			});
			holes.mouseout(function() {
				for(var j = 0; j < holes.length; j++) {
					holes[j].style.boxShadow = "none";
				}
			});
		} else if(index == "1") { //盒子移位置
			layer.msg(biolims.NewStoragePosition.pleaseSelectThePosition);
			$("#myStorageType").attr("type", "4");
			$("#move").attr("oldPosition", $(".chuBox").find(".box-title").attr("shelfPostion"));
			$("#temBox div").each(function(i, val) {
				var span = this.children[0];
				if($(span).hasClass("iconfont")) {
					$(this).addClass("move");
				}

			});
		}
	});
}
//移动样本保存
function sampleMoveSave() {
	var subId = $(".chuBox").find(".box-title").attr("shelfPostion");
	//拼接需要保存的数据			
	var code = "";
	var codeArr = [];
	var positionArr = [];
	$("#temBox .mymove").each(function(i, v) {
		codeArr.push(v.getAttribute("code"));
		positionArr.push(subId + "-" + v.getAttribute("coord"));
	});
	var data1 = {
		code: codeArr
	};
	var str1 = JSON.stringify(data1);
	var data2 = {
		newPosition: positionArr
	};
	var str2 = JSON.stringify(data2);
	// {"position":["A0001-01-01-A02-B03","A0001-01-01-A02-B04","A0001-01-01-A02-B05","A0001-01-01-A02-B06"]}
	// {"code":["DA1706150008","DA1706150009","DA1706150010","DA1706150011"]}
	$.ajax({
		type: "post",
		data: {
			code: str1,
			newPosition: str2,
			oldPosition: ""
		},
		url: ctx + "/storage/moveSample.action",
		success: function(data) {
			console.log(data);
			var data = JSON.parse(data);
			if(data.success) {
				layer.msg(biolims.NewStoragePosition.sampleShiftSuccess);
				$("#move").removeAttr("movecode", "");
				$("#temBox div").unbind("mouseover");
				$("#temBox div").unbind("click");
				var shelfpostion = $(".chuBox .box-title").attr("shelfpostion").split("-");
				var coord = shelfpostion[shelfpostion.length - 1];
				$("#temShelf").find("div[coord=" + coord + "]").click();
			}
		}
	})
}
//血袋移位置
function bloodMove() {
	$("#moveBlood").click(function() {
		if($(".bloodBox .selected").length) {
			layer.msg(biolims.NewStoragePosition.youNeedToMove);
			$("#myStorageType").attr("type", "5");
			var codeArr = [];
			$(".bloodBox .selected").each(function(i, v) {
				codeArr.push($(v).children("td").eq(1).text());
			});
			$(".bloodBox").attr("code", codeArr);
		} else {
			layer.msg(biolims.NewStoragePosition.shiftSampleFirst);
		}
	});
}

function saveMoveShelfBox() {
	layer.confirm(biolims.NewStoragePosition.confirmToMove, function(indexx) {
		var newPosition = [];
		var hh = parseInt(that.attr("h"));
		var cc = $("#moveShelfBox").attr("oldPosition").split(",").length;
		for(var j = 0; j < holes.length; j++) {
			if(hh <= holes[j].getAttribute("h") && holes[j].getAttribute("h") < hh + cc) {
				newPosition.push(subId + "-" + holes[j].getAttribute("coord"));
				holes[j].style.backgroundColor = "red";
			}
		}
		console.log(oldPosition);
		console.log(newPosition);
		// 旧盒子位置
		// {"oldPosition":["A0001-01-01-A02"}
		// 新盒子位置
		// {"newPosition":["A0001-01-01-A05"}
		//							$.ajax({
		//								type: "post",
		//								data: {
		//									oldPosition: oldPosition,
		//									newPosition: newPosition,
		//								},
		//								url: ctx + "/storage/xxxxxx.action",
		//								success: function(data) {
		//									console.log(data);
		//									var data = JSON.parse(data);
		//									if(data.success) {
		//										layer.msg(biolims.NewStoragePosition.sampleShiftSuccess);
		//										$("#myStorageType").attr("type", "2");
		//										$(".chosedtemShelf").click();
		//									} else {
		//										layer.msg(biolims.NewStoragePosition.sampleShiftFailed);
		//									}
		//								}
		//							})
		layer.close(indexx);
	});
}