var kuType;
$(function() {
	kuType = "0";
	showPosition();
	
	layui.use('layer', function() {
		layer = layui.layer;
		layer.config({
			offset: '200px'
		});
	});
	//默认显示架子为8*8
	createShelf(8, 8, "#temShelf");
	//选择入库类型操作
	$(".myStorageType").click(function() {
		var index = $(".myStorageType").index(this);
		$(this).parent("#myStorageType").attr("type", index);
		$(this).addClass("active").siblings(".myStorageType").removeClass("active");
	});
	
	//接收待存样本信息
	var options = table(true, "",
		"/storage/newPosition/selSampleInTemp.action", [
			{
			"data": "patientName",
			"title": biolims.common.sampleName,
		}, 
		{
			"data": "code",
			"title": biolims.common.code,
		}, {
			"data": "sampleType",
			"title": biolims.common.sampleType,
		},
		{
			"data": "concentration",
			"title": biolims.NewStoragePosition.concentration,
		}, {
			"data": "volume",
			"title": biolims.common.bulk,
		}, 
		{
			"data": "sumTotal",
			"title": biolims.common.sumNum,
		}, {
			"data": "productName",
			"title": "产品",
		}, {
			"data": "sampleDeteyionName",
			"title": "检测项",
		}], null)
	sampleOrderTable = renderData($("#staySaveTem"), options);
	sampleOrderTable.on('draw', function() {
		var ichecks = $("#staySaveTem").find("tbody .icheck");
		ichecks.on('ifChanged', function() {
			var staySaveTemLength = $("#staySaveTem .selected").length;
			layer.msg(biolims.common.Youselect + staySaveTemLength + biolims.common.data);
		});
	});
	//查询待存样本
	$("#search").unbind("click").click(function() {
		var stayCode = $("#searchText").val().split(" ");
		var scanCode = [];
		for(var i = 0; i < stayCode.length; i++) {
			if(stayCode[i] != "") {
				scanCode.push(stayCode[i].trim());
			}
		}
		var str = scanCode.join(",");
		sampleOrderTable.settings()[0].ajax.data = {
			strObj: str
		};
		sampleOrderTable.ajax.reload();
	});
	
	//查询待存样本
	$("#searchType").unbind("click").click(function() {
		var sType = $("#searchSampleType").val().trim();
		/*var scanCode = [];
		for(var i = 0; i < stayCode.length; i++) {
			if(stayCode[i] != "") {
				scanCode.push(stayCode[i].trim());
			}
		}
		var str = scanCode.join(",");*/
		sampleOrderTable.settings()[0].ajax.data = {
			sType: sType
		};
		sampleOrderTable.ajax.reload();
	});
	
	
	//点击切换冰箱
	$("#oneself").click(function(){
		kuType = "0";
		$(".parentWrap").empty();
		showPosition();
	})
	$("#public").click(function(){
		kuType = "1";
		$(".parentWrap").empty();
		showPosition();
	})
	$("#sample").click(function(){
		kuType = "2";
		$(".parentWrap").empty();
		showPosition();
	})
	function showPosition(){
	//接收冰箱数据
	$.ajax({
		type: "post",
		data: {
			id: 0 ,
			kuType : kuType
		},
		url: ctx + "/storage/newPosition/showNewStoragePosition.action",
		success: function(data) {
			var data1 = JSON.parse(data);
			var info1 = data1.data;
			if(info1.length) {
				for(var i = 0; i < info1.length; i++) {
					var li1 = "<li class='menuGroup'><span class='groupTitle' id=" + info1[i].id + ">" + info1[i].name + "（" + info1[i].id + "）" + "</span><section class='sidebar'><ul class='sidebar-menu'></ul></section></li>";
					$(".parentWrap").append(li1);
				}
				var spans = $(".menuGroup").children("span");
				//默认加载第一个冰箱数据展开
				$.ajax({
					type: "post",
					data: {
						id: spans[0].id
					},
					url: ctx + "/storage/newPosition/showNewStoragePosition.action",
					success: function(data) {
						var data2 = JSON.parse(data);
						var info2 = data2.data;
						for(var i = 0; i < info2.length; i++) {
							var li2 = "<li class='treeview'><a href='####' id=" + info2[i].id + " class='ceng'><i class='fa fa-calculator'></i> <span>" + info2[i].name + "</span><span class='pull-right-container'><i class='fa fa-angle-left pull-right'></i></span></a><ul class='treeview-menu'></ul></li>";
							$(spans[0]).siblings("section").children("ul").append(li2);
						}
						$(spans[0]).siblings("section").slideDown();
						cengClick();
					}
				});
				//点击每个冰箱加载层数据并展开
				spans.click(function() {
					layer.load(2, {
						time: 500
					});
					if($(this).siblings("section").css("display") == "block") {
						$(this).siblings("section").slideUp();
					} else {
						var self = this;
						$(self).siblings("section").children("ul").empty();
						$.ajax({
							type: "post",
							url: ctx + "/storage/newPosition/showNewStoragePosition.action",
							data: {
								id: this.id
							},
							success: function(data) {
								var data2 = JSON.parse(data);
								var info2 = data2.data;
								for(var i = 0; i < info2.length; i++) {
									var li2 = "<li class='treeview'><a href='####' id=" + info2[i].id + " class='ceng'><i class='fa fa-calculator'></i> <span>" + info2[i].name + "</span><span class='pull-right-container'><i class='fa fa-angle-left pull-right'></i></span></a><ul class='treeview-menu'></ul></li>";
									$(self).siblings("section").children("ul").append(li2);
								}
								$(self).siblings("section").slideToggle().parent("li").siblings("li").find("section").hide();
								cengClick();
							}
						});
					}
				});

			} else {
				layer.msg("没有数据");
			}
		}
	});
	}
	//冰箱的层点击事件
	function cengClick() {
		$(".ceng").click(function() {
			layer.load(2, {
				time: 500
			});
			var self2 = this;
			$(self2).siblings("ul").empty();
			$.ajax({
				type: "post",
				data: {
					id: this.id
				},
				url: ctx + "/storage/newPosition/showNewStoragePosition.action",
				success: function(data) {
					var data3 = JSON.parse(data);
					var info3 = data3.data;
					for(var i = 0; i < info3.length; i++) {
						if(info3[i].storageContainer) {
							var rowNum = info3[i].storageContainer.rowNum;
							var colNum = info3[i].storageContainer.colNum;

						} else {
							var rowNum = info3[i].storageContainer;
							var colNum = info3[i].storageContainer;

						}
						var li3 = "<li class='temShelf' row=" + rowNum + " col=" + colNum + " id=" + info3[i].id + "><a href='####'><i class='fa fa-adjust'></i>" + info3[i].name + "</a></li>";
						$(self2).siblings("ul").append(li3);
					}
					//点击对应储位   架子/排 背景色变化并生成对应规格架子
					choseShelf();
				}
			});
		})

	}

	//点击对应储位   架子/排 背景色变化并生成对应规格架子
	function choseShelf() {
		var temShelfs = $(".temShelf");
		var animate = $("<i class='fa fa-refresh fa-spin ani'></i>");
		for(var i = 0; i < temShelfs.length; i++) {
			$(temShelfs[i]).click(function() {
				//每次点击保持架子出现 盒子和信息框隐藏
				$(".chuBox").hide();
				$("#temBox .out").removeClass("out");
				$(".bloodBox").hide();
				$(".chushelf").show();
				$("#temShelf").empty();
				$("#temInfo").fadeOut(500);
				//背景色排他变化
				temShelfs.removeClass("chosedtemShelf");
				$(this).addClass("chosedtemShelf");
				//架子标题变化
				$(".chushelfTitle").html("/" + $(this).text());
				var row = $(this).attr("row");
				var col = $(this).attr("col");
				var id = $(this).attr("id");
				if(row !== "null") {
					//加载动画
					$("#temShelf").css("opacity", "0.5");
					$("#animate").append(animate);
					//根据要求生成不同规格架子
					createShelf(row, col, "#temShelf");
					//渲染架子里面的数据，点击架子里面的盒子显示对应规格
					renderShelf(id)
				}

			})
		}
	}
	//渲染架子/排 中的数据
	function renderShelf(id) {
		var temShelfs = $("#temShelf div");
		$.ajax({
			type: "post",
			data: {
				id: id
			},
			url: ctx + "/storage/newPosition/showNewStoragePositionData.action",
			success: function(data) {
				var data = JSON.parse(data);
				if(data.success) {
					var dataName = data.data;
					var box = data.data1;
					var shelfName = JSON.parse(dataName)[0];
					var hasbox = JSON.parse(box)[0];
					var index = 0;
					for(var k in shelfName) {
						if(shelfName[k] && hasbox[k]) {
							temShelfs[index].innerHTML = shelfName[k];
							temShelfs[index].setAttribute("boxId", hasbox[k]);
							temShelfs[index].style.backgroundColor = "#AED09E";
							temShelfs[index].className = "mytemShelf";
						}
						if(shelfName[k] && !hasbox[k] && shelfName[k] != "0") {
							temShelfs[index].innerHTML = shelfName[k];
							temShelfs[index].style.backgroundColor = "#FF7F5B";
							temShelfs[index].className = "myBloodShelf";
						}
						index++;
					}
					//数据加载好后加载动画自杀
					$("#temShelf").css("opacity", "1");
					$(".ani").remove();
				} else {
					//没有数据加载动画也自杀
					$("#temShelf").css("opacity", "1");
					$(".ani").remove();
				}
			}
		});
		shelfsClick(id);
		//悬浮提示(判断是否是盒子入库)
		var hasNewbox = $(".chuBox").find(".box-title").attr("shelfid");
		if(hasNewbox) {
			var holes = $("#temShelf div");
			holes.mouseover(function() {
				var hh = parseInt(this.getAttribute("h"));
				var cc = hasNewbox.split(",").length;
				for(var j = 0; j < holes.length; j++) {
					if(hh <= holes[j].getAttribute("h") && holes[j].getAttribute("h") < hh + cc) {
						holes[j].style.boxShadow = "0 0 3px blue";
					}
				}
			});
			holes.mouseout(function() {
				for(var j = 0; j < holes.length; j++) {
					holes[j].style.boxShadow = "none";
				}
			});
		}

	}
	//点击架子(或保存盒子，或查看盒子，或血袋入库，或血袋出库)
	function shelfsClick(id) {
		$("#temShelf div").click(function() {
			var type = $("#myStorageType").attr("type");
			if(type == "0") { //样本入库
				if($(this).hasClass("mytemShelf")) {
					var boxId = this.getAttribute("boxId");
					var that = this;
					renderBox(id, that, boxId);
					temHover();
					temClick();
					saveTem();
					cleanTem();
				} else {
					layer.msg(biolims.NewStoragePosition.pleaseClick);
				}
			} else if(type == "1") { //血袋入库
				var selectedBlood = $("#staySaveTem .selected");
				if(selectedBlood.length) {
					if($(this).text()) {
						layer.msg(biolims.NewStoragePosition.thisLocation);
						return false;
					}
					var coord = $(this).attr("coord");
					layer.confirm(biolims.NewStoragePosition.confirmToStore, function(index) {
						var subId = $(".chuBox").find(".box-title").attr("shelfPostion");
						//拼接需要保存的数据			
						var code = "";
						var codeArr = [];
						selectedBlood.each(function(i, v) {
							codeArr.push($(v).children("td").eq(2).text());
						});
						var data1 = {
							code: codeArr
						};
						var str1 = JSON.stringify(data1);
						//位置 {"position":"A0001-01-01-A02"
						//样本编号 {"code":["DA1706150008","DA1706150009","DA1706150010","DA1706150011"]}
						$.ajax({
							type: "post",
							data: {
								code: str1,
								position: $(".chosedtemShelf").attr("id") + "-" + coord
							},
							url: ctx + "/storage/saveSampleInByBlood.action",
							success: function(data) {
								console.log(data);
								var data = JSON.parse(data);
								if(data.success) {
									layer.open({
										title: biolims.NewStoragePosition.submitSuccess,
										content: biolims.common.inputStorageTaskNum + data.id
									});
									selectedBlood.remove();
									$(".chosedtemShelf").click();
								}
							}
						})
						layer.close(index);
					});

				} else {
					layer.msg(biolims.NewStoragePosition.pleaseChoose);
				}

			} else if(type == "2") { //批量新盒子选位置
				if($(this).text()) {
					layer.msg(biolims.NewStoragePosition.thisLocation)
				} else {
					var ids = $(".chuBox").find(".box-title").attr("shelfid");
					var chosedtemShelfId=$(".chosedtemShelf").attr("id");
					var hh = parseInt(this.getAttribute("h"));
					var cc = ids.split(",").length;
					var iceboxs=[];
					var holes=$("#temShelf div");
					for(var j = 0; j < holes.length; j++) {
						if(hh <= holes[j].getAttribute("h") && holes[j].getAttribute("h") < hh + cc) {
							iceboxs.push(chosedtemShelfId+"-"+holes[j].getAttribute("coord"));
						}
					}
					layer.confirm("确认将盒子存储于此位置？", function(index) {
						$.ajax({
							type: "post",
							data: {
								id: ids,
								iceBox:iceboxs.join(",")
							},
							url: ctx + "/storage/newPosition/saveSampleInTask.action",
							success: function(data) {
								var data = JSON.parse(data);
								if(data.success) {
									layer.open({
										title: biolims.NewStoragePosition.storageSuccess,
										content: biolims.common.inputStorageTaskNum + data.id
									});
									$(".chuBox").find(".box-title").removeAttr("shelfid");
									$("#myStorageType .active").removeClass("active");
									$(".myStorageType").eq(0).addClass("active");
									$("#myStorageType").attr("type", "0");
									$(".chosedtemShelf").click();
								} else {
									layer.msg(biolims.NewStoragePosition.storageFailed);
								}
							}
						})
						layer.close(index);
					});

				}

			} else if(type == "3") { //新盒子选位置
				if($(this).text()) {
					layer.msg(biolims.NewStoragePosition.thisLocation)
				} else {
					var coord = $(this).attr("coord");
					layer.confirm("确认将盒子存储于此位置？", function(index) {
						$.ajax({
							type: "post",
							data: {
								id: $(".chuBox").find(".box-title").attr("shelfid"),
								iceBox: $(".chosedtemShelf").attr("id") + "-" + coord
							},
							url: ctx + "/storage/newPosition/saveSampleInTask.action",
							success: function(data) {
								var data = JSON.parse(data);
								if(data.success) {
									layer.open({
										title: biolims.NewStoragePosition.storageSuccess,
										content: biolims.common.inputStorageTaskNum + data.id
									});
									$(".chuBox").find(".box-title").removeAttr("shelfid");
									$("#myStorageType .active").removeClass("active");
									$(".myStorageType").eq(0).addClass("active");
									$("#myStorageType").attr("type", "0");
									$(".chosedtemShelf").click();
								} else {
									layer.msg(biolims.NewStoragePosition.storageFailed);
								}
							}
						})
						layer.close(index);
					});

				}
			}
		})
	}
	//渲染库中盒子中的样本
	function renderBox(id, that, boxId) {
		//架子隐藏
		$(".chushelf").fadeOut(500);
		$(".chuBox").find(".box-title").removeClass("newBox")
		var coord = that.getAttribute("coord");
		var subId = id + "-" + coord;
		//盒子出现,标题改变并加载动画
		$(".chuBox").show();
		$("#saveOutTem").hide();
		$("#saveOutBox").hide();
		$("#saveTem").show();
		$("#cleanTem").show();
		var title = $(".chushelfTitle").parent(".box-title").text();
		$(".chuBox").find(".box-title").text(title + "/" + coord + biolims.sample.box);
		$(".chuBox").find(".box-title").attr("shelfPostion", subId);
		$(".chuBox").find(".box-title").attr("boxId", boxId);
		var animate = $("<i class='fa fa-refresh fa-spin ani'></i>");
		$("#animate2").append(animate);
		$.ajax({
			type: "post",
			data: {
				id: id,
				subId: subId
			},
			async: false,
			url: ctx + "/storage/newPosition/showNewStorageBox.action",
			success: function(data) {
				var data = JSON.parse(data);
				var row = data.rowNum;
				var col = data.colNum;
				var picCode = data.picCode;
				var info = data.data;
				var iconFont = JSON.parse(picCode)[0];
				var temInfo = JSON.parse(info);
				//创建对应规格盒子	
				createShelf(row, col, "#temBox");
				//显示己经存在的样本图标
				var temBox = $("#temBox div");
				var index = 0;
				for(var k in iconFont) {
					if(iconFont[k]) {
						var hasTemInfo = temInfo[0][k][0];
						temBox[index].children[0].className = "iconfont" + " " + iconFont[k];
						temBox[index].children[0].setAttribute("code", hasTemInfo.code);
						temBox[index].children[0].setAttribute("type", hasTemInfo.sampleType);
						temBox[index].children[0].setAttribute("patientName", hasTemInfo.patientName);
						temBox[index].children[0].setAttribute("sampleCode", hasTemInfo.sampleCode);
						temBox[index].children[0].setAttribute("location", hasTemInfo.location);
						temBox[index].children[0].setAttribute("concentration", hasTemInfo.concentration);
						temBox[index].children[0].setAttribute("volume", hasTemInfo.volume);
						temBox[index].children[0].setAttribute("note", hasTemInfo.note);
					}
					index++;
				}
				//数据加载好加载动画自杀
				$(".ani").remove();
			}
		});
	}
	//样本入库保存
	function saveTem() {
		var subId = $(".chuBox").find(".box-title").attr("shelfPostion");
		$("#saveTem").unbind("click").click(function() {
			//拼接需要保存的数据			
			var code = "";
			var codeArr = [];
			var positionArr = [];
			$("#temBox .mysample").each(function(i, v) {
				codeArr.push(v.getAttribute("code"));
				positionArr.push(subId + "-" + v.getAttribute("coord"));
			});
			var data1 = {
				code: codeArr
			};
			var str1 = JSON.stringify(data1);
			var data2 = {
				position: positionArr
			};
			var str2 = JSON.stringify(data2);
			//位置 {"position":["A0001-01-01-A02-B03","A0001-01-01-A02-B04","A0001-01-01-A02-B05","A0001-01-01-A02-B06"]}
			//样本编号 {"code":["DA1706150008","DA1706150009","DA1706150010","DA1706150011"]}
			$.ajax({
				type: "post",
				data: {
					code: str1,
					position: str2
				},
				url: ctx + "/storage/saveSampleIn.action",
				success: function(data) {
					var data = JSON.parse(data);
					if(data.id=="EXIST"){
						top.layer.msg("请重新选择样本");
						return false;
					}
					if(data.success) {
						layer.open({
							title: biolims.NewStoragePosition.submitSuccess,
							content: biolims.common.inputStorageTaskNum + data.id
						});
						$("#temBox div").off("mouseover");
						var shelfpostion = $(".chuBox .box-title").attr("shelfpostion").split("-");
						var coord = shelfpostion[shelfpostion.length - 1];
						$("#temShelf").find("div[coord=" + coord + "]").click();
					}
				}
			})

		})

	}
})

/** 
 * 根据要求生成要求规格的架子或盒子
 * @param  m => 行
 * @param  n => 列
 */
function createShelf(m, n, element) {
	var m = parseInt(m);
	var n = parseInt(n);
	$(element).empty();
	var arr = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "k", "L"];
	var tr0 = document.createElement("tr");
	var num = "";
	for(var i = 0; i <= n; i++) {
		num += '<td>' + i + '</td>';
	}
	tr0.innerHTML = num;
	tr0.children[0].innerHTML = "X";
	$(element).append(tr0);
	var h = 0;
	for(var i = 0; i < m; i++) {
		var tr = document.createElement("tr");
		var tds = "";
		var x = i + 1;
		for(var j = 0; j <= n; j++) {
			var jj = j < 10 ? "0" + j : j;
			tds += '<td><div x=' + arr[i] + ' y=' + j + ' z=' + (x - m) + ' coord=' + arr[i] + jj + '><span></span></div></td>';
			x += m;
		}
		tr.innerHTML = tds;
		tr.children[0].innerHTML = arr[i];
		$(element).append(tr);
	}
	var divs = $(element).find("div");
	for(var i = 0; i < divs.length; i++) {
		var h = i + 1;
		divs[i].setAttribute("h", h);
	}
}
//样本悬浮提示样本数量
function temHover() {
	var holes = $("#temBox div");
	holes.mouseover(function() {
		var hh = parseInt(this.getAttribute("h"));
		var cc = $("#staySaveTem .selected").length;
		for(var j = 0; j < holes.length; j++) {
			if(hh <= holes[j].getAttribute("h") && holes[j].getAttribute("h") < hh + cc) {
				holes[j].style.boxShadow = "0 0 3px #007BB6";
			}
		}
	});
	holes.mouseout(function() {
		for(var j = 0; j < holes.length; j++) {
			holes[j].style.boxShadow = "none";
		}
	});

}

//渲染盒子=》选择的未入库盒子中的样本
function renderOutBox(id) {
	//架子隐藏
	var animate = $("<i class='fa fa-refresh fa-spin ani'></i>");
	$("#animate2").append(animate);
	$.ajax({
		type: "post",
		data: {
			boxId: id,
		},
		url: ctx + "/storage/selectSampleByBox.action",
		success: function(data) {
			//console.log(data);
			var info = JSON.parse(data);
			var sample=info.data;
			var picCodex=info.picCode;
			for(var i = 0; i < sample.length; i++) {
				var item=sample[i];
				var tembox = $("#temBox").find("div[coord=" + item.boxLocation + "]").children("span")[0];
				tembox.className = "iconfont" + " " + picCodex[i];
				tembox.setAttribute("code", item.code);
				tembox.setAttribute("type", item.sampleType);
				if(item.sampleInfo){
					tembox.setAttribute("patientName", item.sampleInfo.patientName);
				}else{
					tembox.setAttribute("patientName", "");
				}
				tembox.setAttribute("sampleCode", item.sampleCode);
				tembox.setAttribute("location", item.location);
				tembox.setAttribute("concentration", item.concentration);
				tembox.setAttribute("volume", item.volume);
				tembox.setAttribute("note", item.note);
			}
			//数据加载好加载动画自杀
			$(".ani").remove();
		}
	});
}
//清除已经扔到孔板的样本数据
function cleanTem() {
	$("#cleanTem").unbind("click").click(function() {
		var shelfpostion = $(".chuBox .box-title").attr("shelfpostion").split("-");
		var coord = shelfpostion[shelfpostion.length - 1];
		$("#temShelf").find("div[coord=" + coord + "]").click();
		sampleOrderTable.ajax.reload();
	})
}