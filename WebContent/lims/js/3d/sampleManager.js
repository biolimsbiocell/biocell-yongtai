$(function() {
	layui.use('layer', function() {
		layer = layui.layer;
		layer.config({
			offset: '200px'
		});
	});
	//默认显示架子为8*8
	createShelf(8, 8, "#temShelf");
	//查询库内样本位置
	searchPostion()
	//接收冰箱数据
	$.ajax({
		type: "post",
		data: {
			id: 0
		},
		url: ctx + "/storage/newPosition/showNewStoragePosition.action",
		success: function(data) {
			var data1 = JSON.parse(data);
			var info1 = data1.data;
			if(info1.length) {
				for(var i = 0; i < info1.length; i++) {
					var li1 = "<li class='menuGroup'><span class='groupTitle' id=" + info1[i].id + ">" + info1[i].name + "（" + info1[i].id + "）" + "</span><section class='sidebar'><ul class='sidebar-menu'></ul></section></li>";
					$(".parentWrap").append(li1);
				}
				var spans = $(".menuGroup").children("span");
				//默认加载第一个冰箱数据展开
				$.ajax({
					type: "post",
					data: {
						id: spans[0].id
					},
					url: ctx + "/storage/newPosition/showNewStoragePosition.action",
					success: function(data) {
						var data2 = JSON.parse(data);
						var info2 = data2.data;
						for(var i = 0; i < info2.length; i++) {
							var li2 = "<li class='treeview'><a href='####' id=" + info2[i].id + " class='ceng'><i class='fa fa-calculator'></i> <span>" + info2[i].name + "</span><span class='pull-right-container'><i class='fa fa-angle-left pull-right'></i></span></a><ul class='treeview-menu'></ul></li>";
							$(spans[0]).siblings("section").children("ul").append(li2);
						}
						$(spans[0]).siblings("section").slideDown();
						cengClick();
					}
				});
				//点击每个冰箱加载层数据并展开
				spans.click(function() {
					layer.load(2, {
						time: 500,
					});
					$(".iceChosed").removeClass("iceChosed");
					$(this).addClass("iceChosed");
					if($(this).siblings("section").css("display") == "block") {
						$(this).siblings("section").slideUp();
					} else {
						var self = this;
						$(self).siblings("section").children("ul").empty();
						$.ajax({
							type: "post",
							url: ctx + "/storage/newPosition/showNewStoragePosition.action",
							data: {
								id: this.id
							},
							success: function(data) {
								var data2 = JSON.parse(data);
								var info2 = data2.data;
								for(var i = 0; i < info2.length; i++) {
									var li2 = "<li class='treeview'><a href='####' id=" + info2[i].id + " class='ceng'><i class='fa fa-calculator'></i> <span>" + info2[i].name + "</span><span class='pull-right-container'><i class='fa fa-angle-left pull-right'></i></span></a><ul class='treeview-menu'></ul></li>";
									$(self).siblings("section").children("ul").append(li2);
								}
								$(self).siblings("section").slideToggle().parent("li").siblings("li").find("section").hide();
								cengClick();
							}
						});
					}
				});

			} else {
				layer.msg(biolims.NewStoragePosition.noIce);
			}
		}
	});
	//冰箱的层点击事件
	function cengClick() {
		$(".ceng").click(function() {
			$(".iceChosed").removeClass("iceChosed");
			$(this).addClass("iceChosed");
			layer.load(2, {
				time: 500
			});
			var self2 = this;
			$(self2).siblings("ul").empty();
			$.ajax({
				type: "post",
				data: {
					id: this.id
				},
				url: ctx + "/storage/newPosition/showNewStoragePosition.action",
				success: function(data) {
					var data3 = JSON.parse(data);
					var info3 = data3.data;
					for(var i = 0; i < info3.length; i++) {
						if(info3[i].storageContainer) {
							var rowNum = info3[i].storageContainer.rowNum;
							var colNum = info3[i].storageContainer.colNum;

						} else {
							var rowNum = info3[i].storageContainer;
							var colNum = info3[i].storageContainer;

						}
						var li3 = "<li class='temShelf' row=" + rowNum + " col=" + colNum + " id=" + info3[i].id + "><a href='####'><i class='fa fa-adjust'></i>" + info3[i].name + "</a></li>";
						$(self2).siblings("ul").append(li3);
					}
					//点击对应储位   架子/排 背景色变化并生成对应规格架子
					choseShelf();
				}
			});
		})

	}

	//点击对应储位   架子/排 背景色变化并生成对应规格架子
	function choseShelf() {
		var temShelfs = $(".temShelf");
		var animate = $("<i class='fa fa-refresh fa-spin ani'></i>");
		for(var i = 0; i < temShelfs.length; i++) {
			$(temShelfs[i]).click(function() {
				$(".iceChosed").removeClass("iceChosed");
				$(this).addClass("iceChosed");
				//每次点击保持架子出现 盒子和信息框隐藏
				$(".chuBox").hide();
				$("#temBox .out").removeClass("out");
				$(".bloodBox").hide();
				$(".chushelf").show();
				$("#temShelf").empty();
				$("#temInfo").fadeOut(500);
				//背景色排他变化
				temShelfs.removeClass("chosedtemShelf");
				$(this).addClass("chosedtemShelf");
				//架子标题变化
				$(".chushelfTitle").html("/" + $(this).text());
				var row = $(this).attr("row");
				var col = $(this).attr("col");
				var id = $(this).attr("id");
				if(row !== "null") {
					//加载动画
					$("#temShelf").css("opacity", "0.5");
					$("#animate").append(animate);
					//根据要求生成不同规格架子
					createShelf(row, col, "#temShelf");
					//渲染架子里面的数据，点击架子里面的盒子显示对应规格
					renderShelf(id)
				}

			})
		}
	}
	//渲染架子/排 中的数据
	function renderShelf(id) {
		var temShelfs = $("#temShelf div");
		$.ajax({
			type: "post",
			data: {
				id: id
			},
			url: ctx + "/storage/newPosition/showNewStoragePositionData.action",
			success: function(data) {
				var data = JSON.parse(data);
				if(data.success) {
					var dataNum = data.data;
					var box = data.data1;
					var shelfNum = JSON.parse(dataNum)[0];
					var hasbox = JSON.parse(box)[0];
					var index = 0;
					for(var k in shelfNum) {
						if(shelfNum[k] && hasbox[k]) {
							temShelfs[index].innerHTML = shelfNum[k];
							temShelfs[index].setAttribute("boxId", hasbox[k]);
							temShelfs[index].style.backgroundColor = "#AED09E";
							temShelfs[index].className = "mytemShelf";
						}
						if(!hasbox[k] && shelfNum[k] != "0") {
							temShelfs[index].innerHTML = shelfNum[k];
							temShelfs[index].style.backgroundColor = "#FF7F5B";
							temShelfs[index].className = "myBloodShelf";
						}
						index++;
					}
					//数据加载好后加载动画自杀
					$("#temShelf").css("opacity", "1");
					$(".ani").remove();
					//点击移位（移动架子上的盒子或者血袋）
					$("#moveShelfBox").click(function() {
						layer.msg(biolims.NewStoragePosition.pleaseSelectThePosition);
						$("#myStorageType").attr("type", "6");
					});
				} else {
					//没有数据加载动画也自杀
					$("#temShelf").css("opacity", "1");
					$(".ani").remove();
				}
			}
		});
		shelfsClick(id);
		if($("#moveShelfBox").attr("oldPosition")) {
			var holes = $("#temShelf div");
			holes.mouseover(function() {
				var hh = parseInt(this.getAttribute("h"));
				var cc = $("#moveShelfBox").attr("oldPosition").split(",").length;
				for(var j = 0; j < holes.length; j++) {
					if(hh <= holes[j].getAttribute("h") && holes[j].getAttribute("h") < hh + cc) {
						holes[j].style.boxShadow = "0 0 3px red";
					}
				}
			});
			holes.mouseout(function() {
				for(var j = 0; j < holes.length; j++) {
					holes[j].style.boxShadow = "none";
				}
			});
		}
	}
	//点击架子(或保存盒子，或查看盒子，或血袋入库，或血袋出库)
	function shelfsClick(id) {
		var oldtemShelfPosition = [];
		$("#temShelf div").click(function() {
			var type = $("#myStorageType").attr("type");
			if(type == "2") { //样本出库或血袋出库
				if($(this).hasClass("mytemShelf")) { //样本出库
					var boxId = this.getAttribute("boxId");
					var that = this;
					renderBox(id, that, boxId);
					temOutTable(id, that);
					outStorage();
				} else if($(this).hasClass("myBloodShelf")) { //血袋出库
					var that = this;
					bloodOutTable(id, that);
					temNextFlow();
				} else {
					layer.msg(biolims.NewStoragePosition.pleaseClick);
				}
			} else if(type == "4") { //样本盒子移动位置
				if($(this).text()) {
					layer.msg(biolims.NewStoragePosition.thisLocation)
				} else {
					var that = this;
					layer.confirm(biolims.NewStoragePosition.confirmToMove, function(indexx) {
						var coord = $(that).attr("coord");
						// 样本编号
						// {"code":["DA1706150008","DA1706150009","DA1706150010","DA1706150011"]}
						// 旧盒子位置
						// {"oldPosition":["A0001-01-01-A02"}
						// 新盒子位置
						// {"newPosition":["A0001-01-01-A05"}
						$.ajax({
							type: "post",
							data: {
								code: "",
								oldPosition: $("#move").attr("oldPosition"),
								newPosition: $(".chosedtemShelf").attr("id") + "-" + coord,
								boxId: $(".chuBox").find(".box-title").attr("boxId")
							},
							url: ctx + "/storage/moveSample.action",
							success: function(data) {
								console.log(data);
								var data = JSON.parse(data);
								if(data.success) {
									layer.msg(biolims.NewStoragePosition.sampleShiftSuccess);

									$("#myStorageType").attr("type", "2");
									$(".chosedtemShelf").click();
								} else {
									layer.msg(biolims.NewStoragePosition.sampleShiftFailed);
								}
							}
						})
						layer.close(indexx);
					});
				}
			} else if(type == "5") { //血袋移动位置
				if($(this).text()) {
					layer.msg(biolims.NewStoragePosition.thisLocation)
				} else {
					var that = this;
					layer.confirm(biolims.NewStoragePosition.confirmToStore, function(indexx) {
						var coord = $(that).attr("coord");
						// {"position":["A0001-01-01-A02-B03","A0001-01-01-A02-B04","A0001-01-01-A02-B05","A0001-01-01-A02-B06"]}
						// {"code":["DA1706150008","DA1706150009","DA1706150010","DA1706150011"]}
						$.ajax({
							type: "post",
							data: {
								code: $(".bloodBox").attr("code"),
								oldPosition: "",
								newPosition: $(".chosedtemShelf").attr("id") + "-" + coord,
							},
							url: ctx + "/storage/moveSampleByBlood.action",
							success: function(data) {
								console.log(data);
								var data = JSON.parse(data);
								if(data.success) {
									layer.msg(biolims.NewStoragePosition.bloodBag);
									$("#myStorageType").attr("type", "2");
									$(".chosedtemShelf").click();
								}
							}
						})
						layer.close(indexx);
					});
				}
			} else if(type == "6") { //架子移位置
				//样本移位置
				var holes = $("#temShelf div");
				//var oldPosition = [];
				Array.prototype.remove = function(val) {
					var index = this.indexOf(val);
					if(index > -1) {
						this.splice(index, 1);
					}
				};
				var that = $(this);
				var subId = $(".chosedtemShelf").attr("id");
				if(that.hasClass("mytemShelf") || that.hasClass("myBloodShelf")) {
					var coord = subId + "-" + that.attr("coord");
					if(that.hasClass("move")) {
						that.removeClass("move");
						oldtemShelfPosition.remove(coord);
					} else {
						that.addClass("move");
						oldtemShelfPosition.push(coord);
					}
					$("#moveShelfBox").attr("oldPosition", oldtemShelfPosition);
				} else {
					var cc = $("#moveShelfBox").attr("oldPosition").split(",").length;
					if(!cc) {
						return false;
					}
					layer.confirm(biolims.NewStoragePosition.confirmToMove, function(indexx) {
						var newPosition = [];
						var hh = parseInt(that.attr("h"));
						for(var j = 0; j < holes.length; j++) {
							if(hh <= holes[j].getAttribute("h") && holes[j].getAttribute("h") < hh + cc) {
								newPosition.push(subId + "-" + holes[j].getAttribute("coord"));
								holes[j].style.backgroundColor = "red";
							}
						}
						
						// 旧盒子位置
						// {"oldPosition":["A0001-01-01-A02"}
						// 新盒子位置
						// {"newPosition":["A0001-01-01-A05"}
													$.ajax({
														type: "post",
														data: {
															oldPosition:$("#moveShelfBox").attr("oldPosition"),
															newPosition:newPosition.join(","),
														},
														url: ctx + "/storage/newPosition/moveBox.action",
														success: function(data) {
															console.log(data);
															var data = JSON.parse(data);
															if(data.success) {
																$("#moveShelfBox").removeAttr("oldPosition");
																holes.unbind("mouseover");
																layer.msg(biolims.NewStoragePosition.sampleShiftSuccess);
																$("#myStorageType").attr("type", "2");
																$(".chosedtemShelf").click();
															} else {
																layer.msg(biolims.NewStoragePosition.sampleShiftFailed);
															}
														}
													})
						layer.close(indexx);
					});
				}

				holes.mouseover(function() {
					var hh = parseInt(this.getAttribute("h"));
					var cc = $("#moveShelfBox").attr("oldPosition").split(",").length;
					for(var j = 0; j < holes.length; j++) {
						if(hh <= holes[j].getAttribute("h") && holes[j].getAttribute("h") < hh + cc) {
							holes[j].style.boxShadow = "0 0 3px red";
						}
					}
				});
				holes.mouseout(function() {
					for(var j = 0; j < holes.length; j++) {
						holes[j].style.boxShadow = "none";
					}
				});

			}
		})

	}
	//渲染盒子中的样本
	function renderBox(id, that, boxId) {
		//架子隐藏
		$(".chushelf").fadeOut(500);
		$(".chuBox").find(".box-title").removeClass("newBox")
		var coord = that.getAttribute("coord");
		var subId = id + "-" + coord;
		//盒子出现,标题改变并加载动画
		$(".chuBox").show();
		$("#saveTem").show();
		var title = $(".chushelfTitle").parent(".box-title").text();
		$(".chuBox").find(".box-title").text(title + "/" + coord + biolims.sample.box);
		$(".chuBox").find(".box-title").attr("shelfPostion", subId);
		$(".chuBox").find(".box-title").attr("boxId", boxId);
		var animate = $("<i class='fa fa-refresh fa-spin ani'></i>");
		$("#animate2").append(animate);
		$.ajax({
			type: "post",
			data: {
				id: id,
				subId: subId
			},
			async: false,
			url: ctx + "/storage/newPosition/showNewStorageBox.action",
			success: function(data) {
				var data = JSON.parse(data);
				var row = data.rowNum;
				var col = data.colNum;
				var picCode = data.picCode;
				var info = data.data;
				var iconFont = JSON.parse(picCode)[0];
				var temInfo = JSON.parse(info);
				//创建对应规格盒子	
				createShelf(row, col, "#temBox");
				//显示己经存在的样本图标
				var temBox = $("#temBox div");
				var index = 0;
				for(var k in iconFont) {
					if(iconFont[k]) {
						var hasTemInfo = temInfo[0][k][0];
						temBox[index].children[0].className = "iconfont" + " " + iconFont[k];
						temBox[index].children[0].setAttribute("code", hasTemInfo.code);
						temBox[index].children[0].setAttribute("type", hasTemInfo.sampleType);
						temBox[index].children[0].setAttribute("samId", hasTemInfo.sampleTypeId);
					}
					index++;
				}
				//数据加载好加载动画自杀
				$(".ani").remove();
			}
		});
		if($("#move").attr("movecode")) {
			var holes = $("#temBox div");
			var code = $("#move").attr("movecode").split(",");
			holes.click(function() {
				var span = this.children[0];
				if($(span).hasClass("iconfont")) {
					layer.msg(biolims.NewStoragePosition.pleaseClickNo);
				} else {
					var that = this;
					layer.confirm(biolims.NewStoragePosition.confirmToMoveSample, function(indexx) {
						var hh = parseInt(that.getAttribute("h"));
						var cc = code.length;
						var index = 0;
						for(var j = 0; j < holes.length; j++) {
							if(hh <= holes[j].getAttribute("h") && holes[j].getAttribute("h") < hh + cc) {
								holes[j].className = "mymove";
								holes[j].style.backgroundColor = "red";
								holes[j].setAttribute("code", code[index]);
								index++;
							}
						}
						layer.close(indexx);
						sampleMoveSave();
					});
				}
			});
			holes.mouseover(function() {
				var hh = parseInt(this.getAttribute("h"));
				var cc = code.length;
				for(var j = 0; j < holes.length; j++) {
					if(hh <= holes[j].getAttribute("h") && holes[j].getAttribute("h") < hh + cc) {
						holes[j].style.boxShadow = "0 0 3px red";
					}
				}
			});
			holes.mouseout(function() {
				for(var j = 0; j < holes.length; j++) {
					holes[j].style.boxShadow = "none";
				}
			});
		} else {
			sampleMove();
		}

	}
	//渲染样本出库的datatables
	function temOutTable(id, that) {
		$("#moveBlood").hide();
		$(".nextStorage").eq(1).hide();
		$(".chushelf").fadeOut(500);
		$(".chuBox").find(".box-title").removeClass("newBox")
		var coord = that.getAttribute("coord");
		var subId = id + "-" + coord;
		//盒子出现,标题改变并加载动画
		$(".bloodBox").show();
		var title = $(".chushelfTitle").parent(".box-title").text();
		$(".bloodBox").find(".box-title").text(title + "/" + coord);
		$(".bloodBox .box-body").html('<table class="table" id="bloodTable"></table>');
		var options = table(true, "",
			"/storage/newPosition/selSampleInItemByBox.action?location=" + subId, [{

				"data": "patientName",
				"title": biolims.common.sampleName,
			}, {
				"data": "code",
				"title": biolims.common.code,
				"createdCell": function(td, data) {
					$(td).attr("code", data).addClass("code");
				}
			}, {
				"data": "sampleType",
				"title": biolims.common.sampleType,
			}, {
				"data": "location",
				"title": biolims.tStorageReagentBuySerial.position,
			}, {
				"data": "concentration",
				"title": biolims.NewStoragePosition.concentration,
			}, {
				"data": "volume",
				"title": biolims.common.bulk,
			}, {
				"data": "sumTotal",
				"title": biolims.common.sumNum,
			}], null)
		var sampleTable = renderData($("#bloodTable"), options);
		// 选择数据并提示
		sampleTable.on('draw', function() {
			var index = 0;
			$("#bloodTable .icheck").on(
				'ifChanged',
				function(event) {
					var code = $(this).parents("tr").children("td").eq(2).text()
					if($(this).is(':checked')) {
						$("#temBox").find("span[code='" + code + "']").parent("div").addClass("out");
					} else {
						$("#temBox").find("span[code='" + code + "']").parent("div").removeClass("out");
					}
				});
		});
	}
	//渲染血袋出库的datatables
	function bloodOutTable(id, that) {
		$("#moveBlood").show();
		$(".nextStorage").eq(1).show();
		$(".chushelf").fadeOut(500);
		$(".chuBox").find(".box-title").removeClass("newBox")
		var coord = that.getAttribute("coord");
		var subId = id + "-" + coord;
		//盒子出现,标题改变并加载动画
		$(".bloodBox").show();
		var title = $(".chushelfTitle").parent(".box-title").text();
		$(".bloodBox").find(".box-title").text(title + "/" + coord);
		$(".bloodBox .box-body").html('<table class="table" id="bloodTable"></table>');
		var options = table(true, "",
			"/storage/selSampleByLocation.action?position=" + subId, [{
				"data": "code",
				"title": biolims.common.code,
				"createdCell": function(td, data, rowdata) {
					$(td).attr("samId", rowdata.sampleTypeId);
				}
			}, {
				"data": "sampleType",
				"title": biolims.common.sampleType,
			}, {
				"data": "location",
				"title": biolims.tStorageReagentBuySerial.position,
			}, {
				"data": "concentration",
				"title": biolims.NewStoragePosition.concentration,
			}, {
				"data": "volume",
				"title": biolims.common.bulk,
			}, {
				"data": "sumTotal",
				"title": biolims.common.sumNum,
			}], null);
		renderData($("#bloodTable"), options);
		bloodMove();
	}
	//样本入库保存
	function saveTem() {
		var subId = $(".chuBox").find(".box-title").attr("shelfPostion");
		$("#saveTem").unbind("click").click(function() {
			//拼接需要保存的数据			
			var code = "";
			var codeArr = [];
			var positionArr = [];
			$("#temBox .mysample").each(function(i, v) {
				codeArr.push(v.getAttribute("code"));
				positionArr.push(subId + "-" + v.getAttribute("coord"));
			});
			var data1 = {
				code: codeArr
			};
			var str1 = JSON.stringify(data1);
			var data2 = {
				position: positionArr
			};
			var str2 = JSON.stringify(data2);
			//位置 {"position":["A0001-01-01-A02-B03","A0001-01-01-A02-B04","A0001-01-01-A02-B05","A0001-01-01-A02-B06"]}
			//样本编号 {"code":["DA1706150008","DA1706150009","DA1706150010","DA1706150011"]}
			$.ajax({
				type: "post",
				data: {
					code: str1,
					position: str2
				},
				url: ctx + "/storage/saveSampleIn.action",
				success: function(data) {
					console.log(data);
					var data = JSON.parse(data);
					if(data.success) {
						layer.open({
							title: biolims.NewStoragePosition.submitSuccess,
							content: biolims.common.inputStorageTaskNum + data.id
						});
						$("#temBox div").off("mouseover");
					}
				}
			})

		})

	}
	//样本出库造作
	function outStorage() {
		if(!$("#move").attr("movecode")) {
			var temBox = $("#temBox div");
			temBox.click(function() {
				var span = this.children[0];
				if($(span).hasClass("iconfont")) {
					var that = $(this);
					var code = that.children("span").attr("code");
					if(that.hasClass("out")) {
						that.removeClass("out");
						$("#bloodTable").find(".code[code=" + code + "]").parent("tr").find(".icheck").iCheck('uncheck');

					} else {
						$("#bloodTable").find(".code[code=" + code + "]").parent("tr").find(".icheck").iCheck('check');
						that.addClass("out");
					}
				}
			});
			temNextFlow();
		}

	}

	//下一步流向
	function temNextFlow() {
		//下一步流向操作。
		$(".nextStorage").unbind("click").click(function() {
			$("#nextStorBody").empty();
			var code = [];
			var samId = [];
			//判断是血袋出库还是样本出库
			if($("#temBox .out").length) { //样本出库
				console.log("样本出库");
				$("#temBox .out").each(function(i, val) {
					code.push(val.children[0].getAttribute("code"));
					samId.push(val.children[0].getAttribute("samId"));
				});
			} else if($("#bloodTable .selected").length) { //血袋出库
				console.log("血袋出库")
				$("#bloodTable .selected").each(function(i, val) {
					code.push($(val).children("td").eq(1).text());
					samId.push($(val).children("td").eq(1).attr("samId"));
				});
			} else {
				layer.msg(biolims.NewStoragePosition.pleaseSelect);
				return false;
			}
			console.log(code);
			console.log(samId);
			//下步流向弹出框的显示或隐藏
			$(".nextStor").fadeIn(500);
			$(".rmvNextStor").click(function() {
				$(".nextStor").fadeOut(500);
			});
			for(var i = 0; i < code.length; i++) {
				var tr = "<tr><td><input type='checkbox' samId=" + samId[i] + " class='chose'/></td><td>" + code[i] + "</td><td></td><td></td><td class='notetd'><input type='text' class='note form-control'/></td></tr>";
				$("#nextStorBody").append(tr);
			}
			//全选与反选
			$("#allCheck").click(function() {
				$(this).parents("thead").siblings("tbody").find("input").prop("checked", $(this).prop("checked"));
			})
			//是否返库选择
			$("#dropdownMenu1").click(function() {
				var chose1 = $("#nextStorBody tr input:checked");
				var menu1 = $(this).siblings(".dropdown-menu");
				if(chose1.length == 0) {
					$(".alert-info").text(biolims.common.pleaseSelectData).fadeIn(500).fadeOut(2000);
					menu1.hide();
				} else {
					menu1.show();
					$(".back").unbind("click").click(function() {
						for(var i = 0; i < chose1.length; i++) {
							$(chose1[i]).parents("tr").children("td")[2].innerHTML = $(".back").text();
							menu1.hide();
						}
					});
					$(".noBack").unbind("click").click(function() {

						for(var i = 0; i < chose1.length; i++) {
							$(chose1[i]).parents("tr").children("td")[2].innerHTML = $(".noBack").text();
							menu1.hide();
						}
					});
				}
			})
			//下步流向选择
			$("#dropdownMenu2").unbind("click").click(function() {
				var chose = $("#nextStorBody tr input:checked");
				var menu = $(this).siblings(".dropdown-menu");
				menu.empty();
				if(chose.length == 0) {
					$(".alert-info").text(biolims.common.pleaseSelectData).fadeIn(500).fadeOut(2000);
					menu.hide();
				} else {
					menu.hide();
					var samId = chose[chose.length - 1].getAttribute("samId");
					$.ajax({
						type: "post",
						data: {
							sampleTypeId: samId,
							model:"SampleOut"
						},
						url: ctx + "/storage/newPosition/selNextFlowBySampleTypeId.action",
						success: function(data) {
							var data = JSON.parse(data);
							if(data.success) {
								var nextFlowArr=[];
								var nextFlowIdArr=[];
								for(var a=0;a<data.data.length;a++){
									nextFlowArr.push(data.data[a].name);
									nextFlowIdArr.push(data.data[a].id);
									var li = "<li flowId=" + data.data[a].id + "><a href='#'>" + data.data[a].name + "</a></li>";
									menu.append(li);
								}
								/*var nextFlowArr = data.data[0].name;
								var nextFlowIdArr = data.data[0].id;*/
								/*if(nextFlowArr.length>0){
									var nextFlow=nextFlowArr.split(",");
								}else{
									console.log("没数据！");
									return false;
								}
								if(nextFlowIdArr.length>0){
									var nextFlowId = nextFlowIdArr.split(",");
								}*/
								/*for(var i = 0; i < nextFlowId.length; i++) {
									var li = "<li flowId=" + nextFlowId[i] + "><a href='#'>" + nextFlow[i] + "</a></li>";
									menu.append(li);
								}*/
								menu.show();
								var lis = menu.find("li");
								lis.click(function() {
									for(var j = 0; j < chose.length; j++) {
										$(chose[j]).parents("tr").children("td")[3].innerHTML = $(this).text();
										$(chose[j]).parents("tr").children("td")[3].setAttribute("flowid", this.getAttribute("flowid"));
									}
									menu.hide();
								})
							}
						}
					});
				}

			})
			//扫码匹配
			$("#csan").unbind("click").click(function() {
				var scanText = $("#scanText").val().split(" ");
				var match = [];
				var a = "";
				for(var i = 0; i < code.length; i++) {
					for(var j = 0; j < scanText.length; j++) {
						if(code[i].trim() == scanText[j].trim()) {
							a = 0;
							break;
						} else {
							a = 1;
						}
					}
					if(a == 1) {
						match.push(code[i]);
					}
				}
				console.log(match)
				if(match.length == 0) {
					$(".alert-info").text(biolims.common.yesPleaseOutputStorage).fadeIn(500).fadeOut(2000);
				} else {
					var noMatch = match.join(",");
					console.log(noMatch)
					$(".alert-info").text(noMatch + biolims.common.codeScavengingMismatch).fadeIn(500).fadeOut(2000);
				}
			})
			//确认并出库
			$(".confirm").unbind("click").click(function() {
				var outType = $('#outSelect option:selected').text();
				var trss = $("#nextStorBody tr");
				var arr = [];
				trss.each(function(i, val) {
					var tr = val;
					var json = {};
					json.code = $(tr).find("td").eq(1).text();
					json.isDepot = $(tr).find("td").eq(2).text();
					json.nextFlowName = $(tr).find("td").eq(3).text();
					json.nextFlowId = $(tr).find("td").eq(3)[0].getAttribute("flowid");
					json.note = $(tr).find("td").eq(4).find("input").val();
					arr.push(json);
				});
				var data = {
					strobj: arr
				};
				var str = JSON.stringify(data);
				$.ajax({
					type: "post",
					data: {
						strobj: str,
						outType: outType
					},
					url: ctx + "/storage/saveSampleOut.action",
					//发送前验证下部流向是否为空
					beforeSend: function() {
						var trss = $("#nextStorBody tr");
						var isOk = "";
						trss.each(function(i, val) {
							var tr = val;
							var isReturn = $(tr).find("td").eq(2).text();
							if(isReturn) {} else {
								$(".alert-info").text(biolims.wk.pleaseifBackLib).fadeIn(500).fadeOut(2000);
								isOk = false;

							}
							var next = $(tr).find("td").eq(3).text();
							if(next) {} else {
								$(".alert-info").text(biolims.common.pleaseSelectNextFlow + '！').fadeIn(500).fadeOut(2000);
								isOk = false;

							}
						});
						if(outType == biolims.NewStoragePosition.outboundType) {
							$(".alert-info").text(biolims.common.pleaseChooseType).fadeIn(500).fadeOut(2000);
							isOk = false;
						}
						return isOk;
					},
					success: function(data) {
						console.log(data);
						var data = JSON.parse(data);
						if(data.success) {
							layer.open({
								title: biolims.NewStoragePosition.outboundSuccess,
								content: biolims.common.outputStorageTaskNum + data.id
							});
							$("#temBox .out").removeClass("out");
							$("#bloodTable .selected").remove();
							$(".nextStor").fadeOut(500);
						}
					}
				});

			});

		})

	}
	//查询库内样本位置
	function searchPostion() {
		$("#searchPositionBtn").unbind("click").click(function() {
			var stayCode = $("#searchPostionCode").val().split(" ");
			var scanCode = [];
			for(var i = 0; i < stayCode.length; i++) {
				if(stayCode[i] != "") {
					scanCode.push(stayCode[i].trim());
				}
			}
			if(scanCode.length == 0) {
				$(".alert-info").text(biolims.common.pleaseinputSampleCode).fadeIn(500).fadeOut(2000);
			} else {
				var str = scanCode.join(",");
				console.log(str)
				$.ajax({
					type: "post",
					data: {
						strobj: str
					},
					url: ctx + "/storage/newPosition/selCodeOrLocation.action",
					success: function(data) {

						var data = JSON.parse(data);

						if(data.success) {
							if(data.data.length !== 0) {
								$("#searchPositionBody").empty();
								var result = template("templatePosition", data);
								$("#searchPositionBody").html(result);
								$(".searchPositionBlock").fadeIn();
							}
							if(data.codes.length != 0 || data.locations.length != 0) {
								layer.msg(biolims.common.sample + '：' + data.codes + data.locations + biolims.common.notFind);
							}
							$(".rmvSearchPostion").click(function() {
								$(".searchPositionBlock").fadeOut();
							});
						}
					}
				});
			}

		})
	}
})

/** 
 * 根据要求生成要求规格的架子或盒子
 * @param  m => 行
 * @param  n => 列
 */
function createShelf(m, n, element) {
	var m = parseInt(m);
	var n = parseInt(n);
	$(element).empty();
	var arr = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "k", "L"];
	var tr0 = document.createElement("tr");
	var num = "";
	for(var i = 0; i <= n; i++) {
		num += '<td>' + i + '</td>';
	}
	tr0.innerHTML = num;
	tr0.children[0].innerHTML = "X";
	$(element).append(tr0);
	var h = 0;
	for(var i = 0; i < m; i++) {
		var tr = document.createElement("tr");
		var tds = "";
		var x = i + 1;
		for(var j = 0; j <= n; j++) {
			var jj = j < 10 ? "0" + j : j;
			tds += '<td><div x=' + arr[i] + ' y=' + j + ' z=' + (x - m) + ' coord=' + arr[i] + jj + '><span></span></div></td>';
			x += m;
		}
		tr.innerHTML = tds;
		tr.children[0].innerHTML = arr[i];
		$(element).append(tr);
	}
	var divs = $(element).find("div");
	for(var i = 0; i < divs.length; i++) {
		var h = i + 1;
		divs[i].setAttribute("h", h);
	}
}
//样本悬浮提示样本数量
function temHover() {
	var holes = $("#temBox div");
	holes.mouseover(function() {
		var hh = parseInt(this.getAttribute("h"));
		var cc = $("#staySaveTem .selected").length;
		for(var j = 0; j < holes.length; j++) {
			if(hh <= holes[j].getAttribute("h") && holes[j].getAttribute("h") < hh + cc) {
				holes[j].style.boxShadow = "0 0 3px #007BB6";
			}
		}
	});
	holes.mouseout(function() {
		for(var j = 0; j < holes.length; j++) {
			holes[j].style.boxShadow = "none";
		}
	});

}