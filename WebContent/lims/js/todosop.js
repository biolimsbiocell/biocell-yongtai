$(document).ready(function() {
	//点击全选
	check_all($(".check-all"));

	function check_all(element) {
		element.click(function() {
			$(this).parents("thead").siblings("tbody").find("input").prop("checked", $(this).prop("checked"));
			if($(this).parents("thead").siblings("tbody").find("input").prop("checked")) {
				$(this).parents("thead").siblings("tbody").find("tr").css("background-color", "#90caaf");
			} else {
				$(this).parents("thead").siblings("tbody").find("tr").css("background-color", "white");
			}
		});
	}
	//点击shift全选
	function shift_click_grid(element) {
		var rem = new Array();
		element.click(function(e) {
			var e = window.event || e;
			if($(this).find("input").prop("checked")) {
				$(this).find("input").prop("checked", false);
				$(this).css("background-color", "#FFF");
			} else {
				$(this).find("input").prop("checked", true);
				$(this).css("background-color", "#90caaf");
			}
			//shift配合鼠标左键全选
			rem.push(element.index($(this)))
			if(e.shiftKey) {
				var iMin = Math.min(rem[rem.length - 2], rem[rem.length - 1])
				var iMax = Math.max(rem[rem.length - 2], rem[rem.length - 1])
				for(i = iMin; i <= iMax; i++) {
					element.eq(i).find("input").prop("checked", true);
					element.eq(i).css("background-color", "#90caaf");
				}

			}
			//阻止事件冒泡
			e = e || window.event;
			if(e.stopPropagation) {
				e.stopPropagation();
			} else {
				e.cancelBubble = true;
			}

		});
		//与程序无关，是禁止鼠标选中文字。点选的时候容易选中文字 太讨厌 。  
		document.onselectstart = function(event) {
			event = window.event || event;
			event.returnValue = false;
		}
	}
	//拖拽函数
	function drag() {
		document.getElementById("grid-body").onmousedown = function() {
			document.getElementById("mask2").style.display = "none";
		}
		document.getElementById("grid-body").onmouseup = function() {
			document.getElementById("mask2").style.display = "none";

			document.onmousemove = function(e) {
				document.getElementById("mask2").style.display = "block";
				document.getElementById("mask2").style.left = e.pageX + 10 + "px";
				document.getElementById("mask2").style.top = e.pageY + 10 + 'px';
				document.getElementById("mask2").innerHTML = $("#grid-body tr input:checked").length;
			}
		}
		var holes = document.getElementById("grid").getElementsByTagName("div");
		for(var i = 0; i < holes.length; i++) {
			holes[i].onmouseover = function() {
				var zz = parseInt(this.getAttribute("z"));
				var yy = parseInt(this.getAttribute("y"));
				var cc = parseInt($("#grid-body tr input:checked").length);
				for(var j = 0; j < holes.length; j++) {

					if(holes[j].getAttribute("y") >= yy && zz <= holes[j].getAttribute("z") && holes[j].getAttribute("z") < zz + cc) {
						holes[j].className = "myhover";
					} else {
						holes[j].className = null;
					}
				}

			}
			holes[i].onmouseout = function() {
				for(var j = 0; j < holes.length; j++) {
					holes[j].className = null;
				}
			}
			holes[i].onclick = function() {
				document.onmousemove = null;
				document.getElementById("mask2").style.display = "none";
				var zz = parseInt(this.getAttribute("z"));
				var yy = parseInt(this.getAttribute("y"));
				var cc = parseInt($("#grid-body tr input:checked").length);
				var xxx = [];
				var yyy = [];
				for(var j = 0; j < holes.length; j++) {
					if(holes[j].getAttribute("y") >= yy && zz <= holes[j].getAttribute("z") && holes[j].getAttribute("z") < zz + cc) {
						holes[j].style.backgroundColor = "#007BB6";
						if(holes[j].getAttribute("y") == yy) {
							xxx.push(holes[j].getAttribute("x"));
							yyy.push(holes[j].getAttribute("y"));
						}

					} else {
						holes[j].className = null;
					}
				}
				for(var j = 0; j < holes.length; j++) {
					if(holes[j].getAttribute("y") >= yy && zz <= holes[j].getAttribute("z") && holes[j].getAttribute("z") < zz + cc) {
						holes[j].style.backgroundColor = "#007BB6";
						if(holes[j].getAttribute("y") == yy + 1) {
							xxx.push(holes[j].getAttribute("x"));
							yyy.push(holes[j].getAttribute("y"));
						}

					} else {
						holes[j].className = null;
					}
				}
				for(var j = 0; j < holes.length; j++) {
					if(holes[j].getAttribute("y") >= yy && zz <= holes[j].getAttribute("z") && holes[j].getAttribute("z") < zz + cc) {
						holes[j].style.backgroundColor = "#007BB6";
						if(holes[j].getAttribute("y") == yy + 2) {
							xxx.push(holes[j].getAttribute("x"));
							yyy.push(holes[j].getAttribute("y"));
						}

					} else {
						holes[j].className = null;
					}
				}
				for(var j = 0; j < holes.length; j++) {
					if(holes[j].getAttribute("y") >= yy && zz <= holes[j].getAttribute("z") && holes[j].getAttribute("z") < zz + cc) {
						holes[j].style.backgroundColor = "#007BB6";
						if(holes[j].getAttribute("y") == yy + 3) {
							xxx.push(holes[j].getAttribute("x"));
							yyy.push(holes[j].getAttribute("y"));
						}

					} else {
						holes[j].className = null;
					}
				}
				for(var j = 0; j < holes.length; j++) {
					if(holes[j].getAttribute("y") >= yy && zz <= holes[j].getAttribute("z") && holes[j].getAttribute("z") < zz + cc) {
						holes[j].style.backgroundColor = "#007BB6";
						if(holes[j].getAttribute("y") == yy + 4) {
							xxx.push(holes[j].getAttribute("x"));
							yyy.push(holes[j].getAttribute("y"));
						}

					} else {
						holes[j].className = null;
					}
				}

				//因为jQuery获取的grid元素是逆序的，所以反向遍历
				var trs = $("#grid-body tr input:checked").parents("tr");
				for(var i = 0; i < trs.length / 2; i++) {
					var temp = trs[i];
					trs[i] = trs[trs.length - 1 - i];
					trs[trs.length - 1 - i] = temp;
				}
				var gridData = [];
				trs.each(function(i, val) {
					var tr = val;
					var json = {
						code: "",
						sampleCode: "",
						productName: "",
						sampleNum: "",
						sampleType: "",
						productNum: "",
					}
					json.code = $(tr).find("td").eq(1).text();
					json.sampleCode = $(tr).find("td").eq(2).text();
					json.productName = $(tr).find("td").eq(3).text();
					json.sampleNum = $(tr).find("td").eq(4).text();
					json.sampleType = $(tr).find("td").eq(5).text();
					json.productNum = $(tr).find("td").eq(6).text();
					gridData.push(json);
				});
				var resultData = {
					gridData: gridData
				};
				var result2 = template("clickGridTemplate", resultData);
				$("#result-body").append(result2);
				for(var i = 0; i < xxx.length; i++) {
					$("#result-body tr")[i].getElementsByTagName("input")[0].value = xxx[i] + yyy[i];
				}
			}
		}

		//点击清除按钮页96孔板和动态生成的样本中心 重绘
		$("#rmv-sam").click(function() {
			document.getElementById("mask2").style.display = "none";
			$("#result-body").empty();
			$("#grid div").css("background-color", "#fff");
		})
	}

	//var formId = sessionStorage.getItem("formId");
	var formId = "HSTQ1703100005";
	var str = "";
	//conten1和content2页面交互数据
	$.ajax({
		type: "post",
		url: ctx+"/main/showTaskItem.action",
		data: {
			formId: formId,
		},
		datatype: "json",
		success: function(data) {
			var info = JSON.parse(data);
			//更新样本数据
			var result1 = template("sampleTemplate", info);
			$("#sample-body").html(result1);
			$("#sample-body tr").find("input").prop("checked", true);
			$("#sample-body tr").css("background-color", "#90caaf");
			//更新sop
			$("#sop-body").find("button").text(info["template"]);
			//点击 下一步跳转到第二页面
			$("#nextbtn1").click(function(e) {
				$("#content1").fadeOut(500);
				$("#content2").fadeIn(500);
				//更新孔板目标数据
				$("#grid-body").html(result1);
				$("#grid-body tr").find("input").prop("checked", true);
				$("#grid-body tr").css("background-color", "#90caaf");
				//更新孔板结果数据
				var result2 = template("gridTemplate", info);
				$("#result-body").html(result2);
				//存储后台坐标
				var arrx = [];
				var arry = [];
				var arrId = [];
				var dicSample = [];
				var infoArr = info["data"];
				for(var i = 0; i < infoArr.length; i++) {
					for(var k in infoArr[i]) {
						if(k == "rowCode") {
							arrx.push(infoArr[i][k])
						}
						if(k == "colCode") {
							arry.push(infoArr[i][k])
						}
						if(k == "id") {
							arrId.push(infoArr[i][k])
						}
						if(k == "dicSampleType") {
							dicSample.push(infoArr[i][k])
						}
					}
				}
				//将后台坐标反映至96孔板
				var holes = document.getElementById("grid").getElementsByTagName("div");
				for(var i = 0; i < arrx.length; i++) {
					for(var z = 0; z < holes.length; z++) {
						if(holes[z].getAttribute("x") == arrx[i] && holes[z].getAttribute("y") == arry[i]) {
							holes[z].style.backgroundColor = "#007BB6";
						}
					}
				}
				//将坐标及样本数据反映至result-body
				var trs = $("#result-body tr");
				for(var i = 0; i < trs.length; i++) {
					trs[i].getElementsByTagName("input")[0].value = arrx[i] + arry[i];
				}
				shift_click_grid($("#grid-body tr"));
				drag();
				//点击下一步向后台发送 实验数据json
				$("#nextbtn2").click(function() {
					//拼接content2中样本结果数据
					var trss = $("#result-body tr");
					var arr = [];
					trss.each(function(i, val) {
						var tr = val;
						var json = {
							id: "",
							cord: "",
							code: "",
							sampleCode: "",
							productName: "",
							sampleNum: "",
							sampleType: "",
							productNum: "",
							dicSampleType: ""
						}
						json.id = arrId[i];
						json.cord = $(tr).find("input").eq(0).val();
						json.code = $(tr).find("input").eq(1).val();
						json.sampleCode = $(tr).find("input").eq(2).val();
						json.productName = $(tr).find("input").eq(3).val();
						json.sampleNum = $(tr).find("input").eq(4).val();
						json.sampleType = $(tr).find("input").eq(5).val();
						json.productNum = $(tr).find("input").eq(6).val();
						json.dicSampleType = dicSample[i];
						arr.push(json);
					});
					var data = {
						strobj: arr
					};
					str = JSON.stringify(data);
					//发送数据并验证
					$.ajax({
						type: "post",
						url: ctx+"/main/saveItem.action",
						data: {
							strobj: str
						},
						datatype: "json",
						success: function(data) {
							data = JSON.parse(data);
							if(data["success"]) {
								$("#content2").hide(500);
								$("#content3").show(500);
							} else {
								alert("保存失败");
							}
						}
					});

				});

			});

			//点击上一步返回页面1
			$("#prev2").click(function() {
				$("#content2").hide(500);
				$("#content1").show(500);
			});

		}
	});
	//conten3页面数据交互
	$.ajax({
		type: "post",
		url: ctx+"/main/showTemplate.action",
		data: {
			formId: formId
		},
		datatype: "json",
		success: function(data) {
			var data = JSON.parse(data);
			var tem = data.templeat;
			var reagent = data.reagent;
			var cos = data.cos;
			var steps = "";
			//动态生成实验步骤
			for(var i = 0; i < tem.length; i++) {
				steps += "<tr><td><div>" + tem[i].name + "<img src='../../img/down.png' width='30px' /></div></td></tr>"
			}
			$(".step-head table").html(steps)
			var divs = $(".step-head").find("div");
			//点击步骤生成步骤明细   原辅料明细  设备明细
			divs.click(function() {
				var index = divs.index(this);
				if(index == 0) {
					$(".step-body").css("top", $(this).position().top);
				}
				if(index > 0) {
					$(".step-body").css("top", $(this).position().top - 80);
				}
				$("#reagent").html("");
				$("#instrument").html("");
				$("#reagent").append($("<h4><i class='fa fa-eyedropper'></i>原辅料 <button class='btn btn-xs btn-info pull-right moreRet'>增加原辅料</button></h4>"));
				$("#instrument").append($("<h4><i class='fa  fa-flask'></i>设备<button class='btn btn-xs btn-info pull-right moreIst'>增加设备</button></h4>"))
				for(var i = 0; i < tem.length; i++) {
					if(tem[i].name == $(this).text()) {
						$("#tem input")[0].id = tem[i].id;
						$("#tem input")[0].code = tem[i].code;
						$("#tem input")[0].value = tem[i].testUserName;
						$("#tem input")[1].value = tem[i].startTime;
						$("#tem input")[2].value = tem[i].endTime;
						$("#tem input")[3].value = tem[i].state;
						$("#tem div")[0].innerHTML = tem[i].note;
						//通过步骤code关联原辅料itemId 生成原辅料明细
						var code = tem[i].code;
						for(var j = 0; j < reagent.length; j++) {
							if(code == reagent[j].itemId) {
								var table = $("<table class='table no-border'><tr><td><input type='checkbox' class='checkRet'><i class='fa fa-trash-o remRet'></i><label>原辅料名称：<input type='text' id=" + reagent[j].id + " value=" + reagent[j].name + " disabled></label> </td><td><label>原辅料批次：<input type='text' value=" + reagent[j].batch + " /></label></td><td><label> 过期日期：<input type='text' value=" + reagent[j].expireDate + "></label></td><td><label>sn：<input type='text' value=" + reagent[j].sn + " ></label></td></tr><tr><td> <label style='padding-left: 41px;'>是否正常：<input type='text'  value=" + reagent[j].isGood + " ></label></td><td><label>样本数量：<input type='text' value=" + reagent[j].sampleNum + " disabled></label></td><td><label>单个用量：<input type='text' value=" + reagent[j].oneNum + " ></label></td><td><label>用量：<input type='text' value=" + reagent[j].num + " disabled></label></td></tr></table>");

								if(table.find("input").eq(5).val() == "1") {
									table.find("input").eq(5).val("是");
								}
								if(table.find("input").eq(5).val() == "0") {
									table.find("input").eq(5).val("否");
								}
								$("#reagent").append(table);

							}
						}
						//通过步骤code关联设备itemId 生成设备明细
						for(var z = 0; z < cos.length; z++) {
							if(code == cos[z].itemId) {
								var table = $("<table class='table no-border' style='margin-bottom: 20px !important;'><tr><td><input type='checkbox' class='checkIns' /><i class='fa fa-trash-o remIst'></i><label>设备编号：<input type='text' value=" + cos[z].code + " id=" + cos[z].id + " ></label></td><td><label> 设备名称：<input type='text' value=" + cos[z].name + "  disabled ></label></td><td><label> 设备类型：<input type='text' value=" + cos[z].type.name + " ></label></td><td><label> 设备状态：<input type='text' value=" + cos[z].state + " ></label></td></tr><tr><td><label style='padding-left: 41px;'> 是否正常：<input type='text'value=" + cos[z].isGood + " ></label></td><td> <label>温度：<input type='text' value=" + cos[z].temperature + " ></label></td><td><label>转速：<input type='text' value=" + cos[z].speed + " ></label> </td><td> <label>时间：<input type='text' value=" + cos[z].time + " ></label></td></tr></table>");

								if(table.find("input").eq(5).val() == "1") {
									table.find("input").eq(5).val("是");
								}
								if(table.find("input").eq(5).val() == "0") {
									table.find("input").eq(5).val("否");
								}
								$("#instrument").append(table);

							}
						}
					}
				}
				$(".step-body").fadeOut(200);
				for(var i = 0; i < divs.length; i++) {
					$(divs[i]).find("img").css('width', "0");
					$(divs[i]).css("border", "none");
				}
				$(this).css("border", "2px solid #CCA8E9");
				$(this).find("img").animate({
					width: '100%'
				});
				$(".step-body").css({
					width: "10%"
				})
				$(".step-body").fadeIn(500, function() {
					$(".step-body").animate({
						width: "94%"
					}, 500)
				});
				//因为step-body脱标，设置背景高度跟随step-body变化				
				$("#stepbg").height(600 + $(".step-body").height() / 2);
				//点击获取当前时间
				getTime ()
				//增加原辅料
				moreRet();
				//增加设备
				moreIst();

			})
			//点击获取当前时间
			function getTime () {
				$(".startDate").click(function () {
					$(this).siblings("input").val(moment().format('YYYY-MM-DD HH:mm'))
				});
				$(".stopDate").click(function () {
					$(this).siblings("input").val(moment().format('YYYY-MM-DD HH:mm'))
				});
			}
			//增加原辅料
			function moreRet() {
				$(".moreRet").click(function() {
					var checkRet = $(".checkRet");
					for(var i = 0; i < checkRet.length; i++) {
						if(checkRet[i].checked) {
							var table = $(checkRet[i]).parents("table").clone();
							table.find(".checkRet").attr("checked",false);
							$("#reagent").append(table);
						}
					}
					//删除原辅料
					remRet();
				})
			}
			//删除原辅料
			function remRet() {
				$(".remRet").click(function() {
					$(this).parents("table").remove();
				});
				$(".remIst").click(function() {
					$(this).parents("table").remove();
				});

			}
			//增加设备
			function moreIst() {
				$(".moreIst").click(function() {
					var checkIns = $(".checkIns");
					for(var i = 0; i < checkIns.length; i++) {
						if(checkIns[i].checked) {
							var table = $(checkIns[i]).parents("table").clone();
							table.find(".checkIns").attr("checked",false);
							$("#instrument").append(table);
						}
					}
					//删除设备
					remRet();
				})
			}
			//点击保存发送数据
			$(".savetem").click(function() {
				//拼接content3中样本数据为json发送给后台
				var trs1 = $("#tem input");
				var arr1 = [];
				var arr2 = [];
				var arr3 = [];
				var json = {
					id: "",
					code: "",
					testUserName: "",
					startTime: "",
					endTime: "",
					state: "",
					note: ""
				}
				json.id = trs1.eq(0)[0].id;
				json.code = trs1.eq(0)[0].code;
				json.testUserName = trs1.eq(0).val();
				json.startTime = trs1.eq(1).val();
				json.endTime = trs1.eq(2).val();
				json.state = trs1.eq(3).val();
				arr1.push(json);
				var table2 = $("#reagent table");
				for(var i = 0; i < table2.length; i++) {
					var trs2 = $(table2[i]).find("input");
					var json = {
						id: "",
						name: "",
						batch: "",
						expireDate: "",
						sn: "",
						isGood: "",
						sampleNum: "",
						oneNum: "",
						num: ""
					}
					json.id = trs2.eq(1)[0].id;
					json.name = trs2.eq(1).val();
					json.batch = trs2.eq(2).val();
					json.expireDate = trs2.eq(3).val();
					json.sn = trs2.eq(4).val();
					if(trs2.eq(5).val() == "是") {
						json.isGood = 1;
					}
					if(trs2.eq(5).val() == "否") {
						json.isGood = 0;
					}
					json.sampleNum = trs2.eq(6).val();
					json.oneNum = trs2.eq(7).val();
					json.num = trs2.eq(8).val();
					arr2.push(json);
				}
				var table3 = $("#instrument table");
				for(var i = 0; i < table3.length; i++) {
					var trs3 = $(table3[i]).find("input");
					var json = {
						id: "",
						code: "",
						name: "",
						type: {},
						state: "",
						isgood: "",
						temperature: "",
						speed: "",
						time: ""
					};
					json.id = trs3.eq(1)[0].id;
					json.code = trs3.eq(1).val();
					json.name = trs3.eq(2).val();
					json.type.name = trs3.eq(3).val();
					json.state = trs3.eq(4).val();
					if(trs3.eq(5).val() == "是") {
						json.isgood = 1;
					}
					if(trs3.eq(5).val() == "否") {
						json.isgood = 0;
					}
					json.temperature = trs3.eq(6).val();
					json.speed = trs3.eq(7).val();
					json.time = trs3.eq(8).val();
					arr3.push(json);
				}

				var data1 = {
					templeat: arr1
				};
				var str1 = JSON.stringify(data1);
				var data2 = {
					reagent: arr2
				};
				var str2 = JSON.stringify(data2);
				var data3 = {
					cos: arr3
				};
				var str3 = JSON.stringify(data3);
				//发送数据并验证
				$.ajax({
					type: "post",
					url: ctx+"/main/saveTemplate.action",
					data: {
						templeat: str1,
						reagent: str2,
						cos: str3
					},
					datatype: "json",
					success: function(data) {
						data = JSON.parse(data);
						if(data["success"]) {
							alert("保存成功")
						} else {
							alert("保存失败");
						}
					}
				});
			});
			//点击上一步，返回页面二
			$("#prev3").click(function() {
				$("#content3").hide(500);
				$("#content2").show(500);
			});
			//点击下一步，更新页面4数据
			$("#nextbtn3").click(function() {
				$.ajax({
					type: "post",
					url: ctx+"/main/selResult.action",
					data: {
						formId: formId
					},
					datatype: "json",
					success: function(data) {

						var js = JSON.parse(data)

						if(js["data"] == 0) {
							$.ajax({
								type: "post",
								url: ctx+"/main/saveResult.action",
								data: {
									strobj: str,
									formId: formId
								},
								success: function(data) {
									//更新实验结果
									var data = JSON.parse(data);
									var result = template("template4", data);
									$("#save-result").html(result);
								}
							});
						} else {
							//更新实验结果
							var result = template("template4", js);
							$("#save-result").html(result);
						}
					}
				});
				$("#content3").hide(500);
				$("#content4").show(500);
			})
			//点击返回页面3
			$("#prev4").click(function() {
				$("#content4").hide(500);
				$("#content3").show(500);
			});
			//点击保存页面4数据
			$("#nextbtn4").click(function() {
				var trs4 = $("#save-result tr");
				var arr4 = [];
				trs4.each(function(i, val) {
					var tr = val;
					var json = {
						id: "",
						code: "",
						sampleType: "",
						productName: "",
						dicSampleType: "",
						contraction: "",
						volume: "",
						sampleNum: "",
						od260: "",
						od280: "",
						rin: "",
						sampleConsumeV: "",
						totalGrade: "",
						zlGrade: "",
						result: "",
						nextFlow: "",
						submit: "",
						note: ""
					}
					json.id = $(tr).find("input").eq(1).val();
					json.code = $(tr).find("input").eq(2).val();
					json.sampleType = $(tr).find("input").eq(3).val();
					json.productName = $(tr).find("input").eq(4).val();
					json.dicSampleType = $(tr).find("input").eq(5).val();
					json.contraction = $(tr).find("input").eq(6).val();
					json.volume = $(tr).find("input").eq(7).val();
					json.sampleNum = $(tr).find("input").eq(8).val();
					json.od260 = $(tr).find("input").eq(9).val();
					json.od280 = $(tr).find("input").eq(10).val();
					json.rin = $(tr).find("input").eq(11).val();
					json.sampleConsumeV = $(tr).find("input").eq(12).val();
					json.totalGrade = $(tr).find("input").eq(13).val();
					json.zlGrade = $(tr).find("input").eq(14).val();
					json.result = $(tr).find("input").eq(15).val();
					json.nextFlow = $(tr).find("input").eq(16).val();
					json.submit = $(tr).find("input").eq(17).val();
					json.note = $(tr).find("input").eq(18).val();
					arr4.push(json);
				});
				var data4 = {
					strobj: arr4
				};
				var str4 = JSON.stringify(data4);
				//发送数据并验证
				$.ajax({
					type: "post",
					url: ctx+"/main/updResult.action",
					data: {
						strobj: str4
					},
					datatype: "json",
					success: function(data) {
						data = JSON.parse(data)
						if(data.success) {
							alert("保存成功")
						}
					}
				});
			});
		}
	});

});