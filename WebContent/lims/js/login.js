$(function() {
	var errmsg=$("#errmsg").val()
	if(errmsg){
		$(".mask").text(errmsg).fadeIn(1000).fadeOut(1000);
	}
	//点击英语登录为英文
	if($("#english")[0].checked){
		$("#logg").text("Login");
	}else{
		$("#logg").text("登录");
	}
	$("#english").click(function  () {
		$("#logg").text("Login");
	})
	//点击中文登录为中文
	$("#china").click(function  () {
		$("#logg").text("登录");
	})
	//登录交互功能实现
	$("#logg").click(function() {
		var user = $(".name").val();
		var pad = $(".psd").val();
		sessionStorage.setItem("userId", user);
		var loginstr = "";
		var loginNotFound = "";
		var passNotFound = "";
		//if($("#china")[0].checked){
		
		$("#language").val("zh_CN");
		
//		var language="zh_CN";
		//}
		if($("#english")[0].checked){
//			language="en";
			$("#language").val("en");
			loginstr="Login,Please wait......";
			loginNotFound = "User name not found!";
			passNotFound = "User password not match!"
		}else{
			loginstr="登录中，请稍后......";
			loginNotFound = "用户名不存在!";
			passNotFound = "用户名密码不匹配!";
		}
		$(".mask").text(loginstr).fadeIn(500).fadeOut(2000);
		$("#login").attr("action", "/main/newToMain.action");
		top.layer.load(4, {shade:0.3}); 
		$("#login").submit();
		
//		$.ajax({
//			type: "post",
//			url: ctx+"/main/newToMain.action",
//			data: {
//				user: user,
//				pad: pad,
//				language:language
//			},
//			datatype: "json",
//			success: function(data) {
//				var obj = JSON.parse(data);
//				if(obj["data"] == 0) {
//					$(".mask").text(passNotFound).fadeIn(1000).fadeOut(1000);
//				} else if(obj["data"] == 1) {
//					sessionStorage.setItem("userName", obj.name); 
//					window.location.href= window.ctx+"/main/toPortal.action";
//				} else if(obj["data"] == 2) {
//					$(".mask").text(loginNotFound).fadeIn(1000).fadeOut(1000);
//				}
//			}
//		});
		return false;
	})
})