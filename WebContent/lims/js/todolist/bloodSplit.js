$(document).ready(function() {

	$("body", window.parent.document).animate({
		scrollTop: 0
	}, 1000);
	var formId = sessionStorage.getItem("formId");
	//conten1和content2页面交互数据
	$.ajax({
		type: "post",
		url: ctx + "/experiment/plasma/newBloodSplitAction/showTaskItem.action",
		data: {
			formId: formId,
		},
		datatype: "json",
		success: function(data) {
			console.log(data)
			var info = JSON.parse(data);
			//更新样本数据
			var result1 = template("sampleTemplate", info);
			$("#sample-body").html(result1);
			$("#sample-body tr").find("input").prop("checked", true);
			$("#sample-body tr").css("background-color", "#90caaf");
			//查询匹配
			$("#search").unbind("click").click(function() {
				var stayCode = $("#searchText").val().split(" ");
				var scanCode = [];
				for(var i = 0; i < stayCode.length; i++) {
					if(stayCode[i] != "") {
						scanCode.push(stayCode[i].trim());
					}
				}
				if(scanCode.length == 0) {
					alert(biolims.common.pleaseinputSampleCode);
				} else {
					var str = scanCode.join(",");
					$.ajax({
						type: "post",
						data: {
							formId: formId,
							strobj: str
						},
						url: ctx + "/experiment/plasma/newBloodSplitAction/selTaskItemByCode.action",
						success: function(data) {
							info = JSON.parse(data);
							if(info.success) {
								$("#sample-body").empty();
								result1 = template("sampleTemplate", info);
								$("#sample-body").html(result1);
								$("#sample-body tr").find("input").prop("checked", true);
								$("#sample-body tr").css("background-color", "#90caaf");
								if(info.notFindCode && info.notFindCode.length != 0) {
									alert(biolims.common.code+'：' + data.notFindCode + biolims.common.notFind);
								}
							}
						}
					});
				}

			})
			//更新sop
			$("#sop-body").find("button").text(info["template"]);
			//点击 下一步跳转到第二页面
			$("#nextbtn1").click(function(e) {
				var row = info.rowNum;
				var col = info.colNum;
				if(row) {
					if(parseInt(row) > 12) {
						$("#changeOne").removeClass("col-xs-6").addClass("col-xs-12");
						$("#changeTwo").removeClass("col-xs-6").addClass("col-xs-12");
					}
					createShelf(row, col, "#grid");
					$("#content1").fadeOut(500);
					$("#content2").fadeIn(500);
					$("#content2").attr("have", "yes");
					$("body", window.parent.document).animate({
						scrollTop: 0
					}, 1000);
					//更新孔板目标数据
					$("#grid-body").html(result1);
					$("#grid-body tr").find("input").prop("checked", true);
					$("#grid-body tr").css("background-color", "#90caaf");
					//更新孔板结果数据
					var result2 = template("gridTemplate", info);
					$("#result-body").html(result2);
					//存储后台坐标
					var arrx = [];
					var arry = [];
					var arrId = [];
					var dicSample = [];
					var infoArr = info["data"];
					for(var i = 0; i < infoArr.length; i++) {
						for(var k in infoArr[i]) {
							if(k == "rowCode") {
								arrx.push(infoArr[i][k])
							}
							if(k == "colCode") {
								arry.push(infoArr[i][k])
							}
							if(k == "id") {
								arrId.push(infoArr[i][k])
							}
						}
					}
					//将后台坐标反映至96孔板
					var holes = document.getElementById("grid").getElementsByTagName("div");
					for(var i = 0; i < arrx.length; i++) {
						for(var z = 0; z < holes.length; z++) {
							if(holes[z].getAttribute("x") == arrx[i] && holes[z].getAttribute("y") == arry[i]) {
								holes[z].style.backgroundColor = "#007BB6";
							}
						}
					}
					//将坐标及样本数据反映至result-body
					var trs = $("#result-body tr");
					for(var i = 0; i < trs.length; i++) {
						trs[i].getElementsByTagName("input")[0].value = arrx[i] + arry[i];
						var inputVal = trs[i].getElementsByTagName("input")[0].value;
						if(inputVal == "0" || inputVal == "NaN") {
							trs[i].getElementsByTagName("input")[0].value = null;
						}
					}
					//点击全选
					check_all($(".check-all"));
					shift_click_grid($("#grid-body tr"));
					drag();
					//点击下一步向后台发送 实验数据json
					$("#nextbtn2").unbind("click").click(function() {
						//拼接content2中样本结果数据
						var trss = $("#result-body tr");
						var arr = [];
						trss.each(function(i, val) {
							var tr = val;
							var json = {
								id: "",
								cord: "",
								code: "",
								sampleCode: "",
								labCode: "",
								productId: "",
								productName: "",
								sampleType: "",
								dicSampleTypeId: "",
								dicSampleTypeName: "",
								productNum: ""
							}
							json.id = arrId[i];
							json.cord = $(tr).find("input").eq(0).val();
							json.code = $(tr).find("input").eq(1).val();
							json.sampleCode = $(tr).find("input").eq(2).val();
							json.labCode = $(tr).find("input").eq(3).val();
							json.productId = $(tr).find("input").eq(4).val();
							json.productName = $(tr).find("input").eq(5).val();
							json.sampleType = $(tr).find("input").eq(6).val();
							json.dicSampleTypeId = $(tr).find("input").eq(7).val();
							json.dicSampleTypeName = $(tr).find("input").eq(8).val();
							json.productNum = $(tr).find("input").eq(9).val();
							arr.push(json);
						});
						var data = {
							strobj: arr
						};
						var str = JSON.stringify(data);
						//发送数据并验证
						$.ajax({
							type: "post",
							url: ctx + "/experiment/plasma/newBloodSplitAction/saveItem.action",
							data: {
								strobj: str
							},
							datatype: "json",
							success: function(data) {
								data = JSON.parse(data);
								if(data["success"]) {
									$("#content2").hide(500);
									$("#content3").show(500);
									$("body", window.parent.document).animate({
										scrollTop: 0
									}, 1000);
								} else {
									alert(biolims.common.saveFailed);
								}
							}
						});

					});
					//点击上一步返回页面1
					$("#prev2").click(function() {
						$("#content2").hide(500);
						$("#content1").show(500);
						$("body", window.parent.document).animate({
							scrollTop: 0
						}, 1000);
					});

				} else {
					$("#content2").hide(500);
					$("#content2").attr("have", "no");
					$("#content1").hide(500);
					$("#content3").show(500);
					$("body", window.parent.document).animate({
						scrollTop: 0
					}, 1000);
				}

			});

		}
	});
	//conten3页面数据交互
	$.ajax({
		type: "post",
		url: ctx + "/experiment/plasma/newBloodSplitAction/showTemplate.action",
		data: {
			formId: formId
		},
		datatype: "json",
		success: function(data) {
			console.log(data)
			var data = JSON.parse(data);
			var tem = data.templeat;
			var reagent = data.reagent;
			var cos = data.cos;
			var steps = "";
			//动态生成实验步骤
			for(var i = 0; i < tem.length; i++) {
				steps += "<tr><td><div>" + tem[i].name + "<img src='../../img/down.png' width='30px' /></div></td></tr>"
			}
			$(".step-head table").html(steps)
			var divs = $(".step-head").find("div");
			//默认加载第一个步骤
			stepHandle(0)
			//点击步骤生成步骤明细   原辅料明细  设备明细
			divs.click(stepHandle)

			function stepHandle(num) {
				if(arguments[0] === 0) {
					var index = 0;
					var txt = $(divs[0]).text();
					var self = $(divs[0])
				} else {
					var index = divs.index(this);
					var txt = $(this).text();
					var self = $(this);
				}

				if(index == 0) {
					$(".step-body").css("top", self.position().top);
				}
				if(index > 0) {
					$(".step-body").css("top", self.position().top - 80);
				}
				$("#reagent").html("");
				$("#instrument").html("");
				$("#reagent").append($("<h4><i class='fa fa-eyedropper'></i>"+biolims.common.reagentName+
						"<button class='btn btn-xs btn-info pull-right moreRet hidden'>"+biolims.common.addReagent+"</button></h4>"));
				$("#instrument").append($("<h4><i class='fa  fa-flask'></i>"+biolims.common.instrumentName+
						"<button class='btn btn-xs btn-info pull-right moreIst hidden'>"+biolims.common.addInstrument+"</button></h4>"))
				for(var i = 0; i < tem.length; i++) {
					if(tem[i].name == txt) {
						$("#tem input")[0].id = tem[i].id;
						$("#tem input")[0].code = tem[i].code;
						$("#tem input")[0].value = sessionStorage.getItem("userName");
						$("#tem input")[1].value = tem[i].startTime;
						$("#tem input")[2].value = tem[i].endTime;
						$("#tem div")[0].innerHTML = tem[i].note;
						//通过步骤code关联原辅料itemId 生成原辅料明细
						var code = tem[i].code;
						for(var j = 0; j < reagent.length; j++) {
							if(code == reagent[j].itemId) {
								var table = $("<table class='table no-border'><tr><td><input type='checkbox' class='checkRet hidden'><i class='fa fa-trash-o remRet hidden'></i><label>"+biolims.common.reagentName+"：<input type='text' id=" + reagent[j].id + " value=" + reagent[j].name + " disabled></label> </td><td><label>"+biolims.common.reagentBatchNum+"：<input type='text' value=" + reagent[j].batch + " disabled/></label></td><td><label>"+biolims.common.sampleNum+"：<input type='text' value=" + reagent[j].sampleNum + " disabled></label></td></tr><tr><td><label>sn：<input type='text' value=" + reagent[j].sn + " ></label></td><td><label>"+biolims.common.singleDose+"：<input type='text' value=" + reagent[j].oneNum + " ></label></td><td><label>"+biolims.common.dose+"：<input type='text' value=" + reagent[j].num + " ></label></td></tr></table>");

								if(table.find("input").eq(1).val() == "null") {
									table.find("input").eq(1).val("");
								}
								if(table.find("input").eq(2).val() == "null") {
									table.find("input").eq(2).val("");
								}
								if(table.find("input").eq(3).val() == "null") {
									table.find("input").eq(3).val("");
								}
								if(table.find("input").eq(4).val() == "null") {
									table.find("input").eq(4).val("");
								}
								if(table.find("input").eq(5).val() == "null") {
									table.find("input").eq(5).val("");
								}
								if(table.find("input").eq(6).val() == "null") {
									table.find("input").eq(6).val("");
								}
								if(table.find("input").eq(7).val() == "null") {
									table.find("input").eq(7).val("");
								}
								if(table.find("input").eq(8).val() == "null") {
									table.find("input").eq(8).val("");
								}
								$("#reagent").append(table);

							}
						}
						//通过步骤code关联设备itemId 生成设备明细
						for(var z = 0; z < cos.length; z++) {
							if(code == cos[z].itemId) {
								if(cos[z].type) {
									var cosType = cos[z].type.name;
								} else {
									var cosType = null;
								}
								var table = $("<table class='table no-border' style='margin-bottom: 20px !important;'><tr><td><input type='checkbox' class='checkIns hidden' /><i class='fa fa-trash-o remIst hidden'></i><label>"+biolims.common.instrumentNo+"：<input type='text' disabled id="+cos[z].id+" value="+cos[z].code+"  ></label></td><td><label> "+biolims.common.instrumentName+"：<input type='text' disabled value="+cos[z].name+" ></label></td><td><label> "+biolims.common.instrumentType+"：<input type='text' value=" + cosType + "  disabled ></label></td></tr><tr><td> <label>"+biolims.common.temperature+"：<input type='text' value=" + cos[z].temperature + " ></label></td><td><label>"+biolims.common.speed+"：<input type='text' value=" + cos[z].speed + " ></label> </td></tr></table>");

								if(table.find("input").eq(1).val() == "null") {
									table.find("input").eq(1).val("");
								}
								if(table.find("input").eq(2).val() == "null") {
									table.find("input").eq(2).val("");
								}
								if(table.find("input").eq(3).val() == "null") {
									table.find("input").eq(3).val("");
								}
								if(table.find("input").eq(4).val() == "null") {
									table.find("input").eq(4).val("");
								}
								if(table.find("input").eq(5).val() == "null") {
									table.find("input").eq(5).val("");
								}
								if(table.find("input").eq(6).val() == "null") {
									table.find("input").eq(6).val("");
								}
								if(table.find("input").eq(7).val() == "null") {
									table.find("input").eq(7).val("");
								}
								if(table.find("input").eq(8).val() == "null") {
									table.find("input").eq(8).val("");
								}
								$("#instrument").append(table);

							}
						}
					}
				}
				$(".step-body").fadeOut(200);
				for(var i = 0; i < divs.length; i++) {
					$(divs[i]).find("img").css('width', "0");
					$(divs[i]).css("border", "none");
				}
				self.css("border", "2px solid #CCA8E9");
				self.find("img").animate({
					width: '100%'
				});
				$(".step-body").css({
					width: "10%"
				})
				$(".step-body").fadeIn(500, function() {
					$(".step-body").animate({
						width: "100%"
					}, 500)
				});
				//因为step-body脱标，设置背景高度跟随step-body变化				
				$("#stepbg").height(600 + $(".step-body").height() / 2);
				//点击获取当前时间
				getTime()
				//增加原辅料
				moreRet();
				//增加设备
				moreIst();

			}
			//点击获取当前时间
			function getTime() {
				$(".startDate").click(function() {
					$(this).siblings("input").val(moment().format('YYYY-MM-DD'))
				});
				$(".stopDate").click(function() {
					$(this).siblings("input").val(moment().format('YYYY-MM-DD'))
				});
			}
			//增加原辅料
			function moreRet() {
				$(".moreRet").click(function() {
					var checkRet = $(".checkRet");
					for(var i = 0; i < checkRet.length; i++) {
						if(checkRet[i].checked) {
							var table = $(checkRet[i]).parents("table").clone();
							table.find(".checkRet").attr("checked", false);
							$("#reagent").append(table);
						}
					}
					//删除原辅料
					remRet();
				})
			}
			//删除原辅料
			function remRet() {
				$(".remRet").click(function() {
					$(this).parents("table").remove();
				});
				$(".remIst").click(function() {
					$(this).parents("table").remove();
				});

			}
			//增加设备
			function moreIst() {
				$(".moreIst").click(function() {
					var checkIns = $(".checkIns");
					for(var i = 0; i < checkIns.length; i++) {
						if(checkIns[i].checked) {
							var table = $(checkIns[i]).parents("table").clone();
							table.find(".checkIns").attr("checked", false);
							$("#instrument").append(table);
						}
					}
					//删除设备
					remRet();
				})
			}
			//点击保存发送数据
			$(".savetem").unbind("click").click(function() {
				//拼接content3中样本数据为json发送给后台
				var trs1 = $("#tem input");
				var arr1 = [];
				var arr2 = [];
				var arr3 = [];
				var json = {
					id: "",
					code: "",
					userId: "",
					startTime: "",
					endTime: "",
					note: ""
				}
				json.id = trs1.eq(0)[0].id;
				json.code = trs1.eq(0)[0].code;
				json.userId = sessionStorage.getItem("userId");
				json.startTime = trs1.eq(1).val();
				json.endTime = trs1.eq(2).val();
				json.note = $("#tem div")[0].innerHTML;
				arr1.push(json);
				var table2 = $("#reagent table");
				for(var i = 0; i < table2.length; i++) {
					var trs2 = $(table2[i]).find("input");
					var json = {
						id: "",
						name: "",
						batch: "",
						sn: "",
						sampleNum: "",
						oneNum: "",
						num: ""
					}
					json.id = trs2.eq(1)[0].id;
					json.name = trs2.eq(1).val();
					json.batch = trs2.eq(2).val();
					json.sampleNum = trs2.eq(3).val();
					json.sn = trs2.eq(4).val();
					json.oneNum = trs2.eq(5).val();
					json.num = trs2.eq(6).val();
					arr2.push(json);
				}
				var table3 = $("#instrument table");
				for(var i = 0; i < table3.length; i++) {
					var trs3 = $(table3[i]).find("input");
					var json = {
						id: "",
						code: "",
						name: "",
						typeName: "",
						temperature: "",
						speed: "",
					};
					json.id = trs3.eq(1)[0].id;
					json.code = trs3.eq(1).val();
					json.name = trs3.eq(2).val();
					json.typeName = trs3.eq(3).val();
					json.temperature = trs3.eq(4).val();
					json.speed = trs3.eq(5).val();
					arr3.push(json);
				}

				var data1 = {
					templeat: arr1
				};
				var str1 = JSON.stringify(data1);
				var data2 = {
					reagent: arr2
				};
				var str2 = JSON.stringify(data2);
				var data3 = {
					cos: arr3
				};
				var str3 = JSON.stringify(data3);
				//发送数据并验证
				$.ajax({
					type: "post",
					url: ctx + "/experiment/plasma/newBloodSplitAction/saveTemplate.action",
					data: {
						templeat: str1,
						reagent: str2,
						cos: str3
					},
					datatype: "json",
					success: function(data) {
						data = JSON.parse(data);
						if(data["success"]) {
							alert(biolims.common.saveSuccess)
						} else {
							alert(biolims.common.saveFailed);
						}
					}
				});
			});
		}
	});

	//点击上一步，返回页面二或页面一
	$("#prev3").click(function() {
		if($("#content2").attr("have") == "yes") {
			$("#content3").hide(500);
			$("#content2").show(500);
			$("body", window.parent.document).animate({
				scrollTop: 0
			}, 1000);
		} else {
			$("#content3").hide(500);
			$("#content1").show(500);
			$("body", window.parent.document).animate({
				scrollTop: 0
			}, 1000);
		}
	});
	//点击下一步，更新页面4数据
	$("#nextbtn3").unbind("click").click(function() {
		var trss = $("#sample-body tr");
		var arr = [];
		trss.each(function(i, val) {
			var tr = val;
			var json = {
				code: "",
				sampleCode: "",
				labCode: "",
				productId: "",
				productName: "",
				sampleType: "",
				dicSampleTypeId: "",
				dicSampleTypeName: "",
				productNum: ""
			}
			json.code = $(tr).find("td").eq(1).text();
			json.sampleCode = $(tr).find("td").eq(2).text();
			json.labCode = $(tr).find("td").eq(3).text();
			json.productId = $(tr).find("td").eq(4).text();
			json.productName = $(tr).find("td").eq(5).text();
			json.sampleType = $(tr).find("td").eq(6).text();
			json.dicSampleTypeId = $(tr).find("td").eq(7).text();
			json.dicSampleTypeName = $(tr).find("td").eq(8).text();
			json.productNum = $(tr).find("td").eq(9).text();
			arr.push(json);
		});
		var data = {
			strobj: arr
		};
		var str = JSON.stringify(data);
		$.ajax({
			type: "post",
			url: ctx + "/experiment/plasma/newBloodSplitAction/selResult.action",
			data: {
				formId: formId
			},
			datatype: "json",
			success: function(data) {

				var js = JSON.parse(data)

				if(js["data"] == 0) {
					$.ajax({
						type: "post",
						url: ctx + "/experiment/plasma/newBloodSplitAction/saveResult.action",
						data: {
							strobj: str,
							formId: formId
						},
						success: function(data) {
							//更新实验结果
							var data = JSON.parse(data);
							var result = template("template4", data);
							$("#save-result").html(result);
						}
					});
				} else {
					//更新实验结果
					var result = template("template4", js);
					$("#save-result").html(result);
				}
			}
		});
		$("#content3").hide(500);
		$("#content4").show(500);
		$("body", window.parent.document).animate({
			scrollTop: 0
		}, 1000);
	})
	//点击返回页面3
	$("#prev4").click(function() {
		$("#content4").hide(500);
		$("#content3").show(500);
		$("body", window.parent.document).animate({
			scrollTop: 0
		}, 1000);
	});
	//点击保存页面4数据
	$("#nextbtn4").unbind("click").click(function() {
		var trs4 = $("#save-result tr");
		var arr4 = [];
		trs4.each(function(i, val) {
			var tr = val;
			var json = {
				id: "",
				code: "",
				sampleCode: "",
				productId: "",
				productName: "",
				dicSampleTypeId: "",
				dicSampleTypeName: "",
				concentration: "",
				volume: "",
			}
			json.id = $(tr).find("input").eq(1).val();
			json.code = $(tr).find("input").eq(2).val();
			json.sampleCode = $(tr).find("input").eq(3).val();
			json.productId = $(tr).find("input").eq(4).val();
			json.productName = $(tr).find("input").eq(5).val();
			json.dicSampleTypeId = $(tr).find("input").eq(6).val();
			json.dicSampleTypeName = $(tr).find("input").eq(7).val();
			json.concentration = $(tr).find("input").eq(8).val();
			json.volume = $(tr).find("input").eq(9).val();
			arr4.push(json);
		});
		var data4 = {
			strobj: arr4
		};
		var str4 = JSON.stringify(data4);
		//发送数据并验证
		$.ajax({
			type: "post",
			url: ctx + "/experiment/plasma/newBloodSplitAction/updResult.action",
			data: {
				strobj: str4
			},
			datatype: "json",
			success: function(data) {
				data = JSON.parse(data)
				if(data.success) {
					alert(biolims.common.saveSuccess);
				} else {
					alert(biolims.common.saveFailed);
				}
			}
		});
	});

	//全选与反选
	function check_all(element) {
		element.click(function() {
			$(this).parents("thead").siblings("tbody").find("input").prop("checked", $(this).prop("checked"));
			if($(this).parents("thead").siblings("tbody").find("input").prop("checked")) {
				$(this).parents("thead").siblings("tbody").find("tr").css("background-color", "#90caaf");
			} else {
				$(this).parents("thead").siblings("tbody").find("tr").css("background-color", "white");
			}
			document.onmousemove = function(event) {
				var e = window.event || event;
				if($("#grid-body tr input:checked").length != 0) {
					document.getElementById("mask2").style.display = "block";
					document.getElementById("mask2").style.left = e.pageX + 10 + "px";
					document.getElementById("mask2").style.top = e.pageY + 10 + 'px';
					document.getElementById("mask2").innerHTML = $("#sample-body tr input:checked").length;
				} else {
					document.getElementById("mask2").style.display = "none";
				}

			}
		});
	}
	//点击shift全选
	function shift_click_grid(element) {
		var rem = new Array();
		element.click(function(e) {
			var e = window.event || e;
			if($(this).find("input").prop("checked")) {
				$(this).find("input").prop("checked", false);
				$(this).css("background-color", "#FFF");
			} else {
				$(this).find("input").prop("checked", true);
				$(this).css("background-color", "#90caaf");
			}
			//shift配合鼠标左键全选
			rem.push(element.index($(this)))
			if(e.shiftKey) {
				var iMin = Math.min(rem[rem.length - 2], rem[rem.length - 1])
				var iMax = Math.max(rem[rem.length - 2], rem[rem.length - 1])
				for(i = iMin; i <= iMax; i++) {
					element.eq(i).find("input").prop("checked", true);
					element.eq(i).css("background-color", "#90caaf");
				}

			}
			//阻止事件冒泡
			e = e || window.event;
			if(e.stopPropagation) {
				e.stopPropagation();
			} else {
				e.cancelBubble = true;
			}

		});
		//与程序无关，是禁止鼠标选中文字。点选的时候容易选中文字 太讨厌 。  
		document.onselectstart = function(event) {
			event = window.event || event;
			event.returnValue = false;
		}
	}
	//拖拽函数
	function drag() {
		document.getElementById("table-self").onmousedown = function() {
			document.getElementById("mask2").style.display = "none";
		}
		document.getElementById("table-self").onmouseup = function() {
			document.getElementById("mask2").style.display = "none";
			document.onmousemove = function(e) {
				var e = window.event || event;
				var length = $("#grid-body tr input:checked").length
				if(length != 0) {
					document.getElementById("mask2").style.display = "block";
					document.getElementById("mask2").style.left = e.pageX + 10 + "px";
					document.getElementById("mask2").style.top = e.pageY + 10 + 'px';
					document.getElementById("mask2").innerHTML = length;
				} else {
					document.getElementById("mask2").style.display = "none";
				}
			}
			var holes = document.getElementById("grid").getElementsByTagName("div");
			for(var i = 0; i < holes.length; i++) {
				holes[i].onmouseover = function() {
					var zz = parseInt(this.getAttribute("z"));
					var yy = parseInt(this.getAttribute("y"));
					var cc = parseInt($("#grid-body tr input:checked").length);
					for(var j = 0; j < holes.length; j++) {

						if(holes[j].getAttribute("y") >= yy && zz <= holes[j].getAttribute("z") && holes[j].getAttribute("z") < zz + cc) {
							holes[j].className = "myhover";
						} else {
							holes[j].className = null;
						}
					}

				}
				holes[i].onmouseout = function() {
					for(var j = 0; j < holes.length; j++) {
						holes[j].className = null;
					}
				}
				holes[i].onclick = function() {
					document.onmousemove = null;
					document.getElementById("mask2").style.display = "none";
					$("#result-body").empty();
					$("#grid div").css("background-color", "#fff");
					var zz = parseInt(this.getAttribute("z"));
					var yy = parseInt(this.getAttribute("y"));
					var cc = parseInt($("#grid-body tr input:checked").length);
					var xxx = [];
					var yyy = [];
					for(var j = 0; j < holes.length; j++) {
						if(holes[j].getAttribute("y") >= yy && zz <= holes[j].getAttribute("z") && holes[j].getAttribute("z") < zz + cc) {
							holes[j].style.backgroundColor = "#007BB6";
							if(holes[j].getAttribute("y") == yy) {
								xxx.push(holes[j].getAttribute("x"));
								yyy.push(holes[j].getAttribute("y"));
							}

						} else {
							holes[j].className = null;
						}
					}
					for(var j = 0; j < holes.length; j++) {
						if(holes[j].getAttribute("y") >= yy && zz <= holes[j].getAttribute("z") && holes[j].getAttribute("z") < zz + cc) {
							holes[j].style.backgroundColor = "#007BB6";
							if(holes[j].getAttribute("y") == yy + 1) {
								xxx.push(holes[j].getAttribute("x"));
								yyy.push(holes[j].getAttribute("y"));
							}

						} else {
							holes[j].className = null;
						}
					}
					for(var j = 0; j < holes.length; j++) {
						if(holes[j].getAttribute("y") >= yy && zz <= holes[j].getAttribute("z") && holes[j].getAttribute("z") < zz + cc) {
							holes[j].style.backgroundColor = "#007BB6";
							if(holes[j].getAttribute("y") == yy + 2) {
								xxx.push(holes[j].getAttribute("x"));
								yyy.push(holes[j].getAttribute("y"));
							}

						} else {
							holes[j].className = null;
						}
					}
					for(var j = 0; j < holes.length; j++) {
						if(holes[j].getAttribute("y") >= yy && zz <= holes[j].getAttribute("z") && holes[j].getAttribute("z") < zz + cc) {
							holes[j].style.backgroundColor = "#007BB6";
							if(holes[j].getAttribute("y") == yy + 3) {
								xxx.push(holes[j].getAttribute("x"));
								yyy.push(holes[j].getAttribute("y"));
							}

						} else {
							holes[j].className = null;
						}
					}
					for(var j = 0; j < holes.length; j++) {
						if(holes[j].getAttribute("y") >= yy && zz <= holes[j].getAttribute("z") && holes[j].getAttribute("z") < zz + cc) {
							holes[j].style.backgroundColor = "#007BB6";
							if(holes[j].getAttribute("y") == yy + 4) {
								xxx.push(holes[j].getAttribute("x"));
								yyy.push(holes[j].getAttribute("y"));
							}

						} else {
							holes[j].className = null;
						}
					}

					//因为jQuery获取的grid元素是逆序的，所以反向遍历
					var trs = $("#grid-body tr input:checked").parents("tr");
					for(var i = 0; i < trs.length / 2; i++) {
						var temp = trs[i];
						trs[i] = trs[trs.length - 1 - i];
						trs[trs.length - 1 - i] = temp;
					}
					var gridData = [];
					trs.each(function(i, val) {
						var tr = val;
						var json = {
							code: "",
							sampleCode: "",
							labCode: "",
							productId: "",
							productName: "",
							sampleType: "",
							dicSampleTypeId: "",
							dicSampleTypeName: "",
							productNum: ""
						}
						json.code = $(tr).find("td").eq(1).text();
						json.sampleCode = $(tr).find("td").eq(2).text();
						json.labCode = $(tr).find("td").eq(3).text();
						json.productId = $(tr).find("td").eq(4).text();
						json.productName = $(tr).find("td").eq(5).text();
						json.sampleType = $(tr).find("td").eq(6).text();
						json.dicSampleTypeId = $(tr).find("td").eq(7).text();
						json.dicSampleTypeName = $(tr).find("td").eq(8).text();
						json.productNum = $(tr).find("td").eq(9).text();
						gridData.push(json);
					});
					var resultData = {
						gridData: gridData
					};
					var result2 = template("clickGridTemplate", resultData);
					$("#result-body").append(result2);
					for(var i = 0; i < xxx.length; i++) {
						$("#result-body tr")[i].getElementsByTagName("input")[0].value = xxx[i] + yyy[i];
					}
				}
			}
		}
	}
	//动态孔板函数
	function createShelf(m, n, element) {
		var m = parseInt(m);
		var n = parseInt(n);
		$(element).empty();
		var arr = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "k", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
		var tr0 = document.createElement("tr");
		var num = "";
		for(var i = 0; i <= n; i++) {
			num += '<td>' + i + '</td>';
		}
		tr0.innerHTML = num;
		tr0.children[0].innerHTML = "X";
		$(element).append(tr0);
		var h = 0;
		for(var i = 0; i < m; i++) {
			var tr = document.createElement("tr");
			var tds = "";
			var x = i + 1;
			for(var j = 0; j <= n; j++) {
				var jj = j < 10 ? "0" + j : j;
				tds += '<td><div x=' + arr[i] + ' y=' + j + ' z=' + (x - m) + ' coord=' + arr[i] + jj + '><span></span></div></td>';
				x += m;
			}
			tr.innerHTML = tds;
			tr.children[0].innerHTML = arr[i];
			$(element).append(tr);
		}
		var divs = $(element).find("div");
		for(var i = 0; i < divs.length; i++) {
			var h = i + 1;
			divs[i].setAttribute("h", h);
		}
	}

});