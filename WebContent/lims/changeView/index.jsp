<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">		
		<title>Bio-LIMS</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="${ctx}/lims/changeView/dist/viewer.min.css" />

		<style type="text/css">
			#test {
				padding: 10px;
			}
			
			#test .docs-pictures {
				margin: 0;
				padding: 0;
				list-style: none;
				display: block;
			}
			
			#test .docs-pictures li {
				margin: 30px 10px !important;
				float: left;
				width: 28%;
				height: 100px;
				margin: 0 -1px -1px 0;
				overflow: hidden;
			}
			
			#test .docs-pictures li img {
				width: 90%;
			}
		</style>
	</head>

	<body>

		<div id="test">
			<input type="hidden" id="itemId" value="${requestScope.fList}">
			<ul class="docs-pictures">
				<c:if test="${requestScope.fList!=''}">
				<c:forEach var="flist" items="${fList}">
					<c:if test="${flist.fileType=='webp'||flist.fileType=='jpg'||flist.fileType=='png'||flist.fileType=='gif'}" >
						<li><img id="upLoadImg" src="${ctx}/operfile/downloadById.action?id=${flist.id}"></li>
					</c:if>
				</c:forEach>
				</c:if>
				<c:if test="${requestScope.fList=='[]'}">
					<h1>NO Data！</h1>
				</c:if>
			</ul>
		</div>

	</body>
	<script type="text/javascript" src="${ctx}/lims/changeView/assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="${ctx}/lims/changeView/dist/viewer.min.js"></script>
	<script type="text/javascript" src="${ctx}/lims/changeView/assets/js/main.js"></script>
	<script type="text/javascript">
//		$(function () {
//			if($("#itemId").val().length==2){
//				$('#picFrame', window.parent.document).hide();
//				$('#inputProduct', window.parent.document).removeClass("col-xs-7").addClass("col-xs-12");
//			}
//		})
	</script>
</html>