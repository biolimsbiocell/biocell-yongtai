(function($) {
	"use strict";

	$.fn.treegridData = function(options, param) {
		//如果是调用方法
		if(typeof options == 'string') {
			return $.fn.treegridData.methods[options](this, param);
		}

		//如果是初始化组件
		options = $.extend({}, $.fn.treegridData.defaults, options || {});
		var target = $(this);
		//得到根节点
		target.getRootNodes = function(data) {
			var result = [];
			$.each(data, function(index, item) {
				if(!item[options.parentColumn]) {
					result.push(item);
				}
			});
			return result;
		};
		var j = 0;
		//递归获取子节点并且设置子节点
		target.getChildNodes = function(data, parentNode, parentIndex, tbody) {
			$.each(data, function(i, item) {
				if(item[options.parentColumn] == parentNode[options.id]) {
					j++;
					//var nowParentIndex = ((j++)+parentIndex + 1);
					var nowParentIndex = (parentIndex + 1);
					var tr = $('<tr><td><input type="checkbox" class="icheck"/></td></tr>');
					tr.addClass('treegrid-' + nowParentIndex);
					tr.addClass('treegrid-parent-' + parentIndex);
					$.each(options.columns, function(index, column) {
						var td = $('<td></td>');
						if(column.field == "createUser.name") {
							td.text(item["createUser"]["name"]);
						} else if(column.field == "acceptUserGroup.name") {
							td.text(item["acceptUserGroup"]["name"]);
						} else {
							td.text(item[column.field]);
						}
						tr.append(td);
					});
					tbody.append(tr);
					target.getChildNodes(data, item, nowParentIndex, tbody)
				}
			});
		};
		target.addClass('table');
		if(options.striped) {
			target.addClass('table-striped');
		}
		if(options.bordered) {
			target.addClass('table-bordered');
		}
		if(options.url) {
			$.ajax({
				type: options.type,
				url: options.url,
				data: options.ajaxParams,
				dataType: "JSON",
				success: function(data, textStatus, jqXHR) {
					//console.log(data);
					//构造表头
					var thr = $('<tr><th style="padding:10px;"></th></tr>');
					$.each(options.columns, function(i, item) {
						var th = $('<th style="padding:10px;"></th>');
						th.text(item.title);
						thr.append(th);
					});
					var thead = $('<thead></thead>');
					thead.append(thr);
					target.append(thead);

					//构造表体
					var tbody = $('<tbody></tbody>');
					var rootNode = target.getRootNodes(data);
					$.each(rootNode, function(i, item) {
						var tr = $('<tr><td><input type="checkbox" class="icheck"/></td></tr>');
						tr.addClass('treegrid-' + (j + i));
						$.each(options.columns, function(index, column) {
							var td = $('<td></td>');
							if(column.field == "createUser.name") {
								td.text(item["createUser"]["name"]);
							} /*else if(column.field == "acceptUserGroup.name") {
								td.text(item["acceptUserGroup"]["name"]);
							} */else {
								td.text(item[column.field]);
							}
							tr.append(td);
						});
						tbody.append(tr);
						target.getChildNodes(data, item, (j + i), tbody);
					});
					target.append(tbody);
					target.treegrid({
						expanderExpandedClass: options.expanderExpandedClass,
						expanderCollapsedClass: options.expanderCollapsedClass
					});
					//多选框格式化
					var ichecks=target.find('.icheck');
					ichecks.iCheck({
						checkboxClass: 'icheckbox_square-blue',
						increaseArea: '20%' // optional
					});
					ichecks.on('ifChanged', function() {
						if($(this).is(':checked')) {
							$(this).parents("tr").addClass("chosed");
						} else {
							$(this).parents("tr").removeClass("chosed");
						}
					});
					//treeGridChosed (target);
					if(!options.expandAll) {
						target.treegrid('collapseAll');
					}
				}
			});
		} else {
			//也可以通过defaults里面的data属性通过传递一个数据集合进来对组件进行初始化....有兴趣可以自己实现，思路和上述类似
		}
		return target;
	};

	$.fn.treegridData.methods = {
		getAllNodes: function(target, data) {
			return target.treegrid('getAllNodes');
		},
		//组件的其他方法也可以进行类似封装........
	};

	$.fn.treegridData.defaults = {
		id: 'id',
		parentColumn: 'parent',
		data: [], //构造table的数据集合
		type: "GET", //请求数据的ajax类型
		url: null, //请求数据的ajax的url
		ajaxParams: {}, //请求数据的ajax的data属性
		expandColumn: null, //在哪一列上面显示展开按钮
		expandAll: true, //是否全部展开
		striped: false, //是否各行渐变色
		bordered: false, //是否显示边框
		columns: [],
		expanderExpandedClass: 'glyphicon glyphicon-chevron-down', //展开的按钮的图标
		expanderCollapsedClass: 'glyphicon glyphicon-chevron-right' //缩起的按钮的图标

	};
})(jQuery);

//查询
function search() {
	searchContent = searchOptions();
	var myTable;
	var myUrl;
	$("body").append('<div id="searchItemLaye" class="hide"><div id="searchItemLayer" class="row"></div></div>');
	var str = "";
	searchContent.forEach(function(val, i) {
		if(val.type == "input") {
			str += '<div class="col-sm-6" style="margin-top: 10px;"><div class="input-group"><span class="input-group-addon">' + val.txt + '</span><input type="text" class="form-control searchItemLayerInput" searchname=' + val.searchName + '></div></div>';
		}
		if(val.type == "select") {
			var selectOptArr = val.options.split("|");
			var selectOptVal = val.changeOpt.split("|");
			var opt = "";
			selectOptArr.forEach(function(v, j) {
				opt += "<option value=" + selectOptVal[j] + ">" + v + "</option>";
			});
			str += '<div class="col-sm-6" style="margin-top: 10px;"><div class="input-group"><span class="input-group-addon">' + val.txt + '</span><select class="form-control searchItemLayerInput addItemLayerSelect" searchname=' + val.searchName + '>' + opt + '</select></div></div>';
		}
		if(val.type == "dataTime") {
			str += '<div class="col-sm-6" style="margin-top: 10px;"><div class="input-group"><span class="input-group-addon">' + val.txt + '</span><input type="date" mark=' + val.mark + ' class="form-control searchItemLayerInput searchItemLayerDate" searchname=' + val.searchName + '></div></div>';
		}
		if(val.type == "layer") {
			str += '<div class="col-sm-6" style="margin-top: 10px;"><div class="input-group"><span class="input-group-addon">' + val.txt + '</span><input type="text" class="form-control searchItemLayerInput searchItemLayerTK" searchname=' + val.searchName + '><span class="input-group-btn"><button class="btn btn-info" type="button" style="height: 34px;" onclick=' + val.action + '><i class="glyphicon glyphicon-search"></i></button></span></div></div>';
		}
		if(val.type == "table") {
			myTable = val.table;
			myUrl = val.reloadUrl;
		}
	});
	$("#searchItemLayer").html(str);
	top.layer.open({
		type: 1,
		content: $("#searchItemLaye").html(),
		anim: '1',
		area: ['650px', "300px"],
		shade: 0,
		title: biolims.common.find,
		btn: biolims.common.selected,
		yes: function(index, layero) {
			//获取添加的数据
			var searchItemLayerValue = {};
			$(".searchItemLayerInput", parent.document).each(function(i, val) {
				if($(val).hasClass("addItemLayerSelect")) {
					var k = $(val).attr("searchname");
					var val = $(val).children("option:selected").val();
					searchItemLayerValue[k] = val;
				} else if($(val).hasClass("searchItemLayerDate")) {
					var k = $(val).attr("searchname");
					var val = $(val).attr("mark") + $(val).val();
					searchItemLayerValue[k] = val;
				} else if($(val).hasClass("searchItemLayerTK")) {
					var k = $(val).attr("searchname");
					searchItemLayerValue[k + "-name"] = $(val).val();
					searchItemLayerValue[k + "-id"] = $(val).attr("sid");
				} else {debugger
					var k = $(val).attr("searchname");
					if($.trim($(val).val()) != null && $.trim($(val).val()) != "") {
						searchItemLayerValue[k] = "%" + $.trim($(val).val()) + "%";
					} else {
						searchItemLayerValue[k] = $.trim($(val).val());
					}
				}
			});
			//重新加载datatable
			var query = JSON.stringify(searchItemLayerValue);
			myTable.empty();
			myTable.treegridData({
				id: 'id',
				parentColumn: 'parent',
				type: "post", //请求数据的ajax类型
				url: myUrl, //请求数据的ajax的url
				ajaxParams: {
					query: query
				}, //请求数据的ajax的data属性
				expandColumn: null, //在哪一列上面显示展开按钮
				striped: false, //是否各行渐变色
				bordered: true, //是否显示边框
				expandAll: false, //是否全部展开
				columns: [{
						title: '编码',
						field: 'id'
					},
					{
						title: '描述',
						field: 'name'
					},
					{
						title: '项目定价',
						field: 'productFee'
					},
					{
						title: '创建人',
						field: 'createUser.name'
					},
					{
						title: '状态',
						field: 'stateName'
					},
					{
						title: '负责组',
						field: 'acceptUserGroup.name'
					},
					{
						title: '父级',
						field: 'parentName'
					},
					{
						title: '上级编码',
						field: 'parent'
					}
				]
			});
			top.layer.close(index);
		},
		btnAlign: 'c',
//		success: function(index, layero) {
//			$(".searchItemLayerDate").datepicker({
//				language: "zh-TW",
//				autoclose: true, //选中之后自动隐藏日期选择框
//				format: "yyyy-mm-dd" //日期格式，详见 
//			});
//		},
		end: function() {
			$("#searchItemLaye").remove();
		}
	});
}