/* 
 * 文件名称 :productBootstraptree.js
 * 创建者 : 郭恒开
 * 创建日期: 2018/04/12
 * 文件描述: 组织机构的treeGrid操作
 * 
 */
function closeSon(ele) {
	ele.each(function(i, v) {
		if($(v).find(".glyphicon").length) {
			var sonIndex = $(this).attr("class").split("-")[1];
			console.log(sonIndex);
			$("tr[parent='parent-" + sonIndex + "']").hide();
			closeSon($("tr[parent='parent-" + sonIndex + "']"))
		}
	})
}

function openSon(ele) {
	ele.each(function(i, v) {
		var sonIndex = $(this).attr("class").split("-")[1];
		$("tr[parent='parent-" + sonIndex + "']").show();
		openSon($("tr[parent='parent-" + sonIndex + "']"));
	})
}
$(document).ready(function() {
	var data = [{
			"id": "134312",
			"name": "测试01",
			"parent": ""
		},
		{
			"id": "cp01",
			"name": "产品01NGS",
			"parent": "xxx"
		}
	]
	var treegridIndex = 3;
	openItem();

	function openItem() {
		$(".glyphicon-chevron-right").unbind("click").click(function() {
			var paddLeft = $(this).css("padding-left");
			var parentIndex = $(this).parent("td").parent("tr").attr("class").split("-")[1];
			$(this).removeClass("glyphicon-chevron-right").addClass("glyphicon glyphicon-chevron-down");
			if($("tr[parent='parent-" + parentIndex + "']").length) {
				$("tr[parent='parent-" + parentIndex + "']").show();
				openSon($("tr[parent='parent-" + parentIndex + "']"));
			} else {
				for(var i = 0; i < data.length; i++) {
					var tr = $("<tr class='treegr-" + treegridIndex + "' parent='parent-" + parentIndex + "'></tr>");
					var tds = "";
					var paddMore = parseInt(paddLeft) + 10;
					var paddMmore = parseInt(paddLeft) + 25;
					if(data[i].parent) {
						tds += "<td><span class='glyphicon glyphicon-chevron-right' style='padding-left:" + paddMore + "'></span><input type='checkbox'/></td>";

					} else {
						tds += "<td><input type='checkbox' style='margin-left:" + paddMmore + "'/></td>";
					}
					tds += "<td>" + data[i].id + "</td>";
					tds += "<td>" + data[i].name + "</td>";
					tr.append(tds);
					$(this).parent("td").parent("tr").after(tr);
					treegridIndex++;
				}
				openItem();
			}
			$(".glyphicon-chevron-down").unbind("click").click(function() {
				console.log("xxxxx");
				$(this).removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-right");
				var parentIndex = $(this).parent("td").parent("tr").attr("class").split("-")[1];
				$("tr[parent='parent-" + parentIndex + "']").hide();
				closeSon($("tr[parent='parent-" + parentIndex + "']"))
				openItem();
			});

		});
	}