<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <title>报表发送-待发送样本</title> -->
<script type="text/javascript" src="${ctx}/js/report/showSampleWaitList.js"></script>
<script type="text/javascript"	src="${ctx}/javascript/handleSearchForm.js"></script>
<body>
	<div>
		<div id="report_wait_grid_div"></div>
	</div>
	<div id="bat_rep_div" style="display:none" >
		<form  class="frame-table">
			<table>
			<tr>
						<td class="label-title">
							<fmt:message key="biolims.common.detectionItems"/>
						</td>
						<td>
							<input type="text" id="sampleJcxm">
						</td>
					</tr>
					<tr>
						<td class="label-title">
							<fmt:message key="biolims.common.sampleCode"/>
						</td>
						<td>
							<input type="text" id="sampleYbh">
						</td>
					</tr>
					
			
			
			
			</table>
		</form>	
	</div>
</body>
</html>