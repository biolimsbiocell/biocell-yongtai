<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>报表发送</title>
<head>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<link rel=stylesheet type="text/css" href="${ctx}/css/sample.css">
<script type="text/javascript" src="${ctx}/js/report/mainframe.js"></script>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var jsBtn = Ext.get('jsBtn');
jsBtn.on('click', 
 function js(){
var win = Ext.getCmp('js');
if (win) {win.close();}
var js= new Ext.Window({
id:'js',modal:true,title:'<fmt:message key="biolims.common.recipient"/>',layout:'fit',width:document.body.clientWidth/1.5,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/core/user/userSelect.action?flag=js' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 js.close(); }  }]  });     js.show(); }
);
});
 function setjs(id,name){
 document.getElementById("accept_user_id").value = id;
document.getElementById("accept_user_name").value = name;
var win = Ext.getCmp('js')
if(win){win.close();}
}
</script>

		
		<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var qcBtn = Ext.get('qcBtn');
qcBtn.on('click', 
 function qc(){
var win = Ext.getCmp('qc');
if (win) {win.close();}
var qc= new Ext.Window({
id:'qc',modal:true,title:'<fmt:message key="biolims.common.auditor"/>',layout:'fit',width:document.body.clientWidth/1.5,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/core/user/userSelect.action?flag=qc' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 qc.close(); }  }]  });     qc.show(); }
);
});
 function setqc(id,name){
 document.getElementById("qc_user_id").value = id;
document.getElementById("qc_user_name").value = name;
var win = Ext.getCmp('qc')
if(win){win.close();}
}
</script>


</head>

<body>
	<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
	<div style="float:left;width:25%" id="left"></div>
	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass">
		<input type="hidden" id="report_id" value="${requestScope.reportId}">
		<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
<%-- 		<input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" /> --%>
		
	<s:form theme="simple" method="post" id="report_form"> 
		<input type="hidden" name="itemDataJson" id="item_data_json">
		<table class="frame-table">
			<tr>
				<td class="label-title">
					<fmt:message key="biolims.common.serialNumber"/>
				</td>
				<td>
					<s:textfield maxlength="32" id="sr_id" name="sr.id" cssClass="input-20-length readonlytrue" readonly="true"></s:textfield>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
				</td>
				<td class="label-title">
					<fmt:message key="biolims.common.describe"/>
				</td>
				<td>
					<s:textfield maxlength="100" id="note" name="sr.note" cssClass="input-40-length"></s:textfield>
				</td>
				<td class="label-title">
					<fmt:message key="biolims.common.commandPerson"/>
				</td>
				<td >
					<s:textfield id="send_user_name" readonly="true" cssClass="readonlytrue" name="sr.sendUser.name"></s:textfield>
					<s:hidden id="send_user_id" name="sr.sendUser.id"></s:hidden>
				</td>
			</tr>
			<tr>
			<td class="label-title"><fmt:message key="biolims.common.firstInstance"></fmt:message> </td>
					<td><input type="text" name="acceptUser" id="accept_user_name"
						readonly="readonly"
						value='<s:property value="sr.acceptUser.name"/>'> <s:hidden
							name="sr.acceptUser.id" id="accept_user_id" /> <span id="jsBtn"
						class="select-search-btn">&nbsp;&nbsp;&nbsp;</span> <img class='requiredimage'
						src='${ctx}/images/required.gif' /></td>
			
			<td class="label-title">
					<fmt:message key="biolims.common.commandTime"/>
				</td>
				<td>
					<input type="text" class="readonlytrue" readonly="readonly" name="sr.confirmDate" value='<s:date name="sr.confirmDate" format="yyyy-MM-dd HH:mm:ss"/>'>
				</td>
				<td class="label-title"><fmt:message key="biolims.common.retrial"/></td>
				<td><input type="text" name="qcUser" id="qc_user_name"
						readonly="readonly"
						value='<s:property value="sr.qcUser.name"/>'> <s:hidden
							name="sr.qcUser.id" id="qc_user_id" /> <span id="qcBtn"
						class="select-search-btn">&nbsp;&nbsp;&nbsp;</span> <img class='requiredimage'
						src='${ctx}/images/required.gif' />
				</td>
			</tr>
			<tr>
				<td class="label-title">
					<fmt:message key="biolims.common.workflowState"/>
				</td>
				<td>
				<s:hidden name="sr.state" id = "sr_state"> </s:hidden>
				<s:textfield id="sr_stateName" name="sr.stateName" cssClass="readonlytrue" readonly="true"> </s:textfield>
				</td>
			</tr>																
		</table>
		<input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
	</s:form>
<!-- 	<div class="main-centent"> -->
<!-- 		<div> -->
<!-- 			<div class="lefttab" id="left"></div> -->
<!-- 		</div> -->
<!-- 		<div  id="right" class="righttab"> -->
<!-- 		<div id="report_div"></div> -->
		<div id="report_item_div" width="100%" height:10px></div>	
<!-- 		</div>	 -->
<!-- 		</div> -->
	</div>
</body>
</html>