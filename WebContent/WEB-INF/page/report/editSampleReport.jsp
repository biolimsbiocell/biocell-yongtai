<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<link rel="stylesheet" href="${ctx}/css/experimentLab.css" />
<body>
	<div class="container-fluid" padding: 0px"
		 id="content" style="margin-left: 0px;">
		<input type="hidden" id="bpmTaskId" value="${requestScope.bpmTaskId}" />
		<section class="content">
		<div class="row">
			<!--表格-->
			<div class="col-xs-12">
				<div class="box box-info box-solid">
					<div class="box-header with-border">
						<i class="fa fa-bell-o"></i>
						<h3 class="box-title">
							<fmt:message key="biolims.common.reportGeneration" /><small style="color: #fff;"> <fmt:message
									key="biolims.common.orderId" />: <span id="report_id"><s:property
										value="report.id" /></span> <fmt:message
									key="biolims.common.commandPerson" />: <span
								userId="<s:property value="report.sendUser.id"/>"
								id="report_sendUser"><s:property
										value="report.sendUser.name" /></span> <fmt:message
									key="biolims.sample.createDate" />: <span
								id="report_sendDate"><s:property
										value="report.sendDate" /></span> <fmt:message
									key="biolims.common.state" />: <span
								state="<s:property value="report.state"/>" id="report_state"><s:property
										value="report.stateName" /></span>
							</small>
						</h3>
					</div>
					<div class="box-body">
						<!--待处理样本-->
						<div class="col-md-8 col-xs-12">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title"><fmt:message key="biolims.workflow.waitReport" /></h3>
								</div>
								<div class="box-body ipadmini">
									<table
										class="table table-hover table-striped table-bordered table-condensed"
										id="reportTemp" style="font-size: 14px;"></table>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-xs-12">
							<!--SOP-->
							<div class="box box-primary">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title">
										<fmt:message key="biolims.common.note" />
									</h3>
								</div>
								<!-- /.box-header -->
								<div class="box-body">
									<div class="input-group">
										<span class="input-group-addon bg-aqua"><fmt:message
												key="biolims.common.describe" /></span> <input type="text"
											id="report_note" class="form-control"
											changelog="<s:property value=" report.note "/>"
											placeholder="<fmt:message key="biolims.common.placeholder"/>"
											value="<s:property value=" report.note "/>">
									</div>
									<div class="input-group" style="margin-top: 10px">
										<span class="input-group-addon bg-aqua"><fmt:message key="biolims.common.reportDate" /></span> <input
											type="text"
											changelog="<s:property value=" report.reportDate "/>"
											id="reportDate" class="form-control" class="Wdate"
											value="<s:date name=" report.reportDate " format="yyyy-MM-dd "/>">
									</div>
								</div>

							</div>
							<!--实验员-->
							<div class="box box-info">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title">
										<fmt:message key="biolims.common.chooseTheExperimenter" />
									</h3>
								</div>
								<div class="box-body no-padding">
									<ul class="users-list clearfix">
										<c:forEach var="selUser" items="${selUser}">
											<input type="hidden" class="changelogUserid"
												value="${selUser.user.id}" />
											<input type="hidden" class="changelogUsername"
												value="${selUser.user.name}" />
											<li class="userLi" userid="${selUser.user.id}"><img
												class="userChosed" src="${ctx}/lims/dist/img/testuser.png"
												alt="User Image"> <a class="users-list-name" href="#">${selUser.user.name}</a>
											</li>
										</c:forEach>
										<c:forEach var="user" items="${user}">
											<li class="userLi" userid="${user.user.id}"><img
												src="${ctx}/lims/dist/img/testuser.png" alt="User Image">
												<a class="users-list-name" href="#">${user.user.name}</a></li>
										</c:forEach>
									</ul>
								</div>
							</div>
						</div>

					</div>
					<div class="box-footer">
						<div class="pull-right">
							<button type="button" class="btn btn-primary" id="save">
								<i class="glyphicon glyphicon-saved"></i>
								<fmt:message key="biolims.common.save" />
							</button>
							<button type="button" class="btn btn-primary" id="next">
								<i class="glyphicon glyphicon-arrow-down"></i>
								<fmt:message key="biolims.common.nextStep" />
							</button>
						</div>

					</div>
				</div>
			</div>

		</div>

		</section>
	</div>
	<script type="text/javascript"
		src="${ctx}/js/report/editSampleReport.js"></script>
</body>
</html>