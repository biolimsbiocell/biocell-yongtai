<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<link rel="stylesheet" href="${ctx}/css/experimentLab.css" />
<style type="text/css">
			.dt-buttons {
				float: none;
			}
			.tablebtns{
				position: initial;
			}
		</style>
<body>
<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div class="container-fluid" padding: 0px"
		 id="content" style="margin-left: 0px;">
		<input type="hidden" id="bpmTaskId" value="${requestScope.bpmTaskId}" />
		<input type="hidden" id="report_note" value="<s:property value="report.note"/>">
		<input type="hidden" id="checkUser_name"
			value="<s:property value="dnaTask.checkUser.name"/>">
		<input type="hidden" id="checkUser_id"
			value="<s:property value="dnaTask.checkUser.id"/>">
		<section class="content">
		<div class="row">
			<!--表格-->
			<div class="col-xs-12">
				<div class="box box-info box-solid">
					<div class="box-header with-border">
						<i class="fa fa-bell-o"></i>
						<h3 class="box-title">
							<fmt:message key="biolims.common.reportGeneration" /> <small style="color: #fff;"> <fmt:message
									key="biolims.common.orderId" />: <span id="report_id"><s:property
										value="report.id" /></span> <fmt:message
									key="biolims.common.commandPerson" />: <span
								userId="<s:property value="report.sendUser.id"/>"
								id="report_sendUser"><s:property
										value="report.sendUser.name" /></span> <fmt:message
									key="biolims.sample.createDate" />: <span
								id="report_sendDate"><s:property
										value="report.sendDate" /></span> <fmt:message
									key="biolims.common.state" />: <span
								state="<s:property value="report.state"/>" id="report_state"><s:property
										value="report.stateName" /></span>
							</small>
						</h3>
					</div>
					<div class="box-body">
						<!--待处理样本-->
						<div class="col-md-12 col-xs-12">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title"><fmt:message key="biolims.common.detailed" /></h3>
								</div>
								<div class="box-body ipadmini">
									<table
										class="table table-hover table-striped table-bordered table-condensed"
										id="reportItem" style="font-size: 14px;"></table>
								</div>
							</div>
						</div>
					</div>
					<div class="box-footer">
						<div class="pull-right">
						<button type="button" class="btn btn-primary" onclick="tjsp()" id="tjsp"><i class="glyphicon glyphicon-random"></i> <fmt:message key="biolims.common.submit" />
                </button>
                	<button type="button" class="btn btn-primary" style="display: none" id="sp" onclick="sp()" ><i class="glyphicon glyphicon-saved"></i> <fmt:message key="biolims.common.handle"/>
                </button>
                <button type="button" class="btn btn-primary"  id="changeState" onclick="changeState()" ><i class="glyphicon glyphicon-saved"></i> <fmt:message key="biolims.workflow.completeName" />
                </button>
							<button type="button" class="btn btn-primary" id="save" onclick="saveItem()">
								<i class="glyphicon glyphicon-saved"></i>
								<fmt:message key="biolims.common.save" />
							</button>
							<button type="button" class="btn btn-primary" id="pre">
								<i class="glyphicon glyphicon-arrow-up"></i>
								<fmt:message key="biolims.common.back" />
							</button>
						</div>

					</div>
				</div>
			</div>

		</div>

		</section>
	</div>
<script type="text/javascript" src="${ctx}/js/report/showSampleReportItemList.js"></script>
</body>
</html>