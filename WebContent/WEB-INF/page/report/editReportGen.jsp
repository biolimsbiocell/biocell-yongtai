<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ page language="java" import="java.util.List" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>

<%
	String path = request.getContextPath();
	String cPath = request.getScheme() + "://"+ request.getServerName() + ":" + request.getServerPort()+ path ;
	String basePath = request.getScheme() + "://"+ request.getServerName() + ":" + request.getServerPort()+ path + "/operfile/downloadById.action?id="+request.getAttribute("fileId");
	String basePathDownId = request.getScheme() + "://"+ request.getServerName() + ":" + request.getServerPort()+ path + "/operfile/downloadById.action?id=";
	String basePathDownName = request.getScheme() + "://"+ request.getServerName() + ":" + request.getServerPort()+ path + "/operfile/downloadFileByPath.action?fileName=";
%>
<script type="text/javascript">
function viewdoc() {
	document.all.DSOFramer.ShowView(3);
	document.all.DSOFramer.Open("<%=basePath%>", true, "Word.Document");
	document.all.DSOFramer.Menubar = true;
 	document.all.DSOFramer.SetMenuDisplay(64);
	document.all.DSOFramer.SetMenuDisplay(126); 
	var word = new Object(document.all.DSOFramer.ActiveDocument);
	cs();
}
function savedoc() {
	}
		function printdoc() {
			document.all.DSOFramer.PrintOut();
		}
		function uploaddoc(){
			document.all.DSOFramer.HttpInit();
			document.all.DSOFramer.HttpAddPostCurrFile("File",encodeURI("<%=request.getAttribute("fileName")%>"));
			document.all.DSOFramer.HttpPost("<%=cPath%>/fileUpload?contentId=<%=request.getAttribute("contentId")%>&modelType=<%=request.getAttribute("type")%>&useType=doc&type=1&picUserId="+window.userId);
			alert("上传保存成功！");
		}
		function ppw() {
			document.all.DSOFramer.PrintPreview();
		}
		function ppwe() {
			document.all.DSOFramer.PrintPreviewExit();
		}
		function reloaddoc(){
			document.location.href=this.location.href+"&reloadAction=true";
		}
		function cs() {
			document.all.DSOFramer.SetFieldValue("Code","<%=request.getAttribute("Code")%>","");
			document.all.DSOFramer.SetFieldValue("unit_name","<%=request.getAttribute("unit_name")%>","");
			document.all.DSOFramer.SetFieldValue("PatientName","<%=request.getAttribute("PatientName")%>","");
			document.all.DSOFramer.SetFieldValue("Gender","<%=request.getAttribute("Gender")%>","");
			document.all.DSOFramer.SetFieldValue("age","<%=request.getAttribute("age")%>","");
			document.all.DSOFramer.SetFieldValue("solution_name","<%=request.getAttribute("solution_name")%>","");
			document.all.DSOFramer.SetFieldValue("phone","<%=request.getAttribute("phone")%>","");
			document.all.DSOFramer.SetFieldValue("SampleKind","<%=request.getAttribute("SampleKind")%>","");
			document.all.DSOFramer.SetFieldValue("doctor_name","<%=request.getAttribute("doctor_name")%>","");
			document.all.DSOFramer.SetFieldValue("mobile","<%=request.getAttribute("mobile")%>"," ");
			document.all.DSOFramer.SetFieldValue("inspectDate","<%=request.getAttribute("inspectDate")%>","");
			document.all.DSOFramer.SetFieldValue("clinDiagnos","<%=request.getAttribute("clinDiagnos")%>","");
			document.all.DSOFramer.SetFieldValue("disease","<%=request.getAttribute("disease")%>","");
			document.all.DSOFramer.SetFieldValue("disea","<%=request.getAttribute("disease")%>","");
			document.all.DSOFramer.SetFieldValue("Correlation","<%=request.getAttribute("clinDiagnos")%>","");
			document.all.DSOFramer.SetFieldValue("sequencingName","<%=request.getAttribute("sequencingName")%>",""); 
			document.all.DSOFramer.SetFieldValue("dicSummary","<%=request.getAttribute("dicSummary")%>",""); 
			document.all.DSOFramer.SetFieldValue("nation","<%=request.getAttribute("nation")%>",""); 
			var i= 1;
			/* <s:iterator value="#request.listGenVer" id="listGenVer">
				i++;
				document.all.DSOFramer.ActiveDocument.Tables(2).Rows.Add();
			    document.all.DSOFramer.ActiveDocument.Tables(2).Cell(i, 1).Range.InsertAfter("<s:property escape="false" value='%{#listGenVer.geneName}'/>");
	     		document.all.DSOFramer.ActiveDocument.Tables(2).Cell(i, 2).Range.InsertAfter("<s:property escape="false" value='%{#listGenVer.cspot}'/>");
	     		document.all.DSOFramer.ActiveDocument.Tables(2).Cell(i, 3).Range.InsertAfter("<s:property escape="false" value='%{#listGenVer.type}'/>");
	     		document.all.DSOFramer.ActiveDocument.Tables(2).Cell(i, 4).Range.InsertAfter("<s:property escape="false" value='%{#listGenVer.pspot}'/>");
	     		document.all.DSOFramer.ActiveDocument.Tables(2).Cell(i, 5).Range.InsertAfter("<s:property escape="false" value='%{#listGenVer.jb}'/>");
	     		document.all.DSOFramer.ActiveDocument.Tables(2).Cell(i, 6).Range.InsertAfter("<s:property escape="false" value='%{#listGenVer.yc}'/>");
	     		document.all.DSOFramer.ActiveDocument.Tables(2).Cell(i, 7).Range.InsertAfter("<s:property escape="false" value='%{#listGenVer.hgmdPos}'/>");
		 </s:iterator> */
		
		 /* <s:iterator value="#request.listGenVer1" id="listGenVer1">
				i++;
				document.all.DSOFramer.ActiveDocument.Tables(4).Rows.Add();
			    document.all.DSOFramer.ActiveDocument.Tables(4).Cell(i, 1).Range.InsertAfter("<s:property escape="false" value='%{#listGenVer1.tbjy}'/>");//突变基因
  				document.all.DSOFramer.ActiveDocument.Tables(4).Cell(i, 2).Range.InsertAfter("<s:property escape="false" value='%{#listGenVer1.cspot}'/>");//核苷酸改变
  				document.all.DSOFramer.ActiveDocument.Tables(4).Cell(i, 3).Range.InsertAfter("<s:property escape="false" value='%{#listGenVer1.type}'/>");//纯和/杂合
  				document.all.DSOFramer.ActiveDocument.Tables(4).Cell(i, 4).Range.InsertAfter("<s:property escape="false" value='%{#listGenVer1.pspot}'/>");//氨基酸改变
  				document.all.DSOFramer.ActiveDocument.Tables(4).Cell(i, 5).Range.InsertAfter("<s:property escape="false" value='%{#listGenVer1.zcrpl}'/>");//致病性分析
  				document.all.DSOFramer.ActiveDocument.Tables(4).Cell(i, 6).Range.InsertAfter("<s:property escape="false" value='%{#listGenVer1.jbname}'/>");//疾病/表型
 	 			document.all.DSOFramer.ActiveDocument.Tables(4).Cell(i, 7).Range.InsertAfter("<s:property escape="false" value='%{#listGenVer1.ycfs}'/>");//遗传方式
		 </s:iterator> */
		}
</script>		
</head>
<body onload="setTimeout(viewdoc,1000);">
		<div id="dsoDoc" align="center" >
		<input type="button" value='<fmt:message key="biolims.common.saveToTheServer"/>' onclick="uploaddoc()"></input>
		&nbsp;&nbsp;&nbsp;
		<input type="button" value='<fmt:message key="biolims.common.generateANewReport"/>' onclick="reloaddoc()"></input>
		&nbsp;&nbsp;&nbsp;
		<input type="button" value='<fmt:message key="biolims.common.printPreview"/>' onclick="ppw();"></input>
		&nbsp;&nbsp;&nbsp;
		<input type="button" value='<fmt:message key="biolims.common.outOfPrintPreview"/>' onclick="ppwe();"></input>
		<OBJECT id='DSOFramer' align='middle' style='LEFT: 0px; WIDTH: 100%; TOP: 0px; HEIGHT: 95%'
		classid=clsid:00460182-9E5E-11d5-B7C8-B8269041DD57 codeBase='${ctx}/dso/dso.CAB#V2.3.0.1'>
		<PARAM NAME='_ExtentX' VALUE='6350'>
		<PARAM NAME='_ExtentY' VALUE='6350'>
		<PARAM NAME='BorderColor' VALUE='-2147483632'>
		<PARAM NAME='BackColor' VALUE='-2147483643'>
		<PARAM NAME='ActivationPolicy' VALUE='6'>
		<PARAM NAME='ForeColor' VALUE='-2147483640'>
		<PARAM NAME='TitlebarColor' VALUE='-2147483635'>
		<PARAM NAME='TitlebarTextColor' VALUE='-2147483634'>
		<PARAM NAME='BorderStyle' VALUE='1'>
		<PARAM NAME='Titlebar' VALUE='0'>
		<PARAM NAME='Toolbars' VALUE='1'>
		<PARAM NAME='Menubar' VALUE='0'>
		<param name='BorderStyle' value='1'>
	</OBJECT>
	</div>
</body>
</html>