<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title='<fmt:message key="biolims.common.attachment"/>'
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=weiChatCancerType&id=${weiChatCancerType.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/weichat/weiChatCancerTypeEdit.js"></script>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title"  ><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"   >
                   	<input type="text" size="20" maxlength="25" id="weiChatCancerType_id"
                   	 name="weiChatCancerType.id" title="<fmt:message key="biolims.common.serialNumber"/>"
                   	   
	value="<s:property value="weiChatCancerType.id"/>"
                   	  
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.name"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="50" id="weiChatCancerType_cancerTypeName"
                   	 name="weiChatCancerType.cancerTypeName" title="<fmt:message key="biolims.common.name"/>"
                   	   
	value="<s:property value="weiChatCancerType.cancerTypeName"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			<td class="label-title"><fmt:message key="biolims.common.attachment"/></td><td></td>
						<td title="<fmt:message key="biolims.common.afterSaveCanLookAtTheAttachment"/>" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message key="biolims.common.attachment"/></span>
			</tr>
			
			
            </table>
            <input type="hidden" name="weiChatCancerTypeSeedOneJson" id="weiChatCancerTypeSeedOneJson" value="" />
            <input type="hidden" name="weiChatCancerTypeSeedTwoJson" id="weiChatCancerTypeSeedTwoJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="weiChatCancerType.id"/>" />
            </form>
            <div id="tabs">
            <ul>
			<li><a href="#weiChatCancerTypeSeedOnepage"><fmt:message key="biolims.common.tumorChildTableOne"/></a></li>
			<li><a href="#weiChatCancerTypeSeedTwopage"><fmt:message key="biolims.common.tumorChildTableTwo"/></a></li>
           	</ul> 
			<div id="weiChatCancerTypeSeedOnepage" width="100%" height:10px></div>
			<div id="weiChatCancerTypeSeedTwopage" width="100%" height:10px></div>
			</div>
        	</div>
	</body>
	</html>
