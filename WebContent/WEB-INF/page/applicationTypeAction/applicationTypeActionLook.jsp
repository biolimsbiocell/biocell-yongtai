<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@page import="java.util.*,com.biolims.applicationType.model.*" %>
<head>
<title></title>

<link rel="stylesheet" href="${ctx}/lims/plugins/icheck/skins/square/blue.css">

<%	
String formId =(String)request.getAttribute("formId");
List<ApplicationTypeAction> ataList = new ArrayList();
ataList = (List<ApplicationTypeAction>)  request.getAttribute("applicationTypeActionList");  
%>
</head>
<body>
	 <input type="hidden" id="applicationTypeActionId"  />
	  <input type="hidden" id="formId"  value="<%=formId%>"/>
		<table style="margin-top: 81px;width: 80%;" class="section_table" cellpadding="0"
								cellspacing="0" >
								<tbody>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span>
										 	<label		title=<fmt:message key="biolims.common.select"/>  class="text label"><fmt:message key="biolims.common.selectStateMovement"/>	</label>
										 </td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap>
											<table>
											<%
											for(ApplicationTypeAction wr:ataList){
											%>
												<tr class="text">	
													<td>
														<div align="center" id="state_div"><input type="radio" id="select_state" name="select_state" onclick="check(this.value)" value="<%=wr.getId()%>" />
														</div>
													</td>
													<td>
													<c:if test='${sessionScope.lan=="zh_CN"}'>
														<div align="left"><%=wr.getActionName() %></div>
													</c:if>
													<c:if test='${sessionScope.lan=="en"}'>
														<div align="left"><%=wr.getEnName() %></div>
													</c:if>
													</td>
													<td>
													</td>
									</tr>
									<%}%>
							</table></td>
							<td >&nbsp;</td>
							</tr>
							</tbody>
							</table>
							<script type="text/javascript" src="${ctx}/lims/plugins/jQuery/jquery-2.2.3.min.js"></script>
							<script type="text/javascript" src="${ctx}/lims/plugins/icheck/icheck.min.js"></script>
							<script type="text/javascript" src="${ctx}/js/applicationTypeAction/applicationTypeActionLook.js"></script>
							
		</body>
</html>