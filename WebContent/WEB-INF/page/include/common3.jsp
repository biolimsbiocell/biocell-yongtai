<link rel="stylesheet" type="text/css" href="${ctx}/javascript/lib/ext-3.4.0/resources/css/ext-all.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/javascript/lib/ext-3.4.0/examples/ux/css/LockingGridView.css" />
<script type="text/javascript" src="${ctx}/javascript/lib/ext-3.4.0/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="${ctx}/javascript/lib/ext-3.4.0/ext-all.js"></script>
<script type="text/javascript" src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/LockingGridView.js"></script>
<script type="text/JavaScript" src="${ctx}/js/language/ext-lang-<%=session.getAttribute("lan") %>.js"></script>
<script type="text/javascript" src="${ctx}/javascript/ext-ui-combo-pagesize.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleWorkflow.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleExt.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleJson.js"></script>
<script type="text/javascript" src="${ctx}/javascript/gridToExcel.js"></script>
<style type= "text/css" >   
    .x-selectable, .x-selectable * {   
        -moz-user-select: text! important ;   
        -khtml-user-select: text! important ;   
    }  
    </style>
<script>
if  (!Ext.grid.GridView.prototype.templates) {   
    Ext.grid.GridView.prototype.templates = {};   
}   
Ext.grid.GridView.prototype.templates.cell =  new  Ext.Template(   
     '<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}" tabIndex="0" {cellAttr}>' ,   
     '<div class="x-grid3-cell-inner x-grid3-col-{id}" {attr}>{value}</div>' ,   
     '</td>'   
);
</script>
<link rel=stylesheet type="text/css" href="${ctx}/css/style.css">
<link rel=stylesheet type="text/css" href="${ctx}/javascript/lib/jquery-ui/1.8.17/css/default/jquery-ui-1.8.17.custom.css">
<script type="text/javascript" src="${ctx}/javascript/lib/jquery-1.7.1.js"></script>
<script type="text/javascript" src="${ctx}/javascript/lib/jquery-impromptu.1.5.js"></script>
<script type="text/javascript" src="${ctx}/javascript/validate.js"></script>
<script type="text/javascript" src="${ctx}/javascript/lib/json2.js"></script>
<script type="text/javascript" src="${ctx}/javascript/lib/jquery.dateFormat-1.0.js"></script>
<script type="text/javascript" src="${ctx}/javascript/lib/jquery-ui/1.8.17/jquery-ui.js"></script>
<script type="text/javascript" src="${ctx}/javascript/common.js"></script>
<script type="text/javascript" src="${ctx}/javascript/lib/My97DatePicker/WdatePicker.js"></script>
    