<link rel=stylesheet type="text/css" href="${ctx}/css/style.css">
<link rel=stylesheet type="text/css" href="${ctx}/javascript/lib/jquery-ui/1.8.17/css/default/jquery-ui-1.8.17.custom.css">
<script type="text/javascript" src="${ctx}/javascript/lib/jquery-1.7.1.js"></script>
<script type="text/javascript" src="${ctx}/javascript/lib/jquery-impromptu.1.5.js"></script>
<script type="text/javascript" src="${ctx}/javascript/validate.js"></script>
<script type="text/javascript" src="${ctx}/javascript/lib/json2.js"></script>
<script type="text/javascript" src="${ctx}/javascript/lib/jquery.dateFormat-1.0.js"></script>
<script type="text/javascript" src="${ctx}/javascript/lib/jquery-ui/1.8.17/jquery-ui.js"></script>
<script type="text/javascript" src="${ctx}/javascript/common.js"></script>
