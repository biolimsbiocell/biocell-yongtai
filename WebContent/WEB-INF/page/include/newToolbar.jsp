<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@page import="com.biolims.util.DateUtil"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.biolims.common.constants.SystemConstants"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<fmt:setBundle basename="ResouseInternational/msg" />
<head>
<%
String workflowRights = "";
//是否从待办事宜过来
if((request.getParameter("bpmTaskId")!=null&&!request.getParameter("bpmTaskId").equals("")&&!request.getParameter("bpmTaskId").equals("null"))||request.getAttribute("adminTaskId")!=null){
	workflowRights = "true";
}
String rightsAction = ""; 
//获得用户所包含的功能权限
if(request.getAttribute("rightsAction")!=null)
	rightsAction = (String)request.getAttribute("rightsAction");  
String workflowState = ""; 
//获得记录状态
if(request.getAttribute("workflowState")!=null)
	workflowState = (String)request.getAttribute("workflowState");  
//获得功能下的权限
String rightsFunction = ""; 
if(request.getAttribute("rightsFunction")!=null)
	rightsFunction = (String)request.getAttribute("rightsFunction");  
//获得功能模式
String handlemethod = "";
if(request.getAttribute("handlemethod")!=null)
	handlemethod = (String)request.getAttribute("handlemethod");  
String copyMode = "false";
//是否为复制模式
if(request.getAttribute("copyBl")!=null&&request.getAttribute("copyBl").equals("true")){
	String newhandlemethod = "add";
	copyMode = "true";
	request.setAttribute("handlemethod",newhandlemethod);
}
//在审批中,将功能模式设置为“查看”
if(workflowState.equals(SystemConstants.DIC_STATE_WORKFLOW_IN_PROCESS)){
	String newhandlemethod = SystemConstants.PAGE_HANDLE_METHOD_VIEW;
	request.setAttribute("handlemethod",newhandlemethod);
}
//审批状态为取消/无效
if(workflowState.equals(SystemConstants.DIC_STATE_WORKFLOW_NO_PROCESS)){
	String newhandlemethod = SystemConstants.PAGE_HANDLE_METHOD_VIEW;
	request.setAttribute("handlemethod",newhandlemethod);
}
//审批状态为有效，审批完
if(workflowState.equals(SystemConstants.DIC_STATE_WORKFLOW_YES_PROCESS)){
	String newhandlemethod = SystemConstants.PAGE_HANDLE_METHOD_VIEW;
	request.setAttribute("handlemethod",newhandlemethod);
}
%>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<style>
			.dataTable>thead>tr>th,
			.dataTable>tbody>tr>td {
				word-break: keep-all !important;
			}
			.glyphicon {
				margin-right: 3px;
			}
			.tablebtns{
				float:right;
			}
			.dt-buttons{
				margin:0;
				float:right;
			}
			.dt-button-collection {
				font-size: 12px;
				max-height:260px;
				overflow:auto;
			}
			.box-title small{
				color:#fff;
			}
			.layui-layer-content{
				padding: 0 20px;
			}
		</style>
	</head>

	<body>
		<div role="group" style="position: fixed; top: 0; background: #eee; z-index: 100; width: 100%; padding: 15px 15px 5px">
			<button type="button" class="btn btn-info" onclick="search()">
			<i class="glyphicon glyphicon-search"></i> <fmt:message key="biolims.common.find"/>
		</button>
		
		<%//有新建权限，在列表，详细，新增模式下
		if(rightsFunction.contains(SystemConstants.RIGHTS_ADD)&&rightsAction.contains(SystemConstants.RIGHTS_ADD)){%> 
			<button id="btn_add" type="button" class="btn btn-info" onclick="add()">
			<i class="glyphicon glyphicon-plus"></i> <fmt:message key="biolims.common.create"/>
		</button>
		<%}%>
		
		<%//有修改权限，在列表模式
		if(rightsFunction.contains(SystemConstants.RIGHTS_MODIFY)&&rightsAction.contains(SystemConstants.RIGHTS_MODIFY)){%> 
			<button id="btn_edit" type="button" class="btn btn-info" onclick="edit()">
			<i class="glyphicon glyphicon-pencil"></i> <fmt:message key="biolims.common.edit"/>
		</button>
		<%}%>
			<button id="btn_view" type="button" class="btn btn-info" onclick="view()">
			<i class="glyphicon glyphicon-eye-open"></i> <fmt:message key="biolims.common.check"/>
		</button>
		</div>
	</body>

</html>