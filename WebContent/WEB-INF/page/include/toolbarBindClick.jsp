﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@page import="com.biolims.util.DateUtil"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.biolims.common.constants.SystemConstants"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<%
String workflowRights = "";
//是否从待办事宜过来
if((request.getParameter("bpmTaskId")!=null&&!request.getParameter("bpmTaskId").equals("")&&!request.getParameter("bpmTaskId").equals("null"))||request.getAttribute("adminTaskId")!=null){
	workflowRights = "true";
}
String rightsAction = ""; 
//获得用户所包含的功能权限
if(request.getAttribute("rightsAction")!=null)
	rightsAction = (String)request.getAttribute("rightsAction");  
String workflowState = ""; 
//获得记录状态
if(request.getAttribute("workflowState")!=null)
	workflowState = (String)request.getAttribute("workflowState");  
//获得功能下的权限
String rightsFunction = ""; 
if(request.getAttribute("rightsFunction")!=null)
	rightsFunction = (String)request.getAttribute("rightsFunction");  
//获得功能模式
String handlemethod = "";
if(request.getAttribute("handlemethod")!=null)
	handlemethod = (String)request.getAttribute("handlemethod");  
String copyMode = "false";
//是否为复制模式
if(request.getAttribute("copyBl")!=null&&request.getAttribute("copyBl").equals("true")){
	String newhandlemethod = "add";
	copyMode = "true";
	request.setAttribute("handlemethod",newhandlemethod);
}
//在审批中,将功能模式设置为“查看”
if(workflowState.equals(SystemConstants.DIC_STATE_WORKFLOW_IN_PROCESS)){
	String newhandlemethod = SystemConstants.PAGE_HANDLE_METHOD_VIEW;
	request.setAttribute("handlemethod",newhandlemethod);
}

//审批状态为取消/无效
if(workflowState.equals(SystemConstants.DIC_STATE_WORKFLOW_NO_PROCESS)){
	String newhandlemethod = SystemConstants.PAGE_HANDLE_METHOD_VIEW;
	request.setAttribute("handlemethod",newhandlemethod);
}
//审批状态为有效，审批完
if(workflowState.equals(SystemConstants.DIC_STATE_WORKFLOW_YES_PROCESS)){
	String newhandlemethod = SystemConstants.PAGE_HANDLE_METHOD_VIEW;
	request.setAttribute("handlemethod",newhandlemethod);
}
if(workflowState.equals(SystemConstants.DIC_STATE_WORKFLOW_IN_PROCESS_EDIT)){
	String newhandlemethod = SystemConstants.PAGE_HANDLE_METHOD_MODIFY;
	request.setAttribute("handlemethod",newhandlemethod);
}


%>
<script type="text/javascript">
	Ext.onReady(function(){
	<%if(request.getAttribute("noTab")==null){%> 
    	var tabs = new Ext.TabPanel({
        renderTo: 'maintab',
        <s:if test="#request.handlemethod=='list'">
 		height:document.body.clientHeight,
 		</s:if>
 		autoWidth:true,
        activeTab: 0,
        margins: '0 0 0 0',
        items: [{
          title: '${title}',      
          contentEl:'markup'
        }]
    });
    <%}%>
});
</script>
<div  id="toolbar" style="margin:0 0 0 0">
<table id="pageToolbar" class="toolbar" cellspacing="0" cellpadding="0"
	width="100%">
	<tr>
	<%if(request.getParameter("noButton")==null&&request.getAttribute("noButton")==null){%> 
		<td NOWRAP valign="middle">
		<table cellspacing="0" cellpadding="0">
			<tr>
			<s:if test="#request.handlemethod=='list'">
				<td NOWRAP align="right" valign="middle">
				<table cellspacing="0" cellpadding="0" class="toolbarsection">
					<tr>
						<td width="3px" height="29px"></td>
						<td>&nbsp;</td>
					</tr>
				</table>
				</td>
				<td NOWRAP align="right" valign="middle">
					<table cellspacing="0" cellpadding="0" class="toolbarsection">
						<tr>
							<td width="3px" height="23px"></td>
							<td nowrap class="quicksearch_control"><span style="cursor: pointer;" id="opensearch"><img	class="toolbarbuttonimage" 	name="opensearch" src="${ctx}/images/find.gif"	 title="<fmt:message key="biolims.common.openTheSearchCriteria"/>" ><fmt:message key="biolims.common.find"/></span></td>
						</tr>
					</table>
				</td>
				<td style="padding-top: 2px; padding-bottom: 2px" width="2px">
								<table cellpadding="0" cellspacing="0" width="2px" height="19px">
									<tr>
										<td height="4px"></td>
									</tr>
									<tr>
										<td width="2px" height="11px">
										</td>
									</tr>
									<tr>
										<td height="4px"></td>
									</tr>
								</table>
				</td>
				</s:if>
				<td NOWRAP align="right" valign="middle">
				<table cellspacing="0" cellpadding="0" class="toolbarsection">
					<tr>
						<td width="3px" height="23px"></td>
						<td NOWRAP align="left" valign="middle">
						<table cellspacing="0" width="100%" cellpadding="0"	class="toolbarsection">
							<tr>
						<%//有新建权限，在列表，详细，新增模式下
							if(rightsFunction.contains(SystemConstants.RIGHTS_ADD)&&rightsAction.contains(SystemConstants.RIGHTS_ADD)){%> 
								<td NOWRAP class="toolbarbutton" id="toolactions_0"	align="center"	>
									<span style="cursor: pointer;" id="toolbarbutton_add"><img	src="${ctx}/images/new.png"	 class="toolbarbuttonimage" title="<fmt:message key="biolims.common.new"/>"><fmt:message key="biolims.common.create"/></span>
								</td>					
								<%}%>
								<s:if test="#request.handlemethod=='list'">
								<%
								//有修改权限，在列表模式
								if(rightsFunction.contains(SystemConstants.RIGHTS_MODIFY)&&rightsAction.contains(SystemConstants.RIGHTS_MODIFY)){%> 
								<td NOWRAP class="toolbarbutton" id="toolactions_2"	align="center" >
									<span style="cursor: pointer;" id="toolbarbutton_modify" ><img src="${ctx}/images/edit.png" class="toolbarbuttonimage"	 title="<fmt:message key="biolims.common.edit"/>" ><fmt:message key="biolims.common.edit"/></span>
								</td>
								<%}%>
								<%
								if(rightsFunction.contains(SystemConstants.RIGHTS_SEARCH)&&rightsAction.contains(SystemConstants.RIGHTS_SEARCH)){%> 
								<td NOWRAP class="toolbarbutton" id="toolactions_2"	align="center" >
									<span style="cursor: pointer;" id="toolbarbutton_scan" ><img  src="${ctx}/images/search.png" class="toolbarbuttonimage"	 title="<fmt:message key="biolims.common.check"/>" ><fmt:message key="biolims.common.check"/></span>
								</td> 
								<%}%>
								<%if(rightsFunction.contains(SystemConstants.RIGHTS_SEARCH)&&rightsAction.contains(SystemConstants.RIGHTS_SEARCH)){%>
								<td NOWRAP class="toolbarbutton" id="toolactions_4"	align="center" >
									<span style="cursor: pointer;" id="toolbarbutton_export_excel" ><img  src="${ctx}/images/nav_icon_downloadcsv.gif" class="toolbarbuttonimage"  title="<fmt:message key="biolims.common.export"/>" ><fmt:message key="biolims.common.export"/></span>
								</td> 
								<%} %>
								</s:if>
								<s:if test="#request.handlemethod!='list'">
								<td NOWRAP class="toolbarbutton" id="toolactions_3"		align="center" >
									<span style="cursor: pointer;" id="toolbarbutton_list"><img 	src="${ctx}/images/list.png" class="toolbarbuttonimage" title="<fmt:message key="biolims.common.list" />"  ><fmt:message key="biolims.common.list" /></span>
								</td>
								</s:if>
								<!-- 修改连接进入 -->
								<s:if test="#request.handlemethod=='modify'">
								<%//功能有修改，新建权限，用户有修改，新建权限
								if((rightsFunction.contains(SystemConstants.RIGHTS_ADD)&&rightsAction.contains(SystemConstants.RIGHTS_ADD))||(rightsFunction.contains(SystemConstants.RIGHTS_MODIFY)&&rightsAction.contains(SystemConstants.RIGHTS_MODIFY))){%>
									<%//状态为新建或者空或者是复制
										if(workflowState==null||workflowState.equals("")||workflowState.equals(SystemConstants.DIC_STATE_NEW)||copyMode.equals("true")){%>  
								<td NOWRAP class="toolbarbutton" id="toolactions_1"	align="center" >
									<span style="cursor: pointer;" id="toolbarbutton_save" ><img src="${ctx}/images/save.png"	class="toolbarbuttonimage"  title="<fmt:message key="biolims.common.saveAll"/>"	 ><fmt:message key="biolims.common.save"/></span>
									<input type="hidden" id="limsRightsModify" value="1"/>
									
								</td>
									<%}%>
								<%}%>
								</s:if>
								<s:if test="#request.handlemethod=='view'">
								<%//功能有修改，新建权限，用户有修改，新建权限
								if((rightsFunction.contains(SystemConstants.RIGHTS_ADD)&&rightsAction.contains(SystemConstants.RIGHTS_ADD))||(rightsFunction.contains(SystemConstants.RIGHTS_MODIFY)&&rightsAction.contains(SystemConstants.RIGHTS_MODIFY))||(rightsFunction.contains("A")&&rightsAction.contains("A"))){%>
									<%//状态为新建或者空或者是复制
										if(copyMode.equals("true")||(rightsFunction.contains("A")&&rightsAction.contains("A"))){%>  
								<td NOWRAP class="toolbarbutton" id="toolactions_1"	align="center" >
									<span style="cursor: pointer;" id="toolbarbutton_save" ><img src="${ctx}/images/save.png"	class="toolbarbuttonimage"  title="<fmt:message key="biolims.common.saveAll"/>"	 ><fmt:message key="biolims.common.save"/></span>
									<input type="hidden" id="limsRightsModify" value="1"/>
								</td>
									<%}%>
								<%}%>
								</s:if>
								<!-- 新建连接进入 -->
								<s:if test="#request.handlemethod=='add'">
								<%//功能有修改，新建权限，用户有修改，新建权限
								if((rightsFunction.contains(SystemConstants.RIGHTS_ADD)&&rightsAction.contains(SystemConstants.RIGHTS_ADD))||(rightsFunction.contains(SystemConstants.RIGHTS_MODIFY)&&rightsAction.contains(SystemConstants.RIGHTS_MODIFY))){%>
								<%	//状态为新建或者空或者是复制
								if(workflowState==null||workflowState.equals("")||workflowState.equals(SystemConstants.DIC_STATE_NEW)||copyMode.equals("true")){%>  
								<td NOWRAP class="toolbarbutton" id="toolactions_1"		align="center" >
									<span style="cursor: pointer;" id="toolbarbutton_save" ><img	src="${ctx}/images/save.png"	class="toolbarbuttonimage"  title="<fmt:message key="biolims.common.saveAll"/>"	 ><fmt:message key="biolims.common.save"/></span>
									<input type="hidden" id="limsRightsModify" value="1"/>
								</td>									<%}%>
								<%}%>
								</s:if>
								<%
								//从待办事宜进入，并且是编辑中。
								if(workflowRights.equals("true")&&workflowState.equals(SystemConstants.DIC_STATE_WORKFLOW_IN_PROCESS_EDIT)){%> 
								<td NOWRAP class="toolbarbutton" id="toolactions_1"		align="center" > 
									<span style="cursor: pointer;" id="toolbarbutton_save" ><img	src="${ctx}/images/save.png"	class="toolbarbuttonimage"  title="<fmt:message key="biolims.common.saveAll"/>"	 ><fmt:message key="biolims.common.save"/></span>
									<input type="hidden" id="limsRightsModify" value="1"/>
								</td>
								<%}%>
							<%//假如在审批中，通过待办事宜审批连接过来 
								if((workflowState.startsWith(SystemConstants.DIC_STATE_WORKFLOW_IN_PROCESS)||workflowState.equals(SystemConstants.DIC_STATE_WORKFLOW_IN_PROCESS_EDIT))&&workflowRights.equals("true")){%> 
								<td NOWRAP class="toolbarbutton" id="toolactions_5"		align="center" >
									<span style="cursor: pointer;" id="toolbarbutton_sp"		taskId="<%=request.getParameter("bpmTaskId") %>" >	<img				src="${ctx}/images/submit.png"	class="toolbarbuttonimage" title="<fmt:message key="biolims.common.handle"/>" ><fmt:message key="biolims.common.handle"/></span>
								</td>
								<%}%>
								<!-- 修改连接进入 -->
								<s:if test="#request.handlemethod=='modify'"> 
									<%
									//功能有审批权限，并且用户有审批权限
									if(rightsFunction.contains(SystemConstants.RIGHTS_SUBMIT)&&rightsAction.contains(SystemConstants.RIGHTS_SUBMIT)){%> 
									<%
									//假如状态为新建
										if(workflowState.equals("")||workflowState.equals(SystemConstants.DIC_STATE_NEW)){%> 
									<td NOWRAP class="toolbarbutton" id="toolactions_5"		align="center" >
										<span style="cursor: pointer;" id="toolbarbutton_tjsp"	><img	src="${ctx}/images/submit.png"	class="toolbarbuttonimage"  title="<fmt:message key="biolims.common.submit"/>" ><fmt:message key="biolims.common.submit"/></span>
									</td>
										<%}%>
									<%}%>
									<%
									//功能有改变状态权限，并且用户有改变状态权限
									if(rightsFunction.contains(SystemConstants.RIGHTS_CHANGE)&&rightsAction.contains(SystemConstants.RIGHTS_CHANGE)){%>
										<%
										//假如状态为空，状态为新建或者失效或者有效 
										if(workflowState.equals(SystemConstants.DIC_STATE_WORKFLOW_NO_PROCESS)||workflowState.equals("")||workflowState.equals(SystemConstants.DIC_STATE_NEW)||workflowState.startsWith(SystemConstants.DIC_STATE_WORKFLOW_IN_PROCESS_EDIT_PRE)&&!workflowRights.equals("true")){%>    
										<td NOWRAP class="toolbarbutton" id="toolactions_5"	align="center" >
											<span style="cursor: pointer;" id="toolbarbutton_status"	><img src="${ctx}/images/status.png"	class="toolbarbuttonimage" title="<fmt:message key="biolims.common.changeState"/>" ><fmt:message key="biolims.common.state"/></span>
										</td>
										<%} %>
									<%} %>
								</s:if>
								<s:if test="#request.handlemethod=='modify'">
								<%
									//功能有打印权限，并且用户打印有权限
									if(rightsFunction.contains(SystemConstants.RIGHTS_PRINT)){%>
									<td NOWRAP class="toolbarbutton" id="toolactions_5"		align="center" >
										<span style="cursor: pointer;" id="toolbarbutton_print"><img  src="${ctx}/images/print.png"	class="toolbarbuttonimage"  title="<fmt:message key="biolims.common.print"/>" ><fmt:message key="biolims.common.print"/></span>
									</td>
									<%} %>
								</s:if> 
								
								<s:if test="#request.handlemethod=='view'">
								
								<%
								if(rightsFunction.contains(SystemConstants.RIGHTS_CHANGE)&&rightsAction.contains(SystemConstants.RIGHTS_CHANGE)){
										//假如状态为空，状态为新建或者失效或者有效 
										
if(workflowState.equals(SystemConstants.DIC_STATE_WORKFLOW_NO_PROCESS)||workflowState.equals("")||workflowState.equals(SystemConstants.DIC_STATE_NEW)||workflowState.startsWith(SystemConstants.DIC_STATE_WORKFLOW_IN_PROCESS_EDIT)&&!workflowRights.equals("true")){%>    
										<td NOWRAP class="toolbarbutton" id="toolactions_5"	align="center" >
											<span style="cursor: pointer;" id="toolbarbutton_status"><img 	src="${ctx}/images/status.png"	class="toolbarbuttonimage"  title="<fmt:message key="biolims.common.changeState"/>" ><fmt:message key="biolims.common.state"/></span>
										</td>
										<%}} %>
								<%
									//功能有改变状态权限，并且用户有改变状态权限
									if(rightsFunction.contains(SystemConstants.RIGHTS_PRINT)){%>
									<td NOWRAP class="toolbarbutton" id="toolactions_5"		align="center" >
										<span style="cursor: pointer;" id="toolbarbutton_print"><img  src="${ctx}/images/print.png"	class="toolbarbuttonimage" title="<fmt:message key="biolims.common.print"/>" ><fmt:message key="biolims.common.print"/></span>
									</td>
									<%} %>
								</s:if> 
								
							<%
							// 已审批 
							if(workflowState.equals(SystemConstants.DIC_STATE_WORKFLOW_YES_PROCESS)&&rightsFunction.contains(SystemConstants.RIGHTS_SUBMIT)&&rightsAction.contains(SystemConstants.RIGHTS_SUBMIT)){%> 
									<!-- td NOWRAP class="toolbarbutton" id="toolactions_5"		align="center" >
										<span style="cursor: pointer;" id="toolbarbutton_scan"><img 	src="${ctx}/images/btn_showassignments.gif"		class="toolbarbuttonimage"  title="查看" >查看</span>
									</td-->
							<%}%>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
					<%if(request.getAttribute("handlemethod").equals("modify")||request.getAttribute("handlemethod").equals("view")||request.getAttribute("handlemethod").equals("list")){%> 
						<td NOWRAP align="right" valign="middle">
							<table cellspacing="0" cellpadding="0" class="toolbarsection">
								<tr>
									<td width="3px" height="23px"></td>
									<td>&nbsp;</td>
									<td nowrap valign="middle" class="toolbar_combobox_control"	id="actiondropdown">
										<input type="text"	class="input_parts text input False toolbar_combobox_field"	id="actiondropdown_textbox" name="actiondropdown_textbox"	value="<fmt:message key='biolims.common.selectFunction'/>" title="<fmt:message key='biolims.common.selectFunction'/>"		style="width: ; background-image: url(${ctx}/images/dropdown.gif); background-repeat: no-repeat; background-position: top right">
									</td>
								</tr>
							</table>
						</td>
					<%} %>							
			</tr>
			
		</table>
		</td>
		<td align="right" height="29px" class="text label"></td>
		<%} %>
	</tr>
</table>
<input type="hidden" id="workflowRights" value="<%=workflowRights%>"/>
<input type="hidden" id="copyBl" value="${copyBl}"/>
<input type="hidden" id="toolbarSaveButtonFlag" value=""></input>
</div>
	<%	if(request.getParameter("noButton")==null&&request.getAttribute("noButton")==null){%> 
		<%if(request.getAttribute("noTab")==null){%> 
			<div id="maintab" style="margin:0 0 0 0"></div>
		<%} %>
		
		<%
			if(rightsAction.contains(SystemConstants.RIGHTS_ORTHER)){%> 
					<script type="text/javascript">
					var menu;
					

						menu = new Ext.menu.Menu({
									margin: '0 0 0 0'
								}); 
						var button1 = Ext.get('actiondropdown_textbox');
						if(button1){
							button1.on('click', function(){
								menu.setPosition(button1.getX(), button1.getY()+17);
								menu.show(button1);
							});
							
		
							
						}
						
					
					</script>
			<%} %>	
		
	<%} %>
	</html>