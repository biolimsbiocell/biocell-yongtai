<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
    	<!--上传文件模态框-->
			<div class="modal fade" id="uploadFile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h3 class="modal-title" id="exampleModalLabel"><fmt:message key="biolims.common.uploadFiles"/>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button></h3>
						</div>
						<div class="modal-body">
							<div class="file-loading">
								<input id="uploadFileVal" name="excelFile" multiple type="file">
							</div>
							<div id="kartik-file-errors"></div>
						</div>
						<div class="modal-footer">
        					<button type="button" class="btn btn-default" data-dismiss="modal"><fmt:message key="biolims.common.close"/></button>
    					  </div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="uploadCsv" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h3 class="modal-title" id="exampleModalLabel"><fmt:message key="biolims.common.uploadFiles"/>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button></h3>
						</div>
						<div class="modal-body">
							<div class="file-loading">
								<input id="uploadCsvVal" name="excelFile" multiple type="file">
							</div>
							<div id="kartik-file-errors"></div>
						</div>
						<div class="modal-footer">
        					<button type="button" class="btn btn-default" data-dismiss="modal"><fmt:message key="biolims.common.close"/></button>
    					  </div>
					</div>
				</div>
			</div>
 	</body>
</html>