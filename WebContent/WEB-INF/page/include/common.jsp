<link rel="stylesheet" href="${ctx}/lims/dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="${ctx}/lims/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="${ctx}/lims/bootstrap/css/bootstrap-switch.min.css">
<link rel="stylesheet" href="${ctx}/lims/plugins/datepicker/datepicker3.css">
<link rel="stylesheet" href="${ctx}/lims/dist/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="${ctx}/lims/plugins/datatables/datatables.min.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/lims/plugins/bootstrap-fileinput/css/fileinput.min.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/lims/plugins/stepWizard/custom.min.css" />
<link rel="stylesheet" href="${ctx}/lims/plugins/icheck/skins/square/blue.css">
<link rel="stylesheet" type="text/css" href="${ctx}/lims/plugins/datetimepicker/bootstrap-datetimepicker.min.css">
<input type="hidden" id="workflowState" value="${requestScope.workflowState }">
<style type="text/css">
	.dataTables_scrollBody .adon {
		font-size: 10px;
		position: absolute;
		cursor: pointer;
		color: #91CEFF;
	}
	.dataTables_scrollBody .fillTop,
	.dataTables_scrollBody .fillBottom,
	.dataTables_scrollBody .fillRight,
	.dataTables_scrollBody .fillLeft{
		background-color: #41A4C3;
		height: 2px;
		width: 2px;
		position: absolute;
	}
	
			

.timePacker{
    /*width: 350px;*/
    /*height: 190px;*/
    border: 2px solid #C5C8CC;
    background-color: white;
    padding: 10px;
    top: 718px;
    z-index: 999;
    border-radius: 5px;
}
.timePacker-title{
    width: auto;
    padding: 4px;
    font-size: 14px;
    font-family: 微软雅黑;
    font-weight: 600;
}
.timePacker-content{
    width: 380px;
    height: 160px;
}
.timePacker-content ul {
    width: 380px;
    height: 160px;
    overflow: hidden;
}
.timePacker-content ul .hoursList {
    float: left;
    display: block;
    width: 30px;
    height: 30px;
    background-color: #F6F7F7;
    color: #000000;
    font-size: 14px;
    font-family: 微软雅黑;
    text-align: center;
    line-height: 30px;
    margin: 4px 4px;
    cursor: pointer;
}
.timePackerSelect{
    color: white!important;
    background-color: #007BDB!important;
}
/*.nowTime{*/
    /*color: #007BDB;*/
/*}*/
.timePacker-content ul .mList {
    float: left;
    display: block;
    width: 23px;
    height: 17px;
    background-color: #F6F7F7;
    /*color: #000000;*/
    font-size: 14px;
    font-family: 微软雅黑;
    text-align: center;
    line-height: 20px;
    margin: 4px 4px;
    cursor: pointer;
}
.timePacker-content ul .hoursList:hover,.timePacker-content ul .mList:hover{
    color: white!important;
    background-color: #007BDB!important;
}
.timePacker-close{
    display: block;
    width: 22px;
    height: 18px;
    float: right;
    cursor: pointer;
    margin-right: 16px;
}
.timePacker-back-hours img{
    width: 100%;
    height: 100%;
}
</style>
<script type="text/javascript" src="${ctx}/lims/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="${ctx}/lims/plugins/datatables/datatables.min.js"></script>
<script type="text/javascript" src="${ctx}/lims/plugins/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="${ctx}/lims/plugins/bootstrap-fileinput/js/fileinput.js"></script>
<script type="text/javascript" src="${ctx}/lims/plugins/bootstrap-fileinput/js/locales/zh.js"></script>
<script type="text/javascript" src="${ctx}/lims/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${ctx}/lims/bootstrap/js/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="${ctx}/lims/plugins/icheck/icheck.min.js"></script>
<script type="text/javascript" src="${ctx}/javascript/common/dataTablesExtend.js"></script>
<script type="text/JavaScript" src="${ctx}/js/language/ext-lang-<%=session.getAttribute("lan")%>.js"></script>
<script type="text/javascript" src="${ctx}/javascript/validate.js"></script>
<script type="text/javascript" src="${ctx}/lims/plugins/jQuery/jquery-form.js"></script>
<script type="text/javascript" src="${ctx}/lims/plugins/datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="${ctx}/lims/plugins/datetimepicker/locales/bootstrap-datetimepicker.zh-TW.js"></script>
<script type="text/javascript" src="${ctx}/lims/plugins/datetimepicker/locales/bootstrap-datetimepicker.zh-CN.js"></script>
<script type="text/javascript" src="${ctx}/lims/plugins/datepicker/locales/bootstrap-datepicker.zh-CN.js"></script>
<script type="text/javascript" src="${ctx}/lims/plugins/datepicker/locales/bootstrap-datepicker.zh-TW.js"></script>
<script type="text/javascript">
	$(function() {
		var sUserAgent = navigator.userAgent.toLowerCase(); 
		   var isIpad = sUserAgent.match(/ipad/i) == "ipad";
		if(isIpad) {
			$('.ipadmini').css("max-width", $(".ipadmini").width());
			$(window).resize(function() {
				var width = $('.ipadmini').parent(".box").width();
				$('.ipadmini').css("max-width", width);
			});
		}

	});
</script>