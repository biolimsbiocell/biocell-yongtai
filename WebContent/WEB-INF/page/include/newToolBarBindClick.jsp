<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@page import="com.biolims.util.DateUtil"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.biolims.common.constants.SystemConstants"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

		<%
String workflowRights = "";
//是否从待办事宜过来
if((request.getParameter("bpmTaskId")!=null&&!request.getParameter("bpmTaskId").equals("")&&!request.getParameter("bpmTaskId").equals("null"))||request.getAttribute("adminTaskId")!=null){
	workflowRights = "true";
}
String modelName="";
if(request.getAttribute("modelName")=="SampleReceive"){
	modelName="true";
}
String rightsAction = ""; 
//获得用户所包含的功能权限
if(request.getAttribute("rightsAction")!=null)
	rightsAction = (String)request.getAttribute("rightsAction");  
String workflowState = ""; 
//获得记录状态
if(request.getAttribute("workflowState")!=null)
	workflowState = (String)request.getAttribute("workflowState");  
//获得功能下的权限
String rightsFunction = ""; 
if(request.getAttribute("rightsFunction")!=null)
	rightsFunction = (String)request.getAttribute("rightsFunction");  
//获得功能模式
String handlemethod = "";
if(request.getAttribute("handlemethod")!=null)
	handlemethod = (String)request.getAttribute("handlemethod");  
String copyMode = "false";
//是否为复制模式
if(request.getAttribute("copyBl")!=null&&request.getAttribute("copyBl").equals("true")){
	String newhandlemethod = "add";
	copyMode = "true";
	request.setAttribute("handlemethod",newhandlemethod);
}
//在审批中,将功能模式设置为“查看”
if(workflowState.equals(SystemConstants.DIC_STATE_WORKFLOW_IN_PROCESS)){
	String newhandlemethod = SystemConstants.PAGE_HANDLE_METHOD_VIEW;
	request.setAttribute("handlemethod",newhandlemethod);
}

//审批状态为取消/无效
if(workflowState.equals(SystemConstants.DIC_STATE_WORKFLOW_NO_PROCESS)){
	String newhandlemethod = SystemConstants.PAGE_HANDLE_METHOD_VIEW;
	request.setAttribute("handlemethod",newhandlemethod);
}
//审批状态为有效，审批完
if(workflowState.equals(SystemConstants.DIC_STATE_WORKFLOW_YES_PROCESS)){
	String newhandlemethod = SystemConstants.PAGE_HANDLE_METHOD_VIEW;
	request.setAttribute("handlemethod",newhandlemethod);
}
if(workflowState.equals(SystemConstants.DIC_STATE_WORKFLOW_IN_PROCESS_EDIT)){
	String newhandlemethod = SystemConstants.PAGE_HANDLE_METHOD_MODIFY;
	request.setAttribute("handlemethod",newhandlemethod);
}
System.out.println(workflowState+"--------------------------------------------------"+rightsFunction+"========================"+rightsAction);


%>

		<title>JSP Page</title>
		<style type="text/css">
			.chosed {
				background-color: #5AC8D8 !important;
				color: #fff;
			}
			
			.row {
				margin-bottom: 5px;
			}
			
			.panel {
				background-color: transparent;
			}
			
			.input-group .input-group-addon {
				background-color: rgb(238, 238, 238);
			}
			
			.input-group-btn>.btn {
				border-radius: 0 3px 3px 0;
				height: 34px;
			}
			
			.layui-layer-content {
				padding: 0 20px;
			}
			
			.table-responsive th,
			.table-responsive td {
				white-space: nowrap;
			}
			
			.dt-button-collection {
				font-size: 12px;
				max-height: 260px;
				overflow: auto;
			}
			
			.box-title small span {
				margin-right: 20px;
			}
		</style>
	</head>

	<body>
		<div role="group" style="position: fixed; top: 0; background: #eee; z-index: 100; width: 100%; padding: 15px 15px 5px">
		
	<%if((workflowRights==null)||workflowRights.equals("")){%>
			<button type="button" class="btn btn-info" id="btn_list" onclick="list();">
			<i class="glyphicon glyphicon-list"></i> <fmt:message key="biolims.common.list" />
		</button>
			<%}%>
			<s:if test="#request.handlemethod=='modify'||#request.handlemethod=='add'">
				<%//功能有修改，新建权限，用户有修改，新建权限
		if((rightsFunction.contains(SystemConstants.RIGHTS_ADD)&&rightsAction.contains(SystemConstants.RIGHTS_ADD))||(rightsFunction.contains(SystemConstants.RIGHTS_MODIFY)&&rightsAction.contains(SystemConstants.RIGHTS_MODIFY))){%>
				<%//状态为新建或者空或者是复制
		if(workflowState==null||workflowState.equals("")||workflowState.equals(SystemConstants.DIC_STATE_NEW)||copyMode.equals("true")||"20".equals(workflowState)){%>
				<button type="button" class="btn btn-info" id="btn_save" onclick="save()">
			<i class="glyphicon glyphicon-save"></i> <fmt:message key="biolims.common.save"/>
		</button>
		
					

				<%if(((workflowRights==null)||workflowRights.equals(""))&&!workflowState.equals(SystemConstants.DIC_STATE_WORKFLOW_IN_PROCESS_EDIT)){%>
				<button type="button" class="btn btn-info" id="btn_submit" onclick="tjsp()">
			<i class="glyphicon glyphicon-open"></i> <fmt:message key="biolims.common.submit"/>
		</button>
				<%}%>
				<%}%>
				<%}%>
						<%//SampleReceive
		if(modelName.equals("true")){%>
		<button type="button" class="btn btn-info" id="btn_save" onclick="save()">
			<i class="glyphicon glyphicon-save"></i> <fmt:message key="biolims.common.save"/>
		</button>
		<%}%>
				<%//假如状态为空，状态为新建或者失效或者有效 
			if(workflowRights.equals("true")){%>

				<button type="button" class="btn btn-info" onclick="ck()">
			<i class="glyphicon glyphicon-retweet"></i> 查看流程图
		</button>
				<button type="button" class="btn btn-info" onclick="sp()">
			<i class="glyphicon glyphicon-check"></i> <fmt:message key="biolims.common.handle"/>
		</button>
				<%}else{%>
				<%//功能有改变状态权限，并且用户有改变状态权限
		if(rightsFunction.contains(SystemConstants.RIGHTS_CHANGE)&&rightsAction.contains(SystemConstants.RIGHTS_CHANGE)){%>
				<%//假如状态为空，状态为新建或者失效或者有效 
			if((workflowState.equals(SystemConstants.DIC_STATE_WORKFLOW_NO_PROCESS)||workflowState.equals("")||workflowState.equals(SystemConstants.DIC_STATE_NEW)||workflowState.startsWith(SystemConstants.DIC_STATE_WORKFLOW_IN_PROCESS_EDIT_PRE)&&!workflowRights.equals("true"))
					&&!workflowState.equals(SystemConstants.DIC_STATE_WORKFLOW_IN_PROCESS_EDIT)){%>
				<button id="btn_changeState" type="button" class="btn btn-info" onclick="changeState()">
			<i class="glyphicon glyphicon-adjust"></i> <fmt:message key="biolims.common.state"/>
		</button>
				<%}%>
				<%}%>
				<%}%>
			</s:if>
			<!--button type="button" class="btn btn-info" onclick="print()">
			<i class="glyphicon glyphicon-print"></i> <fmt:message key="biolims.common.print"/>
		</button-->
		</div>
	</body>

</html>