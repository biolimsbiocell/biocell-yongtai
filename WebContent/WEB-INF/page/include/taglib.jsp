<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setBundle basename="ResouseInternational/msg" />
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<script type="text/javascript">
	window.ctx = "${ctx}";
	window.userId="${userSession.id}";
	window.userName = "${userSession.name}";
	window.groupIds = "${groupIds}";
</script>
<% 
response.setHeader("Pragma","No-cache");    
response.setHeader("Cache-Control","no-cache");    
response.setDateHeader("Expires", -10);   
%>