<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="biolims.common.editTheStorageLocation"/></title>
</head>
<body>
	<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
	<script type="text/javascript"
		src="${ctx}/javascript/storage/position/editStoragePosition.js"></script>

	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showDicState = Ext.get('showDicState');
showDicState.on('click', 
 function showDicStateFun(){
var win = Ext.getCmp('showDicStateFun');
if (win) {win.close();}
var showDicStateFun= new Ext.Window({
id:'showDicStateFun',modal:true,title:'<fmt:message key="biolims.common.selectTheState"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/dic/state/stateSelect.action?type=storagePosition&flag=showDicStateFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 showDicStateFun.close(); }  }]  });     showDicStateFun.show(); }
);
});
 function setshowDicStateFun(id,name){
 document.getElementById("sp_state_id").value = id;
document.getElementById("sp_state_name").value = name;
var win = Ext.getCmp('showDicStateFun')
if(win){win.close();}
}
</script>




	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var upStoragePositionDetail = Ext.get('upStoragePositionDetail');
upStoragePositionDetail.on('click', 
 function upStoragePositionDetailFun(){
var win = Ext.getCmp('upStoragePositionDetailFun');
if (win) {win.close();}
var upStoragePositionDetailFun= new Ext.Window({
id:'upStoragePositionDetailFun',modal:true,title:'<fmt:message key="biolims.common.detailed"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.form.TextArea({id:'upStoragePositionDetail1',value:document.getElementById('sp.upStoragePosition.name').value}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 upStoragePositionDetailFun.close(); }  }]  });     upStoragePositionDetailFun.show(); }
);
});
</script>


	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var spDepartmentImg = Ext.get('spDepartmentImg');
spDepartmentImg.on('click', 
 function departmentselect(){
var win = Ext.getCmp('departmentselect');
if (win) {win.close();}
var departmentselect= new Ext.Window({
id:'departmentselect',modal:true,title:'<fmt:message key="biolims.common.chooseDepartment"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/core/department/departmentSelect.action?flag=departmentselect' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 departmentselect.close(); }  }]  });     departmentselect.show(); }
);
});
 function setdepartmentselect(id,name){
 document.getElementById("sp.department.id").value = id;
document.getElementById("sp.department.name").value = name;
var win = Ext.getCmp('departmentselect')
if(win){win.close();}
}
</script>


	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showStoragePosition = Ext.get('showStoragePosition');
showStoragePosition.on('click', 
 function showStoragePositionFun(){
var win = Ext.getCmp('showStoragePositionFun');
if (win) {win.close();}
var showStoragePositionFun= new Ext.Window({
id:'showStoragePositionFun',modal:true,title:'<fmt:message key="biolims.common.chooseTheLocation"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/storage/common/showStoragePositionTree.action?flag=showStoragePositionFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 showStoragePositionFun.close(); }  }]  });     showStoragePositionFun.show(); }
);
});
 function setshowStoragePositionFun(id,name){
 document.getElementById("sp.upStoragePosition.id").value = id;
document.getElementById("sp.upStoragePosition.name").value = name;
var win = Ext.getCmp('showStoragePositionFun')
if(win){win.close();}
}
</script>

		
		
		<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var storage_container_btn = Ext.get('storage_container_btn');
storage_container_btn.on('click', 
 function freezeSize(){
var win = Ext.getCmp('freezeSize');
if (win) {win.close();}
var freezeSize= new Ext.Window({
id:'freezeSize',modal:true,title:'<fmt:message key="biolims.common.selectTheContainer"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/storage/container/showContainerList.action?flag=freezeSize' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 freezeSize.close(); }  }]  });     freezeSize.show(); }
);
});
 function setfreezeSize(id,name){
 document.getElementById("storage_container_id").value = id;
document.getElementById("storage_container_name").value = name;
var win = Ext.getCmp('freezeSize')
if(win){win.close();}
}
</script>


		<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var storage_container_btn1 = Ext.get('storage_container_btn1');
storage_container_btn1.on('click', 
 function freezeSize1(){
var win = Ext.getCmp('freezeSize1');
if (win) {win.close();}
var freezeSize1= new Ext.Window({
id:'freezeSize1',modal:true,title:'',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/storage/container/showContainerList.action?flag=freezeSize1' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 freezeSize1.close(); }  }]  });     freezeSize1.show(); }
);
});
 function setfreezeSize1(id,name){
 document.getElementById("sub_storage_container_id").value = id;
document.getElementById("sub_storage_container_name").value = name;
var win = Ext.getCmp('freezeSize1')
if(win){win.close();}
}
</script>


		
	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
		<s:form action="/storage/position/editStoragePosition.action"
			method="post" theme="simple" id="storagePositionForm">
			<s:hidden name="info" id="container_info"></s:hidden>
			<s:hidden name="delItemsPosi" id="container_del"></s:hidden>
			<table width="100%" cellpadding="0" cellspacing="0">
				<tbody>
					<tr>
						<td>
							<table width="100%" class="section_table" cellpadding="0">
								<tbody>
									<tr class="control textboxcontrol">
										<td valign="top" class="controlTitle" nowrap><span
											class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key="biolims.common.locationCode"/>"
											class="text label"> <fmt:message key="biolims.common.locationCode"/> </label></td>
										<td class="requiredcolumn" nowrap width="10px"><img
											class='requiredimage' src='${ctx}/images/required.gif' /></td>
										<td nowrap><input type="text"
											class="input_parts text input default  readonlyfalse false"
											name="sp.id" id="sp_id" value='<s:property value="sp.id"/>'
											title="<fmt:message key="biolims.common.locationCode"/>  " size="20" maxlength="30"></td>
										<td nowrap>&nbsp;</td>
									</tr>
									<tr class="control multiparttextboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="<fmt:message key="biolims.common.upID"/>" class="text label"> <fmt:message key="biolims.common.upID"/></label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><input type="text"
											class="input_parts text input  readonlytrue false"
											name="sp.upStoragePosition.id" readonly="readOnly"
											id="sp.upStoragePosition.id"
											value='<s:property value="sp.upStoragePosition.id"/>'
											title="<fmt:message key="biolims.common.upID"/> " onblur="textbox_ondeactivate(event);"
											onfocus="shared_onfocus(event,this)" size="20" maxlength="30">
											<%-- <img alt='选择' id='showStoragePosition'
											src='${ctx}/images/img_lookup.gif' class='detail' />  --%>
											<input	type="text" class="input_parts text input  readonlytrue"
											name="sp.upStoragePosition.name" readonly="readOnly"
											id="sp.upStoragePosition.name"
											value="<s:property value="sp.upStoragePosition.name"/>"
											title="<fmt:message key="biolims.common.upID"/> " onblur="textbox_ondeactivate(event);"
											onfocus="shared_onfocus(event,this)" size="20" maxlength="62">
											<%-- <img id="upStoragePositionDetail"
											src="${ctx}/images/img_longdescription_off.gif"
											class='detail' alt="详细"> --%></td>
										<td nowrap></td>
									</tr>
									<tr class="control textboxcontrol">
											
												
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="<fmt:message key="biolims.common.locationState"/>" class="text label"><fmt:message key="biolims.common.locationState"/> </label>
												</td>
												<td class="requiredcolumn" nowrap width="10px"><img
													class='requiredimage' src='${ctx}/images/required.gif' /></td>
												<td nowrap><input type="text"
													onblur="textbox_ondeactivate(event);"
													onfocus="shared_onfocus(event,this)"
													class="input_parts text input default  readonlyfalse false"
													readonly="readOnly" name="" id="sp_state_name"
													value="<s:property value="sp.state.name"/>" size="20" /> <img
													alt='<fmt:message key="biolims.common.selectTheState"/>' id='showDicState'
													src='${ctx}/images/img_lookup.gif' class='detail' /> <input
													type="hidden" name="sp.state.id" id="sp_state_id"
													value="<s:property value="sp.state.id"/>" /></td>
												<td nowrap><input type="hidden" name="sp.isUse" id="sp_isUse"
													value="<s:property value="sp.isUse"/>" /></td>
											</tr>
								</tbody>
							</table>
						</td>
						<td class="sectioncol " width='33%' valign="top" align="right">
							<div class="section standard_section  ">
								<div class="section">
									<table width="100%" class="section_table" cellpadding="0">
										<tbody>
											<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="<fmt:message key="biolims.common.locationName"/>" class="text label"><fmt:message key="biolims.common.locationName"/></label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
												</td>
												<td nowrap><input type="text"
													onblur="textbox_ondeactivate(event);"
													onfocus="shared_onfocus(event,this)"
													class="input_parts text input default  readonlyfalse false"
													name="sp.name" id="sp.name"
													value='<s:property value="sp.name"/>' title="<fmt:message key="biolims.common.locationName"/>"
													onblur="textbox_ondeactivate(event);"
													onfocus="shared_onfocus(event,this)" size="50"
													maxlength="55"></td>
												<td nowrap>&nbsp;</td>
											</tr>
											<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="<fmt:message key="biolims.common.organisation"/>" class="text label"> <fmt:message key="biolims.common.organisation"/> </label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
												</td>
												<td nowrap><input type="text"
													onblur="textbox_ondeactivate(event);" maxlength="62"
													onfocus="shared_onfocus(event,this)" readonly="readOnly"
													class="input_parts text input default  false false"
													name="sp.department.name" id="sp.department.name"
													value='<s:property value="sp.department.name"/>'
													title="<fmt:message key="biolims.common.organisation"/>" onblur="textbox_ondeactivate(event);"
													onfocus="shared_onfocus(event,this)" size="50"> <input
													type="hidden" name="sp.department.id" id="sp.department.id"
													value='<s:property value="sp.department.id"/>'> <img
													alt='<fmt:message key="biolims.common.select"/>' id='spDepartmentImg'
													src='${ctx}/images/img_lookup.gif' class='detail' /> <img
													alt='' src='${ctx}/images/blank.gif' class='detail'
													style='display: none' /></td>
												<td nowrap>&nbsp;</td>
												
											</tr>
										
										</tbody>
									</table>
								</div>
							</div>
						</td>
						<td class="sectioncol " width='33%' valign="top" align="right">
							<div class="section standard_section ">
								<div class="section">
									<table width="100%" class="section_table" cellpadding="0">
										<tbody>
											<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="<fmt:message key="biolims.common.locationType"/>" class="text label"><fmt:message key="biolims.common.locationType"/> </label>
												</td>
												<td class="requiredcolumn" nowrap width="10px"><img
													class='requiredimage' src='${ctx}/images/required.gif' /></td>
												<td nowrap><input type="text"
													onblur="textbox_ondeactivate(event);"
													onfocus="shared_onfocus(event,this)"
													class="input_parts text input default  false false" name=""
													id="sp.type.name"
													value='<s:property value="sp.type.name"/>' title="<fmt:message key="biolims.common.location"/> "
													onblur="textbox_ondeactivate(event);"
													onfocus="shared_onfocus(event,this)" size="10"
													maxlength="20" readonly="readOnly"> <input
													type="hidden" name="sp.type.id"
												
													id="sp.type.id" value='<s:property value="sp.type.id"/>'>
													<img alt='<fmt:message key="biolims.common.select"/>' id='dicStorageType'
													src='${ctx}/images/img_lookup.gif' class='detail' /> <img
													alt='' src='${ctx}/images/blank.gif' class='detail'
													style='display: none' /></td>
												<td nowrap>&nbsp;</td>
											</tr>
											<s:if test="sp.upStoragePosition.id!=null && !sp.upStoragePosition.id eq ''">	
												<tr class="control textboxcontrol">
													<td class="label-title">
																<fmt:message key="biolims.common.containerType"/>
															</td>
															<td class="requiredcolumn" nowrap width="10px">
													</td>
															<td>
																<s:textfield maxlength="100" id="storage_container_name" name="sp.storageContainer.name" readonly="true" ></s:textfield >
																
																<s:hidden name="sp.storageContainer.id" id="storage_container_id"/>
																<span id="storage_container_btn" class="select-search-btn"></span>
															</td>	
															
																<td nowrap>&nbsp;</td>										
												</tr>
													<!--tr class="control textboxcontrol">
													<td class="label-title">
																<fmt:message key="biolims.common.theChildContainerType"/>
															</td>
															<td class="requiredcolumn" nowrap width="10px">
													</td>
															<td>
																<s:textfield maxlength="100" id="sub_storage_container_name" name="sp.subStorageContainer.name" readonly="true" ></s:textfield >
																
																<s:hidden name="sp.subStorageContainer.id" id="sub_storage_container_id"/>
																<span id="storage_container_btn1" class="select-search-btn"></span>
															</td>	
															
																<td nowrap>&nbsp;</td>										
												</tr-->
											</s:if>
											<s:else>	
												<tr class="control textboxcontrol">
												<td class="label-title">
																<fmt:message key="biolims.common.ziji1ruerceng"/>
															</td>
															<td class="requiredcolumn" nowrap width="10px"><img
														class='requiredimage' src='${ctx}/images/required.gif' />
													</td>
															<td>
																<input type="text" maxlength="100" id="storage_rowNum" 
																value='<s:property value="sp.rowNum"/>' title="请填入数字" size="10"
																name="sp.rowNum" onkeyup="value=value.replace(/[^\d]/g,'')" >
															<input type="text" maxlength="100" id="storage_rowName"  size="10"
																value='<s:property value="sp.rowName"/>' title="请填入名称"
																name="sp.rowName"  >	
															</td>	
															
																
															
																<td nowrap>&nbsp;</td>										
												</tr>
												<tr class="control textboxcontrol">
												
												<td class="label-title">
																<fmt:message key="biolims.common.ziji2ruerpai"/>
															</td>
															<td class="requiredcolumn" nowrap width="10px"><img
														class='requiredimage' src='${ctx}/images/required.gif' />
													</td>
															<td>
																<input type="text" maxlength="100" id="storage_colNum"  size="10"
																value='<s:property value="sp.colNum"/>' title="请填入数字" 
																name="sp.colNum"  onkeyup="value=value.replace(/[^\d]/g,'')">
																
																<input type="text" maxlength="100" id="storage_colName"  size="10"
																value='<s:property value="sp.colName"/>' title="请填入名称"
																name="sp.colName"  >
																
																
															</td>	
															
																<td nowrap>&nbsp;</td>										
												</tr>
											</s:else>
										</tbody>
									</table>
									</s:form>
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			<div id = "3d_image"></div>
			
			
			<input type="hidden" id="storage_container_id"
				value="<s:property value="#request.sw.id"/>" />
			<input type="hidden" id="handlemethod"
				value="${request.handlemethod}" />
			<s:if test="sp.id!=null">
				<div id="store_obj_div" style="padding-left: 10px;"></div>
				<input type="hidden" id="id" value="" />
			</s:if>
	</div>
</body>
</html>