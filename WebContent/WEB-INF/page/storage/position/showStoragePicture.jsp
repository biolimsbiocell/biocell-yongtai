<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- 	接收action内list集合 -->
	<%@ page language="java" import="java.util.*" %>  
	<%@page import="com.biolims.storage.position.model.*"%>
	
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<link rel="stylesheet" type="text/css"
	href="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/treegrid.css"
	rel="stylesheet" />
<script type="text/javascript"
	src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGridSorter.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGridColumnResizer.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGridNodeUI.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGridLoader.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGridColumns.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGrid.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/storage/position/showStoragePositionTree.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/handleSearchForm.js"></script>
<!-- 调整样式 -->
<style type="text/css">
thead {
font-size:30px;} 
</style>
<!-- 触发点击事件 -->
<script type="text/javascript">
	function cherk(a){
		window.location= window.ctx +'/storage/position/toEditStoragePosition.action?id='+a;
	}
</script>
</head>
<body>
	<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<!-- 	写入java代码 -->
	<% List<StoragePosition> list2=(List<StoragePosition>)request.getAttribute("list2");	
			%>		
	<%  
	List<StoragePosition> list=(List<StoragePosition>)request.getAttribute("list");
	String name ="";
	int colNum = 0;
	int rowNum = 0;
		for(int i=0;i<list.size();i++){
			colNum = Integer.parseInt(list.get(i).getColNum());
			rowNum = Integer.parseInt(list.get(i).getRowNum());
			name = list.get(i).getId();
			 %>
		<table  border="1" width="100%" align="center" rules="all" cellspacing="10px" >
			<br>
		<br>
			<thead>
  		 	 <tr bgcolor="lightblue">
      		<th colspan="<%=colNum%>" style="text-align:center;"><%=name %></th>
   			 </tr>
 			 </thead>
			<tbody align="center" >
			<%	
			for(int m=1;m<=rowNum;m++){ %> 
			<tr>
			<%for(int j=1;j<=colNum;j++){
				String id ="";
				if(m<10){
					id=name+"-0"+m;
				}else{
					id=name+"-"+m;
				}
				if(j<10){
					id=id+"-0"+j;
				}else{
					id=id+"-"+j;
				}
				String count = "";
				for(int n=0;n<list2.size();n++){
					if(list2.get(n).getId().equals(id)){
						count=list2.get(n).getIsUse();
					}
				}	
			%>
			<td> 
		    
				<button onclick="cherk('<%=id%>');"><%=count%>
<!-- 				<table  border="1" width="100%" align="center" rules="all" cellspacing="10px">  -->
<!-- 				<tbody align="center"> -->
<%-- 				<%for(int q=1;q<=5;q++){ %> --%>
<!-- 				 <tr>   -->
<%-- 				 <%for(int w=1;w<=5;w++){ %> --%>
<!-- 				  <td> -->
<%-- 				  <%=q%>-<%=w %> --%>
<!-- 				  </td>  -->
<%-- 				  <% }%><br>  --%>
<!-- 				  </tr>  -->
<%-- 				  <%} %> --%>
<!-- 				</tbody> -->
<!-- 				   </table> -->
				
<%-- 				<%=id %> --%>
				</button>
			</td>
			<% }%>
			</tr> 
			<%} %> 
			</tbody>
		</table>
	<%}%>
<div class="mainlistclass" id="gridContainerDiolagdiv" style="height:50%;width:100%"></div>
</body>
</html>