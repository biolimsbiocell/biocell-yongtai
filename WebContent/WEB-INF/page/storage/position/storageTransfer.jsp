﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/javascript/storage/position/storageTransfer.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="18" id="storageTransfer_id"
                   	 name="id" searchField="true" title="<fmt:message key="biolims.common.serialNumber"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="60" id="storageTransfer_name"
                   	 name="name" searchField="true" title="<fmt:message key="biolims.common.describe"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.type"/></td>
                   	<td align="left"  >
					<select id="storageTransfer_type" name="type" searchField="true" class="input-10-length" >
								<option value="1" <s:if test="storageTransfer.type==1">selected="selected"</s:if>><fmt:message key="biolims.common.yes"/></option>
								<option value="0" <s:if test="storageTransfer.type==0">selected="selected"</s:if>><fmt:message key="biolims.common.no"/></option>
					</select>
 					
                   	</td>
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showreason" title='<fmt:message key="biolims.common.selectTheReason"/>'
				hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="showreasonFun" hasSetFun="true"
				documentId="storageTransfer_reason"
				documentName="storageTransfer_reason_name"
			/>
               	 	<td class="label-title" ><fmt:message key="biolims.common.reason"/></td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="storageTransfer_reason_name" searchField="true"  name="reason.name"  value="" class="text input" />
 						<input type="hidden" id="storageTransfer_reason" name="storageTransfer.reason.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.selectTheReason"/>' id='showreason' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			<g:LayOutWinTag buttonId="showcreateUser" title='<fmt:message key="biolims.common.selectTheCreatePerson"/>'
				hasHtmlFrame="true"
				html="${ctx}/storage/position/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('storageTransfer_createUser').value=rec.get('id');
				document.getElementById('storageTransfer_createUser_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="storageTransfer_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="storageTransfer_createUser" name="storageTransfer.createUser.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.selectTheCreatePerson"/>' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showconfirmUser" title='<fmt:message key="biolims.common.selectTheAuditor"/>'
				hasHtmlFrame="true"
				html="${ctx}/storage/position/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('storageTransfer_confirmUser').value=rec.get('id');
				document.getElementById('storageTransfer_confirmUser_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.auditor"/></td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="storageTransfer_confirmUser_name" searchField="true"  name="confirmUser.name"  value="" class="text input" />
 						<input type="hidden" id="storageTransfer_confirmUser" name="storageTransfer.confirmUser.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.selectTheAuditor"/>' id='showconfirmUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.auditDate"/></td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startconfirmDate" name="startconfirmDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="confirmDate1" name="confirmDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endconfirmDate" name="endconfirmDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="confirmDate2" name="confirmDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.workflowStateID"/></td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="40" maxlength="30" id="storageTransfer_state"
                   	 name="state" searchField="true" title="<fmt:message key="biolims.common.workflowStateID"/>"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
 					<input type="hidden" id="storageTransfer_state" name="state" searchField="true" value=""/>"
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.workflowState"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="40" maxlength="30" id="storageTransfer_stateName"
                   	 name="stateName" searchField="true" title="<fmt:message key="biolims.common.workflowState"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.note"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="60" id="storageTransfer_note"
                   	 name="note" searchField="true" title="<fmt:message key="biolims.common.note"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_storageTransfer_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_storageTransfer_tree_page"></div>
</body>
</html>



