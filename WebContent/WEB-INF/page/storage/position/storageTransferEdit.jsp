<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title='<fmt:message key="biolims.common.attachment"/>'
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=storageTransfer&id=${storageTransfer.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/javascript/storage/position/storageTransferEdit.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="18" id="storageTransfer_id"
                   	 name="storageTransfer.id" title="<fmt:message key="biolims.common.serialNumber"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="storageTransfer.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="60" id="storageTransfer_name"
                   	 name="storageTransfer.name" title="<fmt:message key="biolims.common.describe"/>"
                   	   
	value="<s:property value="storageTransfer.name"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.type"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
					<select id="storageTransfer_type" name="storageTransfer.type" class="input-10-length" onChange="change()">
							<s:if test="storageTransfer.id!=null && storageTransfer.id != 'NEW'">
								<s:if test="storageTransfer.type==0">	<option value="0" selected="selected" ><fmt:message key="biolims.common.cells"/></option> </s:if>
								<s:if test="storageTransfer.type==1">	<option value="1" selected="selected" ><fmt:message key="biolims.common.plasmid"/></option> </s:if>
								<s:if test="storageTransfer.type==2">	<option value="2" selected="selected" >BAC</option> </s:if>
								<s:if test="storageTransfer.type==3">	<option value="3" selected="selected" ><fmt:message key="biolims.common.animal"/></option> </s:if>
							</s:if>		
							<s:if test="storageTransfer.id eq 'NEW'">
								<option value="0" selected="selected"><fmt:message key="biolims.common.cells"/></option>
								<option value="1"><fmt:message key="biolims.common.plasmid"/></option>
								<option value="2">BAC</option>
								<option value="3"><fmt:message key="biolims.common.animal"/></option>
							</s:if>		
					</select>
                   	</td>
			</tr>
			<tr>
			
			
			<g:LayOutWinTag buttonId="showreason" title='<fmt:message key="biolims.common.selectTheReason"/>'
				hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="storageTransferReasons" hasSetFun="true"
				documentId="storageTransfer_reason"
				documentName="storageTransfer_reason_name"
			/>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.reason"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="storageTransfer_reason_name"  value="<s:property value="storageTransfer.reason.name"/>" readonly="readOnly"  />
 						<input type="hidden" id="storageTransfer_reason" name="storageTransfer.reason.id"  value="<s:property value="storageTransfer.reason.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectTheReason"/>' id='showreason' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showcreateUser" title='<fmt:message key="biolims.common.selectTheCreatePerson"/>'
				hasHtmlFrame="true"
				html="${ctx}/storage/position/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('storageTransfer_createUser').value=rec.get('id');
				document.getElementById('storageTransfer_createUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="storageTransfer_createUser_name"  value="<s:property value="storageTransfer.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="storageTransfer_createUser" name="storageTransfer.createUser.id"  value="<s:property value="storageTransfer.createUser.id"/>" > 
 						<%-- <img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    /> --%>                   		
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="15" maxlength="" id="storageTransfer_createDate"
                   	 name="storageTransfer.createDate" title="<fmt:message key="biolims.common.commandTime"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	  value="<s:date name="storageTransfer.createDate" format="yyyy-MM-dd HH:mm:ss"/>"
                   	                      	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
			
			<g:LayOutWinTag buttonId="showconfirmUser" title='<fmt:message key="biolims.common.selectTheAuditor"/>'
				hasHtmlFrame="true"
				html="${ctx}/storage/position/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('storageTransfer_confirmUser').value=rec.get('id');
				document.getElementById('storageTransfer_confirmUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.auditor"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="storageTransfer_confirmUser_name"  value="<s:property value="storageTransfer.confirmUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="storageTransfer_confirmUser" name="storageTransfer.confirmUser.id"  value="<s:property value="storageTransfer.confirmUser.id"/>" > 
 						<%-- <img alt='选择审核人' id='showconfirmUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		 --%>
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.auditDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="15" maxlength="" id="storageTransfer_confirmDate"
                   	 name="storageTransfer.confirmDate" title="<fmt:message key="biolims.common.auditDate"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	  value="<s:date name="storageTransfer.confirmDate" format="yyyy-MM-dd"/>"
                   	                      	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.workflowStateID"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	  
 					<input type="hidden" id="storageTransfer_state" name="storageTransfer.state" value="<s:property value="storageTransfer.state"/>"/>
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.workflowState"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="30" id="storageTransfer_stateName"
                   	 name="storageTransfer.stateName" title="<fmt:message key="biolims.common.workflowState"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="storageTransfer.stateName"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.note"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="60" id="storageTransfer_note"
                   	 name="storageTransfer.note" title="<fmt:message key="biolims.common.note"/>"
                   	   
	value="<s:property value="storageTransfer.note"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			<td class="label-title"><fmt:message key="biolims.common.attachment"/></td><td></td>
						<td title="<fmt:message key="biolims.common.afterSaveTheBasicCanMaintainReviewImages"/>" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message key="biolims.common.attachment"/></span>
			</tr>
			
			
            </table>
            <input type="hidden" name="storageTransferItemJson" id="storageTransferItemJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="storageTransfer.id"/>" />
            </form>
            <div id="tabs">
            <ul>
			<li><a href="#storageTransferItempage"><fmt:message key="biolims.common.materialTransferInformation"/></a></li>
           	</ul> 
			<div id="storageTransferItempage" width="100%" height:10px></div>
			</div>
        	</div>
	</body>
	</html>
