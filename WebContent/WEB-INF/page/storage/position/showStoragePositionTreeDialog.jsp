<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<link rel="stylesheet" type="text/css"
	href="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/treegrid.css"
	rel="stylesheet" />
<script type="text/javascript"
	src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGridSorter.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGridColumnResizer.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGridNodeUI.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGridLoader.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGridColumns.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGrid.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/storage/position/showStoragePositionTreeDialog.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/handleSearchForm.js"></script>
<script type="text/javascript" src="${ctx}/javascript/common/superTables_compressed.js"></script>
<link rel=stylesheet type="text/css" href="${ctx}/css/superTables_compressed.css">
<g:LayOutWinTag buttonId="dicStorageType" title='<fmt:message key="biolims.common.chooseClassification"/>'
	hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
	isHasSubmit="false" functionName="location" hasSetFun="true"
	documentId="type_id" documentName="type_name" />
<g:LayOutWinTag buttonId="spDepartmentImg" title='<fmt:message key="biolims.common.chooseDepartment"/>'
	hasHtmlFrame="true"
	html="${ctx}/core/department/departmentSelect.action"
	isHasSubmit="false" functionName="departmentselect" hasSetFun="true"
	documentId="department_id" documentName="department_name" />
<g:LayOutWinTag buttonId="showDicState" title='<fmt:message key="biolims.common.selectTheState"/>' hasHtmlFrame="true"
	html="${ctx}/dic/state/stateSelect.action?type=storagePosition"
	isHasSubmit="false" functionName="showDicStateFun" hasSetFun="true"
	documentId="state_id" documentName="state_name" />
</head>

<body>
	<div id="jstj" style="display: none">
		<table cellspacing="0" cellpadding="0" class="toolbarsection">
			<tr>
				<td nowrap class="quicksearch_control" valign="middle"><label
					class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>"><fmt:message key="biolims.common.locationCode"/></label></td>
				<td nowrap class="quicksearch_control" valign="middle"><input
					type="text" searchField="true" maxlength="32" title="<fmt:message key="biolims.common.inputID"/>"
					class="input_parts text input default  readonlyfalse false"
					name="id" id="id" onblur="textbox_ondeactivate(event);" /></td>
				<td nowrap class="quicksearch_control" valign="middle"><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
					<label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>"><fmt:message key="biolims.common.locationName"/></label></td>
				<td nowrap class="quicksearch_control" valign="middle"><input
					type="text" searchField="true" maxlength="55" title="<fmt:message key="biolims.common.inputName"/>"
					class="input_parts text input default  readonlyfalse false"
					name="name" id="name" onblur="textbox_ondeactivate(event);" /></td>

			</tr>
			<tr>
				<td valign="top" align="right" class="controlTitle" nowrap><span
					class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key="biolims.common.locationType"/>"
					class="text label"><fmt:message key="biolims.common.locationType"/></label></td>
				<td nowrap><input type="text"
					onblur="textbox_ondeactivate(event);"
					onfocus="shared_onfocus(event,this)"
					class="input_parts text input default  false false"
					name="type.name" id="type_name" title="<fmt:message key="biolims.common.location"/> " size="10"
					maxlength="20" readonly="readOnly"> <input type="hidden"
					name="type.id" id="type_id" /> <img alt='<fmt:message key="biolims.common.select"/>' id='dicStorageType'
					src='${ctx}/images/img_lookup.gif' class='detail' /></td>
				<td valign="top" align="right" class="controlTitle" nowrap><span
					class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key="biolims.common.organisation"/>"
					class="text label"><fmt:message key="biolims.common.organisation"/></label></td>
				<td nowrap><input type="text"
					onblur="textbox_ondeactivate(event);"
					onfocus="shared_onfocus(event,this)" readonly="readOnly"
					class="input_parts text input default  false false"
					name="department.name" id="department_name" title="<fmt:message key="biolims.common.organisation"/>"
					onblur="textbox_ondeactivate(event);"
					onfocus="shared_onfocus(event,this)" size="10" maxlength="20" /> <input
					type="hidden" name="department.id" id="department_id" /> <img
					alt='<fmt:message key="biolims.common.select"/>' id='spDepartmentImg' src='${ctx}/images/img_lookup.gif'
					class='detail' /></td>
			</tr>
			<tr>
				<td valign="top" align="right" class="controlTitle" nowrap><span
					class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key="biolims.common.locationType"/>"
					class="text label"><fmt:message key="biolims.common.locationType"/></label></td>
				<td nowrap><input type="text"
					onblur="textbox_ondeactivate(event);"
					onfocus="shared_onfocus(event,this)"
					class="input_parts text input default  readonlyfalse false"
					readonly="readOnly" name="state.name" id="state_name" size="20" />
					<img alt='<fmt:message key="biolims.common.select"/>' id='showDicState'
					src='${ctx}/images/img_lookup.gif' class='detail' /> <input
					type="hidden" name="state.id" id="state_id" /></td>
			</tr>
		</table>
	</div>
	<input type="hidden" id="id" value="" />
	<input type="hidden" id="leaf" value="" />
	<input type="hidden" id="tf" value="" />
	<input type="hidden" id="positionPath" value="${path}" />
	<div class="mainlistclass" id="markup" style="height:100%"></div>
	<input type="hidden" id="id" value="" />
	
	<div class="mainlistclass" id="gridContainerDiolagdiv" style="height:50%;width:100%"></div>
	
</body>
</html>