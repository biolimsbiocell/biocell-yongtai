<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="biolims.common.informationOfStorageLocation"/></title>
</head>
<body>
	<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
	<script type="text/javascript"
		src="${ctx}/javascript/storage/position/editStoragePosition2w.js"></script>

	<g:LayOutWinTag buttonId="showDicState" title='<fmt:message key="biolims.common.selectTheState"/>'
		hasHtmlFrame="true"
		html="${ctx}/dic/state/stateSelect.action?type=storagePosition"
		isHasSubmit="false" functionName="showDicStateFun" hasSetFun="true"
		documentId="sp_state_id" documentName="sp_state_name" />



	<g:LayOutWinTag buttonId="dicStorageType" title='<fmt:message key="biolims.common.chooseClassification"/>'
		hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
		isHasSubmit="false" functionName="freezeType" hasSetFun="true"
		documentId="sp.type.id" documentName="sp.type.name" />



	<g:LayOutWinTag buttonId="upStoragePositionDetail" title='<fmt:message key="biolims.common.detailed"/>'
		item="new Ext.form.TextArea({id:'upStoragePositionDetail1',value:document.getElementById('sp.upStoragePosition.name').value})"
		isHasSubmit="false" functionName="upStoragePositionDetailFun" />

	<g:LayOutWinTag buttonId="spDepartmentImg" title='<fmt:message key="biolims.common.chooseDepartment"/>'
		hasHtmlFrame="true"
		html="${ctx}/core/department/departmentSelect.action"
		isHasSubmit="false" functionName="departmentselect" hasSetFun="true"
		documentId="sp.department.id" documentName="sp.department.name" />

	<g:LayOutWinTag buttonId="showStoragePosition" title='<fmt:message key="biolims.common.chooseTheLocation"/>'
		hasHtmlFrame="true"
		html="${ctx}/storage/common/showStoragePositionTree.action"
		isHasSubmit="false" functionName="showStoragePositionFun"
		hasSetFun="true" documentId="sp.upStoragePosition.id"
		documentName="sp.upStoragePosition.name" />
		
		
		<g:LayOutWinTag buttonId="storage_container_btn" title='<fmt:message key="biolims.common.selectTheContainer"/>'
		hasHtmlFrame="true" html="${ctx}/storage/container/showContainerList.action"
		isHasSubmit="false" functionName="freezeSize" hasSetFun="true"
		documentId="storage_container_id" documentName="storage_container_name" />
		
		
		<g:LayOutWinTag buttonId="storage_container_btn1" title='<fmt:message key="biolims.common.selectTheContainer"/>'
		hasHtmlFrame="true" html="${ctx}/storage/container/showContainerList.action"
		isHasSubmit="false" functionName="freezeSize1" hasSetFun="true"
		documentId="sub_storage_container_id" documentName="sub_storage_container_name" />
		
	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
		<s:form action="/storage/position/editStoragePosition.action"
			method="post" theme="simple" id="storagePositionForm">
			<s:hidden name="info" id="container_info"></s:hidden>
			<s:hidden name="delItemsPosi" id="container_del"></s:hidden>
			<table width="100%" cellpadding="0" cellspacing="0">
				<tbody>
					<tr>
						<td>
							<table width="100%" class="section_table" cellpadding="0">
								<tbody>
									<tr class="control textboxcontrol">
										<td valign="top" class="controlTitle" nowrap><span
											class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key="biolims.common.locationCode"/>"
											class="text label"><fmt:message key="biolims.common.locationCode"/> </label></td>
										<td class="requiredcolumn" nowrap width="10px"><img
											class='requiredimage' src='${ctx}/images/required.gif' /></td>
										<td nowrap><input type="text"
											class="input_parts text input default  readonlyfalse false"
											name="sp.id" id="sp_id" value='<s:property value="sp.id"/>'
											title="<fmt:message key="biolims.common.locationCode"/>  " size="20" maxlength="30"></td>
										<td nowrap>&nbsp;</td>
									</tr>
									<tr class="control multiparttextboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="<fmt:message key="biolims.common.upID"/>" class="text label"><fmt:message key="biolims.common.upID"/> </label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><input type="text"
											class="input_parts text input  readonlytrue false"
											name="sp.upStoragePosition.id" readonly="readOnly"
											id="sp.upStoragePosition.id"
											value='<s:property value="sp.upStoragePosition.id"/>'
											title="<fmt:message key="biolims.common.upID"/>" onblur="textbox_ondeactivate(event);"
											onfocus="shared_onfocus(event,this)" size="20" maxlength="30">
											<%-- <img alt='选择' id='showStoragePosition'
											src='${ctx}/images/img_lookup.gif' class='detail' />  --%>
											<input	type="text" class="input_parts text input  readonlytrue"
											name="sp.upStoragePosition.name" readonly="readOnly"
											id="sp.upStoragePosition.name"
											value="<s:property value="sp.upStoragePosition.name"/>"
											title="<fmt:message key="biolims.common.upID"/>" onblur="textbox_ondeactivate(event);"
											onfocus="shared_onfocus(event,this)" size="20" maxlength="62">
											<%-- <img id="upStoragePositionDetail"
											src="${ctx}/images/img_longdescription_off.gif"
											class='detail' alt="详细"> --%></td>
										<td nowrap></td>
									</tr>
									<tr class="control textboxcontrol">
											
												
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="<fmt:message key="biolims.common.locationState"/>" class="text label"><fmt:message key="biolims.common.locationState"/></label>
												</td>
												<td class="requiredcolumn" nowrap width="10px"><img
													class='requiredimage' src='${ctx}/images/required.gif' /></td>
												<td nowrap><input type="text"
													onblur="textbox_ondeactivate(event);"
													onfocus="shared_onfocus(event,this)"
													class="input_parts text input default  readonlyfalse false"
													readonly="readOnly" name="" id="sp_state_name"
													value="<s:property value="sp.state.name"/>" size="20" /> <img
													alt='<fmt:message key="biolims.common.selectTheState"/>' id='showDicState' 
													src='${ctx}/images/img_lookup.gif' class='detail' /> <input
													type="hidden" name="sp.state.id" id="sp_state_id"
													value="<s:property value="sp.state.id"/>" /></td>
												<td nowrap><input type="hidden" name="sp.isUse" id="sp_isUse"
													value="<s:property value="sp.isUse"/>" /></td>
											</tr>
								</tbody>
							</table>
						</td>
						<td class="sectioncol " width='33%' valign="top" align="right">
							<div class="section standard_section  ">
								<div class="section">
									<table width="100%" class="section_table" cellpadding="0">
										<tbody>
											<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="<fmt:message key="biolims.common.locationName"/>" class="text label"><fmt:message key="biolims.common.locationName"/></label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
												</td>
												<td nowrap><input type="text"
													onblur="textbox_ondeactivate(event);"
													onfocus="shared_onfocus(event,this)"
													class="input_parts text input default  readonlyfalse false"
													name="sp.name" id="sp.name"
													value='<s:property value="sp.name"/>' title="<fmt:message key="biolims.common.locationName"/>"
													onblur="textbox_ondeactivate(event);"
													onfocus="shared_onfocus(event,this)" size="50"
													maxlength="55"></td>
												<td nowrap>&nbsp;</td>
											</tr>
											<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="<fmt:message key="biolims.common.organisation"/>" class="text label"><fmt:message key="biolims.common.organisation"/> </label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
												</td>
												<td nowrap><input type="text"
													onblur="textbox_ondeactivate(event);" maxlength="62"
													onfocus="shared_onfocus(event,this)" readonly="readOnly"
													class="input_parts text input default  false false"
													name="sp.department.name" id="sp.department.name"
													value='<s:property value="sp.department.name"/>'
													title="<fmt:message key="biolims.common.organisation"/>" onblur="textbox_ondeactivate(event);"
													onfocus="shared_onfocus(event,this)" size="50"> <input
													type="hidden" name="sp.department.id" id="sp.department.id"
													value='<s:property value="sp.department.id"/>'> <img
													alt='<fmt:message key="biolims.common.select"/>' id='spDepartmentImg'
													src='${ctx}/images/img_lookup.gif' class='detail' /> <img
													alt='' src='${ctx}/images/blank.gif' class='detail'
													style='display: none' /></td>
												<td nowrap>&nbsp;</td>
												
											</tr>
											<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="<fmt:message key="biolims.common.storageType"/>" class="text label"><fmt:message key="biolims.common.storageType"/></label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
												</td>
												<td nowrap>
													<select name="sp.storageType" id="storage_storageType" class="input-10-length">
														<option value="0" <s:if test="sp.storageType==0">selected="selected"</s:if>><fmt:message key="biolims.common.wholeBlood"/></option>
														<option value="1" <s:if test="sp.storageType==1">selected="selected"</s:if>><fmt:message key="biolims.common.blood"/></option>
														<option value="2" <s:if test="sp.storageType==2">selected="selected"</s:if>><fmt:message key="biolims.common.nucleicAcid"/></option>
														<option value="3" <s:if test="sp.storageType==3">selected="selected"</s:if>><fmt:message key="biolims.common.library"/></option>
													</select>
												</td>
												<td nowrap>&nbsp;</td>
												</tr>
										</tbody>
									</table>
								</div>
							</div>
						</td>
						<td class="sectioncol " width='33%' valign="top" align="right">
							<div class="section standard_section ">
								<div class="section">
									<table width="100%" class="section_table" cellpadding="0">
										<tbody>
											<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="<fmt:message key="biolims.common.locationType"/>" class="text label"><fmt:message key="biolims.common.locationType"/> </label>
												</td>
												<td class="requiredcolumn" nowrap width="10px"><img
													class='requiredimage' src='${ctx}/images/required.gif' /></td>
												<td nowrap><input type="text"
													onblur="textbox_ondeactivate(event);"
													onfocus="shared_onfocus(event,this)"
													class="input_parts text input default  false false" name=""
													id="sp.type.name"
													value='<s:property value="sp.type.name"/>' title="<fmt:message key="biolims.common.location"/> "
													onblur="textbox_ondeactivate(event);"
													onfocus="shared_onfocus(event,this)" size="10"
													maxlength="20" readonly="readOnly"> <input
													type="hidden" name="sp.type.id"
												
													id="sp.type.id" value='<s:property value="sp.type.id"/>'>
													<img alt='<fmt:message key="biolims.common.select"/>' id='dicStorageType'
													src='${ctx}/images/img_lookup.gif' class='detail' /> <img
													alt='' src='${ctx}/images/blank.gif' class='detail'
													style='display: none' /></td>
												<td nowrap>&nbsp;</td>
											</tr>
											<s:if test="sp.upStoragePosition.id!=null && !sp.upStoragePosition.id eq ''">	
												<tr class="control textboxcontrol">
													<td class="label-title">
																<fmt:message key="biolims.common.containerType"/>
															</td>
															<td class="requiredcolumn" nowrap width="10px">
													</td>
															<td>
																<s:textfield maxlength="100" id="storage_container_name" name="sp.storageContainer.name" readonly="true" ></s:textfield >
																
																<s:hidden name="sp.storageContainer.id" id="storage_container_id"/>
																<span id="storage_container_btn" class="select-search-btn"></span>
															</td>	
															
																<td nowrap>&nbsp;</td>										
												</tr>
												
												<!-- tr class="control textboxcontrol">
													<td class="label-title">
																<fmt:message key="biolims.common.theChildContainerType"/>
															</td>
															<td class="requiredcolumn" nowrap width="10px">
													</td>
															<td>
																<s:textfield maxlength="100" id="sub_storage_container_name" name="sp.subStorageContainer.name" readonly="true" ></s:textfield >
																
																<s:hidden name="sp.subStorageContainer.id" id="sub_storage_container_id"/>
																<span id="sub_storage_container_btn" class="select-search-btn1"></span>
															</td>	
															
																<td nowrap>&nbsp;</td>										
												</tr-->
											</s:if>
											<s:else>	
												<tr class="control textboxcontrol">
												<td class="label-title">
																<fmt:message key="biolims.common.layerNumber"/>
															</td>
															<td class="requiredcolumn" nowrap width="10px"><img
														class='requiredimage' src='${ctx}/images/required.gif' />
													</td>
															<td>
																<input type="text" maxlength="100" id="storage_rowNum" 
																value='<s:property value="sp.rowNum"/>' title="层数"
																name="sp.rowNum" onkeyup="value=value.replace(/[^\d]/g,'')" >
																
															</td>	
															
																<td nowrap>&nbsp;</td>										
												</tr>
												<tr class="control textboxcontrol">
												
												<td class="label-title">
																<fmt:message key="biolims.common.rowNumber"/>
															</td>
															<td class="requiredcolumn" nowrap width="10px"><img
														class='requiredimage' src='${ctx}/images/required.gif' />
													</td>
															<td>
																<input type="text" maxlength="100" id="storage_colNum" 
																value='<s:property value="sp.colNum"/>' title="<fmt:message key="biolims.common.rowNumber"/>"
																name="sp.colNum"  onkeyup="value=value.replace(/[^\d]/g,'')">
																
															</td>	
															
																<td nowrap>&nbsp;</td>										
												</tr>
											</s:else>
										</tbody>
									</table>
									</s:form>
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			<div id = "3d_image"></div>
			
			
			<input type="hidden" id="storage_container_id"
				value="<s:property value="#request.sw.id"/>" />
			<input type="hidden" id="handlemethod"
				value="${request.handlemethod}" />
			<s:if test="sp.id!=null">
				<div id="store_obj_div" style="padding-left: 10px;"></div>
				<input type="hidden" id="id" value="" />
			</s:if>
	</div>
</body>
</html>