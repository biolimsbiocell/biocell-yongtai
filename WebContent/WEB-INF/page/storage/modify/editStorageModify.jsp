<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="biolims.common.adjustTheManagement" /></title>
<script language = "javascript" >
 function showMainStorageAllSelectList(){
var win = Ext.getCmp('showMainStorageAllSelectList');
if (win) {win.close();}
var showMainStorageAllSelectList= new Ext.Window({
id:'showMainStorageAllSelectList',modal:true,title:'<fmt:message key="biolims.common.choiceOfInventory"/>',layout:'fit',width:880,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/storage/common/showMainStorageAllSelectList.action?flag=showMainStorageAllSelectList' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 showMainStorageAllSelectList.close(); }  }]  });     showMainStorageAllSelectList.show(); }
</script>

<script type="text/javascript"
	src="${ctx}/javascript/storage/modify/editStorageModify.js"></script>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
</head>
<body>
	<s:if test='#request.handlemethod!="view"'>
		<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var searchStorageCheck = Ext.get('searchStorageCheck');
searchStorageCheck.on('click', 
 function searchStorageCheckFun(){
var win = Ext.getCmp('searchStorageCheckFun');
if (win) {win.close();}
var searchStorageCheckFun= new Ext.Window({
id:'searchStorageCheckFun',modal:true,title:'<fmt:message key="biolims.common.selectInventoryList"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/storage/modify/getStorageModifyList.action?flag=searchStorageCheckFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 searchStorageCheckFun.close(); }  }]  });     searchStorageCheckFun.show(); }
);
});
 function setsearchStorageCheckFun(rec){

			document.getElementById('storageModify.storageCheck.id').value=rec.get('id');
			document.getElementById('storageModify_type_id').value=rec.get('type-id');
			document.getElementById('storageModify_type_name').value=rec.get('type-name'); 
			document.getElementById('storageModify.storageCheck.endDate').value=rec.get('endDate');
			gridGrid.store.removeAll();
			gridGrid.store.commitChanges();
		 	store1 =  new Ext.data.JsonStore({
			root: 'results',
			totalProperty: 'total',
			remoteSort: true,
			fields: [{name:'id',type: 'string'},{name:'storage-id',type: 'string'},{name:'storage-name',type: 'string'},{name:'storage-searchCode',type: 'string'},{name:'storage-outPrice',type: 'float'},{name:'num',type: 'float'},{name:'unit',type: 'string'},{name:'modifyNum',type: 'float'},{name:'storage-unit-name',type: 'string'},{name:'storage-producer-name',type: 'string'},{name:'storage-position-id',type: 'string'},{name:'storage-type-id',type: 'string'}],
			proxy: new Ext.data.HttpProxy({url: window.ctx+'/storage/modify/getStorageModifyList.action?id='+document.getElementById('storageModify.storageCheck.id').value,method: 'POST'})
			});
		    var o1 = {start:0,limit:1000};
			store1.load({params:o1});
			setTimeout(insertStorageItem,1000); 
		
var win = Ext.getCmp('searchStorageCheckFun')
if(win){win.close();}
}
</script>

	</s:if>

	<s:if test='#request.storageModify.mainType.id=="materials"'>
		<g:LayOutWinTag
			title='<fmt:message key="biolims.common.choiceOfInventory"/>'
			width="880" height="500" hasHtmlFrame="true"
			html="${ctx}/storage/common/showMainStorageAllSelectList.action"
			isHasSubmit="false" isHasButton="false"
			functionName="showMainStorageAllSelectList" />
	</s:if>

	<g:HandleDataExtTag hasConfirm="false"
		paramsValue="modifyId:'${storageModify.id}'" funName="modifyData"
		url="${ctx}/storage/modify/saveStorageModifyItem.action" />
	<g:HandleDataExtTag funName="delData"
		url="${ctx}/storage/modify/delModifyStorageItem.action" />
	<s:if
		test='#request.handlemethod=="modify"||#request.handlemethod=="add"'>
		<script type="text/javascript"
			src="${ctx}/javascript/handleVaLeave.js"></script>
	</s:if>
	<%@ include file="/WEB-INF/page/include/toolbarNoTab.jsp"%>
	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass">
		<s:form id="storageModifyForm" method="post" theme="simple">
			<table width="100%" cellpadding="0" cellspacing="0">
				<tbody>
					<tr>
						<td>
							<table width="100%" class="section_table" cellpadding="0">
								<tbody>
									<tr class="control textboxcontrol">
										<td valign="top" class="controlTitle" nowrap><span
											class="labelspacer">&nbsp;&nbsp;</span> <label
											title='<fmt:message key="biolims.common.adjustTheNumber"/>'
											class="text label"><fmt:message
													key="biolims.common.adjustTheNumber" /></label></td>
										<td class="requiredcolumn" nowrap width="10px"><img
											class='requiredimage' src='${ctx}/images/required.gif' /></td>
										<td nowrap><input type="text"
											class="input_parts text input default  readonlytrue false"
											name="storageModify.id" id="storageModify_id"
											value='<s:property value="storageModify.id"/>' title="领用编号 "
											size="20" maxlength="30"></td>
										<td nowrap>&nbsp;</td>
									</tr>
									<tr class="control textboxcontrol">
										<td valign="top" class="controlTitle" nowrap><span
											class="labelspacer">&nbsp;&nbsp;</span> <label
											title="<fmt:message key="biolims.common.describe"/>"
											class="text label"><fmt:message
													key="biolims.common.describe" /></label></td>
										<td class="requiredcolumn" nowrap width="10px"><img
											class='requiredimage' src='${ctx}/images/required.gif' /></td>
										<td nowrap><input type="text"
											class="input_parts text input default  readonlyfalse"
											name="storageModify.note" id="storageModify_note"
											value='<s:property value="storageModify.note"/>'
											title="领用编号 " size="40" maxlength="100"></td>
										<td nowrap>&nbsp;</td>
									</tr>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="<fmt:message key="biolims.common.applicant"/>"
											class="text label"><fmt:message
													key="biolims.common.applicant" /></label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><input type="text"
											class="input_parts text input default  readonlytrue false"
											readonly="readOnly" name="storageModify.handleUser.name"
											id="storageModify.handleUser.name"
											value='<s:property value="storageModify.handleUser.name"/>'
											title="<fmt:message key="biolims.common.applicant"/>"
											size="10" maxlength="30"> <input type="hidden"
											name="storageModify.handleUser.id"
											id="storageModify.handleUser.id"
											value='<s:property value="storageModify.handleUser.id"/>'>
										</td>
										<td nowrap>&nbsp;</td>
									</tr>
								</tbody>
							</table>
						</td>
						<td class="sectioncol" width='33%' valign="top" align="right">
							<div class="section standard_section">
								<div class="section">
									<table width="100%" class="section_table" cellpadding="0">
										<tbody>
											<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="<fmt:message key="biolims.common.inventoryType"/>"
													class="text label"><fmt:message
															key="biolims.common.inventoryType" /></label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
												<td nowrap><input type="text"
													class="input_parts text input default  readonlytrue false"
													name="storageModify.type.name" id="storageModify_type_name"
													readonly="readOnly"
													value='<s:property value="storageModify.type.name"/>'
													title="<fmt:message key="biolims.common.inventoryType"/> "
													size="10" maxlength="20"> <input type="hidden"
													name="storageModify.type.id" id="storageModify_type_id"
													value='<s:property value="storageModify.type.id"/>'
													title="<fmt:message key="biolims.common.inventoryType"/> "
													size="10" maxlength="20"></td>
												<td nowrap>&nbsp;</td>
											</tr>
											<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="<fmt:message key="biolims.common.adjustTheDate"/>"
													class="text label"><fmt:message
															key="biolims.common.adjustTheDate" /></label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
												</td>
												<td nowrap><input type="text"
													class="input_parts text input default  readonlytrue false"
													readonly="readOnly" name="storageModify.handleDate"
													id="storageModify_handleDate"
													title="<fmt:message key="biolims.common.adjustTheDate"/>"
													value="<s:date name="storageModify.handleDate" format="yyyy-MM-dd" />"
													size="20" maxlength="30"></td>
												<td nowrap>&nbsp;</td>
											</tr>
											<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="<fmt:message key="biolims.common.state"/>"
													class="text label"><fmt:message
															key="biolims.common.state" /> </label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
												</td>
												<td nowrap><input type="text"
													class="input_parts text input default  readonlytrue false"
													readonly="readOnly" name="storageModify.stateName"
													value="<s:property value="storageModify.stateName"/>"
													id="storageModify_stateName"
													title="<fmt:message key="biolims.common.state"/>" size="20"
													maxlength="30" onblur="textbox_ondeactivate(event);"
													onfocus="shared_onfocus(event,this)"></input></td>
												<s:hidden name="storageModify.state"
													id="storageModify_state"></s:hidden>
												<td nowrap>&nbsp;</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</td>
						<td class="sectioncol " width='33%' valign="top" align="right">
							<div class="section standard_section  ">
								<div class="section">
									<table width="100%" class="section_table" cellpadding="0">
										<tbody>
											<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="<fmt:message key="biolims.common.endTime"/>"
													class="text label"> <fmt:message
															key="biolims.common.endTime" />
												</label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
												</td>
												<td nowrap><input type="text"
													class="input_parts text input default  readonlytrue false"
													readonly="readOnly" name=""
													id="storageModify.storageCheck.endDate"
													value='<s:date name="storageModify.storageCheck.endDate" format="yyyy-MM-dd"/>'
													title="<fmt:message key="biolims.common.endTime"/> ">
												</td>
												<td nowrap>&nbsp;</td>
											</tr>
											<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer"> &nbsp;&nbsp; </span> <label
													title='<fmt:message key="biolims.common.approver"/>'
													class="text label"> <fmt:message
															key="biolims.common.approver" />
												</label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
												</td>
												<td nowrap><s:hidden
														name="storageModify.confirmUser.id"
														id="storageModify_confirmUser_id"></s:hidden> <s:textfield
														cssClass="input_parts text input default  readonlytrue false"
														readonly="true" name="storageModify.confirmUser.name"
														id="storageModify_confirmUser_name"
														title='<fmt:message key="biolims.common.approver"/>  '
														size="10" maxlength="30"></s:textfield></td>
												<td nowrap>&nbsp;</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			<input type="hidden" name="jsonDataStr" id="jsonDataStr" value="" />
			<input type="hidden" id="mainType" name="storageModify.mainType.id"
				value="<s:property value="storageModify.mainType.id"/>">
			<input type="hidden" id="handlemethod"
				value="${request.handlemethod}" />
		</s:form>
		<%-- 		<s:if test='#request.storageModify.mainType.id=="materials"'>
				<g:GridEditTag isAutoWidth="true" height="parent.document.body.clientHeight-200" col="${col}" statement="${statement}" 
					type="${type}" title='<fmt:message key="biolims.common.adjustTheDetail"/>' addMethod="window.showMainStorageAllSelectList()"   delMethodAll="true" 
					url="${path}" statementLisener="${statementLisener}" handleMethod="${handlemethod}"/>
		</s:if>
		<s:if test='#request.storageModify.mainType.id=="product"'>
				<g:GridEditTag isAutoWidth="true" height="parent.document.body.clientHeight-200" col="${col}" statement="${statement}" 
					type="${type}" title='<fmt:message key="biolims.common.adjustTheDetail"/>' addMethod="window.showMainProductAllSelectList()"   delMethodAll="true" 
					url="${path}" statementLisener="${statementLisener}" handleMethod="${handlemethod}"/>
		</s:if> --%>
		<div id="grid" style='width: 100%;'></div>
	</div>
</body>
</html>