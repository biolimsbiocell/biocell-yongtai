<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="biolims.common.adjustTheManagement"/></title>
<script type="text/javascript" src="${ctx}/javascript/storage/modify/showStorageModifyList.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<g:LayOutWinTag buttonId="useTypeBtn" title='<fmt:message key="biolims.common.adjustType"/>' hasHtmlFrame="true"
	html="${ctx}/dic/type/dicTypeSelect.action"
	isHasSubmit="false" functionName="bflx" hasSetFun="true" documentId="storageModify_type_id" documentName="storageModify_type_name" />	
<g:LayOutWinTag buttonId="searchSqr" title='<fmt:message key="biolims.common.applicant"/>'
	hasHtmlFrame="true" html="${ctx}/core/user/userSelect.action"
	isHasSubmit="false" functionName="sqr" hasSetFun="true"
	documentId="modifyUser_id"
	documentName="modifyUser_name" />
</head>
<body>
	<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
	<div  class="mainlistclass" id="markup">
		<div  id="jstj" style="display:none" >
			<table cellspacing="0" cellpadding="0" class="toolbarsection">
				<tr>
					<td>&nbsp;</td>
					<td >
						<td valign="top" align="right"><span>&nbsp;</span>
						<label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>"><fmt:message key="biolims.common.serialNumber"/></label>
					</td>
					<td>&nbsp;</td>
					<td nowrap class="quicksearch_control" valign="middle">
						<input type="text"  searchField="true" class="input_parts text input default  readonlyfalse false"
							name="id" id="id" title="<fmt:message key="biolims.common.inputID"/>" style="width: 90" onblur="textbox_ondeactivate(event);" 
							onfocus="shared_onfocus(event,this)">
					</td>
					<td >
						<span>&nbsp;</span>
						<label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>"><fmt:message key="biolims.common.name"/></label>
					</td>
					<td>&nbsp;</td>
					<td nowrap class="quicksearch_control" valign="middle">
						<input type="text"  searchField="true"  class="input_parts text input default  readonlyfalse false" 
							name="name" id="name" title="<fmt:message key="biolims.common.inputName"/>" style="width: 90"
							onblur="textbox_ondeactivate(event);" 
							onfocus="shared_onfocus(event,this)">
					</td>
					<td nowrap>&nbsp;</td>
					<td valign="top" align="right" class="controlTitle" nowrap >
						<span class="labelspacer">&nbsp;&nbsp;</span>
						<label title="<fmt:message key="biolims.common.type"/>"   class="text label" ><fmt:message key="biolims.common.type"/></label>
					</td>
					<td nowrap>
						<input type="text"   class="input_parts text input default  readonlyfalse false" readonly="readOnly" name="" id="storageModify_type_name" title="类型  "   onblur="textbox_ondeactivate(event);"   onfocus="shared_onfocus(event,this)"  size="10"  maxlength="20"   >
						<input type="hidden"  searchField="true"  name="useType.id" id="storageModify_type_id" >
						<img  title='<fmt:message key="biolims.common.select"/>' id=useTypeBtn  src='${ctx}/images/img_lookup.gif'  class='detail' />
						<img  title=''    src='${ctx}/images/blank.gif' class='detail' style='display:none'/>
					</td>
					<td valign="top" align="right" class="controlTitle" nowrap >
						<label title="<fmt:message key="biolims.common.applicant"/>"    class="text label" ><fmt:message key="biolims.common.applicant"/></label>
					</td>
					<td nowrap>
						<input type="text" class="input_parts text input default  readonlyfalse false" readonly="readOnly" name="modifyUser-name" id="modifyUser_name" value=''  title="申请人 " size="10"  maxlength="30"	>
						<input type="hidden"  searchField="true"  name="modifyUser.id" id="modifyUser_id" value=''>
 						<img  title='<fmt:message key="biolims.common.select"/>'   id="searchSqr" src='${ctx}/images/img_lookup.gif'  class='detail'  />
					</td>
					<td valign="top" align="right" class="controlTitle" nowrap >
						<span class="labelspacer">&nbsp;&nbsp;</span>
						<label title="<fmt:message key="biolims.common.subordinateToTheProject"/>" class="text label" ><fmt:message key="biolims.common.subordinateToTheProject"/></label>
					</td>
					<td nowrap>
						<input type="text"   searchField="true"  class="input_parts text input default  readonlyfalse false" readonly="readOnly" name="project.id" id="project_id" value=''
							title="<fmt:message key="biolims.common.subordinateToTheProject"/> " size="10"  maxlength="30"  >
						<img  title='<fmt:message key="biolims.common.select"/>'  id="searchproject"  src='${ctx}/images/img_lookup.gif'  class='detail' />
					</td>
					<td valign="top" align="right" class="controlTitle" nowrap >
						<span class="labelspacer">&nbsp;&nbsp;</span>
						<label title="<fmt:message key="biolims.common.subordinateToTheExperiment"/>"   class="text label" ><fmt:message key="biolims.common.subordinateToTheExperiment"/></label>
					</td>
					<td>
						<input type="text"  searchField="true"   class="input_parts text input default  readonlyfalse false" readonly="readOnly" name="experiment.id" id="experiment_id" value=""
							title="<fmt:message key="biolims.common.subordinateToTheExperiment"/>"  size="10"  maxlength="30"  >
						<img  title='<fmt:message key="biolims.common.select"/>'   id="searchExperiment" src='${ctx}/images/img_lookup.gif'  class='detail'  />
					</td>
					<!-- td valign="top" align="right" class="controlTitle" nowrap >
						<span class="labelspacer">&nbsp;&nbsp;</span>
						<label title="状态"    class="text label" >状态</label>
					</td>
					<td nowrap>
						<select  searchField="true"  id="storage.state" name="state" style="width:60pt">
							<option value="" >全部</option>
    						<option value="1" >有效</option>
    						<option value="0">无效</option>
						</select>
					</td-->
					<td >
						<span>&nbsp;</span>
						<label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>">
						<fmt:message key="biolims.common.recordNumber"/></label>
					</td>
					<td>&nbsp;</td>
					<td nowrap class="quicksearch_control" valign="middle">
						<input type="text" class="input_parts text input default  readonlyfalse false" name="limitNum"
							onblur="textbox_ondeactivate(event);" 
							onfocus="shared_onfocus(event,this)"
							id="limitNum" title="<fmt:message key="biolims.common.inputTheRecordNumber"/>" style="width: 90">
						<img class="quicksearch_findimage" id="quicksearch_findimage" name="quicksearch_findimage" 
							onclick="commonSearchAction(gridGrid)" src="${ctx}/images/quicksearch.gif" title="<fmt:message key="biolims.common.search"/>">
						<img class="quicksearch_findimage" id="quicksearch_findimage" name="quicksearch_findimage"
							onclick="form_reset()" src="${ctx}/images/no_draw.gif" title="<fmt:message key="biolims.common.empty"/>">
					</td>	
				</tr>
			</table>
		</div>
		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
<input type='hidden' id='extJsonDataString' name='extJsonDataString' value=''/>
<div id='grid'  ></div>
<div id='gridcontainer'  ></div>

		<input type="hidden" id="mainType" value="<%=request.getParameter("mainType")%>" /> 
	</div>
</body>
</html>