<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<title></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style>
.dataTables_scrollBody {
	min-height: 100px;
}
#btn_submit{
	display:none;
}
</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div style="height: 14px"></div>
	<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 46px">
		<!--Form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<i class="glyphicon glyphicon-pencil"></i><fmt:message key="biolims.tStorageModify.warehouseAdjustment"/>
							<small style="color: #fff;"> <fmt:message key="biolims.tStorageModify.id"/>： <span id="storageModify_id"><s:property value="storageModify.id" /></span>
								<fmt:message key="biolims.common.applicant"/>: <span
								userId="<s:property value="storageModify.creatUser.id"/>"
								id="storageModify_createUser"><s:property
										value="storageModify.handleUser.name" /></span>
								<fmt:message key="biolims.common.adjustTheDate"/>: <span
								id="storageModify_handleDate"><s:date name=" storageModify.handleDate " format="yyyy-MM-dd "/></span>		
								<fmt:message key="biolims.tInstrument.state"/>: <span
								state="<s:property value="storageModify.state"/>"
								id="headStateName"><s:property
										value="storageModify.stateName" /></span>
							</small>
					</h3>

					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
											<li>
												<a href="####" onclick="$('.buttons-print').click();"><fmt:message key="biolims.common.print"/></a>
											</li>
											<li>
												<a href="#####" onclick="$('.buttons-copy').click();"><fmt:message key="biolims.common.copyData"/></a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-excel').click();">Excel</a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-csv').click();">CSV</a>
											</li>
											<li role="separator" class="divider"></li>
											<li>
												<a href="####" onclick="itemFixedCol(2)"><fmt:message key="biolims.common.lock2Col"/></a>
											</li>
											<li>
												<a href="####" id="unfixde" onclick="unfixde()"><fmt:message key="biolims.common.cancellock2Col"/></a>
											</li>
										</ul>
						</div>
					</div>
				</div>
				<div class="box-body">
				
					<!--form表单-->
					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
							<input type="hidden" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId")%>" /> <br>
							<div class="row">
									<input type="hidden"
											size="20" maxlength="25" id="storageModifyId"
											name="id"  readonly = "readOnly" value="<s:property value="storageModify.id"/>"
						 			/> 
						 			<input type="hidden" id="storageModify_handleUserId" name="handleUser-id"
										value="<s:property value="storageModify.handleUser.id"/>"
									/>
									<input type="hidden"
											size="20" maxlength="25" id="storageModifySandleDate"
											name="handleDate" value="<s:date name="storageModify.handleDate" format="yyyy-MM-dd"/>"
                   	   				/> 
                   	   				<input type="hidden"
											size="20" maxlength="25" id="storageModifyStateName"
											name="stateName" value="<s:property value="storageModify.stateName"/>"
						 			/>
						 			<input type="hidden"
											size="20" maxlength="25" id="storageModifyState"
											name="state" value="<s:property value="storageModify.state"/>"
						 			/>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon"><fmt:message key="biolims.tStorageModify.confirmUser"/> </span> 
										<input type="text" size="20" readonly="readOnly"
												id="storageModify_confirmUser_name"  changelog="<s:property value="storageModify.confirmUser.name"/>"
												value="<s:property value="storageModify.confirmUser.name"/>"
												class="form-control" /> 
										<input type="hidden"
												id="storageModify_confirmUser_id" name="confirmUser-id"
												value="<s:property value="storageModify.confirmUser.id"/>">
										<span class="input-group-btn">
												<button class="btn btn-info" type="button" 
													onClick="showconfirmUser()">
													<i class="glyphicon glyphicon-search"></i>
												</span>		
									</div>
								</div>	
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon">调整时间 </span> 
                   						<input type="text"
											size="20" maxlength="25" id="storageModify_endDate"
											name="endDate"  changelog="<s:property value="storageModify.endDate"/>"
											class="form-control"  value="<s:date name="storageModify.endDate" format="yyyy-MM-dd"/>"
                   	   					/>  
					
									</div>
								</div>	
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.tStorageModify.note"/> </span> 
                   						<input type="text"
											size="20" maxlength="25" id="storageModify_note"
											name="note"  changelog="<s:property value="storageModify.note"/>"
											  class="form-control" value="<s:property value="storageModify.note"/>"
						 				/>  
					
									</div>
								</div>			
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon"><fmt:message key="biolims.common.storageModifyType"/> </span> 
                   					<select lay-ignore id="storageModify_storageModifyType" name="storageModifyType" class="form-control" 
										changelog="<s:property value=" storageModify.storageModifyType "/>">
											<option value="0" <s:if test="storageModify.storageModifyType==0">selected="selected"</s:if>>
												<fmt:message key='biolims.common.lose' />
											</option>
											<!-- 有效 -->
											<option value="1" <s:if test="storageModify.storageModifyType==1">selected="selected"</s:if>>
												<fmt:message key='biolims.common.fill' />
											</option>
											<!-- 无效 -->
										</select>
									</div>
								</div>
							</div>
							<div id="fieldItemDiv"></div>
								
							<br> 
							<input type="hidden" name="storageModifyItemJson"
								id="storageModifyItemJson" value="" /> 
								
							<input type="hidden"
								id="id_parent_hidden"
								value="<s:property value="storageModify.id"/>" />
							<input type="hidden" id="fieldContent" name="fieldContent" value="<s:property value="storageModify.fieldContent"/>" />	
								
						</form>
					</div>
					<!--table表格-->
					
					</div>
					<div class="box-footer ipadmini">
						<!--库存主数据-->
						<div id="leftDiv" class="col-xs-4">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title"><fmt:message key="biolims.sampleOutApply.storage"/></h3>
								</div>
								<div class="box-body ipadmini">
									<table class="table table-hover table-striped table-bordered table-condensed" id="storageModifyTempdiv" style="font-size: 14px;"></table>
								</div>
							</div>
						</div>
						<!--排板后样本-->
						<div id="rightDiv" class="col-xs-8">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title"><fmt:message key="biolims.common.detailed"/></h3>
								</div>
								<div class="box-body ipadmini">
									<table class="table table-hover table-striped table-bordered table-condensed" id="storageModifyItemdiv" style="font-size: 14px;"></table>
								</div>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript" src="${ctx}/javascript/storage/modify/storageModifyEdit.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/storage/modify/storageModifyItem.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/storage/modify/storageModifyTemp.js"></script>
	<%-- <script type="text/javascript"
		src="${ctx}/javascript/common/dataTablesExtend.js"></script> --%>
</body>

</html>	
