<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style type="text/css">
.dataTables_scrollBody {
	min-height: 150px;
}

.layui-layer-content .input-group {
	margin-top: 12px;
}

#fieldItemDiv .icheckbox_square-blue {
	margin-left: 10px;
}
#btn_submit{
	display:none;
}
</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 50px">
		<!--订单录入的form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<strong id="sampleReceiveTitle"
							type="<s:property value="sampleReceive.type"/>">询价管理</strong>
					</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefresh()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="####" onclick="$('.buttons-print').click();"><fmt:message
											key="biolims.common.print" /></a></li>
								<li><a href="#####" onclick="$('.buttons-copy').click();"><fmt:message
											key="biolims.common.copyData" /></a></li>
								<li><a href="####" onclick="$('.buttons-excel').click();"><fmt:message
											key="biolims.common.export" /></a></li>
								<li><a href="####" onclick="$('.buttons-csv').click();"><fmt:message
											key="biolims.common.exportCSV" /></a></li>
								<li role="separator" class="divider"></li>
								<li><a href="####" id="fixdeLeft2" onclick="fixedCol(2)"><fmt:message
											key="biolims.common.lock2Col" /></a></li>
								<li><a href="####" id="unfixde" onclick="unfixde()"><fmt:message
											key="biolims.common.cancellock2Col" /></a></li>
							</ul>
						</div>
					</div>

				</div>
				<div class="box-body">
					<input type="hidden" name="bpmTaskId" id="bpmTaskId"
						value="<%=request.getParameter(" bpmTaskId ")%>" />
					<form name="form1" id="form1" method="post">
						<div class="row">
							<!-- 编码 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">询价编号<img
										class='requiredimage' src='${ctx}/images/required.gif' /></span><input
										 type="text" size="20" maxlength="25" readonly="readonly"
										id="enquiry_id" name="id" class="form-control"
										title="" value="<s:property value=" enquiry.id "/>" />
								</div>
							</div>
							<!-- 描述 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.name" /></span><input
										 type="text" size="20" maxlength="25"
										id="enquiry_name" name="name" class="form-control"
										title="" value="<s:property value=" enquiry.name "/>" />
								</div>
							</div>
							<!-- 创建人 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">创建人
									</span>
									<input type="hidden" changelog="<s:property value=" enquiry.createUser.id "/>" type="text" size="20" maxlength="25"
										id="enquiry_createUser" name="createUser-id"
										class="form-control" title=""
										value="<s:property value=" enquiry.createUser.id "/>" /> 
									<input  changelog="<s:property value=" enquiry.createUser.name "/>" type="text" size="20" maxlength="25"
										id="enquiry_createUser_name" name="createUser-name"
										class="form-control" title="" readonly="readonly"
										value="<s:property value=" enquiry.createUser.name "/>" />
								</div>
							</div>
						</div>
						<div class="row">
							<!-- 创建日期 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group input-append date ">
									<span class="input-group-addon">创建日期
									</span> <input  changelog="<s:property value=" purchaseApply.createDate "/>" type="text" size="20" maxlength="25"
										id="enquiry_createDate" name="createDate"
										class="form-control" title="" readonly="readonly"
										value="<s:date name=" enquiry.createDate " format="yyyy-MM-dd"/>" />
								</div>
							</div>
							<!-- 审核人 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">审核人
									</span>
									<input type="hidden" changelog="<s:property value=" enquiry.confirmUser.id "/>" type="text" size="20" maxlength="25"
										id="enquiry_confirmUser" name="confirmUser-id"
										class="form-control" title=""
										value="<s:property value=" purchaseApply.confirmUser.id "/>" /> 
									<input  changelog="<s:property value=" enquiry.confirmUser.name "/>" type="text" size="20" maxlength="25"
										id="enquiry_confirmUser_name" name="confirmUser-name"
										class="form-control" title="" readonly="readonly"
										value="<s:property value=" enquiry.confirmUser.name "/>" />
									<span class="input-group-btn">
										<button class="btn btn-info" type="button"
										onClick="selectConfirmUser()">
										<i class="glyphicon glyphicon-search"></i>
									</span>
								</div>
							</div>
							<!-- 审核时间 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group input-append date ">
									<span class="input-group-addon">审核时间
									</span> <input  changelog="<s:property value=" enquiry.confirmDate "/>" type="text" size="20" maxlength="25"
										id="enquiry_confirmDate" name="confirmDate"
										class="form-control" title="" readonly="readonly"
										value="<s:date name=" enquiry.confirmDate " format="yyyy-MM-dd"/>" />
								</div>
							</div>
						</div>
						<div class="row">
							<!-- 状态 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.tStorage.state" /> </span>
										<input type="hidden" size="50" maxlength="127"
											id="enquiry_state" readonly="readOnly"
											title="" name="state"
											class="form-control"
											value="<s:property value="enquiry.state"/>" />
										<input type="text" size="50" maxlength="127"
											id="enquiry_stateName" readonly="readOnly"
											changelog="<s:property value="enquiry.stateName"/>"
											title="" name="stateName"
											class="form-control"
											value="<s:property value="enquiry.stateName"/>" />
									</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<button type="button" class="btn btn-info btn-sm" onclick="fileUp()">
										<fmt:message key="biolims.common.uploadAttachment" />
									</button>&nbsp;&nbsp;
										<button type="button" class="btn btn-info btn-sm"
											onclick="fileView()">
											<fmt:message key="biolims.report.checkFile" />
										</button>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div id="enquiryItem" class="box-footer ipadmini" style="padding-top: 5px; min-height: 320px;">
					<h3>询价管理明细</h3>
					<table class="table table-hover table-striped table-bordered table-condensed"
						id="enquiryItemTable" style="font-size: 14px;"></table>
				</div>
				<div id="supplierEnquiryItem" class="box-footer ipadmini" style="padding-top: 5px; min-height: 320px;">
					<h3>供应商报价</h3>
					<table class="table table-hover table-striped table-bordered table-condensed"
						id="supplierEnquiryItemTable" style="font-size: 14px;"></table>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript"
		src="${ctx}/javascript/storage/enquiry/enquiryEdit.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/storage/enquiry/enquiryItem.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/storage/enquiry/supplierEnquiryItem.js"></script>
</body>
</html>