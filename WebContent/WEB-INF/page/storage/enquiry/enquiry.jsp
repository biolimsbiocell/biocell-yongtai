﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<!-- Font Awesome -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
	</head>

	<body style="height:94%">
		<div>
			<%@ include file="/WEB-INF/page/include/newToolbar.jsp"%>
		</div>
		<div class="content-wrapper" id="content" style="margin-left: 0px;margin-top: 46px">
			<section class="content">
				<div class="row">
					<!--表格-->
					<div class="col-xs-12 col-md-12">
						<div class="box box-info box-solid" id="box">
							<div class="box-header with-border">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title">询价管理<small>询价管理</small></h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool" id="tableRefresh" onclick="tableRefresh()"><i class="glyphicon glyphicon-refresh"></i>
                </button>
									<div class="btn-group">
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Action <span class="caret"></span>
  </button>
										<ul class="dropdown-menu">
											<li>
												<a href="####" onclick="$('.buttons-print').click();"><fmt:message key="biolims.common.print" /></a>
											</li>
											<li>
												<a href="#####" onclick="$('.buttons-copy').click();"><fmt:message key="biolims.common.copyData" /></a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-excel').click();"><fmt:message key="biolims.common.export" /></a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-csv').click();"><fmt:message key="biolims.common.exportCSV" /></a>
											</li>
											<li role="separator" class="divider"></li>
											<li>
												<a href="####" id="fixdeLeft2" onclick="fixedCol(2)"><fmt:message key="biolims.common.lock2Col" /></a>
											</li>
											<li>
												<a href="####" id="unfixde" onclick="unfixde()"><fmt:message key="biolims.common.cancellock2Col" /></a>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="box-body ipadmini">
								<table class="table table-hover table-responsive table-striped table-bordered table-condensed" id="main" style="font-size:14px;">
								</table>
							</div>
						</div>
					</div>

				</div>

			</section>
		</div>
		<div id="enquiry_div"></div>
		<script type="text/javascript" src="${ctx}/javascript/storage/enquiry/enquiry.js"></script>
	</body>

</html>