<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<link rel="stylesheet" href="${ctx}/css/experimentLab.css" />
<style type="text/css">
.dt-buttons {
	float: none;
}

#btn_submit{
  display:none;
}

.tablebtns {
	position: initial;
}
</style>
</head>

<body style="height: 94%">
<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>

	<div class="content-wrapper" id="content" style="margin-left: 0px;margin-top: 46px">
		<input type="hidden" id="storageOut_name"
			value="<s:property value="storageOut.name"/>"> <input
			type="hidden" id="bpmTaskId" value="${requestScope.bpmTaskId}" />
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
		<section class="content" >
		<div class="row" style="">
			<!--表格-->
			<div class="col-xs-12">
				<div class="box box-info box-solid">
					<div class="box-header with-border">
						<i class="fa fa-bell-o"></i>
						<h3 class="box-title">
							<fmt:message key="biolims.storageOut"/> <small style="color: #fff;"> <fmt:message key="biolims.storageOut.id"/>: <span
								id="storageOut_id"><s:property value="storageOut.id" /></span>
							<fmt:message key="biolims.storageOut.createUser"/>: <span
								userId="<s:property value="storageOut.outUser.id"/>"
								id="storageOut_outUser"><s:property
										value="storageOut.outUser.name" /></span><fmt:message key="biolims.storageOut.createDate"/>: <span
								id="storageOut_outDate"><s:date name=" storageOut.outDate " format="yyyy-MM-dd "/></span> <fmt:message key="biolims.storageOut.state"/>: <span
								state="<s:property value="storageOut.state"/>"
								id="headStateName"><s:property
										value="storageOut.stateName" /></span>
							</small>
						</h3>
					</div>
					<div class="box-body ipadmini">

						<div class="HideShowPanel">
							<form name="form1" class="layui-form" id="form1" method="post">
								<div class="row">
									<div class="col-xs-4">
										<div class="input-group">

											<span class="input-group-addon"><fmt:message key="biolims.storageOut.note"/><img
												class='requiredimage' src='${ctx}/images/required.gif' />
											</span> <input type="text" size="20" maxlength="25"
												id="storageOut_note" name="note"
												changelog="<s:property value="storageOut.note"/>"
												class="form-control"
												value="<s:property value="storageOut.note"/>" />

										</div>
									</div>
									<div class="col-xs-4">
										<div class="input-group">

											<span class="input-group-addon"><fmt:message key="biolims.storageOut.applyUser"/></span>

											<input type="text" size="20" readonly="readOnly" id="storageOut_useUser_name" changelog="<s:property value=" storageOut.useUser.name "/>" value="<s:property value=" storageOut.useUser.name "/>" class="form-control" />

											<input type="hidden" id="storageOut_useUser_id" name="useUser-id" value="<s:property value=" storageOut.useUser.id "/>">

											<span class="input-group-btn">
												<button class="btn btn-info" type="button" 
													onClick="showUseUser()">
													<i class="glyphicon glyphicon-search"></i>
												</span>
										</div>
									</div>
									<div class="col-xs-4">
							     	<div class="input-group">
									<span class="input-group-addon">出库类型</span> <select
										class="form-control" lay-ignore=""
										id="ctype" name="storageOut.ctype">
										<option value="0"
												<s:if test='storageOut.ctype=="0"'>selected="selected"</s:if>>出库到生产</option>
										<option value="1"
												<s:if test='storageOut.ctype=="1"'>selected="selected"</s:if>>出库到作废</option>
									</select>
								</div>
							</div>
								</div>
							</form>
						</div>

	</br>






						<!--库存主数据-->
						<div id="leftDiv" class="col-md-4 col-xs-12">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title"><fmt:message key="biolims.storageOut.storage"/></h3>
								</div>
								<div class="box-body ipadmini">
									<table
										class="table table-hover table-striped table-bordered table-condensed"
										id="storageOutAlldiv" style="font-size: 14px;"></table>
								</div>
							</div>
						</div>
						<!--排板后样本-->
						<div id="rightDiv" class="col-md-8 col-xs-12">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title"><fmt:message key="biolims.storageOut.outItem"/></h3>
								</div>
								<div class="box-body ipadmini">
									<table
										class="table table-hover table-striped table-bordered table-condensed"
										id="storageOutItemdiv" style="font-size: 14px;"></table>
								</div>
							</div>
						</div>
					</div>


				</div>
			</div>

		</div>
		</section>
	</div>
	<div style="display: none" id="batch_data" class="input-group">
		<span class="input-group-addon bg-aqua">产物数量</span> <input
			type="number" id="productNum" class="form-control"
			placeholder="" value="">
	</div>
	<script type="text/javascript"
		src="${ctx}/javascript/storage/out/storageOutAllLeft.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/storage/out/storageOutItemRight.js"></script>
	<%-- <script type="text/javascript"
		src="${ctx}/javascript/common/dataTablesExtend.js"></script> --%>
</body>

</html>