<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="biolims.common.warehouseManagement"/></title>
<script type="text/javascript"
	src="${ctx}/javascript/storage/out/showStorageOutList.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/handleSearchForm.js"></script>

<g:LayOutWinTag buttonId="useTypeBtn" title='<fmt:message key="biolims.common.outboundType"/>' hasHtmlFrame="true"
	html="${ctx}/dic/type/dicTypeSelect.action" isHasSubmit="false"
	functionName="cklx" hasSetFun="true" documentId="type_id"
	documentName="type_name" />

<g:LayOutWinTag buttonId="searchSqr" title='<fmt:message key="biolims.common.recipients"/>' hasHtmlFrame="true"
	html="${ctx}/core/user/userSelect.action" isHasSubmit="false" 
	functionName="sqr" hasSetFun="true" documentId="useUser_id"
	documentName="useUser_name" />
<g:LayOutWinTag buttonId="regionType" title='<fmt:message key="biolims.common.selectTheCity"/>' hasHtmlFrame="true"
	html="${ctx}/dic/type/dicTypeSelect.action" isHasSubmit="false"
	functionName="xzcs" hasSetFun="true" documentId="regionType_id"
	documentName="regionType_name" />


</head>
<body>
	<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
	<div class="mainlistclass" id="markup">
		<div id="jstj" style="display: none">
			<table cellspacing="0" cellpadding="0" class="toolbarsection">
				<tr>
					<td valign="top" align="right"><span>&nbsp;</span> <label
						class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditionse"/>"><fmt:message key="biolims.storage.outPutId"/></label></td>
					<td nowrap class="quicksearch_control" valign="middle"><input
						type="text" searchField="true"
						class="input_parts text input default  readonlyfalse false"
						name="id" id="id" onblur="textbox_ondeactivate(event);"
						onfocus="shared_onfocus(event,this)" id="id" title="<fmt:message key="biolims.common.inputID"/>">
					</td>
					<td valign="top" align="right" class="controlTitle" nowrap><label
						title="<fmt:message key="biolims.common.describe"/>" class="text label"><fmt:message key="biolims.common.describe"/></label></td>
					<td nowrap><input type="text" searchField="true"
						class="input_parts text input default   false" name="note"
						id="note" value='' title=" " maxlength="30"></td>

				</tr>
				<tr>
					<td valign="top" align="right" class="controlTitle" nowrap><label
						title="<fmt:message key="biolims.common.recipients"/>" class="text label"><fmt:message key="biolims.common.recipients"/></label></td>
					<td nowrap><input type="text"
						class="input_parts text input default  readonlyfalse false"
						name="useUser.name" id="useUser_name" value='' title="<fmt:message key="biolims.common.recipients"/> "
						maxlength="30"> <img title='<fmt:message key="biolims.common.select"/>' id="searchSqr"
						src='${ctx}/images/img_lookup.gif' class='detail' /><input
						type="hidden" searchField="true" name="useUser.id" id="useUser_id"
						value=''></td>
					<%-- <td valign="top" align="right" class="controlTitle" nowrap><span
						class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key="biolims.common.outboundType"/>"
						class="text label"><fmt:message key="biolims.common.outboundType"/> </label></td>
					<td nowrap><input type="text"
						class="input_parts text input default  readonlyfalse false"
						maxlength="20" title="<fmt:message key="biolims.common.type"/>" name="type.name" id="type_name" /> <img
						title='<fmt:message key="biolims.common.select"/>' id=useTypeBtn src='${ctx}/images/img_lookup.gif'
						class='detail' /> <img title='' src='${ctx}/images/blank.gif'
						class='detail' style='display: none' /><input type="hidden"
						searchField="true" name="type.id" id="type_id"></td> --%>
				</tr>
				<%-- <tr>
					<td valign="top" align="right" class="controlTitle" nowrap><label
						title="领用人" class="text label">所在城市</label></td>
					<td nowrap><input type="text"
						class="input_parts text input default  readonlyfalse false"
						name="regionType.name" id="regionType_name" value='' title="选择城市"
						maxlength="30"> <img title='选择' id="regionType"
						src='${ctx}/images/img_lookup.gif' class='detail' /><input
						type="hidden" searchField="true" name="regionType.id"
						id="regionType_id" value=''></td> --%>
					
					<%-- <td class="label-title">入库日期</td>
			    		<td>
						<input type="text" class="Wdate" readonly="readonly" id=createDate1 name="createDate##@@##>=##@@##date" searchField="true"  onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" />
						</td>
						<td class="label-title">到</td>
			    		<td>
						<input type="text" class="Wdate" readonly="readonly" id="createDate2" name="createDate##@@##<=##@@##date"  searchField="true"  onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" />
						</td> --%>
				<!-- </tr> -->
			</table>
		</div>
		<input type="hidden" id="id" value="" />
		<form name='excelfrm' action='/common/exportExcel.action'
			method='POST'>
			<input type='hidden' id='gridhtm' name='gridhtm' value='' />
		</form>
		<input type='hidden' id='extJsonDataString' name='extJsonDataString'
			value='' />
		<div id='grid'  ></div>
<div id='gridcontainer'  ></div>
	</div>
</body>
</html>