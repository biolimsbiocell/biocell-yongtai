<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<link rel="stylesheet" href="${ctx}/css/experimentLab.css" />
<style type="text/css">
.dt-buttons {
	float: none;
}

.tablebtns {
	position: initial;
}
</style>
</head>

<body style="height: 94%">
<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>

	<div class="content-wrapper" id="content" style="margin-left: 0px;margin-top: 46px">
		<%-- <input type="hidden" id="pleaseVerifyPlan_name"
			value="<s:property value="pleaseVerifyPlan.name"/>"> --%> <input
			type="hidden" id="bpmTaskId" value="${requestScope.bpmTaskId}" />
		<section class="content" >
		<div class="row" style="">
			<!--表格-->
			<div class="col-xs-12">
				<div class="box box-info box-solid">
					<div class="box-header with-border">
						<i class="fa fa-bell-o"></i>
						<h3 class="box-title">
							请验计划 <small style="color: #fff;"> 请验计划编号: <span
								id="pleaseVerifyPlan_id"><s:property value="pleaseVerifyPlan.id" /></span>
							请验人: <span
								userId="<s:property value="pleaseVerifyPlan.outUser.id"/>"
								id="pleaseVerifyPlan_outUser"><s:property
										value="pleaseVerifyPlan.outUser.name" /></span>请验时间: <span
								id="pleaseVerifyPlan_outDate"><s:date name=" pleaseVerifyPlan.outDate " format="yyyy-MM-dd "/></span> 状态: <span
								state="<s:property value="pleaseVerifyPlan.state"/>"
								id="headStateName"><s:property
										value="pleaseVerifyPlan.stateName" /></span>
							</small>
						</h3>
					</div>
					<div class="box-body ipadmini">

						<div class="HideShowPanel">
							<form name="form1" class="layui-form" id="form1" method="post">
								<div class="row">
									<div class="col-xs-4">
										<div class="input-group">

											<span class="input-group-addon">描述<img
												class='requiredimage' src='${ctx}/images/required.gif' />
											</span> <input type="text" size="20" maxlength="25"
												id="pleaseVerifyPlan_note" name="note"
												changelog="<s:property value="pleaseVerifyPlan.note"/>"
												class="form-control" 
												value="<s:property value="pleaseVerifyPlan.note"/>" />

										</div>
									</div>
									<%-- <div class="col-xs-4">
										<div class="input-group">

											<span class="input-group-addon"><fmt:message key="biolims.storageOut.applyUser"/></span>

											<input type="text" size="20" readonly="readOnly" id="storageOut_useUser_name" changelog="<s:property value=" storageOut.useUser.name "/>" value="<s:property value=" storageOut.useUser.name "/>" class="form-control" />

											<input type="hidden" id="storageOut_useUser_id" name="useUser-id" value="<s:property value=" storageOut.useUser.id "/>">

											<span class="input-group-btn">
												<button class="btn btn-info" type="button" 
													onClick="showUseUser()">
													<i class="glyphicon glyphicon-search"></i>
												</span>
										</div>
									</div> --%>
								</div>
							</form>
						</div>

	</br>






						<!--库存主数据-->
						<div id="leftDiv" class="col-md-4 col-xs-12">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title"><fmt:message key="biolims.storageOut.storage"/></h3>
								</div>
								<div class="box-body ipadmini">
									<table
										class="table table-hover table-striped table-bordered table-condensed" 
										id="storageOutAlldiv" style="font-size: 14px;"></table>
								</div>
							</div>
						</div>
						<!--排板后样本-->
						<div id="rightDiv" class="col-md-8 col-xs-12">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title">请验计划明细</h3>
								</div>
								<div class="box-body ipadmini">
									<table
										class="table table-hover table-striped table-bordered table-condensed"
										id="pleaseVerifyPlanItemdiv" style="font-size: 14px;"></table>
								</div>
							</div>
						</div>
					</div>


				</div>
			</div>

		</div>
		</section>
	</div>
	<div style="display: none" id="batch_data" class="input-group">
		<span class="input-group-addon bg-aqua">产物数量</span> <input
			type="number" id="productNum" class="form-control"
			placeholder="" value="">
	</div>
	<script type="text/javascript"
		src="${ctx}/javascript/storage/pleaseverifyplan/pleaseVerifyPlanAllLeft.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/storage/pleaseverifyplan/pleaseVerifyPlanItemRight.js"></script>
</body>

</html>