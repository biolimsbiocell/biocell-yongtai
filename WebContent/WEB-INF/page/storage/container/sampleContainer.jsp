<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<link rel=stylesheet type="text/css" href="${ctx}/css/sample.css">
<script type="text/javascript" src="${ctx}/javascript/common/superTables_compressed.js"></script>
<link rel=stylesheet type="text/css" href="${ctx}/css/superTables_compressed.css">
<script type="text/javascript" src="${ctx}/javascript/storage/container/sampleContainer.js"></script>
</head>
<body>
<input type="hidden" id="hid_3d_row_num" value="${requestScope.rowNum}">
	<table id="cont_table" class="table">
		<s:bean name="org.apache.struts2.util.Counter" id="counter">
	        <s:param name="first" value="0" /><!-- 可以控制开始和结束 -->
	        <s:param name="last" value="%{#request.colNum}" />
	       	<tr align="center">
		        <s:iterator status="stu"> 
		        	<s:if test="#stu.index==0">
		        		<td class="td" style="width:80px;min-width: 80px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;轴&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        	</s:if>
		        	<s:else>
		        		<td class="td">
		        			<s:property value="#stu.index"/>
		        		</td>
		        	</s:else>
		        </s:iterator>
	        </tr> 
        </s:bean>
		<s:bean name="org.apache.struts2.util.Counter" id="counter">
	        <s:param name="first" value="0" /><!-- 可以控制开始和结束 -->
	        <s:param name="last" value="%{#request.rowNum-1}" />
	        <s:set name="indexVal" value="0"></s:set>
	        <s:set name="showIndex" value="0"></s:set>
	        <s:iterator >  
	             <tr class="text">
					<s:bean name="org.apache.struts2.util.Counter" id="counter">
				        <s:param name="first" value="0" /><!-- 可以控制开始和结束 -->
				        <s:param name="last" value="%{#request.colNum}" />
				        <s:set name="indexCol" value="0"></s:set>
				         <s:set name="indexVal" value="#indexVal+1"></s:set> 
				        <s:iterator status="stu"> 
				        	<s:if test="#stu.index==0">
				        		<td class="td show_col" style="width:80px;min-width: 80px;"  align="center">
				        		<s:property value="%{#indexVal}"/>
				        		</td>
				        	</s:if>
				        	<s:else>
				        	 	<s:set name="showIndex" value="#showIndex+1"></s:set> 
				        		<td class="td" num='<s:property value="#indexVal"/>,<s:property value="#indexCol"/>'>
					             	<div class="show-index-div"></div>
								</td>
				        	</s:else>
				        	<s:set name="indexCol" value="#indexCol+1"></s:set> 
				        </s:iterator>
					 </s:bean>
				</tr>
	        </s:iterator>
		 </s:bean>
	</table>
</body>
</html>