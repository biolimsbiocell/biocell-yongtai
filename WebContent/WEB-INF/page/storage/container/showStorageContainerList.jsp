<%-- <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript"
	src="${ctx}/javascript/storage/container/showStorageContainer.js"></script>
</head>
<g:HandleDataExtTag hasConfirm="false" paramsValue="userId:'${userId}'" funName="modifyData" url="${ctx}/sysmanage/container/saveContainer.action"/>
<g:HandleDataExtTag funName="delData" url="${ctx}/sysmanage/dic/delDicType.action" />
   <%@ include file="/WEB-INF/page/include/toolbarNoButton.jsp"%>
<body>
<div  class="mainfullclass">
<input type="hidden" id = "ctx" value="${ctx }">
<input type="hidden" id="id" value="" />
<div id = "show_ct_div"></div>
<div id="3d_image"></div>
</body>
</html> --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</style>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<link rel="stylesheet" href="${ctx}/css/experimentLab.css"/>

</head>
<body>
<div class="content-wrapper" id="content" style="margin-left: 0px;">
			<section class="content">
				<div class="row">
					<!--表格-->
					<div class="col-xs-12">
						<div class="box box-info box-solid">
							<div class="box-header with-border">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title"><fmt:message key="biolims.common.storageManageMent"/></h3>
								<div class="box-tools pull-right">
						 			<button type="button" class="btn btn-box-tool" id="tableRefresh" onclick="tableRefresh()">
						 				<i class="glyphicon glyphicon-refresh"></i>
                                     </button> 
									<div class="btn-group">
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                         Action <span class="caret"></span>
                                         </button>
										<ul class="dropdown-menu">
											<li>
												<a href="####" onclick="$('.buttons-print').click();"><fmt:message key="biolims.common.print"/></a>
											</li>
											<li>
												<a href="#####" onclick="$('.buttons-copy').click();"><fmt:message key="biolims.common.copyData"/></a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-excel').click();">Excel</a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-csv').click();">CSV</a>
											</li>
											<li role="separator" class="divider"></li>
											<li>
												<a href="####" onclick="fixedCol(2)"><fmt:message key="biolims.common.lock2Col"/></a>
											</li>
											<li>
												<a href="####" id="unfixde" onclick="unfixde()"><fmt:message key="biolims.common.cancellock2Col"/></a>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="box-body ipadmini">
								<table class="table table-hover table-striped table-bordered table-condensed" id="showStorageDiv" style="font-size:14px;">
								</table>
							</div>
						</div>
					</div>
				
				</div>

			</section>
		</div>
</body>
<script type="text/javascript" src="${ctx}/javascript/storage/container/showStorageContainerViewList.js"></script>
</html>