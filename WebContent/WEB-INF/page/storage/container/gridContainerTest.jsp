<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<style>
.tip{ font:18px bold;
	 /* font-family:"楷体"; */
	  color:color:#F00;
	  background-color:#ccc;
}
</style>
<link rel=stylesheet type="text/css" href="${ctx}/css/sample.css">
<script type="text/javascript" src="${ctx}/javascript/common/superTables_compressed.js"></script>
<link rel=stylesheet type="text/css" href="${ctx}/css/superTables_compressed.css">
<script type="text/javascript" src="${ctx}/javascript/storage/container/gridContainerTest.js"></script>
</head>
<body>
	<table style="width:98%;">
		<tr>
			<td   class="label-title" >
				<div id="newAdr" style="display:none;align:center;">
					请输入新的存储位置：
					<input type="text" size = "20" id="newLoc" name="newLoc" value="" readonly/>
					<input type="hidden" size = "20" id="oldLoc" name="oldLoc" value=""/>&nbsp;
					<input type="button" value="  确定  " onclick="chengeAdr()">
				</div>
			</td>
			<td class="label-title"  align="right">
			 	<input type="button" id="selAll" onclick="exportexcel();" value="导出96孔板" />
				<!-- <input type="checkbox" id="inverse" onclick="putDouble();" />列序  -->
			</td>
		</tr>
	</table>
	<div id="gridContainerdiv0"></div>
	<form name='excelfrm3' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
	 <input type="hidden" id="hid_3d_row_num" value="${requestScope.rowNum}">
	 <input type="hidden" id="hid_3d_col_num" value="${requestScope.colNum}">
	 <input type="hidden" id="hid_3d_type" value="${requestScope.type}">
	 <input type="hidden" id="hid_3d_maxNum" value="${requestScope.maxNum}">
	 <input type="hidden" id="hid_3d_counts" value="${requestScope.counts}">
</body>
</html>