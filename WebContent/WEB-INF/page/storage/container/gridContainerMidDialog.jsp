<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<link rel=stylesheet type="text/css" href="${ctx}/css/sample.css">

<script type="text/javascript" src="${ctx}/javascript/common/superTables_compressed.js"></script>
<link rel=stylesheet type="text/css" href="${ctx}/css/superTables_compressed.css">
<script type="text/javascript" src="${ctx}/javascript/storage/container/gridContainerMidDialog.js"></script>
</head>
<body>
	<div id="gridContainerdiv"></div>
	 <input type="hidden" id="hid_3d_col_num" value="${requestScope.colNum}">
	 <input type="hidden" id="hid_3d_row_num" value="${requestScope.rowNum}">
	 <input type="hidden" id="tree_Id" value="${requestScope.tree_Id}">
	 <input type="hidden" id="type" value="${requestScope.type}">
	 <div class="mainlistclass" id="gridContainerMidDiolagdiv" style="height:50%;width:100%"></div>
</body>
</html>