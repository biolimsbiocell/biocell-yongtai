<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<style>
.tip{ font:18px bold;
	 /* font-family:"楷体"; */
	  color:color:#F00;
	  background-color:#ccc;
}
</style>
<link rel=stylesheet type="text/css" href="${ctx}/css/sample.css">
<script type="text/javascript" src="${ctx}/javascript/common/superTables_compressed.js"></script>
<link rel=stylesheet type="text/css" href="${ctx}/css/superTables_compressed.css">
<script type="text/javascript" src="${ctx}/javascript/storage/container/gridContainer2w.js"></script>
</head>
<body>
	<div id="gridContainer2wdiv"></div>
	 <input type="hidden" id="hid_3d_row_num" value="${requestScope.rowNum}">
	 <input type="hidden" id="hid_3d_col_num" value="${requestScope.colNum}">
</body>
</html>