<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript"
	src="${ctx}/javascript/sysmanage/container/scanPositionContainerBat.js"></script>

</head>

<body>	
<div id="markup" class="mainfullclass" >
	<table id="cont_table" class="table">
		<s:hidden id="contInfo" value="%{#request.scs.info}"></s:hidden>
		<s:hidden id="showName" value="%{#request.showName}"></s:hidden>
		<s:hidden id="showId" value="%{#request.showId}"></s:hidden>
		<s:hidden id="contId" value="%{#request.contId}"></s:hidden>
		<s:bean name="org.apache.struts2.util.Counter" id="counter">
	        <s:param name="first" value="1" /><!-- 可以控制开始和结束 -->
	        <s:param name="last" value="%{#request.cont.rowNum}" />
	        <s:set name="indexVal" value="0"></s:set>
	        <s:set name="showIndex" value="0"></s:set>
	        <s:iterator >  
	             <tr class="text">
					<s:bean name="org.apache.struts2.util.Counter" id="counter">
				        <s:param name="first" value="0" /><!-- 可以控制开始和结束 -->
				        <s:param name="last" value="%{#request.cont.colNum}" />
				        <s:set name="indexCol" value="0"></s:set>
				        <s:iterator status="stu"> 
				        	<s:if test="#stu.index==0">
				        		<td class="td" style="width:50px;font-size: 18px;" align="center">
				        		<s:property value="%{#request.cont.colNum-#indexVal}"/>
				        		</td>
				        	</s:if>
				        	<s:else>
				        		<td class="td" style="vertical-align:top;" valign="top" num='<s:property value="#indexVal"/>,<s:property value="#indexCol"/>'>
					  	            <s:set name="showIndex" value="#showIndex+1"></s:set> 
					             	&nbsp;
					             	<div style="margin-top: -15px;">
					             		<span class="showIndexSpan">
					             			<s:property value="#showIndex"/>
					             		</span>
						             	<span>
						             		<input type="checkbox" name="contbox" value='<s:property value="#indexVal"/>,<s:property value="#indexCol"/>'>
						             	</span>
					             	</div>
								</td>
				        	</s:else>
				        	<s:set name="indexCol" value="#indexCol+1"></s:set> 
				        </s:iterator>
					 </s:bean>
					 <s:set name="indexVal" value="#indexVal+1"></s:set> 
				</tr>
	        </s:iterator>
		 </s:bean>
		<s:bean name="org.apache.struts2.util.Counter" id="counter">
	        <s:param name="first" value="0" /><!-- 可以控制开始和结束 -->
	        <s:param name="last" value="%{#request.cont.colNum}" />
	       	<tr align="center">
		        <s:iterator status="stu"> 
		        	<s:if test="#stu.index==0">
		        		<td class="td" style="width:50px;height:20px">轴</td>
		        	</s:if>
		        	<s:else>
		        		<td class="td show_row"  style="height:20px">
		        			<s:property value="#stu.index"/>
		        		</td>
		        	</s:else>
		        </s:iterator>
	        </tr> 
        </s:bean>
	</table>
	
	<s:hidden id="delItemIds"></s:hidden>
</div>
</body>
</html>