<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<link rel="stylesheet" href="${ctx}/css/experimentLab.css" />
<style type="text/css">
.dt-buttons {
	float: none;
}
#btn_submit {
   display:none;
}
.tablebtns {
	position: initial;
}
</style>
</head>

<body style="height: 94%">
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div class="content-wrapper" id="content"
		style="margin-left: 0px; margin-top: 46px">
		<input type="hidden" id="storageIn_name"
			value="<s:property value="storageIn.name"/>"> <input
			type="hidden" id="bpmTaskId" value="${requestScope.bpmTaskId}" />
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
		<section class="content">
		<div class="row">
			<!--表格-->
			<div class="col-xs-12">
				<div class="box box-info box-solid">
					<div class="box-header with-border">
						<i class="fa fa-bell-o"></i>
						<h3 class="box-title">
							<fmt:message key="biolims.storageIn" />
							<small style="color: #fff;"> <fmt:message
									key="biolims.storageIn.id" />: <span id="storageIn_id"><s:property
										value="storageIn.id" /></span> <fmt:message
									key="biolims.storageIn.createUser" />: <span
								userId="<s:property value="storageIn.handelUser.id"/>"
								id="storageIn_handelUser"><s:property
										value="storageIn.handelUser.name" /></span> <fmt:message
									key="biolims.storageIn.createDate" /> : <span
								id="storageIn_createDate"><s:date
										name=" storageIn.createDate " format="yyyy-MM-dd " /></span> <fmt:message
									key="biolims.storageIn.state" /> : <span
								state="<s:property value="storageIn.state"/>" id="headStateName"><s:property
										value="storageIn.stateName" /></span>
							</small>
						</h3>
					</div>
					<div class="box-body ipadmini">
						<div class="HideShowPanel">
							<form name="form1" class="layui-form" id="form1" method="post">
								<div class="row">
									<div class="col-xs-4">
										<div class="input-group">

											<span class="input-group-addon"> <fmt:message
													key="biolims.storageIn.note" /> <img class='requiredimage'
												src='${ctx}/images/required.gif' />
											</span> <input type="text" size="20" maxlength="25"
												id="storageIn_note" name="note"
												changelog="<s:property value="storageIn.note"/>"
												class="form-control"
												value="<s:property value="storageIn.note"/>" />
										</div>
									</div>
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon" style="font-family: 黑体;">
												U8出库单日期
											</span> <input class="form-control " type="text" size="20"
												maxlength="25" id="storageIn_handelDate" name="handelDate"
												title=""
												value="<s:date name=" storageIn.handelDate " format="yyyy-MM-dd "/>" />
												
										</div>
									</div>
									<div class="col-xs-4">
										<div class="input-group">

											<span class="input-group-addon"> 部门<img class='requiredimage'
												src='${ctx}/images/required.gif' />
											</span>  <input type="hidden" size="20" maxlength="25"
												id="storageIn_departmnet_id" name="departmnet-id"
												changelog="<s:property value="storageIn.departmnet.id"/>"
												class="form-control"
												value="<s:property value="storageIn.departmnet.id"/>" />
												<input type="text" size="20"  maxlength="25"
												id="storageIn_departmnet_name" 
												changelog="<s:property value="storageIn.departmnet.name"/>"
												class="form-control" readonly="readonly"
												value="<s:property value="storageIn.departmnet.name"/>" />
												<span class="input-group-btn">
												 	<button type="button" class="btn btn-info" onclick="choseParent()"><i class="glyphicon glyphicon-search"></i></button>
												 </span>
										</div>
									</div>
								</div>
							</form>
						</div>
						</br>
						<!--库存主数据-->
						<div id="leftDiv" class="col-md-4 col-xs-12">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title">
										<fmt:message key="biolims.storageIn.storage" />
									</h3>
								</div>
								<div class="box-body ipadmini">
									<table
										class="table table-hover table-striped table-bordered table-condensed"
										id="storageAlldiv" style="font-size: 14px;"></table>
								</div>
							</div>
						</div>
						<!--排板后样本-->
						<div id="rightDiv" class="col-md-8 col-xs-12">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title">
										<fmt:message key="biolims.storageIn.inItem" />
									</h3>
								</div>
								<div class="box-body ipadmini">
									<table
										class="table table-hover table-striped table-bordered table-condensed"
										id="storageInItemdiv" style="font-size: 14px;"></table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</section>
	</div>
	<div style="display: none" id="batch_data" class="input-group">
		<span class="input-group-addon bg-aqua">产物数量</span> <input
			type="number" id="productNum" class="form-control" placeholder=""
			value="">
	</div>
	<script type="text/javascript"
		src="${ctx}/javascript/storage/storageIn/storageAllLeft.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/storage/storageIn/storageInItemRight.js"></script>
	<%-- 	<script type="text/javascript"
		src="${ctx}/javascript/common/dataTablesExtend.js"></script> --%>
</body>

</html>