<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="/operfile/initFileList.action?modelType=storageInInfo&id=${storageInInfo.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/storage/storageIn/item/storageInInfoEdit.js"></script>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
			<input type="hidden" id="serial" value="<%=request.getParameter("serial")%>">
			<input type="hidden" id="storageInId" value="${requestScope.storageInId}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" >原辅料编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="storageInInfo_id"
                   	 name="storageInInfo.id" title="原辅料编号" class="text input readonlytrue" readonly="readOnly"
                   	   
	value="<s:property value="storageInInfo.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="40" maxlength="25" id="storageInInfo_name"
                   	 name="storageInInfo.name" title="描述"  class="text input readonlytrue" readonly="readOnly"
                   	   
	value="<s:property value="storageInInfo.name"/>"
                   	  />
                   	  
                   	</td>
			
               	 	<td class="label-title" >原辅料盒</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="storageInInfo_kit_name"  value="<s:property value="storageInInfo.kit.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="storageInInfo_kit" name="storageInInfo.kit.id"  value="<s:property value="storageInInfo.kit.id"/>" > 
                   	</td>
			</tr>
			<tr>
			
               	 	<td class="label-title" >创建人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="storageInInfo_createUser_name"  value="<s:property value="storageInInfo.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="storageInInfo_createUser" name="storageInInfo.createUser.id"  value="<s:property value="storageInInfo.createUser.id"/>" > 
                   	</td>
			
			
               	 	<td class="label-title" >创建时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="storageInInfo_createDate"
                   	 name="storageInInfo.createDate" title="创建时间"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	  value="<s:date name="storageInInfo.createDate" format="yyyy-MM-dd"/>"
                   	                      	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="storageInInfo_state"
                   	 name="storageInInfo.state" title="状态"
                   	   
	value="<s:property value="storageInInfo.state"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >状态描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="storageInInfo_stateName"
                   	 name="storageInInfo.stateName" title="状态描述"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="storageInInfo.stateName"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="250" id="storageInInfo_note"
                   	 name="storageInInfo.note" title="备注"
                   	   
	value="<s:property value="storageInInfo.note"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr>
			
			
            </table>
            <input type="hidden" name="storageInInfoItemJson" id="storageInInfoItemJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="storageInInfo.id"/>" />
            </form>
            <div id="tabs">
            <ul>
			<!-- <li><a href="#storageInInfoItempage">入库原辅料明细</a></li> -->
           	</ul> 
			<div id="storageInInfoItempage" width="100%" height:10px></div>
			</div>
        	</div>
	</body>
	</html>
