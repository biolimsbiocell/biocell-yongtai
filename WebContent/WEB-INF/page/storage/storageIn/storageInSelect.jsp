<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>

</head>
<script type="text/javascript">
	function setvalue(rec){
		<%if(request.getParameter("flag")!=null){%>
	
		 window.parent.set<%=request.getParameter("flag")%>(rec);
		 <%}%>
	}

	function view(){

		var r = gridGrid.getSelectionModel().getSelected();
		
		if(r==null){

			message(biolims.common.selectRecord);
		}else{
			openDialog(window.ctx+'/storage/storageIn/toViewStorageIn.action?noButton=true&id='+r.data.id);  
			
		}
		}
</script>
<body>
	<div id="markup" class="mainfullclass">
		<g:GridTagNoLock col="${col}"
			tbar="{text: '确认选中', handler:function(){gridSetSelectionValue();}},{text: '查看选中', handler:function(){view();}}"
			isHasTbar="true" type="${type}" title="" isAutoWidth="true"
			url="${path}" callFun="setvalue" callFunRecord="true" />
	</div>

</body>
</html>