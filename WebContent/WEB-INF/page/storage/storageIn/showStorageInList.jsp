<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="biolims.common.purchasingApplicationList"/></title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript"
	src="${ctx}/javascript/storage/storageIn/showStorageInList.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/handleSearchForm.js"></script>

<g:LayOutWinTag buttonId="searchOrder" title='<fmt:message key="biolims.common.chooseThePurchaseOrder"/>'
	hasHtmlFrame="true"
	html="${ctx}/purchase/order/purchaseOrderSelect.action" width="900"
	height="500" isHasSubmit="false" functionName="searchOrderFun"
	hasSetFun="true" extRec="rec" documentId="purchaseOrder_id"
	documentName="purchaseOrder_name"
	extStr="document.getElementById('purchaseOrder_id').value=rec.get('id');" />

<g:LayOutWinTag buttonId="searchUser" title='<fmt:message key="biolims.common.selectingPersonOfTheOperating"/>' hasHtmlFrame="true"
	hasSetFun="true" html="${ctx}/core/user/userSelect.action"
	isHasSubmit="false" functionName="dutyUserFun"
	documentId="handelUser_id" documentName="handelUser_name" />

<g:LayOutWinTag buttonId="searchSupplier" title='<fmt:message key="biolims.common.selectSuppliers"/>'
	hasHtmlFrame="true" html="${ctx}/supplier/common/supplierSelect.action"
	width="900" height="500" isHasSubmit="false"
	functionName="searchSupplierFun" hasSetFun="true" extRec="rec"
	documentId="supplier_id" documentName="supplier_name"
	extStr="document.getElementById('supplier_name').value=rec.get('name');
	document.getElementById('supplier_id').value=rec.get('id');" />
<g:LayOutWinTag buttonId="regionType" title='<fmt:message key="biolims.common.selectTheCity"/>' hasHtmlFrame="true"
	html="${ctx}/dic/type/dicTypeSelect.action" isHasSubmit="false"
	functionName="xzcs" hasSetFun="true" documentId="regionType_id"
	documentName="regionType_name" />
</head>
<body>
	<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>

	<div class="mainlistclass" id="markup">
		<div id="jstj" style="display: none">
			<table cellspacing="0" cellpadding="0" class="toolbarsection">
				<tr>
					<td valign="top" align="right"><span>&nbsp;</span> 
					<label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>">入库明细ID</label></td>

					<td nowrap class="quicksearch_control" valign="middle"><input
						type="text"
						class="input_parts text input default  readonlyfalse false"
						name="id" onblur="textbox_ondeactivate(event);"
						onfocus="shared_onfocus(event,this)" searchField="true" id="sid"
						title="<fmt:message key="biolims.common.inputID"/><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
						onfocus="shared_onfocus(event,this)" searchField="true" id="sid" title="<fmt:message key="biolims.common.inputID"/>"><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> </td>
                   <td valign="top" align="right" class="controlTitle" nowrap><label
				    class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>"> <fmt:message key="biolims.common.putInStoragePerson"/></label></td>
					<td nowrap ><input
						type="hidden" searchField="true" name="handelUser.id"
						id="handelUser_id"></input> <input type="text"				
						class="input_parts text input default  readonlyfalse false"
						name="handelUser-name" onblur="textbox_ondeactivate(event);"
						onfocus="shared_onfocus(event,this)" id="handelUser_name"
						title="<fmt:message key="biolims.common.selectingPersonOfTheOperating"/>"> <img alt='<fmt:message key="biolims.common.selectingPersonOfTheOperating"/>' id='searchUser'
						name='searchUser' src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>
				</tr>
				<tr>
					
					<%-- <td valign="top" align="right" class="controlTitle" nowrap><label
						title="选择城市" class="text label">所在城市</label></td>
					<td nowrap><input type="text"
						class="input_parts text input default  readonlyfalse false"
						name="regionType.name" id="regionType_name" value='' title="选择城市"
						maxlength="30"> <img title='选择' id="regionType"
						src='${ctx}/images/img_lookup.gif' class='detail' /><input
						type="hidden" searchField="true" name="regionType.id"
						id="regionType_id" value=''></td> --%>
				</tr>

				<!-- <tr>
					<td>
						<label class="text label" title="输入查询条件"> 记录数</label>
					</td>
					<td nowrap class="quicksearch_control" valign="middle">
						<input type="text"	class="input_parts text input default  readonlyfalse false" name="limitNum" onblur="textbox_ondeactivate(event);"
						onfocus="shared_onfocus(event,this)" id="limitNum" title="输入记录数" >
					</td>
				</tr> 
				<tr>
						<td class="label-title">出库日期</td>
			    		<td>
						<input type="text" class="Wdate" readonly="readonly" id=createDate1 name="quitDate##@@##>=##@@##date" searchField="true"  onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" />
						</td>
						<td class="label-title">到</td>
			    		<td>
						<input type="text" class="Wdate" readonly="readonly" id="createDate2" name="quitDate##@@##<=##@@##date"  searchField="true"  onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" />
						</td>
			    </tr>
				-->
				<tr>
					<%-- <td class="label-title">入库日期</td>
			    		<td>
						<input type="text" class="Wdate" readonly="readonly" id=createDate1 name="quitDate##@@##>=##@@##date" searchField="true"  onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" />
						</td>
						<td class="label-title">到</td>
			    		<td>
						<input type="text" class="Wdate" readonly="readonly" id="createDate2" name="quitDate##@@##<=##@@##date"  searchField="true"  onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" />
						</td> --%>
				</tr>
			</table>
		</div>
		<input type="hidden" id="id" value="" />
		
		<form name='excelfrm' action='/common/exportExcel.action'
			method='POST'>
			<input type='hidden' id='gridhtm' name='gridhtm' value='' />
		</form>
		<input type='hidden' id='extJsonDataString' name='extJsonDataString'
			value='' />
		<div id='grid'></div>
		<div id='gridcontainer'></div>
	</div>
</body>
</html>