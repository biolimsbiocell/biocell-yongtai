<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
<title></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style>
.dataTables_scrollBody {
	min-height: 100px;
}
</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarNoState.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div style="height: 14px"></div>
	<input type="hidden" id="p_type" value="${requestScope.p_type}" />
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 46px">
		<!--Form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<i class="glyphicon glyphicon-pencil"></i>
						<fmt:message key="biolims.tStorage.warehouse" />
					</h3>

					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="####" onclick="$('.buttons-print').click();">
										<fmt:message key="biolims.common.print" />
								</a></li>
								<li><a href="#####" onclick="$('.buttons-copy').click();">
										<fmt:message key="biolims.common.copyData" />
								</a></li>
								<li><a href="####" onclick="$('.buttons-excel').click();">Excel</a>
								</li>
								<li><a href="####" onclick="$('.buttons-csv').click();">CSV</a>
								</li>
								<li role="separator" class="divider"></li>
								<li><a href="####" onclick="itemFixedCol(2)"> <fmt:message
											key="biolims.common.lock2Col" />
								</a></li>
								<li><a href="####" id="unfixde" onclick="unfixde()"> <fmt:message
											key="biolims.common.cancellock2Col" />
								</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="box-body">
					<!--进度条-->
					<div id="wizard" class="form_wizard wizard_horizontal">
						<ul class="wizard_steps">
							<li><a class="selected step"> <span class="step_no">1</span>
									<span class="step_descr"> Step 1<br /> <small><fmt:message
												key="biolims.tStorage.warehouse" /></small>
								</span>
							</a></li>
							<li><a class="disabled step" id="step_no"> <span
									class="step_no">2</span> <span class="step_descr"> Step
										2<br /> <small><fmt:message
												key="biolims.common.materialPurchaseDetail" /></small>
								</span>
							</a></li>
							<li><a class="disabled step" id="step_no2"> <span
									class="step_no">3</span> <span class="step_descr"> Step
										3<br /> <small>组成成分</small>
								</span>
							</a></li>
						</ul>
					</div>
					<!--form表单-->
					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
							<input type="hidden" id="bpmTaskId"
								value="<%=request.getParameter(" bpmTaskId ")%>" /> <br> <br>
							<div class="row">
								<div class="panel"
									style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
									<div class="panel-heading text-left">
										<h3 class="panel-title" style="font-family: 黑体;">
											<i class="glyphicon glyphicon-bookmark"></i>
											<fmt:message key="biolims.common.basicInformation" />

										</h3>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tStorage.id" /> <img class='requiredimage'
											src='${ctx}/images/required.gif' /> </span> <input type="text"
											size="50" maxlength="127" id="storage_id"
											changelog="<s:property value=" storage.id "/>" name="id"
											title="<fmt:message key= 'biolims.tStorage.id' />"
											readonly="readOnly" class="form-control readonlytrue"
											value="<s:property value=" storage.id "/>" />

									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tStorage.name" /> <img class='requiredimage'
											src='${ctx}/images/required.gif' /> </span> <input type="text"
											size="50" maxlength="127" id="storage_name"
											changelog="${storage.name}" name="name"
											title="<fmt:message key= 'biolims.tStorage.name' />"
											class="form-control"
											value="<s:property value=" storage.name "/>" />

									</div>
								</div>

								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tStorage.type" /> <img class='requiredimage'
											src='${ctx}/images/required.gif' /> </span> <input type="text"
											size="20" readonly="readOnly" id="storage_type_name"
											changelog="<s:property value=" storage.type.name "/>"
											value="<s:property value=" storage.type.name "/>"
											class="form-control" /> <input type="hidden"
											id="storage_type_id" name="type-id"
											value="<s:property value=" storage.type.id "/>"> <span
											class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showtype()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tStorage.studyType" /> <img
											class='requiredimage' src='${ctx}/images/required.gif' /> </span> <input
											type="text" size="20" readonly="readOnly"
											id="storage_studyType_name"
											changelog="<s:property value=" storage.studyType.name "/>"
											value="<s:property value=" storage.studyType.name "/>"
											class="form-control" /> <input type="hidden"
											id="storage_studyType_id" name="studyType-id"
											value="<s:property value=" storage.studyType.id "/>">

										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showstudyType()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tStorage.kit" /> </span> <input type="text" size="20"
											readonly="readOnly" id="storage_kit_name"
											changelog="<s:property value=" storage.kit.name "/>"
											value="<s:property value=" storage.kit.name "/>"
											class="form-control" /> <input type="hidden"
											id="storage_kit_id" name="kit-id"
											value="<s:property value=" storage.kit.id "/>"> <span
											class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showkit()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tStorage.state" /> <img class='requiredimage'
											src='${ctx}/images/required.gif' /> </span> <input type="text"
											size="20" readonly="readOnly" name="state-name"
											id="storage_state_name"
											changelog="<s:property value="storage.state.name"/>"
											value="<s:property value=" storage.state.name "/>"
											class="form-control" /> <input type="hidden"
											id="storage_state_id" name="state-id"
											value="<s:property value=" storage.state.id "/>"> <span
											class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showstate()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tStorage.position" /> </span> <input type="text"
											size="20" readonly="readOnly" id="storage_position_name"
											changelog="<s:property value=" storage.position.name "/>"
											value="<s:property value=" storage.position.name "/>"
											class="form-control" /> <input type="hidden"
											id="storage_position_id" name="position-id"
											value="<s:property value=" storage.position.id "/>">

										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showposition()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon">货号</span> <input type="text"
											size="50" maxlength="127" id="storage_jdeCode"
											changelog="${storage.jdeCode}" name="jdeCode"
											class="form-control"
											value="<s:property value=" storage.jdeCode "/>" />

									</div>
								</div>
								<%-- <div class="col-md-4 col-sm-6 col-xs-12">
										<div class="input-group">

											<span class="input-group-addon"><fmt:message key="biolims.tStorage.searchCode"/> </span>

											<input type="text" size="50" maxlength="127" id="storage_searchCode" changelog="${storage.searchCode}" name="searchCode"  class="form-control" value="<s:property value=" storage.searchCode "/>" />

										</div>
									</div> --%>
								<!-- </div>
									<div class="row"> -->
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tStorage.createUser" /> </span> <input type="text"
											size="20" readonly="readOnly" id="storage_createUser_name"
											changelog="<s:property value=" storage.createUser.name "/>"
											value="<s:property value=" storage.createUser.name "/>"
											class="form-control" /> <input type="hidden"
											id="storage_createUser_id" name="createUser-id"
											value="<s:property value=" storage.createUser.id "/>">

										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showcreateUser()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tStorage.createDate" /> </span> <input type="text"
											size="50" maxlength="127" id="storage_createDate"
											name="createDate"
											changelog="<s:date name=" storage.createDate " format="yyyy-MM-dd "/>"
											title="<fmt:message key= 'biolims.tStorage.createDate' />"
											class="form-control"
											value="<s:date name=" storage.createDate " format="yyyy-MM-dd "/>" />

									</div>
								</div>
								<%-- <div class="col-md-4 col-sm-6 col-xs-12">
										<div class="input-group">

											<span class="input-group-addon"><fmt:message key="biolims.tStorage.barCode"/> </span>

											<input type="text" size="50" maxlength="127" id="storage_barCode" name="barCode" changelog="<s:property value=" storage.barCode "/>" title="<fmt:message key='biolims.tStorage.barCode'/>" class="form-control" value="<s:property value=" storage.barCode "/>" />

										</div>
									</div> --%>
								<!-- </div>
								<div class="row"> -->
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tStorage.ifCall" /> </span> <select
											class="form-control" id="storage_ifCall" name="ifCall"
											changelog="<s:property value=" storage.ifCall "/>"
											title="<fmt:message key= 'biolims.tStorage.ifCall' />"
											class="form-control">
											<option value="0"
												<s:if test="storage.ifCall==0">selected="selected" </s:if>>
												<fmt:message key="biolims.common.no" />
											</option>
											<option value="1"
												<s:if test="storage.ifCall==1">selected="selected" </s:if>>
												<fmt:message key="biolims.common.yes" />
											</option>
										</select>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tStorage.currentIndex" /> </span> <input type="text"
											size="20" maxlength="25" id="storage_currentIndex"
											changelog="${storage.currentIndex}" name="currentIndex"
											title="<fmt:message key= 'biolims.tStorage.currentIndex' />"
											class="form-control"
											value="<s:property value=" storage.currentIndex "/>" />

									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tStorage.engName" /> </span> <input type="text"
											size="50" maxlength="127" id="storage_engName"
											changelog="${storage.engName}" name="engName"
											title="<fmt:message key= 'biolims.tStorage.engName' />"
											class="form-control"
											value="<s:property value=" storage.engName "/>" />

									</div>
								</div>
								<!-- </div>
								<div class="row"> -->
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tStorage.spec" /> </span> <input type="text" size="50"
											maxlength="127" id="storage_spec"
											changelog="<s:property value="storage.spec"/>" name="spec"
											title="<fmt:message key= 'biolims.tStorage.spec' />"
											class="form-control"
											value="<s:property value=" storage.spec "/>" />

									</div>
								</div>

								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon">别名 </span> <input type="text"
											size="50" maxlength="127" id="storage_otherName"
											changelog="<s:property value="storage.otherName"/>"
											name="otherName" title="别名" class="form-control"
											value="<s:property value=" storage.otherName "/>" />

									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon">是否为培养基</span> <select
											class="form-control" id="yesOrNo_medium" name="medium">
											<option value=""
												<s:if test="storage.medium==''">selected="selected"</s:if>>
												<fmt:message key="biolims.common.pleaseSelect" />
											</option>
											<option value="1"
												<c:if test="${storage.medium==1}">selected="selected"</c:if>>
												是</option>
											<option value="0"
												<c:if test="${storage.medium==0}">selected="selected"</c:if>>
												否</option>
										</select>
										<%-- <input type="text" size="50" maxlength="127" id="unitGroupNew_state" changelog="<s:property value="unitGroupNew.state"/>" name="unitGroupNew.state" title="<fmt:message key="biolims.common.email"/>" class="form-control" value="<s:property value="unitGroupNew.state"/>" /> --%>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">试剂标识码<img class='requiredimage'
											src='${ctx}/images/required.gif' />  </span> <input type="text"
											size="50" maxlength="127" id="storage_biaoShiMa" 
											changelog="<s:property value="storage.biaoShiMa"/>"
											name="biaoShiMa" title="试剂标识码" class="form-control"
											value="<s:property value=" storage.biaoShiMa "/>" />
									</div>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="panel"
									style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
									<div class="panel-heading text-left">
										<h3 class="panel-title" style="font-family: 黑体;">
											<i class="glyphicon glyphicon-bookmark"></i>
											<fmt:message key="biolims.common.inventoryInformation" />

										</h3>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tStorage.num" /> </span> <input type="text" size="50"
											readonly="readOnly" maxlength="127" id="storage_num"
											changelog="${storage.num}" name="num"
											title="<fmt:message key= 'biolims.tStorage.num' />"
											class="form-control"
											value="<s:property value=" storage.num "/>" />

									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<!-- 单位组 -->
										<span class="input-group-addon"><fmt:message
												key="biolims.unitGroupNew.unitGroup" /> </span> <input type="text"
											size="50" readonly="readonly" maxlength="127"
											id="storage_unitGroup_name" changelog="${unitGroup.name}"
											name="unitGroup-name"
											title="<fmt:message key= 'biolims.unitGroupNew.unitGroup' />"
											class="form-control"
											value="<s:property value=" storage.unitGroup.name "/>" /> <input
											type="hidden" size="50" maxlength="127"
											id="storage_unitGroup_id" name="unitGroup-id"
											title="<fmt:message key= 'biolims.unitGroupNew.unitGroup' />"
											class="form-control"
											value="<s:property value=" storage.unitGroup.id "/>" /> <span
											class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showunitGroup()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
								<!-- 单位 -->
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.tStorage.unit" /> </span> <input type="text" size="50"
											readonly="readonly" maxlength="127"
											id="storage_unitGroup_mark2"
											changelog="${unitGroup.mark2.name}"
											name="unitGroup-mark2-name"
											title="<fmt:message key= 'biolims.tStorage.unit' />"
											class="form-control"
											value="<s:property value=" storage.unitGroup.mark2.name "/>" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tStorage.saveCondition" /> </span> <input type="text"
											size="50" maxlength="127" id="storage_saveCondition"
											changelog="${storage.saveCondition}" name="saveCondition"
											title="<fmt:message key= 'biolims.tStorage.saveCondition' />"
											class="form-control"
											value="<s:property value=" storage.saveCondition "/>" />

									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tStorage.outPrice" /> </span> <input type="text"
											size="50" maxlength="127" id="storage_outPrice"
											changelog="${storage.outPrice}" name="outPrice"
											title="<fmt:message key= 'biolims.tStorage.outPrice' />"
											class="form-control"
											value="<s:property value=" storage.outPrice "/>" />

									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tStorage.remindLeadTime" /> </span> <input type="text"
											size="50" maxlength="127" id="storage_remindLeadTime"
											changelog="${storage.remindLeadTime}" name="remindLeadTime"
											title="<fmt:message key= 'biolims.tStorage.remindLeadTime' />"
											class="form-control"
											value="<s:property value=" storage.remindLeadTime "/>" />

									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tStorage.safeNum" /> </span> <input type="text"
											size="50" maxlength="127" id="storage_safeNum"
											changelog="${storage.safeNum}" name="safeNum"
											title="<fmt:message key= 'biolims.tStorage.safeNum' />"
											class="form-control"
											value="<s:property value=" storage.safeNum "/>" />

									</div>
								</div>
								<%-- <div class="col-md-4 col-sm-6 col-xs-12">
										<div class="input-group">

											<span class="input-group-addon"><fmt:message key="biolims.tStorage.factStorageNum"/> </span>

											<input type="text" size="50" maxlength="127" id="storage_factStorageNum" changelog="${storage.factStorageNum}" name="factStorageNum" title="<fmt:message key= 'biolims.tStorage.factStorageNum' />" class="form-control" value="<s:property value=" storage.factStorageNum "/>" />

										</div>
									</div> --%>
								<%-- <div class="col-md-4 col-sm-6 col-xs-12">
										<div class="input-group">

											<span class="input-group-addon"><fmt:message key="biolims.tStorage.hasSubmitNum"/> </span>

											<input type="text" size="50" maxlength="127" id="storage_hasSubmitNum" changelog="${storage.hasSubmitNum}" name="hasSubmitNum" title="<fmt:message key= 'biolims.tStorage.hasSubmitNum' />" class="form-control" value="<s:property value=" storage.hasSubmitNum "/>" />

										</div>
									</div> --%>
								<!-- </div>
								<div class="row"> -->
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tStorage.durableDays" /> </span> <input type="text"
											size="50" maxlength="127" id="storage_durableDays"
											changelog="${storage.durableDays}" name="durableDays"
											title="<fmt:message key='biolims.tStorage.durableDays'/>"
											class="form-control"
											value="<s:property value=" storage.durableDays "/>" />

									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tStorage.note" /> </span> <input type="text" size="50"
											maxlength="127" id="storage_note" changelog="${storage.note}"
											name="note" class="form-control"
											value="<s:property value=" storage.note "/>" />

									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tStorage.reactionNum" /> </span> <input type="text"
											size="50" maxlength="127" id="storage_reactionNum"
											changelog="${storage.reactionNum}" name="reactionNum"
											class="form-control"
											value="<s:property value=" storage.reactionNum "/>" />

									</div>
								</div>

								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<!-- 单位组 -->
										<span class="input-group-addon">出库单位组</span> <input
											type="text" size="50" readonly="readonly" maxlength="127"
											id="storage_outUnitGroup_name"
											changelog="${outUnitGroup.name}" name="outUnitGroup-name"
											title="出库单位组" class="form-control"
											value="<s:property value=" storage.outUnitGroup.name "/>" />
										<input type="hidden" size="50" maxlength="127"
											id="storage_outUnitGroup_id" name="outUnitGroup-id"
											title="出库单位组" class="form-control"
											value="<s:property value=" storage.outUnitGroup.id "/>" /> <span
											class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showOutUnitGroup()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
								<!-- 单位 -->
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">出库单位</span> <input type="text"
											size="50" readonly="readonly" maxlength="127"
											id="storage_outUnitGroup_mark2"
											changelog="${outUnitGroup.mark2.name}"
											name="outUnitGroup-mark2-name" title="出库单位"
											class="form-control"
											value="<s:property value=" storage.outUnitGroup.mark2.name "/>" />
									</div>
								</div>

								<%-- <div class="col-xs-4">
										<div class="input-group">

											<span class="input-group-addon">使用单位</span>
											<input type="hidden" id="storage_unit_id" name="unit-id" value="<s:property value="storage.unit.id"/>">
											<input type="text" size="50" maxlength="127" id="storage_unit_name" class="form-control" value="<s:property value="storage.unit.name"/>" />
											<span class="input-group-btn">
												<button class="btn btn-info" type="button" 
													onClick="unitBatch()">
													<i class="glyphicon glyphicon-search"></i>
											</span>
										</div>
									</div> --%>
							</div>
							<br>
							<div class="row">
								<div class="panel"
									style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
									<div class="panel-heading text-left">
										<h3 class="panel-title" style="font-family: 黑体;">
											<i class="glyphicon glyphicon-bookmark"></i>
											<fmt:message key="biolims.common.manufacturerInformation" />

										</h3>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tStorage.producer" /> <img
											class='requiredimage' src='${ctx}/images/required.gif' /> </span> <input
											type="text" size="20" readonly="readOnly"
											id="storage_producer_name"
											changelog="<s:property value=" storage.producer.name "/>"
											value="<s:property value=" storage.producer.name "/>"
											class="form-control" /> <input type="hidden"
											id="storage_producer_id" name="producer-id"
											value="<s:property value=" storage.producer.id "/>">

										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showproducer()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tStorage.dutyUser" /> </span> <input type="text"
											size="20" readonly="readOnly" id="storage_dutyUser_name"
											changelog="<s:property value=" storage.dutyUser.name "/>"
											value="<s:property value=" storage.dutyUser.name "/>"
											class="form-control" /> <input type="hidden"
											id="storage_dutyUser_id" name="dutyUser-id"
											value="<s:property value=" storage.dutyUser.id "/>">

										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showdutyUser()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>

							</div>
							<div class="row">
								<div class="panel"
									style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
									<div class="panel-heading text-left">
										<h3 class="panel-title" style="font-family: 黑体;">
											<i class="glyphicon glyphicon-bookmark"></i>
											<fmt:message key="biolims.common.customFields" />

										</h3>
									</div>
								</div>
							</div>
							<!--
                                	作者：offline
                                	时间：2018-03-19
                                	描述：自定义字段
                                -->
							<div id="fieldItemDiv"></div>

							<br> <input type="hidden" name="storageReagentBuySerialJson"
								id="storageReagentBuySerialJson" value="" /> <input
								type="hidden" name="storageReagentComponentsJson"
								id="storageReagentComponentsJson" value="" /> <input
								type="hidden" id="id_parent_hidden"
								value="<s:property value=" storage.id "/>" /> <input
								type="hidden" id="fieldContent" name="fieldContent"
								value="<s:property value=" storage.fieldContent "/>" /> <input
								type="hidden" id="storage_scopeId" name="scopeId"
								value="<s:property value=" storage.scopeId "/>" /> <input
								type="hidden" id="storage_scopeName" name="scopeName"
								value="<s:property value=" storage.scopeName "/>" />
						</form>
						·
					</div>
					<!--table表格-->
					<div class="HideShowPanel" style="display: none;">
						<table
							class="table table-hover table-striped table-bordered table-condensed"
							id="storageReagentBuySerialTable" style="font-size: 14px;">
						</table>
					</div>
					<div class="HideShowPanel" style="display: none;">
						<table
							class="table table-hover table-striped table-bordered table-condensed"
							id="storageReagentComponentsTable" style="font-size: 14px;">
						</table>
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="button" class="btn btn-primary" id="pre">
							<fmt:message key="biolims.common.back" />
						</button>
						<button type="button" class="btn btn-primary" id="next">
							<fmt:message key="biolims.common.nextStep" />
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/storage/main/storageEditTable.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/storage/main/storageReagentBuySerial.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/storage/main/storageReagentComponents.js"></script>
</body>
</html>