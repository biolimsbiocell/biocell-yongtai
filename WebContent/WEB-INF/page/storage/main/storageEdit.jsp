<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<head>
		<%@ include file="/WEB-INF/page/include/common3.jsp"%>
		<%@ include file="/WEB-INF/page/include/common1.jsp"%>
	</head>
	<s:if test='#request.handlemethod!="view"'>
		<g:LayOutWinTag buttonId="doclinks_img" title='<fmt:message key="biolims.common.attachment"/>' hasHtmlFrame="true" width="900" height="500" html="${ctx}/operfile/initFileList.action\?modelType=storage&id=${storage.id}" isHasSubmit="false" functionName="doc" />
	</s:if>

	<body>
		<%@ include file="/WEB-INF/page/include/newToolBarNoState.jsp"%>
		<script type="text/javascript" src="${ctx}/javascript/storage/main/storageEdit.js"></script>
		<div style="float:left;width:25%;" id="storageTemppage"></div>
		<div id="maintab" style="margin: 0 0 0 0"></div>
		<div id="markup" class="mainclass">
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
			<form name="form1" id="form1" method="post">
				<table class="frame-table">
					<tr>

						<td class="label-title">
							<fmt:message key="biolims.common.rEFnumber" />
						</td>
						<td class="requiredcolumn" nowrap width="10px"><img class='requiredimage' src='${ctx}/images/required.gif' /></td>
						<td align="left">
							<input type="text" size="20" maxlength="25" id="storage_id" name="storage.id" title="<fmt:message key=" biolims.common.serialNumber "/>" value="<s:property value=" storage.id "/>" />

						</td>

						<td class="label-title">
							<fmt:message key="biolims.common.name" />
						</td>
						<td class="requiredcolumn" nowrap width="10px"><img class='requiredimage' src='${ctx}/images/required.gif' /></td>
						<td align="left">
							<input type="text" size="45" maxlength="50" id="storage_name" name="storage.name" title="<fmt:message key=" biolims.common.name "/>" value="<s:property value=" storage.name "/>" />

						</td>

						<g:LayOutWinTag buttonId="showSpec" title='<fmt:message key="biolims.common.selectKitSpecifications"/>' hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action" isHasSubmit="false" functionName="kit" hasSetFun="true" documentId="storage_spec" documentName="storage_spec_name" />

						<td class="label-title">
							<fmt:message key="biolims.common.kitSpecifications" />
						</td>
						<td class="requiredcolumn" nowrap width="10px"><img class='requiredimage' src='${ctx}/images/required.gif' /></td>
						<td align="left">
							<input type="text" size="30" readonly="readOnly" id="storage_spec_name" value="<s:property value=" storage.spec.name "/>" readonly="readOnly" />
							<input type="hidden" id="storage_spec" name="storage.spec.id" value="<s:property value=" storage.spec.id "/>">
							<img alt='<fmt:message key="biolims.common.selectKitSpecifications"/>' id='showSpec' src='${ctx}/images/img_lookup.gif' class='detail' />
						</td>
					</tr>
					<tr>
						<td class="label-title">
							<fmt:message key="biolims.common.shouldHaveTheNumber" />
						</td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left">
							<input type="text" size="20" maxlength="25" id="storage_properNum" name="storage.properNum" title="<fmt:message key=" biolims.common.shouldHaveTheNumber "/>" value="<s:property value=" storage.properNum "/>" />

						</td>

						<td class="label-title">
							<fmt:message key="biolims.common.actualInventory" />
						</td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left">
							<input type="text" size="20" maxlength="25" id="storage_factNum" name="storage.factNum" title="<fmt:message key=" biolims.common.actualInventory "/>" value="<s:property value=" storage.factNum "/>" />

						</td>

						<td class="label-title">
							<fmt:message key="biolims.common.borrowLend" />
						</td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left">
							<input type="text" size="20" maxlength="25" id="storage_inOrOut" name="storage.inOrOut" title="<fmt:message key=" biolims.common.borrowLend "/>" value="<s:property value=" storage.inOrOut "/>" />

						</td>
					</tr>
					<tr>
						<td class="label-title">
							<fmt:message key="biolims.common.canBeCompensationAmount" />
						</td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left">
							<input type="text" size="20" maxlength="25" id="storage_payNum" name="storage.payNum" title="<fmt:message key=" biolims.common.canBeCompensationAmount "/>" value="<s:property value=" storage.payNum "/>" />

						</td>

						<td class="label-title">
							<fmt:message key="biolims.common.givenTheNumber" />
						</td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left">
							<input type="text" size="20" maxlength="25" id="storage_giveNum" name="storage.giveNum" title="<fmt:message key=" biolims.common.givenTheNumber "/>" value="<s:property value=" storage.giveNum "/>" />

						</td>

						<td class="label-title">LOT</td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left">
							<input type="text" size="20" maxlength="25" id="storage_lot" name="storage.lot" title="LOT" value="<s:property value=" storage.lot "/>" />

						</td>

					</tr>
					<tr>
						<td class="label-title">
							<fmt:message key="biolims.common.commandPerson" />
						</td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left">
							<input type="text" size="20" readonly="readOnly" id="storage_createUser_name" value="<s:property value=" storage.createUser.name "/>" class="text input readonlytrue" readonly="readOnly" />
							<input type="hidden" id="storage_createUser" name="storage.createUser.id" value="<s:property value=" storage.createUser.id "/>">
						</td>

						<td class="label-title">
							<fmt:message key="biolims.common.commandTime" />
						</td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left">
							<input type="text" size="20" maxlength="25" id="storage_createDate" name="storage.createDate" title="<fmt:message key=" biolims.common.commandTime "/>" readonly="readOnly" class="text input readonlytrue" value="<s:date name=" storage.createDate " format="yyyy-MM-dd "/>" />

						</td>

						<td class="label-title" style="display:none">
							<fmt:message key="biolims.common.stateID" />
						</td>
						<td class="requiredcolumn" nowrap width="10px" style="display:none"></td>
						<td align="left" style="display:none">
							<input type="text" size="20" maxlength="25" id="storage_state" name="storage.state" title="<fmt:message key=" biolims.common.stateID "/>" readonly="readOnly" class="text input readonlytrue" value="<s:property value=" storage.state "/>"style="display:none" />

						</td>
						<td class="label-title">
							<fmt:message key="biolims.common.state" />
						</td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left">
							<select name="storage.stateName" id="pooling_state">
								<option value="1" <s:if test="storage.stateName == 1">selected="selected"</s:if>>
									<fmt:message key="biolims.common.effective" />
								</option>
								<option value="0" <s:if test="storage.stateName == 0">selected="selected"</s:if>>
									<fmt:message key="biolims.common.invalid" />
								</option>
							</select>

						</td>
					</tr>
					<tr>
						<td class="label-title">
							<fmt:message key="biolims.common.attachment" />
						</td>
						<td></td>
						<td title="<fmt:message key=" biolims.common.afterSaveTheBasicCanMaintainReviewImages "/>" id="doclinks_img"><span class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message key="biolims.common.attachment"/></span>
					</tr>

				</table>
			</form>
		</div>
	</body>

</html>