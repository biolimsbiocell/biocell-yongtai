<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><fmt:message key="biolims.common.purchasingAgent"/></title>
</head>

<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript"
	src="${ctx}/javascript/storage/main/storageReagent.js"></script>
<body>
	<g:HandleDataExtTag hasConfirm="false"
		paramsValue="storageId:'${storageId}'" funName="modifyData"
		url="${ctx}/storage/addStorageReagent.action" />
	<g:HandleDataExtTag funName="delData"
		url="${ctx}/storage/delStorageReagentBuySerial.action" />
	<table width="100%">
		<tr>
			<td><input type="hidden" id="storageId" value="${storageId}" />
				<input type="hidden" id="id" value="" />
				<div id='storageReagentdiv' style='width: 100%;' ></div>
</td>
		</tr>
	</table>
</body>
</html>