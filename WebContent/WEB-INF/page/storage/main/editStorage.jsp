<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<%@ include file="/WEB-INF/page/include/common3.jsp"%>
	<%@ include file="/WEB-INF/page/include/common1.jsp"%>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>
			<fmt:message key="biolims.common.inventoryMasterData" />
		</title>
		<script type="text/javascript" src="WEB-INF/page/js/jquery-1.7.2.js"></script>
		<style type="text/css">
			.tri {
				display: none;
			}
		</style>
		<script type="text/javascript">
			$(function() {
				if($("#storage_state_name").val() == "") {
					$("#storage_state_name").val(biolims.common.effective);
					$("#storage_state_id").val("1s");
				}
				$("#storage_type_name").val(biolims.common.haveABatch);
				$("#storage_type_id").val("12");
				if($("#p_type").val() == 1 && $("#storage_studyType_name").val() == "") {
					$("#storage_studyType_name").val(biolims.common.reagentName);
					$("#storage_studyType_id").val("reagent");
				}
				if($("#p_type").val() == 2 && $("#storage_studyType_name").val() == "") {
					$("#storage_studyType_name").val(biolims.master.materialDetail);
					$("#storage_studyType_id").val("materiel");
				}
			});
		</script>
	</head>
	<s:if test='#request.handlemethod!="view"'>
		<%-- 	<g:LayOutWinTag buttonId="showDicYjly" title="选择类型" --%>
		<%-- 		hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action" --%>
		<%-- 		isHasSubmit="false" functionName="yjlx" hasSetFun="true" --%>
		<%-- 		documentId="storage_studyType_id" --%>
		<%-- 		documentName="storage_studyType_name" /> --%>
		<script language="javascript">
			Ext.onReady(function() {
				Ext.QuickTips.init();
				Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
				var showDicState = Ext.get('showDicState');
				showDicState.on('click',
					function showDicStateFun() {
						var win = Ext.getCmp('showDicStateFun');
						if(win) {
							win.close();
						}
						var showDicStateFun = new Ext.Window({
							id: 'showDicStateFun',
							modal: true,
							title: '<fmt:message key="biolims.common.selectTheState"/>',
							layout: 'fit',
							width: 500,
							height: 500,
							closeAction: 'close',
							plain: true,
							bodyStyle: 'padding:5px;',
							buttonAlign: 'center',
							collapsible: true,
							maximizable: true,
							items: new Ext.BoxComponent({
								id: 'maincontent',
								region: 'center',
								html: "<iframe scrolling='no' name='maincontentframe' src='/dic/state/stateSelect.action?type=storage&flag=showDicStateFun' frameborder='0' width='100%' height='100%' ></iframe>"
							}),
							buttons: [{
								text: biolims.common.close,
								handler: function() {
									showDicStateFun.close();
								}
							}]
						});
						showDicStateFun.show();
					}
				);
			});

			function setshowDicStateFun(id, name) {
				document.getElementById("storage_state_id").value = id;
				document.getElementById("storage_state_name").value = name;
				var win = Ext.getCmp('showDicStateFun')
				if(win) {
					win.close();
				}
			}
		</script>

		<script language="javascript">
			Ext.onReady(function() {
				Ext.QuickTips.init();
				Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
				var dutyNameBtn = Ext.get('dutyNameBtn');
				dutyNameBtn.on('click',
					function dutyUserFun() {
						var win = Ext.getCmp('dutyUserFun');
						if(win) {
							win.close();
						}
						var dutyUserFun = new Ext.Window({
							id: 'dutyUserFun',
							modal: true,
							title: '<fmt:message key="biolims.common.selectLeader"/>',
							layout: 'fit',
							width: document.body.clientWidth / 1.5,
							height: 500,
							closeAction: 'close',
							plain: true,
							bodyStyle: 'padding:5px;',
							buttonAlign: 'center',
							collapsible: true,
							maximizable: true,
							items: new Ext.BoxComponent({
								id: 'maincontent',
								region: 'center',
								html: "<iframe scrolling='no' name='maincontentframe' src='/core/user/userSelect.action?flag=dutyUserFun' frameborder='0' width='100%' height='100%' ></iframe>"
							}),
							buttons: [{
								text: biolims.common.close,
								handler: function() {
									dutyUserFun.close();
								}
							}]
						});
						dutyUserFun.show();
					}
				);
			});

			function setdutyUserFun(id, name) {
				document.getElementById("storage.dutyUser.id").value = id;
				document.getElementById("storage.dutyUser.name").value = name;
				var win = Ext.getCmp('dutyUserFun')
				if(win) {
					win.close();
				}
			}
		</script>

		<script language="javascript">
			Ext.onReady(function() {
				Ext.QuickTips.init();
				Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
				var sourceS = Ext.get('sourceS');
				sourceS.on('click',
					function source() {
						var win = Ext.getCmp('source');
						if(win) {
							win.close();
						}
						var source = new Ext.Window({
							id: 'source',
							modal: true,
							title: '<fmt:message key="biolims.common.selectSource"/>',
							layout: 'fit',
							width: 500,
							height: 500,
							closeAction: 'close',
							plain: true,
							bodyStyle: 'padding:5px;',
							buttonAlign: 'center',
							collapsible: true,
							maximizable: true,
							items: new Ext.BoxComponent({
								id: 'maincontent',
								region: 'center',
								html: "<iframe scrolling='no' name='maincontentframe' src='/dic/type/dicTypeSelect.action?flag=source' frameborder='0' width='100%' height='100%' ></iframe>"
							}),
							buttons: [{
								text: biolims.common.close,
								handler: function() {
									source.close();
								}
							}]
						});
						source.show();
					}
				);
			});

			function setsource(id, name) {
				document.getElementById("storage.source.id").value = id;
				document.getElementById("storage.source.name").value = name;
				var win = Ext.getCmp('source')
				if(win) {
					win.close();
				}
			}
		</script>

		<script language="javascript">
			Ext.onReady(function() {
				Ext.QuickTips.init();
				Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
				var selectKit = Ext.get('selectKit');
				selectKit.on('click',
					function kit() {
						var win = Ext.getCmp('kit');
						if(win) {
							win.close();
						}
						var kit = new Ext.Window({
							id: 'kit',
							modal: true,
							title: '<fmt:message key="biolims.common.selectKitSpecifications"/>',
							layout: 'fit',
							width: 500,
							height: 500,
							closeAction: 'close',
							plain: true,
							bodyStyle: 'padding:5px;',
							buttonAlign: 'center',
							collapsible: true,
							maximizable: true,
							items: new Ext.BoxComponent({
								id: 'maincontent',
								region: 'center',
								html: "<iframe scrolling='no' name='maincontentframe' src='/dic/type/dicTypeSelect.action?flag=kit' frameborder='0' width='100%' height='100%' ></iframe>"
							}),
							buttons: [{
								text: biolims.common.close,
								handler: function() {
									kit.close();
								}
							}]
						});
						kit.show();
					}
				);
			});

			function setkit(id, name) {
				document.getElementById("storage_kit_id").value = id;
				document.getElementById("storage_kit_name").value = name;
				var win = Ext.getCmp('kit')
				if(win) {
					win.close();
				}
			}
		</script>

		<script language="javascript">
			Ext.onReady(function() {
				Ext.QuickTips.init();
				Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
				var dicStorageType = Ext.get('dicStorageType');
				dicStorageType.on('click',
					function dicStorageTypeFun() {
						var win = Ext.getCmp('dicStorageTypeFun');
						if(win) {
							win.close();
						}
						var dicStorageTypeFun = new Ext.Window({
							id: 'dicStorageTypeFun',
							modal: true,
							title: '<fmt:message key="biolims.common.chooseClassification"/>',
							layout: 'fit',
							width: 500,
							height: 500,
							closeAction: 'close',
							plain: true,
							bodyStyle: 'padding:5px;',
							buttonAlign: 'center',
							collapsible: true,
							maximizable: true,
							items: new Ext.BoxComponent({
								id: 'maincontent',
								region: 'center',
								html: "<iframe scrolling='no' name='maincontentframe' src='/storage/common/storageTypeSelect.action?type=1&flag=dicStorageTypeFun' frameborder='0' width='100%' height='100%' ></iframe>"
							}),
							buttons: [{
								text: biolims.common.close,
								handler: function() {
									dicStorageTypeFun.close();
								}
							}]
						});
						dicStorageTypeFun.show();
					}
				);
			});

			function setdicStorageTypeFun(id, name) {
				document.getElementById("storage_type_id").value = id;
				document.getElementById("storage_type_name").value = name;
				var win = Ext.getCmp('dicStorageTypeFun')
				if(win) {
					win.close();
				}
			}
		</script>

		<script language="javascript">
			Ext.onReady(function() {
				Ext.QuickTips.init();
				Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
				var dicUnitBtn = Ext.get('dicUnitBtn');
				dicUnitBtn.on('click',
					function weight() {
						var win = Ext.getCmp('weight');
						if(win) {
							win.close();
						}
						var weight = new Ext.Window({
							id: 'weight',
							modal: true,
							title: '<fmt:message key="biolims.common.selectTheOrganization"/>',
							layout: 'fit',
							width: 500,
							height: 500,
							closeAction: 'close',
							plain: true,
							bodyStyle: 'padding:5px;',
							buttonAlign: 'center',
							collapsible: true,
							maximizable: true,
							items: new Ext.BoxComponent({
								id: 'maincontent',
								region: 'center',
								html: "<iframe scrolling='no' name='maincontentframe' src='/dic/unit/dicUnitSelect.action?flag=weight' frameborder='0' width='100%' height='100%' ></iframe>"
							}),
							buttons: [{
								text: biolims.common.close,
								handler: function() {
									weight.close();
								}
							}]
						});
						weight.show();
					}
				);
			});

			function setweight(id, name) {
				document.getElementById("storage_unit_id").value = id;
				document.getElementById("storage_unit_name").value = name;
				var win = Ext.getCmp('weight')
				if(win) {
					win.close();
				}
			}
		</script>

		<script language="javascript">
			Ext.onReady(function() {
				Ext.QuickTips.init();
				Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
				var showStorConBtn = Ext.get('showStorConBtn');
				showStorConBtn.on('click',
					function storageConditionsType() {
						var win = Ext.getCmp('storageConditionsType');
						if(win) {
							win.close();
						}
						var storageConditionsType = new Ext.Window({
							id: 'storageConditionsType',
							modal: true,
							title: '<fmt:message key="biolims.common.chooseTheStorageConditions"/>',
							layout: 'fit',
							width: 500,
							height: 500,
							closeAction: 'close',
							plain: true,
							bodyStyle: 'padding:5px;',
							buttonAlign: 'center',
							collapsible: true,
							maximizable: true,
							items: new Ext.BoxComponent({
								id: 'maincontent',
								region: 'center',
								html: "<iframe scrolling='no' name='maincontentframe' src='/dic/type/dicTypeSelect.action?flag=storageConditionsType' frameborder='0' width='100%' height='100%' ></iframe>"
							}),
							buttons: [{
								text: biolims.common.close,
								handler: function() {
									storageConditionsType.close();
								}
							}]
						});
						storageConditionsType.show();
					}
				);
			});

			function setstorageConditionsType(id, name) {
				document.getElementById("storage_saveCondition").value = id;
				document.getElementById("storage_saveCondition").value = name;
				var win = Ext.getCmp('storageConditionsType')
				if(win) {
					win.close();
				}
			}
		</script>

		<script language="javascript">
			Ext
				.onReady(function() {
					Ext.QuickTips.init();
					Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
					var wzBtn = Ext.get('wzBtn');
					wzBtn
						.on(
							'click',
							function showStoragePosition() {
								var win = Ext
									.getCmp('showStoragePosition');
								if(win) {
									win.close();
								}
								var showStoragePosition = new Ext.Window({
									id: 'showStoragePosition',
									modal: true,
									title: '<fmt:message key="biolims.common.selectLocation"/>',
									layout: 'fit',
									width: 500,
									height: 500,
									closeAction: 'close',
									plain: true,
									bodyStyle: 'padding:5px;',
									buttonAlign: 'center',
									collapsible: true,
									maximizable: true,
									items: new Ext.BoxComponent({
										id: 'maincontent',
										region: 'center',
										html: "<iframe scrolling='no' name='maincontentframe' src='" +
											ctx +
											"/storage/position/showStoragePositionTreeDialog2.action?setStoragePostion=true&flag=showStoragePosition' frameborder='0' width='100%' height='100%' ></iframe>"
									}),
									buttons: [{
										text: biolims.common.close,
										handler: function() {
											showStoragePosition
												.close();
										}
									}]
								});
								showStoragePosition.show();
							});
				});

			function setshowStoragePosition(id) {
				document.getElementById("storage.position.id").value = id;
				document.getElementById("storage.position.name").value = id;
				var win = Ext.getCmp('showStoragePosition');
				if(win) {
					win.close();
				}
			}
		</script>
		<script language="javascript">
			Ext.onReady(function() {
				Ext.QuickTips.init();
				Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
				var cccsBtn = Ext.get('cccsBtn');
				cccsBtn.on('click',
					function customer() {
						var win = Ext.getCmp('customer');
						if(win) {
							win.close();
						}
						var customer = new Ext.Window({
							id: 'customer',
							modal: true,
							title: '<fmt:message key="biolims.common.manufacturers"/>',
							layout: 'fit',
							width: 900,
							height: 500,
							closeAction: 'close',
							plain: true,
							bodyStyle: 'padding:5px;',
							buttonAlign: 'center',
							collapsible: true,
							maximizable: true,
							items: new Ext.BoxComponent({
								id: 'maincontent',
								region: 'center',
								html: "<iframe scrolling='no' name='maincontentframe' src='/supplier/common/supplierSelect.action?flag=customer' frameborder='0' width='100%' height='100%' ></iframe>"
							}),
							buttons: [{
								text: biolims.common.close,
								handler: function() {
									customer.close();
								}
							}]
						});
						customer.show();
					}
				);
			});

			function setcustomer(rec) {
				document.getElementById('storage.producer.id').value = rec.get('id');
				document.getElementById

				('storage.producer.name').value = rec.get('name');
				document.getElementById('ccsPhone').value = rec.get('linkTel');
				document.getElementById

				('linkMan').value = rec.get('linkMan');
				document.getElementById('fax').value = rec.get('fax');
				var win = Ext.getCmp('customer')
				if(win) {
					win.close();
				}
			}
		</script>

	</s:if>
	<script language="javascript">
		Ext.onReady(function() {
			Ext.QuickTips.init();
			Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
			var doclinks_label = Ext.get('doclinks_label');
			doclinks_label.on('click',
				function doc() {
					var win = Ext.getCmp('doc');
					if(win) {
						win.close();
					}
					var doc = new Ext.Window({
						id: 'doc',
						modal: true,
						title: '<fmt:message key="biolims.common.attachment"/>',
						layout: 'fit',
						width: 900,
						height: 500,
						closeAction: 'close',
						plain: true,
						bodyStyle: 'padding:5px;',
						buttonAlign: 'center',
						collapsible: true,
						maximizable: true,
						items: new Ext.BoxComponent({
							id: 'maincontent',
							region: 'center',
							html: "<iframe scrolling='no' name='maincontentframe' src='/operfile/initFileList.action?modelType=storage&id=NEW&flag=doc' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
						buttons: [{
							text: biolims.common.close,
							handler: function() {
								doc.close();
							}
						}]
					});
					doc.show();
				}
			);
		});
	</script>

	<script language="javascript">
		Ext.onReady(function() {
			Ext.QuickTips.init();
			Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
			var imglinks_img = Ext.get('imglinks_img');
			imglinks_img.on('click',
				function pic() {
					var win = Ext.getCmp('pic');
					if(win) {
						win.close();
					}
					var pic = new Ext.Window({
						id: 'pic',
						modal: true,
						title: '<fmt:message key="biolims.common.picture"/>',
						layout: 'fit',
						width: 900,
						height: 500,
						closeAction: 'close',
						plain: true,
						bodyStyle: 'padding:5px;',
						buttonAlign: 'center',
						collapsible: true,
						maximizable: true,
						items: new Ext.BoxComponent({
							id: 'maincontent',
							region: 'center',
							html: "<iframe scrolling='no' name='maincontentframe' src='/operfile/initFileList.action?modelType=storage&id=NEW&flag=pic' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
						buttons: [{
							text: biolims.common.close,
							handler: function() {
								pic.close();
							}
						}]
					});
					pic.show();
				}
			);
		});
	</script>

	<script language="javascript">
		Ext.onReady(function() {
			Ext.QuickTips.init();
			Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
			var positionDetail = Ext.get('positionDetail');
			positionDetail.on('click',
				function positionDetailFun() {
					var win = Ext.getCmp('positionDetailFun');
					if(win) {
						win.close();
					}
					var positionDetailFun = new Ext.Window({
						id: 'positionDetailFun',
						modal: true,
						title: '<fmt:message key="biolims.common.detailed"/>',
						layout: 'fit',
						width: 500,
						height: 500,
						closeAction: 'close',
						plain: true,
						bodyStyle: 'padding:5px;',
						buttonAlign: 'center',
						collapsible: true,
						maximizable: true,
						items: new Ext.form.TextArea({
							id: 'positionDetail1',
							value: document.getElementById('storage.position.name').value
						}),
						buttons: [{
							text: biolims.common.close,
							handler: function() {
								positionDetailFun.close();
							}
						}]
					});
					positionDetailFun.show();
				}
			);
		});
	</script>

	<script language="javascript">
		Ext.onReady(function() {
			Ext.QuickTips.init();
			Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
			var showUse = Ext.get('showUse');
			showUse.on('click',
				function showUseFun() {
					var win = Ext.getCmp('showUseFun');
					if(win) {
						win.close();
					}
					var showUseFun = new Ext.Window({
						id: 'showUseFun',
						modal: true,
						title: '<fmt:message key="biolims.common.detailed"/>',
						layout: 'fit',
						width: 500,
						height: 500,
						closeAction: 'close',
						plain: true,
						bodyStyle: 'padding:5px;',
						buttonAlign: 'center',
						collapsible: true,
						maximizable: true,
						items: new Ext.form.TextArea({
							id: 'useText',
							value: document.getElementById('storage.useDesc').value
						}),
						buttons: [{
								text: biolims.common.selected,
								handler: function() {
									var wfNodeName = Ext.getCmp("useText").getValue();
									document.getElementById("storage.useDesc").value = wfNodeName;
									showUseFun.close();
								}
							},
							{
								text: biolims.common.close,
								handler: function() {
									showUseFun.close();
								}
							}
						]
					});
					showUseFun.show();
				}
			);
		});
	</script>

	<script language="javascript">
		Ext.onReady(function() {
			Ext.QuickTips.init();
			Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
			var englishname = Ext.get('englishname');
			englishname.on('click',
				function englishnameFun() {
					var win = Ext.getCmp('englishnameFun');
					if(win) {
						win.close();
					}
					var englishnameFun = new Ext.Window({
						id: 'englishnameFun',
						modal: true,
						title: '<fmt:message key="biolims.common.detailed"/>',
						layout: 'fit',
						width: 500,
						height: 500,
						closeAction: 'close',
						plain: true,
						bodyStyle: 'padding:5px;',
						buttonAlign: 'center',
						collapsible: true,
						maximizable: true,
						items: new Ext.form.TextArea({
							id: 'englishname1',
							value: document.getElementById('storage.engName').value
						}),
						buttons: [{
								text: biolims.common.selected,
								handler: function() {
									var wfNodeName = Ext.getCmp("englishname1").getValue();
									document.getElementById("storage.engName").value = wfNodeName;
									englishnameFun.close();
								}
							},
							{
								text: biolims.common.close,
								handler: function() {
									englishnameFun.close();
								}
							}
						]
					});
					englishnameFun.show();
				}
			);
		});
	</script>

	<script language="javascript">
		Ext.onReady(function() {
			Ext.QuickTips.init();
			Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
			var storageNote = Ext.get('storageNote');
			storageNote.on('click',
				function storageNoteFun() {
					var win = Ext.getCmp('storageNoteFun');
					if(win) {
						win.close();
					}
					var storageNoteFun = new Ext.Window({
						id: 'storageNoteFun',
						modal: true,
						title: '<fmt:message key="biolims.common.detailed"/>',
						layout: 'fit',
						width: 500,
						height: 500,
						closeAction: 'close',
						plain: true,
						bodyStyle: 'padding:5px;',
						buttonAlign: 'center',
						collapsible: true,
						maximizable: true,
						items: new Ext.form.TextArea({
							id: 'storageNote1',
							value: document.getElementById('storage.note').value
						}),
						buttons: [{
								text: biolims.common.selected,
								handler: function() {
									document.getElementById('storage.note').value = document.getElementById('storageNote1').value;
									Ext.getCmp('storageNoteFun').close();
								}
							},
							{
								text: biolims.common.close,
								handler: function() {
									storageNoteFun.close();
								}
							}
						]
					});
					storageNoteFun.show();
				}
			);
		});
	</script>

	<script language="javascript">
		Ext.onReady(function() {
			Ext.QuickTips.init();
			Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
			var showCurrencyType = Ext.get('showCurrencyType');
			showCurrencyType.on('click',
				function bz() {
					var win = Ext.getCmp('bz');
					if(win) {
						win.close();
					}
					var bz = new Ext.Window({
						id: 'bz',
						modal: true,
						title: '<fmt:message key="biolims.common.selectTheCurrency"/>',
						layout: 'fit',
						width: 500,
						height: 500,
						closeAction: 'close',
						plain: true,
						bodyStyle: 'padding:5px;',
						buttonAlign: 'center',
						collapsible: true,
						maximizable: true,
						items: new Ext.BoxComponent({
							id: 'maincontent',
							region: 'center',
							html: "<iframe scrolling='no' name='maincontentframe' src='/dic/currency/currencyTypeSelect.action?flag=bz' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
						buttons: [{
							text: biolims.common.close,
							handler: function() {
								bz.close();
							}
						}]
					});
					bz.show();
				}
			);
		});

		function setbz(id, name) {
			document.getElementById("storage.currencyType.id").value = id;
			document.getElementById("storage.currencyType.name").value = name;
			var win = Ext.getCmp('bz')
			if(win) {
				win.close();
			}
		}
	</script>

	<body>
		<input type="hidden" id="p_type" value="${requestScope.p_type}" />
		<%@ include file="/WEB-INF/page/include/toolbarNoTab.jsp"%>
		<script type="text/javascript" src="${ctx}/javascript/storage/main/editStorage.js"></script>
		<div id="maintab" style="margin: 0 0 0 0"></div>
		<div id="markup" class="mainclass">
			<s:form name="form1" theme="simple">
				<table width="100%" cellpadding="0" cellspacing="0">
					<tbody>
						<tr>
							<td>
								<table width="100%" class="section_table" cellpadding="0" cellspacing="0">
									<tbody>
										<tr class="control textboxcontrol">
											<td valign="top" class="controlTitle" nowrap><span class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key=" biolims.common.materialCode "/>" class="text label"><fmt:message
													key="biolims.common.serialNumber" /></label></td>
											<td class="requiredcolumn" nowrap width="10px"><img class='requiredimage' src='${ctx}/images/required.gif' /></td>
											<td nowrap><input type="text" class="input_parts text input default  readonlytrue true" name="storage.id" id="storage_id" readonly="readonly" value='<s:property value="storage.id"/>' title='<fmt:message key="biolims.common.materialCode"/>' onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="20" maxlength="32">
												<img alt='' src='${ctx}/images/blank.gif' class='detail' style='display: none' /></td>

											<td nowrap>&nbsp;</td>
										</tr>
										<tr class="control textboxcontrol">
											<td valign="top" align="right" class="controlTitle" nowrap>
												<span class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key=" biolims.common.category "/>" class="text label"><fmt:message
													key="biolims.common.category" /> </label>
											</td>
											<td class="requiredcolumn" nowrap width="10px">
											</td>
											<td nowrap><input type="text" class="input_parts text input default  readonlyfalse false" readonly="readOnly" name="storage.studyType.name" id="storage_studyType_name" value='<s:property value="storage.studyType.name"/>' title="Index " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="20" maxlength="30">
												<input type=hidden id="storage_studyType_id" name="storage.studyType.id" value='<s:property value="storage.studyType.id"/>'> <img alt='<fmt:message key="biolims.common.select"/>' id='showDicYjly' src='${ctx}/images/img_lookup.gif' class='detail' /></td>
											<td nowrap>&nbsp;</td>
										</tr>
										<%-- <tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="来源" class="text label"> 来源 </label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><input type="text"
											class="input_parts text input default  readonlyfalse false"
											name="" id="storage.source.name"
											value='<s:property value="storage.source.name"/>' title="来源 "
											readonly="readOnly" onblur="textbox_ondeactivate(event);"
											onfocus="shared_onfocus(event,this)" size="10" maxlength="30">
											<input type="hidden" name="storage.source.id"
											id="storage.source.id"
											value='<s:property value="storage.source.id"/>'> <img
											alt='选择' id='sourceS' src='${ctx}/images/img_lookup.gif'
											class='detail' /></td>
										<td nowrap>&nbsp;</td>
									</tr> --%>
										<tr></tr>
										<tr></tr>
										<tr class="control textboxcontrol">
											<td valign="top" align="right" class="controlTitle" nowrap>
												<span class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key=" biolims.common.commandPerson "/>" class="text label"><fmt:message
													key="biolims.common.commandPerson" /> </label>
											</td>

											<td class="requiredcolumn" nowrap width="10px">
											</td>
											<td nowrap><input type="text" onblur="textbox_ondeactivate(event);" readonly="readOnly" onfocus="shared_onfocus(event,this)" class="input_parts text input default  readonlytrue false" name="" onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" id="main_grid1_row1_col1_sec1_2_textbox" value='<s:property value="storage.createUser.name"/>' readonly="readOnly" title='<fmt:message key="biolims.common.commandPerson"/>' size="10" maxlength="30"> <input type="hidden" name="storage.createUser.id" value='<s:property value="storage.createUser.id"/>'>
											</td>
											<td nowrap>&nbsp;</td>
										</tr>

										<%-- 	<tr class="control textboxcontrol" id="tri1">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="<fmt:message key="biolims.common.iFsequence"/>"
											class="text label"><fmt:message
													key="biolims.common.iFsequence" /></label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><input type="text"
											class="input_parts text input default readonlyfalse false"
											name="storage.i5Index" id="storage.i5Index"
											value='<s:property value="storage.i5Index"/>'
											title='<fmt:message key="biolims.common.iFsequence"/>'
											onblur="textbox_ondeactivate(event);"
											onfocus="shared_onfocus(event,this)" size="20" maxlength="30">
											<img alt='' src='${ctx}/images/blank.gif' class='detail'
											style='display: none' /></td>
										<td nowrap>&nbsp;</td>
									</tr> --%>
										<tr class="control textboxcontrol">
											<td valign="top" align="right" class="controlTitle" nowrap>
												<span class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key=" biolims.common.picture "/>" class="text label"><fmt:message
													key="biolims.common.picture" /> </label>
											</td>
											<td class="requiredcolumn" nowrap width="10px">
											</td>
											<td align="left">
												<div id="imglinks_img" style="width: 100px;">
													<img alt="<fmt:message key=" biolims.common.afterSaveTheBasicCanMaintainReviewImages "/>" src='${ctx}/images/img_lookup.gif' class='detail' />
												</div>
											</td>
											<td nowrap>&nbsp;</td>
										</tr>
										<tr class="control textboxcontrol">
											<td valign="top" align="right" class="controlTitle" nowrap>
												<span class="labelspacer">&nbsp;&nbsp;</span> <label title="jdeCode" class="text label">JDE Code</label>
											</td>
											<td class="requiredcolumn" nowrap width="10px">
											</td>
											<td nowrap><input type="text" class="input_parts text input default readonlyfalse false" name="storage.jdeCode" id="storage.jdeCode" value='<s:property value="storage.jdeCode"/>' title="jdeCode " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="20" maxlength="30">
												<img alt='' src='${ctx}/images/blank.gif' class='detail' style='display: none' /></td>
											<td nowrap>&nbsp;</td>
										</tr>
										<tr id="doclinks">
											<td valign="top" align="right" class="controlTitle" nowrap>
												<span class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key=" biolims.common.attachment "/>" class="text label"><fmt:message
													key="biolims.common.attachment" /></label>&nbsp;
											</td>
											<td class="requiredcolumn" nowrap width="10px">
											</td>
											<td align="left">
												<div id="doclinks_label" style="width: 100px;">
													<img alt="<fmt:message key=" biolims.common.afterSaveCanLookAtTheAttachment "/>" class="detail" src="${ctx}/images/img_attach.gif" />
												</div>
											</td>
											<td nowrap>&nbsp;</td>
											<td nowrap></td>
											<td nowrap></td>
										</tr>
									</tbody>
								</table>
							</td>
							<td class="sectioncol" width='33%' valign="top" align="right">
								<div class="section standard_section">
									<div class="section">
										<table width="100%" class="section_table" cellpadding="0" cellspacing="0">
											<tbody>
												<tr class="control textboxcontrol">
													<td valign="top" align="right" class="controlTitle" nowrap>
														<span class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key=" biolims.common.consumableName "/>" class="text label"><fmt:message key="biolims.common.groupName"/></label>
													</td>
													<td class="requiredcolumn" nowrap width="10px">
													</td>
													<td nowrap><input type="text" class="input_parts text input default readonlyfalse false" name="storage.name" id="storage.name" value='<s:property value="storage.name"/>' title='<fmt:message key="biolims.common.consumableName"/>' onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="40" maxlength="55"> <img alt='' src='${ctx}/images/blank.gif' class='detail' style='display: none' /></td>
													<td nowrap>&nbsp;</td>
												</tr>
												<tr style="display: none" class="control textboxcontrol">
													<td valign="top" align="right" class="controlTitle" nowrap>
														<span class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key=" biolims.common.classification "/>" class="text label"><fmt:message
															key="biolims.common.classification" /> </label>
													</td>
													<td class="requiredcolumn" nowrap width="10px">
													</td>
													<td nowrap><input type="text" class="input_parts text input default readonlytrue false" name="" id="" onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" value="<fmt:message key=" biolims.common.consumables "/>" readonly="readOnly" title="<fmt:message key=" biolims.common.classification "/>  " size="20" maxlength="30">
														<s:hidden name="storage.mainType.id" value="1"></s:hidden> <img alt='' src='${ctx}/images/blank.gif' class='detail' style='display: none' /></td>
													<td nowrap>&nbsp;</td>
												</tr>
												<%-- <tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="<fmt:message key="biolims.common.use"/>"
													class="text label"><fmt:message
															key="biolims.common.use" /> </label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
												</td>
												<td align="left" nowrap><input type="text"
													class="input_parts text input default readonlyfalse false"
													name="storage.useDesc" id="storage.useDesc"
													value='<s:property value="storage.useDesc"/>'
													title="<fmt:message key="biolims.common.use"/> "
													onblur="textbox_ondeactivate(event);"
													onfocus="shared_onfocus(event,this)" size="40"
													maxlength="55"> <img
													alt='<fmt:message key="biolims.common.detailed"/>'
													id='showUse'
													src='${ctx}/images/img_longdescription_off.gif'
													name='showUse' class='detail' /></td>
												<td nowrap>&nbsp;</td>
											</tr> --%>
												<tr class="control textboxcontrol" id="tri3">
													<td valign="top" align="right" class="controlTitle" nowrap>
														<span class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key=" biolims.common.kitName "/>" class="text label"><fmt:message
															key="biolims.common.kitName" /> </label>
													</td>
													<td class="requiredcolumn" nowrap width="10px">
													</td>
													<td nowrap><input type="text" class="input_parts text input default  readonlyfalse false" name="storage.kit.name" id="storage_kit_name" value='<s:property value="storage.kit.name"/>' title="<fmt:message key=" biolims.common.kitName "/>" onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="40" maxlength="55"><img alt='<fmt:message key="biolims.common.select"/>' id='selectKit' src='${ctx}/images/img_lookup.gif' class='detail' /> <input type="hidden" name="storage.kit.id" id="storage_kit_id" value="<s:property value=" storage.kit.id "/>" /><img alt='' src='${ctx}/images/blank.gif' class='detail' style='display: none' /></td>
													<td nowrap>&nbsp;</td>
												</tr>
												<tr class="control textboxcontrol">
													<td valign="top" align="right" class="controlTitle" nowrap>
														<span class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key=" biolims.common.specifications1 "/>" class="text label"><fmt:message
															key="biolims.common.specifications1" /> </label>
													</td>
													<td class="requiredcolumn" nowrap width="10px">
													</td>
													<td><input type="text" class="input_parts text input default  readonlyfalse false" name="storage.spec" id="storage.spec" value='<s:property value="storage.spec"/>' title="<fmt:message key=" biolims.common.specifications1 "/> " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="20" maxlength="40"> <img alt='' src='${ctx}/images/blank.gif' class='detail' style='display: none' /></td>
													<td nowrap>&nbsp;</td>
												</tr>
												<tr class="control textboxcontrol">
													<td valign="top" align="right" class="controlTitle" nowrap>
														<span class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key=" biolims.common.commandTime "/>" class="text label"><fmt:message
															key="biolims.common.commandTime" /></label>
													</td>
													<td class="requiredcolumn" nowrap width="10px">
													</td>
													<td nowrap><input type="text" class="input_parts text input default readonlytrue false" readonly="readOnly" name="storage.createDate" id="storage.createDate" onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" value='<s:date name="storage.createDate" format="yyyy-MM-dd"/>' title="<fmt:message key=" biolims.common.commandTime "/>" size="20" maxlength="30"></td>
													<td nowrap>&nbsp;</td>
												</tr>

												<tr class="control textboxcontrol">
													<td valign="top" align="right" class="controlTitle" nowrap>
														<span class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key=" biolims.common.whetherTheFailureToRemind "/>" class="text label"><fmt:message
															key="biolims.common.whetherTheFailureToRemind" /> </label>
													</td>
													<td class="requiredcolumn" nowrap width="10px">
													</td>
													<td nowrap>
														<select name="storage.ifCall" id="storage_ifCall">
															<option value="0" <s:if test="storage.ifCall==0">selected="selected" </s:if>>
																<fmt:message key="biolims.common.no" />
															</option>
															<option value="1" <s:if test="storage.ifCall==1">selected="selected" </s:if>>
																<fmt:message key="biolims.common.yes" />
															</option>
														</select>
													</td>
													<td nowrap>&nbsp;</td>
												</tr>
												<%-- 	<tr class="control textboxcontrol">
											<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="品牌"
													class="text label">品牌</label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
												</td>
												<td nowrap><input type="text"
													class="input_parts text input default  readonlyfalse false"
													name="storage.breed" id="storage.breed"
													value='<s:property value="storage.breed"/>'
													title="品牌"
													onblur="textbox_ondeactivate(event);"
													onfocus="shared_onfocus(event,this)" size="20"
													maxlength="30"> <img alt=''
													src='${ctx}/images/blank.gif' class='detail'
													style='display: none' /></td>
												<td nowrap>&nbsp;</td>
											</tr> --%>
												<tr class="control textboxcontrol">
													<td valign="top" align="right" class="controlTitle" nowrap>
														<span class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key=" biolims.common.sortingNumber " />" class="text label"><fmt:message key="biolims.common.sortingNumber" /></label>
													</td>
													<td class="requiredcolumn" nowrap width="10px">
													</td>
													<td nowrap><input type="text" class="input_parts text input default readonlytrue true" name="storage.currentIndex" id="storage_currentIndex" value='<c:if test=' ${requestScope.max==null} '><s:property value="storage.currentIndex"/></c:if><c:if test='${requestScope.max!="" } '>${requestScope.max}</c:if>' title="<fmt:message key=" biolims.common.sortingNumber " />" onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="20" maxlength="30"></td>
													<td nowrap>&nbsp;</td>
												</tr>
												<%-- 	<tr class="control textboxcontrol" id="tri2">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="i7Index" class="text label"><fmt:message
															key="biolims.common.iSsequence" /> </label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
												</td>
												<td nowrap><input type="text"
													class="input_parts text input default readonlyfalse false"
													name="storage.i7Index" id="storage.i7Index"
													value='<s:property value="storage.i7Index"/>'
													title="<fmt:message key="biolims.common.iSsequence"/>"
													onblur="textbox_ondeactivate(event);"
													onfocus="shared_onfocus(event,this)" size="20"
													maxlength="30"> <img alt=''
													src='${ctx}/images/blank.gif' class='detail'
													style='display: none' /></td>
												<td nowrap>&nbsp;</td>
											</tr> --%>
											</tbody>
										</table>
									</div>
								</div>
							</td>
							<td class="sectioncol " width='33%' valign="top" align="right">
								<div class="section standard_section  ">
									<div class="section">
										<div class="section_description" style="display: none"></div>
										<table width="100%" class="section_table" cellpadding="0" cellspacing="0">
											<tbody>
												<tr>
													<td valign="top" align="right" class="controlTitle" nowrap>
														<span class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key=" biolims.common.type "/>" class="text label"> <fmt:message
															key="biolims.common.type" />
												</label>
													</td>
													<td class="requiredcolumn" nowrap width="10px"><img class='requiredimage' src='${ctx}/images/required.gif' />
													</td>
													<td nowrap><input type="text" class="input_parts text input default readonlyfalse false" readonly="readonly" id="storage_type_name" value="<s:property value='storage.type.name'/>" title="<fmt:message key=" biolims.common.type "/>  " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="10" maxlength="20"> <input type="hidden" name="storage.type.id" id="storage_type_id" value='<s:property value="storage.type.id"/>'> <img alt='<fmt:message key="biolims.common.elect"/>' id='dicStorageType' src='${ctx}/images/img_menu.gif' class='detail' /> <img alt='' src='${ctx}/images/blank.gif' class='detail' style='display: none' /></td>
													<td nowrap>&nbsp;</td>
												</tr>
												<tr style="display: none" class="control textboxcontrol">
													<td valign="top" align="right" class="controlTitle" nowrap>
														<span class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key=" biolims.common.englishName "/>" class="text label"><fmt:message
															key="biolims.common.englishName" /> </label>
													</td>
													<td class="requiredcolumn" nowrap width="10px">
													</td>
													<td nowrap><input type="text" class="input_parts text input default  readonlyfalse false" name="storage.engName" id="storage.engName" value='<s:property value="storage.engName"/>' title="<fmt:message key=" biolims.common.englishName "/> " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="20" maxlength="200"> <img alt='<fmt:message key="biolims.common.detailed"/>' id='englishname' src='${ctx}/images/img_longdescription_off.gif' class='detail' /></td>
													<td nowrap>&nbsp;</td>
												</tr>
												<tr class="control textboxcontrol" id="tri4">
													<td valign="top" align="right" class="controlTitle" nowrap>
														<span class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key=" biolims.common.retrieveTheCode "/>" class="text label">Component Cat No.</label>
													</td>
													<td class="requiredcolumn" nowrap width="10px">
													</td>
													<td nowrap><input type="text" class="input_parts text input default  readonlyfalse false" name="storage.searchCode" id="storage.searchCode" value='<s:property value="storage.searchCode"/>' title="<fmt:message key=" biolims.common.retrieveTheCode "/> " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="10" maxlength="15"> <img alt='' src='${ctx}/images/blank.gif' class='detail' style='display: none' /></td>
													<td nowrap>&nbsp;</td>
												</tr>
												<tr class="control textboxcontrol">
													<td valign="top" align="right" class="controlTitle" nowrap>
														<span class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key=" biolims.common.barcode "/>" class="text label">Catalog No.</label>
													</td>
													<td class="requiredcolumn" nowrap width="10px">
													</td>
													<td nowrap><input type="text" class="input_parts text input default  readonlyfalse false" name="storage.barCode" id="storage.barCode" value='<s:property value="storage.barCode"/>' title="<fmt:message key=" biolims.common.barcode "/> " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="20" maxlength="30"> <img alt='' src='${ctx}/images/blank.gif' class='detail' style='display: none' /></td>
													<td nowrap>&nbsp;</td>
												</tr>
												<tr class="control textboxcontrol">
													<td valign="top" align="right" class="controlTitle" nowrap>
														<span class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key=" biolims.common.state "/>" class="text label"><fmt:message
															key="biolims.common.state" /></label>
													</td>
													<td class="requiredcolumn" nowrap width="10px"><img class='requiredimage' src='${ctx}/images/required.gif' />
													</td>
													<td nowrap><input type="text" onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" class="input_parts text input default readonlyfalse false" name="" id="storage_state_name" readonly="readonly" value="<s:property value=" storage.state.name "/>" size="20" />
														<img alt='<fmt:message key="biolims.common.select"/>' id='showDicState' src='${ctx}/images/img_lookup.gif' class='detail' /> <input type="hidden" name="storage.state.id" id="storage_state_id" value="<s:property value=" storage.state.id "/>" /></td>
													<td nowrap>&nbsp;</td>
												</tr>
												<tr class="control textboxcontrol" id="tri1">
													<td valign="top" align="right" class="controlTitle" nowrap>
														<span class="labelspacer">&nbsp;&nbsp;</span> <label title="i5" class="text label"> i5 </label>
													</td>
													<td class="requiredcolumn" nowrap width="10px">
													</td>
													<td nowrap><input type="text" class="input_parts text input default readonlyfalse false" name="storage.i5" id="storage.i5" value='<s:property value="storage.i5"/>' title="i5 " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="20" maxlength="30"> <img alt='' src='${ctx}/images/blank.gif' class='detail' style='display: none' /></td>
													<td nowrap>&nbsp;</td>
												</tr>
												<tr class="control textboxcontrol" id="tri2">
													<td valign="top" align="right" class="controlTitle" nowrap>
														<span class="labelspacer">&nbsp;&nbsp;</span> <label title="i7" class="text label"> i7 </label>
													</td>
													<td class="requiredcolumn" nowrap width="10px">
													</td>
													<td nowrap><input type="text" class="input_parts text input default readonlyfalse false" name="storage.i7" id="storage.i7" value='<s:property value="storage.i7"/>' title="i7 " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="20" maxlength="30"> <img alt='' src='${ctx}/images/blank.gif' class='detail' style='display: none' /></td>
													<td nowrap>&nbsp;</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
				<table width="100%" class="section_table" cellpadding="0" cellspacing="0">
					<tbody>
						<tr class="sectionrow " valign="top">
							<td class="sectioncol " width='50%' valign="top" align="right">
								<div class="section standard_section marginsection  ">
									<div class="section_header standard_section_header">
										<table width="100%" cellpadding="0" cellspacing="0">
											<tbody>
												<tr>
													<td class="section_header_left text standard_section_label labelcolor" align="left"><span class="section_label"> <fmt:message
															key="biolims.common.inventoryInformation" />
												</span></td>
													<td class="section_header_right" align="right"></td>
												</tr>
											</tbody>
										</table>
									</div>
									<div class=" section_content_border">
										<div class="section_description" style="display: none"></div>
										<table width="100%" class="section_table" cellpadding="0">
											<tbody>
												<%
												if (request.getParameter("viewMode") == null) {
											%>
												<tr class="control textboxcontrol">
													<td valign="top" align="right" class="controlTitle" nowrap>
														<span class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key=" biolims.common.inventoryQuantity "/>" class="text label"><fmt:message
															key="biolims.common.inventoryQuantity" /> </label>
													</td>
													<td class="requiredcolumn" nowrap width="10px">
													</td>
													<td nowrap><input type="text" name="storage.num" id="storage.num" onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" class="input_parts text input default  readonlytrue false" value='<s:property value="%{storage.num!=null?getText(' format.int ',{storage.num}):' '}"/>' title="<fmt:message key=" biolims.common.inventoryQuantity "/>  " readonly="readonly" size="5" maxlength="30"> <input type="hidden" name="storage.factStorageNum" id="storage.factStorageNum" value='<s:property value="storage.factStorageNum"/>'>
														<input type="hidden" name="storage.hasSubmitNum" id="storage.hasSubmitNum" value='<s:property value="storage.hasSubmitNum"/>'>
														<input type="hidden" name="storage.lyNum" id="storage.lyNum" value='<s:property value="storage.lyNum"/>'> <img alt='' src='${ctx}/images/blank.gif' class='detail' style='display: none' /></td>
													<td nowrap>&nbsp;</td>
													<td valign="top" align="right" class="controlTitle" nowrap>
														<span class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key=" biolims.common.unit "/>" class="text label"><fmt:message
															key="biolims.common.unit" /></label>
													</td>
													<td class="requiredcolumn" nowrap width="10px">
													</td>
													<td nowrap><input type="text" class="input_parts text input default readonlyfalse false" name="storage.unit.name" id="storage_unit_name" readonly="readonly" value='<s:property value="storage.unit.name" />' title="<fmt:message key=" biolims.common.unit "/>  " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="10" maxlength="20"> 
														<input type="hidden" name="storage.unit.id" id="storage_unit_id" value='<s:property value="storage.unit.id" />'> <img alt='<fmt:message key="biolims.common.select"/>' id='dicUnitBtn' src='${ctx}/images/img_lookup.gif' class='detail' /> <img alt='' src='${ctx}/images/blank.gif' class='detail' style='display: none' /></td>
													<td nowrap>&nbsp;</td>
													<td valign="top" align="right" class="controlTitle" nowrap>
														<span class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key=" biolims.common.storageConditions "/>" class="text label"><fmt:message
															key="biolims.common.storageConditions" /> </label>
													</td>
													<td class="requiredcolumn" nowrap width="10px">
													</td>
													<td nowrap><input type="text" class="input_parts text input default readonlyfalse false" name="storage.saveCondition" id="storage_saveCondition" value='<s:property value="storage.saveCondition" />' title="<fmt:message key=" biolims.common.conditions "/> " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="10" maxlength="20"> <img alt='<fmt:message key="biolims.common.select"/>' id='showStorConBtn' src='${ctx}/images/img_lookup.gif' class='detail' /> <img alt='' src='${ctx}/images/blank.gif' class='detail' style='display: none' /></td>
													<%-- <td nowrap><input type="text"
													class="input_parts text input default readonlyfalse false"
													name="storage.saveCondition" id="storage.saveCondition"
													value='<s:property value="storage.saveCondition" />'
													title="存储条件  " onblur="textbox_ondeactivate(event);"
													onfocus="shared_onfocus(event,this)" size="40"
													maxlength="50"> <img alt=''
													src='${ctx}/images/blank.gif' class='detail'
													style='display: none' /></td>
												<td nowrap>&nbsp;</td> --%>
												</tr>

												<%
												}
											%>
												<tr>
													<td valign="top" align="right" class="controlTitle" nowrap>
														<span class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key=" biolims.common.safetyStock "/>" class="text label"><fmt:message
															key="biolims.common.safetyStock" /> </label>
													</td>
													<td class="requiredcolumn" nowrap width="10px">
													</td>
													<td nowrap><input type="text" class="input_parts text input default readonlyfalse false" name="storage.safeNum" id="storage_safeNum" value='<s:property value="storage.safeNum" />' title="<fmt:message key=" biolims.common.safetyStock "/>" onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" onkeyup="this.value=this.value.replace(/[^\d\.]/g,'')" onafterpaste="this.value=this.value.replace(/[^\d\.]/g,'')" size="5" maxlength="30"> <img alt='' src='${ctx}/images/blank.gif' class='detail' style='display: none' /></td>

													<td nowrap>&nbsp;</td>
													<td valign="top" align="right" class="controlTitle" nowrap>
														<span class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key=" biolims.common.periodOfValidity "/>" class="text label"><fmt:message
															key="biolims.common.periodOfValidity" /></label>
													</td>
													<td class="requiredcolumn" nowrap width="10px">
													</td>
													<td nowrap><input type="text" class="input_parts text input default readonlyfalse false" name="storage.durableDays" id="storage_durableDays" value='<s:property value="storage.durableDays"/>' title="<fmt:message key=" biolims.common.durableDays "/> " size="5" maxlength="30"></td>
													<td nowrap>&nbsp;</td>
													<td valign="top" align="right" class="controlTitle" nowrap>
														<span class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key=" biolims.common.note "/>" class="text label"><fmt:message
															key="biolims.common.remindTheLeadTime" /> </label>
													</td>
													<td class="requiredcolumn" nowrap width="10px">
													</td>
													<td nowrap><input type="text" class="input_parts text input default readonlyfalse false" name="storage.remindLeadTime" id="storage_remindLeadTime" value='<s:property value="storage.remindLeadTime"/>' title="<fmt:message key=" biolims.common.remindTheLeadTime "/> " size="5" maxlength="30"></td>
												</tr>
												<tr class="control textboxcontrol">
													<td valign="top" align="right" class="controlTitle" nowrap>
														<span class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key=" biolims.common.storageLocation "/>" class="text label"> <fmt:message
															key="biolims.common.storageLocation" /></label>
													</td>
													<td class="requiredcolumn" nowrap width="10px">
													</td>
													<td nowrap><input type="text" class="input_parts text input  false false" name="storage.position.id" id="storage.position.id" value='<s:property value="storage.position.id"/>' title="<fmt:message key=" biolims.common.storageLocation "/> " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="20" maxlength="30" readonly="readOnly"> <img alt='<fmt:message key="biolims.common.select"/>' id='wzBtn' name='show-btn' src='${ctx}/images/img_menu.gif' class='detail' /> <input type="text" class="input_parts text input  readonlytrue" name="storage.position.name" id="storage.position.name" readonly="readonly" value='<s:property value="storage.position.name"/>' title="<fmt:message key=" biolims.common.storageLocation "/>" onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="40" maxlength="62"> <img alt='<fmt:message key="biolims.common.detailed"/>' id="positionDetail" src='${ctx}/images/img_longdescription_off.gif' class='detail' /></td>
													<td nowrap>&nbsp;</td>
													<td valign="top" align="right" class="controlTitle" nowrap>
														<span class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key=" biolims.common.note "/>" class="text label"><fmt:message
															key="biolims.common.note" /></label>
													</td>
													<td class="requiredcolumn" nowrap width="10px">
													</td>
													<td nowrap><input type="text" class="input_parts text input default  readonlyfalse false" name="storage.note" id="storage.note" value='<s:property value="storage.note"/>' title="<fmt:message key=" biolims.common.note "/> " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="40" maxlength="50"> <img alt='<fmt:message key="biolims.common.detailed"/>' id="storageNote" src='${ctx}/images/img_longdescription_off.gif' class='detail' /></td>

													<s:hidden name="storage.outPrice" id="storage.outPrice"></s:hidden>

													<td nowrap>&nbsp;</td>
													<td valign="top" align="right" class="controlTitle" nowrap>
														<span class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key=" biolims.common.reactionNumber "/>" class="text label"><fmt:message key="biolims.common.reactionNumber"/></label>
													</td>
													<td class="requiredcolumn" nowrap width="10px">
													</td>
													<td nowrap><input type="text" class="input_parts text input default readonlyfalse false" name="storage.reactionNum" id="storage_reactionNum" value='<s:property value="storage.reactionNum"/>' title="<fmt:message key=" biolims.common.reactionNumber "/>" size="15" maxlength="30"></td>

												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
				<table width="100%" class="section_table" cellpadding="0">
					<tbody>
						<tr class="sectionrow " valign="top">
							<td class="sectioncol " width='50%' valign="top" align="right">
								<div class="section standard_section marginsection  ">
									<div class="section_header standard_section_header">
										<table width="100%" cellpadding="0" cellspacing="0">
											<tbody>
												<tr>
													<td class="section_header_left text standard_section_label labelcolor" align="left"><span class="section_label"><fmt:message
															key="biolims.common.manufacturerInformation" /></span></td>
													<td class="section_header_right" align="right"></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
				<table width="100%" class="section_table" cellpadding="0">
					<tbody>
						<script language="javascript">
							Ext.onReady(function() {
								Ext.QuickTips.init();
								Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
								var showProducer = Ext.get('showProducer');
								showProducer.on('click',
									function showProducerFun() {
										var win = Ext.getCmp('showProducerFun');
										if(win) {
											win.close();
										}
										var showProducerFun = new Ext.Window({
											id: 'showProducerFun',
											modal: true,
											title: '<fmt:message key="biolims.common.producers"/>',
											layout: 'fit',
											width: 500,
											height: 500,
											closeAction: 'close',
											plain: true,
											bodyStyle: 'padding:5px;',
											buttonAlign: 'center',
											collapsible: true,
											maximizable: true,
											items: new Ext.BoxComponent({
												id: 'maincontent',
												region: 'center',
												html: "<iframe scrolling='no' name='maincontentframe' src='/supplier/common/producerSelect.action?flag=showProducerFun' frameborder='0' width='100%' height='100%' ></iframe>"
											}),
											buttons: [{
												text: biolims.common.close,
												handler: function() {
													showProducerFun.close();
												}
											}]
										});
										showProducerFun.show();
									}
								);
							});

							function setshowProducerFun(rec) {
								document.getElementById('storage_producer_id').value = rec.get('id');
								document.getElementById('storage_producer_name').value = rec.get('name');
								document.getElementById('storage_producer_linkTel').value = rec.get('linkTel');
								document.getElementById('storage_producer_linkMan').value = rec.get('linkMan');
								document.getElementById('storage_producer_email').value = rec.get('email');
								var win = Ext.getCmp('showProducerFun')
								if(win) {
									win.close();
								}
							}
						</script>

						<tr class="control textboxcontrol">
							<td valign="top" align="right" class="controlTitle" nowrap>
								<!-- 生产商 --><span class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key=" biolims.common.producers "/>" class="text label"><fmt:message
									key="biolims.common.producers" /></label>
							</td>

							<td class="requiredcolumn" nowrap width="10px"></td>

							<td nowrap><input type="text" class="input_parts text input default  readonlyfalse false" name="storage.producer.name" readonly="readonly" id="storage_producer_name" value="<s:property value=" storage.producer.name "/>" title="<fmt:message key=" biolims.common.producers "/>  " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="40" maxlength="55">
								<input type="hidden" name="storage.producer.id" id="storage_producer_id" value="<s:property value=" storage.producer.id "/>"></input> <img alt='Select Value' id='showProducer' src='${ctx}/images/img_lookup.gif' name='showProducer' class='detail' /> <img class="detail" src="${ctx}/images/menu_icon_link.gif" onclick="viewCCSupplier();" alt="<fmt:message key=" biolims.common.checkTheProducers "/>">
							</td>

							<td nowrap>&nbsp;</td>
							<td valign="top" align="right" class="controlTitle" nowrap><span class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key=" biolims.common.manufacturersContact "/>" class="text label"><fmt:message
									key="biolims.common.manufacturersContact" /></label></td>

							<td class="requiredcolumn" nowrap width="10px"></td>

							<td nowrap><input type="text" class="input_parts text input default  readonlytrue false" name="storage.producer.linkMan" id="storage_producer_linkMan" value="<s:property value=" storage.producer.linkMan "/>" readonly="readOnly" title="<fmt:message key=" biolims.common.manufacturersContact "/>  " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="10" maxlength="30">
							</td>

							<td nowrap>&nbsp;</td>
							<td valign="top" align="right" class="controlTitle" nowrap><span class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key=" biolims.common.manufacturersTelephone "/>" class="text label"><fmt:message
									key="biolims.common.manufacturersTelephone" /></label></td>

							<td class="requiredcolumn" nowrap width="10px"></td>
							<td nowrap><input type="text" class="input_parts text input default  readonlytrue false" name="storage.producer.linkTel" id="storage_producer_linkTel" value="<s:property value=" storage.producer.linkTel "/>" readonly="readOnly" title="<fmt:message key=" biolims.common.manufacturersTelephone "/> " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="20" maxlength="30">
							</td>
							<td nowrap>&nbsp;</td>
							<td valign="top" align="right" class="controlTitle" nowrap><span class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key=" biolims.common.producersOfEmail "/>" class="text label"><fmt:message
									key="biolims.common.producersOfEmail" /></label></td>
							<td class="requiredcolumn" nowrap width="10px"></td>
							<td nowrap><input type="text" class="input_parts text input default  readonlytrue false" name="storage.producer.email" id="storage_producer_email" value="<s:property value=" storage.producer.email "/>" readonly="readOnly" title="<fmt:message key=" biolims.common.producersOfEmail "/>  " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="20" maxlength="30">
							</td>
							<td nowrap>&nbsp;</td>
						</tr>
				</table>

				<s:hidden id="storageType" value="%{storage.type.id}"></s:hidden>
				<input type="hidden" name="storageReagentJson" id="storageReagentJson" value="" />
			</s:form>
			<%
			if (request.getParameter("viewMode") == null) {
		%>
			<s:if test='#request.handlemethod!="add"'>
				<%
				String storageTypeId = (String) request
								.getAttribute("storage.type.id");
						if (storageTypeId
								.startsWith

								(com.biolims.storage.common.constants.SystemConstants.DIC_STORAGE_TYPE_HAOCAI_CGSJ)) {
			%>
				<iframe scrolling="auto" name="tab11frame" src="${ctx}/storage/storageReagentView.action?handlemethod=${handlemethod}&storageId=${storage.id}" frameborder="0" width="100%" height="100%"></iframe>
				<%
				}
			%>
			</s:if>
			<%
			}
		%>
		</div>
		<script type="text/javascript">
			<s:if test='#request.handlemethod=="modify"'>
		var t = "#storage_id";
		settextreadonlyById(t);
		settextreadonlyById("#storage_type_name");
		jQuery("#dicStorageType").hide();
		</s:if> 
		<s:if test = '#request.handlemethod=="view"' >
				settextreadonlyByAll();
		</s:if>
				
			<%if (request.getParameter("copyMode") != null
					&& request.getParameter("copyMode").equals("true")) {%>
			settextread("storage_id");
			document.getElementById("storage_id").value = "";
			//document.getElementById("storage_state").value="3";
			//setTimeout("firstCommonValue(gridGrid)",1000);
			<%}%>
		</script>
	</body>

</html>