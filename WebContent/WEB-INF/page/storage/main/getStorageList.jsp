<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>
			<fmt:message key="biolims.common.inventoryMasterData" />
		</title>

		<%@ include file="/WEB-INF/page/include/common3.jsp"%>
		<%@ include file="/WEB-INF/page/include/common1.jsp"%>
		<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
		<script type="text/javascript" src="${ctx}/javascript/storage/main/getStorageList.js"></script>

		<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
		<g:LayOutWinTag buttonId="showDicState" title='<fmt:message key="biolims.common.selectTheState"/>' hasHtmlFrame="true" html="${ctx}/dic/state/stateSelect.action?type=storage" isHasSubmit="false" functionName="showDicStateFun" hasSetFun="true" extRec="id,name" extStr="document.getElementById('state_id').value=id;document.getElementById('state_name').value=name;" />

		<g:LayOutWinTag buttonId="searchDutyUser" title='<fmt:message key="biolims.common.selectLeader"/>' hasHtmlFrame="true" hasSetFun="true" html="${ctx}/core/user/userSelect.action" width="document.body.clientWidth/1.5" isHasSubmit="false" functionName="dutyUserFun" documentId="dutyUser_id" documentName="dutyUser_name" />

		<g:LayOutWinTag buttonId="searchCreateUser" title='<fmt:message key="biolims.common.selectTheCreatePerson"/>' hasHtmlFrame="true" hasSetFun="true" html="${ctx}/core/user/userSelect.action" width="document.body.clientWidth/1.5" isHasSubmit="false" functionName="createUserFun" documentId="createUser_id" documentName="createUser_name" />

		<g:LayOutWinTag buttonId="dicStorageType" title='<fmt:message key="biolims.common.chooseClassification"/>' hasHtmlFrame="true" html="${ctx}/storage/common/storageTypeSelect.action?type=1" isHasSubmit="false" functionName="dicStorageTypeFun" hasSetFun="true" documentId="storage_type_id" documentName="storage_type_name" />

		<g:LayOutWinTag buttonId="searchProducer" title='<fmt:message key="biolims.common.selectTheManufacturer"/>' hasHtmlFrame="true" html="${ctx}/supplier/common/supplierSelect.action" width="900" height="500" isHasSubmit="false" functionName="customer" hasSetFun="true" extRec="rec" extStr="document.getElementById('producer_id').value=rec.get('id');document.getElementById('producer_name').value=rec.get('name');" />
		<g:LayOutWinTag buttonId="selectKit" title='' hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action" isHasSubmit="false" functionName="kit" hasSetFun="true" documentId="kit_id" documentName="kit_name" />
	</head>

	<body>
		<div class="mainlistclass" id="markup">
			<input type="hidden" id="selectId" />
			<input type="hidden" id="id" value="" />
			<input type="hidden" id="p_type" value="${requestScope.p_type}" />
			<div id="jstj" style="display: none">
				<s:hidden id="rightsId" name="rightsId"></s:hidden>

				<table cellspacing="0" cellpadding="0" class="toolbarsection">
					<tr>
						<td valign="top" align="right" nowrap><label class="text label" title="">JDE Code</label></td>
						<td nowrap class="quicksearch_control" align="left"><input type="text" name="jdeCode" id="jdeCode" searchField="true" class="input_parts text input default  readonlyfalse false" onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" maxlength="32" title="">
						</td>
						<%-- <td valign="top" align="right" nowrap>
						<label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>"><fmt:message key="biolims.common.serialNumber"/></label>
					</td>
					<td nowrap class="quicksearch_control" valign="middle">
						<input type="text" searchField="true" onfocus="shared_onfocus(event,this)" maxlength="32" title="<fmt:message key="biolims.common.inputID"/>"
							class="input_parts text input default  readonlyfalse false" name="id" id="bianhao"  onblur="textbox_ondeactivate(event);">
					</td> --%>
						<td valign="top" align="right" nowrap><label class="text label" title="<fmt:message key=" biolims.common.inputTheQueryConditions "/>">
						<fmt:message key="biolims.common.componentName" /> </label>
						</td>
						<td nowrap class="quicksearch_control" valign="middle"><input type="text" searchField="true" maxlength="55" onfocus="shared_onfocus(event,this)" id="storage_name" title="<fmt:message key=" biolims.common.inputName "/>" class="input_parts text input default  readonlyfalse false" name="name" onblur="textbox_ondeactivate(event);"></td>
					</tr>
					<tr>
						<td valign="top" align="right" class="controlTitle" nowrap><span class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key=" biolims.common.type "/>" class="text label"><fmt:message key="biolims.common.type" /></label>
						</td>
						<td nowrap><input type="text" class="input_parts text input default  readonlyfalse false" name="" maxlength="20" id="storage_type_name" title="<fmt:message key=" biolims.common.type "/>  " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)"> <img title='<fmt:message key="biolims.common.select"/>' id='dicStorageType' src='${ctx}/images/img_lookup.gif' class='detail' /> <input type="hidden" name="type.id" id="storage_type_id" searchField="true"></td>
						<td valign="top" align="right" class="controlTitle" nowrap><span class="labelspacer">&nbsp;&nbsp;</span> <label title="" class="text label"><fmt:message key="biolims.common.producers" /></label></td>
						<td nowrap><input type="text" class="input_parts text input default  readonlyfalse false" name="" maxlength="20" id="producer_name" title="" onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)"> <img title='<fmt:message key="biolims.common.select"/>' id='searchProducer' src='${ctx}/images/img_lookup.gif' class='detail' /> <input type="hidden" name="producer.id" id="producer_id" searchField="true"></td>
					</tr>
					<%-- <tr>
					<td valign="top" align="right" nowrap><span>&nbsp;</span> <label
						class="text label"
						title="<fmt:message key="biolims.common.inputTheQueryConditions"/>"><fmt:message
								key="biolims.common.leader" /></label></td>
					<td nowrap class="quicksearch_control" valign="middle"><input
						type="text" readonly="readOnly" id="dutyUser_name"
						title="<fmt:message key="biolims.common.inputLeader"/>"
						class="input_parts text input default  readonlyfalse false"
						name="dutyUser.name" onblur="textbox_ondeactivate(event);"
						maxlength="55" onfocus="shared_onfocus(event,this)"> <img
						title='<fmt:message key="biolims.common.select"/>'
						id='searchDutyUser' src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>
					<input type="hidden" name="dutyUser.id" id="dutyUser_id"
						searchField="true">
					<td valign="top" align="right" nowrap><span
						class="labelspacer">&nbsp;&nbsp;</span> <label
						title="<fmt:message key="biolims.common.commandPerson"/>"
						class="text label"><fmt:message
								key="biolims.common.commandPerson" /> </label></td>
					<td nowrap><input type="text"
						class="input_parts text input default  readonlyfalse false"
						readonly="readOnly" id="createUser_name"
						title="<fmt:message key="biolims.common.commandPerson"/>  "
						onblur="textbox_ondeactivate(event);"
						onfocus="shared_onfocus(event,this)" maxlength="20"> <img
						title='<fmt:message key="biolims.common.select"/>'
						id='searchCreateUser' src='${ctx}/images/img_lookup.gif'
						class='detail' /> <input type="hidden" name="createUser.id"
						id="createUser_id" searchField="true"></td>
				</tr> --%>
					<tr>
						<td valign="top" align="right" nowrap><label class="text label" title="<fmt:message key=" biolims.common.inputTheQueryConditions "/>">Catalog No.</label></td>
						<td nowrap class="quicksearch_control" valign="middle"><input type="text" searchField="true" class="input_parts text input default  readonlyfalse false" name="barCode" onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" maxlength="200" id="barCode" title="">
						</td>
						<td valign="top" align="right" class="controlTitle" nowrap><span class="labelspacer">&nbsp;&nbsp;</span> <label title="" class="text label"><fmt:message key="biolims.common.kitName" /></label>
						</td>
						<td nowrap><input type="text" class="input_parts text input default  readonlyfalse false" name="" id="kit_name" readonly="readOnly" /> <img alt='<fmt:message key="biolims.common.select"/>' id='selectKit' src='${ctx}/images/img_lookup.gif' class='detail' /> <input type="hidden" searchField="true" id="kit_id" searchField="true" name="kit.id" /></td>
					</tr>
					<%-- <tr>
					<td valign="top" align="right" nowrap><label
						class="text label"
						title="<fmt:message key="biolims.common.inputTheQueryConditions"/>">
							<fmt:message key="biolims.common.use" />
					</label></td>
					<td nowrap class="quicksearch_control" valign="middle"><input
						type="text" searchField="true" maxlength="200" id="useDesc"
						title="<fmt:message key="biolims.common.inputDescribe"/>"
						name="useDesc"
						class="input_parts text input default  readonlyfalse false"
						onblur="textbox_ondeactivate(event);"
						onfocus="shared_onfocus(event,this)"></td>
					<td valign="top" align="right" valign="top" align="right"
						class="controlTitle" nowrap><spanclass="labelspacer">&nbsp;&nbsp;</span>
						<label title="<fmt:message key="biolims.common.state"/>"
							class="text label"> <fmt:message
								key="biolims.common.specifications1" />
						</label></td>
					<td nowrap><input type="text"
						class="input_parts text input default  readonlyfalse false"
						name="spec" searchField="true" id="spec" /></td>
				</tr> --%>
					<tr>
						<%-- 	<td valign="top" align="right" class="controlTitle" nowrap>
						<span class="labelspacer">&nbsp;&nbsp;</span> 
						<label title="存储位置" class="text label"> 存储位置 </label>
					</td>
					<td nowrap>
						<input type="text" class="input_parts text input  false false" name="position.id" id="position_id"
							searchField="true" title="存储位置 " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" 
							maxlength="30" readonly="readOnly">
						<img alt='选择' id='wzBtn' name='show-btn'src='${ctx}/images/img_lookup.gif' class='detail' />
					</td> --%>
						<td valign="top" align="right" class="controlTitle" nowrap><span class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key=" biolims.common.brand "/>" class="text label"><fmt:message key="biolims.common.component" /> Cat No.</label></td>
						<td nowrap><input type="text" class="input_parts text input default  readonlyfalse false" name="breed" searchField="true" id="breed" searchField="true" title="<fmt:message key=" biolims.common.brand "/>  " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" maxlength="30"></td>
						<%-- <td valign="top" align="right" class="controlTitle" nowrap><span
						class="labelspacer">&nbsp;&nbsp;</span> <label title="备注"
						class="text label"><fmt:message key="biolims.common.note" />
					</label></td>
					<td nowrap><input type="text" class="input_field" name="note"
						searchField="true" id="note"
						title="<fmt:message key="biolims.common.note"/> "
						onblur="textbox_ondeactivate(event);"
						onfocus="shared_onfocus(event,this)" maxlength="50"></td> --%>
					</tr>
					<tr>

						<%-- <td valign="top" align="right" class="controlTitle" nowrap>
						<span class="labelspacer">&nbsp;&nbsp;</span> 
						<label title="生产厂商" class="text label">生产厂商</label>
					</td>
					<td nowrap>
						<input type="text" class="input_parts text input default  readonlyfalse false" name="producer.name"
						id="producer_name" readonly="readonly"  title="生产厂商  " onblur="textbox_ondeactivate(event);"
						onfocus="shared_onfocus(event,this)"  maxlength="30">
						<img alt='选择' id='searchProducer' src='${ctx}/images/img_lookup.gif' class='detail' />
						<input type="hidden" name="producer.id" id="producer_id" searchField="true"  readonly="readonly">
					</td>
				</tr>
				<tr> --%>
						<%-- <td valign="top" align="right" nowrap><label
						class="text label"
						title="<fmt:message key="biolims.common.inputTheQueryConditions"/>">
							<fmt:message key="biolims.common.recordNumber" />
					</label></td>
					<td nowrap class="quicksearch_control" valign="middle"><input
						type="text"
						class="input_parts text input default  readonlyfalse false"
						name="limitNum" onblur="textbox_ondeactivate(event);"
						onfocus="shared_onfocus(event,this)" id="limitNum"
						title="<fmt:message key="biolims.common.inputTheRecordNumber"/>">
					</td> --%>
					</tr>
				</table>
				<div id="search_show_div"></div>
			</div>
			<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
				<input type='hidden' id='gridhtm' name='gridhtm' value='' />
			</form>
			<input type='hidden' id='extJsonDataString' name='extJsonDataString' value='' />
			<div id='grid'></div>
			<div id='gridcontainer'></div>

		</div>

		<div id="bat_uploadcsv_div" style="display: none">
			<input type="file" name="file" id="file-uploadcsv">
			<fmt:message key="biolims.common.upLoadFile" />
		</div>
	</body>

</html>