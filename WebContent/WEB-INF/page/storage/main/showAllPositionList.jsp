<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="biolims.common.inventoryMasterData"/></title>
<script type="text/javascript">
	function setvalue(id,name,searchCode,unitStr){
		 window.parent.setWz(id,name);
	}
</script>
</head>
<body>
	<table width="100%">
		<tr>
			<td><g:GridTagNoLock isAutoWidth="true" col="${col}"
					type="${type}" title='<fmt:message key="biolims.common.listOfPositions"/>' url="${path}" /></td>
		</tr>
		<tr>
			<td></td>
		</tr>
	</table>
</body>
</html>