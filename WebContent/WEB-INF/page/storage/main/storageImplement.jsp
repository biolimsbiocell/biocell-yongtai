<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="biolims.common.instruments"/></title>
<script type="text/javascript"
	src="${ctx}/javascript/storage/main/storageImplement.js"></script>
</head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<body>
	<g:LayOutWinTag buttonId="cccsBtn" title='<fmt:message key="biolims.common.manufacturers"/>' isHasSubmit="false"
		functionName="searchgw"
		html="<iframe scrolling='no' name='maincontentframe' src='${ctx}/storage/common/getSupplierListView.action' 
	frameborder='0' width='100%' height='100%' ></iframe>" />
	<table width="100%" class="section_table" cellpadding="0">
		<tbody>
			<tr class="sectionrow " valign="top">
				<td class="sectioncol " width='50%' valign="top" align="right">
					<div class="section standard_section marginsection  ">
						<div class=" section_content_border">
							<s:form id="implementForm"
								action="/storage/addStorageImplement.action" method="post"
								theme="simple">
								<s:hidden name="si.storageId" id="storageId"
									value="#request.storageId"></s:hidden>
								<table width="100%" class="section_table" cellpadding="0">
									<tbody>
										<tr class="control textboxcontrol">
											<td valign="top" align="right" class="controlTitle" nowrap>
												<span class="labelspacer">&nbsp;&nbsp;</span> <label
												title="<fmt:message key="biolims.common.brand"/>" class="text label"><fmt:message key="biolims.common.brand"/></label>
											</td>
											<td class="requiredcolumn" nowrap width="10px">
											</td>
											<td nowrap><input type="text"
												class="input_parts text input default  false false"
												name="si.storageBreed" id="si.storageBreed"
												value='<s:property value="si.storageBreed"/>' title="<fmt:message key="biolims.common.brand"/>  "
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="20"
												maxlength="30"> <img alt=''
												src='${ctx}/images/blank.gif' class='detail'
												style='display: none' /></td>
											<td nowrap>&nbsp;</td>
											<td valign="top" align="right" class="controlTitle" nowrap>
												<span class="labelspacer">&nbsp;&nbsp;</span> <label
												title="<fmt:message key="biolims.common.manufacturers"/>" class="text label"><fmt:message key="biolims.common.manufacturers"/></label>
											</td>
											<td class="requiredcolumn" nowrap width="10px">
											</td>
											<td nowrap><input type="text"
												class="input_parts text input default  false false"
												name="si.storageProducter.name"
												id="si.storageProducter.name"
												value="<s:property value="si.storageProducter.name"/>"
												title="<fmt:message key="biolims.common.manufacturers"/>  " onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="20"
												maxlength="30"> <input type="hidden"
												name="si.storageProducter.id" id="si.storageProducter.id"
												value='<s:property value="si.storageProducter.id"/>'>
												<img alt='<fmt:message key="biolims.common.select"/>' id='cccsBtn'
												src='${ctx}/images/img_lookup.gif'
												name='main_grid2_row1_col1_sec1_2_detail' class='detail' />
												<img alt='' src='${ctx}/images/blank.gif' class='detail'
												style='display: none' /></td>
											<td nowrap>&nbsp;</td>
											<td valign="top" align="right" class="controlTitle" nowrap>
												<span class="labelspacer">&nbsp;&nbsp;</span> <label
												title="<fmt:message key="biolims.common.origin"/>" class="text label"><fmt:message key="biolims.common.origin"/></label>
											</td>
											<td class="requiredcolumn" nowrap width="10px">
											</td>
											<td nowrap><input type="text"
												class="input_parts text input default  false false"
												name="si.storageProductAddress"
												id="si.storageProductAddress"
												value="<s:property value="si.storageProductAddress"/>"
												title="<fmt:message key="biolims.common.origin"/> " onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="20"
												maxlength="30"> <img alt=''
												src='${ctx}/images/blank.gif' class='detail'
												style='display: none' /></td>
											<td nowrap>&nbsp;</td>
											<td valign="top" align="right" class="controlTitle" nowrap>
												<span class="labelspacer">&nbsp;&nbsp;</span> <label
												title="<fmt:message key="biolims.common.manufacturersTelephone"/>" class="text label"><fmt:message key="biolims.common.manufacturersTelephone"/></label>
											</td>
											<td class="requiredcolumn" nowrap width="10px">
											</td>
											<td nowrap><input type="text"
												class="input_parts text input default  readonlytrue false"
												="" name="" id="ccsPhone"
												value="<s:property value="si.storageProducter.linkTel"/>"
												title="<fmt:message key="biolims.common.manufacturersTelephone"/> " onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="20"
												maxlength="30"> <img alt=''
												src='${ctx}/images/blank.gif' class='detail'
												style='display: none' /></td>
											<td nowrap>&nbsp;</td>
										</tr>
										<tr>
											<td valign="top" align="right" class="controlTitle" nowrap>
												<span class="labelspacer">&nbsp;&nbsp;</span> <label
												title="<fmt:message key="biolims.common.contact"/>" class="text label"><fmt:message key="biolims.common.contact"/></label>
											</td>
											<td class="requiredcolumn" nowrap width="10px">
											</td>
											<td nowrap><input type="text"
												class="input_parts text input default  readonlytrue false"
												readonly="readOnly" id="linkMan"
												value='<s:property value="si.storageProducter.linkMan"/>'
												title="<fmt:message key="biolims.common.contact"/>  " onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="10"
												maxlength="30"> <img alt=''
												src='${ctx}/images/blank.gif' class='detail'
												style='display: none' /></td>
											<td nowrap>&nbsp;</td>
											<td valign="top" align="right" class="controlTitle" nowrap>
												<span class="labelspacer">&nbsp;&nbsp;</span> <label
												title="<fmt:message key="biolims.common.fax"/>" class="text label"><fmt:message key="biolims.common.fax"/></label>
											</td>
											<td class="requiredcolumn" nowrap width="10px">
											</td>
											<td nowrap><input type="text"
												class="input_parts text input default  readonlytrue false"
												readonly="readOnly" id="fax"
												value='<s:property value="si.storageProducter.fax"/>'
												title="<fmt:message key="biolims.common.fax"/> " onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="20"
												maxlength="20"> <img alt=''
												src='${ctx}/images/blank.gif' class='detail'
												style='display: none' /></td>
											<td nowrap>&nbsp;</td>
											<td valign="top" align="right" class="controlTitle" nowrap>
												<span class="labelspacer">&nbsp;&nbsp;</span> <label
												title="<fmt:message key="biolims.common.outboundPrice"/>" class="text label"><fmt:message key="biolims.common.outboundPrice"/></label>
											</td>
											<td class="requiredcolumn" nowrap width="10px">
											</td>
											<td nowrap><input type="text"
												class="input_parts text input default  readonlytrue false"
												readonly="readOnly" name="" id=""
												value='<s:property value="si.outPrice"/>' title="<fmt:message key="biolims.common.outboundPrice"/>  "
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="20"
												maxlength="30"> <img alt=''
												src='${ctx}/images/blank.gif' class='detail'
												style='display: none' /></td>
											<td nowrap>&nbsp;</td>
											<td valign="top" align="right" class="controlTitle" nowrap>
												<span class="labelspacer">&nbsp;&nbsp;</span> <label
												title="<fmt:message key="biolims.common.currency"/>" class="text label"><fmt:message key="biolims.common.currency"/></label>
											</td>
											<td class="requiredcolumn" nowrap width="10px">
											</td>
											<td nowrap><input type="text"
												class="input_parts text input default  readonlytrue false"
												="" name="main_grid2_row1_col1_sec1_2_textbox"
												id="main_grid2_row1_col1_sec1_2_textbox"
												value='<s:property value="si.currencyType.name"/>'
												title="<fmt:message key="biolims.common.currency"/> " onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="5" maxlength="20">
												<img alt='' src='${ctx}/images/blank.gif' class='detail'
												style='display: none' /></td>
											<td nowrap>&nbsp;</td>
										</tr>
										<tr class="control textboxcontrol">
											<td valign="top" align="right" class="controlTitle" nowrap>
												<span class="labelspacer">&nbsp;&nbsp;</span> <label
												title="<fmt:message key="biolims.common.note"/>" class="text label"><fmt:message key="biolims.common.note"/></label>
											</td>
											<td class="requiredcolumn" nowrap width="10px">
											</td>
											<td nowrap><input type="text"
												class="input_parts text input default  false false"
												name="si.remark" id="si.remark"
												value='<s:property value="si.remark"/>' title="<fmt:message key="biolims.common.note"/> "
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="30"
												maxlength="50"> <img alt='<fmt:message key="biolims.common.detailed"/>'
												src='${ctx}/images/img_longdescription_off.gif'
												class='detail' /> <img alt=''
												src='${ctx}/images/blank.gif' ' class='detail'
												style='display: none' /></td>
											<td nowrap>&nbsp;</td>
										</tr>
								</table>
							</s:form>
						</div>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
</body>
</html>