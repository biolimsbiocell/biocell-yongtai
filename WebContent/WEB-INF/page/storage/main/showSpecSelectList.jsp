
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="${ctx}/javascript/storage/main/showSpecSelectList.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
<input type="hidden" id="showType" value="${requestScope.type}">
<input type="hidden" id="p_type" value="${requestScope.p_type}">
		<input type="hidden" id="selectId"/>
	<c:if test="${requestScope.type=='index'}">

		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<table cellspacing="0" cellpadding="0" class="toolbarsection">
				<tr>
	               	 	<td class="label-title" >原辅料盒</td>
	                   	<td align="left"  >
						<input type="text" size="20" maxlength="25" id="kit_name"
	                   	 name="kit.name" searchField="true" title="原辅料盒"  onkeydown="disableEnter(event)"  />
	                   	</td>
	                   		 	<td class="label-title" >名称</td>
	                   	<td align="left"  >
						<input type="text" size="20" maxlength="25" id="name"
	                   	 name="name" searchField="true" title="name"  onkeydown="disableEnter(event)"  />
	                   	</td>
	                   	
	               	 	<td class="label-title" >i5</td>
	                   	<td align="left"  >
						<input type="text" size="20" maxlength="25" id="i5"
	                   	 name="i5" searchField="true" title="i5"  onkeydown="disableEnter(event)"  />
	                   	</td>
	                   		<td class="label-title" >i7</td>
	                   	<td align="left"  >
						<input type="text" size="20" maxlength="25" id="i7"
	                   	 name="i7" searchField="true" title="i7"  onkeydown="disableEnter(event)"  />
	                   	</td>
	                   	<td><input type="button" value="查询" onclick="selkit();"/></td>
	                   	
	                   	
				</tr>
            </table>
	</c:if>
	<div id="showSpecSelectdiv"></div>
  		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
	<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
</body>
</html>



