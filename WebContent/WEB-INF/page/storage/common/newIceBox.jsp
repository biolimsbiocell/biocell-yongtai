<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<title></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style>
.dataTables_scrollBody {
	min-height: 100px;
}
</style>
</head>

<body>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">
		<input type="hidden" id="states"
		value="${requestScope.states}">
	<div class="container-fluid" style="margin-top: 46px">
		<!--Form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<i></i><fmt:message key="biolims.common.refrigerators" />
					</h3>

					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="####" onclick="$('.buttons-print').click();"><fmt:message
											key="biolims.common.print" /></a></li>
								<li><a href="#####" onclick="$('.buttons-copy').click();"><fmt:message
											key="biolims.common.copyData" /></a></li>
								<li><a href="####" onclick="$('.buttons-excel').click();">Excel</a>
								</li>
								<li><a href="####" onclick="$('.buttons-csv').click();">CSV</a>
								</li>
								<li role="separator" class="divider"></li>
								<li><a href="####" onclick="itemFixedCol(2)"><fmt:message
											key="biolims.common.lock2Col" /></a></li>
								<li><a href="####" id="unfixde" onclick="unfixde()"><fmt:message
											key="biolims.common.cancellock2Col" /></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="box-body">
					<!--form表单-->
					<div class="HideShowPanel">
						<form name="form1"  class="layui-form" id="form1" method="post">
							<input type="hidden" id="bpmTaskId"
								value="<%=request.getParameter("bpmTaskId")%>" /> <br>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message key="biolims.common.locationCode" /><img class='requiredimage'
											src='${ctx}/images/required.gif' /> </span> 
											<c:if test="${requestScope.states=='2'}">  
											<input type="text"
											size="20" maxlength="25" id="sp_id" name="id"
											changelog="<s:property value="sp.id"/>"
											class="form-control" readonly
											value="<s:property value="sp.id"/>" />
											</c:if>
											<c:if test="${requestScope.states=='3'}">  
											<input type="text"
											size="20" maxlength="25" id="sp_id" name="id"
											changelog="<s:property value="sp.id"/>"
											class="form-control" 
											value="<s:property value="sp.id"/>" />
											</c:if>

									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<c:if test="${requestScope.states=='2'}">  
										<span class="input-group-addon"><fmt:message key="biolims.common.locationName" /></span> <input type="text"
											size="20" maxlength="25" id="sp_name" name="name"
											changelog="<s:property value="sp.name"/>"
											class="form-control" readonly
											value="<s:property value="sp.name"/>" />
										</c:if>
										<c:if test="${requestScope.states=='3'}">  
										<span class="input-group-addon"><fmt:message key="biolims.common.locationName" /></span> <input type="text"
											size="20" maxlength="25" id="sp_name" name="name"
											changelog="<s:property value="sp.name"/>"
											class="form-control"
											value="<s:property value="sp.name"/>" />
											</c:if>

									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon">温度</span> <input type="text"
											size="20" readonly="readOnly" id="sp_type_name"
											changelog="<s:property value="sp.type.name"/>"
											value="<s:property value="sp.type.name"/>"
											class="form-control" /> <input type="hidden"
											id="sp_type_id" name="type-id"
											value="<s:property value="sp.type.id"/>">

										<span class="input-group-btn">
											<c:if test="${requestScope.states=='3'}">  
											<button class="btn btn-info" type="button"
												onClick="showType()">
												<i class="glyphicon glyphicon-search"></i>
												
												</c:if>
										</span>
									</div>
								</div>
							</div>
							<div class="row">
							<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message key="biolims.common.upID" /></span> <input type="text"
											size="20" readonly="readOnly" id="sp_upStoragePosition_name"
											changelog="<s:property value="sp.upStoragePosition.name"/>"
											value="<s:property value="sp.upStoragePosition.name"/>"
											class="form-control" /> <input type="hidden"
											id="sp_upStoragePosition_id" name="upStoragePosition-id"
											value="<s:property value="sp.upStoragePosition.id"/>">
									</div>
								</div>
								
										<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">位置类型<img class='requiredimage'
											src='${ctx}/images/required.gif' /></span> 
										<select lay-ignore class="form-control" id="sp_positionType" name="positionType">
											<option value="0" 
												<s:if test="sp.positionType==0">selected="selected"</s:if>>自体细胞库</option>
											<option value="1"
												<s:if test="sp.positionType==1">selected="selected"</s:if>>公共细胞库</option>
											<option value="2"
												<s:if test="sp.positionType==2">selected="selected"</s:if>>样本库</option>
										</select>
									</div>
								</div>
								
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message key="biolims.common.locationState" /></span> <input type="text"
											size="20" readonly="readOnly" id="sp_state_name"
											changelog="<s:property value="sp.state.name"/>"
											value="<s:property value="sp.state.name"/>"
											class="form-control" /> <input type="hidden"
											id="sp_state_id" name="state-id"
											value="<s:property value="sp.state.id"/>">

										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showState()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
							</div>
							
							
							<div class=row>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon">级别1</span>
										<c:if test="${requestScope.states=='2'}">  
										 <input type="text"
											id="sp_rowNum"
											changelog="<s:property value="sp.rowNum"/>"
											name = "rowNum" 
											value="<s:property value="sp.rowNum"/>"
											class="form-control" readonly  onkeyup="value=value.replace(/[^\d]/g,'')" />
											 <input type="text"
											id="sp_rowName" 
											name = "rowName" readonly
											changelog="<s:property value="sp.rowName"/>"
											value="<s:property value="sp.rowName"/>"
											class="form-control" readonly > 
										</c:if>
											<c:if test="${requestScope.states=='3'}">  
										 <input type="text"
											id="sp_rowNum"
											changelog="<s:property value="sp.rowNum"/>"
											name = "rowNum"
											value="<s:property value="sp.rowNum"/>"
											class="form-control"  onkeyup="value=value.replace(/[^\d]/g,'')" />
											 <input type="text"
											id="sp_rowName" 
											name = "rowName"
											changelog="<s:property value="sp.rowName"/>"
											value="<s:property value="sp.rowName"/>"
											class="form-control" >
										</c:if>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">级别2</span>
										<c:if test="${requestScope.states=='2'}">  
										 <input type="text"
											id="sp_colNum"
											changelog="<s:property value="sp.colNum"/>"
											name="colNum" 
											value="<s:property value="sp.colNum"/>"
											class="form-control" readonly onkeyup="value=value.replace(/[^\d]/g,'')" />
											 <input type="text"
											id="sp_colName" 
											name="colName"
											changelog="<s:property value="sp.colName"/>"
											value="<s:property value="sp.colName"/>"
											class="form-control"  readonly >
										</c:if>
										<c:if test="${requestScope.states=='3'}">  
										 <input type="text"
											id="sp_colNum"
											changelog="<s:property value="sp.colNum"/>"
											name="colNum"
											value="<s:property value="sp.colNum"/>"
											class="form-control"  onkeyup="value=value.replace(/[^\d]/g,'')" />
											 <input type="text"
											id="sp_colName" 
											name="colName"
											changelog="<s:property value="sp.colName"/>"
											value="<s:property value="sp.colName"/>"
											class="form-control" >
										</c:if>
									</div>
								</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon">级别2容器</span> <input type="text"
											size="20" readonly="readOnly" id="sp_storageContainer_name"
											changelog="<s:property value="sp.storageContainer.name"/>"
											value="<s:property value="sp.storageContainer.name"/>"
											class="form-control" /> <input type="hidden"
											id="sp_storageContainer_id" name="storageContainer-id"
											value="<s:property value="sp.storageContainer.id"/>">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showStorageContainer()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
										<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">盒子容器类型</span> <input type="text"
											size="20" readonly="readOnly" id="sp_subStorageContainer_name"
											changelog="<s:property value="sp.subStorageContainer.name"/>"
											value="<s:property value="sp.subStorageContainer.name"/>"
											class="form-control" /> <input type="hidden"
											id="sp_subStorageContainer_id" name="subStorageContainer-id"
											value="<s:property value="sp.subStorageContainer.id"/>">

										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showSubStorageContainer()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
							</div>
							<div class=row>
					
								<div class="col-xs-4">
									<div class="input-group" style="display:none">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.CostCentre" /> </span> <input type="hidden"
											id="sp_scopeId" name="scopeId"
											value="<s:property value="sp.scopeId"/>"> <input readonly="readonly"
											type="text" size="50" maxlength="127" id="sp_scopeName"
											name="scopeName"
											changelog="<s:property value="sp.scopeName"/>" title=""
											class="form-control"
											value="<s:property value="sp.scopeName"/>" /> <span
											class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showScope()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
								<!-- 位置类型 -->
						
							</div>
							<div id="fieldItemDiv"></div>
							<br> <input type="hidden" name="spStateJson"
								id="spStateJson" value="" /> <input type="hidden"
								id="id_parent_hidden"
								value="<s:property value="sp.id"/>" /> <input
								type="hidden" id="fieldContent" name="fieldContent"
								value="<s:property value="sp.fieldContent"/>" />
						</form>
					</div>
					<!--table表格-->
					<div class="HideShowPanel">
						<table
							class="table table-hover table-striped table-bordered table-condensed"
							id="spStateTable" style="font-size: 14px;">
						</table>
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="button" class="btn btn-primary" onclick='retu()' id="retu">返回</button>
						<button type="button" class="btn btn-primary" onclick='save()'id="save">保存</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/storage/position/newIceBox.js"></script>
</body>

</html>
