<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript"
	src="${ctx}/javascript/handleSearchForm.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/storage/common/showMainStorageAllSelectList.js"></script>


<g:LayOutWinTag buttonId="dicStorageType"
	title='<fmt:message key="biolims.common.chooseClassification"/>'
	hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
	isHasSubmit="false" functionName="yjlx" hasSetFun="true"
	documentId="studyType_id" documentName="studyType_name" />
</head>
<g:LayOutWinTag buttonId="dicKitType" title='' hasHtmlFrame="true"
	html="${ctx}/dic/type/dicTypeSelect.action" isHasSubmit="false"
	functionName="kit" hasSetFun="true" documentId="kit_id"
	documentName="kit_name" />
</head>
<body>
	<div id="markup" class="mainfullclass">
	<input type="hidden" id="id" value="" /> 
		<input type="hidden" id="dataType" value="${dataType}"> <input
			type="hidden" id="id" name="ids" />
		<table>
			<tr>
				<td nowrap><span>&nbsp;</span> <input type="button"
					id="confirmSelect"
					value="<fmt:message key="biolims.common.determineTheSelected"/>"
					onclick="javascript:setMainStorageValue();"></input></td>
				<%-- <td nowrap><label
					title="<fmt:message key="biolims.common.inventoryCoding"/>"
					class="text label">编码</label> <input type="text"
					size="15" class="input_parts text input default  false false"
					title="<fmt:message key="biolims.common.inputBeforeCan"/>"
					name="id" searchField="true" onblur="textbox_ondeactivate(event);"
					onchange="search()" onkeydown="checkKey(this);" maxlength="32" /></td> --%>
				<td valign="top" align="right" class="controlTitle" nowrap><span
					class="labelspacer">&nbsp;&nbsp;</span> <label title=""
					class="text label"><fmt:message key="biolims.common.kitName"/></label></td>
				<td nowrap><input type="text" size="15"
					class="input_parts text input default  readonlyfalse false"
					name="kit.name" maxlength="20" onkeydown="checkKey(this);"
					id="kit_name" title=""  searchField="true" onchange="search()"/> <input type="hidden"
					name="kit.id" id="kit_id" /></td>
				<td nowrap class="quicksearch_control" valign="middle"><label
					title="JDE Code" class="text label">JDE Code</label> <input type="text"
					size="15" class="input_parts text input default  false false"
					title="<fmt:message key="biolims.common.inputBeforeCan"/>"
					name="jdeCode" searchField="true"
					onblur="textbox_ondeactivate(event);" onchange="search()"
					onkeydown="checkKey(this);" maxlength="32" /></td>
				<td nowrap class="quicksearch_control" valign="middle"><label
					title="Component Cat No." class="text label">Component Cat No.</label> <input
					type="text" size="15"
					class="input_parts text input default  false false"
					title="<fmt:message key="biolims.common.inputBeforeCan"/>"
					name="searchCode" searchField="true"
					onblur="textbox_ondeactivate(event);" onchange="search()"
					onkeydown="checkKey(this);" maxlength="32" /></td>
				<%--<td valign="top" align="right" class="controlTitle" nowrap><span
					class="labelspacer">&nbsp;&nbsp;</span> <label
					title="<fmt:message key="biolims.common.type"/>" class="text label">
						<fmt:message key="biolims.common.type" />
				</label></td>
				 <td nowrap><input type="text" size="15"
					class="input_parts text input default  readonlyfalse true"
					name="studyType.name" maxlength="20" onkeydown="checkKey(this);"
					id="studyType_name"
					title="<fmt:message key="biolims.common.type"/>  " /> <input
					type="hidden" name="studyType.id" id="studyType_id"
					searchField="true" /> <img
					title='<fmt:message key="biolims.common.type"/>'
					id='dicStorageType' src='${ctx}/images/img_lookup.gif'
					class='detail' /></td> --%>
				<%--<td nowrap><span>&nbsp;</span> <label class="text label"
					title="<fmt:message key="biolims.common.inputTheQueryConditions"/>"><fmt:message
							key="biolims.common.recordNumber" /></label></td>
				 <td nowrap class="quicksearch_control" valign="middle"><input
					type="text"
					class="input_parts text input default  readonlyfalse false"
					name="limitNum" onblur="textbox_ondeactivate(event);"
					onfocus="shared_onfocus(event,this)" id="limitNum"
					title="<fmt:message key="biolims.common.inputTheRecordNumber"/>"
					style="width: 40"></td> --%>
				<td nowrap class="quicksearch_control" valign="middle"><img class="quicksearch_findimage"
					id="quicksearch_findimage" name="quicksearch_findimage"
					onclick="search()" src="${ctx}/images/quicksearch.gif"
					title="<fmt:message key="biolims.common.search"/>"> <img
					class="quicksearch_findimage" id="quicksearch_findimage"
					name="quicksearch_findimage" onclick="form_reset()"
					src="${ctx}/images/no_draw.gif"
					title="<fmt:message key="biolims.common.empty"/>"></td>
			</tr>
		</table>
		<input type='hidden' id='extJsonDataString' name='extJsonDataString'
			value='' />
		<div  id="from" ></div>
<%-- 		<g:GridTag isAutoWidth="true" height="document.body.clientHeight-30" --%>
<%-- 			col="${col}" type="${type}" title="" url="${path}" isLock="false" --%>
<%-- 			id="from" isCheck="true" /> --%>
	</div>
</body>
</html>