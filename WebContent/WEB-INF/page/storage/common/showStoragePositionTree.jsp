<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript"
	src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGridSorter.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGridColumnResizer.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGridNodeUI.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGridLoader.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGridColumns.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGrid.js"></script>
<script>
	Ext.onReady(function() {
				Ext.BLANK_IMAGE_URL = '${ctx}/images/s.gif';
				Ext.QuickTips.init();
				function reloadtree() {
					var node = treeGrid.getSelectionModel().getSelectedNode();
					if(node==null){
						treeGrid.getLoader().load(treeGrid.getRootNode(), function(treeNode) {
							treeGrid.getRootNode().expand(true);
						}, this);
					}else{
						var path = treeGrid.getSelectionModel().getSelectedNode().getPath('id');
						treeGrid.getLoader().load(treeGrid.getRootNode(), function(treeNode) {
							treeGrid.expandPath(path, 'id', function(bSucess, oLastNode) {
								treeGrid.getSelectionModel().select(oLastNode);
							});
						}, this);
					}
				}
				col = [ {
					header : '位置编码',
					dataIndex : 'id',
					width : 200
				}, {
					header : '位置名称',
					width : 200,
					dataIndex : 'name'
				}
				, {
					header : '是否占用',
					width : 200,
					dataIndex : 'storageObj'
				}
				, {
					header : '类型',
					width : 160,
					dataIndex : 'type-name'
				}, {
					header : '状态',
					width : 60,
					dataIndex : 'state-name'
				}, {
					header : '上级位置编码',
					width : 160,
					dataIndex : 'upId'
				}];

				var tbl = [
				];
				var treeGrid = new Ext.ux.tree.TreeGrid({
				        id:'treeGrid',
				        width:parent.document.body.clientWidth-50,
				        height: parent.document.body.clientHeight-80,
				        renderTo: 'markup',
				        enableDD: true,
				        autoExpandColumn: 'common',
				        columnLines:true,
				        expanded : true,  
				        columns:col,
				        root:new Ext.tree.AsyncTreeNode({  
				            id:'0',  
				            loader:new Ext.tree.TreeLoader({  
				                 dataUrl: '${path}',  
				                 listeners:{  
				                     "beforeload":function(treeloader,node)  
				                     {  
				                        treeloader.baseParams={  
				                        treegrid_id:node.id,  
				                        method:'POST'  
				                        };  
				                     }  
				                 }    
				            })  
				        }),  
				        listeners: {
				        dblclick: function(n) {
				            document.getElementById("id").value=n.attributes.id;
				            document.getElementById("leaf").value=n.attributes.leaf;
					    <%if(request.getParameter("setStoragePostion")!=null){%>
				            if(n.attributes.stateId.substring(0,1)=='1'){
				            	
				            }else{
					            alertMessage("该位置未启用。");
					            return false;
					       }	
				            if(n.attributes.typeSysCode=='3d'){
				            	if(n.attributes.isUseStr=='是'){
						            alertMessage("该低温存储位置已被占用。");
						            return false;
				            	}
					        }
						<%}%>
				            window.parent.set<%=request.getParameter("flag")%>(n.attributes.id,n.attributes.name,n.attributes);
				        }
				    }
				    });
			//	Ext.getCmp('treeGrid').getRootNode().expand(true);
			});
</script>
</head>
<body>
	<input type="hidden" id="id" value="" />
	<input type="hidden" id="leaf" value="" />
	<input type="hidden" id="tf" value="" />
	<div class="mainfullclass" id="markup"></div>
</body>
</html>