<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<script type="text/javascript">

	function setMainStorageValue(rec){
	 					window.parent.setStorageItem(rec);
	}
</script>
<script type="text/javascript">
 function search(){
	 var data = "{\"id\": \""+document.getElementById("storage_id").value+"\",\"type-id\":\"<%=request.getParameter("type")!=null?request.getParameter("type"):""%>\"}";
		var o = {start:0, limit:20,data: data};
        fromGrid.store.load({params:o});
	}
</script>
<script>
function checkKey()
{

 if(event.keyCode == 13)
	 search();

}
</script>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<body>
	<table>
		<tr>
			<td><label title="编号" class="text label">编号</label> <input
				type="text" class="input_parts text input default  false false"
				id="storage_id" title="编号" onblur="textbox_ondeactivate(event);"
				onchange="search()" size="10" onkeydown="checkKey(this);"
				maxlength="32"></input> <input type="button" value="检索"
				onclick="search();"></input></td>
		</tr>
	</table>
	<input type="hidden" id="id" value="" />
	<div id="list"
		style="margin: 0 0 0 0; OVERFLOW-X: auto; OVERFLOW: scroll">
		<g:GridTagNoLock isAutoWidth="false" width="750" col="${col}"
			height="400" type="${type}" title="${title}" url="${path}"
			callFun="setMainStorageValue" callFunRecord="true" />
	</div>

</body>
</html>