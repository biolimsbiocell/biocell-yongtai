<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html>
<html>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<head>
		<title>
			<fmt:message key="biolims.common.noTitleDocuments" />
		</title>
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
	</head>

	<body>
		<!--toolbar按钮组-->
		<div>
			<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
		</div>
		<div style="height: 14px"></div>
		<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">

		<div class="container-fluid" style="margin-top: 46px">
			<!--订单录入的form表单-->
			<div class="col-xs-12" style="padding: 0px">
				<div class="box box-info box-solid">
					<div class="box-header with-border">
						<i class="fa fa-bell-o"></i>
						<h3 class="box-title">订单录入</h3>
						<!-- <button class="btn btn-info"> 上一条</button>
					<button class="btn btn-info"> 下一条</button> -->
						<div class="box-tools pull-right" style="display: none;">
							<button type="button" class="btn btn-box-tool" id="tableRefresh" onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Action <span class="caret"></span>
							</button>
								<ul class="dropdown-menu">
									<li>
										<a href="####" onclick="printDataTablesItem()">打印</a>
									</li>
									<li>
										<a href="#####" onclick="copyDataTablesItem ()">复制表格数据</a>
									</li>
									<li>
										<a href="####" onclick="excelDataTablesItem()">导出Excel</a>
									</li>
									<li>
										<a href="####" onclick="csvDataTablesItem ()">导出CSV</a>
									</li>
									<li role="separator" class="divider"></li>
									<li>
										<a href="####" id="fixdeLeft2" onclick="fixedCol(2)">固定前两列</a>
									</li>
									<li>
										<a href="####" id="unfixde" onclick="unfixde()">解除固定</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="box-body ipadmini">
						<!--进度条-->
						<div id="wizard" class="form_wizard wizard_horizontal">
							<ul class="wizard_steps">
								<li>
									<a class="done step"> <span class="step_no">1</span>
										<span class="step_descr"> Step 1<br /> <small>角色权限</small>
								</span>
									</a>
								</li>
								<li>
									<a class="done step"> <span class="step_no">2</span>
										<span class="step_descr"> Step 2<br /> <small>人员配置</small>
								</span>
									</a>
								</li>

							</ul>
						</div>

						<!--form表单-->
						<div class="HideShowPanel">
							<form name="form1" id="form1" class="layui-form" method="post">
								<input type="hidden" changelog value="<%=request.getParameter(" bpmTaskId ")%>" /> <br>
								<div class="row">
									<!-- 角色编号 -->
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon"><fmt:message key="biolims.common.roleID"/> <img
											class='requiredimage' src='${ctx}/images/required.gif' />
										</span> <input changelog type="text" size="20" maxlength="25" id="id" name="role.id" class="form-control" value="<s:property value=" role.id "/>" />
										</div>
									</div>
									<!--角色名称-->
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon"> <fmt:message key='biolims.common.roleName'/> <img class='requiredimage'
											src='${ctx}/images/required.gif' />
										</span> <input changelog type="text" size="20" maxlength="50" name="role.name" class="form-control" value="<s:property value=" role.name "/>" />

										</div>
									</div>
									<!--说明-->
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon">
											<fmt:message key='biolims.common.explain'/>
										</span> <input class="form-control " changelog type="text" size="20" maxlength="25" name="role.note" value="<s:property value=" role.note "/>" />
										</div>
									</div>

								</div>
								<div class="row">
									<!--待处理模块-->
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon"> <fmt:message key="biolims.dashboard.todoListModular"/></span>
											<input class="form-control" type="text" size="20" readonly="readOnly" title="<s:property value=" role.modelName "/> " id="role_modelName" name="role.modelName" value="<s:property value=" role.modelName "/>" readonly="readOnly" />
											<input type="hidden" id="role_modelId" name="role.modelId" value="<s:property value=" role.modelId "/>">
											<span class="input-group-btn">
												 	<button type="button" class="btn btn-info"><i class="glyphicon glyphicon-search"></i></button>
												 </span>
										</div>
									</div>
									<!--异常处理模块-->
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon"><fmt:message key="biolims.common.exceptionHandlingModular"/></span>
											<input class="form-control" type="text" size="20" readonly="readOnly" title="<s:property value=" role.abnormalName "/> " id="role_abnormalName" name="role.abnormalName" value="<s:property value=" role.abnormalName "/>" readonly="readOnly" />
											<input type="hidden" id="role_abnormalId" name="role.abnormalId" value="<s:property value=" role.abnormalId "/>">
											<span class="input-group-btn">
												 	<button type="button" class="btn btn-info"><i class="glyphicon glyphicon-search"></i></button>
												 </span>
										</div>
									</div>
									<!--状态-->
									<div class="col-xs-4">
										<div class="input-group">

											<span class="input-group-addon"><fmt:message key='biolims.common.state'/></span>
											<select lay-ignore id="role_state" name="role.state" class="form-control">
												<option value="1" <s:if test="role.state==1">selected="selected"</s:if>>
													<fmt:message key='biolims.common.effective' />
												</option>
												<!-- 有效 -->
												<option value="0" <s:if test="role.state==0">selected="selected"</s:if>>
													<fmt:message key='biolims.common.invalid' />
												</option>
												<!-- 无效 -->
											</select>

										</div>
									</div>
								</div>
								<div style="border: 1px solid #eee;height: 1px;margin: 10px 0px;"></div>
									
								
								<div class="col-xs-6">
									<div class="box box-warning">
										<div class="box-header with-border">
											<i class="glyphicon glyphicon-leaf"></i>
											<h3 class="box-title">权限配置</h3>
										</div>
										<div class="box-body" id="xtree1">

										</div>
									</div>
									
								</div>
								<div class="col-xs-6">
									<div class="box box-danger">
										<div class="box-header with-border">
											<i class="glyphicon glyphicon-leaf"></i>
											<h3 class="box-title">工作台配置</h3>
										</div>
										<div class="box-body" id="xtree2">

										</div>
									</div>
								</div>
							</form>
						</div>
						<!--table表格-->
						<div class="HideShowPanel" style="display: none;">
							<table class="table table-hover table-striped table-bordered table-condensed" id="sampleInfoTable" style="font-size: 14px;">
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="${ctx}/lims/plugins/layui-xtree/layui-xtree.js" type="text/javascript"></script>
		<script type="text/javascript" src="${ctx}/javascript/core/role/editRole.js"></script>

	</body>

</html>