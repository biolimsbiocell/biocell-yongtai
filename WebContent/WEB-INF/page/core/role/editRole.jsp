<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
<title>biolims</title>

<link rel="stylesheet" href="${ctx}/lims/plugins/layui/css/layui.css">
<script type="text/javascript" src="${ctx}/lims/plugins/layui/layui.js"></script>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style type="text/css">
#editrights .col-xs-6 {
	margin-top: 15px;
}
</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
				<%@ include file="/WEB-INF/page/include/newToolBarNoState.jsp"%>
	</div>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 55px">
		<!--订单录入的form表单-->
		<div class="hide" id="editPower">
			<div class="row" id="editrights"
				style="margin-top: 15px; padding-left: 48px;">
				<div class="col-xs-6">
					<label> <input type="checkbox" value="1"> <fmt:message
							key='biolims.common.create' />
					</label>
				</div>
				<div class="col-xs-6">
					<label> <input type="checkbox" value="2"> <fmt:message
							key='biolims.common.edit' />
					</label>
				</div>
				<div class="col-xs-6">
					<label> <input type="checkbox" value="3"> <fmt:message
							key='biolims.common.submit' />
					</label>
				</div>
				<div class="col-xs-6">
					<label> <input type="checkbox" value="4"> <fmt:message
							key='biolims.common.delete' />
					</label>
				</div>
				<div class="col-xs-6">
					<label> <input type="checkbox" value="5"> <fmt:message
							key='biolims.common.retrieve' />
					</label>
				</div>
				<div class="col-xs-6">
					<label> <input type="checkbox" value="6"> <fmt:message
							key='biolims.role.other' />
					</label>
				</div>
				<div class="col-xs-6">
					<label> <input type="checkbox" value="7"> <fmt:message
							key='biolims.common.state' />
					</label>
				</div>
				<div class="col-xs-6">
					<label> <input type="checkbox" value="8"> <fmt:message
							key='biolims.role.copy' />
					</label>
				</div>
				<div class="col-xs-6">
					<label> <input type="checkbox" value="9"> <fmt:message
							key='biolims.common.print' />
					</label>
				</div>
			</div>
		</div>

	</div>
	<div class="col-xs-12" style="padding: 0px">
		<div class="box box-info box-solid">
			<div class="box-header with-border">
				<i class="fa fa-bell-o"></i>
				<h3 class="box-title">
					<fmt:message key='biolims.role.manager' />
				</h3>
				<div class="box-tools pull-right" style="display: none;">
					<button type="button" class="btn btn-box-tool" id="tableRefresh"
						onclick="tableRefreshItem()">
						<i class="glyphicon glyphicon-refresh"></i>
					</button>
					<div class="btn-group">
						<button type="button" class="btn btn-default dropdown-toggle"
							data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Action <span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							<li><a href="####" onclick="$('.buttons-print').click();"><fmt:message
										key="biolims.common.print" /></a></li>
							<li><a href="#####" onclick="$('.buttons-copy').click();"><fmt:message
										key="biolims.common.copyData" /></a></li>
							<li><a href="####" onclick="$('.buttons-excel').click();"><fmt:message
										key="biolims.common.export" /></a></li>
							<li><a href="####" onclick="$('.buttons-csv').click();"><fmt:message
										key="biolims.common.exportCSV" /></a></li>
							<li role="separator" class="divider"></li>
							<li><a href="####" id="fixdeLeft2" onclick="fixedCol(2)"><fmt:message
										key="biolims.common.lock2Col" /></a></li>
							<li><a href="####" id="unfixde" onclick="unfixde()"><fmt:message
										key="biolims.common.cancellock2Col" /></a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="box-body ipadmini">
				<!--进度条-->
				<div id="wizard" class="form_wizard wizard_horizontal">
					<ul class="wizard_steps">
						<li><a class="done step"> <span class="step_no">1</span>
								<span class="step_descr"> Step 1<br /> <small><fmt:message
										key="biolims.role.roleRight" /></small>
							</span>
						</a></li>
						<li><a class="done step"> <span class="step_no">2</span>
								<span class="step_descr"> Step 2<br /> <small><fmt:message
										key="biolims.role.userRole" /></small>
							</span>
						</a></li>

					</ul>
				</div>

				<!--form表单-->
				<form name="form1" id="form1" class="layui-form" method="post">
					<input type="hidden" changelog
						value="<%=request.getParameter(" bpmTaskId ")%>" /> <br>
					<div class="row">
						<!-- 角色编号 -->
						<div class="col-xs-4">
							<div class="input-group">
								<span class="input-group-addon"><fmt:message
										key="biolims.common.roleID" /> <img class='requiredimage'
									src='${ctx}/images/required.gif' /> </span> <input type="text"
									size="20" maxlength="25" id="id" name="id" class="form-control"
									value="<s:property value=" role.id "/>" />
							</div>
						</div>
						<!--角色名称-->
						<div class="col-xs-4">
							<div class="input-group">
								<span class="input-group-addon"> <fmt:message
										key='biolims.common.roleName' /> <img class='requiredimage'
									src='${ctx}/images/required.gif' />
								</span> <input changelog type="text" size="20" maxlength="50"
									name="name" class="form-control"
									value="<s:property value=" role.name "/>" />

							</div>
						</div>
						<!--说明-->
						<div class="col-xs-4">
							<div class="input-group">
								<span class="input-group-addon"> <fmt:message
										key='biolims.common.explain' />
								</span> <input class="form-control " changelog type="text" size="20"
									maxlength="25" name="note"
									value="<s:property value=" role.note "/>" />
							</div>
						</div>

					</div>
					<div class="row">
						<!--待处理模块-->
						<div class="col-xs-4">
							<div class="input-group">
								<span class="input-group-addon"> <fmt:message
										key="biolims.dashboard.todoListModular" /></span> <input
									class="form-control" type="text" size="20" readonly="readOnly"
									title="<s:property value=" role.modelName "/> "
									id="role_modelName" name="modelName"
									value="<s:property value=" role.modelName "/>"
									readonly="readOnly" /> <input type="hidden" id="role_modelId"
									name="modelId" value="<s:property value=" role.modelId "/>">
								<span class="input-group-btn">
									<button type="button" class="btn btn-info" onclick="dbsy()">
										<i class="glyphicon glyphicon-search"></i>
									</button>
								</span>
							</div>
						</div>
						<!--异常处理模块-->
						<div class="col-xs-4">
							<div class="input-group">
								<span class="input-group-addon"><fmt:message
										key="biolims.common.exceptionHandlingModular" /></span> <input
									class="form-control" type="text" size="20" readonly="readOnly"
									title="<s:property value=" role.abnormalName "/> "
									id="role_abnormalName" name="abnormalName"
									value="<s:property value=" role.abnormalName "/>"
									readonly="readOnly" /> <input type="hidden"
									id="role_abnormalId" name="abnormalId"
									value="<s:property value=" role.abnormalId "/>"> <span
									class="input-group-btn">
									<button type="button" class="btn btn-info" onclick="yccl()">
										<i class="glyphicon glyphicon-search"></i>
									</button>
								</span>
							</div>
						</div>
						<!--状态-->
						<div class="col-xs-4">
							<div class="input-group">

								<span class="input-group-addon"><fmt:message
										key='biolims.common.state' /></span> <select lay-ignore
									id="role_state" name="state" class="form-control">
									<option value="1"
										<s:if test="role.state==1">selected="selected"</s:if>>
										<fmt:message key='biolims.common.effective' />
									</option>
									<!-- 有效 -->
									<option value="0"
										<s:if test="role.state==0">selected="selected"</s:if>>
										<fmt:message key='biolims.common.invalid' />
									</option>
									<!-- 无效 -->
								</select>

							</div>
						</div>
					</div>
				</form>
				<div style="border: 1px solid #eee; height: 1px; margin: 10px 0px;"></div>

				<div class="HideShowPanel">
					<form class="layui-form">
						<div class="col-xs-6">
							<div class="box box-warning">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title"><fmt:message key='biolims.role.setRole' /></h3>
								</div>
								<div class="box-body" id="xtree1" style="min-height: 150px;">
									<div class="animal"
										style="margin: 75px auto; text-align: center;">
										<span
											class="layui-icon layui-anim layui-anim-rotate layui-anim-loop"
											style="font-size: 50px; color: rgb(0, 150, 136); font-weight: bold;">&#xe63e;</span>
									</div>
								</div>
							</div>

						</div>
						<div class="col-xs-6">
							<div class="box box-danger">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title"><fmt:message key='biolims.role.setDashboard' /></h3>
								</div>
								<div class="box-body" id="xtree2" style="min-height: 150px;">
									<div class="animal"
										style="margin: 75px auto; text-align: center;">
										<span
											class="layui-icon layui-anim layui-anim-rotate layui-anim-loop"
											style="font-size: 50px; color: rgb(0, 150, 136); font-weight: bold;">&#xe63e;</span>
									</div>

								</div>
							</div>
						</div>
						<input type="hidden" id="changeLog" name="changeLog" /> 
						
					</form>
				</div>
				<!--table表格-->
				<div class="HideShowPanel" style="display: none;">
					<table
						class="table table-hover table-striped table-bordered table-condensed"
						id="rolesTable" style="font-size: 14px;">
					</table>
				</div>
			</div>
		</div>
	</div>
	</div>
	<script src="${ctx}/lims/plugins/layui-xtree/layui-xtree.js"
		type="text/javascript"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/core/role/editRole.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/common/dataTablesExtend.js"></script>

</body>

</html>