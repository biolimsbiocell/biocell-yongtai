<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<g:HandleDataExtTag hasConfirm="false" paramsValue="roleId:'${roleId}'" funName="modifyData" url="${ctx}/core/role/saveRoleUser.action"/>
<g:HandleDataExtTag funName="delData" url="${ctx}/core/role/delRoleUser.action" />
<g:LayOutWinTag title="选择人员" width="780" height="500" hasHtmlFrame="true"
	html="${ctx}/core/user/showUserListAllSelect.action"
	isHasSubmit="false" isHasButton="false"
	functionName="showUserList" />	
	<script type="text/javascript">

	function addUserGrid(record){

		 var fields = ['user-name','user-id','user-department-name','user-dicJob-name'];
		 var fieldsfrom=['name','id','department-name','dicJob-name'];
			
		 addgridfromByIdName(gridGrid,record,fields,fieldsfrom,'user-id');

		}
	
	</script>
</head>
<body>

<div  class="mainlistclass" id="markup">

<input type="hidden" id="id" value="" />
<script language = "javascript" >
var gridGrid; 

Ext.onReady(function(){
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
	Ext.grid.RowNumberer = Ext.extend(Ext.grid.RowNumberer, {	width : 30,	renderer:function(value, cellmeta, record, rowIndex, columnIndex, store){	return store.lastOptions.params.start + rowIndex + 1;}});
var store = new Ext.data.JsonStore({
root: 'results',
totalProperty: 'total',
remoteSort: true,
fields: [{name:'id',type: 'string'},{name:'user-id',type: 'string'},{name:'user-name',type: 'string'},{name:'user-department-name',type: 'string'},{name:'user-dicJob-name',type: 'string'}],
proxy: new Ext.data.HttpProxy({url: '${ctx}/core/role/showRoleUserListJson.action?roleId=${roleId}',method: 'POST'})
});
gridGrid = new Ext.grid.EditorGridPanel({
autoWidth:true,
height:document.body.clientHeight-120,
title:'',
autoExpandColumn: 'common',
store: store,
clicksToEdit:1,
columnLines:true,
selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
trackMouseOver:true,
disableSelection:true,
loadMask: true,																														/* 用户账号 ，用户名*/															/* 姓名 */																					/* 部门 */																						/*岗位*/
columns: [new Ext.grid.RowNumberer(),{dataIndex:'id',header: 'ID',width: 150,sortable: true,hidden: true},{dataIndex:'user-id',header: biolims.user.id,width: 150,sortable: true},{dataIndex:'user-name',header: biolims.common.sname,width: 150,sortable: true},{dataIndex:'user-department-name',header: biolims.equipment.departmentName,width: 150,sortable: true},{dataIndex:'user-dicJob-name',header: biolims.user.post,width: 150,sortable: true,id: 'common'}],
stripeRows: true,
viewConfig: {
forceFit:true,
enableRowBody:true
},
tbar: [
{text: biolims.common.fillDetail,//'填加明细'
iconCls:'add',
handler : function(){showUserList()}
}
,'-',
{id:'saveAllModify',text: biolims.common.save, handler : function(){
var record = gridGrid.store.getModifiedRecords();  if(!record || record==''||record.length==0){return true;} else {	var selData='' ;for ( var ij=0;ij<record.length;ij++){ if(record[ij].get('id')!='-1000'){ var array = record[ij].data;selData = selData+Ext.util.JSON.encode(array)+',';}}if(selData.length>0){selData = selData.substring(0,selData.length-1);}
var data = '['+selData+']';
data = rpLsp(data);
var params = "data:'"+data+"'";
modifyData(params);}gridGrid.store.commitChanges();return true;}},'-',
{text: biolims.common.uncheck, handler : function(){
var record = gridGrid.getSelectionModel().clearSelections();  
}},'-',
{text: biolims.common.delSelected,
      handler: function(){
gridGrid.stopEditing();
var record = gridGrid.getSelectionModel().getSelections();  if(!record || record==''||record.length==0){Ext.MessageBox.alert(biolims.common.prompt, biolims.common.pleaseSelectRecord, '');} else {
   Ext.MessageBox.show( {title :biolims.common.prompt,msg :biolims.common.confirm2Del+record.length+biolims.common.record,buttons : Ext.MessageBox.OKCANCEL,
closable : false,
	fn : function(btn) {
	if (btn == 'ok') {
 for ( var ij=0;ij<record.length;ij++){ 
var id =  record[ij].get('id');if(id==undefined){record[ij].set('id','-1000');store.remove(record[ij]);	}else{  store.remove(record[ij]);		var params = "id:'"+id+"'"; delData(params);}}}}})}
 }
},
{id:'showEditColumn',hidden:true,text: biolims.common.editableColAppear, handler : function(){
var i=0;	for (i=0;i<gridGrid.getColumnModel().getColumnCount();i++)	{		if(gridGrid.getColumnModel().getColumnTooltip(i)=='可编辑' ){		gridGrid.getView().getHeaderCell(i).style.backgroundColor=RECORD_INSERT_COLOR;		}	} 
}}
],
bbar: new Ext.PagingToolbar({
pageSize: parseInt((parent.document.body.clientHeight-85)>25?(parent.document.body.clientHeight-85)/25:0),
store: store,
displayInfo: true,
displayMsg: biolims.common.displayMsg,//'当前显示 {0} - {1} 行 共 {2} 行'
beforePageText: biolims.common.page,//'页码：'
afterPageText: biolims.common.afterPageText,//'页 共 {0} 页'
emptyMsg: biolims.common.noData,//'没有可以显示的数据'
plugins : new Ext.ui.plugins.ComboPageSize({ addToItem: false, prefixText: /* '每页' */biolims.common.show, postfixText: /* '条' */biolims.common.entris})
})
});
gridGrid.render('grid');

store.load({params:{start:0, limit:parseInt((parent.document.body.clientHeight-85)>25?(parent.document.body.clientHeight-85)/25:0)}}); 
setTimeout(" Ext.getCmp('showEditColumn').handler()",1000); 	  
});
</script >
<div id='grid' style='width: 100%;' ></div>
 
<%-- <g:GridEditTag isAutoWidth="true"   height="document.body.clientHeight-120"
			col="${col}" statement="${statement}" type="${type}" title="" addMethod="showUserList()"  allButton = "true"
			url="${path}" statementLisener="${statementLisener}" handleMethod="${handlemethod}" pageSize = "parseInt((parent.document.body.clientHeight-85)>25?(parent.document.body.clientHeight-85)/25:0)"/> --%>
</div>

</body>
</html>