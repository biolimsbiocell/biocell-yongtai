<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript"
	src="${ctx}/javascript/core/user/userRoleShow.js"></script>
</head>
<body>
	<s:form name="form1" theme="simple">
		 <input type="hidden" id="userId" value="${userId}" />
		 <input	type="hidden" name="user.roles" id="user.roles"  />
		 <input	type="hidden" name="user.id" id="user.id" value="${userId}" />
		 <input	type="hidden" name="id" id="id"  /> 
		 <script language = "javascript" >
var gridGrid;
Ext.onReady(function(){
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
	Ext.grid.RowNumberer = Ext.extend(Ext.grid.RowNumberer, {	width : 30,	renderer:function(value, cellmeta, record, rowIndex, columnIndex, store){	return store.lastOptions.params.start + rowIndex + 1;}});
var store = new Ext.data.JsonStore({
root: 'results',
totalProperty: 'total',
remoteSort: true,
fields: [{name:'id',type: 'string'},{name:'name',type: 'string'},{name:'note',type: 'string'},{name:'select',type: 'string'}],
proxy: new Ext.data.HttpProxy({url: '${ctx}/core/user/userRoleShowJson.action?userId=${userId}',method: 'POST'})
});
gridGrid = new Ext.grid.GridPanel({
autoWidth:true,
id:gridGrid,
titleCollapse:true,autoScroll:true,
height:document.body.clientHeight - 120,
title:'',
store: store,
tbar: [/* 保存 */
{text: biolims.common.save,id:'saveAllModify', handler:function(){gatherPutRoleIds();}}
],
columnLines:true,
trackMouseOver:true,
loadMask: true,
colModel: new Ext.ux.grid.LockingColumnModel(/* 角色编号 */																		/* 角色名称 */																			/* 说明 */														/* 选择 */
[ new Ext.grid.RowNumberer(),       {dataIndex:'id',header: biolims.user.roleID,width: 150,sortable: true},{dataIndex:'name',header: biolims.user.roleName,width: 150,sortable: true},{dataIndex:'note',header: biolims.sample.note,width: 250,sortable: true},{dataIndex:'select',header: biolims.user.select,width: 100,sortable: true}]
)
,
stripeRows: true,
view: new Ext.ux.grid.LockingGridView(),
viewConfig: {
forceFit:true,
enableRowBody:false,
showPreview:false
},
bbar: new Ext.PagingToolbar({
id: 'bbarId',
pageSize: 1000,
store: store,
displayInfo: true,
displayMsg: biolims.common.displayMsg,//当前显示 {0} - {1} 行 共 {2} 行
beforePageText: biolims.common.page,//页码：
afterPageText: biolims.common.afterPageText,//页 共 {0} 页
emptyMsg: biolims.common.noData//没有可以显示的数据
})
});
gridGrid.render('grid');
gridGrid.getSelectionModel().on('rowselect',function(sm,rowIdx,r){
document.getElementById("id").value=r.data.id;
});
gridGrid.on('rowdblclick',function(){var record = gridGrid.getSelectionModel().getSelections(); 
document.getElementById("id").value=record[0].data.id;edit();
});
store.on('beforeload', function () { store.baseParams = {data: document.getElementById('extJsonDataString').value};  });
store.load({params:{start:0, limit:1000}});
});
function exportexcel(){
gridGrid.title=biolims.common.exportList
var vExportContent = gridGrid.getExcelXml();
var x=document.getElementById('gridhtm');
x.value=vExportContent;
document.excelfrm.submit();}
</script >
<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
<input type='hidden' id='extJsonDataString' name='extJsonDataString' value=''/>
<div id='grid'  ></div>

	</s:form> 
</body>
</html>