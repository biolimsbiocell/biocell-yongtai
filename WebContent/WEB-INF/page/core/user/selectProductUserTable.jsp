<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<title><fmt:message key="biolims.common.noTitleDocuments" /></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style>
.dataTables_scrollBody {
	min-height: 100px;
}
.tablebtns{
				float:right;
			}
.dt-buttons{
				margin:0;
				float:right;
			}
.chosed {
				background-color: #5AC8D8 !important;
				color: #fff;
			}
</style>
</head>
<body>
<input type="hidden" id="groupId" value="${requestScope.groupId}">
<input type="hidden" id="planWorkDate" value="${requestScope.planWorkDate}">
	<div id="selectUserTable"  class="ipadmini" >
		<table class="table table-hover table-bordered table-condensed"
			id="addUserTable" style="font-size: 12px;"></table>
	</div>
<script type="text/javascript" src="${ctx}/javascript/core/user/selectProductUserTable.js"></script>
</body>
</html>