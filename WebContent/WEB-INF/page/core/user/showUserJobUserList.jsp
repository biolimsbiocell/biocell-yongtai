<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>

</head>


<script type="text/javascript"> 


	function gatherPutRoleIds(){
		document.getElementById("user.userJobs").value = getCheckBox("select");
		form1.action = "${ctx}/core/user/setUserJob.action";
		form1.submit();
		
	}
 
	function getCheckBox(name){
		var gv = "";
		var vbox = document.getElementsByName(name);
		for(var i=0;i<vbox.length;i++){
			if(vbox[i].checked){
				if(gv!=""){
					gv+=","+vbox[i].value;
				}else{
					gv+=vbox[i].value;
				}
			}
		}
	//	alert(gv);
		return gv;	
	}
	
</script>



<body>
<s:form name="form1" theme="simple">

		
		<input type="hidden" id="userId" value="${userId}" />
		 <input	type="hidden" name="user.userJobs" id="user.userJobs"  />
		 <input	type="hidden" name="user.id" id="user.id" value="${userId}" />
		 <input	type="hidden" name="id" id="id"  /> 
		 <script language = "javascript" >
var gridGrid;
Ext.onReady(function(){
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
	Ext.grid.RowNumberer = Ext.extend(Ext.grid.RowNumberer, {	width : 30,	renderer:function(value, cellmeta, record, rowIndex, columnIndex, store){	return store.lastOptions.params.start + rowIndex + 1;}});
var store = new Ext.data.JsonStore({
root: 'results',
totalProperty: 'total',
remoteSort: true,
fields: [{name:'id',type: 'string'},{name:'name',type: 'string'},{name:'note',type: 'string'},{name:'select',type: 'string'}],
proxy: new Ext.data.HttpProxy({url: '${ctx}/core/user/showUserJobUserListJson.action?userId=${userId}',method: 'POST'})
});
gridGrid = new Ext.grid.GridPanel({
autoWidth:true,
id:gridGrid,
titleCollapse:true,autoScroll:true,
height:document.body.clientHeight-35,
title:'',
store: store,
tbar: [//保存
{text: biolims.common.save,id:'saveAllModify', handler:function(){gatherPutRoleIds();}}
],
columnLines:true,
trackMouseOver:true,
loadMask: true,
colModel: new Ext.ux.grid.LockingColumnModel(				/* 编号 */																			/* 名称 */																			/* 说明 */																/* 选项 */
[ new Ext.grid.RowNumberer(),       {dataIndex:'id',header: biolims.user.itemNo,width: 150,sortable: true,hidden: true},{dataIndex:'name',header: biolims.common.designation,width: 150,sortable: true},{dataIndex:'note',header: biolims.sample.note,width: 250,sortable: true},{dataIndex:'select',header: biolims.user.select,width: 100,sortable: true}]
)
,
stripeRows: true,
view: new Ext.ux.grid.LockingGridView(),
viewConfig: {
forceFit:true,
enableRowBody:false,
showPreview:false
},
bbar: new Ext.PagingToolbar({
id: 'bbarId',
pageSize: parseInt((document.body.clientHeight-35)>0?(document.body.clientHeight-35)/24:1),
store: store,
displayInfo: true,
displayMsg: biolims.common.displayMsg,//当前显示 {0} - {1} 行 共 {2} 行
beforePageText: biolims.common.page,//'页码：'
afterPageText: biolims.common.afterPageText,//'页 共 {0} 页'
emptyMsg: biolims.common.noData,//'没有可以显示的数据 '
plugins : new Ext.ui.plugins.ComboPageSize({ addToItem: false, prefixText: /* '每页' */biolims.common.show, postfixText: /* '条' */biolims.common.entris})
})
});
gridGrid.render('grid');
gridGrid.getSelectionModel().on('rowselect',function(sm,rowIdx,r){
document.getElementById("id").value=r.data.id;
});
gridGrid.on('rowdblclick',function(){var record = gridGrid.getSelectionModel().getSelections(); 
document.getElementById("id").value=record[0].data.id;edit();
});
store.on('beforeload', function () { store.baseParams = {data: document.getElementById('extJsonDataString').value};  });
store.load({params:{start:0, limit:parseInt((document.body.clientHeight-35)>0?(document.body.clientHeight-35)/24:1)}});
});
function exportexcel(){
gridGrid.title=biolims.common.exportList//'导出列表'
var vExportContent = gridGrid.getExcelXml();
var x=document.getElementById('gridhtm');
x.value=vExportContent;
document.excelfrm.submit();}
</script >
<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
<input type='hidden' id='extJsonDataString' name='extJsonDataString' value=''/>
<div id='grid'  ></div>
<div id='gridcontainer'  ></div>
		 
			<%-- <g:GridTag isAutoWidth="true" col="${col}"
			type="${type}" title="" url="${path}"  isHasTbar="true" tbar="{text: '保存',id:'saveAllModify', handler:function(){gatherPutRoleIds();}}"/> --%>
		 
		
		
	</s:form> 


</body>
</html>