<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript"
	src="${ctx}/javascript/core/user/userCertShow.js"></script>
</head>
<g:HandleDataExtTag hasConfirm="false" paramsValue="userId:'${userId}'" funName="modifyData" url="${ctx}/core/user/addUserCert.action"/>
<g:HandleDataExtTag funName="delData" url="${ctx}/core/user/delUserCert.action" />
<body>
	<input type="hidden" id="userId" value="${userId}" />
	<input	type="hidden" id="id" value="" />
	<script language = "javascript" >
var gridGrid; 
var cob = new Ext.form.ComboBox({ displayField : 'name',valueField : 'id',typeAhead : true,readonly: true,mode : 'local',editable :false ,triggerAction: 'all',store : new Ext.data.SimpleStore({fields : ['id','name'], data :  [['402880fe584863080158486dab370002',biolims.user.day/* 天 */],['402880fe584863080158486dab2d0000',biolims.user.week/* 周 */],['402880fe584863080158486dab360001',biolims.user.month/* 月 */]]   })});var remark = new Ext.form.TextArea({allowBlank: true,maxLength :200,maxLengthText :biolims.master.isNotGreater200/* 不能大于200个字符! */});
Ext.onReady(function(){
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
	Ext.grid.RowNumberer = Ext.extend(Ext.grid.RowNumberer, {	width : 30,	renderer:function(value, cellmeta, record, rowIndex, columnIndex, store){	return store.lastOptions.params.start + rowIndex + 1;}});
var store = new Ext.data.JsonStore({
root: 'results',
totalProperty: 'total',
remoteSort: true,
fields: [{name:'name',type: 'string'},{name:'no',type: 'string'},{name:'sendOrg',type: 'string'},{name:'sendDate',type: 'date',dateFormat:'Y-m-d'},{name:'expireTime'},{name:'dicUnit-id',type: 'string'},{name:'remark',type: 'string'},{name:'id',type: 'string'}],
proxy: new Ext.data.HttpProxy({url: '${ctx}/core/user/userCertShowJsonByQuery.action?userId=${userId}',method: 'POST'})
});
gridGrid = new Ext.grid.EditorGridPanel({
autoWidth:true,
height:document.body.clientHeight-70,
title:'',
autoExpandColumn: 'common',
store: store,
clicksToEdit:1,
columnLines:true,
selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
trackMouseOver:true,
disableSelection:true,
loadMask: true,													/* 证书名称 */																																										/* 证书编号 */																																											/* 发证机关 */																																									/* 发证时间 */																																																/* 有效期 */																																																					/* 有效期单位 */																																										/* 备注 */
columns: [new Ext.grid.RowNumberer(),{dataIndex:'name',header: biolims.user.certificateName,width: 150,sortable: true,hidden: false,editable: true,tooltip: biolims.common.editable,editor: new Ext.form.TextField({allowBlank: true})},{dataIndex:'no',header: biolims.user.certificateNo,width: 150,sortable: true,hidden: false,editable: true,tooltip: biolims.common.editable,editor: new Ext.form.TextField({allowBlank: true})},{dataIndex:'sendOrg',header: biolims.user.issuer,width: 75,sortable: true,hidden: false,editable: true,tooltip: biolims.common.editable,editor: new Ext.form.TextField({allowBlank: true})},{dataIndex:'sendDate',header: biolims.user.issueDate,width: 150,sortable: true,hidden: false,editable: true,renderer: formatDate,tooltip: biolims.common.editable,editor: new Ext.form.DateField({format: 'Y-m-d'})},{dataIndex:'expireTime',header: biolims.sanger.expiryDate,width: 75,sortable: true,hidden: false,editable: true,tooltip: biolims.common.editable,editor: new Ext.form.NumberField({allowBlank: true, allowNegative: false,maxValue: 100000})},{dataIndex:'dicUnit-id',header: biolims.user.validityUnit,width: 75,sortable: true,hidden: false,editable: true,renderer: Ext.util.Format.comboRenderer(cob),tooltip: biolims.common.editable,editor: cob},{dataIndex:'remark',header: biolims.common.note,width: 150,sortable: false,hidden: false,id: 'common',editable: true,tooltip: '可编辑',editor: remark},{dataIndex:'id',header: 'id',width: 150,sortable: false,hidden: true}],
stripeRows: true,
viewConfig: {
forceFit:true,
enableRowBody:true
},
tbar: [
{text: biolims.common.fillDetail,//'填加明细'
iconCls:'add',
handler : function(){
var Ob = gridGrid.getStore().recordType;
var p = new Ob({
});
gridGrid.stopEditing();
store.insert(0, p);
gridGrid.startEditing(0, 0);
}
}
,'-',
{text: biolims.common.delSelected,//'删除选中'
      handler: function(){
gridGrid.stopEditing();
var record = gridGrid.getSelectionModel().getSelections();  if(!record || record==''||record.length==0){Ext.MessageBox.alert(biolims.common.prompt,biolims.common.pleaseSelectRecord, '');} else {
   Ext.MessageBox.show( {title :/* '提示' */biolims.common.prompt,msg :/* '确认删除' */biolims.common.confirm2Del+record.length+/* '记录吗' */biolims.common.record,buttons : Ext.MessageBox.OKCANCEL,
closable : false,
	fn : function(btn) {
	if (btn == 'ok') {
 for ( var ij=0;ij<record.length;ij++){ 
var id =  record[ij].get('id');if(id==undefined){record[ij].set('id','-1000');store.remove(record[ij]);	}else{  store.remove(record[ij]);		var params = "id:'"+id+"'"; delData(params);}}}}})}
 }
},
{id:'showEditColumn',hidden:true,text: /* 显示可编辑列 */biolims.common.editableColAppear, handler : function(){
var i=0;	for (i=0;i<gridGrid.getColumnModel().getColumnCount();i++)	{		if(gridGrid.getColumnModel().getColumnTooltip(i)==biolims.common.editable ){		gridGrid.getView().getHeaderCell(i).style.backgroundColor=RECORD_INSERT_COLOR;		}	} 
}}
],
bbar: new Ext.PagingToolbar({
pageSize: parseInt((document.body.clientHeight-70)>25?(document.body.clientHeight-70)/25:0),
store: store,
displayInfo: true,
displayMsg: biolims.common.displayMsg,//当前显示 {0} - {1} 行 共 {2} 行
beforePageText: biolims.common.page,//页码：
afterPageText: biolims.common.afterPageText,//页 共 {0} 页
emptyMsg: biolims.common.noData,//没有可以显示的数据
plugins : new Ext.ui.plugins.ComboPageSize({ addToItem: false, prefixText: /*每页 */biolims.common.show, postfixText: /*条*/biolims.common.entris})
})
});
gridGrid.render('grid');

store.load({params:{start:0, limit:parseInt((document.body.clientHeight-70)>25?(document.body.clientHeight-70)/25:0)}}); 
setTimeout(" Ext.getCmp('showEditColumn').handler()",1000); 	  
});
</script >
<div id='grid' style='width: 100%;' ></div>
	<%-- 
	 <g:GridEditTag isAutoWidth="true"	col="${col}" statement="${statement}" type="${type}" title="" url="${path}" statementLisener="${statementLisener}" handleMethod="${handlemethod}" /> --%>
</body>
</html>