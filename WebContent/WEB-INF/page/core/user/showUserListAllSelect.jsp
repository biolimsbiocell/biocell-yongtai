<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<script type="text/javascript" src="${ctx}/javascript/core/user/showUserListAllSelect.js"></script>
</head>
<body>
	<div id="markup" class="mainfullclass">
    <input type="hidden" id="id"  value="" />
    	<table>
    		<tr>
    			<td>
    				<label	title="ID" class="text label"><fmt:message key="biolims.common.userName"/></label>
    				<input type="text"	class="input_parts text input default  false false"		name="id" id="user_id" title="ID" onblur="textbox_ondeactivate(event);"  searchField="true"  size="10"  onkeydown="checkKey(this);"
					maxlength="32" searchField="true"></input>
					<input type="button" id="confirmSelect" value="<fmt:message key="biolims.common.determineTheSelected"/>" onclick="javascript:setUserValue();"></input>										
				</td>
				<td nowrap>
					<label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>"><fmt:message key="biolims.common.recordNumber"/></label>
				</td>
				<td nowrap class="quicksearch_control" valign="middle">
					<input type="text"	class="input_parts text input default  readonlyfalse false" name="limitNum"	onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)"	id="limitNum" title="<fmt:message key="biolims.common.inputTheRecordNumber"/>" style="width: 40">
					<img class="quicksearch_findimage" id="quicksearch_findimage" name="quicksearch_findimage"	onclick="search()" src="${ctx}/images/quicksearch.gif" title="搜索">
					<img class="quicksearch_findimage" id="quicksearch_findimage" name="quicksearch_findimage"	onclick="form_reset()" src="${ctx}/images/no_draw.gif" title="清空">
				</td>	
			</tr>
		</table>
		<input type="hidden" id="id" value="" />
		
 		 		<script language = "javascript" >
var fromGrid;
Ext.onReady(function(){
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var sm = new Ext.grid.CheckboxSelectionModel();
	Ext.grid.RowNumberer = Ext.extend(Ext.grid.RowNumberer, {	width : 30,	renderer:function(value, cellmeta, record, rowIndex, columnIndex, store){	return store.lastOptions.params.start + rowIndex + 1;}});
var store = new Ext.data.JsonStore({
root: 'results',
totalProperty: 'total',
remoteSort: true,
fields: [{name:'id',type: 'string'},{name:'name',type: 'string'},{name:'sex',type: 'string'},{name:'birthday',type: 'date',dateFormat:'Y-m-d'},{name:'mobile',type: 'string'},{name:'inDate',type: 'date',dateFormat:'Y-m-d'},{name:'department-id',type: 'string'},{name:'department-name',type: 'string'}],
proxy: new Ext.data.HttpProxy({url: ctx+'/core/user/showUserListAllSelectJson.action',method: 'POST'})
});
fromGrid = new Ext.grid.GridPanel({
autoWidth:true,
id:fromGrid,
titleCollapse:true,autoScroll:true,
height:document.body.clientHeight,
title:'',
store: store,
tbar: [

],
columnLines:true,
trackMouseOver:true,
loadMask: true,
columns:
[ new Ext.grid.RowNumberer(),       sm,{dataIndex:'id',header: '用户账号',width: 80,sortable: true},{dataIndex:'name',header: '姓名',width: 150,sortable: true},{dataIndex:'sex',header: '性别',width: 75,sortable: true},{dataIndex:'birthday',header: '生日',width: 100,sortable: true,renderer: Ext.util.Format.dateRenderer('Y-m-d')},{dataIndex:'mobile',header: '手机',width: 150,sortable: true},{dataIndex:'inDate',header: '入职日期',width: 100,sortable: true,renderer: Ext.util.Format.dateRenderer('Y-m-d')},{dataIndex:'department-id',header: '部门ID',width: 100,sortable: true},{dataIndex:'department-name',header: '部门',width: 150,sortable: true}]
,
stripeRows: true,
selModel:sm,
viewConfig: {
forceFit:true,
enableRowBody:false,
showPreview:false
},
bbar: new Ext.PagingToolbar({
id: 'bbarId',
pageSize: parseInt((document.body.clientHeight)>0?(document.body.clientHeight)/24:1),
store: store,
displayInfo: true,
displayMsg: '当前显示 {0} - {1} 行 共 {2} 行',
beforePageText: '页码：',
afterPageText: '页 共 {0} 页',
emptyMsg: '没有可以显示的数据 ',
plugins : new Ext.ui.plugins.ComboPageSize({ addToItem: false, prefixText: '每页', postfixText: '条'})
})
});
fromGrid.render('from');
fromGrid.getSelectionModel().on('rowselect',function(sm,rowIdx,r){
document.getElementById("id").value=r.data.id;
});
fromGrid.on('rowdblclick',function(){var record = fromGrid.getSelectionModel().getSelections(); 
document.getElementById("id").value=record[0].data.id;edit();
});
store.on('beforeload', function () { store.baseParams = {data: document.getElementById('extJsonDataString').value};  });
store.load({params:{start:0, limit:parseInt((document.body.clientHeight)>0?(document.body.clientHeight)/24:1)}});
});
function exportexcel(){
fromGrid.title='导出列表'
var vExportContent = fromGrid.getExcelXml();
var x=document.getElementById('gridhtm');
x.value=vExportContent;
document.excelfrm.submit();}
</script >
<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
<input type='hidden' id='extJsonDataString' name='extJsonDataString' value=''/>
<div id='from'  ></div>
<div id='fromcontainer'  ></div>
	
	</div>

</body>
</html>