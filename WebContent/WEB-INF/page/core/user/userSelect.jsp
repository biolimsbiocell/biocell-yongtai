<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<script type="text/javascript">
	function setvalue(id,name){
		 <%if(request.getParameter("flag")!=null){%>
		 	window.parent.set<%=request.getParameter("flag")%>(id,name);
		 <%}%>
	}
	function view(){
		if(trim(document.getElementById('id').value)==''){
			message(biolims.common.selectRecord);
			}else{
			openDialog(window.ctx+'/core/user/toViewUser.action?noButton=true&id='+document.getElementById('id').value);  
			}
		}
</script>
<body>
	<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
	<g:LayOutWinTag  buttonId="showDepartment" title="选择部门" 
	hasHtmlFrame="true" html="${ctx}/core/department/departmentSelect.action"
	isHasSubmit="false" functionName="departmentselect" hasSetFun="true"
	documentId="department_id"
	documentName="department_name" />
	<div class="mainfullclass" id="markup">
		<table cellspacing="0" cellpadding="0" class="toolbarsection">
			<tr>
				<td>&nbsp;</td>
				<td nowrap><span>&nbsp;</span> <label class="text label" title=""> <fmt:message key="biolims.user.userId"/> </label></td>
				<td>&nbsp;</td>
				<td nowrap class="quicksearch_control" valign="middle"><input type="text"
					class="input_parts text input default  readonlyfalse false" name="id" onblur="textbox_ondeactivate(event);"
					onfocus="shared_onfocus(event,this)" searchField="true" id="id" title=""></td>
				<td nowrap><span>&nbsp;</span> <label class="text label" title=""> <fmt:message key="biolims.common.sname"/></label></td>
				<td>&nbsp;</td>
				<td nowrap class="quicksearch_control" valign="middle"><input type="text"
					class="input_parts text input default  readonlyfalse false" name="name" onblur="textbox_ondeactivate(event);"
					searchField="true" onfocus="shared_onfocus(event,this)" id="name" title=""></td>
				<td>&nbsp;</td>
<!-- 				<td  valign="top" align="right"><span>&nbsp;</span> -->
<!-- 						<label class="text label" title="输入查询条件"> -->
<!-- 						部门</label></td> -->
		
<!-- 						<td nowrap class="quicksearch_control" valign="middle"> -->
<!-- 						<input type="hidden" name = "department.id" id="department_id" searchField="true"> -->
<!-- 						<input type="text"	class="input_parts text input default  readonlyfalse false" -->
<!-- 							name="department.name" onblur="textbox_ondeactivate(event);"  -->
<!-- 							onfocus="shared_onfocus(event,this)" readonly="readOnly" -->
<!-- 							id="department_name" title="输入" >  -->
<%-- 								<img  alt='选择' id='showDepartment'  src='${ctx}/images/img_lookup.gif'  class='detail'  /> --%>
<!-- 						</td> -->
				
				<td nowrap><span>&nbsp;</span> <label class="text label" title=""><fmt:message key="biolims.common.recordNum"/></label></td>
				<td>&nbsp;</td>
				<td nowrap class="quicksearch_control" valign="middle"><input type="text"
					class="input_parts text input default  readonlyfalse false" name="limitNum" onblur="textbox_ondeactivate(event);"
					onfocus="shared_onfocus(event,this)" id="limitNum" title="" style="width: 50">
					
					<input type="button" value=<fmt:message key="biolims.common.retrieve"/> onclick="javascript:commonSearchAction(gridGrid);">
					<input type="button" value=<fmt:message key="biolims.common.confirmSelected"/> onclick="javascript:gridSetSelectionValue();">
					</td>
			</tr>
		</table>
		<script language = "javascript" >
function gridSetSelectionValue(){
		var r = gridGrid.getSelectionModel().getSelected(); 
		setvalue(r.data.id,r.data.name);
}
var gridGrid;
Ext.onReady(function(){
var store = new Ext.data.JsonStore({
root: 'results',
totalProperty: 'total',
remoteSort: true,
fields: [{name:'id',type: 'string'},{name:'name',type: 'string'},{name:'birthday',type: 'date',dateFormat:'Y-m-d'},{name:'mobile',type: 'string'},{name:'inDate',type: 'date',dateFormat:'Y-m-d'},{name:'department-id',type: 'string'},{name:'department-name',type: 'string'},{name:'dicJob-name',type: 'string'}],
proxy: new Ext.data.HttpProxy({url: '${ctx}/core/user/userSelectJson.action',method: 'POST'})
});
	Ext.grid.RowNumberer = Ext.extend(Ext.grid.RowNumberer, {	width : 30,	renderer:function(value, cellmeta, record, rowIndex, columnIndex, store){	return store.lastOptions.params.start + rowIndex + 1;}});
gridGrid = new Ext.grid.GridPanel({
autoWidth:true,
height:document.body.clientHeight-30,
title:'',
store: store,
columnLines:true,
trackMouseOver:true,
loadMask: true,
columns:[new Ext.grid.RowNumberer(),{dataIndex:'id',header: biolims.user.userId,width: 80,sortable: true},{dataIndex:'name',
			header : biolims.common.sname,
												width : 100,
												sortable : true
											},
											{
												dataIndex : 'birthday',
												header :biolims.common.birthDay,
												width : 100,
												sortable : true,
												renderer : Ext.util.Format
														.dateRenderer('Y-m-d')
											},
											{
												dataIndex : 'mobile',
												header :biolims.user.mobile,
												width : 100,
												sortable : true
											},
											{
												dataIndex : 'inDate',
												header : biolims.user.inDate,
												width : 100,
												sortable : true,
												renderer : Ext.util.Format
														.dateRenderer('Y-m-d')
											}, {
												dataIndex : 'department-id',
												header :biolims.equipment.departmentId,
												width : 100,
												sortable : true,
												hidden : true
											}, {
												dataIndex : 'department-name',
												header : biolims.equipment.departmentName,
												width : 150,
												sortable : true
											}, {
												dataIndex : 'dicJob-name',
												header :biolims.user.post,
												width : 100,
												sortable : true,
												id : 'common'
											} ],
									stripeRows : true,
									bbar : new Ext.PagingToolbar(
											{
												pageSize : parseInt((document.body.clientHeight) > 0 ? (document.body.clientHeight) / 25
														: 1),
												store : store,
												displayInfo : true,
												displayMsg :biolims.common.displayMsg,
												beforePageText :biolims.common.page,
												afterPageText :biolims.common.afterPageText,
												emptyMsg :biolims.common.noData
											})
								});
						gridGrid.render('grid');
						gridGrid.getSelectionModel().on('rowselect',
								function(sm, rowIdx, r) {
								});
						gridGrid.on('rowdblclick', function() {
							gridSetSelectionValue();
						});
						store
								.on(
										'beforeload',
										function() {
											store.baseParams = {
												data : document
														.getElementById('extJsonDataString').value
											};
										});
						store
								.load({
									params : {
										start : 0,
										limit : parseInt((document.body.clientHeight) > 0 ? (document.body.clientHeight) / 25
												: 1)
									}
								});
					});
		</script >
<input type='hidden'  id='extJsonDataString'  name='extJsonDataString' value=''/>
<div id='grid' ></div>
		
		<%-- <g:GridTagNoLock isAutoWidth="true" col="${col}" height="document.body.clientHeight-30" type="${type}" title=""
			url="${path}" /> --%>
	</div>
</body>
</html>