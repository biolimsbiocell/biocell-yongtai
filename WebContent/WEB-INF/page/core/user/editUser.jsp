<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>biolims</title>
<style>
.col-xs-4 {
	margin-bottom: 6px;
}
#btn_submit{
  display:none;
}
#btn_changeState{
  display:none;
}
</style>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<link rel="stylesheet" href="${ctx}/lims/plugins/layui/css/layui.css">
		<script type="text/javascript" src="${ctx}/lims/plugins/layui/layui.js"></script>
<body>
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div style="height: 14px"></div>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 46px">
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<i class="glyphicon glyphicon-pencil"></i><fmt:message key="biolims.common.userManagement"/>
					</h3>

					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="####" onclick="$('.buttons-print').click();">
										<fmt:message key="biolims.common.print" />
								</a></li>
								<li><a href="#####" onclick="$('.buttons-copy').click();">
										<fmt:message key="biolims.common.copyData" />
								</a></li>
								<li><a href="####" onclick="$('.buttons-excel').click();">Excel</a>
								</li>
								<li><a href="####" onclick="$('.buttons-csv').click();">CSV</a>
								</li>
								<li role="separator" class="divider"></li>
								<li><a href="####" onclick="itemFixedCol(2)"> <fmt:message
											key="biolims.common.lock2Col" />
								</a></li>
								<li><a href="####" id="unfixde" onclick="unfixde()"> <fmt:message
											key="biolims.common.cancellock2Col" />
								</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="box-body">
					<!--进度条-->
					<div id="wizard" class="form_wizard wizard_horizontal">
						<ul class="wizard_steps">
							<li><a class="done step"> <span class="step_no">1</span>
									<span class="step_descr"><small><fmt:message key="biolims.common.basicInformation"/></small> </span>
							</a></li>
							<li><a class="done step"> <span class="step_no">2</span>
									<span class="step_descr"><small><fmt:message key="biolims.common.userRights"/></small></span>
							</a></li>
							<li><a class="done step"> <span class="step_no">3</span>
									<span class="step_descr"><small><fmt:message key="biolims.common.staffGroup"/></small></span>
							</a></li>
							<!-- <li><a class="done step"> <span class="step_no">4</span>
									<span class="step_descr"><small>岗位</small></span>
							</a></li> -->
							<li><a class="done step"> <span class="step_no">4</span>
									<span class="step_descr"><small>健康状况</small></span>
							</a></li>
							<li><a class="done step"> <span class="step_no">5</span>
									<span class="step_descr"><small>培训情况</small></span>
							</a></li>
						</ul>
					</div>
					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
						<input type="hidden" id="userRoleJson" value="" name="userRoleJson">
						<input type="hidden" id="userGroupJson" value="" name="userGroupJson">
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.userName" /> <img class='requiredimage'
											src='${ctx}/images/required.gif' /> </span> <input type="text"
											size="50" maxlength="127" id="user_id"
											changelog="<s:property value="user.id"/>" name="user.id"
											title="" class="form-control "
											value="<s:property value="user.id"/>" />

									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.sname" />  </span> <input type="text"
											size="50" maxlength="127" id="user_name"
											changelog="<s:property value="user.name"/>" name="user.name"
											title="" class="form-control"
											value="<s:property value="user.name"/>" />

									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.dateOfBirth" /> 
										</span> <input type="text" size="50" maxlength="127"
											id="user_birthday"
											changelog="<s:date name=" user.birthday " format="yyyy-MM-dd "/>"
											name="user.birthday" title="" class="form-control"
											value="<s:date name=" user.birthday " format="yyyy-MM-dd "/>" />

									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.password" /> <img class='requiredimage'
											src='${ctx}/images/required.gif' /> </span> 
											
											
											
									<input type="password"
											class="form-control" 
											name="user.password" id="user.password" value="<s:property value="user.password"/>"
											changelog="<s:property value="user.password"/>" size="50" maxlength="127"
											title="" class="form-control"/>
											
											<input type="hidden" name="user.userPassword" id="user.userPassword" value='<s:property value="user.userPassword"/>'></input>
									</div>
								</div>
								<div class="col-xs-4">
									<input type="hidden" name="user.sex" changelog
										id="user_sex" value="<s:property value=" user.sex "/>" />
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.gender" />  </span>
										<s:if test="user.sex==1">
											<input type="checkbox" value="1" note="1" changelog checked
												title=<fmt:message key="biolims.common.male" />>
											<input type="checkbox" value="0" note="1" changelog
												title=<fmt:message key="biolims.common.female" />>
										</s:if>
										<s:if test="user.sex==0">
											<input type="checkbox" value="1" note="1" changelog
												title=<fmt:message key="biolims.common.male" />>
											<input type="checkbox" value="0" note="1" changelog checked
												title=<fmt:message key="biolims.common.female" />>
										</s:if>
										<s:if test='user.sex==null'>
											<input type="checkbox" value="1" note="1" changelog
												title=<fmt:message key="biolims.common.male" />>
											<input type="checkbox" value="0" note="1" changelog
												title=<fmt:message key="biolims.common.female" />>
										</s:if>
									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.email" />  </span> <input type="text"
											size="50" maxlength="127" id="user_email"
											changelog="<s:property value="user.email"/>"
											name="user.email" title="" class="form-control"
											value="<s:property value="user.email"/>" />

									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.contact" />  </span> <input type="text"
											size="50" maxlength="127" id="user_contactMethod"
											changelog="<s:property value="user.contactMethod"/>"
											name="user.contactMethod" title="" class="form-control"
											value="<s:property value="user.contactMethod"/>" />

									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.hredate" /> </span> <input type="text"
											size="50" maxlength="127" id="user_inDate"
											changelog="<s:date name=" user.inDate " format="yyyy-MM-dd "/>"
											name="user.inDate" title="" class="form-control"
											value="<s:date name=" user.inDate " format="yyyy-MM-dd "/>" />

									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.subordinateDepartments" /> </span> <input
											type="hidden" id="user_department_id"
											name="user.department.id"
											value="<s:property value="user.department.id"/>"> <input
											type="text" size="50" readonly="readonly" maxlength="127"
											id="user_department_name" name="user.department.name"
											changelog="<s:property value="user.department.name"/>"
											title="<fmt:message key="biolims.common.hospital"/>"
											class="form-control"
											value="<s:property value="user.department.name"/>" /> <span
											class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showDepartment()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.areaOfTheGenus" /> 
										</span> <input type="text" size="50" maxlength="127" id="user_area"
											changelog="<s:property value="user.area"/>" name="user.area"
											title="" class="form-control"
											value="<s:property value="user.area"/>" />

									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.city" /> </span> <input type="text"
											size="50" maxlength="127" id="user_city"
											changelog="<s:property value="user.city"/>" name="user.city"
											title="" class="form-control"
											value="<s:property value="user.city"/>" />

									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.iDcard" /> </span> <input type="text"
											size="50" maxlength="127" id="user_cardNo"
											changelog="<s:property value="user.cardNo"/>"
											name="user.cardNo" title="" class="form-control"
											value="<s:property value="user.cardNo"/>" />

									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.cellPhoneNumber" /> 
										</span> <input type="text" size="50" maxlength="127" id="user_mobile"
											changelog="<s:property value="user.mobile"/>"
											name="user.mobile" title="" class="form-control"
											value="<s:property value="user.mobile"/>" />

									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.addr" /> </span> <input type="text"
											size="50" maxlength="127" id="user_address"
											changelog="<s:property value="user.address"/>"
											name="user.address" title="" class="form-control"
											value="<s:property value="user.address"/>" />

									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.educationalStatus" /> 
										</span> <input type="text" size="50" maxlength="127"
											id="user_edu_name"
											changelog="<s:property value="user.edu.name"/>"
											name="user.edu.name" title="" class="form-control"
											value="<s:property value="user.edu.name"/>" /><input
											type="hidden" id="user_edu_id" name="user.edu.id"
											changelog="<s:property value="user.edu.id"/>"
											value="<s:property value="user.edu.id"/>">

									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.graduateSchool" />
										</span> <input type="text" size="50" maxlength="127"
											id="user_graduate"
											changelog="<s:property value="user.graduate"/>"
											name="user.graduate" title="" class="form-control"
											value="<s:property value="user.graduate"/>" />

									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.state" /> </span> <input type="text"
											size="50" maxlength="127" id="user_state_name"
											changelog="<s:property value="user.state.name"/>"
											name="user.state.name" title="" class="form-control"
											value="<s:property value="user.state.name"/>" /><input
											type="hidden" id="user_state_id" name="user.state.id"
											changelog="<s:property value="user.state.id"/>"
											value="<s:property value="user.state.id"/>">
											<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showState()">
												<i class="glyphicon glyphicon-search"></i>
										</span>

									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.tel" />  </span> <input type="text"
											size="50" maxlength="127" id="user_phone"
											changelog="<s:property value="user.phone"/>"
											name="user.phone" title="" class="form-control"
											value="<s:property value="user.phone"/>" />

									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.emergencyContact" /> 
										</span> <input type="text" size="50" maxlength="127"
											id="user_emergencyContactPerson"
											changelog="<s:property value="user.emergencyContactPerson"/>"
											name="user.emergencyContactPerson" title=""
											class="form-control"
											value="<s:property value="user.emergencyContactPerson"/>" />

									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.emergencyContactMethod" />
										</span> <input type="text" size="50" maxlength="127"
											id="user_emergencyContactMethod"
											changelog="<s:property value="user.emergencyContactMethod"/>"
											name="user.emergencyContactMethod" title=""
											class="form-control"
											value="<s:property value="user.emergencyContactMethod"/>" />

									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.CostCentre" /><img class='requiredimage'
											src='${ctx}/images/required.gif' />  </span> <input type="hidden"
											id="user_scopeId" name="user.scopeId"
											value="<s:property value="user.scopeId"/>"> <input readonly="readonly"
											type="text" size="50" maxlength="127" id="user_scopeName"
											name="user.scopeName"
											changelog="<s:property value="user.scopeName"/>" title=""
											class="form-control"
											value="<s:property value="user.scopeName"/>" /> <span
											class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showScope()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
							</div>
							<input type="hidden" id="changeLog" name="changeLog" /> 
							
						</form>

					</div>
					<div class="HideShowPanel" style="display: none">
						<table
							class="table table-hover table-striped table-bordered table-condensed"
							id="userRoleTable" style="font-size: 14px;">
						</table>
					</div>
					<div class="HideShowPanel" style="display: none">
						<table
							class="table table-hover table-striped table-bordered table-condensed"
							id="userGroupUserTable" style="font-size: 14px;">
						</table>
					</div>
					<div class="HideShowPanel" style="display: none">
						<table
							class="table table-hover table-striped table-bordered table-condensed"
							id="userJobTable" style="font-size: 14px;">
						</table>
					</div>
					<div class="HideShowPanel" style="display: none">
						<table
							class="table table-hover table-striped table-bordered table-condensed"
							id="userCertTable" style="font-size: 14px;">
						</table>
					</div>
					<!-- <div class="HideShowPanel">
						<table
							class="table table-hover table-striped table-bordered table-condensed"
							id="userVaccineTable" style="font-size: 14px;">
						</table>
					</div> -->
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/core/user/editUser.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/core/user/userCertShow.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/core/user/userRoleShow.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/core/user/userGroupUserShow.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/core/user/userJobUserShow.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/common/dataTablesExtend.js"></script>
</body>
</html>
