<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
 

    
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
 <script type="text/javascript">
	function setValue(rec){
		
		
		 <%if(request.getParameter("flag")!=null){%>
		 	window.parent.set<%=request.getParameter("flag")%>(rec);
		  <%}%>
	}
</script>
</head>
<body>
<div  class="mainfullclass" id="markup">

<input type="hidden" id="id" value="" /> <script language = "javascript" >
function gridSetSelectionValue(){
var r = gridGrid.getSelectionModel().getSelected(); 
setValue(r);
}
var gridGrid;
Ext.onReady(function(){
var store = new Ext.data.JsonStore({
root: 'results',
totalProperty: 'total',
remoteSort: true,
fields: [{name:'id',type: 'string'},{name:'name',type: 'string'},{name:'note',type: 'string'},{name:'state',type: 'string'}],
proxy: new Ext.data.HttpProxy({url: '${ctx}/core/userGroup/userGroupSelectJson.action',method: 'POST'})
});
	Ext.grid.RowNumberer = Ext.extend(Ext.grid.RowNumberer, {	width : 30,	renderer:function(value, cellmeta, record, rowIndex, columnIndex, store){	return store.lastOptions.params.start + rowIndex + 1;}});
gridGrid = new Ext.grid.GridPanel({
autoWidth:true,
height:document.body.clientHeight-20,
title:biolims.common.userWorkGroup,
store: store,
columnLines:true,
trackMouseOver:true,
loadMask: true,
columns:[new Ext.grid.RowNumberer(),{dataIndex:'id',header: biolims.common.id,width: 150,sortable: true},{dataIndex:'name',header: biolims.user.eduName1,width: 150,sortable: true},{dataIndex:'note',header: biolims.common.name,width: 250,sortable: true},{dataIndex:'state',header:biolims.common.state,width: 120,sortable: true}],
stripeRows: true,
bbar: new Ext.PagingToolbar({
pageSize: 1000,
store: store,
displayInfo: true,
displayMsg:biolims.common.displayMsg,
beforePageText: biolims.common.page,
afterPageText: biolims.common.afterPageText,
emptyMsg: biolims.common.noData
})
});
gridGrid.render('grid');
gridGrid.getSelectionModel().on('rowselect',function(sm,rowIdx,r){
});
gridGrid.on('rowdblclick',function(){gridSetSelectionValue();
});
store.on('beforeload', function () { store.baseParams = {data: document.getElementById('extJsonDataString').value};  });
store.load({params:{start:0, limit:1000}});
});
</script >
<input type='hidden'  id='extJsonDataString'  name='extJsonDataString' value=''/>
<div id='grid' ></div>


</div>

</body>
</html>