<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/javascript/core/userGroup/showUserGroupUserList.js"></script>
<g:HandleDataExtTag hasConfirm="false" paramsValue="userGroupId:'${userGroupId}'" funName="modifyData" url="${ctx}/core/userGroup/saveUserGroupUser.action"/>
<g:HandleDataExtTag funName="delData" url="${ctx}/core/userGroup/delUserGroupUser.action" />
<g:LayOutWinTag title="选择人员" width="780" height="500" hasHtmlFrame="true"
	html="${ctx}/core/user/showUserListAllSelect.action"
	isHasSubmit="false" isHasButton="false"
	functionName="showUserList" />	
</head>
<body>
<div  class="mainlistclass" id="markup">
<input type="hidden" id="id" value="" /> <g:GridEditTag isAutoWidth="true"   height="document.body.clientHeight-120"
			col="${col}" statement="${statement}" type="${type}" title="" addMethod="showUserList()" 
			url="${path}" statementLisener="${statementLisener}" handleMethod="${handlemethod}" pageSize = "parseInt((parent.document.body.clientHeight-85)>25?(parent.document.body.clientHeight-85)/25:0)"/>

</div>
</body>
</html>