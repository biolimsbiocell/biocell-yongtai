<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE html>
<html>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<head>
		<title>
			biolims
		</title>
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
	</head>

	<body>
		<!--toolbar按钮组-->
		<div>
				<%@ include file="/WEB-INF/page/include/newToolBarNoState.jsp"%>
		</div>
		<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
		<div class="container-fluid" style="margin-top: 55px">
			<div class="col-xs-12" style="padding: 0px">
				<div class="box box-info box-solid">
					<div class="box-header with-border">
						<i class="fa fa-bell-o"></i>
						<h3 class="box-title"><fmt:message key="biolims.common.staffGroupManagement"/></h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" id="tableRefresh" onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Action <span class="caret"></span>
							</button>
								<ul class="dropdown-menu">
									<li>
										<a href="####" onclick="printDataTablesItem()">
											<fmt:message key="biolims.common.print" />
												</a>
									</li>
									<li>
										<a href="#####" onclick="copyDataTablesItem ()">
											<fmt:message key="biolims.common.copyRecord" /></a>
									</li>
									<li>
										<a href="####" onclick="excelDataTablesItem()">导出Excel</a>
									</li>
									<li>
										<a href="####" onclick="csvDataTablesItem ()">导出CSV</a>
									</li>
									<li role="separator" class="divider"></li>
									<li>
										<a href="####" id="fixdeLeft2" onclick="fixedCol(2)">
											<fmt:message key="biolims.common.freezenColumn" />(2)</a>
									</li>
									<li>
										<a href="####" id="unfixde" onclick="unfixde()">
											<fmt:message key="biolims.common.releaseFreezen" /></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="box-body ipadmini">
						<!--form表单-->
						<form name="form1" id="form1" class="layui-form" method="post">
							<input type="hidden" changelog value="<%=request.getParameter(" bpmTaskId ")%>" /> <br>
							<div class="row">
								<!-- 人员组编号 -->
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message key="biolims.common.personnelGroupNumber"/><img
											class='requiredimage' src='${ctx}/images/required.gif' />
										</span> <input type="text" size="20" maxlength="25" id="id" name="userGroup.id" class="form-control" changelog="<s:property value=" userGroup.id "/>" value="<s:property value=" userGroup.id "/>" />
									</div>
								</div>
								<!--人员组名称-->
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message key='biolims.common.roleName'/> <img class='requiredimage'
											src='${ctx}/images/required.gif' />
										</span> <input changelog="<s:property value=" userGroup.name "/>" type="text" size="20" maxlength="50" name="userGroup.name" class="form-control" value="<s:property value=" userGroup.name "/>" />

									</div>
								</div>
								<!--说明-->
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">
											<fmt:message key='biolims.common.explain'/>
										</span> <input class="form-control"  changelog="<s:property value=" userGroup.note "/>" type="text" size="20" maxlength="25" name="userGroup.note" value="<s:property value=" userGroup.note "/>" />
									</div>
								</div>

							</div>
							<div class="row">
								<!--类型-->
								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message key="biolims.common.type"/></span>
										<select lay-ignore id="role_state" name="userGroup.type" class="form-control">
											<!--普通-->
											<option value="0" <s:if test="userGroup.type==0">selected="selected"</s:if>>
												普通
											</option>
											<!--函数-->
											<option value="1" <s:if test="userGroup.type==1">selected="selected"</s:if>>
												<fmt:message key="biolims.common.functionName"/> 
											</option>
										</select>

									</div>
								</div>

								<!--状态-->
								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message key='biolims.common.state'/></span>
										<select lay-ignore id="userGroup_state" name="userGroup.state" class="form-control">
											<option value="1" <s:if test="userGroup.state==1">selected="selected"</s:if>>
												<fmt:message key="biolims.common.effective" />
											</option>
											<option value="0" <s:if test="userGroup.state==0">selected="selected"</s:if>>
												<fmt:message key="biolims.common.invalid" />
											</option>
										</select>

									</div>
								</div>
							</div>
						</form>
						<div style="border: 1px solid #eee;height: 1px;margin: 10px 0px;"></div>
						<div class="HideShowPanel">
							<table class="table table-hover table-striped table-bordered table-condensed" id="userGroupTable" style="font-size: 14px;">
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript" 
			src="${ctx}/javascript/core/userGroup/editUserGroup.js"></script>
		<script type="text/javascript"
			src="${ctx}/javascript/common/dataTablesExtend.js"></script>

	</body>

</html>