<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<body>
<div  class="mainlistclass" id="markup">
<table width="100%">
	<tr>
		<td><input type="hidden" id="id" value="" /><g:GridTag 
			isAutoWidth="true" col="${col}" type="${type}" title="" 
			url="${path}" /></td>
	</tr>
	<tr>
		<td></td>
	</tr>
</table>
</div>
</body>
</html>