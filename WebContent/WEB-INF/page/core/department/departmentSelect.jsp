<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Bio-LIMS</title>
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
		<link rel="stylesheet" type="text/css" href="${ctx}/lims/plugins/treegrid/css/jquery.treegrid.css" />
		<link rel="stylesheet" href="${ctx}/lims/plugins/icheck/skins/square/blue.css">
		<script type="text/javascript" src="${ctx}/lims/plugins/icheck/icheck.min.js"></script>
		<script type="text/javascript" src="${ctx}/lims/plugins/treegrid/js/jquery.treegrid.min.js"></script>
		<script type="text/javascript" src="${ctx}/lims/plugins/treegrid/js/jquery.treegrid.bootstrap3.js"></script>
		<script type="text/javascript" src="${ctx}/lims/plugins/treegrid/js/jquery.treegrid.extension.js"></script>
		<script type="text/javascript" src="${ctx}/javascript/core/department/departmentquery.js"></script>
		
		<style>
			.chosed {
				background-color: #5AC8D8 !important;
				color: #fff;
			}
			
			#mytreeGrid tr {
				cursor: pointer;
			}
		</style>
	</head>
	
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="input-group">
			<span class="input-group-addon">部门</span> 
			<input type="text" size="20" maxlength="25" id="instrumentDepartmentId" name="instrumentDepartmentId" searchname="instrumentDepartmentId" class="form-control" >
		</div>
		
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="input-group">
			<input style="text-align: center;" type="button" class="form-control btn-success" onclick="query()" value="查找">
		</div>
		
	</div>
	<div id="departmentSelectTable" >
		<table class="table table-hover table-bordered table-condensed"
			id="addPatientTable" style="font-size: 12px;"></table>
	</div>
	
	<body>

		<table id="mytreeGrid"></table>

		<script type="text/javascript">
			$(document).ready(function() {
				$('#mytreeGrid').treegridData({
					id: 'id',
					parentColumn: 'parent',
					type: "GET", //请求数据的ajax类型
/* 					url: '/core/department/showProductNewListJson.action', //请求数据的ajax的url */
 					url: '/core/department/showProductNewNewListJson.action', //请求数据的ajax的url
				    ajaxParams: {}, //请求数据的ajax的data属性
					expandColumn: null, //在哪一列上面显示展开按钮
					striped: false, //是否各行渐变色
					bordered: true, //是否显示边框
					expandAll: false, //是否全部展开
					columns: [{
							title: 'ID',
							field: 'id'
						},
						{
							title: biolims.user.abbreviation,
							field: 'briefName'
						},
						{
							title: biolims.user.organizationName,
							field: 'name'
						}
					]
				});

			});
		</script>
		
	</body>
	
</html>