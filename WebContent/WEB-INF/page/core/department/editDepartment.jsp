<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE html>
<html>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<head>
		<title>
			biolims
		</title>
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
	</head>

	<body>
		<!--toolbar按钮组-->
		<div>
					<%@ include file="/WEB-INF/page/include/newToolBarNoState.jsp"%>
		</div>
		<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
		<div class="container-fluid" style="margin-top: 55px">
			<div class="col-xs-12" style="padding: 0px">
				<div class="box box-info box-solid">
					<div class="box-header with-border">
						<i class="fa fa-bell-o"></i>
						<h3 class="box-title"><fmt:message key="biolims.common.organizationManagement"/></h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" id="tableRefresh" onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Action <span class="caret"></span>
							</button>
								<ul class="dropdown-menu">
									<li>
										<a href="####" onclick="printDataTablesItem()">打印</a>
									</li>
									<li>
										<a href="#####" onclick="copyDataTablesItem ()">复制表格数据</a>
									</li>
									<li>
										<a href="####" onclick="excelDataTablesItem()">导出Excel</a>
									</li>
									<li>
										<a href="####" onclick="csvDataTablesItem ()">导出CSV</a>
									</li>
									<li role="separator" class="divider"></li>
									<li>
										<a href="####" id="fixdeLeft2" onclick="fixedCol(2)">固定前两列</a>
									</li>
									<li>
										<a href="####" id="unfixde" onclick="unfixde()">解除固定</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="box-body ipadmini">
						<!--form表单-->
						<form name="form1" id="form1" class="layui-form" method="post">
							<input type="hidden" value="<%=request.getParameter(" bpmTaskId ")%>" /> <br>
							<div class="row">
								<!-- 组织机构ID -->
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message key="biolims.common.organizationId"/><img
											class='requiredimage' src='${ctx}/images/required.gif' />
										</span> <input type="text" size="20" maxlength="25" id="id" changelog="<s:property value="department.id"/>" name="department.id" class="form-control" value="<s:property value=" department.id "/>" />
									</div>
								</div>
								<!--部门名称简称-->
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message key="biolims.common.departmentNameAbbreviation"/> <img class='requiredimage'
											src='${ctx}/images/required.gif' />
										</span> <input type="text" size="20" maxlength="50" changelog="<s:property value="department.briefName"/>" name="department.briefName" class="form-control" value="<s:property value=" department.briefName "/>" />

									</div>
								</div>
								<!--部门名称-->
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">
											 <fmt:message key="biolims.common.departmentName"/>
										</span> <input class="form-control" type="text" size="20" maxlength="25" changelog="<s:property value="department.name"/>" name="department.name" value="<s:property value=" department.name "/>" />
									</div>
								</div>

							</div>
							<div class="row">
								<!--上级组织机构ID-->
								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message key="biolims.common.superiorOrganizationId"/> </span>
										<input class="form-control" type="text" size="20" readonly="readOnly" title=" " changelog="<s:property value="department.upDepartment.name"/>" id="department_upDepartment_name"  value="<s:property value="department.upDepartment.name"/>" readonly="readOnly" />
										<input type="hidden" id="department_upDepartment_id" name="department.upDepartment.id" value="<s:property value="department.upDepartment.id"/>">
										<input type="hidden" id="department_parent" name="department.parent" value="<s:property value="department.parent"/>">
										<span class="input-group-btn">
												 	<button type="button" class="btn btn-info" onclick="choseParent()"><i class="glyphicon glyphicon-search"></i></button>
												 </span>

									</div>
								</div>

								<!--部门状态-->
								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message key="biolims.common.departmentState"/></span>
										<select lay-ignore id="department_state" name="department.state" class="form-control">
											<option value="1" <s:if test="department.state==1">selected="selected"</s:if>>
												<fmt:message key="biolims.common.effective" />
											</option>
											<option value="0" <s:if test="department.state==0">selected="selected"</s:if>>
												<fmt:message key="biolims.common.invalid" />
											</option>
										</select>

									</div>
								</div>
								<!--单位地址-->
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">
											<fmt:message key="biolims.common.addr"/>
										</span> 
										<input class="form-control" changelog="<s:property value="department.address"/>" type="text" size="20" maxlength="25" name="department.address" value="<s:property value="department.address"/>" />
									</div>
								</div>

								
							</div>
							<div class="row">
									<!--备注-->
								<div class="col-xs-6">
									<div class="input-group">
										<span class="input-group-addon">
											<fmt:message key="biolims.common.note"/>
										</span> 
										<input class="form-control" changelog="<s:property value="department.note"/>" type="text" size="20" maxlength="25" name="department.note" value="<s:property value="department.note"/>" />
									</div>
								</div>

							</div>
							<input type="hidden" id="changeLog" name="changeLog"  />
						</form>
						<div style="border: 1px solid #eee;height: 1px;margin: 10px 0px;"></div>
						<div class="HideShowPanel">
							<table class="table table-hover table-striped table-bordered table-condensed" id="departmentTable" style="font-size: 14px;">
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript" 
			src="${ctx}/javascript/core/department/editDepartment.js"></script>
		<script type="text/javascript"
			src="${ctx}/javascript/common/dataTablesExtend.js"></script>
	</body>

</html>