<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>biolims</title>
<style>
</style>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<g:LayOutWinTag buttonId="regionType" title="选择城市" hasHtmlFrame="true"
			html="${ctx}/dic/type/dicTypeSelect.action" isHasSubmit="false"
			functionName="xzcsfr" hasSetFun="true"
			documentId="contract_regionType_id"
			documentName="contract_regionType_name" />
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="searchmaincontract" title="选择主合同"
		hasHtmlFrame="true"
		html="${ctx}/contract/common/contractSelect.action" width="900"
		height="500" isHasSubmit="false" functionName="contractfun"
		hasSetFun="true" extRec="rec"
		extStr="document.getElementById('contract.mainContract.id').value=rec.get('id');document.getElementById('contract.mainContract.note').value=rec.get('note');" />
	<g:LayOutWinTag buttonId="searchsupplier" title="选择客户"
		hasHtmlFrame="true" html="${ctx}/crm/customer/customer/crmCustomerSelect.action"
		width="900" height="550" isHasSubmit="false" functionName="CrmCustomerFun"
		hasSetFun="true" extRec="rec"
		extStr="document.getElementById('contract_supplier_id').value=rec.get('id');
		document.getElementById('contract_supplier_name').value=rec.get('name');
		document.getElementById('contract_supplier_dutyManId_name').value=rec.get('dutyManId-name');" />
	<g:LayOutWinTag isHasButton="false" title="选择检测模板" hasHtmlFrame="true"
		html="${ctx}/experiment/common/experimentTemplateSelect.action"
		width="900" height="500" isHasSubmit="false" functionName="dgzs"
		hasSetFun="true" extRec="rec"
		extStr="window.frames['tab11frame'].setMbValue(rec);" />
	<g:LayOutWinTag buttonId="searchcontracttype" title="选择合同类型"
		hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
		isHasSubmit="false" functionName="htlx" hasSetFun="true"
		documentId="contract_contractType_id"
		documentName="contract_contractType_name" />
	<g:LayOutWinTag buttonId="searchcurrenytype" title="选择币种"
		hasHtmlFrame="true"
		html="${ctx}/dic/currency/currencyTypeSelect.action"
		isHasSubmit="false" functionName="bz" hasSetFun="true"
		documentId="contract_currencyType_id"
		documentName="contract_currencyType_name" />
	<g:LayOutWinTag buttonId="searchsktype" title="选择收款形式"
		hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
		isHasSubmit="false" functionName="sfkxs" hasSetFun="true"
		documentId="contract_collectionType_id"
		documentName="contract_collectionType_name" />
	<g:LayOutWinTag buttonId="searchfinanceunit" title="选择单位"
		hasHtmlFrame="true" html="${ctx}/dic/unit/dicUnitSelect.action"
		isHasSubmit="false" functionName="time" hasSetFun="true"
		documentId="contract_financeUnit_id"
		documentName="contract_financeUnit_name" />
		<g:LayOutWinTag buttonId="showdutyManId" title="选择负责人"
						hasHtmlFrame="true" hasSetFun="true"
						width="document.body.clientWidth/1.5"
						html="${ctx}/core/user/userSelect.action" isHasSubmit="false"
						functionName="showdutyManIdFun" documentId="contract_dutyManId"
						documentName="contract_dutyManId_name" />
</s:if>
<g:LayOutWinTag isHasButton="false" title="改变状态" hasHtmlFrame="true"
	html="${ctx}/workflowEngine/applicationTypeActionLook.action?formId=${contract.id}&tableId=collectionContract"
	isHasSubmit="true" functionName="changeState" hasSetFun="false"
	submitName="确定"
	submitUrl="document.frames['maincontentframe'].agree();" />
<g:LayOutWinTag buttonId="doclinks_img" title="附件" hasHtmlFrame="true"
	width="900" height="500"
	html="${ctx}/operfile/initFileList.action?modelType=collectionContract&id=${contract.id}"
	isHasSubmit="false" functionName="doc" />
<g:HandleDataExtTag hasConfirm="false"
	paramsValue="contractId:'${contractId}'" funName="modifyData"
	url="${ctx}/contract/collectionContract/saveContractItem.action" />
<g:HandleDataExtTag funName="delData"
	url="${ctx}/contract/collectionContract/delContractItem.action" />
<g:HandleDataExtTag hasConfirm="false"
	paramsValue="contractId:'${contract.id}'" funName="modifyPayData"
	url="${ctx}/contract/collectionContract/saveContractPayItem.action" />
<g:HandleDataExtTag funName="delPayData"
	url="${ctx}/contract/collectionContract/delContractPayItem.action" />
<body>
	<%@ include file="/WEB-INF/page/include/toolbarNoTab.jsp"%>
	<script type="text/javascript"
		src="${ctx}/javascript/contract/collectionContract/editContract.js"></script>
	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass">
		<input type="hidden" id="handlemethod" value="${request.handlemethod}">
		<s:form name="form1" theme="simple">
			<table width="100%" cellpadding="0" cellspacing="0">
				<tbody>
					<tr>
						<td>
							<table width="100%" class="section_table" cellpadding="0">
								<tbody>
									<tr class="control textboxcontrol">
										<td valign="top" class="controlTitle" nowrap><span
											class="labelspacer">&nbsp;&nbsp;</span> <label title="合同编号"
											class="text label"> 合同编号 </label></td>
										<td class="requiredcolumn" nowrap width="10px"><img
											class='requiredimage' src='${ctx}/images/required.gif' /></td>
										<td nowrap><s:textfield
												cssClass="input_parts text input default  readonlytrue false"
												name="contract.id" id="contract_id" title="合同编号  "
												 size="20" readonly="true"
												maxlength="30"></s:textfield> <img alt=''
											src='${ctx}/images/blank.gif' class='detail'
											style='display: none' /></td>
										<td nowrap>&nbsp;</td>
									</tr>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="合同描述" class="text label"> 合同描述 </label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><s:textfield
												cssClass="input_parts text input default  readonlyfalse false"
												name="contract.note" id="contract_note" title="合同描述  "
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="50"
												maxlength="55"></s:textfield> <img alt=''
											src='${ctx}/images/blank.gif' class='detail'
											style='display: none' /></td>
										<td nowrap>&nbsp;</td>
									</tr>
									<%-- <tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="主合同" class="text label"> 主合同 </label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><s:textfield
												cssClass="input_parts text input  readonlyfalse false"
												name="contract.mainContract.id"
												id="contract.mainContract.id" title="主合同"
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="15"
												maxlength="30" readonly="true"></s:textfield> <img
											alt='选择主合同' id='searchmaincontract' name='searchmaincontract'
											src='${ctx}/images/img_menu.gif' class='detail' /> <img
											class="detail" src="${ctx}/images/menu_icon_link.gif"
											onclick="viewContract();" alt="查看主合同"> <s:textfield
												cssClass="input_parts text input  readonlytrue"
												name="contract.mainContract.note"
												id="contract_mainContract_note" readonly="true" title="主合同 "
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="40"
												maxlength="62"></s:textfield> <img alt=''
											src='${ctx}/images/blank.gif' class='detail'
											style='display: none' /></td>
										<td nowrap></td>
									</tr> --%>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="客户" class="text label"> 客户 </label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><s:textfield
												cssClass="input_parts text input  readonlyfalse false"
												name="contract.supplier.id" id="contract_supplier_id"
												title="客户 " onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="15"
												maxlength="30" readonly="true"></s:textfield> <img
											alt='选择客户' id='searchsupplier' name='searchsupplier'
											src='${ctx}/images/img_menu.gif' class='detail' /> <img
											class="detail" src="${ctx}/images/menu_icon_link.gif"
											onclick="viewCustomer();" alt="查看客户">  <s:textfield
												cssClass="input_parts text input  readonlytrue"
												name="contract.supplier.name" id="contract_supplier_name"
												readonly="true" title="客户 "
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="40"
												maxlength="62"></s:textfield> <img alt=''
											src='${ctx}/images/blank.gif' class='detail'
											style='display: none' /></td>
										<td nowrap></td>
									</tr>
																		<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="附件" class="text label">合同附件</label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td align="left"><img alt="保存基本后,可以维护查看附件"
											id="doclinks_img" class="detail"
											src="${ctx}/images/img_attach.gif" /><span class="text label">共有${requestScope.fileNum}个附件</span></td>
										<td nowrap>&nbsp;</td>
									</tr>
									<%-- <tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="传真" class="text label"> 传真 </label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td align="left" nowrap><s:textfield
												cssClass="input_parts text input default  readonlytrue false"
												name="contract.supplier.fax" id="contract_supplier_fax"
												readonly="true" title="传真 "
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="12"
												maxlength="20"></s:textfield></td>
										<td nowrap>&nbsp;</td>
									</tr> --%>
								</tbody>
							</table>
						</td>
						<td class="sectioncol " width='33%' valign="top" align="right">
							<table width="100%" class="section_table" cellpadding="0">
								<tbody>
								<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="备注" class="text label">备注</label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><s:textfield
												cssClass="input_parts text input default  readonlyfalse false"
												name="contract.remark" id="contract_remark" title="备注 "
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="30"
												maxlength="200"></s:textfield></td>
										<td nowrap>&nbsp;</td>
									</tr> 
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="合同类型" class="text label"> 合同类型 </label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><s:hidden name="contract.contractType.id"
												id="contract_contractType_id"></s:hidden> <s:textfield
												cssClass="input_parts text input default  readonlyfalse false"
												name="contract.contractType.name"
												id="contract_contractType_name" title="合同类型  "
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="10"
												maxlength="30" readonly="true"></s:textfield> <img
											alt='选择合同类型' id='searchcontracttype'
											src='${ctx}/images/img_lookup.gif' name='searchcontracttype'
											class='detail' /> <img alt='' src='${ctx}/images/blank.gif'
											class='detail' style='display: none' /></td>
										<td nowrap>&nbsp;</td>
									</tr>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
										 <label
											title="客户负责人" class="text label">
										负责人 </label></td>
																				<td class="requiredcolumn" nowrap width="10px">
										</td>
					<td align="left"><input type="text" size="10"
						readonly="readOnly" id="contract_dutyManId_name"
						value="<s:property value="contract.dutyManId.name"/>"
						class="text input" /> <input type="hidden"
						id="contract_dutyManId" name="contract.dutyManId.id"
						value="<s:property value="contract.dutyManId.id"/>"> <img
						alt='选择负责人' id='showdutyManId' src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>
						</tr>
									<%-- <tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="合同开始日期" class="text label"> 合同开始日期 </label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><input type="text"
											class="input_parts text input default  false false"
											name="contract.startDate" id="contract_startDate"
											title="合同开始日期" onblur="textbox_ondeactivate(event);"
											onfocus="shared_onfocus(event,this)" size="20" maxlength="30"
											value="<s:date name="contract.startDate" format="yyyy-MM-dd"/>"></input>
										</td>
										<td nowrap>&nbsp;</td>
									</tr> --%>
									<%-- <tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="联系人" class="text label">客户联系人</label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><s:textfield
												cssClass="input_parts text input default  readonlytrue false"
												name="contract.supplier.linkMan"
												id="contract_supplier_linkMan" readonly="true"
												title="客户联系人  " onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="10"
												maxlength="30"></s:textfield></td>
										<td nowrap>&nbsp;</td>
									</tr> --%>

									<!-- tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="委托人" class="text label">委托人</label>
										</td>
										<td class="requiredcolumn" nowrap width="10px"></td>
										<td nowrap><s:textfield
												cssClass="input_parts text input default  readonlyfalse false"
												name="contract.entrustMan"
												id="contract_entrustMan" 
												title="委托人 " onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="20"
												maxlength="30">
											</s:textfield></td>
										<td nowrap>&nbsp;</td>
									</tr-->
								</tbody>
							</table>
						</td>
						<td class="sectioncol " width='33%' valign="top" align="right">
							<div class="section standard_section  ">
								<div class="section">
									<table width="100%" class="section_table" cellpadding="0">
										<tbody>
											<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="合同签订日期" class="text label"> 合同签订日期 </label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
												</td>
												<td nowrap>
												<input type="text"
											name="contract.signDate" id="contract_signDate"
											title="合同签订日期" Class="Wdate"
										    size="20" maxlength="30"
											onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
											value="<s:date name="contract.signDate" format="yyyy-MM-dd"/>"></input>
										</td>
												<td nowrap>&nbsp;</td>
											</tr>
												<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="客户方法人代表或授权签字人" class="text label">客户方法人代表或授权签字人</label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><s:textfield
												cssClass="input_parts text input default  readonlyfalse false"
												name="contract.linkMan" id="contract_linkMan" title="合同联系人 "
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="10"
												maxlength="30"></s:textfield></td>
										<td nowrap>&nbsp;</td>
									</tr>
									 <tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="选择城市" class="text label">所在城市法人</label>
												</td>
												<td class="requiredcolumn" nowrap width="10px"><img
													src="/images/required.gif" class="requiredimage" alt="" />
												</td>
												<td nowrap><s:hidden
														name="contract.regionType.id"
														id="contract_regionType_id"></s:hidden> <s:textfield
														cssClass="input_parts text input default  readonlyfalse false"
														name="contract.regionType.name"
														id="contract_regionType_name" readonly="true"
														title="选择城市 " size="10" maxlength="20"></s:textfield> <img
													alt='选择城市' id='regionType'
													src='${ctx}/images/img_lookup.gif' name='regionType'
													class='detail' /></td>
												<td nowrap>&nbsp;</td>
											</tr> 
											<%-- <tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="合同联系电话" class="text label"> 合同联系电话 </label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
												</td>
												<td nowrap><s:textfield
														cssClass="input_parts text input default  readonlyfalse false"
														name="contract.linkTel" id="contract_linkTel"
														title="合同联系电话  " onblur="textbox_ondeactivate(event);"
														onfocus="shared_onfocus(event,this)" size="20"
														maxlength="30"></s:textfield></td>
												<td nowrap>&nbsp;</td>
											</tr> --%>
										<%-- 	<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="合同结束日期" class="text label"> 合同结束日期 </label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
												</td>
												<td nowrap><input type="text"
													class="input_parts text input default  false false"
													name="contract.endDate" id="contract_endDate"
													title="合同结束日期  " onblur="textbox_ondeactivate(event);"
													onfocus="shared_onfocus(event,this)" size="20"
													maxlength="30"
													value="<s:date name="contract.endDate" format="yyyy-MM-dd"/>"></input>
												</td>
												<td nowrap>&nbsp;</td>
											</tr> --%>
										<%-- 	<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="客户联系电话" class="text label"> 客户联系电话 </label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
												</td>
												<td nowrap><s:textfield
														cssClass="input_parts text input default  readonlytrue false"
														name="contract.supplier.linkTel"
														id="contract_supplier_linkTel" readonly="true"
														title="客户联系电话  " onblur="textbox_ondeactivate(event);"
														onfocus="shared_onfocus(event,this)" size="20"
														maxlength="30"></s:textfield></td>
												<td nowrap>&nbsp;</td>
											</tr> --%>
										<%-- 	<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="工作流状态" class="text label"> 工作流状态 </label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
												</td>
												<td nowrap><s:textfield
														cssClass="input_parts text input default  readonlytrue false"
														name="contract.stateName" readonly="true"
														id="contract_stateName" title="工作流状态  "
														onblur="textbox_ondeactivate(event);"
														onfocus="shared_onfocus(event,this)" size="10"
														maxlength="30"></s:textfield></td>
												<s:hidden id="contract.state" name="contract.state"></s:hidden>
												<td nowrap>&nbsp;</td>
											</tr> --%>
									<%-- 											<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="合同批准人" class="text label"> 合同批准人 </label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td align="left" nowrap><s:hidden
												name="contract.confirmUser.id" id="contract_confirmUser_id"></s:hidden>
											<s:textfield
												cssClass="input_parts text input default  readonlytrue false"
												name="contract.confirmUser.name"
												id="contract_confirmUser_name" title="合同批准人 "
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" readonly="true"
												size="10" maxlength="20"></s:textfield></td>
										<td nowrap>&nbsp;</td>
									</tr> --%>
										</tbody>
									</table>
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="section standard_section  ">
				<div class="section">
					<table width="100%" class="section_table" cellpadding="0">
						<tbody>
							<tr class="sectionrow " valign="top">
								<td class="sectioncol " width='50%' valign="top" align="right">
									<div class="section standard_section marginsection  ">
										<div class="section_header standard_section_header">
											<table width="100%" cellpadding="0" cellspacing="0">
												<tbody>
													<tr>
														<td
															class="section_header_left text standard_section_label labelcolor"
															align="left"><span class="section_label">
																合同收款计划 </span></td>
														<td class="section_header_right" align="right"></td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class=" section_content_border">
											<table width="100%" class="section_table" cellpadding="0">
												<tbody>
													<tr class="control textboxcontrol">
														<td valign="top" align="right" class="controlTitle" nowrap>
															<span class="labelspacer">&nbsp;&nbsp;</span> <label
															title="收款形式" class="text label"> 收款形式 </label>
														</td>
														<td class="requiredcolumn" nowrap width="10px">
														</td>
														<td nowrap><s:hidden
																name="contract.collectionType.id"
																id="contract_collectionType_id"></s:hidden> <s:textfield
																cssClass="input_parts text input default  readonlyfalse false"
																name="contract.collectionType.name"
																id="contract_collectionType_name" title="收款形式 "
																onblur="textbox_ondeactivate(event);"
																onfocus="shared_onfocus(event,this)" size="20"
																maxlength="30" readonly="true"></s:textfield> <img
															alt='选择收款形式' id='searchsktype'
															src='${ctx}/images/img_lookup.gif' name='searchtype'
															class='detail' /> <img alt=''
															id='main_grid1_row1_col2_sec2_4_detail'
															src='${ctx}/images/blank.gif'
															name='main_grid1_row1_col2_sec2_4_detail' class='detail'
															style='display: none' /></td>
														<td nowrap>&nbsp;</td>
														<td valign="top" align="right" class="controlTitle" nowrap>
															<span class="labelspacer">&nbsp;&nbsp;</span> <label
															title="收款期数" class="text label">收款期数</label>
														</td>
														<td class="requiredcolumn" nowrap width="10px">
														</td>
														<td nowrap><input
															class="input_parts text input default  readonlyfalse false"
															name="contract.financePeriod" id="contract_financePeriod"
															title="收款期数  " onblur="textbox_ondeactivate(event);"
															onfocus="shared_onfocus(event,this)"
															onkeyup="this.value=this.value.replace(/\D/g,'')"
															onafterpaste="this.value=this.value.replace(/\D/g,'')"
															size="10" maxlength="20"
															value="<s:property value='contract.financePeriodStr'/>">
															<img alt='' src='${ctx}/images/blank.gif' class='detail'
															style='display: none' /></td>
														<td nowrap>&nbsp;</td>
														<%-- <td valign="top" align="right" class="controlTitle" nowrap>
															<span class="labelspacer">&nbsp;&nbsp;</span> <label
															title="单位" class="text label">单位 </label>
														</td>
														<td class="requiredcolumn" nowrap width="10px">
														</td>
														<td nowrap><s:hidden name="contract.financeUnit.id"
																id="contract_financeUnit_id"></s:hidden> <s:textfield
																class="input_parts text input default  readonlyfalse false"
																name="contract.financeUnit.name"
																id="contract_financeUnit_name" title="单位  "
																onblur="textbox_ondeactivate(event);"
																onfocus="shared_onfocus(event,this)" size="10"
																maxlength="10" readonly="true"></s:textfield> <img
															alt='选择单位' id='searchfinanceunit'
															src='${ctx}/images/img_lookup.gif'
															name='searchfinanceunit' class='detail' /> <img alt=''
															src='${ctx}/images/blank.gif' class='detail'
															style='display: none' /></td>
														<s:hidden name="contract.type" id="contract.type"></s:hidden>
														<td nowrap>&nbsp;</td> --%>
														<td valign="top" align="right" class="controlTitle" nowrap>
															<span class="labelspacer">&nbsp;&nbsp;</span> <label
															title="币种" class="text label"> 币种 </label>
														</td>
														<td class="requiredcolumn" nowrap width="10px">
														</td>
														<td nowrap><s:hidden name="contract.currencyType.id"
																id="contract_currencyType_id"></s:hidden> <s:textfield
																cssClass="input_parts text input default  readonlyfalse false"
																name="contract.currencyType.name"
																id="contract_currencyType_name" title="币种  "
																onblur="textbox_ondeactivate(event);"
																onfocus="shared_onfocus(event,this)" size="10"
																maxlength="30" readonly="true"></s:textfield> <img
															alt='选择币种' id='searchcurrenytype'
															src='${ctx}/images/img_lookup.gif'
															name='searchcurrenytype' class='detail' /> <img alt=''
															src='${ctx}/images/blank.gif'
															name='main_grid1_row1_col1_sec1_2_detail' class='detail'
															style='display: none' /></td>
														<td nowrap>&nbsp;</td>
													</tr>
													<tr class="control textboxcontrol">
														<td valign="top" align="right" class="controlTitle" nowrap>
															<span class="labelspacer">&nbsp;&nbsp;</span> <label
															title="合同金额" class="text label"> 合同金额 </label>
														</td>
														<td class="requiredcolumn" nowrap width="10px">
														</td>
														<td nowrap><s:textfield
																cssClass="input_parts text input default  readonlyfalse false"
																name="contract.fee"
																onkeyup="this.value=this.value.replace(/[^\d\.]/g,'')"
																onafterpaste="this.value=this.value.replace(/[^\d\.]/g,'')"
																id="contract_fee" title="合同金额  "
																onblur="textbox_ondeactivate(event);"
																value="%{contract.fee!=null?getText('format.number',{contract.fee}):''}"
																onfocus="shared_onfocus(event,this)" size="10"
																maxlength="30"></s:textfield> <img alt=''
															src='${ctx}/images/blank.gif'
															name='main_grid1_row1_col1_sec1_2_detail' class='detail'
															style='display: none' /></td>
														<td nowrap>&nbsp;</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<input type="hidden" name="jsonDataStr" id="jsonDataStr" value="" />
			<input type="hidden" name="jsonDataStr1" id="jsonDataStr1" value="" />
		</s:form>


	<%-- 				<g:GridEditTag isAutoWidth="true"
						height="document.body.clientHeight/2" col="${col}"
						statement="${statement}" type="${type}" title="合同明细" url="${path}"
						statementLisener="${statementLisener}"
						handleMethod="${handlemethod}" /> --%>
					<g:GridEditTag isAutoWidth="true"
						height="document.body.clientHeight/2" id="pay"
						modifyDataFunName="modifyPayData" delDataFunName="delPayData"
						col="${col1}" statement="${statement}" type="${type1}"
						title="收款明细" url="${path1}" statementLisener="${statementLisener}"
						handleMethod="${handlemethod}" />

		<div id="tabs1" style="margin: 0 0 0 0"></div>
		<div id="crmContractItempage" width="100%" height:10px></div>
	</div>
</body>
</html>