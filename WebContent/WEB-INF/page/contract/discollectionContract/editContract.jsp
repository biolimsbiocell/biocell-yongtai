<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>biolims</title>
<style>
</style>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="searchmaincontract" title="选择主合同"
		hasHtmlFrame="true"
		html="${ctx}/contract/common/contractSelect.action" width="900"
		height="500" isHasSubmit="false" functionName="contractfun"
		hasSetFun="true" extRec="rec"
		extStr="document.getElementById('contract.mainContract.id').value=rec.get('id');document.getElementById('contract.mainContract.note').value=rec.get('note');" />
	<g:LayOutWinTag buttonId="searchsupplier" title="选择供应商"
		hasHtmlFrame="true"
		html="${ctx}/supplier/common/supplierSelect.action" width="900"
		height="500" isHasSubmit="false" functionName="searchsupplierFun"
		hasSetFun="true" extRec="rec"
		extStr="document.getElementById('contract_supplier_name').value=rec.get('name');document.getElementById('contract_supplier_id').value=rec.get('id');document.getElementById('contract_supplier_linkTel').value=rec.get('linkTel');document.getElementById('contract_supplier_linkMan').value=rec.get('linkMan');document.getElementById('contract_supplier_fax').value=rec.get('fax');" />
	<g:LayOutWinTag isHasButton="false" title="选择检测模板" hasHtmlFrame="true"
		html="${ctx}/experiment/common/experimentTemplateSelect.action"
		isHasSubmit="false" functionName="dgzs" hasSetFun="true" extRec="rec"
		extStr="window.frames['tab11frame'].setMbValue(rec);" />
	<g:LayOutWinTag buttonId="searchcontracttype" title="选择合同类型"
		hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
		isHasSubmit="false" functionName="fkht" hasSetFun="true"
		documentId="contract_contractType_id"
		documentName="contract_contractType_name" />
	<g:LayOutWinTag buttonId="searchcurrenytype" title="选择币种"
		hasHtmlFrame="true"
		html="${ctx}/dic/currency/currencyTypeSelect.action"
		isHasSubmit="false" functionName="bz" hasSetFun="true"
		documentId="contract_currencyType_id"
		documentName="contract_currencyType_name" />
	<g:LayOutWinTag buttonId="searchsktype" title="选择收款形式"
		hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
		isHasSubmit="false" functionName="sfkxs" hasSetFun="true"
		documentId="contract_collectionType_id"
		documentName="contract_collectionType_name" />
	<g:LayOutWinTag buttonId="searchfinanceunit" title="选择单位"
		hasHtmlFrame="true" html="${ctx}/dic/unit/dicUnitSelect.action"
		isHasSubmit="false" functionName="time" hasSetFun="true"
		documentId="contract_financeUnit_id"
		documentName="contract_financeUnit_name" />
</s:if>
<g:LayOutWinTag isHasButton="false" title="改变状态" hasHtmlFrame="true"
	html="${ctx}/workflowEngine/applicationTypeActionLook.action?formId=${contract.id}&tableId=discollectionContract"
	isHasSubmit="true" functionName="changeState" hasSetFun="false"
	submitName="确定"
	submitUrl="document.frames['maincontentframe'].agree();" />
<g:LayOutWinTag buttonId="doclinks_img" title="附件" hasHtmlFrame="true"
	width="900" height="500"
	html="${ctx}/operfile/initFileList.action?modelType=discollectionContract&id=${contract.id}"
	isHasSubmit="false" functionName="doc" />
<g:HandleDataExtTag hasConfirm="false"
	paramsValue="contractId:'${contract.id}'" funName="modifyData"
	url="${ctx}/contract/collectionContract/saveContractItem.action" />
<g:HandleDataExtTag funName="delData"
	url="${ctx}/contract/collectionContract/delContractItem.action" />
<g:LayOutWinTag title="选择对象" width="800" hasHtmlFrame="true"
	html="${ctx}/storage/common/storageSelect.action" isHasSubmit="false"
	isHasButton="false" functionName="mailStorageAll" hasSetFun="true"
	extRec="rec"
	extStr="var record = gridGrid.getSelectionModel().getSelected();
	record.set('objId',rec.get('id'));
	record.set('name',rec.get('name'));
	record.set('storage-id',rec.get('id'));
	record.set('storage-searchCode',rec.get('searchCode'));
	record.set('storage-spec',rec.get('spec'));
	record.set('storage-unit',rec.get('unitStr'));
	record.set('price',rec.get('price'));
	var win = window.parent.Ext.getCmp('mailStorageAll');
	if(win){win.close();}" />
<body>
	<script type="text/javascript">
		
	</script>
	<%@ include file="/WEB-INF/page/include/toolbarNoTab.jsp"%>
	<script type="text/javascript"
		src="${ctx}/javascript/contract/discollectionContract/editContract.js"></script>
	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass">
		<input type="hidden" id="handlemethod" value="${request.handlemethod}">
		<s:form name="form1" theme="simple">
			<table width="100%" cellpadding="0" cellspacing="0">
				<tbody>
					<tr>
						<td>
							<table width="100%" class="section_table" cellpadding="0">
								<tbody>
									<tr class="control textboxcontrol">
										<td valign="top" class="controlTitle" nowrap><span
											class="labelspacer">&nbsp;&nbsp;</span> <label title="合同编号"
											class="text label"> 合同编号 </label></td>
										<td class="requiredcolumn" nowrap width="10px"><img
											class='requiredimage' src='${ctx}/images/required.gif' /></td>
										<td nowrap><s:textfield
												cssClass="input_parts text input default  readonlyfalse false"
												name="contract.id" id="contract_id" title="合同编号  "
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="20"
												maxlength="30"></s:textfield> <img alt=''
											src='${ctx}/images/blank.gif' class='detail'
											style='display: none' /></td>
										<td nowrap>&nbsp;</td>
									</tr>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="合同描述" class="text label"> 合同描述 </label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><s:textfield
												cssClass="input_parts text input default  readonlyfalse false"
												name="contract.note" id="contract_note" title="合同描述  "
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="50"
												maxlength="55"></s:textfield> <img alt=''
											src='${ctx}/images/blank.gif' class='detail'
											style='display: none' /></td>
										<td nowrap>&nbsp;</td>
									</tr>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="主合同" class="text label"> 主合同 </label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><s:textfield
												cssClass="input_parts text input  readonlyfalse false"
												name="contract.mainContract.id"
												id="contract.mainContract.id" title="主合同"
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="15"
												maxlength="30" readonly="true"></s:textfield> <img
											alt='选择主合同' id='searchmaincontract' name='searchmaincontract'
											src='${ctx}/images/img_menu.gif' class='detail' /> <img
											class="detail" src="${ctx}/images/menu_icon_link.gif"
											onclick="viewContract();" alt="查看主合同">  <s:textfield
												cssClass="input_parts text input  readonlytrue"
												name="contract.mainContract.note"
												id="contract_mainContract_note" readonly="true" title="主合同 "
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="40"
												maxlength="62"></s:textfield> <img alt=''
											src='${ctx}/images/blank.gif' class='detail'
											style='display: none' /></td>
										<td nowrap></td>
									</tr>
									<tr>
										<td height="1"></td>
									</tr>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="供应商" class="text label"> 供应商</label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><s:textfield
												cssClass="input_parts text input  readonlyfalse false"
												name="contract.supplier.id" id="contract_supplier_id"
												title="供应商 " onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="15"
												maxlength="30" readonly="true"></s:textfield> <img
											alt='选择供应商' id='searchsupplier' name='searchsupplier'
											src='${ctx}/images/img_menu.gif' class='detail' /> <img
											class="detail" src="${ctx}/images/menu_icon_link.gif"
											onclick="viewSupplier();" alt="查看供应商">  <s:textfield
												cssClass="input_parts text input  readonlytrue"
												name="contract.supplier.name" id="contract_supplier_name"
												readonly="true" title="供应商 "
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="40"
												maxlength="62"></s:textfield> <img alt=''
											src='${ctx}/images/blank.gif' class='detail'
											style='display: none' /></td>
										<td nowrap></td>
									</tr>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="传真" class="text label"> 传真 </label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td align="left" nowrap><s:textfield
												cssClass="input_parts text input default  readonlytrue false"
												name="contract.supplier.fax" id="contract_supplier_fax"
												readonly="true" title="传真 "
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="10"
												maxlength="20"></s:textfield> <img alt=''
											id='main_grid1_row1_col2_sec2_4_detail'
											src='${ctx}/images/blank.gif'
											name='main_grid1_row1_col2_sec2_4_detail' class='detail'
											style='display: none' /></td>
										<td nowrap>&nbsp;</td>
									</tr>
								</tbody>
							</table>
						</td>
						<td class="sectioncol " width='33%' valign="top" align="right">
							<table width="100%" class="section_table" cellpadding="0">
								<tbody>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="合同类型" class="text label"> 合同类型 </label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><s:hidden name="contract.contractType.id"
												id="contract_contractType_id"></s:hidden> <s:textfield
												cssClass="input_parts text input default  readonlyfalse false"
												name="contract.contractType.name"
												id="contract_contractType_name" title="合同类型  "
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="10"
												maxlength="30" readonly="true"></s:textfield> <img
											alt='选择合同类型' id='searchcontracttype'
											src='${ctx}/images/img_lookup.gif' name='searchcontracttype'
											class='detail' /> <img alt='' src='${ctx}/images/blank.gif'
											class='detail' style='display: none' /></td>
										<td nowrap>&nbsp;</td>
									</tr>
									<tr>
										<td height="1"></td>
									</tr>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="合同开始日期" class="text label"> 合同开始日期 </label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><input type="text"
											class="input_parts text input default  false false"
											name="contract.startDate" id="contract_startDate"
											title="合同开始日期" onblur="textbox_ondeactivate(event);"
											onfocus="shared_onfocus(event,this)" size="20" maxlength="30"
											value="<s:date name="contract.startDate" format="yyyy-MM-dd"/>">
											</input></td>
										<td nowrap>&nbsp;</td>
									</tr>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="合同结束日期" class="text label"> 合同结束日期 </label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><input type="text"
											class="input_parts text input default  false false"
											name="contract.endDate" id="contract_endDate"
											title="合同结束日期  " onblur="textbox_ondeactivate(event);"
											onfocus="shared_onfocus(event,this)" size="20" maxlength="30"
											value="<s:date name="contract.endDate" format="yyyy-MM-dd"/>"></input>
										</td>
										<td nowrap>&nbsp;</td>
									</tr>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="联系人" class="text label">联系人</label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><s:textfield
												cssClass="input_parts text input default  readonlytrue false"
												name="contract.supplier.linkMan"
												id="contract_supplier_linkMan" readonly="true" title="联系人  "
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="10"
												maxlength="30"></s:textfield> <img alt=''
											src='${ctx}/images/blank.gif' class='detail'
											style='display: none' /></td>
										<td nowrap>&nbsp;</td>
									</tr>
									<tr id="doclinks">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="附件" class="text label">附件</label>&nbsp;
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td align="left"><img alt="保存基本后,可以维护查看附件"
											id="doclinks_img" class="detail"
											src="${ctx}/images/img_attach.gif" /></td>
										<td nowrap>&nbsp;</td>
										<td nowrap></td>
										<td nowrap></td>
									</tr>
								</tbody>
							</table>
						</td>
						<td class="sectioncol " width='33%' valign="top" align="right">
							<div class="section standard_section  ">
								<div class="section">
									<table width="100%" class="section_table" cellpadding="0">
										<tbody>
											<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="工作流状态" class="text label"> 工作流状态 </label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
												</td>
												<td nowrap><s:textfield
														cssClass="input_parts text input default  readonlytrue false"
														name="contract.stateName" readonly="true"
														id="contract_stateName" title="工作流状态  "
														onblur="textbox_ondeactivate(event);"
														onfocus="shared_onfocus(event,this)" size="10"
														maxlength="30"></s:textfield></td>
												<s:hidden id="contract.state" name="contract.state"></s:hidden>
												<td nowrap>&nbsp;</td>
											</tr>
											<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="合同批准人" class="text label"> 合同批准人 </label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
												</td>
												<td align="left" nowrap><s:hidden
														name="contract.confirmUser.id"
														id="contract_confirmUser_id"></s:hidden> <s:textfield
														cssClass="input_parts text input default  readonlytrue false"
														name="contract.confirmUser.name"
														id="contract_confirmUser_name" title="合同批准人 "
														onblur="textbox_ondeactivate(event);"
														onfocus="shared_onfocus(event,this)" readonly="true"
														size="10" maxlength="20"></s:textfield></td>
												<td nowrap>&nbsp;</td>
											</tr>
											<tr>
												<td height="1"></td>
											</tr>
											<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="币种" class="text label"> 币种 </label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
												</td>
												<td nowrap><s:hidden name="contract.currencyType.id"
														id="contract_currencyType_id"></s:hidden> <s:textfield
														cssClass="input_parts text input default  readonlyfalse false"
														name="contract.currencyType.name"
														id="contract_currencyType_name" title="币种  "
														onblur="textbox_ondeactivate(event);"
														onfocus="shared_onfocus(event,this)" size="10"
														maxlength="30" readonly="true"></s:textfield> <img
													alt='选择币种' id='searchcurrenytype'
													src='${ctx}/images/img_lookup.gif' name='searchcurrenytype'
													class='detail' /></td>
												<td nowrap>&nbsp;</td>
											</tr>
											<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="客户联系电话" class="text label"> 客户联系电话 </label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
												</td>
												<td nowrap><s:textfield
														cssClass="input_parts text input default  readonlytrue false"
														name="contract.supplier.linkTel"
														id="contract_supplier_linkTel" readonly="true"
														title="供应商联系电话  " onblur="textbox_ondeactivate(event);"
														onfocus="shared_onfocus(event,this)" size="20"
														maxlength="30"></s:textfield> <img alt=''
													src='${ctx}/images/blank.gif' class='detail'
													style='display: none' /></td>
												<td nowrap>&nbsp;</td>
											</tr>
											<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="合同签订日期" class="text label"> 合同签订日期 </label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
												</td>
												<td nowrap><input type="text"
													class="input_parts text input default  false false"
													name="contract.signDate" id="contract_signDate"
													title="合同签订日期" onblur="textbox_ondeactivate(event);"
													onfocus="shared_onfocus(event,this)" size="20"
													maxlength="30"
													value="<s:date name="contract.signDate" format="yyyy-MM-dd"/>">
													</input></td>
												<td nowrap>&nbsp;</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="section standard_section  ">
				<div class="section">
					<table width="100%" class="section_table" cellpadding="0">
						<tbody>
							<tr class="sectionrow " valign="top">
								<td class="sectioncol " width='50%' valign="top" align="right">
									<div class="section standard_section marginsection  ">
										<div class="section_header standard_section_header">
											<table width="100%" cellpadding="0" cellspacing="0">
												<tbody>
													<tr>
														<td
															class="section_header_left text standard_section_label labelcolor"
															align="left"><span class="section_label">
																合同付款计划 </span></td>
														<td class="section_header_right" align="right"></td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class=" section_content_border">
											<table width="100%" class="section_table" cellpadding="0">
												<tbody>
													<tr class="control textboxcontrol">
														<td valign="top" align="right" class="controlTitle" nowrap>
															<span class="labelspacer">&nbsp;&nbsp;</span> <label
															title="付款形式" class="text label"> 付款形式 </label>
														</td>
														<td class="requiredcolumn" nowrap width="10px">
														</td>
														<td nowrap><s:hidden
																name="contract.collectionType.id"
																id="contract_collectionType_id"></s:hidden> <s:textfield
																cssClass="input_parts text input default  readonlyfalse false"
																name="contract.collectionType.name"
																id="contract_collectionType_name" title="付款形式 "
																onblur="textbox_ondeactivate(event);"
																onfocus="shared_onfocus(event,this)" size="20"
																maxlength="30" readonly="true"></s:textfield> <img
															alt='选择付款形式' id='searchsktype'
															src='${ctx}/images/img_lookup.gif' name='searchtype'
															class='detail' /> <img alt=''
															id='main_grid1_row1_col2_sec2_4_detail'
															src='${ctx}/images/blank.gif'
															name='main_grid1_row1_col2_sec2_4_detail' class='detail'
															style='display: none' /></td>
														<td nowrap>&nbsp;</td>
														<td valign="top" align="right" class="controlTitle" nowrap>
															<span class="labelspacer">&nbsp;&nbsp;</span> <label
															title="付款周期" class="text label">付款周期</label>
														</td>
														<td class="requiredcolumn" nowrap width="10px">
														</td>
														<td nowrap><input
															class="input_parts text input default  readonlyfalse false"
															name="contract.financePeriod" id="contract_financePeriod"
															title="付款周期  " onblur="textbox_ondeactivate(event);"
															onfocus="shared_onfocus(event,this)"
															onkeyup="this.value=this.value.replace(/\D/g,'')"
															onafterpaste="this.value=this.value.replace(/\D/g,'')"
															size="10" maxlength="20"
															value="<s:property value='contract.financePeriodStr'/>">
															<img alt='' src='${ctx}/images/blank.gif' class='detail'
															style='display: none' /></td>
														<td nowrap>&nbsp;</td>
														<td valign="top" align="right" class="controlTitle" nowrap>
															<span class="labelspacer">&nbsp;&nbsp;</span> <label
															title="单位" class="text label">单位 </label>
														</td>
														<td class="requiredcolumn" nowrap width="10px">
														</td>
														<td nowrap><s:hidden name="contract.financeUnit.id"
																id="contract_financeUnit_id"></s:hidden> <s:textfield
																cssClass="input_parts text input default  readonlyfalse false"
																name="contract.financeUnit.name"
																id="contract_financeUnit_name" title="单位  "
																onblur="textbox_ondeactivate(event);"
																onfocus="shared_onfocus(event,this)" size="10"
																maxlength="10" readonly="true"></s:textfield> <img
															alt='选择单位' id='searchfinanceunit'
															src='${ctx}/images/img_lookup.gif'
															name='searchfinanceunit' class='detail' /> <img alt=''
															src='${ctx}/images/blank.gif' class='detail'
															style='display: none' /></td>
														<s:hidden name="contract.type" id="contract.type"></s:hidden>
														<td nowrap>&nbsp;</td>
													</tr>
													<tr>
														<td height="2"></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<input type="hidden" name="jsonDataStr" id="jsonDataStr" value="" />
			<input type="hidden" name="jsonDataStr2" id="jsonDataStr2" value="" />
		</s:form>
		<div id="item">
			<c:choose>
				<c:when test='${copyBl==true}'>
					<g:GridEditTag isAutoWidth="true"
						height="document.body.clientHeight" pageSize="100000" col="${col}"
						statement="${statement}" type="${type}" title="${title}"
						url="${path}" statementLisener="${statementLisener}"
						handleMethod="${handlemethod}" />
				</c:when>
				<c:otherwise>
					<g:GridEditTag isAutoWidth="true"
						height="document.body.clientHeight" col="${col}"
						statement="${statement}" type="${type}" title="${title}"
						url="${path}" statementLisener="${statementLisener}"
						handleMethod="${handlemethod}" />
				</c:otherwise>
			</c:choose>
		</div>
		<div id="tabs1" style="margin: 0 0 0 0"></div>
	</div>
</body>
</html>
