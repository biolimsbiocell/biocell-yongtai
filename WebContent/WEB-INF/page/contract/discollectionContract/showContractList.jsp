<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript"
	src="${ctx}/javascript/contract/discollectionContract/showContractList.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/handleSearchForm.js"></script>
<g:LayOutWinTag buttonId="searchmaincontract" title="选择主合同"
	hasHtmlFrame="true" html="${ctx}/contract/common/contractSelect.action"
	width="900" height="500" isHasSubmit="false" functionName="contractfun"
	hasSetFun="true" extRec="rec"
	extStr="document.getElementById('mainContract_id').value=rec.get('id');document.getElementById('mainContract_note').value=rec.get('note');" />
<g:LayOutWinTag buttonId="searchsupplier" title="选择供应商"
	hasHtmlFrame="true" html="${ctx}/supplier/common/supplierSelect.action"
	width="900" height="550" isHasSubmit="false" functionName="customer"
	hasSetFun="true" extRec="rec"
	extStr="document.getElementById('supplier_id').value=rec.get('id');" />
<g:LayOutWinTag buttonId="searchcontracttype" title="选择合同类型"
	hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
	isHasSubmit="false" functionName="htlx" hasSetFun="true"
	documentId="contractType_id" documentName="contractType_name" />
</head>
<body>
	<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
	<div class="mainlistclass" id="markup">
		<div id="jstj" style="display: none">
			<table cellspacing="0" cellpadding="0" class="toolbarsection">
				<tr>
					<td>&nbsp;</td>
					<td><span>&nbsp;</span> <label class="text label"
						title="输入查询条件"> 编号</label></td>
					<td>&nbsp;</td>
					<td nowrap class="quicksearch_control" valign="middle"><input
						type="text"
						class="input_parts text input default  readonlyfalse false"
						name="id" onblur="textbox_ondeactivate(event);" searchField="true"
						onfocus="shared_onfocus(event,this)" id="id" title="输入ID"
						style="width: 90"></td>
					<td><span>&nbsp;</span> <label class="text label"
						title="输入查询条件"> 描述</label></td>
					<td>&nbsp;</td>
					<td nowrap class="quicksearch_control" valign="middle"><input
						type="text"
						class="input_parts text input default  readonlyfalse false"
						name="note" onblur="textbox_ondeactivate(event);"
						searchField="true" onfocus="shared_onfocus(event,this)" id="note"
						title="输入描述" style="width: 90"></td>
					<td><span>&nbsp;</span> <label class="text label"
						title="输入查询条件"> 主合同号</label></td>
					<td>&nbsp;</td>
					<td nowrap class="quicksearch_control" valign="middle"><input
						type="text"
						class="input_parts text input default  readonlyfalse false"
						name="mainContract.id" onblur="textbox_ondeactivate(event);"
						searchField="true" onfocus="shared_onfocus(event,this)"
						id="mainContract_id" title="输入主合同号" style="width: 90"> <img
						title='选择' id="searchmaincontract"
						src='${ctx}/images/img_lookup.gif' class='detail' /></td>
					<td><span>&nbsp;</span> <label class="text label"
						title="输入查询条件"> 合同类型</label></td>
					<td>&nbsp;</td>
					<td nowrap class="quicksearch_control" valign="middle"><input
						type="text"
						class="input_parts text input default  readonlyfalse false"
						name="" onblur="textbox_ondeactivate(event);"
						onfocus="shared_onfocus(event,this)" id="contractType_name"
						title="输入合同类型" style="width: 90"> <input type="hidden"
						name="contractType.id" id="contractType_id" searchField="true"></input>
						<img title='选择' id="searchcontracttype"
						src='${ctx}/images/img_lookup.gif' class='detail' /></td>
					<td><span>&nbsp;</span> <label class="text label"
						title="输入查询条件"> 供应商</label></td>
					<td>&nbsp;</td>
					<td nowrap class="quicksearch_control" valign="middle"><input
						type="text"
						class="input_parts text input default  readonlyfalse false"
						name="" onblur="textbox_ondeactivate(event);"
						onfocus="shared_onfocus(event,this)" id="supplier_name"
						title="输入供应商" style="width: 90"> <input type="hidden"
						name="supplier.id" id="supplier_id" searchField="true"></input> <img
						title='选择' id="searchsupplier" src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>
					<td><span>&nbsp;</span> <label class="text label"
						title="输入查询条件"> 记录数</label></td>
					<td>&nbsp;</td>
					<td nowrap class="quicksearch_control" valign="middle"><input
						type="text"
						class="input_parts text input default  readonlyfalse false"
						name="limitNum" onblur="textbox_ondeactivate(event);"
						onfocus="shared_onfocus(event,this)" id="limitNum" title="输入记录数"
						style="width: 90"> <img class="quicksearch_findimage"
						id="quicksearch_findimage" name="quicksearch_findimage"
						onclick="commonSearchAction(gridGrid)"
						src="${ctx}/images/quicksearch.gif" alt="搜索"> <img
						class="quicksearch_findimage" id="quicksearch_findimage"
						name="quicksearch_findimage" onclick="form_reset()"
						src="${ctx}/images/no_draw.gif" title="清空"></td>
				</tr>
			</table>
		</div>

		<input type="hidden" id="id" value="" />
		<g:GridTag isAutoWidth="true" col="${col}" type="${type}"
			title="${title}" url="${path}" />
	</div>
</body>
</html>