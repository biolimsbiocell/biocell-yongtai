<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript"
	src="${ctx}/javascript/contract/discollectionContract/showContractItemList.js"></script>
<g:HandleDataExtTag hasConfirm="false"
	paramsValue="contractId:'${contractId}'" funName="modifyData"
	url="${ctx}/contract/discollectionContract/saveContractItem.action" />
<g:HandleDataExtTag funName="delData"
	url="${ctx}/contract/discollectionContract/delContractItem.action" />
</head>
<g:LayOutWinTag title="选择对象" width="800" hasHtmlFrame="true"
	html="${ctx}/storage/common/storageSelect.action" isHasSubmit="false"
	isHasButton="false" functionName="mailStorageAll" hasSetFun="true"
	extRec="rec"
	extStr="var record = gridGrid.getSelectionModel().getSelected();
	record.set('objId',rec.get('id'));
	record.set('name',rec.get('name'));
	record.set('searchCode',rec.get('searchCode'));
	record.set('storage-spec',rec.get('spec'));
	record.set('storage-unit',rec.get('unitStr'));
	record.set('price',rec.get('price'));
	var win = window.parent.Ext.getCmp('mailStorageAll');
	if(win){win.close();}" />
<body>
	<input type="hidden" id="id" value="" />
	<g:GridEditTag isAutoWidth="true" height="document.body.clientHeight"
		col="${col}" statement="${statement}" type="${type}" title="${title}"
		url="${path}" statementLisener="${statementLisener}"
		handleMethod="${handlemethod}" />
</body>
</html>