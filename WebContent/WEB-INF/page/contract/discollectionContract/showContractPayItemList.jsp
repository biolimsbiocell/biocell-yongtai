<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<g:HandleDataExtTag hasConfirm="false"
	paramsValue="contractId:'${contractId}'" funName="modifyData"
	url="${ctx}/contract/discollectionContract/saveContractPayItem.action" />
<g:HandleDataExtTag funName="delData"
	url="${ctx}/contract/discollectionContract/delContractPayItem.action" />
</head>
<body>
	<input type="hidden" id="id" value="" />
	<g:GridEditTag isAutoWidth="true" col="${col}" statement="${statement}"
		type="${type}" title="" height="document.body.clientHeight"
		url="${path}" statementLisener="${statementLisener}"
		handleMethod="${handlemethod}" />
</body>
</html>