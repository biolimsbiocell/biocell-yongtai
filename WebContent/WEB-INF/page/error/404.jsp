<%@ page contentType="text/html;charset=UTF-8" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>404 - <fmt:message key="biolims.common.pageExistent"/></title>
</head>

<body>
<div>
	<div><h1><fmt:message key="biolims.common.pageExistent"/>.</h1></div>
	<div></div>
</div>
</body>
</html>