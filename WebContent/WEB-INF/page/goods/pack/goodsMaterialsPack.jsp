﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/goods/pack/goodsMaterialsPack.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.serialNumber"/></td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="goodsMaterialsPack_id"
                   	 name="id" searchField="true" title="<fmt:message key="biolims.common.serialNumber"/>"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="goodsMaterialsPack_name"
                   	 name="name" searchField="true" title="<fmt:message key="biolims.common.describe"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="goodsMaterialsPack_create_name" searchField="true"  name="createUser.name"  value="<s:property value="createUser.name"/>" class="text input" />
 						<input type="hidden" id="goodsMaterialsPack_createUser" name="goodsMaterialsPack.createUser.id"  value="" > 
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="goodsMaterialsPack_createDate"
                   	 name="createDate" searchField="true" title="<fmt:message key="biolims.common.commandTime"/>" />
                   	
                   	</td>
               	 	<%--
               	 	<td class="label-title" ><fmt:message key="biolims.common.detectionMethod"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="goodsMaterialsPack_product"
                   	 name="product" searchField="true" title="<fmt:message key="biolims.common.businessTypes"/>"    />
                   	  
                   	</td>
                   	 --%>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_goodsMaterialsPack_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_goodsMaterialsPack_tree_page"></div>
</body>
</html>



