﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>

</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title='<fmt:message key="biolims.common.attachment"/>'
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=goodsMaterialsPack&id=${goodsMaterialsPack.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/goods/pack/goodsMaterialsPackEdit.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.serialNumber"/></td>
           	 	<td class="requiredcolumn" nowrap width="10px"   ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
               	<td align="left"   >
	               	<input type="text" size="20" maxlength="25" id="goodsMaterialsPack_id"
readonly = "readOnly" class="text input readonlytrue"
	               	 name="goodsMaterialsPack.id" title="<fmt:message key="biolims.common.serialNumber"/>" value="<s:property value="goodsMaterialsPack.id"/>"
	                   	  />
                </td>
           	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
           	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
               	<td align="left"  >
                   	<input type="text" size="20" maxlength="50" id="goodsMaterialsPack_name"
                   	 name="goodsMaterialsPack.name" title="<fmt:message key="biolims.common.describe"/>" value="<s:property value="goodsMaterialsPack.name"/>"
                   	  />
                </td>
				<g:LayOutWinTag buttonId="showcreate" title='<fmt:message key="biolims.common.selectTheCreatePerson"/>'
					hasHtmlFrame="true" hasSetFun="true" width="document.body.clientWidth/1.5" 
					html="${ctx}/core/user/userSelect.action"
					isHasSubmit="false"		functionName="UserFun" 
					documentId="goodsMaterialsPack_createUser"
					documentName="goodsMaterialsPack_createUser_name"
				 />
          	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
          	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
              	<td align="left"  >
					<input type="text" size="20" readonly="readOnly"  id="goodsMaterialsPack_createUser_name"  value="<s:property value="goodsMaterialsPack.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
					<input type="hidden" id="goodsMaterialsPack_createUser" name="goodsMaterialsPack.createUser.id"  value="<s:property value="goodsMaterialsPack.createUser.id"/>" > 
			</tr>
			<tr>
				<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
           	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
               	<td align="left"  >
               	  	<input type="text" size="20" maxlength="" id="goodsMaterialsPack_createDate"
                   	 name="goodsMaterialsPack.createDate" title="<fmt:message key="biolims.common.commandTime"/>" readonly = "readOnly" class="text input readonlytrue"  
                   	   Class="Wdate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="goodsMaterialsPack.createDate" format="yyyy-MM-dd"/>" 
                   	  />
                </td>
               	 	<td class="label-title" style="display: none;"><fmt:message key="biolims.common.businessTypes"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"   style="display: none;"><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
               	<td align="left" style="display: none;" >
 						<input type="text" size="20" readonly="readOnly"  id="goodsMaterialsPack_product_name"  value="<s:property value="goodsMaterialsPack.product.name"/>" />
 						<input type="hidden" id="goodsMaterialsPack_product" name="goodsMaterialsPack.product.id"  value="<s:property value="goodsMaterialsPack.product.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectBusinessTypes"/>' id='showtype' src='${ctx}/images/img_lookup.gif' 	class='detail'  onClick="productFun()" />                   		
                   	</td>
                  <td class="label-title" ><fmt:message key="biolims.common.businessTypes"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"   ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
               	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="goodsMaterialsPack_productName" name="goodsMaterialsPack.productName"  value="<s:property value="goodsMaterialsPack.productName"/>" />
 						<input type="hidden" id="goodsMaterialsPack_productId" name="goodsMaterialsPack.productId"  value="<s:property value="goodsMaterialsPack.productId"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectBusinessTypes"/>' id='showtype' src='${ctx}/images/img_lookup.gif' 	class='detail'  onClick="productFun()" />                   		
                   	</td>
          
           			
                   	<td class="label-title" ><fmt:message key="biolims.common.tagNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="50" id="goodsMaterialsPack_num"
                   	 name="goodsMaterialsPack.num" title="<fmt:message key="biolims.common.describe"/>" value="<s:property value="goodsMaterialsPack.num"/>"
                   	 />
                   	  	</td>
                   	  		</tr>
			<tr>              
                   	  	
                   	 <td class="label-title" ><fmt:message key="biolims.common.whetherToCreateTheBarcode"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<select id="goodsMaterialsPack_isCode" name="goodsMaterialsPack.isCode" class="input-10-length" >
								<option value="1" <s:if test="goodsMaterialsPack.isCode==1">selected="selected"</s:if>><fmt:message key="biolims.common.yes"/></option>
								<option value="0" <s:if test="goodsMaterialsPack.isCode==0">selected="selected"</s:if>><fmt:message key="biolims.common.no"/></option>
					</select>
                   	</td>
               
 		    	
                  
                   	 <td class="label-title" ><fmt:message key="biolims.common.state"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<select id="goodsMaterialsPack_state" name="goodsMaterialsPack.state" class="input-10-length" >
								<option value="1" <s:if test="goodsMaterialsPack.state==1">selected="selected"</s:if>><fmt:message key="biolims.common.effective"/></option>
								<option value="0" <s:if test="goodsMaterialsPack.state==0">selected="selected"</s:if>><fmt:message key="biolims.common.invalid"/></option>
					</select>
                   	</td> 
                   	  <td class="label-title"><fmt:message key="biolims.common.attachment"/></td><td></td>
						<td title="<fmt:message key="biolims.common.afterSaveCanLookAtTheAttachment"/>" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message key="biolims.common.attachment"/></span>
							</td>
			</tr>
			
			
            </table>
            <input type="hidden" name="goodsMaterialsDetailsJson" id="goodsMaterialsDetailsJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="goodsMaterialsPack.id"/>" />
            </form>
  <!--           <div id="tabs">
            <ul>
			<li><a href="#goodsMaterialsDetailspage">物料明细</a></li>
           	</ul>  -->
			<div id="goodsMaterialsDetailspage" width="100%" height:10px></div>
			</div>
        	<!-- </div> -->
	</body>
	</html>
