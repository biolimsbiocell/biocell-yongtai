﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: nowrap;
}

</style>
<script type="text/javascript" src="${ctx}/js/goods/sample/sampleInfo.js"></script>
</head>
<body>
	<div id="sampleInfodiv"></div>
	<div id="bat_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsv"><fmt:message key="biolims.common.uploadFile"/>
	</div>
	<div id="bat_sampleInfo_div" style="display: none">
		<table>
				
			<tr>
				<td class="label-title"><span><fmt:message key="biolims.common.hospital"/></span></td>
				<td><input id="isGood"/></td>
			</tr>
			<!-- <tr>
				<td class="label-title"><span>数量</span></td>
				<td><input type="text" id="method" name="method"></td>
			</tr> -->
			
		</table>
	</div>
</body>
</html>


