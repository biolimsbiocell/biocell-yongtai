﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%-- <%@ include file="/WEB-INF/page/include/toolbar.jsp"%> --%>
<script type="text/javascript" src="${ctx}/js/goods/sample/goodsSampleWay.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	<!--  	<td class="label-title" >编号</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="18" id="goodsSampleWay_id"
                   	 name="id" searchField="true" title="编号"    />
                   	
					
 
                  
                   	
                   	  
                   	</td> -->
               	 	<td class="label-title" ><fmt:message key="biolims.common.samplePackageNumber"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="30" maxlength="60" id="goodsSampleWay_code"
                   	 name="code" searchField="true" title="<fmt:message key="biolims.common.samplePackageNumber"/>"    />
                   	 
                   	 <td class="label-title" ><span><fmt:message key="biolims.common.whetherOrNotQualified"/></span></td>
                	<td><select id="goodsSampleWay_isQualified" name="isQualified" searchField="true" style="width:100">
               			<option value="" <s:if test="isQualified==">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    					<option value="1" <s:if test="isQualified==1">selected="selected" </s:if>><fmt:message key="biolims.common.yes"/></option>
    					<option value="0" <s:if test="isQualified==0">selected="selected" </s:if>><fmt:message key="biolims.common.no"/></option>
						</select>
                 	</td>
			<!-- 
			</tr>
			<tr>
               	 	<td class="label-title" >打包人</td>
                   	<td align="left"  >
                  
					<input type="text" size="40" maxlength="30" id="goodsSampleWay_packUser"
                   	 name="packUser" searchField="true" title="打包人"    />
                   	</td>
               	 	<td class="label-title" >是否合格</td>
                   	<td align="left"  >
                  
					<input type="text" size="40" maxlength="30" id="goodsSampleWay_isQualified"
                   	 name="isQualified" searchField="true" title="是否合格"    />
                   	</td> -->
               	 	
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.courierCompany"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="30" maxlength="30" id="goodsSampleWay_expressCompany"
                   	 name="expressCompany" searchField="true" title="<fmt:message key="biolims.common.courierCompany"/>"    />
                   	
          			 </td>
               	 	 <!--    <td class="label-title" >发货时间</td>
                   	<td align="left"  >
                  
					<input type="text" size="40" maxlength="30" id="goodsSampleWay_sendTime"
                   	 name="sendTime" searchField="true" title="发货时间"    />
                   	</td> -->
               	 	<!-- <td class="label-title" >是否签收</td>
                   	<td align="left"  >
                  
					<input type="text" size="30" maxlength="60" id="goodsSampleWay_isReceive"
                   	 name="isReceive" searchField="true" title="签收确认"    />
                   	</td> -->
                   	<td class="label-title" ><span><fmt:message key="biolims.common.whetherOrNotToSign"/></span></td>
                	<td><select id="goodsSampleWay_isReceive" name="isReceive" searchField="true" style="width:100">
               			<option value="" <s:if test="isReceive==">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    					<option value="1" <s:if test="isReceive==1">selected="selected" </s:if>><fmt:message key="biolims.common.yes"/></option>
    					<option value="0" <s:if test="isReceive==0">selected="selected" </s:if>><fmt:message key="biolims.common.no"/></option>
						</select>
                 	</td>
			</tr>
			<tr>
					<td class="label-title" ><fmt:message key="biolims.common.courierNumber"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="30" maxlength="30" id="goodsSampleWay_expressOrder"
                   	 name="expressOrder" searchField="true" title="<fmt:message key="biolims.common.courierNumber"/>"    />
                   	</td>
					
			</tr>
            </table>          
		</form>
		<div id="show_detail_grid_div"></div>
		</div>
		<div id="show_goodsSampleWay_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_goodsSampleWay_tree_page"></div>
		<div id="bat_way_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.whetherOrNotQualified"/></span></td>
                <td><select id="isQualified"  style="width:100">
               			<option value="" <s:if test="isQualified==">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    					<option value="1" <s:if test="isQualified==1">selected="selected" </s:if>><fmt:message key="biolims.common.yes"/></option>
    					<option value="0" <s:if test="isQualified==0">selected="selected" </s:if>><fmt:message key="biolims.common.no"/></option>
					</select>
                 </td>
			</tr>
		</table>
		</div>
	<div id="bat_ok_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.confirmReceipt"/></span></td>
                <td><select id="isReceive"  style="width:100">
               			<option value="" <s:if test="isReceive==">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    					<option value="1" <s:if test="isReceive==1">selected="selected" </s:if>><fmt:message key="biolims.common.yes"/></option>
    					<option value="0" <s:if test="isReceive==0">selected="selected" </s:if>><fmt:message key="biolims.common.no"/></option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
	
</body>
</html>



