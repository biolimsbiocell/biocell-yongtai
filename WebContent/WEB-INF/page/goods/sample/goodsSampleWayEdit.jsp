﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title='<fmt:message key="biolims.common.attachment"/>'
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=goodsSampleWay&id=${goodsSampleWay.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/goods/sample/goodsSampleWayEdit.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="18" id="goodsSampleWay_id"
                   	 name="goodsSampleWay.id" title="<fmt:message key="biolims.common.serialNumber"/>"
                   	   
	value="<s:property value="goodsSampleWay.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.samplePackageNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="60" id="goodsSampleWay_code"
                   	 name="goodsSampleWay.code" title="<fmt:message key="biolims.common.samplePackageNumber"/>"
                   	   
	value="<s:property value="goodsSampleWay.code"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="60" id="goodsSampleWay_name"
                   	 name="goodsSampleWay.name" title="<fmt:message key="biolims.common.describe"/>"
                   	   
	value="<s:property value="goodsSampleWay.name"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.packagingPeople"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="40" maxlength="30" id="goodsSampleWay_packUser"
                   	 name="goodsSampleWay.packUser" title="<fmt:message key="biolims.common.packagingPeople"/>"
                   	   
	value="<s:property value="goodsSampleWay.packUser"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.whetherOrNotQualified"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="40" maxlength="30" id="goodsSampleWay_isQualified"
                   	 name="goodsSampleWay.isQualified" title="<fmt:message key="biolims.common.whetherOrNotQualified"/>"
                   	   
	value="<s:property value="goodsSampleWay.isQualified"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.courierNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="40" maxlength="30" id="goodsSampleWay_order"
                   	 name="goodsSampleWay.order" title="<fmt:message key="biolims.common.courierNumber"/>"
                   	   
	value="<s:property value="goodsSampleWay.order"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.courierCompany"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="40" maxlength="30" id="goodsSampleWay_company"
                   	 name="goodsSampleWay.company" title="<fmt:message key="biolims.common.courierCompany"/>"
                   	   
	value="<s:property value="goodsSampleWay.company"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.deliveryTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="40" maxlength="30" id="goodsSampleWay_sendTime"
                   	 name="goodsSampleWay.sendTime" title="<fmt:message key="biolims.common.deliveryTime"/>"
                   	   
	value="<s:property value="goodsSampleWay.sendTime"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.signForConfirmation"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="60" id="goodsSampleWay_isReceive"
                   	 name="goodsSampleWay.isReceive" title="<fmt:message key="biolims.common.signForConfirmation"/>"
                   	   
	value="<s:property value="goodsSampleWay.isReceive"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.deliveryStatus"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="40" maxlength="30" id="goodsSampleWay_state"
                   	 name="goodsSampleWay.state" title="<fmt:message key="biolims.common.deliveryStatus"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="goodsSampleWay.state"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.deliveryStatus"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="40" maxlength="30" id="goodsSampleWay_stateName"
                   	 name="goodsSampleWay.stateName" title="<fmt:message key="biolims.common.deliveryStatus"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="goodsSampleWay.stateName"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			<td class="label-title"><fmt:message key="biolims.common.attachment"/></td><td></td>
						<td title="<fmt:message key="biolims.common.afterSaveCanLookAtTheAttachment"/>" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message key="biolims.common.attachment"/></span>
			</tr>
			
			
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="goodsSampleWay.id"/>" />
            </form>
<!--             <div id="tabs">
            <ul>
           	</ul> 
			</div> -->
        	</div>
	</body>
	</html>
