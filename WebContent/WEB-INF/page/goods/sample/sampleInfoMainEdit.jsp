﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title='<fmt:message key="biolims.common.attachment"/>'
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleInfoMain&id=${sampleInfoMain.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/goods/sample/sampleInfoMainEdit.js"></script>
<script type="text/javascript">
/* 	$(function(){
		alert($("#sampleInfo_sampleTime").val(new Date()));
	}); */
</script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="18" id="sampleInfoMain_id"
                   	 name="sampleInfoMain.id" title="<fmt:message key="biolims.common.serialNumber"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"
	value="<s:property value="sampleInfoMain.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="40" maxlength="60" id="sampleInfoMain_name"
                   	 name="sampleInfoMain.name" title="<fmt:message key="biolims.common.describe"/>"
                   	   
	value="<s:property value="sampleInfoMain.name"/>"
                   	  />
                   	  
                   	  </td>
			
			
               	 	<td class="label-title"  style="display:none"><fmt:message key="biolims.common.whetherToConfirm"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" style="display:none"></td>            	 	
                   	<td align="left"  style="display:none">
                   	<input type="text" size="20" maxlength="60" id="sampleInfoMain_isQualified" style="display:none"
                   	 name="sampleInfoMain.isQualified" title="<fmt:message key="biolims.common.describe"/>" readonly = "readOnly" class="text input readonlytrue"
                   	   
	value="<s:property value="sampleInfoMain.isQualified"/>"
                   	  />
                   	  
<!--                    	</td> -->
			
			
<!--                	 	<td class="label-title" >送检医院</td> -->
<%--                	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	 --%>
<!--                    	<td align="left"  > -->
<!--                    	<input type="text" size="40" maxlength="60" id="sampleInfoMain_hospital" -->
<!--                    	 name="sampleInfoMain.hospital" title="送检医院" -->
                   	   
<%-- 	value="<s:property value="sampleInfoMain.hospital"/>" --%>
<!--                    	  /> -->
                   	  
<!--                    	</td> -->

			
			<%-- <g:LayOutWinTag buttonId="showproject" title="选择项目"
				hasHtmlFrame="true"
				html="${ctx}/com/biolims/system/product/productSelect.action"
				isHasSubmit="false" functionName="ProductFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('sampleInfoMain_project').value=rec.get('id');
				document.getElementById('sampleInfoMain_project_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" >项目</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sampleInfoMain_project_name"  value="<s:property value="sampleInfoMain.project.name"/>"/>
 						<input type="hidden" id="sampleInfoMain_project" name="sampleInfoMain.project.id"  value="<s:property value="sampleInfoMain.project.id"/>" > 
 						<img alt='选择项目' id='showproject' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
	
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.samplingOfPeople"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="30" id="sampleInfoMain_sampleUser"
                   	 name="sampleInfoMain.sampleUser" title="<fmt:message key="biolims.common.samplingOfPeople"/>"
                   	   
	value="<s:property value="sampleInfoMain.sampleUser"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>			
               	 	<%-- <td class="label-title" >取样时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="" id="sampleInfoMain_sampleTime"
                   	 name="sampleInfoMain.sampleTime" title="取样时间"
                   	    value="<s:property value="sampleInfoMain.sampleTime"/>" 
                   	     
                   	  />
                   	  
                   	</td> --%>

					<%-- <td class="label-title" >发货时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   <input type="text" size="20" maxlength="" id="sampleInfoMain_sendTime"
                   	 name="sampleInfoMain.sendTime" title="发货日期"
                   	   readonly = "readOnly" class="text input readonlytrue"
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="sampleInfoMain.sendTime" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	  
                   	</td> --%>
					<td class="label-title" ><fmt:message key="biolims.common.deliveryTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="30" id="sampleInfoMain_sendTime"
                   	 name="sampleInfoMain.sendTime" title="<fmt:message key="biolims.common.deliveryTime"/>"
                   	 readonly = "readOnly" class="text input readonlytrue"
	value="<s:property value="sampleInfoMain.sendTime"/>"
                   	  />
                   	  
                   	</td>
			
			<%-- <g:LayOutWinTag buttonId="showcompany" title="选择快递公司"
				hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="company" hasSetFun="true"
				documentId="sampleInfoMain_company"
				documentName="sampleInfoMain_company_name"
			/>
			
			 --%>
               	 	<td class="label-title" ><fmt:message key="biolims.common.courierCompany"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sampleInfoMain_company_name"  value="<s:property value="sampleInfoMain.company.name"/>"/>
 						<input type="hidden" id="sampleInfoMain_company" name="sampleInfoMain.company.id"  value="<s:property value="sampleInfoMain.company.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectCourierCompany"/>' id='showcompany' src='${ctx}/images/img_lookup.gif' 	class='detail' onClick="ExpressCompanyFun()"/>                   		
                   	</td>

               	 	<td class="label-title" ><fmt:message key="biolims.common.courierNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="60" id="sampleInfoMain_expressCode"
                   	 name="sampleInfoMain.expressCode" title="<fmt:message key="biolims.common.courierNumber"/>"
                   	   
	value="<s:property value="sampleInfoMain.expressCode"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showcreateUser" title='<fmt:message key="biolims.common.selectTheCreatePerson"/>'
				hasHtmlFrame="true"
				html="${ctx}/goods/sample/sampleInfoMain/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('sampleInfoMain_createUser').value=rec.get('id');
				document.getElementById('sampleInfoMain_createUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sampleInfoMain_createUser_name"  value="<s:property value="sampleInfoMain.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="sampleInfoMain_createUser" name="sampleInfoMain.createUser.id"  value="<s:property value="sampleInfoMain.createUser.id"/>" > 
<%--  						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 --%>                   	</td>

               	 	<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="" id="sampleInfoMain_createDate"
                   	 name="sampleInfoMain.createDate" title="<fmt:message key="biolims.common.commandTime"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="sampleInfoMain.createDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>

               	 	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.workflowState"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	  
 					<input type="hidden" id="sampleInfoMain_state" name="sampleInfoMain.state" value="<s:property value="sampleInfoMain.state"/>"
                   	/></td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.workflowState"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="30" id="sampleInfoMain_stateName"
                   	 name="sampleInfoMain.stateName" title="<fmt:message key="biolims.common.workflowState"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="sampleInfoMain.stateName"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>					
			
			<td class="label-title"><fmt:message key="biolims.common.attachment"/></td><td></td>
						<td title="<fmt:message key="biolims.common.afterSaveCanLookAtTheAttachment"/>" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>${requestScope.fileNum}<fmt:message key="biolims.common.attachment"/></span>
			</tr>
			
			
            </table>
            <input type="hidden" name="sampleInfoSystemJson" id="sampleInfoSystemJson" value="" />
            <input type="hidden" name="sampleInfoJson" id="sampleInfoJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="sampleInfoMain.id"/>" />
            </form>
            <div id="tabs">
            <ul>
            <li><a href="#sampleInfopage"><fmt:message key="biolims.common.packingDetail"/></a></li>
			<li><a href="#sampleInfoSystempage"><fmt:message key="biolims.common.packagingSystemDetail"/></a></li>

           	</ul> 
			<div id="sampleInfoSystempage" width="100%" height:10px></div>
			<div id="sampleInfopage" width="100%" height:10px></div>
			</div>
        	</div>
	</body>
	</html>
