﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/goods/sample/sampleInfoMain.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="18" id="sampleInfoMain_id"
                   	 name="id" searchField="true" title="<fmt:message key="biolims.common.serialNumber"/>"    />
                   	
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="60" id="sampleInfoMain_name"
                   	 name="name" searchField="true" title="<fmt:message key="biolims.common.describe"/>"    />
                   	
<!--                    	</td> -->
<!--                	 	<td class="label-title" >送检医院</td> -->
<!--                    	<td align="left"  > -->
                  
<!-- 					<input type="text" size="50" maxlength="60" id="sampleInfoMainMain_hospital" -->
<!--                    	 name="hospital" searchField="true" title="送检医院"    /> -->
                   	
                   	  
<!--                    	</td> -->
			</tr>
			<tr>
			<%-- <g:LayOutWinTag buttonId="showproject" title="选择项目"
				hasHtmlFrame="true"
				html="${ctx}/goods/sample/sampleInfoMainMain/projectSelect.action"
				isHasSubmit="false" functionName="ProjectFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('sampleInfoMain_project').value=rec.get('id');
				document.getElementById('sampleInfoMain_project_name').value=rec.get('name');" />
               	 	<td class="label-title" >项目</td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="sampleInfoMain_project_name" searchField="true"  name="project.name"  value="" class="text input" />
 						<input type="hidden" id="sampleInfoMain_project" name="sampleInfoMain.project.id"  value="" > 
 						<img alt='选择项目' id='showproject' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
               	 	<td class="label-title" ><fmt:message key="biolims.common.samplingOfPeople"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="40" maxlength="30" id="sampleInfoMain_sampleUser"
                   	 name="sampleUser" searchField="true" title="<fmt:message key="biolims.common.samplingOfPeople"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.samplingTime"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="40" maxlength="30" id="sampleInfoMain_sampleTime"
                   	 name="sampleTime" searchField="true" title="<fmt:message key="biolims.common.samplingTime"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.deliveryTime"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="40" maxlength="30" id="sampleInfoMain_sendTime"
                   	 name="sendTime" searchField="true" title="<fmt:message key="biolims.common.deliveryTime"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<%-- <g:LayOutWinTag buttonId="showcompany" title="选择快递公司"
				hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="showcompanyFun" hasSetFun="true"
				documentId="sampleInfoMain_company"
				documentName="sampleInfoMain_company_name"
			/> --%>
               	 	<td class="label-title" ><fmt:message key="biolims.common.courierCompany"/></td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="sampleInfoMain_company_name" searchField="true"  name="company.name"  value="" class="text input" />
 						<input type="hidden" id="sampleInfoMain_company" name="sampleInfoMain.company.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.selectCourierCompany"/>' id='showcompany' src='${ctx}/images/img_lookup.gif' 	class='detail' onClick="ExpressCompanyTwoFun()"   />                   		
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.courierNumber"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="60" id="sampleInfoMain_expressCode"
                   	 name="expressCode" searchField="true" title="<fmt:message key="biolims.common.courierNumber"/>"    />
                   	                  	  
                   	</td>
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showcreateUser" title='<fmt:message key="biolims.common.selectTheCreatePerson"/>'
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('sampleInfoMain_createUser').value=rec.get('id');
				document.getElementById('sampleInfoMain_createUser_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="sampleInfoMain_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="sampleInfoMain_createUser" name="sampleInfoMain.createUser.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.selectTheCreatePerson"/>' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.workflowState"/></td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="40" maxlength="30" id="sampleInfoMain_state"
                   	 name="state" searchField="true" title="<fmt:message key="biolims.common.workflowState"/>"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
 					<input type="hidden" id="sampleInfoMain_state" name="state" searchField="true" value=""/>"
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.workflowState"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="40" maxlength="30" id="sampleInfoMain_stateName"
                   	 name="stateName" searchField="true" title="<fmt:message key="biolims.common.workflowState"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_sampleInfoMain_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_sampleInfoMain_tree_page"></div>
</body>
</html>



