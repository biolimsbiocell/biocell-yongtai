﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/goods/sample/sampleInfoMainDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.serialNumber"/></td>
               	<td align="left">
                    		<input type="text" maxlength="18" id="sampleInfo_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.describe"/></td>
               	<td align="left">
                    		<input type="text" maxlength="60" id="sampleInfo_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.inspectionHospital"/></td>
               	<td align="left">
                    		<input type="text" maxlength="60" id="sampleInfo_hospital" searchField="true" name="hospital"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.project"/></td>
               	<td align="left">
                    		<input type="text" maxlength="" id="sampleInfo_project" searchField="true" name="project"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.samplingOfPeople"/></td>
               	<td align="left">
                    		<input type="text" maxlength="30" id="sampleInfo_sampleUser" searchField="true" name="sampleUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.samplingTime"/></td>
               	<td align="left">
                    		<input type="text" maxlength="30" id="sampleInfo_sampleTime" searchField="true" name="sampleTime"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.deliveryTime"/></td>
               	<td align="left">
                    		<input type="text" maxlength="30" id="sampleInfo_sendTime" searchField="true" name="sendTime"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.courierCompany"/></td>
               	<td align="left">
                    		<input type="text" maxlength="" id="sampleInfo_company" searchField="true" name="company"  class="input-20-length" onClick="ExpressCompanyFun()"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.courierNumber"/></td>
               	<td align="left">
                    		<input type="text" maxlength="60" id="sampleInfo_expressCode" searchField="true" name="expressCode"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.commandPerson"/></td>
               	<td align="left">
                    		<input type="text" maxlength="" id="sampleInfo_createUser" searchField="true" name="createUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.commandTime"/></td>
               	<td align="left">
                    		<input type="text" maxlength="" id="sampleInfo_createDate" searchField="true" name="createDate"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.workflowState"/></td>
               	<td align="left">
                    		<input type="text" maxlength="30" id="sampleInfo_state" searchField="true" name="state"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.workflowState"/></td>
               	<td align="left">
                    		<input type="text" maxlength="30" id="sampleInfo_stateName" searchField="true" name="stateName"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue"><fmt:message key="biolims.common.search"/></font></span>
		
		<div id="show_dialog_sampleInfoMain_div"></div>
   		
</body>
</html>



