﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/goods/mate/goodsMaterialsSendDialogByState.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.serialNumber"/></td>
               	<td align="left">
                    		<input type="text" maxlength="18" id="goodsMaterialsSend_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.describe"/></td>
               	<td align="left">
                    		<input type="text" maxlength="60" id="goodsMaterialsSend_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.commandPerson"/></td>
               	<td align="left">
                    		<input type="text" maxlength="" id="goodsMaterialsSend_createUser" searchField="true" name="createUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.commandTime"/></td>
               	<td align="left">
                    		<input type="text" maxlength="" id="goodsMaterialsSend_createDate" searchField="true" name="createDate"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.materialPreparation"/></td>
               	<td align="left">
                    		<input type="text" maxlength="" id="goodsMaterialsSend_goodsMaterialsReady" searchField="true" name="goodsMaterialsReady"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.provincesAndAutonomousRSegions"/></td>
               	<td align="left">
                    		<input type="text" maxlength="30" id="goodsMaterialsSend_goodsMaterialsApply" searchField="true" name="goodsMaterialsApply"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.recipient"/></td>
               	<td align="left">
                    		<input type="text" maxlength="30" id="goodsMaterialsSend_receiveUser" searchField="true" name="receiveUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.recipientPhone"/></td>
               	<td align="left">
                    		<input type="text" maxlength="30" id="goodsMaterialsSend_phone" searchField="true" name="phone"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.shippingAddress"/></td>
               	<td align="left">
                    		<input type="text" maxlength="30" id="goodsMaterialsSend_address" searchField="true" name="address"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.courierCompany"/></td>
               	<td align="left">
                    		<input type="text" maxlength="" id="goodsMaterialsSend_company" searchField="true" name="company"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.transportBoxNumber"/></td>
               	<td align="left">
                    		<input type="text" maxlength="10" id="goodsMaterialsSend_num" searchField="true" name="num"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.auditor"/></td>
               	<td align="left">
                    		<input type="text" maxlength="" id="goodsMaterialsSend_confirmUser" searchField="true" name="confirmUser"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.auditDate"/></td>
               	<td align="left">
                    		<input type="text" maxlength="" id="goodsMaterialsSend_confirmDate" searchField="true" name="confirmDate"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.workflowState"/></td>
               	<td align="left">
                    		<input type="text" maxlength="30" id="goodsMaterialsSend_state" searchField="true" name="state"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.workflowState"/></td>
               	<td align="left">
                    		<input type="text" maxlength="30" id="goodsMaterialsSend_stateName" searchField="true" name="stateName"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue"><fmt:message key="biolims.common.search"/></font></span>
		
		<div id="show_dialog_goodsMaterialsSendByState_div"></div>
   		
</body>
</html>



