﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/goods/mate/goodsMaterialsPost.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" >编号</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="18" id="goodsMaterialsPost_id"
                   	 name="id" searchField="true" title="编号"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >描述</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="60" id="goodsMaterialsPost_name"
                   	 name="name" searchField="true" title="描述"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showtype" title="选择寄送类型"
				hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="showtypeFun" hasSetFun="true"
				documentId="goodsMaterialsPost_type"
				documentName="goodsMaterialsPost_type_name"
			/>
               	 	<td class="label-title" >寄送类型</td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="goodsMaterialsPost_type_name" searchField="true"  name="type.name"  value="" class="text input" />
 						<input type="hidden" id="goodsMaterialsPost_type" name="goodsMaterialsPost.type.id"  value="" > 
 						<img alt='选择寄送类型' id='showtype' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showgoodsMaterialsSend" title="选择物料发放"
				hasHtmlFrame="true"
				html="${ctx}/goods/mate/goodsMaterialsPost/goodsMaterialsSendSelect.action"
				isHasSubmit="false" functionName="GoodsMaterialsSendFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('goodsMaterialsPost_goodsMaterialsSend').value=rec.get('id');
				document.getElementById('goodsMaterialsPost_goodsMaterialsSend_name').value=rec.get('name');" />
               	 	<td class="label-title" >物料发放</td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="goodsMaterialsPost_goodsMaterialsSend_name" searchField="true"  name="goodsMaterialsSend.name"  value="" class="text input" />
 						<input type="hidden" id="goodsMaterialsPost_goodsMaterialsSend" name="goodsMaterialsPost.goodsMaterialsSend.id"  value="" > 
 						<img alt='选择物料发放' id='showgoodsMaterialsSend' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('goodsMaterialsPost_createUser').value=rec.get('id');
				document.getElementById('goodsMaterialsPost_createUser_name').value=rec.get('name');" />
               	 	<td class="label-title" >创建人</td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="goodsMaterialsPost_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="goodsMaterialsPost_createUser" name="goodsMaterialsPost.createUser.id"  value="" > 
 						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" >创建日期</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  >状态</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="40" maxlength="30" id="goodsMaterialsPost_state"
                   	 name="state" searchField="true" title="状态"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
 					<input type="hidden" id="goodsMaterialsPost_state" name="state" searchField="true" value=""/>"
                   	</td>
               	 	<td class="label-title" >状态</td>
                   	<td align="left"  >
                  
					<input type="text" size="40" maxlength="30" id="goodsMaterialsPost_stateName"
                   	 name="stateName" searchField="true" title="状态"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_goodsMaterialsPost_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_goodsMaterialsPost_tree_page"></div>
</body>
</html>



