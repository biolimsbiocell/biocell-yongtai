﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/goods/mate/goodsMaterialsApply.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="18" id="goodsMaterialsApply_id"
                   	 name="id" searchField="true" title="<fmt:message key="biolims.common.serialNumber"/>"    />
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.contact"/></td>
                   	<td align="left"  >
						<input type="text" size="50" maxlength="60" id="goodsMaterialsApply_phone"
	                   	 name="phone" searchField="true" title="<fmt:message key="biolims.common.contact"/>"    />
                   	</td>
					<g:LayOutWinTag buttonId="showapplyOrganize" title='<fmt:message key="biolims.common.selectProvincesAndAutonomousRSegions"/>'
						hasHtmlFrame="true"
						html="${ctx}/goods/mate/goodsMaterialsApply/applyOrganizeSelect.action"
						isHasSubmit="false" functionName="ApplyOrganizeFun" 
		 				hasSetFun="true"
						extRec="rec"
						extStr="document.getElementById('goodsMaterialsApply_applyOrganize').value=rec.get('id');
						document.getElementById('goodsMaterialsApply_applyOrganize_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.provincesAndAutonomousRSegions"/></td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="goodsMaterialsApply_applyOrganize_name" searchField="true"  name="applyOrganize.name"  value="" class="text input" />
 						<input type="hidden" id="goodsMaterialsApply_applyOrganize" name="goodsMaterialsApply.applyOrganize.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.selectProvincesAndAutonomousRSegions"/>' id='showapplyOrganize' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showapplyDept" title='<fmt:message key="biolims.common.selectTheNameOfInstitution"/>'
				hasHtmlFrame="true"
				html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="ApplyDeptFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('goodsMaterialsApply_applyDept').value=rec.get('id');
				document.getElementById('goodsMaterialsApply_applyDept_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.nameOfInstitution"/></td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="goodsMaterialsApply_applyDept_name" searchField="true"  name="applyDept.name"  value="" class="text input" />
 						<input type="hidden" id="goodsMaterialsApply_applyDept" name="goodsMaterialsApply.applyDept.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.selectTheNameOfInstitution"/>' id='showapplyDept' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.shippingAddress"/></td>
                   	<td align="left"  >
						<input type="text" size="50" maxlength="60" id="goodsMaterialsApply_address"
	                   	 name="address" searchField="true" title="<fmt:message key="biolims.common.shippingAddress"/>"    />
                   	</td>
						<g:LayOutWinTag buttonId="showtype" title='<fmt:message key="biolims.common.selectApplicationType"/>'
							hasHtmlFrame="true"
							html="${ctx}/goods/mate/goodsMaterialsApply/dicTypeSelect.action"
							isHasSubmit="false" functionName="DicTypeFun" 
			 				hasSetFun="true"
							extRec="rec"
							extStr="document.getElementById('goodsMaterialsApply_type').value=rec.get('id');
							document.getElementById('goodsMaterialsApply_type_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.applicationType"/></td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="goodsMaterialsApply_type_name" searchField="true"  name="type.name"  value="" class="text input" />
 						<input type="hidden" id="goodsMaterialsApply_type" name="goodsMaterialsApply.type.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.selectApplicationType"/>' id='showtype' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
				</tr>
				<tr>
				<g:LayOutWinTag buttonId="showreceiveUser" title='<fmt:message key="biolims.common.selectRecipients"/>' hasHtmlFrame="true"
				hasSetFun="true" html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="dutyUserFun"
				documentId="goodsMaterialsApply_receiveUser" 
				documentName="goodsMaterialsApply_receiveUser_name" />
						
               	 	<td class="label-title" ><fmt:message key="biolims.common.recipient"/></td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="goodsMaterialsApply_receiveUser_name" searchField="true"  name="receiveUser.name"  value="" class="text input" />
 						<input type="hidden" id="goodsMaterialsApply_receiveUser" name="goodsMaterialsApply.receiveUser.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.selectRecipients"/>' id='showreceiveUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.recipientPhone"/></td>
                   	<td align="left"  >
						<input type="text" size="40" maxlength="30" id="goodsMaterialsApply_phone"
	                   	 name="phone" searchField="true" title="<fmt:message key="biolims.common.recipientPhone"/>"    />
                   	</td>
						<g:LayOutWinTag buttonId="showcreateUser" title='<fmt:message key="biolims.common.selectTheCreatePerson"/>'
							hasHtmlFrame="true"
							html="${ctx}/core/user/userSelect.action"
							isHasSubmit="false" functionName="UserFun" 
			 				hasSetFun="true"
							extRec="rec"
							extStr="document.getElementById('goodsMaterialsApply_createUser').value=rec.get('id');
							document.getElementById('goodsMaterialsApply_createUser_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="goodsMaterialsApply_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="goodsMaterialsApply_createUser" name="goodsMaterialsApply.createUser.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.selectTheCreatePerson"/>' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
                   	<td align="left"  >
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
                   	</td>
               	 	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.workflowState"/></td>
                   	<td align="left"   style="display:none">
						<input type="text" size="50" maxlength="60" id="goodsMaterialsApply_state"
	                   	 name="state" searchField="true" title="<fmt:message key="biolims.common.workflowState"/>"   style="display:none"    />
 					<input type="hidden" id="goodsMaterialsApply_state" name="state" searchField="true" value=""/>"
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.workflowState"/></td>
                   	<td align="left"  >
						<input type="text" size="50" maxlength="60" id="goodsMaterialsApply_stateName"
	                   	 name="stateName" searchField="true" title="<fmt:message key="biolims.common.workflowState"/>"    />
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_goodsMaterialsApply_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<select id="grid_priority" style="display: none">
			<option value="0"><fmt:message key="biolims.common.ordinary"/></option>
			<option value="1"><fmt:message key="biolims.common.high"/></option>
		</select>
		<div id="show_goodsMaterialsApply_tree_page"></div>
</body>
</html>



