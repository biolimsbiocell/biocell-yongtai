﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/goods/mate/goodsMaterialsReady.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" >编号</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="18" id="goodsMaterialsReady_id"
                   	 name="id" searchField="true" title="编号"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >描述</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="60" id="goodsMaterialsReady_name"
                   	 name="name" searchField="true" title="描述"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showgoodsMaterialsApply" title="选择申请单"
				hasHtmlFrame="true"
				html="${ctx}/goods/mate/goodsMaterialsReady/goodsMaterialsApplySelect.action"
				isHasSubmit="false" functionName="GoodsMaterialsApplyFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('goodsMaterialsReady_goodsMaterialsApply').value=rec.get('id');
				document.getElementById('goodsMaterialsReady_goodsMaterialsApply_name').value=rec.get('name');" />
               	 	<td class="label-title" >申请单</td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="goodsMaterialsReady_goodsMaterialsApply_name" searchField="true"  name="goodsMaterialsApply.name"  value="" class="text input" />
 						<input type="hidden" id="goodsMaterialsReady_goodsMaterialsApply" name="goodsMaterialsReady.goodsMaterialsApply.id"  value="" > 
 						<img alt='选择申请单' id='showgoodsMaterialsApply' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >所属省区</td>
                   	<td align="left"  >
                  
					<input type="text" size="40" maxlength="30" id="goodsMaterialsReady_applyOrganize"
                   	 name="applyOrganize" searchField="true" title="所属省区"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >收件人</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="60" id="goodsMaterialsReady_receiveUser"
                   	 name="receiveUser" searchField="true" title="收件人"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >收件人电话</td>
                   	<td align="left"  >
                  
					<input type="text" size="40" maxlength="30" id="goodsMaterialsReady_phone"
                   	 name="phone" searchField="true" title="收件人电话"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >收货地址</td>
                   	<td align="left"  >
                  
					<input type="text" size="40" maxlength="30" id="goodsMaterialsReady_address"
                   	 name="address" searchField="true" title="收货地址"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('goodsMaterialsReady_createUser').value=rec.get('id');
				document.getElementById('goodsMaterialsReady_createUser_name').value=rec.get('name');" />
               	 	<td class="label-title" >创建人</td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="goodsMaterialsReady_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="goodsMaterialsReady_createUser" name="goodsMaterialsReady.createUser.id"  value="" > 
 						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" >创建日期</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showconfirmUser" title="选择审核人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('goodsMaterialsReady_confirmUser').value=rec.get('id');
				document.getElementById('goodsMaterialsReady_confirmUser_name').value=rec.get('name');" />
               	 	<td class="label-title" >审核人</td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="goodsMaterialsReady_confirmUser_name" searchField="true"  name="confirmUser.name"  value="" class="text input" />
 						<input type="hidden" id="goodsMaterialsReady_confirmUser" name="goodsMaterialsReady.confirmUser.id"  value="" > 
 						<img alt='选择审核人' id='showconfirmUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" >审核时间</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startconfirmDate" name="startconfirmDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="confirmDate1" name="confirmDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endconfirmDate" name="endconfirmDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="confirmDate2" name="confirmDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >工作流状态</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="40" maxlength="30" id="goodsMaterialsReady_state"
                   	 name="state" searchField="true" title="工作流状态"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
 					<input type="hidden" id="goodsMaterialsReady_state" name="state" searchField="true" value=""/>"
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >工作流状态</td>
                   	<td align="left"  >
                  
					<input type="text" size="40" maxlength="30" id="goodsMaterialsReady_stateName"
                   	 name="stateName" searchField="true" title="工作流状态"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_goodsMaterialsReady_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_goodsMaterialsReady_tree_page"></div>
</body>
</html>



