﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=goodsMaterialsReady&id=${goodsMaterialsReady.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/goods/mate/goodsMaterialsReadyEdit.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" >编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="18" id="goodsMaterialsReady_id"
                   	 name="goodsMaterialsReady.id" title="编号"
                   	   readonly = "readOnly" class="text input readonlytrue"
	value="<s:property value="goodsMaterialsReady.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="40" maxlength="60" id="goodsMaterialsReady_name"
                   	 name="goodsMaterialsReady.name" title="描述"
                   	   
	value="<s:property value="goodsMaterialsReady.name"/>"
                   	  />
                   	  
                   	</td>
			
			
			
			<%-- <g:LayOutWinTag buttonId="showgoodsMaterialsApply" title="选择申请单"
				hasHtmlFrame="true"
				html="${ctx}/goods/mate/goodsMaterialsApply/goodsMaterialsApplySelect.action"
				isHasSubmit="false" functionName="GoodsMaterialsApplyFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('goodsMaterialsReady_goodsMaterialsApply').value=rec.get('id');
				document.getElementById('goodsMaterialsReady_goodsMaterialsApply_name').value=rec.get('name');" />
				 --%>
			
			
               	 	<td class="label-title" >申请单</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="hidden" size="20" readonly="readOnly"  id="goodsMaterialsReady_goodsMaterialsApply_name"  value="<s:property value="goodsMaterialsReady.goodsMaterialsApply.name"/>" />
 						<input type="text" id="goodsMaterialsReady_goodsMaterialsApply" name="goodsMaterialsReady.goodsMaterialsApply.id"  value="<s:property value="goodsMaterialsReady.goodsMaterialsApply.id"/>" > 
 						<img alt='选择申请单' id='showgoodsMaterialsApply' src='${ctx}/images/img_lookup.gif' 	class='detail'  onClick="GoodsMaterialsApplyFun()" />                   		
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >所属省区</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="30" id="goodsMaterialsReady_goodsMaterialsApply_applyOrganize_name"
                   	 name="goodsMaterialsReady.goodsMaterialsApply.applyOrganize.name" title="所属省区"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="goodsMaterialsReady.goodsMaterialsApply.applyOrganize.name"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >收件人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="60" id="goodsMaterialsReady_goodsMaterialsApply_receiveUser"
                   	 name="goodsMaterialsReady.goodsMaterialsApply.receiveUser" title="收件人"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="goodsMaterialsReady.goodsMaterialsApply.receiveUser"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >收件人电话</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="30" id="goodsMaterialsReady_goodsMaterialsApply_phone"
                   	 name="goodsMaterialsReady.goodsMaterialsApply.phone" title="收件人电话"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="goodsMaterialsReady.goodsMaterialsApply.phone"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >收货地址</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="30" id="goodsMaterialsReady_goodsMaterialsApply_address_name"
                   	 name="goodsMaterialsReady.goodsMaterialsApply.address.name" title="收货地址"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="goodsMaterialsReady.goodsMaterialsApply.address.name"/>"
                   	  />
                   	  
                   	</td>
			
					<td class="label-title" >发货时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="" id="goodsMaterialsReady_goodsMaterialsApply_sendDate"
                   	 	name="goodsMaterialsReady.goodsMaterialsApply.sendDate" title="发货时间"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="goodsMaterialsReady.goodsMaterialsApply.sendDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			
			<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('goodsMaterialsReady_createUser').value=rec.get('id');
				document.getElementById('goodsMaterialsReady_createUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" >创建人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="goodsMaterialsReady_createUser_name"  value="<s:property value="goodsMaterialsReady.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="goodsMaterialsReady_createUser" name="goodsMaterialsReady.createUser.id"  value="<s:property value="goodsMaterialsReady.createUser.id"/>" > 
<%--  						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 --%>                   	</td>
			
			</tr>
			<tr>
				
               	 	<td class="label-title" >创建日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="" id="goodsMaterialsReady_createDate"
                   	 name="goodsMaterialsReady.createDate" title="创建日期"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="goodsMaterialsReady.createDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>

			
			
			<%-- <g:LayOutWinTag buttonId="showconfirmUser" title="选择审核人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('goodsMaterialsReady_confirmUser').value=rec.get('id');
				document.getElementById('goodsMaterialsReady_confirmUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" >审核人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="goodsMaterialsReady_confirmUser_name"  value="<s:property value="goodsMaterialsReady.confirmUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="goodsMaterialsReady_confirmUser" name="goodsMaterialsReady.confirmUser.id"  value="<s:property value="goodsMaterialsReady.confirmUser.id"/>" > 
 						<img alt='选择审核人' id='showconfirmUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			
			
               	 	<td class="label-title" >审核时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="" id="goodsMaterialsReady_confirmDate"
                   	 name="goodsMaterialsReady.confirmDate" title="审核时间"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="goodsMaterialsReady.confirmDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			
			</tr>
			<tr>
					 --%>
               	 	<td class="label-title"  style="display:none"  >工作流状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	  
 					<input type="hidden" id="goodsMaterialsReady_state" name="goodsMaterialsReady.state" value="<s:property value="goodsMaterialsReady.state"/>"
                   	/></td>

               	 	<td class="label-title" >工作流状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="30" id="goodsMaterialsReady_stateName"
                   	 name="goodsMaterialsReady.stateName" title="工作流状态"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="goodsMaterialsReady.stateName"/>"
                   	  />
                   	  

			<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr>
			
			
            </table>
            <!-- <input type="hidden" name="goodsMaterialsReadyDetailsJson" id="goodsMaterialsReadyDetailsJson" value="" /> -->
            <input type="hidden" name="goodsMaterialsReadyItemJson" id="goodsMaterialsReadyItemJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="goodsMaterialsReady.id"/>" />
            </form>
            <div id="tabs">
            <ul>
			<li><a href="#goodsMaterialsReadyItempage">物料明细</a></li>
			<!-- <li><a href="#goodsMaterialsReadyDetailspage">物料包明细</a></li> -->
           	</ul> 
			<!-- <div id="goodsMaterialsReadyDetailspage" width="100%" height:10px></div> -->
			<div id="goodsMaterialsReadyItempage" width="100%" height:10px></div>
			</div>
        	</div>
	</body>
	</html>
