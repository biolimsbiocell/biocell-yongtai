﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%-- <%@ include file="/WEB-INF/page/include/toolbar.jsp"%> --%>
<script type="text/javascript" src="${ctx}/js/goods/mate/goodsMaterialsWay.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
<!-- 		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" >快递单号</td>
                   	<td align="left"  >
                  
					<input type="text" size="30" maxlength="18" id="goodsMaterialsWay_expressOrder"
                   	 name="expressOrder" searchField="true" title="快递单号"    />

                   	</td>
               	 	<td class="label-title" >快递公司</td>
                   	<td align="left"  >
                  
					<input type="text" size="30" maxlength="60" id="goodsMaterialsWay_company"
                   	 name="company" searchField="true" title="快递公司"    />

                   	</td>
			</tr>
			<tr>                   	
               	 	<td class="label-title" >收货地址</td>
                   	<td align="left"  >
                  
					<input type="text" size="30" maxlength="60" id="goodsMaterialsWay_address"
                   	 name="address" searchField="true" title="收货地址"    />

                   	</td>

               	 	<td class="label-title" >收件人</td>
                   	<td align="left"  >
                  
					<input type="text" size="30" maxlength="30" id="goodsMaterialsWay_receiver"
                   	 name="receiver" searchField="true" title="收件人"    />

                   	</td>
			</tr>
            </table>          
		</form>
		</div> -->
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<table cellspacing="0" cellpadding="0" class="toolbarsection">
			<tr>	
			<td >
			<label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>">
			<fmt:message key="biolims.common.courierNumber"/>
			</label>
			</td>
			<td>
			<input type="text"  name="expressOrder" id="goodsMaterialsWay_expressOrder" searchField="true" >
			</td>
			<td >
			<label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>">
			<fmt:message key="biolims.common.courierCompany"/>
			</label>
			</td>
			<td>
			<input type="text"  name="company" id="goodsMaterialsWay_company" searchField="true" >
			</td>
			<td >
			<label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>">
			<fmt:message key="biolims.common.shippingAddress"/>
			</label>
			</td>
			<td>
			<input type="text"  name="address" id="goodsMaterialsWay_address" searchField="true" >
			</td>
			<td >
			<label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>">
			<fmt:message key="biolims.common.recipients"/>
			</label>
			</td>
			<td>
			<input type="text"  name="receiver" id="goodsMaterialsWay_receiver" searchField="true" >
			</td>
			<td>
			<input type="button" onClick="selectInfo()" value="<fmt:message key="biolims.common.find"/>">
			</td>
			</tr>
		</table>
		<div id="show_goodsMaterialsWay_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="bat_way_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.whetherOrNotToSent"/></span></td>
                <td><select id="isSend"  style="width:100">
               			<option value="" <s:if test="isSend==">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    					<option value="1" <s:if test="isSend==1">selected="selected" </s:if>><fmt:message key="biolims.common.yes"/></option>
    					<option value="0" <s:if test="isSend==0">selected="selected" </s:if>><fmt:message key="biolims.common.no"/></option>
					</select>
                 </td>
			</tr>
	</table>
	</div>
	<div id="bat_date_div" style="display: none">
		<table>		
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.sendingDate"/></span></td>
               	<td><input id="sendDate" /><fmt:message key="biolims.common.example"/></td>
			</tr>
		</table>
	</div>
</body>
</html>



