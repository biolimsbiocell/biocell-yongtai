﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title='<fmt:message key="biolims.common.attachment"/>'
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=goodsMaterialsSend&id=${goodsMaterialsSend.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/goods/mate/goodsMaterialsSendEdit.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="18" id="goodsMaterialsSend_id"
                   	readonly = "readOnly" class="text input readonlytrue"
                   	 name="goodsMaterialsSend.id" title="<fmt:message key="biolims.common.serialNumber"/>" value="<s:property value="goodsMaterialsSend.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="40" maxlength="60" id="goodsMaterialsSend_name"
                   	 name="goodsMaterialsSend.name" title="<fmt:message key="biolims.common.describe"/>"
                   	   
	value="<s:property value="goodsMaterialsSend.name"/>"
                   	  />
                   	  
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showcreateUser" title='<fmt:message key="biolims.common.selectTheCreatePerson"/>'
				hasHtmlFrame="true"
				html="${ctx}/goods/mate/goodsMaterialsSend/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('goodsMaterialsSend_createUser').value=rec.get('id');
				document.getElementById('goodsMaterialsSend_createUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="goodsMaterialsSend_createUser_name"  value="<s:property value="goodsMaterialsSend.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="goodsMaterialsSend_createUser" name="goodsMaterialsSend.createUser.id"  value="<s:property value="goodsMaterialsSend.createUser.id"/>" > 
<%--  						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 --%>                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="" id="goodsMaterialsSend_createDate"
                   	 name="goodsMaterialsSend.createDate" title="<fmt:message key="biolims.common.commandTime"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="goodsMaterialsSend.createDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			
			
		<%-- 	
			<g:LayOutWinTag buttonId="showgoodsMaterialsReady" title="选择物料准备"
				hasHtmlFrame="true"
				html="${ctx}/goods/mate/goodsMaterialsReady/goodsMaterialsReadySelect.action"
				isHasSubmit="false" functionName="GoodsMaterialsReadyFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('goodsMaterialsSend_goodsMaterialsReady').value=rec.get('id');
				document.getElementById('goodsMaterialsSend_goodsMaterialsReady_name').value=rec.get('name');" />
				 --%>
			
			
               	 	<%-- <td class="label-title" >物料申请</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="hidden" size="20" readonly="readOnly"  id="goodsMaterialsSend_goodsMaterialsReady_name"  value="<s:property value="goodsMaterialsSend.goodsMaterialsReady.name"/>"/>
 						<input type="text" id="goodsMaterialsSend_goodsMaterialsReady" name="goodsMaterialsSend.goodsMaterialsReady.id"  value="<s:property value="goodsMaterialsSend.goodsMaterialsReady.id"/>" > 
 						<img alt='选择物料准备' id='showgoodsMaterialsReady' src='${ctx}/images/img_lookup.gif' 	class='detail'   onClick="GoodsMaterialsReadyFun()" />                   		
                   	</td> --%>
					<td class="label-title" ><fmt:message key="biolims.common.materialApplication"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="hidden" size="20" readonly="readOnly"  id="goodsMaterialsSend_goodsMaterialsApply_name"  value="<s:property value="goodsMaterialsSend.goodsMaterialsApply.name"/>"/>
 						<input type="text" id="goodsMaterialsSend_goodsMaterialsApply" name="goodsMaterialsSend.goodsMaterialsApply.id"  value="<s:property value="goodsMaterialsSend.goodsMaterialsApply.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectMaterialApplication"/>' id='showgoodsMaterialsApply' src='${ctx}/images/img_lookup.gif' 	class='detail'   onClick="GoodsMaterialsApplyFun()" />                   		
                   	</td>
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.provincesAndAutonomousRSegions"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="30" id="goodsMaterialsSend_goodsMaterialsApply_applyOrganize_name"
                   	 name="goodsMaterialsSend.goodsMaterialsApply.applyOrganize.name" title="<fmt:message key="biolims.common.provincesAndAutonomousRSegions"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="goodsMaterialsSend.goodsMaterialsApply.applyOrganize.name"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.transportBoxNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="10" id="goodsMaterialsSend_num"
                   	 name="goodsMaterialsSend.num" title="<fmt:message key="biolims.common.transportBoxNumber"/>"
                   	   
	value="<s:property value="goodsMaterialsSend.num"/>"
                   	  />
                   	  
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showconfirmUser" title='<fmt:message key="biolims.common.selectTheAuditor"/>'
				hasHtmlFrame="true"
				html="${ctx}/goods/mate/goodsMaterialsSend/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('goodsMaterialsSend_confirmUser').value=rec.get('id');
				document.getElementById('goodsMaterialsSend_confirmUser_name').value=rec.get('name');" />
				
			
			
               	 	<%-- <td class="label-title" >审核人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="goodsMaterialsSend_confirmUser_name"  value="<s:property value="goodsMaterialsSend.confirmUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="goodsMaterialsSend_confirmUser" name="goodsMaterialsSend.confirmUser.id"  value="<s:property value="goodsMaterialsSend.confirmUser.id"/>" > 
 						<img alt='选择审核人' id='showconfirmUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			
               	 	<td class="label-title" >审核日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="" id="goodsMaterialsSend_confirmDate"
                   	 name="goodsMaterialsSend.confirmDate" title="审核日期"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="goodsMaterialsSend.confirmDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			</tr>
			<tr> --%>
			
               	 	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.workflowState"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	  
 					<input type="hidden" id="goodsMaterialsSend_state" name="goodsMaterialsSend.state" value="<s:property value="goodsMaterialsSend.state"/>"
                   	/></td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.workflowState"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="30" id="goodsMaterialsSend_stateName"
                   	 name="goodsMaterialsSend.stateName" title="<fmt:message key="biolims.common.workflowState"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="goodsMaterialsSend.stateName"/>"
                   	  />
                   	  
                   	</td>
                   	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.expressCompanyID"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	  
 					<input type="hidden" id="goodsMaterialsSend_companyId" name="goodsMaterialsSend.companyId" value="<s:property value="goodsMaterialsSend.companyId"/>"
                   	/></td>
			<td class="label-title"><fmt:message key="biolims.common.attachment"/></td><td></td>
						<td title="<fmt:message key="biolims.common.afterSaveCanLookAtTheAttachment"/>" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message key="biolims.common.attachment"/></span>
			</tr>
			
			
            </table>
            <input type="hidden" name="goodsMaterialsSendItemJson" id="goodsMaterialsSendItemJson" value="" />
            <input type="hidden" name="goodsMaterialsSendBoxJson" id="goodsMaterialsSendBoxJson" value="" />
            <input type="hidden" name="goodsMaterialsSendExpressJson" id="goodsMaterialsSendExpressJson" value="" />
            <input type="hidden" name="goodsMaterialsSendItemTwoJson" id="goodsMaterialsSendItemTwoJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="goodsMaterialsSend.id"/>" />
            </form>
            <div id="tabs">
            <ul>
			
			<!-- <li><a href="#goodsMaterialsSendItempage">物料发放明细</a></li> -->
			<li><a href="#goodsMaterialsSendItempage2"><fmt:message key="biolims.common.materialIssueDetail"/></a></li>
			<li><a href="#goodsMaterialsSendBoxpage"><fmt:message key="biolims.common.materialTransportBoxDetail"/></a></li>
			<li><a href="#goodsMaterialsSendExpresspage"><fmt:message key="biolims.common.deliveryDetail"/></a></li>
			
           	</ul> 
			<!-- <div id="goodsMaterialsSendItempage" width="100%" height:10px></div> -->
			<div id="goodsMaterialsSendItempage2" width="100%" height:10px></div>
			<div id="goodsMaterialsSendBoxpage" width="100%" height:10px></div>
			<div id="goodsMaterialsSendExpresspage" width="100%" height:10px></div>
			</div>
        	</div>
	</body>
	</html>
