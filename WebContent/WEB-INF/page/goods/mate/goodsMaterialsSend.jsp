﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/goods/mate/goodsMaterialsSend.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="18" id="goodsMaterialsSend_id"
                   	 name="id" searchField="true" title="<fmt:message key="biolims.common.serialNumber"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="60" id="goodsMaterialsSend_name"
                   	 name="name" searchField="true" title="<fmt:message key="biolims.common.describe"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showcreateUser" title='<fmt:message key="biolims.common.selectTheCreatePerson"/>'
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('goodsMaterialsSend_createUser').value=rec.get('id');
				document.getElementById('goodsMaterialsSend_createUser_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="goodsMaterialsSend_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="goodsMaterialsSend_createUser" name="goodsMaterialsSend.createUser.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.selectTheCreatePerson"/>' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showgoodsMaterialsReady" title='<fmt:message key="biolims.common.selectMaterialToPrepare"/>'
				hasHtmlFrame="true"
				html="${ctx}/goods/mate/goodsMaterialsSend/goodsMaterialsReadySelect.action"
				isHasSubmit="false" functionName="GoodsMaterialsReadyFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('goodsMaterialsSend_goodsMaterialsReady').value=rec.get('id');
				document.getElementById('goodsMaterialsSend_goodsMaterialsReady_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.materialApplication"/></td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="goodsMaterialsSend_goodsMaterialsReady_name" searchField="true"  name="goodsMaterialsReady.name"  value="" class="text input" />
 						<input type="hidden" id="goodsMaterialsSend_goodsMaterialsReady" name="goodsMaterialsSend.goodsMaterialsReady.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.selectMaterialToPrepare"/>' id='showgoodsMaterialsReady' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.provincesAndAutonomousRSegions"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="40" maxlength="30" id="goodsMaterialsSend_goodsMaterialsApply"
                   	 name="goodsMaterialsApply" searchField="true" title="<fmt:message key="biolims.common.provincesAndAutonomousRSegions"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.recipient"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="40" maxlength="30" id="goodsMaterialsSend_receiveUser"
                   	 name="receiveUser" searchField="true" title="<fmt:message key="biolims.common.recipient"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showphone" title='<fmt:message key="biolims.common.selectTheRecipientPhone"/>'
				hasHtmlFrame="true"
				html="${ctx}/goods/mate/goodsMaterialsSend/stringSelect.action"
				isHasSubmit="false" functionName="StringFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('goodsMaterialsSend_phone').value=rec.get('id');
				document.getElementById('goodsMaterialsSend_phone_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.recipientPhone"/></td>
                   	<td align="left"  >
 						<input type="text" size="40"   id="goodsMaterialsSend_phone_name" searchField="true"  name="phone.name"  value="" class="text input" />
 						<input type="hidden" id="goodsMaterialsSend_phone" name="goodsMaterialsSend.phone.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.selectTheRecipientPhone"/>' id='showphone' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.shippingAddress"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="40" maxlength="30" id="goodsMaterialsSend_address"
                   	 name="address" searchField="true" title="<fmt:message key="biolims.common.shippingAddress"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showcompany" title='<fmt:message key="biolims.common.selectCourierCompany"/>'
				hasHtmlFrame="true"
				html="${ctx}/system/express/expressCompany/expressCompanySelect.action"
				isHasSubmit="false" functionName="DicTypeFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('goodsMaterialsSend_company').value=rec.get('id');
				document.getElementById('goodsMaterialsSend_company_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.courierCompany"/></td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="goodsMaterialsSend_company_name" searchField="true"  name="company.name"  value="" class="text input" />
 						<input type="hidden" id="goodsMaterialsSend_company" name="goodsMaterialsSend.company.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.selectCourierCompany"/>' id='showcompany' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.transportBoxNumber"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="10" id="goodsMaterialsSend_num"
                   	 name="num" searchField="true" title="<fmt:message key="biolims.common.transportBoxNumber"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showconfirmUser" title='<fmt:message key="biolims.common.selectTheAuditor"/>'
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('goodsMaterialsSend_confirmUser').value=rec.get('id');
				document.getElementById('goodsMaterialsSend_confirmUser_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.auditor"/></td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="goodsMaterialsSend_confirmUser_name" searchField="true"  name="confirmUser.name"  value="" class="text input" />
 						<input type="hidden" id="goodsMaterialsSend_confirmUser" name="goodsMaterialsSend.confirmUser.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.selectTheAuditor"/>' id='showconfirmUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.auditDate"/></td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startconfirmDate" name="startconfirmDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="confirmDate1" name="confirmDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endconfirmDate" name="endconfirmDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="confirmDate2" name="confirmDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.workflowState"/></td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="40" maxlength="30" id="goodsMaterialsSend_state"
                   	 name="state" searchField="true" title="<fmt:message key="biolims.common.workflowState"/>"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
 					<input type="hidden" id="goodsMaterialsSend_state" name="state" searchField="true" value=""/>"
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.workflowState"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="40" maxlength="30" id="goodsMaterialsSend_stateName"
                   	 name="stateName" searchField="true" title="<fmt:message key="biolims.common.workflowState"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_goodsMaterialsSend_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_goodsMaterialsSend_tree_page"></div>
</body>
</html>



