﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=goodsMaterialsPost&id=${goodsMaterialsPost.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/goods/mate/goodsMaterialsPostEdit.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" >编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="18" id="goodsMaterialsPost_id"
                   	 name="goodsMaterialsPost.id" title="编号"
                   	   readonly = "readOnly" class="text input readonlytrue" 
	value="<s:property value="goodsMaterialsPost.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="40" maxlength="60" id="goodsMaterialsPost_name"
                   	 name="goodsMaterialsPost.name" title="描述"
                   	   
	value="<s:property value="goodsMaterialsPost.name"/>"
                   	  />
                   	  
                   	</td>
			
			
			<g:LayOutWinTag buttonId="showtype" title="选择寄送类型"
				hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="postType" hasSetFun="true"
				documentId="goodsMaterialsPost_type"
				documentName="goodsMaterialsPost_type_name"
			/>
			
			
               	 	<td class="label-title" >寄送类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="goodsMaterialsPost_type_name"  value="<s:property value="goodsMaterialsPost.type.name"/>"  />
 						<input type="hidden" id="goodsMaterialsPost_type" name="goodsMaterialsPost.type.id"  value="<s:property value="goodsMaterialsPost.type.id"/>" > 
 						<img alt='选择寄送类型' id='showtype' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
			
			
			
		<%-- 	<g:LayOutWinTag buttonId="showgoodsMaterialsSend" title="选择物料发放"
				hasHtmlFrame="true"
				html="${ctx}/goods/mate/goodsMaterialsSend/goodsMaterialsSendSelect.action"
				isHasSubmit="false" functionName="GoodsMaterialsSendFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('goodsMaterialsPost_goodsMaterialsSend').value=rec.get('id');
				document.getElementById('goodsMaterialsPost_goodsMaterialsSend_name').value=rec.get('name');" /> --%>
				
			
			
               	 	<td class="label-title" >物料发放</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="hidden" size="20" readonly="readOnly"  id="goodsMaterialsPost_goodsMaterialsSend_name"  value="<s:property value="goodsMaterialsPost.goodsMaterialsSend.name"/>"  />
 						<input type="text" id="goodsMaterialsPost_goodsMaterialsSend" name="goodsMaterialsPost.goodsMaterialsSend.id"  value="<s:property value="goodsMaterialsPost.goodsMaterialsSend.id"/>" > 
 						<img alt='选择物料发放' id='showgoodsMaterialsSend' src='${ctx}/images/img_lookup.gif' 	class='detail'  onClick="GoodsMaterialsSendFun()"  />                   		
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('goodsMaterialsPost_createUser').value=rec.get('id');
				document.getElementById('goodsMaterialsPost_createUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" >创建人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="goodsMaterialsPost_createUser_name"  value="<s:property value="goodsMaterialsPost.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="goodsMaterialsPost_createUser" name="goodsMaterialsPost.createUser.id"  value="<s:property value="goodsMaterialsPost.createUser.id"/>" > 
<%--  						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 --%>                   	</td>
			
			
               	 	<td class="label-title" >创建日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="" id="goodsMaterialsPost_createDate"
                   	 name="goodsMaterialsPost.createDate" title="创建日期"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="goodsMaterialsPost.createDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title"  style="display:none"  >状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	  
 					<input type="hidden" id="goodsMaterialsPost_state" name="goodsMaterialsPost.state" value="<s:property value="goodsMaterialsPost.state"/>"
                   	/></td>
			
			
               	 	<td class="label-title" >状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="hidden" size="20" maxlength="30" id="goodsMaterialsPost_state"
                   	 name="goodsMaterialsPost.state" readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="goodsMaterialsPost.state"/>"
                   	  />
                   	<input type="text" size="20" maxlength="30" id="goodsMaterialsPost_stateName"
                   	 name="goodsMaterialsPost.stateName" title="状态"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="goodsMaterialsPost.stateName"/>"
                   	  />
                   	  
                   	</td>
			<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span></td>
			</tr>
			
			
            </table>
            <input type="hidden" name="goodsMaterialsPostItemJson" id="goodsMaterialsPostItemJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="goodsMaterialsPost.id"/>" />
            </form>
<!--             <div id="tabs">
            <ul>
			<li><a href="#goodsMaterialsPostItempage">快递寄送明细</a></li>
           	</ul>  -->
			<div id="goodsMaterialsPostItempage" width="100%" height:10px></div>
			</div>
        <!-- 	</div> -->
	</body>
	</html>
