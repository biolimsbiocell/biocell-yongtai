﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<style>
/* .x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: nowrap;
} */

</style>
<script type="text/javascript" src="${ctx}/js/goods/mate/goodsMaterialsPostItem.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>
<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" >快递单</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="60" id="goodsMaterialsPostItem_expressOrder"
                   	 name="expressOrder" searchField="true" title="快递单"    />
                   	 </td>
                   	 
                   	 <td class="label-title" >收件人</td>
                   	<td align="left"  >
                  
					<input type="text" size="15" maxlength="20" id="goodsMaterialsPostItem_receiver"
                   	 name="receiver" searchField="true" title="收件人"    />
                   	 </td>
                   	 
               	 	
			</tr>
			<tr>
                   	<td class="label-title" ><span>是否提交</span></td>
                	<td><select id="goodsMaterialsPostItem_submit" name="submit" searchField="true" style="width:64">
               			<option value="" size="5">请选择</option>
    					<option value="1" size="5">是</option>
    					<option value="0" size="5">否</option>
						</select>
                 	</td>
                 	
               	 	<td class="label-title" >快递公司</td>
                   	<td align="left"  >
                  
					<input type="text" size="15" maxlength="20" id="goodsMaterialsPostItem_company"
                   	 name="company" searchField="true" title="快递公司"    />
                   	
          			 </td>
          			 
                   	
			</tr>
            </table>          
		</form>
		</div>

	<div id="goodsMaterialsPostItemdiv"></div>
	<div id="bat_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsv">上传CSV文件
	</div>
	<div id="many_bat_div" style="display: none">
		<div class="ui-widget;">
			<div class="ui-state-highlight ui-corner-all jquery-ui-warning">
			</div>
		</div>
		<textarea id="many_bat_text" style="width:650px;height: 339px"></textarea>
	</div>
	
	<div id="bat_submit_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span>是否提交</span></td>
                <td><select id="submit"  style="width:100">
               			<option value="">请选择</option>
    					<option value="1">是</option>
    					<option value="0">否</option>
					</select>
                 </td>
			</tr>
	</table>
	</div>
</body>
</html>


