﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title='<fmt:message key="biolims.common.attachment"/>'
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=goodsMaterialsApply&id=${goodsMaterialsApply.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/goods/mate/goodsMaterialsApplyEdit.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
				<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
	                   	<input type="text" size="20" maxlength="60" id="goodsMaterialsApply_id"
	                   	    class="text input readonlytrue" readonly="readOnly" 
	                   	 	name="goodsMaterialsApply.id" title="<fmt:message key="biolims.common.serialNumber"/>" value="<s:property value="goodsMaterialsApply.id"/>"
	                   	  />
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
	                   	<input type="text" size="23" maxlength="60" id="goodsMaterialsApply_name"
	                   	 	name="goodsMaterialsApply.name" title="<fmt:message key="biolims.common.describe"/>" value="<s:property value="goodsMaterialsApply.name"/>"
	                   	  />
                   	</td>
			<%-- html="${ctx}/goods/mate/goodsMaterialsApply/applyOrganizeSelect.action" --%>
			<%-- <g:LayOutWinTag buttonId="showapplyOrganize" title="选择所属省区"
				hasHtmlFrame="true"
 				
 				html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="ApplyOrganizeFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('goodsMaterialsApply_applyOrganize').value=rec.get('id');
				document.getElementById('goodsMaterialsApply_applyOrganize_name').value=rec.get('name');" /> --%>
               	 	<td class="label-title" ><fmt:message key="biolims.common.provincesAndAutonomousRSegions"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="goodsMaterialsApply_applyOrganize_name"  value="<s:property value="goodsMaterialsApply.applyOrganize.name"/>"/>
 						<input type="hidden" id="goodsMaterialsApply_applyOrganize" name="goodsMaterialsApply.applyOrganize.id"  value="<s:property value="goodsMaterialsApply.applyOrganize.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectTheAgent"/>' id='showapplyOrganize' src='${ctx}/images/img_lookup.gif' 	class='detail'   onClick="ApplyOrganizeFun()" />                 		
                   	</td>
			</tr>
			<tr>
			<%-- html="${ctx}/goods/mate/goodsMaterialsApply/applyDeptSelect.action" --%>
			<%-- <g:LayOutWinTag buttonId="showapplyDept" title="选择单位名称"
				hasHtmlFrame="true"
				
				html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="ApplyDeptFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('goodsMaterialsApply_applyDept').value=rec.get('id');
				document.getElementById('goodsMaterialsApply_applyDept_name').value=rec.get('name');" /> --%>
               	 	<td class="label-title" ><fmt:message key="biolims.common.nameOfInstitution"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="goodsMaterialsApply_applyDept" name="goodsMaterialsApply.applyDept" title="<fmt:message key="biolims.common.nameOfInstitution"/>" value="<s:property value="goodsMaterialsApply.applyDept"/>"  class="text input readonlytrue" readonly="readOnly" />
 						<%-- <input type="hidden" id="goodsMaterialsApply_applyDept" name="goodsMaterialsApply.applyDept.id"  value="<s:property value="goodsMaterialsApply.applyDept.id"/>" > --%> 
<%--  						<img alt='选择单位名称' id='showapplyDept' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 --%>                   	</td>
		
			<%-- <g:LayOutWinTag buttonId="showtype" title="选择申请类型"
				hasHtmlFrame="true"
				html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="DicTypeFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('goodsMaterialsApply_type').value=rec.get('id');
				document.getElementById('goodsMaterialsApply_type_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" >申请类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="goodsMaterialsApply_type_name"  value="<s:property value="goodsMaterialsApply.type.name"/>"/>
 						<input type="hidden" id="goodsMaterialsApply_type" name="goodsMaterialsApply.type.id"  value="<s:property value="goodsMaterialsApply.type.id"/>" > 
 						<img alt='选择申请类型' id='showtype' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
                   	<td class="label-title" ><fmt:message key="biolims.common.applicationType"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<select id="goodsMaterialsApply_type" name="goodsMaterialsApply.type" style="width:169px" class="input-10-length" >
								<option value="0" <s:if test="goodsMaterialsApply.type==0">selected="selected"</s:if>><fmt:message key="biolims.common.send"/></option>
								<option value="1" <s:if test="goodsMaterialsApply.type==1">selected="selected"</s:if>><fmt:message key="biolims.common.invite"/></option>
<!-- 								<option value="2" <s:if test="crmCustomer.profession==2">selected="selected"</s:if>>医院</option>
 -->					</select>
                   	</td>

               	 	<td class="label-title" ><fmt:message key="biolims.common.shippingAddress"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" id="goodsMaterialsApply_address_name"  value="<s:property value="goodsMaterialsApply.address.name"/>"/>
 						<input type="hidden" id="goodsMaterialsApply_address" name="goodsMaterialsApply.address.id"  value="<s:property value="goodsMaterialsApply.address.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectTheShippingAddress"/>' id='showAddress' src='${ctx}/images/img_lookup.gif' 	class='detail'   onClick="ApplyOrganizeAddressFun()" />                   		
                   	</td>          	 	
					<%-- <td class="label-title" >收货地址</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<select id="goodsMaterialsApply_address" name="goodsMaterialsApply_address_name"  onChange="change()" style="width:140px">
								<c:forEach var="address" items="${list}">
								<option value="${address.id}" selected>${address.name}</option></c:forEach>
						</select>
                   	</td> --%>
			</tr>
			<tr>

			
			<%-- 
			<g:LayOutWinTag buttonId="showreceiveUser" title="选择收件人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('goodsMaterialsApply_receiveUser').value=rec.get('id');
				document.getElementById('goodsMaterialsApply_receiveUser_name').value=rec.get('name');" /> --%>
	<%-- 			
			
			
               	 	<td class="label-title" >收件人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="goodsMaterialsApply_receiveUser_name"  value="<s:property value="goodsMaterialsApply.receiveUser"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="goodsMaterialsApply_receiveUser" name="goodsMaterialsApply.receiveUser.id"  value="<s:property value="goodsMaterialsApply.receiveUser.id"/>" > 
  						<img alt='选择收件人' id='showreceiveUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                  	</td> --%>
			
			<td class="label-title" ><fmt:message key="biolims.common.recipients"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="30" id="goodsMaterialsApply_receiveUser"
                   	 name="goodsMaterialsApply.receiveUser" title="<fmt:message key="biolims.common.recipients"/>"
                   	 
	value="<s:property value="goodsMaterialsApply.receiveUser"/>"
                   	  />
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.recipientPhone"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="23" maxlength="30" id="goodsMaterialsApply_phone"
                   	 name="goodsMaterialsApply.phone" title="<fmt:message key="biolims.common.recipientPhone"/>"
                   	   
	value="<s:property value="goodsMaterialsApply.phone"/>"
                   	  />
                   	  
                   	</td>
			
				<td class="label-title" ><fmt:message key="biolims.common.courierCompany"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="goodsMaterialsApply_company_name"  value="<s:property value="goodsMaterialsApply.company.name"/>"/>
 						<input type="hidden" id="goodsMaterialsApply_company" name="goodsMaterialsApply.company.id"  value="<s:property value="goodsMaterialsApply.company.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectCourierCompany"/>' id='showcompany' src='${ctx}/images/img_lookup.gif' 	class='detail' onClick="ExpressCompanyFun()"/>                   		
                   	</td>
               	 	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.workflowState"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
 						<input type="hidden" id="goodsMaterialsApply_state" name="goodsMaterialsApply.state" value="<s:property value="goodsMaterialsApply.state"/>"
                   		/>
                   	</td>
			
			
			</tr>
			<tr>
               	 	
					<td class="label-title" ><fmt:message key="biolims.common.priority"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  <select name="goodsMaterialsApply.priority" id="goodsMaterialsApply_priority" style="width:169px" >
    					<option value="0" <s:if test="goodsMaterialsApply.priority==0">selected="selected" </s:if>><fmt:message key="biolims.common.ordinary"/></option>
    					<option value="1" <s:if test="goodsMaterialsApply.priority==1">selected="selected" </s:if>><fmt:message key="biolims.common.high"/></option>
					</select>
                   	</td>
			<%-- <g:LayOutWinTag buttonId="showcompany" title="选择快递公司"
				hasHtmlFrame="true" html="${ctx}/system/express/expressCompany/expressCompanySelect.action"
				isHasSubmit="false" functionName="ExpressCompanyFun" hasSetFun="true"
				documentId="goodsMaterialsApply_company"
				documentName="goodsMaterialsApply_company_name"
			/> --%>
			
			<td class="label-title" ><fmt:message key="biolims.common.deliveryTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="" id="goodsMaterialsApply_sendDate"
                   	 	name="goodsMaterialsApply.sendDate" title="<fmt:message key="biolims.common.deliveryTime"/>"
                   	    
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="goodsMaterialsApply.sendDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
               	 <td class="label-title"><fmt:message key="biolims.common.attachment"/></td>
					<td></td>
					<td title="<fmt:message key="biolims.common.afterSaveCanLookAtTheAttachment"/>" id="doclinks_img">
						<span class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message key="biolims.common.attachment"/></span>
					</td>
             </tr>
			<tr>
			
			<g:LayOutWinTag buttonId="showcreateUser" title='<fmt:message key="biolims.common.selectTheCreatePerson"/>'
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('goodsMaterialsApply_createUser').value=rec.get('id');
				document.getElementById('goodsMaterialsApply_createUser_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="goodsMaterialsApply_createUser_name"  value="<s:property value="goodsMaterialsApply.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="goodsMaterialsApply_createUser" name="goodsMaterialsApply.createUser.id"  value="<s:property value="goodsMaterialsApply.createUser.id"/>" > 
<%--  						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 --%>                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.workflowState"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="60" id="goodsMaterialsApply_stateName"
	                   	 	name="goodsMaterialsApply.stateName" title="<fmt:message key="biolims.common.workflowState"/>"
	                   	   	readonly = "readOnly" class="text input readonlytrue"  value="<s:property value="goodsMaterialsApply.stateName"/>"
	                   	  />
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="" id="goodsMaterialsApply_createDate"
                   	 name="goodsMaterialsApply.createDate" title="<fmt:message key="biolims.common.commandTime"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="goodsMaterialsApply.createDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
					
					
			</tr>
			<!-- <tr>		
					<td></td>
					<td></td>
					<td class="label-title" id="message" style="font-size:16px;color:red;" >申请的物料将在一周内到达！</td>
					
			</tr> -->
			
			
            </table>
         
            
            <input type="hidden" name="goodsMaterialsApplyWayJson" id="goodsMaterialsApplyWayJson" value="" />
            <input type="hidden" name="goodsMaterialsApplyItemJson" id="goodsMaterialsApplyItemJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="goodsMaterialsApply.id"/>" />
            </form>
            <div id="tabs">
            <ul>
            <li><a href="#goodsMaterialsApplyItempage"><fmt:message key="biolims.common.materialForDetail"/></a></li>
			<li><a href="#goodsMaterialsApplyWaypage"><fmt:message key="biolims.common.courierTrackingDetails"/></a></li>
           	</ul> 
			<div id="goodsMaterialsApplyWaypage" width="100%" height:10px></div>
			<div id="goodsMaterialsApplyItempage" width="100%" height:10px></div>
			</div>
        	</div>
	</body>
	</html>
