﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/goods/mate/goodsMaterialsReadyDialogByState.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">编号</td>
               	<td align="left">
                    		<input type="text" maxlength="18" id="goodsMaterialsReady_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">描述</td>
               	<td align="left">
                    		<input type="text" maxlength="60" id="goodsMaterialsReady_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">申请单</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="goodsMaterialsReady_goodsMaterialsApply" searchField="true" name="goodsMaterialsApply"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">所属省区</td>
               	<td align="left">
                    		<input type="text" maxlength="30" id="goodsMaterialsReady_applyOrganize" searchField="true" name="applyOrganize"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">收件人</td>
               	<td align="left">
                    		<input type="text" maxlength="60" id="goodsMaterialsReady_receiveUser" searchField="true" name="receiveUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">收件人电话</td>
               	<td align="left">
                    		<input type="text" maxlength="30" id="goodsMaterialsReady_phone" searchField="true" name="phone"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">收货地址</td>
               	<td align="left">
                    		<input type="text" maxlength="30" id="goodsMaterialsReady_address" searchField="true" name="address"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">创建人</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="goodsMaterialsReady_createUser" searchField="true" name="createUser"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">创建日期</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="goodsMaterialsReady_createDate" searchField="true" name="createDate"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">审核人</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="goodsMaterialsReady_confirmUser" searchField="true" name="confirmUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">审核时间</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="goodsMaterialsReady_confirmDate" searchField="true" name="confirmDate"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">工作流状态</td>
               	<td align="left">
                    		<input type="text" maxlength="30" id="goodsMaterialsReady_state" searchField="true" name="state"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">工作流状态</td>
               	<td align="left">
                    		<input type="text" maxlength="30" id="goodsMaterialsReady_stateName" searchField="true" name="stateName"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue">搜索</font></span>
		
		<div id="show_dialog_goodsMaterialsReadyByState_div"></div>
   		
</body>
</html>



