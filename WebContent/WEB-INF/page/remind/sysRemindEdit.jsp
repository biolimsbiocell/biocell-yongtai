<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="sysRemind_label" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="/operfile/initFileList.action?modelType=sysRemind&id=sysRemind.id"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<script type="text/javascript" src="${ctx}/javascript/remind/sysRemindEdit.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleVaLeave.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
				<tr>
               	 	<td class="label-title">编码</td>
               	 		<td class="requiredcolumn" nowrap width="10px"><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
               	 	
                   	<td align="left">
                   	<input type="text" size="20" maxlength="36" id="sysRemind_id" name="sysRemind.id" title="编码" 
                   		 	class="text input"
                   		 value="<s:property value="sysRemind.id"/>" 
                   	 />
                   	</td>
               	 	<td class="label-title">提示标题</td>
               	 		<td class="requiredcolumn" nowrap width="10px"><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left">
                   	<input type="text" size="15" maxlength="200" id="sysRemind_title" name="sysRemind.title" title="提示标题" 
                   		 	class="text input"
                   		 value="<s:property value="sysRemind.title"/>" 
                   	 />
                   	</td>
               	 	<td class="label-title">提示人</td>
               	 		<td class="requiredcolumn" nowrap width="10px"><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left">
                   	<input type="text" size="10" maxlength="32" id="sysRemind_remindUser" name="sysRemind.remindUser" title="提示人" 
                   		 	class="text input"
                   		 value="<s:property value="sysRemind.remindUser"/>" 
                   	 />
                   	</td>
				</tr>
				<tr>
			
               	 	<td class="label-title">提示阅读人</td>
               	 		<td class="requiredcolumn" nowrap width="10px"><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left">
 						<input type="text" size="10" readonly="readOnly"  id="sysRemind_readUser_name"  value="<s:property value="sysRemind.readUser.name"/>" class="text input"/>
 						<input type="hidden" id="sysRemind_readUser" name="sysRemind.readUser.id"  value="<s:property value="sysRemind.readUser.id"/>" > 
 						<img alt='选择提示阅读人' id='showreadUser' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td>
               	 	<td class="label-title">提示日期</td>
               	 		<td class="requiredcolumn" nowrap width="10px"><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left">
                   	<input type="text" size="12" maxlength="50" id="sysRemind_startDate" name="sysRemind.startDate" title="提示日期" 
                   		 	Class="Wdate"  onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"    
                   		value="<s:date name="sysRemind.startDate" format="yyyy-MM-dd"/>"
                   	 />
                   	</td>
               	 	<td class="label-title">阅读日期</td>
               	 		<td class="requiredcolumn" nowrap width="10px"><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left">
                   	<input type="text" size="12" maxlength="50" id="sysRemind_readDate" name="sysRemind.readDate" title="阅读日期" 
                   		 	Class="Wdate"  onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"    
                   		value="<s:date name="sysRemind.readDate" format="yyyy-MM-dd"/>"
                   	 />
                   	</td>
				</tr>
				<tr>
               	 	<td class="label-title">内容
               	 		<td class="requiredcolumn" nowrap width="10px"><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left" colspan = "6">
                   	<textarea class="text input  false"  style="width:400px;height:200px"><s:property value="sysRemind.content"/></textarea>
                   	</td>
		
				</tr>
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="sysRemind.id"/>" />
            </form>
        	</div>
	</body>
	</html>
