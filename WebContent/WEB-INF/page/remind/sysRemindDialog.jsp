﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/javascript/remind/sysRemindDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">编码</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="sysRemind_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">提示标题</td>
               	<td align="left">
                    		<input type="text" maxlength="200" id="sysRemind_title" searchField="true" name="title"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">提示人</td>
               	<td align="left">
                    		<input type="text" maxlength="32" id="sysRemind_remindUser" searchField="true" name="remindUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">提示阅读人</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="sysRemind_readUser" searchField="true" name="readUser"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">提示日期</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="sysRemind_startDate" searchField="true" name="startDate"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">阅读日期</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="sysRemind_readDate" searchField="true" name="readDate"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">内容</td>
               	<td align="left">
                    		<input type="text" maxlength="2000" id="sysRemind_content" searchField="true" name="content"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">处理人</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="sysRemind_handleUser" searchField="true" name="handleUser"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">状态</td>
               	<td align="left">
                    		<input type="text" maxlength="32" id="sysRemind_state" searchField="true" name="state"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">内容id</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="sysRemind_contentId" searchField="true" name="contentId"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">表单id</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="sysRemind_tableId" searchField="true" name="tableId"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue">搜索</font></span>
		
		<div id="show_dialog_sysRemind_div"></div>
   		
</body>
</html>



