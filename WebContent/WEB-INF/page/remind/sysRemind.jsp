﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/javascript/remind/sysRemind.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table>
			<tr>
               		<td class="label-title">编码</td>
                   	<td align="left">
					
                   	<input type="text" size="20" maxlength="36" searchField="true" id="sysRemind_id" name="id" title="编码" value="" 
                   		 	class="text input"
                   	 />
                   	</td>
               		<td class="label-title">提示标题</td>
                   	<td align="left">
					
                   	<input type="text" size="15" maxlength="200" searchField="true" id="sysRemind_title" name="title" title="提示标题" value="" 
                   		 	class="text input"
                   	 />
                   	</td>
               		<td class="label-title">提示人</td>
                   	<td align="left">
					
                   	<input type="text" size="10" maxlength="32" searchField="true" id="sysRemind_remindUser" name="remindUser" title="提示人" value="" 
                   		 	class="text input"
                   	 />
                   	</td>
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showreadUser" title="选择提示阅读人"
				hasHtmlFrame="true" hasSetFun="true" width="document.body.clientWidth/1.5" 
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false"		functionName="showreadUserFun" 
				documentId="sysRemind_readUser"
				documentName="sysRemind_readUser_name"
			 />
               		<td class="label-title">提示阅读人</td>
                   	<td align="left">
 						<input type="text" size="10" readonly="readOnly"  id="sysRemind_readUser_name"  value="" class="text input"/>
 						<input type="hidden" id="sysRemind_readUser" name="readUser.id"  value="" searchField="true" > 
 						<img alt='选择提示阅读人' id='showreadUser' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td>
               		<td class="label-title">提示日期</td>
                   	<td align="left">
					
                   	<input type="text" size="12" maxlength="50" searchField="true" id="sysRemind_startDate" name="startDate" title="提示日期" value="" 
                    	Class="Wdate"  onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 
                   	 />
                   	</td>
               		<td class="label-title">阅读日期</td>
                   	<td align="left">
					
                   	<input type="text" size="12" maxlength="50" searchField="true" id="sysRemind_readDate" name="readDate" title="阅读日期" value="" 
                    	Class="Wdate"  onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 
                   	 />
                   	</td>
			</tr>
			<tr>
               		<td class="label-title">内容</td>
                   	<td align="left">
					
                   	<input type="text" size="15" maxlength="2000" searchField="true" id="sysRemind_content" name="content" title="内容" value="" 
                   		 	class="text input"
                   	 />
                   	</td>
			<g:LayOutWinTag buttonId="showhandleUser" title="选择处理人"
				hasHtmlFrame="true" hasSetFun="true" width="document.body.clientWidth/1.5" 
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false"		functionName="showhandleUserFun" 
				documentId="sysRemind_handleUser"
				documentName="sysRemind_handleUser_name"
			 />
               		<td class="label-title">处理人</td>
                   	<td align="left">
 						<input type="text" size="10" readonly="readOnly"  id="sysRemind_handleUser_name"  value="" class="text input"/>
 						<input type="hidden" id="sysRemind_handleUser" name="handleUser.id"  value="" searchField="true" > 
 						<img alt='选择处理人' id='showhandleUser' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td>
               		<td class="label-title">状态</td>
                   	<td align="left">
					
                   	<input type="text" size="15" maxlength="32" searchField="true" id="sysRemind_state" name="state" title="状态" value="" 
                   		 	class="text input"
                   	 />
                   	</td>
			</tr>
			<tr>
               		<td class="label-title">内容id</td>
                   	<td align="left">
					
                   	<input type="text" size="15" maxlength="36" searchField="true" id="sysRemind_contentId" name="contentId" title="内容id" value="" 
                   		 	class="text input"
                   	 />
                   	</td>
               		<td class="label-title">表单id</td>
                   	<td align="left">
					
                   	<input type="text" size="15" maxlength="36" searchField="true" id="sysRemind_tableId" name="tableId" title="表单id" value="" 
                   		 	class="text input"
                   	 />
                   	</td>
			</tr>
        </table>
		</form>
		</div>
		<div id="show_sysRemind_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
</body>
</html>



