<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style>
.dataTables_scrollBody {
	min-height: 100px;
}
.tablebtns{
				float:right;
			}
.dt-buttons{
				margin:0;
				float:right;
			}
.chosed {
				background-color: #5AC8D8 !important;
				color: #fff;
			}
</style>
</head>
<body>
	<div>
		<table class="table table-hover table-bordered table-condensed"
			id="addApplicationTypeTable" style="font-size: 12px;"></table>
			<input type="hidden" id="flag" value="${requestScope.flag}">
			<input type="hidden" id="lan" value="${sessionScope.lan}">
	</div>
	<script type="text/javascript"
		src="${ctx}/js/applicationTypeTable/showDialogApplicationTypeTable.js"></script>
</body>
</html>