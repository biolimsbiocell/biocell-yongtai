<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
 

    
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
 <script type="text/javascript">
	function setvalue(id,name){
		
		
		 <%if(request.getParameter("flag")!=null){%>
		 	window.parent.set<%=request.getParameter("flag")%>(id,name);
		  <%}%>
	}
</script>
</head>
<body>
<div  class="mainfullclass" id="markup">
<input type="hidden" id="id" value="" /> 
<g:GridTagNoLock isAutoWidth="true" col="${col}" height="document.body.clientHeight-30" type="${type}" title=""		url="${path}" />
</div>

</body>
</html>