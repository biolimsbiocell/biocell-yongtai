<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<title></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style>
.dataTables_scrollBody {
	min-height: 100px;
}
</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div id="tableFileLoad"></div>
	<div style="height: 14px"></div>
	<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 46px">
		<!--Form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<i class="glyphicon glyphicon-pencil"></i>偏差完成报告
					</h3>

					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
											<li>
												<a href="####" onclick="$('.buttons-print').click();"><fmt:message key="biolims.common.print"/></a>
											</li>
											<li>
												<a href="#####" onclick="$('.buttons-copy').click();"><fmt:message key="biolims.common.copyData"/></a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-excel').click();">Excel</a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-csv').click();">CSV</a>
											</li>
											<li role="separator" class="divider"></li>
											<li>
												<a href="####" onclick="itemFixedCol(2)"><fmt:message key="biolims.common.lock2Col"/></a>
											</li>
											<li>
												<a href="####" id="unfixde" onclick="unfixde()"><fmt:message key="biolims.common.cancellock2Col"/></a>
											</li>
										</ul>
						</div>
					</div>
				</div>
				<div class="box-body">
					<!--进度条-->
					<div id="wizard" class="form_wizard wizard_horizontal">
						<ul class="wizard_steps">
							<li><a class="selected step"> <span class="step_no">1</span>
									<span class="step_descr"> Step 1<br /> <small>偏差完成报告</small>
								</span>
							</a></li>
										<li><a class="done step"> <span class="step_no">2</span>
											<span class="step_descr"> Step 2<br /> <small>纠正预防措施</small></span>
								</a></li>
										<li><a class="done step"> <span class="step_no">3</span>
											<span class="step_descr"> Step 3<br /> <small>影响的产品</small></span>
								</a></li>
										<li><a class="done step"> <span class="step_no">4</span>
											<span class="step_descr"> Step 4<br /> <small>影响的物料</small></span>
								</a></li>
										<li><a class="done step"> <span class="step_no">5</span>
											<span class="step_descr"> Step 5<br /> <small>影响的设备</small></span>
								</a></li>
						</ul>
					</div>
					<!--form表单-->
					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
							<input type="hidden" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId")%>" /> <br>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">编号<img class='requiredimage' src='${ctx}/images/required.gif' />  </span> 
                   						<input type="text"
											size="50" maxlength="100" id="deviationSurveyPlan_id"
											name="id"  changelog="<s:property value="deviationSurveyPlan.id"/>"
											readonly = "readOnly" class="form-control readonlytrue" 
											value="<s:property value="deviationSurveyPlan.id"/>"
						 				/>  
									</div>
								</div>				
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">描述</span> 
										<textarea id="deviationSurveyPlan_name"
											name="name" placeholder="长度限制2000个字符"
											class="form-control"
											style="overflow: hidden; width: 250px height: 80px;"><s:property
												value="deviationSurveyPlan.name" /></textarea>
									</div>
								<%-- 	<div class="input-group">
									<span class="input-group-addon">描述</span> 
                   						<input type="text"
											size="50" maxlength="100" id="deviationSurveyPlan_name"
											name="name"  changelog="<s:property value="deviationSurveyPlan.name"/>"
											class="form-control"   
											value="<s:property value="deviationSurveyPlan.name"/>"
										 />  
									</div> --%>
								</div>				
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">创建人</span> 
										<input type="text" size="20" readonly="readOnly"
												id="deviationSurveyPlan_createUser_name" name="createUser-name"
												changelog="<s:property value="deviationSurveyPlan.createUser.name"/>"
												value="<s:property value="deviationSurveyPlan.createUser.name"/>"
												class="form-control" /> 
										<input type="hidden"
												id="deviationSurveyPlan_createUser_id" name="createUser-id"
												value="<s:property value="deviationSurveyPlan.createUser.id"/>">
									</div>
								</div>				
									</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">创建时间</span> 
                   						<input type="text"
											size="50" maxlength="100" id="deviationSurveyPlan_createDate"
											name="createDate"  changelog="<s:property value="deviationSurveyPlan.createDate"/>"
											readonly = "readOnly" class="form-control readonlytrue" 
                   	  						value="<s:date name="deviationSurveyPlan.createDate" format="yyyy-MM-dd"/>"
                   	   					/>  
									</div>
								</div>				
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">状态名称</span> 
                   						<input type="text"
											size="50" maxlength="100" id="deviationSurveyPlan_stateName"
											name="stateName"  changelog="<s:property value="deviationSurveyPlan.stateName"/>"
											readonly = "readOnly" class="form-control readonlytrue" 
											value="<s:property value="deviationSurveyPlan.stateName"/>"
						 				/>  
										<input type="hidden"
											size="50" maxlength="100" id="deviationSurveyPlan_state"
											name="state"  changelog="<s:property value="deviationSurveyPlan.state"/>"
											readonly = "readOnly" class="form-control readonlytrue" 
											value="<s:property value="deviationSurveyPlan.state"/>"
						 				/>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">部门负责人</span> 
										<input type="text" size="20" readonly="readOnly" name="departmentUser-name"
												id="deviationSurveyPlan_departmentUser_name"  changelog="<s:property value="deviationSurveyPlan.departmentUser.name"/>"
												value="<s:property value="deviationSurveyPlan.departmentUser.name"/>"
												class="form-control" /> 
										<input type="hidden"
												id="deviationSurveyPlan_departmentUser_id" name="departmentUser-id"
												value="<s:property value="deviationSurveyPlan.departmentUser.id"/>">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" 
												onClick="showdepartmentUser()">
											<i class="glyphicon glyphicon-search"></i>
										</span>		
									</div>
								</div>					
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">偏差编号</span> 
                   						<input type="text"
											size="50" maxlength="100" id="deviationSurveyPlan_deviationHr_no"
											  changelog="<s:property value="deviationSurveyPlan.deviationHr.no"/>"
											class="form-control"   readonly = "readOnly"
											value="<s:property value="deviationSurveyPlan.deviationHr.no"/>"
						 				/>  
						 				<input type="hidden"
											size="50" maxlength="100" id="deviationSurveyPlan_deviationHr_id"
											name="deviationHr-id"  changelog="<s:property value="deviationSurveyPlan.deviationHr.id"/>"
											class="form-control"   
											value="<s:property value="deviationSurveyPlan.deviationHr.id"/>"
						 				/>
						 				<span class="input-group-btn">
											<button class="btn btn-info" type="button" 
												onClick="selectPC()" >
											<i class="glyphicon glyphicon-search"></i>
										</span>	
									</div>
								</div>	
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">偏差描述</span> 
                   						<input type="text"
											size="50" maxlength="100" id="deviationSurveyPlan_deviationHr_name"
											  changelog="<s:property value="deviationSurveyPlan.deviationHr.name"/>"
											class="form-control"   readonly = "readOnly"
											value="<s:property value="deviationSurveyPlan.deviationHr.name"/>"
						 				/>  
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">偏差分类</span> 
						 				<select lay-ignore id="deviationSurveyPlan_deviationHr_type"  class="form-control" disabled="disabled" >
						 					<option value="">---请选择---</option>
						 					<option value="1" <s:if test="deviationSurveyPlan.deviationHr.type==1">selected="selected"</s:if>>微小偏差</option>
						 					<option value="2" <s:if test="deviationSurveyPlan.deviationHr.type==2">selected="selected"</s:if>>一般偏差</option>
						 					<option value="3" <s:if test="deviationSurveyPlan.deviationHr.type==3">selected="selected"</s:if>>重大偏差</option>
						 				</select> 
									</div>
								</div>			
								<%-- <div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">序列号</span> 
                   						<input type="text"
											size="50" maxlength="100" id="deviationSurveyPlan_serialNumber"
											name="serialNumber"  changelog="<s:property value="deviationSurveyPlan.serialNumber"/>"
											class="form-control"   
											value="<s:property value="deviationSurveyPlan.serialNumber"/>"
						 				/>  
									</div>
								</div>	 --%>			
								<%-- <div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">原因调查</span> 
                   						<input type="text"
											size="50" maxlength="100" id="deviationSurveyPlan_reason"
											name="reason"  changelog="<s:property value="deviationSurveyPlan.reason"/>"
											class="form-control"   
											value="<s:property value="deviationSurveyPlan.reason"/>"
										 />  
					
									</div>
								</div> --%>				
							</div>
							<%-- <div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
										<div class="input-group">
										<span class="input-group-addon">选择小组</span> 
											<input type="text" size="20" readonly="readOnly" 
													id="deviationSurveyPlan_userGroup_name"  changelog="<s:property value="deviationSurveyPlan.userGroup.name"/>"
													value="<s:property value="deviationSurveyPlan.userGroup.name"/>"
													class="form-control" /> 
											<input type="hidden"
													id="deviationSurveyPlan_userGroup_id" name="userGroup-id"
													value="<s:property value="deviationSurveyPlan.userGroup.id"/>">
											<span class="input-group-btn">
												<button class="btn btn-info" type="button" 
													onClick="showUserGroup()">
												<i class="glyphicon glyphicon-search"></i>
											</span>		
										</div>
									</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">组长</span> 
										<input type="text" size="20" readonly="readOnly" name="groupLeader-name"
												id="deviationSurveyPlan_groupLeader_name"  changelog="<s:property value="deviationSurveyPlan.groupLeader.name"/>"
												value="<s:property value="deviationSurveyPlan.groupLeader.name"/>"
												class="form-control" /> 
										<input type="hidden"
												id="deviationSurveyPlan_groupLeader_id" name="groupLeader-id"
												value="<s:property value="deviationSurveyPlan.groupLeader.id"/>">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" 
												onClick="showgroupLeader()">
											<i class="glyphicon glyphicon-search"></i>
										</span>		
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">组员</span> 
                   						<input type="text"
											size="50" maxlength="100" id="deviationSurveyPlan_groupMembers"
											name="groupMembers"  changelog="<s:property value="deviationSurveyPlan.groupMembers"/>"
											class="form-control"   
											value="<s:property value="deviationSurveyPlan.groupMembers"/>"
						 				/> 
						 				<span class="input-group-btn">
											<button class="btn btn-info" type="button" 
												onClick="showGroupMembers()">
											<i class="glyphicon glyphicon-search"></i>
										</span> 
									</div>
								</div>				
							</div> --%>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">QA负责人</span> 
										<input type="text" size="20" readonly="readOnly" name="auditUser-name"
												id="deviationSurveyPlan_auditUser_name"  changelog="<s:property value="deviationSurveyPlan.auditUser.name"/>"
												value="<s:property value="deviationSurveyPlan.auditUser.name"/>"
												class="form-control" /> 
										<input type="hidden"
												id="deviationSurveyPlan_auditUser_id" name="auditUser-id"
												value="<s:property value="deviationSurveyPlan.auditUser.id"/>">
										
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" 
												onClick="showauditUser()">
											<i class="glyphicon glyphicon-search"></i>
										</span>		
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">QA负责人审批意见</span> 
                   						<input type="text"
											size="50" maxlength="100" id="deviationSurveyPlan_auditOpinion"
											name="auditOpinion"  changelog="<s:property value="deviationSurveyPlan.auditOpinion"/>"
											class="form-control"   
											readOnly
											value="<s:property value="deviationSurveyPlan.auditOpinion"/>"
						 				/>  
									</div>
								</div>				
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">QA负责人审批日期</span> 
                   						<input type="text"
											size="50" maxlength="100" id="deviationSurveyPlan_auditDate"
											name="auditDate"  changelog="<s:property value="deviationSurveyPlan.auditDate"/>"
											class="form-control readonlytrue"
											readonly = "readOnly"   
                   	  						value="<s:date name="deviationSurveyPlan.auditDate" format="yyyy-MM-dd"/>"
                   	   					/>  
									</div>
								</div>				
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">产品质量负责人</span> 
										<input type="text" size="20" readonly="readOnly" name="approvalUser-name"
												id="deviationSurveyPlan_approvalUser_name"  changelog="<s:property value="deviationSurveyPlan.approvalUser.name"/>"
												value="<s:property value="deviationSurveyPlan.approvalUser.name"/>"
												class="form-control" /> 
										<input type="hidden"
												id="deviationSurveyPlan_approvalUser_id" name="approvalUser-id"
												value="<s:property value="deviationSurveyPlan.approvalUser.id"/>">
										
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" 
												onClick="showapprovalUser()">
											<i class="glyphicon glyphicon-search"></i>
										</span>		
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">产品质量负责人意见</span> 
                   						<input type="text"
											size="50" maxlength="100" id="deviationSurveyPlan_approvalOpinion"
											name="approvalOpinion"  changelog="<s:property value="deviationSurveyPlan.approvalOpinion"/>"
											class="form-control"   
											readOnly
											value="<s:property value="deviationSurveyPlan.approvalOpinion"/>"
						 				/>  
									</div>
								</div>				
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">产品质量负责人审批日期</span> 
                   						<input type="text"
											size="50" maxlength="100" id="deviationSurveyPlan_approvalDate"
											name="approvalDate"  changelog="<s:property value="deviationSurveyPlan.approvalDate"/>"
											class="form-control readonlytrue"   
											readonly = "readOnly"
                   	  						value="<s:date name="deviationSurveyPlan.approvalDate" format="yyyy-MM-dd"/>"
                   	   					/>  
									</div>
								</div>				
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">QA</span> 
										<input type="text" size="20" readonly="readOnly" name="auditQaUser-name"
												id="deviationSurveyPlan_auditQaUser_name"  changelog="<s:property value="deviationSurveyPlan.auditQaUser.name"/>"
												value="<s:property value="deviationSurveyPlan.auditQaUser.name"/>"
												class="form-control" /> 
										<input type="hidden"
												id="deviationSurveyPlan_auditQaUser_id" name="auditQaUser-id"
												value="<s:property value="deviationSurveyPlan.auditQaUser.id"/>">
										
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" 
												onClick="showauditUser1()">
											<i class="glyphicon glyphicon-search"></i>
										</span>		
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">QA审批意见</span> 
                   						<input type="text"
											size="50" maxlength="100" id="deviationSurveyPlan_auditQaOpinion"
											name="auditQaOpinion"  changelog="<s:property value="deviationSurveyPlan.auditQaOpinion"/>"
											class="form-control"   
											readOnly
											value="<s:property value="deviationSurveyPlan.auditQaOpinion"/>"
						 				/>  
									</div>
								</div>				
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">QA审批日期</span> 
                   						<input type="text"
											size="50" maxlength="100" id="deviationSurveyPlan_auditQaDate"
											name="auditQaDate"  changelog="<s:property value="deviationSurveyPlan.auditQaDate"/>"
											class="form-control readonlytrue"   readonly = "readOnly"
											
                   	  						value="<s:date name="deviationSurveyPlan.auditQaDate" format="yyyy-MM-dd"/>"
                   	   					/>  
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">批准人</span> 
										<input type="text" size="20" readonly="readOnly" name="approverUser-name"
												id="deviationSurveyPlan_approverUser_name"  changelog="<s:property value="deviationSurveyPlan.approverUser.name"/>"
												value="<s:property value="deviationSurveyPlan.approverUser.name"/>"
												class="form-control" /> 
										<input type="hidden"
												id="deviationSurveyPlan_approverUser_id" name="approverUser-id"
												value="<s:property value="deviationSurveyPlan.approverUser.id"/>">
										
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" 
												onClick="showApproverUser()">
											<i class="glyphicon glyphicon-search"></i>
										</span>		
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">批准人意见</span> 
                   						<input type="text"
											size="50" maxlength="100" id="deviationSurveyPlan_approverUserOpinion"
											name="approverUserOpinion"  changelog="<s:property value="deviationSurveyPlan.approverUserOpinion"/>"
											class="form-control"   
											value="<s:property value="deviationSurveyPlan.approverUserOpinion"/>"
						 				/>  
									</div>
								</div>				
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">批准人批准日期</span> 
                   						<input type="text"
											size="50" maxlength="100" id="deviationSurveyPlan_approverUserDate"
											name="approverUserDate"  changelog="<s:property value="deviationSurveyPlan.approverUserDate"/>"
											class="form-control datePicker"   
                   	  						value="<s:date name="deviationSurveyPlan.approverUserDate" format="yyyy-MM-dd"/>"
                   	   					/>  
									</div>
								</div>
							</div>
							<!-- <div class="row">
								<div class="panel" style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
									<div class="panel-heading text-left">
										<h3 class="panel-title" style="font-family: 黑体;">
											<i class="glyphicon glyphicon-bookmark"></i> 自定义字段
												
										</h3>
									</div>
								</div>
							</div> -->
							<!-- <div id="fieldItemDiv"></div> -->
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<button type="button" class="btn btn-info" onclick="fileUp()">上传附件</button>
											<span class="text"><fmt:message
													key="biolims.common.aTotalOf" />&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
													key="biolims.common.attachment" /></span>

											<button type="button" class="btn btn-info"
												onclick="fileView()">查看附件</button>
								</div>			
							</div>
							<br> 
							<input type="hidden" name="planInfluenceProductJson"
								id="planInfluenceProductJson" value="" /> 
							<input type="hidden" name="planInfluenceMaterielJson"
								id="planInfluenceMaterielJson" value="" /> 
							<input type="hidden" name="planInfluenceEquipmentJson"
								id="planInfluenceEquipmentJson" value="" /> 
								
							<input type="hidden"
								id="id_parent_hidden"
								value="<s:property value="deviationSurveyPlan.id"/>" />
							<input type="hidden" id="fieldContent" name="fieldContent" value="<s:property value="deviationSurveyPlan.fieldContent"/>" />	
								
						</form>
					</div>
					<!--table表格-->
					  	<div class="HideShowPanel" style="display: none;">
						 <table
							class="table table-hover table-striped table-bordered table-condensed"
							id="correctivePreventiveTable" style="font-size: 14px;">
						</table>
					</div>
					  	<div class="HideShowPanel" style="display: none;">
						 <table
							class="table table-hover table-striped table-bordered table-condensed"
							id="planInfluenceProductTable" style="font-size: 14px;">
						</table>
					</div>
					  	<div class="HideShowPanel" style="display: none;">
						 <table
							class="table table-hover table-striped table-bordered table-condensed"
							id="planInfluenceMaterielTable" style="font-size: 14px;">
						</table>
					</div>
					  	<div class="HideShowPanel" style="display: none;">
						 <table
							class="table table-hover table-striped table-bordered table-condensed"
							id="planInfluenceEquipmentTable" style="font-size: 14px;">
						</table>
					</div>
					</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="button" class="btn btn-primary" id="pre">上一步</button>
						<button type="button" class="btn btn-primary" id="next">下一步</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript" src="${ctx}/js/deviation/plan/deviationSurveyPlanEdit.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/deviation/plan/correctivePreventive.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/deviation/plan/planInfluenceProduct.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/deviation/plan/planInfluenceMateriel.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/deviation/plan/planInfluenceEquipment.js"></script>
</body>

</html>	
