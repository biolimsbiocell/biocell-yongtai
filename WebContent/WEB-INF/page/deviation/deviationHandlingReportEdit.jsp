<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<title></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<link rel="stylesheet" href="${ctx}/lims/plugins/layui/css/layui.css">
<script type="text/javascript" src="${ctx}/lims/plugins/layui/layui.js"></script>
<style>
.dataTables_scrollBody {
	min-height: 100px;
}
#btn_changeState{
	display:none;
}

</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div style="height: 14px"></div>
	<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
	<input type="hidden" id="kind" value="${requestScope.kind}">
	<input type="hidden" id="action" value="${requestScope.action}">
	<div class="container-fluid" style="margin-top: 46px">
		<!--Form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<i class="glyphicon glyphicon-pencil"></i>偏差处理报告
					</h3>

					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
											<li>
												<a href="####" onclick="$('.buttons-print').click();"><fmt:message key="biolims.common.print"/></a>
											</li>
											<li>
												<a href="#####" onclick="$('.buttons-copy').click();"><fmt:message key="biolims.common.copyData"/></a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-excel').click();">Excel</a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-csv').click();">CSV</a>
											</li>
											<li role="separator" class="divider"></li>
											<li>
												<a href="####" onclick="itemFixedCol(2)"><fmt:message key="biolims.common.lock2Col"/></a>
											</li>
											<li>
												<a href="####" id="unfixde" onclick="unfixde()"><fmt:message key="biolims.common.cancellock2Col"/></a>
											</li>
										</ul>
						</div>
					</div>
				</div>
				<div class="box-body">
					<!--进度条-->
					<div id="wizard" class="form_wizard wizard_horizontal">
						<ul class="wizard_steps">
							<li><a class="selected step"> <span class="step_no">1</span>
									<span class="step_descr"> Step 1<br /> <small>偏差处理报告</small>
								</span>
							</a></li>
										<li><a class="done step"> <span class="step_no">2</span>
											<span class="step_descr"> Step 2<br /> <small>影响的产品</small></span>
								</a></li>
										<li><a class="done step"> <span class="step_no">3</span>
											<span class="step_descr"> Step 3<br /> <small>影响的物料</small></span>
								</a></li>
										<li><a class="done step"> <span class="step_no">4</span>
											<span class="step_descr"> Step 4<br /> <small>影响的设备</small></span>
								</a></li>
						</ul>
					</div>
					<!--form表单-->
					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
							<input type="hidden" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId")%>" /> <br>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">编号<img class='requiredimage' src='${ctx}/images/required.gif' />  </span> 
                   						<input type="text"
											size="50" maxlength="100" id="deviationHandlingReport_id"
											name="id"  changelog="<s:property value="deviationHandlingReport.id"/>"
											readonly = "readOnly" class="form-control readonlytrue" 
											value="<s:property value="deviationHandlingReport.id"/>"
						 				/>  
									</div>
								</div>				
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">偏差描述 </span> 
										<textarea id="deviationHandlingReport_name"
											name="name" placeholder="长度限制2000个字符"
											class="form-control"
											style="overflow: hidden; width: 250px height: 80px;"><s:property
												value="deviationHandlingReport.name" /></textarea>
                   						<!--  <input type="text"
											size="50" maxlength="100" id="deviationHandlingReport_name"
											name="name"  changelog="<s:property value="deviationHandlingReport.name"/>"
											class="form-control"   
											value="<s:property value="deviationHandlingReport.name"/>"
						 				/>  
						 				-->
									</div>
								</div>	
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">发现人</span> 
										<input type="text" size="20" readonly="readOnly" name="discoverer-name"
												id="deviationHandlingReport_discoverer_name"  changelog="<s:property value="deviationHandlingReport.discoverer.name"/>"
												value="<s:property value="deviationHandlingReport.discoverer.name"/>"
												class="form-control" /> 
										<input type="hidden"
												id="deviationHandlingReport_discoverer_id" name="discoverer-id"
												value="<s:property value="deviationHandlingReport.discoverer.id"/>">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" 
												onClick="showdiscoverer()">
											<i class="glyphicon glyphicon-search"></i>
										</span>		
									</div>
								</div>			
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">偏差编号<img class='requiredimage' src='${ctx}/images/required.gif' /> </span> 
                   						<input type="text"
											size="50" maxlength="100" id="deviationHandlingReport_no"
											name="no"  changelog="<s:property value="deviationHandlingReport.no"/>"
											class="form-control"   
											value="<s:property value="deviationHandlingReport.no"/>"
						 				/>  
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">偏差影响</span> 
                   						<input type="text"
											size="50" maxlength="100" id="deviationHandlingReport_influence"
											name="influence"  changelog="<s:property value="deviationHandlingReport.influence"/>"
											class="form-control"   
											value="<s:property value="deviationHandlingReport.influence"/>"
										 />  
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">偏差分类</span> 
						 				<select lay-ignore id="deviationHandlingReport_type" name="type" class="form-control">
						 					<option value="">---请选择---</option>
						 					<option value="1" <s:if test="deviationHandlingReport.type==1">selected="selected"</s:if>>微小偏差</option>
						 					<option value="2" <s:if test="deviationHandlingReport.type==2">selected="selected"</s:if>>一般偏差</option>
						 					<option value="3" <s:if test="deviationHandlingReport.type==3">selected="selected"</s:if>>重大偏差</option>
						 				</select> 
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">创建人</span> 
										<input type="text" size="20" readonly="readOnly" name="createUser-name"
												id="deviationHandlingReport_createUser_name"  changelog="<s:property value="deviationHandlingReport.createUser.name"/>"
												value="<s:property value="deviationHandlingReport.createUser.name"/>"
												class="form-control" /> 
										<input type="hidden"
												id="deviationHandlingReport_createUser_id" name="createUser-id"
												value="<s:property value="deviationHandlingReport.createUser.id"/>">
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">创建时间</span> 
                   						<input type="text"
											size="50" maxlength="100" id="deviationHandlingReport_createDate"
											name="createDate"  changelog="<s:property value="deviationHandlingReport.createDate"/>"
											readonly = "readOnly" class="form-control readonlytrue" 
                   	 						value="<s:date name="deviationHandlingReport.createDate" format="yyyy-MM-dd"/>"
                   	   					 />  
									</div>
								</div>				
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">状态名称</span> 
										<input type="hidden"
											size="50" maxlength="100" id="deviationHandlingReport_state"
											name="state"  changelog="<s:property value="deviationHandlingReport.state"/>"
											readonly = "readOnly" class="form-control readonlytrue" 
											value="<s:property value="deviationHandlingReport.state"/>"
						 				/>  
                   						<input type="text"
											size="50" maxlength="100" id="deviationHandlingReport_stateName"
											name="stateName"  changelog="<s:property value="deviationHandlingReport.stateName"/>"
											readonly = "readOnly" class="form-control readonlytrue" 
											value="<s:property value="deviationHandlingReport.stateName"/>"
										 />  
									</div>
								</div>	
											
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">发生部门</span> 
                   						<input type="text"
											size="50" maxlength="100" id="deviationHandlingReport_happenDepartment"
											name="happenDepartment"  changelog="<s:property value="deviationHandlingReport.happenDepartment"/>"
											class="form-control"   
											value="<s:property value="deviationHandlingReport.happenDepartment"/>"
						 				/>  
									</div>
								</div>				
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">发生时间</span> 
                   						<input type="text"
											size="50" maxlength="100" id="deviationHandlingReport_happenDate"
											name="happenDate"  changelog="<s:property value="deviationHandlingReport.happenDate"/>"
											class="form-control datePicker" 
                   	  						value="<s:date name="deviationHandlingReport.happenDate" format="yyyy-MM-dd"/>"
                   	   					 />  
									</div>
								</div>				
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">发生地点</span> 
                   						<input type="text"
											size="50" maxlength="100" id="deviationHandlingReport_happenAddress"
											name="happenAddress"  changelog="<s:property value="deviationHandlingReport.happenAddress"/>"
											class="form-control"   
											value="<s:property value="deviationHandlingReport.happenAddress"/>"
						 				/>  
									</div>
								</div>				
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-8 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">紧急纠正措施</span> 
                   						<input type="text"
											size="50" maxlength="100" id="deviationHandlingReport_correctiveActionName"
											name="correctiveActionName"  changelog="<s:property value="deviationHandlingReport.correctiveActionName"/>"
											class="form-control"   
											value="<s:property value="deviationHandlingReport.correctiveActionName"/>"
						 				/>  
									</div>
									<%-- <div class="input-group">
									<span class="input-group-addon">紧急纠正措施</span>  
										<div id="action_div">
											<c:forEach items="${aList}" var="a">
												<input type="checkbox" name="correctiveActionId" value="${a.id}" title="${a.name}"> 
											</c:forEach>
										</div>
									</div> --%>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">偏差种类</span> 
										<div id="kind_div">
											<c:forEach items="${kList}" var="k">
												<input type="checkbox" name="kindId" value="${k.id}" title="${k.name}"> 
											</c:forEach>
										</div>
									</div>
								</div>				
							</div>
							<div class="row">
								<div class="col-md-6 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">报告人</span> 
										<input type="text" size="20" readonly="readOnly" name="reportUser-name"
												id="deviationHandlingReport_reportUser_name"  changelog="<s:property value="deviationHandlingReport.reportUser.name"/>"
												value="<s:property value="deviationHandlingReport.reportUser.name"/>"
												class="form-control" /> 
										<input type="hidden"
												id="deviationHandlingReport_reportUser_id" name="reportUser-id"
												value="<s:property value="deviationHandlingReport.reportUser.id"/>">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" 
												onClick="showreportUser()">
											<i class="glyphicon glyphicon-search"></i>
										</span>		
									</div>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">报告日期</span> 
                   						<input type="text"
											size="50" maxlength="100" id="deviationHandlingReport_reportDate"
											name="reportDate"  changelog="<s:property value="deviationHandlingReport.reportDate"/>"
											class="form-control readonlytrue"   
											readonly = "readOnly"
                   	  						value="<s:date name="deviationHandlingReport.reportDate" format="yyyy-MM-dd"/>"
                   	   					/>  
									</div>
								</div>	
							</div>
							<div class="row">			
								<div class="col-md-6 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">部门负责人</span> 
										<input type="text" size="20" readonly="readOnly" name="departmentUser-name"
												id="deviationHandlingReport_departmentUser_name"  changelog="<s:property value="deviationHandlingReport.departmentUser.name"/>"
												value="<s:property value="deviationHandlingReport.departmentUser.name"/>"
												class="form-control" /> 
										<input type="hidden"
												id="deviationHandlingReport_departmentUser_id" name="departmentUser-id"
												value="<s:property value="deviationHandlingReport.departmentUser.id"/>">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" 
												onClick="showdepartmentUser()">
											<i class="glyphicon glyphicon-search"></i>
										</span>		
									</div>
								</div>				
								<div class="col-md-6 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">日期</span> 
                   						<input type="text"
											size="50" maxlength="100" id="deviationHandlingReport_departmentDate"
											name="departmentDate"  changelog="<s:property value="deviationHandlingReport.departmentDate.name"/>"
											class="form-control readonlytrue"  
											readonly = "readOnly" 
                   	  						value="<s:date name="deviationHandlingReport.departmentDate" format="yyyy-MM-dd"/>"
                   	   					/>  
									</div>
								</div>				
							</div>
							<div class="row">
								<div class="col-md-6 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">QA审核人</span> 
										<input type="text" size="20" readonly="readOnly" name="monitor-name"
												id="deviationHandlingReport_monitor_name"  changelog="<s:property value="deviationHandlingReport.monitor.name"/>"
												value="<s:property value="deviationHandlingReport.monitor.name"/>"
												class="form-control" /> 
										<input type="hidden"
												id="deviationHandlingReport_monitor_id" name="monitor-id"
												value="<s:property value="deviationHandlingReport.monitor.id"/>">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" 
												onClick="showmonitor()">
											<i class="glyphicon glyphicon-search"></i>
										</span>		
									</div>
								</div>				
								<div class="col-md-6 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">日期</span> 
                   						<input type="text"
											size="50" maxlength="100" id="deviationHandlingReport_monitoringDate"
											name="monitoringDate"  changelog="<s:property value="deviationHandlingReport.monitoringDate.name"/>"
											class="form-control readonlytrue"   
											readonly = "readOnly"
                   	  						value="<s:date name="deviationHandlingReport.monitoringDate" format="yyyy-MM-dd"/>"
                   	   					/>  
									</div>
								</div>				
							</div>
							<div class="row">
								<div class="col-md-6 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">调查组组长</span> 
										<input type="text" size="20" readonly="readOnly" name="groupLeader-name"
												id="deviationHandlingReport_groupLeader_name"  changelog="<s:property value="deviationHandlingReport.groupLeader.name"/>"
												value="<s:property value="deviationHandlingReport.groupLeader.name"/>"
												class="form-control" /> 
										<input type="hidden"
												id="deviationHandlingReport_groupLeader_id" name="groupLeader-id"
												value="<s:property value="deviationHandlingReport.groupLeader.id"/>">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" 
												onClick="showgroupLeader()">
											<i class="glyphicon glyphicon-search"></i>
										</span>		
									</div>
								</div>				
								<div class="col-md-6 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">调查组员</span>
										<input 
											size="50" maxlength="100" type="hidden" id="deviationHandlingReport_groupMemberIds"
											name="groupMemberIds"  changelog="<s:property value="deviationHandlingReport.groupMemberIds"/>"
											class="form-control"   
											value="<s:property value="deviationHandlingReport.groupMemberIds"/>"/>  
                   						<input type="text"
											size="50" maxlength="100" id="deviationHandlingReport_groupMembers"
											name="groupMembers"  changelog="<s:property value="deviationHandlingReport.groupMembers"/>"
											class="form-control"   readonly="readonly"
											value="<s:property value="deviationHandlingReport.groupMembers"/>"
						 				/>  
						 				<span class="input-group-btn">
											<button class="btn btn-info" type="button" 
												onClick="showGroupMembers()">
											<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>				
									</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<button type="button" class="btn btn-info" onclick="fileUp()">上传附件</button>
											<span class="text label"><fmt:message
													key="biolims.common.aTotalOf" />&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
													key="biolims.common.attachment" /></span>

											<button type="button" class="btn btn-info"
												onclick="fileView()">查看附件</button>
								</div>			
							</div>
							<!-- <div id="fieldItemDiv"></div> -->
								
							<br> 
							<input type="hidden" name="influenceProductJson"
								id="influenceProductJson" value="" /> 
							<input type="hidden" name="influenceMaterielJson"
								id="influenceMaterielJson" value="" /> 
							<input type="hidden" name="influenceEquipmentJson"
								id="influenceEquipmentJson" value="" /> 
								
							<input type="hidden"
								id="id_parent_hidden"
								value="<s:property value="deviationHandlingReport.id"/>" />
							<input type="hidden" id="fieldContent" name="fieldContent" value="<s:property value="deviationHandlingReport.fieldContent"/>" />	
								
						</form>
					</div>
					<!--table表格-->
					  	<div class="HideShowPanel" style="display: none;">
						 <table
							class="table table-hover table-striped table-bordered table-condensed"
							id="influenceProductTable" style="font-size: 14px;">
						</table>
					</div>
					  	<div class="HideShowPanel" style="display: none;">
						 <table
							class="table table-hover table-striped table-bordered table-condensed"
							id="influenceMaterielTable" style="font-size: 14px;">
						</table>
					</div>
					  	<div class="HideShowPanel" style="display: none;">
						 <table
							class="table table-hover table-striped table-bordered table-condensed"
							id="influenceEquipmentTable" style="font-size: 14px;">
						</table>
					</div>
					</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="button" class="btn btn-primary" id="pre">上一步</button>
						<button type="button" class="btn btn-primary" id="next">下一步</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" 
		src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript" 
		src="${ctx}/js/deviation/deviationHandlingReportEdit.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/deviation/influenceProduct.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/deviation/influenceMateriel.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/deviation/influenceEquipment.js"></script>
</body>

</html>	
