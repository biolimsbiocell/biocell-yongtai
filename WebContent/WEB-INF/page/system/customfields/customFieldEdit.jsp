<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML>
<html>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
<title><fmt:message key="biolims.common.noTitleDocuments" /></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style>
.dataTables_scrollBody {
	min-height: 150px;
}

.layui-layer-content .input-group {
	margin-top: 12px;
}
</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarNoState.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>

	<div style="height: 14px"></div>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">

	<div class="container-fluid" style="margin-top: 46px">
		<!--订单录入的form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<fmt:message key="biolims.common.customFields" />
						<small style="color: #fff;"> <fmt:message
								key="biolims.common.serialNumber" />: <span><s:property
									value="fieldTemplate.id" /></span> <fmt:message
								key="biolims.common.commandPerson" />: <span
							userId="<s:property value="fieldTemplate.createUser.id"/>"
							id="fieldTemplate_createUser"><s:property
									value="bloodSplit.createUser.name" /></span> <fmt:message
								key="biolims.sample.createDate" />: <span
							id="fieldTemplate_createDate"><s:property
									value="fieldTemplate.createDate" /></span>
						</small>
					</h3>
					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="####" onclick="printDataTablesItem()"><fmt:message
											key="biolims.common.print" /></a></li>
								<li><a href="#####" onclick="copyDataTablesItem ()"><fmt:message
											key="biolims.common.copyData" /></a></li>
								<li><a href="####" onclick="excelDataTablesItem()">Excel</a>
								</li>
								<li><a href="####" onclick="csvDataTablesItem ()">CSV</a></li>
								<li role="separator" class="divider"></li>
								<li><a href="####" id="fixdeLeft2" onclick="fixedCol(2)"><fmt:message
											key="biolims.common.lock2Col" /></a></li>
								<li><a href="####" id="unfixde" onclick="unfixde()"><fmt:message
											key="biolims.common.relievingFixation" /></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="box-body ipadmini">


					<!--form表单-->
					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
						 <input
								type="hidden" value="<%=request.getParameter(" bpmTaskId ")%>" />
							<br>
							<div class="row">
								<input type="hidden" size="20" readonly="readonly"
									maxlength="50" id="fieldTemplate_id"
									changelog="<s:property value="fieldTemplate.id"/>"
									name="fieldTemplate.id" class="form-control" title="编码"
									value="<s:property value=" fieldTemplate.id "/>" />

								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message
												key="biolims.common.name" />
										</span> <input type="text" size="20" maxlength="50"
											id="fieldTemplate_note"
											changelog="<s:property value="fieldTemplate.note"/>"
											name="fieldTemplate.note" class="form-control" title="描述"
											value="<s:property value=" fieldTemplate.note "/>" />
									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message
												key="biolims.common.subordinateModule" /> <img
											class='requiredimage' src='${ctx}/images/required.gif' />
										</span> <input type="hidden" size="20" maxlength="50"
											id="fieldTemplate_subordinateModuleId"
											changelog="<s:property value="fieldTemplate.subordinateModuleId"/>"
											name="fieldTemplate.subordinateModuleId" class="form-control"
											title="所属模块ID"
											value="<s:property value=" fieldTemplate.subordinateModuleId "/>" />
										<input type="text" readonly="readonly" size="20"
											maxlength="50"
											changelog="<s:property value="fieldTemplate.subordinateModuleName"/>"
											id="fieldTemplate_subordinateModuleName"
											name="fieldTemplate.subordinateModuleName"
											class="form-control" title="所属模块"
											value="<s:property value=" fieldTemplate.subordinateModuleName "/>" />
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												id='showsubordinateModule'
												onClick="vouchersubordinateModuleFun()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>
									<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.state" /></span> <select class="form-control" name="fieldTemplate.state">

										<option value="3"
											<s:if test="fieldTemplate.state==3">selected="selected"</s:if>><fmt:message key="biolims.common.invalid" /></option>
										<option value="1"
											<s:if test="fieldTemplate.state==1">selected="selected"</s:if>><fmt:message key="biolims.common.effective" /></option>

									</select>
								</div>
							</div>
								<%-- <div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message
												key="biolims.common.productName" />

										</span> <input type="hidden" size="20" maxlength="50"
											id="fieldTemplate_productId"
											changelog="<s:property value="fieldTemplate.productId"/>"
											name="fieldTemplate.productId" class="form-control"
											value="<s:property value=" fieldTemplate.productId "/>" /> <input
											type="text" readonly="readonly" size="20" maxlength="50"
											changelog="<s:property value="fieldTemplate.productName"/>"
											id="fieldTemplate_productName"
											name="fieldTemplate.productName" class="form-control"
											title="检测项目"
											value="<s:property value=" fieldTemplate.productName "/>" />
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id='showage'
												onClick="voucherProductFun()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div> --%>
							</div>
							<div class="row">
								<input type="hidden" size="30" maxlength="25"
									changelog="<s:property value="fieldTemplate.createDate"/>"
									id="fieldTemplate_createDate" readonly="readonly"
									name="fieldTemplate.createDate" class="form-control"
									title="创建时间"
									value="<s:date name=" fieldTemplate.createDate " format="yyyy-MM-dd " />" />
								<input type="hidden" size="30" readonly="readOnly" id="text"
									changelog="<s:property value="fieldTemplate.createUser.name"/>"
									class="form-control"
									value="<s:property value=" fieldTemplate.createUser.name "/>"
									class="text input readonlytrue" readonly="readOnly" /> <input
									type="hidden" id="fieldTemplate_createUser"
									changelog="<s:property value="fieldTemplate.createUser.id"/>"
									name="fieldTemplate.createUser.id"
									value="<s:property value=" fieldTemplate.createUser.id "/>">

							</div>
							<div id="layerItem" style="display: none;">
								<div class="col-xs-12 col-sm-6">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.fieldCodedName" /> </span> <input type="text"
											id="fieldCodedName" class="form-control" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.fieldType" /> </span> <input type="text"
											id="fieldType" class="form-control datatype" disabled />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.fieldDisplayName" /> </span> <input
											type="text" id="fieldDisplayName" class="form-control" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.sortingNumber" /> </span> <input type="text"
											id="orderNum" class="form-control" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.isReadOnly" /></span> <select
											class="form-control" lay-ignore id="isReadOnly">
											<option><fmt:message key="biolims.common.yes" /></option>
											<option><fmt:message key="biolims.common.no" /></option>
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.fieldLength" /> </span> <input type="text"
											id="fieldLength" class="form-control" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.defaultValue" /> </span> <input type="text"
											id="defaultValue" class="form-control" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.fieldHints" /> </span> <input type="text"
											id="fieldHints" class="form-control" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.required" /> </span> <select
											class="form-control" lay-ignore id="required">
											<option><fmt:message key="biolims.common.yes" /></option>
											<option><fmt:message key="biolims.common.no" /></option>
										</select>
									</div>
								</div>
								<div id="choseCheekBox" class="col-xs-12"
									style="margin-top: 10px; display: none;">
									<p>
										<i class="glyphicon glyphicon-asterisk"></i>
										<fmt:message key="biolims.common.optionInformation" />
									</p>
									<div class="choseCheekBox">
										<div class="col-xs-12 col-sm-6">
											<div class="input-group">
												<span class="input-group-addon"><fmt:message
														key="biolims.common.optionValue" /> </span> <input type="text"
													class="form-control input-sm" placeholder="0" />
											</div>
										</div>
										<div class="col-xs-12 col-sm-6">
											<div class="input-group">
												<span class="input-group-addon"><fmt:message
														key="biolims.common.optionName" /> </span> <input type="text"
													class="form-control input-sm" placeholder="" />
											</div>
										</div>
									</div>
									<div class="choseCheekBox">
										<div class="col-xs-12 col-sm-6">
											<div class="input-group">
												<span class="input-group-addon"><fmt:message
														key="biolims.common.optionValue" /> </span> <input type="text"
													class="form-control input-sm" placeholder="1" />
											</div>
										</div>
										<div class="col-xs-12 col-sm-6">
											<div class="input-group">
												<span class="input-group-addon"><fmt:message
														key="biolims.common.optionName" /> </span> <input type="text"
													class="form-control input-sm" placeholder="" />
											</div>
										</div>
									</div>
									<div class="text-center">
										<button class="btn btn-xs btn-info addItemTembtn"
											style="margin: 10px 0; display: none;">
											<i class="glyphicon glyphicon-plus"></i>
											<fmt:message key="biolims.common.AddTo" />
										</button>
									</div>

								</div>
							</div>

							<input type="hidden" name="fieldTemplateInfoJson"
								id="fieldTemplateInfoJson" value="" /> <input type="hidden"
								id="id_parent_hidden"
								value="<s:property value=" fieldTemplate.id "/>" />
							<!-- 日志  dwb 2018-05-12 14:39:33 -->
							<input type="hidden" id="changeLog" name="changeLog" /> <input
								type="hidden" id="changeLogItem" name="changeLogItem" />
						</form>

					</div>
					<!--table表格-->
					<div class="HideShowPanel">
						<table
							class="table table-hover table-striped table-bordered table-condensed"
							id="customFieldsTable" style="font-size: 14px;">
						</table>
					</div>

				</div>

			</div>
		</div>
	</div>

	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/system/customfields/customFieldEdit.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/system/customfields/customFieldItem.js"></script>

</body>

</html>