<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML>
<html>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
<title><fmt:message key="biolims.common.noTitleDocuments" /></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<link rel="stylesheet" href="${ctx}/lims/plugins/layui/css/layui.css">
<script type="text/javascript" src="${ctx}/lims/plugins/layui/layui.js"></script>
<style>
.dataTables_scrollBody {
	min-height: 150px;
}

.layui-layer-content .input-group {
	margin-top: 12px;
}

#fieldItemDiv .icheckbox_square-blue {
	margin-left: 10px;
}
#yesSpan{
	display:none;
}
#auditDiv{
	display:none;
}
#btn_submit{
	display:none;
}
#btn_changeState{
	display:none;
}
</style>
</head>
<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<!--AI录入-->
	<form method="post" class="hide" id="fileForm" name="fileForm"
		action='${ctx}/common/aiUtils/isIdAreThere.action'
		enctype="multipart/form-data">
		<input type="file" name="AiPicture" id="AiPicture" accept="image/*"
			onchange="subimtBtn()">
	</form>
	<div style="height: 14px"></div>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">
	<input type="hidden" id="flag"
		value="${requestScope.flag}">
	<input type="hidden" id="changeId"
		value="${requestScope.changeId}">
	<input type="hidden" id="order_type"
		value="${requestScope.type}">

	<div class="container-fluid" style="margin-top: 46px">
		<!--订单录入的form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<fmt:message key="biolims.order.title" />
					</h3>
					<small style="color: #fff;"> <fmt:message
							key="biolims.common.commandPerson" />: <span
						id="sampleOrder_createUser"><s:property
								value=" sampleOrder.createUser.name" /></span> <fmt:message
							key="biolims.common.createDate" />: <span
						id="sampleOrder_createDate"><s:date
								name="sampleOrder.createDate" format="yyyy-MM-dd" /></span> <fmt:message
							key="biolims.common.state" />: <span id="sampleOrder_state"><s:property
								value="sampleOrder.stateName" /></span> <fmt:message
							key="biolims.common.auditor" />：<span id="confirmUserName"><s:property
								value="sampleOrder.confirmUser.name " /></span> <fmt:message
							key="biolims.common.completionTime" />：<span
						id="sampleOrder_confirmDate"><s:date
								name=" sampleOrder.confirmDate " format="yyyy-MM-dd " /></span>
					</small>




					<button type="button" class="btn btn-info btn-xs" id="aiBtn">
						<i class="layui-icon">&#xe67c;</i>
						<fmt:message key="biolims.order.AI" />
					</button>
					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="####" onclick="$('.buttons-print').click();"><fmt:message
											key="biolims.common.print" /></a></li>
								<li><a href="#####" onclick="$('.buttons-copy').click();"><fmt:message
											key="biolims.common.copyData" /></a></li>
								<li><a href="####" onclick="$('.buttons-excel').click();"><fmt:message
											key="biolims.common.export" /></a></li>
								<li><a href="####" onclick="$('.buttons-csv').click();"><fmt:message
											key="biolims.common.exportCSV" /></a></li>
								<li role="separator" class="divider"></li>
								<li><a href="####" id="fixdeLeft2" onclick="fixedCol(2)"><fmt:message
											key="biolims.common.lock2Col" /></a></li>
								<li><a href="####" id="unfixde" onclick="unfixde()"><fmt:message
											key="biolims.common.cancellock2Col" /></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="box-body ipadmini">
					<!--进度条-->
					<div id="wizard" class="form_wizard wizard_horizontal">
						<ul class="wizard_steps">
							<li><a class="selected step"> <span class="step_no">1</span>
									<span class="step_descr"> Step 1<br /> <small>订单信息</small>
								</span>
							</a></li>
							<li><a class="done step"> <span class="step_no">2</span>
									<span class="step_descr"> Step 2<br /> <small>样本信息</small>
								</span>
							</a></li>
							<li><a class="done step"> <span class="step_no">3</span>
									<span class="step_descr"> Step 2<br /> <small>生产追踪</small>
								</span>
							</a></li>

						</ul>
					</div>

					<!--form表单-->
					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
							<input type="hidden" id="bpmTaskId"
								value="${requestScope.bpmTaskId}" /> <br>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message
												key="biolims.common.orderNumber" />
										</span> <input list="sampleIdOne" changelog="<s:property value="sampleOrder.id"/>" type="text" size="20"
											maxlength="25" id="sampleOrder_id" name="sampleOrder.id"
											class="form-control"
											title="<fmt:message key=" biolims.common.serialNumber "/>"
											onblur="selSampleOrder();"
											value="<s:property value=" sampleOrder.id "/>" /> <span
											class="input-group-btn">
											<button class="btn btn-info" type="button" id="upLoadImg00"
												onclick="upLoadImg1()">
												<fmt:message key="biolims.common.picture" />
											</button>
										</span> <input type="hidden" id="upload_imga_id" 
										changelog="<s:property value="sampleOrder.upLoadAccessory.id"/>"
											name="sampleOrder.upLoadAccessory.id" class="form-control"
											value="<s:property value=" sampleOrder.upLoadAccessory.id "/>">
										<input type="hidden" id="upload_imga_name11" 
										changelog="<s:property value="sampleOrder.upLoadAccessory.fileName"/>"
											name="sampleOrder.upLoadAccessory.fileName"
											class="form-control"
											value="<s:property value=" sampleOrder.upLoadAccessory.fileName "/>">
										<input type="hidden" id="saveType"  value="cq">
									</div>
								</div>

								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message
												key="biolims.common.sname" /> <img class='requiredimage'
											src='${ctx}/images/required.gif' />
										</span> 
										<input list="sampleName" changelog="<s:property value="sampleOrder.name"/>" type="text" size="20"
											maxlength="50" id="sampleOrder_name" name="sampleOrder.name"
											class="form-control"
											title="<fmt:message key=" biolims.common.sname "/>"
											value="<s:property value=" sampleOrder.name "/>" />

									</div>
								</div>

								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon" style="font-family: 黑体;">
											<fmt:message key="biolims.common.dateOfBirth" />
										</span> <input class="form-control " changelog="<s:property value="sampleOrder.birthDate"/>" type="text" size="20"
											maxlength="25" id="sampleOrder_birthDate" onchange="getAge()"
											name="sampleOrder.birthDate" title="出生日期" class="Wdate"
											value="<s:date name=" sampleOrder.birthDate " format="yyyy-MM-dd "/>" />
									</div>
								</div>

							</div>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.ethnicity" /></span> <input list="sampleName"
											changelog="<s:property value="sampleOrder.nation"/>" type="text" size="20" maxlength="50"
											id="sampleOrder_nation" name="sampleOrder.nation"
											class="form-control"
											title="<fmt:message key=" biolims.common.ethnicity "/>"
											value="<s:property value=" sampleOrder.nation "/>" />
									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.nativePlace" /></span> <input type="text"
											changelog="<s:property value="sampleOrder.nativePlace"/>" size="20" maxlength="50" class="form-control"
											id="nativePlace" name="sampleOrder.nativePlace"
											title="<fmt:message key=" biolims.common.nativePlace "/>"
											value="<s:property value=" sampleOrder.nativePlace "/>" />

									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.common.barcode" /></span> <input type="text"
											changelog="<s:property value="sampleOrder.barcode"/>" size="20" maxlength="50" class="form-control"
											id="sampleOrder_barcode" name="sampleOrder.barcode"
											title="<fmt:message key=" biolims.common.barcode "/>"
											value="<s:property value=" sampleOrder.barcode "/>" />

									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon"><fmt:message
													key="biolims.common.electronicMedicalRecordNumber" /></span> <input
												type="text" changelog="<s:property value="sampleOrder.medicalNumber"/>" size="20"
												id="sampleOrder_medicalNumber"
												name="sampleOrder.medicalNumber"
												value="<s:property value=" sampleOrder.medicalNumber "/>"
												class="form-control" /> <span class="input-group-btn">
												<button class="btn btn-info" type="button"
													id='showagemedicalNumber' onClick="crmPatientTypeTwoFun()">
													<i class="glyphicon glyphicon-search"></i>
												</button>
											</span>
										</div>
									</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.visit" /></span> <input type="text" 
												changelog="<s:property value="sampleOrder.visit"/>"
											size="20" maxlength="50" class="form-control"
											id="sampleOrder_visit" name="sampleOrder.visit"
											title="<fmt:message key=" biolims.common.visit "/>"
											value="<s:property value=" sampleOrder.visit "/>" />
									</div>
								</div>
								<div class="col-xs-4">
									<input type="hidden" name="sampleOrder.gender" 
									changelog="<s:property value="sampleOrder.gender"/>"
										id="sampleOrderGender"
										value="<s:property value=" sampleOrder.gender "/>" />
									<div class="input-group" id="ckGender" lay-filter="ckGender">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.gender" /> <img class='requiredimage'
											src='${ctx}/images/required.gif' /> </span>
										<s:if test="sampleOrder.gender==1">
											<input type="checkbox" value="1"  checked
												title=<fmt:message key="biolims.common.male" />>
											<input type="checkbox" value="0" 
												title=<fmt:message key="biolims.common.female" />>
											<input type="checkbox" value="3" 
												title=<fmt:message key="biolims.common.unknown" />>
										</s:if>
										<s:if test="sampleOrder.gender==0">
											<input type="checkbox" value="1" 
												title=<fmt:message key="biolims.common.male" />>
											<input type="checkbox" value="0"  checked
												title=<fmt:message key="biolims.common.female" />>
											<input type="checkbox" value="3" 
												title=<fmt:message key="biolims.common.unknown" />>
										</s:if>
										<s:if test="sampleOrder.gender==3">
											<input type="checkbox" value="1" 
												title=<fmt:message key="biolims.common.male" />>
											<input type="checkbox" value="0" 
												title=<fmt:message key="biolims.common.female" />>
											<input type="checkbox" value="3"  checked
												title=<fmt:message key="biolims.common.unknown" />>
										</s:if>
										<s:if test='sampleOrder.gender==null'>
											<input type="checkbox" value="1" 
												title=<fmt:message key="biolims.common.male" />>
											<input type="checkbox" value="0" 
												title=<fmt:message key="biolims.common.female" />>
											<input type="checkbox" value="3" 
												title=<fmt:message key="biolims.common.unknown" />>
										</s:if>

									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.crmPatient.age" /><img class='requiredimage'
											src='${ctx}/images/required.gif' /> </span> <input type="text"
											size="20" changelog="<s:property value="sampleOrder.age"/>" maxlength="50" class="form-control"
											id="sampleOrder_age" name="sampleOrder.age" title="年龄"
											value="<s:property value=" sampleOrder.age "/>" />
									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.iDcard" /></span> <input type="text" size="20"
											changelog="<s:property value="sampleOrder.idCard"/>" maxlength="18" class="form-control"
											id="sampleOrder_IDcardNo" name="sampleOrder.idCard"
											title="身份证号"
											value="<s:property value=" sampleOrder.idCard "/>" />
									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.bedNo" /></span> <input type="text" 
												changelog="<s:property value="sampleOrder.bedNo"/>"
											size="20" maxlength="50" class="form-control"
											id="sampleOrder_bedNo" name="sampleOrder.bedNo" title="床号"
											value="<s:property value=" sampleOrder.bedNo "/>" />
									</div>
								</div>

							</div>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.hospitalPatientID" /></span> <input
											type="text" size="20" changelog="<s:property value="sampleOrder.hospitalPatientID"/>" maxlength="50"
											class="form-control" id="sampleOrder_hospitalPatientID"
											name="sampleOrder.hospitalPatientID" title="门诊/住院号"
											value="<s:property value=" sampleOrder.hospitalPatientID "/>" />
									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.sample.oldId" /></span> <input
											type="text" size="20" changelog="<s:property value="sampleOrder.oldId"/>" maxlength="50"
											class="form-control" id="sampleOrder_oldId"
											name="sampleOrder.oldId" title="门诊/住院号" readonly="readonly"
											value="<s:property value=" sampleOrder.oldId "/>" />
									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.sample.personShipId" /></span> <input
											type="text" size="20" changelog="<s:property value="sampleOrder.familyId.id"/>" maxlength="50"
											class="form-control" id="sampleOrder_familyId_id"
											name="sampleOrder.familyId.id" title="门诊/住院号" readonly="readonly"
											value="<s:property value=" sampleOrder.familyId.id "/>" />
									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<input
											type="hidden" size="20" changelog="<s:property value="sampleOrder.flagValue"/>" maxlength="50"
											class="form-control" id="sampleOrder_flagValue"
											name="sampleOrder.flagValue" title="门诊/住院号" readonly="readonly"
											value="<s:property value=" sampleOrder.flagValue "/>" />
									</div>
								</div>

							</div>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
										<input type="hidden" class="form-control" id="sampleOrder_orderType"
										 name="sampleOrder.orderType" value="<s:property value=" sampleOrder.orderType "/>"/>
									</div>
								</div>
							</div>
							<div id="ybxx">
								<div class="row">
									<div class="panel"
										style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
										<div class="panel-heading text-left">
											<h3 class="panel-title" style="font-family: 黑体;">
												<i class="glyphicon glyphicon-bookmark"></i>
												<fmt:message key="biolims.dashboard.productData" />
											</h3>
										</div>
									</div>
								</div>
								<br />

								<div class="row">
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon"><fmt:message
													key="biolims.crmDoctorItem.productName" /><img
												class='requiredimage' src='${ctx}/images/required.gif' /> </span>
											<input type="text" size="20" changelog="<s:property value="sampleOrder.productName"/>" readonly="readOnly"
												id="sampleOrder_productName" name="sampleOrder.productName"
												value="<s:property value=" sampleOrder.productName "/>"
												class="form-control" /> <input type="hidden"
												id="sampleOrder_productId" name="sampleOrder.productId"
												value="<s:property value=" sampleOrder.productId "/>">
											<span class="input-group-btn">
												<button class="btn btn-info" type="button" id='showage'
													onClick="voucherProductFun()">
													<i class="glyphicon glyphicon-search"></i>
												</button>
											</span>
										</div>
									</div>
									<div class="col-xs-4">

										<div class="input-group">
											<span class="input-group-addon"><fmt:message
													key="biolims.common.transportType" /></span> <input type="text"
												changelog="<s:property value="sampleOrder.yushufs.name"/>" size="10" id="sampleOrder_yushufs_name"
												readonly="readonly"
												value="<s:property value=" sampleOrder.yushufs.name "/>"
												class="form-control" /> <input type="hidden" 
												changelog="<s:property value="sampleOrder.yushufs.id"/>"
												id="sampleOrder_yushufs_id" name="sampleOrder.yushufs.id"
												value="<s:property value=" sampleOrder.yushufs.id "/>">
											<span class="input-group-btn">
												<button class="btn btn-info" type="button" id='yushufs'
													onclick="yunshufs()">
													<i class="glyphicon glyphicon-search"></i>
												</button>
											</span>
										</div>
									</div>
									<div class="col-xs-4">
										<div class="input-group">

											<span class="input-group-addon"><fmt:message
													key="biolims.common.note" /></span> <input type="text" 
													changelog="<s:property value="sampleOrder.note"/>"
												size="50" maxlength="250" id="text77"
												name="sampleOrder.note" class="form-control"
												title="<fmt:message key=" biolims.common.note "/>"
												value="<s:property value=" sampleOrder.note "/>" />
										</div>
									</div>
								</div>
							</div>
							<div id="diagnosisInfo">
								<!-- 临床诊断 -->
								<div class="row">
									<div class="panel"
										style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
										<div class="panel-heading text-left">
											<h3 class="panel-title" style="font-family: 黑体;">
												<i class="glyphicon glyphicon-bookmark"></i>
												<fmt:message key="biolims.common.clinicalDiagnosis" />
												<!--临床诊断-->
		<input type="file" class="hidden" name="AiPicture" id="AiPicture2" accept="image/*" onchange="subimtBtn2(this)" value='1234567'>
												<button type="button" class="btn btn-info btn-xs"
													id="aiBtn2">
													<i class="layui-icon">&#xe67c;</i><fmt:message key="biolims.order.AI" />
												</button>
											</h3>
										</div>
									</div>
								</div>
								<br />
							</div>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.healthCondition" /></span> <input
											type="hidden" changelog="<s:property value="sampleOrder.medicalHistoryId"/>" id="sampleOrder_medicalHistoryId"
											name="sampleOrder.medicalHistoryId"
											value="<s:property value=" sampleOrder.medicalHistoryId "/>">

										<input type="text" changelog="<s:property value="sampleOrder.medicalHistory"/>" size="30" readonly="readOnly"
											maxlength="50" id="sampleOrder_medicalHistory"
											name="sampleOrder.medicalHistory" title="临床诊断"
											class="form-control"
											value="<s:property value=" sampleOrder.medicalHistory "/>" />
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												id='showmedicalHistory' onClick="medicalHistoryFun()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>

								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.common.medicationInProgress" /></span> <input
											type="hidden" changelog="<s:property value="sampleOrder.yongyaoId"/>" id="sampleOrder_yongyaoId"
											name="sampleOrder.yongyaoId"
											value="<s:property value=" sampleOrder.yongyaoId "/>" /> <input
											type="text" changelog="<s:property value="sampleOrder.yongyao"/>" size="20" readonly="readOnly"
											id="sampleOrder_yongyao" name="sampleOrder.yongyao"
											value="<s:property value=" sampleOrder.yongyao "/>"
											class="form-control" /> <span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="sampleYongYaoFun()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>

								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.common.frequencyOfOccurrence" /></span> <input
											type="text" changelog="<s:property value="sampleOrder.fspl"/>" size="30" maxlength="50"
											class="form-control" id="text70" name="sampleOrder.fspl"
											title="发生频率" value="<s:property value=" sampleOrder.fspl "/>" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">


										<span class="input-group-addon"><fmt:message
												key="biolims.common.allergicHistory" /></span> <input type="text"
											changelog="<s:property value="sampleOrder.guomin"/>" size="30" maxlength="50" class="form-control"
											id="text70" name="sampleOrder.guomin" title="过敏史"
											value="<s:property value=" sampleOrder.guomin "/>" />
									</div>
								</div>

							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.clinicalDiagnosis" /></span>
										<textarea type="text" changelog="<s:property value="sampleOrder.diagnosis"/>" size="30" maxlength="50"
											class="form-control" id="diagnosis"
											name="sampleOrder.diagnosis" title="临床诊断"><s:property
												value=" sampleOrder.diagnosis " /></textarea>
									</div>
								</div>

							</div>
							<div id="chargeInfo">
								<div class="row">
									<div class="panel"
										style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
										<div class="panel-heading text-left">
											<h3 class="panel-title" style="font-family: 黑体;">
												<i class="glyphicon glyphicon-bookmark"></i>
												<fmt:message key="biolims.common.freightInfo" />
											</h3>
										</div>
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon"><fmt:message
													key="biolims.common.salesRepresentative" /></span> <input
												changelog="<s:property value="sampleOrder.commissioner.name"/>" type="text" size="10"
												id="sampleOrder_commissioner_name" readonly="readonly"
												value="<s:property value=" sampleOrder.commissioner.name "/>"
												class="form-control" /> <input type="hidden"
												id="sampleOrder_commissioner"
												name="sampleOrder.commissioner.id"
												value="<s:property value=" sampleOrder.commissioner.id "/>">
											<span class="input-group-btn">
												<button class="btn btn-info" type="button"
													onclick="showcommissioner()">
													<i class="glyphicon glyphicon-search"></i>
												</button>
											</span>
										</div>
									</div>

									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon"><fmt:message
													key="biolims.common.wayToCharge" /></span> <input type="text"
												changelog="<s:property value="sampleOrder.collectionManner.name"/>" size="10" readonly="readOnly"
												id="sampleOrder_collectionManner_name"
												value="<s:property value=" sampleOrder.collectionManner.name "/>"
												class="form-control" /> <input type="hidden" changelog="<s:property value="sampleOrder.collectionManner.id"/>"
												id="sampleOrder_collectionManner"
												name="sampleOrder.collectionManner.id"
												value="<s:property value=" sampleOrder.collectionManner.id "/>">
											<span class="input-group-btn">
												<button class="btn btn-info" type="button"
													onclick="showCollectionManner()">
													<i class="glyphicon glyphicon-search"></i>
												</button>
											</span>
										</div>
									</div>

									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon"><fmt:message
													key="biolims.common.charges" /></span> <input type="text"
												changelog="<s:property value="sampleOrder.fee"/>" size="20" maxlength="25" class="form-control"
												id="sampleOrder_fee" name="sampleOrder.fee"
												title="<fmt:message key=" biolims.common.charges "/>"
												value="<s:property value=" sampleOrder.fee "/>" />

										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon"><fmt:message
													key="biolims.common.chargeForInstructions" /></span> <input
												type="text" changelog="<s:property value="sampleOrder.chargeNote"/>" size="20" maxlength="25"
												id="sampleOrder_chargeNote" name="sampleOrder.chargeNote"
												class="form-control"
												title="<fmt:message key=" biolims.common.chargeForInstructions "/>"
												value="<s:property value=" sampleOrder.chargeNote "/>" />

										</div>
									</div>
								</div>

								<input type="hidden" readonly="readonly" changelog="<s:property value="sampleOrder.successFlag"/>" size="20"
									maxlength="50" id="sampleOrder_successFlag"
									name="sampleOrder.successFlag" title="审核标志"
									value="<s:property value=" 1 " />" />
							</div>
							<div class="row">
								<div class="panel"
									style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
									<div class="panel-heading text-left">
										<h3 class="panel-title" style="font-family: 黑体;">
											<i class="glyphicon glyphicon-bookmark"></i>
											<fmt:message key="biolims.common.famliyInfo" />
										</h3>
									</div>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.familyMembersOfTheContact" /></span> <input
											type="text" changelog="<s:property value="sampleOrder.family"/>" size="20" maxlength="25" id="text48"
											name="sampleOrder.family" class="form-control"
											title="<fmt:message key=" biolims.common.familyMembersOfTheContact "/>"
											value="<s:property value=" sampleOrder.family "/>" />

									</div>
								</div>

								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.familyMembersOfTheContactPhoneNumber" /></span>

										<input type="text" changelog="<s:property value="sampleOrder.familyPhone"/>" size="20" maxlength="25"
											class="form-control" id="text49"
											onblur="checktelephone(this.value)"
											name="sampleOrder.familyPhone"
											title="<fmt:message key=" biolims.common.familyMembersOfTheContactPhoneNumber "/>"
											value="<s:property value=" sampleOrder.familyPhone "/>" />

									</div>
								</div>

								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.familyMembersAddress" /></span> <input
											type="text" changelog="<s:property value="sampleOrder.familySite"/>" size="30" maxlength="50"
											class="form-control" id="text70"
											name="sampleOrder.familySite"
											title="<fmt:message key=" biolims.common.familyMembersAddress "/>"
											value="<s:property value=" sampleOrder.familySite "/>" />
									</div>
								</div>

							</div>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">email</span> <input
											type="text" changelog="<s:property value="sampleOrder.email"/>" size="30" maxlength="50"
											class="form-control" id="text70"
											onblur="checkemail(this.value)" name="sampleOrder.email"
											title="email"
											value="<s:property value=" sampleOrder.email "/>" />

									</div>
								</div>

								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.common.zipCode" /></span> <input type="text"
											changelog="<s:property value="sampleOrder.zipCode"/>" size="30" maxlength="50" id="text70"
											name="sampleOrder.zipCode" class="form-control"
											title="<fmt:message key=" biolims.common.zipCode "/>"
											value="<s:property value=" sampleOrder.zipCode "/>" />

									</div>
								</div>
							</div>
							<div class="row">
								<div class="panel"
									style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
									<div class="panel-heading text-left">
										<h3 class="panel-title" style="font-family: 黑体;">
											<i class="glyphicon glyphicon-bookmark"></i>
											<fmt:message key="biolims.common.hospitalInfo" />
										</h3>
									</div>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
										<input type="hidden" changelog="<s:property value="sampleOrder.crmCustomer.id"/>" id="sampleOrder_crmCustomer_id"
											name="sampleOrder.crmCustomer.id"
											value="<s:property value=" sampleOrder.crmCustomer.id "/>">
										<span class="input-group-addon"><fmt:message
												key="biolims.sample.medicalInstitutions" /></span> <input
											type="text" changelog="<s:property value="sampleOrder.crmCustomer.name"/>" size="20" maxlength="25"
											readonly="readonly" id="sampleOrder_crmCustomer_name"
											class="form-control" name="sampleOrder.crmCustomer.name"
											title="医疗机构"
											value="<s:property value=" sampleOrder.crmCustomer.name "/>" />
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onclick="showHos()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.common.inspectionDepartment" /></span> <input
											list="sjks" changelog="<s:property value="sampleOrder.inspectionDepartment.id"/>" type="text" size="20"
											id="sampleOrder_inspectionDepartment_name"
											readonly="readonly"
											value="<s:property value=" sampleOrder.inspectionDepartment.name "/>"
											class="form-control" /> <input type="hidden"
											id="sampleOrder_inspectionDepartment_id"
											name="sampleOrder.inspectionDepartment.id"
											value="<s:property value=" sampleOrder.inspectionDepartment.id "/>">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onclick='showinspectionDepartment()'>
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>

								</div>
								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.sample.medicalInstitutionsPhone" /></span> <input
											type="text" changelog="<s:property value="sampleOrder.crmCustomerPhone"/>" size="20" maxlength="25"
											id="sampleOrder_crmCustomerPhone" class="form-control"
											name="sampleOrder.crmCustomerPhone" title="医疗机构联系电话"
											value="<s:property value=" sampleOrder.crmCustomerPhone "/>" />

									</div>
								</div>
							</div>
							<div class="row">

								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.sample.medicalInstitutionsSite" /></span> <input
											type="text" changelog="<s:property value="sampleOrder.crmCustomerSite"/>" size="30" maxlength="50"
											id="sampleOrder_crmCustomerSite" class="form-control"
											name="sampleOrder.crmCustomerSite" title="医疗机构联系地址"
											value="<s:property value=" sampleOrder.crmCustomerSite "/>" />

									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">

										<input type="hidden" changelog="<s:property value="sampleCancerTemp.crmDoctor.id"/>"
											id="sampleCancerTemp_attendingDoctor_id"
											name="sampleCancerTemp.crmDoctor.id"> <span
											class="input-group-addon"><fmt:message
												key="biolims.common.attendingPhysician" /></span> <input
											type="text" changelog="<s:property value="sampleOrder.attendingDoctor"/>" size="20" maxlength="25"
											id="sampleOrder_attendingDoctor" readonly="readonly"
											name="sampleOrder.attendingDoctor" class="form-control"
											title="<fmt:message key=" biolims.common.attendingPhysician "/>"
											value="<s:property value=" sampleOrder.attendingDoctor "/>" />
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id='showDoctor'
												onClick='showDoctors();'>
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>

								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.attendingPhysicianContactInformation" /></span>

										<input type="text" changelog="<s:property value="sampleOrder.attendingDoctorPhone"/>" size="20" maxlength="25"
											id="sampleOrder_attendingDoctorPhone"
											onblur="checktelephone(this.value)" class="form-control"
											name="sampleOrder.attendingDoctorPhone"
											title="<fmt:message key=" biolims.common.attendingPhysicianContactInformation "/>"
											value="<s:property value=" sampleOrder.attendingDoctorPhone "/>" />

									</div>
								</div>

							</div>
							<div class="row">
								<div class="panel"
									style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
									<div class="panel-heading text-left">
										<h3 class="panel-title" style="font-family: 黑体;">
											<i class="glyphicon glyphicon-bookmark"></i>
											<fmt:message key="biolims.common.otherInfo" />
										</h3>
									</div>
								</div>
							</div>
							<br>
							<div class="row" id="auditDiv">
								<input type="hidden" changelog="<s:property value="sampleOrder.createDate"/>" id="sampleOrder_createDate"
									name="sampleOrder.createDate"
									value="<s:date name=" sampleOrder.createDate " format="yyyy-MM-dd " />" />
								<input type="hidden" changelog="<s:property value="sampleOrder.createUser.name"/>" id="sampleOrder_createUser_name"
									value="<s:property value=" sampleOrder.createUser.name "/>" />
								<input type="hidden" changelog="<s:property value="sampleOrder.createUser.id"/>" id="sampleOrder_createUser"
									name="sampleOrder.createUser.id"
									value="<s:property value=" sampleOrder.createUser.id "/>">
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.auditor" /></span> <input type="text"
											changelog="<s:property value="sampleOrder.confirmUser.name"/>" size="20" readonly="readOnly"
											id="sampleOrder_confirmUser_name" class="form-control"
											value="<s:property value=" sampleOrder.confirmUser.name " />"
											readonly="readOnly" /> <input type="hidden" changelog="<s:property value="sampleOrder.confirmUser.id"/>"
											id="sampleOrder_confirmUser"
											name="sampleOrder.confirmUser.id"
											value="<s:property value=" sampleOrder.confirmUser.id " />">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onclick="showconfirmUser()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>


								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.auditDate" /></span> <input type="text"
											changelog="<s:property value="sampleOrder.confirmDate"/>" size="30" maxlength="25" id="text31"
											readonly="readonly" name="sampleOrder.confirmDate"
											class="form-control"
											title="<fmt:message key=" biolims.common.auditDate "/>"
											value="<s:date name=" sampleOrder.confirmDate " format="yyyy-MM-dd "/>" />

									</div>

								</div>
								<input type="hidden" changelog="<s:property value="sampleOrder.state"/>" id="sampleOrder_state"
									name="sampleOrder.state"
									value="<s:property value=" sampleOrder.state "/>"> <input
									type="hidden" changelog="<s:property value="sampleOrder.stateName"/>" id="sampleOrder_stateName"
									name="sampleOrder.stateName"
									value="<s:property value=" sampleOrder.stateName "/>" /> <input
									type="hidden" changelog="<s:property value="sampleOrder.sampleInfoMain"/>" id="sampleOrder.sampleInfoMain"
									name="sampleOrder.sampleInfoMain"
									title="<fmt:message key=" biolims.common.sampleList "/>"
									value="<s:property value=" sampleOrder.sampleInfoMain "/>">

								</div>
								<br/>
								<div class="row">
									<div class="col-xs-4">
										<div class="input-group">
									<button type="button" class="btn btn-info btn-sm" onclick="fileUp()">
										<fmt:message key="biolims.common.uploadAttachment" />
									</button>&nbsp;&nbsp;
									<%-- <span class="text label"><fmt:message
											key="biolims.common.aTotalOf" />${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
											key="biolims.common.attachment" /> --%>
										<button type="button" class="btn btn-info btn-sm"
											onclick="fileView()">
											<fmt:message key="biolims.report.checkFile" />
										</button>
								</div>
									</div>

								</div>
							
							<br>
							<div class="row">
								<div class="panel"
									style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
									<div class="panel-heading text-left">
										<h3 class="panel-title" style="font-family: 黑体;">
											<i class="glyphicon glyphicon-bookmark"></i>
											<fmt:message key="biolims.common.customFields" />

										</h3>
									</div>
								</div>
							</div>
							<br>
							<div class="row" id="fieldItemDiv"></div>

							<input type="hidden" changelog="<s:property value="sampleOrder.fieldContent"/>" name="sampleOrderInfoJson"
								id="sampleOrderInfoJson" value="" /> <input type="hidden"
								changelog id="id_parent_hidden"
								value="<s:property value=" sampleOrder.id "/>" /> <input
								type="hidden" changelog id="fieldContent"
								name="sampleOrder.fieldContent"
								value="<s:property value=" sampleOrder.fieldContent "/>" />
							<input type="hidden" id="changeLog" name="changeLog"  />

						</form>
					</div>
					<!--table表格-->
					<div class="HideShowPanel" style="display: none;">
						<table
							class="table table-hover table-striped table-bordered table-condensed"
							id="sampleInfoTable" style="font-size: 14px;">
						</table>
					</div>
					<!--table表格-->
					<div class="HideShowPanel" style="display: none;">
						<!-- <table
							class="table table-hover table-striped table-bordered table-condensed"
							id="kanbanTable" style="font-size: 14px;">
						</table> -->
						<ul class="timeline">
								<li class="time-label" id="parentWrap">
									<span class="bg-red"> </span>
								</li>
								<script type="text/html" id="template">
									{{each list}}
									<li>
										<i class="fa fa-hourglass-1 bg-blue"></i>

										<div class="timeline-item">
											<span class="time"><i class="fa fa-clock-o"></i></span>

											<h3 class="timeline-header"><a href="####">{{$value.stageName}}</a></h3>

											<div class="timeline-body table-responsive">
												<table class="table table-hover" style="margin-bottom: 0;">
													<tr>
														<td><fmt:message key="biolims.common.executionListId"/> ：{{$value.taskId}}</td>
														{{if $value.acceptUser}}
														<td><fmt:message key="biolims.common.testUserName"/>: {{$value.acceptUser.name}}</td>
														{{else}}
														<td><fmt:message key="biolims.common.testUserName"/> : </td>
														{{/if}}
														<td><fmt:message key="biolims.common.startTime"/> : {{$value.startDate}}</td>
													</tr>
													<tr>
														<td><fmt:message key="biolims.common.endTime"/> : {{$value.endDate}}</td>
														<td></td>
														<td><button class="viewSubTable btn btn-warning btn-xs" taskId={{$value.taskId}}><fmt:message key="biolims.common.viewProduct"/>  <i class="glyphicon glyphicon-plus"></i></button></td>
													</tr>
													<tr class="subTable hide">
														<td colspan="3">
															<table class="table table-bordered table-condensed" style="font-size: 12px;"></table>
														</td>
													</tr>
												</table>
											</div>
										</div>
									</li>

									{{/each}}
								</script>
								<li>
									<i class="fa fa-hourglass-3  bg-gray"></i>
								</li>
							</ul>
					</div>

				</div>
			</div>
		</div>
	</div>
<input type="hidden" id="lan" value="${sessionScope.lan}">
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script src="${ctx}/lims/plugins/template/template.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript"
		src="${ctx}/js/system/sample/sampleOrderMainEdit.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/system/sample/sampleOrderMainItemTable.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/system/sample/taskKanbanEditItem.js"></script>
</body>
</html>