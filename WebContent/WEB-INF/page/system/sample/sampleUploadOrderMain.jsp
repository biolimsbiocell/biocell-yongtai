﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/system/sample/sampleUploadOrderMain.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<script type="text/javascript" src="${ctx}/javascript/lib/jquery.simple_csv.js"></script>
</head>
<body>
		
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="text" id="extJsonDataString" name="extJsonDataString">	
		</div>
		<div id="show_sampleOrder_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_sampleOrder_tree_page"></div>
		
		<div id="bat_sampleOrderDD_div" style="display: none">
			<input type="file" name="file" id="file-uploadcsvDD-order"><fmt:message key="biolims.common.upLoadFile"/>
		</div>
		
		<div id="bat_sampleOrderYB_div" style="display: none">
			<input type="file" name="file" id="file-uploadcsvYB-order"><fmt:message key="biolims.common.upLoadFile"/>
		</div>
</body>
</html>



