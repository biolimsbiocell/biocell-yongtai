<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML>
<html>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
<title><fmt:message key="biolims.common.noTitleDocuments" /></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<link rel="stylesheet" href="${ctx}/lims/plugins/layui/css/layui.css">
<script type="text/javascript" src="${ctx}/lims/plugins/layui/layui.js"></script>
<style>
.dataTables_scrollBody {
	min-height: 150px;
}

.layui-layer-content .input-group {
	margin-top: 12px;
}

#fieldItemDiv .icheckbox_square-blue {
	margin-left: 10px;
}

#yesSpan {
	display: none;
}

#auditDiv {
	display: none;
}

/* #btn_submit {
	display: none;
}

#btn_changeState {
	display: none;
} */
.sampleNumberInput {
	width: 250px;
	height: 34px;
	border-radius: 6px;
	border: 1px solid #ccc;
	/* box-shadow: 1px 1px 1px #888888; */
	position: absolute;
	left: 200px;
	top: 212px;
}

.sampleNumberInputTow {
	width: 250px;
	height: 34px;
	border-radius: 6px;
	border: 1px solid #ccc;
	/* box-shadow: 1px 1px 1px #888888; */
	position: absolute;
	left: 510px;
	top: 212px;
}

.sampleNumberDiv {
	position: absolute;
	left: 140px;
	top: 223px;
}

.sampleNumberDivtow {
	position: absolute;
	left: 466px;
	top: 223px;
}

.sampleNumberbutton {
	position: absolute;
	left: 760px;
	top: 212px;
	border-radius: 4px;
	border: 1px solid #ccc;
	background-color: #5cb85c;
	width: 38px;
	height: 36px;
}
.col-xs-4 {
     margin-bottom: 7px;
}
</style>

<script type="text/javascript">
   var screeWidth = document.body.clientWidth;
   if(screeWidth > 768) {
    screeProportion = ["60%", "75%"];
   } else {
    screeProportion = ["95%", "75%"];
   }
   layui.use('layer', function() {
    layer = layui.layer;
    layer.config({
     offset: '30px'
    });
   });
  </script>
</head>
<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
<!-- 	选择样本-->	
<%-- <div class="modal fade" id="sampleOrderModal" tabindex="-1"
		role="dialog" aria-hidden="flase" data-backdrop="false">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header text-center">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" id="modal-body">
					<div id="sampleOrderpName" class="row">
						
					</div>
					<div class="col-lg-12" id="scancode" style="display: none;">
						<input type="text" class="form-control input-lg" id="scanInput"
							autofocus>
					</div>
				</div>
			</div>
		</div>
	</div> --%>


<!--AI录入-->
	<form method="post" class="hide" id="fileForm" name="fileForm"
		action='${ctx}/common/aiUtils/isIdAreThere.action'
		enctype="multipart/form-data">
		<input type="file" name="AiPicture" id="AiPicture" accept="image/*"
			onchange="subimtBtn()">
	</form>
	<div style="height: 14px"></div>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">
	<input type="hidden" id="flag" value="${requestScope.flag}">
	<input type="hidden" id="changeId" value="${requestScope.changeId}">
	<input type="hidden" id="order_type" value="${requestScope.type}">
	<input type="hidden" id="openFlag" value="${requestScope.open}">
	<input type="hidden" id="productId1" value="${requestScope.productId}">
	<input type="hidden" id="sjh" value="${requestScope.sjh}">
	<div class="container-fluid" style="margin-top: 46px">
		<!--订单录入的form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<fmt:message key="biolims.order.orderTitle" />
					</h3>
					<small style="color: #fff;"> <fmt:message
							key="biolims.common.commandPerson" />: <span
						id="sampleOrder_createUser"><s:property
								value=" sampleOrder.createUser.name" /></span> <fmt:message
							key="biolims.common.createDate" />: <span
						id="sampleOrder_createDate"><s:date
								name="sampleOrder.createDate" format="yyyy-MM-dd HH:mm:ss" /></span> <fmt:message
							key="biolims.common.state" />: <span id="sampleOrder_state"><s:property
								value="sampleOrder.stateName" /></span> <fmt:message
							key="biolims.common.auditor" />：<span id="confirmUserName"><s:property
								value="sampleOrder.confirmUser.name " /></span> <fmt:message
							key="biolims.common.completionTime" />：<span
						id="sampleOrder_confirmDate"><s:date
								name=" sampleOrder.confirmDate " format="yyyy-MM-dd" /></span>
					</small>
					<%-- <button type="button" class="btn btn-info btn-xs" id="aiBtn">
						<i class="layui-icon">&#xe67c;</i>
						<fmt:message key="biolims.order.AI" />
					</button> --%>
					<button id="oldOrder" type="button" class="btn btn-inverse"
						onclick="selectOrder()" style="display: none;">选择订单</button>
					<span id="yesSpan"><button type="button" id="yesBtn"
							class="btn btn-danger" onclick="yesBtn()">确认申请</button></span>
					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="####" onclick="$('.buttons-print').click();"><fmt:message
											key="biolims.common.print" /></a></li>
								<li><a href="#####" onclick="$('.buttons-copy').click();"><fmt:message
											key="biolims.common.copyData" /></a></li>
								<li><a href="####" onclick="$('.buttons-excel').click();"><fmt:message
											key="biolims.common.export" /></a></li>
								<li><a href="####" onclick="$('.buttons-csv').click();"><fmt:message
											key="biolims.common.exportCSV" /></a></li>
								<li role="separator" class="divider"></li>
								<li><a href="####" id="fixdeLeft2" onclick="fixedCol(2)"><fmt:message
											key="biolims.common.lock2Col" /></a></li>
								<li><a href="####" id="unfixde" onclick="unfixde()"><fmt:message
											key="biolims.common.cancellock2Col" /></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="box-body ipadmini">
					<!--进度条-->
					<div id="wizard" class="form_wizard wizard_horizontal">
						<ul class="wizard_steps">
							<li><a class="selected step"> <span class="step_no">1</span>
									<span class="step_descr"> Step 1<br /> <small><fmt:message
												key="biolims.order.orderTitle" /></small>
								</span>
							</a></li>
							<li><a class="done step"> <span class="step_no">2</span>
									<span class="step_descr"> Step 2<br /> <small><fmt:message
												key="biolims.order.itemEditinfo" /></small>
								</span>
							</a></li>

						</ul>
					</div>

					<!--form表单-->
					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
							<input type="hidden" id="bpmTaskId"
								value="${requestScope.bpmTaskId}" /> <br>
                             <div class="row">
								<div class="panel"
									style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
									<div class="panel-heading text-left">
										<h3 class="panel-title" style="font-family: 黑体;">
											<i class="glyphicon glyphicon-bookmark"></i>
											受试者信息
										</h3>
									</div>
								</div>
							</div>
							<br>
							
							<div class="row" id="patientInfor">
								<!-- 产品名称 -->
								<div class="col-xs-4" id="sampleOrder_productId1">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.crmDoctorItem.productName" /><img
											class='requiredimage' src='${ctx}/images/required.gif' /> </span> <input
											type="text" size="20"
											changelog="<s:property value="sampleOrder.productName"/>"
											readonly="readOnly" id="sampleOrder_productName"
											name="sampleOrder.productName"
											value="<s:property value=" sampleOrder.productName "/>"
											class="form-control" /> <input type="hidden"
											id="sampleOrder_productId" name="sampleOrder.productId"
											value="<s:property value=" sampleOrder.productId "/>">
											
										
										<!-- 批次状态 -->
										<input type="hidden"
											id="sampleOrder_batchStateName" name="sampleOrder.batchStateName"
											value="<s:property value=" sampleOrder.batchStateName "/>">
									<%-- 	<input type="hidden"
											id="sampleOrder_batchStateName" name="sampleOrder.batchStateName"
											value="<s:property value=" sampleOrder.productId "/>"> --%>
										<!--  -->
										
										
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id='showage'
												onClick="voucherProductFun()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>
								<!-- 申请单编号 -->
								<div class="col-xs-4" id="sampleOrder_id1">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message
												key="biolims.common.orderNumberCodes" />
										</span> <input list="sampleIdOne"
											changelog="<s:property value="sampleOrder.id"/>" type="text"
											size="20" maxlength="25" id="sampleOrder_id"
											name="sampleOrder.id" class="form-control"
											title="<fmt:message key="biolims.order.orderTitle"/>"
											readonly="readonly"
											value="<s:property value=" sampleOrder.id "/>" /> <span
											class="input-group-btn">
											<button class="btn btn-info" type="button" id="upLoadImg00"
												onclick="upLoadImg1()">图片预览</button>
										</span> <input type="hidden" id="upload_imga_id"
											changelog="<s:property value="sampleOrder.upLoadAccessory.id"/>"
											name="sampleOrder.upLoadAccessory.id" class="form-control"
											value="<s:property value=" sampleOrder.upLoadAccessory.id "/>">
										<input type="hidden" id="upload_imga_name11"
											changelog="<s:property value="sampleOrder.upLoadAccessory.fileName"/>"
											name="sampleOrder.upLoadAccessory.fileName"
											class="form-control"
											value="<s:property value=" sampleOrder.upLoadAccessory.fileName "/>">
										<input type="hidden" id="saveType" value="cq">
									</div>
								</div>
								<!-- 电子病历号 -->
								<div class="col-xs-4">
									<div class="input-group" id="sampleOrder_medicalNumber1">
										<span class="input-group-addon">电子病历号</span> <input
											type="text"
											changelog="<s:property value="sampleOrder.medicalNumber"/>"
											size="20" id="sampleOrder_medicalNumber"
											name="sampleOrder.medicalNumber" readonly="readonly"
											value="<s:property value=" sampleOrder.medicalNumber "/>"
											class="form-control" /> <span class="input-group-btn">
											<button class="btn btn-info" type="button"
												id='showagemedicalNumber' onClick="crmPatientTypeTwoFun()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>
								<!-- </div> -->
								
								
								<!-- 姓名 -->
<!-- 								<div class="row">
 -->								<div class="col-xs-4 " id="sampleOrder_name1">
									<div class="input-group">
										<span class="input-group-addon"> 姓名 </span> <input
											list="sampleName"
											changelog="<s:property value="sampleOrder.name"/>"
											type="text" size="20" maxlength="50" id="sampleOrder_name"
											name="sampleOrder.name" class="form-control" title="姓名"
											value="<s:property value=" sampleOrder.name "/>" />

									</div>
								</div>
								<div class="col-xs-4" id="sampleOrder_abbreviation1">
									<div class="input-group">
										<span class="input-group-addon">姓名缩写 </span> <input
											changelog="<s:property value="sampleOrder.abbreviation"/>"
											type="text" size="20" maxlength="50"
											id="sampleOrder_abbreviation" name="sampleOrder.abbreviation"
											class="form-control" title="姓名缩写"
											value="<s:property value=" sampleOrder.abbreviation "/>" />
									</div>
								</div>
							<!--出生日期  -->
								<div class="col-xs-4 " id="sampleOrder_birthDate1">
									<div class="input-group">
										<span class="input-group-addon" style="font-family: 黑体;">
											<fmt:message key="biolims.common.dateOfBirth" />
										</span> <input class="form-control"
											changelog="<s:property value="sampleOrder.birthDate"/>"
											type="text" size="20" maxlength="25"
											readonly
											id="sampleOrder_birthDate" onchange="jsGetAge()"
											name="sampleOrder.birthDate" title="出生日期"
											value="<s:date name=" sampleOrder.birthDate " format="yyyy-MM-dd" />" />
									</div>
								</div>
							<!-- </div> -->

							<!-- 民族 -->
							<!-- <div class="row"> -->
								<div class="col-xs-4" id="sampleOrder_nation1">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.ethnicity" /></span> <input list="sampleName"
											changelog="<s:property value="sampleOrder.nation"/>"
											type="text" size="20" maxlength="50" id="sampleOrder_nation"
											name="sampleOrder.nation" class="form-control"
											title="<fmt:message key="biolims.common.ethnicity"/>"
											value="<s:property value=" sampleOrder.nation "/>" />
									</div>
								</div>
								<!-- 籍贯 -->

								<div class="col-xs-4 " id="nativePlace1">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.nativePlace" /></span> <input type="text"
											changelog="<s:property value="sampleOrder.nativePlace"/>"
											size="20" maxlength="50" class="form-control"
											id="nativePlace" name="sampleOrder.nativePlace"
											title="<fmt:message key="biolims.common.nativePlace"/>"
											value="<s:property value=" sampleOrder.nativePlace "/>" />

									</div>
								</div>
							<!-- 	</div> -->
								<!-- 性别 -->
<!-- 								<div class="row">
 -->								<div class="col-xs-4" id="ckGender1">
									<input type="hidden" name="sampleOrder.gender"
										changelog="<s:property value="sampleOrder.gender"/>"
										id="sampleOrderGender"
										value="<s:property value=" sampleOrder.gender "/>" />
									<div class="input-group" id="ckGender" style="
                                    height: 34px; lay-filter="ckGender">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.gender" /></span>
										<s:if test='sampleOrder.gender=="1" '>
											<input type="checkbox" value="1" checked
												title=<fmt:message key="biolims.common.male" />>
											<input type="checkbox" value="0"
												title=<fmt:message key="biolims.common.female" />>
											<input type="checkbox" value="3"
												title=<fmt:message key="biolims.common.unknown" />>
										</s:if>
										<s:if test='sampleOrder.gender=="0" '>
											<input type="checkbox" value="1"
												title=<fmt:message key="biolims.common.male" />>
											<input type="checkbox" value="0"  checked
										 		title=<fmt:message key="biolims.common.female" />>
											<input type="checkbox" value="3"
												title=<fmt:message key="biolims.common.unknown" />>
										</s:if>
										<s:if test='sampleOrder.gender=="3" '>
											<input type="checkbox" value="1"
												title=<fmt:message key="biolims.common.male" />>
											<input type="checkbox" value="0"
												title=<fmt:message key="biolims.common.female" />>
											<input type="checkbox" value="3" checked
												title=<fmt:message key="biolims.common.unknown" />>
										</s:if>
										<s:if test='sampleOrder.gender==null '>
											<input type="checkbox" value="1"
												title=<fmt:message key="biolims.common.male" />>
											<input type="checkbox" value="0"
												title=<fmt:message key="biolims.common.female" />>
											<input type="checkbox" value="3" 
												title=<fmt:message key="biolims.common.unknown"/> >
										</s:if>
										<s:if test='sampleOrder.gender=="" '>
											<input type="checkbox" value="1"
												title=<fmt:message key="biolims.common.male" />>
											<input type="checkbox" value="0"
												title=<fmt:message key="biolims.common.female" />>
											<input type="checkbox" value="3" 
												title=<fmt:message key="biolims.common.unknown"/> >
										</s:if>
			
									</div>
								</div>
								<!--年龄  -->
							<!-- </div>
							<div class="row"> -->
								<div class="col-xs-4" id="sampleOrder_age1">
									<div class="input-group ">
										<span class="input-group-addon"><fmt:message
												key="biolims.crmPatient.age" /></span> <input type="text"
											size="20" changelog="<s:property value="sampleOrder.age"/>"
											maxlength="50" class="form-control" id="sampleOrder_age"
											name="sampleOrder.age" title="年龄" onchange="ag(this.value)"
											value="<s:property value=" sampleOrder.age "/>" />
									</div>
								</div>

								<!--身份证号  -->

								<div class="col-xs-4" id="sampleOrder_IDcardNo1">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.iDcard" /></span> <input type="text" size="20"
											changelog="<s:property value="sampleOrder.idCard"/>"
											maxlength="18" class="form-control" id="sampleOrder_IDcardNo"
											name="sampleOrder.idCard" title="身份证号" onchange="checkidcardno(this.value)"
											value="<s:property value=" sampleOrder.idCard "/>" />
									</div>
								</div>
<!-- 								</div>
 -->
								<!-- 筛选号 -->
<!-- 								<div class="row">
 -->								<div class="col-xs-4" id="sampleOrder_filtrateCode1">
									<div class="input-group">
										<span class="input-group-addon">筛选号<img
											class="requiredimage" src="/images/required.gif">
										</span> <input type="text"
											changelog="<s:property value="sampleOrder.filtrateCode"/>"
											size="30" maxlength="50" class="form-control"
											id="sampleOrder_filtrateCode" name="sampleOrder.filtrateCode"
											title="筛选号" onchange="filtrateCode(this.value)"
											value="<s:property value=" sampleOrder.filtrateCode "/>" />
									</div>
								</div>
							<!-- </div>
							随机号
							<div class="row"> -->
								<div class="col-xs-4" id="sampleOrder_randomCode1">
									<div class="input-group">
										<span class="input-group-addon">随机号<img
											class="requiredimage" src="/images/required.gif"></span> <input
											type="text"
											changelog="<s:property value="sampleOrder.randomCode"/>"
											size="30" maxlength="50" class="form-control"
											id="sampleOrder_randomCode" name="sampleOrder.randomCode"
											title="随机号" onchange="randomCode(this.value)"
											value="<s:property value=" sampleOrder.randomCode "/>" />
									</div>
								</div>
							<!-- 门诊住院号 -->
								<div class="col-xs-4" id="sampleOrder_hospitalPatientID1">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.hospitalPatientID" /></span> <input
											type="text" size="20"
											changelog="<s:property value="sampleOrder.hospitalPatientID"/>"
											maxlength="50" class="form-control"
											id="sampleOrder_hospitalPatientID"
											name="sampleOrder.hospitalPatientID" title="门诊/住院号"
											value="<s:property value=" sampleOrder.hospitalPatientID "/>" />
									</div>
									<!-- </div> -->
								</div>


								<%-- <div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.sample.oldId" /></span> <input type="text" size="20"
											changelog="<s:property value="sampleOrder.oldId"/>"
											maxlength="50" class="form-control" id="sampleOrder_oldId"
											name="sampleOrder.oldId"  readonly="readonly"
											value="<s:property value=" sampleOrder.oldId "/>" />
									</div>
								</div> --%>


				

										
<!-- 							</div>
<!--  -->							<!-- <div class="row"> -->
								
<!-- 							</div>
 -->							<!-- 批次号 -->
<!-- 							<div class="row">
 -->								<div class="col-xs-4" id="sampleOrder_barcodeNum1">
									<div class="input-group">
										<span class="input-group-addon">流水号</span> <input
											type="text"
											changelog="<s:property value=" sampleOrder.barcodeNum "/>"
											size="30" maxlength="50" class="form-control"
											id="sampleOrder_barcodeNum" name="sampleOrder.barcodeNum"
											title="患者顺序号" 
											value="<s:property value=" sampleOrder.barcodeNum "/>" />
									</div>
								</div>
								<div class="col-xs-4" id="sampleOrder_barcode1">
									<div class="input-group">
										<span class="input-group-addon">产品批号<%-- <fmt:message
												key="biolims.common.barcode" /> --%></span> <input type="text"
											changelog="<s:property value=" sampleOrder.barcode "/>"
											size="20" maxlength="50" class="form-control"
											id="sampleOrder_barcode" name="sampleOrder.barcode"
											disabled="disabled"
											value="<s:property value=" sampleOrder.barcode "/>" />
									</div>
								</div>
									<!-- CCOI -->
								<div class="col-xs-4">
									<div class="input-group" id="sampleOrder_ccoi1">
										<span class="input-group-addon">CCOI</span> <input type="text"
											changelog="<s:property value=" sampleOrder.ccoi "/>" size="30"
											maxlength="50" class="form-control" id="sampleOrder_ccoi"
											name="sampleOrder.ccoi" title="CCOI" disabled="disabled"
											value="<s:property value=" sampleOrder.ccoi "/>" />
									</div>
								</div>
								
								<div class="col-xs-4">
									<div class="input-group">
										<input type="hidden" size="20"
											changelog="<s:property value="sampleOrder.flagValue"/>"
											maxlength="50" class="form-control"
											id="sampleOrder_flagValue" name="sampleOrder.flagValue"
											readonly="readonly"
											value="<s:property value=" sampleOrder.flagValue "/>" />
									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<input type="hidden" class="form-control"
										changelog="<s:property value="sampleOrder.orderType"/>"
											id="sampleOrder_orderType" name="sampleOrder.orderType"
											value="<s:property value=" sampleOrder.orderType "/>" />
									</div>
								</div>
								<!-- </div> -->
 							</div>
 							
								<!-- 血样包装信息 -->
							<div class="row">
								<div class="panel"
									style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
									<div class="panel-heading text-left">
										<h3 class="panel-title" style="font-family: 黑体;">
											<i class="glyphicon glyphicon-bookmark"></i>
											血样信息
											<%-- <fmt:message key="biolims.common.orderBloodHiv" /> --%>
										</h3>
									</div>
								</div>
							</div>
							<br>
 							<div class="row">
 							<!-- 采血轮次 -->
								<div class="col-xs-4" id="sampleOrder_round1">
									<div class="input-group">
										<span class="input-group-addon" style="font-family: 黑体;">
											采血轮次<img class="requiredimage" src="/images/required.gif">
										</span> <input class="form-control " 
											changelog="<s:property value="sampleOrder.round"/>"
											type="text" size="20" maxlength="25" id="sampleOrder_round"
											name="sampleOrder.round" title="轮次" 
											value="<s:property value=" sampleOrder.round "/>" /><!-- onchange="lunci(this.value)" -->
									</div>
								</div>
 							<!-- 采血时间 -->
								<div class="col-xs-4" id="sampleOrder_drawBloodTime1">
									<div class="input-group">
										<span class="input-group-addon"> 预计采血时间 <img
											class="requiredimage" src="/images/required.gif">
										</span> <input type="text" readonly="readonly"
											changelog="<s:property value="sampleOrder.drawBloodTime"/>"
											size="20" maxlength="25" id="sampleOrder_drawBloodTime"
											name="sampleOrder.drawBloodTime" class="form-control"
											title="预计时间"
											value="<s:date name=" sampleOrder.drawBloodTime " format="yyyy-MM-dd HH:mm" />" onChange="cxsj();" />
									</div>
								</div>
								<div class="col-xs-4" id="sampleOrder_startBackTime1">
									<div class="input-group">
										<span class="input-group-addon"> 预计回输开始日期 <img
											class="requiredimage" src="/images/required.gif">
										</span> <input type="text" readonly="readonly"
											changelog="<s:property value="sampleOrder.startBackTime"/>"
											size="20" maxlength="25" id="sampleOrder_startBackTime"
											name="sampleOrder.startBackTime" class="form-control"
											title="预计回输开始日期 "
											value="<s:date name=" sampleOrder.startBackTime" format="yyyy-MM-dd" />" />
									</div>
								</div>
								<div class="col-xs-4" id="sampleOrder_feedBackTime1">
									<div class="input-group">
										<span class="input-group-addon"> 预计回输结束日期  <img
											class="requiredimage" src="/images/required.gif">
										</span> <input type="text" readonly="readonly"
											changelog="<s:property value="sampleOrder.feedBackTime"/>"
											size="20" maxlength="25" id="sampleOrder_feedBackTime"
											name="sampleOrder.feedBackTime" class="form-control"
											title="预计回输结束日期 "
											value="<s:date name=" sampleOrder.feedBackTime " format="yyyy-MM-dd" />" />
									</div>
								</div>
								<!-- 乙肝HBV -->
								<div class="col-xs-4" id="sampleOrder_hepatitisHbv1">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.HBV" /><img class="requiredimage"
											src="/images/required.gif"></span> <select changelog="<s:property value="sampleOrder.hepatitisHbv"/>"
											class="form-control" lay-ignore=""
											name="sampleOrder.hepatitisHbv" id="sampleOrder_hepatitisHbv">
											<option value=""
												<s:if test='sampleOrder.hepatitisHbv==""'>selected="selected"</s:if>></option>
											<option value="0"
												<s:if test='sampleOrder.hepatitisHbv=="0"'>selected="selected"</s:if>><fmt:message
													key="biolims.common.feminine" />-</option>
											<option value="1"
												<s:if test='sampleOrder.hepatitisHbv=="1"'>selected="selected"</s:if>><fmt:message
													key="biolims.common.masculine" />+</option>
										</select>
									</div>
								</div>
								<!-- 丙肝HCV -->
								<div class="col-xs-4" id="sampleOrder_hepatitisHcv1">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.HCV" /><img class="requiredimage"
											src="/images/required.gif"></span> <select changelog="<s:property value="sampleOrder.hepatitisHcv"/>"
											class="form-control" lay-ignore=""
											name="sampleOrder.hepatitisHcv" id="sampleOrder_hepatitisHcv">
											<option value=""
												<s:if test='sampleOrder.hepatitisHcv==""'>selected="selected"</s:if>></option>
											<option value="0"
												<s:if test='sampleOrder.hepatitisHcv=="0"'>selected="selected"</s:if>><fmt:message
													key="biolims.common.feminine" />-</option>
											<option value="1"
												<s:if test='sampleOrder.hepatitisHcv=="1"'>selected="selected"</s:if>><fmt:message
													key="biolims.common.masculine" />+</option>
										</select>
									</div>
								</div>
								<!-- 艾滋HIV -->
								<div class="col-xs-4" id="sampleOrder_HivVirus1">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.HIV" /><img class="requiredimage"
											src="/images/required.gif"></span> <select
											class="form-control" lay-ignore="" changelog="<s:property value="sampleOrder.HivVirus"/>"
											name="sampleOrder.HivVirus" id="sampleOrder_HivVirus">
											<option value=""
												<s:if test='sampleOrder.HivVirus==""'>selected="selected"</s:if>></option>
											<option value="0"
												<s:if test='sampleOrder.HivVirus=="0"'>selected="selected"</s:if>><fmt:message
													key="biolims.common.feminine" />-</option>
											<option value="1"
												<s:if test='sampleOrder.HivVirus=="1"'>selected="selected"</s:if>><fmt:message
													key="biolims.common.masculine" />+</option>
										</select>
									</div>
								</div>
<!-- 							</div>
 -->
<!-- 							<div class="row">
 -->								<!-- 梅毒TPHA -->
								<div class="col-xs-4" id="sampleOrder_syphilis1">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.TPHA" /><img class="requiredimage"
											src="/images/required.gif"></span> <select changelog="<s:property value="sampleOrder.syphilis"/>"
											class="form-control" lay-ignore=""
											name="sampleOrder.syphilis" id="sampleOrder_syphilis">
											<option value=""
												<s:if test='sampleOrder.syphilis==""'>selected="selected"</s:if>></option>
											<option value="0"
												<s:if test='sampleOrder.syphilis=="0"'>selected="selected"</s:if>><fmt:message
													key="biolims.common.feminine" />-</option>
											<option value="1"
												<s:if test='sampleOrder.syphilis=="1"'>selected="selected"</s:if>><fmt:message
													key="biolims.common.masculine" />+</option>
										</select>
									</div>
								</div>
								<!-- 白细胞计数 -->
								<div class="col-xs-4" id="sampleOrder_whiteBloodCellNum1">
									<div class="input-group">
										<span class="input-group-addon">白细胞计数（10^9/L）<!-- <img
											class="requiredimage" src="/images/required.gif"> --></span> <input
											type="text"
											changelog="<s:property value="sampleOrder.whiteBloodCellNum"/>"
											size="30" maxlength="50" class="form-control"
											id="sampleOrder_whiteBloodCellNum"
											name="sampleOrder.whiteBloodCellNum" onchange="whiteBloodCellNum(this.value)"
											value="<s:property value=" sampleOrder.whiteBloodCellNum "/>" />
										<%-- <span class="input-group-addon">(单位10^/L)</span> --%>
									</div>
								</div>
								<!-- 淋巴细胞百分比 -->
								<div class="col-xs-4" id="percentageOfLymphocytes1">
									<div class="input-group">
										<span class="input-group-addon">淋巴细胞百分比<!-- <img
											class="requiredimage" src="/images/required.gif"> --></span> <input
											type="text"
											changelog="<s:property value="sampleOrder.percentageOfLymphocytes"/>"
											size="30" maxlength="50" class="form-control"
											id="sampleOrder_percentageOfLymphocytes"
											name="sampleOrder.percentageOfLymphocytes" onchange="percentageOfLymphocytes(this.value)"
											value="<s:property value=" sampleOrder.percentageOfLymphocytes "/>" />
										<%-- <span class="input-group-addon">(单位%)</span>  --%>
									</div>
								</div>
<!-- 							</div>
 -->
<!-- 							<div class="row">
 -->								<!-- 淋巴细胞 -->
								<div class="col-xs-4" id="lymphoidCellSeries1">
									<div class="input-group">
										<span class="input-group-addon">淋巴细胞计数（10^9/L）</span> <input
											type="text"
											changelog="<s:property value="sampleOrder.lymphoidCellSeries"/>"
											size="30" maxlength="50" class="form-control"
											id="lymphoidCellSeries" name="sampleOrder.lymphoidCellSeries"
											onchange="lymphoidCell(this.value)"
											value="<s:property value=" sampleOrder.lymphoidCellSeries "/>" />
									</div>
								</div>
								<!-- 计算采血量 -->
								<div class="col-xs-4" id="xueliang">
									<div class="input-group">
										<span class="input-group-addon">计算采血量（ml）</span> <input type="text"
											changelog="<s:property value="sampleOrder.counterDrawBlood"/>"
											size="30" maxlength="50" id="text70"
											name="sampleOrder.counterDrawBlood" class="form-control"
											title="<fmt:message key="biolims.common.counterDrawBlood"/>"
											value="<s:property value=" sampleOrder.counterDrawBlood "/>" />
									</div>
								</div>
								<!-- 采血管数 -->
								<div class="col-xs-4" id="xueguan">
									<div class="input-group">
										<span class="input-group-addon">采血管数（管）</span> <input type="text"
											changelog="<s:property value="sampleOrder.heparinTube"/>"
											size="30" maxlength="50" class="form-control" id="text71"
											name="sampleOrder.heparinTube" title="heparinTube"
											value="<s:property value=" sampleOrder.heparinTube "/>" />
									</div>
								</div>
							</div>

							<!-- 产品主数据 -->
							<%-- <div id="ybxx">
								<div class="row">
									<div class="panel"
										style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
										<div class="panel-heading text-left">
											<h3 class="panel-title" style="font-family: 黑体;">
												<i class="glyphicon glyphicon-bookmark"></i>
												<fmt:message key="biolims.dashboard.productData" />
											</h3>
										</div>
									</div>
								</div>
								<br />

								<div class="row">
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon"><fmt:message
													key="biolims.crmDoctorItem.productName" /><img
												class='requiredimage' src='${ctx}/images/required.gif' /> </span>
											<input type="text" size="20" changelog="<s:property value="sampleOrder.productName"/>" readonly="readOnly"
												id="sampleOrder_productName" name="sampleOrder.productName"
												value="<s:property value=" sampleOrder.productName "/>"
												class="form-control" /> <input type="hidden"
												id="sampleOrder_productId" name="sampleOrder.productId"
												value="<s:property value=" sampleOrder.productId "/>">
											<span class="input-group-btn">
												<button class="btn btn-info" type="button" id='showage'
													onClick="voucherProductFun()">
													<i class="glyphicon glyphicon-search"></i>
												</button>
											</span>
										</div>
									</div>
									<div class="col-xs-4">

										<div class="input-group">
											<span class="input-group-addon"><fmt:message
													key="biolims.common.transportType" /></span> <input type="text"
												changelog="<s:property value="sampleOrder.yushufs.name"/>" size="10" id="sampleOrder_yushufs_name"
												readonly="readonly"
												value="<s:property value=" sampleOrder.yushufs.name "/>"
												class="form-control" /> <input type="hidden" 
												changelog="<s:property value="sampleOrder.yushufs.id"/>"
												id="sampleOrder_yushufs_id" name="sampleOrder.yushufs.id"
												value="<s:property value=" sampleOrder.yushufs.id "/>">
											<span class="input-group-btn">
												<button class="btn btn-info" type="button" id='yushufs'
													onclick="yunshufs()">
													<i class="glyphicon glyphicon-search"></i>
												</button>
											</span>
										</div>
									</div>
									<div class="col-xs-4">
										<div class="input-group">

											<span class="input-group-addon"><fmt:message
													key="biolims.common.note" /></span> <input type="text" 
													changelog="<s:property value="sampleOrder.note"/>"
												size="50" maxlength="250" id="text77"
												name="sampleOrder.note" class="form-control"
												title="<fmt:message key=" biolims.common.note "/>"
												value="<s:property value=" sampleOrder.note "/>" />
										</div>
									</div>
								</div>
							</div> --%>

							<!-- 临床诊断 -->
							<%-- 							<div id="diagnosisInfo">
								<div class="row">
									<div class="panel"
										style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
										<div class="panel-heading text-left">
											<h3 class="panel-title" style="font-family: 黑体;">
												<i class="glyphicon glyphicon-bookmark"></i>
												<fmt:message key="biolims.common.clinicalDiagnosis" />
												<!--临床诊断-->
		<input type="file" class="hidden" name="AiPicture" id="AiPicture2" accept="image/*" onchange="subimtBtn2(this)" value='1234567'>
												<button type="button" class="btn btn-info btn-xs"
													id="aiBtn2">
													<i class="layui-icon">&#xe67c;</i><fmt:message key="biolims.order.AI" />
												</button>
											</h3>
										</div>
									</div>
								</div>
								<br />
							</div>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.healthCondition" /></span> <input
											type="hidden" changelog="<s:property value="sampleOrder.medicalHistoryId"/>" id="sampleOrder_medicalHistoryId"
											name="sampleOrder.medicalHistoryId"
											value="<s:property value=" sampleOrder.medicalHistoryId "/>">

										<input type="text" changelog="<s:property value="sampleOrder.medicalHistory"/>" size="30" readonly="readOnly"
											maxlength="50" id="sampleOrder_medicalHistory"
											name="sampleOrder.medicalHistory" title="临床诊断"
											class="form-control"
											value="<s:property value=" sampleOrder.medicalHistory "/>" />
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												id='showmedicalHistory' onClick="medicalHistoryFun()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.medicationInProgress" /></span> <input
											type="hidden" changelog="<s:property value="sampleOrder.yongyaoId"/>" id="sampleOrder_yongyaoId"
											name="sampleOrder.yongyaoId"
											value="<s:property value=" sampleOrder.yongyaoId "/>" /> <input
											type="text" changelog="<s:property value="sampleOrder.yongyao"/>" size="20" readonly="readOnly"
											id="sampleOrder_yongyao" name="sampleOrder.yongyao"
											value="<s:property value=" sampleOrder.yongyao "/>"
											class="form-control" /> <span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="sampleYongYaoFun()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>

								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.common.frequencyOfOccurrence" /></span> <input
											type="text" changelog="<s:property value="sampleOrder.fspl"/>" size="30" maxlength="50"
											class="form-control" id="text70" name="sampleOrder.fspl"
											title="发生频率" value="<s:property value=" sampleOrder.fspl "/>" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">


										<span class="input-group-addon"><fmt:message
												key="biolims.common.allergicHistory" /></span> <input type="text"
											changelog="<s:property value="sampleOrder.guomin"/>" size="30" maxlength="50" class="form-control"
											id="text70" name="sampleOrder.guomin" title="过敏史"
											value="<s:property value=" sampleOrder.guomin "/>" />
									</div>
								</div>

							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.clinicalDiagnosis" /></span>
										<textarea type="text" changelog="<s:property value="sampleOrder.diagnosis"/>" size="30" maxlength="50"
											class="form-control" id="diagnosis"
											name="sampleOrder.diagnosis" title="临床诊断"><s:property
												value=" sampleOrder.diagnosis " /></textarea>
									</div>
								</div>
							</div> --%>

							<!-- 费用信息 -->
							<%-- <div id="chargeInfo">
								<div class="row">
									<div class="panel"
										style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
										<div class="panel-heading text-left">
											<h3 class="panel-title" style="font-family: 黑体;">
												<i class="glyphicon glyphicon-bookmark"></i>
												<fmt:message key="biolims.common.freightInfo" />
											</h3>
										</div>
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon"><fmt:message
													key="biolims.common.salesRepresentative" /></span> <input
												changelog="<s:property value="sampleOrder.commissioner.name"/>" type="text" size="10"
												id="sampleOrder_commissioner_name" readonly="readonly"
												value="<s:property value=" sampleOrder.commissioner.name "/>"
												class="form-control" /> <input type="hidden"
												id="sampleOrder_commissioner"
												name="sampleOrder.commissioner.id"
												value="<s:property value=" sampleOrder.commissioner.id "/>">
											<span class="input-group-btn">
												<button class="btn btn-info" type="button"
													onclick="showcommissioner()">
													<i class="glyphicon glyphicon-search"></i>
												</button>
											</span>
										</div>
									</div>

									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon"><fmt:message
													key="biolims.common.wayToCharge" /></span> <input type="text"
												changelog="<s:property value="sampleOrder.collectionManner.name"/>" size="10" readonly="readOnly"
												id="sampleOrder_collectionManner_name"
												value="<s:property value=" sampleOrder.collectionManner.name "/>"
												class="form-control" /> <input type="hidden" changelog="<s:property value="sampleOrder.collectionManner.id"/>"
												id="sampleOrder_collectionManner"
												name="sampleOrder.collectionManner.id"
												value="<s:property value=" sampleOrder.collectionManner.id "/>">
											<span class="input-group-btn">
												<button class="btn btn-info" type="button"
													onclick="showCollectionManner()">
													<i class="glyphicon glyphicon-search"></i>
												</button>
											</span>
										</div>
									</div>

									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon"><fmt:message
													key="biolims.common.charges" /></span> <input type="text"
												changelog="<s:property value="sampleOrder.fee"/>" size="20" maxlength="25" class="form-control"
												id="sampleOrder_fee" name="sampleOrder.fee"
												title="<fmt:message key=" biolims.common.charges "/>"
												value="<s:property value=" sampleOrder.fee "/>" />

										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon"><fmt:message
													key="biolims.common.chargeForInstructions" /></span> <input
												type="text" changelog="<s:property value="sampleOrder.chargeNote"/>" size="20" maxlength="25"
												id="sampleOrder_chargeNote" name="sampleOrder.chargeNote"
												class="form-control"
												title="<fmt:message key=" biolims.common.chargeForInstructions "/>"
												value="<s:property value=" sampleOrder.chargeNote "/>" />

										</div>
									</div>
								</div>

								<input type="hidden" readonly="readonly" changelog="<s:property value="sampleOrder.successFlag"/>" size="20"
									maxlength="50" id="sampleOrder_successFlag"
									name="sampleOrder.successFlag" title="审核标志"
									value="<s:property value=" 1 " />" />
							</div> --%>

							<!-- 肿瘤类型及分期 -->
							<div class="row">
								<div class="panel"
									style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
									<div class="panel-heading text-left">
										<h3 class="panel-title" style="font-family: 黑体;">
											<i class="glyphicon glyphicon-bookmark"></i>
											<fmt:message key="biolims.common.cancarTypeinstalment" />
										</h3>
									</div>
								</div>
							</div>
							<br>
							<!-- 肿瘤类型 -->
							<div class="row">
								<div class="col-xs-4" id="sampleOrder_zhongliu_name1">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.dicCancarTyp" /></span> <input list="sjks"
											readonly="readonly" name="sampleOrder.dicType.id"
											changelog="<s:property value="sampleOrder.dicType.id"/>"
											type="hidden" size="20" id="sampleOrder_dicType_id"
											value="<s:property value=" sampleOrder.dicType.id "/>"
											class="form-control" /> <input type="hidden"
											readonly="readonly" class="form-control"
											changelog="<s:property value="sampleOrder.zhongliu.id"/>"
											id="sampleOrder_zhongliu_id" name="sampleOrder.zhongliu.id"
											value="<s:property value=" sampleOrder.zhongliu.id "/>">
										<input type="text" readonly="readonly" class="form-control"
											id="sampleOrder_zhongliu_name"
											changelog="<s:property value="sampleOrder.zhongliu.name"/>"
											value="<s:property value=" sampleOrder.zhongliu.name "/>">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onclick='showcancerTypeTable()'>
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>

								<!-- 肿瘤分期 -->
								<div class="col-xs-4" id="cancerInstalment_name1">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.cancerInstalment" /></span> <input list="sjks"
											changelog="<s:property value="sampleOrder.tumorStaging.name"/>"
											type="text" size="20" id="sampleOrder_cancerInstalment_name"
											readonly="readonly"
											value="<s:property value=" sampleOrder.tumorStaging.name "/>"
											class="form-control" /> <input type="hidden"
											changelog="<s:property value="sampleOrder.tumorStaging.name"/>"
											id="sampleOrder_cancerInstalment_id"
											name="sampleOrder.tumorStaging.id"
											value="<s:property value=" sampleOrder.tumorStaging.id "/>">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onclick='showTimesTable()'>
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>
<!-- 							</div>
 -->
<!-- 							<div class="row">
 -->								<!-- 肿瘤类型备注 -->
								<div class="col-xs-4" id="sampleOrder_zhongliuNote1">
									<div class="input-group">
										<span class="input-group-addon">肿瘤类型备注</span> <input
											type="text"
											changelog="<s:property value="sampleOrder.zhongliuNote"/>"
											size="30" maxlength="50" class="form-control"
											id="sampleOrder_zhongliuNote" name="sampleOrder.zhongliuNote"
											title="肿瘤类型备注"
											value="<s:property value=" sampleOrder.zhongliuNote "/>" />
									</div>
								</div>
								<!-- 分期备注 -->
								<div class="col-xs-4" id="sampleOrder_tumorStagingNote1">
									<div class="input-group">
										<span class="input-group-addon">肿瘤分期备注</span> <input
											type="text"
											changelog="<s:property value="sampleOrder.tumorStagingNote"/>"
											size="30" maxlength="50" class="form-control"
											id="sampleOrder_tumorStagingNote"
											name="sampleOrder.tumorStagingNote" title="肿瘤分期备注"
											value="<s:property value=" sampleOrder.tumorStagingNote "/>" />
									</div>
								</div>
 							</div>
							
							
							<!-- 医院信息 -->
							<div class="row">
								<div class="panel"
									style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
									<div class="panel-heading text-left">
										<h3 class="panel-title" style="font-family: 黑体;">
											<i class="glyphicon glyphicon-bookmark"></i>
											<fmt:message key="biolims.common.hospitalInfo" />
										</h3>
									</div>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-xs-4" id="sampleOrder_crmCustomer_name1">
									<div class="input-group">
										<input type="hidden"
											changelog="<s:property value="sampleOrder.crmCustomer.id"/>"
											id="sampleOrder_crmCustomer_id"
											name="sampleOrder.crmCustomer.id"
											value="<s:property value="sampleOrder.crmCustomer.id"/>">
										<span class="input-group-addon"><fmt:message
												key="biolims.sample.medicalInstitutions" /> <img
											class='requiredimage' src='${ctx}/images/required.gif' /> </span> <input
											type="text"
											changelog="<s:property value="sampleOrder.crmCustomer.name"/>"
											size="20" maxlength="25" readonly="readonly"
											id="sampleOrder_crmCustomer_name" class="form-control"
											title="医疗机构"
											value="<s:property value=" sampleOrder.crmCustomer.name "/>" />
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onclick="showHos()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>
								<div class="col-xs-4" id="inspectionDepartment_name1">
									<div class="input-group">
										<span class="input-group-addon">科室 <img
											class='requiredimage' src='${ctx}/images/required.gif' />
										</span> <input list="sjks"
											changelog="<s:property value="sampleOrder.inspectionDepartment.name"/>"
											type="text" size="20"
											id="sampleOrder_inspectionDepartment_name"
											readonly="readonly"
											value="<s:property value=" sampleOrder.inspectionDepartment.name "/>"
											class="form-control" /> <input type="hidden"
											changelog="<s:property value="sampleOrder.inspectionDepartment.name"/>"
											id="sampleOrder_inspectionDepartment_id"
											name="sampleOrder.inspectionDepartment.id"
											value="<s:property value=" sampleOrder.inspectionDepartment.id "/>">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onclick='showinspectionDepartment()'>
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>
								<div class="col-xs-4" id="sampleOrder_crmPhone1">
									<div class="input-group">
										<span class="input-group-addon">科室电话 </span><input
											type="text"
											changelog="<s:property value="sampleOrder.crmPhone"/>"
											size="20" maxlength="25"
											id="sampleOrder_crmPhone" onchange="findcrmPhone(this.value)"
											class="form-control"
											name="sampleOrder.crmPhone" title="科室电话"
											value="<s:property value=" sampleOrder.crmPhone "/>" />
									</div>
								</div>
<!-- 						</div>
 -->					
<!--                          <div class="row">
 -->								<div class="col-xs-4" id="medicalInstitutionsSite1">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.sample.medicalInstitutionsSite" /></span> <input
											type="text"
											changelog="<s:property value="sampleOrder.medicalInstitutionsSite"/>"
											size="30" maxlength="50"
											id="sampleOrder_medicalInstitutionsSite" class="form-control"
											name="sampleOrder.medicalInstitutionsSite" title="医疗机构联系地址"
											value="<s:property value=" sampleOrder.medicalInstitutionsSite "/>" />

									</div>
								</div>
								<div class="col-xs-4" id="sampleOrder_attendingDoctor1">
									<div class="input-group">
										<input type="hidden"
											changelog="<s:property value="sampleOrder.crmDoctor.id"/>"
											id="sampleOrder_crmDoctor_id" name="sampleOrder.crmDoctor.id"
											value="<s:property value=" sampleOrder.crmDoctor.id "/>"
											>
										<span class="input-group-addon"><fmt:message
												key="biolims.common.attendingPhysician" /></span> <input
											type="text"
											changelog="<s:property value="sampleOrder.attendingDoctor"/>"
											size="20" maxlength="25" id="sampleOrder_attendingDoctor"
											readonly="readonly" name="sampleOrder.attendingDoctor"
											class="form-control"
											title="<fmt:message key="biolims.common.attendingPhysician"/>"
											value="<s:property value=" sampleOrder.attendingDoctor "/>" />
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id='showDoctor'
												onClick='showDoctors();'>
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>
								<div class="col-xs-4" id="attendingDoctorPhone1">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.attendingPhysicianContactInformation" /></span>
										<input type="text"
											changelog="<s:property value="sampleOrder.attendingDoctorPhone"/>"
											size="20" maxlength="25"
											id="sampleOrder_attendingDoctorPhone"
											onchange="findTelephone(this.value)" class="form-control"
											name="sampleOrder.attendingDoctorPhone"
											title="<fmt:message key="biolims.common.attendingPhysicianContactInformation"/>"
											value="<s:property value=" sampleOrder.attendingDoctorPhone "/>" />
									</div>
								</div>
<!-- 							</div>
 -->							<!-- <div class="row"> -->
								<div class="col-xs-4" id="sampleOrder_medicationPlan1">
									<div class="input-group">
										<span class="input-group-addon">临床用药方案</span> <select
											class="form-control"
											changelog="<s:property value="sampleOrder.medicationPlan"/>"
											lay-ignore="" name="sampleOrder.medicationPlan"
											id="sampleOrder_medicationPlan">
											<option value=""
												<s:if test='sampleOrder.medicationPlan==""'>selected="selected"</s:if>>请选择</option>
											<option value="1"
												<s:if test='sampleOrder.medicationPlan=="1"'>selected="selected"</s:if>>放疗</option>
											<option value="2"
												<s:if test='sampleOrder.medicationPlan=="2"'>selected="selected"</s:if>>化疗</option>
											<option value="3"
												<s:if test='sampleOrder.medicationPlan=="3"'>selected="selected"</s:if>>靶向药</option>
											<option value="4"
												<s:if test='sampleOrder.medicationPlan=="4"'>selected="selected"</s:if>>手术</option>
											<option value="5"
												<s:if test='sampleOrder.medicationPlan=="5"'>selected="selected"</s:if>>其他</option>
										</select>
									</div>
								</div>
							<%-- 	<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">代理商</span> <input type="text"
											size="20" maxlength="25" readonly="readonly"
											changelog="<s:property value="sampleOrder.agreementTask.name"/>"
											id="sampleOrder_agreementTask_name" class="form-control"
											value="<s:property value=" sampleOrder.agreementTask.name "/>" />
										<input type="hidden" size="20" maxlength="25"
											changelog="<s:property value="sampleOrder.agreementTask.id"/>"
											id="sampleOrder_agreementTask_id" class="form-control"
											name="sampleOrder.agreementTask.id"
											value="<s:property value=" sampleOrder.agreementTask.id "/>" />
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id='showDoctor'
												onClick='showAgreementTasks();'>
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">代理商联系方式</span> <input
											type="text" size="20" maxlength="25" readonly="readonly"
											changelog="<s:property value="sampleOrder.agreementTask.phone"/>"
											id="sampleOrder_agreementTask_contactPhone"
											class="form-control"
											value="<s:property value=" sampleOrder.agreementTask.phone "/>" />
									</div>
								</div> --%>
							</div>

						
							
							<!-- 家属 -->
							<div class="row" id="home">
								<div class="panel"
									style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
									<div class="panel-heading text-left">
										<h3 class="panel-title" style="font-family: 黑体;">
											<i class="glyphicon glyphicon-bookmark"></i>
											<fmt:message key="biolims.common.famliyInfo" />
										</h3>
									</div>
								</div>
							</div>
							<br>

							<div class="row">
								<div class="col-xs-4" id="homePeople">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.familyMembersOfTheContact" /></span> <input
											type="text"
											changelog="<s:property value="sampleOrder.family"/>"
											size="20" maxlength="25" id="text48"
											name="sampleOrder.family" class="form-control"
											title="<fmt:message key="biolims.common.familyMembersOfTheContact"/>"
											value="<s:property value=" sampleOrder.family "/>" />
									</div>
								</div>
								<div class="col-xs-4" id="homePhone">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.familyMembersOfTheContactPhoneNumber" /></span>

										<input type="text"
											changelog="<s:property value="sampleOrder.familyPhone"/>"
											size="20" maxlength="25" class="form-control" id="text49"
											onchange="checktelephone(this.value)"
											name="sampleOrder.familyPhone"
											title="<fmt:message key="biolims.common.familyMembersOfTheContactPhoneNumber"/>"
											value="<s:property value=" sampleOrder.familyPhone "/>" />
									</div>
								</div>

								<div class="col-xs-4" id="homeAddress">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.familyMembersAddress" /></span> <input
											type="text"
											changelog="<s:property value="sampleOrder.familySite"/>"
											size="30" maxlength="50" class="form-control" id="text72"
											name="sampleOrder.familySite"
											title="<fmt:message key="biolims.common.familyMembersAddress"/>"
											value="<s:property value=" sampleOrder.familySite "/>" />
									</div>
								</div>
							<!-- </div>
							<div class="row"> -->
								<div class="col-xs-4" id="email1">
									<div class="input-group">
										<span class="input-group-addon">email</span> <input
											type="text"
											changelog="<s:property value="sampleOrder.email"/>" size="30"
											maxlength="50" class="form-control" id="text73"
											onchange="checkemail(this.value)" name="sampleOrder.email"
											title="email"
											value="<s:property value=" sampleOrder.email "/>" />
									</div>
								</div>

								<div class="col-xs-4" id="zipCode">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.common.zipCode" /></span> <input type="text"
											changelog="<s:property value="sampleOrder.zipCode"/>"
											size="30" maxlength="50" id="text74" onchange="checkzipCode(this.value)"
											name="sampleOrder.zipCode" class="form-control"
											title="<fmt:message key="biolims.common.zipCode"/>"
											value="<s:property value=" sampleOrder.zipCode "/>" />

									</div>
								</div>
							<!-- </div> -->
							<!-- <div class="row"> -->
								<%-- <div class="col-xs-4" style="display:none" >
									<div class="input-group">
										<span class="input-group-addon"> 参考回输日期(开始) </span> <input
											type="text"
											changelog="<s:property value="sampleOrder.backtolosetime"/>"
											size="20" maxlength="25" id="sampleOrder_backtolosetime"
											name="sampleOrder.backtolosetime" class="form-control"
											title="参考回输日期"
											value="<s:property value=" sampleOrder.backtolosetime "/>" />


									</div>
								</div>
								<div class="col-xs-4" style="display:none">
									<div class="input-group">
										<span class="input-group-addon"> 参考回输日期(结束) </span> <input
											type="text"
											changelog="<s:property value="sampleOrder.timebucket"/>"
											size="20" maxlength="25" id="sampleOrder_timebucket"
											name="sampleOrder.timebucket" class="form-control" title=""
											value="<s:property value=" sampleOrder.timebucket "/>" />
									</div>
								</div>
 --%>
							</div>

							<!-- 其他 -->
							<div class="row">
								<div class="panel"
									style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
									<div class="panel-heading text-left">
										<h3 class="panel-title" style="font-family: 黑体;">
											<i class="glyphicon glyphicon-bookmark"></i>
											<fmt:message key="biolims.common.otherInfo" />
										</h3>
									</div>
								</div>
							</div>
							<br>
							<div class="row">
								<input type="hidden"
									changelog="<s:property value="sampleOrder.createDate"/>"
									id="sampleOrder_createDate" name="sampleOrder.createDate"
									value="<s:date name=" sampleOrder.createDate " format="yyyy-MM-dd HH:mm:ss" />" />
								<input type="hidden"
									changelog="<s:property value="sampleOrder.receiveState"/>"
									id="sampleOrder_receiveState" name="sampleOrder.receiveState"
									value="<s:property value=" sampleOrder.receiveState "/>" /> <input
									type="hidden"
									changelog="<s:property value="sampleOrder.createUser.name"/>"
									id="sampleOrder_createUser_name"
									value="<s:property value=" sampleOrder.createUser.name "/>" />
								<input type="hidden"
									changelog="<s:property value="sampleOrder.createUser.id"/>"
									id="sampleOrder_createUser" name="sampleOrder.createUser.id"
									value="<s:property value=" sampleOrder.createUser.id "/>">
								<div class="col-xs-4" id="sampleOrder_confirmUser_name1">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.auditor" /></span> <input type="text"
											changelog="<s:property value="sampleOrder.confirmUser.name"/>"
											size="20" readonly="readOnly"
											id="sampleOrder_confirmUser_name" class="form-control"
											value="<s:property value=" sampleOrder.confirmUser.name " />"
											readonly="readOnly" /> <input type="hidden"
											changelog="<s:property value="sampleOrder.confirmUser.id"/>"
											id="sampleOrder_confirmUser"
											name="sampleOrder.confirmUser.id"
											value="<s:property value=" sampleOrder.confirmUser.id " />">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onclick="showconfirmUser()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>


								<div class="col-xs-4" id="confirmTime">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.auditDate" /></span> <input type="text"
											changelog="<s:property value="sampleOrder.confirmDate"/>"
											size="30" maxlength="25" id="text31" readonly="readonly"
											name="sampleOrder.confirmDate" class="form-control"
											title="<fmt:message key="biolims.common.auditDate"/>"
											value="<s:date name=" sampleOrder.confirmDate " format="yyyy-MM-dd"/>" />
									</div>

								</div>
								<div class="col-xs-4" id="cancel1">
									<div class="input-group">
										<span class="input-group-addon">作废原因</span> <input
											type="text"
											changelog="<s:property value="sampleOrder.cancel"/>" size="30"
											maxlength="50" class="form-control" id="text73"
											 name="sampleOrder.cancel"
											title="作废原因"
											value="<s:property value=" sampleOrder.cancel "/>" />
									</div>
								</div>
								
								<input type="hidden"
									changelog="<s:property value="sampleOrder.state"/>"
									id="sampleOrder_state" name="sampleOrder.state"
									value="<s:property value=" sampleOrder.state "/>"> <input
									type="hidden"
									changelog="<s:property value="sampleOrder.stateName"/>"
									id="sampleOrder_stateName" name="sampleOrder.stateName"
									value="<s:property value=" sampleOrder.stateName "/>" /> <input
									type="hidden"
									changelog="<s:property value="sampleOrder.sampleInfoMain"/>"
									id="sampleOrder.sampleInfoMain"
									name="sampleOrder.sampleInfoMain"
									title="<fmt:message key="biolims.common.sampleList"/>"
									value="<s:property value=" sampleOrder.sampleInfoMain "/>">

							</div>
							<br />
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
										<button type="button" class="btn btn-info btn-sm"
											onclick="fileUp()">
											<fmt:message key="biolims.common.uploadAttachment" />
										</button>
										<span class="text alabel"><fmt:message
											key="biolims.common.aTotalOf" />${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
											key="biolims.common.attachment" />
										<button type="button" class="btn btn-info btn-sm"
											onclick="fileView()">
											<fmt:message key="biolims.report.checkFile" />
										</button>
									</div>
								</div>

							</div>
							<br>


							<%-- 							<div class="row">
								<div class="panel"
									style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
									<div class="panel-heading text-left">
										<h3 class="panel-title" style="font-family: 黑体;">
											<i class="glyphicon glyphicon-bookmark"></i>
											<fmt:message key="biolims.common.customFields" />

										</h3>
									</div>
								</div>
							</div>
							<br>
							<div class="row" id="fieldItemDiv"></div> --%>
							<input type="hidden"
								changelog="<s:property value="sampleOrder.fieldContent"/>"
								name="sampleOrderInfoJson" id="sampleOrderInfoJson" value="" />
							<input type="hidden" changelog id="id_parent_hidden"
								value="<s:property value=" sampleOrder.id "/>" /> <input
								type="hidden" changelog id="fieldContent"
								name="sampleOrder.fieldContent"
								value="<s:property value=" sampleOrder.fieldContent "/>" /> <input
								type="hidden" id="changeLog" name="changeLog" />

						</form>
					</div>
					<!--table表格-->
					<div class="HideShowPanel" style="display: none;">
						<div id="item_input">
							<!--采血管数-->
							<p class="sampleNumberDiv">
								<fmt:message key="biolims.common.numberVessels" />
							</p>
							<input class="sampleNumberInput" id="sampleNumberInput" onchange="cx(this.value)"
								value="<s:property value=" sampleOrder.heparinTube "/>" />
							<!--条形码-->
							<p class="sampleNumberDivtow">条形码</p>
							<input class="sampleNumberInputTow" id="sampleBarCode" readonly="readonly" />
							<button class="sampleNumberbutton" onclick="saveNumSampleOrder()">
								<fmt:message key="biolims.common.numVesselsconfirm" />
							</button>
						</div>
						<table
							class="table table-hover table-striped table-bordered table-condensed"
							id="sampleInfoTable" style="font-size: 14px;">
						</table>
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="button" class="btn btn-primary" id="pre">
							<fmt:message key="biolims.common.back" />
						</button>
						<button type="button" class="btn btn-primary" id="next">
							<fmt:message key="biolims.common.nextStep" />
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" id="lan" value="${sessionScope.lan}">
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/system/sample/sampleOrderMainEdit.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/system/sample/sampleOrderMainItemTable.js"></script>
</body>
</html>