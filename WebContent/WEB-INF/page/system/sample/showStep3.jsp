<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<title><fmt:message key="biolims.common.noTitleDocuments" /></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style>
</style>
</head>
<body>
	<div class="HideShowPanel">
		<ul class="timeline">
			<li class="time-label" id="parentWrap"><span class="bg-red">
			</span></li>
			<script type="text/html" id="template">
									{{each list}}
									<li>
										<i class="fa fa-hourglass-1 bg-blue"></i>

										<div class="timeline-item">
											<span class="time"><i class="fa fa-clock-o"></i></span>

											<h3 class="timeline-header"><a href="####">{{$value.stageName}}</a></h3>

											<div class="timeline-body table-responsive">
												<table class="table table-hover" style="margin-bottom: 0;">
													<tr>
														<td><fmt:message key="biolims.common.executionListId"/> ：{{$value.taskId}}</td>
														{{if $value.acceptUser}}
														<td><fmt:message key="biolims.common.testUserName"/>: {{$value.acceptUser.name}}</td>
														{{else}}
														<td><fmt:message key="biolims.common.testUserName"/> : </td>
														{{/if}}
														<td><fmt:message key="biolims.common.startTime"/> : {{$value.startDate}}</td>
													</tr>
													<tr>
														<td><fmt:message key="biolims.common.endTime"/> : {{$value.endDate}}</td>
														<td></td>
														<td><button class="viewSubTable btn btn-warning btn-xs" taskId={{$value.taskId}}><fmt:message key="biolims.common.viewProduct"/>  <i class="glyphicon glyphicon-plus"></i></button></td>
													</tr>
													<tr class="subTable hide">
														<td colspan="3">
															<table class="table table-bordered table-condensed" style="font-size: 12px;"></table>
														</td>
													</tr>
												</table>
											</div>
										</div>
									</li>

									{{/each}}
			</script>
			<li><i class="fa fa-hourglass-3  bg-gray"></i></li>
		</ul>
	</div>
	<input type="hidden" id="sampleOrder_id" value="${requestScope.id}">
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script src="${ctx}/lims/plugins/template/template.js"
		type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript"
		src="${ctx}/js/system/sample/taskKanbanEditItem.js"></script>
</body>
</html>