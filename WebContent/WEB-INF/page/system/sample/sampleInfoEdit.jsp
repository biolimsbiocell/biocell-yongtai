<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<head>
		<title></title>
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
		<style>
			.subTable .tablebtns,.subTable .col-sm-2,.subTable .col-sm-5{
				display: none !important;
			}
		</style>
	</head>

	<body>
		<!--toolbar按钮组-->
		<div>
			<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
		</div>
		<div>
			<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
		</div>
		<div style="height: 14px"></div>
		<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
		<div class="container-fluid" style="margin-top: 46px">
			<!--Form表单-->
			<div class="col-xs-12" style="padding: 0px">
				<div class="box box-info box-solid">
					<div class="box-header with-border">
						<i class="fa fa-bell-o"></i>
						<h3 class="box-title">
						<i class="glyphicon glyphicon-pencil"></i><fmt:message key="biolims.common.sampleOfMasterData"/>
					</h3>

						<div class="box-tools pull-right" style="display: none;">
							<button type="button" class="btn btn-box-tool" id="tableRefresh" onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Action <span class="caret"></span>
							</button>
								<ul class="dropdown-menu">
									<li>
										<a href="####" onclick="$('.buttons-print').click();">
											<fmt:message key="biolims.common.print" />
										</a>
									</li>
									<li>
										<a href="#####" onclick="$('.buttons-copy').click();">
											<fmt:message key="biolims.common.copyData" />
										</a>
									</li>
									<li>
										<a href="####" onclick="$('.buttons-excel').click();">Excel</a>
									</li>
									<li>
										<a href="####" onclick="$('.buttons-csv').click();">CSV</a>
									</li>
									<li role="separator" class="divider"></li>
									<li>
										<a href="####" onclick="itemFixedCol(2)">
											<fmt:message key="biolims.common.lock2Col" />
										</a>
									</li>
									<li>
										<a href="####" id="unfixde" onclick="unfixde()">
											<fmt:message key="biolims.common.cancellock2Col" />
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="box-body ipadmini">
						<!--进度条-->
						<div id="wizard" class="form_wizard wizard_horizontal">
							<ul class="wizard_steps">
								<li>
									<a class="done step"> <span class="step_no">1</span>
										<span class="step_descr"> <small><fmt:message key="biolims.common.sampleBasicInformation"/></small>
								</span>
									</a>
								</li>
								<%-- <li>
									<a class="done step"> <span class="step_no">2</span>
										<span class="step_descr"> <small>样本状态</small></span>
									</a>
								</li> --%>
								<li>
									<a class="done step"> <span class="step_no">2</span>
										<span class="step_descr"> <small><fmt:message key="biolims.common.sampleState"/></small></span>
									</a>
								</li>
								<li>
									<a class="done step"> <span class="step_no">3</span>
										<span class="step_descr"> <small><fmt:message key="biolims.common.inventoryInformation"/></small></span>
									</a>
								</li>
								<li>
									<a class="done step"> <span class="step_no">4</span>
										<span class="step_descr"> <small>样本接收信息</small></span>
									</a>
								</li>
								<li>
									<a class="done step"> <span class="step_no">5</span>
										<span class="step_descr"> <small>过程细胞</small></span>
									</a>
								</li>
								<li>
									<a class="done step"> <span class="step_no">6</span>
										<span class="step_descr"> <small>结果细胞</small></span>
									</a>
								</li>
								<li>
									<a class="done step"> <span class="step_no">7</span>
										<span class="step_descr"> <small>质检内容</small></span>
									</a>
								</li> 
							</ul>
						</div>
						<!--form表单-->
						<div class="HideShowPanel">
							<form name="form1" class="layui-form" id="form1" method="post">
								<input type="hidden" id="bpmTaskId" value="<%=request.getParameter(" bpmTaskId ")%>" /> <br>
								<input type="hidden" size="20" maxlength="25" readonly="readonly" id="sampleInfo_sampleStyle" name="sampleInfo.sampleStyle" class="text input readonlytrue" value="<s:property value=" sampleInfo.sampleStyle "/>" />
								<input type="hidden" size="20" maxlength="25" readonly="readonly" id="sampleInfo_reportDate" name="sampleInfo.reportDate" class="text input readonlytrue" value="<s:property value=" sampleInfo.reportDate "/>" />
								<input type="hidden" size="20" maxlength="25" Class="Wdate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" id="sampleInfo_sendReportDate" name="sampleInfo.sendReportDate" value="<s:date name = " sampleInfo.sendReportDate "  format="yyyy-MM-dd "/>" />
								<input type=hidden id="sampleInfo_sampleType_id" name="sampleInfo.sampleType.id" value="<s:property value=" sampleInfo.sampleType.id "/>">
								<input type="hidden" size="20" maxlength="25" id="sampleInfo_id" name="sampleInfo.id" title="<fmt:message key=" biolims.common.sampleCode "/>" value="<s:property value=" sampleInfo.id " />"/>
								<div class="row">
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon">关联订单号</span>
											<input type="text" size="50" maxlength="127" id="sampleInfo_orderNum" name="sampleOrder-id" changelog="<s:property value=" sampleInfo.sampleOrder.id "/>"  class="form-control readonlytrue" readonly="readOnly" value="<s:property value=" sampleInfo.sampleOrder.id "/>" />
											<span class="input-group-btn">
												<button class="btn btn-info" type="button"  onclick="showSampleOrderInfo()">
													<i class="glyphicon glyphicon-search"></i>
												</button>
											</span>
										</div>
									</div>
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon">批次号<img class='requiredimage' src='${ctx}/images/required.gif' />  </span>
											<input type="text" size="50" maxlength="127" id="sampleInfo_code"  readonly="readOnly" class="form-control readonlytrue" value="<s:property value=" sampleInfo.sampleOrder.barcode" />" />
										</div>
									</div>
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon">姓名</span>
											<input type="text" size="50" maxlength="127" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.name "  />" />
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon">性别</span>
											<input type="text" size="50" maxlength="127" class="form-control"  
											<c:if test='${ sampleInfo.sampleOrder.gender eq "1"}'>value="男"</c:if> 
											<c:if test='${ sampleInfo.sampleOrder.gender eq "0"}'>value="女"</c:if>
											<c:if test='${ sampleInfo.sampleOrder.gender eq "3"}'>value="未知"</c:if>  />
										</div>
									</div>
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon">年龄</span>
											<input type="text" size="50" maxlength="127" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.age "  />" />
										</div>
									</div>
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon">电子病历号</span>
											<input type="text" size="50" maxlength="127" id="medicalNumber" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.medicalNumber "  />" />
											<span class="input-group-btn">
												<button class="btn btn-info" type="button"  onclick="showMedicalNumberInfo()">
													<i class="glyphicon glyphicon-search"></i>
												</button>
											</span>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon">产品名称</span>
											<input type="text" size="50" maxlength="127" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.productName "  />" />
										</div>
									</div>
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon">采血轮次</span>
											<input type="text" size="50" maxlength="127" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.round "  />" />
										</div>
									</div>
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon">筛选号</span>
											<input type="text" size="50" maxlength="127" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.filtrateCode "  />" />
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon">随机号</span>
											<input type="text" size="50" maxlength="127" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.randomCode "  />" />
										</div>
									</div>
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon">CCOI</span>
											<input type="text" size="50" maxlength="127" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.ccoi "  />" />
										</div>
									</div>
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon">医疗机构</span>
											<input type="text" size="50" maxlength="127" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.crmCustomer.name "  />" />
										</div>
									</div>	
								</div>
								<div class="row">
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon">预计采血时间</span>
											<input type="text" size="50" maxlength="127" class="form-control" value="<s:date name=" sampleInfo.sampleOrder.drawBloodTime " format="yyyy-MM-dd HH:mm" />" />
										</div>
									</div>
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon">审核人</span>
											<input type="text" size="50" maxlength="127" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.confirmUser.name "  />" />
										</div>
									</div>
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon">审核时间</span>
											<input type="text" size="50" maxlength="127" class="form-control" value="<s:date name=" sampleInfo.sampleOrder.confirmDate " format="yyyy-MM-dd" />"/>
										</div>
									</div>	
								</div>
								<!-- 
                                	作者：offline
                                	时间：2018-03-19
                                	描述：自定义字段
                                -->
								<!-- <div id="fieldItemDiv"></div>  -->

								<br>
								<input type="hidden" name="storageReagentBuySerialJson" id="storageReagentBuySerialJson" value="" />

								<input type="hidden" id="id_parent_hidden" value="<s:property value=" sampleInfo.id "/>" />
							</form>·
						<ul class="timeline">
								<li class="time-label" id="parentWrap">
									<span class="bg-red"> </span>
								</li>
								<script type="text/html" id="template">
									{{each list}}
									<li>
										<i class="fa fa-hourglass-1 bg-blue"></i>

										<div class="timeline-item">
											<span class="time"><i class="fa fa-clock-o"></i></span>

											<h3 class="timeline-header"><a href="####">{{$value.stageName}}</a></h3>

											<div class="timeline-body table-responsive">
												<table class="table table-hover" style="margin-bottom: 0;">
													<tr>
														<td><fmt:message key="biolims.common.executionListId"/> ：{{$value.taskId}}</td>
														{{if $value.acceptUser}}
														<td><fmt:message key="biolims.common.testUserName"/>: {{$value.acceptUser.name}}</td>
														{{else}}
														<td><fmt:message key="biolims.common.testUserName"/> : </td>
														{{/if}}
														<td><fmt:message key="biolims.common.startTime"/> : {{$value.startDate}}</td>
													</tr>
													<tr>
														<td><fmt:message key="biolims.common.endTime"/> : {{$value.endDate}}</td>
														<td></td>
														<td><button class="viewSubTable btn btn-warning btn-xs" taskId={{$value.taskId}}><fmt:message key="biolims.common.viewProduct"/>  <i class="glyphicon glyphicon-plus"></i></button></td>
													</tr>
													<tr class="subTable hide">
														<td colspan="3">
															<table class="table table-bordered table-condensed" style="font-size: 12px;"></table>
														</td>
													</tr>
												</table>
											</div>
										</div>
									</li>

									{{/each}}
								</script>
								<li>
									<i class="fa fa-hourglass-3  bg-gray"></i>
								</li>
							</ul>
						</div>
						<!-- <div class="HideShowPanel" style="display: none;">
							<div class="box box-info">
							<div class="box-header">
								<h3 class="box-title">样本状态预览</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
									<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
								</div>
							</div>
							 /.box-header
							<div class="box-body ipadmini">
							

							</div>
						</div>

						</div> -->
						<!--table表格-->
						<div class="HideShowPanel" style="display: none;">
							<table class="table table-hover table-striped table-bordered table-condensed" id="sampleStateGrid" style="font-size: 14px;">
							</table>
						</div>
						<div class="HideShowPanel" style="display: none;">
							<table class="table table-hover table-striped table-bordered table-condensed" id="sampleInItemInfoGrid" style="font-size: 14px;">
							</table>
						</div>
						<div class="HideShowPanel" style="display: none;">
							<table class="table table-hover table-striped table-bordered table-condensed" id="sampleReceiveShow" style="font-size: 14px;">
							</table>
						</div>
						<div class="HideShowPanel" style="display: none;">
							<table class="table table-hover table-striped table-bordered table-condensed" id="cellPassageItemShow" style="font-size: 14px;">
							</table>
						</div>
						<div class="HideShowPanel" style="display: none;">
							<table class="table table-hover table-striped table-bordered table-condensed" id="cellPassageResultShow" style="font-size: 14px;">
							</table>
						</div>
						<div class="HideShowPanel" style="display: none;">
							<table class="table table-hover table-striped table-bordered table-condensed" id="qualityTestShow" style="font-size: 14px;">
							</table>
						</div>
					</div>
					<!-- <div class="box-footer">
						<div class="pull-right">
							<button type="button" class="btn btn-primary" id="pre">上一步</button>
							<button type="button" class="btn btn-primary" id="next">下一步</button>
						</div>
					</div> -->
				</div>
			</div>
		</div>
		<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
		<script src="${ctx}/lims/plugins/template/template.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript" src="${ctx}/js/system/sample/sampleMainEdit.js"></script>
		<script type="text/javascript" src="${ctx}/js/system/sample/sampleMainEditItem.js"></script>
		<script type="text/javascript" src="${ctx}/js/system/sample/sampleState.js"></script>
		<script type="text/javascript" src="${ctx}/js/system/sample/sampleInItem.js"></script>
		<!-- 样本接收子表 -->
		<script type="text/javascript" src="${ctx}/js/system/sample/sampleReceiveShow.js"></script>
		<!-- 细胞生产明细 -->
		<script type="text/javascript" src="${ctx}/js/system/sample/cellPassageItemShow.js"></script>
		<!-- 细胞生产结果 -->
		<script type="text/javascript" src="${ctx}/js/system/sample/cellPassageResultShow.js"></script>
		<!-- 质检内容 -->
		<script type="text/javascript" src="${ctx}/js/system/sample/qualityTestShow.js"></script>
	</body>
</html>