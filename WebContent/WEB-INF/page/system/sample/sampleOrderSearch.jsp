﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<!-- Font Awesome -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
		<style type="text/css">
			.icheckbox_square-blue {
				margin-right: 10px;
			}
			
			.label {
				float: right;
			}
			
			.panel-title {
				font-size: 14px;
			}
			
			#main th,
			#main td {
				white-space: nowrap !important;
			}
			
			.glyphicon {
				margin-right: 3px;
			}
			
			.tablebtns {
				float: right;
			}
			
			.dt-buttons {
				margin: 0;
				float: right;
			}
			
			.dt-button-collection {
				font-size: 12px;
				max-height: 260px;
				overflow: auto;
			}
			
			.input-group-addon {
				font-size: 12px;
			}
		</style>
	</head>

	<body style="height:94%">
		<!--<div>
			<%@ include file="/WEB-INF/page/include/newToolbar.jsp"%>
		</div>-->
		<div class="content-wrapper" id="content" style="margin-left: 0px;">
			<section class="content">
				<div class="row">
					<!--表格-->
					<div class="col-xs-12 col-md-12">
						<div class="box box-info box-solid" id="box">
							<div class="box-header with-border">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title"><fmt:message key="biolims.order.customer" />
									<small style="color: #fff;"><fmt:message key="biolims.order.customerInquiry" />
										</small></h3>
							</div>
							<div class="box-body">
								<!--查询条件-->
								<div class="col-xs-4">
									<div class="box box-primary">
										<div class="box-header with-border">
											<i class="fa fa-leaf"></i>
											<h3 class="box-title"><fmt:message key="biolims.common.retrieve" /></h3>
										</div>
										<div class="box-body" id="searchOption">
											
										</div>
										<div class="box-footer">
											<!--其他检索条件-->
											<div id="searchOther">

											</div>
										</div>
									</div>
								</div>
								<div class="col-xs-8">
									<div class="row">
										<div class="col-xs-6">
											<div class="box box-warning">
												<div class="box-header with-border">
													<i class="fa fa-leaf"></i>
													<h3 class="box-title"><fmt:message key="biolims.order.barGraph" /></h3>
													<div class="box-tools">
														<div class="input-group" style="width:102px;float:right;">
															<span class="input-group-btn">
       															 <button class="btn btn-info btn-sm" type="button" id="subYear">-</button>
      														</span>

															<input type="text" class="form-control input-sm" id="currentYear" disabled>
															<span class="input-group-btn">
       															 <button class="btn btn-info btn-sm" type="button" id="addYear">+</button>
															</span>
														</div>

													</div>
												</div>
												<div class="box-body">
													<div id="sampleBar" style="height:250px;"></div>
												</div>
											</div>
										</div>
										<div class="col-xs-6">
											<div class="box box-warning">
												<div class="box-header with-border">
													<i class="fa fa-leaf"></i>
													<h3 class="box-title"><fmt:message key="biolims.order.ieChart" /></h3>
												</div>
												<div class="box-body">
													<div id="samplePie" style="height:250px;"></div>
												</div>
											</div>
										</div>
									</div>

									<div class="box box-info">
										<div class="box-header with-border">
											<i class="fa fa-leaf"></i>
											<h3 class="box-title"><fmt:message key="biolims.order.surface" /></h3>
										</div>
										<div class="box-body ipadmini">
											<table class="table table-hover table-striped table-bordered table-condensed" id="main" style="font-size:12px;">
											</table>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>

				</div>

			</section>
		</div>
		<script src="${ctx}/lims/plugins/chartjs/echarts.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript" src="${ctx}/javascript/common/searchExtend.js"></script>
		<script type="text/javascript" src="${ctx}/js/system/sample/sampleOrderSearch.js"></script>
	</body>

</html>