<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>

<link rel="stylesheet" type="text/css"
	href="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/treegrid.css"
	rel="stylesheet" />
<script type="text/javascript"
	src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGridSorter.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGridColumnResizer.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGridNodeUI.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGridLoader.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGridColumns.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGrid.js"></script>
<script type="text/javascript" src="${ctx}/js/system/sample/sampleHistorySelectTree.js"></script>
<script type="text/javascript">
		function setvalue(id,name){
		 window.parent.setYongYaoFun(id,name);

		}
</script>


</head>
<style type="text/css"> 
    .complete .x-tree-node-anchor span {
        text-decoration: line-through;
        color: #777;
    }
</style>
<body>
<form id="departmentSelect" name="form1">
	<input type="hidden" id="id" value="" />
	<input type="hidden" id="selectId" value="" />
	<input type="hidden" id="leaf" value="" />
	<input type="hidden" id="tf" value="" />
	<input type="hidden" id="upId" value="<%=request.getParameter("upId")%>">
	<input type="hidden" id="selectIds" value="<%=request.getParameter("selectId")%>">
	<input type="hidden" id="historyTreePath" value="${path}" />
	
</form>




<div  class="mainlistclass" id="markup"></div>

</body>

</html>