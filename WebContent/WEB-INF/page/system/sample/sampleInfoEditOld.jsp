<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML>
<html>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<title><fmt:message key="biolims.common.sampleOfMasterData"/></title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>	

</head>

<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp" %> 

<script type="text/javascript" src="${ctx}/js/system/sample/sampleMainEdit.js"></script>

 <div id="maintab" style="margin: 0 0 0 0 "></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden"  id="handlemethod"  value="${requestScope.handlemethod}">
            <form name="form1"  id="form1"  method="post"  >
			<table class="frame-table">
			
			
			<tr>
					
                  <input   type="hidden"  size="20" maxlength="25" id="sampleInfo_id" name="sampleInfo.id"  title="<fmt:message key="biolims.common.sampleCode"/>"  value="<s:property value="sampleInfo.id" />"/>
               		
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.sampleCode"/></td>
               	 	<td class="requiredcolumn"  nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                  <input   type="text" readonly="readonly"  size="20" maxlength="25" id="sampleInfo_code" name="sampleInfo.code"   class="text input readonlytrue"  title="<fmt:message key="biolims.common.sampleCode"/>"  value="<s:property value="sampleInfo.code" />"/>
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.sampleType"/></td>
               	 	<td class="requiredcolumn"  nowrap width="10px"  ><img class='requiredimage'  src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="hidden"  size="20"  maxlength="25" readonly="readonly"    id="sampleInfo_sampleStyle"  name="sampleInfo.sampleStyle" class="text input readonlytrue"    value= "<s:property value="sampleInfo.sampleStyle"/>"  />
                   	<input type="hidden"  size="20"  maxlength="25" readonly="readonly"    id="sampleInfo_reportDate"  name="sampleInfo.reportDate" class="text input readonlytrue"    value= "<s:property value="sampleInfo.reportDate"/>"  />
                   	<input type="hidden"  size="20"  maxlength="25"   Class="Wdate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  id="sampleInfo_sendReportDate"  name="sampleInfo.sendReportDate"     value= "<s:date name = "sampleInfo.sendReportDate"  format="yyyy-MM-dd"/>"  />
                   	<input type =hidden  id="sampleInfo_sampleType_id" name="sampleInfo.sampleType.id"   value="<s:property value="sampleInfo.sampleType.id"/>">
                   	<input  type="text" size="20" maxlength="50"  readonly="readonly"  id="sampleInfo_sampleType_name" name="sampleInfo.sampleType.name" class="text input readonlytrue"  title="<fmt:message key="biolims.common.sampleType"/>"  value="<s:property value="sampleInfo.sampleType.name"/>" />
                   	</td>
                   	
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.subjectsName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25"  id="sampleInfo_patientName"  name="sampleInfo.patientName"  title="<fmt:message key="biolims.common.subjectsName"/>"    value="<s:property value="sampleInfo.patientName"  />"   />
                   	</td>
                   
			
			
               	 	
			</tr>
			<tr>
			
				<td class="label-title" ><fmt:message key="biolims.common.sampleNameSpecies"/></td>
               	   	 	<td class="requiredcolumn" nowrap width="10px" ></td>    
                   	<td align="left"  >
                  <input   type="text" size="20" maxlength="25" id="sampleInfo_species" name="sampleInfo.species"  title="<fmt:message key="biolims.common.sampleNameSpecies"/>"  value="<s:property value="sampleInfo.species"/>"/>
                   	</td>
			
				<td class="label-title"><fmt:message key="biolims.common.outCode"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25"  id="sampleInfo_idCard"  name="sampleInfo.idCard"  title="<fmt:message key="biolims.common.externalSampleNumber"/>"    value="<s:property value="sampleInfo.idCard"  />"   />
                   	</td>
                   	
                   	
                  <td class="label-title" ><fmt:message key="biolims.common.receiveTheSampleQuantity"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25"  id="sampleInfo_sampleNum"  name="sampleInfo.sampleNum"  title="<fmt:message key="biolims.common.receiveTheSampleQuantity"/>"    value="<s:property value="sampleInfo.sampleNum"  />"   />
                   	</td> 	
			
			
			
			</tr>
			
			
			
			<tr>
			 <td class="label-title" ><fmt:message key="biolims.common.storingSamples"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25"  id="sampleInfo_location"  name="sampleInfo.location"  title="<fmt:message key="biolims.common.storingSamples"/>"    value="<s:property value="sampleInfo.location"  />"   />
                   	</td> 	
			
               	 	
					<td class="label-title" ><fmt:message key="biolims.common.sampleRelationship"/></td>
		               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
		                   	<td align="left"  >
		                  	 <input   type="text" size="20"   id="sampleInfo_personShip_name" name="sampleInfo.personShip.name" value="<s:property value="sampleInfo.personShip.name"/>"  />
		 					<input type="hidden" id="sampleInfo_personShip_id" name="sampleInfo.personShip.id"  value="<s:property value="sampleInfo.personShip.id"/>" > 
		 					<img alt='<fmt:message key="biolims.common.select"/>' id='showPersonType' src='${ctx}/images/img_lookup.gif' onClick="FamilyPatientShipFun()"	class='detail' />  
			
               	 
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.detectionItems"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="hidden"   id="sampleInfo_productId"  name="sampleInfo.productId"  value="<s:property value="sampleInfo.productId"/>" />
                   	<input  type="text"  size="20"  maxlength="200"  id="sampleInfo_productName" name="sampleInfo.productName" title="<fmt:message key="biolims.common.detectionItemsName"/>"  value="<s:property value="sampleInfo.productName"/>" />
                   	</td>
			</tr>
			<tr>
                   
                   		<input type="hidden" id="sampleInfo_state"  name="sampleInfo.state"  value= "<s:property value="sampleInfo.state"  />"  />
			
                   		<input  type="hidden"   id="sampleInfo_stateName" name="sampleInfo.stateName" value="<s:property value="sampleInfo.stateName"/>" />
                   		
                   		<input  type="hidden"   id="unusual_id" name="unusual.id" value="<s:property value="unusual.id"/>" />
                   		
						<td class="label-title" ><fmt:message key="biolims.common.exceptionTypes"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input  type="text"  size="20"  maxlength="200" readonly="readonly"   class="text input readonlytrue"   title="<fmt:message key="biolims.common.exceptionTypes"/>"  value="<s:property value="unusual.name"/>" />
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.orderCode"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text"  size="20"    id="sampleInfo_orderNum"  name="sampleInfo.orderNum"  value= "<s:property value="sampleInfo.orderNum"  />"  />
                   		<input type="button" id = "btn_sampleOrder"     size="5"  value="<fmt:message key="biolims.common.find"/>" >
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.medicalRecordCode"/></td>
	               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
	                   	<td align="left"  >
	                   	  	<input type="text"  size="20"  maxlength="25"  id="sampleInfo_patientId"  name="sampleInfo.patientId"  title="<fmt:message key="biolims.common.medicalRecordCode"/>"   value= "<s:property value="sampleInfo.patientId"  />"  />
	             			 <input type="button" id = "btn_patientId"     size="5"  value="<fmt:message key="biolims.common.find"/>" >
	              </td>
			
			</tr>
			
			
			<tr>
			
				
			<td class="label-title" ><fmt:message key="biolims.common.receivingDate"/></td>
	               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
	                   	<td align="left"  >
	                   	  
	                   	  	<input type="text"  size="20"  maxlength="25"    title="<fmt:message key="biolims.common.receivingDate"/>" class="text input readonlytrue"    value= "<s:date name = "sampleInfo.sampleReceive.acceptDate"  format="yyyy-MM-dd"/>"  />
	              </td>
	              
	              	<td class="label-title" ><fmt:message key="biolims.common.shouldTheReportDate"/></td>
	               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
	                   	<td align="left"  >
	                   	  
	                   	  	<input type="text" readonly="readonly"   class="text input readonlytrue"  size="20"  maxlength="25"  title="<fmt:message key="biolims.common.shouldTheReportDate"/>"   value= "<s:property value="sampleInfo.sampleOrder.reportDate"  />"  />
	              </td>
	              <td class="label-title" ><fmt:message key="biolims.common.note"/></td>
	               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
	                   	<td align="left"  >
	                   	  
	                   	  	<input type="text"  size="20"  maxlength="25"  id="sampleInfo_note"  name="sampleInfo.note"  title="<fmt:message key="biolims.common.note"/>"   value= "<s:property value="sampleInfo.note"  />"  />
	              </td>
			</tr>

			
			
            <tr>
            	<td class="label-title" ><fmt:message key="biolims.common.samplingDate"/></td>
	               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
	                   	<td align="left"  >
	                   	  
	                   	  	<input type="text"  size="20"  maxlength="25" readonly="readonly"   id="sampleInfo_sampleTime"  name="sampleInfo.sampleTime"   title="<fmt:message key="biolims.common.samplingDate"/>" class="text input readonlytrue"    value= "<s:property value="sampleInfo.sampleTime"/>"  />
	              </td>
            
			</tr>
			<tr class="tr1">
						<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showacceptUser = Ext.get('showacceptUser');
showacceptUser.on('click', 
 function UserFun(){
var win = Ext.getCmp('UserFun');
if (win) {win.close();}
var UserFun= new Ext.Window({
id:'UserFun',modal:true,title:'<fmt:message key="biolims.common.clinical"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/crm/project/showProjectSelectList.action?flag=UserFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 UserFun.close(); }  }]  });     UserFun.show(); }
);
});
 function setUserFun(rec){
document.getElementById('sampleInfo_project').value=rec.get('id');
						document.getElementById('sampleInfo_project_name').value=rec.get('name');
						document.getElementById('sampleInfo_crmCustomer_id').value=rec.get('mainProject-id');
						document.getElementById('sampleInfo_crmCustomer_name').value=rec.get('mainProject-name');
						document.getElementById('sampleInfo_crmDoctor_id').value=rec.get('crmDoctor-id');
						document.getElementById('sampleInfo_crmDoctor_name').value=rec.get('crmDoctor-name');
var win = Ext.getCmp('UserFun')
if(win){win.close();}
}
</script>

	              <%--  	 	<td class="label-title" >科研项目编号</td>
	               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
	                   	<td align="left"  >
	 					 	</td> --%>
	                   	<td class="label-title" ><fmt:message key="biolims.common.scientificResearchProject"/></td>
	               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
	                   	<td align="left"  >
	                   		<input type="hidden" id="sampleInfo_project" name="sampleInfo.project.id"  value="<s:property value="sampleInfo.project.id"/>" readonly = "readOnly" > 
	 						<input type="text" size="20" readonly="readOnly"  id="sampleInfo_project_name"  name="sampleInfo.project.name" value="<s:property value="sampleInfo.project.name"/>" class="text input" readonly="readOnly"  />
	 						 <img alt='<fmt:message key="biolims.common.chooseTheScientificResearchProject"/>' id='showacceptUser'  src='${ctx}/images/img_lookup.gif' 	class='detail'    />                		
	                   	</td>
	                   	
	                   	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var mainProjectBtn = Ext.get('mainProjectBtn');
mainProjectBtn.on('click', 
 function CrmCustomerFun(){
var win = Ext.getCmp('CrmCustomerFun');
if (win) {win.close();}
var CrmCustomerFun= new Ext.Window({
id:'CrmCustomerFun',modal:true,title:'<fmt:message key="biolims.common.selectTheCustomerID"/>',layout:'fit',width:document.body.clientWidth/1.5,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/crm/customer/customer/crmCustomerSelect.action?flag=CrmCustomerFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 CrmCustomerFun.close(); }  }]  });     CrmCustomerFun.show(); }
);
});
 function setCrmCustomerFun(rec){
document.getElementById('sampleInfo_crmCustomer_id').value=rec.get('id');
						document.getElementById('sampleInfo_crmCustomer_name').value=rec.get('name');
var win = Ext.getCmp('CrmCustomerFun')
if(win){win.close();}
}
</script>

						<td class="label-title" ><fmt:message key="biolims.common.selectTheCustomer"/> </td>
		               	 	<td class="requiredcolumn" nowrap width="10px" > </td>            	 	
		                   	<td align="left"  >
		                   	<input type="text" size="35" maxlength="25" id="sampleInfo_crmCustomer_name"
		                   	 name="sampleInfo.crmCustomer.name" title="<fmt:message key="biolims.common.selectTheCustomer"/>  " readonly="readOnly"
								value="<s:property value="sampleInfo.crmCustomer.name"/>"
		                   	  />
		                   	  <input type="hidden" id="sampleInfo_crmCustomer_id" name="sampleInfo.crmCustomer.id" readonly="readOnly"  value="<s:property value="sampleInfo.crmCustomer.id"/>" >
		                   	  <img alt='<fmt:message key="biolims.common.selectTheCustomer"/> ' id='mainProjectBtn' src='${ctx}/images/img_lookup.gif' name='mainProjectBtn' class='detail' /> 
		                 </td>						
										
					 <script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showDoctor = Ext.get('showDoctor');
showDoctor.on('click', 
 function DicProbeFun(){
var win = Ext.getCmp('DicProbeFun');
if (win) {win.close();}
var DicProbeFun= new Ext.Window({
id:'DicProbeFun',modal:true,title:'<fmt:message key="biolims.common.selectTheDoctor"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/crm/doctor/crmPatientSelect.action?flag=DicProbeFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 DicProbeFun.close(); }  }]  });     DicProbeFun.show(); }
);
});
 function setDicProbeFun(rec){
document.getElementById('sampleInfo_crmDoctor_id').value=rec.get('id');
							document.getElementById('sampleInfo_crmDoctor_name').value=rec.get('name');
var win = Ext.getCmp('DicProbeFun')
if(win){win.close();}
}
</script>

		               	 	<td class="label-title" ><fmt:message key="biolims.common.selectTheDoctor"/></td>
		               	 	<td class="requiredcolumn" nowrap width="10px" > </td>            	 	
		                   	<td align="left"  >
		                   	<input type="text" size="20" maxlength="25" id="sampleInfo_crmDoctor_name"
		                   	 name="sampleInfo.crmDoctor.name" title="<fmt:message key="biolims.common.attendingPhysician"/>" readonly="readOnly"
								value="<s:property value="sampleInfo.crmDoctor.name"/>"
		                   	  />
		                   	  <input type="hidden" id="sampleInfo_crmDoctor_id" name="sampleInfo.crmDoctor.id"  readonly="readOnly" value="<s:property value="sampleInfo.crmDoctor.id"/>" >
		                   	  <img alt='<fmt:message key="biolims.common.selectTheDoctor"/>' id='showDoctor' src='${ctx}/images/img_lookup.gif' 	class='detail'    />   
		                 </td>
				</tr>
			

            </table>

            <input type="hidden"  id="id_parent_hidden" value="<s:property value="sampleInfo.id"/>" />
            <div id="tabs">
				<ul>
					<li><a href="#sampleStatePage"><fmt:message key="biolims.common.sampleState"/></a></li>
					<li><a href="#sampleResultPage"><fmt:message key="biolims.common.experimentalResults"/></a></li>
					<li><a href="#sampleInPage"><fmt:message key="biolims.common.inventoryInformation"/></a></li>
				</ul>
                <div id="sampleStatePage"  style="width:100%;"></div>
                <div id="sampleResultPage"  style="width:100%;height:100%">
                		<div id="sampleDnaInfoPage"  style="width:100%;"></div>
                		<div id="sampleUfTaskResultPage"  style="width:100%;"></div>
                		<div id="tabs1">
	                		<ul>
								<li><a href="#sampleWkInfoPage"><fmt:message key="biolims.common.fFPEResultsBloodLibrary"/></a></li>
								<li><a href="#sampleWkInfoPageCtDNA"><fmt:message key="biolims.common.ctDNAResultLibrary"/></a></li>
								<li><a href="#sampleWkInfoPagemRNA"><fmt:message key="biolims.common.mRNAResultLibrary"/></a></li>
								<li><a href="#sampleWkInfoPagerRNA"><fmt:message key="biolims.common.rRNAResultLibrary"/></a></li>
							</ul>
	                		<div id="sampleWkInfoPage"  style="width:100%;"></div>
	                		<div id="sampleWkInfoPageCtDNA"  style="width:100%;"></div>
	                		<div id="sampleWkInfoPagemRNA"  style="width:100%;"></div>
	                		<div id="sampleWkInfoPagerRNA"  style="width:100%;"></div>
                		</div>
                		<div id="samplePoolingInfoPage"  style="width:100%;"></div>
                		<div id="sampleSequenveIngInfoPage"  style="width:100%;"></div>
                		<div id="sampleDesequencIngInfoPage"  style="width:100%;"></div>
                		<div id="samplePcrTaskResultPage"  style="width:100%;"></div>
                		<div id="sampleQc2100ResultPage"  style="width:100%;"></div>
                		<div id="sampleQpcrResultPage"  style="width:100%;"></div>
                		<div id="sampleDnaTaskResultPage"  style="width:100%;"></div>
                		<div id="sampleCheckServiceWkResultPage"  style="width:100%;"></div>
                		<div id="sampleCheckServiceResultPage"  style="width:100%;"></div>
                		<div id="sampleCFdnaResultPage"  style="width:100%;"></div>
                		<div id="sampleOtherResultPage"  style="width:100%;"></div>
                </div>
                 <div id="sampleInPage"  style="width:100%;"></div>
               
			</div>
            </form>
            
            <div id="tabs">
            <ul>
           	</ul> 
           	
			</div>
        	</div>
	</body>
	</html>
