<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>

<script type="text/javascript"
	src="${ctx}/js/system/sample/sampleMain.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
	<div id="jstj" style="display: none">
		<input type="hidden" id="selectId" /> <input type="hidden"
			id="extJsonDataString" name="extJsonDataString">
		<table class="frame-table">


			<tr>

				<input type="hidden" size="20" maxlength="25" id="sampleInfo_id"
					name="id" title="<fmt:message key="biolims.common.serialNumber"/>"
					value="<s:property value="sampleInfo.id" />" />


				<td class="label-title"><fmt:message
						key="biolims.common.code" /></td>
				<td class="requiredcolumn" nowrap width="10px"></td>
				<td align="left"><input type="text" searchField="true"
					size="20" maxlength="25" id="sampleInfo_code" name="code"
					title="<fmt:message key="biolims.common.sampleCode"/>"
					value="<s:property value="sampleInfo.code" />" /></td>


				<td class="label-title"><fmt:message
						key="biolims.common.sampleType" /></td>
				<td class="requiredcolumn" nowrap width="10px"></td>
				<td align="left"><input type="text" searchField="true"
					size="20" maxlength="25" id="sampleInfo_sampleType2"
					name="sampleType2"
					title="<fmt:message key="biolims.common.sampleType2"/>"
					value="<s:property value="sampleInfo.sampleType2" />" /></td>


				<%--                	 	<td class="label-title" ><fmt:message key="biolims.common.sampleType"/></td> --%>
				<!--                	 	<td class="requiredcolumn"  nowrap width="10px"  ></td>            	 	 -->
				<!--                    	<td align="left"  > -->

				<%--                    	<input type ="hidden"  id="sampleInfo_sampleType_id" name="sampleType.id"   value="<s:property value="sampleInfo.sampleType.id"/>"> --%>
				<%--                    	<input list="sampleName"  type="text"  searchField="true"  size="20" maxlength="50"  id="sampleInfo_sampleType_name"  title="<fmt:message key="biolims.common.sampleType"/>"  value="<s:property value="sampleInfo.sampleType.name"/>" /> --%>
				<!--                    	</td> -->

				<%-- <td class="label-title" >样本名称/物种</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ></td>            	 	
                   	<td align="left"  >
                  <input   type="text"  searchField="true"  size="20" maxlength="25" id="sampleInfo_name" name="name"  title="样本名称"  value="<s:property value="sampleInfo.name"/>"/>
                   	</td> --%>



			</tr>
			<tr>


				<td class="label-title"><fmt:message
						key="biolims.common.subjects" /></td>
				<td class="requiredcolumn" nowrap width="10px"></td>
				<td align="left"><input type="text" searchField="true"
					size="20" maxlength="25" id="sampleInfo_patientName"
					name="patientName"
					title="<fmt:message key="biolims.common.patientName"/>"
					value="<s:property value="sampleInfo.patientName"  />" /></td>


				<%-- <td class="label-title" >检测项目编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text"  searchField="true"  size="20"  maxlength="25"  id="sampleInfo_productId"  name="productId"  title="检测项目编号"    value="<s:property value="sampleInfo.productId"/>" />
                   	</td> --%>


				<td class="label-title"><fmt:message
						key="biolims.common.detectionItems" /></td>
				<td class="requiredcolumn" nowrap width="10px"></td>
				<td align="left"><input type="text" searchField="true"
					size="20" maxlength="200" id="sampleInfo_productName"
					name="productName"
					title="<fmt:message key="biolims.common.detectionItemsName"/>"
					value="<s:property value="sampleInfo.productName"/>" /></td>
			</tr>
			<tr>
				<%-- <td class="label-title" >状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text"  searchField="true"  size="20"  maxlength="25"  id="sampleInfo_state"  name="state"  title="状态"    value= "<s:property value="sampleInfo.state"  />"  />
                   	</td>--%>


				<td class="label-title"><fmt:message
						key="biolims.common.stateName" /></td>
				<td class="requiredcolumn" nowrap width="10px"></td>
				<td align="left"><input type="text" searchField="true"
					size="20" maxlength="200" id="sampleInfo_stateName"
					name="stateName" title="状态名称"
					value="<s:property value="sampleInfo.stateName"/>" /></td>



				<td class="label-title"><fmt:message
						key="biolims.common.orderCode" /></td>
				<td class="requiredcolumn" nowrap width="10px"></td>
				<td align="left"><input type="text" searchField="true"
					size="20" id="sampleInfo_orderNum" name="orderNum"
					value="<s:property value="sampleInfo.orderNum"  />" /> <!--                    		<input type="button" id = "btn_sampleOrder"     size="5"  value="查" > -->
				</td>




				<%-- 	<td class="label-title" >备注</td>
	               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
	                   	<td align="left"  >
	                   	  
	                   	  	<input type="text"  searchField="true"   size="20"  maxlength="25"  id="sampleInfo_note"  name="note"  title="备注"   value= "<s:property value="sampleInfo.note"  />"  />
	              </td>
			
                
               <g:LayOutWinTag buttonId="showCancerMainType" title="选择肿瘤类别"
				hasHtmlFrame="true"
				html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="cancerType" 
				hasSetFun="true"
 				documentId="sampleInfo_dicType_id"
				documentName="sampleInfo_dicType_name" />
			
               	 	<td class="label-title" >肿瘤类别</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                  	 <input   type="text"  searchField="true"  size="20"   id="sampleInfo_dicType_name" name="dicType.name" value="<s:property value="sampleInfo.dicType.name"/>"  />
 					<input type="hidden" id="sampleInfo_dicType_id" name="dicType.id"  value="<s:property value="sampleInfo.dicType.id"/>" > 
 					<img alt='选择' id='showCancerMainType' src='${ctx}/images/img_lookup.gif' 	class='detail' />     
			   --%>

			</tr>
			<tr>
				<td class="label-title"><fmt:message
						key="biolims.sample.projectName" /></td>
				<td class="requiredcolumn" nowrap width="10px"></td>
				<td align="left">
					<input type="text" searchField="true" size="20" maxlength="50"
					id="sampleInfo_project_name" name="project.name"
					value="<s:property value="sampleInfo.project.name"/>" />
				</td>


				<td class="label-title"><fmt:message
						key="biolims.common.custom" /></td>
				<td class="requiredcolumn" nowrap width="10px"></td>
				<td align="left">
					<%--                    	<input type ="hidden"  id="sampleInfo_crmCustomer_id" name="sampleInfo.crmCustomer.id"   value="<s:property value="sampleInfo.crmCustomer.id"/>"> --%>
					<input type="text" searchField="true" size="20" maxlength="50"
					id="sampleInfo_crmCustomer_name" name="crmCustomer.name"
					value="<s:property value="sampleInfo.crmCustomer.name"/>" />
				</td>


			</tr>



			<tr>
				<td class="label-title"><fmt:message
						key="biolims.sample.crmDoctorName" /></td>
				<td class="requiredcolumn" nowrap width="10px"></td>
				<td align="left">
					<input type="text" searchField="true" size="20" maxlength="50"
					id="sampleInfo_crmDoctor_name" name="crmDoctor.name"
					value="<s:property value="sampleInfo.crmDoctor.name"/>" />
				</td>
				<td class="label-title"><fmt:message
						key="biolims.common.medicalRecordCode" /></td>
				<td class="requiredcolumn" nowrap width="10px"></td>
				<td align="left"><input type="text" searchField="true"
					size="20" maxlength="25" id="sampleInfo_patientId" name="patientId"
					title="<fmt:message key="biolims.common.medicalRecordCode"/>"
					value="<s:property value="sampleInfo.patientId"  />" /> <!-- 	             			 <input type="button" id = "btn_patientId"     size="5"  value="查" > -->
				</td>
			</tr>
		</table>
	</div>
	<div id="bat_sampleMain_div"></div>
	<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value='' />
	</form>

	<div id="bat_patientId_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><span><fmt:message
							key="biolims.common.electronicMedicalRecords" /></span></td>
				<td><input id="patientId" /></td>
			</tr>

		</table>
	</div>

	<div id="bat_project_div" style="display: none">
		<table>
			<tr class="tr1">
				<g:LayOutWinTag buttonId="showacceptUser"
					title='<fmt:message key="biolims.common.clinical"/>'
					hasHtmlFrame="true"
					html="${ctx}/crm/project/showProjectSelectList.action"
					isHasSubmit="false" functionName="UserFun" hasSetFun="true"
					extRec="rec"
					extStr="document.getElementById('sampleInfo_project').value=rec.get('id');
						document.getElementById('sampleInfo_project_name1').value=rec.get('name');
						document.getElementById('sampleInfo_crmCustomer_id1').value=rec.get('mainProject-id');
						document.getElementById('sampleInfo_crmCustomer_name1').value=rec.get('mainProject-name');
						document.getElementById('sampleInfo_crmDoctor_id1').value=rec.get('crmDoctor-id');
						document.getElementById('sampleInfo_crmDoctor_name1').value=rec.get('crmDoctor-name');" />
				<%--  	 	<td class="label-title" >科研项目编号</td>
	               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
	                   	<td align="left"  >
	 					 	</td> --%>
				<td class="label-title"><fmt:message
						key="biolims.common.scientificResearchProject" /></td>
				<td class="requiredcolumn" nowrap width="10px"></td>
				<td align="left"><input type="hidden" id="sampleInfo_project"
					name="sampleInfo.project.id"
					value="<s:property value="sampleInfo.project.id"/>"
					readonly="readOnly"> <input type="text" size="20"
					readonly="readOnly" id="sampleInfo_project_name1"
					value="<s:property value="sampleInfo.project.name"/>"
					class="text input" readonly="readOnly" /> <img
					alt='<fmt:message key="biolims.common.chooseTheScientificResearchProject"/>'
					id='showacceptUser' src='${ctx}/images/img_lookup.gif'
					class='detail' /></td>
			</tr>

		</table>


	</div>
	<div id="bat_crm_div" style="display: none">
		<table>
			<tr>
				<g:LayOutWinTag buttonId="mainProjectBtn"
					title='<fmt:message key="biolims.common.selectTheCustomerID"/>'
					hasHtmlFrame="true" width="document.body.clientWidth/1.5"
					html="${ctx}/crm/customer/customer/crmCustomerSelect.action"
					isHasSubmit="false" functionName="CrmCustomerFun" hasSetFun="true"
					extRec="rec"
					extStr="document.getElementById('sampleInfo_crmCustomer_id1').value=rec.get('id');
						document.getElementById('sampleInfo_crmCustomer_name1').value=rec.get('name');" />
				<td class="label-title"><fmt:message
						key="biolims.common.selectTheCustomer" /></td>
				<td class="requiredcolumn" nowrap width="10px"></td>
				<td align="left"><input type="text" size="35" maxlength="25"
					id="sampleInfo_crmCustomer_name1" name="sampleInfo.crmCustomer.name"
					title="<fmt:message key="biolims.common.selectTheCustomer"/>  "
					readonly="readOnly"
					value="<s:property value="sampleInfo.crmCustomer.name"/>" /> <input
					type="hidden" id="sampleInfo_crmCustomer_id1"
					name="sampleInfo.crmCustomer.id" readonly="readOnly"
					value="<s:property value="sampleInfo.crmCustomer.id"/>"> <img
					alt='<fmt:message key="biolims.common.selectTheCustomer"/> '
					id='mainProjectBtn' src='${ctx}/images/img_lookup.gif'
					name='mainProjectBtn' class='detail' /></td>
			</tr>

		</table>
	</div>


	<div id="bat_docter_div" style="display: none">
		<table>
			<tr>

				<g:LayOutWinTag buttonId="showDoctor"
					title='<fmt:message key="biolims.common.selectTheDoctor"/>'
					hasHtmlFrame="true"
					html="${ctx}/crm/doctor/crmPatientSelect.action"
					isHasSubmit="false" functionName="DicProbeFun" hasSetFun="true"
					extRec="rec"
					extStr="document.getElementById('sampleInfo_crmDoctor_id1').value=rec.get('id');
							document.getElementById('sampleInfo_crmDoctor_name1').value=rec.get('name');" />
				<td class="label-title"><fmt:message
						key="biolims.common.selectTheDoctor" /></td>
				<td class="requiredcolumn" nowrap width="10px"></td>
				<td align="left"><input type="text" size="20" maxlength="25"
					id="sampleInfo_crmDoctor_name1" name="sampleInfo.crmDoctor.name"
					title="<fmt:message key="biolims.common.attendingPhysician"/>"
					readonly="readOnly"
					value="<s:property value="sampleInfo.crmDoctor.name"/>" /> <input
					type="hidden" id="sampleInfo_crmDoctor_id1"
					name="sampleInfo.crmDoctor.id" readonly="readOnly"
					value="<s:property value="sampleInfo.crmDoctor.id"/>"> <img
					alt='<fmt:message key="biolims.common.selectTheDoctor"/>'
					id='showDoctor' src='${ctx}/images/img_lookup.gif' class='detail' />
				</td>
			</tr>

		</table>
	</div>


</body>
</html>



