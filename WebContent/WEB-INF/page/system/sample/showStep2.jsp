<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<title><fmt:message key="biolims.common.noTitleDocuments" /></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<link rel="stylesheet" href="${ctx}/lims/plugins/layui/css/layui.css">
<script type="text/javascript" src="${ctx}/lims/plugins/layui/layui.js"></script>
<style>
</style>
</head>
<body>
	<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
	<input type="hidden" id="sampleOrder_id" value="${requestScope.id}">
	<div >
		<table class="table table-hover table-bordered table-condensed"
			id="sampleInfoTable" style="font-size: 12px;"></table>
	</div>
<script type="text/javascript" src="${ctx}/js/system/sample/sampleOrderMainItemTable.js"></script>
</body>
</html>