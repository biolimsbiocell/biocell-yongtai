<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<link rel="stylesheet" href="${ctx}/css/experimentLab.css" />
<style type="text/css">
.dt-buttons {
	float: none;
}

.tablebtns {
	position: initial;
}
.dataTables_scrollBody {
	min-height: 150px;
}

.layui-layer-content .input-group {
	margin-top: 12px;
}

#fieldItemDiv .icheckbox_square-blue {
	margin-left: 10px;
}
#box2,#box3,#box1 {
	border-radius: 10px;
	overflow: hidden;
	transition: all 1s;
	width: 80%;
	margin: 10px auto;
	color: #fff;
}

#box2 {
	background: #30B573;
}

#box3 {
	background: #8FAAC6;
}

#box1 {
	background: #EAB9A8;
}

#box3:hover {
	box-shadow: 15px 15px 15px #000;
}

#box2:hover {
	box-shadow: 15px 15px 15px #000;
}

#box1:hover {
	box-shadow: 15px 15px 15px #000;
}

.modal-body .box-body div {
	margin: -34px 13px;
	text-align: center;
}

.modal-body .box-body div a {
	color: #fff;
	display: block;
	margin-top: 23%;
	text-decoration: none;
}

.modal-body .box-body div i {
	font-size: 100px;
}

.modal-body .box-body div p {
	font-weight: 900;
}

.chosedType {
	border: 2px solid #BAB5F6 !important;
}

.input-group {
	margin-top: 10px;
}

.scientific {
	display: none;
}

.box-title small span {
	margin-right: 20px;
}
</style>
</head>
<div>
			<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
		</div>
<body style="height: 94%">
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	
	<div class="modal fade" id="sampleOrderChangeModal" tabindex="-1"
		role="dialog" aria-hidden="flase" data-backdrop="false">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
			<div class="modal-header text-center">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" id="modal-body">
					<div class="row">
						<div class="col-sm-4 col-xs-12">
							<div class="box" id="box1">
								<div class="box-body" style="height: 170px;">
									<div id="cancel">
										<a href="###"> <i class="fa  fa-barcode"></i>
											<p>
												订单取消
												<%-- <fmt:message key="biolims.common.cancel" /> --%>
											</p>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-4 col-xs-12">
							<div class="box" id="box2">
								<div class="box-body" style="height: 170px;">
									<div id="modify">
										<a href="####"> <i class="fa  fa-barcode"></i>
											<p>
												订单调整
												<%-- <fmt:message key="biolims.common.modify" /> --%>
											</p>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-4 col-xs-12">
							<div class="box" id="box3">
								<div class="box-body" style="height: 170px;">
									<div id="add">
										<a href="####"> <i class="fa  fa-barcode"></i>
											<p>
												订单追加
												<%-- <fmt:message key="biolims.common.add" /> --%>
											</p>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-12" id="scancode" style="display: none;">
						<input type="text" class="form-control input-lg" id="scanInput"
							autofocus>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="content-wrapper" id="content" style="margin-left: 0px;margin-top: 46px">
		 <input
			type="hidden" id="bpmTaskId" value="${requestScope.bpmTaskId}" />
		 <input
			type="hidden" id="handlemethod" value="${requestScope.handlemethod}" />
		<section class="content">
		<div class="row">
			<!--表格-->
			<div class="col-xs-12">
				<div class="box box-info box-solid">
					<div class="box-header with-border">
						<i class="fa fa-bell-o"></i>
						<h3 class="box-title">
							<fmt:message key="biolims.orderChange.title"/> <small style="color: #fff;">
							<fmt:message key="biolims.sample.createUserName"/>: <span
								userId="<s:property value="sampleOrderChange.createUser.id"/>"
								id="sampleOrderChange_createUser"><s:property
										value="sampleOrderChange.createUser.name" /></span><fmt:message key="biolims.sample.createDate"/>: <span
								id="sampleOrderChange_createDate"><s:date name="sampleOrderChange.createDate" format="yyyy-MM-dd "/></span> <fmt:message key="biolims.common.state"/>: <span
								state="<s:property value="sampleOrderChange.state"/>"
								id="headStateName"><s:property
										value="sampleOrderChange.stateName" /></span>
							</small>
						</h3>
					</div>
					<div class="box-body ipadmini">

						<div class="HideShowPanel">
							<form name="form1" class="layui-form" id="form1" method="post">
								<div class="row">
									<div class="col-xs-4">
										<div class="input-group">
									<span class="input-group-addon"> <fmt:message
												key="biolims.common.codingA" />
									</span> <input list="sampleIdOne" changelog="<s:property value="sampleOrderChange.id"/>" type="text" size="20"
											maxlength="25" id="sampleOrderChange_id" name="sampleOrderChange.id"
											class="form-control"
											title="<fmt:message key=" biolims.common.serialNumber "/>"
											value="<s:property value=" sampleOrderChange.id "/>" />
										</div>
									</div>
									
									<!-- 追加的类型 -->
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon"><fmt:message key="biolims.sampleOrderChange.changeType"/>
											</span> <input type="text" size="20" maxlength="25"
												id="sampleOrderChange_changeType" name="sampleOrderChange.changeType"
												changelog="<s:property value="sampleOrderChange.changeType"/>"
												class="form-control"
												value="<s:property value="sampleOrderChange.changeType"/>" />

										</div>
									</div>
									<!-- 描述 -->
									<div class="col-xs-4">
										<div class="input-group">

											<span class="input-group-addon"><fmt:message key="biolims.common.describe"/></span>

											<input type="text" size="20"  id="sampleOrderChange_describe" changelog="<s:property value="sampleOrderChange.describe"/>" value="<s:property value="sampleOrderChange.describe"/>" class="form-control" />

										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon"><fmt:message key="biolims.sampleOut.acceptUser"/>
											<img class='requiredimage'
											src='${ctx}/images/required.gif' /></span>
											<input type="text" size="20" readonly="readOnly" id="sampleOrderChange_acceptUser_name" changelog="<s:property value=" sampleOrderChange.acceptUser.name "/>" value="<s:property value=" sampleOrderChange.acceptUser.name "/>"  class="form-control" />
											<input type="hidden" id="sampleOrderChange_acceptUser_id" name="sampleOrderChange.acceptUser.id" value="<s:property value=" sampleOrderChange.acceptUser.id "/>">
											<span class="input-group-btn">
												<button class="btn btn-info" type="button" 
													onclick="showAcceptUser()">
													<i class="glyphicon glyphicon-search"></i>
												</span>
										</div>
									</div>
									<!-- 备注 -->
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon"><fmt:message key="biolims.common.note"/>
											</span> <input type="text" size="20" maxlength="25"
												id="sampleOrderChange_note" name="sampleOrderChange.note"
												changelog="<s:property value="sampleOrderChange.note"/>"
												class="form-control"
												value="<s:property value="sampleOrderChange.note"/>" />

										</div>
									</div>
								</div>
							</form>
						</div>
	</br>
						<div id="rightDiv" class="col-md-12 col-xs-12">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title"><fmt:message key="biolims.orderChange.detail" /></h3>
								</div>
								<div class="box-body ipadmini">
									<table
										class="table table-hover table-striped table-bordered table-condensed"
										id="sampleOrderChangeItemdiv" style="font-size: 14px;"></table>
								</div>
							</div>
						</div>
					</div>


				</div>
			</div>

		</div>
		</section>
	</div>
	
	<script type="text/javascript"
		src="${ctx}/js/system/sample/sampleOrderChangeMainEdit.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/common/dataTablesExtend.js"></script>
</body>

</html>