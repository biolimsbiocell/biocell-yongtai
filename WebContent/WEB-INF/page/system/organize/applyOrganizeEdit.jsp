﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
		<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var doclinks_img = Ext.get('doclinks_img');
doclinks_img.on('click', 
 function doc(){
var win = Ext.getCmp('doc');
if (win) {win.close();}
var doc= new Ext.Window({
id:'doc',modal:true,title:'',layout:'fit',width:900,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/operfile/initFileList.action\?modelType=applyOrganize&id=&flag=doc' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 doc.close(); }  }]  });     doc.show(); }
);
});
</script>

</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/system/organize/applyOrganizeEdit.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"></fmt:message></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="18" id="applyOrganize_id"
                   	 name="applyOrganize.id" title="<fmt:message key="biolims.common.serialNumber"></fmt:message>"
	value="<s:property value="applyOrganize.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.provincesAndAutonomousRSegions"></fmt:message></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="40" maxlength="60" id="applyOrganize_name"
                   	 name="applyOrganize.name" title="<fmt:message key="biolims.common.provincesAndAutonomousRSegions"></fmt:message>"
                   	   
	value="<s:property value="applyOrganize.name"/>"
                   	  />
                   	  
                   	</td>
			
			
			
		<%-- 	<g:LayOutWinTag buttonId="showperson" title="选择负责人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('applyOrganize_person').value=rec.get('id');
				document.getElementById('applyOrganize_person_name').value=rec.get('name');" /> --%>
			<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showperson = Ext.get('showperson');
showperson.on('click', 
 function UserFun(){
var win = Ext.getCmp('UserFun');
if (win) {win.close();}
var UserFun= new Ext.Window({
id:'UserFun',modal:true,title:'',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/core/user/userSelect.action?flag=UserFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 UserFun.close(); }  }]  });     UserFun.show(); }
);
});
 function setUserFun(id,name){
 document.getElementById("applyOrganize_person").value = id;
document.getElementById("applyOrganize_person_name").value = name;
var win = Ext.getCmp('UserFun')
if(win){win.close();}
}
</script>

			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.leader"></fmt:message></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="applyOrganize_person_name"  value="<s:property value="applyOrganize.person.name"/>"/>
 						<input type="hidden" id="applyOrganize_person" name="applyOrganize.person.id"  value="<s:property value="applyOrganize.person.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectLeader"></fmt:message>' id='showperson' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
				<%-- <td class="label-title" >区域代码</td>
           	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
					<input type="hidden" size="15"   id="applyOrganize_markCode_name" searchField="true"  name="applyOrganize.markCode.name"   value="<s:property value="applyOrganize.markCode.name"/>" class="text input" />
 						<input type="text" id="applyOrganize_markCode" name="applyOrganize.markCode.id"   value="<s:property value="applyOrganize.markCode.id" />"> 
 						<img alt='选择负责人' id='showperson' src='${ctx}/images/img_lookup.gif' onClick="selectMark()" class='detail'    />                   		
                </td> --%>
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.nameOfInstitution"></fmt:message></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="18" id="applyOrganize_dept"
                   	 name="applyOrganize.dept" title="<fmt:message key="biolims.common.nameOfInstitution"></fmt:message>"
                   	   
	value="<s:property value="applyOrganize.dept"/>"
                   	  />
                   	  
                   	</td>
			<td class="label-title"><fmt:message key="biolims.common.attachment"></fmt:message></td><td></td>
						<td title="<fmt:message key="biolims.common.afterthepreservation"></fmt:message>" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.common"></fmt:message>${requestScope.fileNum}<fmt:message key="biolims.common.noAttachment"></fmt:message></span>
			</tr>
			
			
            </table>
            <input type="hidden" name="applyOrganizeAddressJson" id="applyOrganizeAddressJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="applyOrganize.id"/>" />
            </form>
            <!-- <div id="tabs">
            <ul>
			<li><a href="#applyOrganizeAddresspage">收货地址</a></li>
           	</ul>  -->
			<div id="applyOrganizeAddresspage" width="100%" height:10px></div>
			</div>
        	<!-- </div> -->
	</body>
	</html>
