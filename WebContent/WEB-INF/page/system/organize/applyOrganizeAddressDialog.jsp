﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/system/organize/applyOrganizeAddressDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="aId" value="${requestScope.aId}">
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.user.itemNo"/></td>
               	<td align="left">
                    		<input type="text" maxlength="18" id="applyOrganize_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.name"/></td>
               	<td align="left">
                    		<input type="text" maxlength="60" id="applyOrganize_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
               	<td class="label-title"><fmt:message key="biolims.common.recipient"/></td>
               	<td align="left">
                    		<input type="text" maxlength="18" id="applyOrganize_receiver" searchField="true" name="receiver"  class="input-20-length"></input>
               	</td>
               	<td class="label-title"><fmt:message key="biolims.common.recipientPhone"/></td>
               	<td align="left">
                    		<input type="text" maxlength="18" id="applyOrganize_phone" searchField="true" name="phone"  class="input-20-length"></input>
               	</td>
           	 	<!-- <td class="label-title">代理商</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="applyOrganize_agent" searchField="true" name="agent"  class="input-20-length"></input>
               	</td> -->
           	 	
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue"><fmt:message key="biolims.common.search"/> </font></span>
		
		<div id="show_dialog_applyOrganizeAddress_div"></div>
   		
</body>
</html>



