﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=officeApply&id=${officeApply.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/system/organize/officeApplyEdit.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" >编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="officeApply_id"
                   	 name="officeApply.id" title="编号"
                   	   
readonly = "readOnly" class="text input readonlytrue"
	value="<s:property value="officeApply.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="30" maxlength="50" id="officeApply_name"
                   	 name="officeApply.name" title="描述"
                   	   
	value="<s:property value="officeApply.name"/>"
                   	  />
                   	  
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true"
				html="${ctx}/system/organize/officeApply/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('officeApply_createUser').value=rec.get('id');
				document.getElementById('officeApply_createUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" >创建人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="officeApply_createUser_name"  value="<s:property value="officeApply.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="officeApply_createUser" name="officeApply.createUser.id"  value="<s:property value="officeApply.createUser.id"/>" > 
 						<%-- <img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    /> --%>                   		
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >创建时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="officeApply_createDate"
                   	 name="officeApply.createDate" title="创建时间"
                   	   class="text input readonlytrue" readonly="readOnly"
                   	  value="<s:date name="officeApply.createDate" format="yyyy-MM-dd"/>"
                   	                      	  />
                   	  
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showacceptUser" title="选择审批人"
				hasHtmlFrame="true"
				html="${ctx}/system/organize/officeApply/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('officeApply_acceptUser').value=rec.get('id');
				document.getElementById('officeApply_acceptUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" >审批人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="officeApply_acceptUser_name"  value="<s:property value="officeApply.acceptUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="officeApply_acceptUser" name="officeApply.acceptUser.id"  value="<s:property value="officeApply.acceptUser.id"/>" > 
<%--  						<img alt='选择审批人' id='showacceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 --%>                   	</td>
			
			
               	 	<td class="label-title" >审批时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="officeApply_acceptDate"
                   	 name="officeApply.acceptDate" title="审批时间"
                   	   class="text input readonlytrue" readonly="readOnly"
                   	  value="<s:date name="officeApply.acceptDate" format="yyyy-MM-dd"/>"
                   	                      	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title"  style="display:none">工作流ID</td>
               	 	<td class="requiredcolumn" nowrap width="10px" style="display:none"></td>            	 	
                   	<td align="left"  style="display:none">
                   	<input type="text" size="20" maxlength="25" id="officeApply_state"
                   	 name="officeApply.state" title="状态"
                   	   class="text input readonlytrue" readonly="readOnly"
	value="<s:property value="officeApply.state"/>"
						style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title">工作流状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   	<input type="text" size="20" maxlength="25" id="officeApply_statename"
                   	 name="officeApply.statename" title="状态名称"
                   	   
	value="<s:property value="officeApply.statename"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   	<input type="text" size="50" maxlength="250" id="officeApply_note"
                   	 name="officeApply.note" title="备注"
                   	  
	value="<s:property value="officeApply.note"/>"
                   	  
                   	  />
                   	  

			<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr>
			
			
            </table>
            <input type="hidden" name="officeApplyItemJson" id="officeApplyItemJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="officeApply.id"/>" />
            </form>
<!--             <div id="tabs">
            <ul>
			<li><a href="#officeApplyItempage">申请明细</a></li>
           	</ul>  -->
			<div id="officeApplyItempage" width="100%" height:10px></div>
			</div>
        	<!-- </div> -->
	</body>
	</html>
