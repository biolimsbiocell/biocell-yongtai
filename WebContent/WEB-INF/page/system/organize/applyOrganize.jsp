﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/system/organize/applyOrganize.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.user.itemNo"/> </td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="18" id="applyOrganize_id"
                   	 name="id" searchField="true" title="编号"    />
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.name"/> </td>
                   	<td align="left"  >
					<input type="text" size="50" maxlength="60" id="applyOrganize_name"
                   	 name="name" searchField="true" title="描述"    />
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.master.personName"/> </td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="applyOrganize_person_name" searchField="true"  name="person.name"  value="" class="text input" />
 						<input type="hidden" id="applyOrganize_person" name="applyOrganize.person.id"  value="" > 
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.dept"/> </td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="18" id="applyOrganize_dept"
                   	 name="dept" searchField="true" title="单位名称"    />
            </tr>
            <tr>
                   	<td class="label-title" > <fmt:message key="biolims.common.markCodeName"/> </td>
                  	<td align="left"  >
 						<input type="hidden" size="15"   id="applyOrganize_markCode_name" searchField="true"  name="applyOrganize.markCode.name"  value="" class="text input" />
 						<input type="text" id="applyOrganize_markCode" name="markCode.id"  value="" > 
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_applyOrganize_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_applyOrganize_tree_page"></div>
</body>
</html>



