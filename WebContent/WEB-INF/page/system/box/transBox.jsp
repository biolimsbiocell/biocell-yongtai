
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/system/box/transBox.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title">编号</td>
                   	<td align="left">
                  
					<input type="text" size="20" maxlength="25" id="transBox_id"
                   	 name="id" searchField="true" title="编号"/>
                   	</td>
               	 	<td class="label-title" >名称</td>
                   	<td align="left"  >
                  
					<input type="text" size="30" maxlength="50" id="transBox_name"
                   	 name="name" searchField="true" title="名称"    />
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_transBox_div"></div>
</body>
</html>



