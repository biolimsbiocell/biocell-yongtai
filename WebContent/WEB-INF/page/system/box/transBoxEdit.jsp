
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var doclinks_img = Ext.get('doclinks_img');
doclinks_img.on('click', 
 function doc(){
var win = Ext.getCmp('doc');
if (win) {win.close();}
var doc= new Ext.Window({
id:'doc',modal:true,title:'',layout:'fit',width:900,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/operfile/initFileList.action\?modelType=transBox&id=&flag=doc' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 doc.close(); }  }]  });     doc.show(); }
);
});
</script>


</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/system/box/transBoxEdit.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title"><fmt:message key="biolims.common.serialNumber"></fmt:message></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left" >
                   	<input type="text" size="20" maxlength="25" id="transBox_id"
                   	 name="transBox.id" title="<fmt:message key="biolims.common.serialNumber"></fmt:message>"
                   	   
					value="<s:property value="transBox.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.name"></fmt:message></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="50" id="transBox_name"
                   	 name="transBox.name" title="<fmt:message key="biolims.common.name"></fmt:message>"
                   	   
					value="<s:property value="transBox.name"/>"
                   	  />
                   	  
                   	</td>
					<td class="label-title" ><fmt:message key="biolims.common.specifications"></fmt:message></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="20" id="transBox_boxSize"
                   	 name="transBox.boxSize" title="<fmt:message key="biolims.common.specifications"></fmt:message>"
                   	   
					value="<s:property value="transBox.boxSize"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>

					<td class="label-title" ><fmt:message key="biolims.common.number"></fmt:message></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="20" id="transBox_num"
                   	 name="transBox.num" title="<fmt:message key="biolims.common.number"></fmt:message>"
                   	   
					value="<s:property value="transBox.num"/>"
                   	  />
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.state"></fmt:message></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	   <select name="transBox.state" id="transBox_state" style="width:150px;">
                   			<option value="1" <s:if test="transBox.state == 1">selected="selected"</s:if>><fmt:message key="biolims.common.effective"></fmt:message></option>
                   			<option value="0" <s:if test="transBox.state == 0">selected="selected"</s:if>><fmt:message key="biolims.common.invalid"></fmt:message></option>
                   		</select>
                   	  
                   	</td>

			<td class="label-title"><fmt:message key="biolims.common.attachment"></fmt:message></td><td></td>
						<td title='<fmt:message key="biolims.common.afterthepreservation"></fmt:message>' id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"></fmt:message>${requestScope.fileNum}<fmt:message key="biolims.common.noAttachment"></fmt:message></span>
			</tr>
			
			
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="transBox.id"/>" />
            </form>
           <!--  <div id="tabs">
            <ul>
           	</ul> 
			</div> -->
        	</div>
	</body>
	</html>
