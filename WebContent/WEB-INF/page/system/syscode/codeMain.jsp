﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/system/syscode/codeMain.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.user.itemNo"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="codeMain_id"
                   	 name="id" searchField="true" title="编号"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.name"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="codeMain_name"
                   	 name="name" searchField="true" title="描述"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.sys.namex"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="codeMain_namex"
                   	 name="namex" searchField="true" title="名字x轴"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.sys.namey"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="codeMain_namey"
                   	 name="namey" searchField="true" title="名字y轴"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.sys.codex"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="codeMain_codex"
                   	 name="codex" searchField="true" title="编码x轴"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.sys.codey"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="codeMain_codey"
                   	 name="codey" searchField="true" title="编码y轴"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.sys.qrx"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="codeMain_qrx"
                   	 name="qrx" searchField="true" title="二维码x轴"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.sys.qry"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="codeMain_qry"
                   	 name="qry" searchField="true" title="二维码y轴"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.sys.ip"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="codeMain_ip"
                   	 name="ip" searchField="true" title="ip地址"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_codeMain_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_codeMain_tree_page"></div>
</body>
</html>



