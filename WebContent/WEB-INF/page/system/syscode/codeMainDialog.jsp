﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="${ctx}/js/system/syscode/codeMainDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">编号</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="codeMain_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">描述</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="codeMain_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">名字x轴</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="codeMain_namex" searchField="true" name="namex"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">名字y轴</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="codeMain_namey" searchField="true" name="namey"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">编码x轴</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="codeMain_codex" searchField="true" name="codex"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">编码y轴</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="codeMain_codey" searchField="true" name="codey"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">二维码y轴</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="codeMain_qrx" searchField="true" name="qrx"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">二维码y轴</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="codeMain_qry" searchField="true" name="qry"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">ip地址</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="codeMain_ip" searchField="true" name="ip"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">状态</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="codeMain_state" searchField="true" name="state"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue">搜索</font></span>
		
		<div id="show_dialog_codeMain_div"></div>
   		
</body>
</html>



