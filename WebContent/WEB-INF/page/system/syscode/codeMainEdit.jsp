﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var doclinks_img = Ext.get('doclinks_img');
doclinks_img.on('click', 
 function doc(){
var win = Ext.getCmp('doc');
if (win) {win.close();}
var doc= new Ext.Window({
id:'doc',modal:true,title:'',layout:'fit',width:900,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/operfile/initFileList.action\?modelType=codeMain&id=&flag=doc' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 doc.close(); }  }]  });     doc.show(); }
);
});
</script>


</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/system/syscode/codeMainEdit.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"></fmt:message></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="codeMain_id"
                   	 name="codeMain.id" title="<fmt:message key="biolims.common.serialNumber"></fmt:message>"
                   	   
	value="<s:property value="codeMain.id"/>"
                   	  />
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.name"></fmt:message></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
	                   	<input type="text" size="20" maxlength="50" id="codeMain_name"
	                   	 name="codeMain.name" title="<fmt:message key="biolims.common.name"></fmt:message>" value="<s:property value="codeMain.name"/>"
	                   	  />
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.IPAddress"></fmt:message></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="50" id="codeMain_ip"
                   	 name="codeMain.ip" title="<fmt:message key="biolims.common.IPAddress"></fmt:message>"
                   	   
	value="<s:property value="codeMain.ip"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr style="display: none;">
               	 	<td class="label-title" ><fmt:message key="biolims.sys.namex"></fmt:message></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="50" id="codeMain_namex"
                   	 name="codeMain.namex" title="<fmt:message key="biolims.sys.namex"></fmt:message>" value="<s:property value="codeMain.namex"/>"
                   	  />
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.sys.namey"></fmt:message></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="codeMain_namey"
                   	 name="codeMain.namey" title="<fmt:message key="biolims.sys.namey"></fmt:message>"
                   	   
	value="<s:property value="codeMain.namey"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.sys.codex"></fmt:message></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="codeMain_codex"
                   	 name="codeMain.codex" title="<fmt:message key="biolims.sys.codex"></fmt:message>"
                   	   
	value="<s:property value="codeMain.codex"/>"
                   	  />
                   	  
                   	</td>
			
			
			</tr>
			<tr style="display: none;">
               	 	<td class="label-title" ><fmt:message key="biolims.sys.codey"></fmt:message></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="codeMain_codey"
                   	 name="codeMain.codey" title="<fmt:message key="biolims.sys.codey"></fmt:message>"
                   	   
	value="<s:property value="codeMain.codey"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.sys.qrx"></fmt:message></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="codeMain_qrx"
                   	 name="codeMain.qrx" title="<fmt:message key="biolims.sys.qrx"></fmt:message>"
                   	   
	value="<s:property value="codeMain.qrx"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.sys.qry"></fmt:message></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="codeMain_qry"
                   	 name="codeMain.qry" title="<fmt:message key="biolims.sys.qry"></fmt:message>"
                   	   
	value="<s:property value="codeMain.qry"/>"
                   	  />
                   	  
                   	</td>
			
			
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.state"></fmt:message></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="codeMain.state" id="codeMain_state">
							<option value=""
								<s:if test="codeMain.state==''">selected="selected" </s:if>>
								<fmt:message key="biolims.common.pleaseSelect"></fmt:message></option>
							<option value="1"
								<s:if test="codeMain.state==1">selected="selected" 
								</s:if>><fmt:message key="biolims.common.effective"></fmt:message></option>
							<option value="0"
								<s:if test="codeMain.state==0">selected="selected" 
								</s:if>><fmt:message key="biolims.common.invalid"></fmt:message></option>
						</select>
                   	</td>
			<td class="label-title"><fmt:message key="biolims.common.attachment"></fmt:message></td><td></td>
						<td title='<fmt:message key="biolims.common.afterthepreservation"></fmt:message>' id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"></fmt:message>${requestScope.fileNum}<fmt:message key="biolims.common.noAttachment"></fmt:message></span>
			</tr>
			<tr>
				<td class="label-title" ><fmt:message key="biolims.common.printInstruction"></fmt:message></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left" colspan='7'  >
                   	<textarea class="text input  false" id="codeMain_code" name="codeMain.code" tabindex="0" title='<fmt:message key="biolims.common.content"></fmt:message>' onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" style="height:250;width:480"><s:property value="codeMain.code"/></textarea>
                   	</td>
			</tr>
			
			
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="codeMain.id"/>" />
            </form>
        	</div>
	</body>
	</html>
