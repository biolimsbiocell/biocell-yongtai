<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style type="text/css">
.chosedType {
	border: 2px solid #BAB5F6 !important;
}

.input-group {
	margin-top: 10px;
}

.scientific {
	display: none;
}

.box-title small span {
	margin-right: 20px;
}
#btn_submit{
	display: none;
}
#btn_changeState{
	display: none;
}
</style>
</head>

<body>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">
		<input type="hidden" id="familyId"
		value="${requestScope.id}">
		<!-- <div class="container-fluid" > -->
		<!--订单录入的form表单-->
		<!-- <div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid"> -->
				<div class="box-header with-border" style="text-align: center;">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<strong id="familyTitle"
							type="<s:property value="sampleReceive.type"/>"><fmt:message
								key="biolims.family.mainData" /></strong>
					</h3>
				</div>
				<div class="box-body">
					<input type="hidden" name="bpmTaskId" id="bpmTaskId"
						value="<%=request.getParameter(" bpmTaskId ")%>" />
					<form name="guideForm" id="formguide" method="post">
						<div class="row">
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.sname" /><img
										class='requiredimage' src='${ctx}/images/required.gif' /></span><input
										list="familyIdOne" type="text" size="20" maxlength="25"
										id="familyItem_patientName" name="patientName" class="form-control"
										title="" value="<s:property value=" familyItem.patientName "/>" />
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.dateOfBirth" /></span><input
										list="familyIdOne" type="text" size="20" maxlength="25"
										id="familyItem_birthDate" name="birthDate" class="form-control"
										title="" value="<s:property value=" familyItem.birthDate "/>" />
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.age" /></span><input
										list="familyIdOne" type="text" size="20" maxlength="25"
										id="familyItem_age" name="age" class="form-control"
										title="" value="<s:property value=" familyItem.age "/>" />
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
										<span class="input-group-addon"><fmt:message key='biolims.common.gender'/></span>
										<select lay-ignore id="familyItem_gender" name="gender" class="form-control" 
										changelog="<s:property value=" familyItem.gender "/>" onchange="showPart()">
											<option value="">
												<fmt:message key='biolims.common.pleaseSelect' />
											</option>
											<option value="1" <s:if test="familyItem.gender==1">selected="selected"</s:if>>
												<fmt:message key='biolims.common.male' />
											</option>
											<!-- 有效 -->
											<option value="0" <s:if test="familyItem.gender==0">selected="selected"</s:if>>
												<fmt:message key='biolims.common.female' />
											</option>
											<option value="-1" <s:if test="familyItem.gender==-1">selected="selected"</s:if>>
												<fmt:message key='biolims.common.unknown' />
											</option>
											<!-- 无效 -->
										</select>
									</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12" id="isDiseaseDiv">
								<div class="input-group">
										<span class="input-group-addon"><fmt:message key='biolims.common.isDisease'/></span>
										<select lay-ignore id="familyItem_isDisease" name="isDisease" class="form-control" 
										changelog="<s:property value=" familyItem.isDisease "/>">
											<option value="">
												<fmt:message key='biolims.common.pleaseSelect' />
											</option>
											<option value="1" <s:if test="familyItem.isDisease==1">selected="selected"</s:if>>
												<fmt:message key='biolims.workflow.yesName' />
											</option>
											<!-- 有效 -->
											<option value="0" <s:if test="familyItem.isDisease==0">selected="selected"</s:if>>
												<fmt:message key='biolims.workflow.noName' />
											</option>
											<!-- 无效 -->
										</select>
									</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12" id="relateManDiv">
								<div class="input-group">
										<span class="input-group-addon"><fmt:message key='biolims.common.relateMan'/></span>
										<select lay-ignore id="familyItem_relateMan" name="relateMan" class="form-control" 
										changelog="<s:property value=" familyItem.relateMan "/>">
											<option  value="">
												<fmt:message key='biolims.common.pleaseSelect' />
											</option>
										</select>
									</div>
							</div>
							
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
										<span class="input-group-addon"><fmt:message key='biolims.common.familyType'/></span>
										<select lay-ignore id="familyItem_familyType" name="familyType" class="form-control" 
										changelog="<s:property value=" familyItem.familyType "/>">
											<option value="">
												<fmt:message key='biolims.common.pleaseSelect' />
											</option>
											<option id="o1" value="-1" <s:if test="familyItem.familyType==-1">selected="selected"</s:if>>
												<fmt:message key='biolims.common.mother' />
											</option>
											<option id="p1" value="1" <s:if test="familyItem.familyType==1">selected="selected"</s:if>>
												<fmt:message key='biolims.common.father' />
											</option>
											<option id="p2" value="2" <s:if test="familyItem.familyType==2">selected="selected"</s:if>>
												<fmt:message key='biolims.common.olderBrother' />
											</option>
											<option id="o2" value="-2" <s:if test="familyItem.familyType==-2">selected="selected"</s:if>>
												<fmt:message key='biolims.common.olderSister' />
											</option>
											<option id="p3" value="3" <s:if test="familyItem.familyType==3">selected="selected"</s:if>>
												<fmt:message key='biolims.common.husband' />
											</option>
											<option id="o3" value="-3" <s:if test="familyItem.familyType==-3">selected="selected"</s:if>>
												<fmt:message key='biolims.common.wife' />
											</option>
											<option id="p4" value="4" <s:if test="familyItem.familyType==4">selected="selected"</s:if>>
												<fmt:message key='biolims.common.littleBrother' />
											</option>
											<option id="o4" value="-4" <s:if test="familyItem.familyType==-4">selected="selected"</s:if>>
												<fmt:message key='biolims.common.littleSister' />
											</option>
											<option value="5" <s:if test="familyItem.familyType==5">selected="selected"</s:if>>
												<fmt:message key='biolims.common.myself' />
											</option>
										</select>
									</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
										<span class="input-group-addon"><fmt:message key='biolims.common.isDead'/></span>
										<select lay-ignore id="familyItem_isDead" name="isDead" class="form-control" 
										changelog="<s:property value=" familyItem.isDead "/>">
											<option value="">
												<fmt:message key='biolims.common.pleaseSelect' />
											</option>
											<option value="1" <s:if test="familyItem.isDead==1">selected="selected"</s:if>>
												<fmt:message key='biolims.workflow.yesName' />
											</option>
											<option value="0" <s:if test="familyItem.isDead==0">selected="selected"</s:if>>
												<fmt:message key='biolims.workflow.noName' />
											</option>
										</select>
									</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
										<span class="input-group-addon"><fmt:message key='biolims.common.isDivorce'/></span>
										<select lay-ignore id="familyItem_isDivorce" name="isDivorce" class="form-control" 
										changelog="<s:property value=" familyItem.isDivorce "/>">
											<option value="">
												<fmt:message key='biolims.common.pleaseSelect' />
											</option>
											<option value="1" <s:if test="familyItem.isDivorce==1">selected="selected"</s:if>>
												<fmt:message key='biolims.workflow.yesName' />
											</option>
											<option value="0" <s:if test="familyItem.isDivorce==0">selected="selected"</s:if>>
												<fmt:message key='biolims.workflow.noName' />
											</option>
										</select>
									</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
										<span class="input-group-addon"><fmt:message key='biolims.common.haveChild'/></span>
										<select lay-ignore id="familyItem_haveChild" name="haveChild" class="form-control" 
										changelog="<s:property value=" familyItem.haveChild "/>" onchange="haveBaby()">
											<option value="">
												<fmt:message key='biolims.common.pleaseSelect' />
											</option>
											<option value="1" <s:if test="familyItem.haveChild==1">selected="selected"</s:if>>
												<fmt:message key='biolims.workflow.yesName' />
											</option>
											<option value="0" <s:if test="familyItem.haveChild==0">selected="selected"</s:if>>
												<fmt:message key='biolims.workflow.noName' />
											</option>
										</select>
									</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
										<span class="input-group-addon"><fmt:message key='biolims.common.isTwins'/></span>
										<select lay-ignore id=familyItem_isTwins name="isTwins" class="form-control" 
										changelog="<s:property value=" familyItem.isTwins "/>">
											<option value="">
												<fmt:message key='biolims.common.pleaseSelect' />
											</option>
											<option value="1" <s:if test="familyItem.isTwins==1">selected="selected"</s:if>>
												<fmt:message key='biolims.workflow.yesName' />
											</option>
											<!-- 有效 -->
											<option value="0" <s:if test="familyItem.isTwins==0">selected="selected"</s:if>>
												<fmt:message key='biolims.workflow.noName' />
											</option>
											<!-- 无效 -->
										</select>
									</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
										<span class="input-group-addon"><fmt:message key='biolims.common.isCustody'/></span>
										<select lay-ignore id=familyItem_isCustody name="isCustody" class="form-control" 
										changelog="<s:property value=" familyItem.isCustody "/>">
											<option value="">
												<fmt:message key='biolims.common.pleaseSelect' />
											</option>
											<option value="1" <s:if test="familyItem.isCustody==1">selected="selected"</s:if>>
												<fmt:message key='biolims.workflow.yesName' />
											</option>
											<!-- 有效 -->
											<option value="0" <s:if test="familyItem.isCustody==0">selected="selected"</s:if>>
												<fmt:message key='biolims.workflow.noName' />
											</option>
											<!-- 无效 -->
										</select>
									</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
										<span class="input-group-addon"><fmt:message key='biolims.common.cancer1'/></span>
										<select lay-ignore id=familyItem_leftUp name="leftUp" class="form-control" 
										changelog="<s:property value=" familyItem.leftUp "/>">
											<option value="">
												<fmt:message key='biolims.common.pleaseSelect' />
											</option>
											<option value="1" <s:if test="familyItem.leftUp==1">selected="selected"</s:if>>
												<fmt:message key='biolims.workflow.yesName' />
											</option>
											<!-- 有效 -->
											<option value="0" <s:if test="familyItem.leftUp==0">selected="selected"</s:if>>
												<fmt:message key='biolims.workflow.noName' />
											</option>
											<!-- 无效 -->
										</select>
									</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
										<span class="input-group-addon"><fmt:message key='biolims.common.cancer2'/></span>
										<select lay-ignore id=familyItem_rightUp name="rightUp" class="form-control" 
										changelog="<s:property value=" familyItem.rightUp "/>">
											<option value="">
												<fmt:message key='biolims.common.pleaseSelect' />
											</option>
											<option value="1" <s:if test="familyItem.rightUp==1">selected="selected"</s:if>>
												<fmt:message key='biolims.workflow.yesName' />
											</option>
											<!-- 有效 -->
											<option value="0" <s:if test="familyItem.rightUp==0">selected="selected"</s:if>>
												<fmt:message key='biolims.workflow.noName' />
											</option>
											<!-- 无效 -->
										</select>
									</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
										<span class="input-group-addon"><fmt:message key='biolims.common.cancer3'/></span>
										<select lay-ignore id=familyItem_leftDown name="leftDown" class="form-control" 
										changelog="<s:property value=" familyItem.leftDown "/>">
											<option value="">
												<fmt:message key='biolims.common.pleaseSelect' />
											</option>
											<option value="1" <s:if test="familyItem.leftDown==1">selected="selected"</s:if>>
												<fmt:message key='biolims.workflow.yesName' />
											</option>
											<!-- 有效 -->
											<option value="0" <s:if test="familyItem.leftDown==0">selected="selected"</s:if>>
												<fmt:message key='biolims.workflow.noName' />
											</option>
											<!-- 无效 -->
										</select>
									</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
										<span class="input-group-addon"><fmt:message key='biolims.common.cancer4'/></span>
										<select lay-ignore id=familyItem_rightDown name="rightDown" class="form-control" 
										changelog="<s:property value=" familyItem.rightDown "/>">
											<option value="">
												<fmt:message key='biolims.common.pleaseSelect' />
											</option>
											<option value="1" <s:if test="familyItem.rightDown==1">selected="selected"</s:if>>
												<fmt:message key='biolims.workflow.yesName' />
											</option>
											<!-- 有效 -->
											<option value="0" <s:if test="familyItem.rightDown==0">selected="selected"</s:if>>
												<fmt:message key='biolims.workflow.noName' />
											</option>
											<!-- 无效 -->
										</select>
									</div>
							</div>
						</div>
					</form>
				<!-- </div>
			</div> -->
		</div>

	<script type="text/javascript"
		src="${ctx}/js/system/family/familyEdit.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/system/family/familyGuide.js"></script>
</body>
</html>