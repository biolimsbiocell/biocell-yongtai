<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style type="text/css">
.chosedType {
	border: 2px solid #BAB5F6 !important;
}

.input-group {
	margin-top: 10px;
}

.scientific {
	display: none;
}

.box-title small span {
	margin-right: 20px;
}
#btn_submit{
	display: none;
}
#btn_changeState{
	display: none;
}
#hideDiv{
	display: none;
}
</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 50px">
		<!--订单录入的form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<strong id="sampleReceiveTitle"
							type="<s:property value="sampleReceive.type"/>"><fmt:message
								key="biolims.family.mainData" /></strong>
					</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefresh()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="####" onclick="$('.buttons-print').click();"><fmt:message
											key="biolims.common.print" /></a></li>
								<li><a href="#####" onclick="$('.buttons-copy').click();"><fmt:message
											key="biolims.common.copyData" /></a></li>
								<li><a href="####" onclick="$('.buttons-excel').click();"><fmt:message
											key="biolims.common.export" /></a></li>
								<li><a href="####" onclick="$('.buttons-csv').click();"><fmt:message
											key="biolims.common.exportCSV" /></a></li>
								<li role="separator" class="divider"></li>
								<li><a href="####" id="fixdeLeft2" onclick="fixedCol(2)"><fmt:message
											key="biolims.common.lock2Col" /></a></li>
								<li><a href="####" id="unfixde" onclick="unfixde()"><fmt:message
											key="biolims.common.cancellock2Col" /></a></li>
							</ul>
						</div>
					</div>

				</div>
				<div class="box-body">
					<input type="hidden" name="bpmTaskId" id="bpmTaskId"
						value="<%=request.getParameter(" bpmTaskId ")%>" />
					<form name="form1" id="form1" method="post">
						<!--保存时的拼的数据-->
						<%-- <input type="hidden" name="confirmDate"
							value="<s:date name=" sampleReceive.confirmDate " format="yyyy-MM-dd "/>" />
						<input type="hidden" id="confirmUserId" name="confirmUser-id"
							value="<s:property value=" sampleReceive.confirmUser.id "/>" />
						<input type="hidden" name="state"
							value="<s:property value="sampleReceive.state"/>" /> <input
							type="hidden" name="stateName"
							value="<s:property value="sampleReceive.stateName"/>" /> <input
							type="hidden" id="sampleReceiveinputid" name="id"
							value="<s:property value="sampleReceive.id "/>" /> <input
							type="hidden" name="acceptUser-id"
							value="<s:property value=" sampleReceive.acceptUser.id "/>" /> --%>
						<div class="row">
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.sample.personShipId" /><img
										class='requiredimage' src='${ctx}/images/required.gif' /></span><input
										list="familyIdOne" type="text" size="20" maxlength="25"
										id="family_id" name="id" class="form-control"
										title="" value="<s:property value=" family.id "/>" />
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.crmPatient.patientStatus" />
									</span>
									<input type="hidden" changelog="<s:property value=" family.cancer.id "/>" list="familyIdOne" type="text" size="20" maxlength="25"
										id="family_cancer_id" name="cancer-id"
										class="form-control" title=""
										value="<s:property value=" family.cancer.id "/>" /> 
									<input  changelog="<s:property value=" family.cancer.cancerTypeName "/>" list="familyIdOne" type="text" size="20" maxlength="25"
										id="family_cancer_cancerTypeName" name="cancer-cancerTypeName"
										class="form-control" title=""
										value="<s:property value=" family.cancer.cancerTypeName "/>" />
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.note" />
									</span> <input  changelog="<s:property value=" family.note "/>" list="familyIdOne" type="text" size="20" maxlength="25"
										id="family_note" name="note"
										class="form-control" title=""
										value="<s:property value=" family.note "/>" />
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.common.hospital" /> </span> <input type="hidden"
											id="family_hospital_id" name="hospital-id"
											value="<s:property value="family.hospital.id"/>">
										<input type="text" size="50" maxlength="127"
											id="family_hospital_name" readonly="readOnly"
											changelog="<s:property value="family.hospital.name"/>"
											title="<fmt:message key="biolims.common.hospital"/>"
											class="form-control"
											value="<s:property value="family.hospital.name"/>" /> <span
											class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showHos()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.sname" />
									</span> 
									<input  changelog="<s:property value=" family.patientName "/>" list="familyIdOne" type="text" size="20" maxlength="25"
										id="family_patientName" name="patientName"
										class="form-control" title=""
										value="<s:property value=" family.patientName "/>" />
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.code" />
									</span> <input  changelog="<s:property value=" family.crmPatient.sampleCode "/>" list="familyIdOne" type="text" size="20" maxlength="25"
										id="family_crmPatient_sampleCode" name="crmPatient-sampleCode"
										class="form-control" title=""
										value="<s:property value=" family.crmPatient.sampleCode "/>" />
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.cancer1" />
									</span> <input  changelog="<s:property value=" family.cancer1 "/>" list="familyIdOne" type="text" size="20" maxlength="25"
										id="family_cancer1" name="cancer1"
										class="form-control" title=""
										value="<s:property value=" family.cancer1 "/>" />
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.cancer2" />
									</span> <input  changelog="<s:property value=" family.cancer2 "/>" list="familyIdOne" type="text" size="20" maxlength="25"
										id="family_cancer2" name="cancer2"
										class="form-control" title=""
										value="<s:property value=" family.cancer2 "/>" />
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.cancer3" />
									</span> <input  changelog="<s:property value=" family.cancer3 "/>" list="familyIdOne" type="text" size="20" maxlength="25"
										id="family_cancer3" name="cancer3"
										class="form-control" title=""
										value="<s:property value=" family.cancer3 "/>" />
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.cancer4" />
									</span> <input  changelog="<s:property value=" family.cancer4 "/>" list="familyIdOne" type="text" size="20" maxlength="25"
										id="family_cancer4" name="cancer4"
										class="form-control" title=""
										value="<s:property value=" family.cancer4 "/>" />
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.electronicMedicalRecordNumber" />
									</span> <input  changelog="<s:property value=" family.patientNo "/>" list="familyIdOne" type="text" size="20" maxlength="25"
										id="family_patientNo" name="patientNo"
										class="form-control" title=""
										value="<s:property value="family.patientNo"/>" />
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<button type="button" class="btn btn-info btn-sm" onclick="fileUp()">
										<fmt:message key="biolims.common.uploadAttachment" />
									</button>&nbsp;&nbsp;
										<button type="button" class="btn btn-info btn-sm"
											onclick="fileView()">
											<fmt:message key="biolims.report.checkFile" />
										</button>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="box-footer ipadmini"
					style="padding-top: 5px; min-height: 320px;">
					<h3><fmt:message key="biolims.family.detail"/></h3>
					<table
						class="table table-hover table-striped table-bordered table-condensed"
						id="main" style="font-size: 14px;"></table>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript"
		src="${ctx}/js/system/family/familyEdit.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/common/dataTablesExtend.js"></script>
</body>
</html>