﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/system/location/saveLocation.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" >编号</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="18" id="saveLocation_id"
                   	 name="id" searchField="true" title="编号"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >描述</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="60" id="saveLocation_name"
                   	 name="name" searchField="true" title="描述"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showupId" title="选择上级编号"
				hasHtmlFrame="true"
				html="${ctx}/system/location/saveLocation/stringSelect.action"
				isHasSubmit="false" functionName="StringFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('saveLocation_upId').value=rec.get('id');
				document.getElementById('saveLocation_upId_name').value=rec.get('name');" />
               	 	<td class="label-title" >上级编号</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="saveLocation_upId_name" searchField="true"  name="upId.name"  value="" class="text input" />
 						<input type="hidden" id="saveLocation_upId" name="saveLocation.upId.id"  value="" > 
 						<img alt='选择上级编号' id='showupId' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_saveLocation_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_saveLocation_tree_page"></div>
</body>
</html>



