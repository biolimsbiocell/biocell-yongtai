<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<title></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style>
.dataTables_scrollBody {
	min-height: 100px;
}
</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div style="height: 14px"></div>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 46px">
		<!--Form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<i class="glyphicon glyphicon-pencil"></i>检测项
					</h3>

					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="####" onclick="$('.buttons-print').click();"><fmt:message
											key="biolims.common.print" /></a></li>
								<li><a href="#####" onclick="$('.buttons-copy').click();"><fmt:message
											key="biolims.common.copyData" /></a></li>
								<li><a href="####" onclick="$('.buttons-excel').click();">Excel</a>
								</li>
								<li><a href="####" onclick="$('.buttons-csv').click();">CSV</a>
								</li>
								<li role="separator" class="divider"></li>
								<li><a href="####" onclick="itemFixedCol(2)"><fmt:message
											key="biolims.common.lock2Col" /></a></li>
								<li><a href="####" id="unfixde" onclick="unfixde()"><fmt:message
											key="biolims.common.cancellock2Col" /></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="box-body">
					<!--进度条-->
					<%-- <div id="wizard" class="form_wizard wizard_horizontal">
						<ul class="wizard_steps">
							<li><a class="selected step"> <span class="step_no">1</span>
									<span class="step_descr"> Step 1<br /> <small>检测项</small>
								</span>
							</a></li>
										<li><a class="disabled step"> <span class="step_no">2</span>
											<span class="step_descr"> Step 2<br /> <small>检测项明细</small></span>
								</a></li>
						</ul>
					</div> --%>
					<!--form表单-->
					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
							<input type="hidden" id="bpmTaskId"
								value="<%=request.getParameter("bpmTaskId")%>" /> <br>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.sampleDetecyion.id" /> <img
											class='requiredimage' src='${ctx}/images/required.gif' /> </span> <input
											type="text" size="20" maxlength="25" id="sampleDeteyion_id"
											name="id" readonly="readonly"
											changelog="<s:property value="sampleDeteyion.id"/>"
											class="form-control readonlytrue"
											value="<s:property value="sampleDeteyion.id"/>" />

									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.sampleDetecyion.name" /> <img
											class='requiredimage' src='${ctx}/images/required.gif' /> </span> <input
											type="text" size="50" maxlength="125"
											id="sampleDeteyion_name" name="name"
											changelog="<s:property value="sampleDeteyion.name"/>"
											class="form-control"
											value="<s:property value="sampleDeteyion.name"/>" />

									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon">检测模版<img
											class='requiredimage' src='${ctx}/images/required.gif' />
										</span><input type="hidden" size="50" maxlength="125"
											id="sampleDeteyion_template_id" name="template-id"
											changelog="<s:property value="sampleDeteyion.template.id"/>"
											class="form-control"
											value="<s:property value="sampleDeteyion.template.name"/>" />
										<input type="text" size="50" maxlength="125"
											id="sampleDeteyion_template_name" name="template-name"
											changelog="<s:property value="sampleDeteyion.template.name"/>"
											class="form-control"
											value="<s:property value="sampleDeteyion.template.name"/>" />
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id=''
												onClick="selectTemplate()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>
							</div>
							<!-- 检测项目编号 -->
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon">检定项目代码<!-- 检测项目编号 --> <img
											class='requiredimage' src='${ctx}/images/required.gif' />
										</span> <input type="text" size="50" maxlength="125"
											id="sampleDeteyion_deteyionname" name="deteyionName"
											changelog="<s:property value="sampleDeteyion.deteyionName"/>"
											class="form-control"
											value="<s:property value="sampleDeteyion.deteyionName"/>" />

									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon">是否推送生产</span> <select
											lay-ignore id="sampleDeteyion_yesOrNo" name="yesOrNo"
											class="form-control">
											<option value="0"
												<s:if test="sampleDeteyion.yesOrNo==0">selected="selected"</s:if>>
												否</option>
											<option value="1"
												<s:if test="sampleDeteyion.yesOrNo==1">selected="selected"</s:if>>
												是</option>
										</select>

									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">是否提醒</span> <select onclick="okko()"
											lay-ignore id="sampleDeteyion_yesOrNo2" name="yesOrNo2"
											class="form-control">
											<option value="0"
												<s:if test="sampleDeteyion.yesOrNo2==0">selected="selected"</s:if>>
												否</option>
											<option value="1"
												<s:if test="sampleDeteyion.yesOrNo2==1">selected="selected"</s:if>>
												是</option>
										</select>
									</div>
								</div>
								</div>
								<div class="row" id="okko">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon">日期(小时)</span> <input
											type="text" size="50" maxlength="125"
											id="sampleDeteyion_remindDate" name="remindDate"
											changelog="<s:property value="sampleDeteyion.remindDate"/>"
											class="form-control"
											value="<s:property value="sampleDeteyion.remindDate"/>" />

									</div>
								</div>
								<%-- 					<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.sampleDetecyion.createUser"/> <img class='requiredimage' src='${ctx}/images/required.gif' />  </span> 
									
										
                   						<input type="hidden"
											size="50" maxlength="125" id="sampleDeteyion_createUser"
											name="createUser"  changelog="<s:property value="sampleDeteyion.createUser.name"/>"
											  class="form-control"   
											value="<s:property value="sampleDeteyion.createUser"/>"/>  
									</div>
								</div>				
									</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.sampleDetecyion.createDate"/> <img class='requiredimage' src='${ctx}/images/required.gif' />  </span> 
									
										
					
									</div>
								</div>				
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.common.state"/> <img class='requiredimage' src='${ctx}/images/required.gif' />  </span> 
									
										
                   						<input type="hidden"
											size="50" maxlength="125" id="sampleDeteyion_state"
											name="state"  changelog="<s:property value="sampleDeteyion.state"/>"
											  class="form-control"   
	value="<s:property value="sampleDeteyion.state"/>"
						 />  
					
									</div>
								</div>				
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.sampleDetecyion.scopeId"/> <img class='requiredimage' src='${ctx}/images/required.gif' />  </span> 
									
										
                   						<input type="hidden"
											size="50" maxlength="125" id="sampleDeteyion_scopeId"
											name="scopeId"  changelog="<s:property value="sampleDeteyion.scopeId"/>"
											  class="form-control"   
	value="<s:property value="sampleDeteyion.scopeId"/>"
						 />  
					
									</div>
								</div>				
									</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.sampleDetecyion.scopeName"/> <img class='requiredimage' src='${ctx}/images/required.gif' />  </span> 
									
										
                   						<input type="hidden"
											size="50" maxlength="125" id="sampleDeteyion_scopeName"
											name="scopeName"  changelog="<s:property value="sampleDeteyion.scopeName"/>"
											  class="form-control"   
	value="<s:property value="sampleDeteyion.scopeName"/>"
						 />  
					
									</div>
								</div>				
									<button type="button" class="btn btn-info" onclick="fileUp()">上传附件</button>
											<span class="text label"><fmt:message
													key="biolims.common.aTotalOf" />&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
													key="biolims.common.attachment" /></span>

											<button type="button" class="btn btn-info"
												onclick="fileView()">查看附件</button>
									</div> --%>
								<!-- 	<div class="row">
									<div class="panel" style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
										<div class="panel-heading text-left">
											<h3 class="panel-title" style="font-family: 黑体;">
												<i class="glyphicon glyphicon-bookmark"></i> 自定义字段
													
											</h3>
										</div>
									</div>
								</div> -->
							</div>
							<div id="fieldItemDiv"></div>

							<br> <input type="hidden" name="sampleDeteyionItemJson"
								id="sampleDeteyionItemJson" value="" /> <input type="hidden"
								id="id_parent_hidden"
								value="<s:property value="sampleDeteyion.id"/>" /> <input
								type="hidden" id="fieldContent" name="fieldContent"
								value="<s:property value="sampleDeteyion.fieldContent"/>" />
						</form>
					</div>
					<!--table表格-->
					<div class="HideShowPanel">
						<table
							class="table table-hover table-striped table-bordered table-condensed"
							id="sampleDeteyionItemTable" style="font-size: 14px;">
						</table>
					</div>
				</div>
				<!-- <div class="box-footer">
					<div class="pull-right">
						<button type="button" class="btn btn-primary" id="pre">上一步</button>
						<button type="button" class="btn btn-primary" id="next">下一步</button>
					</div>
				</div> -->
			</div>
		</div>
	</div>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/system/detecyion/sampleDeteyionEdit.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/system/detecyion/sampleDeteyionItem.js"></script>
</body>

</html>
