<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<script type="text/javascript" src="${ctx}/js/system/sysreport/selReportTemplateList.js"></script>
</head>

<body>
	<div>
		<table >
			<tr>
				<td  class="label-title"><fmt:message key="biolims.common.name"/></td>
				<td>
					<input type="text"  id="sel_search_name">
				</td>
				<td  class="label-title" ><fmt:message key="biolims.common.describe"/></td>
				<td>
					<input type="text"  id="sel_search_note">
					<span class="search-btn" id="sel_search"></span>
				</td>
			</tr>
		</table>
	</div>
	<div id ="report_template_grid_div"></div>
</body>
</html>