<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>

<script type="text/javascript" src="${ctx}/js/system/sysreport/showReportList.js"></script>
</head>
<body>
	<div id ="report_template_grid_div"></div>
	<div style="display: none">
		<select id="grid_state" style="display:none">
			<option value="0"><fmt:message key="biolims.common.failure"/></option>
			<option value="1"><fmt:message key="biolims.common.takeEffect"/></option>
		</select>
	</div>
</body>
</html>