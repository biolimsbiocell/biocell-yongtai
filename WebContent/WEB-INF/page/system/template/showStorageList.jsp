<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="${ctx}/js/system/template/showStorageList.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>
	<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">编码</td>
               	<td align="left">
                    		<input type="text" maxlength="18" id="instrument_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	
           	 	<td class="label-title">描述</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="instrument_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
               	<td class="label-title">分类</td>
               	<td align="left">
                    		<input type="text" maxlength="60" id="instrument_studyType_name" searchField="true" name="studyType.name"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" id="opensearch"><font color="blue">搜索</font></span>
	<div id="showStorageListdiv"></div>
	
</body>
</html>


