<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ page language="java" import="java.util.List" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>

<%
	String path = request.getContextPath();
	String cPath = request.getScheme() + "://"+ request.getServerName() + ":" + request.getServerPort()+ path ;
	String basePath = request.getScheme() + "://"+ request.getServerName() + ":" + request.getServerPort()+ path + "/operfile/downloadById.action?id="+request.getAttribute("fileId");
	/* String basePathDownId = request.getScheme() + "://"+ request.getServerName() + ":" + request.getServerPort()+ path + "/operfile/downloadById.action?id=";
	String basePathDownName = request.getScheme() + "://"+ request.getServerName() + ":" + request.getServerPort()+ path + "/operfile/downloadFileByPath.action?fileName="; */
%>
<script type="text/javascript">
function viewdoc() {
	document.all.DSOFramer.ShowView(3);
	document.all.DSOFramer.Open("<%=basePath%>", true, "Excel.Sheet");
	document.all.DSOFramer.Menubar = true;
	document.all.DSOFramer.SetMenuDisplay(64);
	document.all.DSOFramer.SetMenuDisplay(126);
	var xls = new Object(document.all.DSOFramer.ActiveDocument);
	var xlsheet;
	if(document.getElementById("countSheetNum").value){
		var num = parseInt(document.getElementById("countSheetNum").value);
		xlsheet = xls.Worksheets(num);
	}else{
		xlsheet = xls.Worksheets(1);
	}
}
function savedoc() {
	
}
	
function printdoc() {
	document.all.DSOFramer.PrintOut();
}

function uploaddoc(){
	document.all.DSOFramer.HttpInit();
	document.all.DSOFramer.HttpAddPostCurrFile("File",encodeURI("<%=request.getAttribute("fileName")%>"));
	document.all.DSOFramer.HttpPost("<%=cPath%>/fileUpload?contentId=<%=request.getAttribute("contentId")%>&modelType=<%=request.getAttribute("type")%>&useType=doc&type=1&picUserId="+window.userId);
	alert("上传保存成功！");
}
		
function ppw() {
	document.all.DSOFramer.PrintPreview();
}

function ppwe() {
	
	document.all.DSOFramer.PrintPreviewExit();
}
		
		
function reloaddoc(){
	
	document.location.href=this.location.href+"&reloadAction=true";
}

</script>

		
		
</head>
<body onload="setTimeout(viewdoc,1000);">
		
		<div id="dsoDoc" align="center" >
		
		
		<input type="button" value="保存至服务器" onclick="uploaddoc()"></input>
		&nbsp;&nbsp;&nbsp;
		<input type="button" value="获取数据至报告" onclick="cs()"></input>
		&nbsp;&nbsp;&nbsp;
		<input type="button" value="重新加载模板" onclick="reloaddoc()"></input>
		&nbsp;&nbsp;&nbsp;
		<input type="button" value="打印预览" onclick="ppw();"></input>
		&nbsp;&nbsp;&nbsp;
		<input type="button" value="退出打印预览" onclick="ppwe();"></input>
		<OBJECT id='DSOFramer' align='middle' style='LEFT: 0px; WIDTH: 100%; TOP: 0px; HEIGHT: 95%'
		classid=clsid:00460182-9E5E-11d5-B7C8-B8269041DD57 codeBase='${ctx}/dso/dso.CAB#V2.3.0.1'>
		<PARAM NAME='_ExtentX' VALUE='6350'>
		<PARAM NAME='_ExtentY' VALUE='6350'>
		<PARAM NAME='BorderColor' VALUE='-2147483632'>
		<PARAM NAME='BackColor' VALUE='-2147483643'>
		<PARAM NAME='ActivationPolicy' VALUE='6'>
		<PARAM NAME='ForeColor' VALUE='-2147483640'>
		<PARAM NAME='TitlebarColor' VALUE='-2147483635'>
		<PARAM NAME='TitlebarTextColor' VALUE='-2147483634'>
		<PARAM NAME='BorderStyle' VALUE='1'>
		<PARAM NAME='Titlebar' VALUE='0'>
		<PARAM NAME='Toolbars' VALUE='1'>
		<PARAM NAME='Menubar' VALUE='0'>
		<param name='BorderStyle' value='1'>
	</OBJECT>
	</div>
			
</body>
</html>