<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>

<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<g:LayOutWinTag buttonId="showPosition" title="所属位置" hasHtmlFrame="true"
	html="${ctx}/storage/common/showStoragePositionTree.action"
	isHasSubmit="false" functionName="searchPosition" hasSetFun="true"
	documentId="position_id" documentName="position_name" />
<g:LayOutWinTag buttonId="showDicState" title="选择状态" hasHtmlFrame="true"
	html="${ctx}/dic/state/stateSelect.action?type=instrument"
	isHasSubmit="false" functionName="showDicStateFun" hasSetFun="true"
	documentId="state_id" documentName="state_name" />
<g:LayOutWinTag buttonId="searchdepartment" title="选择组织" hasHtmlFrame="true" width="800" height="520" html="${ctx}/core/department/departmentSelect.action" isHasSubmit="false" functionName="searchdepartmentFun" hasSetFun="true" documentId="department_id" documentName="department_name" />
	<g:LayOutWinTag buttonId="searchUser" title="选择负责人" hasHtmlFrame="true" hasSetFun="true" html="${ctx}/core/user/userSelect.action" isHasSubmit="false" functionName="dutyUserFun" documentId="dutyUser_id" documentName="dutyUser_name" />
	<g:LayOutWinTag buttonId="showPosition" title="选择位置" hasHtmlFrame="true" html="${ctx}/storage/common/showStoragePositionTree.action" isHasSubmit="false" functionName="searchPosition" hasSetFun="true" documentId="position_id" documentName="position_name" />	
	
</head>
<body>
	<script type="text/javascript"
		src="${ctx}/js/system/template/showInstrumentList.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/handleSearchForm.js"></script>
	<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
	<div class="mainlistclass" id="markup">
			<div id="jstj" style="display: none">
			<table cellspacing="0" cellpadding="0" class="toolbarsection">
				<tr>
					<td valign="top" align="right" class="controlTitle" nowrap>
						<span>&nbsp;</span>
						<label class="text label" title="输入查询条件"> 编号</label>
					</td>
					<td nowrap class="quicksearch_control" valign="middle">
						<input type="text" class="input_parts text input default  readonlyfalse false" name="id" onblur="textbox_ondeactivate(event);" searchField="true" onfocus="shared_onfocus(event,this)" id="id" title="输入ID">
					</td>
					<td valign="top" align="right" class="controlTitle" nowrap>
						<span>&nbsp;</span>
						<label class="text label" title="输入查询条件"> 描述</label>
					</td>
					<td nowrap class="quicksearch_control" valign="middle">
						<input type="text" class="input_parts text input default  readonlyfalse false" name="note" onblur="textbox_ondeactivate(event);" searchField="true" onfocus="shared_onfocus(event,this)" id="note" title="输入描述">
					</td>
				</tr>
				<tr>
					<td valign="top" align="right" class="controlTitle" nowrap>
						<span class="labelspacer">&nbsp;&nbsp;</span>
						<label title="状态" class="text label"> 状态 </label>
					</td>
					<td nowrap>
						<input type="text" class="input_parts text input default  readonlyfalse false" name="" id="state_name" readonly="readonly" value="" readonly="readonly">
						<img alt='选择' id='showDicState' src='${ctx}/images/img_lookup.gif' class='detail' />
						<input type="hidden" name="state.id" searchField="true" id="state_id" value="" />
					</td>
					<td valign="top" align="right" class="controlTitle" nowrap>
						<label title="负责人" class="text label"> 负责人 </label>
					</td>
					<td nowrap>
						<input type="text" class="input_parts text input default  readonlyfalse false" readonly="readOnly" id="dutyUser_name" value='' title="负责人 " maxlength="30">
						<img title='选择' id="searchUser" src='${ctx}/images/img_lookup.gif' class='detail' />
						<input type="hidden" searchField="true" name="dutyUser.id" id="dutyUser_id" value=''>
					</td>
					<%-- <td valign="top" align="right" class="controlTitle" nowrap>
						<span class="labelspacer">&nbsp;&nbsp;</span>
						<label title="组织机构" class="text label"> 组织机构 </label>
					</td>
					<td nowrap>
						<input type="text" class="input_parts text input default  readonlyfalse false" readonly="readOnly" name="" id="department_name" title="组织机构  " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" maxlength="20">
						<img title='选择' id='searchdepartment' src='${ctx}/images/img_lookup.gif' class='detail' />
						<input type="hidden" name="department.id" searchField="true" id="department_id">
					</td> --%>
					</tr>
					<tr>
					<td valign="top" align="right" class="controlTitle" nowrap>
						<span class="labelspacer">&nbsp;&nbsp;</span>
						<label title="位置" class="text label"> 位置 </label>
					</td>
					<td nowrap>
						<input type="text" class="input_parts text input default  readonlyfalse false" readonly="readOnly" id="position_name" title="位置 " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" maxlength="20">
						<img title='选择' id='showPosition' src='${ctx}/images/img_lookup.gif' class='detail' />
						<input type="hidden" searchField="true" name="position.id" id="position_id">
					</td>
					

					<td>
						<span>&nbsp;</span>
						<label class="text label" title="输入查询条件"> 记录数</label>
					</td>
					<td nowrap class="quicksearch_control" valign="middle">
						<input type="text" class="input_parts text input default  readonlyfalse false" name="limitNum" onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" id="limitNum" title="输入记录数">
					</td>
				</tr>
			</table>
		</div>
		<input type="hidden" id="id" value="" />
		<g:GridTag isAutoWidth="true" col="${col}" type="${type}"
			title="${title}" url="${path}" />
	</div>

</body>
</html>