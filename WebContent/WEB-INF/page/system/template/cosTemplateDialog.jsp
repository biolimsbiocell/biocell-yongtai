﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML>
<html>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<head>
		<title>
			<fmt:message key="biolims.common.noTitleDocuments" />
		</title>
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
		<style>
			.dataTables_scrollBody {
				min-height: 100px;
			}
			
			.tablebtns {
				float: right;
			}
			
			.dt-buttons {
				margin: 0;
				float: right;
			}
			
			.chosed {
				background-color: #5AC8D8 !important;
				color: #fff;
			}
		</style>
	</head>
	<body>
	<div class="col-md-4 col-sm-3 col-xs-12">
		<div class="input-group">
			<span class="input-group-addon">编号</span> 
			<input type="text" size="20" maxlength="25" id="id"  searchname="id" class="form-control" placeholder="编号查询" >
		</div>
	</div>
	<div class="col-md-4 col-sm-3 col-xs-12">
			<div class="input-group">
			<span class="input-group-addon">名称</span> 
			<input type="text" size="20" maxlength="25" id="name" searchname="name" class="form-control"  placeholder="名称查询">
		</div>
	</div>
	<div class="col-md-4 col-sm-3 col-xs-12">
			<div class="input-group">
			<span class="input-group-addon">房间</span> 
			<input type="text" size="30" maxlength="25" id="roomName" searchname="roomManagement-roomName" class="form-control" value="${requestScope.roomName}" placeholder="房间名称查找">
		</div>
	</div>
	<div class="col-md-1 col-sm-3 col-xs-12">
		<div class="input-group">
			<input style="text-align: center;" type="button" class="form-control btn-success" onclick="query()" value="查找">
		</div>
	</div>
		<div id="cosDialogTable" class="ipadmini">
			<table class="table table-hover table-bordered table-condensed" id="addCos" style="font-size: 12px;"></table>
		</div>
			<input type="hidden" id="cosId" value="${requestScope.typeId}" />
		<script type="text/javascript" src="${ctx}/js/system/template/cosTemplateDialogTable.js"></script>

	</body>

</html>