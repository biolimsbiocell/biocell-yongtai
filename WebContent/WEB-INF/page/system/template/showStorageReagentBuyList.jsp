<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/system/template/showStorageReagentBuyList.js"></script>
<script type="text/javascript" src="${ctx}/javascript/commonSearch.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>
<input type="hidden" id="extJsonDataString" name="extJsonDataString">
		<table cellspacing="0" cellpadding="0" class="toolbarsection">
			<tr>
				<td class="text label" ><label>&nbsp;&nbsp;&nbsp;批次&nbsp;&nbsp;&nbsp;&nbsp;</label>
				</td>
				<td>
					<input type="text" id="code" name="code" searchField="true" onkeydown="disableEnter(event)" >
				</td>
				<!-- <td>
					<input type="button" onClick="selectWaitSamples()" style="font-weight:bold;" value="  查  询  ">
				</td> -->
			</tr>
		</table>
		<div id="showStorageReagentBuydiv"></div>
		<input type="hidden" id="codes" value="${requestScope.codes}"></input>
</body>
</html>


