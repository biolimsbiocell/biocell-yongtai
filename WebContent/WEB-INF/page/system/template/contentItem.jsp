﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%-- <%@ include file="/WEB-INF/page/include/common.jsp"%> --%>
</head>
<body>
	<div>
		<table
			class="table table-hover table-striped table-bordered table-condensed"
			id="contentItem" style="font-size: 14px;">
		</table>
	</div>
	<script type="text/javascript"
		src="${ctx}/js/system/template/contentItem.js"></script>
</body>
</html>


