﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<link rel="stylesheet" href="${ctx}/css/experimentLab.css" />
<style type="text/css">
.btn-sm:hover {
	background: #17B978;
	color: #fff;
}

.input-group {
	margin-top: 10px;
}

.layui-layer-content {
	padding: 0 18px;
}

#contentItem_info {
	display: none;
}
</style>
</head>

<body>
	<input type="hidden" id="" value="${requestScope.itemList}" />
	<input type="hidden" id="" value="${requestScope.cosTypeList}" />
	<input type="hidden" id="" value="${requestScope.reagentList}" />
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">
	<div class="container-fluid" style="">
		<input type="hidden" id="bpmTaskId" value="${requestScope.bpmTaskId}" />
		<section class="content">
		<div class="row">
			<!--表格-->
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<fmt:message key="biolims.common.experimentPattern" />
						<small style="color: #fff;"> <fmt:message
								key="biolims.common.serialNumber" />: <span id="template_id">
								<s:property value="template.id" />
						</span> <fmt:message key="biolims.common.commandPerson" />: <span
							userId="<s:property value="template.createUser.id"/>"
							id="template_createUser"> <s:property
									value="template.createUser.name" />
						</span> <fmt:message key="biolims.common.commandTime" />: <span
							id="template_createDate"> <s:date
									name="template.createDate" format="yyyy-MM-dd" />
						</span>
						</small>
					</h3>
				</div>
				<div class="box-body">
					<!--步骤数-->
					<input type="hidden" class="form-control" id="stepsNum"
						value="<s:property value=" template.stepsNum "/>" />
					<!--步骤儿-->
					<div class="col-xs-1">
						<div id="wizard_verticle" class="form_wizard wizard_verticle"
							style="padding-top: 50px;">
							<ul class="list-unstyled wizard_steps">

							</ul>
						</div>
					</div>
					<!--步骤儿明细-->
					<div class="col-xs-11">
						<div class="box box-primary box-solid">
							<div class="box-header with-border">
								<i class="glyphicon glyphicon-leaf"></i>
								<h3 class="box-title">
									<fmt:message key="biolims.common.templateDetail" />
								</h3>
								<div class="box-tools pull-right">
									<button type="button" id="stepContentBtn"
										class="btn btn-box-tool" onclick="removeStepItem()"
										style="border: 1px solid;">
										<i class="fa fa-trash"></i>
										<fmt:message key="biolims.common.delete" />
									</button>
									<button type="button" id="stepContentBtn"
										class="btn btn-box-tool" onclick="animateEdit(this)"
										style="font-size: 20px; border: 1px solid;">
										<i class="fa fa-file-word-o"></i>
									</button>
									<button type="button" id="stepSaveBtn" class="btn btn-box-tool"
										style="border: 1px solid;" onclick="saveStepItem()">
										<fmt:message key="biolims.common.save" />
									</button>
								</div>
							</div>
							<div class="box-body">
								<!--实验步骤（富文本编辑器）-->
								<div class="col-md-12" id="stepContentModer"
									style="position: absolute; z-index: 6; height: 0%; right: 10px; width: 0%; overflow: hidden;">
									<div class="box box-success">
										<div class="box-header">
											<i class="glyphicon glyphicon-leaf"></i>
											<h3 class="box-title">
												<fmt:message key="biolims.common.stepDetail" />
											</h3>
											<div class="box-tools pull-right"></div>
										</div>
										<div class="box-body" style="height: 100%; overflow: hidden;">
											<form>
												<textarea id="editor1" name="editor1" rows="10" cols="80">
                                            			</textarea>
											</form>
										</div>
									</div>
								</div>
								<!--模板明细-->
								<div class="col-md-6">
									<div class="box box-primary">
										<div class="box-header">
											<i class="fa fa-paw"></i>
											<h3 class="box-title">
												<fmt:message key="biolims.common.stepDetail" />
											</h3>
											<div class="box-tools pull-right"></div>
										</div>
										<div class="box-body">
											<div class="row">
												<div class="col-xs-12 col-sm-12">
													<div class="input-group">
														<span class="input-group-addon"> <fmt:message
																key="biolims.common.serialNumber" />
														</span> <input type="text" class="form-control"
															id="template_code" />
													</div>
												</div>
												<div class="col-xs-12 col-sm-12">
													<div class="input-group">
														<span class="input-group-addon"><fmt:message
																key="biolims.user.eduName" /> <img
															class='requiredimage' src='${ctx}/images/required.gif' />
														</span> <input type="text" class="form-control"
															id="template_name" />
													</div>
												</div>
												<%-- 							<div class="col-xs-12 col-sm-6">
															<div class="input-group">
																<span class="input-group-addon"><fmt:message key="biolims.common.testUserName" />
															
																</span>
																<input type="text" class="form-control" id="test_User_Name" />
																<span class="input-group-btn">
											                	<button class="btn btn-info" type="button" 
													              onClick="showdutyManId()">
													                       <i class="glyphicon glyphicon-search"></i>
												                </button>
												                 </span>		
															</div>
														</div> --%>
												<div class="col-xs-12 col-sm-12">
													<div class="input-group">
														<span class="input-group-addon"><fmt:message
																key="biolims.common.estimatedTime" /> </span> <input
															type="text" class="form-control" id="estimated_time" />

													</div>
												</div>
											</div>

										</div>
									</div>
								</div>
								<!--质检内容-->
								<div class="col-md-6">
									<div class="box box-primary">
										<div class="box-header with-border">
											<i class="fa fa-flask"></i>
											<h3 class="box-title quality">质检内容</h3>
											<small id="zjType"></small>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-sm btn-box-tool"
													onclick="chosezhijian()" style="border: 1px solid;">
													<i class="fa fa-search-plus"></i> 质检项
												</button>
												<!-- <button type="button" class="btn btn-sm btn-box-tool" onclick="choseType()" style="border: 1px solid;">
															<i class="fa fa-search-plus"></i>
															质检类型
                										</button> -->
											</div>
										</div>
										<div class="box-body">
											<ul class="todo-list" id="zhijianBody"
												style="min-height: 150px; max-height: 251px;">
												<li class="nozhijian"><span> <i
														class="glyphicon glyphicon-th-list"></i>
												</span>
													<p class="text">请选择质检内容</p></li>
											</ul>
										</div>
									</div>
								</div>
								<!--原辅料 -->
								<div class="col-md-6">
									<div class="box box-primary">
										<div class="box-header with-border">
											<i class="fa fa-flask"></i>
											<h3 class="box-title">
												<fmt:message key="biolims.common.reagentName" />
											</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-sm btn-box-tool"
													onclick="choseReagent()" style="border: 1px solid;">
													<i class="fa fa-search-plus"></i>
													<fmt:message key="biolims.common.selectReagent" />
												</button>
											</div>
										</div>
										<div class="box-body">
											<ul class="todo-list" id="reagentBody"
												style="min-height: 150px; max-height: 251px;">
												<li class="noReag"><span> <i
														class="glyphicon glyphicon-th-list"></i>
												</span>
													<p class="text">
														<fmt:message key="biolims.common.pleaseSelectReagent" />
													</p></li>
											</ul>
										</div>
									</div>
								</div>
								<!--设备-->
								<div class="col-md-6">
									<div class="box box-primary">
										<div class="box-header with-border">
											<i class="fa fa-balance-scale"></i>
											<h3 class="box-title">
												<fmt:message key="biolims.common.instrumentName" />
											</h3>
											<div class="box-tools pull-right">
												<button type="button" class="btn btn-sm btn-box-tool"
													onclick="choseCos()" style="border: 1px solid;">
													<i class="fa fa-search-plus"></i>
													<fmt:message key="biolims.common.selectEquType" />
												</button>
											</div>
										</div>
										<div class="box-body">
											<ul class="todo-list" id="cosBody"
												style="min-height: 150px; max-height: 251px;">
												<li class="noCos"><span> <i
														class="glyphicon glyphicon-th-list"></i>
												</span>
													<p class="text">
														<fmt:message key="biolims.common.pleaseselectEquType" />
													</p></li>
											</ul>
										</div>
									</div>
								</div>
								<!--自定义字段儿-->
								<div class="col-md-12">
									<div class="box box-success">
										<div class="box-header">
											<i class="fa fa-building-o"></i>
											<h3 class="box-title">
												<fmt:message key="biolims.common.customFields" />
											</h3>
										</div>
										<div class="box-body" id="contentData"
											style="max-height: 300px;">
											<table
												class="table table-hover table-striped table-bordered table-condensed"
												id="contentItem" style="font-size: 14px;">
											</table>
										</div>
										<div class="box-footer" id="layerItem" style="display: none;">
											<div class="col-xs-12 col-sm-6">
												<div class="input-group">
													<span class="input-group-addon"><fmt:message
															key="biolims.common.fieldDisplayName" /> </span> <input
														type="text" class="form-control" />
												</div>
											</div>
											<div class="col-xs-12 col-sm-6">
												<div class="input-group">
													<span class="input-group-addon"><fmt:message
															key="biolims.common.fieldCodedName" /> </span> <input
														type="text" class="form-control" />
												</div>
											</div>
											<div class="col-xs-12 col-sm-6">
												<div class="input-group">
													<span class="input-group-addon"><fmt:message
															key="biolims.common.fieldType" /></span> <input type="text"
														class="form-control datatype" disabled />
												</div>
											</div>
											<div class="col-xs-12 col-sm-6">
												<div class="input-group">
													<span class="input-group-addon"><fmt:message
															key="biolims.common.required" /></span> <select
														class="form-control">
														<option><fmt:message key="biolims.common.no" /></option>
														<option><fmt:message key="biolims.common.yes" /></option>
													</select>
												</div>
											</div>
											<div class="col-xs-12 col-sm-6">
												<div class="input-group">
													<span class="input-group-addon"><fmt:message
															key="biolims.common.defaultValue" /> </span> <input type="text"
														class="form-control" />
												</div>
											</div>
											<div class="col-xs-12 col-sm-6">
												<div class="input-group">
													<span class="input-group-addon"><fmt:message
															key="biolims.common.isReadOnly" /> </span> <select
														class="form-control">
														<option><fmt:message key="biolims.common.no" /></option>
														<option><fmt:message key="biolims.common.yes" /></option>
													</select>
												</div>
											</div>
											<div class="col-xs-12 col-sm-6">
												<div class="input-group">
													<span class="input-group-addon"><fmt:message
															key="biolims.common.qualityChecking" /> </span> <select
														class="form-control">
														<option><fmt:message key="biolims.common.no" /></option>
														<option><fmt:message key="biolims.common.yes" /></option>
													</select>
												</div>
											</div>
											<div id="choseCheekBox" class="col-xs-12"
												style="margin-top: 10px; display: none;">
												<p>
													<i class="glyphicon glyphicon-asterisk"></i>
													<fmt:message key="biolims.common.optionInformation" />
												</p>
												<div class="choseCheekBox">
													<div class="col-xs-12 col-sm-6">
														<div class="input-group">
															<span class="input-group-addon"><fmt:message
																	key="biolims.common.optionValue" /> </span> <input
																type="text" class="form-control input-sm"
																placeholder="0" />
														</div>
													</div>
													<div class="col-xs-12 col-sm-6">
														<div class="input-group">
															<span class="input-group-addon"><fmt:message
																	key="biolims.common.optionName" /> </span> <input type="text"
																class="form-control input-sm" placeholder="basketball" />
														</div>
													</div>
												</div>
												<div class="choseCheekBox">
													<div class="col-xs-12 col-sm-6">
														<div class="input-group">
															<span class="input-group-addon"><fmt:message
																	key="biolims.common.optionValue" /> </span> <input
																type="text" class="form-control input-sm"
																placeholder="1" />
														</div>
													</div>
													<div class="col-xs-12 col-sm-6">
														<div class="input-group">
															<span class="input-group-addon"><fmt:message
																	key="biolims.common.optionName" /> </span> <input type="text"
																class="form-control input-sm" placeholder="football" />
														</div>
													</div>
												</div>
												<div class="text-center">
													<button class="btn btn-xs btn-info addItemTembtn"
														style="margin: 10px 0; display: none;">
														<i class="glyphicon glyphicon-plus"></i>
														<fmt:message key="biolims.common.AddTo" />
													</button>
												</div>

											</div>
										</div>
									
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="button" class="btn btn-primary"
							onclick="preTemplateStep()">
							<i class="glyphicon glyphicon-arrow-up"></i>
							<fmt:message key="biolims.common.back" />
						</button>
					</div>

				</div>
			</div>
		</div>
		</section>
	</div>
	<script type="text/javascript"
		src="${ctx}/lims/plugins/ckeditor/ckeditor.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/system/template/templateEditStep2Old.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/system/template/contentItemOld.js"></script>
</body>

</html>