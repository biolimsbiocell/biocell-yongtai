﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style type="text/css">
.box-title small span {
	margin-right: 20px;
}

.row {
	margin-bottom: 20px !important;
}
</style>
</head>

<body>
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 60px">
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<fmt:message key="biolims.common.experimentalTemplate" />
						<small style="color: #fff;"> <fmt:message
								key="biolims.common.codingA" />: <span id="template_id"><s:property
									value="template.id" /></span> <fmt:message
								key="biolims.tStorage.createUser" />: <span
							userId="<s:property value="template.createUser.id"/>"
							id="template_createUser"> <s:property
									value="template.createUser.name" />
						</span> <fmt:message key="biolims.tStorage.createDate" />: <span
							id="template_createDate"> <s:date
									name="template.createDate" format="yyyy-MM-dd" />
						</span>
						</small>
					</h3>
					<button id="history_version_btn" class="btn btn-sm btn-warning" style="display: none;" onclick="choseHistoryVersionSop()">选择历史版本SOP</button>
				</div>
				<div class="box-body">
					<input type="hidden" id="chose_template_id"/>
					<form name="form1" class="layui-form" id="form1" method="post">
						<input type="hidden" id="bpmTaskId"
							value="<%=request.getParameter("bpmTaskId")%>" /><br> <input
							type="hidden"
							value='<s:date name="template.createDate" format="yyyy-MM-dd" />'
							name="template.createDate"> <input type="hidden"
							value="<s:property value="template.id" />" name="template.id">
						<input type="hidden"
							value="<s:property value="template.createUser.id"/>"
							name="template.createUser.id">
							<input type="hidden"
							value="<s:property value="template.type"/>"
							name="template.type">
						<div class="row">
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.tStorage.name" /><img class='requiredimage'
										src='${ctx}/images/required.gif' /> </span> <input type="text"
										size="50" class="form-control"
										changelog="<s:property value="template.name"/>"
										id="template_name" name="template.name"
										title="<fmt:message key=" biolims.common.describe "/>"
										value="<s:property value=" template.name "/>" />
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.temProductType" /><img
										class='requiredimage' src='${ctx}/images/required.gif' /></span><input
										changelog="<s:property value="template.testType.name"/>"
										type="text" size="20" class="form-control" readonly="readOnly"
										id="template_testType_name" name="template.testType.name"
										value="<s:property value=" template.testType.name "/>" /><input
										changelog="<s:property value="template.testType.id"/>"
										type="hidden" id="template_testType"
										name="template.testType.id"
										value="<s:property value=" template.testType.id "/>"><input
										changelog="<s:property value="template.testType.sysCode"/>"
										type="hidden" id="template_testType_sysCode"
										name="template.testType.sysCode"
										value="<s:property value=" template.testType.sysCode "/>">
									<span class="input-group-btn"><button
											class="btn btn-info" type="button" onclick="sylxCheck()">
											<i class="glyphicon glyphicon-search"></i>
										</button></span>
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.experiProductNumberOfDays" /> </span><input
										type="text" size="20" maxlength="25"
										changelog="<s:property value="template.duringDays"/>"
										id="template_duringDays" name="template.duringDays"
										class="form-control"
										title="<fmt:message key=" biolims.common.experimentNumberOfDays "/>"
										value="<s:property value=" template.duringDays "/>" />
								</div>
							</div>

							<%-- 							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.containerType" /></span><input
											changelog="<s:property value="template.storageContainer.name"/>"
										type="text" size="20" class="form-control" readonly="readOnly"
										id="template_storageContainer_name"
										name="template.storageContainer.name"
										value="<s:property value=" template.storageContainer.name "/>" /><input
										changelog="<s:property value="template.storageContainer.id"/>"
										type="hidden" id="template_storageContainer"
										name="template.storageContainer.id"
										value="<s:property value=" template.storageContainer.id "/>">
									<span class="input-group-btn"><button
											class="btn btn-info" type="button" onclick="containerCheck()">
											<i class="glyphicon glyphicon-search"></i>
										</button></span>
								</div>
							</div> --%>
						</div>

						<div class="row">

							<%-- <div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message key="biolims.common.qualityProductQuantity" />
									</span> <input type="text" size="50" class="form-control"
									changelog="<s:property value="template.qcNum"/>"
										id="template_qcNum" name="template.qcNum" title=""
										value="<s:property value=" template.qcNum "/>" />
								</div>
							</div> --%>
							<%--<div class="col-xs-4">
								 <div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.barCodeTemplate" /></span><input
										changelog="<s:property value="template.codeMain.name"/>"
										type="text" size="20" readonly="readOnly"
										id="template_codeMain_name" class="form-control"
										value="<s:property value=" template.codeMain.name "/>" /> <input
										changelog="<s:property value="template.codeMain.id"/>"
										type="hidden" id="template_codeMain"
										name="template.codeMain.id"
										value="<s:property value=" template.codeMain.id "/>">
									<span class="input-group-btn"><button
											class="btn btn-info" type="button" onclick="selCode()">
											<i class="glyphicon glyphicon-search"></i>
										</button></span>
								</div> 
							</div>--%>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.productTaskResult" /></span> <input type="text"
										size="20" maxlength="25"
										changelog="<s:property value="template.templateFieldsItem"/>"
										id="template_templateFieldsItem" class="form-control"
										name="template.templateFieldsItem" title="选择实验结果"
										value="<s:property value=" template.templateFieldsItem "/>"
										readonly="readonly" /> <input type="hidden"
										changelog="<s:property value="template.templateFieldsItemCode"/>"
										id="template_templateFieldsItemCode"
										name="template.templateFieldsItemCode"
										value="<s:property value=" template.templateFieldsItemCode "/>" />
									<span class="input-group-btn"><button
											class="btn btn-info" type="button" chose="result"
											onclick="showTemplateFields(this)">
											<i class="glyphicon glyphicon-search"></i>
										</button>
										<button class="btn btn-info" type="button" onclick="qkjg()">
											<i class="glyphicon glyphicon-remove"></i>
										</button></span>
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.stepName" /><img class='requiredimage'
										src='${ctx}/images/required.gif' /></span> <input
										changelog="<s:property value="template.stepsNum"/>"
										type="number" id="template_stepsNum" class="form-control"
										name="template.stepsNum"
										value="<s:property value=" template.stepsNum "/>" required />
								</div>
							</div>

							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.ProducttalWarningNumberOfDaysInAdvance" />
									</span><input type="text" size="20" maxlength="25"
										changelog="<s:property value="template.remindDays"/>"
										id="template_remindDays" name="template.remindDays"
										class="form-control"
										title="<fmt:message key=" biolims.common.experimentalWarningNumberOfDaysInAdvance "/>"
										value="<s:property value=" template.remindDays "/>" />
								</div>
							</div>
						</div>
						<!-- <div class="row"> -->
						<%-- 	<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.sampleConsume" />
									</span><input type="text" size="20" maxlength="25"
									changelog="<s:property value="template.sampleNum"/>"
										id="template_sampleNum" name="template.sampleNum"
										class="form-control"
										title="<fmt:message key=" biolims.common.sampleConsume "/>"
										value="<s:property value=" template.sampleNum "/>" />
								</div>
							</div> --%>
						<!-- </div> -->
						<div class="row">
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.multipleProductTypes" /></span><input
										changelog="<s:property value="template.dicSampleTypeName"/>"
										type="text" size="20" readonly="readOnly"
										name="template.dicSampleTypeName"
										id="template_dicSampleTypeName" class="form-control"
										value="<s:property value=" template.dicSampleTypeName "/>" />
									<input type="hidden" size="20" readonly="readOnly"
										changelog="<s:property value="template.dicSampleTypeId"/>"
										name="template.dicSampleTypeId" id="template_dicSampleTypeId"
										value="<s:property value=" template.dicSampleTypeId "/>" /><span
										class="input-group-btn"><button class="btn btn-info"
											type="button" onclick="loadTestDicSampleType1()">
											<i class="glyphicon glyphicon-search"></i>
										</button>
										<button class="btn btn-info" type="button" onclick="dgmx1()">
											<i class="glyphicon glyphicon-remove"></i>
										</button></span>
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.multipleProductnum" /> </span><input
										type="text" size="20" maxlength="25"
										changelog="<s:property value="template.productNum1"/>"
										id="template_productNum1" name="template.productNum1"
										class="form-control"
										title="<fmt:message key=" biolims.common.dicSampleNum "/>"
										value="<s:property value=" template.productNum1 "/>" />
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.temPUserType" /></span><input
										changelog="<s:property value="template.acceptUser.name"/>"
										type="text" size="20" readonly="readOnly"
										id="template_acceptUser_name" class="form-control"
										value="<s:property value=" template.acceptUser.name "/>" /> <input
										changelog="<s:property value="template.acceptUser.id"/>"
										type="hidden" id="template_acceptUser"
										name="template.acceptUser.id"
										value="<s:property value=" template.acceptUser.id "/>"><span
										class="input-group-btn"><button class="btn btn-info"
											type="button" onclick="showacceptUser()">
											<i class="glyphicon glyphicon-search"></i>
										</button></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.singleProductTypes" /></span><input
										changelog="<s:property value="template.dicSampleType.name"/>"
										type="text" size="20" readonly="readOnly"
										id="template_dicSampleType_name" class="form-control"
										value="<s:property value=" template.dicSampleType.name "/>" />
									<input type="hidden" id="template_dicSampleType"
										changelog="<s:property value="template.dicSampleType.id"/>"
										name="template.dicSampleType.id"
										value="<s:property value=" template.dicSampleType.id "/>"><span
										class="input-group-btn"><button class="btn btn-info"
											type="button" onclick="loadTestDicSampleType()">
											<i class="glyphicon glyphicon-search"></i>
										</button>
										<button class="btn btn-info" type="button" onclick="dgmx()">
											<i class="glyphicon glyphicon-remove"></i>
										</button></span>
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.singleProductnum" /> </span><input type="text"
										size="20" maxlength="25"
										changelog="<s:property value="template.productNum"/>"
										id="template_productNum" name="template.productNum"
										class="form-control"
										title="<fmt:message key=" biolims.common.dicSampleNum "/>"
										value="<s:property value=" template.productNum "/>" />
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.ProductdDetail" /></span> <input
										changelog="<s:property value="template.templateFields"/>"
										type="text" size="20" maxlength="25"
										id="template_templateFields" name="template.templateFields"
										title="选择生产明细" class="form-control"
										value="<s:property value=" template.templateFields "/>"
										readonly="readonly" /> <input type="hidden"
										changelog="<s:property value="template.templateFieldsCode"/>"
										id="template_templateFieldsCode"
										name="template.templateFieldsCode"
										value="<s:property value=" template.templateFieldsCode "/>" />
									<span class="input-group-btn"><button
											class="btn btn-info" type="button" chose="item"
											onclick="showTemplateFields(this)">
											<i class="glyphicon glyphicon-search"></i>
										</button>
										<button class="btn btn-info" type="button" onclick="qkmx()">
											<i class="glyphicon glyphicon-remove"></i>
										</button></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.state" /></span> <select class="form-control"
										name="template.status" id="template_status">
										<option value="1"
											<s:if test="template.status==1">selected="selected"</s:if>><fmt:message
												key="biolims.common.effective" /></option>
										<option value="0"
											<s:if test="template.status==0">selected="selected"</s:if>><fmt:message
												key="biolims.common.invalid" /></option>

									</select>
								</div>
							</div>
							<%-- <div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message key="biolims.test.mixSample" /></span> <select class="form-control" name="template.isBlend">
										<option value="0"
											<s:if test="template.isBlend==0">selected="selected"</s:if>><fmt:message key="biolims.common.no" /></option>
										<option value="1"
											<s:if test="template.isBlend==1">selected="selected"</s:if>><fmt:message key="biolims.common.yes" /></option>
									</select>
								</div>
							</div> --%>
							<!-- 						</div>
						<div class="row"> -->
							<%-- 								<input type="hidden" id="template_moduleType" name="template.moduleType" 
								value="<s:property value=" template.moduleType "/>"/>
						<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message key="biolims.common.isSeparate" /></span> <select class="form-control" name="template.isSeparate">
										<option value="0"
											<s:if test="template.isSeparate==0">selected="selected"</s:if>><fmt:message key="biolims.common.no" /></option>
										<option value="1"
											<s:if test="template.isSeparate==1">selected="selected"</s:if>><fmt:message key="biolims.common.yes" /></option>
									</select>
								</div>
							</div> --%>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">结果是否为原辅料<%-- <fmt:message key="biolims.common.isSeparate" /> --%>
									<img class='requiredimage' src='${ctx}/images/required.gif' /></span>
									<select class="form-control" name="template.rightStorage" id="template_rightStorage">
										<option value=""
											<s:if test='template.rightStorage=="0"'>selected="selected"</s:if>>请选择</option>
										<option value="0"
											<s:if test="template.rightStorage==0">selected="selected"</s:if>><fmt:message
												key="biolims.common.no" /></option>
										<option value="1"
											<s:if test="template.rightStorage==1">selected="selected"</s:if>><fmt:message
												key="biolims.common.yes" /></option>
									</select>
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon"><%-- <img
										class='requiredimage' src='${ctx}/images/required.gif' /> --%><fmt:message
											key="biolims.common.chcklishiji" /></span> <input
										changelog="<s:property value="template.storage.name"/>"
										type="text" size="20" maxlength="25"
										id="template_templateFields_id" name="template.storage.name"
										title="选择原辅料类型" class="form-control"
										value="<s:property value=" template.storage.name"/>"
										readonly="readonly" /> <input type="hidden"
										changelog="<s:property value="template.storage.id"/>"
										id="template_templateFieldsCode_name"
										name="template.storage.id"
										value="<s:property value=" template.storage.id "/>" /> <span
										class="input-group-btn"><button class="btn btn-info"
											type="button" chose="item" onclick="choseReagent()">
											<i class="glyphicon glyphicon-search"></i>
										</button>
										<button class="btn btn-info" type="button" onclick="qksslx()">
											<i class="glyphicon glyphicon-remove"></i>
										</button></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">依据文件编号</span>
									<input type="text"
										size="20" maxlength="25"
										changelog="<s:property value="template.documentNum"/>"
										id="template_documentNum" name="template.documentNum"
										class="form-control" title="依据文件编号"
										value="<s:property value=" template.documentNum"/>" />
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">依据文件名称</span>
									<input type="text"
										size="20" maxlength="25"
										changelog="<s:property value="template.documentName"/>"
										id="template_documentName" name="template.documentName"
										class="form-control" title="依据文件名称"
										value="<s:property value="template.documentName"/>" />
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">版本号<img class='requiredimage' src='${ctx}/images/required.gif' /></span>
									<input type="text"
										size="20" maxlength="25"
										changelog="<s:property value="template.versionNum"/>"
										id="template_versionNum" name="template.versionNum"
										class="form-control" title="版本号"
										value="<s:property value="template.versionNum"/>" />
								</div>
							</div>
							</div>
						<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.auditor" /><img class="requiredimage" src="/images/required.gif"></span> <input type="text"
											changelog="<s:property value="template.confirmUser.name"/>"
											size="20" readonly="readOnly"
											id="template_confirmUser_name" class="form-control"
											value="<s:property value=" template.confirmUser.name " />"
											readonly="readOnly" /> <input type="hidden"
											changelog="<s:property value="template.confirmUser.id"/>"
											id="template_confirmUser"
											name="template.confirmUser.id"
											value="<s:property value=" template.confirmUser.id " />">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onclick="showconfirmUser()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>
								
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">状态</span> <input type="text"
											changelog="<s:property value="template.stateName"/>"
											size="20" readonly="readOnly" name="template.stateName"
											id="template_stateName" class="form-control"
											value="<s:property value=" template.stateName " />"
											readonly="readOnly" /> <input type="hidden"
											changelog="<s:property value="template.state"/>"
											id="template_state"
											name="template.state"
											value="<s:property value=" template.state " />">
									</div>
								</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="input-group">
								<button type="button" class="btn btn-info btn-sm"
									onclick="fileUp()">
									<fmt:message key="biolims.common.uploadAttachment" />
								</button>
								&nbsp;&nbsp;
								<%-- <span class="text label"><fmt:message
											key="biolims.common.aTotalOf" />${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
											key="biolims.common.attachment" /> --%>
								<button type="button" class="btn btn-info btn-sm"
									onclick="fileView()">
									<fmt:message key="biolims.report.checkFile" />
								</button>
							</div>
							<!-- </div> -->
						</div>
						<!-- 日志  dwb 2018-05-12 15:31:40 -->
						<input type="hidden" id="changeLog" name="changeLog" />

					</form>
				</div>
				<div class="box-footer">
					<button class="btn btn-primary pull-right"
						onclick="nextTemplateStep()">
						<!-- 自检模板配置 -->
						<fmt:message key="biolims.common.nextStep" />
					</button>
					<%-- <button class="btn btn-primary pull-right"
						onclick="nextTemplateStep(2)">
						生产模板配置
						<fmt:message key="biolims.common.nextStep" />
					</button> --%>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript"
		src="${ctx}/js/system/template/templateEditStep1.js"></script>
</body>

</html>