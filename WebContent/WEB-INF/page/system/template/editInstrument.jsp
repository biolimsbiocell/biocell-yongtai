<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>设备管理</title>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="searchdepartment" title="选择组织" hasHtmlFrame="true" width="800" height="520" html="${ctx}/core/department/departmentSelect.action" isHasSubmit="false" functionName="searchdepartmentFun" hasSetFun="true" documentId="instrument_department_id" documentName="instrument_department_name" />
	<g:LayOutWinTag buttonId="showDutyUser" title="选择负责人" hasHtmlFrame="true" hasSetFun="true" html="${ctx}/core/user/userSelect.action" isHasSubmit="false" functionName="dutyUserFun" documentId="instrument_dutyUser_id" documentName="instrument_dutyUser_name" />
	<g:LayOutWinTag buttonId="searchbz" title="币种" hasHtmlFrame="true" html="${ctx}/dic/currency/currencyTypeSelect.action" isHasSubmit="false" functionName="bz" hasSetFun="true" documentId="instrument_currencyType_id" documentName="instrument_currencyType_name"/>
	<g:LayOutWinTag buttonId="showSupplier" title="供应商" hasHtmlFrame="true" html="${ctx}/supplier/common/supplierSelect.action" isHasSubmit="false" functionName="showSupplierFun" hasSetFun="true" extRec="rec" extStr="document.getElementById('instrument_supplier_id').value=rec.get('id');document.getElementById('instrument_supplier_name').value=rec.get('name');document.getElementById('instrument_supplier_linkTel').value=rec.get('linkTel');document.getElementById('instrument_supplier_linkMan').value=rec.get('linkMan');document.getElementById('instrument_supplier_email').value=rec.get('email');" />
	<g:LayOutWinTag buttonId="showProducer" title="生产商" hasHtmlFrame="true" html="${ctx}/supplier/common/producerSelect.action" isHasSubmit="false" functionName="showProducerFun" hasSetFun="true" extRec="rec" extStr="document.getElementById('instrument_producer_id').value=rec.get('id');document.getElementById('instrument_producer_name').value=rec.get('name');document.getElementById('instrument_producer_linkTel').value=rec.get('linkTel');document.getElementById('instrument_producer_linkMan').value=rec.get('linkMan');document.getElementById('instrument_producer_email').value=rec.get('email');" />
	<g:LayOutWinTag buttonId="showFinanceItem" title="选择财务科目" hasHtmlFrame="true" html="${ctx}/financeItem/financeItemSelect.action" isHasSubmit="false" functionName="showFinanceItemFun" hasSetFun="true" extRec="id,name" extStr="document.getElementById('instrument_financeItem_id').value=id;document.getElementById('instrument_financeItem_name').value=name;" />
	<g:LayOutWinTag buttonId="showStorageType" title="选择分类" hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action" isHasSubmit="false" functionName="device" hasSetFun="true" documentId="instrument_type_id" documentName="instrument_type_name" />
	<g:LayOutWinTag buttonId="showPosition" title="选择位置" hasHtmlFrame="true" html="${ctx}/storage/common/showStoragePositionTree.action" isHasSubmit="false" functionName="showStoragePosition" hasSetFun="true" documentId="instrument_position_id" documentName="instrument_position_name" />
<g:LayOutWinTag buttonId="regionType" title="选择城市" hasHtmlFrame="true"
		html="${ctx}/dic/type/dicTypeSelect.action" isHasSubmit="false"
		functionName="xzcs" hasSetFun="true" documentId="instrument_regionType_id"
		documentName="instrument_regionType_name" />
</s:if>
<s:if test='#request.handlemethod!="add"'>
<g:LayOutWinTag buttonId="doclinks_label" title="附件" hasHtmlFrame="true" width="900" height="500" html="${ctx}/operfile/initFileList.action?modelType=instrument&id=${instrument.id}" isHasSubmit="false" functionName="doc" /></s:if>
<g:LayOutWinTag buttonId="imglinks_img" title="图片" hasHtmlFrame="true" width="900" height="500" html="${ctx}/operfile/initFileList.action?modelType=instrument&id=${instrument.id}" isHasSubmit="false" functionName="pic" />
<g:LayOutWinTag buttonId="instrumentNote" title="详细" item="new Ext.form.TextArea({id:'instrumentNote1',value:document.getElementById('note').value})" isHasSubmit="true" functionName="instrumentNoteFun" submitUrl="document.getElementById('note').value=document.getElementById('instrumentNote1').value;Ext.getCmp('instrumentNoteFun').close();" />
<g:LayOutWinTag buttonId="showDicState" title="选择状态" hasHtmlFrame="true" html="${ctx}/dic/state/stateSelect.action?type=instrument" isHasSubmit="false" functionName="showDicStateFun" hasSetFun="true" documentId="instrument_state_id" documentName="instrument_state_name" />
<body>
	<%@ include file="/WEB-INF/page/include/toolbarNoTab.jsp"%>
	<script type="text/javascript" src="${ctx}/js/system/template/editInstrument.js"></script>
	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass">
		<s:form name="form1" theme="simple">
			<table width="100%" cellpadding="0" cellspacing="0">
				<tbody>
					<tr>
						<td>
							<table width="100%" class="section_table" cellpadding="0" >
								<tbody>
									<tr class="control textboxcontrol">
										<td class="controlTitle" nowrap>
											<!-- <span class="labelspacer">&nbsp;&nbsp;</span> -->
											<label title="设备编码" class="text label"> 设备编码 </label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
											<img alt="" class='requiredimage' src='${ctx}/images/required.gif' />
										</td>
										<td nowrap>
											<input type="text" class="input_parts text input default  readonlyfalse false"  name="instrument.id" id="instrument_id" value='<s:property value="instrument.id"/>' title="设备编码  " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="20" maxlength="30">
										</td>
										<td nowrap>&nbsp;</td>
									</tr>
									<tr class="control textboxcontrol">
									<%-- <td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="选择城市" class="text label">所在城市</label>
										</td>
										<td class="requiredcolumn" nowrap width="10px"><img
											src="/images/required.gif" class="requiredimage" alt="" /></td>
										<td nowrap><s:hidden name="instrument.regionType.id"
												id="instrument_regionType_id"></s:hidden> <s:textfield
												cssClass="input_parts text input default  readonlyfalse false"
												name="instrument.regionType.name" id="instrument_regionType_name"
												readonly="true" title="选择城市 " size="10" maxlength="20"></s:textfield>
											<img alt='选择城市' id='regionType'
											src='${ctx}/images/img_lookup.gif' name='regionType'
											class='detail' /></td>
											<td nowrap>&nbsp;</td> --%>
											</tr>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span>
											<label title="机身" class="text label">机身码 </label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
											 
										</td>
										<td nowrap>
											<input type="text" class="input_parts text input default  readonlyfalse false" name="instrument.searchCode" id="instrument_searchCode" value="<s:property value="instrument.searchCode"/>" title="机身码  " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="10" maxlength="30">
										</td>
										<td nowrap>&nbsp;</td>
									</tr>
									<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span>
													<label title="创建人" class="text label"> 创建人 </label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
													 
												</td>
												<td align="left" nowrap>
													<input type="text" class="input_parts text input default  readonlytrue false" readonly="readOnly" name="instrument.createUser.name" id="instrument_createUser_name" value="<s:property value="instrument.createUser.name"/>" title="创建人 " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="10" maxlength="20">
													<input type="hidden" name="instrument.createUser.id" id="instrument_createUser_id" value="<s:property value="instrument.createUser.id"/>">
												</td>
												<td nowrap>&nbsp;</td>
											</tr>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span>
											<label title="投用日期" class="text label"> 投用日期 </label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
											 
										</td>
										<td nowrap>
											<input type="text" Class="Wdate" readonly="readOnly"
													onfocus="WdatePicker ({skin:'ext',dateFmt:'yyyy-MM-dd'})" name="instrument.useDate" id="instrument_useDate" value="<s:date name="instrument.useDate" format="yyyy-MM-dd"/>" title="投用日期  " size="20" maxlength="30">
										</td>
										<td nowrap>&nbsp;</td>
									</tr>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span>
											<label title="状态" class="text label"> 状态 </label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
											
										</td>
										<td nowrap>
											<input type="text" class="input_parts text input default  readonlyfalse false" name="" id="instrument_state_name" readonly="readonly" value="<s:property value="instrument.state.name"/>" size="20" readonly="readonly">
											<input type="hidden" name="instrument.state.id" id="instrument_state_id" value="<s:property value="instrument.state.id"/>" />
											<img alt='选择' id='showDicState' src='${ctx}/images/img_lookup.gif' class='detail' />
										</td>
										<td nowrap>&nbsp;</td>
									</tr>
								</tbody>
							</table>
						</td>
						<td class="sectioncol " width='33%' valign="top" align="right">
							<table width="100%" class="section_table" cellpadding="0" >
								<tbody>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span>
											<label title="设备名称" class="text label">设备名称 </label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
											 
										</td>
										<td nowrap>
											<input type="text" class="input_parts text input default  readonlyfalse false" name="instrument.note" id="instrument_note" value="<s:property value="instrument.note"/>" title="设备名称  " size="50" maxlength="55">
<%-- 											<img alt='详细' id="instrumentNote" src='${ctx}/images/img_longdescription_off.gif' class='detail' />
 --%>										</td>
										<td nowrap>&nbsp;</td>
									</tr>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span>
											<label title="设备组别" class="text label">设备组别</label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
											 
										</td>
										<td nowrap>
											<input type="text" class="input_parts text input default  readonlyfalse false" name="instrument.spec" id="instrument_spec" value="<s:property value="instrument.spec"/>" title="设备组别" onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="30" maxlength="30">
										</td>
										<td nowrap>&nbsp;</td>
									</tr>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span>
											<label title="负责人" class="text label"> 负责人 </label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
											 
										</td>
										<td nowrap>
											<input type="text" class="input_parts text input default  readonlyfalse false" name="instrument.dutyUser.name" id="instrument_dutyUser_name" readonly="readonly" value="<s:property value="instrument.dutyUser.name"/>" title="负责人  " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="10" maxlength="30">
											<input type="hidden" name="instrument.dutyUser.id" id="instrument_dutyUser_id" value="<s:property value="instrument.dutyUser.id"/>"></input>
											<img alt='Select Value' id='showDutyUser' src='${ctx}/images/img_lookup.gif' name='showDutyUser' class='detail' />
										</td>
										<td nowrap>&nbsp;</td>
									</tr>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span>
											<label title="条形码" class="text label"> 条形码 </label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
											 
										</td>
										<td nowrap>
											<input type="text" class="input_parts text input default  readonlyfalse false" name="instrument.barCode" id="instrument_barCode" value="<s:property value="instrument.barCode"/>" title="条形码  " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="20" maxlength="30">
										</td>
										<td nowrap>&nbsp;</td>
									</tr>
									<tr id="doclinks">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span>
											<label title="附件" class="text label">附件</label>&nbsp;
										</td>
										<td class="requiredcolumn" nowrap width="10px"></td>
					                  <td align="left"><div id="doclinks_label" title="保存基本后,可以维护查看附件" 
						              	class="detail label">
							             <img src="${ctx}/images/img_attach.gif" />共有${requestScope.fileNum}个附件</div>
							          </td>
										<td nowrap>&nbsp;</td>
										<td nowrap></td>
										<td nowrap></td>
									</tr>
								</tbody>
							</table>
						</td>
						<td class="sectioncol ">
							<div class="section standard_section  ">
								<div class="section">
									<table width="100%" class="section_table" cellpadding="0"  id="main_attachments_table">
										<tbody>
											<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span>
													<label title="设备分类" class="text label">设备分类 </label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
													 
												</td>
												<td nowrap>
													<input type="text" class="input_parts text input default  readonlyfalse false" name="instrument.type.name" id="instrument_type_name" readonly="readonly" value="<s:property value="instrument.type.name"/>" title="分类  " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="20" maxlength="30">
													<input type="hidden" name="instrument.type.id" id="instrument_type_id" value="<s:property value="instrument.type.id"/>"></input>
													<img alt='Select Value' id='showStorageType' src='${ctx}/images/img_lookup.gif' name='showStorageType' class='detail' />
												</td>
												<td nowrap>&nbsp;</td>
											</tr>
											<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span>
													<label title="所属部门" class="text label"> 所属部门 </label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
													 
												</td>
												<td align="left" nowrap>
													<input type="text" class="input_parts text input default  readonlyfalse false" readonly="readonly" name="instrument.department.name" id="instrument_department_name" value="<s:property value="instrument.department.name"/>" title="所属部门 " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="20" maxlength="20">
													<input type="hidden" name="instrument.department.id" id="instrument_department_id" value="<s:property value="instrument.department.id"/>"></input>
													<img alt='选择' id='searchdepartment' src='${ctx}/images/img_lookup.gif' class='detail' />
												</td>
												<td nowrap>&nbsp;</td>
											</tr>
											
											<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span>
													<label title="创建日期" class="text label"> 创建日期 </label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
													 
												</td>
												<td align="left" nowrap>
													<input type="text" class="input_parts text input default  readonlytrue false" readonly="readOnly" name="instrument.createDate" id="instrument.createDate" value="<s:date name="instrument.createDate" format="yyyy-MM-dd"/>" title="创建日期 " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="20" maxlength="20">
												</td>
												<td nowrap>&nbsp;</td>
											</tr>
											<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span>
													<label title="图片" class="text label"> 图片 </label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
													 
												</td>
													<td align="left" ><div id="imglinks_img" style="width:100px;">
										<img alt="保存基本后,可以维护查看图片"
													src='${ctx}/images/img_lookup.gif' class='detail' /></div>
										</td>
												<td nowrap>&nbsp;</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			<%-- <table width="100%" class="section_table" cellpadding="0"  id="main_grid2_table">
				<tbody>
					<tr class="sectionrow " valign="top">
						<td class="sectioncol " width='50%' valign="top" id="main_grid2_row1_col1" align="right">
							<div class="section_header standard_section_header">
								<table width="100%" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td class="section_header_left text standard_section_label labelcolor" align="left">
												<span class="section_label"> 厂商信息 </span>
											</td>
											<td class="section_header_right" align="right"></td>
										</tr>
									</tbody>
								</table>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			<div class=" section_content_border">
				<table width="100%" class="section_table" cellpadding="0"  id="main_grid2_row1_col1_sec1_table">
					<tbody>
						<tr class="control textboxcontrol">
							<td valign="top" align="right" class="controlTitle" nowrap>
								<span class="labelspacer">&nbsp;&nbsp;</span>
								<label title="供应商" class="text label"> 供应商 </label>
							</td>
							<td class="requiredcolumn" nowrap width="10px">
								 
							</td>
							<td nowrap>
								<input type="text" class="input_parts text input default  readonlyfalse false" readonly="readonly" name="instrument.supplier.name" id="instrument_supplier_name" value="<s:property value="instrument.supplier.name"/>" title="供应商  " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="40" maxlength="55">
								<input type="hidden" name="instrument.supplier.id" id="instrument_supplier_id" value="<s:property value="instrument.supplier.id"/>"></input>
								<img alt='Select Value' id='showSupplier' src='${ctx}/images/img_lookup.gif' name='showSupplier' class='detail' />
								<img class="detail" src="${ctx}/images/menu_icon_link.gif" onclick="viewSupplier();" alt="查看供应商">
							</td>
							<td nowrap>&nbsp;</td>
							<td valign="top" align="right" class="controlTitle" nowrap>
								<span class="labelspacer">&nbsp;&nbsp;</span>
								<label title="供应商联系人" class="text label">供应商联系人</label>
							</td>
							<td class="requiredcolumn" nowrap width="10px">
								 
							</td>
							<td nowrap>
								<input type="text" class="input_parts text input default  readonlytrue false" name="instrument.supplier.linkMan" id="instrument_supplier_linkMan" value="<s:property value="instrument.supplier.linkMan"/>" readonly="readOnly" title="供应商联系人  " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="10" maxlength="30">
							</td>
							<td nowrap>&nbsp;</td>
							<td valign="top" align="right" class="controlTitle" nowrap>
								<span class="labelspacer">&nbsp;&nbsp;</span>
								<label title="供应商电话" class="text label">供应商电话</label>
							</td>
							<td class="requiredcolumn" nowrap width="10px">
								 
							</td>
							<td nowrap>
								<input type="text" class="input_parts text input default  readonlytrue false" name="instrument.supplier.linkTel" id="instrument_supplier_linkTel" value="<s:property value="instrument.supplier.linkTel"/>" readonly="readOnly" title="供应商电话  " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="20" maxlength="30">
							</td>
							<td nowrap>&nbsp;</td>
							<td valign="top" align="right" class="controlTitle" nowrap>
								<span class="labelspacer">&nbsp;&nbsp;</span>
								<label title="供应商email" class="text label">Email</label>
							</td>
							<td class="requiredcolumn" nowrap width="10px">
								 
							</td>
							<td nowrap>
								<input type="text" class="input_parts text input default  readonlytrue false" name="instrument.supplier.email" id="instrument_supplier_email" value="<s:property value="instrument.supplier.email"/>" readonly="readOnly" title="供应商电话  " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="20" maxlength="30">
							</td>
							<td nowrap>&nbsp;</td>
						</tr>
						<tr class="control textboxcontrol">
							<td valign="top" align="right" class="controlTitle" nowrap>
								<span class="labelspacer">&nbsp;&nbsp;</span>
								<label title="生产商" class="text label"> 生产商 </label>
							</td>
							<td class="requiredcolumn" nowrap width="10px">
								 
							</td>
							<td nowrap>
								<input type="text" class="input_parts text input default  readonlyfalse false" name="instrument.producer.name" readonly="readonly" id="instrument_producer_name" value="<s:property value="instrument.producer.name"/>" title="生产商  " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="40" maxlength="55">
								<input type="hidden" name="instrument.producer.id" id="instrument_producer_id" value="<s:property value="instrument.producer.id"/>"></input>
								<img alt='Select Value' id='showProducer' src='${ctx}/images/img_lookup.gif' name='showProducer' class='detail' />
								<img class="detail" src="${ctx}/images/menu_icon_link.gif" onclick="viewCCSupplier();" alt="查看生产商">
							</td>
							<td nowrap>&nbsp;</td>
							<td valign="top" align="right" class="controlTitle" nowrap>
								<span class="labelspacer">&nbsp;&nbsp;</span>
								<label title="生产商联系人" class="text label">生产商联系人</label>
							</td>
							<td class="requiredcolumn" nowrap width="10px">
								 
							</td>
							<td nowrap>
								<input type="text" class="input_parts text input default  readonlytrue false" name="instrument.producer.linkMan" id="instrument_producer_linkMan" value="<s:property value="instrument.producer.linkMan"/>" readonly="readOnly" title="生产商联系人  " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="10" maxlength="30">
							</td>
							<td nowrap>&nbsp;</td>
							<td valign="top" align="right" class="controlTitle" nowrap>
								<span class="labelspacer">&nbsp;&nbsp;</span>
								<label title="厂商电话" class="text label">厂商电话</label>
							</td>
							<td class="requiredcolumn" nowrap width="10px">
								 
							</td>
							<td nowrap>
								<input type="text" class="input_parts text input default  readonlytrue false" name="instrument.producer.linkTel" id="instrument_producer_linkTel" value="<s:property value="instrument.producer.linkTel"/>" readonly="readOnly" title="厂商电话  " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="20" maxlength="30">
							</td>
							<td nowrap>&nbsp;</td>
							<td valign="top" align="right" class="controlTitle" nowrap>
								<span class="labelspacer">&nbsp;&nbsp;</span>
								<label title="厂商email" class="text label">Email</label>
							</td>
							<td class="requiredcolumn" nowrap width="10px">
								 
							</td>
							<td nowrap>
								<input type="text" class="input_parts text input default  readonlytrue false" name="instrument.producer.email" id="instrument_producer_email" value="<s:property value="instrument.producer.email"/>" readonly="readOnly" title="厂商电话  " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="20" maxlength="30">
							</td>
							<td nowrap>&nbsp;</td>
						</tr>
						<tr>
							<td height="2"></td>
						</tr>
					</tbody>
				</table> --%>
			
			<table width="100%" class="section_table" cellpadding="0" >
				<tbody>
					<tr class="sectionrow ">
						<td class="sectioncol " width='50%' valign="top" id="main_grid2_row1_col1" align="right">
							<div class="section standard_section marginsection  ">
								<div class="section_header standard_section_header">
									<table width="100%" cellpadding="0" cellspacing="0">
										<tbody>
											<tr>
												<td class="section_header_left text standard_section_label labelcolor" align="left">
													<span class="section_label"> 财务信息 </span>
												</td>
												<td class="section_header_right" align="right"></td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class=" section_content_border">
									<table width="100%" class="section_table" cellpadding="0" >
										<tbody>
											<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span>
													<label title="资产号" class="text label">资产号</label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
													 
												</td>
												<td nowrap>
													<input type="text" class="input_parts text input default  readonlyfalse false" name="instrument.capitalCode" id="instrument_capitalCode" value="<s:property value="instrument.capitalCode"/>" title="资产号  " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="20" maxlength="50">
												</td>
												
												<td nowrap>&nbsp;</td>
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span>
													<label title="购入价值" class="text label">购入价值</label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
													 
												</td>
												<td nowrap>
													<input type="text" class="input_parts text input default  readonlyfalse false" name="instrument.price" id="instrument_price" value="<s:property value="instrument.price"/>" title="购入价值  " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" onkeyup="this.value=this.value.replace(/[^\d\.]/g,'')" onafterpaste="this.value=this.value.replace(/[^\d\.]/g,'')" size="20" maxlength="30">
												</td>
												<td nowrap>&nbsp;</td>
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span>
													<label title="币种" class="text label">币种</label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
													 
												</td>
												<td nowrap>
													<input type="hidden" name="instrument.currencyType.id" id="instrument_currencyType_id" value="<s:property value="instrument.currencyType.id"/>">
													<input type="text" class="input_parts text input default  readonlyfalse false" readonly="readonly" name="instrument.currencyType.name" id="instrument_currencyType_name" value="<s:property value="instrument.currencyType.name"/>" title="币种  " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="10" maxlength="30">
													<img alt='Select Value' id='searchbz' src='${ctx}/images/img_lookup.gif' name='searchbz' class='detail' />
												</td>
												<td nowrap>&nbsp;</td>
											</tr>
											<tr class="control textboxcontrol">
												
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			
		</s:form>
		<input type="hidden" id="handlemethod" value="${request.handlemethod}"></input>
		<input type="hidden" id="copyMode" value="${request.copyMode}"></input>
	</div>
</body>
</html>