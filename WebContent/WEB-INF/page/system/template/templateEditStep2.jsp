﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
		<link rel="stylesheet" href="${ctx}/css/experimentLab.css" />
		<style type="text/css">
			.todo-list>.zhijianli .label {
				margin-left: 50px;
			}
		</style>
	</head>

	<body>
		<input type="hidden" id="" value="${requestScope.itemList}" />
		<input type="hidden" id="" value="${requestScope.cosTypeList}" />
		<input type="hidden" id="" value="${requestScope.reagentList}" />
		<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
		<div class="container-fluid" style="">
			<input type="hidden" id="bpmTaskId" value="${requestScope.bpmTaskId}" />
			<section class="content">
				<div class="row">
					<!--表格-->
					<div class="box box-info box-solid">
						<div class="box-header with-border">
							<i class="fa fa-bell-o"></i>
							<h3 class="box-title">
						<fmt:message key="biolims.common.experimentPattern" />
						<small style="color: #fff;"> <fmt:message
								key="biolims.common.serialNumber" />: <span id="template_id">
								<s:property value="template.id" />
						</span> <fmt:message key="biolims.common.commandPerson" />: <span
							userId="<s:property value="template.createUser.id"/>"
							id="template_createUser"> <s:property
									value="template.createUser.name" />
						</span> <fmt:message key="biolims.common.commandTime" />: <span
							id="template_createDate"> <s:date
									name="template.createDate" format="yyyy-MM-dd" />
						</span>
						</small>
					</h3>
						</div>
						<div class="box-body">
							<!--步骤数-->
							<input type="hidden" class="form-control" id="stepsNum" value="<s:property value=" template.stepsNum "/>" />
							<!--步骤儿-->
							<div class="col-xs-1">
								<div id="wizard_verticle" class="form_wizard wizard_verticle" style="padding-top: 50px;">
									<ul class="list-unstyled wizard_steps">

									</ul>
								</div>
							</div>
							<!--步骤儿明细-->
							<div class="col-xs-11">
								<div class="box box-primary box-solid">
									<div class="box-header with-border">
										<i class="glyphicon glyphicon-leaf"></i>
										<h3 class="box-title">
									<fmt:message key="biolims.common.templateDetail" />
								</h3>
										<div class="box-tools pull-right">
											<button type="button" id="stepContentBtn" class="btn btn-box-tool" onclick="removeStepItem()" style="border: 1px solid;">
										<i class="fa fa-trash"></i>
										<fmt:message key="biolims.common.delete" />
									</button>
											<button type="button" id="stepContentBtn" class="btn btn-box-tool" onclick="animateEdit(this)" style="font-size: 20px; border: 1px solid;">
										<i class="fa fa-file-word-o"></i>
									</button>
											<button type="button" id="stepSaveBtn" class="btn btn-box-tool" style="border: 1px solid;" onclick="saveStepItem()">
										<fmt:message key="biolims.common.save" />
									</button>
										</div>
									</div>
									<div class="box-body">
										<!--实验步骤（富文本编辑器）-->
										<div class="col-md-12" id="stepContentModer" style="position: absolute; z-index: 6; height: 0%; right: 10px; width: 0%; overflow: hidden;">
											<div class="box box-success">
												<div class="box-header">
													<i class="glyphicon glyphicon-leaf"></i>
													<h3 class="box-title">
												<fmt:message key="biolims.common.stepDetail" />
											</h3>
													<div class="box-tools pull-right"></div>
												</div>
												<div class="box-body" style="height: 100%; overflow: hidden;">
													<form>
														<textarea id="editor1" name="editor1" rows="10" cols="80">
                                            			</textarea>
													</form>
												</div>
											</div>
										</div>

										<!--箭头步骤-->
										<ul class="nav nav-tabs bwizard-steps" role="tablist" style="border: none;padding-left: 13px;">
											<li role="presentation" class="active" note="1">
												<a href="#firstTab" aria-controls="firstTab" role="tab" data-toggle="tab">工前准备及检查</a>
											</li>
											<li role="presentation" note="2">
												<a href="#secondTab" aria-controls="firstTab" role="tab" data-toggle="tab">质检项</a>
											</li>
											<li role="presentation" note="3">
												<a href="#thirdTab" aria-controls="secondTab" role="tab" data-toggle="tab">生产操作</a>
											</li>
											<li role="presentation" note="4">
												<a href="#fourTab" aria-controls="thirdTab" role="tab" data-toggle="tab">完工清场</a>
											</li>
										</ul>
										<!--具体步骤-->
										<!-- Tab panes -->
										<div class="tab-content" style="padding-top: 8px;">
											<div role="tabpanel" class="tab-pane active" id="firstTab">
												<!--步骤明细-->
												<div class="col-md-6">
													<div class="box box-primary">
														<div class="box-header">
															<i class="fa fa-paw"></i>
															<h3 class="box-title">
												步骤信息
											</h3>
														</div>
														<div class="box-body">
															<div class="row">
																<div class="col-xs-12 col-sm-12">
																	<div class="input-group">
																		<span class="input-group-addon"> <fmt:message
																key="biolims.common.serialNumber" />
														</span> <input type="text" class="form-control" id="template_code" />
																	</div>
																</div>
																<div class="col-xs-12 col-sm-12">
																	<div class="input-group">
																		<span class="input-group-addon"><fmt:message
																key="biolims.user.eduName" /> <img
															class='requiredimage' src='${ctx}/images/required.gif' />
														</span> <input type="text" class="form-control" id="template_name" />
																	</div>
																</div>

																<div class="col-xs-12 col-sm-12">
																	<div class="input-group">
																		<span class="input-group-addon"><fmt:message
																key="biolims.common.estimatedTime" /> </span> <input type="text" class="form-control" id="estimated_time" />

																	</div>
																</div>
																<!-- 是否混合 -->
																<div class="col-xs-12 col-sm-12">
																	<div class="input-group">
																		<span class="input-group-addon">是否此步混合<img class='requiredimage' src='${ctx}/images/required.gif' /></span>
																		<select lay-ignore class="form-control" id="blend">
																			<option value="0">否</option>
																			<option value="1">是</option>
																		</select>
																	</div>
																</div>
																<!-- 是否流入回输计划 -->
																<div class="col-xs-12 col-sm-12">
																	<div class="input-group">
																		<span class="input-group-addon">是否此步流入回输计划<img class='requiredimage' src='${ctx}/images/required.gif' /></span>
																		<select lay-ignore class="form-control" id="reinfusionPlan">
																			<option value="0">否</option>
																			<option value="1">是</option>
																		</select>
																	</div>
																</div>
																<!-- 是否收获 -->
																<div class="col-xs-12 col-sm-12">
																	<div class="input-group">
																		<span class="input-group-addon">是否此步进行收获<img class='requiredimage' src='${ctx}/images/required.gif' /></span>
																		<select lay-ignore class="form-control" id="harvest">
																			<option value="0">否</option>
																			<option value="1">是</option>
																		</select>
																	</div>
																</div>
																<!-- 是否收获 -->
																<div class="col-xs-12 col-sm-12">
																	<div class="input-group">
																		<span class="input-group-addon">是否此步进行入培养箱<img class='requiredimage' src='${ctx}/images/required.gif' /></span>
																		<select lay-ignore class="form-control" id="incubator">
																			<option value="0">否</option>
																			<option value="1">是</option>
																		</select>
																	</div>
																</div>
																<!-- 打印标签编码 -->
																<div class="col-xs-12 col-sm-12">
																	<div class="input-group">
																		<span class="input-group-addon">生产工序代码<!-- 打印标签编码 --> </span> <input type="text" class="form-control" id="print_label_coding" />

																	</div>
																</div>
																<div class="col-xs-12 col-sm-12">
																	<div class="input-group">
																		<span class="input-group-addon">瓶/袋<img class='requiredimage' src='${ctx}/images/required.gif' /></span>
																		<select lay-ignore class="form-control" id="bottleOrBag">
																			<option value="0">瓶</option>
																			<option value="1">袋</option>
																		</select>
																	</div>
																</div>
																<div class="col-xs-12 col-sm-12">
																	<div class="input-group">
																		<span class="input-group-addon">是否细胞观察<img class='requiredimage' src='${ctx}/images/required.gif' /></span>
																		<select lay-ignore class="form-control" id="cellObservation">
																			<option value="0">否</option>
																			<option value="1">是</option>
																		</select>
																	</div>
																</div>
																<%-- <!-- 是否流到运输计划 -->
																<div class="col-xs-12 col-sm-12">
																	<div class="input-group">
																		<span class="input-group-addon">是否此步流入运输计划<img class='requiredimage' src='${ctx}/images/required.gif' /></span>
																		<select lay-ignore class="form-control" id="transportPlan">
																			<option value="0">否</option>
																			<option value="1">是</option>
																		</select>
																	</div>
																</div> --%>
															</div>
														</div>
													</div>
												</div>

												<!--生产记录-->
												<div class="col-md-6">
													<div class="box box-info">
														<div class="box-header with-border">
															<i class="fa fa-keyboard-o"></i>
															<h3 class="box-title quality">生产记录</h3>
															<div class="box-tools pull-right">
																<button class="addTemplateItem btn btn-warning" id="pageOneaddItem">添加生产记录字段</button>
															</div>
														</div>
														<div id="pageone" class="box-body selection_widget">
															<ul class="list" style="padding-left: 0px;">

															</ul>
														</div>
													</div>
													
													<div class="box box-info">
														<div class="box-header with-border">
															<i class="fa fa-keyboard-o"></i>
															<h3 class="box-title quality">工前准备试剂准备量</h3>
														</div>
														<div class="box-body">
															<table class="table table-hover table-striped table-bordered table-condensed" id="onegqzb" style="font-size:12px;"></table>
														</div>
													</div>
												</div>
												<!--操作指令-->
												<div class="col-md-12">
													<div class="box box-success">
														<div class="box-header with-border">
															<i class="fa fa-hand-o-right"></i>
															<h3 class="box-title quality">操作指令</h3>
														</div>
														<div class="box-body">
															<table class="table table-hover table-striped table-bordered table-condensed" id="oneCZZL" style="font-size:12px;"></table>
														</div>
													</div>
												</div>

											</div>
											<div role="tabpanel" class="tab-pane" id="secondTab">
												<!--质检内容-->
												<div class="col-md-12">
													<div class="box box-primary">
														<div class="box-header with-border">
															<i class="fa fa-flask"></i>
															<h3 class="box-title quality">质检内容</h3>
															<small id="zjType"></small>
															<div class="box-tools pull-right">
																<button type="button" class="btn btn-sm btn-box-tool" onclick="chosezhijian()" style="border: 1px solid;">
													<i class="fa fa-search-plus"></i> 质检项
												</button>
																<!-- <button type="button" class="btn btn-sm btn-box-tool" onclick="choseType()" style="border: 1px solid;">
															<i class="fa fa-search-plus"></i>
															质检类型
                										</button> -->
															</div>
														</div>
														<div class="box-body">
															<ul class="todo-list" id="zhijianBody" style="min-height: 150px; max-height: 251px;">
																<li class="nozhijian"><span> <i
														class="glyphicon glyphicon-th-list"></i>
												</span>
																	<p class="text">请选择质检内容</p>
																</li>
															</ul>
														</div>
													</div>
												</div>

											</div>
											<div role="tabpanel" class="tab-pane" id="thirdTab">
												<ul class="timeline" style="margin: 0 15px;">

												</ul>
											</div>
											<div role="tabpanel" class="tab-pane" id="fourTab">
												<!--操作指令-->
												<div class="col-md-8">
													<div class="box box-success">
														<div class="box-header with-border">
															<i class="fa fa-hand-o-right"></i>
															<h3 class="box-title quality">操作指令</h3>
														</div>
														<div class="box-body">
															<table class="table table-hover table-striped table-bordered table-condensed" id="fourCZZL" style="font-size:12px;"></table>
														</div>
													</div>
												</div>
												<!--生产记录-->
												<div class="col-md-4">
													<div class="box box-info">
														<div class="box-header with-border">
															<i class="fa fa-keyboard-o"></i>
															<h3 class="box-title quality">生产记录</h3>
															<div class="box-tools pull-right">
																<button class="addTemplateItem btn btn-warning" id="pageFouraddItem">添加生产记录字段</button>
															</div>
														</div>
														<div id="pageFour" class="box-body selection_widget">
															<ul class="list" style="padding-left: 0px;">

															</ul>
														</div>
													</div>
												</div>
											</div>

										</div>

									</div>
								</div>
							</div>

						</div>
						<div class="box-footer">
							<div class="pull-right">
								<button type="button" class="btn btn-primary" onclick="preTemplateStep()">
							<i class="glyphicon glyphicon-arrow-up"></i>
							<fmt:message key="biolims.common.back" />
						</button>
							</div>

						</div>
					</div>
				</div>
				<div class="box-footer" id="layerItem" style="display: none;">
					<div class="col-xs-12 col-sm-6">
						<div class="input-group">
							<span class="input-group-addon"><fmt:message
															key="biolims.common.fieldDisplayName" /> </span> <input type="text" id="fieldDisplay1" class="form-control" />
						</div>
					</div>
					<div class="col-xs-12 col-sm-6">
						<div class="input-group">
							<span class="input-group-addon"><fmt:message
															key="biolims.common.fieldCodedName" /> </span> <input type="text" id="fieldDisplay2" class="form-control" />
						</div>
					</div>
					<div class="col-xs-12 col-sm-6">
						<div class="input-group">
							<span class="input-group-addon"><fmt:message
															key="biolims.common.fieldType" /></span> <input type="text" id="fieldDisplay3" class="form-control datatype" disabled />
						</div>
					</div>
					<div class="col-xs-12 col-sm-6">
						<div class="input-group">
							<span class="input-group-addon"><fmt:message
															key="biolims.common.required" /></span>
							<select class="form-control" id="fieldDisplay4">
								<option>
									<fmt:message key="biolims.common.no" />
								</option>
								<option>
									<fmt:message key="biolims.common.yes" />
								</option>
							</select>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6">
						<div class="input-group">
							<span class="input-group-addon"><fmt:message
															key="biolims.common.defaultValue" /> </span> <input type="text" class="form-control" id="fieldDisplay5" />
						</div>
					</div>
					<div class="col-xs-12 col-sm-6">
						<div class="input-group">
							<span class="input-group-addon"><fmt:message
															key="biolims.common.isReadOnly" /> </span>
							<select class="form-control" id="fieldDisplay6">
								<option>
									<fmt:message key="biolims.common.no" />
								</option>
								<option>
									<fmt:message key="biolims.common.yes" />
								</option>
							</select>
						</div>
					</div>
					<div id="choseCheekBox" class="col-xs-12" style="margin-top: 10px; display: none;">
						<p>
							<i class="glyphicon glyphicon-asterisk"></i>
							<fmt:message key="biolims.common.optionInformation" />
						</p>
						<div class="choseCheekBox">
							<div class="col-xs-12 col-sm-6">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
																	key="biolims.common.optionValue" /> </span> <input type="text" class="form-control input-sm" placeholder="0" />
								</div>
							</div>
							<div class="col-xs-12 col-sm-6">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
																	key="biolims.common.optionName" /> </span> <input type="text" class="form-control input-sm" placeholder="basketball" />
								</div>
							</div>
						</div>
						<div class="choseCheekBox">
							<div class="col-xs-12 col-sm-6">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
																	key="biolims.common.optionValue" /> </span> <input type="text" class="form-control input-sm" placeholder="1" />
								</div>
							</div>
							<div class="col-xs-12 col-sm-6">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
																	key="biolims.common.optionName" /> </span> <input type="text" class="form-control input-sm" placeholder="football" />
								</div>
							</div>
						</div>
						<div class="text-center">
							<button class="btn btn-xs btn-info addItemTembtn" style="margin: 10px 0; display: none;">
														<i class="glyphicon glyphicon-plus"></i>
														<fmt:message key="biolims.common.AddTo" />
													</button>
						</div>

					</div>
				</div>
			</section>
		</div>
		<script type="text/javascript" src="${ctx}/lims/plugins/ckeditor/ckeditor.js"></script>
		<script type="text/javascript" src="${ctx}/js/system/template/templateEditStep2.js"></script>
		<script type="text/javascript" src="${ctx}/js/system/template/contentItem.js"></script>
	</body>

</html>