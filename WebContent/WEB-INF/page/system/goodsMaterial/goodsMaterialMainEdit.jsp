﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=goodsMaterialMain&id=${goodsMaterialMain.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/system/goodsMaterial/goodsMaterialMainEdit.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" >编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left" >
                   	<input type="text" size="20" maxlength="25" id="goodsMaterialMain_id"
                   	 name="goodsMaterialMain.id" title="编号"
                   	   readonly = "readOnly" class="text input readonlytrue"
	value="<s:property value="goodsMaterialMain.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="50" id="goodsMaterialMain_name"
                   	 name="goodsMaterialMain.name" title="描述"
                   	   
	value="<s:property value="goodsMaterialMain.name"/>"
                   	  />
                   	  
                   	</td>
               	 	<td class="label-title" >业务类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="50" readonly="readOnly"  id="goodsMaterialMain_product_name"  value="<s:property value="goodsMaterialMain.product.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="goodsMaterialMain_product" name="goodsMaterialMain.product.id"  value="<s:property value="goodsMaterialMain.product.id"/>" > 
 						<img alt='选择业务类型' id='showproduct' src='${ctx}/images/img_lookup.gif' onClick="ProductFun()"	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >数量</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="goodsMaterialMain_num"
                   	 name="goodsMaterialMain.num" title="数量"
                   	   
	value="<s:property value="goodsMaterialMain.num"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left">
                   		<select name="goodsMaterialMain.state" id="goodsMaterialMain_state">
							<option value=""
								<s:if test="goodsMaterialMain.state==''">selected="selected" </s:if>>
								请选择</option>
							<option value="1"
								<s:if test="goodsMaterialMain.state==1">selected="selected" 
								</s:if>>有效</option>
							<option value="0"
								<s:if test="goodsMaterialMain.state==0">selected="selected" 
								</s:if>>无效</option>
						</select>
					</td>
			</tr>
			<tr>
			<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr>
			
			
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="goodsMaterialMain.id"/>" />
            </form>
        	</div>
	</body>
	</html>
