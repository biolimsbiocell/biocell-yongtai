﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/system/goodsMaterial/goodsMaterialMain.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title"  style="display:none"  >编号</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="goodsMaterialMain_id"
                   	 name="id" searchField="true" title="编号"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >描述</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="goodsMaterialMain_name"
                   	 name="name" searchField="true" title="描述"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showproduct" title="选择业务类型"
				hasHtmlFrame="true"
				html="${ctx}/system/goodsMaterial/goodsMaterialMain/stringSelect.action"
				isHasSubmit="false" functionName="StringFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('goodsMaterialMain_product').value=rec.get('id');
				document.getElementById('goodsMaterialMain_product_name').value=rec.get('name');" />
               	 	<td class="label-title" >业务类型</td>
                   	<td align="left"  >
 						<input type="text" size="50"   id="goodsMaterialMain_product_name" searchField="true"  name="product.name"  value="" class="text input" />
 						<input type="hidden" id="goodsMaterialMain_product" name="goodsMaterialMain.product.id"  value="" > 
 						<img alt='选择业务类型' id='showproduct' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >数量</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="goodsMaterialMain_num"
                   	 name="num" searchField="true" title="数量"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >状态</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="goodsMaterialMain_state"
                   	 name="state" searchField="true" title="状态"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_goodsMaterialMain_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_goodsMaterialMain_tree_page"></div>
</body>
</html>



