﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>

	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var doclinks_img = Ext.get('doclinks_img');
doclinks_img.on('click', 
 function doc(){
var win = Ext.getCmp('doc');
if (win) {win.close();}
var doc= new Ext.Window({
id:'doc',modal:true,title:'',layout:'fit',width:900,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/operfile/initFileList.action\?modelType=abnomalType&id=&flag=doc' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 doc.close(); }  }]  });     doc.show(); }
);
});
</script>

</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/system/abnomal/abnomalTypeEdit.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"></fmt:message></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left" >
                   	<input type="text" size="20" maxlength="25" id="abnomalType_id"
                   	 name="abnomalType.id" title="<fmt:message key="biolims.common.serialNumber"></fmt:message>"
	value="<s:property value="abnomalType.id"/>"
                   	  />
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"></fmt:message></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="50" id="abnomalType_name"
                   	 name="abnomalType.name" title="<fmt:message key="biolims.common.describe"></fmt:message>"
                   	   
	value="<s:property value="abnomalType.name"/>"
                   	  />
                   	  
                   	</td>
						<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showparent = Ext.get('showparent');
showparent.on('click', 
 function AbnomalTypeFun(){
var win = Ext.getCmp('AbnomalTypeFun');
if (win) {win.close();}
var AbnomalTypeFun= new Ext.Window({
id:'AbnomalTypeFun',modal:true,title:'<fmt:message key="biolims.common.selectTheParent"></fmt:message>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/system/abnomal/abnomalType/abnomalTypeSelect.action?flag=AbnomalTypeFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 AbnomalTypeFun.close(); }  }]  });     AbnomalTypeFun.show(); }
);
});
 function setAbnomalTypeFun(rec){
document.getElementById('abnomalType_parent').value=rec.get('id');
				document.getElementById('abnomalType_parent_name').value=rec.get('name');
var win = Ext.getCmp('AbnomalTypeFun')
if(win){win.close();}
}
</script>

				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.parent"></fmt:message></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="50" readonly="readOnly"  id="abnomalType_parent_name"  value="<s:property value="abnomalType.parent.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="abnomalType_parent" name="abnomalType.parent.id"  value="<s:property value="abnomalType.parent.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectTheParent"></fmt:message>' id='showparent' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
			
			
<%--                	 	<td class="label-title"  style="display:none"  >状态id</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="abnomalType_state"
                   	 name="abnomalType.state" title="状态id"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="abnomalType.state"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td> --%>
					<td class="label-title" ><fmt:message key="biolims.common.processingOpinion"></fmt:message></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  	<select id="abnomalType_method" name="abnomalType.method" style="width:100">
                		<option value="0" <s:if test="abnomalType.method==0">selected="selected" </s:if>><fmt:message key="biolims.common.refuse2Accept"></fmt:message></option>
    					<option value="1" <s:if test="abnomalType.method==1">selected="selected" </s:if>><fmt:message key="biolims.common.qualified"></fmt:message></option>
    					<option value="2" <s:if test="abnomalType.method==2">selected="selected" </s:if>><fmt:message key="biolims.common.concessions2Receive"></fmt:message></option>
						</select>
                   	</td>
			
					<td class="label-title" ><fmt:message key="biolims.common.state"></fmt:message></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  	<select id="abnomalType_state" name="abnomalType.state" style="width:100">
                		<option value="" <s:if test="abnomalType.state==">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"></fmt:message></option>
    					<option value="1" <s:if test="abnomalType.state==1">selected="selected" </s:if>><fmt:message key="biolims.common.effective"></fmt:message></option>
    					<option value="0" <s:if test="abnomalType.state==0">selected="selected" </s:if>><fmt:message key="biolims.common.invalid"></fmt:message></option>
						</select>
                   	</td>
			</tr>
			<tr>
			<td class="label-title"><fmt:message key="biolims.common.attachment"></fmt:message></td><td></td>
						<td title='<fmt:message key="biolims.common.afterthepreservation"></fmt:message>' id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"></fmt:message>${requestScope.fileNum}<fmt:message key="biolims.common.noAttachment"></fmt:message></span>
			</tr>
			
			
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="abnomalType.id"/>" />
            </form>
        	</div>
	</body>
	</html>
