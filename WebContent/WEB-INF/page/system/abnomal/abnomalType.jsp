﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/system/abnomal/abnomalType.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.user.itemNo"/> </td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="abnomalType_id"
                   	 name="id" searchField="true" title=""   style="display:none"    />
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.name"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="abnomalType_name"
                   	 name="name" searchField="true" title=""    />
                   	</td>
			<g:LayOutWinTag buttonId="showparent" title=""
				hasHtmlFrame="true"
				html="${ctx}/com/biolims/system/abnomal/abnomalType/abnomalTypeSelect.action"
				isHasSubmit="false" functionName="AbnomalTypeFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('abnomalType_parent').value=rec.get('id');
				document.getElementById('abnomalType_parent_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.master.parentName"/></td>
                   	<td align="left"  >
 						<input type="text" size="50"   id="abnomalType_parent_name" searchField="true"  name="parent.name"  value="" class="text input" />
 						<input type="hidden" id="abnomalType_parent" name="abnomalType.parent.id"  value="" > 
 						<img alt='选择父级' id='showparent' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
					
               	 	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.stateID"/></td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="abnomalType_state"
                   	 name="state" searchField="true" title=<fmt:message key="biolims.common.stateID"/>   style="display:none"    />
                   	</td>
               	 	
			</tr>
            </table>
		</form>
		</div>
		<div id="show_abnomalType_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_abnomalType_tree_page"></div>
</body>
</html>



