<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<title></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style>
.dataTables_scrollBody {
	min-height: 100px;
}
</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarNoState.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div style="height: 14px"></div>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 46px">
		<!--Form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<i class="glyphicon glyphicon-pencil"></i>
						<fmt:message key="biolims.sample.companyName" />
					</h3>

					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="####" onclick="$('.buttons-print').click();"><fmt:message
											key="biolims.common.print" /></a></li>
								<li><a href="#####" onclick="$('.buttons-copy').click();"><fmt:message
											key="biolims.common.copyData" /></a></li>
								<li><a href="####" onclick="$('.buttons-excel').click();">Excel</a>
								</li>
								<li><a href="####" onclick="$('.buttons-csv').click();">CSV</a>
								</li>
								<li role="separator" class="divider"></li>
								<li><a href="####" onclick="itemFixedCol(2)"><fmt:message
											key="biolims.common.lock2Col" /></a></li>
								<li><a href="####" id="unfixde" onclick="unfixde()"><fmt:message
											key="biolims.common.cancellock2Col" /></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="box-body ipadmini">
					<!--form表单-->
					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
							<input type="hidden" id="bpmTaskId"
								value="<%=request.getParameter("bpmTaskId")%>" /> <br>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.sysExpressCompany.id" /> </span> <input type="text"
											size="20" maxlength="25" id="expressCompany_id" name="id"
											changelog="<s:property value="expressCompany.id"/>"
											readonly="readOnly" class="form-control readonlytrue"
											value="<s:property value="expressCompany.id"/>" />

									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.sysExpressCompany.name" /> </span> <input type="text"
											size="50" maxlength="50" id="expressCompany_name" name="name"
											changelog="<s:property value="expressCompany.name"/>"
											class="form-control"
											value="<s:property value="expressCompany.name"/>" />

									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.sysExpressCompany.phone" /> </span> <input type="text"
											size="20" maxlength="25" id="expressCompany_phone"
											name="phone"
											changelog="<s:property value="expressCompany.phone"/>"
											class="form-control readonlytrue"
											value="<s:property value="expressCompany.phone"/>" />

									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.sysExpressCompany.adress" /> </span> <input
											type="text" size="20" maxlength="25"
											id="expressCompany_address" name="address"
											changelog="<s:property value="expressCompany.address"/>"
											class="form-control"
											value="<s:property value="expressCompany.address"/>" />

									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.sysExpressCompany.state" /> </span> <input
											type="hidden" name="first "
											changelog="<s:property value="expressCompany.state"/>"
											id="expressCompany_state "
											value="<s:property value="expressCompany.state" />" /> <select
											class="form-control" name="state">
											<option value="1"
												<s:if test="expressCompany.state==1">selected="selected"</s:if>>
												<fmt:message key="biolims.common.effective" />
											</option>
											<option value="0"
												<s:if test="expressCompany.state==0">selected="selected"</s:if>>
												<fmt:message key="biolims.common.invalid" />
											</option>
										</select>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.sysExpressCompany.printPath" /> </span> <input
											type="text" size="20" maxlength="25"
											id="expressCompany_printPath" name="printPath"
											changelog="<s:property value="expressCompany.printPath"/>"
											class="form-control"
											value="<s:property value="expressCompany.printPath"/>" />

									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<button type="button" class="btn btn-info btn-sm"
											onclick="fileUp()">
											<fmt:message key="biolims.common.uploadAttachment" />
										</button>
										&nbsp;&nbsp;
										<%-- <span class="text label"><fmt:message
											key="biolims.common.aTotalOf" />${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
											key="biolims.common.attachment" /> --%>
										<button type="button" class="btn btn-info btn-sm"
											onclick="fileView()">
											<fmt:message key="biolims.report.checkFile" />
										</button>
									</div>
								</div>
							</div>
							<div id="fieldItemDiv"></div>

							<br> <input type="hidden" id="id_parent_hidden"
								value="<s:property value="expressCompany.id"/>" /> <input
								type="hidden" id="fieldContent" name="fieldContent"
								value="<s:property value="expressCompany.fieldContent"/>" />

						</form>
					</div>
					<!--table表格-->
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/system/express/expressCompanyEdit.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/common/dataTablesExtend.js"></script>
</body>

</html>
