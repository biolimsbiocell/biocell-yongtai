﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=projectCheck&id=${projectCheck.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/system/projectcheck/projectCheckEdit.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleVaLeave.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title"   >编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px"    ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="projectCheck_id"
                   	 name="projectCheck.id" title="编号"
                   	   
	value="<s:property value="projectCheck.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="50" id="projectCheck_name"
                   	 name="projectCheck.name" title="描述"
                   	   
	value="<s:property value="projectCheck.name"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  >状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left" >
                   		 <select name="projectCheck.state" id="projectCheck_state" style="width:150px;">
                   			<option value="1" <s:if test="projectCheck.state == 1">selected="selected"</s:if>>有效</option>
                   			<option value="0" <s:if test="projectCheck.state == 0">selected="selected"</s:if>>无效</option>
                   		</select>
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" style="display:none">工作流状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px" style="display:none"></td>            	 	
                   	<td align="left" style="display:none" >
                   	<input type="text" size="20" maxlength="10" id="projectCheck_stateName"
                   	 name="projectCheck.stateName" title="工作流状态"
                   	    	   style="display:none"
	value="<s:property value="projectCheck.stateName"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr>
			
			
            </table>
            <input type="hidden" name="projectCheckItemJson" id="projectCheckItemJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="projectCheck.id"/>" />
            </form>
            <div id="tabs">
            <ul>
			<li><a href="#projectCheckItempage">项目质控明细</a></li>
           	</ul> 
			<div id="projectCheckItempage" width="100%" height:10px></div>
			</div>
        	</div>
	</body>
	</html>
