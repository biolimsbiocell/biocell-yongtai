<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<title></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style>
.dataTables_scrollBody {
	min-height: 100px;
}
</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div style="height: 14px"></div>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 46px">
		<!--Form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<i class="glyphicon glyphicon-pencil"></i>条码模板
					</h3>

					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="####" onclick="$('.buttons-print').click();"><fmt:message
											key="biolims.common.print" /></a></li>
								<li><a href="#####" onclick="$('.buttons-copy').click();"><fmt:message
											key="biolims.common.copyData" /></a></li>
								<li><a href="####" onclick="$('.buttons-excel').click();">Excel</a>
								</li>
								<li><a href="####" onclick="$('.buttons-csv').click();">CSV</a>
								</li>
								<li role="separator" class="divider"></li>
								<li><a href="####" onclick="itemFixedCol(2)"><fmt:message
											key="biolims.common.lock2Col" /></a></li>
								<li><a href="####" id="unfixde" onclick="unfixde()"><fmt:message
											key="biolims.common.cancellock2Col" /></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="box-body">
					<!--form表单-->
					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
							<input type="hidden" id="bpmTaskId"
								value="<%=request.getParameter("bpmTaskId")%>" /> <br>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon">编号</span> <input type="text"
											size="50" maxlength="127" id="codeMainNew_id" name="id"
											changelog="<s:property value="codeMainNew.id.name"/>"
											class="form-control"
											value="<s:property value="codeMainNew.id"/>" />

									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon">描述</span> <input type="text"
											size="50" maxlength="127" id="codeMainNew_name" name="name"
											changelog="<s:property value="codeMainNew.name.name"/>"
											class="form-control"
											value="<s:property value="codeMainNew.name"/>" />

									</div>
								</div>
								
							</div>
							<%-- <div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon">名字x轴</span> <input type="text"
											size="50" maxlength="127" id="codeMainNew_namex" name="namex"
											changelog="<s:property value="codeMainNew.namex.name"/>"
											class="form-control"
											value="<s:property value="codeMainNew.namex"/>" />

									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon">名字y轴</span> <input type="text"
											size="50" maxlength="127" id="codeMainNew_namey" name="namey"
											changelog="<s:property value="codeMainNew.namey.name"/>"
											class="form-control"
											value="<s:property value="codeMainNew.namey"/>" />

									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon">编码x轴</span> <input type="text"
											size="50" maxlength="127" id="codeMainNew_codex" name="codex"
											changelog="<s:property value="codeMainNew.codex.name"/>"
											class="form-control"
											value="<s:property value="codeMainNew.codex"/>" />

									</div>
								</div>
							</div> --%>
							<%-- <div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon">编码y轴</span> <input type="text"
											size="50" maxlength="127" id="codeMainNew_codey" name="codey"
											changelog="<s:property value="codeMainNew.codey.name"/>"
											class="form-control"
											value="<s:property value="codeMainNew.codey"/>" />

									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon">二维码x轴</span> <input type="text"
											size="50" maxlength="127" id="codeMainNew_qrx" name="qrx"
											changelog="<s:property value="codeMainNew.qrx.name"/>"
											class="form-control"
											value="<s:property value="codeMainNew.qrx"/>" />

									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon">二维码y轴</span> <input type="text"
											size="50" maxlength="127" id="codeMainNew_qry" name="qry"
											changelog="<s:property value="codeMainNew.qry.name"/>"
											class="form-control"
											value="<s:property value="codeMainNew.qry"/>" />

									</div>
								</div>
							</div> --%>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon">ip地址</span> <input type="text" size="50"
											maxlength="127" id="codeMainNew_ip" name="ip"
											changelog="<s:property value="codeMainNew.ip.name"/>"
											class="form-control"
											value="<s:property value="codeMainNew.ip"/>" />

									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon">状态</span> <select
											id="codeMainNew_state" name="state" class="form-control">
											<option value="有效"
												<s:if test="codeMainNew.state=='有效'">selected="selected"</s:if>>有效</option>
											<option value="无效"
												<s:if test="codeMainNew.state=='无效'">selected="selected"</s:if>>无效</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon">打印指令</span> 
										<textarea id="codeMainNew_code" name="code" style="height:200px"
											changelog="<s:property value="codeMainNew.code.name"/>"
											class="form-control"
											 /><s:property value="codeMainNew.code"/></textarea>
										
										
										<%-- <input type="textArea"
											size="50" maxlength="127" id="codeMainNew_code" name="code"
											changelog="<s:property value="codeMainNew.code.name"/>"
											class="form-control"
											value="<s:property value="codeMainNew.code"/>" /> --%>

									</div>
								</div>
							</div>
							<div id="fieldItemDiv"></div>

							<br> <input type="hidden" id="id_parent_hidden"
								value="<s:property value="codeMainNew.id"/>" /> <input
								type="hidden" id="fieldContent" name="fieldContent"
								value="<s:property value="codeMainNew.fieldContent"/>" />

						</form>
					</div>
					<!--table表格-->
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/system/newsyscode/codeMainNewEdit.js"></script>
</body>

</html>
