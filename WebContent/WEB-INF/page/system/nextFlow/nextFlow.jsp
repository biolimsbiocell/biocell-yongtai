﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/system/nextFlow/nextFlow.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" >编号</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="nextFlow_id"
                   	 name="id" searchField="true" title="编号"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >下一步流向</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="nextFlow_name"
                   	 name="name" searchField="true" title="下一步流向"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showapplicationTypeTable" title="选择关联表单"
				hasHtmlFrame="true"
				html="${ctx}/system/nextFlow/applicationTypeTableSelect.action"
				isHasSubmit="false" functionName="ApplicationTypeTableFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('nextFlow_applicationTypeTable').value=rec.get('id');
				document.getElementById('nextFlow_applicationTypeTable_name').value=rec.get('name');" />
               	 	<td class="label-title" >关联表单</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="nextFlow_applicationTypeTable_name" searchField="true"  name="applicationTypeTable.name"  value="" class="text input" />
 						<input type="hidden" id="nextFlow_applicationTypeTable" name="nextFlow.applicationTypeTable.id"  value="" > 
 						<img alt='选择关联表单' id='showapplicationTypeTable' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true"
				html="${ctx}/system/nextFlow/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('nextFlow_createUser').value=rec.get('id');
				document.getElementById('nextFlow_createUser_name').value=rec.get('name');" />
               	 	<td class="label-title" >创建人</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="nextFlow_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="nextFlow_createUser" name="nextFlow.createUser.id"  value="" > 
 						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" >创建时间</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >状态</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="nextFlow_state"
                   	 name="state" searchField="true" title="状态"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >状态描述</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="nextFlow_stateName"
                   	 name="stateName" searchField="true" title="状态描述"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >备注</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="250" id="nextFlow_note"
                   	 name="note" searchField="true" title="备注"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_nextFlow_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_nextFlow_tree_page"></div>
</body>
</html>



