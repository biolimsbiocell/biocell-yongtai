<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件" hasHtmlFrame="true"
		width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=nextFlow&id=${nextFlow.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
	<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
	<script type="text/javascript"
		src="${ctx}/js/system/nextFlow/nextFlowEdit.js"></script>
	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass">
		<input type="hidden" id="handlemethod"
			value="${requestScope.handlemethod}">
		<form name="form1" id="form1" method="post">
			<table class="frame-table">
				<tr>


					<td class="label-title"><fmt:message
							key="biolims.common.serialNumber"></fmt:message></td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/required.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="nextFlow_id" name="nextFlow.id"
						title="<fmt:message key="biolims.common.serialNumber"></fmt:message>"
						readonly="readOnly" class="text input readonlytrue"
						value="<s:property value="nextFlow.id"/>" />

					</td>


					<td class="label-title"><fmt:message
							key="biolims.common.theNextStepTo"></fmt:message></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20" maxlength="50"
						id="nextFlow_name" name="nextFlow.name"
						title="<fmt:message key="biolims.common.theNextStepTo"></fmt:message>"
						value="<s:property value="nextFlow.name"/>" />

					</td>



					<%-- <g:LayOutWinTag buttonId="showapplicationTypeTable" title="选择关联表单"
				hasHtmlFrame="true"
				html="${ctx}/system/nextFlow/applicationTypeTableSelect.action"
				isHasSubmit="false" functionName="ApplicationTypeTableFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('nextFlow_applicationTypeTable').value=rec.get('id');
				document.getElementById('nextFlow_applicationTypeTable_name').value=rec.get('name');" />
				
			 --%>

					<td class="label-title"><fmt:message
							key="biolims.common.associatedWithTheMainTable"></fmt:message></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="nextFlow_applicationTypeTable_name"
						value="<s:property value="nextFlow.applicationTypeTable.name"/>"
						readonly="readOnly" /> <input type="hidden"
						id="nextFlow_applicationTypeTable"
						name="nextFlow.applicationTypeTable.id"
						value="<s:property value="nextFlow.applicationTypeTable.id"/>">
						<img
						alt='<fmt:message key="biolims.common.associatedWithTheMainTable"></fmt:message>'
						id='showapplicationTypeTable' src='${ctx}/images/img_lookup.gif'
						class='detail' onclick="selectnextStepNameTable();" /></td>
				</tr>
				<tr>



					<g:LayOutWinTag buttonId="showcreateUser"
						title='<fmt:message key="biolims.common.selectTheCreatePerson"></fmt:message>'
						hasHtmlFrame="true"
						html="${ctx}/system/nextFlow/userSelect.action"
						isHasSubmit="false" functionName="UserFun" hasSetFun="true"
						extRec="rec"
						extStr="document.getElementById('nextFlow_createUser').value=rec.get('id');
				document.getElementById('nextFlow_createUser_name').value=rec.get('name');" />



					<td class="label-title"><fmt:message
							key="biolims.common.commandPerson"></fmt:message></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="nextFlow_createUser_name"
						value="<s:property value="nextFlow.createUser.name"/>"
						readonly="readOnly" /> <input type="hidden"
						id="nextFlow_createUser" name="nextFlow.createUser.id"
						value="<s:property value="nextFlow.createUser.id"/>"> <%-- 	<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />   --%>
					</td>


					<td class="label-title"><fmt:message
							key="biolims.common.commandTime"></fmt:message></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="nextFlow_createDate" name="nextFlow.createDate"
						title="<fmt:message key="biolims.common.commandTime"></fmt:message>"
						readonly="readOnly"
						value="<s:date name="nextFlow.createDate" format="yyyy-MM-dd"/>" />
					</td>


					<td class="label-title"><fmt:message
							key="biolims.common.note"></fmt:message></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20"
						maxlength="250" id="nextFlow_note" name="nextFlow.note"
						title="<fmt:message key="biolims.common.note"></fmt:message>"
						value="<s:property value="nextFlow.note"/>" />

					</td>


				</tr>
				<tr>

					<td class="label-title"><fmt:message
							key="biolims.common.state"></fmt:message></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><select id="nextFlow_state"
						name="nextFlow.state" class="input-20-length">
							<option value="1"
								<s:if test="nextFlow.state==1">selected="selected"</s:if>><fmt:message
									key="biolims.common.effective"></fmt:message></option>
							<option value="0"
								<s:if test="nextFlow.state==0">selected="selected"</s:if>><fmt:message
									key="biolims.common.invalid"></fmt:message></option>
					</select></td>

					<td class="label-title"><fmt:message
							key="biolims.common.sortingNumber"></fmt:message></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="nextFlow_sort" name="nextFlow.sort"
						title="<fmt:message key="biolims.common.sortingNumber"></fmt:message>"
						value="<s:property value="nextFlow.sort"/>" />

					</td>
					<td class="label-title">关联模块</td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="nextFlow_mainTable_name"
						value="<s:property value="nextFlow.mainTable.name"/>"
						readonly="readOnly" /> <input type="hidden"
						id="nextFlow_mainTable"
						name="nextFlow.mainTable.id"
						value="<s:property value="nextFlow.mainTable.id"/>">
						<img
						alt='<fmt:message key="biolims.common.associatedWithTheMainTable"></fmt:message>'
						id='showapplicationTypeTable' src='${ctx}/images/img_lookup.gif'
						class='detail' onclick="selectmainTable();" /></td>
				</tr>
				<tr style="display: none;">
					<td class="label-title"><fmt:message
							key="biolims.common.stateDescription"></fmt:message></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="nextFlow_stateName" name="nextFlow.stateName"
						title="<fmt:message key="biolims.common.stateDescription"></fmt:message>"
						readonly="readOnly" class="text input readonlytrue"
						value="<s:property value="nextFlow.stateName"/>" />

					</td>
				</tr>
				<tr>
					<td class="label-title"><fmt:message
							key="biolims.common.attachment"></fmt:message></td>
					<td></td>
					<td
						title='<fmt:message key="biolims.common.afterthepreservation"></fmt:message>'
						id="doclinks_img"><span class="attach-btn"></span><span
						class="text label"><fmt:message
								key="biolims.common.aTotalOf"></fmt:message>${requestScope.fileNum}<fmt:message
								key="biolims.common.noAttachment"></fmt:message></span>
				</tr>


			</table>
			<input type="hidden" id="id_parent_hidden"
				value="<s:property value="nextFlow.id"/>" />
		</form>

	</div>
</body>
</html>
