<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<title></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style>
.dataTables_scrollBody {
	min-height: 100px;
}
</style>
<link rel="stylesheet" type="text/css" href="${ctx}/lims/plugins/datetimepicker/bootstrap-datetimepicker.min.css">
<script type="text/javascript" src="${ctx}/lims/plugins/datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="${ctx}/lims/plugins/datetimepicker/locales/bootstrap-datetimepicker.zh-CN.js"></script>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div style="height: 14px"></div>
	<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 46px">
		<!--Form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<i class="glyphicon glyphicon-pencil"></i>环境监测
					</h3>

					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
											<li>
												<a href="####" onclick="$('.buttons-print').click();"><fmt:message key="biolims.common.print"/></a>
											</li>
											<li>
												<a href="#####" onclick="$('.buttons-copy').click();"><fmt:message key="biolims.common.copyData"/></a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-excel').click();">Excel</a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-csv').click();">CSV</a>
											</li>
											<li role="separator" class="divider"></li>
											<li>
												<a href="####" onclick="itemFixedCol(2)"><fmt:message key="biolims.common.lock2Col"/></a>
											</li>
											<li>
												<a href="####" id="unfixde" onclick="unfixde()"><fmt:message key="biolims.common.cancellock2Col"/></a>
											</li>
										</ul>
						</div>
					</div>
				</div>
				<div class="box-body">
					<input type="hidden" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId")%>" /> <br>
					<!--form表单-->
					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">编码<img
											class='requiredimage' src='${ctx}/images/required.gif' /></span><input
											list="environmentalMonitoring" type="text" size="20" maxlength="25"
											id="environmentalMonitoring_id" name="id" class="form-control" readonly="readonly"
											title="" value="<s:property value=" environmentalMonitoring.id "/>" />
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">实验室编号<img
											class='requiredimage' src='${ctx}/images/required.gif' /></span><input
											list="environmentalMonitoring" type="text" size="20" maxlength="25"
											id="environmentalMonitoring_laboratoryNo" name="laboratoryNo" class="form-control"
											title="" value="<s:property value=" environmentalMonitoring.laboratoryNo "/>" />
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group input-append date form_datetime">
										<span class="input-group-addon">检测时间</span><input
											list="environmentalMonitoring" type="text" size="20" maxlength="25" class="form-control"
											id="environmentalMonitoring_detectionDate" name="detectionDate" class="Wdate" 
											title="" value="<s:date name=" environmentalMonitoring.detectionDate " format="yyyy-MM-dd hh:mm:ss"/>" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">监测类型<img class='requiredimage'
											src='${ctx}/images/required.gif' /></span> 
										<select lay-ignore class="form-control" id="environmentalMonitoring_monitoringType"
										 name="monitoringType" onchange="selectType()">
											<option value="0" 
												<s:if test="environmentalMonitoring.monitoringType==0">selected="selected"</s:if>>尘埃粒子监测</option>
											<option value="1"
												<s:if test="environmentalMonitoring.monitoringType==1">selected="selected"</s:if>>沉降菌监测</option>
											<option value="2"
												<s:if test="environmentalMonitoring.monitoringType==2">selected="selected"</s:if>>纯化水监测</option>
										</select>
									</div>
								</div>
							</div>
						<div id="jc1">
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">室内温度</span><input
											list="environmentalMonitoring" type="text" size="20" maxlength="25"
											id="environmentalMonitoring_indoorTemperature" name="indoorTemperature" class="form-control"
											title="" value="<s:property value=" environmentalMonitoring.indoorTemperature "/>" />
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">室内湿度</span><input
											list="environmentalMonitoring" type="text" size="20" maxlength="25"
											id="environmentalMonitoring_indoorHumidity" name="indoorHumidity" class="form-control"
											title="" value="<s:property value=" environmentalMonitoring.indoorHumidity "/>" />
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">净化区之间压差</span><input
											list="environmentalMonitoring" type="text" size="20" maxlength="25"
											id="environmentalMonitoring_cleanzoneDifferrential" name="cleanzoneDifferrential" class="form-control"
											title="" value="<s:property value=" environmentalMonitoring.cleanzoneDifferrential "/>" />
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">净化区与室外压差</span><input
											list="environmentalMonitoring" type="text" size="20" maxlength="25"
											id="environmentalMonitoring_cleanzoneOutdifferrential" name="cleanzoneOutdifferrential" class="form-control"
											title="" value="<s:property value=" environmentalMonitoring.cleanzoneOutdifferrential "/>" />
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">净化区温度</span><input
											list="environmentalMonitoring" type="text" size="20" maxlength="25"
											id="environmentalMonitoring_cleanzoneTemperature" name="cleanzoneTemperature" class="form-control"
											title="" value="<s:property value=" environmentalMonitoring.cleanzoneTemperature "/>" />
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">净化区湿度</span><input
											list="environmentalMonitoring" type="text" size="20" maxlength="25"
											id="environmentalMonitoring_cleanzoneHumidity" name="cleanzoneHumidity" class="form-control"
											title="" value="<s:property value=" environmentalMonitoring.cleanzoneHumidity "/>" />
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">净化区洁净度</span><input
											list="environmentalMonitoring" type="text" size="20" maxlength="25"
											id="environmentalMonitoring_cleanzoneCleanliness" name="cleanzoneCleanliness" class="form-control"
											title="" value="<s:property value=" environmentalMonitoring.cleanzoneCleanliness "/>" />
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">净化区正压</span><input
											list="environmentalMonitoring" type="text" size="20" maxlength="25"
											id="environmentalMonitoring_cleanzonePositivePressure" name="cleanzonePositivePressure" class="form-control"
											title="" value="<s:property value=" environmentalMonitoring.cleanzonePositivePressure "/>" />
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">净化区噪声</span><input
											list="environmentalMonitoring" type="text" size="20" maxlength="25"
											id="environmentalMonitoring_cleanzoneNoise" name="cleanzoneNoise" class="form-control"
											title="" value="<s:property value=" environmentalMonitoring.cleanzoneNoise "/>" />
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">净化区送风量</span><input
											list="environmentalMonitoring" type="text" size="20" maxlength="25"
											id="environmentalMonitoring_cleanzoneAiroutput" name="cleanzoneAiroutput" class="form-control"
											title="" value="<s:property value=" environmentalMonitoring.cleanzoneAiroutput "/>" />
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">尘埃粒子数</span><input
											list="environmentalMonitoring" type="text" size="20" maxlength="25"
											id="environmentalMonitoring_dustParticleNumber" name="dustParticleNumber" class="form-control"
											title="" value="<s:property value=" environmentalMonitoring.dustParticleNumber "/>" />
									</div>
								</div>
							</div>
						</div>
						<div id="jc2">
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">采样点数</span><input
											list="environmentalMonitoring" type="text" size="20" maxlength="25"
											id="environmentalMonitoring_samplingNumber" name="samplingNumber" class="form-control"
											title="" value="<s:property value=" environmentalMonitoring.samplingNumber "/>" />
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">培养皿数</span><input
											list="environmentalMonitoring" type="text" size="20" maxlength="25"
											id="environmentalMonitoring_cultureDishNumber" name="cultureDishNumber" class="form-control"
											title="" value="<s:property value=" environmentalMonitoring.cultureDishNumber "/>" />
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">菌落数</span><input
											list="environmentalMonitoring" type="text" size="20" maxlength="25"
											id="environmentalMonitoring_clumpCount" name="clumpCount" class="form-control"
											title="" value="<s:property value=" environmentalMonitoring.clumpCount "/>" />
									</div>
								</div>
							</div>
						</div>
						<div id="jc3">
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">性状</span><input
											list="environmentalMonitoring" type="text" size="20" maxlength="25"
											id="environmentalMonitoring_characters" name="characters" class="form-control"
											title="" value="<s:property value=" environmentalMonitoring.characters "/>" />
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">酸碱度</span><input
											list="environmentalMonitoring" type="text" size="20" maxlength="25"
											id="environmentalMonitoring_phValue" name="phValue" class="form-control"
											title="" value="<s:property value=" environmentalMonitoring.phValue "/>" />
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">硝酸盐</span><input
											list="environmentalMonitoring" type="text" size="20" maxlength="25"
											id="environmentalMonitoring_phValue" name="phValue" class="form-control"
											title="" value="<s:property value=" environmentalMonitoring.phValue "/>" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">亚硝酸盐</span><input
											list="environmentalMonitoring" type="text" size="20" maxlength="25"
											id="environmentalMonitoring_nitrite" name="nitrite" class="form-control"
											title="" value="<s:property value=" environmentalMonitoring.nitrite "/>" />
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">氨</span><input
											list="environmentalMonitoring" type="text" size="20" maxlength="25"
											id="environmentalMonitoring_ammonia" name="ammonia" class="form-control"
											title="" value="<s:property value=" environmentalMonitoring.ammonia "/>" />
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">电导率</span><input
											list="environmentalMonitoring" type="text" size="20" maxlength="25"
											id="environmentalMonitoring_conductivity" name="conductivity" class="form-control"
											title="" value="<s:property value=" environmentalMonitoring.conductivity "/>" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">TOC</span><input
											list="environmentalMonitoring" type="text" size="20" maxlength="25"
											id="environmentalMonitoring_toc" name="toc" class="form-control"
											title="" value="<s:property value=" environmentalMonitoring.toc "/>" />
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">易氧化物</span><input
											list="environmentalMonitoring" type="text" size="20" maxlength="25"
											id="environmentalMonitoring_simpleOxide" name="simpleOxide" class="form-control"
											title="" value="<s:property value=" environmentalMonitoring.simpleOxide "/>" />
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">不挥发物</span><input
											list="environmentalMonitoring" type="text" size="20" maxlength="25"
											id="environmentalMonitoring_nonvolatileMatter" name="nonvolatileMatter" class="form-control"
											title="" value="<s:property value=" environmentalMonitoring.nonvolatileMatter "/>" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">重金属</span><input
											list="environmentalMonitoring" type="text" size="20" maxlength="25"
											id="environmentalMonitoring_heavyMetal" name="heavyMetal" class="form-control"
											title="" value="<s:property value=" environmentalMonitoring.heavyMetal "/>" />
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">微生物限度</span><input
											list="environmentalMonitoring" type="text" size="20" maxlength="25"
											id="environmentalMonitoring_microbialLimit" name="microbialLimit" class="form-control"
											title="" value="<s:property value=" environmentalMonitoring.microbialLimit "/>" />
									</div>
								</div>
							</div>
						</div>
							<br/>
							<div class="row">
								<div class="panel" style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
									<div class="panel-heading text-left">
										<h3 class="panel-title" style="font-family: 黑体;">
											<i class="glyphicon glyphicon-bookmark"></i> 自定义字段
												
										</h3>
									</div>
								</div>
							</div>
							<div id="fieldItemDiv"></div>
								
							<br> 
								
							<input type="hidden"
								id="id_parent_hidden"
								value="<s:property value="environmentalMonitoring.id"/>" />
							<input type="hidden" id="fieldContent" name="fieldContent" value="<s:property value="environmentalMonitoring.fieldContent"/>" />	
								
						</form>
					</div>
					<!--table表格-->
					</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript" src="${ctx}/js/system/environmental/environmentalMonitoringEdit.js"></script>
</body>

</html>	
