<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style type="text/css">
.dt-buttons {
	float: none;
}
.table th,
.table td {
	white-space: nowrap;
}

.tablebtns {
	position: initial;
}
.col-xs-4{
	margin-top: 5px;
}
</style>
</head>

<body style="height: 94%">
	<div class="content-wrapper" id="content" style="margin-left: 0px;">
<!-- 		<input type="hidden" id="sampleOut_name" -->
<%-- 			value="<s:property value="sampleOut.name"/>">  --%>
			<input
			type="hidden" id="bpmTaskId" value="${requestScope.bpmTaskId}" />
		<section class="content">
		<div class="row">
			<!--表格-->
			<div class="col-xs-12">
				<div class="box box-info box-solid">
					<div class="box-header with-border">
						<i class="fa fa-bell-o"></i>
						<h3 class="box-title"><fmt:message key="biolims.tStorage.storageBox" />
							<small style="color: #fff;">
							</small>
						</h3>
					</div>
				<div class="box-body ipadmini">

						<div class="HideShowPanel">
							<form name="form1" class="layui-form" id="form1" method="post">
							</form>
						</div>

	</br>


						<!--库存样本-->
						<div class="col-md-12 col-xs-12">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title"><fmt:message key="biolims.tStorage.boxManagement" /></h3>
								</div>
							<div class="box-body ipadmini">
									<table
										class="table table-hover table-striped table-bordered table-condensed"
										id="storageBoxTablediv" style="font-size: 14px;"></table>
								</div>
							</div>
						</div>

				</div>
			</div>

		</div>
		</section>
	</div>
	<script type="text/javascript"
		src="${ctx}/js/system/storage/storageBoxTable.js"></script>
</body>

</html>