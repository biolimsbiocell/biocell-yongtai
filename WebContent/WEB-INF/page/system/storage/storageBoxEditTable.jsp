﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style type="text/css">
.dt-buttons {
	float: none;
}

.table th, .table td {
	white-space: nowrap;
}

.tablebtns {
	position: initial;
}

.col-xs-4 {
	margin-top: 5px;
}
</style>
</head>

<body style="height: 94%">
	<div class="content-wrapper" id="content" style="margin-left: 0px;">
		<!-- 		<input type="hidden" id="sampleOut_name" -->
		<%-- 			value="<s:property value="sampleOut.name"/>">  --%>
		<input type="hidden" id="bpmTaskId" value="${requestScope.bpmTaskId}" />
		<section class="content">
		<div class="row">
			<!--表格-->
			<div class="col-xs-12">
				<div class="box box-info box-solid">
					<div class="box-header with-border">
						<i class="fa fa-bell-o"></i>
						<h3 class="box-title">
							存储盒子 <small style="color: #fff;"> </small>
						</h3>
					</div>
					<div class="box-body ipadmini">

						<div class="HideShowPanel">
							<form name="form1" class="layui-form" id="form1" method="post">
								<div class="row">
									<div class="col-xs-4">
										<div class="input-group">

											<span class="input-group-addon"><fmt:message
													key="biolims.common.serialNumber" /><img
												class='requiredimage' src='${ctx}/images/required.gif' /> </span>
											<input type="text" size="20" maxlength="25"
												readOnly="readOnly" id="storageBox_id" name="storageBox.id"
												changelog="<s:property value="storageBox.id"/>"
												class="form-control"
												value="<s:property value="storageBox.id"/>" />

										</div>
									</div>
									<div class="col-xs-4">
										<div class="input-group">

											<span class="input-group-addon"><fmt:message
													key="biolims.common.describe" /><img class='requiredimage'
												src='${ctx}/images/required.gif' /> </span> <input type="text"
												size="20" maxlength="25" readOnly="readOnly"
												id="storageBox_name" name="storageBox.name"
												changelog="<s:property value="storageBox.name"/>"
												class="form-control"
												value="<s:property value="storageBox.name"/>" />

										</div>
									</div>

									<div class="col-xs-4">
										<div class="input-group">

											<span class="input-group-addon"><fmt:message
													key="biolims.storage.container" /><img
												class='requiredimage' src='${ctx}/images/required.gif' /> </span>
											<input type="text" size="20" maxlength="25"
												readOnly="readOnly" id="storageBox_container_name"
												name="storageBox.storageContainer.name"
												changelog="<s:property value="storageBox.storageContainer.name"/>"
												class="form-control"
												value="<s:property value="storageBox.storageContainer.name"/>" />
											<input type="hidden" size="20" maxlength="25"
												readOnly="readOnly" id="storageBox_container_id"
												name="storageBox.storageContainer.id"
												changelog="<s:property value="storageBox.storageContainer.id"/>"
												class="form-control"
												value="<s:property value="storageBox.storageContainer.id"/>" />

										</div>
									</div>

								</div>

								<div class="row">

									<div class="col-xs-4">
										<div class="input-group">

											<span class="input-group-addon"><fmt:message
													key="biolims.common.storageName" /><img
												class='requiredimage' src='${ctx}/images/required.gif' /> </span>
											<input type="text" size="20" maxlength="25"
												readOnly="readOnly" id="storageBox_newLocationId"
												name="storageBox.newLocationId"
												changelog="<s:property value="storageBox.newLocationId"/>"
												class="form-control"
												value="<s:property value="storageBox.newLocationId"/>" /> <input
												type="hidden" size="20" maxlength="25" readOnly="readOnly"
												id="storageBox_oldLocationId"
												name="storageBox.oldLocationId"
												changelog="<s:property value="storageBox.oldLocationId"/>"
												class="form-control"
												value="<s:property value="storageBox.oldLocationId"/>" />

										</div>
									</div>

									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon"><fmt:message key="biolims.common.state"/></span> <select
												class="form-control" lay-ignore="" id="storageBox_state" disabled
												name="storageBox.state" >
												<option value="0"
													<s:if test="storageBox.state==0">selected="selected"</s:if>>完成</option>
												<option value="1"
													<s:if test="storageBox.state==1">selected="selected"</s:if>>新建</option>
											</select>
										</div>
									</div>
									<%-- <div class="col-xs-4">
										<div class="input-group">

											<span class="input-group-addon"><fmt:message key="biolims.common.state"/><img
												class='requiredimage' src='${ctx}/images/required.gif' />
											</span> <input type="text" size="20" maxlength="25" readOnly="readOnly"
												id="storageBox_state" name="storageBox.state"
												changelog="<s:property value="storageBox.stateName"/>"
												class="form-control"
												value="<s:property value="storageBox.stateName"/>" />

										</div>
									</div> --%>
								</div>
							</form>
						</div>

						</br>






						<!--迁移记录-->
						<div class="col-md-6 col-xs-12">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title">迁移记录</h3>
								</div>
								<div class="box-body ipadmini">
									<table
										class="table table-hover table-striped table-bordered table-condensed"
										id="moveBoxdiv" style="font-size: 14px;"></table>
								</div>
							</div>
						</div>
						<!--样本明细-->
						<div class="col-md-6 col-xs-12">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title">样本明细</h3>
								</div>
								<div class="box-body ipadmini">
									<table
										class="table table-hover table-striped table-bordered table-condensed"
										id="sampleAlldiv" style="font-size: 14px;"></table>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>

		</div>
		</section>
	</div>
	<script type="text/javascript"
		src="${ctx}/js/system/storage/moveBox.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/system/storage/sampleAll.js"></script>
</body>

</html>