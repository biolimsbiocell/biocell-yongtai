﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<script language="javascript">
		Ext
				.onReady(function() {
					Ext.QuickTips.init();
					Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
					var doclinks_img = Ext.get('doclinks_img');
					doclinks_img
							.on(
									'click',
									function doc() {
										var win = Ext.getCmp('doc');
if (win) {win.close();}
var doc= new Ext.Window({
													id : 'doc',
													modal : true,
													title : '<fmt:message key="biolims.common.attachment"/>',
													layout : 'fit',
													width : 900,
													height : 500,
													closeAction : 'close',
													plain : true,
													bodyStyle : 'padding:5px;',
													buttonAlign : 'center',
													collapsible : true,
													maximizable : true,
													items : new Ext.BoxComponent(
															{
																id : 'maincontent',
																region : 'center',
																html : "<iframe scrolling='no' name='maincontentframe' src='/operfile/initFileList.action\?modelType=storageBox&id=&flag=doc' frameborder='0' width='100%' height='100%' ></iframe>"
															}),
													buttons : [ {
														text : biolims.common.close,
														handler : function() {
															doc.close();
														}
													} ]
												});
										doc.show();
									});
				});
	</script>

</s:if>
<body>
	<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
	<script type="text/javascript"
		src="${ctx}/js/system/storage/storageBoxEdit.js"></script>
	<script type="text/javascript" src="${ctx}/javascript/handleVaLeave.js"></script>
	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass">
		<input type="hidden" id="handlemethod"
			value="${requestScope.handlemethod}">
		<form name="form1" id="form1" method="post">
			<table class="frame-table">
				<tr>


					<td class="label-title"><fmt:message
							key="biolims.common.serialNumber" /></td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/required.gif' /></td>
					<td align="left"><input type="text" size="20"
						maxlength="25" id="storageBox_id" name="storageBox.id"
						title="<fmt:message key="biolims.common.serialNumber"/>"
						value="<s:property value="storageBox.id"/>" />

					</td>


					<td class="label-title"><fmt:message
							key="biolims.common.describe" /></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="50" maxlength="50"
						id="storageBox_name" name="storageBox.name"
						title="<fmt:message key="biolims.common.describe"/>"
						value="<s:property value="storageBox.name"/>" />

					</td>
					<%-- <td class="label-title" ><fmt:message key="biolims.common.storageCode"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="storageBox_locationId"
                   	 name="storageBox.locationId" title="<fmt:message key="biolims.common.storageCode"/>" value="<s:property value="storageBox.locationId"/>"
                   	  />
                   	  <img alt='<fmt:message key="biolims.common.selectLocation"/>' id='showlocation' src='${ctx}/images/img_lookup.gif'  onClick='FrozenLocationFun();'	class='detail'    />
                   	</td> --%>
					<script language="javascript">
						Ext
								.onReady(function() {
									Ext.QuickTips.init();
									Ext.BLANK_IMAGE_URL = window.ctx
											+ "/images/s.gif";
									var storageBox_container_btn = Ext
											.get('storageBox_container_btn');
									storageBox_container_btn
											.on(
													'click',
													function freezeSize() {
														var win = Ext.getCmp('freezeSize');
if (win) {win.close();}
var freezeSize= new Ext.Window({
																	id : 'freezeSize',
																	modal : true,
																	title : '',
																	layout : 'fit',
																	width : 500,
																	height : 500,
																	closeAction : 'close',
																	plain : true,
																	bodyStyle : 'padding:5px;',
																	buttonAlign : 'center',
																	collapsible : true,
																	maximizable : true,
																	items : new Ext.BoxComponent(
																			{
																				id : 'maincontent',
																				region : 'center',
																				html : "<iframe scrolling='no' name='maincontentframe' src='/storage/container/showContainerList.action?flag=freezeSize' frameborder='0' width='100%' height='100%' ></iframe>"
																			}),
																	buttons : [ {
																		text : biolims.common.close,
																		handler : function() {
																			freezeSize
																					.close();
																		}
																	} ]
																});
														freezeSize.show();
													});
								});
						function setfreezeSize(id, name) {
							document.getElementById("storageBox_container_id").value = id;
							document
									.getElementById("storageBox_container_name").value = name;
							var win = Ext.getCmp('freezeSize')
							if (win) {
								win.close();
							}
						}
					</script>

					<td class="label-title"><fmt:message
							key="biolims.storage.container" /></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="storageBox_container_name"
						name="storageBox.storageContainer.name"
						title="<fmt:message key="biolims.storage.container"/>"
						value="<s:property value="storageBox.storageContainer.name"/>" />
						<input type="hidden" size="20" maxlength="25"
						id="storageBox_container_id" name="storageBox.storageContainer.id"
						title="<fmt:message key="biolims.storage.container"/>"
						value="<s:property value="storageBox.storageContainer.id"/>" /> <span
						id="storageBox_container_btn" class="select-search-btn"></span></td>

				</tr>
				<tr>
					<td class="label-title"><fmt:message
							key="biolims.common.storageName" /></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="hidden" size="20"
						maxlength="25" id="storageBox_oldLocationId"
						name="storageBox.oldLocationId" title="<fmt:message key="旧储位号"/>"
						value="<s:property value="storageBox.oldLocationId"/>" />
						<input type="text" size="20" maxlength="25"
						id="storageBox_newLocationId" name="storageBox.newLocationId"
						title="" value="<s:property value="storageBox.newLocationId"/>" />
						<img alt='<fmt:message key="biolims.common.selectLocation"/>'
						id='showlocation' src='${ctx}/images/img_lookup.gif'
						onClick='FrozenLocationFun1();' class='detail' /></td>
					<!--td class="label-title" ><fmt:message key="biolims.common.storageName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="storageBox_locationName"
                   	 name="storageBox.locationName" title="<fmt:message key="biolims.common.storageName"/>"
                   	   
	value="<s:property value="storageBox.locationName"/>"
                   	  />
                   	  
                   	</td-->


					<td class="label-title"><fmt:message
							key="biolims.common.state" /></td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/required.gif' /></td>
					<td align="left"><select id="storageBox_state"
						name="storageBox.state" class="input-10-length">
							<option value="1"
								<s:if test="storageBox.state==1">selected="selected"</s:if>><fmt:message
									key="biolims.common.effective" /></option>
							<option value="0"
								<s:if test="storageBox.state==0">selected="selected"</s:if>><fmt:message
									key="biolims.common.invalid" /></option>
					</select></td>
					<td class="label-title"><fmt:message
							key="biolims.common.attachment" /></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td
						title="<fmt:message key="biolims.common.afterSaveCanLookAtTheAttachment"/>"
						id="doclinks_img"><span class="attach-btn"></span><span
						class="text label"><fmt:message
								key="biolims.common.aTotalOf" />&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
								key="biolims.common.attachment" /></span></td>
				</tr>


			</table>
			<input type="hidden" id="id_parent_hidden"
				value="<s:property value="storageBox.id"/>" />
		</form>
		<div id="tabs">
			<div id="storageBoxPage" width="100%" height:10px></div>
		</div>
		<div id="3d_image"></div>
	</div>
</body>
</html>