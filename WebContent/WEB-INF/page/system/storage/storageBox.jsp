﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/system/storage/storageBox.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<script type="text/javascript" src="${ctx}/javascript/lib/jquery.simple_csv.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title"  style="display:none" ><fmt:message key="biolims.common.serialNumber"/></td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="storageBox_id"
                   	 name="id" searchField="true" title="<fmt:message key="biolims.common.serialNumber"/>"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="storageBox_name"
                   	 name="name" searchField="true" title="<fmt:message key="biolims.common.describe"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.storageCode"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="storageBox_locationId"
                   	 name="locationId" searchField="true" title="<fmt:message key="biolims.common.storageCode"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<%-- <td class="label-title" ><fmt:message key="biolims.common.storageName"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="storageBox_locationName"
                   	 name="locationName" searchField="true" title="<fmt:message key="biolims.common.storageName"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td> --%>
               	 	<td class="label-title" ><fmt:message key="biolims.common.state"/></td>
                   	<td align="left"  >
					<select id="storageBox_state" name="state" searchField="true" class="input-10-length" >
								<option value="1" <s:if test="storageBox.state==1">selected="selected"</s:if>><fmt:message key="biolims.common.effective"/></option>
								<option value="0" <s:if test="storageBox.state==0">selected="selected"</s:if>><fmt:message key="biolims.common.invalid"/></option>
					</select>
 					
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_storageBox_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_storageBox_tree_page"></div>
		<div id="bat_upload_div" style="display: none">
		<input type="file" name="file" id="file-upload">上传csv文件
	</div>
	<div id="bat_box_div" style="display: none">
		<table>		
			<tr>
				<td class="label-title"><span>架子位置</span></td>
				<td><input id="storageBox"/>(请按照原架子位置填写)</td>
			</tr>
		</table>
	</div>
</body>
</html>



