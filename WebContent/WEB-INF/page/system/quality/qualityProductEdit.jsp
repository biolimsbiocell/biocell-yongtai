﻿ <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style type="text/css">
.input-group {
	margin-top: 10px;
}
</style>
</head>
<script>
	$(function() {
		$("#scope_id_div").attr("style", "display:none");
	});
</script>

<body>
	<!--toolbar按钮组-->
	<div>
				<%@ include file="/WEB-INF/page/include/newToolBarNoState.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div class="container-fluid" style="margin-top: 56px">
		<!--form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<fmt:message key="biolims.common.QualityProduct" />
						<!-- 供应商信息 -->
					</h3>
				</div>
				<div class="box-body ipadmini">
					<div class="HideShowPanel">
						<input type="hidden" id="handlemethod"
							value="${requestScope.handlemethod}">
						<form name="form1" class="layui-form" id="form1" method="post">
							<input type="hidden" name="createUser.id"
								value="<s:property value=" qualityProduct.createUser.id "/>" /> <input
								type="hidden" name="createUser.name"
								value="<s:property value=" qualityProduct.createUser.name "/>" /> <input
								type="hidden" name="createDate"
								value="<s:date name=" createDate " format="yyyy-MM-dd " />" />
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.encrypt" /></span> <input
											type="text" id="qualityProduct_id" name="qualityProduct.id"
											class="form-control" changelog="<s:property value=" qualityProduct.id "/>"
											value="<s:property value=" qualityProduct.id "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.describe" /></span> <input
											type="text" id="qualityProduct_name" name="qualityProduct.name"
											class="form-control" changelog="<s:property value=" qualityProduct.name "/>"
											value="<s:property value=" qualityProduct.name "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.commandPerson" /></span> <input
											type="hidden" id="qualityProduct_createUser" name="qualityProduct.createUser.id"
											class="form-control" changelog="<s:property value=" qualityProduct.createUser.id "/>"
											value="<s:property value=" qualityProduct.createUser.id "/>" />
											<input
											type="text" id="qualityProduct_createUser_name" readonly="readonly" name="qualityProduct.createUser.name"
											class="form-control" changelog="<s:property value=" qualityProduct.createUser.name "/>"
											value="<s:property value=" qualityProduct.createUser.name "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.receivingDate" /></span> <input readonly="readonly"
											type="text" id="qualityProduct_createDate" name="qualityProduct.createDate"
											class="form-control" changelog="<s:property value=" qualityProduct.createDate "/>"
											value="<s:date name=" qualityProduct.createDate " format="yyyy-MM-dd " />" />
									</div>
								</div>

								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.expectedValue" /></span> <input
											type="text" class="form-control" id="qualityProduct_expectValue"
											name="qualityProduct.expectValue" changelog="<s:property value=" qualityProduct.expectValue "/>"
											value="<s:property value=" qualityProduct.expectValue "/>" />
									</div>
								</div>
								<!--状态-->
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message key='biolims.common.state'/></span>
										<select lay-ignore id="qualityProduct_state" name="qualityProduct.state" class="form-control" 
										changelog="<s:property value=" qualityProduct.state "/>">
											<option value="1" <s:if test="qualityProduct.state==1">selected="selected"</s:if>>
												<fmt:message key='biolims.common.effective' />
											</option>
											<!-- 有效 -->
											<option value="0" <s:if test="qualityProduct.state==0">selected="selected"</s:if>>
												<fmt:message key='biolims.common.invalid' />
											</option>
											<!-- 无效 -->
										</select>
	
									</div>
								</div>
								
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.experimentalType" /> <!-- 实验类型 --></span> <input
											type="hidden" size="20" id="qualityProduct_experType"
											name="qualityProduct.experType.id"
											value="<s:property value=" qualityProduct.experType.id "/>"
											class="form-control" /> <input type="text" size="20"
											id="qualityProduct_experType_name"
											name="qualityProduct.experType.name" changelog="<s:property value=" qualityProduct.experType.name "/>"
											value="<s:property value=" qualityProduct.experType.name "/>"
											class="form-control" /> <span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onclick="sylxCheck()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>
								<%-- <div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.CostCentre" /></span> <input
											type="hidden" size="20" id="qualityProduct_scopeId"
											name="qualityProduct.scopeId" value="<s:property value=" qualityProduct.scopeId "/>"
											class="form-control" /> <input type="text" size="20"
											id="qualityProduct_scopeName" name="qualityProduct.scopeName" 
											changelog="<s:property value=" qualityProduct.scopeName "/>"
											value="<s:property value=" qualityProduct.scopeName "/>"
											class="form-control" /> <span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onclick="showScope()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div> --%>
								
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
									<button type="button" class="btn btn-info btn-sm" onclick="fileUp()">
										<fmt:message key="biolims.common.uploadAttachment" />
									</button>&nbsp;&nbsp;
									<%-- <span class="text label"><fmt:message
											key="biolims.common.aTotalOf" />${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
											key="biolims.common.attachment" /> --%>
										<button type="button" class="btn btn-info btn-sm"
											onclick="fileView()">
											<fmt:message key="biolims.report.checkFile" />
										</button>
								</div>
								</div>

								<div class="col-xs-12">
									<div class="panel"
										style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
										<div class="panel-heading text-left">
											<h3 class="panel-title" style="font-family: 黑体;">
												<i class="glyphicon glyphicon-bookmark"></i>
												<fmt:message key="biolims.common.customFields" />
												<!-- 账户信息 -->
											</h3>
										</div>
									</div>
								</div>
								<!--
                                	作者：offline
                                	时间：2018-03-19
                                	描述：自定义字段
                                -->
								<div id="fieldItemDiv"></div>
								<br>
								<!-- <input type="text" name="supplierBySerialJson" id="supplierBySerialJson" value="" /> -->

								<input type="hidden" id="fieldContent" name="fieldContent" value="<s:property value=" qualityProduct.fieldContent "/>" />
								<%-- <input type="hidden" id="supplier_scope_id" name="supplier.scope.id" value="<s:property value=" supplier.scope.id "/>" />
								<input type="hidden" id="supplier_scope_name" name="supplier.scope.name" value="<s:property value=" storage.scope.name "/>" /> --%>

								<input type="hidden" id="id_parent_hidden"
									value="<s:property value=" qualityProduct.id "/>" />
								<input type="hidden" id="changeLog" name="changeLog"  />	
							</div>
						</form>
						<div class="HideShowPanel" style="display: none;">
							<table class="table table-hover table-striped table-bordered table-condensed" id="storageReagentBuySerialTable" style="font-size: 14px;">
							</table>
						</div>
					</div>
				</div>
				<div class="box-footer"></div>
			</div>
		</div>
	</div>
	<script type="text/javascript"
			src="${ctx}/js/system/quality/qualityProductEdit.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/common/dataTablesExtend.js"></script>
</body>

</html>