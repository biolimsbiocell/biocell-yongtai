﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/system/primers/mappingPrimersLibraryDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.serialNumber"/></td>
               	<td align="left">
                    		<input type="text" maxlength="30" id="mappingPrimersLibrary_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.describe"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="mappingPrimersLibrary_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.commandPerson"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="mappingPrimersLibrary_createUser" searchField="true" name="createUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.commandTime"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="mappingPrimersLibrary_createDate" searchField="true" name="createDate"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.workflowId"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="mappingPrimersLibrary_state" searchField="true" name="state"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.workflowState"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="mappingPrimersLibrary_stateName" searchField="true" name="stateName"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.detectionItemsCode"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="mappingPrimersLibrary_prodectId" searchField="true" name="prodectId"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.detectionItemsName"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="mappingPrimersLibrary_prodectName" searchField="true" name="prodectName"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue"><fmt:message key="biolims.common.search"/></font></span>
			<span onclick="setValue()" ><font color="blue">确认·</font></span>
			
			
		
		<div id="show_dialog_mappingPrimersLibrary_div"></div>
   		
</body>
</html>



