﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title='<fmt:message key="biolims.common.attachment"/>'
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=primersLibrary&id=${primersLibrary.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/system/primers/primersLibraryEdit.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleVaLeave.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.primersCoding"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="40" maxlength="30" id="primersLibrary_id"
                   	 name="primersLibrary.id" title="<fmt:message key="biolims.common.primersCoding"/>"
                   	   
	value="<s:property value="primersLibrary.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.recipientsDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="primersLibrary_receiveDate"
                   	 name="primersLibrary.receiveDate" title="<fmt:message key="biolims.common.recipientsDate"/>"
                   	   
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="primersLibrary.receiveDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.batch"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="primersLibrary_batch"
                   	 name="primersLibrary.batch" title="<fmt:message key="biolims.common.batch"/>"
                   	   
	value="<s:property value="primersLibrary.batch"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.effectiveDoseNmole"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="primersLibrary_edNmole"
                   	 name="primersLibrary.edNmole" title="<fmt:message key="biolims.common.effectiveDoseNmole"/>"
                   	   
	value="<s:property value="primersLibrary.edNmole"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.periodOfValidity"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="primersLibrary_expiryDate"
                   	 name="primersLibrary.expiryDate" title="<fmt:message key="biolims.common.periodOfValidity"/>"
                   	   
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="primersLibrary.expiryDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.totalPrimers"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="primersLibrary_referTotal"
                   	 name="primersLibrary.referTotal" title="<fmt:message key="biolims.common.totalPrimers"/>"
                   	   
	value="<s:property value="primersLibrary.referTotal"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.consumption"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="primersLibrary_hasDosage"
                   	 name="primersLibrary.hasDosage" title="<fmt:message key="biolims.common.consumption"/>"
                   	   
	value="<s:property value="primersLibrary.hasDosage"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.remainingAmount"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="primersLibrary_surplus"
                   	 name="primersLibrary.surplus" title="<fmt:message key="biolims.common.remainingAmount"/>"
                   	   
	value="<s:property value="primersLibrary.surplus"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td style="display: none;" class="label-title" ><fmt:message key="biolims.common.usingState"/></td>
               	 	<td style="display: none;" class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td style="display: none;" align="left"  >
                   	<input style="display: none;" type="text" size="20" maxlength="25" id="primersLibrary_state"
                   	 name="primersLibrary.state" title="<fmt:message key="biolims.common.usingState"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="primersLibrary.state"/>"
                   	  />
                   	  
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.usingStateName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="primersLibrary_stateName"
                   	 name="primersLibrary.stateName" title="<fmt:message key="biolims.common.usingStateName"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="primersLibrary.stateName"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			<td class="label-title"><fmt:message key="biolims.common.attachment"/></td><td></td>
						<td title="<fmt:message key="biolims.common.afterSaveCanLookAtTheAttachment"/>" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>${requestScope.fileNum}<fmt:message key="biolims.common.attachment"/></span>
			</tr>
			
			
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="primersLibrary.id"/>" />
            </form>
            <div id="tabs">
            <ul>
           	</ul> 
			</div>
        	</div>
	</body>
	</html>
