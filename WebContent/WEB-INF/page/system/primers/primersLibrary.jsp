﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/system/primers/primersLibrary.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.primersCoding"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="40" maxlength="30" id="primersLibrary_id"
                   	 name="id" searchField="true" title="<fmt:message key="biolims.common.primersCoding"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.site"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="40" maxlength="30" id="primersLibrary_site"
                   	 name="site" searchField="true" title="<fmt:message key="biolims.common.site"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.recipientsDate"/></td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startreceiveDate" name="startreceiveDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="receiveDate1" name="receiveDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endreceiveDate" name="endreceiveDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="receiveDate2" name="receiveDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.batch"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="primersLibrary_batch"
                   	 name="batch" searchField="true" title="<fmt:message key="biolims.common.batch"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.effectiveDoseNmole"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="primersLibrary_edNmole"
                   	 name="edNmole" searchField="true" title="<fmt:message key="biolims.common.effectiveDoseNmole"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.periodOfValidity"/></td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startexpiryDate" name="startexpiryDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="expiryDate1" name="expiryDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endexpiryDate" name="endexpiryDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="expiryDate2" name="expiryDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.totalPrimers"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="primersLibrary_referTotal"
                   	 name="referTotal" searchField="true" title="<fmt:message key="biolims.common.totalPrimers"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.consumption"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="primersLibrary_hasDosage"
                   	 name="hasDosage" searchField="true" title="<fmt:message key="biolims.common.consumption"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.remainingAmount"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="primersLibrary_surplus"
                   	 name="surplus" searchField="true" title="<fmt:message key="biolims.common.remainingAmount"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.usingState"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="primersLibrary_state"
                   	 name="state" searchField="true" title="<fmt:message key="biolims.common.usingState"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.usingStateName"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="primersLibrary_stateName"
                   	 name="stateName" searchField="true" title="<fmt:message key="biolims.common.usingStateName"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_primersLibrary_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_primersLibrary_tree_page"></div>
</body>
</html>



