<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<title></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style>
.dataTables_scrollBody {
	min-height: 100px;
}
</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
			<%@ include file="/WEB-INF/page/include/newToolBarNoState.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div style="height: 14px"></div>
	<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 46px">
		<!--Form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<i class="glyphicon glyphicon-pencil"></i><fmt:message key="biolims.common.productSite"/>
						<small style="color: #fff;"> <fmt:message key="biolims.equipment.taskId"/>: <span id="mappingPrimersLibrary_id"><s:property value="mappingPrimersLibrary.id" /></span>
								<fmt:message key="biolims.common.commandPerson"/>: <span
								userId="<s:property value="instrumentRepair.creatUser.id"/>"
								id="mappingPrimersLibrary_createUser"><s:property
										value="mappingPrimersLibrary.createUser.name" /></span> <fmt:message key="biolims.common.commandTime"/>: <span
								id="mappingPrimersLibrary_createDate"><s:date name=" mappingPrimersLibrary.createDate " format="yyyy-MM-dd "/></span> <fmt:message key="biolims.tInstrument.state"/>: <span
								state="<s:property value="mappingPrimersLibrary.state"/>"
								id="mappingPrimersLibrary_state"><s:property
										value="mappingPrimersLibrary.stateName" /></span>
							</small>
					</h3>

					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
											<li>
												<a href="####" onclick="$('.buttons-print').click();"><fmt:message key="biolims.common.print"/></a>
											</li>
											<li>
												<a href="#####" onclick="$('.buttons-copy').click();"><fmt:message key="biolims.common.copyData"/></a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-excel').click();">Excel</a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-csv').click();">CSV</a>
											</li>
											<li role="separator" class="divider"></li>
											<li>
												<a href="####" onclick="itemFixedCol(2)"><fmt:message key="biolims.common.lock2Col"/></a>
											</li>
											<li>
												<a href="####" id="unfixde" onclick="unfixde()"><fmt:message key="biolims.common.cancellock2Col"/></a>
											</li>
										</ul>
						</div>
					</div>
				</div>
				<div class="box-body ipadmini">
					<!--进度条-->
					<!--form表单-->
					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
							<input type="hidden" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId")%>" /> <br>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<input type="hidden"
											size="20" maxlength="25" id="mappingPrimersLibraryId"
											name="id" value="<s:property value="mappingPrimersLibrary.id"/>"
						 			/> 
						 			<input type="hidden"
												id="mappingPrimersLibrary_createUserId" name="createUser-id"
												value="<s:property value="mappingPrimersLibrary.createUser.id"/>"
									/>
									<input type="hidden"
											size="20" maxlength="25" id="mappingPrimersLibraryCreateDate"
											name="createDate" value="<s:date name="mappingPrimersLibrary.createDate" format="yyyy-MM-dd"/>"
                   	   					/> 
                   	   				<input type="hidden"
											size="20" maxlength="25" id="mappingPrimersLibraryState"
											name="state" value="<s:property value="mappingPrimersLibrary.state"/>"
										/>  
									<input type="hidden"
											size="20" maxlength="25" id="mappingPrimersLibraryStateName"
											name="stateName" value="<s:property value="mappingPrimersLibrary.stateName"/>"
										 />  	
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.mappingPrimersLibrary.name"/> </span> 
									
										
                   						<input type="text"
											size="50" maxlength="50" id="mappingPrimersLibrary_name"
											name="name"  changelog="<s:property value="mappingPrimersLibrary.name"/>"
											class="form-control" value="<s:property value="mappingPrimersLibrary.name"/>"
										 />  
					
									</div>
								</div>		
								<div class="col-md-4 col-sm-6 col-xs-12">
										<input type="hidden"
											size="20" maxlength="25" id="mappingPrimersLibrary_prodectId"
											name="prodectId" value="<s:property value="mappingPrimersLibrary.prodectId"/>"
						 				/>
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.mappingPrimersLibrary.prodectName"/> </span> 
                   						<input type="text"
											size="20" maxlength="25" id="mappingPrimersLibrary_prodectName"
											name="prodectName"  changelog="<s:property value="mappingPrimersLibrary.prodectName"/>"
											 class="form-control"   readonly="readonly"
											value="<s:property value="mappingPrimersLibrary.prodectName"/>"
						 				/>  
										<span class="input-group-btn">
												<button class="btn btn-info" type="button" id='showage'
													onClick="voucherProductFun()">
													<i class="glyphicon glyphicon-search"></i>
												</button>
										</span>
									</div>
								</div>	
								<div class="col-md-4 col-sm-6 col-xs-12">	
									<div class="input-group">
									<button type="button" class="btn btn-info btn-sm" onclick="fileUp()">
										<fmt:message key="biolims.common.uploadAttachment" />
									</button>&nbsp;&nbsp;
									<%-- <span class="text label"><fmt:message
											key="biolims.common.aTotalOf" />${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
											key="biolims.common.attachment" /> --%>
										<button type="button" class="btn btn-info btn-sm"
											onclick="fileView()">
											<fmt:message key="biolims.report.checkFile" />
										</button>
								</div>
								</div>
							
							</div>
							<div id="fieldItemDiv"></div>
								
							<br> 
							<input type="hidden" name="mappingPrimersLibraryItemJson"
								id="mappingPrimersLibraryItemJson" value="" /> 
								
							<input type="hidden"
								id="id_parent_hidden"
								value="<s:property value="mappingPrimersLibrary.id"/>" />
							<input type="hidden" id="fieldContent" name="fieldContent" value="<s:property value="mappingPrimersLibrary.fieldContent"/>" />	
								
						</form>
					</div>
					<!--table表格-->
					<div class="box-footer ipadmini">
					
					  	<div class="HideShowPanel"  class="box-body ipadmini">
						 <table
							class="table table-hover table-striped table-bordered table-condensed"
							id="mappingPrimersLibraryItemTable" style="font-size: 14px;">
						</table>
						</div>
				</div>	
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript" src="${ctx}/js/system/primers/mappingPrimersLibraryEdit.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/system/primers/mappingPrimersLibraryItem.js"></script>
</body>

</html>	
