﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="${ctx}/js/system/primers/primersLibraryDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.primersCoding"/></td>
               	<td align="left">
                    		<input type="text" maxlength="30" id="primersLibrary_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.recipientsDate"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="primersLibrary_receiveDate" searchField="true" name="receiveDate"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.batch"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="primersLibrary_batch" searchField="true" name="batch"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.effectiveDoseNmole"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="primersLibrary_edNmole" searchField="true" name="edNmole"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.periodOfValidity"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="primersLibrary_expiryDate" searchField="true" name="expiryDate"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.totalPrimers"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="primersLibrary_referTotal" searchField="true" name="referTotal"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.consumption"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="primersLibrary_hasDosage" searchField="true" name="hasDosage"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.remainingAmount"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="primersLibrary_surplus" searchField="true" name="surplus"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.usingState"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="primersLibrary_state" searchField="true" name="state"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.usingStateName"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="primersLibrary_stateName" searchField="true" name="stateName"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue"><fmt:message key="biolims.common.search"/></font></span>
		
		<div id="show_dialog_primersLibrary_div"></div>
   		
</body>
</html>



