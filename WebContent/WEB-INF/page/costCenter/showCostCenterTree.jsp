<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>

<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGridSorter.js"></script>
<script type="text/javascript" src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGridColumnResizer.js"></script>
<script type="text/javascript" src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGridNodeUI.js"></script>
<script type="text/javascript" src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGridLoader.js"></script>
<script type="text/javascript" src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGridColumns.js"></script>
<script type="text/javascript" src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGrid.js"></script>
<script type="text/javascript" src="${ctx}/javascript/costCenter/showCostCenterTree.js"></script>
<g:HandleDataExtTag hasConfirm="false" paramsValue="costCenterId:'${costCenterId}'" funName="modifyData"
	url="${ctx}/costCenter/saveCostCenter.action" refreshMethod="Ext.getCmp('reloadtreeall').handler();" />
<g:HandleDataExtTag funName="delData" hasConfirm="true" confirmTitle="是否确认删除？"
	url="${ctx}/costCenter/delCostCenter.action" refreshMethod="Ext.getCmp('reloadtree').handler();" />
<g:LayOutWinTag isHasButton="false" title="选择组织" hasHtmlFrame="true" width="800" height="520"
	html="${ctx}/core/departmentSelect.action" isHasSubmit="false"
	functionName="showDepartment" hasSetFun="true"  extRec="id,name" extStr="Ext.getCmp(\"department\").setValue(id);"/>
<script>
	
</script>
</head>
<body>


	<input type="hidden" id="id" value="" />
	<input type="hidden" id="name" value="" />
	<input type="hidden" id="leaf" value="" />
	<input type="hidden" id="costCenterJsonPath" value="${path}">

	<table>
		<tr>
			<td>
				<div class="mainlistclass" id="markup"></div>
			</td>
			<td></td>
		</tr>
	</table>


</body>
</html>