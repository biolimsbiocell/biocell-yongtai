<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML>
<html>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<head>
		<title>
			<fmt:message key="biolims.common.noTitleDocuments" />
		</title>
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
		<style type="text/css">
			#costCenter .col-xs-6{
				margin-top: 15px;
			}
		</style>
	</head>

	<body style="height: 85%;">
			<div class="row" id="costCenter" style="margin-top: 15px; padding-left: 48px;">
				
			</div>

		<script type="text/javascript" src="${ctx}/javascript/costCenter/selectCostCenter.js"></script>
	</body>

</html>