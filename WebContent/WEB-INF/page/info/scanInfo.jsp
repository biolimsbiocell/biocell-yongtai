<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/javascript/lib/ckeditor/ckeditor.js"></script>
</head>
<body>
	<%@ include file="/WEB-INF/page/include/toolbarNoTab.jsp"%>
		<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass">
	<g:LayOutWinTag buttonId="doclinks_img" title="附件" hasHtmlFrame="true"
		width="900" height="500"
		html="${ctx}/operfile/initFileList.action?view=true&modelType=info&id=${info.id}"
		isHasSubmit="false" functionName="doc" />
		<table width="100%" style="display:none" cellpadding="0" cellspacing="0">
			<tbody>
			<tr class="control textboxcontrol">
				<td>
					<table width="90%" cellpadding="0" cellspacing="0">	
						<tr>
							<td valign="top" class="controlTitle" nowrap><span
								class="labelspacer">&nbsp;&nbsp;</span> <label title="标题"
								class="text label">标题 </label></td>
							<td class="requiredcolumn" nowrap width="10px"><img
								class='requiredimage' src='${ctx}/images/required.gif' /></td>
							<td nowrap>
								<s:property value="info.title"/>
							</td>
							
							<td valign="top" class="controlTitle" nowrap><span
								class="labelspacer">&nbsp;&nbsp;</span> <label title="创建用户 "
								class="text label">创建用户 </label></td>
							<td class="requiredcolumn" nowrap width="10px"></td>
							<td nowrap>
								<s:property value="info.createUser.name"/>
					
							</td>
						</tr>	
						<tr>
							
							<td valign="top" class="controlTitle" nowrap><span
								class="labelspacer">&nbsp;&nbsp;</span> <label title="创建日期"
								class="text label">创建日期 </label></td>
							<td class="requiredcolumn" nowrap width="10px"></td>
							<td nowrap>
								<s:date name="info.createDate" format="yyyy-MM-dd" />
							</td>
							
							<td valign="top" class="controlTitle" nowrap></td>
							<td class="requiredcolumn" nowrap width="10px"></td>
							<td nowrap>
								
							</td>
						</tr>
						<tr >	
							
							
								<td valign="top"  class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="附件" class="text label">附件</label>&nbsp;
								</td>
								<td class="requiredcolumn" nowrap width="10px"><img
									class='requiredimage' src='${ctx}/images/notrequired.gif' />
								</td>
								<td align="left"><img alt="保存基本后,可以维护查看附件"
									id="doclinks_img" class="detail"
									src="${ctx}/images/img_attach.gif" /></td>
									<td></td>
								<td></td>
						</tr>
					</table>
						
				</td>
		
			</tr>
					
			</tbody>
		</table>
	
	
		<table align="center">
		<tr class="control textboxcontrol">
				
					
							<td nowrap>
							<%out.print(request.getAttribute("info.context")); %>
						</td>
						
					</tr>
			
		</table>
		
	</div>

</body>
</html>