<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page//include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>

<script type="text/javascript"
	src="${ctx}/javascript/info/info.js"></script>
<script type="text/javascript" src="${ctx}/javascript/lib/ckeditor/ckeditor.js"></script>
</head>
<body>
	<%@ include file="/WEB-INF/page/include/toolbarNoTab.jsp"%>
		<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass">
	<g:LayOutWinTag buttonId="doclinks_img" title="附件" hasHtmlFrame="true"
		width="900" height="500"
		html="${ctx}/operfile/initFileList.action?modelType=info&id=${info.id}"
		isHasSubmit="false" functionName="doc" />
		<g:LayOutWinTag buttonId="searchtype" title="选择项目类型" hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action" isHasSubmit="false" functionName="xmlx" hasSetFun="true" documentId="info_type_id" documentName="info_type_name" />
<s:form theme="simple" method="post" action="/info/editInfo.action" name="infoForm">
	
	<s:hidden name="info.id" id="info.id" ></s:hidden>
		<table width="100%" cellpadding="0" cellspacing="0">
			<tbody>
			<tr class="control textboxcontrol">
				<td>
					<table width="90%" cellpadding="0" cellspacing="0">	
						<tr>
							<td valign="top" class="controlTitle" nowrap><span
								class="labelspacer">&nbsp;&nbsp;</span> <label title="标题"
								class="text label">标题 </label></td>
							<td class="requiredcolumn" nowrap width="10px"><img
								class='requiredimage' src='${ctx}/images/required.gif' /></td>
							<td nowrap>
								<s:textfield name="info.title" id="info.title"
								onblur="textbox_ondeactivate(event);" width="100" style="width:400"
								onfocus="shared_onfocus(event,this)"
								cssClass="input_parts text input default  readonlyfalse false"></s:textfield>
							</td>
							
							<td valign="top" class="controlTitle" nowrap><span
								class="labelspacer">&nbsp;&nbsp;</span> <label title="创建用户 "
								class="text label">创建用户 </label></td>
							<td class="requiredcolumn" nowrap width="10px"></td>
							<td nowrap>
								<s:textfield name="info.createUser.name" id="info.createUser.name"
								onblur="textbox_ondeactivate(event);"
								onfocus="shared_onfocus(event,this)"
								cssClass="input_parts text input default  readonlytrue false" readonly="true"></s:textfield>
								<s:hidden name="info.createUser.id"></s:hidden>
							</td>
						</tr>	
						<tr>
							
							<td valign="top" class="controlTitle" nowrap><span
								class="labelspacer">&nbsp;&nbsp;</span> <label title="创建日期"
								class="text label">创建日期 </label></td>
							<td class="requiredcolumn" nowrap width="10px"></td>
							<td nowrap>
								<input type="text" name="info.createDate" id="info.createDate"
								onblur="textbox_ondeactivate(event);"
								onfocus="shared_onfocus(event,this)"
								class="input_parts text input default  readonlytrue false" readonly="readonly" value='<s:date name="info.createDate" format="yyyy-MM-dd" />'>
							</td>
							
							<td valign="top" class="controlTitle" nowrap><span
								class="labelspacer">&nbsp;&nbsp;</span> <label title="是否生效"
								class="text label">是否生效 </label></td>
							<td class="requiredcolumn" nowrap width="10px"></td>
							<td nowrap>
								<s:select list="#{'0':'否','1':'是' }"  name="info.infoState" listKey="key" listValue="value"></s:select>
							</td>
						</tr>
						<tr>	
											<td valign="top" align="right" class="controlTitle" nowrap>
																<span class="labelspacer">&nbsp;&nbsp;</span>
																<label title="类型" class="text label"> 类型 </label>
															</td>
															<td class="requiredcolumn" nowrap width="10px">
																<img class='requiredimage' src='${ctx}/images/notrequired.gif' />
															</td>
															<td nowrap>
																<s:hidden name="info.type.id" id="info_type_id"></s:hidden>
																<s:textfield class="input_parts text input default  readonlyfalse false" name="info.type.name" id="info_type_name" title="类型  " readonly="true" onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="10" maxlength="30"></s:textfield>
																<img alt='选择' id='searchtype' src='${ctx}/images/img_lookup.gif' class='detail' />
															</td>
															<td nowrap>&nbsp;</td>
							
								<td valign="top"  class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="附件" class="text label">附件</label>&nbsp;
								</td>
								<td class="requiredcolumn" nowrap width="10px"><img
									class='requiredimage' src='${ctx}/images/notrequired.gif' />
								</td>
								<td align="left"><img alt="保存基本后,可以维护查看附件"
									id="doclinks_img" class="detail"
									src="${ctx}/images/img_attach.gif" /></td>
									<td></td>
								<td></td>
						</tr>
					</table>
						
				</td>
		
			</tr>
					
			</tbody>
		</table>
		<table style="width:90%;">
		<tr class="control textboxcontrol">
				<td>
					
						<td valign="top" class="controlTitle" nowrap><span
							class="labelspacer">&nbsp;&nbsp;</span> <label title="信息"
							class="text label">信息 </label></td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td nowrap>
							<textarea class="text input  false" rows="10" cols="100"	name="info.context"
										id="info.context" tabindex="0" title="备注 " 
										style="overflow: hidden;"><s:property value="info.context" /></textarea>
						</td>
						<td nowrap>&nbsp;</td>
					</tr>
			<script type="text/javascript">
				CKEDITOR.replace( 'info.context',
						{
							skin : 'office2003',
							height: "350"
						});
				
			
			</script>
		</table>
		</s:form>
	</div>

</body>
</html>