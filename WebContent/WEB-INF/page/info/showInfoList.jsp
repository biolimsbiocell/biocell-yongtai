<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>信息发布管理</title>
<script type="text/javascript" src="${ctx}/javascript/info/showInfoList.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
	<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>


	<div class="mainlistclass" id="markup">
		<div id="jstj" style="display: none"></div>
		<input type="hidden" name="id" id="id">
		<g:GridTag isAutoWidth="true" col="${col}" expandColumn="note"  type="${type}" title="信息发布" url="${path}" />
	</div>
</body>
</html>