<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <title>我审批的流程</title> -->
<head>
<link rel=stylesheet type="text/css" href="${ctx}/css/css-style.css">
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<jsp:include page="/WEB-INF/page/workflow/include/workflow.jsp"/>
<script type="text/javascript" src="${ctx}/js/pccheck/showPCCheckList.js"></script>
<g:LayOutWinTag buttonId="searchdutyuser" title="选择发起人" hasHtmlFrame="true" height="400" html="${ctx}/core/user/userSelect.action" isHasSubmit="false"  functionName="zrr" hasSetFun="true" extRec="id,name" extStr="document.getElementById('send_user').value=id;document.getElementById('send_user_name').value=name;" />
<g:LayOutWinTag buttonId="searchForm" title="选择表单类型" hasHtmlFrame="true" height="400" html="${ctx}/applicationTypeTable/applicationTypeTableSelect.action" isHasSubmit="false" functionName="searchFormFun" hasSetFun="true" extRec="id,name" extStr="document.getElementById('form_name').value=id;document.getElementById('form_name_name').value=name;" />

</head>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoButton.jsp"%>
<div>
	<div class="main-content">
		<table>
		
			<tr>
				<td>
					<span class="label-title">发起人</span>
					<input type='hidden' name="send_user" id="send_user">
					<input type='text' name="send_user_name" id="send_user_name">
					<img alt='选择发起人' id='searchdutyuser' src='${ctx}/images/img_lookup.gif' name='searchtype' class='detail' />
				</td>
				<td>
					<span class="label-title">业务ID</span>
					<input type='text' name="form_id" id="form_id">
				</td>
				<td>
					<span class="label-title">表单类型ID</span>
					<input type='hidden' name="form_name" id="form_name">
					<input type='text' name="form_name_name" id="form_name_name">
					<img alt='选择表单类型' id='searchForm' src='${ctx}/images/img_lookup.gif' name='searchForm' class='detail' />
				</td>
				<td>
					<button id="search_btn">搜索</button>
				</td>
			</tr>
		</table>
	</div>
	<div id="show_pc_check_div"></div>
</div>
</body>
</html>