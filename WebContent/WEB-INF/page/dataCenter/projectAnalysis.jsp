<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<!-- Font Awesome -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
		<style type="text/css">
			.icheckbox_square-blue {
				margin-right: 10px;
			}
			
			.label {
				float: right;
			}
			
			.panel-title {
				font-size: 14px;
			}
			
			#main th,
			#main td {
				white-space: nowrap !important;
			}
			
			.glyphicon {
				margin-right: 3px;
			}
			
			.tablebtns {
				float: right;
			}
			
			.dt-buttons {
				margin: 0;
				float: right;
			}
			
			.dt-button-collection {
				font-size: 12px;
				max-height: 260px;
				overflow: auto;
			}
			
			.input-group-addon {
				font-size: 12px;
			}
			
			.box-title {
				display: inline-block;
			}
			
			.toggleButton {
				display: inline-block;
				margin-left: 20px;
			}
			
			.toggleButton>input {
				margin-right: 10px;
				border: 0;
				height: 30px;
				;
			}
		</style>
	</head>

	<body style="height:94%">
		<!--<div>
			<%@ include file="/WEB-INF/page/include/newToolbar.jsp"%>
		</div>-->
		<div class="content-wrapper" id="content" style="margin-left: 0px;">
			<section class="content">
				<div class="row">
					<!--表格-->
					<div class="col-xs-12 col-md-12">
						<div class="box box-info box-solid" id="box">
							<div class="box-header with-border">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title">数据中心
									<small style="color: #fff;">项目分析
										</small></h3>
							</div>
							<div class="box-body">
								<!--查询条件-->
								<div class="col-xs-4">
									<div class="box box-primary">
										<div class="box-header with-border">
											<i class="fa fa-leaf"></i>
											<h3 class="box-title"><fmt:message key="biolims.common.retrieve" /></h3>
										</div>
										<div class="box-body" id="searchOption">
											<div class="input-group">
												<span class="input-group-addon">临床诊断</span> 
												<input list="sampleName"type="text" class="form-control" />
											</div>
										</div>
										<div class="box-footer">
											<!--其他检索条件-->
											<div id="searchOther">
												<h3 class="panel-title">
													<i class="glyphicon glyphicon-bookmark"></i>
													检测项目
												</h3>
												<div style="min-height: 30px;overflow:hidden;">
													<div class="checkbox">
														<label>
															<input type="checkbox" value="">
															全外显子检测
														</label>
														<span class="label label-success"></span>
													</div>
													<div class="checkbox">
														<label>
															<input type="checkbox" value="">
															肿瘤优选46基因检测
														</label>
														<span class="label label-success"></span>
													</div>
													<div class="checkbox">
														<label>
															<input type="checkbox" value="">
														脑胶质瘤全能基因检测
														</label>
														<span class="label label-success"></span>
													</div>
												</div>
												<button open="N" class="btn btn-xs btn-primary center-block">More...</button>
												<h3 class="panel-title">
													<i class="glyphicon glyphicon-bookmark"></i>
													组织取材部位
												</h3>
												<div style="min-height: 30px;overflow:hidden;">
													<div class="checkbox">
														<label>
															<input type="checkbox" value="">
															脑
														</label>
														<span class="label label-success"></span>
													</div>
													<div class="checkbox">
														<label>
															<input type="checkbox" value="">
															肝
														</label>
														<span class="label label-success"></span>
													</div>
													<div class="checkbox">
														<label>
															<input type="checkbox" value="">
														肺
														</label>
														<span class="label label-success"></span>
													</div>
												</div>
												<button open="N" class="btn btn-xs btn-primary center-block">More...</button>
												<h3 class="panel-title">
													<i class="glyphicon glyphicon-bookmark"></i>
													检测平台
												</h3>
												<div style="min-height: 30px;overflow:hidden;">
													<div class="checkbox">
														<label>
															<input type="checkbox" value="">
															Sanger
														</label>
														<span class="label label-success"></span>
													</div>
													<div class="checkbox">
														<label>
															<input type="checkbox" value="">
															NGS
														</label>
														<span class="label label-success"></span>
													</div>
													<div class="checkbox">
														<label>
															<input type="checkbox" value="">
														Massarray
														</label>
														<span class="label label-success"></span>
													</div>
												</div>
												<button open="N" class="btn btn-xs btn-primary center-block">More...</button>
												<h3 class="panel-title">
													<i class="glyphicon glyphicon-bookmark"></i>
													疾病
												</h3>
												<div style="min-height: 30px;overflow:hidden;">
													<div class="checkbox">
														<label>
															<input type="checkbox" value="">
															脑胶质瘤
														</label>
														<span class="label label-success"></span>
													</div>
													<div class="checkbox">
														<label>
															<input type="checkbox" value="">
															胃腺癌
														</label>
														<span class="label label-success"></span>
													</div>
													<div class="checkbox">
														<label>
															<input type="checkbox" value="">
														肺腺癌
														</label>
														<span class="label label-success"></span>
													</div>
												</div>
												<button open="N" class="btn btn-xs btn-primary center-block">More...</button>
											</div>
										</div>
									</div>
								</div>
								<div class="col-xs-8">
									<div class="row">
										<div class="col-xs-12">
											<div class="box box-warning">
												<div class="box-header with-border">
													<i class="fa fa-leaf"></i>
													<h3 class="box-title">图表展示</h3>
													<div class="toggleButton">
														<button type="button" onclick="columnar()" class="btn btn-default active">
														  <span class="glyphicon" aria-hidden="true">柱状</span>
														</button>
														<button type="button" onclick="lineChart()" class="btn btn-default">
														  <span class="glyphicon" aria-hidden="true">折线</span>
														</button>
														<button type="button" onclick="Pancake()" class="btn btn-default">
														  <span class="glyphicon" aria-hidden="true">饼状</span>
														</button>
													</div>
												</div>
												<div class="box-body">
													<div id="sampleBar" style="height:250px;"></div>
												</div>
											</div>
										</div>
									</div>

									<div class="box box-info">
										<div class="box-header with-border">
											<i class="fa fa-leaf"></i>
											<h3 class="box-title"><fmt:message key="biolims.order.surface" /></h3>
										</div>
										<div class="box-body ipadmini">
											<table class="table table-hover table-striped table-bordered table-condensed" id="main" style="font-size:12px;">
												<thead>
													<tr>
														<th>订单编号</th>
														<th>性别</th>
														<th>年龄</th>
														<th>电子病历号</th>
														<th>检测项目</th>
														<th>SNP数</th>
														<th>CNV数</th>
														<th>基因重排数</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>dada548756</td>
														<td>男</td>
														<td>24</td>
														<td><a href="/analysis/data/dataTask/dossier.action" target="iframename">dkdji7898</a></td>
														<td>全外显子检测</td>
														<td>
															<a href="####" class="snpAdnCnv">24</a>
														</td>
														<td>
															<a href="####" class="snpAdnCnv">08</a>
														</td>
														<td>
															<a href="####" class="snpAdnCnv">46</a>
														</td>
													</tr>
													<tr>
														<td>fgshgf548756</td>
														<td>男</td>
														<td>25</td>
														<td><a href="/analysis/data/dataTask/dossier.action" target="iframename">dkddsfs898</a></td>
														<td>肿瘤优选46基因检测</td>
														<td>
															<a href="####" class="snpAdnCnv">54</a>
														</td>
														<td>
															<a href="####" class="snpAdnCnv">66</a>
														</td>
														<td>
															<a href="####" class="snpAdnCnv">25</a>
														</td>
													</tr>
													<tr>
														<td>434ddd48756</td>
														<td>女</td>
														<td>36</td>
														<td><a href="/analysis/data/dataTask/dossier.action" target="iframename">675545554d</a></td>
														<td>脑胶质瘤全能基因检测</td>
														<td>
															<a href="####" class="snpAdnCnv">22</a>
														</td>
														<td>
															<a href="####" class="snpAdnCnv">33</a>
														</td>
														<td>
															<a href="####" class="snpAdnCnv">03</a>
														</td>
													</tr>
													<tr>
														<td>dada548756</td>
														<td>男</td>
														<td>24</td>
														<td><a href="/analysis/data/dataTask/dossier.action" target="iframename">dkdji7898</a></td>
														<td>全外显子检测</td>
														<td>
															<a href="####" class="snpAdnCnv">24</a>
														</td>
														<td>
															<a href="####" class="snpAdnCnv">08</a>
														</td>
														<td>
															<a href="####" class="snpAdnCnv">46</a>
														</td>
													</tr>
													<tr>
														<td>fgshgf548756</td>
														<td>男</td>
														<td>25</td>
														<td><a href="/analysis/data/dataTask/dossier.action" target="iframename">dkddsfs898</a></td>
														<td>肿瘤优选46基因检测</td>
														<td>
															<a href="####" class="snpAdnCnv">54</a>
														</td>
														<td>
															<a href="####" class="snpAdnCnv">66</a>
														</td>
														<td>
															<a href="####" class="snpAdnCnv">25</a>
														</td>
													</tr>
													<tr>
														<td>434ddd48756</td>
														<td>女</td>
														<td>36</td>
														<td><a href="/analysis/data/dataTask/dossier.action" target="iframename">675545554d</a></td>
														<td>脑胶质瘤全能基因检测</td>
														<td>
															<a href="####" class="snpAdnCnv">22</a>
														</td>
														<td>
															<a href="####" class="snpAdnCnv">33</a>
														</td>
														<td>
															<a href="####" class="snpAdnCnv">03</a>
														</td>
													</tr>
													<tr>
														<td>dada548756</td>
														<td>男</td>
														<td>24</td>
														<td><a href="/analysis/data/dataTask/dossier.action" target="iframename">dkdji7898</a></td>
														<td>全外显子检测</td>
														<td>
															<a href="####" class="snpAdnCnv">24</a>
														</td>
														<td>
															<a href="####" class="snpAdnCnv">08</a>
														</td>
														<td>
															<a href="####" class="snpAdnCnv">46</a>
														</td>
													</tr>
													<tr>
														<td>fgshgf548756</td>
														<td>男</td>
														<td>25</td>
														<td><a href="/analysis/data/dataTask/dossier.action" target="iframename">dkddsfs898</a></td>
														<td>肿瘤优选46基因检测</td>
														<td>
															<a href="####" class="snpAdnCnv">54</a>
														</td>
														<td>
															<a href="####" class="snpAdnCnv">66</a>
														</td>
														<td>
															<a href="####" class="snpAdnCnv">25</a>
														</td>
													</tr>
													<tr>
														<td>434ddd48756</td>
														<td>女</td>
														<td>36</td>
														<td><a href="/analysis/data/dataTask/dossier.action" target="iframename">675545554d</a></td>
														<td>脑胶质瘤全能基因检测</td>
														<td>
															<a href="####" class="snpAdnCnv">22</a>
														</td>
														<td>
															<a href="####" class="snpAdnCnv">33</a>
														</td>
														<td>
															<a href="####" class="snpAdnCnv">03</a>
														</td>
													</tr>
													<tr>
														<td>dada548756</td>
														<td>男</td>
														<td>24</td>
														<td><a href="/analysis/data/dataTask/dossier.action" target="iframename">dkdji7898</a></td>
														<td>全外显子检测</td>
														<td>
															<a href="####" class="snpAdnCnv">24</a>
														</td>
														<td>
															<a href="####" class="snpAdnCnv">08</a>
														</td>
														<td>
															<a href="####" class="snpAdnCnv">46</a>
														</td>
													</tr>
													<tr>
														<td>fgshgf548756</td>
														<td>男</td>
														<td>25</td>
														<td><a href="/analysis/data/dataTask/dossier.action" target="iframename">dkddsfs898</a></td>
														<td>肿瘤优选46基因检测</td>
														<td>
															<a href="####" class="snpAdnCnv">54</a>
														</td>
														<td>
															<a href="####" class="snpAdnCnv">66</a>
														</td>
														<td>
															<a href="####" class="snpAdnCnv">25</a>
														</td>
													</tr>
													<tr>
														<td>434ddd48756</td>
														<td>女</td>
														<td>36</td>
														<td><a href="/analysis/data/dataTask/dossier.action" target="iframename">675545554d</a></td>
														<td>脑胶质瘤全能基因检测</td>
														<td>
															<a href="####" class="snpAdnCnv">22</a>
														</td>
														<td>
															<a href="####" class="snpAdnCnv">33</a>
														</td>
														<td>
															<a href="####" class="snpAdnCnv">03</a>
														</td>
													</tr>
													
												
													
												</tbody>
											</table>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>

				</div>

			</section>
		</div>
		<script src="${ctx}/lims/plugins/chartjs/echarts.js" type="text/javascript" charset="utf-8"></script>
		<!--<script type="text/javascript" src="${ctx}/javascript/common/searchDataEvery.js"></script>-->
		<script type="text/javascript" src="${ctx}/js/dataCenter/projectAnalysis.js"></script>
	</body>

</html>