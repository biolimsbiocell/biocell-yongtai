<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE html>
<html>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<head>
		<title>biolims</title>

		<link rel="stylesheet" href="${ctx}/lims/plugins/layui/css/layui.css">
		<script type="text/javascript" src="${ctx}/lims/plugins/layui/layui.js"></script>
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
		<style type="text/css">
			#editrights .col-xs-6 {
				margin-top: 15px;
			}
			.layui-form-checkbox .layui-icon{
				display: none;
			}
			.layui-form-checkbox {
				    margin-left: -8px;
			}
			.strong{
				font-weight: 600;
			}
			/*.layui-xtree-item{
				height: 18px;
			}*/
		</style>
	</head>

	<body>
		<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
		<div class="container-fluid" style="margin: 5px">
			<div class="row" style="padding: 0px">
				<div class="box box-info box-solid">
					<div class="box-header with-border">
						<i class="fa fa-bell-o"></i>
						<h3 class="box-title">
					病历夹
				</h3>
					</div>
					<div class="box-body ipadmini">
						<!--主表-->
						<div class="col-xs-9">
							<div class="box box-info">
								<div class="box-header with-border">
									<i class="fa fa-bell-o"></i>
									<h3 class="box-title">
									主表
								</h3>
								</div>
								<div class="box-body ipadmini">
									<table class="table table-striped table-hover table-condensed">
										<tr>
											<th>case UUID</th>
											<td>30bc72d5-07b5-48d2-b025-bba9bcf2f09f</td>
										</tr>
										<tr>
											<th>case ID</th>
											<td>TCGA-EO-A22U</td>
										</tr>
										<tr>
											<th>Project</th>
											<td>TCGA-UCEC</td>
										</tr>
										<tr>
											<th>Project Name</th>
											<td>Uterine Corpus Endometrial Carcinoma</td>
										</tr>
										<tr>
											<th>Disease Type</th>
											<td>Uterine Corpus Endometrial Carcinoma</td>
										</tr>
										<tr>
											<th>Program</th>
											<td>TCGA</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						<div class="col-xs-3">
							<div class="small-box bg-primary">
								<div class="inner">
									<h3 class="numtwo">25</h3>
									<p>
										xxxx提示
									</p>
								</div>
								<div class="icon">
									<i class="fa fa-eyedropper"></i>
								</div>
								<a href="####" target="iframename" class="small-box-footer">
									查看详情 <i class="fa fa-arrow-circle-right"></i></a>
							</div>
							<div class="small-box bg-green">
								<div class="inner">
									<h3 class="numtwo">08</h3>
									<p>
										xxxx数量
									</p>
								</div>
								<div class="icon">
									<i class="fa fa-desktop"></i>
								</div>
								<a href="####" target="iframename" class="small-box-footer">
									查看详情 <i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div>
						<div class="col-xs-12" style="border: 1px solid #eee; height: 1px; margin: 10px 0px;"></div>
						<div class="col-xs-6">
							<form class="layui-form">
								<div class="box box-primary">
									<div class="box-header with-border">
										<i class="glyphicon glyphicon-leaf"></i>
										<h3 class="box-title">家系病历</h3>
									</div>
									<div class="box-body" id="xtree1" style="min-height: 150px;font-size: 12px;">
										<div class="animal" style="margin: 75px auto; text-align: center;">
											<span class="layui-icon layui-anim layui-anim-rotate layui-anim-loop" style="font-size: 50px; color: rgb(0, 150, 136); font-weight: bold;">&#xe63e;</span>
										</div>
									</div>
								</div>
							</form>
						</div>
						<div class="col-xs-6">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title">详细情况</h3>
								</div>
								<div class="box-body" id="xtree2" style="min-height: 150px;">
									<table class="table table-striped table-hover">
										<tr>
											<th>家系编号</th>
											<td>30bc72d5</td>
										</tr>
										<tr>
											<th>创建时间</th>
											<td>2018-06-01</td>
										</tr>
										<tr>
											<th>创建人</th>
											<td>郭先森</td>
										</tr>
										<tr>
											<th>检验所</th>
											<td>百奥利盟检验所</td>
										</tr>
									</table>
									<table class="table table-striped table-hover" style="display: none;">
										<tr>
											<th>姓名</th>
											<td>郭二狗</td>
										</tr>
										<tr>
											<th>年龄</th>
											<td>24</td>
										</tr>
										<tr>
											<th>性别</th>
											<td>男</td>
										</tr>
										<tr>
											<th>病史</th>
											<td>费德哥尔摩懒癌晚期</td>
										</tr>
										<tr>
											<th>检测项目</th>
											<td>CPX003--肿瘤检测</td>
										</tr>
										<tr>
											<th>申请医生</th>
											<td>王大夫</td>
										</tr>
										<tr>
											<th>送检科室</th>
											<td>心胸内科</td>
										</tr>
										<tr>
											<th>创建时间</th>
											<td>2018-06-03</td>
										</tr>
									</table>
									<table class="table table-striped table-hover" style="display: none;">
										<tr>
											<th>订单编号</th>
											<td>dfda21321ddaf</td>
										</tr>
										<tr>
											<th>送检单位</th>
											<td>北京协和</td>
										</tr>
										<tr>
											<th>报告邮箱</th>
											<td>dsafa@Gmail.com</td>
										</tr>
										<tr>
											<th>收费方式</th>
											<td>微信</td>
										</tr>
										<tr>
											<th>录入时间</th>
											<td>2018-06-13</td>
										</tr>
									</table>
									<table class="table table-striped table-hover" style="display: none;">
										<tr>
											<th>样本编号</th>
											<td>d888821321ddaf</td>
										</tr>
										<tr>
											<th>样本类型</th>
											<td>血浆</td>
										</tr>
										<tr>
											<th>样本条码</th>
											<td>234568734676543</td>
										</tr>
										<tr>
											<th>批次</th>
											<td>01</td>
										</tr>
										<tr>
											<th>采样日期</th>
											<td>2018-06-13</td>
										</tr>
									</table>
									<table class="table table-striped table-hover" style="display: none;">
										<tr>
											<th>实验编号</th>
											<td>d888123232321ddaf</td>
										</tr>
										<tr>
											<th>实验类型</th>
											<td>核酸提取</td>
										</tr>
										<tr>
											<th>实验环境</th>
											<td>无菌</td>
										</tr>
										<tr>
											<th>实验员</th>
											<td>郭二狗</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="${ctx}/lims/plugins/layui-xtree/layui-xtree.js" type="text/javascript"></script>
		<script type="text/javascript" src="${ctx}/js/dataCenter/dossier.js"></script>
	</body>

</html>