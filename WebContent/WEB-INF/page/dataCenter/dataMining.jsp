<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<!-- Font Awesome -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
		<style type="text/css">
			.icheckbox_square-blue {
				margin-right: 10px;
			}
			
			.label {
				float: right;
			}
			
			.panel-title {
				font-size: 14px;
			}
			
			#main th,
			#main td {
				white-space: nowrap !important;
			}
			
			.glyphicon {
				margin-right: 3px;
			}
			
			.tablebtns {
				float: right;
			}
			
			.dt-buttons {
				margin: 0;
				float: right;
			}
			
			.dt-button-collection {
				font-size: 12px;
				max-height: 260px;
				overflow: auto;
			}
			
			.input-group-addon {
				font-size: 12px;
			}
			
			.box-title {
				display: inline-block;
			}
			
			.toggleButton {
				display: inline-block;
				margin-left: 20px;
			}
			
			.toggleButton>input {
				margin-right: 10px;
				border: 0;
				height: 30px;
			}
			
			.button .btn-primary {
				width: 100%;
			}
			
			.iconH3 {
				margin-top: 6px;
				padding: 0;
			}
			
			#sampleBar>div {
				height: 100%;
				padding: 0;
			}
			
			#line>div,
			canvas {
				width: 50%;
			}
			
			#sampleBar>div,
			canvas {
				width: 50%;
			}
			.tab-content > .tab-pane,
			.pill-content > .pill-pane {
			display: block; /* undo display:none */
			height: 0; /* height:0 is also invisible */
			overflow-y: hidden; /* no-overflow */
			}
			.tab-content > .active,
			.pill-content > .active {
			height: auto; /* let the content decide it */
			} /* bootstrap hack end */
		</style>
	</head>

	<body style="height:94%">
		<!--<div>
			<%@ include file="/WEB-INF/page/include/newToolbar.jsp"%>
		</div>-->
		<div class="content-wrapper" id="content" style="margin-left: 0px;">
			<section class="content">
				<div class="row">
					<!--表格-->
					<div class="col-xs-12 col-md-12">
						<div class="box box-info box-solid" id="box">
							<div class="box-header with-border">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title">数据中心
									<small style="color: #fff;">数据挖掘
										</small></h3>
							</div>
							<div class="box-body">
								<!--查询条件-->
								<div class="col-xs-4">
									<div>
										<ul class="nav nav-tabs" role="tablist">
											<li role="presentation" class="active">
												<a href="#home" aria-controls="home" role="tab" data-toggle="tab">基因</a>
											</li>
											<li role="presentation">
												<a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">突变</a>
											</li>
										</ul>

										<!-- 基因 -->
										<div class="tab-content">
											<div role="tabpanel" class="tab-pane active" id="home">
												<div class="box box-primary">
													<div class="box-header with-border">
														<div class="col-xs-3 iconH3">
															<i class="fa fa-leaf"></i>
															<h3 class="box-title" id="searchOption" style="display: inline-block;"><fmt:message key="biolims.common.retrieve" /></h3>
														</div>
														<div class="input-group input-group-sm col-xs-9">
															<span class="input-group-addon">临床诊断1</span>
															<input list="sampleName" type="text" class="form-control" />
														</div>
													</div>
													<!--大按钮坑-->
													<div class="button"><button type="button" class="btn btn-primary" data-toggle="button" aria-pressed="false" autocomplete="off">
										  批量基因搜索
										</button></div>

													<div class="box-footer">
														<!--其他检索条件-->
														<div id="searchOther">
															<h3 class="panel-title">
													<i class="glyphicon glyphicon-bookmark"></i>
													生物型
												</h3>
															<div style="min-height: 30px;overflow:hidden;">
																<div class="checkbox">
																	<label>
															<input type="checkbox" value="">
															protein
														</label>
																	<span class="label label-success"></span>
																</div>
																<div class="checkbox">
																	<label>
															<input type="checkbox" value="">
															lincRNA
														</label>
																	<span class="label label-success"></span>
																</div>
																<div class="checkbox">
																	<label>
															<input type="checkbox" value="">
														miRNA
														</label>
																	<span class="label label-success"></span>
																</div>
															</div>
															<button open="N" class="btn btn-xs btn-primary center-block">More...</button>
															<h3 class="panel-title">
													<i class="glyphicon glyphicon-bookmark"></i>
													致癌型
												</h3>
															<div style="min-height: 30px;overflow:hidden;">
																<div class="checkbox">
																	<label>
															<input type="checkbox" value="">
															原癌
														</label>
																	<span class="label label-success"></span>
																</div>
																<div class="checkbox">
																	<label>
															<input type="checkbox" value="">
															抑癌
														</label>
																	<span class="label label-success"></span>
																</div>
																<div class="checkbox">
																	<label>
															<input type="checkbox" value="">
														未知
														</label>
																	<span class="label label-success"></span>
																</div>
															</div>
															<button open="N" class="btn btn-xs btn-primary center-block">More...</button>
														</div>
													</div>
												</div>
											</div>
											<!--突变-->
											<div role="tabpanel" class="tab-pane" id="profile">
												<div class="box box-primary">
													<div class="box-header with-border">
														<div class="col-xs-3 iconH3">
															<i class="fa fa-leaf"></i>
															<h3 class="box-title" id="searchOption" style="display: inline-block;"><fmt:message key="biolims.common.retrieve" /></h3>
														</div>
														<div class="input-group input-group-sm col-xs-9">
															<span class="input-group-addon">临床诊断2</span>
															<input list="sampleName" type="text" class="form-control" />
														</div>
													</div>
													<div class="button"><button type="button" class="btn btn-primary" data-toggle="button" aria-pressed="false" autocomplete="off">
										  批量突变搜索
										</button></div>
													<div class="box-footer">
														<!--其他检索条件-->
														<div id="searchOther">
															<h3 class="panel-title">
													<i class="glyphicon glyphicon-bookmark"></i>
													突变类型
												</h3>
															<div style="min-height: 30px;overflow:hidden;">
																<div class="checkbox">
																	<label>
															<input type="checkbox" value="">
															错义
														</label>
																	<span class="label label-success"></span>
																</div>
																<div class="checkbox">
																	<label>
															<input type="checkbox" value="">
															无义
														</label>
																	<span class="label label-success"></span>
																</div>
																<div class="checkbox">
																	<label>
															<input type="checkbox" value="">
														移位
														</label>
																	<span class="label label-success"></span>
																</div>
															</div>
															<button open="N" class="btn btn-xs btn-primary center-block">More...</button>
															<h3 class="panel-title">
													<i class="glyphicon glyphicon-bookmark"></i>
													突变效用
												</h3>
															<div style="min-height: 30px;overflow:hidden;">
																<div class="checkbox">
																	<label>
															<input type="checkbox" value="">
															失能突变
														</label>
																	<span class="label label-success"></span>
																</div>
																<div class="checkbox">
																	<label>
															<input type="checkbox" value="">
															获能突变
														</label>
																	<span class="label label-success"></span>
																</div>
															</div>
															<button open="N" class="btn btn-xs btn-primary center-block">More...</button>
															<h3 class="panel-title">
													<i class="glyphicon glyphicon-bookmark"></i>
													致病型
												</h3>
															<div style="min-height: 30px;overflow:hidden;">
																<div class="checkbox">
																	<label>
															<input type="checkbox" value="">
															突发
														</label>
																	<span class="label label-success"></span>
																</div>
																<div class="checkbox">
																	<label>
															<input type="checkbox" value="">
															恶性
														</label>
																	<span class="label label-success"></span>
																</div>
																<div class="checkbox">
																	<label>
															<input type="checkbox" value="">
															未知
														</label>
																	<span class="label label-success"></span>
																</div>
															</div>
															<button open="N" class="btn btn-xs btn-primary center-block">More...</button>
														</div>
													</div>
												</div>
											</div>
										</div>

									</div>
								</div>
								<div class="col-xs-8">
									<div>

										<!-- 基因右侧图表 -->
										<ul class="nav nav-tabs" role="tablist">
											<li role="presentation" class="active">
												<a href="#jiyin" aria-controls="jiyin" role="tab" data-toggle="tab">基因</a>
											</li>
											<li role="presentation">
												<a href="#tubian" aria-controls="tubian" role="tab" data-toggle="tab">突变</a>
											</li>
										</ul>

										<!-- Tab panes -->
										<div class="tab-content">
											<div role="tabpanel" class="tab-pane active" id="jiyin">
												<div class="row">
													<div class="col-xs-12">

														<div class="box box-warning">
															<div class="box-header with-border">
																<i class="fa fa-leaf"></i>
																<h3 class="box-title">基因</h3>
																<!--<div class="toggleButton">
														<button type="button" onclick="columnar()" class="btn btn-default active">
														  <span class="glyphicon" aria-hidden="true">柱状</span>
														</button>
														<button type="button" onclick="lineChart()" class="btn btn-default">
														  <span class="glyphicon" aria-hidden="true">折线</span>
														</button>
														<button type="button" onclick="Pancake()" class="btn btn-default">
														  <span class="glyphicon" aria-hidden="true">饼状</span>
														</button>
													</div>-->
															</div>
															<div class="box-body">
																<div id="sampleBar" style="height:250px;">
																	<div id="lineChart" class="col-xs-12 col-sm-6"></div>
																	<div id="columnar" class="col-xs-12 col-sm-6"></div>
																</div>
															</div>
														</div>
													</div>
												</div>

												<div class="box box-info">
													<div class="box-header with-border">
														<i class="fa fa-leaf"></i>
														<h3 class="box-title"><fmt:message key="biolims.order.surface" />基因</h3>
													</div>
													<div class="box-body ipadmini">

														<table class="table table-hover table-striped table-bordered table-condensed" style="font-size:12px;">
															<thead>
																<tr>
																	<th>基因</th>
																	<th>突变类型</th>
																	<th>ctDNA突变比例</th>
																	<th>组织样本突变比例</th>
																	
																	<th>染色体</th>
																	<th>转录本编号</th>
																</tr>
															</thead>
															<tbody>
																<tr>
																	<td>ARIDIA</td>
																	<td>缺失移码突变</td>
																	<td>-</td>
																	<td>12.5</td>
																	<td>1</td>
																	<td>NM_006015.4</td>
																</tr>
																<tr>
																	<td>SMARCBI</td>
																	<td>错义突变</td>
																	<td>-</td>
																	<td>14</td>
																	<td>22</td>
																	<td>NM_003073.3</td>
																</tr>
																<tr>
																	<td>TP53</td>
																	<td>错义突变</td>
																	<td>-</td>
																	<td>18.6</td>
																	<td>17</td>
																	<td>NM_001126.2</td>
																</tr>
																<tr>
																	<td>ARIDIA</td>
																	<td>缺失移码突变</td>
																	<td>-</td>
																	<td>12.5</td>
																	<td>1</td>
																	<td>NM_006015.4</td>
																</tr>
																<tr>
																	<td>SMARCBI</td>
																	<td>错义突变</td>
																	<td>-</td>
																	<td>14</td>
																	<td>22</td>
																	<td>NM_003073.3</td>
																</tr>
																<tr>
																	<td>TP53</td>
																	<td>错义突变</td>
																	<td>-</td>
																	<td>18.6</td>
																	<td>17</td>
																	<td>NM_001126.2</td>
																</tr>
																<tr>
																	<td>ARIDIA</td>
																	<td>缺失移码突变</td>
																	<td>-</td>
																	<td>12.5</td>
																	<td>1</td>
																	<td>NM_006015.4</td>
																</tr>
																<tr>
																	<td>SMARCBI</td>
																	<td>错义突变</td>
																	<td>-</td>
																	<td>14</td>
																	<td>22</td>
																	<td>NM_003073.3</td>
																</tr>
																<tr>
																	<td>TP53</td>
																	<td>错义突变</td>
																	<td>-</td>
																	<td>18.6</td>
																	<td>17</td>
																	<td>NM_001126.2</td>
																</tr>
															</tbody>
														</table>

													</div>
												</div>
											</div>
											<div role="tabpanel" class="tab-pane" id="tubian">
												<div class="row">
													<div class="col-xs-12">
														<div class="box box-warning">
															<div class="box-header with-border">
																<i class="fa fa-leaf"></i>
																<h3 class="box-title">突变</h3>
																<!--<div class="toggleButton">
														<button type="button" onclick="columnar()" class="btn btn-default active">
														  <span class="glyphicon" aria-hidden="true">柱状</span>
														</button>
														<button type="button" onclick="lineChart()" class="btn btn-default">
														  <span class="glyphicon" aria-hidden="true">折线</span>
														</button>
														<button type="button" onclick="Pancake()" class="btn btn-default">
														  <span class="glyphicon" aria-hidden="true">饼状</span>
														</button>
													</div>-->
															</div>
															<div class="box-body">
																<div id="sampleBar" style="height:250px;">
																	<div id="line" class="col-xs-12 col-sm-6" style="height:100%;width:50%;"></div>
																	<div id="colu" class="col-xs-12 col-sm-6" style="height:100%;width:50%;"></div>
																</div>
															</div>
														</div>
													</div>
												</div>

												<div class="box box-info">
													<div class="box-header with-border">
														<i class="fa fa-leaf"></i>
														<h3 class="box-title"><fmt:message key="biolims.order.surface" />突变</h3>
													</div>
													<div class="box-body ipadmini">
														<table class="table table-hover table-striped table-bordered table-condensed" id="main" style="font-size:12px;">
															<thead>
																<tr>
																	<th>订单编号</th>
																	<th>性别</th>
																	<th>年龄</th>
																	<th>电子病历号</th>
																	<th>检测项目</th>
																	<th>SNP数</th>
																	<th>CNV数</th>
																	<th>基因重排数</th>
																</tr>
															</thead>
															<tbody>
																<tr>
																	<td>dada548756</td>
																	<td>男</td>
																	<td>24</td>
																	<td>dkdji7898</td>
																	<td>全外显子检测</td>
																	<td>
																		<a href="####">24</a>
																	</td>
																	<td>
																		<a href="####">08</a>
																	</td>
																	<td>
																		<a href="####">46</a>
																	</td>
																</tr>
																<tr>
																	<td>fgshgf548756</td>
																	<td>男</td>
																	<td>25</td>
																	<td>dkddsfs898</td>
																	<td>肿瘤优选46基因检测</td>
																	<td>
																		<a href="####">54</a>
																	</td>
																	<td>
																		<a href="####">66</a>
																	</td>
																	<td>
																		<a href="####">25</a>
																	</td>
																</tr>
																<tr>
																	<td>434ddd48756</td>
																	<td>女</td>
																	<td>36</td>
																	<td>675545554d</td>
																	<td>脑胶质瘤全能基因检测</td>
																	<td>
																		<a href="####">22</a>
																	</td>
																	<td>
																		<a href="####">33</a>
																	</td>
																	<td>
																		<a href="####">03</a>
																	</td>
																</tr>
																<tr>
																	<td>dada548756</td>
																	<td>男</td>
																	<td>24</td>
																	<td>dkdji7898</td>
																	<td>全外显子检测</td>
																	<td>
																		<a href="####">24</a>
																	</td>
																	<td>
																		<a href="####">08</a>
																	</td>
																	<td>
																		<a href="####">46</a>
																	</td>
																</tr>
																<tr>
																	<td>fgshgf548756</td>
																	<td>男</td>
																	<td>25</td>
																	<td>dkddsfs898</td>
																	<td>肿瘤优选46基因检测</td>
																	<td>
																		<a href="####">54</a>
																	</td>
																	<td>
																		<a href="####">66</a>
																	</td>
																	<td>
																		<a href="####">25</a>
																	</td>
																</tr>
																<tr>
																	<td>434ddd48756</td>
																	<td>女</td>
																	<td>36</td>
																	<td>675545554d</td>
																	<td>脑胶质瘤全能基因检测</td>
																	<td>
																		<a href="####">22</a>
																	</td>
																	<td>
																		<a href="####">33</a>
																	</td>
																	<td>
																		<a href="####">03</a>
																	</td>
																</tr>
																<tr>
																	<td>dada548756</td>
																	<td>男</td>
																	<td>24</td>
																	<td>dkdji7898</td>
																	<td>全外显子检测</td>
																	<td>
																		<a href="####">24</a>
																	</td>
																	<td>
																		<a href="####">08</a>
																	</td>
																	<td>
																		<a href="####">46</a>
																	</td>
																</tr>
																<tr>
																	<td>fgshgf548756</td>
																	<td>男</td>
																	<td>25</td>
																	<td>dkddsfs898</td>
																	<td>肿瘤优选46基因检测</td>
																	<td>
																		<a href="####">54</a>
																	</td>
																	<td>
																		<a href="####">66</a>
																	</td>
																	<td>
																		<a href="####">25</a>
																	</td>
																</tr>
																<tr>
																	<td>434ddd48756</td>
																	<td>女</td>
																	<td>36</td>
																	<td>675545554d</td>
																	<td>脑胶质瘤全能基因检测</td>
																	<td>
																		<a href="####">22</a>
																	</td>
																	<td>
																		<a href="####">33</a>
																	</td>
																	<td>
																		<a href="####">03</a>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>

									</div>

								</div>

							</div>
						</div>
					</div>

				</div>

			</section>
		</div>
		<script src="${ctx}/lims/plugins/chartjs/echarts.js" type="text/javascript" charset="utf-8"></script>
		<!--<script type="text/javascript" src="${ctx}/javascript/common/searchDataEvery.js"></script>-->
		<script type="text/javascript" src="${ctx}/js/dataCenter/dataMining.js"></script>
	</body>

</html>