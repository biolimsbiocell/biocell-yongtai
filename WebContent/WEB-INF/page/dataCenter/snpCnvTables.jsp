<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<!-- Font Awesome -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
		<style type="text/css">
			.icheckbox_square-blue {
				margin-right: 10px;
			}
			
			.label {
				float: right;
			}
			
			.panel-title {
				font-size: 14px;
			}
			
			#main th,
			#main td {
				white-space: nowrap !important;
			}
			
			.glyphicon {
				margin-right: 3px;
			}
			
			.tablebtns {
				float: right;
			}
			
			.dt-buttons {
				margin: 0;
				float: right;
			}
			
			.dt-button-collection {
				font-size: 12px;
				max-height: 260px;
				overflow: auto;
			}
			
			.input-group-addon {
				font-size: 12px;
			}
			
			.box-title {
				display: inline-block;
			}
			
			.toggleButton {
				display: inline-block;
				margin-left: 20px;
			}
			
			.toggleButton>input {
				margin-right: 10px;
				border: 0;
				height: 30px;
				;
			}
		</style>
	</head>

	<body style="height:94%">
		<!--<div>
			<%@ include file="/WEB-INF/page/include/newToolbar.jsp"%>
		</div>-->
		<div class="content-wrapper" id="content" style="margin-left: 0px;">
			<section class="content">
				<div class="row">
					<!--表格-->
					<div class="col-xs-12 col-md-12">
						<div class="box box-info box-solid" id="box">
							<div class="box-header with-border">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title">数据中心
									<small style="color: #fff;">SNP、CNV、基因重排
										</small></h3>
							</div>
							<div class="box-body">
								<!--饼图-->
								<div class="col-xs-3">
									<div class="box box-warning">
										<div class="box-header with-border">
											<i class="fa fa-leaf"></i>
											<h3 class="box-title">
												SNP
											</h3>
										</div>
										<div class="box-body">
											<div id="snp" style="height:180px;"></div>
										</div>
									</div>
								</div>
								<div class="col-xs-3">
									<div class="box box-warning">
										<div class="box-header with-border">
											<i class="fa fa-leaf"></i>
											<h3 class="box-title">
												CNV
											</h3>
										</div>
										<div class="box-body">
											<div id="cnv" style="height:180px;"></div>
										</div>
									</div>
								</div>
								<div class="col-xs-3">
									<div class="box box-warning">
										<div class="box-header with-border">
											<i class="fa fa-leaf"></i>
											<h3 class="box-title">
												基因重排
											</h3>
										</div>
										<div class="box-body">
											<div id="geneReload" style="height:180px;"></div>
										</div>
									</div>
								</div>
								<div class="col-xs-3">
									<div class="box box-warning">
										<div class="box-header with-border">
											<i class="fa fa-leaf"></i>
											<h3 class="box-title">
												突变效应
											</h3>
										</div>
										<div class="box-body">
											<div id="muta" style="height:180px;"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="box-footer">
								<div class="col-xs-12">
									<div class="box box-info">
										<div class="box-header with-border">
											<i class="fa fa-leaf"></i>
											<h3 class="box-title">
												基因点突变/缺失/插入分析情况
											</h3>
										</div>
										<div class="box-body ipadmini">
											<table class="table table-hover table-striped table-bordered table-condensed" style="font-size:12px;">
												<thead>
													<tr>
														<th>基因</th>
														<th>突变类型</th>
														<th>ctDNA突变比例</th>
														<th>组织样本突变比例</th>
														<th>核苷酸变化</th>
														<th>氨基酸变化</th>
														<th>染色体</th>
														<th>转录本编号</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>ARIDIA</td>
														<td>缺失移码突变</td>
														<td>-</td>
														<td>12.5</td>
														<td>c.2911delG</td>
														<td>p.G971fs</td>
														<td>1</td>
														<td>NM_006015.4</td>
													</tr>
													<tr>
														<td>SMARCBI</td>
														<td>错义突变</td>
														<td>-</td>
														<td>14</td>
														<td>c.1129C>T</td>
														<td>p.R377C</td>
														<td>22</td>
														<td>NM_003073.3</td>
													</tr>
													<tr>
														<td>TP53</td>
														<td>错义突变</td>
														<td>-</td>
														<td>18.6</td>
														<td>c.743G>AT</td>
														<td>p.R248Q</td>
														<td>17</td>
														<td>NM_001126.2</td>
													</tr>
													<tr>
														<td>ARIDIA</td>
														<td>缺失移码突变</td>
														<td>-</td>
														<td>12.5</td>
														<td>c.2911delG</td>
														<td>p.G971fs</td>
														<td>1</td>
														<td>NM_006015.4</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="col-xs-12">
									<div class="box box-info">
										<div class="box-header with-border">
											<i class="fa fa-leaf"></i>
											<h3 class="box-title">
												拷贝数分析结果
											</h3>
										</div>
										<div class="box-body ipadmini">
											<table class="table table-hover table-striped table-bordered table-condensed" style="font-size:12px;">
												<thead>
													<tr>
														<th>基因</th>
														<th>染色体</th>
														<th>起始位置</th>
														<th>终止位置</th>
														<th>ctDNA样本改变倍数</th>
														<th>组织样本改变倍数</th>
														<th>改变类型</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>CCND1</td>
														<td>11</td>
														<td>-</td>
														<td>-</td>
														<td>-</td>
														<td>2.6</td>
														<td>基因扩增</td>
													</tr>
													<tr>
														<td>TP53</td>
														<td>17</td>
														<td>-</td>
														<td>-</td>
														<td>-</td>
														<td>0.6</td>
														<td>单拷贝数缺失</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="col-xs-12">
									<div class="box box-info">
										<div class="box-header with-border">
											<i class="fa fa-leaf"></i>
											<h3 class="box-title">
												基因重排分析结果
											</h3>
										</div>
										<div class="box-body ipadmini">
											<table class="table table-hover table-striped table-bordered table-condensed" style="font-size:12px;">
												<thead>
													<tr>
														<th>基因1</th>
														<th>基因1位于的染色体</th>
														<th>基因1的断点位置</th>
														<th>基因2</th>
														<th>基因2位于的染色体</th>
														<th>基因2的断点位置</th>
														<th>结构变化类型</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td colspan="7">未检测出基因重排</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>

				</div>

			</section>
		</div>
		<script src="${ctx}/lims/plugins/chartjs/echarts.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript" src="${ctx}/js/dataCenter/snpCnvTables.js"></script>
	</body>
</html>