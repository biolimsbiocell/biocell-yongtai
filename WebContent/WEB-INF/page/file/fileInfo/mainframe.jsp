<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><fmt:message key="biolims.common.documentManagement"/></title>
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/javascript/file/fileInfo/mainframe.js"></script>
</head>
<body>
	<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
	<div id="maintab" style="margin:0 0 0 0"></div>
	<div id = "markup" class="main-content">
		<input type="hidden" id="id_hid" value="${requestScope.id}">
		<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
		<div id="tabs-0"></div>
	</div>
</body>
</html>