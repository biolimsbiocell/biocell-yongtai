<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- <title>文档明细的增加</title> -->
<g:LayOutWinTag buttonId="uploadUser" title='<fmt:message key="biolims.common.uploadThePersonnel"/>' hasHtmlFrame="true" html="${ctx}/core/user/userSelect.action"
	isHasSubmit="false" functionName="zrr" hasSetFun="true" extRec="id,name"
	extStr="document.getElementById('fif_uploadUser_id').value=id;document.getElementById('fif_uploadUser_name').value=name;" />
<g:LayOutWinTag buttonId="studyDirection" title='<fmt:message key="biolims.common.researchDirection"/>' hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action" 
isHasSubmit="false" functionName="yjfx" hasSetFun="true" documentId="fif_studyDirection_id" documentName="fif_studyDirection_name" />
<g:LayOutWinTag buttonId="relatedModelType" title='<fmt:message key="biolims.common.associatedModuleType"/>' hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action" 
isHasSubmit="false" functionName="glmbt" hasSetFun="true" documentId="fif_relatedModelType_id" documentName="fif_relatedModelType_name" />
</head>

<script>

function downFiles(){
	var id = $("#fif_id").val();
	if(id){
			window.open(window.ctx + '/operfile/downloadById.action?id='+id,'','');
	
	}
}
</script>
<body>
	<div class="main-content">
		<s:form theme="simple" method="post" id="fileInfo_form">
			<table class="frame-table">
				<tr>
						<s:hidden id="fif_id" name="fif.id" ></s:hidden>
						
					<td class="label-title"><fmt:message key="biolims.common.documentName"/></td>
					<td>
						<s:textfield id="fif_fileName" name="fif.fileName" cssClass="input-40-length"></s:textfield>
					</td>
					<td class="label-title"><fmt:message key="biolims.common.associatedModuleType"/></td>
					<td>
						<s:hidden id="fif_relatedModelType_id" name="fif.relatedModelType.id"></s:hidden> 
						<s:textfield maxlength="200" id="fif_relatedModelType_name" name="fif.relatedModelType.name" cssClass="input-30-length"></s:textfield>
						<span id="relatedModelType" class="select-search-btn"></span>
					</td>
					<td class="label-title">文件</td>					
					<td>
						<input type="button" value="点击查看" onClick="downFiles();">
					</td>
				</tr>
				<tr>
					<td class="label-title"><fmt:message key="biolims.common.contentId"/></td>
					<td>
						<s:textfield maxlength="100" id="fif_modelContentId" name="fif.modelContentId" cssClass="input-40-length"></s:textfield>
					</td>
					<td class="label-title"><fmt:message key="biolims.common.researchDirection"/></td>
					<td>
						<s:hidden id="fif_studyDirection_id" name="fif.studyDirection.id"></s:hidden> 
						<s:textfield maxlength="200" id="fif_studyDirection_name" name="fif.studyDirection.name" cssClass="input-30-length"></s:textfield>
						<span id="studyDirection" class="select-search-btn"></span>
					</td>
					<td class="label-title"><fmt:message key="biolims.common.subjectHeadings"/></td>
					<td>
						<input type="text" id="fif_keyWord" name="fif.keyWord">
					</td>
				</tr>
				<tr>
					<td class="label-title"><fmt:message key="biolims.common.state"/></td>
					<td>
						<s:textfield maxlength="32" name="fif.stateName" cssClass="readonlytrue" readonly="true"></s:textfield>
					</td>
					<td class="label-title"><fmt:message key="biolims.common.uploadThePersonnel"/></td>
					<td>
					    <s:textfield id="fif_uploadUser_name" name="fif.uploadUser.name" readonly="true"></s:textfield>
					    <s:hidden id="fif_uploadUser_id" name="fif.uploadUser.id"></s:hidden> 
					    <span id="uploadUser" class="select-search-btn"></span>
					</td>
					<td class="label-title"><fmt:message key="biolims.common.uploadTheTime"/></td>
					<td>
						<input type="text" id="fif_uploadTime" class="readonlytrue" readonly="readonly" value='<s:date name="fif.uploadTime" format="yyyy-MM-dd HH:mm:ss"/> '>
					</td>
				</tr>
				<tr>
					<td class="label-title"><fmt:message key="biolims.common.documentDescribing"/></td>
					<td>
						<s:textfield id="fif_contentNote" name="fif.contentNote"></s:textfield>
					</td>
					<td class="label-title"><fmt:message key="biolims.common.relatedProjects"/></td>					
					<td>
						<s:textfield id="fif_project" name="fif.project"></s:textfield>
					</td>
					<td class="label-title"><fmt:message key="biolims.common.associatedTasks"/></td>
					<td>
						<s:textfield id="fif_projectTask" name="fif.projectTask"></s:textfield>
					</td>
				</tr>
				<tr>
					<td class="label-title"><fmt:message key="biolims.common.correlationExperiment"/></td>
					<td>
						<s:textfield id="fif_experiment" name="fif.experiment"></s:textfield>
					</td>
					<td class="label-title">版本号</td>
					<td>
						<input type="text" readonly="readonly" id="fif_versionNo" name="fif.versionNo">
					</td>
				</tr>
			</table>
			<table class="frame-table" >
				<!-- ------------文件用途说明----------------- -->
				<tr>
					<td colspan="8">
						<div class="standard-section-header type-title">
							<label><fmt:message key="biolims.common.explanationsOfPurposesFile"/></label>
						</div>
					</td>
				</tr>
			</table>
			<table>
			   	<tr>
			   		<td class="label-title"><fmt:message key="biolims.common.explanationsOfPurposesFile"/></td>
					<td>
						<s:textarea id="fif_fileNote" name="fif.fileNote" rows="10" cols="100"></s:textarea>
					</td>
			   	</tr>
			</table>
		</s:form>
	</div>
	<div id="fileUp" width="100%" height:10px></div>
</body>
</html>