<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <title>文档管理</title> -->
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/javascript/common/handleSearchForm.js"></script>
<script type="text/javascript" src="${ctx}/javascript/file/fileInfo/showFileInfoList.js"></script>
<g:LayOutWinTag buttonId="uploadUser" title='<fmt:message key="biolims.common.uploadThePersonnel"/>' hasHtmlFrame="true" html="${ctx}/core/user/userSelect.action"
	isHasSubmit="false" functionName="zrr" hasSetFun="true" extRec="id,name"
	extStr="document.getElementById('fif_uploadUser_id').value=id;document.getElementById('fif_uploadUser_name').value=name;" />
<g:LayOutWinTag buttonId="studyDirection" title='<fmt:message key="biolims.common.researchDirection"/>' hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action" 
isHasSubmit="false" functionName="yjfx" hasSetFun="true" documentId="fif_studyDirection_id" documentName="fif_studyDirection_name" />
<g:LayOutWinTag buttonId="relatedModelType" title='<fmt:message key="biolims.common.associatedModuleType"/>' hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action" 
isHasSubmit="false" functionName="glmbt" hasSetFun="true" documentId="fif_relatedModelType_id" documentName="fif_relatedModelType_name" />
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>

<body>
	<div>
		<div id="jstj" style="display: none">
			<table cellspacing="0" cellpadding="0" class="frame-iefull-table">
				<tr>
					<td class="label-title"><fmt:message key="biolims.common.code"/></td>
					<td>
						<input maxlength="32" id="sid" searchField="true" name="id" cssClass="input-40-length" readonly="true"></input>
						<img class='requiredimage' src='${ctx}/images/required.gif' /></td>
					<td class="label-title"><fmt:message key="biolims.common.documentName"/></td>
					<td>
						<input id="fileName" searchField="true" name="fileName" cssClass="input-40-length"></input>
					</td>
				</tr>
				<tr>
					<td class="label-title"><fmt:message key="biolims.common.documentName"/></td>
					<td>
						<input type="hidden" searchField="true" id="relatedModelType_id" name="relatedModelType.id"></input> 
						<input maxlength="200" id="relatedModelType_name" name="relatedModelType.name" cssClass="input-30-length"></input>
						<span id="relatedModelType" class="select-search-btn"></span>
					</td>
					<td class="label-title"><fmt:message key="biolims.common.contentId"/></td>
					<td>
						<input maxlength="100" id="modelContentId" searchField="true" name="modelContentId" cssClass="input-40-length"></input>
					</td>
				</tr>
				<tr>
					<td class="label-title"><fmt:message key="biolims.common.researchDirection"/></td>
					<td>
						<input type="hidden" id="studyDirection_id" name="studyDirection.id"></input> 
						<input maxlength="200" id="studyDirection_name" searchField="true" name="studyDirection.name" cssClass="input-30-length"></input>
						<span id="studyDirection" class="select-search-btn"></span>
					</td>
					<td class="label-title"><fmt:message key="biolims.common.subjectHeadings"/></td>
					<td>
						<input type="text" id="keyWord" searchField="true" name="keyWord">
					</td>
				</tr>
				<tr>
					<td class="label-title"><fmt:message key="biolims.common.state"/></td>
					<td>
						<input maxlength="32" id="stateName" searchField="true" name="stateName" cssClass="readonlytrue" readonly="true"></input>
					</td>
					<td class="label-title"><fmt:message key="biolims.common.uploadThePersonnel"/></td>
					<td>
					    <input id="uploadUser_name" name="uploadUser.name" readonly="true"></input>
					    <input type="hidden" id="uploadUser_id" searchField="true" name="uploadUser.id"></input> 
					    <span id="uploadUser" class="select-search-btn"></span>
					</td>
				</tr>
				<tr>
					<td class="label-title"><fmt:message key="biolims.common.uploadTheTime"/></td>
					<td>
						<input type="text" id="uploadTime" searchField="true" class="readonlytrue" readonly="readonly">
					</td>
					<td class="label-title"><fmt:message key="biolims.common.documentDescribing"/></td>
					<td>
						<input id="contentNote" searchField="true" name="contentNote"></input>
					</td>
				</tr>
				<tr>
					<td class="label-title"><fmt:message key="biolims.common.relatedProjects"/></td>					
					<td>
						<input id="project" searchField="true" name="project"></input>
					</td>
					<td class="label-title"><fmt:message key="biolims.common.associatedTasks"/></td>
					<td>
						<input id="projectTask" searchField="true" name="projectTask"></input>
					</td>
				</tr>
				<tr>
					<td class="label-title"><fmt:message key="biolims.common.correlationExperiment"/></td>
					<td>
						<input id="experiment" searchField="true" name="experiment"></input>
					</td>
					<td class="label-title"><fmt:message key="biolims.common.correlationMethod"/></td>					
					<td>
						<input id="function"  searchField="true" name="function"></input>
					</td>
				</tr>
				<tr>
					<td class="label-title"><fmt:message key="biolims.common.associatedInstruments"/></td>
					<td>
						<input id="equipment" searchField="true" name="equipment"></input>
					</td>
				</tr>
			</table>
		</div>
		<div>
			<input type="hidden" value="" id="id"><input type="hidden" id="extJsonDataString" name="extJsonDataString">
		</div>
		<div id="file_info_grid_div"></div>
	</div>
</body>
</html>