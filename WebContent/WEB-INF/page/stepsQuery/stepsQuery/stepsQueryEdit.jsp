﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- Font Awesome -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<!--<link rel="stylesheet" href="${ctx}/lims/css/layui.css" /> -->
<script type="text/javascript" src="${ctx}/biolims/layui.all.js"></script>
<script type="text/javascript" src="${ctx}/biolims/js/echarts.js"></script>
<script language="javascript" src="${ctx}/javascript/jQuery.print.js"></script>
<script  src="http://www.jq22.com/jquery/jquery-migrate-1.2.1.min.js"></script>
<style type="text/css">
.dataTables_scrollBody {
	max-height: 250px;
}

.dataTables_scrollHead {
	width: 100% !important;
}

.dataTables_scrollHeadInner {
	width: 100% !important;
}

.dataTables_scrollHeadInner>table {
	width: 100% !important;
}

.dataTables_scrollBody {
	width: 100% !important;
}
</style>
</head>

<body style="height: 94%">
	<%-- 	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarNoState.jsp"%>
	</div> --%>

	<div class="content-wrapper" id="content" style="margin-left: 0px;">

		<section class="content">

		<div class="row">
			<!--表格-->
			<div class="col-xs-12 col-md-12">
				<div class="box box-info box-solid" id="box">
					<div class="box-header with-border">
						<i class="fa fa-bell-o"></i>

						<h3 class="box-title">生产步骤查询</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" id="tableRefresh"
								onclick="tableRefresh()">
								<i class="glyphicon glyphicon-refresh"></i>
							</button>
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle"
									data-toggle="dropdown" aria-haspopup="true"
									aria-expanded="false">
									Action <span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="####" onclick="$('.buttons-print').click();">
											<!-- 打印 --> <fmt:message key="biolims.common.print" />
									</a></li>
									<li><a href="#####" onclick="$('.buttons-copy').click();">
											<!-- 复制表格数据 --> <fmt:message key="biolims.common.copyData" />
									</a></li>
									<li><a href="####" onclick="$('.buttons-excel').click();">Excel</a>
									</li>
									<li><a href="####" onclick="$('.buttons-csv').click();">CSV</a>
									</li>
									<li role="separator" class="divider"></li>
									<li><a href="####" onclick="fixedCol()"> <!-- 固定前两列 -->
											<fmt:message key="biolims.common.lock2Col" /></a></li>
									<li><a href="####" id="unfixde" onclick="unfixde()"> <fmt:message
												key="biolims.common.cancellock2Col" />
									</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div id="searchTop" style="margin-top: 10px">
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="input-group ">
								<span class="input-group-addon" style="font-family: 新宋体;">开始日期</span>
								<input id="startDate" class="form-control Wdate1 col-md-4"
									type="text" placeholder="开始时间(yyyy-MM-dd)" title="开始时间"
									format="yyyy-MM-dd" />

							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="input-group ">
								<span class="input-group-addon" style="font-family: 新宋体;">结束日期</span>
								<input id="endDate" class="form-control Wdate1" type="text"
									placeholder="结束时间(yyyy-MM-dd)" title="结束时间" format="yyyy-MM-dd" />

							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="input-group ">
								<span class="input-group-addon" style="font-family: 新宋体;">步骤名称</span>
								<input id="productHideFields" class="form-control" type="text"
									title="步骤名称" /> <input id="productHideFieldIds"
									class="form-control" type="hidden" /> <span
									class="input-group-btn">
									<button class="btn btn-info" type="button"
										onclick="showSteps()">
										<i class="glyphicon glyphicon-search"></i>
									</button>
								</span>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="input-group ">
								<button id="searchBtn" onclick="chaxun1()"
									class="btn btn-info btn-sm">查询</button>
								<span> </span>
							</div>
						</div>
					</div>
					<div class="box-body">
						<table
							class="table table-hover table-responsive table-striped table-bordered table-condensed"
							id="main" style="font-size: 14px;">
						</table>
					</div>
				</div>
				<div id="messaryReport" style="width: 100%; height: 500px;"></div>
				<!-- <div id="chart" style="width: 100%; height: 500px;"></div>-->
			</div>

		</div>

		</section>
	</div>

	<script type="text/javascript"
		src="${ctx}/js/stepsQuery/stepsQuery/stepsQueryEdit.js"></script>
</body>


</html>