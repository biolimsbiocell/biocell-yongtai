<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style type="text/css">
.dataTables_scrollBody {
	min-height: 150px;
}

.layui-layer-content .input-group {
	margin-top: 12px;
}

#fieldItemDiv .icheckbox_square-blue {
	margin-left: 10px;
}
</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
		<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
		<div class="container-fluid" style="margin-top: 55px">
			<div class="col-xs-12" style="padding: 0px">
				<div class="box box-info box-solid">
					<div class="box-header with-border">
						<i class="fa fa-bell-o"></i>
						<h3 class="box-title"><fmt:message key="biolims.common.documentManagement"/></h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" id="tableRefresh" onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Action <span class="caret"></span>
							</button>
								<ul class="dropdown-menu">
									<li>
										<a href="####" onclick="printDataTablesItem()">打印</a>
									</li>
									<li>
										<a href="#####" onclick="copyDataTablesItem ()">复制表格数据</a>
									</li>
									<li>
										<a href="####" onclick="excelDataTablesItem()">导出Excel</a>
									</li>
									<li>
										<a href="####" onclick="csvDataTablesItem ()">导出CSV</a>
									</li>
									<li role="separator" class="divider"></li>
									<li>
										<a href="####" id="fixdeLeft2" onclick="fixedCol(2)">固定前两列</a>
									</li>
									<li>
										<a href="####" id="unfixde" onclick="unfixde()">解除固定</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="box-body ipadmini">
						<!--form表单-->
						<form name="form1" id="form1" class="layui-form" method="post">
							<input type="hidden" value="<%=request.getParameter(" bpmTaskId ")%>" /> <br>
							<div class="row">
								<!-- 文件名称 -->
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message key="biolims.common.documentName"/><img
											class='requiredimage' src='${ctx}/images/required.gif' />
										</span>
										<input type="hidden" changelog="<s:property value="dif.id"/>" name="dif.id" id="id" value="<s:property value=" dif.id "/>" />
										<input type="text" id="dif_fileName" changelog="<s:property value=" dif.fileNamed "/>" name="dif.fileName" class="form-control" value="<s:property value=" dif.fileName "/>" />
									</div>
								</div>
								<!--研究方向-->
								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message key="biolims.common.researchDirection"/></span>
										<input class="form-control" type="text" size="20" readonly="readOnly" changelog="<s:property value=" dif.studyDirection.name "/>" id="dif_studyDirection_name" value="<s:property value=" dif.studyDirection.name "/>"/>
										<input type="hidden" id="dif_studyDirection_id" name="dif.studyDirection.id" value="<s:property value=" dif.studyDirection.id "/>">
												<span class="input-group-btn">
												 	<button type="button" class="btn btn-info" onclick="choseYJ();"><i class="glyphicon glyphicon-search"></i></button>
												 </span>

									</div>
								</div>
								<!--上传人-->
								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message key="biolims.common.uploadThePersonnel"/></span>
										<input class="form-control" type="text" size="20" readonly="readOnly" changelog="<s:property value=" dif.uploadUser.name "/>" id="dif_uploadUser_name" value="<s:property value=" dif.uploadUser.name "/>"/>
										<input type="hidden" id="dif_uploadUser_id" name="dif.uploadUser.id" value="<s:property value=" dif.uploadUser.id "/>">
										<span class="input-group-btn">
												 	<button type="button" class="btn btn-info" onclick="choseUpPeople()"><i class="glyphicon glyphicon-search"></i></button>
												 </span>

									</div>
								</div>

							</div>
							<div class="row">
								<!--上传时间-->
								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message key="biolims.common.uploadTheTime"/> </span>
										<input class="form-control" type="text" size="20" name="dif.uploadTime" readonly="readOnly" changelog="<s:date name="dif.uploadTime" format="yyyy-MM-dd "/>" value="<s:date name="dif.uploadTime" format="yyyy-MM-dd "/>"/>

									</div>
								</div>
								<!--文件说明-->
								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message key="biolims.common.documentDescribing"/> </span>
										<input class="form-control" type="text" size="20" name="dif.contentNote" changelog="<s:property value=" dif.contentNote "/>" value="<s:property value=" dif.contentNote "/>"/>

									</div>
								</div>
								<!--父级-->
								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message key="biolims.common.parent"/></span>
										<input class="form-control" type="text" size="20" readonly="readOnly" changelog="<s:property value=" dif.parent.name "/>" id="dif_parent_name" value="<s:property value=" dif.parent.name "/>"/>
										<input type="hidden" id="dif_parent_id" name="dif.parent.id" value="<s:property value=" dif.parent.id "/>">
										<span class="input-group-btn">
												 	<button type="button" class="btn btn-info" onclick="choseParent()"><i class="glyphicon glyphicon-search"></i></button>
												 </span>

									</div>
								</div>

							</div>
							<div class="row">
								<!--文件用途说明-->
								<div class="col-xs-12">
									<label class="input-group-addon">
										<fmt:message key="biolims.common.explanationsOfPurposesFile"/>
									</label>
										<textarea placeholder="<fmt:message key="biolims.common.placeholder"/>" name="dif.fileNote" style="width: 100%;">${dif.fileNote}</textarea>
								</div>

							</div>
							<input type="hidden" id="changeLog" name="changeLog" />
							<input type="hidden" id="documentInfoItemJson" name="documentInfoItemJson" />
						</form>
						<div style="border: 1px solid #eee;height: 1px;margin: 10px 0px;"></div>
						<div class="HideShowPanel">
							<table class="table table-hover table-striped table-bordered table-condensed" id="documentTable" style="font-size: 14px;">
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript" 
			src="${ctx}/javascript/document/documentInfo/editDocumentInfo.js"></script>
	</body>
</html>