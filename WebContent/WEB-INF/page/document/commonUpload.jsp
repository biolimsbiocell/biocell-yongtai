<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript"
	src="${ctx}/javascript/lib/jquery.fileupload.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/lib/jquery.iframe-transport.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/document/commonUpload.js"></script>
<link rel=stylesheet type="text/css" href="${ctx}/css/css-style.css">

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
</head>
<body>
<div id="upload_document_div">
	<input type="hidden" value="${requestScope.moduleType}" id="module_type">
	<input type="hidden" value="${requestScope.isUpload }" id="is_upload">
	<s:if test="#request.isUpload=='true'">
	<input type="document" name="document" id="document-upload" >
	</s:if>
	<input type="hidden" value="" id="document_id">
	<table class="frame-table" style="margin-top: 10px;">
			<tr>
				<td class="label-title td-nowrap"  style="width:60px">
					<fmt:message key="biolims.common.documentName"/>:
				</td>
				<td>
					<span id="document_name">
						<s:property value="documentInfo.documentName"/>
						&nbsp;
					</span>
				</td>
			</tr>
			<tr>
				<td class="label-title td-nowrap"  style="width:60px">
					<fmt:message key="biolims.common.uploadTheTime"/>:
				</td>
				<td>
					<span id="upload_time">
						<s:date name="documentInfo.uploadTime" format="yyyy-MM-dd HH:mm:ss"/>
						&nbsp;
					</span>
				</td>
			</tr>
		</table>
</div>
</body>
</html>